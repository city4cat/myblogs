
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


说明：  

- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
- 人物右脸对应图片左侧，人物左脸对应图片右侧。  
- 如果图片或格式有问题，可以访问[这个链接](./readme.md)  



# FC的画风-面部-耳(侧面)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96894))  

## 结构，词义辨析  
auricle n. 耳廓[^cd]  
helix: n. 耳轮[^cd]  
tragus: n. 耳屏[^imaios] [^yd]  
earlobe: n. 耳垂[^cd] [^yd]  
antiHelix: n. 对耳轮[^imaios] [^yd]  
concha: n. 耳甲[^imaios]，外耳[^cd] [^yd]  
antiTragus: n. 对耳屏[^imaios] [^yd]  

注:(?)表示该词条的翻译为本文作者的猜测、没有明确的出处。以下同。  

[^cd]: [Cambridge词典](https://dictionary.cambridge.org/zhs/词典/英语-汉语-简体/)  
[^li]: [Linguee词典](https://cn.linguee.com/中文-英语/)  
[^qq]: [腾讯翻译](https://fanyi.qq.com/)  
[^yd]: [有道翻译](https://fanyi.youdao.com/index.html)  
[^dl]: [DeepL翻译](https://www.deepl.com/)  
[^bi]: [Being翻译](https://cn.bing.com/translator)  
[^mw]: [Merriam-Webster词典](https://www.merriam-webster.com/dictionary/)  
[^tf]: [thefreedictionary](https://www.thefreedictionary.com/)  
[^imaios]: [英文IMAIOS搜索](https://www.imaios.com/en/imaios-search/(search_text)/)或[中文IMAIOS搜索](https://www.imaios.com/cn/imaios-search/(search_text)/)  
[^scid]: [SCIdict](http://www.scidict.org/items/lip%20vermilion.html)  
[^etym]: [etymonline](https://www.etymonline.com/cn/)  


## 如何描述耳  
### 资料3[^3]中的术语  
![](img/Form-of-the-Head-and-Neck_ear-parts.jpg)  

- Helix  
- Tragus  
- Earlobe  
- AntiHelix  
- Concha  
- AntiTragus  


- [^3]中给出了耳朵的大小、位置和倾斜度：  
    ![](img/FormOfTheHeadAndNeck.p69.0.jpg) 
    ![](img/FormOfTheHeadAndNeck.p69.1.jpg)  
耳朵的大小和位置可以总结为：(竖向)耳朵的长度 = (竖向)鼻下点与眉头的距离 = (横向)眉梢点与耳垂的距离  

后续使用以下方网格里的红色线段确定角色的耳朵是否符合上述标准。横向和竖向的红色线段等长。  
    ![](img/ear_distance_mark.jpg)  

- [^1]中给出的面部素描参考图，其耳朵符合标准：  
    - 青年：  
        ![](img/Drawing.The.Head.hands.p127.female.teenage_ear_distance.jpg) 
        ![](img/Drawing.The.Head.hands.p126.male.teenage_ear_distance.jpg)  

    - 成年：  
        ![](img/Drawing.The.Head.hands.p077.female_ear_distance.jpg) 
        ![](img/Drawing.The.Head.hands.p043.male_ear_distance.jpg)  


- [^2]中给出的亚洲人种(Asian)、高加索人种(Caucasian)(欧美)：  
    ![](img/ear_distance_asian.jpg) ![](img/ear_distance_caucasian.jpg)  
    亚洲人种的耳朵的水平位置基本符合标准；耳朵的竖向位置不符合标准--偏下;耳朵未显示完全，所以无法判断耳朵大小是否符合标准。  
    高加索人种的耳朵的水平位置不符合标准(不排除"眉梢化妆过长")；耳朵的竖向位置不符合标准--偏下;耳朵大小符合标准。  
 

## 角色耳朵的特点   
（待整理）  


## 部分角色的耳朵   


### 紫苑(Shion)-侧面  
每组图片的更详细信息，可参见[^4]。该角色多数只露少部分耳朵，所以无法判断耳朵大小是否符合标准。以下说的符合标准与否，指的是耳朵位置。       

- basis, 未显示耳朵，无法比较:    
    ![](../fc_drawing_style__face/side_img/face_side_shion_basis.png)    

- low-bridge, 水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_shion_low-bridge.png) 
    ![](img/face_side_shion_low-bridge_ear_distance.jpg)    

- kiss，水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_shion_kiss.png) 
    ![](img/face_side_shion_kiss_ear_distance.jpg)  

- short-nose, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_shion_short-nose.png)     

- long-nose, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_shion_long-nose.png)      


### 塩谷/盐谷(Shionoya)(紫苑男装)-侧面  
FC里该角色的侧面镜头约有96张。由于图片太多，为了减少工作量，只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  

- basis, 水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_shya_basis.png) 
    ![](img/face_side_shya_basis_ear_distance.jpg)  

- curve-bridge, 水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_shya_curve-bridge.png) 
    ![](img/face_side_shya_curve-bridge_ear_distance.jpg)  

- -se, 水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_shya_-se.png) 
    ![](img/face_side_shya_-se_ear_distance.jpg)  


### 雅彦(Masahiko)-侧面  
每组图片的更详细信息，可参见[^4]。该角色只露半个耳朵，所以无法判断耳朵大小是否符合标准。以下说的符合标准与否，指的是耳朵位置。     

- basis, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_basis.jpg)  

- +ls_+li, 耳朵的位置不一致，有符合标准的，也有水平间距小于标准的:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_+ls_+li.png) 
    ![](img/face_side_masahiko_+ls_+li_ear_distance.jpg)  

- lower-nose-tip, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_lower-nose-tip.png)  

- lower-nose-tip_+ls_+li, 基本符合标准:   
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_lower-nose-tip_+ls_+li.png) 
    ![](img/face_side_masahiko_lower-nose-tip_+ls_+li_ear_distance.jpg)   

- long-nose, 耳朵的位置不一致，有符合标准的，也有水平间距小于标准的:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_long-nose.png) 
    ![](img/face_side_masahiko_long-nose_ear_distance.jpg)  

- curved-bridge, 耳朵和眉毛的位置不统一，暂时无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_curved-bridge.png)  

- caucasian, 耳朵和眉毛有两个位置，一个基本符合标准，一个水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_caucasian.png) 
    ![](img/face_side_masahiko_caucasian_ear_distance.0.jpg) 
    ![](img/face_side_masahiko_caucasian_ear_distance.1.jpg)  

- +ls_+li_+pg_+dc, 基本符合标准:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_+ls_+li_+pg_+dc.png) 
    ![](img/face_side_masahiko_+ls_+li_+pg_+dc_ear_distance.jpg)  

- long-nose-bridge, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_long-nose-bridge.png)   

- +se, 13_190_1基本符合标准；13_190_7、06_186_1不符合，水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_masahiko_+se.png)  

- 总结： 大致分为符合标准，和水平间距小于标准。当觉得图里"眼睛和耳朵水平间距挺大"时，实际上这时是符合标准的。  



### 雅美(Masami)-侧面  
每组图片的更详细信息，可参见[^4]。该角色不露、或只露半个耳朵，所以无法判断耳朵大小是否符合标准。以下说的符合标准与否，指的是耳朵位置。    

- basis, 未显示耳朵，无法比较：  
    ![](../fc_drawing_style__face/side_img/face_side_masami_basis.png)  

- -li_-pg_-dc，水平间距小于标准:    
    ![](../fc_drawing_style__face/side_img/face_side_masami_-li_-pg_-dc.png)   

- -g_-li_-pg_-dc，水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_masami_-g_-li_-pg_-dc.png)   

- +ls_+li, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_masami_+ls_+li.png)  

- +g_-se，水平间距小于标准:    
    ![](../fc_drawing_style__face/side_img/face_side_masami_+g_-se.png)    

- -g_-se, 未显示耳朵，无法比较：  
    ![](../fc_drawing_style__face/side_img/face_side_masami_-g_-se.png)   


### 若苗空(Sora)-侧面  
每组图片的更详细信息，可参见[^4]。  
    
- basis，符合标准:    
    ![](../fc_drawing_style__face/side_img/face_side_sora_basis.png) 
    ![](img/face_side_sora_basis_ear_distance.jpg)  

- anger, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_sora_anger.png)  

- flat，水平间距小于标准:  
    ![](../fc_drawing_style__face/side_img/face_side_sora_flat.png) 
    ![](img/face_side_sora_flat_ear_distance.jpg)  

- long-bridge, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_sora_long-bridge.png)  


### 若苗紫(Yukari)-侧面  
每组图片的更详细信息，可参见[^4]。该角色不露、或只露半个耳朵，所以无法判断耳朵大小是否符合标准。以下说的符合标准与否，指的是耳朵位置。    

- basis，水平间距小于标准:    
    ![](../fc_drawing_style__face/side_img/face_side_yukari_basis.png) 
    ![](img/face_side_yukari_basis_ear_distance.jpg)  

    该组里单个图片05_073_1：  
    ![](img/face_side_yukari_ear_distance_05_073_1.jpg)  

- low-bridge, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_yukari_low-bridge.png)  

- low-bridge2, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_yukari_low-bridge2.png)   


### 浅冈叶子(Yoko)-侧面  


### 浩美(Hiromi)-侧面**


### 熏(Kaoru)-侧面**


### 江岛(Ejima)-侧面**


### 辰巳(Tatsumi)-侧面**


### 导演-侧面**
FC里该角色的侧面镜头约有28张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  

- basis：  
    ![](../fc_drawing_style__face/side_img/face_side_director_basis.png) 
    ![](img/face_side_director_basis_ear_distance.jpg)    

- 12_161_6, 未显示耳朵，无法比较:  
    ![](../fc_drawing_style__face/side_img/face_side_director_12_161_6.png)   


### 早纪(Saki)-侧面  


### 真琴(Makoto)-侧面  


### 摄像师-侧面  
  
- basis, 耳朵位置符合标准，耳朵偏大:    
    ![](../fc_drawing_style__face/side_img/face_side_cameraman_basis.png) 
    ![](img/face_side_cameraman_basis_ear_distance.jpg)  

- -g_-c_-li_-pg_-dc_-m，耳朵位置、大小符合标准：  
    ![](../fc_drawing_style__face/side_img/face_side_cameraman_-g_-c_-li_-pg_-dc_-m.png) 
    ![](img/face_side_cameraman_-g_-c_-li_-pg_-dc_-m_ear_distance.jpg)  


### 浅葱(Asagi)-侧面  


### 和子(Kazuko)-侧面  
FC里该角色的侧面镜头约有25张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  

- basis，耳朵位置符合标准，耳朵大小符合标准或偏小：  
    ![](../fc_drawing_style__face/side_img/face_side_kazuko_basis.png) 
    ![](img/face_side_kazuko_basis_ear_distance.jpg)  

- long-bridge_-sn_-ls_-li_-pg_-dc，未显示耳朵:  
    ![](../fc_drawing_style__face/side_img/face_side_kazuko_long-bridge_-sn_-ls_-li_-pg_-dc.png)   


### 爷爷-侧面  


### 横田进(Susumu)-侧面  


### 宪司(Kenji)-侧面  
FC里该角色的侧面镜头约有36张。只处理特写镜头、大画面、重要情节的镜头。可以分为以下几组：  

- basis，未显示耳朵：  
    ![](../fc_drawing_style__face/side_img/face_side_kenji_basis.png)   

- +g_+dc_+m，位置符合标准，耳朵偏大:  ：  
    ![](../fc_drawing_style__face/side_img/face_side_kenji_+g_+dc_+m.png) 
    ![](img/face_side_kenji_+g_+dc_+m_ear_distance.jpg)  

- +prn_+c_-m，耳朵大小、位置符合标准：  
    ![](../fc_drawing_style__face/side_img/face_side_kenji_+prn_+c_-m.png) 
    ![](img/face_side_kenji_+prn_+c_-m_ear_distance.jpg)  

- +g_+prn_-li_-pg_-dc_-m，位置符合标准:  
    ![](../fc_drawing_style__face/side_img/face_side_kenji_+g_+prn_-li_-pg_-dc_-m.png) 
    ![](img/face_side_kenji_+g_+prn_-li_-pg_-dc_-m_ear_distance.jpg)  


### 顺子(Yoriko)-侧面  


### 森(Mori)-侧面  


### 叶子母-侧面  


### 理沙(Risa)-侧面  


### 美菜(Mika)-侧面  
没有合适的图(侧面图太小,或张嘴).  


### 齐藤茜(Akane)-侧面  


### 仁科(Nishina)-侧面  


### 葵(Aoi)-侧面  


### 奶奶-侧面  


### 文哉(Fumiya)-侧面  


### 齐藤玲子(Reiko)-侧面  


### 松下敏史(Toshifumi)-侧面  



### 章子(Shoko)-侧面  


### 阿透(Tooru)-侧面  


### 京子(Kyoko)-侧面  
        

### 一马(Kazuma)-侧面  


### 一树(Kazuki)-侧面  


### 千夏(Chinatsu)-侧面  


### 绫子(Aya)-侧面  


### 典子(Tenko)-侧面  


### 麻衣(Mai)-侧面  


### 古屋公弘(Kimihiro)-侧面  


### 古屋朋美(Tomomi)-侧面  


### 八不(Hanzu)-侧面  


### 哲(Te)-侧面  
 






## 参考资料  
[1]. Drawing The Head & Hands, Andrew Loomis.  
[2]. [Differences between Caucasian and Asian attractive faces](https://www.researchgate.net/publication/318548459), Seung Chul Rhee, 2017.  
[3]. Form of the Head and Neck, 2021, Uldis Zarins.  
[4]. [FC的画风-面部-侧面](../fc_drawing_style__face/fc_face_side.md)

[^1]: Drawing The Head & Hands, Andrew Loomis.  
[^2]: [Differences between Caucasian and Asian attractive faces](https://www.researchgate.net/publication/318548459), Seung Chul Rhee, 2017.  
[^3]: Form of the Head and Neck, 2021, Uldis Zarins.  
[^4]: [FC的画风-面部-侧面](../fc_drawing_style__face/fc_face_side.md)

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



