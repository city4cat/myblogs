https://womenwriteaboutcomics.com/2018/01/hipster-manga-angel-heart/

Hipster Manga: Angel Heart
前卫漫画：天使心  

By Tia Kalla January 20, 2018

![](https://womenwriteaboutcomics.com/wp-content/uploads/2018/01/angelheartf.jpg)  

Although it’s only half the name of the series, “heart” is literally at the center of the long-running manga Angel Heart by Tsukasa Hojo. (Literally-literally, not figuratively-literally.) It’s both a story of a transplanted heart and the code name of a young assassin aptly named Glass Heart. After a particularly gut-wrenching kill, Glass Heart decides to end her career by ending her life. The Taiwanese mafia, keen to keep its valuable asset, steals a heart from an organ donor–the heart of a recently killed woman named Kaori Makimura. (For those who have read City Hunter: yes, that Kaori Makimura.) The memories remaining in that heart prompt Glass Heart to wake from a coma and travel to Shinjuku, Japan in search of the heart’s original owner.  
虽然"心"只是该系列的一半名称，但它确是北条司的长篇漫画《天使心》的中心所在。(字面意义上的，而不是比喻意义上的。）这既是一个关于移植心脏的故事，也是一个年轻杀手的代号，她的名字叫"玻璃心"。在一次特别令人痛心的刺杀任务之后，玻璃心决定通过结束自己的生命来结束她的职业生涯。台湾黑手党热衷于保留其宝贵的资产，从一个器官捐赠者那里偷了一颗心脏--一个名叫牧村香的最近被杀的女人的心脏。(对于那些读过《城市猎人》的人来说：是的，就是那个牧村香。）留在心脏里的记忆促使玻璃心从昏迷中醒来，前往日本新宿寻找心脏的原主人。  

In Shinjuku, Kaori’s fiance, Ryo Saeba (yes, that Ryo Saeba), retired from the quasi-legal “sweeper” business he ran with Kaori, struggles with dealing with both her death and the literal stealing of her heart. Glass Heart’s appearance into his life thrusts both his past and future together, prompting him to restart his business of protecting the peace of Shinjuku. He also adopts her, the girl with Kaori’s heart, as his daughter and gives her the name Xiang Ying. From there, the series alternates between episodes of Ryo and Xiang Ying taking jobs from the shadows of Shinjuku to help people with smarts and guns, and longer arcs exploring the pasts and presents of its cast and their relationships.  
在新宿，香的未婚夫冴羽獠（对，就是那个冴羽獠），从他和香一起经营的半合法的"委托"业务中退休了，在处理她的死和她的心脏被盗的问题上挣扎。玻璃心的出现将他的过去和未来推到了一起，促使他重新开始保护新宿和平的事业。他还收养了她--这个拥有香的心脏的女孩作为他的女儿--并给她起名为香莹。从那时起，该系列剧交替出现了獠和香莹与新宿的黑暗打交道，用智慧和武器帮助人们的情节，同时也展现了角色的过去、现在以及人物关系。  

![](https://womenwriteaboutcomics.com/wp-content/uploads/2018/01/angelheart1-300x265.png)  
Glass Heart is good at making friends.  
玻璃心善于交朋友。


English readers may recognize the name Tsukasa Hojo from the release of his most well-known manga City Hunter. Although Angel Heart uses the same characters, it is not a direct sequel, but an alternate universe story. You don’t have to have read that series to enjoy Angel Heart as backstory and details are provided (and often contradict City Hunter’s canon anyway.) But like any good AU fanfic(注：Alternate Universe fanfic), seeing what’s different and what’s the same is an extra layer of fun. And for those that read City Hunter, but didn’t enjoy certain elements, like Ryo’s immaturity or the running erection jokes, the Ryo of Angel Heart is portrayed more maturely, tempered by tragedy and the passing of time. He still has moments of immaturity and mokkori, but not nearly as relenting–less the horny rovings of a young man and more the acting out of an older man who falls into old habits to avoid confronting his feelings. It also reflects his change from the main character of City Hunter to a mentor and father to the main character of Xiang Ying. Even the art of Angel Heart is more mature compared to that of City Hunter. City Hunter’s art is sometimes messy, and leans toward the cartoonish during its comedic moments, which does fit its status as a comedy and action manga. Angel Heart, conversely, maintains the more realistic proportions and the beautiful art, especially in the faces. Even the cartoony comedy isn’t as cartoony as his older works. To me, that also reflects the maturing of Tsukasa Hojo as an artist and storyteller.
英文读者可能会从他最知名的漫画《城市猎人》的发行中认识到北条司这个名字。虽然《天使心》使用了相同的人物，但它不是直接的续集，而是一个另一个宇宙的故事。你不一定要读过那个系列才能欣赏《天使心》，因为它提供了背景故事和细节（而且往往与《城市猎人》的相矛盾）。但就像任何好的另类宇宙同人作品一样，看到什么是不同的、什么是相同的，也是一种乐趣。对于那些读过《城市猎人》，但不喜欢某些元素的人来说，比如獠的不成熟或者是勃起的笑话；因悲伤和时间流逝的影响，《天使心》中的獠被描绘得更加成熟。他仍然有不成熟的时候，但不像以前那样放任自流--没有年轻人的好色，更多是出于一个中年人为避免内心情感而落入旧习惯的行为。这也反映了他从《城市猎人》的主角转变为香莹的导师和父亲。甚至《天使心》的艺术比《城市猎人》更加成熟。《城市猎人》的艺术有时有些乱，在其搞笑时倾向于卡通化，这确实符合其作为一部喜剧和动作漫画的地位。相反，《天使心》保持了更真实的比例和美的艺术，特别是在面部。即使是卡通式的喜剧也不像他的老作品那样卡通。对我来说，这也反映了作为艺术家和讲故事者的北条司的成熟。  
![](https://womenwriteaboutcomics.com/wp-content/uploads/2018/01/angelheart2-300x218.png)  
City Hunter (left) and Angel Heart (right).
城市猎人（左）和天使之心（右）。  

Angel Heart is a series known for its action. And to be sure, it has plenty of that. It’s kind of hard not to when you’re talking about a former assassin and a sharpshooter encountering every type of violent unsavory in Shinjuku. Like Tsukasa Hojo’s other works, it also has a deft hand with the comedy, not only from the previously-established oddball cast of City Hunter, but with the new .47 Colt in the works, Xiang Ying. Xiang Ying is very good at sniping, gunfighting, and other military maneuvers, making her well-suited to the sweeper job Ryo runs. She is not very good at being an ordinary girl, which creates hilarious fish out of water moments. (“What do you mean, I shouldn’t stop a car by shooting out its engine?”) But more than those elements, Angel Heart is a story about heart (figuratively this time).  
《天使心》是一个以动作著称的连载。而且可以肯定的是，它有大量的动作。当你在讲一个前杀手和一个神枪手在新宿遇到各种类型的暴力不良分子时，很难不这样做。就像北条司的其他作品一样，搞笑手法一流，不仅来自《城市猎人》已有的奇特演员，而且还有作品中的新的.47 Colt，香莹。香莹非常擅长狙击、枪战和其他军事行动，这使她很适合獠经营的清道夫工作。她并不擅长做一个普通的女孩，这就造成了搞笑场面。("你的意思是，我不应该通过射击引擎来让车停下来？")但比起这些元素，《天使心》是一个关于心脏的故事（这次是比喻）。  


Rather than romance, Tsukasa Hojo centers family in the story, primarily through the father-daughter relationship of Ryo and Xiang Ying. It’s not a typical father-daughter story, because Ryo and Xiang Ying are not your typical father and daughter. (It’s difficult to ground your kid when she can shoot her way out of any confinement.) But putting their relationship against a background of violence, trauma, and crime highlights the normality and the strength of their bonds against an abnormal world.  
与其说是浪漫，北条司在故事中以家庭为中心，主要是通过獠和香莹的父女关系。这不是一个真正的父女故事，因为獠和香莹不是真正的父女。(当你的孩子能从任何禁锢中逃脱时，就很难将她禁锢起来）。但是，将他们的关系放在暴力、创伤和犯罪的背景下，突出了他们在一个不正常的世界中的正常性和纽带的力量。


Family is also in the stories of the blind cafe owner who adopts an orphan, the police chief who loved Kaori’s brother and watches out for Kaori’s widower Ryo, the daughter of one of Glass Heart’s victims, and the leader of the Taiwanese mafia who has his own reasons for keeping Xiang Ying alive. Angel Heart may lure readers in with a seinen-like promise of danger and dick jokes, brutality and banter. But the thing that kept readers invested in a series for sixteen years were the shining examples of humanity among the turbulence of Shinjuku. The simple, unto-death loyalty of a childhood friend, the reflections of a fellow heart transplant, the ruminations on being able to change a deadly future…  
家庭也存在于收养孤儿的盲人咖啡馆老板的故事中、爱着香的哥哥并提防獠的警察局长--玻璃心的一个受害者的女儿、以及台湾黑手党的头目--他有自己的原因要让香莹活着。《天使心》可能会以惊险和荤笑话、残暴和戏谑来吸引读者。但是，能让读者在该连载中倾注16年的，是新宿动荡中闪烁出的人性。一个童伴简单的、不惜一切代价的忠诚，一个同伴心脏移植的反思，对能够改变致命未来的思考......  

![](https://womenwriteaboutcomics.com/wp-content/uploads/2018/01/angelheart3-195x300.png)  

Manga licensing and release is often tricky. There’s any number of reasons a Japanese manga might not see an English release. (Too old, content too mature, licensing rights complications, not drawn by Hiro Mashima, etc.) It often takes me by surprise to find something I’d been reading in Japanese made it to our shores. So, uh, there may have been excited flailing and squealing when I found out that Angel Heart has actually had an (albeit very limited) official English release. In 2015, [the first thirteen chapters were released online in English](http://www.manga-audition.com/comic-zenon/angel-heart-by-tsukasa-hojo/) by the Silent Manga Audition Community, of which Hojo Tsukasa is an honorary judge. [The Silent Manga Audition](http://www.manga-audition.com/) challenges amateur manga artists and writers to write a short manga without dialogue, both as a focus on artistry and silent storytelling and also making the contest itself accessible internationally to both readers and authors.  
漫画的许可和发行往往是棘手的。一部日本漫画可能有很多原因不能在英语中发行。(太老了，内容太成熟了，许可权问题，不是由真島ヒロ画的，等等）当我发现我一直在阅读的日文作品进入我们国家时，常常会感到惊讶。所以，呃，当我发现《天使心》实际上已经有了一个（尽管只有几章）官方英文版本时，可能会有兴奋的炫耀和尖叫声。2015年，默白漫画大赛社区在网上发布了前十三章的英文版本，北条司是该社区的特邀评委。默白漫画选拔赛尝试让业余漫画家和编剧写一部没有对话的短篇漫画，既是对艺术性和无对白故事的关注，也使比赛本身在国际上为读者和作者所接受。

While Angel Heart itself is not a silent manga, seeing it released in English fits in with their goal of seeing good manga more internationally accessible. Sadly, there aren’t any releases planned beyond those thirteen chapters, and there’s no print version. But there’s clearly interest in getting Angel Heart translated and commercially released, so I hope the English-language manga publishers are paying attention. With its popularity, beauty, and heart, the only reason Angel Heart shouldn’t be on bookshelves is because it keeps flying off them.  
虽然《天使心》本身不是一部无对白漫画，但以英文发行,符合他们"目睹好的漫画作品更易国际化"的目标。遗憾的是，除了这13个章节之外，没有任何发行计划，也没有印刷版本。但是，人们显然对《天使心》的翻译和商业发行感兴趣，所以我希望英语漫画出版商能够关注。以它的知名度、美感和爱心，《天使心》应该是书架上让读者爱不释手的作品。  


