https://www.hottoys.jp/item/view/100001711.php


![](https://www.hottoys.jp/catalog/swfdata/ht3241/image/ryo-title.png)

![](https://www.hottoys.jp/catalog/swfdata/ht3241/image/ryo-01.jpg)
![](https://www.hottoys.jp/catalog/swfdata/ht3241/image/ryo-02.jpg)

![](https://www.hottoys.jp/catalog/swfdata/ht3241/image/ryo-03.png)
![](https://www.hottoys.jp/catalog/swfdata/ht3241/image/ryo-04.png)
![](https://www.hottoys.jp/catalog/swfdata/ht3241/image/ryo-03.png)

[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_1.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=1)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_2.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=2)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_3.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=3)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_4.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=4)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_5.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=5)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_6.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=6)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_7.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=7)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_8.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=8)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_9.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=9)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_10.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=10)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_11.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=11)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_12.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=12)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_13.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=13)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_14.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=14)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_15.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=15)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_16.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=16)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_17.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=17)
[![](https://www.hottoys.jp/catalog/swfdata/ht3241/imgview_image/up_18.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3241&thum_id=18)

新宿の冴羽?、危険とモッコリの日々。  
新宿的冴羽?每天都在危险中度过。  

# 【コミック・マスターピース】 『シティーハンター』１／６スケールフィギュア　冴羽 獠  
「Comic Masterpiece」『City Hunter』1/6 Scale Figure 冴羽 獠  
Comic Masterpiece - 1/6 Scale Fully Poseable Figure: City Hunter - Ryo Saeba

¥19,800 (税込)  
2012年4月発売  


## 製品紹介
产品介绍  

ホットトイズの「コミック・マスターピース」シリーズに、日本の漫画界に絶大な影響を与えた不朽の名作『シティーハンター』がラインナップ。第一弾として、美女が大好きな凄腕スイーパー、冴羽 獠の全貌をホットトイズが精巧に立体化だ。  
在hottoys的“Comic Masterpiece”系列中，加入了对日本漫画界产生巨大影响的不朽名作《城市猎人》。作为第一弹，獠的全貌精巧地立体化了，爱上美女的厉害Sweeper冴羽獠的全貌。

北条 司による『シティーハンター』は、1985年から1991年まで「週刊少年ジャンプ」に連載されたハードボイルドコメディ漫画。テレビアニメ化や映画化を果たし、現在、実写版ドラマが韓国で制作されるなど、国内外を問わず今日でも人気の高い作品である。  
北条司创作的《城市猎人》是1985年至1991年在《周刊少年Jump》上连载的硬汉喜剧漫画。完成了tv动画化和电影化，现在，真人版电视剧在韩国被制作等，无论在国内外到今天都是人气很高的作品。

法では裁く事ができない闇事件を請け負い、様々な事件を解決するシティーハンター、冴羽 獠。普段は無類のスケベで女好きだが、その実力は裏社会ではナンバー1で、超人的な銃の腕前を持っている。新宿駅東口の伝言版に書かれた「XYZ」が、彼に仕事を依頼するメッセージだ。  
承接法律无法制裁的黑暗事件，并解决各种事件的城市猎人——冴羽獠。平时是个不折不扣的色鬼，喜欢女色，但他的实力在地下社会是首屈一指的，拥有超人的枪法。新宿站东口的留言版上写着“XYZ”，是委托他工作的信息。

全高約30センチ、30箇所以上が可動するフィギュアは、強く優しい都会の戦士で遊び人という冴羽 獠の魅力を最大限に再現。ヘッドは２つ付属し、1つ目は、ワルを始末するスイーパーのクールな獠のヘッド。2つ目は、新宿のタネ馬、獠ちゃんを再現できるモッコリ顔のヘッドだ。獠の魅力的な表情を再現するために、一つひとつがハンドペイントで塗装が施されている。コスチュームは、シルエットまで忠実な仕上がりで、水色のジャケットと着せ替え用のロングコートが付属。ミリタリーアイテムを得意とするホットトイズならではの技術力で、ショルダーホルスターに収納できる愛用銃コルト・パイソン357マグナムや狙撃用のM1カービン・ライフルの武器も細部まで忠実な仕上がり。またアクセサリーとして、モッコリ獠ちゃん時をより楽しめる冴子のレースブラをはじめ「IPPATSU」ポーズなどの差し替えハンドパーツが付属。ヒーローの冴羽 獠だけでなく三枚目の獠ちゃんまでも演出できるのは嬉しいポイントだ！  
全高约30厘米，可移动30处以上的人偶，最大限度地再现了强悍温柔的都会战士兼花花公子的冴羽獠的魅力。附属两个獠头，第一个是，清理瓦尔的扫头酷獠头。第二个，是可以再现新宿的种马、獠张的大头。为了再现獠的魅力表情，逐一用手漆进行涂饰。服装，连轮廓忠实的完成，水色的夹克和换装用的长大衣附属。以擅长军事道具的hottoyz特有的技术力，能收纳在肩架的爱用枪柯尔特·派森357马格南和狙击用的M1卡尔文步枪的武器细节也忠实完成。另外，作为装饰品，獠子的花边胸罩等“IPPATSU”姿势等替换手零件也附属于獠子，能让獠时更开心。不仅是英雄冴羽獠，连第三张的獠酱也能演出，是令人高兴的点!

ホットトイズが、『シティーハンター』に対するリスペクトの想いを込めてお贈りする、冴羽 獠の希少なハイエンド立体化。同時発表された美貌の女刑事、[野上 冴子](http://www.hottoys.jp/item/view/100001712)と共に手に入れて、名作『シティーハンター』の魅力的な世界感を楽しもう！  
hottoys，怀着对《城市猎人》的尊敬之情赠送，冴羽獠的稀有高端立体化。与同时被发表了的美貌的女刑警，野上冴子一起到手，享受名作“城市猎人”的魅力的世界感吧!  

※当アイテムは、日本と香港のみ発売いたします。Available for sale in Japan and Hong Kong only.  
※本商品，只在日本和香港发售。Available for sale in Japan and Hong Kong only。  

## 製品スペック
产品规格  

<table class="spec" summary="spec">
<tbody><tr>
<!--------メーカー--------->
<th abbr="maker">Maker</th>
<td>
Hottoys
</td>
</tr>
<!--------シリーズ番号--------->
<tr>
<th abbr="series_number">Series番号</th>
<td>CM#02</td>
</tr>
<!--------スケール--------->
<tr>
<th abbr="scale">Scale</th>
<td>1/6 Scale</td>
</tr>
<!--------サイズ--------->
<tr>
<th abbr="size">Size</th>
<td>高約30cm</td>
</tr>
<!--------重さ--------->
<tr>
<th abbr="weight">重</th>
<td>約400g（仅实体）、約1kg（包含包装）</td>
</tr>
<!--------可動ポイント--------->
<tr>
<th abbr="movable">可動Point</th>
<td>30个可移动点</td>
</tr>
<!--------電池--------->
<!--------パッケージ--------->
<tr>
<th abbr="package">Package</th>
<td>Flip Top Window Box</td>
</tr>
<!--------パッケージサイズ--------->
<tr>
<th abbr="package_size">Package Size</th>
<td>約W24×H36×D12（cm）</td>
</tr>
<!--------生産情報--------->
<tr>
<th abbr="limit">生産情報</th>
<td>限定生産</td>
</tr>
<!--------流通方法--------->
<tr>
<th abbr="distribution">流通方法</th>
<td>一般販売</td>
</tr>
<!--------製品種別--------->
<tr>
<th abbr="kind">製品種別</th>
<td>High-end １／６ Scale可動式Figure</td>
</tr>
<!--------付属品(武器)--------->
<tr>
<th abbr="attachments_weapon">付属品（武器）</th>
<td>Colt Python 357 Magnum（附带holster(枪套)）、M1 Carbine Rifle </td>
</tr>
<!--------付属品(アクセサリー)--------->
<tr>
<th abbr="attachments_accessories">付属品（Accessory）</th>
<td>Mokkori表情的替换用头、冴子的Lace Bra、 Coat、手部替换零件（x6）</td>
</tr>
<!--------スペシャル機能--------->
<!--------発売元--------->
<!--------販売元--------->
<!--------発売元・販売元--------->
<tr>
<th abbr="sale2">発売元・販売元</th>
<td>株式会社Hottoys Japan</td>
</tr>
<!--------バーコード番--------->
<tr>
<th abbr="barcode">条形码号</th>
<td>4897011174068 </td>
</tr>
<!--------制作スタッフ--------->
<tr>
<th abbr="staff">制作Staff</th>
<td>造形師：林 浩己<br>Chief造形師：ユリ<br>Chief Painter：ＪＣ・ホン<br>衣装製作：High Rim</td>
</tr>
</tbody></table>

## 作品紹介

### 『City Hunter』

北条 司による『シティーハンター』は、1985年から1991年まで「週刊少年ジャンプ」に連載されたハードボイルドコメディ漫画。テレビアニメ化や映画化を果たし、現在は徳間書店刊行の「コミックゼノン」に本作のパラレルワールドを描いた『エンジェル・ハート2ndシーズン』が好評連載中。日本の漫画に絶大な影響を与えた不朽の名作として、現在、実写版ドラマが韓国で制作されるなど、国内外を問わず今日でも人気の高い作品である。  
北条司创作的《城市猎人》是1985年至1991年在《周刊少年Jump》上连载的硬汉喜剧漫画。完成了TV动画化和电影化，现在在德间书店发行的《Comic Zenon》上，描写本作品的平行世界的《天使心 2nd季》好评连载中。作为对日本漫画产生巨大影响的不朽名作，现在，真人版电视剧在韩国被制作等，无论在国内外到今天都是人气很高的作品。

新宿駅東口の伝言版に書かれた仕事依頼のメッセージ「XYZ」。それは追いつめられた人間が記したSOS。法では裁く事ができない闇事件を請け負い、依頼人たちと触れあい、心の傷を癒しながら依頼を完遂する獠と香。そんな彼らを人々はシティーハンターと呼んだ。  
新宿站东口的留言版上写着工作委托的信息“XYZ”。那是被逼上绝路的人写下的SOS。承包法律无法制裁的黑暗事件，与委托人接触，一边治愈心灵创伤一边完成委托的獠和香。人们称他们为城市猎人。

[http://www.comic-zenon.jp/](http://www.comic-zenon.jp/)

© 北条司/NSP 1985　版権許諾証AA-400  
Prototype shown, final product appearance and colors may vary.  
画像は試作品のため実際の製品と異なる場合があります。  
簡単な組み立て作業を必要とする場合があります。  
対象年齢は15歳以上。  
图像可能与实际产品不同，因为它是原型。  
可能需要简单的装配工作。  
适用年龄为15岁以上。  