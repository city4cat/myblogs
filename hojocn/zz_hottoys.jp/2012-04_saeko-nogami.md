https://www.hottoys.jp/item/view/100001712.php


![](https://www.hottoys.jp/catalog/swfdata/ht3242/image/saeko-title.png)

![](https://www.hottoys.jp/catalog/swfdata/ht3242/image/saeko-01.jpg)
![](https://www.hottoys.jp/catalog/swfdata/ht3242/image/saeko-02.jpg)

![](https://www.hottoys.jp/catalog/swfdata/ht3242/image/saeko-03.png)
![](https://www.hottoys.jp/catalog/swfdata/ht3242/image/saeko-04.png)
![](https://www.hottoys.jp/catalog/swfdata/ht3242/image/saeko-03.png)

[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_1.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=1)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_2.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=2)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_3.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=3)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_4.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=4)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_5.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=5)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_6.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=6)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_7.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=7)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_8.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=8)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_9.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=9)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_10.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=10)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_11.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=11)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_12.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=12)
[![](https://www.hottoys.jp/catalog/swfdata/ht3242/imgview_image/up_13.jpg)](https://www.hottoys.jp/catalog/flash/imgview.php?product_id=ht3242&thum_id=13)

冴子お姉さんのポリス・ハンマーにご用心！  
小心冴子姐姐的警锤!  

# 【コミック・マスターピース】 『シティーハンター』１／６スケールフィギュア　野上 冴子
「Comic Masterpiece」『City Hunter』 1/6 Scale Figure 野上 冴子  
Comic Masterpiece - 1/6 Scale Fully Poseable Figure: City Hunter - Saeko Nogami

¥19,800 (税込)
2012年4月発売



## 製品紹介
产品介绍  

ホットトイズの「コミック・マスターピース」シリーズに、日本の漫画界に絶大な影響を与えた不朽の名作『シティーハンター』がラインナップ。第二弾として、警視総監を父にもつ美貌の女刑事、野上 冴子をホットトイズが精巧に立体化だ。  
在Hottoys的“Comic Masterpiece”系列中，加入了对日本漫画界产生巨大影响的不朽名作《城市猎人》。在第二部中，将父亲是警视总监的美貌女刑警野上 冴子巧妙地立体化了。

北条 司による『シティーハンター』は、1985年から1991年まで「週刊少年ジャンプ」に連載されたハードボイルドコメディ漫画。テレビアニメ化や映画化を果たし、現在、実写版ドラマが韓国で制作されるなど、国内外を問わず今日でも人気の高い作品である。  
北条司创作的《城市猎人》是1985年至1991年在《周刊少年Jump》上连载的硬汉喜剧漫画。完成了tv动画化和电影化，现在，真人版电视剧在韩国被制作等，无论在国内外到今天都是人气很高的作品。

野上 冴子は、警視庁特捜課の敏腕女刑事でシティーハンター冴羽 獠の天敵。獠を色気で釣って手玉に取り、警察では捜査不能の危険な仕事を押し付けている。あざやかなナイフさばきと、妖艶な色仕掛けを得意とする冴子は、「警視庁の女豹」の異名を持つ。  
野上 冴子是警视厅特别搜查课的干练女刑警、城市猎人冴羽獠的天敌。以暧昧手段将獠招致獠，将警察无法调查的危险工作强加给他。冴子擅长犀利的刀法和妖艳的性感，绰号“警视厅女豹”。

全高約28センチ、28箇所以上が可動するフィギュアは、北条 司が描く美しい女性の代表的存在である野上 冴子をハイエンド立体化。冴子の持つしなやかな肢体とセクシーな魅力を、余すことなく再現している。体のラインを強調するジャケットとスリットが入ったスカートは、ディテールや素材にこだわり、全体のシルエットまで忠実な仕上がり。ヘッドパーツは、美しさと気丈さを併せ持つ冴子の魅力的な表情を再現するために、一つひとつがハンドペイントで塗装が施されている。ガーターベルトに装着したホルダーに収納できる愛用のナイフはもちろんのこと、ワルサーPPK/SやS&W M60チーフスペシャルの武器が付属。そして本アイテム最大の魅力は、『シティーハンター』を語る上で欠かすことのできないアンチ・モッコリの武器、100ｔハンマー（ポリス版）が付属している点だ。同時発表の[冴羽 獠](http://www.hottoys.jp/item/view/100001711.php)と併せてディスプレーすれば、依頼報酬モッコリの貸し借りをめぐって、?と駆け引きするシーンを再現することができる。  
全高约28厘米，可动28处以上的手办，是北条司笔下美丽女性的代表人物野上 冴子的高端立体化。冴子的柔美的肢体和性感的魅力，毫无保留地再现了出来。强调身体曲线的夹克和开叉的裙子，在细节和素材上都很讲究，连整体的轮廓也忠实完成。头部部分，为了再现兼具美丽和刚强的冴子充满魅力的表情，每一个都用手漆进行了涂饰。安装在吊带上的支架上可以收纳的爱刀自不必说，附带武器Walther PPK/S和S&W M60Chief Special。而且本道具最大的魅力在于，附带了“城市猎人”不可缺少的反Mokkori的武器--100tHammer(Police版)。如果与同时发售的冴羽獠一起展示的话，可重现双方为借出和借入委托报酬Mokkori而讨价还价的场面。

ホットトイズが、『シティーハンター』に対するリスペクトの想いを込めてお贈りする、綺麗なお姉さん野上 冴子。名作『シティーハンター』からの今後のラインナップも目が離せない。  
Hottoyz带着对《城市猎人》的尊重，送给您漂亮的姐姐野上 冴子。名作《城市猎人》之后的阵容也令人瞩目。

※当アイテムは、日本と香港のみ発売いたします。Available for sale in Japan and Hong Kong only.  
※本商品，只在日本和香港发售。Available for sale in Japan and Hong Kong only。  

## 製品スペック
产品规格

<table class="spec" summary="spec">
<tbody><tr>
<!--------メーカー--------->
<th abbr="maker">Maker</th>
<td>
Hottoys
</td>
</tr>
<!--------シリーズ番号--------->
<tr>
<th abbr="series_number">Series番号</th>
<td>CM#03</td>
</tr>
<!--------スケール--------->
<tr>
<th abbr="scale">スケール</th>
<td>1/6 Scale</td>
</tr>
<!--------サイズ--------->
<tr>
<th abbr="size">Size</th>
<td>高約28cm</td>
</tr>
<!--------重さ--------->
<tr>
<th abbr="weight">重</th>
<td>約320g（仅实体）、約920g（包含包装）</td>
</tr>
<!--------可動ポイント--------->
<tr>
<th abbr="movable">可動Point</th>
<td>28个可移动点</td>
</tr>
<!--------電池--------->
<!--------パッケージ--------->
<tr>
<th abbr="package">Package</th>
<td>Flip Top Window Box</td>
</tr>
<!--------パッケージサイズ--------->
<tr>
<th abbr="package_size">Package Size</th>
<td>約W25×H36×D12（cm）</td>
</tr>
<!--------生産情報--------->
<tr>
<th abbr="limit">生産情報</th>
<td>限定生産</td>
</tr>
<!--------流通方法--------->
<tr>
<th abbr="distribution">流通方法</th>
<td>一般販売</td>
</tr>
<!--------製品種別--------->
<tr>
<th abbr="kind">製品種別</th>
<td>High-end １／６ Scale可動式Figure</td>
</tr>
<!--------付属品(武器)--------->
<tr>
<th abbr="attachments_weapon">付属品（武器）</th>
<td>Walther PPK/S、S&amp;W M60 Chief Special（附带holster(枪套)）、投掷刀（x6、附带holder（枪柄）） </td>
</tr>
<!--------付属品(アクセサリー)--------->
<tr>
<th abbr="attachments_accessories">付属品（Accessory）</th>
<td>100ｔHammer（Police版）、手铐、替换用Hand Parts（x4）</td>
</tr>
<!--------スペシャル機能--------->
<!--------発売元--------->
<!--------販売元--------->
<!--------発売元・販売元--------->
<tr>
<th abbr="sale2">発売元・販売元</th>
<td>株式会社Hottoys Japan</td>
</tr>
<!--------バーコード番--------->
<tr>
<th abbr="barcode">条形码号</th>
<td>4897011174075</td>
</tr>
<!--------制作スタッフ--------->
<tr>
<th abbr="staff">制作Staff</th>
<td>造形師：林 浩己<br>Chief造形師：ユリ<br>Chief Painter：ＪＣ・ホン<br>衣装製作：High Rim</td>
</tr>
</tbody></table>

## 作品紹介


### 『City Hunter』

北条 司による『シティーハンター』は、1985年から1991年まで「週刊少年ジャンプ」に連載されたハードボイルドコメディ漫画。テレビアニメ化や映画化を果たし、現在は徳間書店刊行の「コミックゼノン」に本作のパラレルワールドを描いた『エンジェル・ハート2ndシーズン』が好評連載中。日本の漫画に絶大な影響を与えた不朽の名作として、現在、実写版ドラマが韓国で制作されるなど、国内外を問わず今日でも人気の高い作品である。  
北条司创作的《城市猎人》是1985年至1991年在《周刊少年Jump》上连载的硬汉喜剧漫画。完成了tv动画化和电影化，现在在德间书店发行的《Comic Zenon》上，描写本作品的平行世界的《天使心2ndSeason》好评连载中。作为对日本漫画产生巨大影响的不朽名作，现在，真人版电视剧在韩国被制作等，无论在国内外到今天都是人气很高的作品。

新宿駅東口の伝言版に書かれた仕事依頼のメッセージ「XYZ」。それは追いつめられた人間が記したSOS。法では裁く事ができない闇事件を請け負い、依頼人たちと触れあい、心の傷を癒しながら依頼を完遂する獠と香。そんな彼らを人々はシティーハンターと呼んだ。  
新宿站东口的留言版上写着工作委托的信息“XYZ”。那是被逼上绝路的人写下的SOS。承包法律无法制裁的黑暗事件，与委托人接触，一边治愈心灵创伤一边完成委托的獠和香。人们称他们为城市猎人。

[http://www.comic-zenon.jp/](http://www.comic-zenon.jp/)

## Legal

© 北条司/NSP 1985　版権許諾証AA-400  
Prototype shown, final product appearance and colors may vary.  
画像は試作品のため実際の製品と異なる場合があります。  
簡単な組み立て作業を必要とする場合があります。  
対象年齢は15歳以上。  
图像可能与实际产品不同，因为它是原型。  
可能需要简单的装配工作。  
适用年龄为15岁以上。  