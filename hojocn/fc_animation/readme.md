
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 



# FC动图  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96860))  

机器学习现在已经可以做到这种程度了：给定一张人物照片，生成人物的小幅动作。(https://mp.weixin.qq.com/s/B4ag3GHhBIwge19veNj4IA)

工具链接：https://www.myheritage.com/deep-nostalgia  
其有App可用：MyHeritage。  
缺点是需要注册，且只允许免费处理几张图片。

使用该工具生成了两个FC图片的动画：  
若苗紫苑:    
![](img/MyHeritage_Shion.gif)  

若苗紫:  
![](img/MyHeritage_yukari_anim05_2.gif)  

个人感受：  
- 人物动作幅度稍大就有很明显的瑕疵。  
- 测试了FC里的几张黑白图片，大部分无法识别出人脸。  
    
---

- 以下使用Avatarify App制作的。可以处理黑白图片；没有数量限制。  
![](img/Avatarify/shion_wow.gif) 
![](img/Avatarify/shion_blink_pufuu.gif) 
![](img/Avatarify/shion_wow2.gif) 
![](img/Avatarify/shion_blink.gif) 
![](img/Avatarify/yukari_blink.gif) 
![](img/Avatarify/yukari_blink_pufuu.gif)  



**不同卡通风格的图片**  

由Voila APP生成的不同卡通风格的FC图片：  
![](img/app_voila1.jpg) 
![](img/app_voila2.jpg) 
![](img/app_voila3.jpg) 
![](img/app_voila4.jpg) 
![](img/app_voila5.jpg) 
![](img/app_voila6.jpg) 
![](img/app_voila7.jpg) 


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
