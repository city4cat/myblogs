source:  
https://ekizo.mandarake.co.jp/auction/item/itemInfoEn.html?index=570422


# Tsukasa Hojo Hand-Drawn Color Shikishi "Angel Heart"

[![](./img/0001698966-2239752903.jpg)](https://img.mandarake.co.jp/aucimg/8/9/6/6/0001698966.octet-stream) 
[![](./img/0001698968.jpg)](https://img.mandarake.co.jp/aucimg/8/9/6/8/0001698968.octet-stream) 
[![](./img/0001698969.jpg)](https://img.mandarake.co.jp/aucimg/8/9/6/9/0001698969.octet-stream)  
（注：点击查看大图）  


## 图片中的文字及译文  
読者各位  

平素は「週刊コミックバンチ」に格別のご高配を賜り、厚く御礼申し上げます。  
またこのたびは、「バンチスペシャルお年玉」にご応募をいただき、まことにありがとうございました。  
厳正な抽選の結果、あなたが当選しましたので、賞品をお送りいたします。  
今後とも、「週刊コミックバンチ」へのご支援、何とぞよろしくお願いいたします。  

平时承蒙特别关照「周刊Comic Bunch」，在此深表谢意。  
另外这次，收到「Bunch Special新年紅包」的应募，真的非常感谢。  
经过严格的抽奖，您中奖了，所以将奖品寄给您。  
今后也请多多支持「周刊Comic Bunch」。  

2002年2月
(株)新潮社


## Informatoin  
Current Price:560,000 yen
	
Start Price: 30,000 yen
	
Your Maximum Bid:  0 yen

Final Item Price (may be displayed as 0 if not the winning bidder):   0 yen

No. of Bids:   112

No. of Watchers:  94

Time Left:  Closed

Start Time:  2018/04/20 00:00:00

Live Time:  2018/05/09 21:21:58

2018/05/09 05:21:58

Item Number:  5148z86

Auction Style:  LIVE EVENT

Bid Increments [Info]:  10,000 yen

Bidding has ended

Item's Details:  
(item number) 5148  
(title) Tsukasa Hojo Hand-Drawn Color Shikishi "Angel Heart"  
(size) 240 x 270 mm  
(side note) Present page copy winning notice  
(condition) good  
(starting bid price) 30,000 yen  

(comment) "Weekly comic bunch" 2002 Lottery prize winning item. As you can see from the gift page, it is published as a great item of superball. The thought for Prof. Hojo's readers is drawn exactly  

Translation is done by machine translator.  
Item Information  

Size (Package Size)  
    2 mm × 240 mm × 270 mm  

Weight  
    105 g  


## References

https://ocr.space/


