

Do NOT modify files/sub-folders under the public folder:  
$(ProjectRootDir)/public     


Instead, modify files/sub-folders under the folder:  
$(ProjectRootDir)/hojocn/fc_drawing_style__face_composition/web,  

Then copy all files/sub-folders under that folder into public folder:  
$(ProjectRootDir)/public   
