source:  
https://koalasplayground.com/2011/03/29/a-koalas-guide-to-the-best-japanese-mangas/


# A Koala's Guide to the Best Japanese Mangas - A Koala's Playground

March 29, 2011  by  ockoala

Mangas, or Japanese comic books, were my first source of entertainment. Before I watched my first TV show, I read my first manga. More than a quarter of a century’s worth of manga reading has made this medium my most knowledgeable subject matter (yes, it may appear I know a lot about dramas, but that is just an illusion). The amount of mangas my siblings and I own would open a bookstore, and a pretty big one at that. 

Mangas really reached the peak of their mass appeal in the 80s and 90s. I can trace the decline of quality and the rise of commercial packaging to around the time **Yuu Watase**‘s ***Fushigi Yuugi*** hit the stage in the late 90s. It was a fast and addicting read, but when I finished it, I realized that it was quite contrived, pedestrian, and maddenly lame.

Allow me an analogy – if teen lit used to be represented by such masterpieces such as ***Lord of the Rings*** and ***His Dark Materials*** trilogies, when the dreck that was ***Twilight*** emerged to represent the genre, suddenly your heart and soul feels empty. Watching people embrace mediocrity made me want to weep.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/touch.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/touch.jpg)

When endodo4ever asked me for a list of what I considered the best mangas I have ever read (out of hundreds of titles), I knew this was a chance for me to commemorate the truly exceptional mangas that left their marks on that literary genre. I’ve read both shonen (boys) and shoujo (girls) mangas, though my list is skewered predominantely towards shoujo (as if my Hello Kitty affliction doesn’t give my girly preferences away).

I have decided not to rank the mangas, but have merely compiled the best of the best, with a blurb about each story to whet your appetites. If the story sounds interesting, I highly suggest you read the more detailed synopsis on Wikipedia. If available, I’ve included the Japanese, English and Chinese titles of each manga for ease of reference, since it’s title may be different depending on which language is the source of your manga crack.

**1. Glass no Kamen (The Glass Mask ) 千面女郎 by Miuchi Suzue**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/gnk1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/gnk1.jpg)

One of the longest running mangas of all time, serialized starting in January of 1976 and still going on today, now up to 44 tankobon (individual) volumes. That’s a lot of manga, people, with a story that my sister and I often wonder whether the author might croak before finishing it. Morbid, yes, but totally probable considering her snail pace in continuing her story in the last decade.

***Glass no Kamen*** is an epic theatrical saga of a young girl named Maya who is an acting savant. Her desire, dedication, and innate talent takes her on a journey from minor actress to one of the leading stage performers of her time. Her rival Ayumi is the daughter of a famous actress, and a genuine talent in her own right. The two girls strive to become the one chosen as the leading actress to perform the legendary stage play Kurenai Tennyo or The Crimson Goddess.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/gnk2.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/gnk2.jpg)

This series is amazingly crafted, taking Maya and Ayumi from dueling stage performances, to television actress, to indie plays, to national performances, and so on and so forth. Each opportunity and each performance take the girls on a parallel journey to the same goal, which only one girl can attain.

The illustrations are wonderfully drawn, teaching me about theater and plays from age 6 when I started reading GnK. Truly one of the most sweeping stories ever constructed (it’s ballet cousin would be ***Swan***, which I will discuss further down) about the subject of acting. The love stories are developed slowly in the background, and actually are the least interesting part. You read it for the carefully constructed acting roadblocks, and how Maya and Ayumi hurdle past each one through hard work and perseverance.

My favorite segments are when teenage Maya and Ayumi do alternate-night performances as Helen Keller in The Miracle Worker and later when Maya and Ayumi are cast respectively as the angelic younger princess and the dark and tormented elder princess in the national production of The Two Princesses. I cannot stress enough the breadth and depth of GnK, and how it is a genuine labor of love and a lifetime’s devotion from the author to us, the readers.

**2. Candy Candy 甜甜 by Igarashi Yumiko**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Candy%20Candy/candy-candy-wallpaper-big.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Candy%20Candy/candy-candy-wallpaper-big.jpg)

Probably one of the, if not THE, most well-known shoujo mangas to come out of Japan. ***Candy Candy*** likely changed the lives of many a girl (and guy) who read the manga and/or watched the anime during the formative years of their childhood. Heck, it’s theme song shows up randomly in dramas from time to time. This was the manga which turned me into a manga addict. I wrote an entire post about it [here](https://koalasplayground.com/2010/06/19/how-candy-candy-taught-me-about-dramas-2/), so I shant repeat myself again. Truly a masterpiece that symbolizes what shoujo manga can and should be about.

**3. Basara / 7 Seeds 幻海奇情 by Tamura Yumi**

[![](http://i966.photobucket.com/albums/ae145/ockoala/7%20Seeds/basara.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/7%20Seeds/basara.jpg) [![](http://i966.photobucket.com/albums/ae145/ockoala/7%20Seeds/7-seeds-vol-6-de-tamura-yumi-737804.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/7%20Seeds/7-seeds-vol-6-de-tamura-yumi-737804.jpg)

***Basara*** is one of the most harrowing and soul-rending mangas I have ever read. It’s story and execution straddles both shoujo (with an epic love story that makes Romeo and Juliet seem like child’s play) and shonen (with battles and wars fought to determine the fate of a country).

The story takes place in a future Japan, reduced to a pre-modern state by a catastrophe at the end of the 21st century. Sarasa, our heroine, has twin brother Tatara, who is prophesied to be the “child of destiny” who will bring back the country’s independence and stop the tyrannical rule of the despotic Golden Emperor. When Tatara is killed, Sarasa pretends to be him in order to keep the downtrodden from losing hope. She crosses paths with a young man named Shuri, and they fall in love.

![](http://i966.photobucket.com/albums/ae145/ockoala/7%20Seeds/untitled-1.jpg "7")

Unbeknownest to either of them, Shuri is the annointed Red King, the youngest son of the Golden Emperor (who has given all his children a territory to self-govern and a title, hoping they would destroy each other and never challenge him for the throne in succession). Shuri has sworn to destroy the revolutionaries led by Sarasa and avenge the death of his beloved cousin.

You can imagine what hell these two have to go through to be together. The manga spawns 27 books, and is split into Part I and Part II, with Part I ending in such a gutwrenching way I almost was too scared to keep reading. And that is a hallmark of a well-constructed story.

After Basara, I thought there was no way in hell Tamura Yumi could top that manga, or even follow up with something even close to as good. I was wrong. She followed up with ***7 Seeds*** (currently up to book 17), and it is already as good as Basara. Depending on how 7 Seeds finishes, I will make a determination at such time. I wrote about 7 Seeds before, click [here](https://koalasplayground.com/2010/06/28/7-seeds/) and [here](https://koalasplayground.com/2010/06/29/7-seeds-reading-links-otp-speculations/) to read a plot synopsis and why it’s such a mindblowingly stellar story.

**4. Touch 鄰家女孩 / Rough 我愛芳鄰 by Adachi Mitsuru**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/touch4.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/touch4.jpg) [![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/touch2.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/touch2.jpg)

Adachi is a prolific manga writer, and his subject matter is mainly one genre and one genre only – the sports coming-of-age story. Within that genre, he picks baseball as his most frequent sport, followed by boxing, and occasionally swimming and track. I’ve selected what I consider to be his best two works to highlight here, but some of his other works are better than 99% of the manga out there (such as ***H2***, ***Katsu***, ***Cross Game***, ***Slow Step***, and ***Niji Iro Togarashi).***

Adachi can be considered the best pure storyteller in manga. He is a master of using quiet action to convey the emotion and propel the story, and an expert at crafting subtle yet loaded dialogue. His stories are always humanistic and about the everyday details, never bombastic or frantic. Reading his mangas is a calming experience, akin to floating on a lazy river and soaking in the atmosphere.

***Touch*** is Adachi’s most famous and iconic work. It tells the story of a pair of twin brothers, Tatsuya and Kazuya, and Minami, the girl next door who grows up with them. Both boys love Minami, yet elder twin Tatsuya lets his beloved earnest younger twin Kazuya pursue her diligently as he takes a back seat, until he is forced to acknowledge his pitching talent and make a once-in-a-lifetime attempt to win the high school baseball championship at Koshien. Touch made me cry my eyes out (I cannot reach book 7 without crying even today), and likely added to my love of sports (yes, I am a huge sports fan and know my teams and stats, in case anyone wants to talk shop with me).

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/rough-1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/rough-1.jpg)

But it is his followup manga ***Rough*** that is my personal favorite Adachi manga. Rough is an adorable boarding school story with the hero being a talented high school freestyle swimmer. He meets his childhood sweetheart again, who also happens to be the daughter of his family’s rival Japanese snack store and a talented diver. The two kids reconnect and find that sometimes love cannot be stopped and stories can have different endings.

Rough and Touch (and almost all of Adachi’s manga – with the exception of Niji Iro Togarashi which is set in a fictional Edo period Japan) chroncicle high schoolers who have dreams, fears, doubts, and talents, depicting them in delicate real life settings that are heartwarming to read. Adachi loves the first love stories, and he elevates both in Touch and Rough so that you finish reading it and wish to god that you had a neighborhood oppa or best guy friend growing up.

**5. Inuyasha 犬夜叉/ Urusei Yatsura 福星小子/ Maison Ikkoku 相聚一刻 by Takahashi Rumiko**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/uy.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/uy.jpg)

If I had to pick my top five favorite manga writers, without a doubt Takahashi would be on the list. Her talent is undisputed, and her imagination unparallel. Her full-length mangas are typically a blend of fantasy and cultural creations that can only come from her fertile mind. I’ve listed what I consider her most famous and best works, but her ***Ranma 1/2*** was also exceedingly successful and a genuine treat to read, even if I find it less substantive and long-lasting a work.

***Urusei Yatsura*** may just be the one manga I have re-read the most times. I got lucky, because I scored a collectors edition set that has all the translator annotations that make every single inside joke understandable. The series tells the story of Ataru, a lazy, perverted high-school boy who finds himself saving earth when he wins a race against an alien princess and prevents the alien race from subjugating the humans.

![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/uy3.jpg "1")

Luckily or unluckily for Ataru, the beautiful and short-tempered alien princess Lum falls in love with him, and moves in to stake her claim on this sorry excuse for a young man. What ensues is 34 volumes of the most hilarious, side-splitting, witty vignettes I have ever read in my entire life, with some of the wackiest but most loveable side-characters ever created (which is a Takahashi specialty).

Sadly, Urusei is influenced heavily on Japanese culture and language, and many of the jokes and slap-stick situations in the manga won’t be funny unless its parody is explained to the non-Japanese reader. My set has those annotations, and if there was ever a fire, I’d grab my kids and my set of Urusei Yatsura. The moment I feel bored, I read one quick chapter and I am chuckling and giggling like a loon. So. Cute.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/iy.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/iy.jpg)

***Inuyasha*** is Takahashi’s latest epic, and her most sweeping story to date. It tells of a young high school girl Kagome who gets transported back to the Sengoku period. There she meets a half-demon (the son of a dog demon and a human woman) Inuyasha, and together with other allies, they must travel the war-torn land to find fragments of a mystical jewel that is needed to defeat a powerful evil entity.

The story is collected in 57 books, is genuinely intense, sweeping, and dramatic. It is also Takahashi’s most romantic book, with the creation of a love triangle that spans death and reincarnation. The hero is one of my favorite characters of all time, the short-tempered and irascible Inuyasha. This is one series that is definitely available for English language speakers, and I highly recommend it.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/mi-1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/mi-1.jpg)

***Maison Ikkoku*** is Takahashi’s only attempt at a full-length manga without any fantasy elements, and is also her most mature story. It’s been called a bittersweet romantic comedy, and that describes it so aptly. It’s about a young widow Kyoko who gets a second chance to start her life afresh as the landlord of a dilapidated apartment building. She’s surrounded by a bevy of odd renters, and finds herself pursued by Godai, a man younger than herself. The story has trademark Takahashi humor throughout, but is amazingly humanistic and uplifting. It’s about second chances in life and love, and reading it enriches your heart and soul.

**6. City Hunter 城市獵人/ Cat’s Eye by Tsukasa Hojo**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ch.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ch.jpg)

Tsukasa Hojo is also another writer (like Takahashi and Adachi) that writes mangas that appeal to both men and women, though his style and genre is pure shonen. His drawing style is also very realistic and three-dimensional, with a penchant for curvy ladies and muscular men.

***City Hunter*** showcases the writer’s love of Bond Ladies, that’s for sure. CH is about Ryo Saeba, a Tokyo private eye, and Kaori Makimura, his girlfriday. The manga is episodic, with new cases and new ladies in distress introduced every few chapters, and are usually self-contained. The overarching mystery as to Ryo’s background and upbringing, and his mysterious past, is finally discussed in the end and the story reaches a conclusion that I find fitting.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ce.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ce.jpg)

***Cat’s Eye*** was his earlier work, and while the drawing style doesn’t have CH’s sophistication and masterful technique, the story was nevertheless wonderfully moving. It’s about three beautiful sisters who run a cafe called Cat’s Eye during the day, but become cat burglars at night stealing their famous father’s artwork in an attempt to get him to reveal his identity to them. The main character is the middle sister, who is in love with a earnest young detective who so happens to be in charge of catching the cat burglars who so confound the police. The ending was a little WTF, but still managed to bring the story home and leave me happy as a kitty with cream.

**7. Ouke no Monshou (Crest of the Royal Family) 尼羅河女兒 / Hakushaku Reijō (The Earl’s Daughter) 伯爵令嬢 Hosokawa Chieko**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/om.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/om.jpg)

This is the granddaddy of all time travel mangas/novels/dramas. In fact, the writer of the current Chinese trend towards Qing dynasty time travel dramas confessed to getting his inspiration from ***Ouke no Monshou***. The story centers around an American Egyptology student named Carol Lido, who travels back in time to ancient Egypt.

She becomes embroiled in the political affairs of Egypt with its rivals and allies during that time period, and she falls in love and eventually marries Memphis, a young pharoah. She’s revered as a goddess of the Nile due to her knowledge of history being considered an ability to foretell the future.

This manga is still ongoing, and to be honest, it lost its way around the mid-way point. The beginning was exhilirating, and Carol’s adventures and love story was interesting and delightful to read. However, Carol is ultimately a beautiful but physically weak character, so she becomes very passive in the story and always at the mercy of the various Kings and Princes who covet her and steal her away from Memphis.

Ouke is truly a legendary work, but the writer would have done herself a favor by ending it earlier rather than allow it to meander around and recycle plot points left and right. It has become an abduction of the week story by this point, and I wish the god the writer can end it so I can move on.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/hr.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/hr.jpg)

In comparison, because Hosokawa was so restrained and in control of herself in her earlier work ***Hakushaku Reijō***, I actually like the manga better at this point than the more epic Ouke no Monshou. Hakushaku Reijō is about the long-lost daughter of an Earl who is discovered in an orphanage and brought home to Paris to be reunited with her mother and grandfather. During the Channel voyage, she befriends a girl stowaway who is a pickpocket.

Her kindness is not reciprocated, as the pickpocket uses an unfortunate ship disaster to try to kill the real Earl’s daughter and take her place since those who know her identity have perished. The real daughter does not die, but loses her memory, and is discovered by a young French aristocrat who is in love with her. He lies that he is her fiancee, and takes her home to recover. The story follows her regaining her real identity and family, and choosing between her first love and her true love.

**8. Dragon Ball / Dr. Slump by Toriyama Akira**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/db.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/db.jpg)

Raise you hands if you’ve never read or heard about ***Dragon Ball***? This might be the most well-known manga in recent memory, and it truly deserves it popularity. It’s a story that blends cultural fiction with action and laughs to create an addicting and interesting manga. The story centers around Goku, a young man who turns out to be from another planet, who becomes a fearsome warrior and fights for justice and the safety of Earth.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ds.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ds.jpg)

Toriyama Akira’s earlier work is just as funny and addicting, and his ***Dr. Slump*** is one of my favorite children’s mangas. It skews young (like ***Doraemon***), but contains a lot of adult inside jokes and references. It’s called a gag manga (about a crude scientist who creates a robot girl who brings happiness and chaos to the fictional Penguin Village), and is just crammed full of Japanese and American pop culture and crude humor that will make you shake your head and giggle like school kid.

**9. Black Jack / Princess Knight / Astro Boy / Buddha by Osamu Tezuka**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/astro.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/astro.jpg) [![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/Blackjack.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/Blackjack.jpg) [![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/buddha.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/buddha.jpg)

Osamu Tezuka is the undisputed and revered father of Japanese manga. He created the industry as we know it, and his works live on in perpetuity for its depth and breadth. I’ve listed a few of his most well-known works, which shows that his range is unparallel (from science fiction to medical procedural to princess stories to spiritual awakenings). I’m in no way qualified to even discuss why he’s a God amongst manga writers, but believe me whenI say his works is not about the visual presentation as much as its about the story he wants to tell. Many of his works are being published in English in collectors editions with impeccable translations. I highly recommend any of his mangas, but my personal favorite is Black Jack (who was clearly the inspiration for Dr. Gregory House).

**10. Swan 芭蕾群英 by Arioshi Kyoko**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/s.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/s.jpg)

Swan is the most iconic ballet manga ever written. Nothing has ever been attempted at such scale and attention to detail. This manga gets into the psychological and emotional highs and lows of becoming a prima ballerina, and you are utterly captivated. The three-way battle of the swans of Swan Lake set in Paris in the early mid-portions of the manga remains my favorite showdown sequence in any medium and any sport.

The drawing took me awhile to get used to, but the writer’s attention to the delicate and exquisite lines of ballet dancers has never been matched. You literally see a ballerina on the page, and its breathtaking and riveting. The latter half wasn’t as interesting for me, when the heroine Masumi goes to New York to study modern ballet. In the end, my OTP got together and Masumi became a world class prima ballerina, which made this story always an epic read from beginning to end.

**11. Yuyu Hakusho (Ghost Files) 幽☆遊☆白書 by Togashi Yoshihiro**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/yyh.png)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/yyh.png)

This manga is a more violent and emotionally mature version of Dragon Ball, blending fighting with the supernatural. I went in with no expectations, and found myself quite taken by the story and the concept. A high school delinquent from a broken family accidentally dies in a car accident when he saves the life of a little girl. He wasn’t suppose to elect to save the girl and die in her place, so the God of the Underworld allows him a chance to return to the land of the living by completing some tasks.

This is only one part of the story, and our lead character comes back to the living only to find that he must continue to battle demons, monsters and villains to keep the world safe. This story has some great characters and exciting battle sequences, all grounded in a humanistic approach towards concepts of good and evil (i.e. not all demons are bad, most humans can be quite evil). There is a sweet love story developed slowly from beginning to end, and shows that shonen manga can have a broad appeal if done right.

**12. Ruruoni Kenshin (Meiji’s Romantic Swordsman) 浪人劍心 Watsuki Nobuhiro**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/rr.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/rr.jpg)

A must-read for a fictional look at the real historical post-Edo period, where Japan was on the brink of modernity and samurais were a dying breed that have been outdated and outlawed. The story is about a fictional forever-youthful assassin named Himura Kenshin, from the Bakumatsu (a samurai sect), who becomes a wanderer to protect the people of Japan during an era of painful rebirth.

Watsuki wrote this series upon his desire of making a shonen manga different from the other ones that were published in that time, with Kenshin being a former assassin who struggles with what he has done in his previous incarnation, and the story taking a more serious tone as it continued. I loved Ruruoni, a story about finding forgiveness and redemption wrapped in a incisive historical critique of post-Edo Japan.

**13. Haikara-san ga Tōru (Fashionable Girl This Way Comes) 窈窕淑女 / Yokohama Monogatari (Yokohama Story) 横滨故事 by Yamato Waki**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/hs1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/hs1.jpg)

Yamato Waki is one of the early dames of shoujo manga, and her ***Haikara-san ga Tōru*** won the first ever ***Kodansha Award*** (the Oscars of mangas) in the shoujo manga category. She’s been retired since the late 90s, but she left behind a collection of works that remain brilliant and timeless. She loves Japanese cultural history, and her speciality is chronicling the fictional lives of girls in important time periods in Japanese history.

Haikara-san ga Tōru tells the story of Benio, a tomboy in 1920s Japan (the Taisho period), and her funny, sweet, and poignant journey to adulthood. The story takes us from Tokyo to Manchuria and back again, finally culminating in the Great Kanto Earthquake, where Benio and her true love Shinobu finds their way back to each other. I freaking love this story to death – it’s sweeping romance and hilarious antics counter-balance and create a story that transcends all barriers.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ym1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ym1.jpg) [![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ym.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ym.jpg)

Yamato continued to develop her craft, and her ***Yokohama Monogatari*** is also a masterpiece coming-of-age story set in the Meiji period of two best friends who take diverging yet equally monumental paths in life. The journey of Mariko, the rich princess with pride and determination, and Uno, her best friend/servant, from Yokohama to London and San Francisco, respectively, in a time of great cultural change was brilliantly brought to life. Both girls have sweeping love stories that are just romantic beyond belief.

**14. Sora wa Akai Kawa no Hotori (Red River, also known as Anatolia Story) / Yami no Purple Eye (Purple Eyes in the Dark) 魔影紫光 by Shinohara Chie**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/y.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/y.jpg)

Shinohara Chie’s niche in shoujo manga is in combining the supernatural with the sweeping romance. Her ***Yami no Purple Eye*** won her a Kodansha Award, and was one of the first mangas I read and couldn’t stop re-reading and actually had dreams about. The story is about a girl named Michiko who turns sixteen and discovers to her horror that she is a shapeshifter who turns into a spotted leopard. She has a loving human boyfriend, but she is being hunted by an increasingly deranged scientist who wants to expose her secret to validate her crazy father’s life long work.

The story is split into two parts, with Michiko meeting another fellow shapeshifter who has the ability to turn into a black panther, and she ends up pregnant with his child. Their baby is a rarity in the shapeshifting world, coming from the union of two shapeshifters, and the daughter’s story is revealed in the second half of the series. Yami is exhilirating, dark, and emotional, with the supernatural used to emphasize how how those with powers long for normalcy and a life lived with purpose and hope.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/rr1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/rr1.jpg) [![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/rr-1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/rr-1.jpg)

I’ve enjoyed all of Shinohara’s works, including her twin saga ***Umi no Yami, Tsuki no Kage*** about two twins who get infected with a virus that gives them the power to levitate and pass through solid objects and they turn against each other for the love of one guy, and her forbidden love saga ***Aoi no Fuuin*** about a demon and her demon hunter who fall in love with each other.

But it was with ***Red River*** that she once again reached new heights in her storytelling. Red River is the successor to Ouke no Monshou in the time-travel genre, and tells of a teeange girl who finds herself transported back to ancient Anatolia. Unlike Ouke, the heroine Yuri is not a damsel in distress, but instead becomes a young warrior who ends up staying in the past, marrying the King, and becoming a part of fictional history.

**15. Saint Seiya (Knights of the Zodiac) 聖闘士星矢 by Kuromada Masami**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ss.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ss.jpg)

A classic 80s shonen manga that spawned legions of toys across all of Asia and likely made kids learn about Greek mythology when otherwise no one would know the difference between Hera and Athena. The story is about five mystical young men destined to become warriors, the “Saints” (or “Knights”), who fight wearing sacred armored clothes, the designs of which derive from the various constellations the characters have adopted as their destined guardian symbols.

The Saints are sworned to defend the reincarnation of the Greek goddess Athena in her battle against the other Olympian gods who want to dominate Earth. Like Dragon Ball, Saint Seiya is as series of challenges and battles that bring our five protagonists against increasingly more powerful enemies. It’s a great series that has all the hallmarks of shonen manga but manages to tell an interesting and and creative story about redemption, hope, and courage.

**16. Kimagure Orange Road 古靈精怪 by Matsumoto Izumi**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ko.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/ko.jpg)

A wonderful teenage romance-meets-fantasy manga, ***Orange Road*** started off light and fluffly and got progressively more intricate and emotionally riveting. It’s a high school coming-of-age story anchored by a love triangle between the main character, an indecisive high school boy with supernatural powers named Kyosuke, Madoka, a mercurial and enigmatic girl with whom he falls in love at first glance, and Hikaru, Madoka’s ditzy and energetic best friend who loves Kyosuke. The story is surprisingly sophisticated and tender, and I find myself able to re-read it years later and derive the same enjoyment and pleasure.

**17. Waltz wa Shiroi de Dress (Waltz in a White Dress) /Magnolia Waltz 白木蘭円舞曲 by Saito Chiho**

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/wd1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/wd1.jpg)

I have a love-hate relationship with Saito Chiho. She is one of the best artists around with her illustrative technique (god, are her guys and girls gorgeous), but her stories are flimsy and inconsistent fare that almost never sustain the beauty of her artwork (I view Yuu Watase the same way, except Saito is a much better illustrator). The one exception is her Waltz series, though this story only reaches the heights of greatness because she elected to make it a series instead of ending it at the end of Waltz in a White Dress.

Waltz is set in the Meiji period where a young girl named Koto, who aspires to be a fashion designer, instead finds herself bumping up against the strictures of her era. She finds herself reluctantly engaged to a seemingly cold young lieutenant Masaomi, whom she has known since childhood, but falls into a torrid love affair with a British commander named Aster, who is half Indian. When it seems that Aster has died, Koto marries Masaomi, and on their wedding night Astor reappears and asks her to elope with him. While it appears that Masaomi will fight Astor to keep Koto by his side, in the end Masaomi lets Koto leave on a steamer bound for Shanghai with Astor. He loves her, but having been diagnosed with leukemia, he wants her to find her happiness with a man who doesn’t have a death sentence looming.

[](http://i966.photobucket.com/albums/ae145/ockoala/Manga/wd.jpg)

![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/wd.jpg "3")

I was really really pissed with the ending of Waltz in a White Dress, because I totally shipped Koto with her woobie husband, who I knew totally loved her but couldn’t show it because Astor got in the way. Perhaps I wasn’t the only one, because Chiho Saito decided the continue her story, following Koto and Astor to Shanghai where she bore him a baby boy. But Astor was involved with the movement for Indian independence from its British colonial masters back in India, and he leaves Koto to head to India to head the movement.

Koto meets up again with Masaomi, to whom she is still legally married, and he knows that she’s had a baby with Astor. Under orders from the Japanese goverment to assist in the Indian independence movement, Masaomi accompanies Koto to India, and their relationship continues its slow burn where he keeps her at bay so that she doesn’t discover that he still loves her. When they reach India and manage to reunite with Astor, at a final rally, Astor is mortally wounded and dies. Masaomi manages to safely get Koto and the baby on a ship back to Shanghai and coaxes her to not give up her life for the sake of her baby and those who need her.

[](http://i966.photobucket.com/albums/ae145/ockoala/Manga/mw.jpg)

![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/mw.jpg "4")

Koto opens a dress shop in Shanghai and goes through the motions until she hears that Masaomi has been sent to Manchuria for a tour of duty. Masaomi’s older brother, who is also good friends with Koto, finally reveals to her that Masaomi requested the assignment to get away from Koto, and that he is dying of leukemia, and he knew it when he decided to let her elope with Astor. Koto heads to Manchuria with the idea that she wants to take care of him, but discovers that her heart didn’t die when Astor died, and that she was always in love with both men.

[](http://i966.photobucket.com/albums/ae145/ockoala/Manga/mw1.jpg)

![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/mw1.jpg "2")

The couple have this amazingly romantic reconcilation in the snowy plains of Manchuria, and Koto manages to convince Masaomi to end his military career and return to Shanghai with her. His older brother welcomes them back, and realizes with joy that the couple have reconciled. The story jumps forward 8 years to the end of World War II, and we see that Koto had another son with Masaomi before he passed away, and has since become a successful designer raising her two sons alone.

Her two sons from the two men she loved realize that their beloved mother has another chance at love, with Masaomi’s older brother who has always been by her side since she was a child and before she married Masaomi and before she eloped with Astor. Waltz is one of the most romantic shoujo mangas I have ever read (both guys get the girl!), and is ripe for a drama adapation if ever there was one.

**18. Kanata Kara 彼方から (From Far Away) by Hikawa Kyoko**

[](http://i966.photobucket.com/albums/ae145/ockoala/Manga/kk.jpg)

![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/kk.jpg "5")

Of all the time-travel mangas (since Ouke no Monshou originated this sub-genre), Kanata Kara shares the crown as my favorite along with Inuyasha. It’s effortlessly brisk, a story told with a beginning, middle, and end that all make sense, and the illustrations are gorgeous. It tells of a high school girl Noriko, who falls into a strange world where she has become the Awakening that is the source of great fear and power. Legend foretells that when the Awakening arrives in their world, the Sky Monster will be awakened and destroy civilization.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/kk2.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/kk2.jpg) [![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/kk1.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/kk1.jpg)

Noriko meets a solitary warrior named Izark, and accompanies him. Unbeknowst to her, Izark is on a quest to change his fate by trying to kill the Awakening only to find out that the Awakening is Noriko. They travel together and Noriko learns the language of this new world, and Izark tries to figure out what to do with her, and with his awakening demon powers. It’s a battle between good and evil, and a love that transcends time. The entire manga is well-plotted and touching, and I loved every page of it.

**19. Tokimeki Tonight (Exciting Tonight) by Ikeno Koki**

![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/tm.jpg "1")

Imagine if ***Itazura na Kiss*** had fantasy elements, and voila, you will find its predecessor ***Tokimeki Tonight***. Ranze, a not-very-bright high school girl, is in love with Shun, the cutest boy in school, who is cold and dismissive of her. Except Ranze isn’t quite your average girl, for she comes from a supernatural family with a vampire father, a werewolf mother, and a werewolf little brother. Ranze herself has the ability to shapeshift into anything she bites.

![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/tm1.jpg "8")

The story started off cute and little girl like, but quickly ramped up its mythology and epic multi-verse lore, bringing the story of Ranze and Shun not just about unrequited high school love, but about a reincarnated pair of love-crossed lovers in a fantasy world. Ikeno continues her story from Ranze and Shun, to Ranze’s younger brother’s story with the girl he loves, and finally to the love stories of Ranze and Shun’s son and daughter with their respective OTPs.

I think Tokimeki Tonight is adorable and always a fun read, even if I like certain of the story segments better than others. It’s a classic shoujo manga that may be somewhat simplistic in delivery, but nevertheless tells a great story and has a lot of heart and sweetness.

**My Final Thoughts:**

I’m very picky about my mangas, moreso than I am about my dramas actually. Dramas and the television medium can become outdated quickly, even though there are genuine masterpieces that can transcend technological advances because the story is so solid. Mangas and the written medium (including of course novels) don’t have that constraint. The ability to hand draw an illustration is the same today as it was sixty years ago when mangas first took off in Japan. Which is why I hold manga writers of this generation up to the same standards as their predecessors, and most of the newcomers fall so woefully short it’s hurts me to read them.

[![](http://i966.photobucket.com/albums/ae145/ockoala/Manga/m3.jpg)](http://i966.photobucket.com/albums/ae145/ockoala/Manga/m3.jpg)

The mangas I recommend are all as readable today as the day I read them, because the story within is constructed with care and consideration. Reading these mangas, and many more, helped expand my horizons and build my understanding of storytelling. This list is by no means exhaustive, and reflects only my taste amongst the hundreds of mangas I’ve read. Have fun reading if you decide to check any of them out.

Posted in [Japanese Manga](https://koalasplayground.com/category/japanese-manga/)  Tagged [Candy Candy](https://koalasplayground.com/tag/candy-candy/), [Cat's Eye](https://koalasplayground.com/tag/cats-eye/), [City Hunter](https://koalasplayground.com/tag/city-hunter/), [Dr. Slump](https://koalasplayground.com/tag/dr-slump/), [Dragon Ball](https://koalasplayground.com/tag/dragon-ball/), [Glass no Kamen](https://koalasplayground.com/tag/glass-no-kamen/), [Inuyasha](https://koalasplayground.com/tag/inuyasha/), [Kanata Kara](https://koalasplayground.com/tag/kanata-kara/), [Magnolia Waltz](https://koalasplayground.com/tag/magnolia-waltz/), [Maison Ikkoku](https://koalasplayground.com/tag/maison-ikkoku/), [Orange Road](https://koalasplayground.com/tag/orange-road/), [Ouke no Monshou](https://koalasplayground.com/tag/ouke-no-monshou/), [Red River](https://koalasplayground.com/tag/red-river/), [Rough](https://koalasplayground.com/tag/rough/), [Ruroni Kenshin](https://koalasplayground.com/tag/ruroni-kenshin/), [Saint Seiya](https://koalasplayground.com/tag/saint-seiya/), [Swan](https://koalasplayground.com/tag/swan/), [Touch](https://koalasplayground.com/tag/touch/), [Urusei Yatsura](https://koalasplayground.com/tag/urusei-yatsura/), [Yami no Purple Eyes](https://koalasplayground.com/tag/yami-no-purple-eyes/), [Yokohama Monogatori](https://koalasplayground.com/tag/yokohama-monogatori/), [Yuyu Hakusho](https://koalasplayground.com/tag/yuyu-hakusho/)  [permalink](https://koalasplayground.com/2011/03/29/a-koalas-guide-to-the-best-japanese-mangas/ "Permalink to A Koala’s Guide to the Best Japanese Mangas") 

References:  
https://www.lambdatest.com/free-online-tools/html-to-markdown-converter