source:  
https://soranews24.com/2014/11/01/animes-10-biggest-male-perverts-as-chosen-by-japanese-fans/

# Anime’s 10 biggest male perverts, as chosen by Japanese fans

Casey Baseel, Nov 1, 2014  

![AP 11](https://soranews24.com/wp-content/uploads/sites/3/2014/10/ap-111.png?w=580)

With the huge volume of **anime** that Japan produces, the medium has its own archetypes that each generation of creators adds new entries to. In the cute mascot character category, you’ll find Totoro, _Magic Knight Rayearth’s_ Mokona, and _Pokémon’s_ Pikachu. Looking for giant robots? Let us direct you to the full line of Gundam, Evangelion, and [Ingram](https://soranews24.com/2014/10/19/coming-face-to-boot-with-the-giant-patlabor-statue%E3%80%90photos%E3%80%91/) variants.

But Japanese animation isn’t just cuddly adorableness and cool technology. It’s also filled with raging hormones and irrepressible libidos, as shown in this **list of anime’s perviest male characters**.

Anime and video game database **Charapedia** polled 4,470 men and 5,530 women to compile its rankings. The competition was stiff, but the following 10 beat off their rivals for their hard-fought spots in the top 10.

**10. Issei Hyodo (High School DxD)**

![AP 1](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjMyMiIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

Issei’s tale gets off to a rocky start, as he’s fatally stabbed while on a date, before being resurrected by the dark magic of his demonic classmate Rias. With no loss in his lecherousness post-shanking, Issei becomes a literal horny devil.

**9. Meliodas (The Seven Deadly Sins)**

![AP 2](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjMxMCIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

Lust is one of the seven deadly sins, so it’s a fitting transgression for the hero of the [hot new anime series](https://soranews24.com/2014/10/17/the-10-best-anime-of-the-fall-season-according-to-japanese-otaku/) of the same name.

**8. Tadao Yokoshima (Ghost Sweeper Mikami)**

![AP 3](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjQxOSIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

Tadao works a low-paying and dangerous job. He’s willing to overlook both of those occupational shortcomings, though, in light of his ghost-hunting boss’ shapely body.

**7. Rito Yuuki (To Love-Ru)**

![AP 4](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjQzNCIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

Maybe Rito should get in touch with Issei. Sure, Issei is surrounded by actual devils, whereas Rito deals with attractive devil-like aliens, but still, the two could probably exchange some useful pointers.

**6. Arsene Lupin III (Lupin III)**

![AP 5](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjU0NiIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

Creator Monkey Punch’s globe-trotting thief made his manga debut 47 years ago, meaning decades of comic fans have stood witness to his lady-loving escapades.

**5. Sanji (One Piece)**

![AP 6](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjQzNCIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

The Straw Hat Pirates’ resident chef enjoys fine cuisine, the female form, and (in unedited versions at least) smoking constantly. He is not, however, [legal tender](https://soranews24.com/2014/10/30/in-anime-we-trust-man-arrested-for-selling-dollar-bills-with-unlicensed-one-piece-stickers/).

**4. Ataru Moroboshi (Urusei Yatsura)**

![AP 7](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjQzNCIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

In the very first episode of _Urusei Yatsura’s_ TV adaptation, the Earth is threatened with alien conquest. Overcome with grief, teenage Ataru’s distraught classmate clings to him, sobbing. He responds by copping a feel of her butt.

**3. Gintoki Sakata (Gintama)**

![AP 8](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjMxNiIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

After aliens conquer Japan and strip the samurai of their status, former swordsman Gintoki sees no reason to suppress his love of sweets, manga, or women.

**2. Kame Sennin (Dragon Ball)**

![AP 9](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjU3NyIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

Some call his elderly martial arts master Kame Sennin. Others prefer Turtle Sage, Muten Roshi, or Master Roshi. But all call him a dirty old man.

**1. Ryo Saeba (City Hunter)**

![AP 10](data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjQzNCIgd2lkdGg9IjU4MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIC8+)

_Dragon Ball_ movies are [still being made](https://soranews24.com/2014/10/10/dragon-ball-z-voice-actors-go-crazy-in-the-booth-show-just-how-tough-the-job-can-be%E3%80%90video%E3%80%91/), and _Lupin III_ just got a [live-action theatrical feature](https://soranews24.com/2014/04/26/lupin-iii-springs-into-action-in-trailer-for-live-action-movie-%E3%80%90video%E3%80%91/). _City Hunter_, on the other hand, hasn’t had an animated installment since 1999, and the manga where private eye/mercenary Ryo’s adventures began had its last issue published in 1991.

Nevertheless, Ryo received more votes in the poll than any other character, also coming in first when looking just at responses from men, women, and respondents over 30. He narrowly missed a perfect sweep by coming in second among votes from fans aged 10 to 29 (who made Gintoki their number-one pick).

Ryo’s willingness to hit on any attractive woman, whether or not he’s in the middle of sleuthing or exchanging gunfire, **left an impression not only on manga readers, but on the Japanese language, too**. He’s credited with popularizing the word _mokkori_, a specific kid of onomatopoeia for the “boing!” of getting a sudden boner.

Between Ryo’s sex drive and the decade-plus gap since the last _City Hunter_ anime, we’re surprised no one’s produced a sequel yet starring a dozen of his illegitimate children.

Source: [Niconico News](http://news.nicovideo.jp/watch/nw1300480), [Charapedia](http://www.charapedia.jp/research/0039/)

Top image: [Anime Vice](http://www.animevice.com/forums/battles/33/issei-hyoudou-vs-master-roshi/345130/?page=1), [Dreamwidth](http://ship-manifesto.dreamwidth.org/245234.html), [Tumblr](http://porkodjavelo.tumblr.com/post/57344856423/imboscarsi-quasi-con-belen-di-domenica-mattina), [Fanpop](http://www.fanpop.com/clubs/sakata-gintoki/images/28003009/title/gintoki-screencap) (edited by RocketNews24)

Insert images: Anime Vice, [PT Anime](http://ptanime.com/primeiras-impressoes-nanatsu-no-taizai/os-sete-pecados-mortais-a-revelacao-de-meliodas/), [Cemzoo](http://wiki.cemzoo.com/wiki/Tadao_Yokoshima), [Forochile](http://inazumaelevencl.forochile.org/t121-personajes-de-anime-parecidos-xd), [Zerochan](http://www.zerochan.net/24298), Dreamwidth, Tumblr, Fanpop, [Yahoo! Japan](http://blogs.yahoo.co.jp/nakanekougyou1122), [YouTube](http://www.youtube.com/watch?v=OQ0GeTz9ImE)

References:  
https://html-to-markdown.com/demo
