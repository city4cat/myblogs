source:  
http://www.sequentialtart.com/archive/aug02/hojo.shtml
http://www.sequentialtart.com/archive/aug02/home.shtml

（译注：该采访日期可能是2002年8月）  


[Home](http://www.sequentialtart.com/archive/aug02/home.shtml) > 
[Interviews](http://www.sequentialtart.com/archive/aug02/interviews.shtml)  

# Hunting Manga Dreams in the City
## Tsukasa Hojo
在城市中追寻漫画梦 北条司  

### by [Trisha L. Sebastian](mailto:trisha@sequentialtart.com)

I was a little intimidated when I stepped into the suite where Tsukasa Hojo was conducting private interviews. In addition to Hojo-san, Nobuhiko Horie, the co-founder of **Coamix, Inc.** was in the room along with Jonathan Tarbox, the translator for the premiere issue of **Raijin Comics #0**, a representatives from **Gutsoon! Entertainment**’s public relations firm, and an **Anime Expo** staffer and translator, Angie Shima Karino. I’ve never done an interview in front of so many people before.  
当走进北条司正在进行私人采访的套房时，我有点害怕。除了北条之外，还有Coamix, Inc.的联合创始人堀江信彦，以及Jonathan Tarbox，他是Raijin Comics创刊号的翻译，也是Gutsoon! Entertainment的代表。还有娱乐公司的公关公司、Anime Expo的工作人员和翻译Angie Shima Karino。我从来没有在这么多人面前做采访。

Dazed, I exchanged business cards with Horie-san, grateful that I remembered the etiquette involved. The following interview was warm and friendly. When there were questions Hojo-san could not answer, Horie-san was able to step in and clarify some remarks. At the end of the interview, I had maybe two more questions to ask, and asked to submit them to Hojo-san later, to which he replied with a laugh, "Don’t make them so hard." I couldn’t tell if he was serious or not, but I was told later that he appreciated that I asked so many questions about his work and that I did my homework.  
我迷迷糊糊地和崛山交换了名片，庆幸自己还记得其中的礼节。接下来的采访非常热情友好。当有问题无法回答时，堀江山能够介入并澄清一些言论。在采访结束时，我可能还有两个问题要问，并要求稍后提交给北条先生，他笑着回答说:“别把问题问得这么难。”我不知道他是认真的还是不认真的，但后来我被告知，他很感激我问了那么多关于他工作的问题，也很感激我做了功课。  

Tsukasa Hojo’s manga work has spanned 20 years, with titles like **Cat’s Eye**, **Family Compo**, and **Neko Manma Okawari**. Some of them have been adapted into live-action and anime formats. The title most American audiences would be familiar with is **City Hunter** which was both an anime and a live action movie as well as a manga. **City Hunter** will be published in manga format in American for the very first time in **Raijin Comics**.  
北条司的漫画作品已经有20年的历史了，他的作品包括《Cat’s Eye》、《Family Compo》和《Neko Manma Okawari》。其中一些已经被改编成真人电影和动画。大多数美国观众都熟悉的作品是《City Hunter》，它既是一部动漫，也是一部真人电影，也是一部漫画。City Hunter将在Raijin Comics首次以漫画形式在美国出版。  
（译注：作品Neko Manma Okawari的常见的中译名为《白猫少女》。参见[Neko Manma Okawari](https://ww.mangadex.tv/chapter/manga-mh964342/chapter-3)）  

* * *

**Sequential Tart:** _So when did you know you wanted to become a manga artist?_  
你什么时候知道自己想成为一名漫画家的?  

**Tsukasa Hojo:** After I became a manga artist. [Laughter] I’ve been drawing since I was little, but I never drew a story; it was just pieces of art. I started to draw a little more in high school but I didn’t have much speed with the number of drawings, so I didn’t know that I would be able to do manga.  
在我成为漫画家之后。(笑)我从小就开始画画，但我从来没有画过故事;那只是一副画。我在高中时开始画更多的画，但是我画的速度很慢，所以我不知道我能画漫画。  

**ST:** _What kind of manga did you read when you were younger and how would you say that influenced you?_  
你年轻的时候看的是什么类型的漫画，你认为这对你有什么影响?  

**TH:** I didn’t really buy manga [when I was younger]. Right after I was born was when the TV was prevalent in Japan and became more commonplace, and TV animation just started up at that time. I had more influence from TV animation than comics so I grew up watching [shows like] **Tetsuwan Atam** (**Astro Boy**. If you were to ask me if there was one favorite, I wouldn’t be able to name it because at the time, everything was interesting, so I watched everything that was on. I’m not the only one, [and] all of my friends probably have more knowledge of animation than I do.  
我年轻的时候不怎么买漫画。我出生后不久，电视在日本开始普及，电视动画也在那个时候开始出现。比起漫画，电视动画对我的影响更大，所以我是看着像《铁臂阿童木》这样的节目长大的。如果你问我有没有最喜欢的，我说不出来，因为当时一切都很有趣，所以我看了所有正在播放的节目。我不是唯一一个这样的人，我所有的朋友可能都比我更了解动画。

**ST:** _Did you study at a formal school after high school?_  
高中毕业后你在正规学校学习了吗?

**TH:** I did attend a design school, the Kyushu Sangyou Daigaku in the arts division. I actually debuted [as a manga artist] before I graduated when I entered a contest for **Shounen Jump** for the Tezuka Prize. [Hojo was the runner up.]  
我确实上过一所设计学校，九州大学艺术学院。实际上，在我毕业之前，我参加了少年 Jump的手冢奖比赛，成为了一名漫画家。[北条是亚军。]  

![](http://www.sequentialtart.com/images/0802/ao_hojo_1.jpg)

**ST:** _What was the very first manga series that you drew?_  
你画的第一个漫画系列是什么?  

**TH:** [It was] **Cat’s Eye**.  

**ST:** _How did you create it? What was the idea behind it?_
你是如何创造它的?这背后的想法是什么?  

**TH:** [Laughter] To explain it would take a little long...  
(笑)说来话长……  

**ST:** _Two or three sentences?_  
简单说说吧?  

**TH:** I was out drinking with my college buddies and that’s how the idea came about. [Laughter] It started off with the story about a family where the dad and the son are cops and the mom and the daughter were the robbers. But of course the son and dad don’t know that the other family members are robbers. At night, the mom would be in bed with the dad and asking about [his day] and finding out what happened.  
我和我的大学朋友出去喝酒，这就是这个想法的由来。(笑)故事开始于一个家庭，父亲和儿子是警察，母亲和女儿是强盗。当然，儿子和爸爸不知道其他家庭成员是强盗。晚上，妈妈会和爸爸一起躺在床上，问他今天过得怎么样，想知道发生了什么。  

**ST:** _What is the typical work day for you like? How much time do you spend drawing and how much time do you spend writing?_  
你的工作日通常是什么样的?你花多少时间画画，花多少时间写作?  

**TH:** My day usually runs from about eleven thirty in the morning to past one am or two thirty. Of course I eat and drink during that time, too. [Laughter]  
我的一天通常从早上十一点半到凌晨一点或两点半。当然，在这段时间里我也会吃喝。(笑)  

**ST:** _Do you have any assistants and if so, what do they do for you?_   
你有助手吗?如果有，他们为你做什么?

**TH:** I have four assistants. [They] handle the finishing of the rough sketches and the scenery.  
我有四个助手。(他们)负责完成草图和布景。

**ST:** _What kind of art tools do you use? What kind of brushes and pens?_  
你使用什么样的美术工具?什么样的画笔和钢笔？

**TH:** They’re called “spoon” pens.  
它们被称为“spoon”笔。  

**ST:** _Of the manga series you’ve written, which is your favorite and why?_  
在你所写的漫画连载中，你最喜欢哪一部，为什么?  

**TH:** The one that I’m working on right now is my favorite. Because I believe that what I’m working on is the most interesting and most fun, I’m not able to draw anything is not as interesting as the previous, if not more interesting.  
我现在正在做的是我最喜欢的。因为我相信我现在做的是最有趣最好玩的，不能逊色于以前的作品，尽可能要超越以前的作品。

**ST:** _What are you currently working on right now?_  
你现在在做什么?  

**TH:** I’m doing a weekly comic called **Angel Heart**.  
我正在做一个名为Angel Heart的周刊漫画。

**ST:** _What is **Angel Heart** about?_  
Angel Heart讲的是什么?

**TH:** That is a long answer. [Laughter]  
说来话长。(笑)  

**ST:** _How far in the series are you right now?_  
你现在这个连载多久?  

**TH:** It’s been running for about a year and a half.  
大约一年半了。  

**ST:** _Can you give a brief summary about the first chapter?_  
你能简单概括一下第一话吗?  

![](http://www.sequentialtart.com/images/0802/ao_hojo_2.jpg)


**TH:** It’s based on events in **City Hunter**.  
它基于City Hunter中的一些事。  

**Nobuhiko Horie:** It’s more of a sequel, using the same characters as **City Hunter** where Ryo was married to Kaori. Unfortunately, she passes away in an accident and her heart gets transplanted into the body of an assassin.  
堀江信彦: 这更像是一个续集，使用与City Hunter相同的角色，在City Hunter中，獠娶了香织。不幸的是，她在一次事故中去世，她的心脏被移植到一个杀手的身体里。

**ST:** _That sounds similar to the movie Return to Me with David Duchovny and Minnie Driver._  
这听起来很像David Duchovny和Minnie Driver主演的电影《Return to Me》。  

**TH:** My story came first. [Laughter]  
先有我的故事的。(笑)  

**NH:** Because the assassin has Kaori’s heart, they start to develop a father-daughter relationship. She’s only fifteen, but she was raised as an assassin. Basically, although Kaori died, she still lives through the girl.  
因为杀手有香的心脏，他们开始发展父女关系。她只有15岁，却被培养成一名杀手。基本上，虽然香死了，但她仍然通过这个女孩活着。  

**ST:** _Is this kind of supernatural story something you find yourself returning to as a theme?_  
这种超自然的故事是你想要回归创作的主题吗?

**TH:** I can’t decide now. But I have used that kind of theme before in **City Hunter**, so it’s not new. In all of my work, it appears somehow.  
我现在不能确定。但我之前在City Hunter中使用过这种主题，所以这并不新鲜。在我所有的作品中，它以某种方式出现。  

**ST:** _In addition to using supernatural elements, what other elements do you find yourself using again and again?_  
除了使用超自然元素，你还发现自己经常使用哪些元素?  

**TH:** I don’t want to really talk about themes, but probably [the idea of] family [comes up a lot].  
我不想刻意谈论主题，但家庭这个概念可能会经常出现。  

**ST:** _What would you say has influenced your art style?_  
你认为是什么影响了你的艺术风格?  

**TH:** Everything that has caught my eye, I consider an influence.  
凡是吸引我的，我认为都有影响。

**ST:** _Do you follow any titles right now and if so, which ones?_  
你现在有追任何作品吗?如果有，是哪些?  

**TH:** No, because I am too busy. Reading manga is tiring. [Laughs]  
不，因为我太忙了。看漫画很累。(笑)  

**NH:** In Japan, a lot of manga-ka don’t read other people’s work. That’s because what happens when you read other people’s work, that’s all that you see and the imagination doesn’t take off. So a lot of them try and refrain from looking at others’ work.  
在日本，很多漫画家不读别人的作品。这是因为当你读别人的作品时会怎样，就是你所看到的那些，不会为你增加想象力。所以他们中的很多人都尽量不去看别人的作品。  

**ST:** _That’s an interesting difference because I’ve noticed that a lot of American creators read other works._  
这是一个有趣的差异，因为我注意到很多美国创作者都阅读其他作品。  

**NH:** There’s a saying in Japanese where basically the meaning is that your ideas are not going to get any greater that what you have already seen. Usually the manga-ka have already seen a lot from when they were young and you get to a point where you have to rely on your own imagination.  
日本有句谚语，意思是见多识广。通常漫画家从小就已经看过很多东西了，所以你必须依靠自己的想象力。

![](http://www.sequentialtart.com/images/0802/ao_hojo_3.jpg)

**ST:** _When **Coamix, Inc.** was founded in June 2000, what influenced your decision to join them?_  
当2000年6月成立Coamix公司时，是什么影响了你决定加入的?  

**TH:** [With a look at Horie-san] I wanted to save this very knowledgeable and influential editor from falling to his demise. [Much laughter.] That’s a joke.  
(看了堀江一眼)我想挽救这位知识渊博、影响力巨大的编辑，让他免于悲惨命运。(大笑)开个玩笑。  

**NH:** There’s some truth in that. [Laughs.]  
这话有几分道理(笑)。  

**ST:** _How much input do you have in the material that will be in **Raijin Comics**?_   
你对将在Raijin Comics的素材有多少投入?  

**TH:** Ultimately, it will be the editor [who chooses]. [Horie-san and I] see each other every day and every day is like a meeting and during those talks, we might say, “Well, how about ... ?”  
最终，这将是编辑的选择。(堀江山和我)每天都见面，每天都像是在开会，在谈话中，我们可能会说，“嗯，这样如何如何……？”  

**ST:** _I saw the video that was shown at the opening ceremonies and I thought that it was very, very funny. How long did that take to put together?_  
我看了开场仪式上播放的视频，我觉得非常非常有趣。花了多长时间拼凑起来的?

**TH:** It was extraordinarily hard — three hours. [Laughter.]  
非常辛苦——三个小时。(笑)

**ST:** _Whose idea was it?_  
这是谁的主意?  

**TH:** It was the two of us [him and Horie-san] talking.  
是我们两个人(他和堀江)在说话。  

**ST:** _Will you be appearing at any other conventions this year?_  
你今年还会出席其他的大会吗?  

**TH:** My staff or other people from the company might be [at other conventions], but I will not because I'm very busy.  
我的员工或公司的其他人可能会参加其他的大会，但我不会去，因为我很忙。  

![](http://www.sequentialtart.com/images/ImagesINT/linksbar.gif)

[www.raijincomics.com](http://www.raijincomics.com)

Scratching your head over a word or phrase? Check out our [glossary](ao_glossary.shtml)!

