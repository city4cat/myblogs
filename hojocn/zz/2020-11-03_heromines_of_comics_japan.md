source:  
https://www.europecomics.com/heroine-comics-japan/


# The Heroines of Comics: Japan

November 3, 2020

Forgotten, neglected, and caricatured, women long held secondary roles in the highly masculine world of comics. But those days are over, thanks to figures like [Wonder Woman](https://en.wikipedia.org/wiki/Wonder_Woman) and [Mary Jane Watson](https://en.wikipedia.org/wiki/Mary_Jane_Watson), [Laureline](https://en.wikipedia.org/wiki/Val%C3%A9rian_and_Laureline) and [Marji](https://en.wikipedia.org/wiki/Persepolis_(comics)), [Lucy Heartfilia](https://en.wikipedia.org/wiki/Lucy_Heartfilia) and [Lady Snowblood](https://en.wikipedia.org/wiki/Lady_Snowblood_(manga)). Whether adventurers or lady loves, femmes fatales or free spirits, these bold heroines now count among the most striking and significant characters of the ninth art. Here are their stories.

**KAORI MAKIMURA**

[Tsukasa Hojo](https://en.wikipedia.org/wiki/Tsukasa_Hojo) (writer and artist), [_City Hunter_](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-102287-8) (1985), [Shueisha](https://www.shueisha.co.jp/english/)

**The Mokkori effect**

Ever heard of the Mokkori effect? In short, it’s “the effect felt in Ryo’s pants on seeing a beautiful woman.” [Ryo Saeba](https://en.wikipedia.org/wiki/City_Hunter#Characters) is a city hunter, a “sweeper” in Tokyo’s Shinjuku district, and he has one mission: tracking down thugs and teaching them a lesson. Ryo shares the lead with a young woman called [Kaori Makimura](https://fr.wikipedia.org/wiki/Kaori_Makimura). She’s the foster sister of Ryo’s partner, Hideyuki, who was murdered by mob criminals. For Kaori, it’s love at first sight. And she’s the jealous type: she takes a swipe at him every time he seems a little too interested in his clients. “My body can’t lie!” he shoots back. Ryo prefers his clients to be pretty women likely to repay him with sexual favors. By contrast, he’s wholly unmoved by Kaori; the dismissive brute doesn’t even “see her as a woman.” She’s just a colleague to him, he says. In blunt terms, “the only woman in the world that doesn’t get me going!”

**Lady and the brute**

It’s true that Kaori perhaps isn’t taking the right approach to attract Ryo’s attention: she talks like a tomboy and looks like one, with a haircut and clothes that are far more functional than trendy. Too bad for Ryo, because he’s missing out on a young woman who’s actually very attractive. He’s a prominent pervert: they call him the “Shinjuku Stallion” because of his spectacular, shall we say, “stamina.” Nothing floats his boat more than a “Miss Mokkori,” the type of woman who can set off the “Mokkori effect” in his pants. As Kaori’s jealousy gets worse, so do the painful swipes to Ryo’s head… _City Hunter_ wasn’t initially a huge success, but then [_Cat’s Eye_](https://en.wikipedia.org/wiki/Cat%27s_Eye_(manga)) author Tsukasa Hojo and his publisher decided to ramp up the funny. The result: a successful mix of action, violence, good-natured fun and romance. Readers were enraptured, and eventually, so was Ryo: after no less than three hundred episodes of the saga, he reveals he has feelings for Kaori.

[![City Hunter Nicky Larson Manga Seinen](https://www.europecomics.com/assets/uploads/2020/11/City-Hunter-800px-707x1024.jpg)](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-102287-8)

**NANA AND NANA**

[Aï Yazawa](https://en.wikipedia.org/wiki/Ai_Yazawa) (writer and artist), [_Nana_](https://books.shueisha.co.jp/items/contents.html?isbn=4-08-856209-7) (2000), [Shueisha](https://www.shueisha.co.jp/english/)

The Nana manga series reached a diverse audience, offering punk rock, fashion and love stories.

**Love, always**

Are love and happiness unattainable, or can they come true? This is the question that hangs over the two heroines throughout the series. They’re both called Nana, they’re both twenty years old, and that’s about all they have in common. The first, [Nana Osaki](https://en.wikipedia.org/wiki/List_of_Nana_characters#Nana_Oosaki), is the lead singer in a punk rock band and in a relationship with one of the musicians. The other, [Nana Komatsu](https://en.wikipedia.org/wiki/List_of_Nana_characters#Nana_Komatsu)—also known as Hachi—is in and out of love all the time, more into the idea of love than the reality of it. They first meet on a train to Tokyo, a place where they have both decided to settle. They talk at length, and even with Hachi spilling all her secrets along the journey, by the time they get to their destination, it seems unlikely they’ll keep in touch. Fate throws them back together when they visit the same apartment and decide it makes financial sense to rent it together. This marks the beginning of what will become an enduring, tempestuous, but fierce friendship. Both characters will face their own journey of making their dreams and love stories come true.

**An inner glimpse**

Between Hachi—the frail, emotional young woman in search of the love of her life—and Osaki—the feisty little punk determined to live off her music and make it in the merciless world of show business without sacrificing her happiness—it seems that fortune is likely to favor the latter. As the story unfolds, however, both characters evolve and reveal surprising layers. As it turns out, the ever-optimistic Hachi is the one with the thicker skin, able to bring stability in the dynamic between the duo, whereas the charismatic Nana Osaki reveals hidden weaknesses. Aï Yazawa sidesteps mawkishness and facile clichés to draw a subtle and nuanced portrait of two women on a journey of self-exploration. This [_shojo_](https://en.wikipedia.org/wiki/Sh%C5%8Djo_manga) manga (manga stories targeted to young Japanese girls) was very successful in both Japan and France; it was eventually adapted into two live-action films and an animated TV show. _Nana_ owes its success in part to its backdrop of the music industry and the way it portrays relationships between musicians. Yazawa’s background as a stylist is illustrated beautifully in the elegance of his characters’ outfits, one of many thoughtful details in a gripping story of emotional growth and the pursuit of happiness.

[![Nana Cover 1 Manga Shojo](https://www.europecomics.com/assets/uploads/2020/11/Nana-800px-683x1024.jpg)](https://books.shueisha.co.jp/items/contents.html?isbn=4-08-856209-7) © Shueisha Inc.

**NAUSICAÄ**

[Hayao Miyazaki](https://en.wikipedia.org/wiki/Hayao_Miyazaki) (writer and artist), [_Nausicaä of the Valley of the Wind_](https://www.viz.com/nausicaa-of-the-valley-of-the-wind) (1982), [VIZ Media](https://www.viz.com/) – Originally published in Japanese by [Tokuma Shoten Publishing](https://www.tokuma.jp/)

**From Ulysses to Miyazaki**

We wonder what Homer would think if he found out he had provided inspiration for a comic book character… That’s exactly what happened when Hayao Miyazaki—perusing a dictionary on Greek mythology—stumbled upon Nausicaä, a princess who healed Ulysses’ wounds in the Odyssey. She’s also based on a character from a classic Japanese folk tale called “[The Princess Who Loved Insects](https://en.wikipedia.org/wiki/The_Lady_who_Loved_Insects),” about a girl who preferred to spend her days studying caterpillars over finding Prince Charming. But Nausicaä—daughter of Jihl, who rules over the peaceful realm of the Valley of the Wind—has a more complicated destiny to fulfill. She must go to war with the Dorok empire in a post-apocalyptic world where industrial civilization has self-destructed. Most of the surface of the Earth is heavily polluted and bare, a place where only insects survive amidst giant bacteria that spread toxic vapors.

**Cherish life**

In a distant future where violence rules, Nausicaä stands out. She lives out her humanist ideals in a war-like culture. She advocates for peace—“Choose love, not hate!”—without being sanctimonious. Through Nausicaä’s actions and words, Miyazaki communicates his world vision and lays out the fundamental ideas that permeate his work: his great love of life, and his desire to protect the environment against the destructive nature of humans. “I love life! I love the light, the sky, the people, the insects… I love them more than anything!” proclaims Nausicaä. _Nausicaä of the Valley of the Wind_ appeared in the monthly Japanese magazine [_Animage_](https://en.wikipedia.org/wiki/Animage) from 1982 to 1994. She was voted “best female character of all time” several years in a row. The manga was also adapted for the big screen in 1984 as _[Nausicaä of the Valley of the Wind](https://en.wikipedia.org/wiki/Nausica%C3%A4_of_the_Valley_of_the_Wind_(film)),_ which portrays a messianic young woman who excels in a mission that no one prepared her for. This passionate, empathetic woman is adept at both combat and the use of technology. She is loved by her people and reaches out to every kind of species. Thirty years on, Nausicaä remains an unforgettable character with timeless beliefs.

[![Nausicaa Manga Anime Hayao Miyazaki](https://www.europecomics.com/assets/uploads/2020/11/nausicaa-800px.jpg)](https://www.viz.com/nausicaa-of-the-valley-of-the-wind) © VIZ Media (Originally published in Japanese by © Tokuma Shoten Inc.)

**SAKURA HARUNO**

[Masashi Kishimoto](https://en.wikipedia.org/wiki/Masashi_Kishimoto) (writer and artist), [_Naruto_](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-703447-9) (1999), [Shueisha](https://www.shueisha.co.jp/english/comics/#Girls)

**Bully the Kid**

Sakura has come a long way from the playground bullies teasing her about her big head. Masashi Kishimoto, aware of his shortcomings in drawing female characters, didn’t plan on making Sakura a lead. Her unattractiveness made her the target of mockery at school. Were it not for her friend [Ino Yamanaka](https://en.wikipedia.org/wiki/List_of_Naruto_characters#Ino_Yamanaka), she’d be totally alone. She was, however, gifted in her studies, to the point where her grades got her into the famous Team 7 of the Ninja Academy, along with [Naruto Uzumaki](https://en.wikipedia.org/wiki/Naruto_Uzumaki) and [Sasuke Uchiha](https://en.wikipedia.org/wiki/Sasuke_Uchiha). At first, she won’t notice Naruto, who has a secret crush on her. No, Sakura likes Sasuke, the bad boy. But high-born Sasuke is hiding a tragic past: his entire family was murdered by his older brother [Itachi](https://en.wikipedia.org/wiki/Itachi_Uchiha). As the sole survivor, his only goal is vengeance. By contrast, Naruto comes across as a rowdy and undisciplined kid. Yes, he’s kind and polite, but he’s a long way from becoming the ninja master he dreams to be.

**Believe in the impossible**

Despite his weaknesses, Naruto will turn out to be a good friend; he’ll use his infinite inner energy, willpower and leadership skills to reach his goals.

Just as he’s on the verge of getting over Sakura—making way for things to develop with Sasuke—she actually starts to fall for him. The more she discovers about Sasuke, the more Sakura has mixed feelings towards Naruto. For instance, she finds out that Naruto is sealed with the [nine-tailed demon fox](https://en.wikipedia.org/wiki/List_of_Naruto_characters#Kurama), which makes his life very complicated. And when Sakura reveals her feelings to Sasuke, he brushes her off. Sakura will be in the spotlight more and more as the episodes go by, solidifying her place in the saga as one of leads, as well as one of the most well-loved characters. She will grow stronger, become a better fighter, master her spiritual energy (the infamous “chakra”) and get better at healing those around her.

The lesson in all this: the destinies of manga heroes, just like ours, are not pre-determined. With willpower, team spirit, energy and self-confidence, anything is possible. That’s the core message of this manga series, which ultimately became a global hit. The message was so powerful it even inspired a child psychiatry program. After all, Naruto’s ups and downs are a metaphor for teen life: rejected because of his inner demons, then accepted for who he is and treated as a hero.

[![Naruto Manga Shonen Ninja](https://www.europecomics.com/assets/uploads/2020/11/Naruto2-800px-657x1024.jpg)](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-703447-9) © Shueisha Inc.

**GALLY**

[Yukito Kishiro](https://en.wikipedia.org/wiki/Yukito_Kishiro) (writer and artist), [_Battel Angel Alita_](https://kodanshacomics.com/series/battle-angel-alita/) (1990), [KodanshaComics](https://kodanshacomics.com/)

**Cyborg**

“I don’t remember anything from my past life… what kind of person was I? Life itself is something undefined… Is it beautiful or ugly, a punishment or a precious gift, I don’t even know… But I’ll find out!” Gally is a cyborg brought to life by genius mechanic and bounty hunter [Ido Daisuke](https://battleangel.fandom.com/wiki/Daisuke_Ido). Ido repairs androids and robots that he finds in the streets of Kuzutetsu, the scrapyard town located below the aerial city of Zalem, where the privileged live. One day, Ido finds a young girl’s head and fits it onto a robotic body, which he names Gally in honor of his recently deceased cat. He’s touched by her beauty and comes to think of her as his daughter, heedless of the fact that things won’t pan out as he imagines. Determined to follow her own path, Gally rejects the plans Ido has made for her. She becomes a bounty hunter herself and, despite her lack of memory, discovers a real talent for combat. But she also notices that she has feelings—buried deep within—and falls in love with [Yugo](https://battleangel.fandom.com/wiki/Hugo), a young boy whose dream is to make it to Zalem.

**Humanity**

Gally is independent, stubborn, self-confident and proud. She takes part in in the ultra-violent Motorball races, having mastered the Panzer Kunst, a long-forgotten cyborg fighting style. Gally experiences several flashbacks during these races, pushing her to keep fighting in the hope of fully recovering her lost past. _Gunnm_ depicts a cruel dystopia, similar to that in [_Blade Runner_](https://en.wikipedia.org/wiki/Blade_Runner) or [_Rollerball_](https://en.wikipedia.org/wiki/Rollerball_(1975_film)), in which Gally questions her humanity and her limits as an android, clinging to the hope for a better future and fighting to shape it. For personal reasons, Yukito Kishiro was forced to go on hiatus after publishing volume nine of the series, but resumed in 2000 with [_Gunnm Last Order_](https://en.wikipedia.org/wiki/Battle_Angel_Alita:_Last_Order), which placed Gally in a science-fiction epic in Zalem and explored her origin story.

[![Alita manga seinen](https://www.europecomics.com/assets/uploads/2020/11/alita-1-800PX-683x1024.png)](https://kodanshacomics.com/series/battle-angel-alita/) © Kodansha Comics

**SAKURA KINOMOTO**

[Clamp](https://en.wikipedia.org/wiki/Clamp_(manga_artists)) (writer and artist), [_Cardcaptor Sakura_](https://kodanshacomics.com/series/cardcaptor-sakura-clear-card/) (1996), [KodanshaComics](https://kodanshacomics.com/)

**Card hunter**

Curiosity killed the cat! That’s a risk fourth-grader Sakura Kinomoto is willing to take after grabbing a forbidden book from her father’s library. As soon as she opens the book, a little stuffed lion named [Kerberos](https://en.wikipedia.org/wiki/List_of_Cardcaptor_Sakura_characters#Cerberus), a.k.a. Kelo, springs out. Kelo tells her this isn’t just any book: hidden within are Clow Cards, created by a mighty sorcerer who gave the cards dark powers that can be unleashed if they are removed from the book. Each card is actually alive, has a name and can cast a spell. Unfortunately Kelo, who is meant to guard the cards, fell asleep. It’s now up to Sakura to recover the escaped cards, making her the new Cardcaptor. Definitely not an easy job, especially as she still has to go to school. There’s some good news, at least: Sakura has magic powers, which is what allowed her to open the book in the first place. As Kelo points out though, magic powers may be great, but they don’t cure morning laziness!

**Magical girl**

Sakura Kinomoto is a typical “magical girl,” a manga heroine with magical powers. Characters like these are very popular in Japanese comics, for example in the super hit [_Sailor Moon_](https://en.wikipedia.org/wiki/Sailor_Moon), which also served as inspiration for _Cardcaptor_. Sakura’s new persona won’t keep her from being a regular schoolgirl though and everything that comes with it, including crushes. This series was created by Clamp, the renowned group of four female writers who have published in many different genres. Initially written for children, _Cardcaptor Sakura_ also picks up a few themes with a slightly older readership in mind, like the attraction [Tomoyo](https://en.wikipedia.org/wiki/List_of_Cardcaptor_Sakura_characters#Tomoyo_Daidouji) feels for her friend Sakura. The artwork uses elegant lines, and the pages are adorned with arabesques of flower petals, giving the story attributes of a fantastic tale full of tenderness, livened up by the adorable Kelo.

Clamp was formed in 1987 by twelve scriptwriter-artists who became seven, then four. The collective tackles a variety of topics, often placing the same characters in different stories, all of which hope to reach out to kids, teenagers and adults alike.

[![Cardcaptor Sakura Magicial Girl Manga Shojo](https://www.europecomics.com/assets/uploads/2020/11/cardcaptor-800px-683x1024.jpeg)](https://kodanshacomics.com/series/cardcaptor-sakura-clear-card/) © Kodansha Comics

**LADY SNOW BL** **OOD**

[Kazuo Koike](https://en.wikipedia.org/wiki/Kazuo_Koike) (writer) & [Kazuo Kamimura](https://kamimurakazuo.com/) (artist), [_Lady Snowblood_](https://www.darkhorse.com/Books/10-242/Lady-Snowblood-Volume-1-The-Deep-Seated-Grudge-Part-1-TPB) (1972), [DarkHorseComics](https://www.darkhorse.com/)

They call her Lady Snowblood or Princess Bloodshed. An inspiration for Tarantino’s _[Kill Bill](https://en.wikipedia.org/wiki/Kill_Bill:_Volume_1),_ she is neither a professional killer nor a regular thief, but a young woman obsessed with her mission: avenging her mother and getting her way in a male-dominated world. Her fate is sealed: she’ll never know love or happiness, doomed as she is to endlessly do the “dance of death.”

**Blood child**

“My name is Lady Snowblood. I am the child of bloodshed, born on a day of falling snow.” Yuki is her real name. Snow and blood, white and red, life and death, innocence and crime… She was born as pure as the snowflakes that fell on that day and was predestined to be dancing around death, always trying to reach purity through violence. Yuki was born in prison, where her mother Osayo was sent after killing the murderers of her son and husband. Osayo resigned herself to her fate, and seduced the men she came across in her captivity in the hope of giving birth to a strong boy who would restore her honor and right all wrongs. But life is full of surprises, and she had a girl. Twenty years later, Yuki is ready to deal with unfinished business and face her destiny.

**Bloody screaming red**

“Lady Snowblood is a family drama, with seduction, vengeance and death embodied in this young avenger,” explains [Jean-Pierre Dionnet](https://en.wikipedia.org/wiki/Jean-Pierre_Dionnet) in the introduction to the French edition. Lady Snowblood is superbly illustrated in black and white by Kazuo Kamimura and written by [_Crying Freeman_](https://en.wikipedia.org/wiki/Crying_Freeman) and [_Lone Wolf and Cub_](https://en.wikipedia.org/wiki/Lone_Wolf_and_Cub) writer Kazuo Koike, who Dionnet describes as the modern Alexandre Dumas. Lady Snowblood is a fascinating and troubling character, an angel-faced heroine that kills in the most brutal ways. Yuki is a bloody, screaming red avalanche, killing for money, but more than that, to avenge her mother’s honor. Her life, her ambitions, even her own happiness: they all pale in comparison to her mission. She must be selfless in order to get the job done. This is what makes her such an admirable character. In her own words: “I surrender myself to my destiny, without letting anyone in, without love. I’m the woman that never turns around, I’m Lady Snowblood, the avenger.”

[![Lady Snowblood seinen Manga Geisha](https://www.europecomics.com/assets/uploads/2020/11/lady-800px-721x1024.jpg)](https://www.darkhorse.com/Books/10-242/Lady-Snowblood-Volume-1-The-Deep-Seated-Grudge-Part-1-TPB) © Shueisha Inc.

**Unattainable happiness**

The 1973 film adaptation of [_Lady Snowblood_](https://en.wikipedia.org/wiki/Lady_Snowblood_(film)) that featured [Meiko Kaji](https://en.wikipedia.org/wiki/Meiko_Kaji) as Yuki inspired Quentin Tarantino in the making of _Kill Bill_. But the movies don’t have the time to delve into the details of Yuki’s origin story, which is exactly what the comics were able to do. We’re talking about a storyline of more than one thousand three hundred pages, in which Yuki’s adventures and destiny unfold against the backdrop of 19th-century Japan and the turbulent era of the Meiji dynasty. _Lady Snowblood_ revolves around one crucial question: will she finally be free once vengeance is complete? Will she be able to choose her own fate? Or is she bound to remain a killing machine, a human-like demon on the path of vengeance, forgotten— as one of her masters tells her—by Buddha himself?

After many years of causing bloodshed, a guilt-ridden Yuki decides to be of service to Japanese society, using her skills to help the destitute in the slums. But it won’t be long before she finds herself doing the dance of death all over again, sliding back into a meaningless life of violence. It’s as if her fate was sealed by her mother’s will on the day she was born, a destiny much stronger than her desire to escape all the savagery. As Jean-Pierre Dionnet explains: “A woman who lives on the edges leaves tears behind, but a woman who lives for vengeance leaves her heart.” He adds, “These characters will never be happy, their fate is pre-determined. Their only choice is to keep on living this dangerous life so as to achieve their destiny.”

[![Lady Snowblood Geisha Manga Seinen](https://www.europecomics.com/assets/uploads/2020/11/lady2-800px-788x1024.jpg)](https://www.darkhorse.com/Books/10-242/Lady-Snowblood-Volume-1-The-Deep-Seated-Grudge-Part-1-TPB) © Shueisha Inc.

**LUCY HEARTFILIA**

[Hiro Mashima](https://en.wikipedia.org/wiki/Hiro_Mashima) (writer and artist), [_Fairy Tail_](https://kodanshacomics.com/series/fairy-tail-blue-mistral/) (2006), [KodanshaComics](https://kodanshacomics.com/)

**A young and sexy wizard**

At first sight, Lucy Heartfilia could be mistaken for a typical bimbo seen on reality shows. The image couldn’t be further from the truth: this seventeen-year-old loves reading and is secretly writing her first novel. But more importantly, just like her mother before her, Lucy is a wizard. A celestial wizard, in fact, capable of opening doors between worlds and calling upon spirits with whom she has signed a pact. She’s not like the old and ugly wizards we sometimes see, or the pretentious half-witches that surround themselves with mystery and think such powers shouldn’t be in the hands of a mere girl. Lucy is living proof that wizards can be young, sexy and fun.

**Nutjobs**

Being wild and free, Lucy obviously dreams of joining the Fairy Tail Guild, the most famous group of wizards, which is made up of kind and welcoming nutjobs. The group makes a mess everywhere they go, always up for a fight and constantly measuring up against one another. “After all, the world wouldn’t be a fun place without them!” notes one of the characters. Lucy will make a lot of friends, not only in the guild but also among readers, who will flock to this story of adventures and setbacks, full of humor and shared madness, very much like [_Dragon Ball_](https://www.viz.com/dragon-ball) or [_One Piece_](https://www.viz.com/one-piece). As crazy as this environment may be, Lucy still manages to keep a cool head. She didn’t have it easy as a kid, having lost her mother when she was ten. She came to resent her father for focusing on his work over her upbringing. But now, Lucy can count on her friends. In the Fairy Tail Guild, “one person’s happiness is everyone’s happiness. And one person’s tears are everyone’s tears.”

[![Fairy Tail Shonen Comics](https://www.europecomics.com/assets/uploads/2020/11/fairy-tail-1-800px-683x1024.jpg)](https://kodanshacomics.com/series/fairy-tail-blue-mistral/) © Kodansha Comics

**B UL** **MA**

[Akira Toriyama](https://en.wikipedia.org/wiki/Akira_Toriyama) (writer and artist), [_Dragon Ball_](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-782520-6) (1984), [Shueisha](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-782520-6) / Bird Studio

**A romantic teenager**

At the beginning of _Dragon Ball_, Bulma is but a teenager. And like most girls her age, she’s very romantic and dreams of finding her Prince Charming. To make her wish come true, she needs to find the seven Dragon Balls whose magical powers—once united—can grant anyone their most heartfelt wish. Her Dragon Ball detector will lead her to the first one, kept by future saga lead [Sangoku](https://en.wikipedia.org/wiki/Goku), an adorably naïve, monkey-tailed boy. She asks him to join her quest, thinking he might be a good bodyguard down the line. Bulma’s a tough cookie, and her wits make up for her lack of combat skills in a world where fighting is daily business. “Touch your own butt, old man!” she tells [Turtle Hermit](https://en.wikipedia.org/wiki/Master_Roshi), known for his perverted antics. Smart and inventive, she will become known for her inventions like the Dragon Ball detector and the Micro Band, a watch that can shrink the person who wears it.

**Wife and mother**

_Dragon_ Ball is the perfect example of the kind of [_shōnen_](https://en.wikipedia.org/wiki/Sh%C5%8Dnen_manga) manga aimed at teenage boys. Sangoku—a “mirror-hero”—shares their concerns about life. He goes through the various stages of maturity, increasing his self-knowledge and pushing his limits by subduing his opponents, who sometimes end up siding with him. By the end of the saga, Bulma is a grown woman who’s followed her own path. After more than a thousand pages of storyline, she has become a wife and a mother, while always being there for her friends. Just like Sangoku, she has evolved and changed outfits and hairstyle countless times. She’s also become more mature and had two children with Sangoku’s old foe-turned-friend [Vegeta](https://en.wikipedia.org/wiki/Vegeta). Toriyama’s wild imagination transported readers with humor and the colorful characters that make up the big family in this fast-paced, funny and punchy story. With around 350 million copies sold worldwide, _Dragon Ball_ was a massive hit. In France, the title was launched in 1993, solidifying manga’s place alongside the country’s well-loved _bandes dessinées_.

[![Dragon Ball Shonen Manga](https://www.europecomics.com/assets/uploads/2020/11/dragon-ball-800-px-737x1024.jpg)](https://books.shueisha.co.jp/items/contents.html?isbn=978-4-08-782520-6) © Shueisha Inc. / Bird Studio

_The Heroines of Comics : Japan_ is the third and last of a three part article series _The Heroines of Comics_.

Part 1: [The Heroines of Comics : Europe](https://www.europecomics.com/heroines-european-comics/)

Part 2: [The Heroines of Comics](https://www.europecomics.com/heroines-comics-usa/) [: USA](https://www.europecomics.com/heroines-comics-usa/)

Header image: \[SHURAYUKIHIME] © [KAZUO KOIKE](https://en.wikipedia.org/wiki/Kazuo_Koike) & [KAZUO KAMIMURA](https://kamimurakazuo.com/) / Koike Shoin Publishing Co., Ltd., 1972

Return to Part 2


References:  
https://html-to-markdown.com/demo