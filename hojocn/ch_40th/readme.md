
# 关于City Hunter 40周年「北条司展」的一些信息

<a name="Sources"></a>  
## 1. 数据来源  

- 北条司官网: 
[03-18](./official_2024-03-18.md)  

- [Gallery Zenon官网](./gallery-zenon.md), 
[on X(Twitter)](././gallery-zenon_twitter.md), 
[福冈展](./gallery-zenon_fukuoka.md)      

- COAMIX官网: 
[03-18](./coamix_2024-03-18.md), 
[04-17](./coamix_2024-04-17.md),   

- [COAMIX官方微博](https://m.weibo.cn/detail/5013567556292490)    

- Edition-88.com:  
[03-19](./edition-88_2024-03-19.md),  
[04-11 Instagram Campaign](./edition-88_2024-04-11.md), 
[04-11 X Campaign](./edition-88_2024-04-11_2.md),  
[05-17 Instagram Campaign](./edition-88_2024-05-17.md), 
[05-17 X Campaign](./edition-88_2024-05-17_2.md),  
[05-22 Report](./edition-88_2024-05-22_report.md),  
[Goods](./edition-88_goods.md),  

- SNS Messages: [东京展](./twitter.md), 
[福冈展](./twitter_fukuoka.md)  

- [福冈展@九州福冈御宅族媒体](https://fukuoka-otaku.net/hojo-tsukasa-ten/), 
[「北条司展 IN 福岡」会場Report](./2024-10-18_report_fukuoka.md)  

- 其他：  
    - [受人敬仰的老师的原创画展@Note.com](../zz_note.com/2024-11-07_hojo-exhibition.md)  
    - [GALLERY ZENON 在吉祥寺开幕！北条司展正在展出 @kichinavi.net](./kichinavi.net_2024-04-17.md),  
    - [因为北条司展太精彩了，所以我迅速写下了感想... @denfaminicogamer](./denfaminicogamer_2024-04-18.md)  
    - [守护XYZ的香 @Note](../zz_note.com/2024-04-26_hojo-exhibition.md)  
    - [yasubeblog @hatenablog](./hatenablog_2024-05-01.md),  
    - [【北条司展】170 多幅原画、真人电影原稿和其他珍贵展品](https://animageplus.jp/articles/detail/58124)    
    - [「北条司展」（城市猎人的作者）上的插图作品视频给我留下了深刻印象 @seidoku.jp](./seidoku_2024-05-29.md)  
    - [北条司展@Note.com](../zz_note.com/2024-06-12_hojo-exhibition.md)  
    - [在吉祥寺的新景点Gallery Zenon 举办『北条司展』@Ameblo.jp](./ameblo_2024-06-15.md)  
    - [展期仅剩几天！ 北条司展@Note.com](../zz_note.com/2024-06-21_hojo-exhibition.md)  

-------------------  

<a name="Details"></a>  
## 2. 展览信息 & 部分展品  

---  

- [活动概述](./details0.md#eventinfo)  
- [东京展场外](./details0.md#event01)  
- [东京展一楼展区](./details0.md#event02)  
- [东京展二楼展区](./details0.md#event03)  
- [东京展一楼商品区](./details0.md#event04)  
- [东京展其他场景](./details0.md#event05)  
- [福冈展场外](./details0.md#event10)  
- [福冈展展区](./details0.md#event11)  
- [福冈展留言区](./details0.md#event12)  
- [福冈展展区商品区](./details0.md#event13)  
- [买家实拍](./details0.md#BuyersPhoto)  

---  

- [写在前面的话](./details1.md#note)  
- [作者简介 & 致辞](./details1.md#greetings)  
- [『我是男子汉！』](./details1.md#i-am-a-man):  
    [解说文字](./details1.md#i-am-a-man-info)  
    [![](img/thumb/fukusei-otoko-02.jpg)](./details1.md#1980-08-i0) 
    [![](img/thumb/GLlumlLbsAAqp2A-1.jpg)](./details1.md#1980-08-i1) 
    [![](img/thumb/GLlumlLbsAAqp2A-2.jpg)](./details1.md#1980-08-i2) 
    [![](img/thumb/GLlumlLbsAAqp2A-3.jpg)](./details1.md#1980-08-i3) 

- [『Cat's♥Eye』](./details1.md#ce):  
    [解说文字](./details1.md#ce-info)  
    [![](img/thumb/GMLxqllboAAQU3h.jpg)](./details1.md#1981-04-i0) 
    [![](img/thumb/GP9Emm4bwAAVURq.jpg)](./details1.md#1981-11-i0) 
    [![](img/thumb/GL6vgHvbgAAuPEe.jpg)](./details1.md#1981-12-i0) 
    [![](img/thumb/GMG2-AhaIAATU_G.jpg)](./details1.md#1982-07-i0) 
    [![](img/thumb/GP4kAKuaMAAsy8T.jpg)](./details1.md#1982-11-i0) 
    [![](img/thumb/GUgSgRfa8AEJsVx.jpg)](./details1.md#1983-04-i0) 
    [![](img/thumb/GMrZ68eXEAATkRS.jpg)](./details1.md#1983-0407-i0) 
    [![](img/thumb/GN6e01sbIAA20vy-1.jpg)](./details1.md#1983-0407-i1) 
    [![](img/thumb/441505787_17969220491718230_9027313139535278166_n.jpg)](./details1.md#1983-0407-i2) 
    [![](img/thumb/GNs7ClDa0AAdPRA.jpg)](./details1.md#1983-07-i0) 
    [![](img/thumb/GLbVz2LaAAANswV-1.jpg)](./details1.md#1983-07-i1) 
    [![](img/thumb/GP4kAMXaYAAuW7u.jpg)](./details1.md#1983-07-i2) 
    [![](img/thumb/GNruiz8a4AIRkaa.jpg)](./details1.md#1983-11-i0) 
    [![](img/thumb/GaUeNnhbUAAf7WG.jpg)](./details1.md#1983-12-i0) 
    [![](img/thumb/GLWG9SAacAARxSN.jpg)](./details1.md#1984-03-i0) 
    [![](img/thumb/GP4kANda4AAlGN-.jpg)](./details1.md#1984-05-i0) 
    [![](img/thumb/GP2hAahaAAAhWkz.jpg)](./details1.md#1984-08-i0) 
    [![](img/thumb/GP4kAOTbcAAgcvi.jpg)](./details1.md#1984-09-i0) 
    [![](img/thumb/GMLxqkcaoAAuHxS.jpg)](./details1.md#1985-unknown-i0)  

- [『City Hunter』](./details1.md#ch):  
    [解说文字](./details1.md#ch-note)  
    [![](img/thumb/GLbg_gUaIAAv2Fx-0.jpg)](./details1.md#ch00) 
    [![](img/thumb/GLbg_gUaIAAv2Fx-1.jpg)](./details1.md#ch001) 
    [![](img/thumb/ch-3.jpg)](./details1.md#ch002) 
    [![](img/thumb/ch-2.jpg)](./details1.md#ch003) 
    [![](img/thumb/GNrui09b0AAYuOt.jpg)](./details1.md#ch004) 
    [![](img/thumb/GaEOFx9b0AQNQFD-0.jpg)](./details1.md#ch005) 
    [![](img/thumb/GN6e01uagAAI7S-1.jpg)](./details1.md#ch006) 
    [![](img/thumb/GLubRMsbwAAczxE.jpg)](./details1.md#ch007) 
    [![](img/thumb/GLgp2Kzb0AAvGMD-0.jpg)](./details1.md#ch008) 
    [![](img/thumb/GLgp2Kzb0AAvGMD-1.jpg)](./details1.md#ch009) 
    [![](img/thumb/GLrTOA4aMAApJlc-0.jpg)](./details1.md#ch010) 
    [![](img/thumb/GLrTOA4aMAApJlc-1.jpg)](./details1.md#ch011) 
    [![](img/thumb/GLrTOA4aMAApJlc-2.jpg)](./details1.md#ch012) 
    [![](img/thumb/GLrTOA4aMAApJlc-3.jpg)](./details1.md#ch013) 
    [![](img/thumb/GNrui07bEAAHuP_.jpg)](./details1.md#ch014) 
    [![](img/thumb/GLlsB26aYAAur3j-1.jpg)](./details1.md#ch015) 
    [![](img/thumb/GNYFBRrbYAAy0Z4.jpg)](./details1.md#ch016) 
    [![](img/thumb/GaUeNwtbUAAFfd9.jpg)](./details1.md#ch017) 
    [![](img/thumb/GObhSsBbYAACjgI.jpg)](./details1.md#ch018) 
    [![](img/thumb/GaYo2Bea8AAvVSY.jpg)](./details1.md#ch019) 
    [![](img/thumb/GNhgqi4bUAAj9ul.jpg)](./details1.md#ch020) 
    [![](img/thumb/GMpBzHBb0AAz1kw.jpg)](./details1.md#ch021) 
    [![](img/thumb/GLvzDJybYAAokIu.jpg)](./details1.md#ch022) 
    [![](img/thumb/cityhunter1.jpg)](./details1.md#ch023) 
    [![](img/thumb/GPoc-cnaQAAtDL8.jpg)](./details1.md#ch024) 
    [![](img/thumb/GPoc-ckbQAA5ZqL.jpg)](./details1.md#ch025) 
    [![](img/thumb/GPoc-cka0AAnNAN.jpg)](./details1.md#ch026) 
    [![](img/thumb/GPoc-cnaMAAGP1A.jpg)](./details1.md#ch027) 
    [![](img/thumb/444952288_17969220500718230_1914369075704752665_n.jpg)](./details1.md#ch028) 
    [![](img/thumb/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3.webp)](./details1.md#ch029) 
    [![](img/thumb/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3.webp)](./details1.md#ch029.5) 
    [![](img/thumb/441567956_17969220527718230_5452684696600726760_n.jpg)](./details1.md#ch030) 
    [![](img/thumb/GapiNseaoAEOuH2.jpg)](./details1.md#ch031) 
    [![](img/thumb/GPr3XFPbAAEudoW.jpg)](./details1.md#ch032) 
    [![](img/thumb/447197235_828063905376636_1750104749879296705_n.jpg)](./details1.md#ch033) 
    [![](img/thumb/GNBdNdHawAAeNhn.jpg)](./details1.md#ch034) 
    [![](img/thumb/GLXd5Eda0AAU9J6.jpg)](./details1.md#ch035) 
    [![](img/thumb/GQHWDU7b0AABtrt.jpg)](./details1.md#ch036) 
    [![](img/thumb/2024-05-29-13.01.33.jpg)](./details1.md#ch037) 
    [![](img/thumb/GOJnVhfaEAAt7u1.jpg)](./details1.md#ch038) 
    [![](img/thumb/GP9Emq5bcAEj6I-.jpg)](./details1.md#ch039) 
    [![](img/thumb/6_hojo-ten_88graphlight_1.jpg)](./details1.md#ch040) 
    [![](img/thumb/GP4j_p1a0AAM5jp.jpg)](./details1.md#ch041) 
    [![](img/thumb/GPWGf5GbcAAmgJU.jpg)](./details1.md#ch042) 
    [![](img/thumb/GPIzChUb0AAo3kj.jpg)](./details1.md#ch043) 
    [![](img/thumb/GPljn8kaMAA6oM1.jpg)](./details1.md#ch044) 
    [![](img/thumb/GQSJEDEaQAAl4j0.jpg)](./details1.md#ch045) 

- [『白猫少女』](./details2.md#catlady):   
    [解说文字](./details2.md#catlady-note)  
    [![](img/thumb/GL7EP2-bAAAvyDD.jpg)](./details2.md#catlady01) 
    [![](img/thumb/GauY3PYbUAAbQY9.jpg)](./details2.md#catlady02) 
    [![](img/thumb/GN6fEKnaMAAAaL-3.jpg)](./details2.md#catlady03) 
    [![](img/thumb/GLw8JEAbQAAx2NA.jpg)](./details2.md#catlady04) 

- [『SPLASH!』](./details2.md#splash):  
  (似乎未展示)

- [『SPLASH! 2』](./details2.md#splash2):  
  (似乎未展示)

- [『天使的礼物』](./details2.md#angelgift):  
    [解说文字](./details2.md#angelgift-note)  
    [![](img/thumb/3_hojo-ten_fukuseigenko_0.jpg)](./details2.md#angelgift01) 
    [![](img/thumb/GP4kBcxa4AEN-IB.jpg)](./details2.md#angelgift02) 

---  

- [『SPLASH! 3』](./details2.md#splash3):  
    [解说文字](./details2.md#splash3-note)  
    [![](img/thumb/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3.webp)](./details2.md#splash300) 
    [![](img/thumb/th_A4_09513-0.webp)](./details2.md#splash301) 
    [![](img/thumb/C6SaAlVy7hn.jpg)](./details2.md#splash302) 
    [![](img/thumb/GLRg8aoaUAAzaQc-r.jpg)](./details2.md#splash303) 
    [![](img/thumb/GQCqdmEa0AAzi5a.jpg)](./details2.md#splash304) 

- [『SPLASH! 4』](./details2.md#splash4):  
  (似乎未展示)

- [『TAXI DRIVER』](./details2.md#taxidriver):  
    [解说文字](./details2.md#taxidriver-note)  
    [![](./img/thumb/fukusei-taxi-02.jpg)](./details2.md#taxidriver01) 
    [![](img/thumb/GMLdVQnbAAAsWUv.jpg)](./details2.md#taxidriver02) 

- [『Family Plot』](./details2.md#familyplot):  
    [解说文字](./details2.md#familyplot-note)  
    [![](img/thumb/GP4kBdHaIAEt6c0.jpg)](./details2.md#familyplot01) 
    [![](img/thumb/447057571_974441087488357_3539414436810596178_n.jpg)](./details2.md#familyplot02) 
    [![](img/thumb/GPiklt2bMAAGs8r.jpg)](./details2.md#familyplot03) 

- [『少女の季節』](./details2.md#girlseason):  
    [解说文字](./details2.md#girlseason-note)  
    [![](img/thumb/GP4kBgWbUAAG_zh.jpg)](./details2.md#girlseason01) 
    [![](img/thumb/GPjuOTtbAAAKyJT.jpg)](./details2.md#girlseason02) 

- [『樱花盛开时』](./details2.md#cheeryblossoms):  
    [解说文字](./details2.md#cheeryblossoms-note)  
    [![](img/thumb/GLaXKJRbMAAix0D.jpg)](./details2.md#cheeryblossoms01) 
    [![](img/thumb/GXXZ5SNaYAAzK9V.jpg)](./details2.md#cheeryblossoms02) 
    [![](img/thumb/GMtB5jXaAAAi0xz.jpg)](./details2.md#cheeryblossoms03) 
    [![](img/thumb/GLaXKJVaAAADX2V.jpg)](./details2.md#cheeryblossoms04) 

- [『阳光少女』](./details2.md#underthedappledsun):  
    [解说文字](./details2.md#underthedappledsun-note)  
    [![](img/thumb/GNtBqeAbcAAF4uA.jpg)](./details2.md#underthedappledsun01) 
    [![](img/thumb/GX4yVb0aUAQRiht.jpg)](./details2.md#underthedappledsun02) 
    [![](img/thumb/GLaXKJUaIAAeA0i.jpg)](./details2.md#underthedappledsun03) 
    [![](img/thumb/GLg0WJLaUAAr205.jpg)](./details2.md#underthedappledsun04) 
    [![](img/thumb/GYeJ86nasAANR8N.jpg)](./details2.md#underthedappledsun05) 
    [![](img/thumb/GX4yVbxa4AAosbd.jpg)](./details2.md#underthedappledsun06) 
    [![](img/thumb/GM1O4WRaQAEcDn2.jpg)](./details2.md#underthedappledsun07) 
    [![](img/thumb/GZAxvYRbsAAvPAB.jpg)](./details2.md#underthedappledsun08) 
    [![](img/thumb/GZAxvYRbAAQsbf9.jpg)](./details2.md#underthedappledsun09) 

- [『Rash!!』](./details2.md#rash):   
    [解说文字](./details2.md#rash-note)  
    [![](img/thumb/GMslm-DbAAAMAvc.jpg)](./details2.md#rash01) 
    [![](img/thumb/GZmRYLPaMAA_y5T.jpg)](./details2.md#rash02) 
    [![](img/thumb/GZmRYLObkAAth0H.jpg)](./details2.md#rash03) 
    [![](img/thumb/GZmRYLOaEAALt8B.jpg)](./details2.md#rash04) 
    [![](img/thumb/GMslm-CacAAMMVJ.jpg)](./details2.md#rash05) 
    [![](./img/thumb/fukusei-rash-02_4800x.jpg)](./details2.md#rash06) 
    [![](img/thumb/GaKTtZtbcAA3YAI.jpg)](./details2.md#rash07) 
    [![](img/thumb/GMpBzHBbMAEfct2.jpg)](./details2.md#rash08) 
    [![](img/thumb/GNAUMPVbcAQ76NY.jpg)](./details2.md#rash09) 

- [『SPLASH! 5』](./details2.md#splash5):  
  (似乎未展示)

- [『F.COMPO』](./details2.md#fcompo):  
    [解说文字](./details2.md#fcompo-note)  
    [![](img/thumb/GP4kBika4AAceZb.jpg)](./details2.md#fcompo01) 
    [![](img/thumb/GPr5ysHaIAETVtL.jpg)](./details2.md#fcompo02) 
    [![](img/thumb/447689498_964887941764408_4264677107388491346_n.jpg)](./details2.md#fcompo03) 
    [![](img/thumb/Ga4W4BnaMAAOiQt-dr.jpg)](./details2.md#fcompo04) 
    [![](img/thumb/447194197_476524268156325_6779590998158583109_n.jpg)](./details2.md#fcompo05) 
    [![](img/thumb/GQdosvtaIAMttrL.jpg)](./details2.md#fcompo06) 
    [![](img/thumb/GPEV86AbgAAx784-0.jpg)](./details2.md#fcompo07) 
    [![](img/thumb/GPEV86AbgAAx784-1.jpg)](./details2.md#fcompo08) 
    [![](img/thumb/447113427_1779870629168580_2289813680382420427_n.jpg)](./details2.md#fcompo09) 

- [『Angel Heart』](./details2.md#angelheart):  
    [解说文字](./details2.md#angelheart-note)  
    [![](img/thumb/GQkca4QaEAA099J.jpg)](./details2.md#angelheart01) 
    [![](img/thumb/GO-gcKobMAAlubr.jpg)](./details2.md#angelheart02) 
    [![](img/thumb/GQF-NJubEAAHgSI.jpg)](./details2.md#angelheart03) 
    [![](img/thumb/GQvs5FzbsAAzBo5-0.jpg)](./details2.md#angelheart04) 
    [![](img/thumb/GP4kA0pbIAINPlN.jpg)](./details2.md#angelheart05) 
    [![](img/thumb/1040g008314cc8640me404a18f2vloj5oa8uolao!nd_dft_wlteh_webp_3.webp)](./details2.md#angelheart07) 
    [![](img/thumb/GP4kAzsaAAEtt7S.jpg)](./details2.md#angelheart08) 
    [![](img/thumb/GQFP2B_aQAA9kDh.jpg)](./details2.md#angelheart09) 
    [![](img/thumb/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3.webp)](./details2.md#angelheart10) 
    [![](img/thumb/448915927_3759504364367737_7351037500985462730_n.jpg)](./details3.md#angelheart12) 
    [![](img/thumb/1_hojo-ten_88graph_80062c9b-1999-4174-b696-d31f53a61845_1.jpg)](./details3.md#angelheart13) 
    [![](img/thumb/GQLcPN-bsAAwxcB.jpg)](./details3.md#angelheart14) 
    [![](img/thumb/GQr4XpoawAAgTtl-0.jpg)](./details3.md#angelheart15) 
    [![](img/thumb/GOOh88fa0AA4idt.jpg)](./details3.md#angelheart16) 
    [![](img/thumb/GaUeNuCbUAMtz2O.jpg)](./details3.md#angelheart17) 
    [![](img/thumb/GQLyg2aaUAAdzGm.jpg)](./details3.md#angelheart18) 
    [![](img/thumb/GMKmes2bIAAq4q2.jpg)](./details3.md#angelheart19) 
    [![](img/thumb/GP4kA3XasAAcK6k.jpg)](./details3.md#angelheart20) 

---  

- [其他](./details3.md#others):  
[![](img/thumb/GOJnVhhbEAAUiXM.jpg)](./details3.md#others01) 
[![](img/thumb/GMVijw7aEAA62VF.jpg)](./details3.md#others02) 
[![](img/thumb/GLiG0mCb0AApB21.jpg)](./details3.md#others03) 
[](./details3.md#) 
[](./details3.md#) 


---  
- [总结](./details-summary.md)

-------------------  

<a name="Review"></a>  
## 3. 观后感  
[个人感受](./review.md)  

-------------------  

**Links:**  

- https://ocr.space/  
- [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
- [html-to-markdown](https://codebeautify.org/html-to-markdown), 
  [HTML to Markdown Converter](https://htmlmarkdown.com/)  
- [DeepL](https://www.deepl.com)  
- [腾讯翻译君](https://fanyi.qq.com/)  
- [有道翻译](https://fanyi.youdao.com)  
- [Bing翻译](https://cn.bing.com/translator)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
