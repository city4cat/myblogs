https://news.denfaminicogamer.jp/kikakuthetower/240418b  

# 北条司展が素晴らしすぎたので、速攻で感想を書いた。連載作品から読切までを網羅した大満足の原画展、ネットフリックス版の「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も展示  
因为北条司展太精彩了，所以我迅速写下了感想。网罗连载作品到单话故事的大满足原画展，也展示了北条司老师描绘的Netflix版“铃木亮平饰演冴羽獠”的作品  

2024年4月18日 12:19  


![article-thumbnail-240418b](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_00071.jpg)

**『シティーハンター』**が好きだ。
我喜欢《城市猎人》。  

子供のころ。最初に憧れたヒーローは**冴羽獠**だった。  
孩提时代最初憧憬的英雄是冴羽獠。  

“新宿の種馬”こと冴羽獠は、感情表現のリミッターが外れているほど女好きで、普段はおちゃらけてばかり。  
“新宿之种马”冴羽獠，打破了嗜女色到感情表达的极限，平时只会逗笑。  

ただ、自分にとって大切な人たちのピンチや心を揺さぶられた依頼人のために、動くときは言葉にできないほどのカッコよさがある。決めるところはビシッと決める。アクションシーンだけでなく、日常の中でちょっとイイことを言う時も、普段とのギャップもあってか、本当にカッコよすぎる。  
然而，当他在紧要关头为自己重要的人或心动的委托人采取行动时，他的冷静是无法形容的。 当做决定时，他很果断。 不仅是在动作戏中，在日常生活中说一些好听的话时，他也实在是太酷了，这大概是他与平时性格的差距所致。  

そんな冴羽獠のカッコよさに惹かれたのか、『シティーハンター』は私にとっても大切な作品として、心に残り続けていた。  
也许正是冴羽獠的这种酷吸引了我，《城市猎人》系列一直是我心中的重要作品。  

また、2024年4月25日（木）は、**鈴木亮平氏の主演でNetflix映画『シティーハンター』の配信が開始**されることもあり、個人的にも『シティーハンター』熱が再燃しはじめていた。予告映像を見ただけで原作への愛が伝わってくる。今か今かと配信を楽しみにしているのは、私だけではないはずだ。  
此外，2024 年 4 月 25 日（周四），由铃木亮平主演的 Netflix 电影《城市猎人》将上映，我个人的《城市猎人》热又开始重新点燃。 光看预告片，我就能感受到大家对原著故事的热爱。 我相信，期待影片发行的不止我一个人。  

そんな中、電ファミに一通のメールが届いた。  
就在此时，Denfami 收到了一封邮件。  

> 2024年4月17日(水)よりギャラリーゼノン（旧：ゼノンサカバ）にて「北条司展 The road to 『CITY HUNTER』40th anniversary 2025〜Limited Special Exhibition〜」がスタート致します――。  
「北条司展 The road to 『CITY HUNTER』40th anniversary 2025〜Limited Special Exhibition〜」将于 2024 年 4 月 17 日（周三）在Gallery Zenon（原 Zenon Sakaba）举行。  

…行くしかない。  
...别无选择，只能前往。  

しかも、前日には関係者向けの内覧会があるとのこと。普段は電ファミでも執筆以外のお仕事ばかりしている私だが、ここは行くしかない。私が行くしかないと思い、編集長に打診し、内覧会に足を運んだ。  
而且，前一天还有面向相关人士的内部展览会。平时在Denfaminico也只做写作以外的工作的我，这里只能去了。我想只能由我去了，于是向主编打听，去看了内部展览会。  

**もうね。本当にいい。素晴らしすぎる。**  
我不得不说，它真的很棒。 真的很不错。   

展示コーナーに入った瞬間。『シティーハンター』の主人公とヒロインが待ち構えている。  
进入展区的那一刻 《城市猎人》的男女主角正在等着我。  

[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_001](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_00071.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_00071)  
北条司展报告。这是一次非常令人满意的原创作品展览，涵盖了从连载作品到读后感的所有内容。 展览中还有一幅作品，是北条司绘制的铃木亮平饰演的冴羽。  

じっくりと見….ようと思ったとき、頭に電撃が走った。この場では、もっこりしたと言えばいいだろうか。  
请慢慢欣赏.... 当我准备去看展览的时候，脑子里突然电击了一下。 我想我应该说Mokkori。（译注：待校对）  

**北条司先生作品の楽曲（私の場合は、『シティーハンター』）の曲を聴きながら展示を見て回ったらとんでもない瞬間になるのではないか**、と。  
我想，如果我一边听着北条司作品的音乐（我听的是《城市猎人》），一边环顾展览，那将是一个非同寻常的时刻。  

早速、愛用のイヤホンを耳にねじ込む。どの曲にする…と一瞬悩んだが、まずはこれだろう。  
我立刻把心爱的耳机塞进耳朵里。 一时间，我不知道自己该听哪首歌，但必须先听这首歌。  

「CITY HUNTER〜愛よ消えないで〜」続いて、「MR. PRIVATE EYE」、「SARA」、「STILL LOVE HER (失われた風景)」をリピートしながら展示会場を歩き回る。生原画一つひとつをじっくりと見つつ、**北条司先生に質問のコーナー**もあったので、そちらもふむふむと読み込んでいく。  
我在展厅里走了一圈，重复着 “城市猎人--爱恋”，然后是 “MR.PRIVATE EYE”、“SARA ”和 “STILL LOVE HER (Lost Landscape)”。 在仔细观看每幅原画的同时，我还看到了一个向北条司老师提问的角落，于是我也仔细阅读了一下。  

[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_002](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09841.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09841)

すべての展示を見終わるのに、おおよそ30分くらいだっただろうか。高い満足感に包まれた私は今、北条司先生のスペシャルブレンドコーヒーを飲みながらこの原稿を書いている。美味い。これがキャッツアイの味か。  
看完所有的展览，大约花了30分钟。被高度满足感包围的我，现在一边喝着北条司老师的特别调制咖啡，一边写着这篇稿子。真好喝。这就是猫眼的味道吗?  

毎回書いているような気がするが、私は記者ではなく営業だ。いや、最近は営業の仕事ばかりでもないのだが。  
好像每次都在写，但我不是记者，是营业员。不，最近也不是全是销售的工作。  

改めて、北条司展の見どころを3行で伝えて、短いがこの原稿を終えたいと思う。  
再次，我想用3行文字介绍北条司展的看点，简短地结束这个原稿。  

> ・北条司先生ファンでも、ファンじゃなくても大満足の展示が多数  
> ・ネットフリックスのドラマが楽しみなファンは絶対足を運んだほうがいい  
> ・鈴木亮平さんのファンの方は絶対に絶対に行ったほうがいい
·不管是北条司老师的粉丝，还是不是粉丝，都有很多非常满意的展示  
·期待netflix电视剧的粉丝们绝对要去看看  
·铃木亮平的粉丝们一定要去  

今回、**鈴木亮平さん扮する冴羽獠を北条司先生が描き下ろした展示**もあった。正直、カッコよすぎた。シビれた。  
这次，北条司老师也描绘了铃木亮平扮演的冴羽獠的獠。说实话，太帅了。麻木了。  

語彙がなくなるくらいよかったので、皆さんもぜひ、期間内に足を運んでみてください。  
已经词穷了，所以请大家一定要在这期间去看看。  

イヤホンから「Get Wild」のイントロが流れはじめたので、今日はこのあたりで筆を置きたい。  
耳机里开始播放《Get Wild》的开头，今天就写到这里吧。  

以下は、展示会で表に出していいお写真を紹介していく。とにかく、『シティーハンター』、北条司展はいいぞ！   
下面介绍一些可以在展示会正面展示的照片。总之，《城市猎人》，北条司展不错!  

[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_003](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_00209.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_00209) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_004](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_00214.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_00214) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_005](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09450.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09450) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_006](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09513.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09513) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_007](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09871.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09871) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_008](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09844.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09844) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_009](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09467.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09467) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_010](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09880.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09880) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_011](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09882.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09882) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_012](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09496.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09496) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_013](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09952.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09952) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_014](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_00112.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_00112) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_015](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_00130.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_00130) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_016](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09591.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09591) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_017](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09593.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09593) 
[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_018](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/th_A4_09628.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/th_a4_09628) 

  

文／[川野優希](https://news.denfaminicogamer.jp/tag/川野優希)

[ギャラリーゼノン公式サイト](https://gallery-zenon.jp/)
Gallery Zenon官方网站  

- - -

**「北条司展 The road to 『CITY HUNTER』40th anniversary 2025〜Limited Special Exhibition〜」**

[![北条司展レポート。連載作品から読切までを網羅した大満足の原画展。「鈴木亮平さん演じる冴羽獠」を北条司先生が描き下ろした作品も_019](https://img-denfaminicogamer.com/wp-content/uploads/2024/04/hojo-ten_2024_jp-300x425.jpg)](https://news.denfaminicogamer.jp/kikakuthetower/240418b/attachment/hojo-ten_2024_jp)

会期  
前期 2024年4月17日〜5月19日  
後期 2024年5月22日〜6月23日  
※火曜定休

2025年2月に控える『シティーハンター』連載40周年に向け、漫画家北条司デビュー作から連載作品や読切等、ほぼ全ての作品を網羅したこれまでの北条ワールドと歴史を体感していただける原画展を開催します。
在2025年2月即将迎来《城市猎人》连载40周年之际，将举办从漫画家北条司的出道作品到连载作品、读切等，网罗几乎所有作品的北条世界和历史的原画展。  

【前期 4/17-5/19】展示予定作品  
シティーハンター  
キャッツ♥アイ  
こもれ陽の下で…  
RASH!!  
SPRASH！（译注：应为"SPLASH！"）   
おれは男だ！  
ネコまんまおかわり♡  
TAXI DRIVER  
桜の花 咲くころ

【後期 5/22-6/23】展示予定作品  
シティーハンター  
キャッツ♥アイ  
エンジェル・ハート  
F.COMPO  
天使の贈り物  
ファミリー・プロット  
少女の季節




### この記事に関するタグ

*   [イベントレポート](https://news.denfaminicogamer.jp/tag/%e3%82%a4%e3%83%99%e3%83%b3%e3%83%88%e3%83%ac%e3%83%9d%e3%83%bc%e3%83%88)
*   [ギャラリーゼノン](https://news.denfaminicogamer.jp/tag/%e3%82%ae%e3%83%a3%e3%83%a9%e3%83%aa%e3%83%bc%e3%82%bc%e3%83%8e%e3%83%b3)
*   [北条司](https://news.denfaminicogamer.jp/tag/%e5%8c%97%e6%9d%a1%e5%8f%b8)
*   [北条司展](https://news.denfaminicogamer.jp/tag/%e5%8c%97%e6%9d%a1%e5%8f%b8%e5%b1%95)
*   [北条司展 The road to 『CITY HUNTER』40th anniversary 2025〜Limited Special Exhibition〜](https://news.denfaminicogamer.jp/tag/%e5%8c%97%e6%9d%a1%e5%8f%b8%e5%b1%95-the-road-to-%e3%80%8ecity-hunter%e3%80%8f40th-anniversary-2025%e3%80%9climited-special-exhibition%e3%80%9c)
*   [川野優希](https://news.denfaminicogamer.jp/tag/%e5%b7%9d%e9%87%8e%e5%84%aa%e5%b8%8c)