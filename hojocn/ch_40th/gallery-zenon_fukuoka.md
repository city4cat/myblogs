source:  
https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/  

# EXHIBITIONS

![北条司展 IN 福岡](https://gallery-zenon.jp/wp-content/uploads/2024/09/mv-hojo-fukuoka.jpg.webp)

### 北条司展 IN 福岡

The road to 『CITY HUNTER』 40th anniversary 2025 〜Limited Special Exhibition〜  

**会期**  
**2024年10月16日(水)〜10月27日(日)**  
営業時間 10:00～21:00 ※最終日のみ19:00 CLOSE  

開催場所:六本松 蔦屋書店 ギャラリースペース（福岡県福岡市中央区六本松4丁目2-1 六本松421 2F）  

2025年2月に控える『シティーハンター』連載40周年に向け、漫画家北条司デビュー作から連載作品や読切等、ほぼ全ての作品を網羅したこれまでの北条ワールドと歴史を体感していただける原画展を開催します。

©北条司／コアミックス

[チケット販売サイトへ](./ticketme_2024-10-16.md)  

|        | 入場料    | 大人<br>中学生以上 |  
|:------:|:--------:|:----------------:|  
| 平日    | 前売券    | 1,500円（税込）   |  
|        | 当日券    | 1,800円（税込）   |  
| 土日    | 前売券    | 1,800円（税込）   |  
|        | 当日券    | 2,100円（税込）   |  


*   小学生まで保護者同伴で入場無料。中学生からは上記料金のチケットが必要です。
*   中学生からはチケットが必要です。
*   小学生まではチケットを持った入場者がいる場合は無料で入場できます。
*   入場にはチケットの購入が必須となります。チケットをお持ちでない方は展示コーナー・グッズコーナーのご利用はできません。
*   入場には発行されたQRコードが必要になります。
*   チケットの購入は全てのグッズのご購入をお約束するものではございません。
*   障害者手帳、療育手帳又は精神障害者保健福祉手帳を所有している方もチケットの購入が必要となります。
*   再入場はできません。
*   写真撮影時、フラッシュ使用、自撮り棒・三脚等の撮影補助機材の使用を禁止いたします。
*   ギャラリースペースではベビーカーでの入場ができません。入口にてベビーカーをお預けいただく必要があります。
*   ペットを連れてのご入場はできません。（盲導犬・聴導犬・介助犬等の補助犬含む）
*   会場内に濡れた傘を持ち込む場合には、展示物保護のため、会場に設置された傘立てを必ずご使用いただきます。
*   会場内での盗難、置き引き、紛失等に関して、主催者は一切責任を負いません。荷物・貴重品の管理はご自身で行ってください。
*   天災・疫病・不慮の事故などにより、本展覧会の開催が急遽中止となる場合があります。また、都合により展示内容の一部を予告なく変更する場合があります。
*   会場内の額、展示品、装飾物（壁面を含む）、展示ケース等には触れないでください。
*   展示物保護の為大きな荷物の持ち込みはご遠慮ください。手荷物、帽子などが額や展示物に接触しないようご注意ください。リュックサックは前に抱えてのご鑑賞をお願いいたします。
*   ギャラリースペースでは飲食、喫煙は禁止いたします。
*   携帯電話はマナーモードに設定の上、会場内での通話はご遠慮ください。
*   ご来場の際は、注意事項をご一読の上、スタッフの指示・誘導に従ってください。他の来場者の迷惑となる行為は固くお断りします。お守りいただけない場合は、ご入場をお断りする場合や退場していただく場合があります。尚、スタッフの指示に従わずに生じた事故に関して、主催者は一切責任を負いません。
*   最終入場時間は閉場30分前までとなります。
*   混雑時に写真撮影をされる方は、鑑賞されている方を優先とし、周囲のお客様にご配慮いただくとともに、スタッフの指示に従ってください。
*   状況によっては、撮影可能エリアであっても、撮影を制限・ご遠慮いただく場合がございます。
*   動画の撮影はご遠慮ください。
*   撮影した写真の用途は私的利用に限ります。商用利用はご遠慮ください。
*   近隣の皆様へのご迷惑及び事故の防止の為、徹夜を含む前日からのご来場は絶対にご遠慮ください。徹夜でのご来場による事故・トラブルなどについて、主催者は一切責任を負いません。

#### 展示予定作品

*   シティーハンター  
*   キャッツ♥アイ  
*   こもれ陽の下で…  
*   RASH!!  
*   SPLASH！  
*   おれは男だ！  
*   ネコまんまおかわり♡  
*   TAXI DRIVER  
*   桜の花 咲くころ  
*   エンジェル・ハート  
*   F.COMPO  
*   天使の贈り物  
*   ファミリー・プロット  
*   少女の季節  

#### 多数の生原画が登場！

*   ![キャッツ♥アイ原画](https://gallery-zenon.jp/wp-content/uploads/2024/06/ce-1.jpg.webp)    
*   ![キャッツ♥アイ原画](https://gallery-zenon.jp/wp-content/uploads/2024/06/ce-2.jpg.webp)    
*   ![シティーハンター原画](https://gallery-zenon.jp/wp-content/uploads/2024/06/ch-1.jpg.webp)    
*   ![シティーハンター原画](https://gallery-zenon.jp/wp-content/uploads/2024/06/ch-2.jpg.webp)    
*   ![シティーハンター原画](https://gallery-zenon.jp/wp-content/uploads/2024/06/ch-3.jpg.webp)    

#### 入場者特典

特製カード全6種からランダムに1枚プレゼントします。

![入場者特典](https://gallery-zenon.jp/wp-content/uploads/2024/09/hukuoka-tokuten-re.jpg.webp)

*   デザインはイメージです。変更になる場合がございます。（予定サイズ 91mm x 55mm）

[トップページに戻る](https://gallery-zenon.jp/)  

[ギャラリーゼノンの公式facebook](https://www.facebook.com/people/Gallery-Zenon/100063738872905/) 
[ギャラリーゼノンの公式instagram](https://www.instagram.com/galleryzenon/) 
[ギャラリーゼノンの公式x](https://twitter.com/GalleryZenon)  
[GALLERY ZENON｜ギャラリーゼノン](https://gallery-zenon.jp/)  

Copyright ©GALLERY ZENON All rights reserved. 