source:  
https://edition-88.com/collections/hojotsukasa?page=1  
https://edition88.com/collections/tsukasa-hojo  

## Goods

## [複製原稿 / おれは男だ！（北条司）](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript1)
[![](./img/thumb/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_4800x.jpg) 
[![](./img/thumb/fukusei-otoko-02.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-otoko-02_4800x.jpg) 

¥1,100

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

北条司先生のデビュー作『おれは男だ！』の複製原稿です。  
高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、ホワイトを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4サイズ。  
作品の世界観が楽しめるコレクションアイテムです。  

【仕様】  
●サイズ：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

🄫北条司／コアミックス  
発送時期：7月上旬～下旬頃にお届け予定  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [複製原稿 / TAXI DRIVER（北条司）](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript2)
[![](./img/thumb/fukusei-taxi-01_b457e2fe-8448-4329-8f3d-81ecbeee34dd.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-taxi-01_b457e2fe-8448-4329-8f3d-81ecbeee34dd_4800x.jpg) 
[![](./img/thumb/fukusei-taxi-02.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-taxi-02_4800x.jpg) 

¥1,100  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

北条司先生の短編集『TAXI DRIVER」の複製原稿です。  
高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、ホワイトを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4サイズ。  
作品の世界観が楽しめるコレクションアイテムです。  

【仕様】  
●サイズ：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

🄫北条司／コアミックス  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [複製原稿 / RASH!!（北条司）](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript4)
[![](./img/thumb/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x.jpg) 
[![](./img/thumb/fukusei-rash-02_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-rash-02_4800x.jpg) 

¥1,100  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

北条司先生の短編集『RASH!!』の複製原稿です。  
高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、ホワイトを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4サイズ。  
作品の世界観が楽しめるコレクションアイテムです。  

【仕様】  
●サイズ：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

🄫北条司／コアミックス  
発送時期：7月上旬～下旬頃にお届け予定  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [複製原稿 / SPLASH 3（北条司）](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript5)
[![](./img/thumb/fukusei-splash-01_4c92c9f1-1b5c-4161-9a79-37a4a77ac727_4800x.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-splash-01_4c92c9f1-1b5c-4161-9a79-37a4a77ac727_4800x.jpg) 
[![](./img/thumb/fukusei-splash-02_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-splash-02_4800x.jpg) 

¥1,100  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

北条司先生の短編集『SPLASH 3』の複製原稿です。  
高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、ホワイトを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4サイズ。  
作品の世界観が楽しめるコレクションアイテムです。  

【仕様】  
●サイズ：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

🄫北条司／コアミックス  
発送時期：7月上旬～下旬頃にお届け予定  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [複製原稿 / 桜の花咲くころ（北条司）](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript3)
[![](./img/thumb/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x.jpg) 
[![](./img/thumb/fukusei-sakura-02_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-sakura-02_4800x.jpg) 

¥1,100  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

北条司先生の短編集『桜の花 咲くころ」の複製原稿です。  
高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、ホワイトを使用した部分、青色で書かれた下書きの線等も見ることができます！  
複製原稿は原寸に近いB4サイズ。  
作品の世界観が楽しめるコレクションアイテムです。  

【仕様】  
●サイズ：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

🄫北条司／コアミックス  
発送時期：7月上旬～下旬頃にお届け予定  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [複製原稿 / ネコまんまおかわり♡（北条司）](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript6)
[![](./img/thumb/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x.jpg) 
[![](./img/thumb/fukusei-neko-02_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-neko-02_4800x.jpg) 

¥1,100  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

北条司先生の短編集『ネコまんまおかわり♡』の複製原稿です。  
高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、ホワイトを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4サイズ。  
作品の世界観が楽しめるコレクションアイテムです。  

【仕様】  
●サイズ：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

🄫北条司／コアミックス  
発送時期：7月上旬～下旬頃にお届け予定  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


---  



## [「シティーハンター」88グラフ 7 / 北条司（直筆サイン入り/国内版限定200枚）](https://edition-88.com/products/cityhunter-88graph7)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-07_d44d5eb9-89ee-4297-a1a4-0592caebcb33_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-07_d44d5eb9-89ee-4297-a1a4-0592caebcb33_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-01_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-01_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-08_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-08_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-02_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-02_4800x.jpg)
[![](./img/thumb/88graph1-04_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-04_4800x.jpg)
[![](./img/thumb/88graph1-05_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-05_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-06_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-06_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-03_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-03_4800x.jpg)

¥50,600  

税込み 送料計算済み チェックアウト時  

ジャンプ・コミックス20巻カバーイラストの88グラフです。  

シティーハンターの88グラフでは1枚毎、パール絵の具の粒子を吹き付けています。  
見る角度によって光沢が強調され、作品の奥行きが表現できました。  
シートサイズは原画の原寸サイズとなっており、より原画に近い状態でお楽しみいただけます。  

写真で切り取っているカラー部分とセピア色部分と分けて色調整をしました。  
セピア色の部分が赤くなりすぎないよう、黄色くなりすぎないよう注意しました。  

●仕様：北条司直筆サイン・エディション入り、ダブルマット仕様、木製額入り  
●技法：88グラフ  
●素材  
　紙：版画用中性紙  
　額：木製、UVカットアクリル（表面カバー）  
　額カラー：ブラック  
　マットカラー：上段ブラック/下段ブルー  
●総エディション数：380（国内版200、インターナショナル版180）  
（エディションナンバーはお選びいただけません。）  
●サイズ  
　イラストサイズ：362×257mm  
　額サイズ：525×410×厚さ20mm  

●作品証明書付  

Ⓒ北条司／コアミックス1985  
発送時期：ご注文から約2ヶ月以内に発送致します。  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。  
個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  

### [City Hunter, 88Graph #7 / Tsukasa Hojo](https://edition88.com/products/cityhunter-88graph7)
Sale price¥45,100 JPY Unframed, ¥50,600 JPY Black Frame  

Tax included. Shipping calculated at checkout  

This 88Graph is from the illustration for the cover page of Jump Comics Vol.27.  

In the production process, color adjustments were made separately for the color portion in the center of the illustration and the sepia color portion.  
We took great care to replicate the color tones of the original illustration without making the sepia portion too assertive, so that it would not fall into the red or yellow tones.  

Each City Hunter print is hand sprayed with pearlescent paint, having a luminosity that shifts with the viewer’s perspective, displaying depth.  

The 88Graph is the same image size as the original piece.  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380 (International edition 180, Regular edition 200 )  
●Medium: 88 Graph (Giclee and UV on fine art paper for international edition)  
●Black Frame (Wood, UV Resistant Acrylic)/ Unframed  
●Size  
　Image size 362 x 257mm / 14.25 x 10.12inch  
　Sheet size 412 x 297mm / 16.22 x 11.69inch  
　Frame size 525 x 410 x 20mm / 20.67 x 16.14 x 0.79inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  
●Certificate of Authenticity  

ⒸTsukasa Hojo/Coamix 1985  

Please choose 'Frame' or 'Unframed'.  

SHIPPING DATE: Within 2 months after receiving your order  

Important Notice  
●Since this product is made to order, please allow a certain amount of time for shipping.　
●Our products are shipped from Japan and may be subject to customs duties, import taxes, customs fees, etc. in the destination country.?EDITION88 DOES NOT collect those fees directly and therefore those are not included in your total amount due. Please read our policies before placing your order.  

## [「シティーハンター」88グラフ 8 / 北条司（直筆サイン入り/国内版限定200枚）](https://edition-88.com/products/cityhunter-88graph8)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-02_b92ebab8-0cde-4020-a3e8-b4b59d5eb0d0_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-02_b92ebab8-0cde-4020-a3e8-b4b59d5eb0d0_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-01_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-01_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-07_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-07_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-03_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-03_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-04_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-04_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-05_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-05_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-06_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-06_4800x.jpg) 

¥50,600  

税込み 送料計算済み チェックアウト時  

ジャンプ・コミックス31巻表紙イラストの88グラフです。  

シティーハンターの版画では1枚毎、パール絵の具の粒子を吹き付けています。  
見る角度によって光沢が強調され、作品の奥行きが表現できました。  
シートサイズは原画の原寸サイズとなっており、より原画に近い状態でお楽しみいただけます。  

通常出力では再現の難しい、鮮やかなピンク色を原画そっくりに再現しました。他部分の色は鮮やかになりすぎないように調整しました。  

●仕様：北条司直筆サイン・エディション入り、ダブルマット仕様、木製額入り  
●技法：88グラフ  
●素材  
　紙：版画用中性紙  
　額：木製、UVカットアクリル（表面カバー）  
　額カラー：ブラック  
　マットカラー：上段ブラック/下段イエロー  
●総エディション数：380（国内版200、インターナショナル版180）  
（エディションナンバーはお選びいただけません。）  
●サイズ  
　イラストサイズ：379×267m  
　額サイズ：525×410×厚さ20mm  
  
●作品証明書付  

🄫北条司／コアミックス1985  
発送時期：ご注文から約2ヶ月以内に発送致します。  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。  
個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  
  
### [City Hunter, 88Graph #8 / Tsukasa Hojo](https://edition88.com/products/cityhunter-88graph8?variant=45582736163040)
Sale price ¥45,100 JPY Unframed, ¥50,600 JPYBlack Frame  

Tax included. Shipping calculated at checkout  

This 88Graph is from the illustration for the front cover of Jump Comics Vol.31.  

In the production process, we were able to replicate the bright pink color exactly as in the original, which is usually difficult to do. We carefully balanced the overall color scheme, taking care not to make other parts of the image too bright.  

Each 'City Hunter' 88Graph is hand sprayed with pearlescent paint, having a luminosity that shifts with the viewer’s perspective, displaying depth.  

The 88Graph is the same image size as the original piece.  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Medium: 88 Graph (Giclee and UV on fine art paper for international edition)  
●Black Frame (Wood, UV Resistant Acrylic)/ Unframed  
●Size  
　Image size 379 x 267mm / 14.92 x 10.51inch  
　Sheet size 425 x 304mm / 16.73 x 11.97inch  
　Frame size 525 x 410 x 20mm / 20.67 x 16.14 x 0.79inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  
●Certificate of Authenticity  

ⒸTsukasa Hojo/Coamix 1985  

Please choose 'Frame' or 'Unframed'.  

SHIPPING DATE: Within 2 months after receiving your order  

Important Notice  
●Since this product is made to order, please allow a certain amount of time for shipping.　
●Our products are shipped from Japan and may be subject to customs duties, import taxes, customs fees, etc. in the destination country.?EDITION88 DOES NOT collect those fees directly and therefore those are not included in your total amount due. Please read our policies before placing your order.  


## [「シティーハンター」88グラフ シルバー仕様１ / 北条司（直筆サイン入り / 国内版限定200枚）](https://edition-88.com/products/cityhunter-88graph-silver1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-01_e174bafd-fbcc-42ae-a772-7296b10c541b_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-01_e174bafd-fbcc-42ae-a772-7296b10c541b_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-02_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-02_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-03_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-03_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-04_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-04_4800x.jpg) 

¥48,400  

税込み 送料計算済み チェックアウト時  

ジャンプ・コミックス20巻表紙の88グラフ シルバー仕様です。  

厚みのある額を使用しているため、壁掛けはもちろん、デスク周りにも置いて飾ることができます。  

下地にテキスチャーのあるアルミニウムの特別紙を採用することで、  
表面のテキスチャーとシルバーの光沢が混ざり合い、夜景に立体感のある輝きが生まれました。見る角度や光源の位置によって作品の輝きの変化を楽しむことができます。  
建物の光が描かれている部分以外は下地のシルバーが完全に見えないよう処理しました。そうすることでシルバー部分の仕上がりがより際立ち、作品全体に奥行きが生まれました。  

額装はボードの厚みを生かした浮かしを採用することで、作品に豪華さを与えています。  

●仕様：北条司直筆サイン・エディション入り、木製額入り  
●技法：88グラフ  
●素材  
　紙：アルミニウム  
　額：木製、UVカットアクリル（表面カバー）  
　額カラー：ブラック  
　マットカラー：ブラック  
●エディション：380（国内版200、インターナショナル版180）  
（エディションナンバーはお選びいただけません。）  
●サイズ  
　イラストサイズ：約333×242mm  
　額サイズ：370×446×厚さ30mm  

●作品証明書付  

©北条司／コアミックス1985  
発送時期：ご注文から2ヶ月以内に発送致します。  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  

### [City Hunter, 88Graph silver ver. #1 / Tsukasa Hojo](https://edition88.com/products/cityhunter-88graph-silver1)
Sale price¥48,400 JPY  

Tax included. Shipping calculated at checkout  

This 88Graph is from the illustration for the front cover of Jump Comics Vol.20.  

We used special aluminum paper with a textured base for this piece. The mix of the surface texture and the silver sheen creates a three-dimensional sparkle in the night scene. You can enjoy the changing glow of the artwork depending on the viewing angle and the position of the light source.  

Areas other than where the lights of the buildings are depicted were treated to completely hide the silver base. This treatment makes the silver sections stand out more, adding depth to the entire piece.  

The framing utilizes the thickness of the board to create a floating effect, which adds a touch of luxury to the artwork.  

●Hand-signed by Tsukasa Hojo
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Medium: 88 Graph (UV print on fine art paper）  
●Black Frame (Wood, UV Resistant Acrylic)  
●Size  
　Image size 333 x 242 mm / 13.11 x 19.53 inch  
　 Frame size 446 x 370 x 35mm / 17.56 x 14.57 x 1.38 inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  
●Certificate of Authenticity  

ⒸTsukasa Hojo/Coamix 1985  

SHIPPING DATE: Within 2 months after receiving your order  

Important Notice  
●Since this product is made to order, please allow a certain amount of time for shipping.　
●Our products are shipped from Japan and may be subject to customs duties, import taxes, customs fees, etc. in the destination country.?EDITION88 DOES NOT collect those fees directly and therefore those are not included in your total amount due. Please read our policies before placing your order.  

## [「シティーハンター」88グラフ シルバー仕様２ / 北条司（直筆サイン入り /国内版限定200枚）](https://edition-88.com/products/cityhunter-88graph-silver2)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-01_e0ca25b7-c700-4b0b-b168-62570bf02114_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-01_e0ca25b7-c700-4b0b-b168-62570bf02114_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-02_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-02_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-04_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-04_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-03_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-03_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-05_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-05_4800x.jpg) 

¥48,400  

税込み 送料計算済み チェックアウト時  

ジャンプ・コミックス22巻表紙の88グラフ シルバー仕様です。  

厚みのある額を使用しているため、壁掛けはもちろん、デスク周りにも置いて飾ることができます。  

下地にテキスチャーのあるアルミニウムの特別紙を採用しています。  
背景に描かれた星のみ、下地のシルバーが見えるよう処理することで、イラストの華やかさを強調しています。  

額装はボードの厚みを生かした浮かしを採用することで、作品に豪華さを与えています。  

●仕様：北条司直筆サイン・エディション入り、木製額入り  
●技法：88グラフ  
●素材  
　紙：アルミニウム  
　額：木製、UVカットアクリル（表面カバー）  
　額カラー：ブラック  
　マットカラー：ブラック  
●エディション：380（国内版200、インターナショナル版180）  
（エディションナンバーはお選びいただけません。）  
●サイズ  
　イラストサイズ：約333×242mm  
　額サイズ：370×446×厚さ30mm  

●作品証明書付  

©北条司／コアミックス1985  
発送時期：ご注文から2ヶ月以内に発送致します。  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  

### 海外版 [City Hunter, 88Graph silver ver. #2 / Tsukasa Hojo](https://edition88.com/products/cityhunter-88graph-silver2)  
¥48,400 JPY  

Tax included. Shipping calculated at checkout  

This 88Graph is from the illustration for the front cover of Jump Comics Vol.22.  

We are using a special textured aluminum paper for the substrate. By treating only the stars depicted in the background to allow the silver base to show through, we emphasize their brilliance. Additionally, the beautiful shades of blue and purple in the background blend with the silver base, creating an elegant shimmer.  

The framing utilizes the thickness of the board to create a floating effect, which adds a touch of luxury to the artwork.  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380 (International edition 180, Regular edition 200 )  
●Medium: 88 Graph (UV print on fine art paper）  
●Black Frame (Wood, UV Resistant Acrylic)  
●Size  
　Image size 333 x 242 mm / 13.11 x 19.53 inch  
　 Frame size 446 x 370 x 35mm / 17.56 x 14.57 x 1.38 inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  
●Certificate of Authenticity  

ⒸTsukasa Hojo/Coamix 1985  

SHIPPING DATE: Within 2 months after receiving your order  

*Those who select framed version will receive the illustrated book of 'Cat's Eye 40th anniversary Original Art Exhibit - And to City Hunter'. Please note that the unframed version does not come with the book.  

Important Notice  
●Since this product is made to order, please allow a certain amount of time for shipping.　
●Our products are shipped from Japan and may be subject to customs duties, import taxes, customs fees, etc. in the destination country.?EDITION88 DOES NOT collect those fees directly and therefore those are not included in your total amount due. Please read our policies before placing your order.  

----  

## [「シティーハンター」88グラフ9 / 北条司（直筆サイン入り/国内版限定200枚）](https://edition-88.com/products/cityhunter-88graph9)  

[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-02_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-02_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city-01_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city-01_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-03_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-03_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-04_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-04_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-05_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-05_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-06_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-06_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-07_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-07_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-08_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-08_4800x.jpg) 

¥50,600

税込み 送料計算済み チェックアウト時

週刊少年ジャンプ 1988年　第47号表紙の88グラフです。

シティーハンターの88グラフでは1枚毎、パール絵の具の粒子を吹き付けています。
見る角度によって光沢が強調され、作品の奥行きが表現できました。
シートサイズは原画の原寸サイズとなっており、より原画に近い状態でお楽しみいただけます。

グレーや茶色などの色が黄色や赤色に転びやすかったため、遼のコートや茶色い机は細心の注意をはらって調整しました。

●仕様：北条司直筆サイン・エディション・手彩色入り、木製額入り
●技法：88グラフ
●素材
　紙：版画用中性紙
　額：木製、UVカットアクリル（表面カバー）
　額カラー：ブラック
　マットカラー：上:ブラック/下:ブルー
●総エディション数：380　（国内版200、インターナショナル版180）
（エディションナンバーはお選びいただけません。）
●サイズ
　イラストサイズ：272×235mm
　額サイズ：525×410×厚さ20mm

●作品証明書付

©北条司／コアミックス1985
発送時期：ご注文から約2ヶ月以内に発送致します。

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。
個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。

### [海外版](https://edition88.com/products/cityhunter-88graph9)  
Unframed ¥45,100 JPY / Black Frame ¥50,600 JPY

Tax included. Shipping calculated at checkout

This 88Graph is from the illustration for the front cover of Weekly Shonen Jump No.47 published in 1988.

The expression and pose of Kaori, holding a gun on the table, captivate the viewer. Ryo, depicted behind her as if to protect her, provides a sense of security.

In the production of 88Graph, we had to be particularly careful with color adjustments because colors like gray and brown tended to shift towards yellow and red. Thus, special attention was paid to the hues of Ryo's coat and the brown table.

Each 'City Hunter' print is hand sprayed with pearlescent paint, having a luminosity that shifts with the viewer’s perspective, displaying depth.
The 88Graph is the same image size as the original piece.

●Hand-signed by Tsukasa Hojo
●Limited edition of 380(International edition 180 , Regular edition 200 )
●Medium: 88 Graph (Giclee and UV on fine art paper for international edition)
●Black Frame (Wood, UV Resistant Acrylic)/ Unframed
●Size
　Image size 272 x 235mm / 10.71 x 9.25inch
　Sheet size 312 x 265mm / 12.28 x 10.43inch
　Frame size 525 x 410 x 20mm / 20.67 x 16.14 x 0.79inch
●Officially licensed by Coamix Inc.
●Manufactured by EDITION88
●Certificate of Authenticity

ⒸTsukasa Hojo/Coamix 1985

Please choose 'Frame' or 'Unframed'.

SHIPPING DATE: Within 2 months after receiving your order

Important Notice
●Since this product is made to order, please allow a certain amount of time for shipping.　
●Our products are shipped from Japan and may be subject to customs duties, import taxes, customs fees, etc. in the destination country.?EDITION88 DOES NOT collect those fees directly and therefore those are not included in your total amount due. Please read our policies before placing your order.

---- 

## [「エンジェル・ハート」88グラフ1 / 北条司（直筆サイン入り/国内版限定200枚）](https://edition-88.com/products/angelheart-88graph1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-02_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-01_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-01_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-03_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-03_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-04_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-04_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-05_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-05_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-06_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-06_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-07_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-angelheart-07_4800x.jpg) 

¥59,400

税込み 送料計算済み チェックアウト時

週刊少年ジャンプ 1933年 第30号表紙の88グラフです。

エンジェル・ハートの88グラフでは1枚毎、パール絵の具の粒子を吹き付けています。
見る角度によって光沢が強調され、作品の奥行きが表現できました。
シートサイズは原画の原寸サイズとなっており、より原画に近い状態でお楽しみいただけます。

シャツやパンツの紫色は鮮やかに出力することができ、原画そっくりになりました。
背景のビル街の色の調整が難しく、時間をかけて丁寧に再現しました。

●仕様：北条司直筆サイン・エディション・手彩色入り、木製額入り
●技法：88グラフ
●素材
　紙：版画用中性紙
　額：木製、UVカットアクリル（表面カバー）
　額カラー：ブラック
　マットカラー：ブラック
●総エディション数：380　（国内版200、インターナショナル版180）
（エディションナンバーはお選びいただけません。）
●サイズ
　イラストサイズ：399×274mm
　額サイズ：622×471×厚さ20mm

●作品証明書付

©北条司／コアミックス
発送時期：ご注文から約2ヶ月以内に発送致します。

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。
個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。

### [海外版](https://edition88.com/products/angelheart-88graph1)
Unframed ¥53,900 JPY / Black Frame ¥59,400 JPY

Tax included. Shipping calculated at checkout

This 88Graph is from the illustration for the front cover of Weekly Shonen Jump No.30 published in 1993.

The composition of the two people placed diagonally with the cityscape in the background is beautiful.

In the production process, we were able to replicate the purple of the shirt and pants as vividly as in the original.
It was difficult to adjust the colors of the buildings in the background, and we spent a lot of time carefully replicating them.

 'Angel Heart' 88Graph is hand sprayed with pearlescent paint, having a luminosity that shifts with the viewer’s perspective, displaying depth.
The 88Graph is the same image size as the original piece.

●Hand-signed by Tsukasa Hojo
●Limited edition of 380(International edition 180 , Regular edition 200 )
●Medium: 88 Graph (Giclee and UV on fine art paper for international edition)
●Black Frame (Wood, UV Resistant Acrylic)/ Unframed
●Size
　Image size 399 x 274mm / 15.70 x 10.79inch
　Sheet size 439 x 304mm / 17.28 x 11.97inch
　Frame size 622 x 471 x 20mm / 24.49 x 18.54 x 0.79inch
●Officially licensed by Coamix Inc.
●Manufactured by EDITION88
●Certificate of Authenticity

ⒸTsukasa Hojo/Coamix

Please choose 'Frame' or 'Unframed'.

SHIPPING DATE: Within 2 months after receiving your order

Important Notice
●Since this product is made to order, please allow a certain amount of time for shipping.　
●Our products are shipped from Japan and may be subject to customs duties, import taxes, customs fees, etc. in the destination country.?EDITION88 DOES NOT collect those fees directly and therefore those are not included in your total amount due. Please read our policies before placing your order.


## [複製原稿 / 天使の贈りもの](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript7)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-tenshi_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-tenshi_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-tenshi-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-tenshi-02_4800x.jpg)

¥1,100

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

北条司先生の短編集『天使の贈りもの」の複製原稿です。
高精細なプリントで再現しています。
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。
黒ベタや、ホワイトを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！
複製原稿は原寸に近いB4サイズ。
北条司作品の世界観が楽しめるコレクションアイテムです。

【仕様】
●サイズ：B4(H364×W257mm)
●技法：オフセットプリント
●素材　紙：上質紙

©北条司／コアミックス
発送時期：7月下旬～8月中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


## [複製原稿 / 少女の季節](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript8)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-syojo_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-syojo_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-syojo-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-syojo-02_4800x.jpg)

¥1,100

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

北条司先生の短編集『少女の季節」の複製原稿です。
高精細なプリントで再現しています。
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。
黒ベタや、ホワイトを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！
複製原稿は原寸に近いB4サイズ。
北条司作品の世界観が楽しめるコレクションアイテムです。

【仕様】
●サイズ：B4(H364×W257mm)
●技法：オフセットプリント
●素材　紙：上質紙

©北条司／コアミックス
発送時期：7月下旬～8月中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


----  

## [「北条司展 The road to 『CITY HUNTER』40th anniversary 2025」図録](https://edition-88.com/products/hojoten-zuroku1)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-01_37d0494d-06d3-4269-aff2-f9e6ff85dcfd_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-01_37d0494d-06d3-4269-aff2-f9e6ff85dcfd_9600x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-02_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-02_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-03_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-03_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-05_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-05_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-04_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-04_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-07_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-07_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-06_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hojo-zuroku-06_4800x.jpg) 

¥2,750  

税込み 送料計算済み チェックアウト時  

「北条司展 The road to 『CITY HUNTER』40th anniversary 2025 〜Limited Special exhibition〜」に展示された作品をまとめた図録です。  

『キャッツ♥アイ』『シティーハンター』、デビュー作から連載作品や読切を収録。  
アナログの原稿から1点1点スキャン。  
北条司先生の作品を色鮮やかに再現しています。  
展示会の開催を記念して、北条司先生のスペシャルインタビューを掲載。  

※本書の内容は「北条司展 The road to 『CITY HUNTER』40th anniversary 2025」にて展示された作品の一部となります。全ての展示品を掲載してはおりません。  

【仕様】  
●著者：北条司  
●発行：株式会社VISION8  
●サイズ：A4(約H297×W210×D5mm)  
●ページ数：フルカラー76ページ(表紙含む）  

発送時期：ご注文日より、営業日2週間以内に発送致します。  

※ご注文状況により、発送にお時間を頂く場合がございます。  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [クリアファイルセット / シティーハンター](https://edition-88.com/products/cityhunter-clearfile-set1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/clear_file-01_e851d08d-72d7-4d68-9a8a-a0832180f28f_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/clear_file-01_e851d08d-72d7-4d68-9a8a-a0832180f28f_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/clearfile-02_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/clearfile-02_4800x.jpg) 

¥1,000  

税込み 送料計算済み チェックアウト時  

シティーハンターのカラーイラストを使用した、クリアファイル2枚セットです。  

手描きで描かれた原画を、高精細のスキャナーでデジタル化。  
普段使いにも、コレクターズアイテムとしてもおすすめの一品です。  
保管に便利なA4サイズで、書類やプリントが楽しく整理できます。  

【仕様】  
●サイズ：H310×W220mm  
●オフセット印刷  
●素材：PP(ポリプロピレン)  

©北条司／コアミックス 1985  

発送時期：ご注文日より、営業日2週間以内に発送致します。  

※ご注文状況により、発送にお時間を頂く場合がございます。  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [アートタイル（car） / シティーハンター](https://edition-88.com/products/cityhunter-arttile9)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile1_165279c3-2b07-43a5-a719-59c6ab5baa4b_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile1_165279c3-2b07-43a5-a719-59c6ab5baa4b_4800x.jpg) 

¥990  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

シティーハンターのイラストを使用したアートタイル。  

直径約90mm、厚さ5mmと片手に乗るサイズです。  
国産の粘土を使用して焼成した、美濃の素焼きの陶器です。  
表面に肉眼では見えない大変細かい穴が空いているため吸水性が高く、耐熱性もあるため、コースターとしても機能性抜群です！  
来客時のおもてなしにもオススメ♪  
アートとしてお部屋に飾るのにも場所を選びません。  

【仕様】  
●サイズ：直径約90mm 厚さ5mm  
●重さ：約80g  
●素材：陶器  
●生産国　日本  
※珪藻土(アスベスト)は使用しておりません  
※水滴以外は吸水面にシミが生じる場合があります。  
※食器洗い機は非対応です。  
※素材の性質上、細かい傷がある、吸水の穴が目立つことがございます。  

🄫北条司／コアミックス 1985  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [マグカップ（集合）/ 北条司](https://edition-88.com/products/hojoten-mug1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/mag-03_65f6e9d9-57be-4b5c-b6ad-ba3f74198a56_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/mag-03_65f6e9d9-57be-4b5c-b6ad-ba3f74198a56_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/mag-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/mag-02_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/mag-01_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/mag-01_4800x.jpg) 

¥1,815  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

「北条司展」のメインビジュアルのイラストを使用したマグカップです。  

プリントの工程は1つ1つ、手作業で行っています。  
昇華転写プリントのため、発色がとても綺麗です。  
飲み口が広く、容量約310mlとたっぷり入るサイズ感で、コーヒーや紅茶、スープといろいろな飲み物を楽しめます。  
92mmとしっかり高さがあるので、ペン立てとしても使用できます。  

アートタイルとセット使いもオススメ！  

【仕様】  
●サイズ：約H92mm×W80(取手部分を除く) / 口径φ80mm  
●素材：陶器  
●容量：約310ml  

©北条司／コアミックス  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [アクリルブロック（Climbing） / キャッツ・アイ](https://edition-88.com/products/catseye-acrylicblock1)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acryl_block_cats-01_eb80365f-e82e-4eb1-bb3b-0382f6cf5933_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acryl_block_cats-01_eb80365f-e82e-4eb1-bb3b-0382f6cf5933_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylblock_cats-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylblock_cats-02_4800x.jpg) 

¥6,600  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

キャッツ♥アイのイラストをコンパクトなサイズでそのまま飾れるアクリルブロックに。  

透明感があるアクリルに光を当てると、より作品を美しく引き立たせ、厚みがあることで、立体的に見せてくれます。  
デスクや棚等お気に入りの場所に飾れば、好きな作品をより身近に感じていただけます。  
  
【仕様】  
●素材：アクリル  
●サイズ：高さ148×幅100×厚さ20mm  

🄫北条司／コアミックス 1981  
発送時期：7月上旬～下旬頃にお届け予定  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  

## [アクリルブロック（Jump） / キャッツ・アイ](https://edition-88.com/products/catseye-acrylicblock2)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acryl_block_jump-01_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acryl_block_jump-01_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylblock_jump-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylblock_jump-02_4800x.jpg)

¥6,600

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

キャッツ・アイのイラストをコンパクトなサイズでそのまま飾れるアクリルブロックに。

透明感があるアクリルに光を当てると、より作品を美しく引き立たせ、厚みがあることで、立体的に見せてくれます。
デスクや棚等お気に入りの場所に飾れば、好きな作品をより身近に感じていただけます。

【仕様】
●素材：アクリル
●サイズ：高さ148×幅100×厚さ20mm
発送時期：7月下旬～8月上旬頃にお届け予定

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


## [アクリルブロック（aim） / シティーハンター](https://edition-88.com/products/cityhunter-acrylicblock1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acryl_block_city-01_bb4bf52e-3819-4a87-ace7-822ad75922e0_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acryl_block_city-01_bb4bf52e-3819-4a87-ace7-822ad75922e0_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylblock_city-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylblock_city-02_4800x.jpg) 

¥6,600  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

シティーハンターのイラストをコンパクトなサイズでそのまま飾れるアクリルブロックに。  

透明感があるアクリルに光を当てると、より作品を美しく引き立たせ、厚みがあることで、立体的に見せてくれます。  
デスクや棚等お気に入りの場所に飾れば、好きな作品をより身近に感じていただけます。  

【仕様】  
●素材：アクリル  
●サイズ：高さ148×幅100×厚さ20mm  
発送時期：7月上旬～下旬頃にお届け予定  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [トートバッグ(shoot） / シティーハンター](https://edition-88.com/products/cityhunter-totebag3)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/tote-01_b692bd86-0e8d-47dd-a02a-c1889467998d_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/tote-01_b692bd86-0e8d-47dd-a02a-c1889467998d_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/tote-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/tote-02_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/osm-tote03_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/osm-tote03_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/osm-tote04_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/osm-tote04_4800x.jpg) 

¥2,420  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

持ち歩きに便利なアイテム。  

シティーハンターのイラストを使用した、ファスナー付きトートバッグです。  

しっかりとした厚みがある、丈夫な生地です。  
袋部分のサイズは約H380×W370mm。  
収納口は約350mm。  
天ファスナー付きなので、貴重品もバッチリ保護できます。  
マチが約113mmあり、A4クリアファイルもすっぽり入るサイズ感。  
内ポケットも外ポケットもあるので、鍵やパスケースを入れておくのにも便利です。  
ハンドル部分はH570×W30mm。肩掛けできる、ちょうど良い長さです。  

【仕様】  
●サイズ：本体 約H380×W370mm、マチ 約113mm / 収納口 約350mm / 持ち手 H570×W30mm  
●素材：コットン  

©北条司／コアミックス 1985  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [メガネ拭き / こもれ陽の下で…（北条司）](https://edition-88.com/products/hojoten-glassescloth1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganehuki-komorebi_3d61b32a-7d81-4bdd-8c88-cedf47349c34_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganehuki-komorebi_3d61b32a-7d81-4bdd-8c88-cedf47349c34_4800x.jpg) 

¥990  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

北条司先生の短編集『こもれ陽の下で…」のイラストを使用したメガネ拭きが登場！  

プリントの工程は1つ1つ、手作業で行っています。  
昇華転写プリントのため、発色がとても綺麗です。  
約18×25.5cmと、少し大きめサイズ感です。  
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。  
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。  

【仕様】  
●サイズ：約H180×W255mm  
※素材の製法上、形に多少個体差ございます。予めご了承ください。  
●素材：ポリエステル100%(マイクロファイバー生地)  

🄫北条司／コアミックス  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [メガネ拭き（dance） / シティーハンター](https://edition-88.com/products/cityhunter-glassescloth9)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganehuki-city3_fd397c18-b3d1-40ae-8cb1-1694cd084d2e_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganehuki-city3_fd397c18-b3d1-40ae-8cb1-1694cd084d2e_4800x.jpg) 

¥990  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

シティーハンターのイラストを使用したメガネ拭きが登場！  

プリントの工程は1つ1つ、手作業で行っています。  
昇華転写プリントのため、発色がとても綺麗です。  
約18×25.5cmと、少し大きめサイズ感です。  
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。  
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。  

【仕様】  
●サイズ：約H180×W255mm  
※素材の製法上、形に多少個体差ございます。予めご了承ください。  
●素材：ポリエステル100%(マイクロファイバー生地)  

🄫北条司／コアミックス 1985  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [メガネ拭き（Rouge） / シティーハンター](https://edition-88.com/products/cityhunter-glassescloth8)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganehuki-city2_97532462-a232-4ff9-9e91-07042e5a7cc5_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganehuki-city2_97532462-a232-4ff9-9e91-07042e5a7cc5_4800x.jpg) 

¥990  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

シティーハンターのイラストを使用したメガネ拭きが登場！  

プリントの工程は1つ1つ、手作業で行っています。  
昇華転写プリントのため、発色がとても綺麗です。  
約18×25.5cmと、少し大きめサイズ感です。    
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。  
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。  

【仕様】  
●サイズ：約H180×W255mm  
※素材の製法上、形に多少個体差ございます。予めご了承ください。  
●素材：ポリエステル100%(マイクロファイバー生地)  

🄫北条司／コアミックス 1985  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [メガネ拭き（window light） / シティーハンター](https://edition-88.com/products/cityhunter-glassescloth7)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganehuki-city_c1cc2489-8f00-4f70-8ad6-ebda55ddaa22_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganehuki-city_c1cc2489-8f00-4f70-8ad6-ebda55ddaa22_4800x.jpg) 

¥990  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

シティーハンターのイラストを使用したメガネ拭きが登場！  

プリントの工程は1つ1つ、手作業で行っています。  
昇華転写プリントのため、発色がとても綺麗です。  
約18×25.5cmと、少し大きめサイズ感です。  
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。  
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。  

【仕様】  
●サイズ：約H180×W255mm  
※素材の製法上、形に多少個体差ございます。予めご了承ください。  
●素材：ポリエステル100%(マイクロファイバー生地)  

Ⓒ北条司／コアミックス 1985  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [ビッグタオル( window light）/ シティーハンター](https://edition-88.com/products/cityhunter-bigtowel1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/big_towel-01_9ae8386e-6f81-4414-a92b-68d6bb26f524_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/big_towel-01_9ae8386e-6f81-4414-a92b-68d6bb26f524_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/bigtowel-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/bigtowel-02_4800x.jpg) 

¥4,235  

税込み 送料計算済み チェックアウト時  

受注期間：5月20日(月)～6月10日(月)  

しっかりとした厚みと、優しい手触りのビックタオルです。  

色鮮やかな高発色のプリント。  
サイズはH1200×W600 mmと大きめなのでバスタオルとしても、タペストリーのようにお部屋に飾るのもおすすめです。  

【仕様】  
●サイズ：約H1200×W600mm  
※タオルの製法上、形に多少個体差ございます。予めご了承ください。  
●素材：ポリエステル80%、ポリアミド20％  

Ⓒ北条司／コアミックス 1985  
発送時期：7月上旬～下旬頃にお届け予定  

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  


## [アクリルブロック(shoot） / シティーハンター](https://edition-88.com/products/cityhunter-acrylicblock2)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylicblock-city2-01_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylicblock-city2-01_4800x.jpg)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylicblock-city2-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/acrylicblock-city2-02_4800x.jpg)

¥6,600

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

シティーハンターのかっこいいのイラストをコンパクトなサイズでそのまま飾れるアクリルブロックに。

透明感があるアクリルに光を当てると、より作品を美しく引き立たせ、厚みがあることで、立体的に見せてくれます。
デスクや棚等お気に入りの場所に飾れば、好きな作品をより身近に感じていただけます。

【仕様】
●素材：アクリル
●サイズ：高さ148×幅100×厚さ20mm
発送時期：7月下旬～8月中旬頃にお届け予定

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。
  
## [メガネ拭き（woods） / シティーハンター](https://edition-88.com/products/cityhunter-glassescloth10)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganefuki-city4_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganefuki-city4_4800x.jpg)

¥990

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

シティーハンターのイラストを使用したメガネ拭きが登場！

プリントの工程は1つ1つ、手作業で行っています。
昇華転写プリントのため、発色がとても綺麗です。
約18×25.5cmと、少し大きめサイズ感です。
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。

【仕様】
●サイズ：約H180×W255mm
※素材の製法上、形に多少個体差ございます。予めご了承ください。
●素材：ポリエステル100%(マイクロファイバー生地)

©北条司／コアミックス 1985
発送時期：7月下旬～8月中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


## [メガネ拭き / ファミリー・プロット](https://edition-88.com/products/hojoten-glassescloth2)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganefuki-family_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganefuki-family_4800x.jpg)

¥990

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

ファミリー・プロットのイラストを使用したメガネ拭きが登場！

プリントの工程は1つ1つ、手作業で行っています。
昇華転写プリントのため、発色がとても綺麗です。
約18×25.5cmと、少し大きめサイズ感です。
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。

【仕様】
●サイズ：約H180×W255mm
※素材の製法上、形に多少個体差ございます。予めご了承ください。
●素材：ポリエステル100%(マイクロファイバー生地)

c北条司／コアミックス 1985
発送時期：7月下旬～8月中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


## [メガネ拭き / F.COMPO](https://edition-88.com/products/hojoten-glassescloth3)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganefuki-fcompo_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/meganefuki-fcompo_4800x.jpg)  

¥990

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

F.COMPOのイラストを使用したメガネ拭きが登場！

プリントの工程は1つ1つ、手作業で行っています。
昇華転写プリントのため、発色がとても綺麗です。
約18×25.5cmと、少し大きめサイズ感です。
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。

【仕様】
●サイズ：約H180×W255mm
※素材の製法上、形に多少個体差ございます。予めご了承ください。
●素材：ポリエステル100%(マイクロファイバー生地)

c北条司／コアミックス 1985
発送時期：7月下旬～8月中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


## [アートタイル（aim） / シティーハンター](https://edition-88.com/products/cityhunter-arttile10)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile-city2_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile-city2_4800x.jpg)

¥990

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

シティーハンターのイラストを使用したアートタイル。

直径約90mm、厚さ5mmと片手に乗るサイズです。
国産の粘土を使用して焼成した、美濃の素焼きの陶器です。
表面に肉眼では見えない大変細かい穴が空いているため吸水性が高く、耐熱性もあるため、コースターとしても機能性抜群です！
来客時のおもてなしにもオススメ♪
アートとしてお部屋に飾るのにも場所を選びません。

【仕様】
●サイズ：直径約90mm 厚さ5mm
●重さ：約80g
●素材：陶器
●生産国　日本
※珪藻土(アスベスト)は使用しておりません
※水滴以外は吸水面にシミが生じる場合があります。
※食器洗い機は非対応です。
※素材の性質上、細かい傷がある、吸水の穴が目立つことがございます。

©北条司／コアミックス 1985
発送時期：8月上旬～中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


## [アートタイル（cheers）/ エンジェル・ハート](https://edition-88.com/products/angelheart-arttile1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile-angelheart_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile-angelheart_4800x.jpg)

¥990

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

エンジェル・ハートのイラストを使用したアートタイル。

直径約90mm、厚さ5mmと片手に乗るサイズです。
国産の粘土を使用して焼成した、美濃の素焼きの陶器です。
表面に肉眼では見えない大変細かい穴が空いているため吸水性が高く、耐熱性もあるため、コースターとしても機能性抜群です！
来客時のおもてなしにもオススメ♪
アートとしてお部屋に飾るのにも場所を選びません。

【仕様】
●サイズ：直径約90mm 厚さ5mm
●重さ：約80g
●素材：陶器
●生産国　日本
※珪藻土(アスベスト)は使用しておりません
※水滴以外は吸水面にシミが生じる場合があります。
※食器洗い機は非対応です。
※素材の性質上、細かい傷がある、吸水の穴が目立つことがございます。

©北条司／コアミックス
発送時期：8月上旬～中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


## [アートタイル（smile） / エンジェル・ハート](https://edition-88.com/products/angelheart-arttile2)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile-angelheart2_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile-angelheart2_4800x.jpg)

¥990

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

エンジェル・ハートのイラストを使用したアートタイル。

直径約90mm、厚さ5mmと片手に乗るサイズです。
国産の粘土を使用して焼成した、美濃の素焼きの陶器です。
表面に肉眼では見えない大変細かい穴が空いているため吸水性が高く、耐熱性もあるため、コースターとしても機能性抜群です！
来客時のおもてなしにもオススメ♪
アートとしてお部屋に飾るのにも場所を選びません。

【仕様】
●サイズ：直径約90mm 厚さ5mm
●重さ：約80g
●素材：陶器
●生産国　日本
※珪藻土(アスベスト)は使用しておりません
※水滴以外は吸水面にシミが生じる場合があります。
※食器洗い機は非対応です。
※素材の性質上、細かい傷がある、吸水の穴が目立つことがございます。

©北条司／コアミックス
発送時期：8月上旬～中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。


## [アートタイル / F.COMPO](https://edition-88.com/products/hojoten-arttile1)
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile-fcompo_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/arttile-fcompo_4800x.jpg)

¥990

税込み 送料計算済み チェックアウト時

受注期間：2024年6月24日(月)～7月15日(月)

F.COMPOのイラストを使用したアートタイル。

直径約90mm、厚さ5mmと片手に乗るサイズです。
国産の粘土を使用して焼成した、美濃の素焼きの陶器です。
表面に肉眼では見えない大変細かい穴が空いているため吸水性が高く、耐熱性もあるため、コースターとしても機能性抜群です！
来客時のおもてなしにもオススメ♪
アートとしてお部屋に飾るのにも場所を選びません。

【仕様】
●サイズ：直径約90mm 厚さ5mm
●重さ：約80g
●素材：陶器
●生産国　日本
※珪藻土(アスベスト)は使用しておりません
※水滴以外は吸水面にシミが生じる場合があります。
※食器洗い機は非対応です。
※素材の性質上、細かい傷がある、吸水の穴が目立つことがございます。

©北条司／コアミックス
発送時期：8月上旬～中旬頃にお届け予定

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。

----  

----  

此外还有"CE 40周年"时的展品。详见：  
[北条司 @edition-88](https://edition-88.com/collections/hojotsukasa?page=1)  
[Tsukasa Hojo @edition88](https://edition88.com/collections/tsukasa-hojo)  
