source:  
https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-report  
https://edition88.com/blogs/news/hojo-tsukasa-exhibition-40th-2025-report (English version)  

2024年 5月 22日  

# 北条司展後期、本日(5/22)よりスタート！  
北条司展览後期，今天（5月22日）开始举办！   

本日より北条司展 The road to 『CITY HUNTER』40th anniversary 2025 〜Limited Special exhibition〜 後半がスタート！  
今天，北条司展 The road to 『CITY HUNTER』40th anniversary 2025 〜Limited Special exhibition〜 后半部分 开始！   

会場の様子をご紹介いたします。  
入口のフォトスポットに、先生がサインを入れてくださいました！  
这里是会场一角。  
老师在入口处的拍照点签名！  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7503_1024x1024.jpg?v=1716287928)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7505_1024x1024.jpg?v=1716287928)

**キャッツ♥アイ / CAT'S♥EYE▼**  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7484_1024x1024.jpg?v=1716288067)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7475_1024x1024.jpg?v=1716288072)

**シティーハンター / CITY HUNTER▼**  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7462_1024x1024.jpg?v=1716288081)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7486_1024x1024.jpg?v=1716288085)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7483_1024x1024.jpg?v=1716288239)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7487_1024x1024.jpg?v=1716288243)
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7469_1024x1024.jpg?v=1716288333)

**エンジェル・ハート / Angel Heart ▼**  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7493_1024x1024.jpg?v=1716289961)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7495_1024x1024.jpg?v=1716289969)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7494_1024x1024.jpg?v=1716290072)

**短編集/ Short story collection▼**  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7478_1024x1024.jpg?v=1716290136)![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7479_1024x1024.jpg?v=1716290141)

**当時の書籍や先生の私物▼**  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7476_1024x1024.jpg?v=1716290148)![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7477_1024x1024.jpg?v=1716290152)

**過去に販売された商品▼**  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7482_1024x1024.jpg?v=1716290158)

**物販コーナーの様子▼**  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7506_1024x1024.jpg?v=1716290440)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7490_600x600.jpg?v=1716290445)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_7488_1024x1024.jpg?v=1716290449)

2024年6月23日（日）まで、東京・吉祥寺のGALLERY ZENONにて開催中です！  
展览将在东京吉祥寺 GALLERY ZENON 举行，直至 2024 年 6 月 23 日（星期日）！ 

©北条司／コアミックス　©Tsukasa Hojo/COAMIX  