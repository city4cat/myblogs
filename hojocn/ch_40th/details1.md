
## City Hunter 40周年「北条司展」展览信息 & 部分展品（Part 2/5） 

<a name="note"></a>  
## 写在前面的话  
- City Hunter 40周年「北条司展」以下简称“画展”。  
- 为了对比绘画风格的演变，本文尽可能地按作品的时间顺序排列原画。  
- 文本讨论什么、不讨论什么:  
    - 本文主要关注作者北条司的绘画。  
    - 图片上有些效果是印刷方法导致的，有些效果是纸张导致的，本文不讨论这些话题。    
    - 暂不考虑非原画商品（如亚克力商品等）上的图片
- 为了便于观察作者的绘画风格的变迁，本文按创作时间顺序罗列展品。  


-----  
<a name="greetings"></a>  
## 作者简介 & 致辞  

[![](img/thumb/20240503093952.jpg "现场拍照@hatenablog")](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503093952.jpg) 
[![](img/thumb/GLV88hza8AACECa.jpg "现场拍照@twitter")](https://pbs.twimg.com/media/GLV88hza8AACECa?format=jpg&name=4096x4096)  

英文原文：  
>Greetings  
Thank you very much for attending the GALLERY ZENON Grand Opening Special Event, "Tsukasa Hojo Exhibition."   
This exhibition covers not only serialized works but also many short stories. As the creator, I must admit it's quite embarrassing to see my old works (laughs).  
However, being a manga artist is a profession where embarrassment is inevitable.  
Since there aren't many opportunities to view original drawings, please feel free to look around and enjoy.  
Your freedom to feel and even laugh at some of the artworks is welcomed (laughs).  
I am truly grateful for the support of all the fans, which allows me to continue being a manga artist. The true star of manga is not the artist, but the "work" itself.  
There is no greater joy than seeing people enjoy my creations.  
Please take your time and enjoy the exhibition.  
April 2024  
Tsukasa Hojo  

译文：   
>致辞  
非常感谢您参加 GALLERY ZENON 的开业特别活动"北条司展"。  
这次展览不仅有连载作品，还有许多短篇故事。作为创作者，我必须承认，看到自己以前的作品很尴尬（笑）。  
不过，作为漫画家，尴尬是在所难免的。  
由于欣赏原画的机会不多，请大家随意欣赏。  
欢迎大家自由感受，甚至嘲笑某些作品（笑）。  
我真心感谢所有粉丝的支持，是他们让我能够继续成为一名漫画家。漫画的真正主角不是艺术家，而是 "作品"本身。 
没有什么比看到人们喜欢我的作品更让人高兴的了。  
请大家慢慢欣赏展览。  
2024年4月  
北条司  

---  

<a name="i-am-a-man"></a>  
## おれは男だ！  (我是男子汉！/ I am a man![^exh_man])  (1980)  

[^exh_man]: 展品显示该作品的英文名为“I am a man!”。   

<a name="i-am-a-man-info"></a>  
### 解说文字  
[![](img/thumb/GM5ttlbaEAAIwHa.jpg)](https://pbs.twimg.com/media/GM5ttlbaEAAIwHa?format=jpg&name=4096x4096) 
[![](img/thumb/GN6fEKobEAAYLfH.jpg)](https://pbs.twimg.com/media/GN6fEKobEAAYLfH?format=jpg&name=4096x4096) 
[![](img/thumb/GN6fEKnaMAAAaL-.jpg)](https://pbs.twimg.com/media/GN6fEKnaMAAAaL-?format=jpg&name=4096x4096) 


（待完善。因照片不清晰，故无法识别的文字用字符“#”代替。）
  
>おれは男だ!  
我是男子汉！  
I am a Man!  
週刊少年ジャンプ 1980年8月增刊号  
周刊少年Jump 1980年8月增刊号  
...  

>**北条先生に聞きました!**  
**我们请教了北条老师！**  

>Q：大学在学中の作品でデピュ一作てすが、当時のェビソードなどれ聞かせくたさい。    
还是一名大学生的时候，就开始制作的第一部作品，请介绍一下您当时的工作情况。  

>集英社主催の手冢賞で準入選した事をきつかけに、担当编集者(现ユア#ックス社長の堀江氏)がついて、その後に最初に#いた作品ですがこれが揭載されデビュ一作品になりました。  
大学4年生で卒業制作もありとても忙しかったのですが、大学生は#だと思つてろのか堀江さんから#け#けとすく催促きれた記憶があります(笑)。  
我在集英社举办的手冢奖中获得亚军，并因此结识了责任编辑（现Coamix社长堀江），这是在那之后的第一个作品，这是连载后的第一部作品。  
虽然大学4年级的学生也有毕业制作，所以很忙，但我记得堀江先生突然催促....(笑)。(译注：原文不完整导致此处译文不通，故省略。)  

<a name="i-am-a-man_title2"></a>   

>Q：「アナクロ ラプソディー」という別タイトルもありましたがー？  
你还有另一个标题，「Anachronistic Rhapsody」（译注：不合时宜的狂想曲）。  

>私はずつと「アナクロ ラプソディー」というタイトルで#いていたのでずが、揭載した週刊少年ジャンプを見たら「おれは男だ」というタイトルに変わっていました。  
堀江さんが相識なく聯手に変えたのですが--ダサすぎて#に何も言えませんでした(笑)。  
我一直用「Anachronistic Rhapsody」这个标题进行创作的，但当我看到集英社出版的周刊少年Jump时，标题被改成了「我是男子汉！」。  
堀江完全不告知我就把标题改成了「我是男子汉！」--这实在是太离谱了，我无话可说（笑）。  

---

<a name="1980-08-i0"></a>  
###  "我是男子汉！" 扉页 (1980.08)  
[![](./img/thumb/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_4800x.jpg) 
[![](./img/thumb/fukusei-otoko-02.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-otoko-02_4800x.jpg) 
[![](img/thumb/5_hojo-ten_fukuseigenko_0.jpg "源自「北条司展」, B4尺寸。News@edition-88")
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info) 
[![](img/thumb/GLlumlLbsAAqp2A-0.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLlumlLbsAAqp2A?format=jpg&name=4096x4096) 
[![](img/thumb/GLWG9SEaoAA0_pj.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLWG9SEaoAA0_pj?format=jpg&name=4096x4096) 


细节：   

- 左下角签名日期为“‘80.4”  

<a name="i-am-a-man_title"></a>   

- 作品的标题为手写体“**アナクロ♡ラプソディ**”。同时，左上角有印刷体“**Anachronistic Rhapsody!**”字样。  
考虑到“Anachronistic Rhapsody”的日文为：“アナクロニスティック・ラプソディ”；而“アナクロ♡ラプソディ”可看作其缩写。正如上述解说文字所说，该作品的原标题为“アナクロ♡ラプソディ”（英文名为“Anachronistic Rhapsody!”）。    
![](img/GLWG9SEaoAA0_pj-0.jpg)  

- 头发的细节：  
![](img/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_1.jpg) 
![](img/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_2.jpg)  
![](img/fukusei-otoko-02_0.jpg) 
![](img/fukusei-otoko-02_1.jpg) 
![](img/fukusei-otoko-02_2.jpg) 
![](img/fukusei-otoko-02_3.jpg) 

- 面部的细节：  
![](img/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_3.jpg) 
![](img/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_4.jpg)  
![](img/fukusei-otoko-02_4.jpg) 
![](img/fukusei-otoko-02_5.jpg) 

- 排线阴影：  
![](img/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_5.jpg) 
![](img/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_6.jpg) 
![](img/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_7.jpg)  
排线似乎有凹凸感：  
![](img/fukusei-otoko-02_6.jpg) 
![](img/fukusei-otoko-02_7.jpg) 

- 黑色物体（衣服）之间用白线间隔。  
![](img/fukusei-otoko-01_c09bd13a-74d7-4c06-b07c-78d0754886b4_8.jpg)  
下图左侧的白线（男主袖子和上衣的分隔）似乎是用白色颜料；右侧的白线（两角色的分隔）似乎是留白。这在降低gamma值后容易看出。  
![](img/fukusei-otoko-02_8.jpg) 
![](img/fukusei-otoko-02_9.jpg) 
![](img/fukusei-otoko-02_8-gamma.jpg) 
![](img/fukusei-otoko-02_9-gamma.jpg) 

- 边线清晰可见。  
![](img/fukusei-otoko-02_10.jpg) 
![](img/fukusei-otoko-02_11.jpg) 


---

<a name="1980-08-i1"></a>  
### “我是男子汉！” (1980.08)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLlumlLbsAAqp2A-1.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLlumlLbsAAqp2A?format=jpg&name=4096x4096) 
[![](img/thumb/GN6fEKobEAAYLfH.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GN6fEKobEAAYLfH?format=jpg&name=4096x4096)   

细节：   

(未找到足够大的图片以查看其细节。)  

---

<a name="1980-08-i2"></a>  
### “我是男子汉！” (1980.08)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLlumlLbsAAqp2A-2.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLlumlLbsAAqp2A?format=jpg&name=4096x4096)  

细节：   

(未找到足够大的图片以查看其细节。)  

---

<a name="1980-08-i3"></a>  
### “我是男子汉！” (1980.08)  

![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLlumlLbsAAqp2A-3.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLlumlLbsAAqp2A?format=jpg&name=4096x4096) 

细节：   

(未找到足够大的图片以查看其细节。)  

---  

---

<a name="ce"></a>  
## Cat's♥Eye  (1981～1985)  

<a name="ce-info"></a>  
### 解说文字  
[![](img/thumb/441930786_7336354519795260_1495645532144644959_n-0.jpg "现场拍照@Facebook")
](https://www.facebook.com/hashtag/北条司展) 
[![](img/thumb/GayirkQbMAA65jl-0.jpg)
](https://pbs.twimg.com/media/GayirkQbMAA65jl?format=jpg&name=4096x4096) 
[![](img/thumb/Gaoi37LaEAEUE9C-0.jpg)
](https://pbs.twimg.com/media/Gaoi37LaEAEUE9C?format=jpg&name=4096x4096) 

（注：福冈展有更多的说明文字。但照片分辨率不够大，上看不清楚其中的文字。）  


>喫茶店「キャッツアイ」を営む美人三姉妹、泪・瞳・愛。彼女たちの裏の顔は、美術品をターゲットにする怪盗・キャッツアイだった。瞳の恋人で警察官の内海俊夫は、その正体を知らぬままキャッツを追う......。  
经营咖啡店“猫眼”的美人三姐妹，泪·瞳·爱。 她们的真面目是 "猫眼"，一个专偷艺术品的怪盗。 瞳的恋人·警官内海俊夫在不知道猫的真实身份的情况下追捕他们......。  

>華麗なる三姉妹のアクションと、瞳・俊夫の恋模様が交錯する北条司の連載デビュー作。  
华丽的三姐妹的动作戏、瞳·俊夫的爱情故事交织在一起的北条司的连载出道作。  


---

<a name="1981-04-i0"></a>  
### CE 第1话 （1981.04) 
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMLxqllboAAQU3h.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GMLxqllboAAQU3h?format=jpg&name=4096x4096) 
[![](img/thumb/GNs7CZJbwAAz17U.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GNs7CZJbwAAz17U?format=jpg&name=4096x4096) 

细节：     

- 对于(正面)鼻梁处的线条，作者习惯鼻梁左侧（图片右侧）的线条，如下图左一。这个规律在该话中已出现（左二、左三）。    
![](img/GMLxqllboAAQU3h-0.jpg) 
![](img/GMLxqllboAAQU3h-1.jpg) 

- 白色颜料的涂改痕迹。  
![](img/GMLxqllboAAQU3h-2.jpg) 

- 排线阴影：  
![](img/GMLxqllboAAQU3h-3.jpg) 
![](img/GMLxqllboAAQU3h-4.jpg) 

- 脸庞处有白色涂抹：  
![](img/GMLxqllboAAQU3h-5.jpg) 
![](img/GMLxqllboAAQU3h-6.jpg) 

- 特效线条处似乎有白色涂抹痕迹。  
![](img/GMLxqllboAAQU3h-7.jpg) 

- 拟声词上喷洒有白色颜料。拟声词边界用白色描边。    
![](img/GMLxqllboAAQU3h-9.jpg) 
![](img/GMLxqllboAAQU3h-8.jpg) 

更多细节参见[CE40th纪念原画展](../ce_40th/details1.md#1981-04-i5)


---

<a name="1981-11-i0"></a>  
### CE 2卷 第13话 ”父亲的肖像“ (1981.11)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPETU7LaAAABKuH.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GPETU7LaAAABKuH?format=jpg&name=4096x4096) 
[![](img/thumb/GP9Emm4bwAAVURq.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GP9Emm4bwAAVURq?format=jpg&name=4096x4096) 
[![](img/thumb/GUgSgSDXMAAtfu6.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GUgSgSDXMAAtfu6?format=jpg&name=4096x4096) 

细节：   

参见[CE40th纪念原画展](../ce_40th/details1.md#1981-11-i1)


---  

<a name="1981-12-i0"></a>  
###  CE 3卷 19话 “希望时间就此停止之卷” (1981.12)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GL6vgHvbgAAuPEe.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GL6vgHvbgAAuPEe?format=jpg&name=4096x4096)
[![](img/thumb/439462833_17884137000028057_5026646394702991927_n.jpg "现场拍照@facebook")
](https://www.facebook.com/photo/?fbid=7281659578598185&set=pcb.7281659735264836) 
[![](img/thumb/435793274_18432706447038826_2995574109973761107_n.jpg "现场拍照@facebook")
](https://www.facebook.com/hashtag/北条司展) 
[![](img/thumb/GNs7CcKbAAAbRk8.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GNs7CcKbAAAbRk8?format=jpg&name=4096x4096) 

细节：   

- 左页面的右下角有疑似“ッツアイ...?”的剪纸字样。  


---  

<a name="1982-07-i0"></a>  
### CE 插画 (1982.07)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMG2-AhaIAATU_G.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMG2-AhaIAATU_G?format=jpg&name=4096x4096) 
[![](img/thumb/440234281_17884136979028057_8140942751192469916_n.jpg "现场拍照@facebook")
](https://www.facebook.com/photo/?fbid=7281659585264851&set=pcb.7281659735264836) 
[![](img/thumb/GNrmIQzbIAAzRbZ.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GNrmIQzbIAAzRbZ?format=jpg&name=4096x4096)  
[![](img/thumb/GQkfK91akAUlAea.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQkfK91akAUlAea?format=jpg&name=4096x4096)  

细节：     

参见[CE40th纪念原画展](../ce_40th/details1.md#1982-07-i1)

---  

<a name="1982-11-i0"></a>  
### CE 插画 (1982.11)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GP4kAKuaMAAsy8T.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GP4kAKuaMAAsy8T?format=jpg&name=4096x4096) 
[![](img/thumb/GQF-NJxbEAQydhX.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQF-NJxbEAQydhX?format=jpg&name=4096x4096) 

细节：     

参见[CE40th纪念原画展](../ce_40th/details1.md#1982-11-i1)

---  

<a name="1983-04-i0"></a>  
###  CE 91话 18卷版第11卷“求婚记” (1983.04)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPETU7LaAAABKuH.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GPETU7LaAAABKuH?format=jpg&name=4096x4096) 
[![](img/thumb/GUgSgRfa8AEJsVx.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GUgSgRfa8AEJsVx?format=jpg&name=4096x4096) 
[![](img/thumb/GUgSgRha8AAOQy_.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GUgSgRha8AAOQy_?format=jpg&name=4096x4096) 

细节：   
参见[CE40th纪念原画展](../ce_40th/details1.md#1983-04-i1)

---  

<a name="1983-0407-i0"></a>  
###  CE 完全版第10卷 96话“浅谷的初吻” (1983.04～07)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMrZ68eXEAATkRS.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMrZ68eXEAATkRS?format=jpg&name=4096x4096) 

细节：   
参见[CE40th纪念原画展](../ce_40th/details1.md#1983-0407-i2)



---

<a name="1983-0407-i1"></a>  
###  CE 完全版第10卷 96话“浅谷的初吻” (1983.04～07)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GN6e01sbIAA20vy-1.jpg "现场拍照，右侧图@twitter")
](https://pbs.twimg.com/media/GN6e01sbIAA20vy?format=jpg&name=4096x4096)

细节：  
参见[CE40th纪念原画展](../ce_40th/details1.md#1983-0407-i2)



---

<a name="1983-0407-i2"></a>  
### CE 插画 (1983.04～07)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/441505787_17969220491718230_9027313139535278166_n.jpg "现场拍照@Instagram")
](https://www.instagram.com/explore/tags/北条司展) 

细节：   
参见[CE40th纪念原画展](../ce_40th/details1.md#1983-0407-i1)



---

<a name="1983-07-i0"></a>  
###  CE 99话 (18卷版第12卷 “抓住我一輩子！之卷”) (1983.07)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLbVz2LaAAANswV-0.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLbVz2LaAAANswV?format=jpg&name=4096x4096)
[![](img/thumb/GNs7ClDa0AAdPRA.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNs7ClDa0AAdPRA?format=jpg&name=4096x4096)
[![](img/thumb/GN7bPSubkAAvBNc.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GN7bPSubkAAvBNc?format=jpg&name=4096x4096)

细节：   

- 金发猫眼的头发高光。  
![](img/GLbVz2LaAAANswV-2.jpg) 
![](img/GN7bPSubkAAvBNc-1.jpg) 
![](img/GN7bPSubkAAvBNc-2.jpg) 
![](img/GN7bPSubkAAvBNc-3.jpg) 

- 网点纸遮盖部份头发。  
![](img/GN7bPSubkAAvBNc-0.jpg) 

- 网点纸处有蓝色示意线。  
![](img/GN7bPSubkAAvBNc-4.jpg) 

- 面部细节：  
![](img/GN7bPSubkAAvBNc-5.jpg) 
![](img/GN7bPSubkAAvBNc-6.jpg) 

- 对话框处的补丁。  
![](img/GN7bPSubkAAvBNc-7.jpg) 
![](img/GN7bPSubkAAvBNc-12.jpg) 

- 背景排线阴影。  
![](img/GN7bPSubkAAvBNc-8.jpg) 
![](img/GN7bPSubkAAvBNc-9.jpg) 

- 角色一侧（右侧）用白色线条间隔背景。    
![](img/GN7bPSubkAAvBNc-10.jpg) 

- 网点纸着色。  
![](img/GN7bPSubkAAvBNc-11.jpg) 

---

<a name="1983-07-i1"></a>  
###  CE 99话 (18卷版第12卷 “抓住我一輩子！之卷”) (1983.07)   
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLbVz2LaAAANswV-1.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLbVz2LaAAANswV?format=jpg&name=4096x4096)
[![](img/thumb/GNs7ClDa0AAdPRA.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNs7ClDa0AAdPRA?format=jpg&name=4096x4096)

细节：   

- 原稿很整洁。    


---

<a name="1983-07-i2"></a>  
### CE 插画 (1983.07.11)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GP4kAMXaYAAuW7u.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GP4kAMXaYAAuW7u?format=jpg&name=4096x4096) 

细节：   
参见[CE40th纪念原画展](../ce_40th/details1.md#1983-07-11)

---

<a name="1983-11-i0"></a>  
### CE 周刊少年Jump 1983 Issue#48 Front Cover Poster (可能为1983.11)  

![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLXGnSXbEAA-5X7.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GLXGnSXbEAA-5X7?format=jpg&name=4096x4096) 
[![](img/thumb/439587763_17884136991028057_8959542228828319372_n.jpg "现场拍照@facebook")
](https://www.facebook.com/photo/?fbid=7281659588598184&set=pcb.7281659735264836) 
[![](img/thumb/GNruiz8a4AIRkaa.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GNruiz8a4AIRkaa?format=jpg&name=4096x4096) 
[![](img/thumb/GQkfK92akAAQsp5.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQkfK92akAAQsp5?format=jpg&name=4096x4096) 


细节：   
参见[CE40th纪念原画展](../ce_40th/details1.md#1983-11-i1)


---

<a name="1983-12-i0"></a>  
### CE 1983年第46号 封面 (1983年底？)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GaUeNnhbUAAf7WG.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GaUeNnhbUAAf7WG?format=jpg&name=4096x4096)
[![](img/thumb/Gayzt0gaAAMg4B7.jpg "现场拍照@X")
](https://pbs.twimg.com/media/Gayzt0gaAAMg4B7?format=jpg&name=4096x4096)

细节：   
参见[CE40th纪念原画展](../ce_40th/details1.md#1983-i2)

---

<a name="1984-03-i0"></a>  
###  CE 插画 (1984.03)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLWG9SAacAARxSN.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GLWG9SAacAARxSN?format=jpg&name=4096x4096)
[![](img/thumb/GL2aqMub0AAhF8C.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GL2aqMub0AAhF8C?format=jpg&name=4096x4096) 
[![](img/thumb/438102725_3136284203174118_7890191886764488716_n.jpg "现场拍照@facebook")
](https://www.facebook.com/hashtag/北条司展) 
[![](img/thumb/GQkfK91akAc9T25.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQkfK91akAc9T25?format=jpg&name=4096x4096) 

细节：   
参见[CE40th纪念原画展](../ce_40th/details1.md#1984-03-i1)


---

<a name="1984-05-i0"></a>  
### CE 139话“似曾相識的風” （18卷版第17卷） (1984.05)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPETU-TaoAAdu6b.jpg "现场拍照@Instagram")
](https://pbs.twimg.com/media/GPETU-TaoAAdu6b?format=jpg&name=4096x4096) 
[![](img/thumb/GP4kANda4AAlGN-.jpg "现场拍照@Instagram")
](https://pbs.twimg.com/media/GP4kANda4AAlGN-?format=jpg&name=4096x4096) 

下图左侧页面在CE40周年时有展示。当时在edition-88.com上有高清大图可供查看细节。    
![](img/thumb/GP4kANda4AAlGN-.jpg) 

左侧页面的细节：   

- 逆光时头发边缘加了白色发丝。    
![](img/GP4kANda4AAlGN-l0.jpg) 
![](img/GP4kANda4AAlGN-l0g.jpg) 
![](img/GP4kANda4AAlGN-l1.jpg) 
![](img/GP4kANda4AAlGN-l1g.jpg) 

- 月光下的沙滩上的影子。  
![](img/GP4kANda4AAlGN-l2.jpg) 

- 影子的深浅有渐变。  
![](img/GP4kANda4AAlGN-l3.jpg) 


右侧页面的细节：   

- 头发的细节。  
![](img/GP4kANda4AAlGN--2.jpg) 
![](img/GP4kANda4AAlGN--3.jpg) 
![](img/GP4kANda4AAlGN--1.jpg) 
![](img/GP4kANda4AAlGN--0.jpg) 

另，参见[CE40th纪念原画展](../ce_40th/details1.md#1984-05-i3)


---

<a name="1984-08-i0"></a>  
### CE 157话“永远的牵绊” （18卷版第18卷） (约1984.08)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPETU-TaoAAdu6b.jpg "现场拍照@Instagram")
](https://pbs.twimg.com/media/GPETU-TaoAAdu6b?format=jpg&name=4096x4096) 
[![](img/thumb/GP2hAahaAAAhWkz.jpg "现场拍照@Instagram")
](https://pbs.twimg.com/media/GP2hAahaAAAhWkz?format=jpg&name=4096x4096) 

细节：   

- 飞机机体上的排线着色。排线是曲线。  
![](img/GP2hAahaAAAhWkz-0.jpg) 
![](img/GP2hAahaAAAhWkz-1.jpg)  
交叉排线着色：  
![](img/GP2hAahaAAAhWkz-2.jpg) 

- 拟声词和背景之间有白线间隔：  
![](img/GP2hAahaAAAhWkz-5.jpg)  
角色和背景之间有白线间隔：  
![](img/GP2hAahaAAAhWkz-4.jpg) 
![](img/GP2hAahaAAAhWkz-6.jpg) 

- 该分格里的女主的面部阴影较淡。网点稀疏。    
![](img/GP2hAahaAAAhWkz-7.jpg) 

- 该分格里的男主的面部阴影较重。  
![](img/GP2hAahaAAAhWkz-9.jpg) 

- 领带的排线着色；衣服里面的排线阴影。  
![](img/GP2hAahaAAAhWkz-8.jpg) 

- 页面底部似乎有纸屑，上面手写了文字。    
![](img/GP2hAahaAAAhWkz-3.jpg) 


---

<a name="1984-09-i0"></a>  
### CE 18卷版第18卷"再一次恋爱之卷" (1984.09.19)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/ce-2.jpg "「北条司展」 @GalleryZenon") 
](https://gallery-zenon.jp/) 
[![](img/thumb/GP4kAOTbcAAgcvi.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GP4kAOTbcAAgcvi?format=jpg&name=4096x4096)


细节：   
参见[CE40th纪念原画展](../ce_40th/details2.md#1984-09-i1)

---

<a name="1985-unknown-i0"></a>  
### CE 插画 (1985)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/ce-1.jpg "「北条司展」 @GalleryZenon")
](https://gallery-zenon.jp/) 
[![](img/thumb/GMLxqkcaoAAuHxS.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMLxqkcaoAAuHxS?format=jpg&name=4096x4096)
[![](img/thumb/GMnRWG9aAAAe4mF.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMnRWG9aAAAe4mF?format=jpg&name=4096x4096)
[![](img/thumb/GQkfK94bwAAeMvZ.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQkfK94bwAAeMvZ?format=jpg&name=4096x4096)

细节：   

参见[CE40th纪念原画展](../ce_40th/details2.md#1985-unknown-i1)



---

---

<a name="ch"></a>  
## シティーハンター(City Hunter)  (1985～1991)  

<a name="ch-note"></a>  
### 解说文字  
[![](img/thumb/GQzBkQsaMAAvPF-.jpg "现场拍照@hatenablog")
](https://pbs.twimg.com/media/GQzBkQsaMAAvPF-?format=jpg&name=4096x4096) 
[![](img/thumb/GaU_7b4bsAA1JT1.jpg "现场拍照@hatenablog")
](https://pbs.twimg.com/media/GaU_7b4bsAA1JT1?format=jpg&name=4096x4096) 

（注：福冈展上有更多的说明文字）：   

> **シティーハンター**  
**CITYHUNTER**  
週刊少年ジャンプ 1985年13号・1991年50号  
周刊少年Jump 1985年第13期～1991年第50期  

> 新宿駅東ロの伝言板に「XYZ」と書き込むことで現れるスイーパー・シティハンター。腕は立つけど無類の女好き・冴羽獠と、そのパートナー・香のドタバタコンビが、美人依頼人にもっこりしながら仕事を果たす。単行本累計5000万部を超え、今なお愛されるアクションコメディ。    
在新宿站东口的留言板上写下 "XYZ"，CityHunter就会出现。 身怀绝技却又风流成性的冴羽獠和他的搭档香这对滑稽组合，一边完成任务，一边被美丽的委托人迷住。 这部动作喜剧的总销量超过 5000 万册，至今仍受到人们的喜爱。  

> **北条先生に聞きました!**  
**北条老师问答!**  

> Q:「シティーハンター」の獠と香を理想のカップルと呼ぶファンも多くぉりますが、二人の關係性は当初からの狙いでしたでしょうか。  
问：有很多粉丝称「CITYHUNTER」中的獠和香是理想的情侣，两个人的关系是从一开始就设定好的吗？  

> 当初の構想には無かったですね。  
長期連載の流れの中で、作者の想像をこえた關係性が薬[^unocr]かれていきましを。  
...理想ですかね？私はそもそも冴羽獠になりたくないんてすよれ（笑）。  
だってあんな危険な仕事だし、大きなハンマーでも殴られたくないですよ（笑）。  
在最初的构想中是没有的。  
长期连载的过程中，超越作者想象的关系性被药[^unocr]化了。  
……理想吗？我本来就不想变成冴羽獠（笑）。  
因为是那么危险的工作，我也不想被大锤打（笑）。  

> Q:「シティーハンター」の中で、好きなキャラクターは誰でしょう。  
问：在「CITYHUNTER」中，喜欢哪个角色？  

> 描いていて樂しかったのは海坊主（ファルコン）です。なんか何やってもおかしいじぐないてすか（笑）。  
海坊主は元々ー回限りのキャラクターだったんですが、アニメのスタァフが気に人っていると聞いて、試しにもう一度漫画に出してみたらやつばり面白くて。アニメが魅力に気づせてくれたキャラクターですな。  
画的很开心的时候是海坊主（Falcon）。不管做什么都很奇怪吧（笑）。  
海坊主本来只是个一回限定的角色，听说他动画里的样子很受欢迎，就试着再出一次漫画，果然很有趣。动画中注意到魅力的角色呢。  

> Q：「シティーハンター」が2025年で40周年をむかえますが、ご感想などをお聞かせください。  
问：「CITYHUNTER」将于2025年迎来40周年，请谈谈您的感想。  

> 私の中では読み切りのシティーハンター（1983年）がスタート地点なので、実は2023年が40周年なんですよね（笑）。  
今だにアニメなども制作してもらっていて...不思議な感覚ですよね。  
今までも何度も言っていますが、私が漫画家になった頃、漫画は読み捨ての文化だと考えられていました。  
後々残るようなものては無く、連載が終おったらアッという問に忘れさられるものだと。  
そんな漫画が40年もたって、アニメ化されてお祝いもしてもらって、もちろんとても嬉しい事なのですが...本当に不思議だと感じています。  
对我来说，结束的城市猎人（1983年）是起点，所以实际上2023年是40周年呢（笑）。  
现在还在制作动画什么的…不可思议的感觉吧。  
我之前也说过很多次，我刚成为漫画家的时候，漫画被认为是一种看完就弃的文化。  
没有什么东西可以留存下来，连载结束后一转眼就被遗忘了。  
这样的漫画经过了40年，被动画化庆祝，当然是非常高兴的事…真的觉得很不可思议。  

[^unocr]: 该字看不清，识别可信度低。  



---  

<a name="ch00"></a>  
### CH 第1卷 第5话 (1985)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLbg_gUaIAAv2Fx-0.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLbg_gUaIAAv2Fx?format=jpg&name=4096x4096) 
[![](img/thumb/GM7uQ3YbUAAgtgL.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GM7uQ3YbUAAgtgL?format=jpg&name=4096x4096) 
[![](img/thumb/GNYg8s-bIAEPJxz.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNYg8s-bIAEPJxz?format=jpg&name=4096x4096) 

细节：   

- 边框、线条有大量白色颜料的修补。  
![](img/GM7uQ3YbUAAgtgL-0.jpg) 
![](img/GM7uQ3YbUAAgtgL-4.jpg) 
![](img/GM7uQ3YbUAAgtgL-5.jpg) 
 
- 特效处的白色颜料的修补。  
![](img/GM7uQ3YbUAAgtgL-1.jpg) 


- 角色面部的白色颜料的修补。  
![](img/GM7uQ3YbUAAgtgL-2.jpg) 
![](img/GM7uQ3YbUAAgtgL-3.jpg) 

- 手部的线条。  
![](img/GM7uQ3YbUAAgtgL-4.jpg)  ![](img/GM7uQ3YbUAAgtgL-6.jpg)  
![](img/GM7uQ3YbUAAgtgL-5.jpg)  ![](img/GM7uQ3YbUAAgtgL-7.jpg) 

---  

<a name="ch001"></a>  
### CH 第1卷 第5话 (1985)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLbg_gUaIAAv2Fx-1.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLbg_gUaIAAv2Fx?format=jpg&name=4096x4096) 
[![](img/thumb/GNYg8s-bIAEPJxz.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNYg8s-bIAEPJxz?format=jpg&name=4096x4096) 

细节：   

(未找到足够大的图片以查看其细节。)  


---  

<a name="ch002"></a>  
### CH 第1卷 第9话 (1985)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/ch-3.jpg "「北条司展」 @GalleryZenon")
](https://gallery-zenon.jp/) 

细节：   

- 某对话框处涂抹较多白色。这在降低gamma值后(左二)容易看出。      
![](img/ch-3_0.jpg) 
![](img/ch-3_0-gamma.jpg) 

- 某一边框处涂抹较多白色。这在降低gamma值后容易看出。  
![](img/ch-3_1-gamma.jpg) 

- 头发的细节。  
![](img/ch-3_2.jpg) 
![](img/ch-3_3.jpg) 

- 头发的着色不是全黑，墨色深浅不一。    
![](img/ch-3_4.jpg) 

- 头发的着色不是全黑，墨色深浅不一。   
![](img/ch-3_6.jpg) 
![](img/ch-3_7.jpg) 
![](img/ch-3_8.jpg) 
![](img/ch-3_9.jpg) 

- 摩托车的牌子。  
![](img/ch-3_10.jpg) 

- 摩托车的高光。  
![](img/ch-3_11.jpg) 


---  

<a name="ch003"></a>  
### CH 第2卷 第14话   (1985)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/ch-2.jpg "「北条司展」 @GalleryZenon")
](https://gallery-zenon.jp/) 
[![](img/thumb/20240503100643.jpg "现场拍照@hatenablog")
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503100643.jpg) 
[![](img/thumb/GN6e01uagAAI7S-.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GN6e01uagAAI7S-?format=jpg&name=4096x4096) 

细节：   

- C中一个镜头（左一）聚焦的特效线条用了白色着色。这在降低gamma值后（左三）容易看出。       
![](img/ch-2_crop1.jpg) 
![](img/ch-2_crop0.jpg) 
![](img/ch-2_crop0-gamma.jpg) 

- C中枪械的细节：  
![](img/ch-2_crop2.jpg)  
![](img/ch-2_crop3.jpg) 

- 该镜头似乎是后来补的，四周有明显的刻痕。这在降低gamma值后（左二）容易看出。     
![](img/ch-2_crop4.jpg) 
![](img/ch-2_crop4-gamma.jpg) 

- 枪火的着色。这在降低gamma值后（左二）的效果。  
![](img/ch-2_crop5.jpg) 
![](img/ch-2_crop5-gamma.jpg) 

- 原画左上角有如下图标。可能和该镜头中角色的姿态有关，[疑问]这表示什么意思？  
![](img/ch-2_crop6.jpg) 

- 裙子的着色。  
![](img/ch-2_crop7.jpg) 

- 背景遮阳伞的着色。伞里面的阴影应该是先着红色，后整体再着灰色。    
![](img/ch-2_crop8.jpg) 

- 背景植被的画法。  
![](img/ch-2_crop9.jpg) 

- 拟声词的灰色里透着背景里的红色。    
![](img/ch-2_crop10.jpg) 

- 溅血的画法。  
![](img/ch-2_crop11.jpg) 
![](img/ch-2_crop12.jpg) 
![](img/ch-2_crop13.jpg) 

- 溅血的方向与特效聚焦线条的方向一致。  
![](img/ch-2_crop14.jpg) 

---

(1986年1月，作者创作了[《白猫少女》](./details2.md#catlady)。)  

---  

<a name="ch004"></a>  
### CH 第6卷 第58话 扉页 (1986)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/2_city_88graph_silver_3.jpg "News@edition-88")
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info)
[![](img/thumb/GLlsB26aYAAur3j-0.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GLlsB26aYAAur3j?format=jpg&name=4096x4096)
[![](img/thumb/GMTii53aAAAHUiB.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GMTii53aAAAHUiB?format=jpg&name=4096x4096)
[![](img/thumb/GNtCbtBbgAA3i4I.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GNtCbtBbgAA3i4I?format=jpg&name=4096x4096)
[![](img/thumb/GNrui09b0AAYuOt.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GNrui09b0AAYuOt?format=jpg&name=4096x4096)

细节：   

- 右下角的水珠逼真。细节如下：  
![](img/GMTii53aAAAHUiB-0.jpg) 
![](img/GMTii53aAAAHUiB-1.jpg) 
![](img/GMTii53aAAAHUiB-2.jpg)   

- 皮鞋上的高光逼真。  
![](img/GNtCbtBbgAA3i4I-0.jpg)  


---

<a name="ch005"></a>  
###  CH 第6卷 第58话 (1986)  
![](img/not_given.jpg) 
[![](img/thumb/GaEOFx9b0AQNQFD-0.jpg "现场拍照@X")  
](https://pbs.twimg.com/media/GaEOFx9b0AQNQFD?format=jpg&name=4096x4096)

细节：   
(未找到足够大的图片以查看其细节。)  


---

<a name="ch006"></a>  
### CH 第6卷 第58话 (1986)  
![](img/not_given.jpg) 
[![](img/thumb/GN6e01uagAAI7S-1.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GN6e01uagAAI7S-?format=jpg&name=4096x4096) 

细节：   
(未找到足够大的图片以查看其细节。)  


---

<a name="ch007"></a>  
### CH 第7卷 第59话 (1986.03)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLubRMsbwAAczxE.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GLubRMsbwAAczxE?format=jpg&name=4096x4096) 
[![](img/thumb/GNq6lgJb0AAUzAw.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GNq6lgJb0AAUzAw?format=jpg&name=4096x4096) 

细节：   

- 黑色区域有明显涂抹的痕迹。且边线不直。  
![](img/GLubRMsbwAAczxE-0.jpg) 
![](img/GLubRMsbwAAczxE-1.jpg) 

- 头发区域有蓝色排线示意该区域为阴影。  
![](img/GNq6lgJb0AAUzAw-0.jpg) 

- 头发外围深浅不一。浅色区域可能是因为着色少，也可能是覆盖的网点纸；  
![](img/GNq6lgJb0AAUzAw-5.jpg) 

- 衣服纹理的墨色浅。  
![](img/GNq6lgJb0AAUzAw-1.jpg) 

- 用白色颜料修改边线。  
![](img/GNq6lgJb0AAUzAw-2.jpg) 

- 用白色颜料修改边特效线。  
![](img/GNq6lgJb0AAUzAw-3.jpg) 

- 被褥的纹理上有白色排线。  
![](img/GNq6lgJb0AAUzAw-4.jpg) 



---

<a name="ch008"></a>  
###  CH 第11卷 第102话 (1987.02)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLgp2Kzb0AAvGMD-0.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLgp2Kzb0AAvGMD?format=jpg&name=4096x4096)
[![](img/thumb/GMfJ5Y9aYAAxGA7.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLgp2Kzb0AAvGMD?format=jpg&name=4096x4096)

细节：   

- 线条细腻：  
![](img/GLgp2Kzb0AAvGMD-2.jpg)  

- 排线细腻：  
![](img/GLgp2Kzb0AAvGMD-3.jpg)  


---

<a name="ch009"></a>  
###  CH 第11卷 第102话 (1987.02)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLgp2Kzb0AAvGMD-1.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLgp2Kzb0AAvGMD?format=jpg&name=4096x4096) 
[![](img/thumb/439928144_462598799449966_5368214399722308066_n.jpg "现场拍照，左侧图@Instagram")
](https://www.instagram.com/explore/tags/北条司展) 

细节：   
(未找到足够大的图片以查看其细节。)  


---

<a name="ch010"></a>  
### CH 第12卷 第118话 (1987.06)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLrTOA4aMAApJlc-0.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096) 
[![](img/thumb/441875710_3663457003905036_6674379421201644752_n.jpg "现场拍照@facebook")
](https://www.facebook.com/photo/?fbid=3663457040571699&set=pcb.3663457097238360) 

细节：   

- 獠想象的画面，覆盖了网点纸，且线条颜色浅。  
![](img/441875710_3663457003905036_6674379421201644752_n-0.jpg) 

- 用蓝色线圈出阴影的区域。  
![](img/441875710_3663457003905036_6674379421201644752_n-1.jpg) 

---
<a name="ch011"></a>  
### CH 第12卷 第118话 (1987.06)  
![](img/not_given.jpg) 
[![](img/thumb/GLrTOA4aMAApJlc-1.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096)

细节：   
(未找到足够大的图片以查看其细节。)  


---
<a name="ch012"></a>  
###  CH 第13卷 第127话 (1987.08)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLrTOA4aMAApJlc-2.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096) 
[![](img/thumb/GMHrB48bgAA-Quv.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMHrB48bgAA-Quv?format=jpg&name=4096x4096) 
[![](img/thumb/20240503101034.jpg "现场拍照@hatenablog")
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503101034.jpg) 
[![](img/thumb/441415948_3663457010571702_5340787680204487064_n.jpg "现场拍照@facebook")
](https://www.facebook.com/hashtag/北条司展) 
[![](img/thumb/GQ1u8Kya0AE1VtO.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQ1u8Kya0AE1VtO?format=jpg&name=4096x4096) 

细节：   
(未找到足够大的图片以查看其细节。)  

---
<a name="ch013"></a>  
###  CH 第13卷 第127话 (1987.08)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLrTOA4aMAApJlc-3.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096) 
[![](img/thumb/GMHrB48bgAA-Quv.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMHrB48bgAA-Quv?format=jpg&name=4096x4096) 
[![](img/thumb/20240503101034.jpg "现场拍照@hatenablog")
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503101034.jpg) 

细节：   

(未找到足够大的图片以查看其细节。)  

---
<a name="ch014"></a>  
### CH 插画 (1987.08)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLWG9R-aEAAYWqH.jpg "News@edition-88")
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info) 
[![](img/thumb/GNrui07bEAAHuP_.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNrui07bEAAHuP_?format=jpg&name=4096x4096)

细节：   

- 图中獠笑嘻嘻地说：“Ippatsuman”。这可能指：1982-1983年日本动漫电视连续剧《逆転！イッパツマン》。中译名为《逆轉‧一發超人》。  

- 腿部和臀部的曲线。  
![](img/GLWG9R-aEAAYWqH-0.jpg) 
![](img/GLWG9R-aEAAYWqH-1.jpg) 

- 头发的高光：  
![](img/GLWG9R-aEAAYWqH-2.jpg) 

- 褶皱：  
![](img/GLWG9R-aEAAYWqH-3.jpg) 
![](img/GLWG9R-aEAAYWqH-4.jpg) 

- 背景的云。  
![](img/GLWG9R-aEAAYWqH-5.jpg) 

- 白色颜料的修补。  
![](img/GNrui07bEAAHuP_-0.jpg) 
![](img/GNrui07bEAAHuP_-1.jpg) 

---
<a name="ch015"></a>  
###  CH 16卷扉绘 (1987.10)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/2_city_88graph_silver_2.jpg "News@edition-88")
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info) 
[![](img/thumb/GLlsB26aYAAur3j-1.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GLlsB26aYAAur3j?format=jpg&name=4096x4096) 
[![](img/thumb/GL1rXo-aYAAaCfr.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GLlsB26aYAAur3j?format=jpg&name=4096x4096) 
[![](img/thumb/GMLxp_kbIAA_-rW.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GMLxp_kbIAA_-rW?format=jpg&name=4096x4096) 
[![](img/thumb/20240503101539.jpg "现场拍摄@hatenablog")
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503101539.jpg) 
[![](img/thumb/GNtCbtBbgAA3i4I.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GNtCbtBbgAA3i4I?format=jpg&name=4096x4096) 
[![](img/thumb/GNspz_2bYAAnDnu.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GNspz_2bYAAnDnu?format=jpg&name=4096x4096) 

细节：   

- 背景灯光的光线。  
![](img/GMLxp_kbIAA_-rW-0.jpg) 
![](img/GMLxp_kbIAA_-rW-1.jpg) 

- 褶皱：  
![](img/GMLxp_kbIAA_-rW-2.jpg) 
![](img/GMLxp_kbIAA_-rW-3.jpg)  
![](img/GMLxp_kbIAA_-rW-6.jpg) 
![](img/GMLxp_kbIAA_-rW-6-1.jpg) 
![](img/GMLxp_kbIAA_-rW-7.jpg) 
![](img/GMLxp_kbIAA_-rW-7-1.jpg) 

- 背景人物不是复制粘贴的。因为即使是线条也有细微的差异。    
![](img/GMLxp_kbIAA_-rW-4.jpg) 
![](img/GMLxp_kbIAA_-rW-5.jpg) 


---
<a name="ch016"></a>  
###  CH 第17卷 第167话 (1988.05)  
![](img/not_given.jpg) 
[![](img/thumb/GNYFBRrbYAAy0Z4.jpg "现场拍照@X")  
](https://pbs.twimg.com/media/GNYFBRrbYAAy0Z4?format=jpg&name=4096x4096)

细节：   

- 背景建筑地简化。     
![](img/GNYFBRrbYAAy0Z4-0.jpg) 

- 黑色区域的着色并不均匀。  
![](img/GNYFBRrbYAAy0Z4-1.jpg) 
![](img/GNYFBRrbYAAy0Z4-2.jpg) 

---

(此后，作者创作了[《天使的礼物》](./details2.md#angelgift)。)  

---  

<a name="ch017"></a>  
### CH 18卷 183话 (1988.09)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GaUeNwtbUAAFfd9.jpg "现场拍照， 右侧图@twitter")
](https://pbs.twimg.com/media/GaUeNwtbUAAFfd9?format=jpg&name=4096x4096) 


细节：   

- glow效果的边缘涂有白色颜料。  
![](img/GaUeNwtbUAAFfd9-0.jpg) 
![](img/GaUeNwtbUAAFfd9-1.jpg) 


---
<a name="ch018"></a>  
### CH 18卷 183话 (1988.09)    
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GObhSsBbYAACjgI.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GObhSsBbYAACjgI?format=jpg&name=4096x4096) 
[![](img/thumb/GaUeNwtbUAAFfd9.jpg "现场拍照，左侧图@twitter")
](https://pbs.twimg.com/media/GaUeNwtbUAAFfd9?format=jpg&name=4096x4096) 


细节：   

- 发丝的细节。  
![](img/GObhSsBbYAACjgI-1.jpg) 
![](img/GObhSsBbYAACjgI-0.jpg) 

- 对话框文字块的一侧涂抹有白色颜料。这种情况很常见。    
![](img/GObhSsBbYAACjgI-2.jpg) 


---
<a name="ch019"></a>  
### CH 18卷 183话 (1988.09)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GOPtvcKbQAAKt2x.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GOPtvcKbQAAKt2x?format=jpg&name=4096x4096) 
[![](img/thumb/GObhSqvaMAAO4V4.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GObhSqvaMAAO4V4?format=jpg&name=4096x4096) 
[![](img/thumb/GaYo2Bea8AAvVSY.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GaYo2Bea8AAvVSY?format=jpg&name=4096x4096) 
[![](img/thumb/GaOGVhAbUAMhTLc.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GaOGVhAbUAMhTLc?format=jpg&name=4096x4096) 

细节：   

- 配图上的标记：  
![](img/GaOGVhAbUAMhTLc-0.jpg) 

- 左页面的右侧有手写文字。    
![](img/GOPtvcKbQAAKt2x-0.jpg) 

- 白颜料画雨线。  
![](img/GObhSqvaMAAO4V4-2.jpg) 
![](img/GObhSqvaMAAO4V4-3.jpg) 
![](img/GaYo2Bea8AAvVSY-1.jpg) 
![](img/GaYo2Bea8AAvVSY-1g.jpg) 

- 雨在头顶溅起的水花。  
![](img/GObhSqvaMAAO4V4-0.jpg) 
![](img/GObhSqvaMAAO4V4-1.jpg) 

- 血迹的墨色深。  
![](img/GaYo2Bea8AAvVSY-2.jpg) 
![](img/GaYo2Bea8AAvVSY-2g.jpg) 
![](img/GaYo2Bea8AAvVSY-3.jpg) 
![](img/GaYo2Bea8AAvVSY-3g.jpg) 

- 对话框里的文字是贴上去的。边缘一侧涂有白颜料。  
![](img/GaYo2Bea8AAvVSY-0.jpg) 
![](img/GaYo2Bea8AAvVSY-0g.jpg) 



---
<a name="ch020"></a>  
### CH 1988年少年Jump 47号封面 / 185话[^jump198847] (可能为1988.10)  
[^jump198847]: [Weekly Shonen Jump #1034 - No. 47, 1988 released by Shueisha on October 31, 1988](https://comicvine.gamespot.com/weekly-shonen-jump-1034-no-47-1988/4000-530886/) 

[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-05_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph-city9-05_4800x.jpg) 
[![](img/thumb/1_hojo-ten_88graph_80062c9b-1999-4174-b696-d31f53a61845_0.jpg "News@edition-88") 
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info)
[![](img/thumb/441264103_7336355099795202_5380821469102162257_n-1.jpg "现场拍照@facebook") 
](https://www.facebook.com/hashtag/北条司展)
[![](img/thumb/GNhgqi4bUAAj9ul.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GNhgqi4bUAAj9ul?format=jpg&name=4096x4096) 
[![](img/thumb/1040g008314cc8640me004a18f2vloj5o3bsv468!nd_dft_wlteh_webp_3.webp "现场拍照@XHS") 
](https://www.xiaohongshu.com/discovery/item/66777d24000000001c036982?xsec_token=CBHXy0bQRVdICoXMY3K4EnHFqYe_BXeCqyWHmYiXUbaRc=) 

细节：   

- 衣服的褶皱和高光逼真。  
![](img/441264103_7336355099795202_5380821469102162257_n-1-0.jpg) 
![](img/441264103_7336355099795202_5380821469102162257_n-1-1.jpg) 

- edition-88版本的细节。  
![](img/88graph-city9-05_4800x-0.jpg) 
![](img/88graph-city9-05_4800x-1.jpg)  
由此猜测，轮廓线条是印刷的，然后手工上色。  

- (edition-88版本)玻璃碎片。  
![](img/88graph-city9-08_4800x-3.jpg) 

- (edition-88版本)酒杯  
![](img/88graph-city9-08_4800x-4.jpg) 
![](img/88graph-city9-08_4800x-5.jpg) 

- (edition-88版本)盘子的倒影或阴影。  
![](img/88graph-city9-08_4800x-2.jpg)   

- 现场拍照（左一列）对比edition-88版本（左二列）。  
![](img/1040g008314cc8640me004a18f2vloj5o3bsv468!nd_dft_wlteh_webp_3-0.webp) 
![](img/88graph-city9-05_4800x-6.webp)  
![](img/1040g008314cc8640me004a18f2vloj5o3bsv468!nd_dft_wlteh_webp_3-0g.webp) 
![](img/88graph-city9-05_4800x-6g.webp)  
![](img/1040g008314cc8640me004a18f2vloj5o3bsv468!nd_dft_wlteh_webp_3-1.webp) 
![](img/88graph-city9-05_4800x-7.webp) 


---

(此后，作者创作了[《SPLASH! 3》](./details2.md#splash3)。)  


---
<a name="ch021"></a> 
###  CH 插画 (1988.12)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-02_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-02_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-03_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-03_4800x.jpg) 
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-04_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver1-04_4800x.jpg) 
[![](img/thumb/GLp9d3Ma4AE27ID-1.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLp9d3Ma4AE27ID?format=jpg&name=4096x4096) 
[![](img/thumb/GLg0WFubgAABDon.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLg0WFubgAABDon?format=jpg&name=4096x4096) 
[![](img/thumb/GLWHS2wbUAADreU.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLWHS2wbUAADreU?format=jpg&name=4096x4096) 
[![](img/thumb/GLlsB1ubQAA1-UX.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLlsB1ubQAA1-UX?format=jpg&name=4096x4096) 
[![](img/thumb/GMfJ5OZawAA1f_p.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMfJ5OZawAA1f_p?format=jpg&name=4096x4096) 
[![](img/thumb/GMj4fWkaYAA8oZU.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMj4fWkaYAA8oZU?format=jpg&name=4096x4096) 
[![](img/thumb/GMpBzHBb0AAz1kw.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMpBzHBb0AAz1kw?format=jpg&name=4096x4096) 
[![](img/thumb/GNsp0C1acAAI4dE.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNsp0C1acAAI4dE?format=jpg&name=4096x4096) 
[![](img/thumb/GN7bPSvbUAALaEq.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GN7bPSvbUAALaEq?format=jpg&name=4096x4096) 
[![](img/thumb/GP7wAzbbUAA6CKM.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GP7wAzbbUAA6CKM?format=jpg&name=4096x4096) 

细节：   

- 背景的灯光。  
![](img/GLg0WFubgAABDon-0.jpg) 
![](./img/88silver1-02_4800x-0.jpg) 
![](./img/88silver1-02_4800x-1.jpg)  
下图应该是电子广告板的效果。[疑问]这种效果是如何画出来的？或许，是贴的某种材质的纸？    
![](./img/88silver1-02_4800x-2.jpg) 
![](./img/88silver1-04_4800x-2.jpg) 
![](./img/88silver1-04_4800x-1.jpg) 

- 头发的细节。  
![](img/GMpBzHBb0AAz1kw-1.jpg) 
![](img/GN7bPSvbUAALaEq-8.jpg) 
![](img/GN7bPSvbUAALaEq-9.jpg) 

- 眼白上部有灰色阴影。这种画法显得更真实。    
![](img/GMpBzHBb0AAz1kw-6.jpg) 
![](img/GMpBzHBb0AAz1kw-7.jpg) 

- 眼部、嘴唇、指甲有化妆。  
![](img/GMpBzHBb0AAz1kw-6.jpg) 
![](img/GN7bPSvbUAALaEq-13.jpg) 
![](img/GN7bPSvbUAALaEq-12.jpg) 

- [edition-88商品展示图]((https://edition-88.com/products/cityhunter-88graph-silver1))中，香的嘴唇上有蓝色墨点。[GalleryZenon现场展品](https://pbs.twimg.com/media/GN7bPSvbUAALaEq?format=jpg&name=4096x4096)中没有。    
![](./img/88silver1-02_4800x-4.jpg) 
![](./img/88silver1-03_4800x-0.jpg) 
![](./img/88silver1-04_4800x-0.jpg) 
![](img/GN7bPSvbUAALaEq-13.jpg) 

- 香的鼻梁处的高光。  
![](img/GMpBzHBb0AAz1kw-0.jpg) 

- 服饰细节：  
![](img/GN7bPSvbUAALaEq-10.jpg) 
![](img/GN7bPSvbUAALaEq-11.jpg) 

- 枪械上的细节。  
![](img/GMpBzHBb0AAz1kw-3.jpg) 
![](img/GP7wAzbbUAA6CKM-0.jpg) 

- 褶皱的高光。降低gamma值后更易看出。  
![](img/GLg0WFubgAABDon-1.jpg) 
![](img/GLg0WFubgAABDon-2.jpg) 
![](img/GMpBzHBb0AAz1kw-5.jpg) 
![](img/GLg0WFubgAABDon-5.jpg) 
![](img/GLg0WFubgAABDon-3.jpg) 
![](img/GN7bPSvbUAALaEq-0.jpg) 
![](img/GN7bPSvbUAALaEq-1.jpg) 
![](img/GN7bPSvbUAALaEq-2.jpg) 
![](img/GN7bPSvbUAALaEq-3.jpg) 
![](img/GN7bPSvbUAALaEq-4.jpg) 
![](img/GN7bPSvbUAALaEq-5.jpg) 

- 褶皱线条：  
![](img/GN7bPSvbUAALaEq-6.jpg) 
![](img/GN7bPSvbUAALaEq-7.jpg) 

- 黑色轮廓线被颜料部分地遮盖。  
![](img/GMpBzHBb0AAz1kw-2.jpg) 

- 膝盖处的褶皱。  
![](img/GLg0WFubgAABDon-6.jpg) 

- 腿部的着色。  
![](img/GLg0WFubgAABDon-7.jpg) 

- 浅色区域能看到纸的横向纹理。    
![](./img/88silver1-02_4800x-3.jpg) 

- 金属上的高光。  
![](img/GLg0WFubgAABDon-4.jpg) 
![](img/GMpBzHBb0AAz1kw-4.jpg) 

---
<a name="ch022"></a> 
### CH 第19卷 第195话 (1989.01)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLvzDJybYAAokIu.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GLvzDJybYAAokIu?format=jpg&name=4096x4096) 

细节：   

- 头发的墨色深浅不一、粗细不一。  
![](img/GLvzDJybYAAokIu-0.jpg) 
![](img/GLvzDJybYAAokIu-1.jpg) 

- 排线阴影。  
![](img/GLvzDJybYAAokIu-2.jpg) 

- 表示脸颊泛红的排线。线条下部似乎更亮一些。这可能暗示线条处微微凹陷（灯光在上部），进而暗示作者画这些线条时比较用力。      
![](img/GLvzDJybYAAokIu-3.jpg) 
![](img/GLvzDJybYAAokIu-4.jpg) 

- 似乎用白色颜料遮盖了一条特效线条。  
![](img/GLvzDJybYAAokIu-5.jpg) 

- 深色衣服胸前的高光用排线。  
![](img/GLvzDJybYAAokIu-6.jpg) 

- 深色衣服的着色的笔迹。    
![](img/GLvzDJybYAAokIu-7.jpg) 

- 似乎是用白色遮盖了一些东西。  
![](img/GLvzDJybYAAokIu-8.jpg) 

- 用网点纸粘上的内容。  
![](img/GLvzDJybYAAokIu-11.jpg) 
![](img/GLvzDJybYAAokIu-10.jpg) 

- 画纸右下角有“KMK”字样。可能是画纸的品牌，例如[KMK Kent](https://www.artilleryphilippines.com/en/papers-sketchbooks/39622-muse-kmk-kent-block-paper-200g-a4-size.html)。       
![](img/GLvzDJybYAAokIu-9.jpg) 

---
<a name="ch023"></a> 
###  CH Volume 22 封面 (1989.04)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-02_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88silver2-02_4800x.jpg) 
[![](img/thumb/cityhunter1.jpg "「北条司展」 News@Coamix") 
](https://www.coamix.co.jp/topics/coamix_gallery_2403)
[![](img/thumb/2_city_88graph_silver_1.jpg "「北条司展」 News@edition-88") 
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info)
[![](img/thumb/ch-1.jpg "「北条司展」 @GalleryZenon") 
](https://gallery-zenon.jp/) 
[![](img/thumb/GLp9d3Ma4AE27ID-0.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLp9d3Ma4AE27ID?format=jpg&name=4096x4096) 
[![](img/thumb/GMrZ68aWoAATWrT.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMrZ68aWoAATWrT?format=jpg&name=4096x4096) 
[![](img/thumb/GMomNn7b0AAVzPp.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMomNn7b0AAVzPp?format=jpg&name=4096x4096) 

解说文字（因为照片不清晰，所以原文待校对）：  
>Q：「シティーハンクー」が2025年で40周年をひかえますが、ご感想などをお第かせくだきい。  
《城市猎人》到2025年将迎来40周年，请介绍一下您的感想。 

>私の中では読み切りのシティーハンクー(1983年)がスタート地点なので、実は2023年が40周年なんですよね（笑）。  
我的起点是单话故事《城市猎人》(1983年)，所以其实2023年是40周年(笑)。  

>今だにアニメなども物作してもらっていて...不思議な感覺ですよわ。  
现在也制作了动画等作品…真是不可思议。  

>今までも何度も言っていますが、私が漫画家になった頃、漫画は読み物ての文化だと考えもれていました。  
我之前也说过很多次，在我成为漫画家的时候，我遗漏了漫画是读物文化的想法。  

>後々殘るようなものでは無く、連載が終わったらアッという間に忘れさられるものだと。  
并不会留下想念，连载结束后就会立刻被遗忘。  

>そんな漫画が40年もたって、アニメ化されてお祝いもしてもみって、もらろんとても嬉しい事なのですが・・・  
那样的漫画过了40年，动画化庆祝也看了，当然是非常高兴的事…  

>本当に不思議だと感じています。  
我真的觉得很不可思议。  

细节：   

- 头发的细节。  
![](./img/88silver2-02_4800x-3.jpg) 
![](./img/88silver2-02_4800x-10.jpg) 
![](./img/88silver2-02_4800x-12.jpg) 

- 眼部细节。香的眼睛呈蓝色。獠的眼睛有一点高光，香的眼睛有两点高光。      
![](./img/88silver2-02_4800x-4.jpg) 
![](./img/88silver2-02_4800x-11.jpg) 
![](./img/88silver2-02_4800x-11gamma.jpg) 

- 衣服的褶皱。  
![](./img/88silver2-02_4800x-5.jpg) 
![](./img/88silver2-02_4800x-6.jpg) 
![](./img/88silver2-02_4800x-7.jpg) 
![](./img/88silver2-02_4800x-15.jpg) 
![](./img/88silver2-02_4800x-16.jpg)  
![](./img/88silver2-02_4800x-8.jpg) 
![](./img/88silver2-02_4800x-9.jpg) 

- 枪的细节。左轮处的高光可能暗示其中有子弹。  
![](./img/88silver2-02_4800x-13.jpg)  
金属着色逼真：  
![](./img/88silver2-02_4800x-14.jpg) 

- 脚和鞋的链接处。  
![](./img/88silver2-02_4800x-1.jpg) 
![](./img/88silver2-02_4800x-2.jpg) 

- 背景：  
![](./img/88silver2-02_4800x-0.jpg) 
![](./img/88silver2-03_4800x-0.jpg)  
星光中心、较大的白色斑点中心似乎呈银色，且凹陷。[疑问]这是如何画的？    
![](./img/88silver2-05_4800x-0.jpg) 
![](./img/88silver2-05_4800x-1.jpg) 
![](./img/88silver2-05_4800x-2.jpg) 
![](./img/88silver2-05_4800x-3.jpg) 


---
<a name="ch024"></a> 
### CH 21卷 第216话 (1989.05)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GPoc-cnaQAAtDL8.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPoc-cnaQAAtDL8?format=jpg&name=4096x4096)   

细节：   

- 拟声词上的网点纸痕迹。  
![](img/GPoc-cnaQAAtDL8-0.jpg) 

- 乌鸦的边线若隐若现。  
![](img/GPoc-cnaQAAtDL8-1.jpg) 
![](img/GPoc-cnaQAAtDL8-1g.jpg) 

- 白色圆点处、发丝和背景排线间的间隔处，白色颜料涂抹的痕迹。  
![](img/GPoc-cnaQAAtDL8-2.jpg) 
![](img/GPoc-cnaQAAtDL8-2g.jpg) 

- 前景人物和背景人物的间隔。  
![](img/GPoc-cnaQAAtDL8-3.jpg) 

- 蓝色标记阴影区域。  
![](img/GPoc-cnaQAAtDL8-4.jpg) 

- 页面右下角有一小块暗色区域。[疑问]这是什么？    
![](img/GPoc-cnaQAAtDL8-5.jpg) 


---
<a name="ch025"></a> 
### CH 21卷 第216话 (1989.05)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GPox99nbwAAueBt.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPox99nbwAAueBt?format=jpg&name=4096x4096) 
[![](img/thumb/GPoc-ckbQAA5ZqL.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPoc-ckbQAA5ZqL?format=jpg&name=4096x4096) 
[![](img/thumb/GQ_8SJya4AASkH4.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQ_8SJya4AASkH4?format=jpg&name=4096x4096) 

细节：   

- 前景的植被。  
![](img/GPox99nbwAAueBt-0.jpg) 
![](img/GPox99nbwAAueBt-0gamma.jpg) 

- CE咖啡屋的网点纸阴影的痕迹。  
![](img/GPox99nbwAAueBt-1.jpg) 

- 墨色笔迹。  
![](img/GPox99nbwAAueBt-1gamma.jpg) 

- 白色颜料修改。  
![](img/GPox99nbwAAueBt-2.jpg) 
![](img/GPox99nbwAAueBt-3.jpg) 
![](img/GPox99nbwAAueBt-3gamma.jpg)  
以下的凸起可能是因为白色颜料涂改(然后覆盖了网点纸):    
![](img/GPox99nbwAAueBt-7.jpg) 
![](img/GPox99nbwAAueBt-7gamma.jpg) 

- 角色头部的线条有白色颜料修改。  
![](img/GPox99nbwAAueBt-4.jpg) 

- 海怪的眼镜。  
![](img/GPox99nbwAAueBt-5.jpg) 
![](img/GPox99nbwAAueBt-5gamma.jpg) 

- 海怪的胡子。  
![](img/GPox99nbwAAueBt-6.jpg) 
![](img/GPox99nbwAAueBt-6gamma.jpg) 



---
<a name="ch026"></a> 
### CH 22卷 第221话 (1989.06)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GPoc-cka0AAnNAN.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPoc-cka0AAnNAN?format=jpg&name=4096x4096)   

细节：   

- 墨色不均匀的痕迹。  
![](img/GPoc-cka0AAnNAN-0.jpg) 
![](img/GPoc-cka0AAnNAN-1.jpg) 
![](img/GPoc-cka0AAnNAN-3.jpg) 

- 用边框将一个画面分为两个分格。几乎看不出修改的痕迹。    
![](img/GPoc-cka0AAnNAN-2.jpg) 

- 深色衣服的交叉排线着色。  
![](img/GPoc-cka0AAnNAN-7.jpg) 
![](img/GPoc-cka0AAnNAN-8.jpg) 

- 蓝色标记阴影区域。  
![](img/GPoc-cka0AAnNAN-6.jpg) 
![](img/GPoc-cka0AAnNAN-5.jpg) 
![](img/GPoc-cka0AAnNAN-4.jpg) 

---
<a name="ch027"></a> 
### CH 22卷 第221话 (1989.06)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GPoc-cnaMAAGP1A.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPoc-cnaMAAGP1A?format=jpg&name=4096x4096)   

细节：   

- 特效线条。  
![](img/GPoc-cnaMAAGP1A-0.jpg) 

- [疑问]这种特效光环是如何制作的？如果是手工画的话，工作量有些大。  
![](img/GPoc-cnaMAAGP1A-1.jpg) 
![](img/GPoc-cnaMAAGP1A-2.jpg) 
![](img/GPoc-cnaMAAGP1A-3.jpg) 

- 特效。头发的交叉排线着色。  
![](img/GPoc-cnaMAAGP1A-4.jpg)  
交叉排线边缘有白色颜料的排线：  
![](img/GPoc-cnaMAAGP1A-5.jpg) 
![](img/GPoc-cnaMAAGP1A-5g.jpg) 

- 网点纸的痕迹。  
![](img/GPoc-cnaMAAGP1A-6.jpg) 
![](img/GPoc-cnaMAAGP1A-7.jpg)  
用白色颜料涂抹网点纸的边缘：  
![](img/GPoc-cnaMAAGP1A-8.jpg) 

- 背景中的白色颜料。  
![](img/GPoc-cnaMAAGP1A-8.jpg) 
![](img/GPoc-cnaMAAGP1A-9.jpg) 
![](img/GPoc-cnaMAAGP1A-9g.jpg) 
![](img/GPoc-cnaMAAGP1A-10.jpg) 
![](img/GPoc-cnaMAAGP1A-10g.jpg) 

---
<a name="ch028"></a>  
###  CH 第23卷 第236话 (1989.10)  
![](img/not_given.jpg) 
[![](img/thumb/444952288_17969220500718230_1914369075704752665_n.jpg "现场拍照@Instagram")
](https://www.instagram.com/explore/tags/北条司展) 
[![](img/thumb/GQuaD0eaAAAYok0.jpg "现场拍照，下方@X")
](https://pbs.twimg.com/media/GQuaD0eaAAAYok0?format=jpg&name=4096x4096) 
[![](img/thumb/1040g008313f6tb3dg42043c4vm568hru133bhl8!nd_dft_wlteh_webp_3.webp "现场拍照，下方@X")
](https://www.xiaohongshu.com/discovery/item/66599f35000000000f00f6da?xsec_token=CBFzF7H7zDUMgUS59kiAND3ae7MW2vzfYnIM7ff80EQbg=) 
[![](img/thumb/GQqqWLEbAAAIhDY.jpg "现场拍照，右侧下方@X")
](https://pbs.twimg.com/media/GQqqWLEbAAAIhDY?format=jpg&name=4096x4096) 


细节：   

- 发丝的细节。  
![](img/444952288_17969220500718230_1914369075704752665_n-1.jpg) 
![](img/444952288_17969220500718230_1914369075704752665_n-2.jpg) 
![](img/444952288_17969220500718230_1914369075704752665_n-4.jpg) 

- 滑稽表情的面部的排线。  
![](img/444952288_17969220500718230_1914369075704752665_n-3.jpg) 

- 小圆圈内涂满了白色颜料。我猜，这是为了遮盖圆圈中的背景线条。之所以涂白色颜料，我猜，是因为这么做效率高。    
![](img/444952288_17969220500718230_1914369075704752665_n-0.jpg) 
![](img/444952288_17969220500718230_1914369075704752665_n-0gamma.jpg) 


---
<a name="ch029"></a>  
###  CH 23卷 238话 (1989.10)  
![](img/not_given.jpg) 
[![](img/thumb/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3.webp "现场拍照@X")
](https://www.xiaohongshu.com/discovery/item/665c4cb5000000001401878e?xsec_token=CBPNX0yvu4FellunCStle5NO2yHGufMtLVPilo8GUpmNc=) 
[![](img/thumb/GPd-5jkaYAAJvlH.jpg "现场拍照, 右侧图@X")
](https://pbs.twimg.com/media/GPd-5jkaYAAJvlH?format=jpg&name=4096x4096) 
[![](img/thumb/GQuaD0eaAAAYok0.jpg "现场拍照，上方右侧图@X")
](https://pbs.twimg.com/media/GQuaD0eaAAAYok0?format=jpg&name=4096x4096) 
[![](img/thumb/447917045_841280884715741_6212598045269112751_n.jpg "现场拍照，右侧图，https://www.instagram.com/p/C76dgwRSbsB/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447917045_841280884715741_6212598045269112751_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=105&_nc_ohc=FGeotA4Dp5QQ7kNvgF5hsKs&_nc_gid=f3f0c5bc6bdd4fb793fd0739acecbffa&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4NTE0Nzg1ODA3NjAzMjIyMw%3D%3D.3-ccb7-5&oh=00_AYB-0cYYDEJUg8qSjH-awMlB24ZUyMOhVpB30dbgjfaJmQ&oe=6727A44B&_nc_sid=10d13b) 
[![](img/thumb/GQqqWLEbAAAIhDY.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQqqWLEbAAAIhDY?format=jpg&name=4096x4096) 
[![](img/thumb/GQ_8SJ0bkAAOAic.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQ_8SJ0bkAAOAic?format=jpg&name=4096x4096) 
[![](img/thumb/1040g008313f6tb3dg42043c4vm568hru133bhl8!nd_dft_wlteh_webp_3.webp "现场拍照，上方右侧图@X")
](https://www.xiaohongshu.com/discovery/item/66599f35000000000f00f6da?xsec_token=CBFzF7H7zDUMgUS59kiAND3ae7MW2vzfYnIM7ff80EQbg=) 


细节：   

- 背景光线特效，用白颜料绘制。  
![](img/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3-0.webp) 
![](img/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3-0g.webp) 

- 背景特效，可以看到墨渍。  
![](img/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3-1.webp) 
![](img/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3-2.webp) 

- 大吼的拟声词上有白色墨点。  
![](img/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3-5.webp) 

- 褶皱线条流畅。  
![](img/GQ_8SJ0bkAAOAic-0.jpg) 
![](img/GQ_8SJ0bkAAOAic-1.jpg) 

- 棉拖鞋的着色有凹凸不平的质感。    
![](img/GQ_8SJ0bkAAOAic-2.jpg) 

- 人物在地面的影子，可以看到墨渍。  
![](img/GQ_8SJ0bkAAOAic-3.jpg) 
![](img/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3-3.webp) 
![](img/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3-4.webp) 

- 不同的地板，颜色稍有不同。远处的地板颜色稍浅。    
![](img/GQ_8SJ0bkAAOAic-4.jpg)  


---
<a name="ch029.5"></a>  
###  CH 23卷 238话 (1989.10)  
![](img/not_given.jpg) 
[![](img/thumb/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3.webp "现场拍照@X")
](https://www.xiaohongshu.com/discovery/item/665c4cb5000000001401878e?xsec_token=CBPNX0yvu4FellunCStle5NO2yHGufMtLVPilo8GUpmNc=) 
[![](img/thumb/GPd-5jkaYAAJvlH.jpg "现场拍照，左侧图@X")
](https://pbs.twimg.com/media/GPd-5jkaYAAJvlH?format=jpg&name=4096x4096) 
[![](img/thumb/GQuaD0eaAAAYok0.jpg "现场拍照，上左侧图方@X")
](https://pbs.twimg.com/media/GQuaD0eaAAAYok0?format=jpg&name=4096x4096) 
[![](img/thumb/447917045_841280884715741_6212598045269112751_n.jpg "现场拍照，左侧图，https://www.instagram.com/p/C76dgwRSbsB/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447917045_841280884715741_6212598045269112751_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=105&_nc_ohc=FGeotA4Dp5QQ7kNvgF5hsKs&_nc_gid=f3f0c5bc6bdd4fb793fd0739acecbffa&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4NTE0Nzg1ODA3NjAzMjIyMw%3D%3D.3-ccb7-5&oh=00_AYB-0cYYDEJUg8qSjH-awMlB24ZUyMOhVpB30dbgjfaJmQ&oe=6727A44B&_nc_sid=10d13b) 
[![](img/thumb/GQqqWLEbAAAIhDY.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQqqWLEbAAAIhDY?format=jpg&name=4096x4096) 
[![](img/thumb/1040g008313f6tb3dg42043c4vm568hru133bhl8!nd_dft_wlteh_webp_3.webp "现场拍照，上方左侧图@X")
](https://www.xiaohongshu.com/discovery/item/66599f35000000000f00f6da?xsec_token=CBFzF7H7zDUMgUS59kiAND3ae7MW2vzfYnIM7ff80EQbg=) 


细节：   

- 背景特效的笔触。  
![](img/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3-0.webp) 
![](img/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3-1.webp) 

- 书的颜色、深浅错落有致。有种美感。    
![](img/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3-2.webp)  
![](img/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3-3.webp)  
![](img/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3-4.webp)  

- 双色绘制时，阴影不使用网点纸。    
![](img/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3-5.webp) 
![](img/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3-6.webp) 

- 表示动作的特效线条。  
![](img/GPd-5jkaYAAJvlH-0.jpg) 
![](img/GPd-5jkaYAAJvlH-0g.jpg) 


---
<a name="ch030"></a> 
###  CH 24卷 第240话 (1989.11)  
![](img/not_given.jpg) 
[![](img/thumb/441567956_17969220527718230_5452684696600726760_n.jpg "现场拍照@Instagram")  
](https://www.instagram.com/explore/tags/北条司展)

细节：   

- 背景里昏黄的灯光。  
![](img/441567956_17969220527718230_5452684696600726760_n-0.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-0gamma.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-1.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-1gamma.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-2.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-2gamma.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-3.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-3gamma.jpg)  
因为是黑白画面，所以灯光中心为白色。当灯光的光斑较小时，光斑中心涂抹白色颜料；当灯光的光斑较大时，我猜，挖空光斑中心（如[Taxi Driver中的某图](#taxi-driver_light-hole)所示）。我猜，这样做的原因是为了提高工作效率。  

- 背景天空的纹理上似乎有蓝色笔迹(标示阴影区域)。    
![](img/441567956_17969220527718230_5452684696600726760_n-4.jpg) 

- 女性角色的睫毛被简化为一根。  
![](img/441567956_17969220527718230_5452684696600726760_n-13.jpg) 

- 衣服上的高光。  
![](img/441567956_17969220527718230_5452684696600726760_n-5.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-5gamma.jpg) 

- 角色外边缘有白色排线。[疑问]这种白色排线是表示什么意思？    
![](img/441567956_17969220527718230_5452684696600726760_n-6.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-6gamma.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-7.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-8.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-8gamma.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-9.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-9gamma.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-10.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-10gamma.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-11.jpg) 
![](img/441567956_17969220527718230_5452684696600726760_n-11gamma.jpg) 

- 背景植被的排线阴影。  
![](img/441567956_17969220527718230_5452684696600726760_n-12.jpg) 

---

(此后，作者创作了[《TAXI DRIVER》](./details2.md#taxidriver)。)  


---
<a name="ch031"></a> 
###  CH 庆祝250话 1990年 少年Jump 16号封面 (可能为1990.02)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMPeKMraYAAyNfr.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GMPeKMraYAAyNfr?format=jpg&name=4096x4096) 
[![](img/thumb/GNw_zyMbYAAYnTQ.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GNw_zyMbYAAYnTQ?format=jpg&name=4096x4096) 
[![](img/thumb/GapiNseaoAEOuH2.jpg "福冈展，现场拍摄@X")
](https://pbs.twimg.com/media/GapiNseaoAEOuH2?format=jpg&name=4096x4096) 

细节：   

- 相比东京展，福冈展上的该展品能看到其四周更大区域。例如：  
    - 相比东京展（左一），福冈展上能看到其下部更大的区域（左二）；  
    ![](img/GMPeKMraYAAyNfr-0.jpg) 
![](img/GapiNseaoAEOuH2-0.jpg) 
    - 福冈展上能看到其右上角有红色手写文字，疑似"カット A"，直译应该为"Cut A"；  
    ![](img/GapiNseaoAEOuH2-1.jpg) 

- 衣服的半透明效果：  
![](img/GNw_zyMbYAAYnTQ-0.jpg) 
![](img/GNw_zyMbYAAYnTQ-1.jpg) 
![](img/GNw_zyMbYAAYnTQ-2.jpg) 

- 金属光泽：  
![](img/GNw_zyMbYAAYnTQ-3.jpg)  
![](img/GNw_zyMbYAAYnTQ-4.jpg) 
![](img/GNw_zyMbYAAYnTQ-5.jpg) 

- 拐角处的着色。  
![](img/GNw_zyMbYAAYnTQ-6.jpg) 

- 沙发的线条不平滑。    
![](img/GNw_zyMbYAAYnTQ-7.jpg) 


---
<a name="ch032"></a> 
### CH 26卷 265话 (1990.05-06)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GO3V9yUa8AACJeI.jpg "现场拍照, 左图@twitter")
](https://pbs.twimg.com/media/GO3V9yUa8AACJeI?format=jpg&name=4096x4096) 
[![](img/thumb/GPhsRg0aQAAwPDm.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPhsRg0aQAAwPDm?format=jpg&name=4096x4096) 
[![](img/thumb/GPr3XFPbAAEudoW.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPr3XFPbAAEudoW?format=jpg&name=4096x4096) 
[![](img/thumb/447202956_1128669671523801_474780129820858108_n.jpg "现场拍照@https://www.instagram.com/p/C76dgwRSbsB/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447202956_1128669671523801_474780129820858108_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDA3eDEwMDcuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=109&_nc_ohc=T5LRMsiP3UAQ7kNvgG24i9a&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzE2OTA1NjYxMw%3D%3D.3-ccb7-5&oh=00_AYD538ImontPyBAHy9qaWAk8h3wC1Rtfd_SFmEXEynN1rw&oe=67278713&_nc_sid=10d13b) 
[![](img/thumb/447108693_1513897612856601_1090414155099548340_n.jpg "现场拍照@https://www.instagram.com/p/C76dgwRSbsB/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447108693_1513897612856601_1090414155099548340_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=109&_nc_ohc=tT7kd6B8BCQQ7kNvgGVjuqu&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzI2MTM1NDM4Ng%3D%3D.3-ccb7-5&oh=00_AYBQIyJMn9ysrln0jEIhAvhgiIVzDJPeOOq5TMQm_i3_cA&oe=6727754B&_nc_sid=10d13b) 

细节：   

- 头发着色细节。  
![](img/GO3V9yUa8AACJeI-0.jpg) 
![](img/GO3V9yUa8AACJeI-1.jpg) 

- 头发细节。  
![](img/GO3V9yUa8AACJeI-4.jpg) 
![](img/GO3V9yUa8AACJeI-5.jpg) 
![](img/GO3V9yUa8AACJeI-2.jpg) 
![](img/GO3V9yUa8AACJeI-3.jpg) 

- 眼神。  
![](img/GO3V9yUa8AACJeI-5.jpg) 
![](img/GO3V9yUa8AACJeI-6.jpg) 

- 面部细节。  
![](img/GO3V9yUa8AACJeI-7.jpg) 
![](img/GO3V9yUa8AACJeI-8.jpg) 

- 排线阴影。  
![](img/GO3V9yUa8AACJeI-9.jpg) 
![](img/GO3V9yUa8AACJeI-10.jpg) 

- 胸前深色衣服的着色有pattern。  
![](img/GPr3XFPbAAEudoW-0.jpg) 
![](img/GPr3XFPbAAEudoW-0g.jpg) 

- 台词的纸色有白、有黄。  
![](img/GPr3XFPbAAEudoW-1.jpg) 
![](img/GPr3XFPbAAEudoW-2.jpg) 
![](img/GPr3XFPbAAEudoW-3.jpg) 


---
<a name="ch033"></a> 
### CH 26卷 265话 (1990.05~06)    
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GQO5ak5bIAAJ1Q4.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQO5ak5bIAAJ1Q4?format=jpg&name=4096x4096) 
[![](img/thumb/GPr3XDfa8AEZrdm.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPr3XDfa8AEZrdm?format=jpg&name=4096x4096) 
[![](img/thumb/447253042_1450705312481090_6103582543695686744_n.jpg "现场拍照@https://www.instagram.com/p/C76dgwRSbsB/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447253042_1450705312481090_6103582543695686744_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDUweDEwNDkuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=102&_nc_ohc=SAph72sJ-xMQ7kNvgElWp7P&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzE2ODkzMzk1MQ%3D%3D.3-ccb7-5&oh=00_AYBaxyw5GTGXAmemgLJACOyVipXwHRvvcpToFnHU7oeSVA&oe=67277B4E&_nc_sid=10d13b) 
[![](img/thumb/447197235_828063905376636_1750104749879296705_n.jpg "现场拍照@https://www.instagram.com/p/C76dgwRSbsB/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447197235_828063905376636_1750104749879296705_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi45ODZ4OTg2LnNkci5mMjkzNTAuZGVmYXVsdF9pbWFnZSJ9&_nc_ht=scontent.cdninstagram.com&_nc_cat=100&_nc_ohc=DbXF1B4PENYQ7kNvgHiRcNE&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzI2OTcwNzI4Mw%3D%3D.3-ccb7-5&oh=00_AYBpqxjyOJGY5MhiKohdA_jVfoD7X4MhqhR607aqkRoO9w&oe=6727A610&_nc_sid=10d13b)   

细节：   

- 头发。  
![](img/GQO5ak5bIAAJ1Q4-2.jpg)  
![](img/GQO5ak5bIAAJ1Q4-4.jpg) 
![](img/GQO5ak5bIAAJ1Q4-5.jpg)  
![](img/GQO5ak5bIAAJ1Q4-3.jpg)  

- 眼部。  
![](img/GQO5ak5bIAAJ1Q4-0.jpg)  
![](img/GQO5ak5bIAAJ1Q4-1.jpg) 

- 下颌处的排线阴影。  
![](img/GQO5ak5bIAAJ1Q4-6.jpg) 

- 衣服的褶皱排线。  
![](img/GQO5ak5bIAAJ1Q4-8.jpg) 
![](img/GQO5ak5bIAAJ1Q4-9.jpg) 

- 背景道具精致。  
![](img/GQO5ak5bIAAJ1Q4-10.jpg) 
![](img/GQO5ak5bIAAJ1Q4-11.jpg) 

- 背景中的云。用蓝色线条勾勒处轮廓，再贴上网点纸阴影。  
![](img/GQO5ak5bIAAJ1Q4-7.jpg) 

- 头发的着色。  
![](img/GPr3XDfa8AEZrdm-6.jpg) 
![](img/GPr3XDfa8AEZrdm-7.jpg)  
![](img/GPr3XDfa8AEZrdm-8.jpg)  
![](img/GPr3XDfa8AEZrdm-9.jpg) 

- 用网点纸着色。似乎有一个缺口。    
![](img/GPr3XDfa8AEZrdm-0.jpg) 

- 对白的修补痕迹。  
![](img/GPr3XDfa8AEZrdm-1.jpg) 

- 排线阴影。  
![](img/GPr3XDfa8AEZrdm-2.jpg) 
![](img/GPr3XDfa8AEZrdm-3.jpg) 
![](img/GPr3XDfa8AEZrdm-4.jpg) 
![](img/GPr3XDfa8AEZrdm-5.jpg) 


---

<a name="ch034"></a> 
### CH 1990单行本第27卷封面 (1990，具体月份未知)  
[![](./img/thumb/88graph1-04_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-04_4800x.jpg) 
[![](./img/thumb/88graph1-05_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph1-05_4800x.jpg) 
[![](img/thumb/GLqu-KkbIAA_VnS.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLqu-KkbIAA_VnS?format=jpg&name=4096x4096) 
[![](img/thumb/GLvzDJyasAAfyue.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLvzDJyasAAfyue?format=jpg&name=4096x4096) 
[![](img/thumb/GLvzDJ2b0AAsHIk.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLvzDJ2b0AAsHIk?format=jpg&name=4096x4096) 
[![](img/thumb/GMLxqnYbsAEMXsY.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMLxqnYbsAEMXsY?format=jpg&name=4096x4096) 
[![](img/thumb/GMTii7racAAd4LU.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMTii7racAAd4LU?format=jpg&name=4096x4096) 
[![](img/thumb/GMfPs-ab0AAjU8J.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMfPs-ab0AAjU8J?format=jpg&name=4096x4096)
[![](img/thumb/GMe-S39bEAEyJnC.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMe-S39bEAEyJnC?format=jpg&name=4096x4096) 
[![](img/thumb/GMjwHFnbsAArCHX.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMjwHFnbsAArCHX?format=jpg&name=4096x4096) 
[![](img/thumb/GM1PdXKbIAAMPbl.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GM1PdXKbIAAMPbl?format=jpg&name=4096x4096) 
[![](img/thumb/GNBdNdHawAAeNhn.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNBdNdHawAAeNhn?format=jpg&name=4096x4096) 

解说文字：  
>Q:「シティーハンクー」の獠と香を理想のカップルと呼ぶファンも多くおりますが、二人の関係性は当初からの狙いでしたでしようか。  
很多粉丝将《城市猎人》中的獠和香的关系称为理想的情侣，但他们的关系从一开始就是既定目标吗？  

>当初の構想には無かったですわ。  
它不在最初的构思中。  
長期連載の流れの中で、作者の想像をこえた関係性が築かれていきました。  
在长期连载的过程中，建立了一种超出作者想象的关系。  
...理想ですかわ？私はそもそも冴羽獠になりたくないんですよね(笑)。  
... 理想吗？ 我一开始就不想当冴羽獠（笑）。  
だってあんな危険な仕事だし、大きなハンマーでも殴られたくないですよ(笑)。  
因为这是一份危险的工作，我可不想被人用大锤子砸（笑）。  

细节：   

- 背景里的高光、大楼里的灯源。    
![](img/GLqu-KkbIAA_VnS-0.jpg) 
![](img/88graph1-02_4800x-0.jpg) 
![](img/GLqu-KkbIAA_VnS-1.jpg) 
![](img/GLqu-KkbIAA_VnS-2.jpg) 
![](img/GLqu-KkbIAA_VnS-3.jpg) 
![](img/GLvzDJyasAAfyue-0.jpg) 
![](img/GLvzDJyasAAfyue-1.jpg) 
![](img/88graph1-02_4800x-1.jpg) 

- 一处细节。  
![](img/GLvzDJ2b0AAsHIk-0.jpg) 
![](img/GLvzDJ2b0AAsHIk-1.jpg) 

- 獠的头发的高光和着色  
![](img/GLvzDJyasAAfyue-2.jpg) 
![](img/GLvzDJyasAAfyue-3.jpg)  
![](img/GLvzDJyasAAfyue-4.jpg) 
![](img/GLvzDJyasAAfyue-5.jpg) 

- 有零星的白色墨点。降低gamma值后（左二列）更易看出。这可能是作画时无意溅洒的。    
![](img/88graph1-06_4800x-0.jpg) 
![](img/88graph1-06_4800x-0gamma.jpg)  
![](img/88graph1-06_4800x-1.jpg) 
![](img/88graph1-06_4800x-1gamma.jpg)  
![](img/88graph1-05_4800x-0.jpg) 
![](img/88graph1-05_4800x-0gamma.jpg)  

- 獠的眼神。  
![](img/88graph1-04_4800x-0.jpg) 
![](img/88graph1-04_4800x-1.jpg) 

- 每个眼睛的两处高光，只有右下处的高光点了白色颜料。    
![](img/GLvzDJyasAAfyue-6.jpg) 
![](img/GLvzDJyasAAfyue-7.jpg)  
下图每个眼睛的三处高光，只有2处点了高光。  
![](img/GLvzDJ2b0AAsHIk-2.jpg) 
![](img/GLvzDJ2b0AAsHIk-4.jpg) 

- 嘴唇上淡淡的高光。  
![](img/GLvzDJ2b0AAsHIk-5.jpg) 

- 褶皱：  
![](img/GLqu-KkbIAA_VnS-4.jpg) 
![](img/GLqu-KkbIAA_VnS-5.jpg) 
![](img/GLqu-KkbIAA_VnS-6.jpg) 
![](img/GLvzDJ2b0AAsHIk-6.jpg) 
![](img/GLvzDJ2b0AAsHIk-7.jpg) 

- 褶皱和高光：  
![](img/GLqu-KkbIAA_VnS-7.jpg) 
![](img/GLqu-KkbIAA_VnS-8.jpg) 

- 胸前衣服的着色：  
![](img/88graph1-02_4800x-3.jpg) 
![](img/88graph1-02_4800x-2.jpg) 


- 腋窝处可能是排线阴影。  
![](img/GLvzDJyasAAfyue-11.jpg) 

- 手部姿态：  
![](img/GLqu-KkbIAA_VnS-11.jpg) 
![](img/GLqu-KkbIAA_VnS-10.jpg) 
![](img/GLvzDJyasAAfyue-12.jpg) 

- 香烟和烟雾的细节。  
![](img/GLvzDJyasAAfyue-10.jpg) 
![](img/GLvzDJyasAAfyue-9.jpg) 
![](img/GLvzDJyasAAfyue-8.jpg) 

- 紫色的过渡。  
![](img/GLqu-KkbIAA_VnS-9.jpg) 



---
<a name="ch035"></a> 
### CH 插画 (1990.12)  
[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-04_100x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-04_4800x.jpg) 
[![](img/thumb/441264103_7336355099795202_5380821469102162257_n-0.jpg "现场拍照@facebook")
](https://www.facebook.com/hashtag/北条司展) 
[![](img/thumb/439175109_963174088243816_7910888953370963938_n.jpg "现场拍照@instagram")
](https://www.instagram.com/explore/tags/北条司展) 
[![](img/thumb/GQMbLBAbwAA6EaR.jpg "现场拍照@instagram")
](https://pbs.twimg.com/media/GQMbLBAbwAA6EaR?format=jpg&name=4096x4096) 
[![](img/thumb/GLXd5Eda0AAU9J6.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLXd5Eda0AAU9J6?format=jpg&name=4096x4096) 
[![](img/thumb/20240503100502.jpg "现场拍照@hatenablog")
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503100502.jpg) 
[![](img/thumb/GaO5cSCacAAwiTw.jpg "现场拍照@hatenablog")
](https://pbs.twimg.com/media/GaO5cSCacAAwiTw?format=jpg&name=4096x4096) 
[![](img/thumb/1040g008313f6tb3dg41043c4vm568hru5htc49o!nd_dft_wlteh_webp_3.webp "现场拍照@hatenablog")
](https://www.xiaohongshu.com/discovery/item/66599f35000000000f00f6da?xsec_token=CBFzF7H7zDUMgUS59kiAND3ae7MW2vzfYnIM7ff80EQbg=) 

解说文字：  
> Q:「シティーハンター」の中で、好きなキャラクターは誰でしょう。  
问：在「CITYHUNTER」中，喜欢哪个角色？  

> 描いていて樂しかったのは海坊主（ファルコン）です。なんか何やってもおかしいじぐないてすか（笑）。  
海坊主は元々ー回限りのキャラクターだったんですが、アニメのスタァフが気に人っていると聞いて、試しにもう一度漫画に出してみたらやつばり面白くて。アニメが魅力に気づせてくれたキャラクターですな。  
画的很开心的时候是海坊主（Falcon）。不管做什么都很奇怪吧（笑）。  
海坊主本来只是个一回限定的角色，听说他动画里的样子很受欢迎，就试着再出一次漫画，果然很有趣。动画中注意到魅力的角色呢。  

细节：   

- 前景人物的部分边缘线条粗，可能是为了突出前景人物。  
![](img/88graph2-02_b92ebab8-0cde-4020-a3e8-b4b59d5eb0d0_4800x-2.jpg) 
![](img/88graph2-02_b92ebab8-0cde-4020-a3e8-b4b59d5eb0d0_4800x-1.jpg) 
![](img/88graph2-02_b92ebab8-0cde-4020-a3e8-b4b59d5eb0d0_4800x-0.jpg) 

- 作者绘画的一个特点是：（几乎）只画鼻梁右侧边线。  
![](img/88graph2-03_4800x-0.jpg) 

- 线条细节：  
![](img/88graph2-04_4800x-0.jpg) 
![](img/88graph2-04_4800x-1.jpg) 
![](img/88graph2-04_4800x-2.jpg)  

- 食指处的线条细节：  
![](img/88graph2-05_4800x-0.jpg) 
![](img/88graph2-05_4800x-1.jpg)  
![](img/88graph2-05_4800x-2.jpg) 
![](img/88graph2-05_4800x-3.jpg)  
食指呈双边线可能是为了表示此处是"透过枪火后呈虚线"效果：  
![](img/88graph2-03_4800x-3.jpg) 

- 枪火边缘的效果：  
![](img/88graph2-03_4800x-2.jpg) 

- 局部对比。  [88graph2-03](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-03_4800x.jpg)中的局部图如下：   
![](img/88graph2-03_4800x-5.jpg) 
![](img/88graph2-03_4800x-4.jpg)  
[88graph2-06](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/88graph2-06_4800x.jpg)中的局部图如下。[疑问]黑色边线变细可能是因为亮度提高。    
![](img/88graph2-06_4800x-1.jpg) 
![](img/88graph2-06_4800x-0.jpg)  

- 服饰着色。  
![](img/88graph2-03_4800x-1.jpg) 



---
<a name="ch036"></a> 
### CH 周刊少年Jump 1991年新年3-4合并号 / 292话[^jump1991-3-4]  (可能为1990年底[^jump1991-3-4])  
[^jump1991-3-4]: [Weekly Shonen Jump #1141 - No. 3-4, 1991 released by Shueisha on January 8, 1991.](https://comicvine.gamespot.com/weekly-shonen-jump-1141-no-3-4-1991/4000-541538/)

![](img/not_given.jpg "官方照片") 
[![](img/thumb/6_hojo-ten_88graphlight_0.jpg "News@edition-88")
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info) 
[![](img/thumb/GQHWDU7b0AABtrt.jpg "现场拍摄@X")
](https://pbs.twimg.com/media/GQHWDU7b0AABtrt?format=jpg&name=4096x4096) 
[![](img/thumb/GQCqdmFbEAMB4LK.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQCqdmFbEAMB4LK?format=jpg&name=4096x4096) 
[![](img/thumb/picture_pc_4dc7df3513996bd1de2ce11792b9ca97.webp "现场拍照@note.com")
](https://note.com/naofumitasaki/n/nc4c8852e3788) 
[![](img/thumb/448955048_7745951785525791_4938777759867780664_n.jpg "现场拍照@facebook")
](https://www.facebook.com/hashtag/北条司展) 
[![](img/thumb/449102935_993496752037726_7250011905259076459_n.jpg "现场拍照@https://www.instagram.com/p/C8l86ZuyVbE/?img_index=3")
](https://scontent.cdninstagram.com/v/t51.29350-15/449102935_993496752037726_7250011905259076459_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=110&_nc_ohc=Q0amiG7zFiwQ7kNvgEA6XrF&_nc_gid=4a64154189ff4fc4979990c97fd63e7c&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM5NzM4OTM3OTE0NzkyMzE5Ng%3D%3D.3-ccb7-5&oh=00_AYCzBETMuJpArvqboCTsmOFm5-quDtON2QSupKv-vYWkuA&oe=672780A5&_nc_sid=10d13b) 


细节：   

- 可能是纸张的质感。  
![](img/GQCqdmFbEAMB4LK-0.jpg)  

- 眼神。  
![](./img/449102935_993496752037726_7250011905259076459_n0.jpg) 
![](./img/449102935_993496752037726_7250011905259076459_n1.jpg) 

- 手部。  
![](./img/449102935_993496752037726_7250011905259076459_n2.jpg) 
![](./img/449102935_993496752037726_7250011905259076459_n3.jpg) 
![](./img/449102935_993496752037726_7250011905259076459_n4.jpg) 
![](./img/449102935_993496752037726_7250011905259076459_n5.jpg) 

- 衣服褶皱的高光。  
![](./img/449102935_993496752037726_7250011905259076459_n6.jpg) 
![](./img/449102935_993496752037726_7250011905259076459_n7.jpg) 


---
<a name="ch037"></a> 
### CH 29卷 300话 (1991.02)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/2024-05-29-13.01.33.jpg "现场拍照@seidoku.jp")
](https://seidoku.jp/wp-content/uploads/2024/05/2024-05-29-13.01.33.jpg) 
[![](img/thumb/GPr3XM8b0AAQmt7.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPr3XM8b0AAQmt7?format=jpg&name=4096x4096) 
[![](img/thumb/GQrHhyDbYAA6Jtw.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQrHhyDbYAA6Jtw?format=jpg&name=4096x4096) 
[![](img/thumb/GQwUvXybYAASnKR.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQwUvXybYAASnKR?format=jpg&name=4096x4096) 

细节：   

- 边缘未修改整齐，因为这里在印刷时被裁剪掉。  
![](./img/2024-05-29-13.01.33-0.jpg)  
![](./img/2024-05-29-13.01.33-1.jpg) 

- 枪械着色逼真。  
![](./img/2024-05-29-13.01.33-2.jpg) 
![](./img/2024-05-29-13.01.33-5.jpg)  
可看到枪膛里的螺纹！  
![](./img/2024-05-29-13.01.33-3.jpg) 
![](./img/2024-05-29-13.01.33-6.jpg) 

- 背景特效线条细腻。  
![](./img/2024-05-29-13.01.33-4.jpg)  
背景黑色有涂抹的痕迹：  
![](./img/GPr3XM8b0AAQmt7-0.jpg) 

- 手掌和枪的透视不一致。  
![](./img/2024-05-29-13.01.33-7.jpg) 

- 服饰上的高光。  
![](./img/2024-05-29-13.01.33-8.jpg) 
![](./img/2024-05-29-13.01.33-8gamma.jpg) 
![](./img/2024-05-29-13.01.33-9.jpg) 
![](./img/2024-05-29-13.01.33-10.jpg) 

- 背景中的铁丝网细腻。  
![](./img/2024-05-29-13.01.33-11.jpg) 
![](./img/2024-05-29-13.01.33-12.jpg) 

- 头发的细节。  
![](img/GQwUvXybYAASnKR-0.jpg) 
![](img/GQwUvXybYAASnKR-1.jpg) 

- 眼部。  
![](img/GQwUvXybYAASnKR-2.jpg)  
![](img/GQwUvXybYAASnKR-3.jpg) 

- 鼻梁一侧的排线。  
![](img/GQwUvXybYAASnKR-4.jpg) 
![](img/GQwUvXybYAASnKR-5.jpg) 

- 下颌处的排线。  
![](img/GQwUvXybYAASnKR-6.jpg) 

- 网点纸的点阵不是圆形。  
![](img/GQwUvXybYAASnKR-7.jpg) 

---
<a name="ch038"></a> 
### CH 29卷 第300话 (1991.02)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GOJnVhfaEAAt7u1.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GOJnVhfaEAAt7u1?format=jpg&name=4096x4096) 
[![](img/thumb/GOvBCNwa4AE1RPi.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GOvBCNwa4AE1RPi?format=jpg&name=4096x4096) 
[![](img/thumb/2024-05-29-13.01.37.jpg "现场拍照@seidoku.jp")
](https://seidoku.jp/wp-content/uploads/2024/05/2024-05-29-13.01.37.jpg) 
[![](img/thumb/GPBuBtrboAATbMY.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPBuBtrboAATbMY?format=jpg&name=4096x4096) 
[![](img/thumb/GPr3XM6aoAAxtm9.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPr3XM6aoAAxtm9?format=jpg&name=4096x4096) 
[![](img/thumb/GQrHhyDbYAA6Jtw.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQrHhyDbYAA6Jtw?format=jpg&name=4096x4096) 
[![](img/thumb/GQwUvXva4AAHCq8.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQwUvXva4AAHCq8?format=jpg&name=4096x4096) 
[![](img/thumb/464263417_1296064014884678_1899232942009007124_n.jpg "现场拍照@https://www.instagram.com/p/DBd2-NVzGtr/")
](https://scontent.cdninstagram.com/v/t51.29350-15/464263417_1296064014884678_1899232942009007124_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=102&_nc_ohc=PlMoXOFLf5AQ7kNvgHQ9yCJ&_nc_gid=e429b55fec704960a207eb04edf8369d&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NTE4MzQzODM1NjM4NTk4Mg%3D%3D.3-ccb7-5&oh=00_AYAIKFnw0c4vBIA3yrwsHK65niwzrPPYT-lYRLKvxByqFA&oe=6727A963&_nc_sid=10d13b) 

细节：   

- 面部大量排线阴影。  
![](img/GOJnVhfaEAAt7u1-0.jpg) 

- 画面边缘的发丝。  
![](img/GOJnVhfaEAAt7u1-1.jpg) 
![](img/GOJnVhfaEAAt7u1-2.jpg) 

- 网点纸的边缘。有些边缘不可见，有些边缘清晰可见。  
![](img/GOJnVhfaEAAt7u1-3.jpg) 
![](img/GOJnVhfaEAAt7u1-4.jpg) 

- 深灰色网点纸的边缘（图片中央）。  
![](img/GOJnVhfaEAAt7u1-5.jpg) 

- 枪械着色逼真。  
![](img/GOJnVhfaEAAt7u1-6.jpg) 
![](img/GOJnVhfaEAAt7u1-7.jpg) 
![](img/GOJnVhfaEAAt7u1-8.jpg)  
疑似使用了双层网点纸：  
![](img/GOJnVhfaEAAt7u1-7.jpg) 

- 头发细节：  
![](img/GOJnVhfaEAAt7u1-9.jpg) 
![](img/GQwUvXva4AAHCq8-0.jpg) 
![](img/GQwUvXva4AAHCq8-1.jpg) 

- （面部使用了大面积网点纸）网点纸的边缘：  
![](img/GOJnVhfaEAAt7u1-10.jpg) 
![](img/GOJnVhfaEAAt7u1-10gamma.jpg)  

- （面部使用了大面积网点纸）网点纸覆盖了部分头发：  
![](img/GOJnVhfaEAAt7u1-11.jpg) 
![](img/GOJnVhfaEAAt7u1-11gamma.jpg) 

- 眼部细节。  
![](img/GQwUvXva4AAHCq8-2.jpg) 
![](img/GQwUvXva4AAHCq8-3.jpg)  
![](img/GQwUvXva4AAHCq8-4.jpg)  
![](img/GQwUvXva4AAHCq8-5.jpg)  

- 嘴部细节。  
![](img/GQwUvXva4AAHCq8-6.jpg)  
![](img/GQwUvXva4AAHCq8-7.jpg) 

- 背景墨色不均匀。但似乎在最终印刷版里无此瑕疵。  
![](img/464263417_1296064014884678_1899232942009007124_n0.jpg)  




---
<a name="ch039"></a> 
### CH 第303话 (1991.02)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GPr2dMzawAAu50M.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPr2dMzawAAu50M?format=jpg&name=4096x4096) 
[![](img/thumb/GPd0D6qbYAACsEj.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPd0D6qbYAACsEj?format=jpg&name=4096x4096) 
[![](img/thumb/GPRL0V3bEAA-xBi.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPRL0V3bEAA-xBi?format=jpg&name=4096x4096) 
[![](img/thumb/GP9Emq5bcAEj6I-.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GP9Emq5bcAEj6I-?format=jpg&name=4096x4096) 
[![](img/thumb/GQVQ_vsaoAAQGly.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQVQ_vsaoAAQGly?format=jpg&name=4096x4096) 
[![](img/thumb/464207413_918403753499789_6460485170279673723_n.jpg "现场拍照@https://www.instagram.com/p/DBlCh2Vv9cY")
](https://scontent.cdninstagram.com/v/t51.29350-15/464675494_954008060100760_7738149828028631224_n.jpg?se=-1&stp=dst-jpegr_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMjE1eDE1MTguaGRyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=104&_nc_ohc=UqCunpXhJ6EQ7kNvgE5Nps8&_nc_gid=488452a054ca48f18b3e688e1c60338e&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NzIwNDU5NTc5MTI5MDcwNA%3D%3D.3-ccb7-5&oh=00_AYCOL-8JAlRgFPnJQtLbQ0_BuePNx2MAII4RnGLNGg_5CA&oe=67277994&_nc_sid=10d13b) 

细节：   

- 分格的边缘。之所以不修整，是因为在印刷时这里会被裁剪。    
![](img/GPr2dMzawAAu50M-0.jpg) 

- 头发。  
![](img/GPd0D6qbYAACsEj-0.jpg) 
![](img/GPd0D6qbYAACsEj-1.jpg) 

- 眼部。  
![](img/GPd0D6qbYAACsEj-3.jpg) 
![](img/GPd0D6qbYAACsEj-2.jpg)  
睫毛处似乎有白色颜料。  
![](img/GPd0D6qbYAACsEj-2gamma.jpg) 

- 未画鼻梁的线条，但能感受到该线条。    
![](img/GPd0D6qbYAACsEj-6.jpg) 

- 排线阴影、交叉排线阴影。  
![](img/GPd0D6qbYAACsEj-4.jpg) 
![](img/GPd0D6qbYAACsEj-5.jpg) 

- 头发的着色。  
![](img/GPRL0V3bEAA-xBi-0.jpg) 
![](img/GPRL0V3bEAA-xBi-2.jpg) 

- 对侧的眼睛被简化为阴影。  
![](img/GPRL0V3bEAA-xBi-1.jpg) 

- 面部表情。嘴部与背景之间的空白似乎是涂抹了白色颜料。    
![](img/GPRL0V3bEAA-xBi-7.jpg)  
![](img/GPRL0V3bEAA-xBi-8.jpg) 

- 衣服褶皱的排线。  
![](img/GPRL0V3bEAA-xBi-3.jpg) 
![](img/GPRL0V3bEAA-xBi-4.jpg) 
![](img/GPRL0V3bEAA-xBi-5.jpg) 
![](img/GPRL0V3bEAA-xBi-6.jpg) 

- 用蓝色线条标识阴影区域。  
![](img/GPr2dMzawAAu50M-1.jpg) 

- 配图：  
    - 可能是关于对白的注解。  
    ![](img/GQVQ_vsaoAAQGly-0.jpg) 
    
    - 配图中的边框有重影。  
    ![](img/GQVQ_vsaoAAQGly-1.jpg) 
    ![](img/GQVQ_vsaoAAQGly-2.jpg) 
    ![](img/GQVQ_vsaoAAQGly-3.jpg) 
    
    - 配图中的两处似乎有镂空。[疑问]为何？    
    ![](img/GQVQ_vsaoAAQGly-4.jpg) 
    ![](img/GQVQ_vsaoAAQGly-5.jpg) 


---
<a name="ch040"></a> 
### CH 29卷 304话 扉页 (1991.02)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/6_hojo-ten_88graphlight_1.jpg "News@edition-88")
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info) 
[![](img/thumb/GOvXB9Wa8AAvBHm.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GOvXB9Wa8AAvBHm?format=jpg&name=4096x4096) 
[![](img/thumb/GO9VSDUboAAfxAJ.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GO9VSDUboAAfxAJ?format=jpg&name=4096x4096) 
[![](img/thumb/GO9VSC3bUAARKwS.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GO9VSC3bUAARKwS?format=jpg&name=4096x4096) 
[![](img/thumb/GP4j_m0bUAAmhob.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GP4j_m0bUAAmhob?format=jpg&name=4096x4096) 
[![](img/thumb/GQCqdmDbEAMRldc.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQCqdmDbEAMRldc?format=jpg&name=4096x4096) 
[![](img/thumb/GQF-NJxbEAYtQi6.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQF-NJxbEAYtQi6?format=jpg&name=4096x4096) 
[![](img/thumb/GQMbLBAbgAAag_2.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQMbLBAbgAAag_2?format=jpg&name=4096x4096) 
[![](img/thumb/GQLcQC7a8AAYnS9.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQLcQC7a8AAYnS9?format=jpg&name=4096x4096) 
[![](img/thumb/GQVQ_sLaIAAM9Gb.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQVQ_sLaIAAM9Gb?format=jpg&name=4096x4096) 
[![](img/thumb/GQrHhrUaoAAxCAo.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQrHhrUaoAAxCAo?format=jpg&name=4096x4096) 
[![](img/thumb/GQr4XppbgAA4-SW.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQr4XppbgAA4-SW?format=jpg&name=4096x4096) 
[![](img/thumb/GaFGwLfb0AETOV_.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GaFGwLfb0AETOV_?format=jpg&name=4096x4096) 

细节：   

- 头发的细节。  
![](./img/GOvXB9Wa8AAvBHm-0.jpg) 
![](./img/GOvXB9Wa8AAvBHm-1.jpg)  
![](./img/GaFGwLfb0AETOV_3.jpg) 
![](./img/GaFGwLfb0AETOV_3g.jpg)  
![](./img/GaFGwLfb0AETOV_4.jpg) 
![](./img/GaFGwLfb0AETOV_4g.jpg)  
![](./img/GaFGwLfb0AETOV_0.jpg) 
![](./img/GaFGwLfb0AETOV_0g.jpg)  
![](./img/GaFGwLfb0AETOV_6.jpg) 
![](./img/GaFGwLfb0AETOV_6g.jpg)  
![](./img/GaFGwLfb0AETOV_1.jpg) 
![](./img/GaFGwLfb0AETOV_1g.jpg)  
![](./img/GaFGwLfb0AETOV_2.jpg) 
![](./img/GaFGwLfb0AETOV_2g.jpg)  

- 面部细节。  
![](./img/GOvXB9Wa8AAvBHm-2.jpg) 
![](./img/GOvXB9Wa8AAvBHm-3.jpg) 
![](./img/GOvXB9Wa8AAvBHm-4.jpg)  
![](./img/GaFGwLfb0AETOV_5.jpg) 
![](./img/GaFGwLfb0AETOV_5g.jpg) 

- 一缕缕光线的效果。  
![](./img/GOvXB9Wa8AAvBHm-5.jpg) 
![](./img/GOvXB9Wa8AAvBHm-5gamma.jpg) 
![](./img/GOvXB9Wa8AAvBHm-6.jpg) 
![](./img/GOvXB9Wa8AAvBHm-6gamma.jpg) 
![](img/GO9VSC3bUAARKwS-0.jpg) 
![](img/GO9VSC3bUAARKwS-0gamma.jpg) 
![](img/GQCqdmDbEAMRldc-1.jpg) 

- 衣服褶皱。  
![](./img/GOvXB9Wa8AAvBHm-7.jpg) 
![](./img/GOvXB9Wa8AAvBHm-8.jpg) 
![](./img/GOvXB9Wa8AAvBHm-8gamma.jpg) 
![](./img/GOvXB9Wa8AAvBHm-9.jpg) 
![](./img/GOvXB9Wa8AAvBHm-9gamma.jpg) 
![](./img/GOvXB9Wa8AAvBHm-10.jpg) 

- 左上角人物阴影处的草地。    
![](img/GO9VSC3bUAARKwS-1.jpg) 
![](img/GO9VSC3bUAARKwS-2.jpg) 
![](img/GO9VSC3bUAARKwS-3.jpg) 
![](img/GO9VSC3bUAARKwS-4.jpg) 
![](img/GQCqdmDbEAMRldc-0.jpg) 


---
<a name="ch041"></a> 
### CH 插画 (1991.12)  
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GOJnVhgaMAEq_QE.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GOJnVhgaMAEq_QE?format=jpg&name=4096x4096) 
[![](img/thumb/GPgVFBZacAAmbAt.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPgVFBZacAAmbAt?format=jpg&name=4096x4096) 
[![](img/thumb/GP4j_p1a0AAM5jp.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GP4j_p1a0AAM5jp?format=jpg&name=4096x4096) 
[![](img/thumb/GQFP129bEAEy_wa.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQFP129bEAEy_wa?format=jpg&name=4096x4096) 
[![](img/thumb/picture_pc_1481c55d3f3270a54d49b74192ae1c05.webp "现场拍照@note.com")
](https://note.com/naofumitasaki/n/nc4c8852e3788) 

细节：   

- 背景的海原神头像呈素描风格。  
![](img/GOJnVhgaMAEq_QE-2.jpg) 
![](img/GOJnVhgaMAEq_QE-1.jpg) 
![](img/GOJnVhgaMAEq_QE-0.jpg) 

- 海原神的脖子处喷洒了白色颜料，其轮廓勾勒出衣领。  
![](img/GP4j_p1a0AAM5jp-0.jpg) 

- 头发和面部。  
![](img/GP4j_p1a0AAM5jp-1.jpg)  
![](img/GP4j_p1a0AAM5jp-2.jpg)  
![](img/picture_pc_1481c55d3f3270a54d49b74192ae1c05-0.jpg) 

- 单册鼻翼处点有白色高光。  
![](img/picture_pc_1481c55d3f3270a54d49b74192ae1c05-1.jpg) 
![](img/picture_pc_1481c55d3f3270a54d49b74192ae1c05-1g.jpg) 

- 服饰的细节。  
![](img/picture_pc_1481c55d3f3270a54d49b74192ae1c05-2.jpg) 

- 褶皱。膝盖和脚部的褶皱被简化。    
![](img/GP4j_p1a0AAM5jp-3.jpg) 
![](img/GP4j_p1a0AAM5jp-4.jpg) 
![](img/GP4j_p1a0AAM5jp-5.jpg) 
![](img/GP4j_p1a0AAM5jp-6.jpg) 
![](img/GP4j_p1a0AAM5jp-7.jpg) 


---
<a name="ch042"></a> 
### CH 第？卷 第？话   
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GPWGf5GbcAAmgJU.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPWGf5GbcAAmgJU?format=jpg&name=4096x4096)   

细节：   

- 对话框内有对白的草稿笔迹。这意味着该笔迹不影响不影响最终印刷效果。  
![](img/GPWGf5GbcAAmgJU-0.jpg) 


---
<a name="ch043"></a> 
### CH 第？卷 第？话   
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GPIzChUb0AAo3kj.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPIzChUb0AAo3kj?format=jpg&name=4096x4096)   

细节：   

- 头发边缘与背景之间有白色间隔。  
![](img/GPIzChUb0AAo3kj-0.jpg) 
![](img/GPIzChUb0AAo3kj-0gamma.jpg) 

- 腿部的排线阴影。这在作品中很常见。    
![](img/GPIzChUb0AAo3kj-1.jpg) 

- 网点纸刮蹭的排线，且有渐变。  
![](img/GPIzChUb0AAo3kj-2.jpg) 


---
<a name="ch044"></a> 
### CH 第？卷 第？话   
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GPljn8kaMAA6oM1.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPljn8kaMAA6oM1?format=jpg&name=4096x4096)   

细节：   

- 外眼角和头发的间隔。  
![](img/GPljn8kaMAA6oM1-1.jpg) 

- 头发的着色和发丝。  
![](img/GPljn8kaMAA6oM1-0.jpg) 


---
<a name="ch045"></a> 
###  CH 第？卷 第？话   
![](img/not_given.jpg) 
[![](img/thumb/GQSJEDEaQAAl4j0.jpg "现场拍照@X")  
](https://pbs.twimg.com/media/GLp9d3Ma4AE27ID?format=jpg&name=4096x4096)

细节：   

- 头发。左一可看到网点纸的痕迹。    
![](img/GQSJEDEaQAAl4j0-0.jpg) 
![](img/GQSJEDEaQAAl4j0-1.jpg) 
![](img/GQSJEDEaQAAl4j0-2.jpg) 

- 眼神。  
![](img/GQSJEDEaQAAl4j0-2.jpg)  
![](img/GQSJEDEaQAAl4j0-3.jpg) 


---

---

[^hojocn]:[北条司中文简介 By CatNj v4.0 updated](http://www.hojocn.com/bbs/viewthread.php?tid=3094)

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
