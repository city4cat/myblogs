
## City Hunter 40周年「北条司展」展览信息 & 部分展品（Part 3/5）  

City Hunter 40周年「北条司展」以下简称“画展”。  



---

---
<a name="catlady"></a>  
## ネコまんまおかわり♡(白猫少女)  (可能为1986.01[^jump1986i6])  

[^jump1986i6]: [Weekly Shonen Jump #893 - No. 6, 1986 released by Shueisha on January 22, 1986. ](https://comicvine.gamespot.com/weekly-shonen-jump-893-no-6-1986/4000-507000/)

---
<a name="catlady-note"></a>  
### 说明文字  
[![](img/thumb/GLllyr6aIAAtUCg.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GLllyr6aIAAtUCg?format=jpg&name=4096x4096)   

>**ネコまんま おかわり♡**  
**再来一碗猫饭♡**（译注：作品名的直译）  
**Neko Manma Okawari♡**[^jump1986i6-name]  

[^jump1986i6-name]: 有资料[Weekly Shonen Jump #893 - No. 6, 1986 released by Shueisha on January 22, 1986.](https://comicvine.gamespot.com/weekly-shonen-jump-893-no-6-1986/4000-507000/)显示，当年连载时的作品名为"ネコまんまおかわり♡/Neko Manma wo Kawari♡"。  

> 週刊少年ジャンプ 1986年6号  
周刊少年Jump 1986年6号  

>週刊少年ジャンプ 1986年6号に掲載。45べージ。『JC1』・『文庫1』・「自選集』に収録されている。『JC1』では最後のハートの表記は白抜きの♡だが、『自選集』では黒塗りの❤となっている。  
连载于《周刊少年Jump》1986年第6号。45页。收录于《JC1》、《文库1》、《自选集》。在《JC1》中，最后的♡是白色的♡，但在《自选集》中是❤。  

>ネコを主人公とした『鶴の恩返し』のパロディ作品。  
模仿《鹤的报恩》，以猫为主人公。  

>**北条先生に聞きました!**  
**北条老师问答!**  

>Q:猫の表現がとても可愛いのですが、苦労したことなど教えてください。  
猫的表现很可爱，能谈谈您遇到的困难吗?  

>そうですか?いまあらためて見ると・・・ひどいなこりゃ(笑)。  
是吗?现在再看的话……它很糟糕(笑)。  

>この時はシティーハンターを連載中で、短編作品は基本的に連載している中でスケジュ一ルを捻出して制作していましたので苦労した記憶ばかりです。  
那个时候正在连载《城市猎人》，短篇作品基本上都是在连载的过程中挤出时间来制作的，所以都是很辛苦的记忆。  

>当時は猫を飼ってはいないのですが、近所に普通に野良猫がいる時代でしたので観察したり・・・そういえば窓を開けていたらアシスタントのべットの上で猫が寝ているなんて事もある時代でしたね。  
当时我还没有养猫，但小区里经常有流浪猫，我就观察它们......我记得有时候打开窗户，一只猫就睡在我助手的床上。   


>Q:猫派ですか?犬派ですか?  
Q:您喜欢猫吗？ 还是喜欢狗？  

>どちらも好きですね。子供の頃は両方飼つていた時期があります。  
都喜欢。我小时候两种都养过。  

>ネコまんまの定義はわからないのですが、犬にはネコまんまをあげてましたね。  
虽然不知道猫饭的定义是什么，但我经常给我的狗吃猫粮。   

>毎日食べて飽きないのかなって思ってました(笑)。  
每天吃也不会腻吧(笑)。（译注：待校对）  

---

<a name="catlady01"></a>  
### 白猫少女 扉页 (1986)  
[![](./img/thumb/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x.jpg) 
[![](./img/thumb/fukusei-neko-02_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-neko-02_4800x.jpg) 
[![](img/thumb/GLWG9SEaQAAFYTj.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLWG9SEaQAAFYTj?format=jpg&name=4096x4096) 
[![](img/thumb/GL7EP2-bAAAvyDD.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GL7EP2-bAAAvyDD?format=jpg&name=4096x4096) 
[![](img/thumb/GN6fEKnaMAAAaL-2.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GN6fEKnaMAAAaL-?format=jpg&name=4096x4096) 

细节：   

- 发丝飘逸：  
![](img/GLWG9SEaQAAFYTj-0.jpg) 

- 发丝的细节。  
![](./img/fukusei-neko-02_4800x-8.jpg) 

- 眼部细节。  
![](./img/fukusei-neko-02_4800x-0.jpg) 

- 眉毛的笔触。  
![](./img/fukusei-neko-02_4800x-9.jpg) 
![](./img/fukusei-neko-02_4800x-9gamma.jpg) 

- 相机的长镜头上似乎加了高光。对比调整gamma值后（左二列）的效果。    
![](img/GLWG9SEaQAAFYTj-1.jpg) 
![](img/GLWG9SEaQAAFYTj-2.jpg)  
![](./img/fukusei-neko-02_4800x-6.jpg) 
![](./img/fukusei-neko-02_4800x-6gamma.jpg) 

- 相机排线阴影。  
![](./img/fukusei-neko-02_4800x-7.jpg) 

- 衣服上高光的表现形式。  
![](img/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x-0.jpg) 
![](./img/fukusei-neko-02_4800x-10.jpg) 
![](./img/fukusei-neko-02_4800x-11.jpg) 
![](./img/fukusei-neko-02_4800x-12.jpg)  
![](img/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x-1.jpg) 
![](img/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x-2.jpg) 
![](img/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x-3.jpg) 

- 皮带的着色。  
![](./img/fukusei-neko-02_4800x-1.jpg) 

- 衣服上高光的表现形式。  
![](img/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x-4.jpg) 
![](img/fukusei-neko-01_73bd07a5-42c4-4229-a9ea-6b6920bab742_4800x-5.jpg)  
![](./img/fukusei-neko-02_4800x-2.jpg) 
![](./img/fukusei-neko-02_4800x-3.jpg)  
[疑问]下图的白色，不知是使用的白色颜料？还是刮蹭网点纸？  
![](./img/fukusei-neko-02_4800x-4.jpg) 
![](./img/fukusei-neko-02_4800x-4gamma.jpg)  
![](./img/fukusei-neko-02_4800x-5.jpg) 
![](./img/fukusei-neko-02_4800x-5gamma.jpg) 


---
<a name="catlady02"></a>  
### 白猫少女 (1986)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GN6fEKnaMAAAaL-2.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GN6fEKnaMAAAaL-?format=jpg&name=4096x4096)
[![](img/thumb/GauY3PYa0AAWXsI.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GauY3PYa0AAWXsI?format=jpg&name=4096x4096) 
[![](img/thumb/GauY3PYbUAAbQY9.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GauY3PYbUAAbQY9?format=jpg&name=4096x4096) 

细节：   

- 镜头的内容有交叉。  
![](img/GauY3PYa0AAWXsI-0.jpg) 
![](img/GauY3PYa0AAWXsI-1.jpg) 

- 左侧配图上有如下红色标记。  
![](img/GauY3PYbUAAbQY9-0.jpg) 

- 对比原画，左侧配图表明如下文字是后来加上的。  
![](img/GauY3PYbUAAbQY9-1.jpg) 

---
<a name="catlady03"></a>  
### 白猫少女 (1986)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GN6fEKnaMAAAaL-3.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GN6fEKnaMAAAaL-?format=jpg&name=4096x4096) 
[![](img/thumb/GauY3PVaEAAC6Li.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GauY3PVaEAAC6Li?format=jpg&name=4096x4096) 

细节：   

(未找到足够大的图片以查看其细节。)  

---
<a name="catlady04"></a>  
###  白猫少女(1986)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLw8JEAbQAAx2NA.jpg "现场拍照@X")  
](https://pbs.twimg.com/media/GLw8JEAbQAAx2NA?format=jpg&name=4096x4096)

细节：   

- 使用大量白色斑点表现雪花。  
![](img/GLw8JEAbQAAx2NA-1.jpg) 
![](img/GLw8JEAbQAAx2NA-0.jpg) 
![](img/GLw8JEAbQAAx2NA-6.jpg) 
![](img/GLw8JEAbQAAx2NA-5.jpg)  

- 灯光的周围也用了白色颜料。  
![](img/GLw8JEAbQAAx2NA-2.jpg) 
![](img/GLw8JEAbQAAx2NA-3.jpg) 

- 男主和女主相拥的镜头是剪纸贴上的。    
![](img/GLw8JEAbQAAx2NA-4.jpg) 

- 白颜料的线可能是掩盖网点纸的边缘。  
![](img/GLw8JEAbQAAx2NA-8.jpg) 
![](img/GLw8JEAbQAAx2NA-7.jpg) 

- 为了表示晶莹的光芒，用白颜料将实线变为虚线。  
![](img/GLw8JEAbQAAx2NA-9.jpg) 

- 画纸有侧有如下的痕迹。[疑问]是白色颜料的痕迹？还是纸张表面的划痕？    
![](img/GLw8JEAbQAAx2NA-10.jpg) 
![](img/GLw8JEAbQAAx2NA-11.jpg) 

---

(此后，作者[继续创作《CityHunter》](./details1.md#ch004)。)  

--- 

---
<a name="splash"></a>  
## SPLASH! (1987)[^hojocn]

---

---

<a name="splash2"></a>  
## SPLASH! 2 (1987)[^hojocn]

---

---

<a name="angelgift"></a>  
## 天使の贈り物 (天使的礼物) (1988.07)

---
<a name="angelgift-note"></a>  
### 说明文字  
![](img/not_given.jpg "官方照片") 

(待搜集)  

---
<a name="angelgift01"></a>  
### 天使的礼物 (1988.07)  

[![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-tenshi-02_100x.jpg)
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukuseigenkou-tenshi-02_4800x.jpg)
[![](img/thumb/3_hojo-ten_fukuseigenko_0.jpg "B4尺寸, News@edition-88")
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info) 
[![](img/thumb/GP4kBcxa4AEN-IB.jpg "左侧图。现场拍照@twitter") 
](https://pbs.twimg.com/media/GP4kBcxa4AEN-IB?format=jpg&name=4096x4096) 

细节：   

- 面部排线阴影。  
![](img/GP4kBcxa4AEN-IB-2.jpg) 

- 白色颜料的修改。  
![](img/GP4kBcxa4AEN-IB-1.jpg) 

- 头发的细节。  
![](img/GP4kBcxa4AEN-IB-3.jpg) 
![](img/GP4kBcxa4AEN-IB-4.jpg) 

- 排线。  
![](img/GP4kBcxa4AEN-IB-5.jpg) 

- 该页面右侧有黄色便签。  
![](img/GP4kBcxa4AEN-IB-0.jpg) 

---
<a name="angelgift02"></a>  
### 天使的礼物 (1988.07)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GP4kBcxa4AEN-IB.jpg "右侧图。现场拍照@twitter") 
](https://pbs.twimg.com/media/GP4kBcxa4AEN-IB?format=jpg&name=4096x4096) 

细节：   

- 对话框底色为黑色，这不常见。  
![](img/GP4kBcxa4AEN-IB-6.jpg) 

- 背景中排列了多个卡通猫头图案，每个猫头都有少许差异，线条形状不完全相同。  
![](img/GP4kBcxa4AEN-IB-7.jpg) 

- 头发的着色。  
![](img/GP4kBcxa4AEN-IB-8.jpg) 
![](img/GP4kBcxa4AEN-IB-9.jpg) 

- 发丝。  
![](img/GP4kBcxa4AEN-IB-11.jpg) 

- 着色阴影。  
![](img/GP4kBcxa4AEN-IB-10.jpg) 

- (edition-88版本)拟声词：  
![](img/fukuseigenkou-tenshi-02_4800x-0.jpg) 

- (edition-88版本)排线阴影。  
![](img/fukuseigenkou-tenshi-02_4800x-1.jpg) 

- (edition-88版本)眉毛。  
![](img/fukuseigenkou-tenshi-02_4800x-2.jpg) 

- (edition-88版本)头发的细节。  
![](img/fukuseigenkou-tenshi-02_4800x-3.jpg) 
![](img/fukuseigenkou-tenshi-02_4800x-8.jpg) 

- (edition-88版本)线条犀利。  
![](img/fukuseigenkou-tenshi-02_4800x-4.jpg) 
![](img/fukuseigenkou-tenshi-02_4800x-5.jpg) 
![](img/fukuseigenkou-tenshi-02_4800x-6.jpg) 
![](img/fukuseigenkou-tenshi-02_4800x-7.jpg) 

---

(此后，作者继续创作[《CityHunter》](./details1.md#ch017)。)  
 

---

---
<a name="splash3"></a>  
## SPLASH! 3 (1988.11)[^hojocn]

([疑问] SPLASH!系列有1～5。本展似乎只展出了SPLASH!3。SPLASH!3有何独特之处吗？)  

<a name="splash3-note"></a>  
### 说明文字  
![](img/not_given.jpg "官方照片")  
(待搜集)  


---
<a name="splash300"></a>  
### SPLASH! 3 (1988.11)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3.webp "现场拍摄@xhs")
](https://www.xiaohongshu.com/discovery/item/666cc3b1000000001d016c84?xsec_token=CB2_Szx3z4CNlttX-VfAdbA4E-XgWFDjtI6dVfm_r-cyU=) 


细节：   

- 本页的镜头均为男主的回忆内容，故分格无黑色边线框。

- 如下几个镜头几乎是灰色画面（只有女主为彩色）。女主出现后，回忆的镜头逐渐由灰色变成了彩色。以女主为中心，画面中的灰色渐退变为彩色。[疑问]为何如此？我猜，灰色表示男主郁闷的心情，而女主是其眼中的色彩。        
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-0.webp) 
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-1.webp) 
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-2.webp) 
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-3.webp) 

- 该分格(panel)边缘涂满了白色颜料。该区域的纸面有大量黄色痕迹。[疑问]为何如此？      
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-2.webp) 
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-2g.webp) 

- 背景色未囊括前景物体，有种破格的效果。    
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-4.webp) 
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-6.webp) 

- 结合剧情，女主的脸颊微红表示其害羞。    
![](img/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3-5.webp) 


---
<a name="splash301"></a>  
### SPLASH! 3 (1988.11)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/th_A4_09513-0.webp "现场拍摄，右侧图@denfaminicogamer")
](https://news.denfaminicogamer.jp/kikakuthetower/240418b) 
[![](img/thumb/GaQ-b-ubUAEGtGS.jpg "现场拍摄@twitter")
](https://pbs.twimg.com/media/GaQ-b-ubUAEGtGS?format=jpg&name=4096x4096) 
[![](img/thumb/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3.webp "现场拍摄@xhs")
](https://www.xiaohongshu.com/discovery/item/666cc3b1000000001d016c84?xsec_token=CB2_Szx3z4CNlttX-VfAdbA4E-XgWFDjtI6dVfm_r-cyU=) 

细节：   

- 不同的背景色对应角色不同的心情。  
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-0.webp) 

- 表情。  
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-1.webp) 
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-4.webp) 

- 海浪泡沫、明媚的光线、云均用白色颜料绘制。（照片有反光）  
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-2.webp) 
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-2g.webp) 
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-3.webp) 
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-3g.webp) 


- 花边、褶皱逼真。  
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-5.webp) 
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-5g.webp) 

- 边缘处有白色颜料。  
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-6.webp) 
![](img/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3-6g.webp) 


---

<a name="splash302"></a>  
### SPLASH! 3 (1988.11)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/C6SaAlVy7hn.jpg "现场拍摄@Instagram")  
](https://www.instagram.com/explore/tags/北条司展)

细节：   

- 夜晚的灯光涂膜了大量白色颜料。    
![](img/C6SaAlVy7hn-0.jpg) 
![](img/C6SaAlVy7hn-0gamma.jpg) 
![](img/C6SaAlVy7hn-1.jpg) 
![](img/C6SaAlVy7hn-1gamma.jpg) 


---
<a name="splash303"></a>  
### SPLASH! 3 (1988.11)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLRg8aoaUAAzaQc-r.jpg "现场拍摄@twitter")
](https://pbs.twimg.com/media/GLRg8aoaUAAzaQc?format=jpg&name=4096x4096) 
[![](img/thumb/GaQ-b-sa0AA2fya.jpg "现场拍摄@twitter")
](https://pbs.twimg.com/media/GaQ-b-sa0AA2fya?format=jpg&name=4096x4096) 


细节：   

- 白色网点纸盖过画面，形成景深(DOF)效果。  
![](img/GLRg8aoaUAAzaQc-0.jpg) 
![](img/GLRg8aoaUAAzaQc-1.jpg)  
焦点在背景女主。  
![](img/GLRg8aoaUAAzaQc-2.jpg)  
但前景男主的眼部似乎去除了网点阴影。    
![](img/GLRg8aoaUAAzaQc-3.jpg) 

---
<a name="splash304"></a>  
### SPLASH! 3 (1988.11)  
[![](./img/thumb/fukusei-splash-01_4c92c9f1-1b5c-4161-9a79-37a4a77ac727_4800x.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-splash-01_4c92c9f1-1b5c-4161-9a79-37a4a77ac727_4800x.jpg) 
[![](./img/thumb/fukusei-splash-02_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-splash-02_4800x.jpg) 
[![](img/thumb/GLRg8aoaUAAzaQc.jpg "现场拍摄@X") 
](https://pbs.twimg.com/media/GLRg8aoaUAAzaQc?format=jpg&name=4096x4096) 
[![](img/thumb/GLvaOnEakAAotG0.jpg "现场拍摄@X") 
](https://pbs.twimg.com/media/GLvaOnEakAAotG0?format=jpg&name=4096x4096) 
[![](img/thumb/GL77HLsaYAAw423.jpg "现场拍摄@X") 
](https://pbs.twimg.com/media/GL77HLsaYAAw423?format=jpg&name=4096x4096) 
[![](img/thumb/GORsEXyaEAEJL08.jpg "现场拍摄@X") 
](https://pbs.twimg.com/media/GORsEXyaEAEJL08?format=jpg&name=4096x4096) 
[![](img/thumb/GPjuOEJakAAna-N.jpg "现场拍摄@X") 
](https://pbs.twimg.com/media/GPjuOEJakAAna-N?format=jpg&name=4096x4096) 
[![](img/thumb/GQCqdmEa0AAzi5a.jpg "现场拍摄@X") 
](https://pbs.twimg.com/media/GQCqdmEa0AAzi5a?format=jpg&name=4096x4096) 
[![](img/thumb/GaETchnb0AMbrGH.jpg "现场拍摄@X") 
](https://pbs.twimg.com/media/GaETchnb0AMbrGH?format=jpg&name=4096x4096) 

细节：   

- 雨线。  
![](./img/GaETchnb0AMbrGH-6.jpg) 
![](./img/GaETchnb0AMbrGH-6g.jpg) 
![](./img/GaETchnb0AMbrGH-7.jpg) 
![](./img/GaETchnb0AMbrGH-7g.jpg) 
![](./img/GaETchnb0AMbrGH-8.jpg) 
![](./img/GaETchnb0AMbrGH-8g.jpg) 

- 杯子内液体及着色。  
![](img/GLvaOnEakAAotG0-0.jpg) 
![](img/GLvaOnEakAAotG0-3.jpg) 
![](./img/GaETchnb0AMbrGH-5.jpg) 
![](./img/GaETchnb0AMbrGH-5g.jpg)  
![](img/GLvaOnEakAAotG0-5.jpg) 
![](./img/GaETchnb0AMbrGH-1.jpg) 
![](./img/GaETchnb0AMbrGH-2.jpg) 
![](./img/GaETchnb0AMbrGH-3.jpg) 
![](./img/GaETchnb0AMbrGH-4.jpg)  

- 杯子口及着色。  
![](img/GLvaOnEakAAotG0-1.jpg) 
![](./img/GaETchnb0AMbrGH-0.jpg) 

- 用白色颜料画雨线。  
![](img/GLvaOnEakAAotG0-2.jpg) 
![](img/GLvaOnEakAAotG0-4.jpg) 

- 这个镜头是贴上去的。  
![](img/GL77HLsaYAAw423-0.jpg) 

- 有些地方看不出白色颜料涂抹的痕迹。  
![](img/fukusei-splash-02_4800x-0.jpg) 
![](img/fukusei-splash-02_4800x-0gamma.jpg) 
![](img/fukusei-splash-02_4800x-1.jpg) 
![](img/fukusei-splash-02_4800x-1gamma.jpg) 

- 有些地方能看出白色颜料涂抹的痕迹。  
![](img/GQCqdmEa0AAzi5a-0.jpg) 
![](img/GQCqdmEa0AAzi5a-0g.jpg) 

- 画面边沿。  
![](img/fukusei-splash-01_4c92c9f1-1b5c-4161-9a79-37a4a77ac727_4800x-0.jpg) 
![](img/fukusei-splash-01_4c92c9f1-1b5c-4161-9a79-37a4a77ac727_4800x-1.jpg) 
![](img/fukusei-splash-01_4c92c9f1-1b5c-4161-9a79-37a4a77ac727_4800x-2.jpg) 
![](img/fukusei-splash-01_4c92c9f1-1b5c-4161-9a79-37a4a77ac727_4800x-3.jpg) 

---

(此后，作者继续创作[《CityHunter》](./details1.md#ch021)。)  
 

---

---
<a name="splash4"></a>  
## SPLASH! 4 (1989)[^hojocn]


---

---
<a name="taxidriver"></a>  
## TAXI DRIVER  (1990)

<a name="taxidriver-note"></a>  
### 解说文字  
（待搜集）  

---
<a name="taxidriver01"></a> 
### TAXI DRIVER  (1989.12)   
[![](./img/thumb/fukusei-taxi-01_b457e2fe-8448-4329-8f3d-81ecbeee34dd.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-taxi-01_b457e2fe-8448-4329-8f3d-81ecbeee34dd_4800x.jpg) 
[![](./img/thumb/fukusei-taxi-02.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-taxi-02_4800x.jpg) 
[![](img/thumb/5_hojo-ten_fukuseigenko_1.jpg "B4尺寸, News@edition-88")
](https://edition-88.com/blogs/blog/hojo-tsukasa-exhibition-40th-2025-info) 
[![](img/thumb/GLw8JEDaoAAfQAK.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLw8JEDaoAAfQAK?format=jpg&name=4096x4096) 

细节：   

<a name="taxi-driver_light-hole"></a>   

- 背景灯光是剪贴上的，且中空。    
![](img/GLw8JEDaoAAfQAK-1.jpg) 
![](img/GLw8JEDaoAAfQAK-0.jpg) 

- 背景灯光辉光的效果。部分加了排线。  
![](img/fukusei-taxi-02_0.jpg) 
![](img/fukusei-taxi-02_1.jpg) 

- 对白文字有补丁。  
![](img/GLw8JEDaoAAfQAK-2.jpg) 

- 头发的细节。  
![](img/fukusei-taxi-02_2.jpg) 
![](img/fukusei-taxi-02_3.jpg) 
![](img/fukusei-taxi-02_4.jpg) 

- 粗发丝未被背景里的着色效果(网点纸)覆盖(下图左一)。部分极细的发丝被网点纸覆盖(下图左二)。  
![](img/fukusei-taxi-02_6.jpg) 
![](img/fukusei-taxi-02_5.jpg) 

- 面部的网点纸。  
![](img/GLw8JEDaoAAfQAK-3.jpg)  
![](img/GLw8JEDaoAAfQAK-4.jpg) 

- 眼睛的细节。  
![](img/fukusei-taxi-02_12.jpg) 

- 衣领处的线条可以看到边沿。这在调整gamma值后(下图左二)更易看出。我猜，这暗示作者笔力重。      
![](img/fukusei-taxi-02_13.jpg) 
![](img/fukusei-taxi-02_13-gamma.jpg) 

- 手腕处的交叉排线和高光。因为交叉排线均匀，所以不确定是手工排线？还是使用的是网点纸？[疑问]。    
![](img/GLw8JEDaoAAfQAK-5.jpg) 
![](img/GLw8JEDaoAAfQAK-6.jpg) 
![](img/fukusei-taxi-02_11.jpg) 

- 衣领处，图左侧(下图左一左二)的排线犀利、均匀；图右侧(下图左三)的排线相对不均匀。我猜，作者画时未转动纸张。这导致右手画下图左三中的排线方向时有劣势。    
![](img/fukusei-taxi-02_7.jpg) 
![](img/fukusei-taxi-02_8.jpg) 
![](img/fukusei-taxi-02_9.jpg) 

- 排线犀利，有非白。  
![](img/fukusei-taxi-02_10.jpg) 

- 座椅靠背处的细节。  
![](img/GLw8JEDaoAAfQAK-7.jpg) 

- 座椅靠背处用白色颜料擦去部分线条。    
![](img/GLw8JEDaoAAfQAK-8.jpg) 
![](img/GLw8JEDaoAAfQAK-9.jpg) 



---
<a name="taxidriver02"></a>
### TAXI DRIVER  (1990)   
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMLdVQnbAAAsWUv.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMLdVQnbAAAsWUv?format=jpg&name=4096x4096)
[![](img/thumb/GNw_zyNbgAEEjtE.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GNw_zyNbgAEEjtE?format=jpg&name=4096x4096)

细节：   

- 破格：  
![](img/GNw_zyNbgAEEjtE-0.jpg) 

- 灯光中心被挖空。  
![](img/GNw_zyNbgAEEjtE-1.jpg) 
![](img/GNw_zyNbgAEEjtE-2.jpg) 

- 下眼眶加阴影。这不常见。  
![](img/GNw_zyNbgAEEjtE-3.jpg) 


- 这两个镜头角色动作一致。左二镜头是剪贴上的。  
![](img/GMLdVQnbAAAsWUv-0.jpg) 
![](img/GMLdVQnbAAAsWUv-1.jpg)  
左二镜头虽然是剪贴上的，但角色表情有改动：  
![](img/GMLdVQnbAAAsWUv-2.jpg) 
![](img/GMLdVQnbAAAsWUv-3.jpg)  
左二镜头虽然是剪贴上的，但网点阴影不一致。由此推测，流程是：复印-->粘贴-->修改表情、贴网点纸。    
![](img/GMLdVQnbAAAsWUv-4.jpg) 
![](img/GMLdVQnbAAAsWUv-5.jpg) 

- 十字架上的阴影及反光。以及缀饰链子的画法。    
![](img/GMLdVQnbAAAsWUv-6.jpg) 

---

(此后，作者继续创作[《CityHunter》](./details1.md#ch031)。)  
 

---

---
<a name="familyplot"></a>  
## ファミリー・プロット  (Family Plot) (1992.01)  

<a name="familyplot-note"></a>  
### 解说文字  
[![](img/thumb/447361073_3779830275631099_2956132758657113377_n.jpg "现场拍照@https://www.instagram.com/p/C7t8Bz7PoRy/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447361073_3779830275631099_2956132758657113377_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=107&_nc_ohc=z2kkyWsF5zYQ7kNvgFw7c1H&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzU1MTgzMQ%3D%3D.3-ccb7-5&oh=00_AYBkpHgCApx7noaIkcu1p95PUVtK1ig1JuhgVVl0n9lHqA&oe=67279B2F&_nc_sid=10d13b) 

>**ファミリー・プロット**  
**家庭故事**  
**Family Plot**  
週刊少年ジャンプ 19892年11号  
周刊少年Jump 1992年第11号  

> 写真家の秀幸は婚約者として玲子を息子の和也に紹介するが、和也は独占欲から玲子に反発し、家を飛び出す。後を追いかけようとした秀幸は階段から転落し、記憶喪失となる。最愛の人物から他人の様な目で見られる辛さを共感し、和也は徐々に玲子と打ち解けていく。  
摄影师秀幸将玲子作为未婚妻介绍给儿子和也，和也却因为占有欲而反抗玲子，离家出走。追在后面的秀幸从楼梯上摔了下来，丧失了记忆。和也感受到了被最爱的人当成外人看待的痛苦，慢慢和玲子融洽起来。  

>**北条先生に聞きました!**  
**北条老师问答!**  

> Q:記憶喪失がテーマの短編作品ですが、どのようなきっかけで描かれましたか。  
这是以记忆丧失为主题的短篇作品，是在怎样的契机下创作的？  

> 当時、また堀江さんが担当編集になり、記憶喪失がテーマの面白い映画があるんだよ・・・とそう聞いて制作した短編です。映画を観てから制作すると引っ張られるといけないので、描き終わってから観たのですが・・・・・・堀江さんが言ってたシーンがことごとく無くて、全然違うストーリでした(笑)。  
当时，堀江又担任责任编辑，听说有一部以记忆丧失为主题的有趣电影...于是就制作了这个短篇。看了电影之后再创作的话就跳不出来了，所以画完之后再看了......堀江先生说的场景全都没有，是完全不同的故事（笑）。  


---

<a name="familyplot01"></a>  
<a name="FamilyPlotDoorPage"></a>  

### Family Plot 扉绘 (1992.01)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GP4kBdHaIAEt6c0.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GP4kBdHaIAEt6c0?format=jpg&name=4096x4096) 
[![](img/thumb/Ga5oXbtaMAAis6F.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/Ga5oXbtaMAAis6F?format=jpg&name=4096x4096) 

细节：   

- 树影斑驳的效果。  
![](img/GP4kBdHaIAEt6c0-0.jpg) 
![](img/GP4kBdHaIAEt6c0-1.jpg) 

- 头发。  
![](img/GP4kBdHaIAEt6c0-2.jpg) 
![](img/GP4kBdHaIAEt6c0-3.jpg) 

- 服饰。  
![](img/GP4kBdHaIAEt6c0-4.jpg) 
![](img/GP4kBdHaIAEt6c0-5.jpg) 
![](img/GP4kBdHaIAEt6c0-6.jpg) 
![](img/GP4kBdHaIAEt6c0-7.jpg) 
![](img/GP4kBdHaIAEt6c0-8.jpg) 
![](img/GP4kBdHaIAEt6c0-9.jpg) 
![](img/GP4kBdHaIAEt6c0-10.jpg) 
![](img/GP4kBdHaIAEt6c0-11.jpg) 

- 角色和地面交界处画了似乎是小草的东西。  
![](img/GP4kBdHaIAEt6c0-12.jpg) 
![](img/GP4kBdHaIAEt6c0-13.jpg) 
![](img/GP4kBdHaIAEt6c0-14.jpg) 
![](img/GP4kBdHaIAEt6c0-15.jpg) 

- 背景的樱花树没有明显的边线。  
![](img/GP4kBdHaIAEt6c0-17.jpg) 
![](img/GP4kBdHaIAEt6c0-16.jpg) 

- 该作品与[GALLERY ZENON 特绘](./details2.md#GALLERYZENONSpecial)有[相似点](./details2.md#v.s.FamilyPlot)。  


---

<a name="familyplot02"></a>  
### Family Plot (1992.01)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/447057571_974441087488357_3539414436810596178_n.jpg "现场拍照@https://www.instagram.com/p/C7t8Bz7PoRy/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447057571_974441087488357_3539414436810596178_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi44NDZ4ODQ2LnNkci5mMjkzNTAuZGVmYXVsdF9pbWFnZSJ9&_nc_ht=scontent.cdninstagram.com&_nc_cat=101&_nc_ohc=COq_52PbfWgQ7kNvgHLn_mD&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzYxMDY0Nw%3D%3D.3-ccb7-5&oh=00_AYAvOJgD0A4_zyLK_OmhNegx6QnqGIi9XV8l61Iz5iq0qw&oe=67277954&_nc_sid=10d13b) 
[![](img/thumb/GQ_5SX8boAAMA_S.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GQ_5SX8boAAMA_S?format=jpg&name=4096x4096) 
[![](img/thumb/447518600_350877797669263_493765614366573513_n.jpg "现场拍照@https://www.instagram.com/p/C7t8Bz7PoRy/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447518600_350877797669263_493765614366573513_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=107&_nc_ohc=eMrPjvw6tzYQ7kNvgGEfUYH&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzQ4NzQyMg%3D%3D.3-ccb7-5&oh=00_AYBoSHLwcA6h5VhGBLk1ZLc38WdBqLoe_e5vLsl_bI0LhA&oe=67279FAB&_nc_sid=10d13b) 

细节：   

- 相机着色逼真。  
![](img/GQ_5SX8boAAMA_S-0.jpg) 
![](img/GQ_5SX8boAAMA_S-0g.jpg) 
![](img/GQ_5SX8boAAMA_S-1.jpg) 
![](img/GQ_5SX8boAAMA_S-1g.jpg)   


---
<a name="familyplot03"></a>  
### Family Plot (1992.01)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPiklt2bMAAGs8r.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPiklt2bMAAGs8r?format=jpg&name=4096x4096) 
[![](img/thumb/447104119_993123972434073_5505083241439909873_n.jpg "现场拍照@https://www.instagram.com/p/C7t8Bz7PoRy/")
](https://scontent.cdninstagram.com/v/t51.29350-15/447104119_993123972434073_5505083241439909873_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=109&_nc_ohc=8IqezXtFJDMQ7kNvgFi9qsP&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzU3OTk2NQ%3D%3D.3-ccb7-5&oh=00_AYBvPdNzkx8aRUo6a3Zi1G_Nxjxi7mVQomQBrqUriWDIAw&oe=6727933D&_nc_sid=10d13b) 

细节：   

- 前景手部的阴影使用的是网点纸。    
![](img/GPiklt2bMAAGs8r-0.jpg) 
![](img/GPiklt2bMAAGs8r-7.jpg) 

- 手部排线阴影。  
![](img/GPiklt2bMAAGs8r-1.jpg) 

- 树干的线条精致。  
![](img/GPiklt2bMAAGs8r-2.jpg) 

- 前景表示胶片边缘的线条。个别位置有白色颜料涂抹的痕迹。    
![](img/GPiklt2bMAAGs8r-3.jpg) 
![](img/GPiklt2bMAAGs8r-4.jpg) 
![](img/GPiklt2bMAAGs8r-5.jpg) 
![](img/GPiklt2bMAAGs8r-5g.jpg) 
![](img/GPiklt2bMAAGs8r-6.jpg) 
![](img/GPiklt2bMAAGs8r-6g.jpg) 


---

---
<a name="girlseason"></a>  
## 少女の季節 (1992)[^hojocn]  

<a name="girlseason-note"></a> 
### 解说文字  
（待搜集）


---
<a name="girlseason01"></a>  
### 少女的季节 (1992)  
![](img/not_given.jpg "官方照片")
[![](img/thumb/GP4kBgWbUAAG_zh.jpg "右侧页。现场拍照@twitter")
](https://pbs.twimg.com/media/GP4kBgWbUAAG_zh?format=jpg&name=4096x4096) 

细节：   

- 分格(panel)似乎是剪贴上去的。[疑问]为何要这么做？   
![](img/GP4kBgWbUAAG_zh-0.jpg) 

- 画面里的画布上似乎涂抹了白色颜料。  
![](img/GP4kBgWbUAAG_zh-1.jpg) 


---
<a name="girlseason02"></a>  
### 少女的季节 (1992)  
[![](img/thumb/3_hojo-ten_fukuseigenko_1.jpg "@edition-88")
](https://edition-88.com/products/hojoten-duplicate%EF%BD%8Danuscript8) 
[![](img/thumb/GPjuOTtbAAAKyJT.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GPjuOTtbAAAKyJT?format=jpg&name=4096x4096) 
[![](img/thumb/GP4kBgWbUAAG_zh.jpg "左侧页。现场拍照@twitter")
](https://pbs.twimg.com/media/GP4kBgWbUAAG_zh?format=jpg&name=4096x4096) 

细节：   

- 该页面的很多分格(panel)似乎是剪贴上去的。[疑问]为何要这么做？    
![](img/GPjuOTtbAAAKyJT-0.jpg) 
![](img/GPjuOTtbAAAKyJT-1.jpg) 
![](img/GPjuOTtbAAAKyJT-2.jpg) 
![](img/GPjuOTtbAAAKyJT-3.jpg) 
![](img/GPjuOTtbAAAKyJT-4.jpg) 
![](img/GPjuOTtbAAAKyJT-5.jpg) 

- 圆点用白色颜料填充。  
![](img/GPjuOTtbAAAKyJT-6.jpg) 
![](img/GPjuOTtbAAAKyJT-6g.jpg) 
![](img/GPjuOTtbAAAKyJT-7.jpg) 
![](img/GPjuOTtbAAAKyJT-7g.jpg) 

- 香的头发的着色。着色的线条方向有利于表现挥锤的力度大。    
![](img/fukuseigenkou-syojo-02_4800x-0.jpg) 

- 服饰的褶皱。  
![](img/fukuseigenkou-syojo-02_4800x-2.jpg) 
![](img/fukuseigenkou-syojo-02_4800x-1.jpg) 

- 香胳膊处的排线。表现挥锤的力度和速度。  
![](img/fukuseigenkou-syojo-02_4800x-3.jpg) 
![](img/fukuseigenkou-syojo-02_4800x-4.jpg) 

- 排线阴影。  
![](img/fukuseigenkou-syojo-02_4800x-5.jpg) 

- 拟声词。看不出涂抹白色颜料的痕迹。    
![](img/fukuseigenkou-syojo-02_4800x-7.jpg) 
![](img/fukuseigenkou-syojo-02_4800x-8.jpg) 

- 大锤子上的网点纸着色，以及排线阴影。  
![](img/fukuseigenkou-syojo-02_4800x-9.jpg) 

- 特效聚焦线条，以及獠的影子。  
![](img/GPjuOTtbAAAKyJT-8.jpg) 
![](img/GPjuOTtbAAAKyJT-8g.jpg)  
下图看得出特效聚焦线条笔力重。同时，这意味着绘画时先画特效聚焦线条、然后着色影子。    
![](img/fukuseigenkou-syojo-02_4800x-6.jpg) 

- 排线细腻。  
![](img/GPjuOTtbAAAKyJT-9.jpg) 

- 黑色区域着色。同时，看不出白线的笔触。  
![](img/GPjuOTtbAAAKyJT-10.jpg) 
![](img/GPjuOTtbAAAKyJT-10g.jpg) 
![](img/GPjuOTtbAAAKyJT-10g2.jpg)  
![](img/fukuseigenkou-syojo-02_4800x-10.jpg) 
![](img/fukuseigenkou-syojo-02_4800x-10g.jpg) 
![](img/fukuseigenkou-syojo-02_4800x-10g2.jpg)  
考虑到该分格似乎是剪贴上去的，所以该分格是复印并剪贴上去的吗？[疑问]  


---

---
<a name="cheeryblossoms"></a>  
## 桜の花咲くころ (樱花盛开时) (1993)


<a name="cheeryblossoms-note"></a>  
### 解说文字    
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GXXZ5SObMAE9b4B.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GXXZ5SObMAE9b4B?format=jpg&name=4096x4096)

>**桜の花咲くころ**  
**樱花盛开时**  
**When Cherry Blossoms Bloom** [^cherry]    
週刊少年ジャンプ 1993年3・4合併号  
周刊少年Jump 1993年第3-4合并号  

[^cherry]:  
此处，《樱花盛开时》的英文名可能有误。我猜，其本意可能是：“When The Cherry Blossoms”  

> 小5の桜井真樹は隣に引っ越して来た花屋の少女・西九条紗羅の事が気にかかっていた。そして命の恩人である庭の桜の木が枯れかけている事を非常に気にしていた。桜の木の相談をきっかけとして真崎と紗羅の交流が始まる事となった......。  
小学5年级的樱井真树很在意隔壁搬来的花店少女西九条纱罗。他非常担心花园里的樱桃树正在枯死，这棵樱桃树是救命恩人。以樱之木的商量为契机，真崎和纱罗开始了交流......。  

>**北条先生に聞きました!**  
**北条老师问答!**  

> Q:桜の表現が美しい作品ですが、桜の表現で苦労した点や工夫した点をお教えください。  
这是一部表现樱花很美的作品，请谈一下在表现樱花方面遇到的困难和下的功夫。  

> 桜は細かく描きすぎると汚くなるし、塊として描くと桜に見えないので結構苦労はしました。  
今見ても遠景の桜は無理があるかなと思います。難しいですね・・・どう描けばいいんだろうな。クローズアップスケールの桜は細かく描けば伝わるので、遠景の持っ華やかさとアップを組み合わせて桜を表現していました。  
樱花画得太细了会显得脏，画成块状又不像樱花，所以花了不少功夫。  
现在看来，远景的樱花似乎有些勉强。好难啊……该怎么画呢？特写比例的樱花只要仔细描绘就能传达，所以我结合使用了远景的华丽和特写镜头来表现樱花。  

> あまりスケッチとかはしないので、写真や資料を集めて、頭に入れて・・・そしてその写真や資料を見ないようにして描くようにしています。見てしまうと単なる模写になってしまいますので。  
因为不怎么画素描，所以收集照片和资料，记在脑子里……然后不看那些照片和资料来画。看了就会变成单纯的临摹。  



---
<a name="cheeryblossoms01"></a>  
### 樱花盛开时  (1993)  
[![](./img/thumb/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x.jpg) 
[![](./img/thumb/fukusei-sakura-02_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-sakura-02_4800x.jpg) 
[![](img/thumb/GLaXKJRbMAAix0D.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GLaXKJRbMAAix0D?format=jpg&name=4096x4096) 

细节：   


- 头发的深浅不一。  
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-4.jpg) 
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-5.jpg) 

- 衣服处的头发半透明。  
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-5.jpg) 

- 头顶边线的笔触。  
![](img/fukusei-sakura-02_4800x-10.jpg) 
![](img/fukusei-sakura-02_4800x-10gamma.jpg) 

- 面部曲线。  
![](img/fukusei-sakura-02_4800x-18.jpg) 
![](img/fukusei-sakura-02_4800x-19.jpg) 

- 头发的细节：  
![](img/fukusei-sakura-02_4800x-11.jpg) 
![](img/fukusei-sakura-02_4800x-12.jpg) 
![](img/fukusei-sakura-02_4800x-19.jpg) 
![](img/fukusei-sakura-02_4800x-13.jpg) 
![](img/fukusei-sakura-02_4800x-14.jpg) 
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-2.jpg) 
![](img/fukusei-sakura-02_4800x-15.jpg) 
![](img/fukusei-sakura-02_4800x-16.jpg) 

- 发丝和网点纸的交界。  
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-2.jpg) 

- 不同网点纸的交界处，几乎看不出痕迹。  
![](img/fukusei-sakura-02_4800x-17.jpg) 

- 网点纸渐变阴影，排线阴影，黑色颜料阴影着色。  
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-3.jpg) 

- 腿部网点纸着色。    
![](img/GLaXKJRbMAAix0D-0.jpg) 
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-0.jpg) 


- 用高光笔画树皮的横向纹理（[疑问]表示划痕？）。  
![](img/GLaXKJRbMAAix0D-1.jpg) 
![](img/fukusei-sakura-02_4800x-2.jpg) 
![](img/fukusei-sakura-02_4800x-9.jpg)  
![](img/GLaXKJRbMAAix0D-1gamma.jpg) 
![](img/fukusei-sakura-02_4800x-2gamma.jpg) 
![](img/fukusei-sakura-02_4800x-9gamma.jpg) 

- 树干处的排线：  
![](img/fukusei-sakura-02_4800x-0.jpg) 

- 树干处的交叉排线：  
![](img/GLaXKJRbMAAix0D-2.jpg) 
![](img/fukusei-sakura-02_4800x-1.jpg) 

- 树皮外层（左侧）的边缘处似乎是加了白点（表示树皮的厚度？）。    
![](img/GLaXKJRbMAAix0D-3.jpg) 
![](img/GLaXKJRbMAAix0D-3-gamma.jpg) 
![](img/fukusei-sakura-02_4800x-3.jpg) 
![](img/fukusei-sakura-02_4800x-3gamma.jpg) 

- 树干处的白色颜料：  
![](img/fukusei-sakura-02_4800x-4.jpg) 
![](img/fukusei-sakura-02_4800x-5.jpg) 
![](img/fukusei-sakura-02_4800x-6.jpg)  
![](img/fukusei-sakura-02_4800x-4gamma.jpg) 
![](img/fukusei-sakura-02_4800x-5gamma.jpg) 
![](img/fukusei-sakura-02_4800x-6gamma.jpg) 

- 树皮最外层的着色：  
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-1.jpg) 
![](img/fukusei-sakura-02_4800x-7.jpg) 
![](img/fukusei-sakura-02_4800x-8.jpg)  
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-1gamma.jpg) 
![](img/fukusei-sakura-02_4800x-7gamma.jpg) 
![](img/fukusei-sakura-02_4800x-8gamma.jpg) 

- 黑色背景的涂抹痕迹。  
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-6.jpg) 
![](img/fukusei-sakura-01_8a419687-223b-4f1d-8c8f-2241c65778c8_4800x-6gamma.jpg) 


---
<a name="cheeryblossoms02"></a>  
### 樱花盛开时  (1993)  
![](./img/not_given.jpg) 
[![](img/thumb/GXXZ5SNaYAAzK9V.jpg "现场拍照@twitter")
](https://pbs.twimg.com/media/GXXZ5SNaYAAzK9V?format=jpg&name=4096x4096) 

细节：   

- 用眼球凸起来表现角色受到惊吓时的滑稽效果。原作（左一）在眼球上画了一点表示眼仁。去掉这一点（左二），做个对比。  
![](img/GXXZ5SNaYAAzK9V-0.jpg) 
![](img/GXXZ5SNaYAAzK9V-01.jpg) 


---
<a name="cheeryblossoms03"></a>  
### 樱花盛开时   (1993)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLrD7FDa4AAf090.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLrD7FDa4AAf090?format=jpg&name=4096x4096)
[![](img/thumb/GLaXKJVa8AAP8nG.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLaXKJVa8AAP8nG?format=jpg&name=4096x4096)
[![](img/thumb/GLXoQcqagAE1mUX.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLXoQcqagAE1mUX?format=jpg&name=4096x4096)
[![](img/thumb/GLWGNZjaAAAzY1v.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLWGNZjaAAAzY1v?format=jpg&name=4096x4096)
[![](img/thumb/GMtB5jXaAAAi0xz.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMtB5jXaAAAi0xz?format=jpg&name=4096x4096)

细节：   

- 密集的白色涂料斑点。  
![](img/GLrD7FDa4AAf090-1.jpg) 
![](img/GLrD7FDa4AAf090-2.jpg) 
![](img/GLrD7FDa4AAf090-0.jpg) 

- 调整gamma值后容易看到左下方的树干附近有白色涂料。[疑问]这是为什么？    
![](img/GLrD7FDa4AAf090-3.jpg)  


---
<a name="cheeryblossoms04"></a>  
### 樱花盛开时  (1993)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLaXKJVaAAADX2V.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLaXKJVaAAADX2V?format=jpg&name=4096x4096)

细节：   

- 从近处看，能看出网点纸的痕迹。在网点纸上喷洒白色颜料。  
![](img/GLaXKJVaAAADX2V-0.jpg)  


---  

---
<a name="underthedappledsun"></a>  
## こもれ陽の下で…  (阳光少女) (1993～1994)  

<a name="underthedappledsun-note"></a>  
### 解说文字:  
[![](img/thumb/GX4yVb1aUAMR-za.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GX4yVb1aUAMR-za?format=jpg&name=4096x4096) 

**こもれ陽の下で・・・**  
**Dappied sunlight**[^dappled]  
週刊少年ジャンプ 1993年31号 - 1994年5・6合併号  
周刊少年Jump    1993年31号 - 1994年第5-6合并号  

[^dappled]:  
此处，《阳光少女》的英文名应该有误。应该为：“Dappled Sunlight”  


妹の怪我の原因となったエゴノキの木を切ろうとした少年・北崎達也の前に現れた少女・紗羅。彼女は植物と交感できる力を持っていた。短編『桜の花 咲くころ』を土台に連載化された、永遠の少女と彼女に出会った人々の交流を描くハートフルストーリー。  
少女纱罗出现在少年北崎达也面前，他打算砍掉妹妹受伤的原因——自私树。她拥有与植物交流的力量。以短篇《樱花盛开时》为基础连载化，描写永远的少女与她相遇的人们之间的交流故事。  

---
<a name="underthedappledsun01"></a>  
### 阳光少女 第1话 扉页 (1993)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLvaOnCasAAU0q2.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLvaOnCasAAU0q2?format=jpg&name=4096x4096) 
[![](img/thumb/GMLxrKLaIAAgxWP.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMLxrKLaIAAgxWP?format=jpg&name=4096x4096) 
[![](img/thumb/438059891_3136284806507391_1148735151949064351_n.jpg "现场拍照@facebook") 
](https://www.facebook.com/photo/?fbid=3136284009840804&set=pcb.3136284143174124)
[![](img/thumb/GNtBqeAbcAAF4uA.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GNtBqeAbcAAF4uA?format=jpg&name=4096x4096) 
[![](img/thumb/GZAxvYTbMAA8BfX.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GZAxvYTbMAA8BfX?format=jpg&name=4096x4096) 

<a name="RobertBParker"></a>  

解说文字：  
> **北条先生に聞きました!**  
**北条老师问答！**  

> Q:西九条隼人という海坊主に似たキャラクターがいたり、これまでと違い自然物の作画の多い作品ですね。  
有一个角色叫西九条隼人，他很像海坊主，与以往的作品不同，您画了很多自然物。  

> 久々に海坊主的なキャラクターを描いてみたくなり、やはり描いていて楽しかったです。  
很长一段时间以来，我第一次想画一个剃光头的人物，画他还是很有趣的。  

> 海坊主はロバート・B・パーカーの小説に出てくる黒人のホークというキャラクターをイメージしました。  
海坊主的灵感来自罗伯特 帕克(Robert B. Parker)小说中的黑人角色霍克(Hawk)。  

> ずっと都会を描いていたので都会を描く事に飽きた面はあります。  
我已经画了很长时间的城市，我已经厌倦了画城市。  

> 自然物の方が焦点を決めてコッコッと描けば良いので楽しいです。  
画自然物体更有趣，因为你可以专注于它们，把它们画得栩栩如生。  

> ビルとかはとにかく細かく均一的な作画作業になり、描いていても達成感も無いんですよね。描き終わった時の解放感しかないです(笑)。  
而画建筑物的过程则是非常细致和如一，我在画建筑物时没有成就感。只有画完它们时，我才会有一种解脱感（笑）。   

细节：  
(未找到足够大的图片以查看其细节。)  


---
<a name="underthedappledsun02"></a>  
### 阳光少女 第1话 (1993)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GX4yVb0aUAQRiht.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GX4yVb0aUAQRiht?format=jpg&name=4096x4096) 
[![](img/thumb/GYeJ86obUAApXBm.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GYeJ86obUAApXBm?format=jpg&name=4096x4096) 

细节：   

- 左侧配图有若干标记。拟声词涂有颜色。部分对话框似乎涂有颜色。    
![](img/GYeJ86obUAApXBm-0.jpg) 
![](img/GYeJ86obUAApXBm-1.jpg) 
![](img/GYeJ86obUAApXBm-2.jpg) 
![](img/GYeJ86obUAApXBm-4.jpg) 
![](img/GYeJ86obUAApXBm-3.jpg) 
![](img/GYeJ86obUAApXBm-5.jpg) 

- 汽车烟囱上的空隙结构逼真。  
![](img/GX4yVb0aUAQRiht-0.jpg) 
![](img/GX4yVb0aUAQRiht-2.jpg) 
![](img/GX4yVb0aUAQRiht-1.jpg) 

- 汽车金属面板上的凸起结构逼真。似乎未使用变色颜料画高光。    
![](img/GX4yVb0aUAQRiht-2.jpg) 
![](img/GX4yVb0aUAQRiht-3.jpg) 
![](img/GX4yVb0aUAQRiht-3g.jpg) 


---
<a name="underthedappledsun03"></a>  
### 阳光少女 第1话 (1993)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLaXKJUaIAAeA0i.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLaXKJUaIAAeA0i?format=jpg&name=4096x4096) 
[![](img/thumb/GLXoQmuakAAXCTZ.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLXoQmuakAAXCTZ?format=jpg&name=4096x4096) 
[![](img/thumb/GLvaOe6asAAWZPS.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLvaOe6asAAWZPS?format=jpg&name=4096x4096) 
[![](img/thumb/GMtB5i6bMAAQX7T.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMtB5i6bMAAQX7T?format=jpg&name=4096x4096) 
[![](img/thumb/GaFGwLdb0AI7Vwx.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GaFGwLdb0AI7Vwx?format=jpg&name=4096x4096) 

细节：   

- 车轮处的灰色阴影有两层。在调整gamma后（左二）容易看出。  
![](img/GLaXKJUaIAAeA0i-0.jpg) 
![](img/GLaXKJUaIAAeA0i-1.jpg) 

- 用白色绘制光线明媚的效果。并将胳膊的边缘实线变为虚线。    
![](img/GLaXKJUaIAAeA0i-2.jpg) 

- 喷洒大量白色颜料。  
![](img/GLaXKJUaIAAeA0i-3.jpg) 

- C3中的细节（调整gamma值后）。  
![](img/GLvaOe6asAAWZPS-0.jpg) 



---
<a name="underthedappledsun04"></a>  
###  阳光少女 第1话 (1993)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLg0WJLaUAAr205.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLg0WJLaUAAr205?format=jpg&name=4096x4096)
[![](img/thumb/GMPIK8pa4AAhkvr.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMPIK8pa4AAhkvr?format=jpg&name=4096x4096)
[![](img/thumb/GYeJ86nasAANR8N.jpg "现场拍照，左侧图@X") 
](https://pbs.twimg.com/media/GYeJ86nasAANR8N?format=jpg&name=4096x4096) 
[![](img/thumb/GYeJ86naUAA3nf1.jpg "现场拍照，左侧图@X") 
](https://pbs.twimg.com/media/GYeJ86naUAA3nf1?format=jpg&name=4096x4096) 

细节：   

- 左侧配图里对第一分格中的文字有如下标记：  
![](img/GYeJ86naUAA3nf1-0.jpg)  
此外，对第一分格有标记"P52->40%"。  
[疑问]以上这些标记是什么意思？  

- 灰色和白色过渡平滑，似乎没有使用网点纸。  
![](img/GLg0WJLaUAAr205-0.jpg) 
![](img/GLg0WJLaUAAr205-5.jpg) 

- 白色墨点。  
![](img/GLg0WJLaUAAr205-1.jpg) 

- 人体上(胸前)的灰色和白色边界处涂抹了白色颜料。降低gamma值后更易看出。    
![](img/GLg0WJLaUAAr205-3.jpg) 
![](img/GLg0WJLaUAAr205-2.jpg) 


- 白色的细发丝是使用白色涂料(高光笔？)画的。  
![](img/GLg0WJLaUAAr205-4.jpg) 
![](img/GLg0WJLaUAAr205-6.jpg)  
![](img/GYeJ86nasAANR8N-0.jpg) 
![](img/GYeJ86nasAANR8N-0g.jpg)  
![](img/GYeJ86nasAANR8N-2.jpg) 
![](img/GYeJ86nasAANR8N-2g.jpg)  
![](img/GYeJ86nasAANR8N-3.jpg) 
![](img/GYeJ86nasAANR8N-3g.jpg) 

- 无边框对话框和镜头边框衔接处涂了白色颜料。  
![](img/GLg0WJLaUAAr205-7.jpg) 

- 白色光线与背景灰点衔接自然，降低gamma值后也看不出。似乎没有使用网点纸。  
![](img/GLg0WJLaUAAr205-8.jpg) 
![](img/GLg0WJLaUAAr205-9.jpg) 

- 用高光笔绘制光线。  
![](img/GLg0WJLaUAAr205-10.jpg) 


---
<a name="underthedappledsun05"></a>  
###  阳光少女 第1话 (1993)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMPIK8pa4AAhkvr.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMPIK8pa4AAhkvr?format=jpg&name=4096x4096)
[![](img/thumb/GYeJ86nasAANR8N.jpg "现场拍照，右侧图@X") 
](https://pbs.twimg.com/media/GYeJ86nasAANR8N?format=jpg&name=4096x4096) 

细节：   

- 远景的花上加了白色颜料。  
![](img/GYeJ86nasAANR8N-5.jpg) 
![](img/GYeJ86nasAANR8N-5g.jpg)  
![](img/GYeJ86nasAANR8N-6.jpg) 
![](img/GYeJ86nasAANR8N-6g.jpg) 

- 图左上角，沙罗的爸爸出镜。  
![](img/GYeJ86nasAANR8N-10.jpg) 

- 阴影排线细腻。  
![](img/GYeJ86nasAANR8N-9.jpg) 

- 植物叶子逼真。  
![](img/GYeJ86nasAANR8N-4.jpg) 
![](img/GYeJ86nasAANR8N-4g.jpg) 

- 手的姿态。食指处似乎有白颜料勾边。左手小拇指根处的线条特别。        
![](img/GYeJ86nasAANR8N-7.jpg) 
![](img/GYeJ86nasAANR8N-7g.jpg)  
![](img/GYeJ86nasAANR8N-8.jpg) 
![](img/GYeJ86nasAANR8N-8g.jpg) 

- 花盆的阴影过渡平滑。       
![](img/GYeJ86nasAANR8N-7.jpg) 
![](img/GYeJ86nasAANR8N-7g.jpg)  


---
<a name="underthedappledsun06"></a>  
### 阳光少女 第2卷 封面 / 第20话 黑白扉页 (1993.10)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/439508615_17884137009028057_3672213558842265780_n.jpg "现场拍照@facebook") 
](https://www.facebook.com/photo/?fbid=7281659575264852&set=pcb.7281659735264836) 
[![](img/thumb/GX4yVbxa4AAosbd.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GX4yVbxa4AAosbd?format=jpg&name=4096x4096) 

细节：   

- 发丝的细节。细发丝为白色颜料；宽发丝为留白。      
![](img/GX4yVbxa4AAosbd-1.jpg) 
![](img/GX4yVbxa4AAosbd-1g.jpg) 
![](img/GX4yVbxa4AAosbd-0.jpg) 
![](img/GX4yVbxa4AAosbd-0g.jpg) 

- 头发和面部。  
![](img/GX4yVbxa4AAosbd-4.jpg)  
![](img/GX4yVbxa4AAosbd-5.jpg) 

- 脚踝处的线条。  
![](img/GX4yVbxa4AAosbd-2.jpg) 
![](img/GX4yVbxa4AAosbd-3.jpg) 


---
<a name="underthedappledsun07"></a>  
###  阳光少女 最终话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GM1O4WRaQAEcDn2.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GM1O4WRaQAEcDn2?format=jpg&name=4096x4096) 
[![](img/thumb/GZAxvYUbAAAAtFs.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GZAxvYUbAAAAtFs?format=jpg&name=4096x4096) 

细节：  

- 右侧配文的英文版显示了作品的英文名：“Dappled sunlight”。  

- 有些镜头疑似是被剪贴上去的。  
![](img/GM1O4WRaQAEcDn2-0.jpg)  
![](img/GM1O4WRaQAEcDn2-1.jpg)  

- 脚部阴影的多层着色。  
![](img/GM1O4WRaQAEcDn2-5.jpg)  

- 三片花瓣也在地面上留有阴影。  
![](img/GM1O4WRaQAEcDn2-6.jpg)  

- 拟声词处的阴影排线不整齐。我猜，可能是先画了拟声词，后画了排线阴影。  
![](img/GM1O4WRaQAEcDn2-4.jpg)  

- 纸张底部有"KMK KENT"字样。可能是画纸的品牌，例如[KMK Kent](https://www.artilleryphilippines.com/en/papers-sketchbooks/39622-muse-kmk-kent-block-paper-200g-a4-size.html)。         
![](img/GM1O4WRaQAEcDn2-2.jpg)  

---
<a name="underthedappledsun08"></a>  
###  阳光少女 最终话(1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMPiViaakAAV8pc.jpg "现场拍照，右侧图@X")
](https://pbs.twimg.com/media/GMPiViaakAAV8pc?format=jpg&name=4096x4096) 
[![](img/thumb/GZAxvYRbsAAvPAB.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GZAxvYRbsAAvPAB?format=jpg&name=4096x4096) 

细节：  

- 右侧配文的英文版显示了作品的英文名：“Dappled sunlight”。  

- 从边框处看，第1分格(panel)是粘贴上去的。  
![](img/GMPiViaakAAV8pc-4.jpg) 
![](img/GMPiViaakAAV8pc-5.jpg) 

- 背景的树木是用网点纸粘贴上去的。  
![](img/GMPiViaakAAV8pc-6.jpg) 

- 头部似乎有一条亮线(红箭头所示)。[疑问]这是什么？    
![](img/GMPiViaakAAV8pc-7.jpg) 
![](img/GMPiViaakAAV8pc-8.jpg) 
![](img/GMPiViaakAAV8pc-9.jpg) 

- 飞舞的樱花瓣和女主周围有网点纸的痕迹。    
![](img/GMPiViaakAAV8pc-10.jpg) 
![](img/GMPiViaakAAV8pc-11.jpg)  
![](img/GMPiViaakAAV8pc-12.jpg) 
![](img/GMPiViaakAAV8pc-13.jpg) 


---  

<a name="underthedappledsun09"></a>  
###  阳光少女 最终话(199x)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMPiViaakAAV8pc.jpg "现场拍照，左侧图@X")
](https://pbs.twimg.com/media/GMPiViaakAAV8pc?format=jpg&name=4096x4096) 
[![](img/thumb/GZAxvYRbAAQsbf9.jpg "现场拍照，左侧图@X")
](https://pbs.twimg.com/media/GZAxvYRbAAQsbf9?format=jpg&name=4096x4096) 

细节：  

- 配图标记有如下文字。[疑问]这些标记是什么意思？    
![](img/GZAxvYRbAAQsbf9-0.jpg) 
![](img/GZAxvYRbAAQsbf9-1.jpg)  

- 网点纸的边缘。  
![](img/GMPiViaakAAV8pc-0.jpg) 
![](img/GMPiViaakAAV8pc-1.jpg)  
![](img/GMPiViaakAAV8pc-2.jpg) 
![](img/GMPiViaakAAV8pc-3.jpg) 


---

---
<a name="rash"></a>  
## RASH!!  (1994～1995)

<a name="rash-note"></a>  
### 解说文字  
[![](./img/thumb/GZmRYLmaUAAsxJj.jpg "@X")
](https://pbs.twimg.com/media/GZmRYLmaUAAsxJj?format=jpg&name=4096x4096) 

> **RASH!!**  
**ラッシュ!!**  
週刊少年ジャンプ 1994年43号 - 1995年9号  
周刊少年Jump 1994年43号 - 1995年9号  

> 帝都医科大学から故郷にある刑務所の刑務医となるため戻ってきた女医・朝霞勇希。「rash(無鉄砲・むこうみず)」な彼女は、所内のトラブルに首を突っ込みながら、患者の体だけでなく心の病も治そうとしていく。  
为了成为故乡监狱的刑务医而从帝都医科大学返回的女医朝霞勇希。「rash（鲁莽）」的她，一头扎进狱内的纠纷中，不仅治疗患者的身体，还想治愈患者的心病。  


---
<a name="rash01"></a>  
### RASH!! 第1话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMslm-DbAAAMAvc.jpg "可能是现场拍照@X")  
](https://pbs.twimg.com/media/GMslm-DbAAAMAvc?format=jpg&name=4096x4096)

细节：   

(未找到足够大的图片以查看其细节。)  


---
<a name="rash02"></a>  
### RASH!! 第1话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLXoQtwawAAiQ5V.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLg0WJLaUAAr205?format=jpg&name=4096x4096)
[![](img/thumb/GZmRYLPaMAA_y5T.jpg "现场拍照，右侧图@X")
](https://pbs.twimg.com/media/GZmRYLPaMAA_y5T?format=jpg&name=4096x4096) 


细节：   

- 文字大小不一。文字越大可能意味着语气越强。  
![](./img/GZmRYLPaMAA_y5T-2.jpg) 

- 遮挡角色嘴部的栏杆被渐进地隐藏。补全栏杆(左二)，做个对比。     
![](./img/GZmRYLPaMAA_y5T-1.jpg) 
![](./img/GZmRYLPaMAA_y5T-1m.jpg)  
从效果来看，渐隐栏杆后似乎并未增加美感。[疑问]为何要渐隐遮面部的栏杆？  

- 蓝线示意阴影边缘。  
![](img/GLXoQtwawAAiQ5V-0.jpg) 

- 头发的高光。  
![](img/GLXoQtwawAAiQ5V-1.jpg) 

- 发丝深浅不一。  
![](img/GLXoQtwawAAiQ5V-2.jpg) 

---
<a name="rash03"></a>  
### RASH!! 第1话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GZmRYLObkAAth0H.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GZmRYLObkAAth0H?format=jpg&name=4096x4096) 
[![](img/thumb/GZmRYLPaMAA_y5T.jpg "现场拍照，左侧图@X")
](https://pbs.twimg.com/media/GZmRYLPaMAA_y5T?format=jpg&name=4096x4096) 

细节：   

- 原稿边缘的注释。[疑问]这些文字是什么意思？  
![](./img/GZmRYLObkAAth0H-15.jpg) 
![](./img/GZmRYLObkAAth0H-16.jpg) 

- 遮挡角色嘴部的栏杆被渐进地隐藏。渐隐部分涂抹白色颜料。    
![](./img/GZmRYLObkAAth0H-0.jpg) 
![](./img/GZmRYLObkAAth0H-0g.jpg) 
![](./img/GZmRYLPaMAA_y5T-0.jpg) 
![](./img/GZmRYLPaMAA_y5T-0g.jpg) 

- 头发：  
![](./img/GZmRYLObkAAth0H-1.jpg) 
![](./img/GZmRYLObkAAth0H-2.jpg) 
![](./img/GZmRYLObkAAth0H-3.jpg) 
![](./img/GZmRYLObkAAth0H-4.jpg) 

- 眼部：  
![](./img/GZmRYLObkAAth0H-5.jpg) 
![](./img/GZmRYLObkAAth0H-6.jpg) 

- 嘴部。下嘴唇有高光。    
![](./img/GZmRYLObkAAth0H-7.jpg) 
![](./img/GZmRYLObkAAth0H-7g.jpg) 

- 衣服的褶皱。  
![](./img/GZmRYLObkAAth0H-8.jpg) 
![](./img/GZmRYLObkAAth0H-9.jpg) 
![](./img/GZmRYLObkAAth0H-10.jpg) 
![](./img/GZmRYLObkAAth0H-11.jpg) 

- 挡风玻璃边缘有白色勾线、左侧涂有白色颜料。  
![](./img/GZmRYLObkAAth0H-12.jpg) 
![](./img/GZmRYLObkAAth0H-12g.jpg) 

- 车座椅涂有白色高光。    
![](./img/GZmRYLObkAAth0H-13.jpg) 
![](./img/GZmRYLObkAAth0H-13g.jpg)  
![](./img/GZmRYLObkAAth0H-14.jpg) 
![](./img/GZmRYLObkAAth0H-14g.jpg) 

- 公交车前面的金属栅栏。    
![](./img/GZmRYLObkAAth0H-17.jpg) 
![](./img/GZmRYLObkAAth0H-17g.jpg) 

- 车灯的反光效果。  
![](./img/GZmRYLObkAAth0H-19.jpg) 
![](./img/GZmRYLObkAAth0H-19g.jpg)  
![](./img/GZmRYLObkAAth0H-18.jpg) 
![](./img/GZmRYLObkAAth0H-18g.jpg) 



---
<a name="rash04"></a>  
### RASH!! 第1话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GZmRYLOaEAALt8B.jpg "现场拍照，右侧图@X")
](https://pbs.twimg.com/media/GZmRYLOaEAALt8B?format=jpg&name=4096x4096) 

细节：   

- 手部姿态。  
![](./img/GZmRYLOaEAALt8B-0.jpg) 

- 膝盖窝写实。  
![](./img/GZmRYLOaEAALt8B-1.jpg) 

- 皮肤紧贴玻璃的效果。  
![](./img/GZmRYLOaEAALt8B-11.jpg) 

- 整页是两色画，但该镜头是单色。可能是为了配合剧情。  
![](./img/GZmRYLOaEAALt8B-2.jpg) 

- 高光。  
![](./img/GZmRYLOaEAALt8B-3.jpg) 
![](./img/GZmRYLOaEAALt8B-3g.jpg) 
![](./img/GZmRYLOaEAALt8B-4.jpg) 
![](./img/GZmRYLOaEAALt8B-4g.jpg) 
![](./img/GZmRYLOaEAALt8B-6.jpg) 
![](./img/GZmRYLOaEAALt8B-6g.jpg) 
![](./img/GZmRYLOaEAALt8B-7.jpg) 
![](./img/GZmRYLOaEAALt8B-7g.jpg) 

- 面部。  
![](./img/GZmRYLOaEAALt8B-5.jpg) 

- 栅栏写实。对比其高光。  
![](./img/GZmRYLOaEAALt8B-8.jpg) 
![](./img/GZmRYLOaEAALt8B-8g.jpg) 
![](./img/GZmRYLOaEAALt8B-9.jpg) 
![](./img/GZmRYLOaEAALt8B-9g.jpg)  

- 该建筑的房檐写实。对比其高光。  
![](./img/GZmRYLOaEAALt8B-10.jpg) 
![](./img/GZmRYLOaEAALt8B-10g.jpg) 


---
<a name="rash05"></a>  
### RASH!! 第1话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMslm-CacAAMMVJ.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMslm-CacAAMMVJ?format=jpg&name=4096x4096) 
[![](img/thumb/GaKTtZsaQAAfXFK.jpg "现场拍照，右侧图@X")
](https://pbs.twimg.com/media/GaKTtZsaQAAfXFK?format=jpg&name=4096x4096) 

说明文字：  
>**北条先生に聞きました!**  
**北条老师问答!**  

> Q:週刊少年ジャンプでは最後の連載になった作品ですが、当時の状況などお聞かせください。  
这是《周刊少年Jump》最后连载的作品，请谈一下当时的状况。  

> 「こもれ陽の下で・・・」を好きなように描かせてもらった事もあり、次は編集部の意見を全面的に取り入れてみようと思い描いた作品です。  
気に入らないアイデアも取り入れて描いてみたり、色々な意味で学べた作品でもあります(笑)。  
作画面ではクリアーな漫画として調和のとれた表現ができた作品にも感じています。  
「阳光少女」是我按照自己喜欢的方式画的作品，接下来想要全面采纳编辑部的意见。  
我试着画出我不喜欢的想法，也是从各种意义上学习的作品（笑）。  
我也觉得这部作品能够在银幕上和谐地表达自己，成为一部清晰的漫画。    

细节：   

- 眉目传神。  
![](img/GMslm-CacAAMMVJ-0.jpg) 

- 针线的细线边沿涂抹了白色颜料。在与背景线条的交叉处，这有助于显示遮挡关系。  
![](img/GMslm-CacAAMMVJ-2.jpg) 
![](img/GMslm-CacAAMMVJ-1.jpg)  
![](img/GMslm-CacAAMMVJ-10.jpg) 
![](img/GMslm-CacAAMMVJ-11.jpg) 

- 本页镜头4的右下角的细节。疑似在白色颜料上加了网点纸。    
![](img/GMslm-CacAAMMVJ-3.jpg) 

- 排线犀利。  
![](img/GMslm-CacAAMMVJ-4.jpg) 
![](img/GMslm-CacAAMMVJ-5.jpg) 
![](img/GMslm-CacAAMMVJ-12.jpg) 

- 表盘上的白线深浅不一。  
![](img/GMslm-CacAAMMVJ-6.jpg) 
![](img/GMslm-CacAAMMVJ-7.jpg) 

- 网点纸覆盖了衣服线条和拟声词。所以，网点纸为于在制作流程的后期。  
![](img/GMslm-CacAAMMVJ-8.jpg) 

- 方向盘处的着色。  
![](img/GMslm-CacAAMMVJ-9.jpg) 

- 台词上的补丁。  
![](img/GMslm-CacAAMMVJ-13.jpg) 

- 脚部有白色颜料。  
![](img/GMslm-CacAAMMVJ-14.jpg) 
![](img/GMslm-CacAAMMVJ-17.jpg) 

- 驾驶座上的着色。  
![](img/GMslm-CacAAMMVJ-15.jpg) 

- 仰视角色时，下颌处用排线阴影修饰，而没有硬边线。    
![](img/GMslm-CacAAMMVJ-16.jpg) 


---
<a name="rash06"></a>  
### RASH!! 第1话 (1994)  
[![](./img/thumb/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x.webp "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x.jpg) 
[![](./img/thumb/fukusei-rash-02_4800x.jpg "@edition-88")
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/fukusei-rash-02_4800x.jpg) 
[![](img/thumb/GaKTtZsaQAAfXFK.jpg "现场拍照，左侧图@X")
](https://pbs.twimg.com/media/GaKTtZsaQAAfXFK?format=jpg&name=4096x4096) 

说明文字：  
（同上图）  

细节：  

- 背景特效线条流畅、且长度长。线条流畅意味着每条线是一笔画出。同时，线条不是直线，而是有弧度。[疑问]作者如何一笔画出这么长的流畅曲线？  
![](./img/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x-4.jpg) 

- 手部的线条。似乎能看出作者的笔力重。    
![](./img/fukusei-rash-02_4800x-8.jpg) 
![](./img/fukusei-rash-02_4800x-8gamma.jpg) 

- 发丝：  
![](./img/fukusei-rash-02_4800x-1.jpg) 
![](./img/fukusei-rash-02_4800x-0.jpg)  
![](./img/fukusei-rash-02_4800x-9.jpg) 
![](./img/fukusei-rash-02_4800x-10.jpg) 

- 眼部细节。  
![](./img/fukusei-rash-02_4800x-11.jpg) 

- 女主用脚攻击对手时，鞋底和鞋跟处的排线以示动感。  
![](./img/fukusei-rash-02_4800x-5.jpg) 
![](./img/fukusei-rash-02_4800x-4.jpg) 

- 对手头部受到猛击时的动感。  
![](./img/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x-1.jpg)  
![](./img/fukusei-rash-02_4800x-7.jpg) 
![](./img/fukusei-rash-02_4800x-6.jpg) 

- 表示速度的横向特效线条覆盖了背景里的竖向特效线条。用白色间隔表明遮挡关系。但看不出该白色间隔使用的是白色颜料，[疑问]这里的白色间隔是如何画的？  
![](./img/fukusei-rash-02_4800x-2.jpg) 
![](./img/fukusei-rash-02_4800x-2-gamma.jpg) 

- 衣服的褶皱。  
![](./img/fukusei-rash-02_4800x-21.jpg) 

- 拟声词的边线。可能是被左侧的网点纸覆盖了一部分。  
![](./img/fukusei-rash-02_4800x-16.jpg) 
![](./img/fukusei-rash-02_4800x-16gamma.jpg) 


- 腿部线条流畅。打斗时肌肉可能紧绷，进而棱角分明。但这里腿部棱角不分明，[疑问]为何如此？这符合事实？还是艺术化处理？  
![](./img/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x-0.jpg) 

- 大量排线。  
![](./img/fukusei-rash-02_4800x-23.jpg) 
![](./img/fukusei-rash-02_4800x-22.jpg) 
![](./img/fukusei-rash-02_4800x-13.jpg) 
![](./img/fukusei-rash-02_4800x-12.jpg) 
![](./img/fukusei-rash-02_4800x-3.jpg) 
![](./img/fukusei-rash-02_4800x-15.jpg) 
![](./img/fukusei-rash-02_4800x-14.jpg) 
![](./img/fukusei-rash-02_4800x-18.jpg) 
![](./img/fukusei-rash-02_4800x-17.jpg)  
![](./img/fukusei-rash-02_4800x-20.jpg) 
![](./img/fukusei-rash-02_4800x-19.jpg) 
![](./img/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x-3.jpg) 
![](./img/fukusei-rash-02_4800x-25.jpg) 
![](./img/fukusei-rash-01_efb07693-cc9a-4eb6-88af-9c4ee798fb4e_4800x-2.jpg) 
![](./img/fukusei-rash-02_4800x-24.jpg) 


---
<a name="rash07"></a>  
### RASH!! 第1话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GaKTtZtbcAA3YAI.jpg "现场拍照，右侧图@twitter") 
](https://pbs.twimg.com/media/GaKTtZtbcAA3YAI?format=jpg&name=4096x4096) 

细节：  

- 嘴部线条的留白。  
![](./img/GaKTtZtbcAA3YAI-0.jpg) 
![](./img/GaKTtZtbcAA3YAI-1.jpg) 


---
<a name="rash08"></a>  
### RASH!! 第1话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMIkP62aUAA6qC3.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GMIkP62aUAA6qC3?format=jpg&name=4096x4096)
[![](img/thumb/GMpBzHBbMAEfct2.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GMpBzHBbMAEfct2?format=jpg&name=4096x4096) 
[![](img/thumb/GaKTtZubYAEZBnW.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GaKTtZubYAEZBnW?format=jpg&name=4096x4096) 
[![](img/thumb/GaKTtZtbcAA3YAI.jpg "现场拍照，左侧图@twitter") 
](https://pbs.twimg.com/media/GaKTtZtbcAA3YAI?format=jpg&name=4096x4096) 

细节：     

- 眼部有白色颜料修补的痕迹。  
![](img/GMpBzHBbMAEfct2-5.jpg) 
![](img/GMpBzHBbMAEfct2-6.jpg) 

- 多层网点纸。  
![](img/GMIkP62aUAA6qC3-0.jpg) 

- 背景的光斑效果，且有扭曲效果。[疑问]这是如何制作或绘画的？  
![](img/GMpBzHBbMAEfct2-0.jpg) 
![](img/GaKTtZubYAEZBnW-0.jpg) 

- 对于发丝、鼻梁、手指的线条，只在线条上加了网点纸。因为线条很细，所以这真是让人难以置信！  
![](img/GMpBzHBbMAEfct2-1.jpg) 
![](img/GMpBzHBbMAEfct2-2.jpg) 
![](img/GMpBzHBbMAEfct2-3.jpg) 

- 手指上的擦伤效果，使用了网点纸。  
![](img/GMpBzHBbMAEfct2-4.jpg) 

- 左侧衣服的着色(网点纸)浅。  
![](img/GMpBzHBbMAEfct2-7.jpg) 

- 衣物和头发上喷洒了白色颜料斑点。  
![](img/GMpBzHBbMAEfct2-8.jpg) 

---
<a name="rash09"></a>  
### RASH!! 第1话 (1994)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GNAUMPVbcAQ76NY.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNAUMPVbcAQ76NY?format=jpg&name=4096x4096) 
[![](img/thumb/GaKTtZsaMAAmbc8.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GaKTtZsaMAAmbc8?format=jpg&name=4096x4096) 

细节：  

- 月亮上的汗滴。  
![](img/GaKTtZsaMAAmbc8-1.jpg) 

- 车灯的辉光。    
![](img/GNAUMPVbcAQ76NY-0.jpg) 
![](img/GNAUMPVbcAQ76NY-1.jpg) 

- 轮胎纹理逼真。  
![](img/GNAUMPVbcAQ76NY-0.jpg) 

- 云的边缘处涂抹有白色颜料。    
![](img/GNAUMPVbcAQ76NY-2.jpg) 
![](img/GNAUMPVbcAQ76NY-3.jpg) 

- 网点纸和白色颜料。  
![](./img/GaKTtZsaMAAmbc8-0.jpg) 
![](./img/GaKTtZsaMAAmbc8-0g.jpg) 


---

---
<a name="splash5"></a>  
## SPLASH! 5 (1995)[^hojocn]

---

---

<a name="fcompo"></a>  
## F.COMPO  (1996～2000)

<a name="fcompo-note"></a>  
### 解说文字  
[![](img/thumb/GPEV8e9aMAAJHzE.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GPEV8e9aMAAJHzE?format=jpg&name=4096x4096)
[![](img/thumb/447277372_420903664144732_3595368693293587572_n.jpg "现场拍照@X")
](https://scontent.cdninstagram.com/v/t51.29350-15/447277372_420903664144732_3595368693293587572_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=104&_nc_ohc=gwx4TO5JV-IQ7kNvgEyuKTg&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzY1NjE3OA%3D%3D.3-ccb7-5&oh=00_AYA034-KKstH6t6I_53oxcuhyxvMV2zD-l_elN8vjp9f4g&oe=6727A04E&_nc_sid=10d13b)  

  
> F.COMPO   
**ファミリー・プロット**  
**Family Compo**    
MANGAオールマン 1996年第10号 - 2000年23号  
MANGA ALLMAN 1996年第10号 - 2000年23号

> 両親を失い、親戚の家に居侯することになった大学生・雅彦。彼が身を寄せる若苗家は女性のような父・紫と男性のような母・空、そして性別のわからない娘(?)の紫苑という男女逆転家庭だった。青年誌での初連載作にして、「家族愛」という北条の作品テーマが色濃く反映されたハートウォーミングコメディ。  
失去双亲，住在亲戚家的大学生雅彦。他栖身的若苗家是像女性一样的父亲·紫和像男性一样的母亲·空，和性别不明的女儿（?）紫苑的男女逆转家庭。作为青年杂志的首次连载作品，“家庭溫暖”这一北条作品主题被浓厚地反映出来的暖心喜剧。  

>**北条先生に聞きました!**  
**北条老师问答!**  

> Q:現代でいうLGBTQなどを先取りしたと言われる事もある作品ですが、構想のきっかけなどを教えてください。  
这部作品被认为是LGBTQ等群体的先驱，请告诉我们它的构想是什么。  

> 当時は、おかまタレントさんがテレビのパラエティー番組などで活躍していて、おかまの結婚式という企画がありました。それを見ていたら、このカップルは子供が欲しかったら養子緣組するしかないのかな、でもおかまとおなべのふたりが結婚したら子供は出来るのかなと...単純な思いつきですが、そこから作品の着想が生まれました。  
当时，跨性別女性[^transwomen]艺人活跃在电视综艺节目中，有跨性別女性的婚礼企划。看到这个，我就在想，这对情侣如果想要孩子的话，就只能领养孩子了吧，但是男同的两人结婚的话，能不能生孩子呢…虽然只是一个简单的想法，但却由此产生了作品的构思。  

[^transwomen]:  
[人妖](https://mytransgenderdate.com/zh-tw/definitions#人妖), [何謂人妖，為何要避免使用這個詞？](https://myladyboydate.com/blog/2020/04/what-is-a-shemale)

> またそんな両親を見て育った子供は、人間って男にも女にも自由になれるんだと思っていても不思議は無いかなあと思い、面白い作品になリそうな手応えを感にました。  
另外，看着这样的父母长大的孩子，觉得人不管是男是女都可以自由自在也没什么不可思议的吧，感觉会成为有趣的作品。  

> 最初は週刊少年ジャンプでの連載の提案をしたのですが、2回ポツになり、最終的に编集長から、週刊少年ジャンプでは連載するのが難しいテーマですと言われました。  
そこで新創刊した青年誌で連載する事になりました。  
最初提议在《周刊少年Jump》上连载，但两次被取消，最后主编说这是很难在《周刊少年Jump》上连载的主题。  
于是决定在新创刊的青年杂志上连载。  

> Q:初の青年誌といら事で、作画面で心掛けた点などごぎいますか。  
最初的青年杂志，在作画方面有什么特别的地方吗？  

> 青年誌だから...という事は持にありませんでしたが、ホ一ムドフマに近い作品ですので、シティーハンターのような絵では合わないと考えました。  
頭身も10頭身とか12頭身みたいにはせす、出來るだけ自然な体型にして、ポーズにしてもかつニよ...  (注：后续文字看不清)  
因为是青年杂志…我并没有这样的想法，但我认为这是一幅与ホ一ムドフマ相近的作品，不适合画城市猎人那样的画。  
头身也要像10头身或12头身那样，身体要尽量保持自然，即使是姿势...（注：后续文字看不清）  

---
<a name="fcompo01"></a>  
### FC 第1卷 封面 (1996.05)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPEV8j9bAAEh4nx.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GPEV8j9bAAEh4nx?format=jpg&name=4096x4096) 
[![](img/thumb/GP4kBika4AAceZb.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GP4kBika4AAceZb?format=jpg&name=4096x4096) 

细节：     

- 头发。  
![](img/GP4kBika4AAceZb-0.jpg) 
![](img/GP4kBika4AAceZb-1.jpg) 
![](img/GP4kBika4AAceZb-2.jpg) 
![](img/GP4kBika4AAceZb-3.jpg) 
![](img/GP4kBika4AAceZb-4.jpg) 
![](img/GP4kBika4AAceZb-5.jpg) 

- 衣服褶皱。  
![](img/GP4kBika4AAceZb-6.jpg) 
![](img/GP4kBika4AAceZb-7.jpg) 
![](img/GP4kBika4AAceZb-8.jpg) 
![](img/GP4kBika4AAceZb-11.jpg) 
![](img/GP4kBika4AAceZb-12.jpg) 
![](img/GP4kBika4AAceZb-13.jpg) 
![](img/GP4kBika4AAceZb-14.jpg) 
![](img/GP4kBika4AAceZb-15.jpg) 

- 鞋尖有白色高光。  
![](img/GP4kBika4AAceZb-9.jpg) 
![](img/GP4kBika4AAceZb-10.jpg) 


---
<a name="fcompo02"></a>  
### FC 第1话  (1996)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPr5ysHaIAETVtL.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GPr5ysHaIAETVtL?format=jpg&name=4096x4096) 
[![](img/thumb/Ga4W4BnaMAAOiQt.jpg "现场拍照，左上左侧@twitter") 
](https://pbs.twimg.com/media/Ga4W4BnaMAAOiQt?format=jpg&name=4096x4096) 

细节：     

- 深色衣服的着色。  
![](img/GPr5ysHaIAETVtL-0.jpg) 

- 地面的阴影。  
![](img/GPr5ysHaIAETVtL-1.jpg) 
![](img/GPr5ysHaIAETVtL-2.jpg) 

- 头发。  
![](img/GPr5ysHaIAETVtL-6.jpg) 
![](img/GPr5ysHaIAETVtL-7.jpg)  
头发的着色的墨迹。  
![](img/GPr5ysHaIAETVtL-3.jpg) 
![](img/GPr5ysHaIAETVtL-8.jpg) 

- 排线阴影。  
![](img/GPr5ysHaIAETVtL-4.jpg) 
![](img/GPr5ysHaIAETVtL-9.jpg) 
![](img/GPr5ysHaIAETVtL-10.jpg) 

- 胶片的排线着色。  
![](img/GPr5ysHaIAETVtL-5.jpg) 

- 背景光晕的着色墨迹。  
![](img/GPr5ysHaIAETVtL-11.jpg) 


---
<a name="fcompo03"></a>  
### FC 第1话  (1996)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/447689498_964887941764408_4264677107388491346_n.jpg "现场拍照@https://www.instagram.com/p/C7t8Bz7PoRy/") 
](https://scontent.cdninstagram.com/v/t51.29350-15/447689498_964887941764408_4264677107388491346_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=104&_nc_ohc=brCXBupqYT0Q7kNvgEhOgOd&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzgwNzY3MTE1Ng%3D%3D.3-ccb7-5&oh=00_AYDee6420PevIT427zMmYaCAHpEm-WR8Wz8SqMpVx1DtvQ&oe=672796B9&_nc_sid=10d13b) 
[![](img/thumb/Ga4W4BnaMAAOiQt.jpg "现场拍照，左上右侧@twitter") 
](https://pbs.twimg.com/media/Ga4W4BnaMAAOiQt?format=jpg&name=4096x4096) 

细节：     

- 用蓝色勾勒阴影边界。  
![](./img/447689498_964887941764408_4264677107388491346_n0.jpg) 

- 边线处有白色颜料的修正。  
![](./img/447689498_964887941764408_4264677107388491346_n1.jpg) 
![](./img/447689498_964887941764408_4264677107388491346_n1g.jpg) 
![](./img/447689498_964887941764408_4264677107388491346_n2.jpg) 
![](./img/447689498_964887941764408_4264677107388491346_n2g.jpg) 

- 黑色区域的颜色深浅不一。  
![](./img/447689498_964887941764408_4264677107388491346_n3.jpg) 
![](./img/447689498_964887941764408_4264677107388491346_n3g.jpg) 
![](./img/447689498_964887941764408_4264677107388491346_n4.jpg) 
![](./img/447689498_964887941764408_4264677107388491346_n4g.jpg) 

- 分格(panel)外侧贴有白色小纸片。[疑问]这白色小纸片是做什么的？   
![](./img/447689498_964887941764408_4264677107388491346_n5.jpg) 


---
<a name="fcompo04"></a>  
### FC 第5话 (1996)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/Ga4W4BnaMAAOiQt.jpg "现场拍照，左下@twitter")
![](img/thumb/Ga4W4BnaMAAOiQt-dr.jpg "现场拍照，左下@twitter")
](https://pbs.twimg.com/media/Ga4W4BnaMAAOiQt?format=jpg&name=4096x4096) 

细节：  
（未找到足够清晰的大图。）  

---
<a name="fcompo05"></a>  
### FC 第2卷 封面 (1996)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPEV8rLbsAE2Sgm.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GPEV8rLbsAE2Sgm?format=jpg&name=4096x4096) 
[![](img/thumb/447194197_476524268156325_6779590998158583109_n.jpg "现场拍照@https://www.instagram.com/p/C7t8Bz7PoRy/") 
](https://scontent.cdninstagram.com/v/t51.29350-15/447194197_476524268156325_6779590998158583109_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi44NzZ4ODc3LnNkci5mMjkzNTAuZGVmYXVsdF9pbWFnZSJ9&_nc_ht=scontent.cdninstagram.com&_nc_cat=109&_nc_ohc=Xm4AtpoBQwgQ7kNvgEeoKq7&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzgxNjI4NjU3Mg%3D%3D.3-ccb7-5&oh=00_AYCd8acvh62Pa92lvvdfuk-V0BGuqPlZ13yekKIjsnbbKg&oe=67278FC8&_nc_sid=10d13b) 

细节：     

- 右側配图似乎有红色注解。但未找到足够大的图片以查看其细节。 


---
<a name="fcompo06"></a>  
### FC 2卷 13话  (1997.02)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GQdosvtaIAMttrL.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GQdosvtaIAMttrL?format=jpg&name=4096x4096)

细节：     
(未找到足够大的图片以查看其细节。)  

---
<a name="fcompo07"></a>  
### FC 第40话  (1998.03)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPEV86AbgAAx784-0.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GPEV86AbgAAx784?format=jpg&name=4096x4096)

细节：     
(未找到足够大的图片以查看其细节。)  

---
<a name="fcompo08"></a>  
### FC 第70话 “紫苑的诞生” (1999.06)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPEV86AbgAAx784-1.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GPEV86AbgAAx784?format=jpg&name=4096x4096)
[![](img/thumb/Ga4W4BnaMAAOiQt.jpg "现场拍照，右上@twitter")
](https://pbs.twimg.com/media/Ga4W4BnaMAAOiQt?format=jpg&name=4096x4096) 

细节：     
(未找到足够大的图片以查看其细节。)  


---
<a name="fcompo09"></a>  
### FC 第102话 最终话  (2000.10)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GPEV86AbgAAx784-2.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GPEV86AbgAAx784?format=jpg&name=4096x4096) 
[![](img/thumb/447113427_1779870629168580_2289813680382420427_n.jpg "现场拍照@https://www.instagram.com/p/C7t8Bz7PoRy/") 
](https://scontent.cdninstagram.com/v/t51.29350-15/447113427_1779870629168580_2289813680382420427_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=100&_nc_ohc=OS1KxcGAJsQQ7kNvgEnrIdS&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzY5Mzk3OA%3D%3D.3-ccb7-5&oh=00_AYC2N18uM0L6ECs-U4qAII01N5VddIj6OcGsBSxCxHWwUA&oe=67278E3A&_nc_sid=10d13b) 
[![](img/thumb/Ga4W4BnaMAAOiQt.jpg "现场拍照，右下@twitter")
](https://pbs.twimg.com/media/Ga4W4BnaMAAOiQt?format=jpg&name=4096x4096) 

细节：     

- 花瓣边缘内侧涂有白色颜料。  
![](./img/447113427_1779870629168580_2289813680382420427_n0.jpg) 
![](./img/447113427_1779870629168580_2289813680382420427_n0g.jpg) 

- 边线处有白色颜料的修正。  
![](./img/447113427_1779870629168580_2289813680382420427_n1.jpg) 

- 分格(panel)外侧贴有白色小纸片。[疑问]这白色小纸片是做什么的？  
![](./img/447113427_1779870629168580_2289813680382420427_n2.jpg) 



-----  

-----  
<a name="angelheart"></a>  
## エンジェル・ハート  (Angel Heart) (2001-2017)

<a name="angelheart-note"></a>  
### 解说文字: 
[![](img/thumb/Gaoi37KboAAxAQY-0.jpg "现场拍照@X")
](https://pbs.twimg.com/media/Gaoi37KboAAxAQY?format=jpg&name=4096x4096) 

> **工ンジェノレ・ハ一ト**  
**Angel Heart**  

> 週刊コミックバンチ 2001年1号 - 2010年36-37合併号  
月判コミックゼノン 2010年12月号 - 2017年7月号  
周刊ComicBunch 2001年1号 - 2010年36-37合并号  
月判ComicZenon 2010年12月号 - 2017年7月号  

> 香の心臓を通して“親子”となった獠と香瑩(ッヤンイン）。シテイハンターとして依頼人たちを助けながら、彼ら自身もまた家族として成長していく"......。月刊コミックゼノン創刊に合わせて2ndシ一ズンへと交人した、新しいシテイハンタ一の物語。  
通过香的心脏成为“亲子”的獠和香莹。作为CityHunter帮助委托人们的同时，他们自己也作为家人而成长"......。月刊ComicZenon创刊之际，与2nd一尊交人的新指定Hunter的故事。  

> **北条先生に聞きました! **  
**北条老师问答!**    

> Q:「シティーハンター」のセルフリメイク的作品の、「エンジェル・ハート」についてお[^unocr]聞[^unocr]かせください。  
问：请说说「CityHunter」的Self-remake作品「Angel Heart」。  

> 「シティーハンター」が少し尻切れトンポ的に終わった事もあり、もう一度冴羽獠を描[^unocr]いてみたい気持ちがありました。  
週刊コミックパンチ創刊のための新連載企画を考えていた時に、編集部からは「眠狂四郎」（柴田錬三郎の小說）を提案きれましたがしつくり来なくて。シティーハンターの続編的な作品を提案しましたが、連載終了から10年近くもの時が経っているし、それに伴って時代背景も変わってしまっているので、続編というよりもセルプリメイクという形の作品になりました。シティーハンターが終わってからの自分の中に、獠をもっと描きたいという思いがずっとあり、獠の深いところにある感情を引き出すために描いた作品です。悔いなくやり切れたと思っています。  
「CityHunter」结束得有点不过瘾，所以我想再一次描绘冴羽獠。  
在考虑周刊ComicBunch创刊的新连载企划的时候，编辑部提出了《眠狂四郎》（柴田炼三郎的小说）的提案，但是没有做出来。提出了CityHunter的续篇作品，但连载结束已经过了近10年，时代背景也发生了变化，所以与其说是续篇，不如说是Self-remake形式的作品。CityHunter结束后，我一直想再画一部獠，为了引出獠内心深处的感情而画的作品。我觉得已经无怨无悔地完成了。  

[^unocr]: 该字看不清，识别可信度低。  


---  
<a name="angelheart01"></a>  
### AH 第1卷 扉页 (2001.0?)     
[![](img/thumb/GQkca4QaEAA099J.jpg "现场拍照，左下图，右下图@X")
](https://pbs.twimg.com/media/GQkca4QaEAA099J?format=jpg&name=4096x4096) 

细节：  

- 头发。  
![](img/GQkca4QaEAA099J-0.jpg) 
![](img/GQkca4QaEAA099J-1.jpg) 
![](img/GQkca4QaEAA099J-2.jpg) 
![](img/GQkca4QaEAA099J-3.jpg) 

- 面部细节。  
![](img/GQkca4QaEAA099J-4.jpg) 
![](img/GQkca4QaEAA099J-5.jpg) 
![](img/GQkca4QaEAA099J-5g.jpg) 
![](img/GQkca4QaEAA099J-6.jpg) 
![](img/GQkca4QaEAA099J-7.jpg) 

---  
<a name="angelheart02"></a>  
### AH 第1卷 扉页 (2001.03)     
[![](img/thumb/GO-gcKobMAAlubr.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GO-gcKobMAAlubr?format=jpg&name=4096x4096) 
[![](img/thumb/GO-gcM2bsAA9DN7.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GO-gcM2bsAA9DN7?format=jpg&name=4096x4096) 
[![](img/thumb/GOfVN86bYAAH8nC.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GOfVN86bYAAH8nC?format=jpg&name=4096x4096) 

细节：  

- 头发的细节。  
![](img/GO-gcKobMAAlubr-0.jpg) 
![](img/GO-gcKobMAAlubr-1.jpg)  
发尖似乎有模糊（下图左二）：  
![](img/GO-gcKobMAAlubr-10.jpg) 
![](img/GO-gcKobMAAlubr-11.jpg) 

- 更近距离看头发的细节。墨迹在灯光下反光。    
![](img/GO-gcM2bsAA9DN7-2.jpg) 
![](img/GO-gcM2bsAA9DN7-1.jpg) 
![](img/GO-gcM2bsAA9DN7-0.jpg) 

- 眼部细节。  
![](img/GO-gcKobMAAlubr-2.jpg) 
![](img/GO-gcKobMAAlubr-2gamma.jpg) 

- Glass Heart 胸前伤疤的细节。  
![](img/GO-gcM2bsAA9DN7-8.jpg) 

- 服饰着色的渐变均匀。  
![](img/GO-gcKobMAAlubr-5.jpg) 
![](img/GO-gcKobMAAlubr-6.jpg)  
细节：  
![](img/GO-gcM2bsAA9DN7-6.jpg) 
![](img/GO-gcM2bsAA9DN7-7.jpg) 

- 蕾丝边缘区域的墨色重。  
![](img/GOfVN86bYAAH8nC-3.jpg) 
![](img/GOfVN86bYAAH8nC-4.jpg)  
相对于边缘区域，蕾丝内部区域的墨色浅。  
![](img/GOfVN86bYAAH8nC-5.jpg) 
![](img/GOfVN86bYAAH8nC-6.jpg) 
![](img/GOfVN86bYAAH8nC-7.jpg)  
边缘区域与内部区域的交界处，疑似有剪贴的痕迹。浅色纹理在上层。      
![](img/GOfVN86bYAAH8nC-0.jpg) 
![](img/GOfVN86bYAAH8nC-1.jpg) 
![](img/GOfVN86bYAAH8nC-2.jpg)  
我猜，蕾丝内部区域的纹理画在半透明纸上，然后剪贴到画作上。  

- 蕾丝内部区域的纹理精致、逼真。  
![](img/GOfVN86bYAAH8nC-5.jpg) 
![](img/GOfVN86bYAAH8nC-6.jpg) 
![](img/GOfVN86bYAAH8nC-7.jpg)  
[疑问]是否有可能将实物的纹理复印而得到该纹理？  

- 边缘区域也有少数浅墨色的线条。    
![](img/GOfVN86bYAAH8nC-4.jpg)  

- 蕾丝边缘的毛刺。  
![](img/GO-gcM2bsAA9DN7-9.jpg) 

- 枪械着色逼真。枪管中的螺纹可见。  
![](img/GO-gcKobMAAlubr-3.jpg) 
![](img/GO-gcKobMAAlubr-4.jpg) 
![](img/GO-gcKobMAAlubr-4gamma.jpg) 

- 前景人物有错位，可能是运动模糊效果。  
![](img/GO-gcKobMAAlubr-16.jpg)  
错位的细节（应该是覆盖了半透明纸）：  
![](img/GO-gcM2bsAA9DN7-4.jpg) 
![](img/GO-gcM2bsAA9DN7-5.jpg) 

- 玻璃破碎。破碎纹理逼真。大部分看不出白色颜料涂抹痕迹。   
![](img/GO-gcKobMAAlubr-7.jpg) 
![](img/GO-gcKobMAAlubr-7gamma.jpg) 
![](img/GO-gcKobMAAlubr-8.jpg) 
![](img/GO-gcKobMAAlubr-8gamma.jpg) 
![](img/GO-gcKobMAAlubr-9.jpg) 
![](img/GO-gcKobMAAlubr-9gamma.jpg) 
![](img/GO-gcKobMAAlubr-15.jpg) 
![](img/GO-gcKobMAAlubr-15gamma.jpg)  
更近距离能看出白色颜料涂抹痕迹：  
![](img/GO-gcM2bsAA9DN7-3.jpg) 
![](img/GO-gcM2bsAA9DN7-3gamma.jpg) 

- 喷洒的墨点。  
![](img/GO-gcKobMAAlubr-12.jpg) 
![](img/GO-gcKobMAAlubr-13.jpg) 
![](img/GO-gcKobMAAlubr-14.jpg) 


---
<a name="angelheart03"></a>  
### AH 第30话 Title Page (2001.12)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/446118128_768511995473483_4509225350457071389_n.jpg "现场拍照@instagram") 
](https://www.instagram.com/explore/tags/北条司)
[![](img/thumb/GQF-NJubEAAHgSI.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GQF-NJubEAAHgSI?format=jpg&name=4096x4096)

细节：   

- 背景。  
![](img/GQF-NJubEAAHgSI-0.jpg) 
![](img/GQF-NJubEAAHgSI-1.jpg) 

- 头发。  
![](img/GQF-NJubEAAHgSI-2.jpg) 

- 服饰褶皱。  
![](img/GQF-NJubEAAHgSI-3.jpg) 

- 靴子上的高光。  
![](img/GQF-NJubEAAHgSI-4.jpg) 
![](img/GQF-NJubEAAHgSI-4g.jpg) 

- 背景人物的面部。  
![](img/GQF-NJubEAAHgSI-5.jpg) 
![](img/GQF-NJubEAAHgSI-6.jpg) 


---
<a name="angelheart04"></a>  
### AH 第7话  (2002.x)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GQvs5FzbsAAzBo5-0.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GQvs5FzbsAAzBo5?format=jpg&name=4096x4096) 

细节：   

- 左侧页面：分格1为黑白(草稿？)，其余分格为彩色。  
右侧页面：分格1为彩色，其余分格为黑白(草稿？)。   
[疑问]这样的展示想说明什么？  
![](img/GQvs5FzbsAAzBo5-1.jpg) 
![](img/GQvs5FzbsAAzBo5-2.jpg) 

---
<a name="angelheart05"></a>  
### AH 第？话 Title Page (2003.06)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GP4kA0pbIAINPlN.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GP4kA0pbIAINPlN?format=jpg&name=4096x4096) 
[![](img/thumb/GQVQ_tWaIAAbij3.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GQVQ_tWaIAAbij3?format=jpg&name=4096x4096) 
[![](img/thumb/GaFGwLfb0AIrTH8.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GaFGwLfb0AIrTH8?format=jpg&name=4096x4096) 
[![](img/thumb/GOOiAe4bQAAkeUQ.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GOOiAe4bQAAkeUQ?format=jpg&name=4096x4096)

细节：   

- 喷洒的白色颜料。  
![](img/GP4kA0pbIAINPlN-0.jpg) 
![](img/GP4kA0pbIAINPlN-0g.jpg) 

- 头发。  
![](img/GP4kA0pbIAINPlN-1.jpg) 
![](img/GP4kA0pbIAINPlN-7.jpg) 
![](img/GP4kA0pbIAINPlN-8.jpg) 
![](img/GP4kA0pbIAINPlN-9.jpg) 

- 眼部。高光似乎不是点的白色颜料。    
![](img/GP4kA0pbIAINPlN-2.jpg) 
![](img/GP4kA0pbIAINPlN-2g.jpg) 

- 耳部的细节。有排线阴影。  
![](img/GP4kA0pbIAINPlN-3.jpg) 

- 褶皱。周围衣服投影的阴影随褶皱起伏：    
![](img/GP4kA0pbIAINPlN-4.jpg) 
![](img/GP4kA0pbIAINPlN-5.jpg) 
![](img/GP4kA0pbIAINPlN-6.jpg) 

- 獠的眼泪。  
![](img/GP4kA0pbIAINPlN-10.jpg) 
![](img/GaFGwLfb0AIrTH8-0.jpg) 
![](img/GaFGwLfb0AIrTH8-1.jpg) 



---
<a name="angelheart07"></a>  
### AH 第100话  (2003.07)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GOPumY1aAAAuCt6.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GOPumY1aAAAuCt6?format=jpg&name=4096x4096) 
[![](img/thumb/441487916_17969220509718230_8058935361559129881_n.jpg "现场拍照@Instagram") 
](https://www.instagram.com/explore/tags/北条司展) 
[![](img/thumb/GQpykamakAIXtMI.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GQpykamakAIXtMI?format=jpg&name=4096x4096) 
[![](img/thumb/GaUeNyMbUAEQNhp.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GaUeNyMbUAEQNhp?format=jpg&name=4096x4096) 
[![](img/thumb/1040g008314cc8640me404a18f2vloj5oa8uolao!nd_dft_wlteh_webp_3.webp "现场拍照@X") 
](https://www.xiaohongshu.com/discovery/item/66777d24000000001c036982?xsec_token=CBHXy0bQRVdICoXMY3K4EnHFqYe_BXeCqyWHmYiXUbaRc=) 


细节：  

- 眉毛的细节。  
![](img/441487916_17969220509718230_8058935361559129881_n-0.jpg) 
![](img/441487916_17969220509718230_8058935361559129881_n-1.jpg) 
![](img/441487916_17969220509718230_8058935361559129881_n-2.jpg) 

- 衣服的褶皱很少用黑色线条，这意味着弱化褶皱的效果。而黑色线条表示边线、织线。    
![](img/441487916_17969220509718230_8058935361559129881_n-3.jpg) 
![](img/441487916_17969220509718230_8058935361559129881_n-4.jpg) 
![](img/441487916_17969220509718230_8058935361559129881_n-5.jpg) 
![](img/441487916_17969220509718230_8058935361559129881_n-6.jpg) 
![](img/441487916_17969220509718230_8058935361559129881_n-7.jpg) 

- 墙壁着色逼真。  
![](img/GOPumY1aAAAuCt6-0.jpg) 
![](img/GOPumY1aAAAuCt6-1.jpg) 
![](img/GOPumY1aAAAuCt6-1gamma.jpg) 
![](img/GOPumY1aAAAuCt6-2.jpg) 
![](img/GOPumY1aAAAuCt6-2gamma.jpg) 
![](img/GOPumY1aAAAuCt6-3.jpg) 
![](img/GOPumY1aAAAuCt6-4.jpg) 

- 地板着色逼真。  
![](img/GOPumY1aAAAuCt6-5.jpg)  
![](img/GOPumY1aAAAuCt6-6.jpg) 

---
<a name="angelheart08"></a>  
### AH 第？话 Title Page (2004.05)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GP4kAzsaAAEtt7S.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GP4kAzsaAAEtt7S?format=jpg&name=4096x4096)
[![](img/thumb/GQLcPXsa0AAy8cC.jpg "现场拍照@twitter") 
](https://pbs.twimg.com/media/GQLcPXsa0AAy8cC?format=jpg&name=4096x4096) 
[![](img/thumb/448920933_491327600013891_8801919280400046273_n.jpg "现场拍照@https://www.instagram.com/p/C8l-nsYyFET/") 
](https://scontent.cdninstagram.com/v/t51.29350-15/448920933_491327600013891_8801919280400046273_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=103&_nc_ohc=gxPuFZ6nWXEQ7kNvgGwiZk7&_nc_gid=ac8dccf67271454195584ccc47006eb0&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTgxNzMxMw%3D%3D.3-ccb7-5&oh=00_AYAhlp6dQNi2yt2cuMsb44-YjrACqi7WJ9mywCNUp_ymWg&oe=6727AC07&_nc_sid=10d13b) 


细节：   

- 头发的细节。  
![](img/GP4kAzsaAAEtt7S-0.jpg) 

- 锁骨处没有硬边线。  
![](img/GP4kAzsaAAEtt7S-6.jpg) 

- 衣服褶皱。  
![](img/GP4kAzsaAAEtt7S-1.jpg) 
![](img/GP4kAzsaAAEtt7S-2.jpg)  
![](img/GP4kAzsaAAEtt7S-3.jpg) 
![](img/GP4kAzsaAAEtt7S-5.jpg) 
![](img/GP4kAzsaAAEtt7S-4.jpg) 



---
<a name="angelheart09"></a>  
### AH 插画 (2008.03)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GQFP2B_aQAA9kDh.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQFP2B_aQAA9kDh?format=jpg&name=4096x4096) 
[![](img/thumb/GQLyg3haUAA_FFG.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQLyg3haUAA_FFG?format=jpg&name=4096x4096) 

细节：  

- 枪械的细节。  
![](img/GQFP2B_aQAA9kDh-0.jpg) 
![](img/GQFP2B_aQAA9kDh-1.jpg) 
![](img/GQFP2B_aQAA9kDh-2.jpg) 

- 指甲的线条不封闭。  
![](img/GQFP2B_aQAA9kDh-2.jpg) 

- 头发。  
![](img/GQFP2B_aQAA9kDh-3.jpg) 
![](img/GQFP2B_aQAA9kDh-9.jpg) 

- 眼部。睫毛和头发之间有白色间隔。    
![](img/GQFP2B_aQAA9kDh-4.jpg)  
獠的嘴唇有淡淡的高光。  
![](img/GQFP2B_aQAA9kDh-10.jpg) 

- 服饰的褶皱和排线。  
![](img/GQFP2B_aQAA9kDh-5.jpg) 
![](img/GQFP2B_aQAA9kDh-6.jpg) 
![](img/GQFP2B_aQAA9kDh-7.jpg) 
![](img/GQFP2B_aQAA9kDh-8.jpg) 
![](img/GQFP2B_aQAA9kDh-11.jpg) 
![](img/GQFP2B_aQAA9kDh-14.jpg)  
![](img/GQFP2B_aQAA9kDh-12.jpg) 
![](img/GQFP2B_aQAA9kDh-13.jpg) 
![](img/GQFP2B_aQAA9kDh-15.jpg) 
![](img/GQFP2B_aQAA9kDh-16.jpg) 
![](img/GQFP2B_aQAA9kDh-17.jpg) 


---
<a name="angelheart10"></a>  
### AH 23卷 扉页 (2009.12)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/446193243_435315232764271_6928286911164269964_n.jpg "现场拍照@instagram") 
](https://www.instagram.com/explore/tags/北条司) 
[![](img/thumb/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3.webp "现场拍照@instagram") 
](https://www.xiaohongshu.com/discovery/item/666eb764000000001d01934d?xsec_token=CBBjwP2i9GqHgiBiCjgXGyvVE0EXTcxizrSAv4nVg_mVA=) 

细节：   

- 牙齿上的高光。  
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-0.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-0g.webp) 

- 白颜料修边。  
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-20.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-20g.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-15.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-15g.webp) 

- 角色头部。冴子、獠、香莹、Miki的发色一致，信宏发色浅。对比香莹、信宏，獠的嘴唇画出了厚度，这可能与侧脸角度有关。这是作者的绘画特点之一。      
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-0.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-2.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-3.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-4.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-5.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-21.webp) 

- 包裹的褶皱逼真。礼带上有白线。    
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-1.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-1g.webp) 

- 毛绒边画有硬边线。这不写实，但似乎是AH的绘画特点。    
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-6.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-7.webp) 

- 毛绒材质的渲染。  
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-6.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-6g.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-16.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-16g.webp) 

- 礼帽的高光逼真。  
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-8.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-8g.webp) 

- 布料褶皱。  
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-10.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-10g.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-9.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-11.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-12.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-13.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-15.webp)  
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-17.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-17g.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-18.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-18g.webp) 

- 花束上的高光。  
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-14.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-14g.webp) 

- 格子纹理。[疑问]这使用了网点纸吗？  
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-16.webp) 
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-16g.webp) 

- 签名似乎是使用软笔。    
![](img/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3-19.webp) 









[^hojocn]:[北条司中文简介 By CatNj v4.0 updated](http://www.hojocn.com/bbs/viewthread.php?tid=3094)

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
