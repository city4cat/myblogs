
## City Hunter 40周年「北条司展」展览信息 & 部分展品（Part 4/5）  

City Hunter 40周年「北条司展」以下简称“画展”。  


---  
<a name="angelheart12"></a>  
### AH2 插画 (2010.06)  
[![](img/thumb/448915927_3759504364367737_7351037500985462730_n.jpg "现场拍照，https://www.instagram.com/p/C8l-nsYyFET/")
](https://scontent-ams4-1.cdninstagram.com/v/t51.29350-15/448915927_3759504364367737_7351037500985462730_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams4-1.cdninstagram.com&_nc_cat=101&_nc_ohc=h-keKsRk3ZcQ7kNvgHiG1NF&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzUyMjQ3MDMxMg%3D%3D.3-ccb7-5&oh=00_AYCnX1UgE_AUTpvma1MYzpcPx0Hw4J_CSQrnsBcp11bOYg&oe=6727887A&_nc_sid=2d3a3f) 

细节：   

- 如果按透视比例，则角色的像应该近大远小。总体来看，海坊主、野上的像被缩小了；Miki的像被放大了。[疑问]野上的像被缩小了？    

- 背景中的排线。  
![](./img/448915927_3759504364367737_7351037500985462730_n13.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n11.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n15.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n16.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n17.jpg) 

- 头发。  
![](./img/448915927_3759504364367737_7351037500985462730_n3.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n7.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n4.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n10.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n9.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n8.jpg) 
    - 变色子（变色龙）的发丝飘逸；且发色更鲜艳，这意味着ta可能是染发。    
    ![](./img/448915927_3759504364367737_7351037500985462730_n0.jpg) 
    ![](./img/448915927_3759504364367737_7351037500985462730_n1.jpg) 
    - 香的头发边缘涂抹白色颜料。  
![](./img/448915927_3759504364367737_7351037500985462730_n31.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n31g.jpg)  
![](./img/448915927_3759504364367737_7351037500985462730_n36.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n36g.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n37.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n37g.jpg) 

- 眼神。  
![](./img/448915927_3759504364367737_7351037500985462730_n7.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n5.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n10.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n2.jpg) 


- 图右侧，眼角的睫毛和背景头发之间留有空白。  
![](./img/448915927_3759504364367737_7351037500985462730_n10.jpg) 

- 獠嘴角的皱纹。去掉它（下图左二），做一个对比。  
![](./img/448915927_3759504364367737_7351037500985462730_n6.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n6-0.jpg) 

- 香的面部细节：嘴唇的高光涂抹白色颜料；下颌、锁骨处的阴影排线。    
![](./img/448915927_3759504364367737_7351037500985462730_n32.jpg)  
![](./img/448915927_3759504364367737_7351037500985462730_n33.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n33g.jpg)  
![](./img/448915927_3759504364367737_7351037500985462730_n34.jpg)  
![](./img/448915927_3759504364367737_7351037500985462730_n35.jpg) 

- 背景角色（Miki、变色子）的下颌处无排线阴影。  
![](./img/448915927_3759504364367737_7351037500985462730_n38.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n39.jpg) 

- 衣服上的褶皱。  
![](./img/448915927_3759504364367737_7351037500985462730_n22.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n23.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n27.jpg)  
![](./img/448915927_3759504364367737_7351037500985462730_n28.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n29.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n30.jpg)  
    - 褶皱逼真。  
![](./img/448915927_3759504364367737_7351037500985462730_n19.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n20.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n21.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n24.jpg)  
![](./img/448915927_3759504364367737_7351037500985462730_n25.jpg)  
![](./img/448915927_3759504364367737_7351037500985462730_n26.jpg)  

- 衣服上的排线阴影。  
![](./img/448915927_3759504364367737_7351037500985462730_n14.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n12.jpg)  
![](./img/448915927_3759504364367737_7351037500985462730_n28.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n29.jpg) 
![](./img/448915927_3759504364367737_7351037500985462730_n30.jpg) 

- 手指根部的线条写实。（参见FC里的分析：[手指跟部的皱纹](../fc_drawing_style__hand/drawing_details_on_hand.md) ）  
![](./img/448915927_3759504364367737_7351037500985462730_n40.jpg) 

- 猫出镜。  
![](./img/448915927_3759504364367737_7351037500985462730_n18.jpg) 


---
<a name="angelheart13"></a>  
### AH2 插画 (2010.08)  
[![](img/thumb/1_hojo-ten_88graph_80062c9b-1999-4174-b696-d31f53a61845_1.jpg "@edition-88")
](https://edition-88.com/products/angelheart-88graph1) 
[![](img/thumb/GOOh-U4asAAY00C.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GOOh-U4asAAY00C?format=jpg&name=4096x4096) 
[![](img/thumb/GQFP14LaQAABhRj.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQFP14LaQAABhRj?format=jpg&name=4096x4096) 

细节：   

- 背景。  
![](img/GQFP14LaQAABhRj-0.jpg) 
![](img/GQFP14LaQAABhRj-1.jpg) 
![](img/GQFP14LaQAABhRj-2.jpg) 

- 服饰褶皱。  
![](img/GQFP14LaQAABhRj-3.jpg) 
![](img/GQFP14LaQAABhRj-4.jpg) 

- 裤子的着色。  
![](img/GQFP14LaQAABhRj-5.jpg) 
![](img/GQFP14LaQAABhRj-11.jpg) 
![](img/GQFP14LaQAABhRj-12.jpg) 

- 香莹手指的细节。  
![](img/GQFP14LaQAABhRj-6.jpg) 

- 香莹脚踝处的线条。左图脚踝处的线条独特，似乎是AH的一个特点。    
![](img/GQFP14LaQAABhRj-7.jpg) 
![](img/GQFP14LaQAABhRj-8.jpg) 

- 香莹腿部的姿态。  
![](img/GQFP14LaQAABhRj-9.jpg) 
![](img/GQFP14LaQAABhRj-10.jpg) 

- (edition-88版本)两角色之间用白线间隔：    
![](img/88graph-angelheart-04_4800x-0.jpg) 

- (edition-88版本)头发的细节。  
![](img/88graph-angelheart-04_4800x-1.jpg) 
![](img/88graph-angelheart-04_4800x-2.jpg) 

- (edition-88版本)眼部的细节。  
![](img/88graph-angelheart-04_4800x-3.jpg)  
![](img/88graph-angelheart-04_4800x-4.jpg)  

- (edition-88版本)鼻梁处有浅色边线。    
![](img/88graph-angelheart-04_4800x-6.jpg)  

- (edition-88版本)嘴唇处有浅色边线。  
![](img/88graph-angelheart-04_4800x-5.jpg) 

- (edition-88版本)衣服褶皱。  
![](img/88graph-angelheart-05_4800x-0.jpg) 
![](img/88graph-angelheart-05_4800x-2.jpg) 
![](img/88graph-angelheart-05_4800x-3.jpg)  
膝盖处的衣服褶皱：  
![](img/88graph-angelheart-06_4800x-0.jpg) 
![](img/88graph-angelheart-06_4800x-1.jpg) 

- (edition-88版本)可能是拉链。  
![](img/88graph-angelheart-05_4800x-1.jpg) 

- (edition-88版本)枪的细节。  
![](img/88graph-angelheart-05_4800x-4.jpg) 
![](img/88graph-angelheart-05_4800x-4g.jpg)  
![](img/88graph-angelheart-05_4800x-5.jpg) 

- (edition-88版本)脚踝处的线条。  
![](img/88graph-angelheart-07_4800x-0.jpg) 
![](img/88graph-angelheart-07_4800x-1.jpg) 

- (edition-88版本)鞋的高光。  
![](img/88graph-angelheart-07_4800x-2.jpg) 
![](img/88graph-angelheart-07_4800x-3.jpg) 

---  
<a name="angelheart14"></a>  
### AH2 未知画面 (2014.07)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GQLcPN-bsAAwxcB.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQLcPN-bsAAwxcB?format=jpg&name=4096x4096) 


细节：   

- 能看到笔触、墨渍。  
![](img/GQLcPN-bsAAwxcB-0.jpg) 


---  
<a name="angelheart15"></a>  
### AH2 第9卷 封面 (201x)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GQr4XpoawAAgTtl.jpg "现场拍照，左下图@X") 
![](img/thumb/GQr4XpoawAAgTtl-0.jpg "现场拍照，左下图@X") 
](https://pbs.twimg.com/media/GQr4XpoawAAgTtl?format=jpg&name=4096x4096) 

细节：   
(未找到足够大的图片以查看其细节。)  


---  
<a name="angelheart16"></a>  
### AH2 插画 (2015.01)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GOOh88fa0AA4idt.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GOOh88fa0AA4idt?format=jpg&name=4096x4096) 
[![](img/thumb/GP4kA3sagAA_TN_.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GP4kA3sagAA_TN_?format=jpg&name=4096x4096) 
[![](img/thumb/GQLyg2aaMAAWSaX.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQLyg2aaMAAWSaX?format=jpg&name=4096x4096) 
[![](img/thumb/448974338_1592190394659749_6476996291249314842_n.jpg "现场拍照，https://www.instagram.com/p/C8l-nsYyFET/")
](https://scontent-ams4-1.cdninstagram.com/v/t51.29350-15/448974338_1592190394659749_6476996291249314842_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams4-1.cdninstagram.com&_nc_cat=105&_nc_ohc=vMg2vzGTzfkQ7kNvgEFU79B&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTc2NzM3Ng%3D%3D.3-ccb7-5&oh=00_AYAcMooLMZcr3Pq46NSrv9o-laCMnut_OR-GIXCwwVNZDA&oe=67279834&_nc_sid=2d3a3f  ) 

细节：   

- 项链的腾空衬托出角色的动作。  
![](./img/448974338_1592190394659749_6476996291249314842_n12.jpg) 

- 领带的线条柔和、颜色过渡平滑，衬托出其材质。  
![](./img/448974338_1592190394659749_6476996291249314842_n13.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n14.jpg) 

- 手指根部的线条写实。（参见FC里的分析：[手指跟部的皱纹](../fc_drawing_style__hand/drawing_details_on_hand.md) ）    
![](./img/448974338_1592190394659749_6476996291249314842_n15.jpg) 

- 左一图的纽扣逼真。左二图的纽扣似乎无立体感。    
![](./img/448974338_1592190394659749_6476996291249314842_n0.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n1.jpg) 

- 折线阴影和袖口的折线边缘表明袖子是硬布料。   
![](./img/448974338_1592190394659749_6476996291249314842_n2.jpg) 

- 排线细腻。[疑问]是否是新的画法？    
![](./img/448974338_1592190394659749_6476996291249314842_n3.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n4.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n5.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n6.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n7.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n8.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n9.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n10.jpg) 
![](./img/448974338_1592190394659749_6476996291249314842_n11.jpg) 

- 左一图的左下角可以看到清晰的衣服边缘，所以，似乎是特意处理成半成品的风格。    
![](img/GOOh88fa0AA4idt-2.jpg) 
![](img/GOOh88fa0AA4idt-1.jpg) 
![](img/GOOh88fa0AA4idt-0.jpg) 



---  
<a name="angelheart17"></a>  
### CH 插画 (2015.05)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GMLxqo9bsAAMAQf.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMLxqo9bsAAMAQf?format=jpg&name=4096x4096) 
[![](img/thumb/GaUeNuCbUAMtz2O.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GaUeNuCbUAMtz2O?format=jpg&name=4096x4096) 

常见于CH完整版的书脊。是AH的画风。    

细节：   

- 阴影。  
![](img/GMLxqo9bsAAMAQf-0.jpg) 

- 线条非常流畅。  
![](img/GMLxqo9bsAAMAQf-1.jpg) 
![](img/GMLxqo9bsAAMAQf-2.jpg) 

- 野上的面部。  
![](img/GMLxqo9bsAAMAQf-3.jpg) 


---  
<a name="angelheart18"></a>  
### AH2 插画 (2015.08)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GOgqiuHaAAALkfG.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GOgqiuHaAAALkfG?format=jpg&name=4096x4096) 
[![](img/thumb/GQLyg2aaUAAdzGm.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQLyg2aaUAAdzGm?format=jpg&name=4096x4096) 
[![](img/thumb/449020545_1364981594182672_6982033195588405414_n.jpg "现场拍照，https://www.instagram.com/p/C8l-nsYyFET/ ")
](https://scontent-ams2-1.cdninstagram.com/v/t51.29350-15/449020545_1364981594182672_6982033195588405414_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams2-1.cdninstagram.com&_nc_cat=104&_nc_ohc=xVxGTiJ7SXEQ7kNvgG9alee&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTY0ODIyOQ%3D%3D.3-ccb7-5&oh=00_AYCfGOnqhxymHWzT9twWu4VipuBkxAsaMmJLlYj9OwdZag&oe=67279F21&_nc_sid=2d3a3f) 

细节：   

- 手指的细节。（参见FC里的分析：[手指跟部的皱纹](../fc_drawing_style__hand/drawing_details_on_hand.md) ）  
![](img/GQLyg2aaUAAdzGm-0.jpg) 
![](img/GQLyg2aaUAAdzGm-4.jpg) 

- 帽子的着色。  
![](img/GQLyg2aaUAAdzGm-2.jpg) 

- 头发。  
![](img/GQLyg2aaUAAdzGm-3.jpg) 
![](img/GQLyg2aaUAAdzGm-4.jpg) 
![](img/GQLyg2aaUAAdzGm-1.jpg) 

- 面部。  
![](img/GQLyg2aaUAAdzGm-5.jpg) 
![](img/GQLyg2aaUAAdzGm-5g.jpg) 
![](img/GQLyg2aaUAAdzGm-6.jpg) 
![](img/GQLyg2aaUAAdzGm-6g.jpg)  
獠的鼻梁一侧的阴影有排线：  
![](img/GQLyg2aaUAAdzGm-7.jpg) 

- 金属环着色逼真。  
![](img/GQLyg2aaUAAdzGm-28.jpg) 
![](img/GQLyg2aaUAAdzGm-28g.jpg) 

- 服饰和褶皱。  
![](img/GQLyg2aaUAAdzGm-8.jpg) 
![](img/GQLyg2aaUAAdzGm-9.jpg) 
![](img/GQLyg2aaUAAdzGm-18.jpg) 
![](img/GQLyg2aaUAAdzGm-19.jpg) 
![](img/GQLyg2aaUAAdzGm-15.jpg) 
![](img/GQLyg2aaUAAdzGm-16.jpg) 
![](img/GQLyg2aaUAAdzGm-17.jpg)  
![](img/GQLyg2aaUAAdzGm-26.jpg) 
![](img/GQLyg2aaUAAdzGm-27.jpg) 

- 服饰上的高光。  
![](img/GQLyg2aaUAAdzGm-10.jpg) 
![](img/GQLyg2aaUAAdzGm-10g.jpg) 
![](img/GQLyg2aaUAAdzGm-11.jpg) 
![](img/GQLyg2aaUAAdzGm-11g.jpg) 
![](img/GQLyg2aaUAAdzGm-12.jpg) 
![](img/GQLyg2aaUAAdzGm-12g.jpg) 
![](img/GQLyg2aaUAAdzGm-13.jpg) 
![](img/GQLyg2aaUAAdzGm-13g.jpg) 
![](img/GQLyg2aaUAAdzGm-19.jpg) 
![](img/GQLyg2aaUAAdzGm-19g.jpg) 

- 香莹脚踝处的线条。  
![](img/GQLyg2aaUAAdzGm-21.jpg) 
![](img/GQLyg2aaUAAdzGm-22.jpg) 

- 香脚踝处的线条和着色。（左二）脚踝右侧有阴影。    
![](img/GQLyg2aaUAAdzGm-23.jpg) 
![](img/GQLyg2aaUAAdzGm-24.jpg) 

- 鞋底的着色。  
![](img/GQLyg2aaUAAdzGm-14.jpg) 

- 鞋底的排线。  
![](img/GQLyg2aaUAAdzGm-20.jpg) 

- 画纸边缘有小块区域粗糙。[疑问]不知是否是创作时留下的。    
![](img/GQLyg2aaUAAdzGm-29.jpg) 
![](img/GQLyg2aaUAAdzGm-25.jpg) 


---  

<a name="angelheart19"></a>  
### 熊本国际漫画节 (2017.02)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLmtTE2bUAASJOd.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLmtTE2bUAASJOd?format=jpg&name=4096x4096)
[![](img/thumb/GLaRp0JaUAAW127.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLaRp0JaUAAW127?format=jpg&name=4096x4096)
[![](img/thumb/GLWGNZeacAAZEPS.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GLWGNZeacAAZEPS?format=jpg&name=4096x4096)
[![](img/thumb/GMLxqAwbYAAdcSZ.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMLxqAwbYAAdcSZ?format=jpg&name=4096x4096)
[![](img/thumb/GMKmes2bIAAq4q2.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMKmes2bIAAq4q2?format=jpg&name=4096x4096)
[![](img/thumb/GMe-S3-bwAA3a-A.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GMe-S3-bwAA3a-A?format=jpg&name=4096x4096)
[![](img/thumb/GM5zlAGbcAA2bPV.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GM5zlAGbcAA2bPV?format=jpg&name=4096x4096)

细节：   

- 排线阴影：  
![](img/GLmtTE2bUAASJOd-0.jpg) 
![](img/GLmtTE2bUAASJOd-1.jpg)  

- 木材的纹理细腻。  
![](img/GLmtTE2bUAASJOd-3.jpg)  

- 钻机上有“Ryo”字样：  
![](img/GLmtTE2bUAASJOd-2.jpg)  


---  
<a name="angelheart20"></a>  
### AH2 插画 (2017.04)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GOOh_vsbYAAseyN.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GOOh_vsbYAAseyN?format=jpg&name=4096x4096) 
[![](img/thumb/GP4kA3XasAAcK6k.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GP4kA3XasAAcK6k?format=jpg&name=4096x4096) 
[![](img/thumb/GQLyg3fbYAAWgtP.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQLyg3fbYAAWgtP?format=jpg&name=4096x4096) 

细节：   

- 头发的细节和高光。:)    
![](img/GP4kA3XasAAcK6k-0.jpg) 
![](img/GP4kA3XasAAcK6k-1.jpg) 
![](img/GP4kA3XasAAcK6k-2.jpg) 
![](img/GP4kA3XasAAcK6k-3.jpg) 
![](img/GP4kA3XasAAcK6k-4.jpg) 

- 獠的嘴唇也加了高光。  
![](img/GP4kA3XasAAcK6k-30.jpg) 

- 服饰的细节。  
![](img/GP4kA3XasAAcK6k-5.jpg) 
![](img/GP4kA3XasAAcK6k-6.jpg) 

- 服饰的褶皱。或许，褶皱的不同画法表达了不同的质感。    
    - 男士西服的褶皱使用排线阴影：  
    ![](img/GP4kA3XasAAcK6k-10.jpg) 
    ![](img/GP4kA3XasAAcK6k-9.jpg) 
    ![](img/GP4kA3XasAAcK6k-8.jpg) 
    ![](img/GP4kA3XasAAcK6k-7.jpg) 
    - 女士服饰的褶皱：  
    ![](img/GP4kA3XasAAcK6k-13.jpg) 
    ![](img/GP4kA3XasAAcK6k-14.jpg) 
    ![](img/GP4kA3XasAAcK6k-12.jpg) 
    ![](img/GP4kA3XasAAcK6k-11.jpg) 
    ![](img/GP4kA3XasAAcK6k-22.jpg) 
    ![](img/GP4kA3XasAAcK6k-22-2.jpg) 
    ![](img/GP4kA3XasAAcK6k-23.jpg)  
    - Miki的帽子的褶皱也是排线阴影：  
    ![](img/GP4kA3XasAAcK6k-27.jpg) 

- 手臂在衣服上投下阴影。使用交叉排线。  
![](img/GP4kA3XasAAcK6k-16.jpg) 
![](img/GP4kA3XasAAcK6k-15.jpg) 

- 酒杯和其中的液体。  
![](img/GP4kA3XasAAcK6k-17.jpg) 
![](img/GP4kA3XasAAcK6k-17g.jpg) 
![](img/GP4kA3XasAAcK6k-18.jpg) 
![](img/GP4kA3XasAAcK6k-19.jpg)  
冴子的液体颜色稍重。可能是因为背景是肤色。    
![](img/GP4kA3XasAAcK6k-20.jpg)  
Miki年龄小，似乎喝的是橙汁：  
![](img/GP4kA3XasAAcK6k-21.jpg) 

- 指甲的线条不是封闭曲线。  
![](img/GP4kA3XasAAcK6k-24.jpg) 
![](img/GP4kA3XasAAcK6k-25.jpg) 
![](img/GP4kA3XasAAcK6k-26.jpg) 

- 服饰上的白色颜料：  
![](img/GP4kA3XasAAcK6k-29.jpg) 
![](img/GP4kA3XasAAcK6k-29g.jpg) 
![](img/GP4kA3XasAAcK6k-12.jpg) 
![](img/GP4kA3XasAAcK6k-12g.jpg) 
![](img/GP4kA3XasAAcK6k-28.jpg) 
![](img/GP4kA3XasAAcK6k-28g.jpg) 


---

---
<a name="others"></a>  
## 其他  

<a name="others01"></a>  
### 剧场版 "CITY HUNTER - Angel Dust" Original Art (2022.08)   
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GOMYghvaIAAra1i.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GOMYghvaIAAra1i?format=jpg&name=4096x4096) 
[![](img/thumb/GOJnVhhbEAAUiXM.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GOJnVhhbEAAUiXM?format=jpg&name=4096x4096) 
[![](img/thumb/GPmis_0aEAAW_U6.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GPmis_0aEAAW_U6?format=jpg&name=4096x4096) 
[![](img/thumb/GP2hAaha4AAqJKO.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GP2hAaha4AAqJKO?format=jpg&name=4096x4096) 
[![](img/thumb/GP9XznAbMAAm_p-.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GP9XznAbMAAm_p-?format=jpg&name=4096x4096) 
[![](img/thumb/GQBkmr9bEAIwB8L.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GQBkmr9bEAIwB8L?format=jpg&name=4096x4096) 
[![](img/thumb/GQ6sxwNbwAMndTC.jpg "现场拍照@X") 
](https://pbs.twimg.com/media/GQ6sxwNbwAMndTC?format=jpg&name=4096x4096) 

细节：   

- 姿态曲线流畅。  
![](img/GOMYghvaIAAra1i-forceline0.jpg) 
![](img/GOMYghvaIAAra1i-forceline1.jpg) 
![](img/GOMYghvaIAAra1i-forceline2.jpg) 

- 头发。  
![](img/GP2hAaha4AAqJKO-0.jpg) 
![](img/GP9XznAbMAAm_p--0.jpg) 
![](img/GP9XznAbMAAm_p--1.jpg) 
![](img/GP9XznAbMAAm_p--2.jpg) 

- 面部。  
![](img/GP2hAaha4AAqJKO-1.jpg)  
![](img/GP9XznAbMAAm_p--3.jpg)  
![](img/GP9XznAbMAAm_p--4.jpg) 

- 褶皱。  
![](img/GP2hAaha4AAqJKO-2.jpg)  
有排线阴影：  
![](img/GP9XznAbMAAm_p--5.jpg) 
![](img/GP9XznAbMAAm_p--6.jpg) 

- 服饰的着色。  
![](img/GP9XznAbMAAm_p--7.jpg) 
![](img/GP9XznAbMAAm_p--8.jpg) 

- 裤子上的着色。  
![](img/GOJnVhhbEAAUiXM-0.jpg) 
![](img/GOJnVhhbEAAUiXM-1.jpg) 
![](img/GP2hAaha4AAqJKO-3.jpg) 
![](img/GQBkmr9bEAIwB8L-0.jpg) 
![](img/GQBkmr9bEAIwB8L-1.jpg) 
![](img/GQBkmr9bEAIwB8L-2.jpg) 

- 脚部的线条犀利。  
![](img/GP2hAaha4AAqJKO-4.jpg) 
![](img/GP2hAaha4AAqJKO-5.jpg) 

- 脚部的排线。  
![](img/GQBkmr9bEAIwB8L-3.jpg) 
![](img/GQBkmr9bEAIwB8L-4.jpg) 
![](img/GQBkmr9bEAIwB8L-5.jpg) 
![](img/GQBkmr9bEAIwB8L-6.jpg) 
![](img/GQBkmr9bEAIwB8L-7.jpg) 

- 边线外侧有白色涂抹。[疑问]这是修改瑕疵？还是为了表现某种艺术效果？    
![](img/GQBkmr9bEAIwB8L-8.jpg) 
![](img/GQBkmr9bEAIwB8L-9.jpg) 
![](img/GQBkmr9bEAIwB8L-10.jpg) 

---
<a name="others02"></a>  
### Netflix Movie (2024.02)
![](img/not_given.jpg "官方图片") 
[![](img/thumb/GLcAKbrasAEocVo.jpg "现场拍照1@X")
](https://pbs.twimg.com/media/GLcAKbrasAEocVo?format=jpg&name=4096x4096) 
[![](img/thumb/GLXGmhnasAAZiDh.jpg "现场拍照2@X")
](https://pbs.twimg.com/media/GLXGmhnasAAZiDh?format=jpg&name=4096x4096) 
[![](img/thumb/GMLxp-4a4AAoEFj.jpg "现场拍照3@X")
](https://pbs.twimg.com/media/GMLxp-4a4AAoEFj?format=jpg&name=4096x4096) 
[![](img/thumb/GMLYGBPbcAA8F5R.jpg "现场拍照4@X")
](https://pbs.twimg.com/media/GMLYGBPbcAA8F5R?format=jpg&name=4096x4096) 
[![](img/thumb/GMKmesWbEAADxxd.jpg "现场拍照5@X")
](https://pbs.twimg.com/media/GMKmesWbEAADxxd?format=jpg&name=4096x4096) 
[![](img/thumb/GMjwIZMaUAAlxCs.jpg "现场拍照6@X")
](https://pbs.twimg.com/media/GMjwIZMaUAAlxCs?format=jpg&name=4096x4096) 
[![](img/thumb/GMu91RHbQAAuaFb.jpg "现场拍照7@X")
](https://pbs.twimg.com/media/GMu91RHbQAAuaFb?format=jpg&name=4096x4096) 
[![](img/thumb/GMztCbzaUAApe_o.jpg "现场拍照8@X")
](https://pbs.twimg.com/media/GMztCbzaUAApe_o?format=jpg&name=4096x4096) 
[![](img/thumb/GMVijw7aEAA62VF.jpg "现场拍照9@X")
](https://pbs.twimg.com/media/GMVijw7aEAA62VF?format=jpg&name=4096x4096) 
[![](img/thumb/GMldZhca8AAYoGO.jpg "现场拍照10@X")
](https://pbs.twimg.com/media/GMldZhca8AAYoGO?format=jpg&name=4096x4096) 
[![](img/thumb/GQ6sxwNboAAoMlA.jpg "现场拍照11@X")
](https://pbs.twimg.com/media/GQ6sxwNboAAoMlA?format=jpg&name=4096x4096) 
[![](img/thumb/GP7wAzZaMAA9ddi.jpg "现场拍照12@X")
](https://pbs.twimg.com/media/GP7wAzZaMAA9ddi?format=jpg&name=4096x4096) 
[![](img/thumb/GMldZ9kaQAAf3-q.jpg "现场拍照13@X")
](https://pbs.twimg.com/media/GMldZ9kaQAAf3-q?format=jpg&name=4096x4096) 
[![](img/thumb/GMldZscbwAA67o9.jpg "现场拍照14@X")
](https://pbs.twimg.com/media/GMldZscbwAA67o9?format=jpg&name=4096x4096) 
[![](img/thumb/GLa-IJAaUAAcEw4.jpg "现场拍照15@X")
](https://pbs.twimg.com/media/GLa-IJAaUAAcEw4?format=jpg&name=4096x4096) 
[![](img/thumb/GMDRs_XbQAAOaCD.jpg "现场拍照16@X")
](https://pbs.twimg.com/media/GMDRs_XbQAAOaCD?format=jpg&name=4096x4096) 
[![](img/thumb/GNOHYqlbMAAKq0L.jpg "现场拍照17@X")
](https://pbs.twimg.com/media/GNOHYqlbMAAKq0L?format=jpg&name=4096x4096) 
[![](img/thumb/GQLcPLfbYAAnW_l.jpg "现场拍照18@X")
](https://pbs.twimg.com/media/GQLcPLfbYAAnW_l?format=jpg&name=4096x4096) 

细节：   

- 泪埠处有白色高光。    
![](img/GNOHYqlbMAAKq0L-0.jpg) 

- 眉毛的细节。  
![](img/GNOHYqlbMAAKq0L-1.jpg) 

- 鼻翼处和眼角处地淡淡阴影。  
![](img/GNOHYqlbMAAKq0L-2.jpg) 
![](img/GNOHYqlbMAAKq0L-3.jpg) 

- 发丝的细节。  
![](img/GNOHYqlbMAAKq0L-4.jpg) 
![](img/GNOHYqlbMAAKq0L-5.jpg) 
![](img/GNOHYqlbMAAKq0L-6.jpg) 

- 强化的金属光泽。左二为近照(C2)；左三为实物照片。  
![](img/GLcAKbrasAEocVo-0.jpg) 
![](img/GLa-IJAaUAAcEw4-0.jpg) 
![](img/GLcAKbzbIAAKQs7-0.jpg) 

- 手部阴影。左二为实物照片。  
![](img/GLcAKbrasAEocVo-1.jpg) 
![](img/GLcAKbzbIAAKQs7-1.jpg) 

- 衣服的着色逼真。这种布料的质感可能和纸张（第一行左三）有关。第二行为实物照片。  
![](img/GLcAKbrasAEocVo-2.jpg) 
![](img/GLcAKbrasAEocVo-3.jpg) 
![](img/GLcAKbrasAEocVo-5.jpg)  
![](img/GLcAKbzbIAAKQs7-2.jpg) 
![](img/GLcAKbzbIAAKQs7-3.jpg) 

- 左二为实物照片。  
![](img/GLcAKbrasAEocVo-4.jpg) 
![](img/GLcAKbzbIAAKQs7-4.jpg) 

- C1(下图左一列)中阴影逼真，其边缘过渡柔和。C2(下图左二列)中看到阴影边缘有大量的排线阴影。  
![](img/GLcAKbrasAEocVo-1-1.jpg) ![](img/GLa-IJAaUAAcEw4-1.jpg)  
![](img/GLcAKbrasAEocVo-1-2.jpg) ![](img/GLa-IJAaUAAcEw4-2.jpg)  
![](img/GLcAKbrasAEocVo-1-4.jpg) ![](img/GLa-IJAaUAAcEw4-4.jpg)  
![](img/GLcAKbrasAEocVo-1-5.jpg) ![](img/GLa-IJAaUAAcEw4-5.jpg)  
![](img/GLcAKbrasAEocVo-1-6.jpg) ![](img/GLa-IJAaUAAcEw4-6.jpg)  

- 手腕的光影交界处着色逼真。近处看其细节(C2)。    
![](img/GLcAKbrasAEocVo-11.jpg) 
![](img/GLa-IJAaUAAcEw4-3.jpg) 

- 边线外侧加了白色颜料。  
![](img/GLa-IJAaUAAcEw4-7.jpg) 
![](img/GLa-IJAaUAAcEw4-8.jpg) 

- 右下角纸面有破损。我猜，可能是原先在此处签名，后将签名下移。    
![](img/GLa-IJAaUAAcEw4-9.jpg) 
![](img/GLa-IJAaUAAcEw4-10.jpg) 

- 金属的高光。  
![](img/GMldZ9kaQAAf3-q-5.jpg) 
![](img/GMldZ9kaQAAf3-q-6.jpg) 

- 衣领处的高光。  
![](img/GMldZ9kaQAAf3-q-4.jpg) 
![](img/GP7wAzZaMAA9ddi-0.jpg) 

- 黑色区域的亮斑可能源自纸。    
![](img/GMldZ9kaQAAf3-q-3.jpg) 

- 手部阴影。  
![](img/GMldZ9kaQAAf3-q-0.jpg)  
![](img/GMldZ9kaQAAf3-q-1.jpg) 

- 排线阴影。  
![](img/GMldZhca8AAYoGO-0.jpg) 
![](img/GMldZ9kaQAAf3-q-2.jpg) 


- 胳膊处的细节。  
![](img/GP7wAzZaMAA9ddi-1.jpg) 
![](img/GP7wAzZaMAA9ddi-2.jpg) 
![](img/GP7wAzZaMAA9ddi-3.jpg) 
![](img/GP7wAzZaMAA9ddi-4.jpg) 
![](img/GP7wAzZaMAA9ddi-5.jpg) 
![](img/GP7wAzZaMAA9ddi-6.jpg) 
![](img/GP7wAzZaMAA9ddi-7.jpg) 

- 签名的笔迹下方有白线。这可能是由纸凹陷所致，这意味着作者的笔力重。  
![](img/GQLcPLfbYAAnW_l-0.jpg) 
![](img/GQLcPLfbYAAnW_l-1.jpg) 

---
<a name="others03"></a>  
<a name="GALLERYZENONSpecial"></a>  

### GALLERY ZENON 特绘 (2024.03?/05?)  
![](img/not_given.jpg "官方照片") 
[![](img/thumb/GLiG0mCb0AApB21.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLiG0mCb0AApB21?format=jpg&name=4096x4096) 
[![](img/thumb/GLbrf07bwAAQDQs.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLbrf07bwAAQDQs?format=jpg&name=4096x4096) 
[![](img/thumb/GMLxrLvbYAAt_hm.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMLxrLvbYAAt_hm?format=jpg&name=4096x4096) 
[![](img/thumb/GMqHRIja8AALC_C.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GMqHRIja8AALC_C?format=jpg&name=4096x4096) 
[![](img/thumb/GL0j-ZabsAASM6K.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GL0j-ZabsAASM6K?format=jpg&name=4096x4096) 
[![](img/thumb/GQzBkQtbMAAicmX.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GQzBkQtbMAAicmX?format=jpg&name=4096x4096) 
[![](img/thumb/GNq6lgCa0AAKOfc.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GNq6lgCa0AAKOfc?format=jpg&name=4096x4096) 
[![](img/thumb/GLXebZ0bMAAdhFE.jpg "现场拍照@X")
](https://pbs.twimg.com/media/GLXebZ0bMAAdhFE?format=jpg&name=4096x4096) 


说明文字：  

> GALLERY ZENON Grand Opening Special Exhibition  
Tsukasa Hojo Exhibition  
The Road To 『CITY HUNTER』40th Anniversary 2025  
Limited Special Exhibition  
Original Art  

（译注：可见，这是为本画展特别画的。）    

细节：   

- 由于树荫的投在玻璃上，所以从獠的头后方看到了玻璃后面（店里的）“CZ”字样。这是"Cafe Zenon"的标志。这暗示獠和香在"Cafe Zenon"店门前和咖啡。    
![](img/GLiG0mCb0AApB21-3.jpg)  

- [疑问]深色线条为何有点阵的pattern？着色区域则没有点阵的pattern。（难道说线条是打印品，然后手工上色？）        
![](img/GLXebZ0bMAAdhFE-0.jpg) 
![](img/GLXebZ0bMAAdhFE-1.jpg) 

- 面部细节。  
![](img/GNq6lgCa0AAKOfc-5.jpg) 
![](img/GNq6lgCa0AAKOfc-6.jpg) 
![](img/GNq6lgCa0AAKOfc-7.jpg) 

- 手指关节突出。  
![](img/GNq6lgCa0AAKOfc-8.jpg) 

- 线条犀利。   
![](img/GNq6lgCa0AAKOfc-1.jpg) 
![](img/GNq6lgCa0AAKOfc-2.jpg)  
![](img/GNq6lgCa0AAKOfc-3.jpg) 
![](img/GNq6lgCa0AAKOfc-4.jpg) 

- 黑裤子边沿有白色颜料修饰的高光。  
![](img/GNq6lgCa0AAKOfc-0.jpg) 

- 鞋底的着色逼真，由其是从远处看（左三）。  
![](img/GLiG0mCb0AApB21-2.jpg) 
![](img/GLiG0mCb0AApB21-0.jpg) 
![](img/GLiG0mCb0AApB21-1.jpg)， 
![](img/GL0j-ZabsAASM6K-0.jpg) 
![](img/GL0j-ZabsAASM6K-1.jpg)， 
![](img/GL0j-ZabsAASM6K-2.jpg) 
![](img/GL0j-ZabsAASM6K-3.jpg) 

- 右下角的签名疑似为："TSUKASA '24 MAY"。这表示2024年05月。但该展前期(2024年4月17日起)已展出该画作。  


<a name="v.s.FamilyPlot"></a>  

- 该作品与[FamilyPlot扉页](./details2.md#FamilyPlotDoorPage)的相似点。  
    - 两作品都是色彩饱和度低。  
    ![](img/thumb/GLiG0mCb0AApB21.jpg) 
    ![](img/thumb/GP4kBdHaIAEt6c0.jpg)  
    - 两幅画作中的女主的服饰颜色相同--都是黄颜色外衣、红色内衬、浅蓝色牛仔裤。  
    ![](img/GLiG0mCb0AApB21-vs0.jpg) 
    ![](img/GLiG0mCb0AApB21-vs1.jpg) 
    ![](img/GP4kBdHaIAEt6c0-18.jpg) 


---  
