https://kichinavi.net/gallery-zenon/

# 吉祥寺に「GALLERY ZENON（ギャラリーゼノン）」がオープン！北条司展が開催中
 2024年4月17日  

![](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon15.jpg)  

こんにちは、キチナビ（[@kichinavikun](https://twitter.com/kichinavikun)）です＾＾  
大家好，我是 Kichinavi (@kichinavikun) ＾＾  

2024年4月17日（水）、吉祥寺駅から徒歩4分の場所に**「GALLERY ZENON（ギャラリーゼノン）」**がオープンしました。  
2024 年 4 月 17 日（周三），GALLERY ZENON 在吉祥寺开业，从吉祥寺站步行只需四分钟。  

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon4.jpg)  
吉祥寺 "GALLERY ZENON"  

GALLERY ZENON（ギャラリーゼノン）は、コアミックス（出版社）が運営するギャラリーです。  
GALLERY ZENON "是由 Coamix（出版商）经营的一家画廊。  

漫画コンテンツやアートが楽しめる新しい形のお店です。  
这是一家可以欣赏漫画内容和艺术的新型商店。  

*   ［前期］2024年4月17日（水）〜5月19日（日）
*   ［後期］2024年5月22日（水）〜6月23日（日）

は漫画家の北条司（ほうじょう つかさ）さんの原画展が開催されています。  
正在举办漫画家北条司的原创作品展。  

どんなお店になったのか早速行ってきました♪  
这是一家什么样的店呢?我马上就去了♪  

※4/17(水)〜4/21(日)の間は完全予約制です。チケットの詳細は「[HP](https://gallery-zenon.jp/)」から確認できます。  
*4月17日（周三）至4月21日（周日）期间需提前预约。 门票详情请见 "HP"。 

## 目次  
目录  

- [北条司さんの原画展は見応え抜群｜グッズも多数販売](#rtoc-1)  
北条司的原画展令人目不暇接｜许多商品正在销售中  
- [ギャラリー内にはカフェスペースも併設｜限定メニューもあり](#rtoc-2)  
画廊内的咖啡厅｜限定菜单  
    - [獠のもっこりオムライス 1,580円](#rtoc-3)  
    獠的紫菜蛋包饭：1,580 日元
    - [香のプンプン、おしおきパンケーキ 1,580円](#rtoc-4)  
    香的PengPeng、おしおき煎饼：1,580日元
    - [今宵あなたのハートをいただきに参ります 1,200円](#rtoc-5)  
    今晚我来送你一颗爱心，1200日元  
- [GALLERY ZENON（ギャラリーゼノン）はワクワク体験が詰まったお店だった](#rtoc-6)  
GALLERY ZENON 是一家充满刺激体验的商店
- [吉祥寺「GALLERY ZENON（ギャラリーゼノン）」の店舗情報](#rtoc-7)  
吉祥寺 GALLERY ZENON 的信息

## 北条司さんの原画展は見応え抜群｜グッズも多数販売
北条司的原画展令人目不暇接｜许多商品正在销售中  

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon25.jpg)

GALLERY ZENON（ギャラリーゼノン）ではオープンからしばらくの間は、「キャッツ♥アイ」や「シティーハンター」でお馴染みの漫画家、北条司さんの原画展を開催。  
GALLERY ZENON 在开幕后的一段时间内举办了漫画家北条司原画展，他因《猫之♥眼》和《城市猎人》而闻名。   

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon11.jpg) 
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon9.jpg) 
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon10.jpg) 
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon18.jpg)

デビュー作から連載作品や読切等ほぼ全作品が網羅されていて、ファンにはたまらない内容でしたね。  
从他的处女作到连载和单话故事，几乎涵盖了他所有的作品，是粉丝们不容错过的地方。  

あえて撮影は少なめにしたので、ぜひ足を運んで直接見にきて欲しいです。  
因为特意少拍摄了，所以请一定要亲自去看。  

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon6.jpg) 
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon7.jpg) 
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon21.jpg) 
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon20.jpg) 
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon22.jpg) 
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon23.jpg)

入り口付近では、グッズも多数販売しています。  
入口附近有许多商品出售。

## ギャラリー内にはカフェスペースも併設｜限定メニューもあり
画廊内的咖啡厅｜限定菜单  

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon13.jpg)

GALLERY ZENON（ギャラリーゼノン）内には、カフェスペースも併設されています。  
GALLERY ZENON 内还有一个咖啡厅。  

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon28.jpg)

天皇の料理人を務めた菱江 隆（ひしえ たかし）シェフのオリジナルカレーやケーキ、ドリンクをはじめ、北条司展コラボメニューもあります。  
曾担任天皇の料理人的主厨 菱江 隆制作的原创咖喱、蛋糕和饮料，以及北条司展合作菜单。  

北条司展【前期】4/17(水)〜5/19(日)のコラボメニューはこちら。  
点击此处查看北条司展【前期】的合作菜单 4 月 17 日（周三）～ 5 月 19 日（周日）。    
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon27.jpg)

### 獠のもっこりオムライス 1,580円
獠的紫菜蛋包饭：1,580 日元  

【獠と写真が撮れる限定フォトフレーム&限定ステッカー付き】  
定番のもっこりオムライスがNewバージョンで再登場！獠の厚紙を後ろに立てて写真を撮ると…  
【附獠和能拍照的限定相框&限定贴纸】  
经典的日式蛋包饭新版本再次登场!将獠的厚纸立在身后拍照的话…  

### 香のプンプン、おしおきパンケーキ 1,580円
香的PengPeng、おしおき煎饼：1,580日元  

【香と写真が撮れる限定フォトフレーム&限定ステッカー付き】  
ふわふわ厚焼きパンケーキにクリーミーホイップをかけた、ふわとろ食感が楽しめるパンケーキ。  
【附能拍摄香和照片的限定相框&限定贴纸】  
在松软的厚薄饼上撒上奶油搅汁，可以享受蓬松口感的薄饼。  

### 今宵あなたのハートをいただきに参ります 1,200円
今晚我来送你一颗爱心，1200日元  

【瞳と写真が撮れる限定フォトフレーム&限定ステッカー付き】  
泪、瞳、愛のテーマカラーのゼリーを使ったグレープフルーツ風味のドリンクです。  
【附有能与瞳拍照的限定相框&限定贴纸】
是使用泪、瞳、爱的主题色果冻的葡萄柚风味饮料。  

カフェのみ利用の場合もチケットの購入が必要です。  
只使用咖啡馆的情况下也需要购票。  
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon15.jpg)  

先ほど別のお店でランチを食べたばかりですが、せっかくなので今回は獠のもっこりオムライスを注文。  
刚才在别的店刚吃过午餐，但因为难得，这次獠地点了蛋包饭。  

チキンライスで作ったデミグラスソースのオムライスです。  
这是用鸡肉饭做成的蛋包饭。  
![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon14.jpg)

毎年もっこりが大きくなっている噂があるんだとか。  
有传言说Mokkori的个头一年比一年大。   

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon19.jpg)

デミグラスソースが濃厚で味も美味しかったです＾＾  
调味汁很浓，味道也很好^ ^  

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon26.jpg)

会計時にもらう限定カードのQRコードを読み込むと、人気キャラクターと一緒に写真も撮れます。  
结账时，只要读取限定卡上的二维码，就能与人气角色合影。  

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon16.jpg)

お皿もかわいかったです＾＾  
盘子也很可爱 ^ ^  

カフェは人気で列ができていたので、ある程度時間に余裕を持って行かれるのがおすすめです。  
因为咖啡馆很受欢迎，排队等候的人很多，所以建议大家一定要有充裕的时间。  

## GALLERY ZENON（ギャラリーゼノン）はワクワク体験が詰まったお店だった
GALLERY ZENON 是一家充满刺激体验的商店  

![吉祥寺「GALLERY ZENON（ギャラリーゼノン）」](https://kichinavi.net/wp-content/uploads/2024/03/gallery-zenon5.jpg)

今回はGALLERY ZENON（ギャラリーゼノン）さんに行ってきましたが、ワクワクがたくさん詰まったお店でした。  
这次我们来到了充满激情的 GALLERY ZENON。  

原画展やコラボメニューなど、ファンにはたまらないお店でしたね。  
原画展和合作菜单让这里成为粉丝们的好去处。  

カフェゼノンさんの雰囲気が大好きなので、またリニューアルオープンしてくれて嬉しいです＾＾  
我喜欢 Cafe Zenon 的氛围，所以很高兴它又重新开张了 ^ ^  

漫画やアートが好きな方は、行かれてみてはいかがでしょうか？  
如果你喜欢漫画和艺术，为什么不去那里呢？



## 吉祥寺「GALLERY ZENON（ギャラリーゼノン）」の店舗情報
吉祥寺 GALLERY ZENON 的信息  

|     |     |
| --- | --- |
| 住所  | 東京都武蔵野市吉祥寺南町2丁目11−1 |
| アクセス(Access) | 吉祥寺駅から徒歩4分(从吉祥寺站步行 4 分钟) |
| 営業時間 | 11:00〜18:00（最終入場/LO 17:30）  <br>※最新の営業日時はお店に確認をお願いします(请向店方确认最新的营业日期和时间) |
| 定休日 | 火曜日(周二) |
| 入場料金 | 平日事前予約チケット 大人1,000円,小学生500円  <br>休日事前予約チケット 大人1,300円,小学生500円 |
| 電話番号 | 不明  |
| X   | [X](https://twitter.com/GalleryZenon) |
| Instagram | [Instagram](https://www.instagram.com/galleryzenon/?hl=ja) |
| HP  | [HP](https://gallery-zenon.jp/) |
