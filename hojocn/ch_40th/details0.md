
## City Hunter 40周年「北条司展」展览信息 & 部分展品（Part 1/5）  

City Hunter 40周年「北条司展」以下简称“画展”。  


-----  
<a name="eventinfo"></a>  
### 活动概述  

[1(东京展)](./official_2024-03-18.md):  
展示名：GALLERY ZENON 开业纪念企划「北条司展」  
英文名：The Road To 『CITY HUNTER』40th Anniversary 2025 〜Limited Special Exhibition〜[^english]  
地点：GALLERY ZENON（〒180-0003 东京都武藏野市吉祥寺南町2-11-1）  
展期：前期2024年4月17日(周三) 〜5月19日(周日) ※周二固定休息  
　　　后期2024年5月22日(周三) 〜6月23日(周日) ※周二固定休息  
官网：[gallery-zenon.jp](./gallery-zenon.md)  


[2(福冈展)](./gallery-zenon_fukuoka.md):  
展示名：北条司展 IN 福岡  
英文名：The Road To 『CITY HUNTER』 40th Anniversary 2025 〜Limited Special Exhibition〜[^english]    
地点：六本松 蔦屋書店 Gallery Space（福岡県福岡市中央区六本松4丁目2-1 六本松421 2F）  
展期：2024年10月16日(水)〜10月27日(日)  
　　　10:00～21:00 ※最終日のみ19:00 CLOSE  
官网：[gallery-zenon.jp/...](./gallery-zenon_fukuoka.md)  
（注：有网友[提到](https://x.com/chiroru_special/status/1847953515379138796)，福冈展的展品少。但从现场拍照来看，新增了一些说明文字；详见本文后续。）  

[^english]: [Gallery Zenon官网](./gallery-zenon.md)。  
注：展名的原英文为“The road to 『CITY HUNTER』 40th anniversary 2025 〜Limited Special Exhibition〜”。其中若干单词的首字母为小写；但我觉得，此处首字母若为大写，则更正式。     

<a name="event01"></a>  
### 东京展场外  
[<img title="1(点击查看原图)" height="100" src="./img/thumb/GLnedLqbkAE3mhj.jpg"/> 
](https://pbs.twimg.com/media/GLnedLqbkAE3mhj?format=jpg&name=4096x4096) 
[<img title="2(点击查看原图)" height="100" src="./img/thumb/GLpH2NLbcAAiic1.jpg"/> 
](https://pbs.twimg.com/media/GLpH2NLbcAAiic1?format=jpg&name=4096x4096) 
[<img title="3(点击查看原图)" height="100" src="./img/thumb/GLrD7CvacAAQgiW.jpg"/> 
](https://pbs.twimg.com/media/GLrD7CvacAAQgiW?format=jpg&name=4096x4096) 
[<img title="4(点击查看原图)" height="100" src="./img/thumb/GLVYJU3aQAEfKmj.jpg"/> 
](https://pbs.twimg.com/media/GLVYJU3aQAEfKmj?format=jpg&name=4096x4096) 
[<img title="5(点击查看原图)" height="100" src="./img/thumb/GLltOrPa0AAqWqH.jpg"/> 
](https://pbs.twimg.com/media/GLltOrPa0AAqWqH?format=jpg&name=4096x4096) 
[<img title="6(点击查看原图)" height="100" src="./img/thumb/GLp9d3LbIAAiKuM.jpg"/> 
](https://pbs.twimg.com/media/GLp9d3LbIAAiKuM?format=jpg&name=4096x4096) 
[<img title="7(点击查看原图)" height="100" src="./img/thumb/GLcF73NaIAAGoa1.jpg"/> 
](https://pbs.twimg.com/media/GLcF73NaIAAGoa1?format=jpg&name=4096x4096) 
[<img title="8(点击查看原图)" height="100" src="./img/thumb/GLcF73ObcAAVtn0.jpg"/> 
](https://pbs.twimg.com/media/GLcF73ObcAAVtn0?format=jpg&name=4096x4096) 
[<img title="9(点击查看原图)" height="100" src="./img/thumb/GLcGR1ibcAAe5jy.jpg"/> 
](https://pbs.twimg.com/media/GLcGR1ibcAAe5jy?format=jpg&name=4096x4096) 
[<img title="10(点击查看原图)" height="100" src="./img/thumb/GLlM2S6akAAsaz2.jpg"/> 
](https://pbs.twimg.com/media/GLlM2S6akAAsaz2?format=jpg&name=4096x4096) 
[<img title="11(点击查看原图)" height="100" src="./img/thumb/GLu6kz1agAAEMGn.jpg"/> 
](https://pbs.twimg.com/media/GLu6kz1agAAEMGn?format=jpg&name=4096x4096) 
[<img title="12(点击查看原图)" height="100" src="./img/thumb/GLu6kz2bcAAdAWg.jpg"/> 
](https://pbs.twimg.com/media/GLu6kz2bcAAdAWg?format=jpg&name=4096x4096) 
[<img title="13(点击查看原图)" height="100" src="./img/thumb/GLu6kz4boAAOwtL.jpg"/> 
](https://pbs.twimg.com/media/GLu6kz4boAAOwtL?format=jpg&name=4096x4096) 
[<img title="13.5(点击查看原图)" height="100" src="./img/thumb/GMPEAiAbcAAf-jY.jpg"/> 
](https://pbs.twimg.com/media/GMPEAiAbcAAf-jY?format=jpg&name=4096x4096) 
[<img title="13.6(点击查看原图)" height="100" src="./img/thumb/GMoiEsvbwAArMQz.jpg"/> 
](https://pbs.twimg.com/media/GMoiEsvbwAArMQz?format=jpg&name=4096x4096) 
[<img title="13.7(点击查看原图)" height="100" src="./img/thumb/GMeaD1haoAAaH1z.jpg"/> 
](https://pbs.twimg.com/media/GMeaD1haoAAaH1zY?format=jpg&name=4096x4096) 
[<img title="13.8(点击查看原图)" height="100" src="./img/thumb/GM7uQ3fasAAyF2-.jpg"/> 
](https://pbs.twimg.com/media/GM7uQ3fasAAyF2-?format=jpg&name=4096x4096) 
[<img title="13.9(点击查看原图)" height="100" src="./img/thumb/GNZNUQFbUAAENAz.jpg"/> 
](https://pbs.twimg.com/media/GNZNUQFbUAAENAz?format=jpg&name=4096x4096) 
[<img title="14(点击查看原图)" height="100" src="./img/thumb/GNtDWLJakAAm2WJ.jpg"/> 
](https://pbs.twimg.com/media/GNtDWLJakAAm2WJ?format=jpg&name=4096x4096) 
[<img title="15(点击查看原图)" height="100" src="./img/thumb/GNtDWLIawAAoiHP.jpg"/> 
](https://pbs.twimg.com/media/GNtDWLIawAAoiHP?format=jpg&name=4096x4096) 
[<img title="16(点击查看原图)" height="100" src="./img/thumb/GNsJCoeaMAEPWhs.jpg"/> 
](https://pbs.twimg.com/media/GNsJCoeaMAEPWhs?format=jpg&name=4096x4096) 
[<img title="16.5(点击查看原图)" height="100" src="./img/thumb/GN6fW3jbsAA-bKw.jpg"/> 
](https://pbs.twimg.com/media/GN6fW3jbsAA-bKw?format=jpg&name=4096x4096) 
[<img title="后期1(点击查看原图)" height="100" src="./img/thumb/GOW_5MWaMAAkeHv.jpg"/> 
](https://pbs.twimg.com/media/GOW_5MWaMAAkeHv?format=jpg&name=4096x4096) 
[<img title="后期2(点击查看原图)" height="100" src="./img/thumb/GOQgeZxbEAAjn-x.jpg"/> 
](https://pbs.twimg.com/media/GOQgeZxbEAAjn-x?format=jpg&name=4096x4096) 
[<img title="后期3(点击查看原图)" height="100" src="./img/thumb/GOQ3TPtaEAAY9_w.jpg"/> 
](https://pbs.twimg.com/media/GOQ3TPtaEAAY9_w?format=jpg&name=4096x4096) 
[<img title="后期4(点击查看原图)" height="100" src="./img/thumb/GOJaz7kbgAEU1_J.jpg"/> 
](https://pbs.twimg.com/media/GOJaz7kbgAEU1_J?format=jpg&name=4096x4096) 
[<img title="后期5(点击查看原图)" height="100" src="./img/thumb/GPh3dkraoAAOlCT.jpg"/> 
](https://pbs.twimg.com/media/GPh3dkraoAAOlCT?format=jpg&name=4096x4096) 
[<img title="后期6(点击查看原图)" height="100" src="./img/thumb/GP11sLLasAAS6Vk.jpg"/> 
](https://pbs.twimg.com/media/GP11sLLasAAS6Vk?format=png&name=4096x4096) 
[<img title="最终日(点击查看原图)" height="100" src="./img/thumb/GQvVbCrbwAAEdPq.jpg"/> 
](https://pbs.twimg.com/media/GQvVbCrbwAAEdPq?format=png&name=4096x4096) 
[<img title="某日(点击查看原图)" height="100" src="./img/thumb/1040g008314cc8640me0g4a18f2vloj5orp1s398!nd_dft_wgth_webp_3.webp"/> 
](https://www.xiaohongshu.com/discovery/item/66777d24000000001c036982?xsec_token=CBHXy0bQRVdICoXMY3K4EnHFqYe_BXeCqyWHmYiXUbaRc=) 
[<img title="17(点击查看原图)" height="100" src="./img/thumb/GLltO2RaYAABRTE.jpg"/> 
](https://pbs.twimg.com/media/GLltO2RaYAABRTE?format=jpg&name=4096x4096) 
[<img title="18(点击查看原图)" height="100" src="./img/thumb/GNtCoo-bcAI9LM0.jpg"/> 
](https://pbs.twimg.com/media/GNtCoo-bcAI9LM0?format=jpg&name=4096x4096) 
[<img title="19(点击查看原图)" height="100" src="./img/thumb/GLfrzPxaMAAqOF4.jpg"/> 
](https://pbs.twimg.com/media/GLfrzPxaMAAqOF4?format=jpg&name=4096x4096) 
[<img title="20(点击查看原图)" height="100" src="./img/thumb/GLhHPrjbwAAxrOJ.jpg"/> 
](https://pbs.twimg.com/media/GLhHPrjbwAAxrOJ?format=jpg&name=4096x4096) 
[<img title="后期5(点击查看原图)" height="100" src="./img/thumb/GOQkgLCacAAanW1.jpg"/> 
](https://pbs.twimg.com/media/GOQkgLCacAAanW1?format=jpg&name=4096x4096) 
[<img title="后期6(点击查看原图)" height="100" src="./img/thumb/GOQkfUGawAA6tgc.jpg"/> 
](https://pbs.twimg.com/media/GOQkfUGawAA6tgc?format=jpg&name=4096x4096) 
[<img title="后期7(点击查看原图)" height="100" src="./img/thumb/GOQkfn3aIAIHTBe.jpg"/> 
](https://pbs.twimg.com/media/GOQkfn3aIAIHTBe?format=jpg&name=4096x4096) 
[<img title="后期8(点击查看原图)" height="100" src="./img/thumb/GOQkf0abQAAce6Z.jpg"/> 
](https://pbs.twimg.com/media/GOQkf0abQAAce6Z?format=jpg&name=4096x4096) 
[<img title="(点击查看原图)" height="100" src="./img/thumb/GQpjZ2lbUAA7XOh.jpg"/> 
](https://pbs.twimg.com/media/GQpjZ2lbUAA7XOh?format=jpg&name=4096x4096) 

<a name="event02"></a>  
### 东京展一楼展区  
[<img title="Cafe Zenon及投影" height="100" src="./img/thumb/441867332_905090321630483_7711964196564015906_n.jpg"/> 
](https://www.facebook.com/hashtag/北条司展) 
[<img title="MiniCooper的两侧分别是餐厅、商品区(点击查看原图)" height="100" src="./img/thumb/GLgyuVnbMAAnPMl.jpg"/> 
](https://pbs.twimg.com/media/GLgyuVnbMAAnPMl?format=jpg&name=4096x4096) 
[<img title="MiniCooper的一侧是商品区(点击查看原图)" height="100" src="./img/thumb/1040g0083125lv3cp4o0g5n3l5j1k29694ngivr8!nd_dft_wgth_webp_3.webp"/> 
](https://www.xiaohongshu.com/discovery/item/662f189b000000001c009cad?xsec_token=CBe2A5KlVK09yIgOMWKil96N7WxRvScRvJm5Qvx3D8fmU=) 
[<img title="17(点击查看原图)" height="100" src="./img/thumb/GMPlKmZbAAAzSbi.jpg"/> 
](https://pbs.twimg.com/media/GMPlKmZbAAAzSbi?format=jpg&name=4096x4096) 
[<img title="18(点击查看原图)" height="100" src="./img/thumb/GMPlKmbbUAADZ_S.jpg"/> 
](https://pbs.twimg.com/media/GMPlKmbbUAADZ_S?format=jpg&name=4096x4096) 
[<img title="18.5(点击查看原图)" height="100" src="./img/thumb/20240503093041.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503093041.jpg) 
[<img title="18.7(点击查看原图)" height="100" src="./img/thumb/20240503093057.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503093057.jpg) 
[<img title="19(点击查看原图)" height="100" src="./img/thumb/GNcwG4WbcAAHW7w.jpg"/> 
](https://pbs.twimg.com/media/GNcwG4WbcAAHW7w?format=jpg&name=4096x4096) 
[<img title="19(点击查看原图)" height="100" src="./img/thumb/GLZJVLBa4AAveOo.jpg"/> 
](https://pbs.twimg.com/media/GLZJVLBa4AAveOo?format=jpg&name=4096x4096) 
[<img title="19后期(点击查看原图)" height="100" src="./img/thumb/GOKZZlobEAA4A0e.jpg"/> 
](https://pbs.twimg.com/media/GOKZZlobEAA4A0e?format=jpg&name=4096x4096) 
[<img title="20(点击查看原图)" height="100" src="./img/thumb/GLV85sDacAAGh6C.jpg"/> 
](https://pbs.twimg.com/media/GLV85sDacAAGh6C?format=jpg&name=4096x4096) 
[<img title="21(点击查看原图)" height="100" src="./img/thumb/GLcAKbqaMAATQEO.jpg"/> 
](https://pbs.twimg.com/media/GLcAKbqaMAATQEO?format=jpg&name=4096x4096) 
[<img title="22(点击查看原图)" height="100" src="./img/thumb/GLb3rK4a0AAwirs.jpg"/> 
](https://pbs.twimg.com/media/GLb3rK4a0AAwirs?format=jpg&name=4096x4096) 
[<img title="后期(点击查看原图)" height="100" src="./img/thumb/GOMYgh0bQAADspc.jpg"/> 
](https://pbs.twimg.com/media/GOMYgh0bQAADspc?format=jpg&name=4096x4096) 
[<img title="后期，北条签名(点击查看原图)" height="100" src="./img/thumb/GOKOTgMaUAAQ0un.jpg"/> 
](https://pbs.twimg.com/media/GOKOTgMaUAAQ0un?format=jpg&name=4096x4096) 
[<img title="后期，铃木亮平签名(点击查看原图)" height="100" src="./img/thumb/GP11sLNbAAAig5n.jpg"/> 
](https://pbs.twimg.com/media/GP11sLNbAAAig5n?format=png&name=4096x4096) 
[<img title="23(点击查看原图)" height="100" src="./img/thumb/GLc416qasAAPwfI.jpg"/> 
](https://pbs.twimg.com/media/GLc416qasAAPwfI?format=jpg&name=4096x4096) 
[<img title="23.4(点击查看原图)" height="100" src="./img/thumb/GNs6LFDaAAAnb6U.jpg"/> 
](https://pbs.twimg.com/media/GNs6LFDaAAAnb6U?format=jpg&name=4096x4096) 
[<img title="23.4(点击查看原图)" height="100" src="./img/thumb/GQcOLYxaIAMxkAP.jpg"/> 
](https://pbs.twimg.com/media/GQcOLYxaIAMxkAP?format=jpg&name=4096x4096) 
[<img title="23.5(点击查看原图)" height="100" src="./img/thumb/GLcgZWea0AAr8IK.jpg"/> 
](https://pbs.twimg.com/media/GLcgZWea0AAr8IK?format=jpg&name=4096x4096) 
[<img title="23.7(点击查看原图)" height="100" src="./img/thumb/GLmtS9wbAAAAJdg.jpg"/> 
](https://pbs.twimg.com/media/GLmtS9wbAAAAJdg?format=jpg&name=4096x4096) 
[<img title="23.8后期(点击查看原图)" height="100" src="./img/thumb/GPgTGeEbEAACf58.jpg"/> 
](https://pbs.twimg.com/media/GPgTGeEbEAACf58?format=jpg&name=4096x4096) 
[<img title="24后期(点击查看原图)" height="100" src="./img/thumb/GOMYghvaIAAra1i.jpg"/> 
](https://pbs.twimg.com/media/GOMYghvaIAAra1i?format=jpg&name=4096x4096) 
[<img title="24(点击查看原图)" height="100" src="./img/thumb/GLcAKbrasAEocVo.jpg"/> 
](https://pbs.twimg.com/media/GLcAKbrasAEocVo?format=jpg&name=4096x4096) 
[<img title="25(点击查看原图)" height="100" src="./img/thumb/GLcAKbzbIAAKQs7.jpg"/> 
](https://pbs.twimg.com/media/GLcAKbzbIAAKQs7?format=jpg&name=4096x4096) 
[<img title="42.5(点击查看原图)" height="100" src="./img/thumb/GN6e01uagAAI7S-.jpg"/> 
](https://pbs.twimg.com/media/GN6e01uagAAI7S-?format=jpg&name=4096x4096) 
[<img title="42.6(点击查看原图)" height="100" src="./img/thumb/GN6e01sbIAA20vy.jpg"/> 
](https://pbs.twimg.com/media/GN6e01sbIAA20vy?format=jpg&name=4096x4096) 
[<img title="30(点击查看原图)" height="100" src="./img/thumb/GLlumlLbsAAqp2A-1.jpg"/> 
](https://pbs.twimg.com/media/GLlumlLbsAAqp2A?format=jpg&name=4096x4096) 
[<img title="31(点击查看原图)" height="100" src="./img/thumb/GLlumlLbsAAqp2A-2.jpg"/> 
](https://pbs.twimg.com/media/GLlumlLbsAAqp2A?format=jpg&name=4096x4096) 
[<img title="32(点击查看原图)" height="100" src="./img/thumb/GLlumlLbsAAqp2A-3.jpg"/> 
](https://pbs.twimg.com/media/GLlumlLbsAAqp2A?format=jpg&name=4096x4096) 
[<img title="32.5(点击查看原图)" height="100" src="./img/thumb/GM5ttlbboAAdh0F.jpg"/> 
](https://pbs.twimg.com/media/GM5ttlbboAAdh0F?format=jpg&name=4096x4096) 
[<img title="32.6(点击查看原图)" height="100" src="./img/thumb/437537047_3136284753174063_3331222805259697633_n.jpg"/> 
](https://www.facebook.com/hashtag/北条司展) 
[<img title="42.3(点击查看原图)" height="100" src="./img/thumb/GNtIFhCaoAAubOx.jpg"/> 
](https://pbs.twimg.com/media/GNtIFhCaoAAubOx?format=jpg&name=4096x4096) 
[<img title="42.3(点击查看原图)" height="100" src="./img/thumb/th_A4_00214.webp"/> 
](https://news.denfaminicogamer.jp/kikakuthetower/240418b) 
[<img title="似乎是（不同于CafeZenon旁边的）另一个投影(点击查看原图)" height="100" src="./img/thumb/th_A4_09841.webp"/> 
](https://news.denfaminicogamer.jp/kikakuthetower/240418b) 
[<img title="42.3(点击查看原图)" height="100" src="./img/thumb/th_A4_09871.webp"/> 
](https://news.denfaminicogamer.jp/kikakuthetower/240418b) 
[<img title="32.8(点击查看原图)" height="100" src="./img/thumb/437123002_3136284713174067_5308807969396000572_n.jpg"/> 
](https://www.facebook.com/hashtag/北条司展) 
[<img title="32.9(点击查看原图)" height="100" src="./img/thumb/GM7uQ3fagAAbsfa.jpg"/> 
](https://pbs.twimg.com/media/GM7uQ3fagAAbsfa?format=jpg&name=4096x4096) 
[<img title="33(点击查看原图)" height="100" src="./img/thumb/GLrTOA4aMAApJlc-0.jpg"/> 
](https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096) 
[<img title="34(点击查看原图)" height="100" src="./img/thumb/GLrTOA4aMAApJlc-1.jpg"/> 
](https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096) 
[<img title="35(点击查看原图)" height="100" src="./img/thumb/GLrTOA4aMAApJlc-2.jpg"/> 
](https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096) 
[<img title="36(点击查看原图)" height="100" src="./img/thumb/GLrTOA4aMAApJlc-3.jpg"/> 
](https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096) 
[<img title="32.7(点击查看原图)" height="100" src="./img/thumb/437473109_3136284663174072_5149570692766340758_n.jpg"/> 
](https://www.facebook.com/hashtag/北条司展) 
[<img title="37.5(点击查看原图)" height="100" src="./img/thumb/GLp9d3Ma4AE27ID-0.jpg"/> 
](https://pbs.twimg.com/media/GLp9d3Ma4AE27ID?format=jpg&name=4096x4096) 
[<img title="38(点击查看原图)" height="100" src="./img/thumb/GLrD7FDa4AAf090.jpg"/> 
](https://pbs.twimg.com/media/GLrD7FDa4AAf090?format=jpg&name=4096x4096) 
[<img title="42(点击查看原图)" height="100" src="./img/thumb/GLmtTE2bUAASJOd.jpg"/> 
](https://pbs.twimg.com/media/GLmtTE2bUAASJOd?format=jpg&name=4096x4096) 
[<img title="42.3(点击查看原图)" height="100" src="./img/thumb/20240503102900.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240503/20240503102900.jpg) 
[<img title="42.3(点击查看原图)" height="100" src="./img/thumb/GNtBqeAaEAAQzxN.jpg"/> 
](https://pbs.twimg.com/media/GNtBqeAaEAAQzxN?format=jpg&name=4096x4096) 
[<img title="42.3(点击查看原图)" height="100" src="./img/thumb/GNtBqd-bcAApJzZ.jpg"/> 
](https://pbs.twimg.com/media/GNtBqd-bcAApJzZ?format=jpg&name=4096x4096) 
[<img title="后期2(点击查看原图)" height="100" src="./img/thumb/GQwr9LxbIAAkVVk.jpg"/> 
](https://pbs.twimg.com/media/GQwr9LxbIAAkVVk?format=jpg&name=4096x4096) 
[<img title="后期2.5(点击查看原图)" height="100" src="./img/thumb/GOt-91fa0AA1ZLO.jpg"/> 
](https://pbs.twimg.com/media/GOt-91fa0AA1ZLO?format=jpg&name=4096x4096) 
[<img title="后期3(点击查看原图)" height="100" src="./img/thumb/GQLpifaaUAEl_03.jpg"/> 
](https://pbs.twimg.com/media/GQLpifaaUAEl_03?format=jpg&name=4096x4096) 
[<img title="后期1(点击查看原图)" height="100" src="./img/thumb/GOOL4A9bYAAhNDp.jpg"/> 
](https://pbs.twimg.com/media/GOOL4A9bYAAhNDp?format=jpg&name=4096x4096) 
[<img title="后期4(点击查看原图)" height="100" src="./img/thumb/GQvs5HGbIAAM3yJ.jpg"/> 
](https://pbs.twimg.com/media/GQvs5HGbIAAM3yJ?format=jpg&name=4096x4096) 
[<img title="后期5(点击查看原图)" height="100" src="./img/thumb/GQvs5FzbsAAzBo5.jpg"/> 
](https://pbs.twimg.com/media/GQvs5FzbsAAzBo5?format=jpg&name=4096x4096) 

<a name="event03"></a>  
### 东京展二楼展区
[<img title="51(点击查看原图)" height="100" src="./img/thumb/GLmhNHEakAE32Hm.jpg"/> 
](https://pbs.twimg.com/media/GLmhNHEakAE32Hm?format=jpg&name=4096x4096) 
[<img title="52(点击查看原图)" height="100" src="./img/thumb/20240507092916.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240507/20240507092916.jpg) 
[<img title="52.5(点击查看原图)" height="100" src="./img/thumb/GPEWsE6bQAAf-XQ.jpg"/> 
](https://pbs.twimg.com/media/GPEWsE6bQAAf?format=jpg&name=4096x4096) 
[<img title="26资料、分镜等(点击查看原图)" height="100" src="./img/thumb/20240507092013.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240507/20240507092013.jpg) 
[<img title="周刊少年Jump 12月10日增刊，獠手中的枪指向自己。为什么？(点击查看原图)" height="100" src="./img/thumb/445141275_365824903134357_8880821983570487475_n.jpg"/> 
](https://www.instagram.com/p/C7JcH0eve0p/?img_index=5) 
[<img title="27资料、分镜等(点击查看原图)" height="100" src="./img/thumb/20240507092504.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240507/20240507092504.jpg) 
[<img title="28.3墨镜是Ray-Ban牌(点击查看原图)" height="100" src="./img/thumb/GMj9a9BaIAAl6bq.jpg"/> 
](https://pbs.twimg.com/media/GMj9a9BaIAAl6bq?format=jpg&name=4096x4096) 
[<img title="52.5(点击查看原图)" height="100" src="./img/thumb/20240507092354.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240507/20240507092354.jpg) 
[<img title="38.3(点击查看原图)" height="100" src="./img/thumb/GNbVNVLa8AAFzlX.jpg"/> 
](https://pbs.twimg.com/media/GNbVNVLa8AAFzlX?format=jpg&name=4096x4096) 
[<img title="38.5(点击查看原图)" height="100" src="./img/thumb/GMx5hKbbwAAScSK.jpg"/> 
](https://pbs.twimg.com/media/GMx5hKbbwAAScSK?format=jpg&name=4096x4096) 
[<img title="38.5(点击查看原图)" height="100" src="./img/thumb/GNqw2G8b0AAdJMW.jpg"/> 
](https://pbs.twimg.com/media/GNqw2G8b0AAdJMW?format=jpg&name=4096x4096) 
[<img title="57(点击查看原图)" height="100" src="./img/thumb/GLm6AB5aIAAF78b.jpg"/> 
](https://pbs.twimg.com/media/GLm6AB5aIAAF78b?format=jpg&name=4096x4096) 
[<img title="58(点击查看原图)" height="100" src="./img/thumb/GLmhNWMbUAELL5O.jpg"/> 
](https://pbs.twimg.com/media/GLmhNWMbUAELL5O?format=jpg&name=4096x4096) 
[<img title="58.5(点击查看原图)" height="100" src="./img/thumb/GL7EP27b0AAypdc.jpg"/> 
](https://pbs.twimg.com/media/GL7EP27b0AAypdc?format=jpg&name=4096x4096) 
[<img title="59(点击查看原图)" height="100" src="./img/thumb/GLq8uXza4AAJVYX.jpg"/> 
](https://pbs.twimg.com/media/GLq8uXza4AAJVYX?format=jpg&name=4096x4096) 
[<img title="59.2(点击查看原图)" height="100" src="./img/thumb/GLvbtnUagAAB1jf.jpg"/> 
](https://pbs.twimg.com/media/GLvbtnUagAAB1jf?format=jpg&name=4096x4096) 
[<img title="留言本(点击查看原图)" height="100" src="./img/thumb/GLwo7IabsAAoSHh.jpg"/> 
](https://pbs.twimg.com/media/GLwo7IabsAAoSHh?format=jpg&name=4096x4096)[^sketchbook] 
[<img title="60(点击查看原图)" height="100" src="./img/thumb/GLc4OJjaAAAkIfq.jpg"/> 
](https://pbs.twimg.com/media/GLc4OJjaAAAkIfq?format=jpg&name=4096x4096) 
[<img title="60.1(点击查看原图)" height="100" src="./img/thumb/GMLxrMSbsAAbwC9.jpg"/> 
](https://pbs.twimg.com/media/GMLxrMSbsAAbwC9?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦(点击查看原图)" height="100" src="./img/thumb/GNYg8s-agAAhrKf.jpg"/> 
](https://pbs.twimg.com/media/GNYg8s-agAAhrKf?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦2(点击查看原图)" height="100" src="./img/thumb/GN60aTAbQAALe4V.jpg"/> 
](https://pbs.twimg.com/media/GN60aTAbQAALe4V?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦3(点击查看原图)" height="100" src="./img/thumb/GOLcwSQawAAXuv5.jpg"/> 
](https://pbs.twimg.com/media/GOLcwSQawAAXuv5?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦4(点击查看原图)" height="100" src="./img/thumb/GOuAhrSbAAESQH5.jpg"/> 
](https://pbs.twimg.com/media/GOuAhrSbAAESQH5?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦5(点击查看原图)" height="100" src="./img/thumb/GPJDU9Ua4AATqif.jpg"/> 
](https://pbs.twimg.com/media/GPJDU9Ua4AATqif?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦6(点击查看原图)" height="100" src="./img/thumb/GPaFLMGagAA0QwU.jpg"/> 
](https://pbs.twimg.com/media/GPaFLMGagAA0QwU?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦7(点击查看原图)" height="100" src="./img/thumb/GPh2youakAA-KuW.jpg"/> 
](https://pbs.twimg.com/media/GPh2youakAA-KuW?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦8(点击查看原图)" height="100" src="./img/thumb/GPh2yorbsAAf1Jv.jpg"/> 
](https://pbs.twimg.com/media/GPh2yorbsAAf1Jv?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦9(点击查看原图)" height="100" src="./img/thumb/GPh2yorawAAPrIU.jpg"/> 
](https://pbs.twimg.com/media/GPh2yorawAAPrIU?format=jpg&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦10(点击查看原图)" height="100" src="./img/thumb/GPh2yorakAAGu9R.jpg"/> 
](https://pbs.twimg.com/media/GPh2yorakAAGu9R?format=jpg&name=4096x4096) 
[<img title="留言本上铃木亮平的涂鸦(点击查看原图)" height="100" src="./img/thumb/GP11sLLacAA5yqM.jpg"/> 
](https://pbs.twimg.com/media/GP11sLLacAA5yqM?format=png&name=4096x4096) 
[<img title="森田望智、伊倉一恵的留言(点击查看原图)" height="100" src="./img/thumb/GQZ8XnOaIAQdy-X.jpg"/> 
](https://pbs.twimg.com/media/GQZ8XnOaIAQdy-X?format=png&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦11(点击查看原图)" height="100" src="./img/thumb/GQqgo4lbMAAL_00.jpg"/> 
](https://pbs.twimg.com/media/GQqgo4lbMAAL_00?format=png&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦12(点击查看原图)" height="100" src="./img/thumb/GQwZyoeaoAEBwNK.jpg"/> 
](https://pbs.twimg.com/media/GQwZyoeaoAEBwNK?format=png&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦12(点击查看原图)" height="100" src="./img/thumb/GQ6jU66bwAE8dGP.jpg"/> 
](https://pbs.twimg.com/media/GQ6jU66bwAE8dGP?format=png&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦13(点击查看原图)" height="100" src="./img/thumb/GQ6jVAnbwAUQV6m.jpg"/> 
](https://pbs.twimg.com/media/GQ6jVAnbwAUQV6m?format=png&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦14(点击查看原图)" height="100" src="./img/thumb/GQ1ROkzaAAACmb2.jpg"/> 
](https://pbs.twimg.com/media/GQ1ROkzaAAACmb2?format=png&name=4096x4096) 
[<img title="留言本上粉丝的涂鸦14.1(点击查看原图)" height="100" src="./img/thumb/1040g008314cc8640me604a18f2vloj5oh3hueo0!nd_dft_wlteh_webp_3.webp"/> 
](https://www.xiaohongshu.com/discovery/item/66777d24000000001c036982?xsec_token=CBHXy0bQRVdICoXMY3K4EnHFqYe_BXeCqyWHmYiXUbaRc=) 
[<img title="留言本上粉丝的涂鸦14.2(点击查看原图)" height="100" src="./img/thumb/1040g008314cc8640me6g4a18f2vloj5oip8res8!nd_dft_wlteh_webp_3.webp"/> 
](https://www.xiaohongshu.com/discovery/item/66777d24000000001c036982?xsec_token=CBHXy0bQRVdICoXMY3K4EnHFqYe_BXeCqyWHmYiXUbaRc=) 
[<img title="留言本上粉丝的涂鸦14.3(点击查看原图)" height="100" src="./img/thumb/1040g008314cc8640me5g4a18f2vloj5oeal285g!nd_dft_wlteh_webp_3.webp"/> 
](https://www.xiaohongshu.com/discovery/item/66777d24000000001c036982?xsec_token=CBHXy0bQRVdICoXMY3K4EnHFqYe_BXeCqyWHmYiXUbaRc=) 
[<img title="留言本上粉丝的涂鸦14.4(点击查看原图)" height="100" src="./img/thumb/1040g008313f6tb3dg45043c4vm568hru8q0qe8o!nd_dft_wlteh_webp_3.webp"/> 
](https://www.xiaohongshu.com/discovery/item/66599f35000000000f00f6da?xsec_token=CBFzF7H7zDUMgUS59kiAND3ae7MW2vzfYnIM7ff80EQbg=) 
[<img title="留言本上粉丝的涂鸦15(点击查看原图)" height="100" src="./img/thumb/GQ6bfMzaQAA20cO.jpg"/> 
](https://pbs.twimg.com/media/GQ6bfMzaQAA20cO?format=png&name=4096x4096) 
[<img title="61.0(点击查看原图)" height="100" src="./img/thumb/20240507092638.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240507/20240507092638.jpg) 
[<img title="62.1(点击查看原图)" height="100" src="./img/thumb/GQqqWLSasAAgZ40.jpg"/> 
](https://pbs.twimg.com/media/GQqqWLSasAAgZ40?format=jpg&name=4096x4096) 
[<img title="61.2(点击查看原图)" height="100" src="./img/thumb/448915469_768460891837775_6721435720937718114_n.jpg"/> 
](https://scontent-ams2-1.cdninstagram.com/v/t51.29350-15/448915469_768460891837775_6721435720937718114_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams2-1.cdninstagram.com&_nc_cat=111&_nc_ohc=2gNLyRSTnI0Q7kNvgGo4ud4&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTYwNTc3Mg%3D%3D.3-ccb7-5&oh=00_AYCJiJBx0yaQ5a1SRJS92w_VSIOyu1MfI7T7FFuLiVqXVg&oe=67277199&_nc_sid=2d3a3f) 
[<img title="61.3(点击查看原图)" height="100" src="./img/thumb/449080005_454864213924638_2540173413885485248_n.jpg"/> 
](https://scontent-ams2-1.cdninstagram.com/v/t51.29350-15/449080005_454864213924638_2540173413885485248_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams2-1.cdninstagram.com&_nc_cat=106&_nc_ohc=DqxWoTv_x4oQ7kNvgFCB76R&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTU5MDE1OQ%3D%3D.3-ccb7-5&oh=00_AYBvHWd8g1Pczo3qr-Fz6vgJpg7NBJu9hVGoI4atvLsdng&oe=67276F76&_nc_sid=2d3a3f) 
[<img title="61.4(点击查看原图)" height="100" src="./img/thumb/449084922_1821405185038495_8744258635626615810_n.jpg"/> 
](https://scontent-ams4-1.cdninstagram.com/v/t51.29350-15/449084922_1821405185038495_8744258635626615810_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams4-1.cdninstagram.com&_nc_cat=107&_nc_ohc=q0iRZkKpotQQ7kNvgE32cZb&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTc4MjQ1NA%3D%3D.3-ccb7-5&oh=00_AYCFIuh2fWrL2MRm1PizgqqE4teJALg_OBdt9Nhu371RUQ&oe=67277063&_nc_sid=2d3a3f) 
[<img title="62(点击查看原图)" height="100" src="./img/thumb/th_A4_09450.webp"/> 
](https://news.denfaminicogamer.jp/kikakuthetower/240418b) 
[<img title="62.5(点击查看原图)" height="100" src="./img/thumb/GPr80F7boAEuMsw.jpg"/> 
](https://pbs.twimg.com/media/GPr80F7boAEuMsw?format=jpg&name=4096x4096) 
[<img title="62.6(点击查看原图)" height="100" src="./img/thumb/GQrgTvlbYAAdkUJ.jpg"/> 
](https://pbs.twimg.com/media/GQrgTvlbYAAdkUJ?format=jpg&name=4096x4096) 
[<img title="63(点击查看原图)" height="100" src="./img/thumb/GM5ttlbaEAAIwHa.jpg"/> 
](https://pbs.twimg.com/media/GM5ttlbaEAAIwHa?format=jpg&name=4096x4096) 
[<img title="64(点击查看原图)" height="100" src="./img/thumb/GN6fEKobEAAYLfH.jpg"/> 
](https://pbs.twimg.com/media/GN6fEKobEAAYLfH?format=jpg&name=4096x4096) 
[<img title="65(点击查看原图)" height="100" src="./img/thumb/GN6fEKnaMAAAaL-.jpg"/> 
](https://pbs.twimg.com/media/GN6fEKnaMAAAaL-?format=jpg&name=4096x4096) 
[<img title="Splash(点击查看原图)" height="100" src="./img/thumb/GLRg8aoaUAAzaQc.jpg"/> 
](https://pbs.twimg.com/media/GLRg8aoaUAAzaQc?format=jpg&name=4096x4096) 
[<img title="Splash,TaxiDriver(点击查看原图)" height="100" src="./img/2F_1.jpg"/> 
](https://www.coamix.co.jp/_next/image?url=https%3A%2F%2Fimages.microcms-assets.io%2Fassets%2F482f30523aae43bb88a97259ee592abe%2F81fdb6cf69374144a7938343a6be650a%2F2F_1.jpg%3Ffm%3Dwebp&w=1080&q=75) 
[<img title="66(点击查看原图)" height="100" src="./img/thumb/20240507092811.jpg"/> 
](https://cdn-ak.f.st-hatena.com/images/fotolife/y/yasubelog/20240507/20240507092811.jpg) 
[<img title="67(点击查看原图)" height="100" src="./img/thumb/GOvxH6CbEAA8K0k.jpg"/> 
](https://pbs.twimg.com/media/GOvxH6CbEAA8K0k?format=jpg&name=4096x4096) 

<a name="event04"></a>  
### 东京展一楼商品区  
[<img title="40(点击查看原图)" height="100" src="./img/thumb/GMdRAEhbQAAeRYE.jpg"/> 
](https://pbs.twimg.com/media/GMdRAEhbQAAeRYE?format=jpg&name=4096x4096) 
[<img title="41(点击查看原图)" height="100" src="./img/goods.jpg"/> 
](https://www.coamix.co.jp/_next/image?url=https%3A%2F%2Fimages.microcms-assets.io%2Fassets%2F482f30523aae43bb88a97259ee592abe%2F252d61b84475461599f0dc9a399781ce%2Fgoods.jpg%3Ffm%3Dwebp&w=1080&q=75) 
[<img title="50(点击查看原图)" height="100" src="./img/thumb/437926358_442920601590442_6979718179354540683_n.jpg"/> 
](https://www.facebook.com/TheEdition88/posts/pfbid02Kuc9ZTCYMVSpRxFpujRGk2kxSaXyMx3Rr2SyC8n4oRh9NJxNEcMEatg6ikCd5vBAl?__cft__[0]=AZV9HLfemvUxYl-CDHHEDnsbLMDTAYegY2wC8w7hX3D0E36hClSHruslqzNjIabfiJCRTIJJxjKpZI0mBzInmHO7Irz5E6zsUeYf8FeZnLlw_tPRslmoV-52G6EkmVw0ndj20o5NPDVIy3l3EcaS3G63fl67ZMltaKO0CxNW2BNLu3IXbYfgDrIlX_O7iTNCOrIITUGbxMavLzh5sA2TEpxp&__tn__=%2CO%2CP-R  ) 
[<img title="50(点击查看原图)" height="100" src="./img/thumb/gallery-zenon22.jpg"/> 
](https://kichinavi.net/gallery-zenon/) 
[<img title="42(点击查看原图)" height="100" src="./img/thumb/GMnh0AZbAAACCe1.jpg"/> 
](https://pbs.twimg.com/media/GMnh0AZbAAACCe1?format=jpg&name=4096x4096) 
[<img title="43(点击查看原图)" height="100" src="./img/thumb/GMnh0AZaEAAUNg-.jpg"/> 
](https://pbs.twimg.com/media/GMnh0AZaEAAUNg-?format=jpg&name=4096x4096) 
[<img title="44(点击查看原图)" height="100" src="./img/thumb/GMnh0AYbQAAxLKX.jpg"/> 
](https://pbs.twimg.com/media/GMnh0AYbQAAxLKX?format=jpg&name=4096x4096) 
[<img title="45(点击查看原图)" height="100" src="./img/thumb/GMsrJYtaQAAobNm.jpg"/> 
](https://pbs.twimg.com/media/GMsrJYtaQAAobNm?format=jpg&name=4096x4096) 
[<img title="46(点击查看原图)" height="100" src="./img/thumb/GNrrvz1a8AAM22t.jpg"/> 
](https://pbs.twimg.com/media/GNrrvz1a8AAM22t?format=jpg&name=4096x4096) 
![](img/thumb/2_hojo-ten_acrylicblock_0.jpg) 
![](img/thumb/2_hojo-ten_acrylicblock_1.jpg) 
[<img title="47(点击查看原图)" height="100" src="./img/thumb/GLiGy9UaMAAj7Gi.jpg"/> 
](https://pbs.twimg.com/media/GLiGy9UaMAAj7Gi?format=jpg&name=4096x4096) 
[<img title="48(点击查看原图)" height="100" src="./img/thumb/GLhGV0Ea0AAnL3C.jpg"/> 
](https://pbs.twimg.com/media/GLhGV0Ea0AAnL3C?format=jpg&name=4096x4096) 
[<img title="49(点击查看原图)" height="100" src="./img/thumb/GLhdpVvawAA_C9N.jpg"/> 
](https://pbs.twimg.com/media/GLhdpVvawAA_C9N?format=jpg&name=4096x4096) 
[<img title="50(点击查看原图)" height="100" src="./img/thumb/gallery-zenon21.jpg"/> 
](https://kichinavi.net/gallery-zenon/) 
[<img title="51(点击查看原图)" height="100" src="./img/thumb/gallery-zenon7.jpg"/> 
](https://kichinavi.net/gallery-zenon/) 
[<img title="52(点击查看原图)" height="100" src="./img/thumb/GLfd74HaEAAW_IZ.jpg"/> 
](https://pbs.twimg.com/media/GLfd74HaEAAW_IZ?format=jpg&name=4096x4096) 
[<img title="53(点击查看原图)" height="100" src="./img/thumb/440929353_924759052992049_5267001205666543415_n.jpg"/> 
](https://www.facebook.com/permalink.php?story_fbid=pfbid0Sm8eSPLM3z7Zg3X1BMe3pm2SKm3vpDWfDN9iAtZxNkCW3fHURJhRRC6gBMsw2AChl&id=100063738872905&__cft__[0]=AZVdbxCh30jMct4kIfvMbTjOn-lH1ewAt2AH0Zi1qpBR_t8W2slFh1pZanQXAZP-A5WswM70QlJL6yE1n-NbkfqPHFjvH8_7im3W_tKoy678ndTagh_9VyvQCtbEBzJJtu51wT3BaHSzXAUmATYCcBrShEoFDDzaxe-FpF08XCkXJWZnjQbWk5JOFZCpHdVzu1ZHFkBAWICaDLUGdGN-XOhK&__tn__=%2CO%2CP-R) 
[<img title="54(点击查看原图)" height="100" src="./img/thumb/438254108_441213351761167_7571083949047636205_n.jpg"/> 
](https://www.facebook.com/TheEdition88/posts/pfbid034Lf1dLWfKe9EEqbV22CJmKnjejdyd3XxzEDSsx2MaFsm4C5bQ3YvXzsRi3QsRnatl?__cft__[0]=AZXqLetQyVgvPOvkCagcPJodn9GbZGDHvCK8HKFRtQ-OS0ZNSXHAVmYhIW1zSsSwBuySRG7YbfLu8ui4eZT4oWCq5S0LltZK_TBugLjBlVKBgbTFINjeMzBNuSs_QHP7cqG3HcvfdsRy022LEzmYF97sv9Y2lTQCI2WGN27JjnJXWgxFe-lrGkzrBsge6L-Kefh6WwWwMCSPwJQPv_6YJ6Hx&__tn__=%2CO%2CP-R) 
[<img title="55(点击查看原图)" height="100" src="./img/thumb/gallery-zenon23.jpg"/> 
](https://kichinavi.net/gallery-zenon/) 
[<img title="56(点击查看原图)" height="100" src="./img/thumb/gallery-zenon20.jpg"/> 
](https://kichinavi.net/gallery-zenon/) 
[<img title="57(点击查看原图)" height="100" src="./img/thumb/gallery-zenon6.jpg"/> 
](https://kichinavi.net/gallery-zenon/) 

<a name="event05"></a>  
### 东京展其他场景  

[<img title="54(点击查看原图)" height="100" src="./img/cafe.jpg"/> 
](https://www.coamix.co.jp/_next/image?url=https%3A%2F%2Fimages.microcms-assets.io%2Fassets%2F482f30523aae43bb88a97259ee592abe%2F45f2f128a8e94626954cb6ff7cac1cf5%2Fcafe.jpg%3Ffm%3Dwebp&w=1080&q=75) 
[<img title="56(点击查看原图)" height="100" src="./img/thumb/GLb3rM1bMAAT52a.jpg"/> 
](https://pbs.twimg.com/media/GLb3rM1bMAAT52a?format=jpg&name=4096x4096) 
[<img title="61(点击查看原图)" height="100" src="./img/thumb/GLlunsebQAExaCE.jpg"/> 
](https://pbs.twimg.com/media/GLlunsebQAExaCE?format=jpg&name=4096x4096) 
[<img title="62(点击查看原图)" height="100" src="./img/thumb/GLhI5gJbsAAnp9S.jpg"/> 
](https://pbs.twimg.com/media/GLhI5gJbsAAnp9S?format=jpg&name=4096x4096) 
[<img title="63(点击查看原图)" height="100" src="./img/thumb/GLhUyjHbwAEFJqu.jpg"/> 
](https://pbs.twimg.com/media/GLhUyjHbwAEFJqu?format=jpg&name=4096x4096) 
[<img title="后期(点击查看原图)" height="100" src="./img/thumb/GOKzAI8bwAAj9qC.jpg"/> 
](https://pbs.twimg.com/media/GOKzAI8bwAAj9qC?format=jpg&name=4096x4096) 
[<img title="64(点击查看原图)" height="100" src="./img/thumb/GLlLDM9bMAAbTrs.jpg"/> 
](https://pbs.twimg.com/media/GLlLDM9bMAAbTrs?format=jpg&name=4096x4096) 
[<img title="65(点击查看原图)" height="100" src="./img/thumb/GLlyGigbwAApdQt.jpg"/> 
](https://pbs.twimg.com/media/GLlyGigbwAApdQt?format=jpg&name=4096x4096) 
[<img title="66(点击查看原图)" height="100" src="./img/thumb/GLlyGjHawAAXEju.jpg"/> 
](https://pbs.twimg.com/media/GLlyGjHawAAXEju?format=jpg&name=4096x4096) 
[<img title="66.1(点击查看原图)" height="100" src="./img/thumb/447599988_1185883082763294_6775023398282985122_n.jpg"/> 
](https://scontent.cdninstagram.com/v/t51.29350-15/447599988_1185883082763294_6775023398282985122_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=106&_nc_ohc=tB-FHnp5EHIQ7kNvgFdm64K&_nc_gid=2ce403efc21c49a784f4c858c54e6dd6&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTIwMTg3NzY4NDE4NzgwMw%3D%3D.3-ccb7-5&oh=00_AYCU8Y1xmOFJBEMKDy7b504M8LsfE404H67HhYooGW4yng&oe=67277FCE&_nc_sid=10d13b) 
[<img title="66.2(点击查看原图)" height="100" src="./img/thumb/447515166_2817350341762080_4400973400564973301_n.jpg"/> 
](https://scontent.cdninstagram.com/v/t51.29350-15/447515166_2817350341762080_4400973400564973301_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=100&_nc_ohc=ORJHXYGHfXEQ7kNvgGJKtco&_nc_gid=2ce403efc21c49a784f4c858c54e6dd6&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTIwMTg3NzU0OTk0OTE5OQ%3D%3D.3-ccb7-5&oh=00_AYCqXEtF87_Y5YktrVy25jyxOME0mZVLCIdSU1wPjIcI7A&oe=672786D3&_nc_sid=10d13b) 
[<img title="66.3(点击查看原图)" height="100" src="./img/thumb/447464673_961623635655987_1790022386997377649_n.jpg"/> 
](https://scontent.cdninstagram.com/v/t51.29350-15/447464673_961623635655987_1790022386997377649_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=103&_nc_ohc=ydKveQsDib0Q7kNvgEfpgsx&_nc_gid=2ce403efc21c49a784f4c858c54e6dd6&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTIwMTg3NzU0OTc0NTc1NQ%3D%3D.3-ccb7-5&oh=00_AYCWnTt4nHPpKdkiI0CZP3WSa0Gslbv01o-LWr0eh6hRFw&oe=6727884D&_nc_sid=10d13b) 
[<img title="67(点击查看原图)" height="100" src="./img/thumb/GLmG9eybUAAYZi-.jpg"/> 
](https://pbs.twimg.com/media/GLmG9eybUAAYZi-?format=jpg&name=4096x4096) 
[<img title="68(点击查看原图)" height="100" src="./img/thumb/GLplalIa4AAqVTo.jpg"/> 
](https://pbs.twimg.com/media/GLplalIa4AAqVTo?format=jpg&name=4096x4096) 
[<img title="68.5(点击查看原图)" height="100" src="./img/thumb/GMzOBZSakAES3wZ.jpg"/> 
](https://pbs.twimg.com/media/GMzOBZSakAES3wZ?format=jpg&name=4096x4096) 
[<img title="图中文字是北条司推荐的咖啡，详见注释(点击查看原图)" height="100" src="./img/thumb/GLWv6OVacAAfI0j.jpg"/> 
](https://pbs.twimg.com/media/GLWv6OVacAAfI0j?format=jpg&name=4096x4096) [^coffee] 
[<img title="69(点击查看原图)" height="100" src="./img/thumb/GL6aL7OaEAAlogM.jpg"/> 
](https://pbs.twimg.com/media/GL6aL7OaEAAlogM?format=jpg&name=4096x4096)[^coffee2] 
[<img title="71(点击查看原图)" height="100" src="./img/thumb/GMUdHorWgAAEM8b.jpg"/> 
](https://pbs.twimg.com/media/GMUdHorWgAAEM8b?format=jpg&name=4096x4096) 
[<img title="71.5(点击查看原图)" height="100" src="./img/thumb/GMChNkrasAAQLHM.jpg"/> 
](https://pbs.twimg.com/media/GMChNkrasAAQLHM?format=jpg&name=4096x4096) 
[<img title="71.5(点击查看原图)" height="100" src="./img/thumb/440333092_17884137027028057_5467865927626413386_n.jpg"/> 
](https://www.facebook.com/photo/?fbid=7281659565264853&set=pcb.7281659735264836) 
[<img title="72(点击查看原图)" height="100" src="./img/thumb/GPhLoZ4aEAAEHF3.jpg"/> 
](https://pbs.twimg.com/media/GPhLoZ4aEAAEHF3?format=jpg&name=4096x4096) 
[<img title="73(点击查看原图)" height="100" src="./img/thumb/GLg2QZgbwAAtkmF.jpg"/> 
](https://pbs.twimg.com/media/GLg2QZgbwAAtkmF?format=jpg&name=4096x4096) 
[<img title="71(点击查看原图)" height="100" src="./img/thumb/GMChsOqa4AAeU1n.jpg"/> 
](https://pbs.twimg.com/media/GMChsOqa4AAeU1n?format=jpg&name=4096x4096) 
[<img title="72(点击查看原图)" height="100" src="./img/thumb/GLg6kvobEAAVnPM.jpg"/> 
](https://pbs.twimg.com/media/GLg6kvobEAAVnPM?format=jpg&name=4096x4096) 
[<img title="73(点击查看原图)" height="100" src="./img/thumb/GL_U2BHbcAAjSPY.jpg"/> 
](https://pbs.twimg.com/media/GL_U2BHbcAAjSPY?format=jpg&name=4096x4096) 
[<img title="朝日新闻报道(点击查看原图)" height="100" src="./img/thumb/GLmzjW9bUAAQlj9.jpg"/> 
](https://pbs.twimg.com/media/GLmzjW9bUAAQlj9?format=jpg&name=4096x4096) 

入场者特典 特制卡 前期：  
![](img/thumb/ht-zenkitokuten_10.jpg) 
![](img/thumb/ht-zenkitokuten_11.jpg) ，
![](img/thumb/fukuoka-tokuten-re.jpg-20.webp) 
![](img/thumb/fukuoka-tokuten-re.jpg-21.webp) ， 
![](img/thumb/fukuoka-tokuten-re.jpg-30.webp) 
![](img/thumb/fukuoka-tokuten-re.jpg-31.webp) ， 
![](img/thumb/fukuoka-tokuten-re.jpg-40.webp) 
![](img/thumb/fukuoka-tokuten-re.jpg-41.webp) ， 
![](img/thumb/ht-zenkitokuten_50.jpg) 
![](img/thumb/ht-zenkitokuten_51.jpg) ， 
![](img/thumb/fukuoka-tokuten-re.jpg-60.webp) 
![](img/thumb/ht-zenkitokuten_61.jpg) 



[^sketchbook]: 黄黑色本子似乎是常用的漫画草稿本：[![](https://blog.fromjapan.co.jp/en/wp-content/uploads/2022/01/Art-Supplies-Paper.png)](https://blog.fromjapan.co.jp/en/anime/the-best-art-supplies-from-japan-for-mangakas-and-illustrators.html)  

[^coffee]:  
漫画家と珈琲 NO.1  Tsukasa Hojo Blend  ・北条司プレンド・  
漫画家 北条司監修の特製オリジナルプレンドです。北条司が学生の頃から愛してやまないスマトラ島産「マンデリン」を中心にタンザニア、プラジル、コロンビアの豆をプレンドしました。濃厚な味わいと良質で纖細な苦みを是非ご賞味ください。  
【北条司コメント】  
私にとって咖啡は何気ない生活のー部であり欠かせないものです。普段は朝起きた時やランチの後などに毎日飲みますし、喫茶店にもよく行きます。漫画連載中は決まって夜10時にアシスタントたちと珈琲プレイクしていましたね。私のおすすめマンデリンプレンドを皆さんにも楽しんで頂けると嬉しいです。  
（译(待校对)：漫画家和咖啡NO.1  Tsukasa Hojo Blend  ・北条司Blend・  
这是由漫画家北条司监督的特别原创混合咖啡。它混合了坦桑尼亚、巴西和哥伦比亚的咖啡豆，重点是北条司从学生时代起就喜爱的Mandarin咖啡豆。 请尽情享受这浓郁的口感和细腻的苦味。   
[Tsukasa Hojo的评论]  
咖啡是我日常生活中不可或缺的一部分。 我通常在每天早上起床或午饭后喝咖啡，也经常去咖啡店。 我在创作漫画系列时，经常在晚上 10 点和助手们一起喝咖啡。 希望大家喜欢我推荐的Mandarin Blend。）   

[^coffee2]:  
・水出しアイスコーヒー   Y600  
北条司ブレンドを使った当店のアイスコーヒは、水からゆっくりと抽出することにより、强い酸味や油膜が抑えられ、なめらかで味わし深しい美味しさを引き出します。    
・スペシャリティ水出しアイスコーヒー  ※数量限定 Y650～Y700  
当店のスペシャルティー水出しアイスコーヒーは、北条司プしンドと日本の百名水選の熊本県南阿蘇の「白川水源」の水を使用しています。1滴1滴落とし、約5時間かけて抽出するアイス一ヒーは、豆の豊かな香りとマイルドでコクのある甘みがたっぶりと溶け込んだこだわりの1杯です!  
・ドリップコーヒー Y600  
北条司ブレンド、その他シングルオリジンコーヒーを4種ご用意しております詳しくはスタッフまでお声掛けください。  
※ご注文を頂いてから豆を挽き、1杯ずっハンドドリップでお淹れ致します。  
1杯ずつお淹れします為、ご提供に5分ほど顶きますのでご了承くださいませ。  
（译：  
・水冲冰咖啡 Y600  
我们的冰咖啡使用北条司Blend配方，用水慢慢冲泡，减少了浓酸和油膜，带来柔滑、深厚和美味的口感。   
・特制冰咖啡，加水冲泡 ※数量有限 Y650-Y700  
我们的特制冰咖啡采用北条路普辛朵咖啡豆和熊本县南阿苏的白川水源（日本百佳水源之一）制作而成。冰咖啡经过五个小时一滴一滴的冲泡，咖啡豆的浓郁香气和温和醇厚的甜味在这杯特别的咖啡中得到了充分的体现!   
・滴漏式咖啡 600 美元  
我们备有北条司混合咖啡和其他4种单品咖啡，详情请向工作人员咨询。  
※我们在收到您的订单后会研磨咖啡豆，然后手工滴漏每1杯咖啡。 
每杯咖啡需等待 5 分钟。 
）

<a name="event10"></a>  
### 福冈展场外  
[<img title="FA01(点击查看原图)" height="100" src="img/thumb/GaNxIw-bUAAxKAd.jpg"/> 
](https://pbs.twimg.com/media/GaNxIw-bUAAxKAd?format=jpg&name=large) 
[<img title="FA02(点击查看原图)" height="100" src="img/thumb/GaTG0_-bUAEiGMh.jpg"/> 
](https://pbs.twimg.com/media/GaTG0_-bUAEiGMh?format=jpg&name=large) 

<a name="event11"></a>  
### 福冈展展区    
[<img title="FB01(点击查看原图)" height="100" src="img/thumb/GaBQeunbkAA0qzd.jpg"/> 
](https://pbs.twimg.com/media/GaBQeunbkAA0qzd?format=jpg&name=large) 
[<img title="FB015(点击查看原图)" height="100" src="img/thumb/Ga4q9Z8aQAEnQFD.jpg"/> 
](https://pbs.twimg.com/media/Ga4q9Z8aQAEnQFD?format=jpg&name=large) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-2.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-3.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/GaxzJbUboAEFdL6.jpg"/>
](https://pbs.twimg.com/media/GaxzJbUboAEFdL6?format=jpg&name=4096x4096) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-4.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB03(点击查看原图)" height="100" src="img/thumb/GaEOGAVb0AAz-6T.jpg"/> 
](https://pbs.twimg.com/media/GaEOGAVb0AAz-6T?format=jpg&name=large) 
[<img title="FB04(点击查看原图)" height="100" src="img/thumb/GaO5cSDbUAM8g4u.jpg"/> 
](https://pbs.twimg.com/media/GaO5cSDbUAM8g4u?format=jpg&name=large) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-11.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/Ga5Cc4wboAAvncE.jpg"/> 
](https://pbs.twimg.com/media/Ga5Cc4wboAAvncE?format=jpg&name=large) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/GaU_7YEbYAApPeR.jpg"/> 
](https://pbs.twimg.com/media/GaU_7YEbYAApPeR?format=jpg&name=large) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/Ga5Cc41aoAAh2Ja.jpg"/> 
](https://pbs.twimg.com/media/Ga5Cc41aoAAh2Ja?format=jpg&name=large) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/Ga4W4BpaMAAk8Ro.jpg"/> 
](https://pbs.twimg.com/media/Ga4W4BpaMAAk8Ro?format=jpg&name=large) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/Ga4W4BnaMAAOiQt.jpg"/> 
](https://pbs.twimg.com/media/Ga4W4BnaMAAOiQt?format=jpg&name=large) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/Ga4q9Z8boAAbgjS.jpg"/> 
](https://pbs.twimg.com/media/Ga4q9Z8boAAbgjS?format=jpg&name=large) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/Ga4q9Z6bsAAA4De.jpg"/> 
](https://pbs.twimg.com/media/Ga4q9Z6bsAAA4De?format=jpg&name=large) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/GayirkQbMAA65jl.jpg"/>
](https://pbs.twimg.com/media/GayirkQbMAA65jl?format=jpg&name=4096x4096) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/GayZYmlbsAAuTns.jpg"/>
](https://pbs.twimg.com/media/GayZYmlbsAAuTns?format=jpg&name=4096x4096) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/GayirkPboAAqhkd.jpg"/>
](https://pbs.twimg.com/media/GayirkPboAAqhkd?format=jpg&name=4096x4096) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/GasdXdOboAAYgwr.jpg"/>
](https://pbs.twimg.com/media/GasdXdOboAAYgwr?format=jpg&name=4096x4096) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/GasdXcdaAAE1MWN.jpg"/>
](https://pbs.twimg.com/media/GasdXcdaAAE1MWN?format=jpg&name=4096x4096) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/Gaoi37LaEAEUE9C.jpg"/>
](https://pbs.twimg.com/media/Gaoi37LaEAEUE9C?format=jpg&name=4096x4096) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/Gaoi37KboAAxAQY.jpg"/>
](https://pbs.twimg.com/media/Gaoi37KboAAxAQY?format=jpg&name=4096x4096) 

<a name="event12"></a>  
### 福冈展留言区  
[<img title="FC01(点击查看原图)" height="100" src="img/thumb/Ga5Cc4vakAAOcG_.jpg"/> 
](https://pbs.twimg.com/media/Ga5Cc4vakAAOcG_?format=jpg&name=large) 
[<img title="FC01(点击查看原图)" height="100" src="img/thumb/GaO5cSBbUAI7RrD.jpg"/> 
](https://pbs.twimg.com/media/GaO5cSBbUAI7RrD?format=jpg&name=large) 
[<img title="FC02(点击查看原图)" height="100" src="img/thumb/GaETchpaUAA_R3Q.jpg"/> 
](https://pbs.twimg.com/media/GaETchpaUAA_R3Q?format=jpg&name=large) 
[<img title="FC03(点击查看原图)" height="100" src="img/thumb/Ga5oXXhaIAEZjAK.jpg"/> 
](https://pbs.twimg.com/media/Ga5oXXhaIAEZjAK?format=jpg&name=large) 
[<img title="FB05(点击查看原图)" height="100" src="img/thumb/GasdXiKbIAAq-TZ.jpg"/>
](https://pbs.twimg.com/media/GasdXiKbIAAq-TZ?format=jpg&name=4096x4096) 

<a name="event13"></a>  
### 福冈展展区商品区
[<img title="FD01(点击查看原图)" height="100" src="img/thumb/GasdXZ-aAAAqPQB.jpg"/> 
](https://pbs.twimg.com/media/GasdXZ-aAAAqPQB?format=jpg&name=large) 
[<img title="FD03(点击查看原图)" height="100" src="img/thumb/GYoqnvLaoAA1pYw.jpg"/> 
](https://pbs.twimg.com/media/GYoqnvLaoAA1pYw?format=jpg&name=large) 
[<img title="FD04(点击查看原图)" height="100" src="img/thumb/GYoqnvEbkAEUbKn.jpg"/> 
](https://pbs.twimg.com/media/GYoqnvEbkAEUbKn?format=jpg&name=large) 
[<img title="FD05(点击查看原图)" height="100" src="img/thumb/GZMiNE8bQAA-E4i.jpg"/> 
](https://pbs.twimg.com/media/GZMiNE8bQAA-E4i?format=jpg&name=large) 
[<img title="FD06(点击查看原图)" height="100" src="img/thumb/GZMiMR8bYAA2L4r.jpg"/> 
](https://pbs.twimg.com/media/GZMiMR8bYAA2L4r?format=jpg&name=large) 
[<img title="FD07(点击查看原图)" height="100" src="img/thumb/GaU7tGUasAA0mG3.jpg"/> 
](https://pbs.twimg.com/media/GaU7tGUasAA0mG3?format=jpg&name=large) 
[<img title="FD08(点击查看原图)" height="100" src="img/thumb/GaU7taobUAAV1dZ.jpg"/> 
](https://pbs.twimg.com/media/GaU7taobUAAV1dZ?format=jpg&name=large) 
[<img title="FD09(点击查看原图)" height="100" src="img/thumb/GaU7s5KaYAAfnn4.jpg"/> 
](https://pbs.twimg.com/media/GaU7s5KaYAAfnn4?format=jpg&name=large) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-6.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-7.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-8.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-9.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-10.webp"/> 
](./2024-10-18_report_fukuoka.md) 
[<img title="FB02(点击查看原图)" height="100" src="img/thumb/20241015-hojotsukasaten-12.webp"/> 
](./2024-10-18_report_fukuoka.md) 


<a name="BuyersPhoto"></a>  
### 买家实拍
以下图片源于网上买家的照片，侵则删。  
[<img title="B00(点击查看原图)" height="100" src="img/thumb/GLrTOA3a8AAfniq.jpg"/> 
](https://pbs.twimg.com/media/GLrTOA3a8AAfniq?format=jpg&name=large) 
[<img title="B01(点击查看原图)" height="100" src="img/thumb/GLfc3DiaQAAaJ8J.jpg"/> 
](https://pbs.twimg.com/media/GLfc3DiaQAAaJ8J?format=jpg&name=large) 
![](img/thumb/GLkiyLHbgAAGlHa.jpg) 
![](img/thumb/GLkiyLGbAAABpyz.jpg) 
![](img/thumb/GLaflwdbkAAAsT7.jpg) 
![](img/thumb/GLZ4L6zbAAA-IaX.jpg) 
![](img/thumb/GLh0qfcboAA7rb8.jpg) 
![](img/thumb/GLh0qfdbAAAUz9c.jpg) 
![](img/thumb/GLh0qfdbwAI6cvl.jpg) 
![](img/thumb/GLh0qfgbEAAp0vI.jpg) 
[<img title="B02(点击查看原图)" height="100" src="./img/thumb/GLwo7IaaUAAM3uB.jpg"/> 
](https://pbs.twimg.com/media/GLwo7IaaUAAM3uB?format=jpg&name=4096x4096) 
[<img title="B03(点击查看原图)" height="100" src="./img/thumb/GMNWKClaUAARbPA.jpg"/> 
](https://pbs.twimg.com/media/GMNWKClaUAARbPA?format=jpg&name=4096x4096) 
[<img title="B04(点击查看原图)" height="100" src="./img/thumb/GMPoxkuaQAAj4sW.jpg"/> 
](https://pbs.twimg.com/media/GMPoxkuaQAAj4sW?format=jpg&name=4096x4096) 
[<img title="B05(点击查看原图)" height="100" src="./img/thumb/GMzu7WaawAAkU0_.jpg"/> 
](https://pbs.twimg.com/media/GMzu7WaawAAkU0_?format=jpg&name=4096x4096) 
[<img title="B06(点击查看原图)" height="100" src="./img/thumb/GMywrmPagAAE5Mm.jpg"/> 
](https://pbs.twimg.com/media/GMywrmPagAAE5Mm?format=jpg&name=4096x4096) 
[<img title="B07(点击查看原图)" height="100" src="./img/thumb/GQMYe_QacAAq1I2.jpg"/> 
](https://pbs.twimg.com/media/GQMYe_QacAAq1I2?format=jpg&name=4096x4096) 
[<img title="入场特典1(点击查看原图)" height="100" src="./img/thumb/GQMYe-paEAAqLEj.jpg"/> 
](https://pbs.twimg.com/media/GQMYe-paEAAqLEj?format=jpg&name=4096x4096) 
[<img title="入场特典2(点击查看原图)" height="100" src="./img/thumb/GQLxyAxbgAAqXgU.jpg"/> 
](https://pbs.twimg.com/media/GQLxyAxbgAAqXgU?format=jpg&name=4096x4096) 
[<img title="入场特典3(点击查看原图)" height="100" src="./img/thumb/GQLxyA0aAAAPCcN.jpg"/> 
](https://pbs.twimg.com/media/GQLxyA0aAAAPCcN?format=jpg&name=4096x4096) 
[<img title="B08(点击查看原图)" height="100" src="./img/thumb/GQNa-j_a0AAy8vK.jpg"/> 
](https://pbs.twimg.com/media/GQNa-j_a0AAy8vK?format=jpg&name=4096x4096) 
[<img title="B09(点击查看原图)" height="100" src="./img/thumb/GZ_6s8ibEAArHVK.jpg"/> 
](https://pbs.twimg.com/media/GZ_6s8ibEAArHVK?format=jpg&name=4096x4096) 
[<img title="B10(点击查看原图)" height="100" src="./img/thumb/GY9DuXNbAAATDrN.jpg"/> 
](https://pbs.twimg.com/media/GY9DuXNbAAATDrN?format=jpg&name=4096x4096) 
[<img title="B11(点击查看原图)" height="100" src="./img/thumb/GY3GdHbaAAAkavy.jpg"/> 
](https://pbs.twimg.com/media/GY3GdHbaAAAkavy?format=jpg&name=4096x4096) 
[<img title="B12(点击查看原图)" height="100" src="./img/thumb/GaFGwLib0AMGqSf.jpg"/> 
](https://pbs.twimg.com/media/GaFGwLib0AMGqSf?format=jpg&name=4096x4096) 
[<img title="B13(点击查看原图)" height="100" src="./img/thumb/GaFH1Rnb0AERqiO.jpg"/> 
](https://pbs.twimg.com/media/GaFH1Rnb0AERqiO?format=jpg&name=4096x4096) 
[<img title="B14(点击查看原图)" height="100" src="./img/thumb/GaOV0YqbUAQ8p1u.jpg"/> 
](https://pbs.twimg.com/media/GaOV0YqbUAQ8p1u?format=jpg&name=4096x4096) 
[<img title="B15(点击查看原图)" height="100" src="./img/thumb/GaLDy3YagAAbzDr.jpg"/> 
](https://pbs.twimg.com/media/GaLDy3YagAAbzDr?format=jpg&name=4096x4096) 
[<img title="B15(点击查看原图)" height="100" src="./img/thumb/GafiAq8bEAAEkvF.jpg"/> 
](https://pbs.twimg.com/media/GafiAq8bEAAEkvF?format=jpg&name=4096x4096) 
[<img title="B15(点击查看原图)" height="100" src="./img/thumb/Ga4q9Z6bIAAMu7x.jpg"/> 
](https://pbs.twimg.com/media/Ga4q9Z6bIAAMu7x?format=jpg&name=4096x4096) 

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
