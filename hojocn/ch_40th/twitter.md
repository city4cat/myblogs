source:  
    - [北条司展 - Twitter](https://twitter.com/hashtag/北条司展?f=live)  
    - [(#北条司展) until:2024-10-15 since:2024-06-24](https://x.com/search?q=(%23北条司展) until:2024-10-15 since:2024-06-24&src=typed_query&f=live)  
    - [北条司原画展 - Twitter](https://x.com/hashtag/北条司原画展?f=live)  
    - [北条司展 - Facebook](https://www.facebook.com/hashtag/北条司展)  
    - [北条司展 - Instagram](https://www.instagram.com/explore/tags/北条司展)  
    - [北条司展後期 - Instagram](https://www.instagram.com/explore/tags/北条司展後期)  
    - [北条司展 - XHS](https://www.xiaohongshu.com/search_result?keyword=北条司展&source=web_explore_feed)  
    - [GALLERYZENON - Twitter](https://twitter.com/hashtag/GALLERYZENON?f=live)  
    - [GalleryZenon Offical@Twitter](https://twitter.com/GalleryZenon)
    - [北条司 - Twitter](https://twitter.com/hashtag/北条司?f=live)  
    - [キャッツアイ - Twitter](https://twitter.com/hashtag/キャッツアイ?f=live)  
    - [カフェゼノン - Twitter](https://twitter.com/hashtag/カフェゼノン?f=live)   //Cafe Zenon  
    - [ギャラリーゼノン - Twitter](https://twitter.com/hashtag/ギャラリーゼノン?f=live)  //Gallery Zenon  
    - [ゼノンギャラリー - Twitter](https://twitter.com/hashtag/ゼノンギャラリー?f=live)  //Zenon Gallery  
    - [冴羽獠 - Twitter](https://twitter.com/hashtag/冴羽獠?f=live)  
    - [吉祥寺 - Twitter](https://twitter.com/hashtag/吉祥寺?f=live)  
    - [原画展 - Twitter](https://twitter.com/hashtag/原画展?f=live)  
    - [鈴木亮平 - Instagram](https://www.instagram.com/ryoheisuzuki_cityhunter/)  


https://www.instagram.com/p/C61UDoRSRdz  
カフェメニューはボリューム満点。後期はまたメニュー変わるようなので、また食べます🍀  
咖啡菜单分量十足。后期菜单好像又变了，再吃🍀  
```   
https://scontent.cdninstagram.com/v/t51.29350-15/436294695_320389327753256_7244825573645416655_n.webp?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=108&_nc_ohc=KsJoMqfCyO0Q7kNvgHziNrW&_nc_gid=13a52985c32245548644ab60e7b78388&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM2NTY4NDQ5ODE4Mzc1Njg5Ng%3D%3D.3-ccb7-5&oh=00_AYDNGBZdZ1j_rk37InlAAEmdGp3hsudTmQ7qLdcuyMeYog&oe=6727B5C9&_nc_sid=10d13b  
```   


https://www.instagram.com/p/C8l-nsYyFET/  
北条司展Part.3  Angel Heartも❣️  Angel Heartは、途中まで買って読んでたんだけどどこからかなぁ…話に頭がついて行かなくて断念してるんだよねぇ。いつかまた、ちまちま1巻から買い直して揃えたいなぁ。過去に発売されてたのかな？非売品もあるのかな？グッズもたくさん展示されてて、きっと今じゃ手に入らないんだろうけどめちゃ欲しくなった😭奈美恵さんのもだけどさー  好きだと収集癖がウズウスしちゃう😂7枚目のライトとか、本当欲しいんだけど😍部屋に飾りたいわー✨️  
北条司展Part.3  Angel Heart❣️  Angel Heart读到一半，不知从哪里开始读… 脑子跟不上，只好死心了。总有一天，我还想重新买一集。过去发售过吗？也有非卖品吗？展示了很多周边商品，虽然现在肯定买不到，但是很想要😭奈美惠小姐的也是  喜欢的话就会有收集癖😂第七张灯什么的，真的很想要😍装饰房间✨️  
```   
https://scontent-ams4-1.cdninstagram.com/v/t51.29350-15/448915927_3759504364367737_7351037500985462730_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams4-1.cdninstagram.com&_nc_cat=101&_nc_ohc=h-keKsRk3ZcQ7kNvgHiG1NF&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzUyMjQ3MDMxMg%3D%3D.3-ccb7-5&oh=00_AYCnX1UgE_AUTpvma1MYzpcPx0Hw4J_CSQrnsBcp11bOYg&oe=6727887A&_nc_sid=2d3a3f  
https://scontent-ams4-1.cdninstagram.com/v/t51.29350-15/448974338_1592190394659749_6476996291249314842_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams4-1.cdninstagram.com&_nc_cat=105&_nc_ohc=vMg2vzGTzfkQ7kNvgEFU79B&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTc2NzM3Ng%3D%3D.3-ccb7-5&oh=00_AYAcMooLMZcr3Pq46NSrv9o-laCMnut_OR-GIXCwwVNZDA&oe=67279834&_nc_sid=2d3a3f  
https://scontent-ams2-1.cdninstagram.com/v/t51.29350-15/449020545_1364981594182672_6982033195588405414_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams2-1.cdninstagram.com&_nc_cat=104&_nc_ohc=xVxGTiJ7SXEQ7kNvgG9alee&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTY0ODIyOQ%3D%3D.3-ccb7-5&oh=00_AYCfGOnqhxymHWzT9twWu4VipuBkxAsaMmJLlYj9OwdZag&oe=67279F21&_nc_sid=2d3a3f  
https://scontent-ams2-1.cdninstagram.com/v/t51.29350-15/449080005_454864213924638_2540173413885485248_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams2-1.cdninstagram.com&_nc_cat=106&_nc_ohc=DqxWoTv_x4oQ7kNvgFCB76R&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTU5MDE1OQ%3D%3D.3-ccb7-5&oh=00_AYBvHWd8g1Pczo3qr-Fz6vgJpg7NBJu9hVGoI4atvLsdng&oe=67276F76&_nc_sid=2d3a3f  
https://scontent-ams2-1.cdninstagram.com/v/t51.29350-15/448915469_768460891837775_6721435720937718114_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams2-1.cdninstagram.com&_nc_cat=111&_nc_ohc=2gNLyRSTnI0Q7kNvgGo4ud4&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTYwNTc3Mg%3D%3D.3-ccb7-5&oh=00_AYCJiJBx0yaQ5a1SRJS92w_VSIOyu1MfI7T7FFuLiVqXVg&oe=67277199&_nc_sid=2d3a3f  
https://scontent-ams4-1.cdninstagram.com/v/t51.29350-15/449084922_1821405185038495_8744258635626615810_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent-ams4-1.cdninstagram.com&_nc_cat=107&_nc_ohc=q0iRZkKpotQQ7kNvgE32cZb&_nc_gid=62fffa2472874754b3b206f514b91697&edm=ACpohRwBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTc4MjQ1NA%3D%3D.3-ccb7-5&oh=00_AYCFIuh2fWrL2MRm1PizgqqE4teJALg_OBdt9Nhu371RUQ&oe=67277063&_nc_sid=2d3a3f  
https://scontent.cdninstagram.com/v/t51.29350-15/448920933_491327600013891_8801919280400046273_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=103&_nc_ohc=gxPuFZ6nWXEQ7kNvgGwiZk7&_nc_gid=ac8dccf67271454195584ccc47006eb0&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM5NzM5Njg4NzQyMTgxNzMxMw%3D%3D.3-ccb7-5&oh=00_AYAhlp6dQNi2yt2cuMsb44-YjrACqi7WJ9mywCNUp_ymWg&oe=6727AC07&_nc_sid=10d13b  
```   


https://www.instagram.com/p/C8l86ZuyVbE/?img_index=3  
北条司展Part.2  実は、この北条司展…前期もありまして。私が行ったのは最終日1日前の滑り込みだったんだけどね。こう見ると、何とか前期も行けたら良かったと後悔する程だったよ。グッズも最終日前日もあってか、少なくなってたし。6枚目😭😭😭  このシーン堪らなく好きだー💕💕どの原画もカッコよくてステキで…自分の語彙力の無さにガッカリするんだけど、とにかくワクワクが止まらなかった😍  読んだことある人ならわかると思うけどさ？  獠と香がいい感じで終わるじゃん？  だから、この2人の絵を見るとにんまり🥰しちゃうんだよね。 CITYHUNTERばかり写真撮ってたけど他にも沢山展示されてた🙌🏻  私が北条司先生の作品に触れたのは、おそらくCAT’S EYEが初めじゃないかな？  テレビで見て、コミックをちょろちょろ買って…   で、CITYHUNTERを知って全巻揃えて何度も読んで、実家を出る時に置いてきちゃったら処分されてて😓  また読みたくなって大人買いして全巻揃えたなぁー。  アニメのキャラにここまでカッコイイ❣️大好き❣️ってなったのは、獠 が初めてじゃないかな。こんな人が実際居たらいーのに🥲と思っちゃうよね😅  
北条司展Part.2  事实上，北条司展…前期也有。 我去的是最后一天1天前的滑水。这么一看，真后悔上学期要是也能上就好了。周边也有最后一天的前一天吧，变少了。第六张😭😭😭  非常喜欢这个场景💕💕每一幅原画都很帅很好看…虽然对自己没有词汇能力感到失望，但还是很兴奋😍  读过的人应该都知道吧？獠和香不是很好吗？所以，看到这两个人的画就会心一笑🥰。只拍了CITYHUNTER的照片不过其他也展示了很多🙌🏻我第一次接触北条司老师的作品，大概是CAT’S EYE吧？看电视，随便买几本漫画…然后，知道了CITYHUNTER，集齐看了好几遍，离开家的时候放在那里，结果被处理掉了😓想再读一次，买了大人买了全套。在动漫角色里喜欢到这种地步❣️，獠应该是第一个吧？如果真的有这样的人🥲就好了😅  
```   
https://scontent.cdninstagram.com/v/t51.29350-15/449102935_993496752037726_7250011905259076459_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=110&_nc_ohc=Q0amiG7zFiwQ7kNvgEA6XrF&_nc_gid=4a64154189ff4fc4979990c97fd63e7c&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM5NzM4OTM3OTE0NzkyMzE5Ng%3D%3D.3-ccb7-5&oh=00_AYCzBETMuJpArvqboCTsmOFm5-quDtON2QSupKv-vYWkuA&oe=672780A5&_nc_sid=10d13b  
```   


https://www.instagram.com/p/C7scTPTvPBp/  
北条司展後期3種ランダム入場特典(@￣□￣@;)！！(1、2枚目)😌フード注文特典、ARフォトフレームと限定ステッカー(4枚目) カメラ内蔵のスマートフォン等で、限定カードのQRを読み込むと気軽にどこでもARで人気キャラクターと一緒に写真が撮れます!自分だけの写真を撮ってSNSで自慢しよう!やってみた( \*´艸)北条司展カフェ前半1回目は、ARフォトフレームに気がつかず、家に帰ってから気がついて、2回目は、うまく設定が、出来ず、北条司展後期、今回は店員さんにやり方聞いたらやっと出来たよ😌前半のないのが、残念💦だけど、出来て良かった♥️  
北条司展后期3种随机入场特典（@￣□￣@;）! !（第1、2张）😌食物订购特典、AR相框和限定贴纸（第4张）利用内置相机的智能手机等，读取限定卡的QR，就能轻松在任何地方用AR和人气角色一起拍照！拍下只属于自己的照片，在SNS上炫耀吧！我试过了(*´´)北条司展咖啡前半部分第1次没注意到AR相框，回家后才注意到，第2次设定不太好，北条司展后期，这次问了店员做法后终于做好了😌前半部分没有的不过，很遗憾💦，不过，能做到太好了♥️  
```   
https://scontent.cdninstagram.com/v/t51.29350-15/447464673_961623635655987_1790022386997377649_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=103&_nc_ohc=ydKveQsDib0Q7kNvgEfpgsx&_nc_gid=2ce403efc21c49a784f4c858c54e6dd6&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTIwMTg3NzU0OTc0NTc1NQ%3D%3D.3-ccb7-5&oh=00_AYCWnTt4nHPpKdkiI0CZP3WSa0Gslbv01o-LWr0eh6hRFw&oe=6727884D&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447599988_1185883082763294_6775023398282985122_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=106&_nc_ohc=tB-FHnp5EHIQ7kNvgFdm64K&_nc_gid=2ce403efc21c49a784f4c858c54e6dd6&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTIwMTg3NzY4NDE4NzgwMw%3D%3D.3-ccb7-5&oh=00_AYCU8Y1xmOFJBEMKDy7b504M8LsfE404H67HhYooGW4yng&oe=67277FCE&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447515166_2817350341762080_4400973400564973301_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=100&_nc_ohc=ORJHXYGHfXEQ7kNvgGJKtco&_nc_gid=2ce403efc21c49a784f4c858c54e6dd6&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTIwMTg3NzU0OTk0OTE5OQ%3D%3D.3-ccb7-5&oh=00_AYCqXEtF87_Y5YktrVy25jyxOME0mZVLCIdSU1wPjIcI7A&oe=672786D3&_nc_sid=10d13b  
```   


https://www.instagram.com/p/C76dgwRSbsB/  
【#北条司展@吉祥寺GalleryZENON】『キャッツ♡ アイ』『シティーハンター』で知られる #北条司 先生の原画展へ  
漫画家さんの原画というのは想像以上に描き込まれているとは知っていたけど元来画力がおありの先生のそれはここまですごいんだ❣️と驚愕の作品群だった  
※映像以外の作品掲載は許可済  
今回オープンした #GalleryZENON は、#コミックゼノン と何か関連があるのだろうか？ギャラリーの経営にも先生は携わっておられるのかな？高架下にアトリエやショップをつくるのが流行っているけど、こちらもおしゃんなスペースであった  
お世話になったお客さま、ありがとうございました🙇‍♀️
【#北条司展@吉祥寺GalleryZENON】因《猫♡爱》《城市猎人》而知名的北条司老师的原画展  
虽然我知道漫画家的原画画得比想象中还要精细，但老师原本就很有绘画能力，他的原画画得竟然如此厉害❣️真是令人惊愕的作品群  
※影像以外的作品刊载许可  
这次开放的#GalleryZENON和# comic zenon有什么关系吗？  
先生也参与画廊的经营吗？虽然现在流行在高架下建造工作室和商店，但这里的空间也很雅致  
承蒙关照的客人，谢谢🙇‍♀️  
```   
https://scontent.cdninstagram.com/v/t51.29350-15/447917045_841280884715741_6212598045269112751_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=105&_nc_ohc=FGeotA4Dp5QQ7kNvgF5hsKs&_nc_gid=f3f0c5bc6bdd4fb793fd0739acecbffa&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4NTE0Nzg1ODA3NjAzMjIyMw%3D%3D.3-ccb7-5&oh=00_AYB-0cYYDEJUg8qSjH-awMlB24ZUyMOhVpB30dbgjfaJmQ&oe=6727A44B&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447202956_1128669671523801_474780129820858108_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDA3eDEwMDcuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=109&_nc_ohc=T5LRMsiP3UAQ7kNvgG24i9a&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzE2OTA1NjYxMw%3D%3D.3-ccb7-5&oh=00_AYD538ImontPyBAHy9qaWAk8h3wC1Rtfd_SFmEXEynN1rw&oe=67278713&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447108693_1513897612856601_1090414155099548340_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=109&_nc_ohc=tT7kd6B8BCQQ7kNvgGVjuqu&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzI2MTM1NDM4Ng%3D%3D.3-ccb7-5&oh=00_AYBQIyJMn9ysrln0jEIhAvhgiIVzDJPeOOq5TMQm_i3_cA&oe=6727754B&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447253042_1450705312481090_6103582543695686744_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDUweDEwNDkuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=102&_nc_ohc=SAph72sJ-xMQ7kNvgElWp7P&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzE2ODkzMzk1MQ%3D%3D.3-ccb7-5&oh=00_AYBaxyw5GTGXAmemgLJACOyVipXwHRvvcpToFnHU7oeSVA&oe=67277B4E&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447197235_828063905376636_1750104749879296705_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi45ODZ4OTg2LnNkci5mMjkzNTAuZGVmYXVsdF9pbWFnZSJ9&_nc_ht=scontent.cdninstagram.com&_nc_cat=100&_nc_ohc=DbXF1B4PENYQ7kNvgHiRcNE&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzI2OTcwNzI4Mw%3D%3D.3-ccb7-5&oh=00_AYBpqxjyOJGY5MhiKohdA_jVfoD7X4MhqhR607aqkRoO9w&oe=6727A610&_nc_sid=10d13b  
```   


https://www.instagram.com/p/C7t8Bz7PoRy/  
北条司展後期 ☆その他展示物2F(@￣□￣@;)！！色々あったのですね、シティーハンター、キャッツアイ、エンジェル・ハート  くらいしか知らなかったので、見に行けて良かったです😌素敵な空間😌  
北条司展后期☆其他展品2f（@￣□￣@;）! !发生了很多事呢，城市猎人、猫眼、天使之心  只知道这些，能去看看真是太好了😌美好的空间😌  
```   
(Family Plot)  
https://scontent.cdninstagram.com/v/t51.29350-15/447361073_3779830275631099_2956132758657113377_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=107&_nc_ohc=z2kkyWsF5zYQ7kNvgFw7c1H&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzU1MTgzMQ%3D%3D.3-ccb7-5&oh=00_AYBkpHgCApx7noaIkcu1p95PUVtK1ig1JuhgVVl0n9lHqA&oe=67279B2F&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447104119_993123972434073_5505083241439909873_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=109&_nc_ohc=8IqezXtFJDMQ7kNvgFi9qsP&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzU3OTk2NQ%3D%3D.3-ccb7-5&oh=00_AYBvPdNzkx8aRUo6a3Zi1G_Nxjxi7mVQomQBrqUriWDIAw&oe=6727933D&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447148637_471248375484090_2955262543371521839_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=106&_nc_ohc=gyiGCszlPDAQ7kNvgEJJcr_&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzgxNjEyNjYzNA%3D%3D.3-ccb7-5&oh=00_AYC6WjF9C8_W5W9H8cW2CrQ4zuS630faMSjMbEIyBTbFaQ&oe=67278A5E&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447057571_974441087488357_3539414436810596178_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi44NDZ4ODQ2LnNkci5mMjkzNTAuZGVmYXVsdF9pbWFnZSJ9&_nc_ht=scontent.cdninstagram.com&_nc_cat=101&_nc_ohc=COq_52PbfWgQ7kNvgHLn_mD&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzYxMDY0Nw%3D%3D.3-ccb7-5&oh=00_AYAvOJgD0A4_zyLK_OmhNegx6QnqGIi9XV8l61Iz5iq0qw&oe=67277954&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447518600_350877797669263_493765614366573513_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=107&_nc_ohc=eMrPjvw6tzYQ7kNvgGEfUYH&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzQ4NzQyMg%3D%3D.3-ccb7-5&oh=00_AYBoSHLwcA6h5VhGBLk1ZLc38WdBqLoe_e5vLsl_bI0LhA&oe=67279FAB&_nc_sid=10d13b  
(FC)  
https://scontent.cdninstagram.com/v/t51.29350-15/447277372_420903664144732_3595368693293587572_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=104&_nc_ohc=gwx4TO5JV-IQ7kNvgEyuKTg&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzY1NjE3OA%3D%3D.3-ccb7-5&oh=00_AYA034-KKstH6t6I_53oxcuhyxvMV2zD-l_elN8vjp9f4g&oe=6727A04E&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447194197_476524268156325_6779590998158583109_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi44NzZ4ODc3LnNkci5mMjkzNTAuZGVmYXVsdF9pbWFnZSJ9&_nc_ht=scontent.cdninstagram.com&_nc_cat=109&_nc_ohc=Xm4AtpoBQwgQ7kNvgEeoKq7&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzgxNjI4NjU3Mg%3D%3D.3-ccb7-5&oh=00_AYCd8acvh62Pa92lvvdfuk-V0BGuqPlZ13yekKIjsnbbKg&oe=67278FC8&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447613054_791245112975632_7525118710730830260_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=104&_nc_ohc=0Nud4-QXdOcQ7kNvgHXqFgs&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzQ1MTg2OA%3D%3D.3-ccb7-5&oh=00_AYAD7F3py2bx9-17AD9SjFC8Ti1dTkCKH3NZ-17cDV5xsQ&oe=67279769&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447689498_964887941764408_4264677107388491346_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=104&_nc_ohc=brCXBupqYT0Q7kNvgEhOgOd&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzgwNzY3MTE1Ng%3D%3D.3-ccb7-5&oh=00_AYDee6420PevIT427zMmYaCAHpEm-WR8Wz8SqMpVx1DtvQ&oe=672796B9&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/447113427_1779870629168580_2289813680382420427_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=100&_nc_ohc=OS1KxcGAJsQQ7kNvgEnrIdS&_nc_gid=3b622755730c41ca809fa3e9ddcc86c1&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYyMjg5MzY3MzY5Mzk3OA%3D%3D.3-ccb7-5&oh=00_AYC2N18uM0L6ECs-U4qAII01N5VddIj6OcGsBSxCxHWwUA&oe=67278E3A&_nc_sid=10d13b  
```   


https://www.instagram.com/p/C7t3oE-vV1x/  
北条司展後期 ☆シティーハンター(@￣□￣@;)！！プロポーズ😌?冴羽さん香ちゃんにプロポーズ的なことしてたのね😌?アニメは再放送も見てるのに、このシーン見てなかったかも😌気になる～😌
北条司展后期☆城市猎人（@￣□￣@;）! !求婚😌？冴羽向香求婚了呢😌？动画片重播也看了，这个场景可能没看😌很好奇~😌
```   
https://scontent.cdninstagram.com/v/t51.29350-15/447069803_1367465257262160_2172102767345552813_n.heic?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMDgweDEwODAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=100&_nc_ohc=RcLs9fnWks0Q7kNvgHj7e_8&_nc_gid=47344319f97a44f09bf924ce2b9b662f&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzM4MTYwMzUzMzE2ODk3MzYxNw%3D%3D.3-ccb7-5&oh=00_AYDdoivShftMQmDouCge0BLEwO74PHqXO8tu0ZSFLwj3bQ&oe=672771CA&_nc_sid=10d13b  
```   


https://www.instagram.com/p/DBn47RpSr9r/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA==
カオスな感じが好き過ぎる京極街  そんでもって、キャッツアイに憧れていた  小学生の頃の自分を思い出した🐈‍⬛💋  
太喜欢混沌的感觉京极街  所以我很憧憬猫眼  想起了小学时的自己🐈‍⬛💋  
https://www.instagram.com/p/DBn47RpSr9r/?img_index=2    


https://x.com/shizuka8001/status/1809839438861926694  
で開催された #北条司展 🔫✨最後の最後に増えたサインを拝見しに行って来た時のもの。もうほんと心躍りました✨洞爺湖に行く前ギリギリだったので、行けるかどうかと思いながらなんとか😂頑張ってよかった😂w  ギャラリーは奇跡的に貸切状態でした👀✨
北条司展🔫✨最后去看增加的签名时的照片。真的很高兴✨快要去洞爷湖了，想着能不能去😂好好努力了😂w画廊奇迹般的包场了👀✨  
https://pbs.twimg.com/media/GR3XXshbYAA-92j?format=jpg&name=4096x4096  

https://x.com/oimotoika/status/1813692850749813241  
原画😍🩷suiさん！見て！髪の毛！涙！服の影！黒い部分の二度塗り、三度塗り！グラデーショーーーーーーーーーーーーーーン！場面とは真逆に盛り上がっちゃっているけど、😝すごいでしょぉぉぉぉぉぉぉ！！なんだ！？この繊細さは🖊💗  これぞ、北条マジック🧙‍♂️✨#北条司展 #シティーハンター  
原画😍🩷sui ！你看！头发！眼泪!衣服的影子！黑色部分涂两遍，三遍！新娘秀———！虽然和场面相反很热烈，😝很厉害吧啊啊啊啊！ ！什么嘛！?这种细腻🖊💗这就是北条魔法🧙‍♂️✨#北条司展#城市猎人  

https://x.com/nemuru_train/status/1824472124725268737  
北条司展で購入した88グラフが届きました。初日の初回入場オープン待ちで購入したものです🫶どこ飾ろうー！  
在北条司展购买的88Graph送到了。第一天的初次入场等待开放时购买的🫶哪里装饰—！  
https://pbs.twimg.com/media/GVHTp3Ea8AE0HXm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GVHTp3EaEAIxslV?format=jpg&name=medium  

https://x.com/oda08118004/status/1821726401642500242  
キャッツアイ の放送日に #北条司展 投稿180  #キャッツ・アイ 3、4枚目同じ場面はコピーを貼り付け、吹き出しもホワイトの下にうっすら下の絵が見えたり原画展ならではの楽しみです  パリ2024 連日盛り上がってます！アニメ1期でパリに旅立ち2期でパリから戻ったキャッツ！フランス実写版秋配信決定  
猫眼之放送日 #北条司展投稿180 #猫眼第3、4张同样的场面粘贴拷贝，对话框白色下面可以隐约看到下面的画，只有原画展才有的乐趣巴黎2024每天都很兴奋！动画第1季前往巴黎，第2季从巴黎回来的Cats！法国秋季真人版配信决定  
https://pbs.twimg.com/media/GUgSgSDXMAAtfu6?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GUgSgRfa8AEJsVx?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GUgSgRha8AAOQy_?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1824440449131745732  
キャッツアイ の放送日に #北条司展 投稿181  来年連載開始40周年、今なおアニメ劇場版や実写版が制作される人気作 #シティーハンター   #パリ2024 で「無課金おじさん」で話題になったユフス・ディケッチ選手のポケットに手を入れ射撃する姿と冴羽獠も同じ構えをすることでSNSで取り上げれましたね  
猫眼之放送日#北条司展投稿181 明年连载开始40周年，现在仍然制作动画剧场版和真人版的人气作品 #城市猎人 #巴黎2024 因「無課金おじさん」而成为话题的Yusuf Dikeç选手把手伸进口袋里射击的样子，冴羽獠也是因为摆出同样姿势而在SNS上被报道的。  

https://x.com/oda08118004/status/1829459523804836018  
キャッツアイ の放送日に #北条司展 投稿182  クールな獠ちゃん、ギャグテイストな獠ちゃん、2枚目の獠ちゃんの心情はいかに？美人の女性が多く登場する #シティーハンター にはセクシーな描写もでてくるものです。北条先生の絵は綺麗なので生で見る方が圧倒的にお勧めです  
《猫眼》播出日#北条司展投稿182 Cooling、搞笑风格的獠、第2张獠的心情是怎样的？美丽女性登场的#城市猎人中也会出现性感的描写。北条老师的画很漂亮，绝对推荐去现场看  

https://x.com/oda08118004/status/1832070476203753781  
キャッツアイ の放送日に #北条司展 投稿183  北条司展にデビュー作の展示。線が細いというか目元の描き方も少しあっさりとした、今とは絵の印象が違いますね  それにしても「アナクロ・ラプソディー」のタイトルで描いたのに「俺は男だ!」に勝手に変わっていたとは凄い話だ（1枚目でも確認できます   
猫眼放送日#北条司展投稿183  北条司展出道作品的展示。线条很细，眼睛的画法也很简单，和现在的画给人的印象不一样。即便如此，画的题目是「Anachronistic Rhapsody」，“我是男人！”的Title擅自改变，真是了不起的故事(第一张也能确认  

https://x.com/oda08118004/status/1834612008060436845  
キャッツアイ の放送日に #北条司展 投稿184  「桜の花咲くころ」の西九条 紗羅ちゃん、シティーハンターに同姓同名のキャラが登場しますよね  紗羅ちゃんのパパの見た目 #シティーハンター の海坊主と似てて名前も同じ隼人って面白い  今日のMステでJO1の皆様が「Get Wild」をカバー！格好良かった  
猫眼放送日#北条司展投稿184  《樱花盛开时》里的西九条纱罗，在城市猎人里也有同名同姓的角色登场吧  纱罗爸爸的长相 和#城市猎人的海怪很像名字也一样的隼人很有趣  今天的MStay JO1的大家翻唱“Get Wild”！很帅  
https://pbs.twimg.com/media/GXXZ5SMa0AAbvbp?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GXXZ5SObMAE9b4B?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GXXZ5SNaYAAzK9V?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1836961049129668855  
キャッツアイ の放送日に #北条司展 投稿185  前回載せた「桜の花咲くころ」の10年後を描いた「こもれ陽の下で…」です  キャッツ、#シティーハンター に次ぐ少年ジャンプ連載作品で、北条先生が好きなように描いた作品です  雑誌やコミックになると分からなくなる部分も楽しめるのが展示会の楽しみ  
猫眼动画放送日#北条司展投稿185  描写了上次刊登的《樱花盛开之时》10年后的《在阳光下…》。继《猫》、《城市猎人》之后的少年Jump连载作品，是北条老师按照自己喜欢的方式描绘的作品  在杂志和漫画中看不懂的部分也能乐在其中，这就是展示会的乐趣  
https://pbs.twimg.com/media/GX4yVbxa4AAosbd?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GX4yVb1aUAMR-za?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GX4yVb0aUAEFn7V?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GX4yVb0aUAQRiht?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1839590659352662288  
キャッツアイ の放送日に #北条司展 投稿186  今回も「こもれ陽の下で…」です（1話目の原稿）1、2枚目の場面の次のページは #北条司 先生の作品によく見られる？？ビンタシーンがあります  ビンタの理由、紗羅と木の精が似てる理由、木の精と兄妹の関係性など気になった方は読んでみてください   
猫眼动画放送日#北条司展投稿186  这次也是“阳光下…”（第一话原稿）  第1、2张的场面的下一页#在北条司老师的作品中经常能看到？ ？有一个掌掴的镜头。  掌掴的理由、纱罗和木精相似的理由、木精和兄妹的关系等感兴趣的人请读一读  
https://pbs.twimg.com/media/GYeJ86magAAbQ1C?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GYeJ86obUAApXBm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GYeJ86nasAANR8N?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GYeJ86naUAA3nf1?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1842026944025788426  
キャッツアイ の放送日に #北条司展 投稿187  今回も「こもれ陽の下で…」です（1枚目は1話目の、2-4枚目は最終話）  キャッツ、シティーハンターに次ぐ少年ジャンプでの連載作品。アクション要素があった前2作から一変して植物が題材の作品で、北条司先生の前2作とはまた違う世界観が楽しめます  
https://pbs.twimg.com/media/GZAxvYTbMAA8BfX?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GZAxvYRbAAQsbf9?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GZAxvYRbsAAvPAB?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GZAxvYUbAAAAtFs?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1844665375361028500  
キャッツアイ の放送日に #北条司展 投稿188  キャッツ・アイ #シティーハンター 、前回まで載せていた「こもれ陽の下で…」に次ぐ週刊少年ジャンプ連載作品で   北条先生にとって週刊少年ジャンプ最後の連載作品の「RASH!!」   わかりやすく第1話とプレートにありますが作品的にはKARTE1表記なんです  
猫眼の放送日 #北条司展 投稿188  猫眼 #CityHunter 是继《在阳光下...》之后的第二部周刊少年Jump 连载作品。   对于北条老师来说，周刊少年Jump最后的连载作品「RASH!!」     这很容易理解，而且在第1话和盘子上，但就作品而言，它是 KARTE1 符号  
https://pbs.twimg.com/media/GZmRYLmaUAAsxJj?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GZmRYLObkAAth0H?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GZmRYLPaMAA_y5T?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GZmRYLOaEAALt8B?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1847201211646378342  
キャッツアイ の放送日に #北条司展 投稿189  TVKの #キャッツ・アイ もいよいよ2が始まるわね！タイトルの #RASH の意味は「無鉄砲」、2枚目何てまさにそのような感じね  
北条司 先生の連載作品では一番短い全16話で、先生のコメントも正直というか本当はまだ色々言いたいことがありそうな感じ  
猫眼放送日 #北条司展投稿189  TVK的#猫眼2也终于开始了！Title中的#RASH的意思是“鲁莽”，第2张就是这样的感觉。  
这是北条司老师连载作品中最短的，全16话，老师的评论也很诚实，感觉他其实还有很多话想说。  
https://pbs.twimg.com/media/GaKTtZubYAEZBnW?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaKTtZsaQAAfXFK?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaKTtZsaMAAmbc8?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaKTtZtbcAA3YAI?format=jpg&name=4096x4096  


https://x.com/GalleryZenon/status/1805553218375893189  
【御礼】  
【谢礼】  

「北条司展」にたくさんのご来場をいただきありがとうございました。  
先ほど、皆さんが書いてくださったメッセージブックがスタッフから届きました。  
どれも想いの詰まった素敵な内容で  
これからひとつひとつ大切に読ませていただきます。  
非常感谢「北条司展」有这么多的参观者。  
刚才工作人员给我寄来了大家写的留言册。  
每一个都充满了美好的想法  
今后我会一一仔细阅读。  

またSNS等で応援してくださった全ての方に感謝いたします。  
同时感谢在SNS等支持我的所有人。  

本当にありがとうございました。  
真的非常感谢。 

北条司   

https://pbs.twimg.com/media/GQ6bCLYbEAANXqX?format=jpg&name=900x900  
https://pbs.twimg.com/media/GQ6bfMzaQAA20cO?format=jpg&name=4096x4096  

https://x.com/xyz_mizu/status/1806860830845411713  
Ｘのキャンペーン企画で当選した北条司先生の直筆サイン入り図録が届きました。表紙をめくると大きく書かれたサインがそこに…‼️ヤバいです。家宝です。もうめちゃくちゃ嬉しい😭どうもありがとうございます😭  我收到了一本有北条司签名的目录，在 X 活动项目中获奖。翻开封面，大大的签名就在那里...... ‼️糟了。 这是传家宝。我已经太高兴了😭。非常感谢 😭。
https://pbs.twimg.com/media/GRNCVmsacAAxE-K?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GRNCWYCbsAApur6?format=jpg&name=4096x4096  

https://x.com/sui1198838/status/1806331990201037227  
実は意外と気に入ってるのが「美樹がカウンターを飛び越える」ところw  美樹の人となりが表れててさ  他人の気持ちを汲めて優しい  香を気にかけて大切に思っている  めちゃくちゃ運動神経いい!かっこいい!とか  見た“だけ”で伝わってさ  説明臭くなくて好みなんだよね  
实际上，我出乎意料地喜欢美树跳过柜台的那部分。这能表现出美树的个性。她很善良，能理解别人的感受。她关心阿香，照顾她。她很爱运动！ 她很酷！她很酷  一看就知道。我喜欢这个故事，因为它没有废话。
https://pbs.twimg.com/media/GRFhWH-aIAASEPy?format=jpg&name=4096x4096  

https://x.com/SoyoWing/status/1806345288438997480  
もう終了してしまいましたが、先日行ってきました。最高‼️  
我已经完成了、我前几天去过那里。最佳 ‼️  
https://pbs.twimg.com/media/GRFtdbfbIAAdFKX?format=jpg&name=4096x4096  

https://x.com/earlgreytealeaf/status/1805936083252748510  
北条先生のメッセージ、、🥲本当に感謝しかないです🙏✨改めて、ありがとうございました！！また先生の作品に触れられる機会を  楽しみに待っています🍀  カメラのレンズが本当にこちらを写して  そうで、色彩の引き出しが天晴れっ👏🏻  
https://pbs.twimg.com/media/GQ_5SX8boAAMA_S?format=jpg&name=4096x4096  

https://x.com/earlgreytealeaf/status/1805939373122486524   
🐈背＋🦀股＝No. 1スイーパー😎　　〜しおりちゃんを添えて〜  
🐈背+🦀大腿 = No. 1 Sweeper😎　　〜与 😎香〜  
https://pbs.twimg.com/media/GQ_8SJ0bkAAOAic?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQ_8SJya4AASkH4?format=jpg&name=4096x4096  

https://x.com/tanusan1103/status/1797624122719248512  
原画展　名シーン多数展示。制作中の構想を垣間見れたり、絵が美しく、顔の表情から滲み出す感情を想像すると胸が熱くなる😭😭AH苦手だけど、最後の絵だけは、吸い込まれました。獠の涙、後ろで寄り添っている香の切ないでも神々しい表情…涙涙😭  
原画展 展出了许多著名场景。你可以看到制作过程中的构思，这些图画非常漂亮，想象着他们脸上的表情😭，我的心就会怦怦直跳。我不爱AH，但只有最后一张照片吸引了我。獠的泪水，香依偎在他身后的悲伤而又神圣的表情......泪目😭  
https://pbs.twimg.com/media/GPJxnDVbMAAtXMs?format=jpg&name=4096x4096  

https://x.com/KOHARU36991352/status/1784490006196949017  
北条司展行ってきました✨  もっこりオムライスめちゃ美味しかった😋 小分けで使ったお皿売ってたら買った…。X情報でA3の硬質ケース必需ッ！って書いてたので持参したけどマジで持ってきてよかった😭  
我去看了北条司展 ✨蛋包饭太好吃了 😋如果有卖的话，我一定会买我用过的小份量的盘子...... X 信息说你需要一个 A3 硬盒！ 我真的很高兴我带了它😭  
https://pbs.twimg.com/media/GMPIK8pa4AAhkvr?format=jpg&name=4096x4096  

https://x.com/atsu_RSactor06/status/1794370908460839094  
北条司先生原画展✨後期😊北条先生のサインと獠ちゃんと香さんのツーショット原画✨そしてサイン入り3ショット✨  
北条司先生原画展✨后期😊，北条先生的签名和獠、香的两张原创照片✨，以及签名的三张照片✨   
https://pbs.twimg.com/media/GObi0SBbkAAuLvM?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GObi0SMa8AADt15?format=jpg&name=4096x4096  

https://x.com/annzuironosora/status/1805030387900391477  
非オタ友達と北条司展後期を見てきたんですが「靴の裏、すご！」「ネトフリ良かったよー」と大興奮😊。「もっこりの英訳知りたい」とGoogleレンズで翻訳し出したのが1番面白かったです  
我和我的非极客朋友们去看了北条司展後期，我们都非常兴奋：'鞋底，太神奇了！ 我太激动了！ 我想知道 "Mokkori "的英文翻译"  
https://pbs.twimg.com/media/GQzBkQtbMAAicmX?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQzBkQsaMAAvPF-?format=jpg&name=4096x4096  

https://x.com/yukita_/status/1805090488493752750  
有識者に聞きたいんだけど、久しぶりにtmaf用に引っ張り出してきたこれ、このイラストってなんらかのイラスト集なりなんなりに収録されてます？？？？？？？私見たことないなと思って。  
我想请教一下专家，但我已经很久没有把这幅画拿出来给 tmaf 看了，这幅插图是否被收录在某本插图集或其他什么书中：？？？？？？？。 我只是觉得我从没见过。  
https://pbs.twimg.com/media/GQz4Oh7bsAAL2Ex?format=jpg&name=4096x4096  

https://x.com/AngelTouchPlus/status/1805188342411399288  
生まれて初めて瞳を描いたよ😅#ギャラリーゼノン #北条司 展でクリスマスイベント会場の下見を兼ねて観て来ました。丁寧な仕事に感動🥰  
生平第一次描绘了😅瞳  #galleryzennon #在北条司展上兼作圣诞活动会场的事先观察来了。郑重的工作感动🥰  
https://pbs.twimg.com/media/GQ1ROkzaAAACmb2?format=jpg&name=medium  

https://x.com/senkouchoco0802/status/1805560084782891117  
こちらこそ、前編と後編で   北条先生の貴重な生原画観れて  最高でした。また、開催して下さいッ‼️絶対に足を運びますッ‼️僭越ながら、応援メッセージも  描かせて頂きました。  
很高兴能在展览的第一部分和第二部分看到北条老师珍贵的原画。 请再次举办展览。‼️ 我一定会去看展览的。我还画了一条支持留言。 我也画了一幅表示支持的画。  
https://pbs.twimg.com/media/GQ6jU66bwAE8dGP?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQ6jVAnbwAUQV6m?format=jpg&name=4096x4096

https://x.com/Inysh_Kgm1016/status/1805601997497975247  
一度しか行けなかったけど、生原稿には感動しました💖北条先生のイラスト制作の映像、色の塗り方を食い入るように見てました　昔私も同じインクを使って、イラストを描いてましたが、やはりプロとは塗り方が違う💦　ラインの描き方も、何もかも  感動してしまいました❗原画展ありがとうございました  
我只去了一次，但现场草稿给我留下了深刻印象💖，我看了北条老师的插图制作视频和他涂色的方式。我看了北条老师的插图制作视频和他的上色方式。 我以前也用同样的墨水画插图，但他的上色方式💦 线条和一切都与专业人士不同。我印象非常深刻 ❗非常感谢你的原创展览    

https://x.com/B66Ah3R7DxOnygL/status/1805570477001523649  
北条先生、貴重な原画の数々を見せていただき、ありがとうございました。何回も原画展に足を運んだのは、北条司展が初めてでした。メッセージも、初めて書きました！週末の仕事帰りに、先生の原画を見て幸せな気持ちで帰路に着くのが最高でした。また、是非、開催をお願いいたします。  
感谢北条老师为我们展示了这么多珍贵的原画。北条司展是我第一次多次参观原画展。 这也是我第一次写留言！周末下班回家的路上看到您的原画，带着愉快的心情回家，真是太好了。请再次举办展览。  
https://pbs.twimg.com/media/GQ6sxwNbwAMndTC?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQ6sxwSbwAAV0eD?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQ6sxwNboAAoMlA?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQ6sxwSaQAIbrYg?format=jpg&name=4096x4096  


https://x.com/imu5imu4/status/1805379553319338147  
自分で想像してた以上に #北条司展 ロス……ファンの皆様の思い出ポスト拝見してはロス……今まで何度か原画拝見する機会があって、その度にロスってたけど今回ももれなくロス……（ロス言い過ぎ）本当に開催ありがとうございました😭✨入場特典も毎回どきどきわくわくだったな✨  
比自己想象的还要厉害#北条司展 ロス……粉丝们的回忆邮筒，拜读了，ロス……至今为止有好几次拜见原画的机会，每次都损失了，这次也毫无遗漏地损失了……(ロス言过其实)真的谢谢你举办了😭✨门票优惠也每次都忐忑不安的✨  
https://pbs.twimg.com/media/GQ3_HknbwAQBLit?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQ3_HuZaQAAoROU?format=jpg&name=4096x4096  

https://x.com/Mimosa_pon/status/1803407934967464221  
好きすぎる  
我太爱你了  
https://pbs.twimg.com/media/GQb99J8a8AAnOQA?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQb99J-a4AAfLLI?format=jpg&name=4096x4096 

https://x.com/ArechiManga/status/1804454669952987169  
 ¡Curiosidades de fin de semana! ¿Sabíais que #CityHunter empezó su serialización en el volumen 13 de la Shonen Jump de 1985? Además, esa misma semana se celebraban los 850 números publicados de la revista.  
 您知道《城市猎人》于 1985 年在《SHONEN JUMP》第 13 卷开始连载吗？此外，同一周也是该杂志的第 850 期。  
https://pbs.twimg.com/media/GQq19IvWkAAsdoP?format=png&name=small  

https://x.com/Mimosa_pon/status/1804868407659299206  
こんなに好きになるなんて、こんなに楽しいことに巡りあえるなんて、想像していませんでした。ありがとうございました！来年は40周年、そして映画の続きも、Netflixの第2弾も、きっときっと！期待しております✨  
我从未想过自己会如此爱上它，也从未想过自己会发现它如此有趣。 非常感谢你们，我很高兴能参与其中！ 明年将是 40 周年纪念，我相信会有更多的电影和第二部 Netflix 系列！ 我们非常期待 ✨      
https://pbs.twimg.com/media/GQwuOfDbIAAnIDB?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQwuOfCa4AATEqh?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQwuOfHakAALrqC?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQwuOe-aMAA1uxX?format=jpg&name=4096x4096  

https://x.com/MuUrsae/status/1804902359270645958  
Pardonnez la piètre qualité de mes photos, elles ne rendent pas justice à ce trésor. Je confirme avec une joie immense que cette impression signée par maître Tsukasa Hojo en personne est magnifique, exceptionnelle ! Merci infiniment @TheEdition88 🙏💕  
请原谅我的照片质量太差--它们没有为这件珍品做出应有的贡献。我很高兴地确认，这幅由北条司亲笔签名的印刷品非常精美绝伦！非常感谢 @TheEdition88  🙏💕  
https://pbs.twimg.com/media/GQxLBh6WgAAIfbq?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQxLDsAWQAAkLac?format=jpg&name=4096x4096  

https://x.com/tenchotenchoooo/status/1804665007487308059  
おはようございます。今日も頑張りましょう。  
早上好。祝你今天好运。  
https://pbs.twimg.com/media/GQt1OdjboAEADYB?format=jpg&name=4096x4096  

https://x.com/oceanblop/status/1804705473154969818  
https://pbs.twimg.com/media/GQuaD0eaAAAYok0?format=jpg&name=4096x4096  

https://x.com/kurukurukuruto1/status/1798139780863312330  
当たりますように🎯  
祝你中奖🎯  
https://pbs.twimg.com/media/GPRGmYsawAAzRqe?format=jpg&name=medium  

https://x.com/ViVA2020renewal/status/1799046108955746424  
→今回オープンした #GalleryZENON は、#コミックゼノン と何か関連があるのだろうか？ギャラリーの経営にも先生は携わっておられるのかな？高架下にアトリエやショップをつくるのが流行っているけど、こちらもおしゃんなスペースであった  お世話になったお客さま、ありがとうございました🙇‍♀️  
→ 新开业的 #GalleryZENON 与 #ComicZenon 有什么关系吗？ 这位老师是否也参与了画廊的管理？ 在高架铁轨下建工作室和商店很受欢迎，但这里也是一个可爱的空间。 感谢所有帮助过我们的顾客🙇‍♀️   
https://pbs.twimg.com/media/GPd-5jkaYAAJvlH?format=jpg&name=4096x4096  

https://x.com/mansanmansan/status/1804769995555020864  
最終日だったけど、#北条司展 に行ってきました！#シティーハンター  が人気だったけど、ﾜｲはやはり #キャッツアイ w原画、もうちょい見たかったけど、幸せな時間でした。有難うございます！北条司先生！  
虽然是最后一天，但我还是去看了#北条司展！ #城市猎人#很受欢迎，但我还是想多看看#猫眼#，但这是一段快乐的时光。 非常感谢！ 北条司先生！   
https://pbs.twimg.com/media/GQvUvYbbgAAS9EE?format=jpg&name=4096x4096  

https://x.com/mansanmansan/status/1804770745303601307  
ま、ＸＹＺは、しっかり書いて帰ったけどw  
好了，XYZ 写得很好，可以回家了w  
https://pbs.twimg.com/media/GQvVbCrbwAAEdPq?format=jpg&name=4096x4096  

https://x.com/manaty2525happy/status/1804794809019703590  
最終日にやっと行けた‼️細部まで細かく描かれている原画に見惚れたー‼️最香‼️‼️また開催してください😆  
我终于在展览的最后一天去了‼️，并爱上了那些细致入微的原画‼️最香‼️‼️ 请再次举办😆  
https://pbs.twimg.com/media/GQvrS07aoAAjAzw?format=jpg&name=4096x4096  

https://x.com/0reiji0/status/1804786135849177311  
前期3回、後期4回行きました！楽しかったです😊是非また開催してくださいm(_ _)m  
前期我去了3次，後期去了4次！ 非常有趣😊请再举办一次！m(_ _)m  
https://pbs.twimg.com/media/GQvjag1bAAAmWGd?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQvjahIboAAiS5a?format=jpg&name=4096x4096  

https://x.com/3939sakutyan/status/1804796551719448719  
最終日😢めっちゃこんでるのに奇跡的にエンジェル・ハートが誰もいなくなったー！笑  
最后一天，明明很拥挤，Angel Heart却奇迹般地没有人了！笑  
https://pbs.twimg.com/media/GQvs5FzbsAAzBo5?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQvs5HGbIAAM3yJ?format=jpg&name=4096x4096  

https://x.com/tendonma2020/status/1804836352548368446  
今日で北条司展は終わりました。タグに便乗しただけですが、たくさんのリツイートと👍をありがとうございました🙇 北条先生には益々のご活躍を願っております✨    
北条司展今天结束了。 我只是利用了这个标签，但很多人转发并👍非常感谢🙇我们祝愿北条老师继续取得成功✨    
https://pbs.twimg.com/media/GQmlDMJakAM-WGF?format=jpg&name=4096x4096  

https://x.com/kikicks336/status/1804840366874468417  
ゼノン最終日行って参りました🥲前期も後期も娘と来られて嬉しかった…駅に向かいながらもうこれでお終いって思うと泣けました🥲2ヶ月間ホントによく通った…外国人だけの特典を貰いに夫、わざわざ来てくれたのに私のミスにより取得できず💔ホントに濃い2カ月ありがとうございました  
我去了Zenon的最后一天🥲我很高兴能和女儿在这里度过前期和後期......走到车站的时候我哭了，我想这就是结束了🥲这两个月我真的很顺利......我老公千里迢迢来拿外国人专享福利，却因为我的失误没能拿到。 💔谢谢你，这两个月真的很紧张
https://pbs.twimg.com/media/GQwUvXva4AAHCq8?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQwUvXybYAASnKR?format=jpg&name=4096x4096  

https://x.com/zA69gM3gleFD1s8/status/1804845911190683879  
おそばせながら、#北条司展、本当にありがとうございました😌地方民のため数回しか足を運べないの惜しい限りでしたが、前期後期しっかり楽しませていただきました😌美麗な北条先生肉筆の作品群にコラボカフェに遊び心溢れる展示品、いつまでも居たい極上空間でした😌XYZ 40周年展もよろしく頼む!  
非常感谢举办 #北条司展 😌 我来自农村，只能参观几次，但我非常喜欢前期後期 😌 北条先生的美丽画作、合作咖啡馆和有趣的展品让这里成为永远的绝佳去处 😌XYZ 40 周年纪念展做出同样的贡献！  
https://pbs.twimg.com/media/GQwZyXqbkAALeex?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQwZyaBbwAAHokk?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQwZyiVb0AARw0P?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQwZyoeaoAEBwNK?format=jpg&name=4096x4096  



https://x.com/chiroru_special/status/1804845085663908270  
GALLERY ZENON北条司展最終日！！念願の獠ちゃんと写真撮りました🥰この等身大獠ちゃん、おうちに迎えたいなぁ❤️ちなみに店頭のコルトパイソン357マグナム、完売でしたー🙌💕来年は40周年記念でシティーハンターOnlyの展示会かしら😆  
GALLERY ZENON 北条司展最后一天！ 我和心仪已久的獠合影留念 🥰我很想把这个真人大小的獠放在家里❤️对了，店里的Colt Python357Magnum手枪卖完了🙌💕也许明年我们会在 40 周年纪念时举办城市猎人限定展😆  


https://x.com/imu5imu4/status/1804844181686558999  
今週末も各地で #シティーハンター や北条司先生由来のイベント盛り沢山で幸せだったな🫶✨何度も言っちゃうけど、こんなに #シティーハンター や北条先生に思いを馳せることの出来る令和、生きててよかったぁ😭✨ADの円盤も楽しみ過ぎて過呼吸になっちゃう✨  
这个周末，我很高兴能在不同的地方看到这么多#城市猎人#和北条司先生衍生的活动🫶✨我知道我一直在说这句话，但我很高兴我在 2025 年还活着，因为我还能这么想 #城市猎人和北条老师 😭✨我太期待 AD 碟片了，我都快喘不过气来了✨  
https://pbs.twimg.com/media/GQwYN-PaEAAnamW?format=jpg&name=4096x4096  

https://x.com/nobotakasan/status/1804858907493351793  
最終日の伝言板XYZ〜🤗✨最終日を共に過ごせましたキセキ✨  
最后一天的留言板xyz~🤗✨最后一天一起甜蜜生活的奇迹✨
https://pbs.twimg.com/media/GQwlnD2a8AAhlNV?format=jpg&name=4096x4096  

https://x.com/pinktanpopo34/status/1804865175809663043  
北条司展 最終日行って来ました！美しい原画を心ゆくまで見て溜息ついてハンマー写真撮ってもっこりカレー食べて最高に楽しかった！写真撮って下さった方とお喋りできたのも嬉しかった！ファン歴浅くて必死で追いかける中この原画展に会えて本当によかった！ありがとうございました #ギャラリーゼノン  
我参加了最后一天的 北条司展！ 我尽情地欣赏着美丽的原画，一边感叹，一边拍锤子照，还吃了Mokkori咖喱，真是开心极了！ 我还很高兴能和给我拍照的人聊上几句！ 我成为粉丝的时间不长，一直在努力追赶，但我很高兴能看到这场原创艺术展！ 谢谢#GalleryZenon  
https://pbs.twimg.com/media/GQwrTvBaMAAv-ax?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQwrTvBaYAAOQJA?format=jpg&name=4096x4096  

https://x.com/earlgreytealeaf/status/1804865891932479646  
皆さま約2ヶ月間  本当にお疲れ様でした🍻そして幸せな時間を  ありがとうございました！！！またCH40周年でお会い出来る事を  祈っております🙏✨  
感谢大家近两个月的辛勤工作🍻和快乐时光！ 我们希望在 CH40 周年庆典上再次见到你们。 祝你们一切顺利🙏✨   
https://pbs.twimg.com/media/GQwr9LxbIAAkVVk?format=jpg&name=4096x4096  

https://x.com/lieco_Aki/status/1804363908075130981  
着いた～😆え、凄い並んでる😳ビックリ！！  
到达了~😆呃,厉害并排😳吓了一跳!!  
https://pbs.twimg.com/media/GQpjZ2lbUAA7XOh?format=jpg&name=4096x4096  

https://newscast.jp/news/5896927  
『劇場版シティーハンター 天使の涙(エンジェルダスト)』凱旋トークショーに神谷明・伊倉一恵が登場！Blu-ray&DVD発売記念特別上映会の詳細も解禁。  
《剧场版城市猎人 天使之泪》凯旋脱口秀神谷明·伊仓一惠登场!蓝光&DVD发售纪念特别上映会的详细内容也解禁。  

https://x.com/lieco_Aki/status/1804380580337107295  
来て良かった！！😆😆デジタルなこの時代に今もペンと筆を使って作品を生み出してる北条司先生に脱帽✨✨デジタルだと簡単に修正が出来る。紙に描くという緊張感が好きなんです  ってカッコいいじゃないですか！！😍プロって凄い💞  
我很高兴我来了！ 😆😆向北条司先生致敬，在这个数码时代，他仍然用钢笔和毛笔进行创作✨✨数码时代很容易进行修改。 我喜欢在纸上绘画的紧张感，很酷吧！！ 😍专业人士很了不起💞  
https://pbs.twimg.com/media/GQpykamakAIXtMI?format=jpg&name=4096x4096  

https://x.com/BrueSreasta/status/1804441915850703348  
IM CRYING 🥹🥹😭😭It’s @/hojo_official’s “The road to『CITY HUNTER』 40th anniversary 2025 ~Limited Special exhibition~” by @/GalleryZenon 🥹💙  
One of my favorite anime, and it’s part of my childhood 💙
https://pbs.twimg.com/media/GQqqWLEbAAAIhDY?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQqqWLSasAAgZ40?format=jpg&name=4096x4096  

https://x.com/kuminmin1983/status/1804004380917797245  
凄い凄い凄い凄い凄い！！！本当にありがとうございます。  
太棒了，太棒了，太棒了！！！太棒了！ 非常感谢。  
https://pbs.twimg.com/media/GQkca4QaEAA099J?format=jpg&name=4096x4096  

https://x.com/SfmaTacky/status/1804501252921028626  
北条司先生の展示会行ってきました(｀･ω･´)XYZ ありがとう  
我去看了北条司老师的展览(｀･ω･´)XYZ 谢谢  
https://pbs.twimg.com/media/GQrgTvlbYAAdkUJ?format=jpg&name=4096x4096  

https://x.com/PwdTyg/status/1804431234468319643  
北条司展　行ってきました〜きらり山さんの美麗イラストあったのでつい撮ってしまった😅シティハンターといえばのやつが貼ってあったりして✨ペールトーンシリーズのアクスタやっぱカッコいいな〜🩷良い時間でした〜  
https://pbs.twimg.com/media/GQqgo4lbMAAL_00?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQqgo4kbYAE-SUH?format=jpg&name=4096x4096  

https://x.com/momonja_oyasumi/status/1804527698607571430  
最終日前日になんとか行けたー！幸せな空間で心が満たされた！！また漫画読み返そう❤️‍🔥  
我在最后一天的前一天赶到了那里！快乐的空间充满了我的心！！让我们再看一遍漫画： ❤️‍🔥  
https://pbs.twimg.com/media/GQr4XpmaQAAKgzl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQr4XppbgAA4-SW?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQr4XpoawAAgTtl?format=jpg&name=4096x4096  

https://x.com/taro200288/status/1804473986606301640  
ギャラリーゼノンさんで開催中の北条司先生の原画展、明日で会期終了なのでもう一度目に焼き付けてきました🥹何度見てもホント美しい✨スタッフさんも良い方達ばかりでホント心地良い場所です❤️素敵な原画展の開催、ありがとうございました🥰  
Zenon Gallery的北条司原画展明天就要结束了，所以我已经有机会去看了 🥹即使看了很多遍，它仍然非常漂亮 ✨工作人员都是很好的人，这里真的很舒适。❤️，感谢你们举办这次精彩的原创艺术展🥰  
https://pbs.twimg.com/media/GQrHhrUaoAAxCAo?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQrHhyDbYAA6Jtw?format=jpg&name=4096x4096  

https://x.com/MonPanier12/status/1804529469904044188  
今日はちゃんと👓持って行ったせいか、自分の中で香ちゃん祭りでした！気がつけば写真、香ちゃんのアップばっかり。目の表情(？)が〜💕  
也许是因为我今天带上了我的 👓，但在我心中，这就是香的节日！ 我发现自己只拍了 香的特写。 她的眼神～💕  
https://pbs.twimg.com/media/GQr5_IlbwAArWnQ?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1804007407871201487  
キャッツアイ の放送日に #北条司展 投稿174  今回はアニメ絶賛再放送中「キャッツ・アイ」です！ 2年前の「キャッツ♥アイ40周年記念原画展」以来の原画。北条先生の絵やはり綺麗、#キャッツ・アイ も狙いたくなるお宝です  この綺麗な絵を見れるのも日曜が最後、急げ！    
在#猫眼#播出当天，#北条つかさ#展览将在174号贴出    这次是广受好评的重播动画《猫眼》！这是两年前 "猫眼 40 周年原画展 "之后，我第一次看到他的原画。 北条老师的画依然很美，是让人也想去看看 #猫眼#的宝藏！ 周日是欣赏这幅美丽画作的最后一天，请抓紧时间！   
https://pbs.twimg.com/media/GQkfK91akAUlAea?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQkfK94bwAAeMvZ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQkfK91akAc9T25?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQkfK92akAAQsp5?format=jpg&name=4096x4096  

https://x.com/tw_tomori/status/1804148445521023411  
展示会はしご③#北条司 さんの描く光はとても眩しかった  #シティハンター や #キャッツアイ は  自分の知らないところで本当にそういう世界線が存在するのではないか、と思ってしまうようなリアリティを感じるから惹かれるのかな    
展览梯子③#北条司先生画的光非常耀眼#城市猎人和#猫眼  在自己不知道的地方，是不是真的存在这样的世界线呢?正是因为有这样的真实感才会被吸引吧  

https://x.com/hanikam_i/status/1803673571908342190  
日ぼっちで行ってきました！最高空間すぎた！！！  
我坐着日光浴床去那里玩了一天！空间太大了！  
https://pbs.twimg.com/media/GQfvjbBaIAcVpls?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQfvjbBaIAABx42?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQfvja_aIAEpu5t?format=jpg&name=4096x4096  

https://x.com/meimei12612149/status/1803757291977765104  
ケント紙の名前？と思って写メしたらやはりそうでしたー  「バロンケント」って言うんだね。黄色味がかったもので発色も良く線も綺麗に出るのかー（ググった）今日はマニアックな所ばかり見てます笑  
这是Kent纸的名字吗？ 我拍了一张照片，还是那个名字--叫做"Kent Baron"。 这是一种淡黄色的纸张，颜色很好，线条简洁（我在谷歌上搜索了一下）我今天看到的都是疯子，笑死我了。   
https://pbs.twimg.com/media/GQg7serbYAASS2J?format=jpg&name=4096x4096  

https://x.com/yLA4V1Khmm10795/status/1803553616097419386  
シティーハンター 見てください🎵ギャラリーゼノンで夢のレイアウト（酒好き😍）CH好きな人と繋がりたくフォローさせて頂きました💖宜しくです🤗  
https://pbs.twimg.com/media/GQeCdI1aIAE5d5c?format=jpg&name=large  

https://x.com/taketora0131/status/1802716457345970342  
後期にも行ってきた〜♪新しい原画も見られて楽しい。それにしても北条先生の絵って、情報量が多いのに輪郭付近の描き方とかデフォルメ混ぜたりとか凄い工夫があるので、画集では解りにくい作業痕が見られるのも原画展の魅力。23日まで！  
后期也去了~♪新的原画也能看开心。话虽如此，北条老师的画信息量很大，但轮廓附近的画法和变形混合等都下了很大的功夫，能在画集上看到难以理解的痕迹，这也是原画展的魅力所在。截止到23日!  

https://x.com/AoiTakarabako/status/1803525299310830055  
おパヨ～ございます👧  
https://pbs.twimg.com/media/GQdosvtaIAMttrL?format=jpg&name=4096x4096  

https://x.com/moon_8_star/status/1802936285147136294  
2回目行ってきました！！本当綺麗…やはりアナログが好きだ。RKまじで推せる。お互いを信頼しきってるから、命も人生も委ねられるんだろうな。愛だなあ、尊い。  
我已经是第二次去那里了！ 它真的很美......我还是喜欢模拟......我可以郑重推荐 RK。 我想我们是如此信任对方 以至于我们可以把自己的生活和生命交给对方 这就是爱，这就是珍贵。   
https://pbs.twimg.com/media/GQVQ_sLaIAAM9Gb?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQVQ_tWaIAAbij3?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQVQ_vsaoAAQGly?format=jpg&name=4096x4096  

https://x.com/GalleryZenon/status/1803265450790494384  
https://pbs.twimg.com/media/GQZ8XnOaIAQdy-X?format=jpg&name=4096x4096  

https://x.com/ugiskm/status/1803386642050457816  
北条先生の生原稿、見る角度によって髪の毛を描いたペンの筆跡、ベタの塗り跡が分かってとても興味深かった。見ていて飽きない。こんな貴重な物を間近に見られて、ただ素直に嬉しい✨ #北条司展  
从不同角度欣赏北条先生的原始手稿、头发上的笔触和坚实的绘画作品非常有趣。 我百看不厌。 能近距离观赏如此珍贵的物品，我真的很开心 ✨ #北条司展  
https://x.com/ugiskm/status/1803386117649211603  
香ちゃん……綺麗✨ずーっと憧れの女性🥰  
香..... 美丽✨我一直很欣赏的女性🥰  
https://pbs.twimg.com/media/GQbqAuYbMAAjFxJ?format=jpg&name=4096x4096  

https://x.com/tendonma2020/status/1803425780883661261  
幸せ空間  
快乐空间  
https://pbs.twimg.com/media/GQcOLYxaIAMxkAP?format=jpg&name=4096x4096  

https://x.com/tororodiru/status/1802591656262107395  
北条先生はカラー原画が本当に綺麗で、ギャラリーでは描いている動画が流れているのですが、汚れないようにの気遣いがすごいんです。重ね塗りが端麗すぎて涙出そう…  
北条老师的彩色原画真的很美，展厅里还有他画画的视频，但他非常小心，生怕弄脏了。 叠画非常工整，几乎让我热泪盈眶......  

https://x.com/tororodiru/status/1802624027909120216  
見てこの33年経ってからの印刷の進化を！  
看看33年后印刷业的发展！  
https://pbs.twimg.com/media/GQQ0_3tbYAAoWCA?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQQ0_3wakAALMjc?format=jpg&name=4096x4096  

https://x.com/riyoko_kisaki/status/1802627878187909274  
条司展 後期へ。エンジェル・ハート始め『家族』がテーマになってる優しさと温かさのあるエピソードや作品が多めな感じでした🌸北条先生のカラーイラストの繊細さ美しさ本当に惚れ惚れ✨フランス版C.Hサインや鈴木亮平さんのスケッチコメントも。獠のもっこりオムカレーもいただきました🍽️  
北北条司展 後期。 从《天使心》开始，有许多亲切温馨的插曲和以 "家庭 "为主题的作品 🌸北条老师的彩色插图精致美丽，真是可爱✨。法国 C.H 签名和铃木良平的素描评论。 我们还从獠的Mokkori吃到了大块煎蛋咖喱🍽️  

https://x.com/kittysuki/status/1802668148950528465  
弟がきて  はい！おみやげって手渡された。北条司展に行ってきたらしい。そしてよかったらしい。よし！行くか！チケット取れるか確認する！  
我哥哥来找我了  是的 他给了我一个纪念品。他去看了北条司展。他说很不错。好了！我们走吧！我去看看能不能买到票 ！   
https://pbs.twimg.com/media/GQRdIMfbEAARph9?format=jpg&name=4096x4096  

https://x.com/taketora0131/status/1802716457345970342  
後期にも行ってきた〜♪新しい原画も見られて楽しい。それにしても北条先生の絵って、情報量が多いのに輪郭付近の描き方とかデフォルメ混ぜたりとか凄い工夫があるので、画集では解りにくい作業痕が見られるのも原画展の魅力。23日まで！  
我还去看了展览的後期，看到新的原画也很有趣。 展览将持续到 23 日！   
https://pbs.twimg.com/media/GQSJEDEaQAAl4j0?format=jpg&name=4096x4096  

https://x.com/onkonkan/status/1802266112320803271  
⭐️伊倉のコメント⭐️#Netflix 映画「シティーハンター」 大ヒットおめでとう㊗️亮平くん、望智ちゃん、とっても素敵でした！！司くんご夫妻も一緒に、6人でのお祝い会でした❣  
⭐️伊仓的评语⭐️#恭喜Netflix电影《城市猎人》大热㊗️亮平、望智都很棒! !司敦夫妇也一起,6人的庆祝会上❣  
https://pbs.twimg.com/media/GQLvTmNbYAArB-m?format=jpg&name=medium  

https://x.com/imu5imu4/status/1802244976891265491  
長い年月、こうして北条先生の絵や漫画や、原画を拝見できること本当にありがたいし幸せだなぁ……先生、ずっと描き続けてくれて本当にありがとうございます✨先生のサインも大好きです！こんなに長い年月のサインを拝見できて幸せだなぁ  
能看到北条老师这么多年来的画作、漫画和原创作品，我非常感激和高兴！...... 非常感谢老师这么多年来一直坚持画画✨我也很喜欢您的签名！ 能看到您这么多年的签名，我非常感激和高兴！   
https://pbs.twimg.com/media/GQLcPLfbYAAnW_l?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQLcPN-bsAAwxcB?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQLcPXsa0AAy8cC?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQLcQC7a8AAYnS9?format=jpg&name=4096x4096  

https://x.com/earlgreytealeaf/status/1802259589116858537  
北条先生の作品には人を魅了する力がありますね😌出来る事ならずっと続いてほしい企画です🙏残すところあと一週間、、貴重な原画の数々、目に焼き付けます👀✨  
北条先生的作品有一种让人着迷的力量😌我希望这个项目能永远进行下去🙏只剩一周了，我要目睹珍贵的原作👀✨  
https://pbs.twimg.com/media/GQLpifaaUAEl_03?format=jpg&name=4096x4096  

https://x.com/CeVc9c8/status/1802268653368324516  
入場特典も被らず一安心w  
https://pbs.twimg.com/media/GQLxyA0aAAAPCcN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQLxyAxbgAAqXgU?format=jpg&name=4096x4096  

https://x.com/CeVc9c8/status/1802269470427345175  
吉祥寺ブラブラ出来たし楽しかった〜😊またゼノンでイベントあったら行きたい！  
我可以在吉祥寺闲逛，非常有趣 😊 如果在Zenon再有活动，我也想去！   
https://pbs.twimg.com/media/GQLyg2aaMAAWSaX?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQLyg2aaUAAdzGm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQLyg3haUAA_FFG?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQLyg3fbYAAWgtP?format=jpg&name=4096x4096  

https://x.com/TOMOM0112358/status/1802311210454634652  
シテイーハンター  キャッツアイ  北条司展  前半・後半入店特典+フードステッカー  コンプリート出来ました。どのカードも最高！！！また、こう言うところに行きたいな～最高の空間やったな～  
https://pbs.twimg.com/media/GQMYe-paEAAqLEj?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQMYe_QacAAq1I2?format=jpg&name=4096x4096  

https://x.com/islalaland1214/status/1802314163936751680  
私も獠〜!!!!って追っかけ回したい❤️  
我也想獠~!!!! 我想追着你到处跑 ❤️  
https://pbs.twimg.com/media/GQMbLBAbgAAag_2?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQMbLBAbwAA6EaR?format=jpg&name=4096x4096  

https://x.com/love010mege/status/1802384314765857183  
沢山写真も撮れたし、今回予定よりだいぶ出遅れてしまったのでお目当てのもので残念ながら売り切れてしまっていたものもあったけれど、素敵なグッズや北条先生の複製原画も買えて大大大満足のめちゃくちゃ楽しい一日でした！北条司先生、本当に有難うございました！！  
我拍了很多照片，遗憾的是，由于我这次比原计划晚了很多，有些我想要的商品已经卖完了，但我还是买到了一些很棒的商品和北条老师画作的原版复制品，所以这是非常非常令人满意和愉快的一天！ 非常感谢北条司老师！  
https://pbs.twimg.com/media/GQNa-j_a0AAy8vK?format=jpg&name=4096x4096  

https://www.instagram.com/p/C8OscDKxzJz/?igsh=MWtqZTlydXUwbXdyYQ%3D%3D&img_index=1  

https://x.com/anikingerZ/status/1801809135530320220  
北条司展後期。今回はエンジェルハートの展示も多く、ホントに眼福！  
北条司展後期。 今年的展览有很多天使心，让人大开眼界！  
https://pbs.twimg.com/media/GQFP129bEAEy_wa?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQFP14LaQAABhRj?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQFP2B_aQAA9kDh?format=jpg&name=4096x4096  

https://x.com/Zetton700/status/1801860100254032118  
北条司展  後期キメてきました！前期も神がかった絵が多かったですが  後期は更に良過ぎました(^^)入れ替わったシティーハンター 、キャッツ・アイに加えてエンジェル・ハートや天使の贈り物の原画が見れてホント感無量です！  
https://pbs.twimg.com/media/GQF-NJxbEAYtQi6?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQF-NJvbEAESGi2?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQF-NJubEAAHgSI?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQF-NJxbEAQydhX?format=jpg&name=4096x4096  


https://x.com/2topsmap_nao/status/1801956715828797915  
先日の北条司展の戦利品😄大きさは、名刺より一回りぐらいなので小さいです。でも、｢混合版画技法を活用した｣という商品で、写真をアップにしても、細かいとこまで鮮明だし色も鮮やか。最近はこういったグッズ類のお迎えは控えてたけど、思わず買っちゃったよねー😅  
最近举办的北条司展的收获😄尺寸很小，比名片小一号。 但这是一款 "采用混合印刷技术 "的产品，所以即使是特写照片，每个细节都很清晰，色彩也很鲜艳。 我最近一直没买这类商品，但还是不假思索地买了😅  
https://pbs.twimg.com/media/GQHWDU8aAAAjXgg?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQHWDU7b0AABtrt?format=jpg&name=4096x4096  


https://x.com/kikicks336/status/1801959022133915789  
CHの展示、ほんとによくできてると思うのよ、話が繋がってるもの「アニキがぁっ」のシュガボ香の涙を獠は忘れられなかったんだろうと思う  いつ死んでもおかしくない世界でしか生きられない自分は香をまた悲しませる。それはできない、と思ってたはずで。だからさゆりさん回では槇村に→  
CH的展览，我觉得做得非常好，故事是连在一起的。 我觉得「アニキがぁっ」的シュガボ香的眼泪，獠忘不了。 我只能活在一个随时可能死去的世界里，我会让香再次伤心。 他一定认为自己做不到这一点。 所以在小百合小姐的那一集里，她对槇村说→    

https://x.com/tenchotenchoooo/status/1801375526390796508  
おはようございます。このシーンも、良き。   
早上好。 这个场景也不错。  
https://pbs.twimg.com/media/GP_FfgKbEAAo4t3?format=jpg&name=4096x4096  

https://x.com/mamiya99/status/1801413250292290046  
昨日の東京弾丸日帰り✈️から一夜明け方またいつもの何気ない朝  一人日帰りで東京へ行くなんて勇気が無くて悶々と悩んでたけど、えぃ❗って扉を開け行ってしまえば悩んでいた事が今は晴れ晴れ✨う～んシークレット狙っていたけどこちらでした  でもこの獠好きだし大切にしたい✨  
昨天，东京一日游✈️，又是黎明时分，一个平常随意的早晨。 我没有勇气独自去东京一日游，我在痛苦中担心，但一打开门去❗，我的担心就一扫而空✨，嗯，我的目标是秘密，但它是这样的。但我喜欢它，我要珍惜它✨  
https://pbs.twimg.com/media/GP_mfKXbEAM9wTi?format=jpg&name=4096x4096  

https://x.com/latelierfay/status/1801550469086777526  
ホントにアナログ絵…？と近づいて見ると汚れたかはみ出た部分をホワイトで修正してるの見えて震えた。てかこのパンツもなにをどう塗るとこうなるの…👀(写真はホワイトがわかりやすいように加工してます)  
这真的是一幅模拟画作吗？ 当我走近一看，我发现污渍或突出的部分已经用白色进行了修正，我不禁打了个寒颤。我的意思是，这些裤子也是用什么颜料画的...👀(照片经过处理，更容易看清白色部分）  
https://pbs.twimg.com/media/GQBkmr9bEAIwB8L?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1801624643150954507  
キャッツアイ の放送日でも #シティーハンター  投稿173  今回から北条司先生の多くの作品の原画が堪能できる #北条司展 の様子をアップしていきます！  さぁ皆様どれが先生のデビュー作か分かりますか？オープン記念の描きおろし「シティーハンター」の2人に、鈴木亮平さん主演の実写版関連の展示  
就在《猫眼#城市猎人》173 期播出当天，我们也将更新#北条司展，您可以欣赏到他的许多作品原画！ 你能猜出哪一幅是他的处女作吗？ 展览包括两幅《城市猎人》的新画作，以及与铃木亮平主演的真人版相关的展览 
https://pbs.twimg.com/media/GQCoDuyaQAIQdhr?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQCoDuybEAE6RyF?format=jpg&name=4096x4096  

https://x.com/pinktanpopo34/status/1801627283394072603  
カラー原画の繊細な線と彩色にうっとり。グッズコーナーを徘徊し、カフェでは動画を繰り返し観て（居座ってごめんなさい）行く度にハンマー持って写真撮って頂いて。楽しかった❣️  獠ちゃんもお持ち帰り💕  
彩色原画细腻的线条和色彩令我陶醉。 我在商品区闲逛，在咖啡馆反复观看视频（抱歉没走成），每次去都要和锤子合影，非常有趣。 ❣️ 我还带了一些獠chan💕回家  
https://pbs.twimg.com/media/GQCqdmFbEAMB4LK?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQCqdmDbEAMRldc?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GQCqdmEa0AAzi5a?format=jpg&name=4096x4096  

https://x.com/sui1198838/status/1800905009158345151  
昨日の日経記事を読んであ…となったのは「もう連載を描かれることはないのかも」アシから漫画家に!という時代ではなくなってきた→アシさんを育てるのも楽しかった→今は連載してないしアシさん居ない→でも連載にはアシさん要  「漫画は描いてるけど習作」  いやそう仰らず読ませてください #北条司    

https://x.com/fukaiasaihiroi/status/1801233815182217563  
吉祥寺ギャラリーゼノンさんで北条司展！　死ぬほど絵が上手くて格好ええ……。作家挨拶に「こりゃひどい絵だなって感想も自由です（笑）」って書いてたけど、この絵を見てそんなこと思うの、レオナルド・ダ・ヴィンチでもムリやろ（笑）  
在吉祥寺画廊 Zenon 举办的北条司展！　画得太好、太酷了，简直要了你的命。...... 画家在致辞中写道："您可以认为这是一幅糟糕的画（笑）"，但即使是达芬奇看了这幅画也不会这么想（笑）  
https://pbs.twimg.com/media/GP9Emm4bwAAVURq?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP9Emq5bcAEj6I-?format=jpg&name=4096x4096  

https://x.com/2topsmap_nao/status/1801140804431528436  
行ってきたぁー😆😆😆ステキだった。繊細で美しい原画に惚れ惚れ。原稿は大好きなシーンばかりでテンション上がった。  
我去过那里 😆😆😆那里很可爱。 我爱上了那些精致美丽的原画。原稿都是我最喜欢的场景，我非常激动。  
https://pbs.twimg.com/media/GP7wAzZaMAA9ddi?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP7wAzbbUAA6CKM?format=jpg&name=4096x4096  

https://x.com/yuzucacandy/status/1801254982513221897  
北条司展後期行ってきた。やっぱり手描きの原画は最高。物凄い満足感。もう一度行こうかな。  
我去看了后半部分的北条刚展览。 手绘原画依然是最棒的。 极大的满足感。 我想我还会再去的。  
https://pbs.twimg.com/media/GP9XznAbMAAm_p-?format=jpg&name=4096x4096  

https://x.com/tenchotenchoooo/status/1801043066306113665  
おはようございます。  
早上好。  
https://pbs.twimg.com/media/GP6XB8PaEAAn6TF?format=jpg&name=4096x4096  

https://x.com/hirosuke_kun/status/1800789515982000295  
仕事のふりして吉祥寺ギャラリーゼノンの #北条司 展🚘🔫実直なサラリーマンやらせてもらってます😉これはファン必見ってやつですよマジで。北条先生と鈴木亮平さんまで本日いらしてた模様🥹無理してでも比較的空いててゆっくり見られる平日に来るべし。  
假装在工作，我正在吉祥寺画廊 Zenon 观看#北条司 展🚘🔫我可以做一个诚实的商人😉说真的，这是粉丝们必看的展览。今天连北条老师和铃木良平都来了 🥹你应该在工作日来，因为那时相对空闲，即使不得不来，你也可以随意观看。  
https://pbs.twimg.com/media/GP2wez6bUAABrp5?format=jpg&name=4096x4096  

https://x.com/thelowtierchara/status/1800916522128507196  
北条司展 後期  半世紀以上生きて来て初めてコラボカフェ(でいいのか)経験しましたw  北条司先生監修の「特製スペシャルブレンド」もいただきましたよ  
北条司展 後期 在我半个多世纪的人生中，我第一次体验了合作咖啡馆（是这样吗？  还品尝了由北条司先生监制的 "特调Special Blend"  
https://pbs.twimg.com/media/GP4kCBxbQAAWIu3?format=jpg&name=4096x4096  

https://x.com/thelowtierchara/status/1800916512192278837  
北条司展 後期  カラー原稿も然ることながら、漫画本来の白黒原稿に至ってもその美しさときたら  
北条司展 後期  彩色手稿，甚至是黑白手稿原件的精美程度无与伦比。
https://pbs.twimg.com/media/GP4kBcxa4AEN-IB?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kBdHaIAEt6c0?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kBgWbUAAG_zh?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kBika4AAceZb?format=jpg&name=4096x4096  

https://x.com/thelowtierchara/status/1800916501794562287  
北条司展 後期  残念ながらエンジェル・ハートは読破してないので何れ機会を作って必ず  
北条司展 後期 遗憾的是，我还没有读完天使心，所以我一定会抓住机会  
https://pbs.twimg.com/media/GP4kAzsaAAEtt7S?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kA0pbIAINPlN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kA3XasAAcK6k?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kA3sagAA_TN_?format=jpg&name=4096x4096  

https://x.com/thelowtierchara/status/1800916490184790166  
北条司展 後期  ちょうど今アニメが再放送されていることもあって声優陣様の艶のあるお声がずっと脳内再生されていました  
北条司展 後期 目前正在重播这部动画片，所以声优们充满魅力的声音一直在我脑中回放   
https://pbs.twimg.com/media/GP4kAKuaMAAsy8T?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kANda4AAlGN-?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kAMXaYAAuW7u?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4kAOTbcAAgcvi?format=jpg&name=4096x4096  

https://x.com/thelowtierchara/status/1800916480667877809  
北条司展 後期  キターン！www  平日だったのでメチャメチャゆったり自分のペースで観覧出来ました  
北条司展 後期  キターン！www   因为是工作日，我可以悠闲地观展   
https://pbs.twimg.com/media/GP4j_m0bUAAmhob?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP4j_p1a0AAM5jp?format=jpg&name=4096x4096  

https://x.com/takenoko_2018/status/1800772471354016194  
Gallery-zenonで開催中の北条司展に行ってきました。一言で言って最高🤗美しいカラーイラストや迫力満点の原画が並び、キャッツ・アイからシティ・ハンター、エンジェル・ハートまで、世界に浸ってしまいました  
我去参观了在 Gallery-zenon 举办的北条司展。 从《猫眼》到《城市猎人》和《天使心》，一幅幅精美的彩色插图和极具感染力的原画让我沉浸其中！   
https://pbs.twimg.com/media/GP2hAaha4AAqJKO?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP2hAajbsAAOxu4?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP2hAahaAAAhWkz?format=jpg&name=4096x4096  

https://x.com/coamix_sales/status/1800724863956787481  
北条司展 6/23(日)まで！📢／な、なな、なんと！北条司先生と鈴木亮平さんのサインがッ✨✨＼いろんなところに描いていただいています🖊探してみてください👀💕  
https://pbs.twimg.com/media/GP11sLNbIAAThDm?format=png&name=4096x4096  
https://pbs.twimg.com/media/GP11sLLacAA5yqM?format=png&name=4096x4096  
https://pbs.twimg.com/media/GP11sLNbAAAig5n?format=png&name=4096x4096  
https://pbs.twimg.com/media/GP11sLLasAAS6Vk?format=png&name=4096x4096  

https://x.com/kikicks336/status/1800712898807009338  
並んでる間、店内がバタバタしてたから…誰か来たのかな？って  亮平さん、いつ来られるかなぁ？って思ってたら！！  ぎゃああああ！！  先生も！！お二人ご一緒だったんですね！！  
在我排队等候的时候，店里人头攒动，我想......谁来了？ 我不禁问道。 良平，你什么时候来？ 我在想！ 哎呀呀  老师也来了 你们两个在一起  
https://pbs.twimg.com/media/GP1q0h2bMAA1o0g?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP1q0h0bAAAgQPC?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP1q0hzagAAOZcV?format=jpg&name=4096x4096  


https://x.com/mamiya99/status/1800339738072895720  
「シティーハンター」作画比較①  
https://jgjhgjf.hatenablog.com/entry/2017/11/24/225513  
「シティーハンター」作画比較②  
https://jgjhgjf.hatenablog.com/entry/2017/11/24/235045  
「シティーハンター」作画比較③  
https://jgjhgjf.hatenablog.com/entry/2018/02/15/045251  

https://x.com/tenchotenchoooo/status/1800636793500664268  
https://pbs.twimg.com/media/GP0lnZ2agAAmi0F?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GP0lnZ3bsAAiGk2?format=jpg&name=4096x4096  

https://x.com/imu5imu4/status/1800436624385573224  
北条先生のインタビュー記事ありがとうございます🙏✨  先生の「今」のお気持ちや考え、そして「これから」の声を聞けるのは本当に嬉しいし、ありがたいです😭✨  それもやはりギャラリーゼノンさんで原画展を開催して頂いたからこそ🎉改めて開催ありがとうございます🎉  
感谢您对北条先生的采访🙏✨  能听到他 “现在 ”的感受和想法，以及他的 “未来”😭✨，真的很高兴，也很欣慰  这也是因为你们在Zenon Gallery举办的原画展🎉，再次感谢你们举办这次展览🎉  

https://x.com/imu5imu4/status/1800436776219390009  
先生の描くキャラクターが魅力的なのは勿論だけれど、ストーリーに沿ってキャラクターの心の機微がぐんぐん伝わってくるところが本当に魅力的で大好き✨どんな時も「今も修行中」と仰られる、先生の謙虚で真摯な姿勢は一個人としても見習う部分が多いです！  
老师画的角色很有魅力是理所当然的，但是随着故事的发展，角色的内心微妙的地方也很有魅力，非常喜欢✨无论什么时候都说“现在也在修行中”，老师谦虚真挚的态度作为个人也有很多值得学习的地方!  

https://x.com/imu5imu4/status/1800436872428339541  
書道に例え、とても良くわかります……  とはいえ、わたしは息を飲むような美しい楷書がとても好きなので、そういう意味では先生の絵が好きなのはもうDNAに組み込まれているんだろうなあ  でも楷書も行書も草書も隷書も好き笑  つまり北条先生の描かれる絵も漫画も、いつもいつでも大好きです！  
用书法来比喻，非常好理解……话虽如此，但我非常喜欢优美得令人窒息的楷书，从这个意义上来说，喜欢老师的画已经融入了我的DNA。楷书、行书、草书、隶书我都喜欢笑也就是说，北条老师画的画和漫画，我一直都很喜欢!  

https://x.com/3kaori025/status/1800520900762349594  
後期の個人的見どころ  
・エンジェルハートのカラー原画  
・AHに対する先生のコメント  
・先生がシティーハンターと読者層に合わせて人物の描き方を変えたというＦ．COMPOの原稿  
・シティーハンターの中後期の原画の美しさ  
・ファンからのメッセージが並ぶ空間  
后期的个人看点  
・天使心的彩色原画  
・老师对AH的评语  
・老师根据城市猎人和读者群改变了人物描写方法的F.COMPO原稿  
・城市猎人中后期原画之美  
・粉丝留言林立的空间  

https://x.com/oya_betsuaca/status/1800021993078878662  
シュガーボーイ  大好きな話  原画が一番素敵ではあるが、原画でも写真でも漫画で見ても綺麗さが変わらないのでおののいてる  
Sugar Boy  我最喜欢的故事  原著是最棒的，但我很害怕，因为原著、照片和漫画都一样美  
https://pbs.twimg.com/media/GPr2dMzawAAu50M?format=jpg&name=4096x4096  


https://x.com/oya_betsuaca/status/1800023021010432319  
なにこの絵…好き。線の書き方とか影の付け方とか、ほんと好き  プロポーズっぽいこと言うのに、毎回ボケになっちゃうとこ、好き（好きしか言ってないww）  そして大注目すべきは銃の質感よ！なにこれ！すご！  （好きすぎて語彙力低下してるオタク）  
这是什么画......我喜欢。我很喜欢你画线条和阴影的方式。我喜欢她说的话，听起来像是求婚，但每次都变成了笑话（我只是说我喜欢！）。枪的纹理是最引人注目的地方！ 这是什么？ 太神奇了！(我太喜欢了，我词穷了，书呆子）  
https://pbs.twimg.com/media/GPr3XDfa8AEZrdm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPr3XFPbAAEudoW?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPr3XM8b0AAQmt7?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPr3XM6aoAAxtm9?format=jpg&name=4096x4096  

https://x.com/oya_betsuaca/status/1800023998983774379  
このイラスト、シャーペンと墨汁着けたペン先と絵の具のみで描かれているらしい。恐ろしい…  
据说这幅插图是仅用机械铅笔、墨水笔尖和颜料绘制的。令人惊骇...  
https://pbs.twimg.com/media/GPr4R6PaQAArbuv?format=jpg&name=4096x4096  

https://x.com/oya_betsuaca/status/1800025659781374190  
ファミリーコンポ。空さん好きだったなぁ…  
Family Compo部分。我喜欢空先生...  
https://pbs.twimg.com/media/GPr5ysHaIAETVtL?format=jpg&name=4096x4096  

https://x.com/oya_betsuaca/status/1800027809240523237  
仏版シティーハンターからもサインが！！なんだ今回の展示は…多国籍だ  見所多すぎだ  
https://pbs.twimg.com/media/GPr7v4Ia8AALIlu?format=jpg&name=4096x4096  

https://x.com/oya_betsuaca/status/1800028982517387403  
宝塚版シティーハンターも好きです！アニメやドラマも含めて、（多分）初めてミックが登場したから、その点でも胸熱だった。宝塚はいつも作品に対するリスペクトがあるから、観てて安心する。これで彩風さんのかっこよさを知った…  
我还喜欢宝冢版的《城市猎人》！ 这是米克第一次出演电视剧，包括（可能）动画片和电视剧，所以我也非常兴奋。 宝冢一直很尊重作品，所以我看得很放心。 现在我知道 Saikaze 有多酷了...  

https://x.com/oya_betsuaca/status/1800031476186313167  
昔のイラストから最近のを見てると、画風もそうなんだけど、カラーの塗りが全然違う。昔から綺麗な絵なんだけど、あのリアルな質感はきっと試行錯誤しながら努力された結果なんだなーと、しみじみしました。原画展って、リアルな絵の迫力も素晴らしいけど、こういうのも分かるから良い。  
当我从旧插图中看到最近的插图时，色彩和绘画风格完全不同。 这些画一直都很美，但逼真的质感一定是反复试验和努力的结果。原画展很好，因为你可以看到写实画面的力量，但它也很好，因为你可以理解这种东西。  

https://x.com/kikicks336/status/1800293961518723304  
先生のインタビュー！見たことないやつ！しかも日経だよ！先生の習作だけを集めた展示とかも見たい！ギャラリーゼノンのテラスの描き下ろしも、ネトフリのポスターも、天使の涙の獠も全て感動でした！漫画、読みたいです！原画展、まだまだ行けるので行くぞ〜！  
教师访谈！ 我从未见过的采访 而且是日经！ 我想看一个展览，只展出他的研究成果！ Zenon Gallery 的露台画、Netflix 的海报和天使泪 獠都非常感人！ 我想看漫画！ 我还可以去看原作展，所以我要去！  
https://www.nikkei.com/article/DGXZQOUD036920T00C24A6000000/  


https://x.com/sabashio_38/status/1799307109684937049  
満たされ  
心满意足  
https://pbs.twimg.com/media/GPhsRgwawAAonZG?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPhsRg0aQAAwPDm?format=jpg&name=4096x4096  


https://x.com/AT_583/status/1799422688500535543  
西日暮里駅に依頼があった  
已向西日暮站提出请求  
https://pbs.twimg.com/media/GPjVZV8aQAAfrX5?format=jpg&name=medium  

https://x.com/5Rwph0EEiX83793/status/1799579066045178312  
いっけない！！ポチするの忘れてたっ！！そうならないように、朝から推しポチ活しましょう📽✨📽愛と宿命のマグナムhttp://dreampass.jp/m89959  
https://pbs.twimg.com/media/GPljn8kaMAA6oM1?format=jpg&name=4096x4096  

https://www.instagram.com/p/C7_4sD7JBJN/?igsh=YmRra2l4aGVpMnY0  

https://x.com/AoiTakarabako/status/1799566432755396684  
おパヨ～ございます🌄  
https://pbs.twimg.com/media/GPlYIRFaIAAYOD6?format=jpg&name=4096x4096  

https://x.com/MarchanTop3/status/1799646510340247716  
北条司の特徴トップ３  ３位 女性キャラクターが美人  ２位 女性キャラクターがセクシー  １位 男性キャラクターがワイルド  
北条司的3大特点  第3位 女性角色美丽  第2位 女性角色性感  第1位 男性角色狂野  

https://x.com/michanachan/status/1799648424050180170  
奇跡的にゼノンカフェの北条司原画展後期いけた✨  稲葉さんのライブのおかげっ😭前期も思ってんけど  映像みてる方もう少しセンターによってくれないと  壁側の展示が見れないっ  真剣に見られてるのに声かけるのもっ  ていうか声かけれないっ  遠くから眺めたっ😆  
奇迹般地，我得以去 Zenon Cafe✨观看北条司原画展後期  多亏了稻叶先生的现场演唱会😭  前期我也是这么想的  如果观看图片的人不稍微靠近中心一点，就无法看到展览的墙面  我看不到墙上的展览  当他们如此专注地看着我时，我甚至无法与他们交谈  我是说，我没法跟他们说话  我只是远远地看着  
https://pbs.twimg.com/media/GPmis_0aEAAW_U6?format=jpg&name=4096x4096  

https://x.com/Yoshiro35913325/status/1799700116884148534  
参观完泽农咖啡馆后，我又看了看訚井！  我看到了所有的訚井，但我最喜欢这两个！  
https://pbs.twimg.com/media/GPnRt6oa0AAGlA1?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPnRt_masAAxYQH?format=jpg&name=4096x4096  

https://x.com/hassakuseijin/status/1799700332131553565  
有很多原画，我激动不已💓‼‼只上传了我最喜欢的#City_Hunter，当然也有很多#Cats_EYE，还有一些#北条司 先生的心爱之物，所以有很多可以看♪。顺便说一下，迷你千丹☆是 @ganguu2 迷你千丹☆穿着 @ganguu2 的 #冴羽獠 模特！  

https://x.com/Ko_ichi_32/status/1799209057838104626  
「北条司展」に行ってきました😆今回、ワイフが行けず二人分のチケットを握りしめて🎫ぼっちで参加😅貴重な展示品の数々にリアタイで触れていた作品に感無量でした😊入場者特典のカードも名刺っぽい感じでイイね👍🏻場所は「東京都武蔵野市吉祥寺南町2-11-1」#北条司展  
我去了 “北条司展”😆这次妻子去不了，我拿了两张票🎫，一个人去了😅很多珍贵的展品和后领带里接触过的作品让我很感动😆入场券的卡片也不错，像名片一样。👍🏻 地点是 “东京都武藏野市吉祥寺南町 2-11-1" #北条司展  
https://pbs.twimg.com/media/GPgTGeEbEAACf58?format=jpg&name=4096x4096  

https://x.com/Ko_ichi_32/status/1799211233503252977  
「北条司展」では「シティーハンター」や「キャッツ・アイ」「エンジェル・ハート」など原画や立体造形物、アイテムなど数々の展示品がありました😆とにかく「シティーハンター」が大好きだったので、食い入るように展示品に見入ってました😁#北条司展 #シティーハンター  
「北条司展」上，有许多展品，包括《城市猎人》、《猫眼》和《天使心》的原画、手办和物品 😆 反正我喜欢《城市猎人》，所以我把展品都看完了😁#北条司展 #城市猎人  
https://pbs.twimg.com/media/GPgVFBZacAAmbAt?format=jpg&name=4096x4096  

https://x.com/mqshino/status/1799271215066169800  
ヽ(´▽｀)/はーい！✨✨  
ヽ(´▽｀)/我祝你好运！ ✨✨  
https://pbs.twimg.com/media/GPhLoZ4aEAAEHF3?format=jpg&name=medium  

https://x.com/kenichi_oimo/status/1799318677894189079  
ファンアートとメッセージが愛にあるれててたまらないっすね🫶  
粉丝们的艺术作品和留言太有爱了🫶  
https://pbs.twimg.com/media/GPh2yorbsAAf1Jv?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPh2yorawAAPrIU?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPh2yorakAAGu9R?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPh2youakAA-KuW?format=jpg&name=4096x4096  

https://x.com/kenichi_oimo/status/1799319410341200097  
外出たら増えてました！   
当我出门时，它又增加了！  
https://pbs.twimg.com/media/GPh3dkraoAAOlCT?format=jpg&name=4096x4096  

https://x.com/tendonma2020/status/1799369047773167981  
少し前ですがギャラリーゼノンに行きました🔫素敵な絵をありがとうございます✨  
前不久，我去了Zenon Gallery 🔫 谢谢你的可爱画作✨  
https://pbs.twimg.com/media/GPiklt2bMAAGs8r?format=jpg&name=4096x4096  

https://x.com/u_tan_uu_tan/status/1799449984666112421  
キャッツアイ連載当時から先生の作品が大好きで、展示された原稿の美しさに見入るばかりです。代表作だけでなく短編の原稿も拝見できたことが嬉しく、2階スペースもじっくり堪能しました。まだまだ先生の絵を見ていきたいです、よろしくお願いします！  
从《猫眼》连载开始，我就喜欢上了他的作品，不禁赞叹展出的手稿之美。 我很高兴不仅能看到她的杰作，还能看到她的短篇小说手稿，我非常喜欢二楼的空间。 我还想看到您更多的画作，非常感谢！  
https://pbs.twimg.com/media/GPjuOAYagAA3-SC?format=jpg&name=medium  
https://pbs.twimg.com/media/GPjuOEJakAAna-N?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPjuOTtbAAAKyJT?format=jpg&name=4096x4096  

https://x.com/hassakuseijin/status/1799700319842238488  
GalleryZenon   にて開催中‼︎  #北条司展 へ💨やっと行けたー😭やっと #冴羽獠 ちゃんに会えたー😭今回ゼノンさんが、ギャラリー仕様になって初めてでしたが  予約なので、すんなりー。ミニちぃたん☆用の100tハンマー欲しいですっ♪  続く  
GalleryZenon 我终于去了 ‼︎ 💨 💨 😭 的 #北条司展💨 😭 我终于见到了 獠  😭 这是 Zenon 第一次被改造成画廊，但这是一次很棒的体验  我提前预约了，所以很轻松。 我想为我的迷你版契丹☆买一个 100t 的锤子！未完待续  
https://pbs.twimg.com/media/GPnR5XhbEAE7TIG?format=jpg&name=4096x4096  

https://x.com/jojo_start_1987/status/1799781051256213619  
5/25 北条司展  北条司先生もこの齢になったからこそ拝見したかった…  シティーハンターは外せない作品…獠ちゃんに憧れてモデルガンを買ってみたりもあったことを思い出しました…  ありがとうございました…  先生もますますのご健勝とご活躍をお祈りしております…    
5/25 北条司展  在我这个年纪，我就想去看看北条司..城市猎人是我不能错过的作品......我还记得，因为崇拜北条司，我甚至买了一把枪模......非常感谢  祝您身体健康，万事如意...  
https://pbs.twimg.com/media/GPobU6OboAASFER?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPobU4laAAAofT1?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPobU8PbQAAwSeb?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPobU7kbQAA-GCh?format=jpg&name=4096x4096  

https://x.com/We_are_CH/status/1799781101327847794  
北条司展 拝見しました 私は幼少期にキャッツ・アイと出逢い、10代20代と通じてシティーハンターや他作品を愛読していました  今回大好きなCH真柴しおり回の原画を観た時、少年ジャンプを夢中で読んでいた頃の想いが蘇り胸が熱くなりました  北条司先生の作品は時代を超えて愛される作品だと思います！  
我参观了北条司展  我第一次接触《猫眼》是在孩提时代，十几岁到二十几岁时一直喜欢阅读《城市猎人》和其他作品  这次看到我最喜欢的CH 真柴しおり回的原画时，我被自己当年热衷阅读少年Jump的回忆所打动  我认为北条司的作品是永恒的，深受喜爱！   

https://x.com/We_are_CH/status/1799782869885493364  
大好きな真柴しおり回  しおりちゃんを優しく抱きしめる香ちゃんに聖母みを感じる✨  展示はないけど、おむつ替える獠 のシーン大好きだったなぁ💓  
最喜欢的真柴しおり回  温柔地抱着书签的香酱，让人感觉到圣母气质✨  但我喜欢换尿布的场景没有展览💓  
https://pbs.twimg.com/media/GPoc-cnaQAAtDL8?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPoc-ckbQAA5ZqL?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPoc-cka0AAnNAN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPoc-cnaMAAGP1A?format=jpg&name=4096x4096  


https://x.com/ViVA2020renewal/status/1799046099300434422  
【#北条司展@吉祥寺GalleryZENON】 『キャッツ♡ アイ』『シティーハンター』で知られる #北条司 先生の原画展へ  漫画家さんの原画というのは想像以上に描き込まれているとは知っていたけど  元来画力がおありの先生のそれはここまですごいんだ❣️と  驚愕の作品群だった  ※映像以外の作品掲載は許可済→  
【#北条司展@吉祥寺GalleryZENON】 我们去参观了以《猫♡眼》和《城市猎人》而闻名的北条司的原画展  我知道漫画家的原画比我想象的要细腻  我惊讶地发现，他画的东西比我想象的要多得多❣️这是一组令人惊叹的作品  除图片以外的作品均已获准出版→  
https://pbs.twimg.com/media/GPd-4_iasAAmaop?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPd-4_faQAAam3_?format=jpg&name=4096x4096  


https://x.com/amechan_1822/status/1798688199008727198  
念願の大好きなジャンプ作家さんの展示を見に行きました🚗シティーハンターもエンジェルハートもキャッツアイも単行本やアニメでどっぷりハマっていて、子供の頃からずっと憧れの世界でした✨カラー原画本当に丁寧に描かれていて感動しました💖  
我去看了心仪已久的 JUMP 画家的画展🚗🚗我是通过书和动画迷上《城市猎人》、《天使心》和《猫眼》的，从小就一直很羡慕他们的世界✨💖原作的彩图画得很仔细，真的很佩服💖  
https://pbs.twimg.com/media/GPY5XvUbcAEm07l?format=jpg&name=4096x4096  

https://x.com/pinktanpopo34/status/1798771535844659369  
ゆっくりｶﾚｰを食べてじっくり展示を見てまたゆっくりｹｰｷを食べて3時間半ほど居ちゃった。もう買えない取り皿の写真も撮させていただきました😊ﾒｯｾｰｼﾞﾉｰﾄにはﾍﾞﾃﾗﾝ・ﾙｰｷｰ問わず先生への愛が溢れていて絵も上手で親子で来ている方もいて素敵☺️✨✨  
我呆了三个半小时，慢慢吃咖喱，慢慢看展览，又慢慢吃蛋糕。 我还拍下了那些再也买不到的盘子😊留言条上写满了对老师们的爱，有老教师，也有新秀，画得也不错，还有人带着父母和孩子一起来，真是太好了☺️✨✨  
https://pbs.twimg.com/media/GPaFLMGbsAAd78p?format=jpg&name=900x900  
https://pbs.twimg.com/media/GPaFLMIaEAAlVX-?format=jpg&name=900x900  
https://pbs.twimg.com/media/GPaFLMGagAA0QwU?format=jpg&name=4096x4096  

https://x.com/smiling_3flower/status/1798166843808702492  
https://pbs.twimg.com/media/GPRfNLTa4AAnMoX?format=jpg&name=4096x4096  

https://x.com/CH_0326M/status/1797585436917023028  
シティーハンターやキャッツアイといった代表作はもちろん他短編作品も拝見出来て嬉しかったです‼️北条先生の絵の変遷、歴史を肌で感じることが出来る貴重な空間でした  たくさんの方、海外の方にも是非観てほしいです！The Tsukasa Hojo exhibition is worth visiting.❤️  
我很高兴不仅看到了他的杰作，如《城市猎人》和《猫眼》，还看到了他的其他短篇作品‼️在这个珍贵的空间里，我亲身感受到了北条老师绘画的转变和历史。我希望有更多的人，甚至是来自海外的人，能够看到这次展览！值得一看的北条刚展览❤️  
https://pbs.twimg.com/media/GPJOaoJboAEBiEe?format=jpg&name=4096x4096  

https://x.com/Kaikai07494232/status/1797617122736255447  
北条先生の画力が素晴らしい！このクオリティのが掲載されてた当時の少年誌やっぱスゴイわ。  
北条老师的绘画技巧非常高超！ 这种质量的作品出版时，那个时代的青春少年杂志令人惊叹。  

https://x.com/mihohi1/status/1797744640592150754  
北条先生の描く絵は完璧過ぎて、逆に端の処理やホワイトに萌え〜〜を感じてしまいます🥰第二弾行きたかったなぁ😂全国巡回展示して欲しい🙏  
北条老师的画太完美了，相反，边缘处理和白色🥰让我觉得很萌~~真希望能去看第二届🥰真希望展览能在全国各地巡回展出🙏  
https://pbs.twimg.com/media/GPLfOKaasAIyhwo?format=jpg&name=4096x4096    
https://pbs.twimg.com/media/GPLfOKdakAAZS3C?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPLfOKbasAISl5Y?format=jpg&name=4096x4096  

https://x.com/shizuka8001/status/1797467481457786950  
吉祥寺 @GalleryZenon さんで開催中の #北条司展 🔫✨原画に泣き懐かしのグッズにホワホワしながら何度見ても飽きない空間の提供がさすが❤︎今回のための書き下ろし獠＆香は、見たかった光景でもあって。更にジャンプ表紙の先生は獠に匹敵のソルジャーです⚔️  
https://pbs.twimg.com/media/GPHjI6jaUAAETa3?format=jpg&name=4096x4096  

https://x.com/kaishumiakaxyz/status/1797542037526606323  
昨日はフォロワーさんと北条司展へ🎶  何度観ても北条先生が綺麗すぎて😭✨  語彙力無くて綺麗しか言えない！  久しぶりに地蔵ビルにも足を伸ばして  Netflixのポスターも眺めて来ました^^  
昨天，我和我的追随者先生/女士一起去了展览，🎶无论我看了多少遍，北条老师都太😭✨美了，无法言表，我只能说美！  很长一段时间以来，我第一次去了地藏大厦，看了Netflix的海报^^  
https://pbs.twimg.com/media/GPIm8WjacAEZIW2?format=jpg&name=4096x4096  

https://x.com/shinsk_k/status/1797557627305775313  
シティーハンターは  私に人生の全てを  教えてくれた作品です！  
城市猎人  是这部作品教会了我关于生活的一切！  

https://x.com/meimei12612149/status/1797580810662588898  
先生の絵は温かみがあり男性が描いたとは思えない美しさに魅了されました。これで毎週15ページ描かれてたなんて😭色味も特に描き下ろしのゼノンでの獠香が  温かみがあり大好きです。新宿だけでなく吉祥寺にも二人はいるんだなと😊  
我被他画中的温暖和美丽深深吸引，我简直不敢相信这些画是一个人画的。我不敢相信他每周都会画 15 页这样的画😭。色彩，尤其是新画的Zenon的獠香。我喜欢这种温暖的感觉。不仅是新宿，吉祥寺也是如此😊。  


https://x.com/Risuke3K/status/1797573232180224325  
キャッツ・アイの連載１回めから   欠かさず拝見しております。 #北条司展 前期にお邪魔しました。素晴らしかったです✨  原画をじっくり観て  掲示板とノートにメッセージを描いて  コラボメニューを頂いて  あっという間に３時間経ってました。  御縁が有ると嬉しいです。  
从#猫眼系列#第1辑开始，我就一直在关注。我参观了北条司展 前期。 我仔细看了原画，在告示板和笔记本上画了留言，还拿到了合作菜单。一眨眼就过了 3 个小时。 很高兴有机会认识你。   
https://pbs.twimg.com/media/GPJDU8TaAAIXAjl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPJDU9Ua4AATqif?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPJDU-LaIAEnpQ0?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPJDU-xacAELIM0?format=jpg&name=4096x4096  

https://x.com/yuruyuru0217/status/1797620677534715970
シティーハンターのアニメは小学校低学年から見ててあの時代からモッコリは流行ってましたねwwwその最高傑作の作品を小さい時から同じように育って見てて月日は流れエンジェルハートの漫画で泣きました…最高の思い出をありがとうございました！！  
我从小学低年级就开始看《城市猎人》动漫，而 Mokkori 从那个时代开始就很受欢迎 www 我从小就以同样的方式看着那部杰作长大，几个月过去了，我为天使之心漫画哭了...... 非常感谢你留下最美好的回忆！  


https://x.com/yfdm0127abc/status/1797643613872218230  
ご縁がありますよーに(꒪˙꒳˙꒪ )  前期は時間が無くて食べられなかった  もっこりオムライスと北条司ブレンドも  後期でしっかり味わえて幸せでした🫶永遠にこの作品の魅力に取り憑かれています   
祝你好运！前期因为没时间，我吃不下饭。我很高兴能在後期品尝到蛋包饭和北条司混合料理。我永远迷恋这部作品的魅力。  
https://pbs.twimg.com/media/GPKDVlXbEAESx0a?format=jpg&name=4096x4096  

https://x.com/annzuironosora/status/1797645501967548800  
4月、北条司展のために新幹線に乗り東京に向かいました。はじめて見る先生の生原画は繊細で美しく陰影もトーンも丁寧で愛が詰まっていました。これは唯一無二の芸術品です。修正跡も見れて当時の時代の雰囲気も感じられました。素敵な企画をありがとうございました🔫🔨  
今年 4 月，我乘坐子弹头列车前往东京参加北条司展。 我第一次看到了他的现场原画，这些画作细腻、优美，阴影和色调都很讲究，充满了爱。这是一件独一无二的艺术作品。 我还看到了修改的痕迹，感受到了当时的氛围。 感谢你们的精彩企画 🔫🔨  
https://pbs.twimg.com/media/GPKFCcWbkAAdxMM?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPKFCcXaUAACpHm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPKFCcbaYAEwnWZ?format=jpg&name=4096x4096  

https://x.com/nukamiso1336/status/1797669155640061995  
エンジェルハートが大好きです  特にシャンインと信宏がストリートダンスのチームに入る話が大好きです。シャンインの立場、境遇、想いが垣間見れるこの話がとても印象にのこっています！  
我喜欢《天使心》  我尤其喜欢香莹和信宏加入街舞队的故事。 这个故事让我们看到了香莹的立场、境遇和情感，给我留下了非常深刻的印象！  


https://x.com/oimotoika/status/1797720941247762775  
高校の頃より北条先生のファンです😝母に買って貰ったコチラの画集を毎日眺め、結婚後も唯一の👰嫁入り道具として持参しました💗結婚して約20年になりますが🥸旦那よりも冴羽を眺めている時間の方が多いような気が致します。それくらい北条作品が大好きです😍  
我从高中时代起就是北条老师的粉丝😝😝，我妈妈给我买了这本美术书，我每天都看，甚至在我结婚后，我还把它作为我唯一的👰聘礼💗💗我结婚大约 20 年了，但我觉得我看 Saeba 的时间比看我丈夫🥸 的时间还多。这就是我对北条作品的喜爱😍  
https://pbs.twimg.com/media/GPLJqsZbYAAXEfL?format=jpg&name=medium  

https://x.com/coamix_sales/status/1797486472033148993  
抽選で3名さまに当たる！💕📕北条司サイン入り図録📕💕＼☀️🌈北条司展 後期スタート記念  合同プレゼントキャンペーン🌈☀️  ①下記のアカウントをフォロー  (@GalleryZenon/@coamix_sales)②GALLERY ZENONアカウントのこの投稿を引用RP(北条司作品への想いをぜひ！)   
抽取三名获奖者！ 💕📕📕北条司签名的画册 📕💕＼☀️🌈庆祝北条司展後期开始 联合呈现活动🌈☀️ ①关注以下账号（@GalleryZenon/@coamix_sales）②Gallery Zenon 在您的账户上RP本帖（请分享您对北条司作品的看法）  
https://pbs.twimg.com/media/GPH0arLa8AA8Fzx?format=jpg&name=medium  

https://x.com/chiroru_special/status/1796839028916207683  
本日のGALLERY ZENONの成果物🎵 アクリルキーホルダーかわいい🩷 そして、何と言ってもやっとガラス越しのキスシーンのカード手に入れた！！！😆💕めっちゃ嬉しい  
今天的 GALLERY ZENON 手工艺品🎵 亚克力钥匙扣可爱🩷 更重要的是，我终于拿到了隔着玻璃的吻戏卡片！ 太开心了😆💕💖  
https://pbs.twimg.com/media/GO-nkwPaMAAVeP2?format=jpg&name=4096x4096  

https://x.com/tou_tsu/status/1796853343333310564  
我来到池袋，顺便去了动漫东京站🤗。 当然，我是来找冴羽獠的✨  
https://pbs.twimg.com/media/GO-0lr2awAA5tkM?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO-0lr0aIAA40V6?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO-0lr1bMAAPtv_?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO-0lr2bcAAM2Io?format=jpg&name=4096x4096  

https://x.com/miu596lala/status/1796858415647731793  
振り返り  ⑲クランクイン  作品はファンの人たちのもの、愛のある改変に絶対しなければ、という強い思い  「黙ってあの世で土下座してろ」、これだけが獠の怒りが見える台詞  実写化する際の実在感  考古学の役をやりたい  全身ﾌｫﾄ有  
回顾过去  (⑲Crank-in)  这部作品属于粉丝，我强烈地感觉到，我必须对它做一个有爱的改动。 唯一能表现余怒的台词是："闭嘴，来世跪下。 真人版改编的真实感  我想演个考古的角色  提供全身照  
https://www.crank-in.net/interview/145213/1  

https://x.com/yoireiwa4100/status/1796873949315576160  
埼玉の玉川温泉には  冴羽獠がいるらしい…  女湯を覗いてなければいいが  
在埼玉县的玉川温泉  据说冴羽獠也在那里...  我希望他没有偷看女浴室...  
https://pbs.twimg.com/media/GO_HVaCboAAbAZb?format=jpg&name=900x900

https://x.com/tmn1974/status/1797442642562941364  
先日の展示会で頂いた扇子がなんとトンボ柄(´・ω・｀)  
我在最近一次展览上收到的扇子上的蜻蜓图案真漂亮(´・ω・｀)  
https://pbs.twimg.com/media/GPHMjtpb0AAgfBi?format=jpg&name=4096x4096  


https://x.com/Fuyudutu/status/1795295739998077369  
シティーハンター展に遠すぎていけないけど、喫茶キャッツアイみたいな雰囲気のお店見つけた。テンション上がる〜 #シティーハンター展 #北条司 #喫茶キャッツアイ風  #サイフォン式コーヒー  #喫茶なかやま #熊本県  
去参加城市猎人展太远了，不过我找到了一家像猫眼咖啡馆一样有氛围的店。 我兴奋起来 - #城市猎人展#北条冢#猫眼咖啡店的风格#虹吸式咖啡#中山咖啡店#熊本县
https://pbs.twimg.com/media/GOor8g5a4AAJSUP?format=jpg&name=900x900  
https://pbs.twimg.com/media/GOor8g6aEAAZ0lP?format=jpg&name=medium  
https://pbs.twimg.com/media/GOor8g5bYAEMCN9?format=jpg&name=small  


https://x.com/michanachan/status/1795449602012852502  
原画展いって  久しぶりに読み返しっ  好きでめちゃくちゃ読んでてんなぁ  もちろんっ初版‼️好きな話がいっぱいっ    
我去看了原画展  我很久没有再读这本书了  我非常喜欢读它  当然！初版 ‼️我喜欢的故事太多了！  
https://pbs.twimg.com/media/GOq35Z_XkAA9mVB?format=jpg&name=4096x4096  


https://x.com/sero6953kim/status/1797283268552999393  
보수동 컬렉션 3: 벚꽃이 필 무렵 재삼미디어 핵적판, 만화 시티헌터 시리즈로 유명한 호조 츠카사 (#北条司) 작가님의 단편 만화책입니다! 1994년 발행이니 거의 30년이나 된 귀한 책이네요  
《樱花盛开时》是因漫画《城市猎人》系列而出名的Tsukasa Hojo（#北条司）的短篇漫画作品！它出版于 1994 年，因此已有近 30 年的历史！  
https://pbs.twimg.com/media/GPE7muSboAAvRmz?format=jpg&name=4096x4096  


https://x.com/shizuka8001/status/1796739805168250958  
吉祥寺の @GalleryZenon で開催中の #北条司展 にお邪魔してきました〜🔫✨  やっぱりの安定もっこりオムライス→カレー🍛でカフェも満喫❤️何度見ても新しい喜びがある作品の原画を見ることができるなんてもう喜びしかありません😭✨尊い✨✨✨また通います🐾    
我们参观了位于吉祥寺的 @GalleryZenon 的 北条司展 🔫✨，还在咖啡馆品尝了Mokkori煎蛋饭 → 咖喱🍛 ❤️，能看到无论看多少次都能带来新乐趣的作品原作，真是一件令人开心的事😭✨珍贵✨✨✨，我们还会再去的🐾。     
https://pbs.twimg.com/media/GO9NUqBbAAEgWvx?format=jpg&name=medium  
https://pbs.twimg.com/media/GO9NUqBbAAADUaj?format=jpg&name=medium  

https://x.com/commu434/status/1796748562061210069  
私はこの絵がついてたジャンプ見開きカラーページをキレイに切り抜き、部屋の壁に何年も貼っていたのだよ。そして、一昨日、原画を泣きながら見たさ。なんか、勝手に久々に親戚の子供に会ったみたいな感覚だったわ（笑）   
我把印有这张照片的《跳跃》彩页整齐地剪下来，贴在房间的墙上，一贴就是好几年。 前天，我看着这幅画的原稿哭了。 我觉得自己好像很久以来第一次见到了亲戚的孩子（笑）   
https://pbs.twimg.com/media/GO9VSC3bUAARKwS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO9VSDUboAAfxAJ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO9VSFSaoAAcdAY?format=jpg&name=4096x4096  


https://x.com/commu434/status/1796749531700416743  
鈴木亮平 さんを描いた北条先生のイラストあって！先生は最近目がしんどいみたいなんだけど  鈴木さんの赤Ｔシャツからわかる胸板の厚さが緻密に描かれていて何度も引いて寄ってして見て楽しんだ！鈴木さん嬉しいだろうなー  
有一幅北条老师画的铃木良平的插图！ 最近他的眼睛似乎有点问题，但我很喜欢看这幅画，并把它反复拉回来看，因为从他的红色 T 恤可以看到铃木先生胸前胸牌的厚度，画得非常精确！ 铃木先生一定很高兴！   

https://x.com/pjhhXe2OtP3LjUG/status/1796805739023098244  
撮影OKの場所も多くて  シティーハンターの名シーンだったり  キャッツアイは久しぶりに見たけど  愛ちゃんのお父さんへの強い憧れだったり色々思い出した😌展示の注意書きとか  細かいとこまで凝ってた！  
有很多地方是允许拍摄的，我记住了很多东西，比如《城市猎人》里的著名场景，还有《猫眼》里小艾对父亲的强烈崇拜，我已经很久没有看到了 😌展览说明和其他细节都非常精致！   

https://x.com/pjhhXe2OtP3LjUG/status/1796815676944032157  
感想をノートに書ける  スペースがあったんだけど  他の人の絵が上手すぎて  気が引けたので  伝言板だけ書いておいた！おみやげはキャッツアイの付箋と  シティーハンターの名言キーホルダーと図録✨海坊主のアメ見つからなかった😂  
本子上有写感想的地方，但别人画得太好了，我觉得很别扭，就写了个留言板！ 纪念品是猫眼便签、《城市猎人》名言钥匙扣和一本目录✨，我没找到海坊主的糖果😂  
https://pbs.twimg.com/media/GO-SVJTbMAAi83t?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO-SVQnaUAAk_UC?format=jpg&name=4096x4096  

https://x.com/frog88/status/1796831195579216222  
すごいもの、おもしろいものたくさんありましたが、とりあえずそうですね、こちらのレース見て卒倒しました。  
有很多厉害的、有趣的东西，总之是这样，是的，当我看到这场比赛时，我晕倒了。  
https://pbs.twimg.com/media/GO-gcKobMAAlubr?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO-gcM2bsAA9DN7?format=jpg&name=4096x4096  

https://x.com/hamavha/status/1797059221231661473    
ようやく後期2回目にいってきた  観る度にキャラクターが今にも動き出しそうというか、まるで映画を観ている様な感覚  コラボフードはキャッツとAHを注文  ❤️のグラスがめちゃくちゃ可愛い  
今年下半年，我终于第二次去看了  每次看到这里，人物都好像活了过来，就像在看电影一样！ 我们点了 Cats 和 AH 作为合作食物  来自 ❤️ 的Glass太可爱了！
https://pbs.twimg.com/media/GPBuBtrboAATbMY?format=jpg&name=4096x4096  

https://x.com/3939sakutyan/status/1797237074971787435  
天使の涙の獠ちゃんのパネルのお出迎え😍からのNetflixと原画がおとなりにあったー！うわーい！！  
天使之泪的獠欢迎您来😍。来自Netflix 和原画就在隔壁！哇哦！！  
https://pbs.twimg.com/media/GPERmCTaQAABFU9?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPERmD3bwAAYMQP?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPERmCfaEAEZaQM?format=jpg&name=4096x4096    

https://x.com/3939sakutyan/status/1797238980603175128  
そーいえばこんなシーンあったなぁ。懐かしいー！俊夫のプロポーズは、覚えてなかった。また、よまなきゃな。  
我记得有这么一幕 我错过了 我不记得俊夫的求婚了 我得再读一遍。    
https://pbs.twimg.com/media/GPETU4Qa0AARkgY?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPETU7LaAAABKuH?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPETU-TaoAAdu6b?format=jpg&name=4096x4096  

https://x.com/3939sakutyan/status/1797239510159478979  
先にカフェでオムライスをいただいたので、これができました！  やってみたいよね？  
我先在咖啡馆吃了个煎蛋卷 这样就能做这个了  你想试试吗？  
https://pbs.twimg.com/media/GPETz5WbcAALeQA?format=jpg&name=4096x4096  

https://x.com/3939sakutyan/status/1797240056828289119  
2つの絵を合わせてできてるんですねぇ。知らなかった！  
它是由两张照片拼接而成的。这我倒不知道！  
https://pbs.twimg.com/media/GPEUTmzacAAz4sA?format=jpg&name=4096x4096  

https://x.com/3939sakutyan/status/1797241863910617455  
ファミリーコンポすきー♥️雑司ヶ谷の若苗家にいってみたいとおもったけど、そこは存在しないというか墓地の住所だったはず。たしか。   
Family Compo suki ♥ 我想拜访位于雑司ヶ谷的若苗家，但它并不存在，或者它应该是墓地的地址。我想。  
https://pbs.twimg.com/media/GPEV8e9aMAAJHzE?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPEV8j9bAAEh4nx?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPEV8rLbsAE2Sgm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPEV86AbgAAx784?format=jpg&name=4096x4096  


https://x.com/3939sakutyan/status/1797242675185512692  
2階がたまたま誰もいなくなったー！   
二楼正好是空的！  
https://pbs.twimg.com/media/GPEWsE6bQAAf-XQ?format=jpg&name=4096x4096  

https://x.com/pinktanpopo34/status/1797282991158550848  
北条司展 で買ったグッズ、クリアカードバインダーに百均の普通サイズカードファイル足して入場特典カードやもっこり旗を収納👍  
在北条司展上购买的物品：从百元店购买的透明卡片夹和普通大小的卡片文件，用于存放入场奖励卡和 Mokkori 旗帜👍  
https://pbs.twimg.com/media/GPE7WdhaUAAD287?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPE7WdhbQAAFixO?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPE7WdibIAAsZw3?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPE7Wdha4AAtSFG?format=jpg&name=4096x4096  

https://x.com/pinktanpopo34/status/1797284457831166251  
クリアカードフォルダは劇場版シティーハンター天使の涙の入場特典にピッタリでした☺️  
透明卡片夹是《城市猎人电影：天使泪》的最佳入场特典☺️ 
https://pbs.twimg.com/media/GPE8sARaAAAjzfS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPE8sAObgAAh_ek?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPE8sAPa0AA2M95?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPE8sARacAAh-jS?format=jpg&name=4096x4096  

https://x.com/ciitan126/status/1797291582095585374  
ギャラリーゼノン の #北条司展 でゲットしたもの👍(2枚目の写真、娘と行ったので、オムライスの旗も特典も2つずつあります😁) こんなに買うとは(以下同文w  
这是在 Zenon Gallery的 #北条司展上买的👍（第二张照片，我和女儿一起去的，所以有两个煎蛋旗和两个特价😁）。我不知道自己会买这么多（下图同一句w）
https://pbs.twimg.com/media/GPFDKTibgAAUgnT?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GPFDKqTbIAEwd2l?format=jpg&name=4096x4096  

https://x.com/ari27spider/status/1796408035713405018  
ねえもうさこれ以上好きにさせないで欲しいまじで綺麗な足に綺麗なモッコリポーズにこの笑顔死ぬやんけ亮平ちゃんに会いたい絶対泣く自信しかないんやけどまじでやばい(語彙力どこ)  
https://pbs.twimg.com/media/GO4flhTaoAAKctG?format=jpg&name=4096x4096  

https://x.com/oda08118004/status/1796473944196989171  
キャッツアイ の放送日でも #シティーハンター 投稿172  最近「おじゃる丸」で怪盗三姉妹アイキャッツ登場！親から「昔 #キャッツ・アイ っていう漫画があってね…」など子供と会話したら嬉しいですね   墓場の画廊の展示は銀狐の回、アニメだと山寺宏一さんのイケボとコミカルなリアクション満載です  
即使在《猫眼》播出之日，#CityHunter 提交 172。 怪盗三姐妹猫眼最近出现在「おじゃる丸」中！ 家长们跟孩子们聊起这个话题时，就会说 “以前有个漫画叫#猫眼#......”，真好。  墓地画廊的展览是银狐的插曲，动画里山寺宏一的声音很好看，反应也很滑稽！  
https://pbs.twimg.com/media/GO5bb6-asAALDEK?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO5bb7DbYAAuq7i?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO5bb7AbcAERhGP?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GO5bb7HbYAIfy6r?format=jpg&name=4096x4096  

https://x.com/pinktanpopo34/status/1796554388854755556  
北条司展 後期に行って来ました⸜(*ˊᗜˋ*)⸝エントランス？も後期展示に合わせて変わり、等身大（多分）の獠がお出迎え💕カフェで熊本国際漫画祭メインビジュアル作成の動画を繰り返し見て幸せ☺️本当に細かくて美しくて何回見てもため息が出る💕また来たい❣️  
我去看了北条司展 後期⸜(*ˊᗜˋ*)⸝入口？ 後期展览的入口也换了，一个（也许是）真人大小的“獠 ”在那里迎接你💕在咖啡厅里反复观看熊本国际漫画节主要视觉创作的视频，我很开心。☺️，真的很细致、很美，无论看多少遍💕，我都会感叹，我还想再来一次❣️  
https://pbs.twimg.com/media/GO6ksYlaEAMqKB0?format=jpg&name=small  


https://x.com/ryosaeba357xyz1/status/1796188467015131198  
https://pbs.twimg.com/media/GO1X4MhW0AAFMZH?format=jpg&name=4096x4096  

https://x.com/5Rwph0EEiX83793/status/1796327088959762677  
https://pbs.twimg.com/media/GO3V9yUa8AACJeI?format=jpg&name=4096x4096  

https://x.com/vex1FXHl1u4671/status/1796081719298474461  
アニマックでシティハンター特集するんでみよう👍  北条先生の版画展近畿でして欲しいなぁ❣️綺麗なイラストで好き❤️エンジェルハートは終わったんかな⁈  
让我们一起看动画片《城市猎人》👍我希望北条老师的版画展能在近畿举行。❣️ 我喜欢他精美的插图❤我想知道《天使心》是否已经完结⁈  
https://pbs.twimg.com/media/GOz2yVmbwAAsVd-?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOz2ypaaIAAApWa?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOz2y9abgAAHrYT?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOz2zT9bAAA4NMI?format=jpg&name=4096x4096  

https://x.com/GalleryZenon/status/1796074974949556467  
大変お待たせ致しました！  シティーハンタートートバッグ入荷しました。是非ごご来店お待ちしております。  
感谢您的耐心等待！City Hunter 手提包现已有货。 我们期待您光临本店。  
https://pbs.twimg.com/media/GOzwqyJbcAA1Cp2?format=jpg&name=900x900  
https://pbs.twimg.com/media/GOzwqyIbUAATCK0?format=jpg&name=900x900  

https://x.com/ats0027/status/1795633450390884429  
https://pbs.twimg.com/media/GOtfBpYbkAES4RF?format=jpg&name=medium  

https://x.com/taiki_e/status/1795663738781180350  
ギャラリーZenonにて『北条司展』を観てきました。北条先生の流麗な原画はもちろんですが、こういう展示ではラフスケッチなどにいつも目が行きます。作家さんの体温というか創作の熱量を感じて刺激をいつも受けています。  

https://x.com/5Rwph0EEiX83793/status/1795668483185025077  
もっとシティーハンターの原画見たかったなぁ…ソニアとかマリィとかミックとか。またシティーハンターの原画展はあるのでは？と思ってしまうチョイスだった。  
我希望能看到更多城市猎人的原创画作......比如索尼娅、玛丽和米克的作品。难道不应该再举办一次《城市猎人》原创画展吗？ 这是一个让我思考的选择。  
https://pbs.twimg.com/media/GOt-91fa0AA1ZLO?format=jpg&name=4096x4096  

https://x.com/ats0027/status/1795670202971242662  
記帳してきた♪  
https://pbs.twimg.com/media/GOuAhrSbAAESQH5?format=jpg&name=4096x4096  

https://x.com/5Rwph0EEiX83793/status/1795707747369107638   
ＡＨは観たくない…けどＣＨや他の原画は観たいという方へ  ギャラリーのプロジェクターのある奥の部屋にＡＨはあるので、カラスとトンボの壁を背にしたら、そこから右奥に行かずに２階へ向かうのがよいかと思います👍    
不想看AH...... 但对于那些想看CH和其他原画的人来说，AH在画廊有投影仪的后面房间里，所以如果你背对着乌鸦和蜻蜓的墙，我认为👍最好去二楼，而不是从那里去后面   

https://x.com/5Rwph0EEiX83793/status/1795741122280702148  
https://pbs.twimg.com/media/GOvBCNwa4AE1RPi?format=jpg&name=4096x4096  

https://x.com/makkylucky/status/1795765314787111352  
https://pbs.twimg.com/media/GOvXB9Wa8AAvBHm?format=jpg&name=4096x4096  

https://x.com/chibi_aya_728/status/1795793997786431986  
https://pbs.twimg.com/media/GOvxHwvbEAAjNx4?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOvxH6CbEAA8K0k?format=jpg&name=4096x4096  

https://x.com/seidoku/status/1795825180587303314  
「北条司展」（シティーハンターの作者）でイラスト制作の動画を見て感動しました。 https://seidoku.jp/hojo-2024/ 北条司展に行った時の様子や感想を書いています。 #北条司展 #シティーハンター #吉祥寺  
「北条司展」（城市猎人的作者）上的插图作品视频给我留下了深刻印象。https://seidoku.jp/hojo-2024/ 我写下了我参观北条司展的感受。  


https://www.instagram.com/p/C7X4s7FSnT5/?img_index=3  
https://www.instagram.com/p/C7X4s7FSnT5/?img_index=4  
https://www.instagram.com/p/C7ZNocvPV6P/?img_index=5  

https://x.com/michoumi_ch/status/1795112864463605856  
40周年の締めくくりにフランスのジャパンエキスポってイベントに来たらいいんじゃない？シティーハンターも大ヒットしたしフランスではニッキーという名で人気やし過去にはTRFも来たよだから今年の夏のはじめにどうぞ来なくてどうするの絶対来てぇぇぇ呼んでぇぇ！！  
为什么不来法国的日本博览会庆祝 40 周年呢？ 《城市猎人》很火，Nicky这个名字在法国也很受欢迎，TRF 过去也来过这里，为什么不在今年夏初来呢？  

https://x.com/KaoriM_FR/status/1795118426085634055  
Voici un petit aperçu de quelques unes des illustrations de l'artbook des 20 ans de carrière du maître Hojo. 😊  
下面是这本艺术画册中的部分插图的预览，这本画册是为了庆祝北条大师 20 年的艺术生涯。😊  
https://pbs.twimg.com/media/GOmKsJvXAAE0voW?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOmKsKtWUAAPf7J?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOmKsL4WoAAOIlb?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOmKsQGXUAA7vTd?format=jpg&name=4096x4096  

https://x.com/gravvesends/status/1795236455096226182  
More sanrio style ryo and kaori animals + crow  #シティーハンター📷 #CITYHUNTER  


https://x.com/yLA4V1Khmm10795/status/1795239391797141508  
冴羽 獠を探せ(笑)  
帮我找冴羽獠（笑）  
https://pbs.twimg.com/media/GOn4tUvbIAAkPYR?format=jpg&name=large  

https://x.com/nadesiko_soft/status/1795154193969193432  
綺麗な原稿だ^_^  シティーハンター推しの中、キャッツアイ原稿に集中する  
https://pbs.twimg.com/media/GOmrOQbaAAEX9VQ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOmrOQcaAAA4Ex9?format=jpg&name=4096x4096  

https://x.com/XV750_235_1/status/1794999413771051364  
北条司展素敵すぎた🥹書き下ろしイラストや原画はどれも素敵だしコラボメニュー美味しくて可愛いかった🥰グッズもたくさん買えて特典もシークレット当たって幸!!💞獠ちゃんかっこよかった(⑉• •⑉)❤︎    
北条司展太好了🥹新画的插图和原画都很精彩，合作菜单既美味又可爱🥰，我买了很多好吃的，还赢得了秘密奖金！ 💞我很高兴赢得了秘密奖励 ⑉- -⑉ ❤︎  
https://pbs.twimg.com/media/GOkeaHjaoAAgugF?format=jpg&name=4096x4096  

https://x.com/rabbit77625/status/1794601980540985556  
北条司展　の後期へ AngelHeartのグッズが完売しちゃった  何も購入できなかった😂前回二階があることに気づいてなかったので、今日は無事に2階の展示も見た！ラストは特典ARを回収。前回はオムライス  今回はカレー  
北条司展的後期 AngelHeart 的商品已经售罄！ 什么都没买到😂 上次没注意到还有楼上的展览，所以今天顺利看完了楼上的展览！ 最后，我领取了 AR 福利。 上次是蛋包饭，这次是咖喱饭。  
https://pbs.twimg.com/media/GOe0_QKaMAEjUWu?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOe0_QBagAAhpsP?format=jpg&name=4096x4096  

https://x.com/kinakonose/status/1794637415573024845  
後期。ADの麗しい獠ちゃんがお出迎え！本当に先生の絵はずっと見ていられる。海原パパ背景の獠ちゃん、目や髪の毛が細かくて細かくて何の画材なんでしょう！AH香瑩のレースのキワの細かさったら！本当に細部まで見放題。写真撮り放題で、今回も良い時間を過ごせました。  
后期AD的美丽獠酱迎接你!老师的画真的可以一直看下去。海原爸爸背景的獠酱，眼睛和头发细小细小的是什么画材呢!AH香莹的蕾丝的时候的细小!真的连细节都可以随便看。因为可以随便拍照，这次也度过了美好的时光。
https://pbs.twimg.com/media/GOfVN86bYAAH8nC?format=jpg&name=4096x4096  

https://x.com/senkouchoco0802/status/1794652724317098128  
行って参りました。北条司展　後期  やっぱり、時間指定だから  ゆっくり鑑賞できて  眼福×2でごさいました。欲しかったトートバッグも  買えて大満足です。🤗    
我去了。北条司展后期果然，因为是指定时间，所以能慢慢欣赏真是一饱眼福。买了想要的ToteBag非常满足。🤗
https://pbs.twimg.com/media/GOfjJAvawAEJDvd?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOfjJC4aYAAVZFK?format=jpg&name=4096x4096  

https://x.com/mifu0009ddd2/status/1794731234142384266  
キャンペーンありがとうございます。前期のは当たらなかったのかな？と思うので今回は当たったらと妄想してます( ˊᵕˋ )💭エンジェル・ハートの原画をお目にかかれました( ⌯ﾉㅿﾉ)何度も何度もまじまじと凝視してしまいました💦  
谢谢活动。前期的没中吗?所以我觉得这次是正确的话和妄想(ˊᵕˋ)💭Glass Heart的原画,见到你了(⌯ﾉㅿﾉ)多次也目不转睛地凝视了💦  
https://pbs.twimg.com/media/GOgqiuHaAAALkfG?format=jpg&name=4096x4096  


https://www.instagram.com/p/C7ZNa-0vXHR/?img_index=5  
https://www.instagram.com/p/C7ZNa-0vXHR/?img_index=6  
https://www.instagram.com/p/C7ZNa-0vXHR/?img_index=7  
https://www.instagram.com/p/C7ZNa-0vXHR/?img_index=8  

https://www.instagram.com/p/C6SaAlVy7hn/  
CITY HUNTERもCAT'S EYEも  夏休み朝のこども劇場とかで見てたなぁくらいの知識量やけど   やっぱり生の原画は素敵だぁ   てか細かっすごっ！な近距離でまじまじとみれたのしあわせ。黒ベタも印刷された黒とはまた違う塗りたくられた黒。ぜんぶ手描き。作者さんの描いてるときの映像も流れてて、ペン入れして瞳に命宿ってく感じとか。筆で薄い色味から何度も何度も色を重ねてって立体感が生まれてく感じとか。かっこよ。  体のラインのむちむち感、筋肉の感じとか、服のひっぱられたり余ったりする感じとか。夜の街の光とか。グラスのモノクロ、カラーのコマ、デジタルちゃうから塗り分け作ってコピペして色つけれるとかやないんやもんな。どんな技がぁあああふぁー！同トレス職人技。わからんすぎてすごい。ホワイトの立体感に生感じるぅ。画用紙の感じとか。素材の厚み。編集さん？の赤文字とか青文字とか意味さっぱりやけど、セリフの印刷の詰め方とか、一律貼りじゃなくて1文字ずつ、カットしてってどんな意味あるんやろ〜とか。天地縮小ってなになになになに( ´ ω ` )とか。原稿かっこよ。ルビにもこだわりっ！髪の毛のふわふわさに。女性キャラの瞳に唇のキラキラさと、鼻筋のキリリさと線細か〜！ハイライトとか、髪の毛1本に対しての顔にできる影に、服のシワに素材感に、アタリの凹みに、模様のシート(トーンやっけ)に、生ーーーーすごーーーーー。描いて塗ってくところも見れたの嬉しい。すげぇ  印刷でどう写るんかとか、どう効果が生まれるんか  感覚でわかるんかな。そこから原画の元に合わせてつくられて調整されてくのもすごいなぁ。それがみんなの目に届くんやもんなぁ。ほかにも知らない作品もたくさんあって、こんなに作品つくってはったんや。って思って読んでみたいな( ´ ω ` )思ったり。昔のジャンプってこんなんやったんや。とか、立体モニュメント？に、ネームの一部とか、アクスタとか食器とかのグッズに、色んな展示があってむちゃくちゃ楽しかった( ´ ω ` )✨行けてよかった  チケット制でwebからも取れるし、余裕があれば当日も会場で買えるみたい。ぐるぐる好きなように回れるからとても良き( ´ ω ` )  もっかい観たいとか、人落ち着いてきたとこ覗きに行ったり。あんなに間近に観られるなんて最高だぁああああ。。。写真たくさん撮れて嬉しみ。でもやっぱり目からの情報は目からしか感じられないって思う( ´ ω ` )すてき。原画たくさんみれたの嬉しかった。やっぱり命やどるよね。必然。素敵な展示会行けてよかった( ´ ω ` )  ネトフリの鈴木亮平さんのCITY HUNTER映像も流れてとって、やっぱりおもろそう〜みたい〜ポスターにサインに、作者さんの同じポーズのイラストもあった  漫画家さん、ほんま芸術家やとおもう。  



https://www.instagram.com/p/C655HpShRcb/?img_index=4  
北条司展 😍一部をお裾分け📷前期は今週5/19まで！後期は5/22-6/23 ＠吉祥寺 だよ💫ちなみに私はCITY HUNTERも勿論大好きなんだけど、キャッツアイの瞳にめちゃくちゃ憧れて  予告状の代わりにトランプを  かっこよく投げたかった人です🃏笑  
北条司展 😍的一部分 📷前期于本周 5 月 19 日结束！ 後期从 5 月 22 日至 6 月 23 日在吉祥寺举办💫顺便说一句，我当然喜欢《城市猎人》、我非常喜欢猫眼！
用扑克牌代替预告信  一般想投人🃏笑的  


https://www.instagram.com/p/C7JcH0eve0p/?img_index=5  
https://scontent-cdg4-2.cdninstagram.com/v/t51.29350-15/445141275_365824903134357_8880821983570487475_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0Mzkuc2RyLmYyOTM1MCJ9&_nc_ht=scontent-cdg4-2.cdninstagram.com&_nc_cat=109&_nc_ohc=4VPFagSMm7sQ7kNvgEASTOm&edm=AGyKU4gBAAAA&ccb=7-5&ig_cache_key=MzM3MTM0OTQ3MTExMDU2OTgyOQ%3D%3D.2-ccb7-5&oh=00_AYDHmLQeR8z6ZKRZB1hn2Bqlth_i_sKLsrKY0UVOhihGLg&oe=66587416&_nc_sid=2011ad  

https://www.instagram.com/p/C544lg9S6Cn/?img_index=3  

https://www.instagram.com/p/C544lg9S6Cn/?img_index=2  
北条司展 行って参った  原画展行くと、漫画家さんの凄さが  身に染みる〜🥹👏  
https://scontent-cdg4-3.cdninstagram.com/v/t51.29350-15/439175109_963174088243816_7910888953370963938_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE4MDAuc2RyLmYyOTM1MCJ9&_nc_ht=scontent-cdg4-3.cdninstagram.com&_nc_cat=104&_nc_ohc=H8pu3j2e_YEQ7kNvgEZhM-w&edm=AGyKU4gBAAAA&ccb=7-5&ig_cache_key=MzM0ODY3NTE4Mjk2NTAxMDEzMg%3D%3D.2-ccb7-5&oh=00_AYBcOY_QCofbhRNLkaN3vr5Phr4TIjH3pVj4Rlm3PPL8Bw&oe=66586FA0&_nc_sid=2011ad  

https://www.instagram.com/p/C6Bl0NaPni1/?img_index=6  

https://www.instagram.com/p/C6Lt0c-xjKE/?img_index=4  
【北条司展 The road to 『CITY HUNTER』40th anniversary 2025 〜】北条司展に行って来ました！会場は新しくできた「ギャラリーゼノン」。漫画出版社が運営するギャラリーとのことで、今後のイベントも楽しみです。北条司作品と言えば、「シティーハンター」や「キャッツアイ」が有名ですが、小学生の頃に読んでいた「こもれ陽の下で…」も好きでした。グラスの中で揺れるドリンク、雨を受けての波紋、クロスフィルターをつけて撮影したかのようなキラキラした逆光の表現。どれも凄すぎてエグいって(´∀｀)Netflixシティーハンターはこれから見ます。  
【北条司展 The road to 『CITY HUNTER』40th anniversary 2025 〜】我去看了北条司展！ 展览地点是新建的 Zenon Gallery。 画廊是由一家漫画出版社经营的，我期待着今后的活动。 说起北条司的作品，《城市猎人》和《猫眼》是大家耳熟能详的，但我也很喜欢小学时读过的《阳光少女》。 杯中摇曳的饮料，雨中荡漾的涟漪，闪闪发光的逆光，仿佛是用十字滤镜拍摄的。 都太赞了......我现在要去看网红《城市猎人》了。 

https://www.instagram.com/p/C69BariyJ6U/?img_index=5  
後期も行けたら行きます  
如果可以的话，後期我还会去的  

https://www.instagram.com/p/C7ReB0iStXo/  
このイラストのグッズ希望です💁  明後日はまた  #北条司展  お世話になります🙇    
我希望能用这幅插图买到一些商品 💁 后天我将回来参加 #北条司展 🙇   

https://www.instagram.com/p/C6JTpWlPhej/?img_index=4  
今日は北条司展とトキワ荘に行って来ました。泣けてくるほど素敵だった。当時シティハンターを読んで冴羽獠の様な男になりたいと強く憧れたが、たまにモッコリするただのおじさんになってしまった。どこで間違えたのか⋯。山本高樹さんというジオラマ作家さんを今日初めて知り、衝撃を受けました。めちゃくちゃ凄かった。まだまだ世の中にはとんでもねぇ人がいるもんですね。漫画家を含め、職人さんってやっぱり最高だ。  
今天我去看了北条司展和常盘庄。 那感觉好极了，让我流下了眼泪。 那时，我读《城市猎人》，强烈渴望成为像冴羽獠那样的男人，但我却成了一个时不时嘲笑别人的大叔。 我到底哪里做错了......？ 今天是我第一次认识立体模型艺术家山本高樹先生，我很震惊。 他太了不起了。 世界上还是有一些了不起的人的。 手艺人，包括漫画家，都是最棒的。   


https://x.com/retrofuture_08/status/1794369215396565020  
Netflixで #映画シティーハンター 観て、原作に興味持った方！今なら槇村の最期のシーン(亮平さんが濡れる事にこだわった所)や、ラストシーンで映った槇村兄妹のツーショット写真の元ネタの超絶美しい原画が拝めます！！吉祥寺の GALLERYZENONで北条司展やってますのでぜひ💪  
如果您在 Netflix 观看了 #CityHunter 并对原版故事感兴趣！现在您可以看到 槇村 的最后一幕（亮平坚持要淋湿），以及 槇村兄妹们在最后一幕的两张照片。您可以看到原版故事的精美原画！北条司展正在吉祥寺的 GALLERYZENON 举办，敬请光临💪  
https://pbs.twimg.com/media/GObhSqvaMAAO4V4?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GObhSsBbYAACjgI?format=jpg&name=4096x4096  

https://x.com/t04211025y32/status/1794304877147152417  
北条司展後半、訪問しました!懲りずにブラインド購入しましたが、被ったのは1つだけ💕シークレットも貰え、満足です。  
我参观了下半场的北条刚展览！我没有吸取教训就盲目地买了，但只买到了一件💕我还得到了一个秘密，很满意。  
https://pbs.twimg.com/media/GOamxomagAA7xlH?format=jpg&name=4096x4096  

https://x.com/yLA4V1Khmm10795/status/1794130439009161679  
シティーハンター 待たせたな💖今日は最高のメンバーでギャラリーゼノンと吉祥寺を満喫予定💪香👕着て大きな紙袋を持っていたら私なので、お気軽にお声がけ下さいね🎶  
https://pbs.twimg.com/media/GOYIID5aYAAX1-o?format=jpg&name=4096x4096  

https://x.com/chiroru_special/status/1794148273281995067  
誕生日✨朝から仕事だけど、午後はシティーハンター愛に溢れた時間を過ごしたくて、GALLERY ZENONに行く！！🥰1年前の自分はこんなにシティーハンターにハマるとは思わなかったです😅今はなくてはならない存在です❤️今年もシティーハンター愛に溢れた1年になりますように✨  
生日✨ 我上午要工作，但下午要去 GALLERY ZENON 度过一个充满《城市猎人》之爱的下午！ 🥰 一年前，我没想到自己会如此沉迷于《城市猎人》😅 现在我已经不可或缺了❤️ 祝愿又一年充满《城市猎人》的爱✨  
https://pbs.twimg.com/media/GOYYVuubgAExPge?format=jpg&name=4096x4096  

https://x.com/challengespirit/status/1794140490679648537  
週刊少年ジャンプ #集英社 「地上げ」!? シャッター通り 地元住民“恨み節”のワケ  #MAG2NEWS https://mag2.com/p/news/599757   先生方へ 住民の方々を助けてあげて   
周刊少年Jump #集英社 “抢地”？被关闭的街道，当地居民为何 “苦不堪言” #MAG2NEWS https://mag2.com/p/news/599757 亲爱的老师们，请帮助居民    

https://x.com/aoi_tuno/status/1793853130901533156  
北条司展楽しかった〜‼️ #北条司展　#GALLERYZENON インタビューもあって最っっ高‼️‼️  
我很喜欢北条刚的展览~‼️ #北条司展　#GALLERYZENON  还有一个访谈，非常棒‼️‼️  

https://x.com/Welsh_Corgi_18/status/1793909157181169906  
うわああああぁぁぁ！！！！かっこい！！！額装して飾りたい！！！！息子の名前を決める時に北条司先生の名前から｢条と司｣の字をコソッと拝借して｢条司｣に決めました！  
哇！！！ ！！！！太酷了  我想把它裱起来展示！！！！在给儿子取名字的时候，我偷偷借用了北条司老师名字中的｢条和司｣字，取名为 ｢条司｣！  

https://x.com/yfdm0127abc/status/1794051020726956137  
昨日行ってきました🫶  欲しいものは全部買えました！伝言板にも書けました！前後期どちらも行けて幸せです♥️獠パネルとツーショしたかったのですがスタッフさんにお声がけする勇気が出ず撮れませんでした💦 また行きたい\(*ˊᗜˋ*)/  ご縁がありますよーに(꒪˙꒳˙꒪ )  
我昨天去的🫶  我想买的东西都买到了！ 我可以在留言板上写留言了！我很高兴能去前后两个时段：♥️我想和獠合影，但我没有勇气问工作人员💦。我还想再去一次。希望有机会见到您！  
https://pbs.twimg.com/media/GOW_5MWaMAAkeHv?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOW_1ZHaIAAIHHR?format=jpg&name=4096x4096  

https://x.com/yfdm0127abc/status/1793550201992720739  
後期開催ありがとうございます🫶  本日行かせていただきました〜XYZのプレートを購入できました！そして前回埋まっていて書けなかった伝言板にも書けて嬉しいです(﹡ˆᴗˆ﹡)ご縁がありますよーに(꒪˙꒳˙꒪ )  
感谢你们举办後期的活动🫶，我今天去了--我买到了 XYZ 牌！ 我很高兴能在留言板上写留言，上次留言板被占满了，我不能在上面写（﹡ˆᴗˆ﹡），希望我们能一起度过美好的时光（꒪˙˙꒪）   

https://x.com/00_byron00/status/1793618839269515311  
〜北条司 展〜  
https://pbs.twimg.com/media/GOQ20VZbcAAyesi?format=jpg&name=small  
https://pbs.twimg.com/media/GOQ20VXaQAA2Fwk?format=jpg&name=small  


https://x.com/GalleryZenon/status/1793093656389578965  
大変お待たせ致しました！複製原稿「ネコまんまおかわり♡」「SPLASH!3」、シティーハンタークリアファイルセット入荷しました。  
感谢您的耐心等待！ 複製原稿 "白猫少女"、"SPLASH!3 "和 "城市猎人 "透明文件套装现已有货。  
https://pbs.twimg.com/media/GOJZLNZaYAEA_o8?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOJZLNXasAAGB8P?format=jpg&name=4096x4096  

https://x.com/hamavha/status/1793450842403868949  
後期早速行ってきました  個人的に楽しみにしていたエンジェルハートの鮮やかなカラー原画に感動  ADの獠ちゃんの生原画も観れたのはとても嬉しくて、SNSで初めて公開された時の高ぶりが一気に蘇りました  オムカレーとても美味しかった😋  
我是後期首日的。 我很高兴看到了《天使心》鲜艳的彩色原画，这是我个人非常期待看到的。 我也很高兴看到了AD之獠的现场原画，这让我回想起了第一次在SNS上播放时的激动心情。    

https://x.com/xyz_mizu/status/1793455209378926887  
後期はエンジェル・ハートの展示がたくさんありました。絵の美しさは言わずもがな。A·Hは否定的なファンもいるかもだけど、これはこれで私は嫌いじゃないです。泣けるのよとにかく。C·Hも泣けるんだけど、こっちのが毎話泣いてた気がするw  
展览的后半部分有很多《天使心》的展览。不用我多说，你也知道这些照片有多美。A-H 可能有一些负面粉丝，但我并不讨厌这部作品。 反正它能让我流泪。C-H 也会让我流泪，但我认为这一部每集都会让我流泪。 
https://pbs.twimg.com/media/GOOh88fa0AA4idt?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOOh-U4asAAY00C?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOOh_vsbYAAseyN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOOiAe4bQAAkeUQ?format=jpg&name=4096x4096  

https://x.com/jackiechan_jp/status/1793538527973834864  
GALLERY ZENON 【北条司展】  メチャクチャ最高ダタ💕  
GALLERY ZENON 【北条司展】  Mecha Kucha 最佳数据  
https://pbs.twimg.com/media/GOPtvcJaYAAoSkJ?format=jpg&name=small  
https://pbs.twimg.com/media/GOPtvcKbQAAKt2x?format=jpg&name=4096x4096  

https://x.com/coamix_sales/status/1793541118531088585  
6/23(日)まで開催❣／『#北条司展』in GALLERY ZENON後期スタートしました🌈☀️＼前期とは展示内容、入場者特典  コラボメニューが変わっています！✨なんと！冴羽獠のパネルまで😍😍皆さまのご来場お待ちしております！  
展览将持续到6月23日（周日）❣/ 『#北条司展』in GALLERY ZENON後期已经开始🌈☀️＼展览内容和入场奖励合作菜单与上半场相比有所变化！ ✨真没想到！ 包括冴羽獠的展板😍😍我们也期待您的到来！   
https://pbs.twimg.com/media/GOPwJEMaQAAjbNT?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOPumY1aAAAuCt6?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOPuqqMboAAGHtW?format=jpg&name=4096x4096  

https://x.com/chibi_aya_728/status/1793593981768786366   
本日も #北条司展 へ。前期のヴィジュアルはカフェに☺️今日は夕方の空いてる時間にカフェにお邪魔したので撮れました😌ギャラリー内で観られる北条先生の制作の様子の動画は、後半部分の着色のところが少し追加されていて、さらに素敵〜✨何度観ても「すごいなぁ」ってため息ばかりが出ます💕    
今天我们又去看了 #北条司展。咖啡馆里有前期的作品 ☺️今天我利用晚上的空闲时间去了咖啡馆，所以能拍到这些😌您可以在画廊里观看北条先生的创作的情景的视频，后半部分多加了一点色彩✨，效果更好～无论看多少遍，都只能感叹“太神奇了”💕  
https://pbs.twimg.com/media/GOQgNcyaQAA34b4?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOQgNpVbsAAPTG8?format=jpg&name=small  
https://pbs.twimg.com/media/GOQgNrrbYAAZAwi?format=jpg&name=4096x4096  

https://x.com/memewithusagi/status/1793594263105876116  
行ってきました！はぁ〜もう、ここに住みたい！感動して涙しそうになったのを堪えました🥹 グッズは買えたけど、カフェは時間がなくて行けなかった😢来月にリベンジ🌋予約済👍  まだ行けてない方もいると思うので、画像はこの辺で✨    
https://pbs.twimg.com/media/GOQgeZxbEAAjn-x?format=jpg&name=4096x4096  

https://x.com/chibi_aya_728/status/1793598698230022450  
反射が激しいけど、 #映画シティーハンター のそれぞれのお写真。ギャラリー内に展示してあるポスターには四人の方々のサインが入ってるので、是非ご覧になってください💕  
尽管反光很严重，但这是每个人在#电影城市猎人#中的照片。画廊里展示的海报上有四个人的签名💕。  

https://x.com/jackiechan_jp/status/1793619359237296406  
GALLERY ZENON  　　【北条司展】  「伝言板」  ッデ依頼(？)書イデキダ🖊️  明日マデ残デナイカナ❓2階ッノ先生ヘノ“メッセージ”ッモ書イダヨォ✍️先生ッニ届ゲッ‼️  
GALLERY ZENON　　  【北条司展】  「伝言板」请求 写在 🖊️明天之前没有留言❓我还给二楼的老师写了一封信。✍️我会发给老师的。‼️  
https://pbs.twimg.com/media/GOQ3TPtaEAAY9_w?format=jpg&name=4096x4096  

https://x.com/AYAS33326641/status/1793677383431856500  
北条司展も凄かった😍✨先生の画力に表現力、とにかく全てが美しくて✨毎日見ていたいくらい素敵❗期間限定なんてもったいないから、是非とも北条司ミュージアムを造ってほしい(笑)ちなみに、スプラッシュ3のこの紫陽花のような色合いが好き  
北条司展太棒了😍✨他的绘画能力和表现力，一切都那么美✨❗，我真想每天都能看到它❗❗可惜时间有限，真希望你们能建一个北条司博物馆（笑）顺便说一句，我很喜欢 Splash 3 这种绣球花般的色调！  
https://pbs.twimg.com/media/GORsERPakAA13us?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GORsEXyaEAEJL08?format=jpg&name=4096x4096  

https://x.com/5Rwph0EEiX83793/status/1793772797304697154  
おはようございます☀  あと数日頑張ったら北条司展に行ける…  獠に会える…  そう思って、今日も頑張りますっ👍✨  
早上好*  如果你再努力几天 你就能去参加北条司展览... 你可以见到獠.....   我也是这么想的 我今天会努力的👍✨  


https://x.com/suiho_u/status/1793195729688707360  
親友と一緒にシティーハンターのPOPUPStoreに来た！  
我和我最好的朋友一起来到城市猎人 POPUPStore！   
https://pbs.twimg.com/media/GOK2AajbcAAU0uv?format=jpg&name=medium  

https://x.com/Nabe04148095/status/1793222379470266375  
原画展後期初日、行って来ました。買って、食べて、見て、午後ずっといました。しずかさん、ありがとう。楽しかったです。外国人の方も結構来ていました。  
我是在原画展後期首日去的。 我整个下午都在那里，买东西、吃东西、看东西。 谢谢你，香。 很开心。 有很多外国游客。  
https://pbs.twimg.com/media/GOLOBn6bMAA8-3g?format=jpg&name=small  
https://pbs.twimg.com/media/GOLOBn5bEAEqcif?format=jpg&name=900x900  
https://pbs.twimg.com/media/GOLOBn4aAAA91RA?format=jpg&name=900x900  
https://pbs.twimg.com/media/GOLOBn6bIAAQfc_?format=jpg&name=900x900  

https://x.com/saerikaduffy/status/1793241494805246320  
ギャラリーはやっぱり遠いなぁ😆  往復６時間超。 後期の原画は家族って感じが多かった😊  北条先生のブレンドを初体験。コーヒー豆🫘でも買えるといいなと、リクエストしました😘  #シティーハンター  #ギャラリーゼノン  #北条司原画展  
画廊还很远，😆往返需要6个多小时。 后半部分的原画有很多家庭的感觉，这是😊我第一次体验北条老师的混合。 我希望我也能买到咖啡豆🫘，所以我提出了一个😘要求#シティーハンター #ギャラリーゼノン #北条司原画展     

https://x.com/taka2380038/status/1793261486821024248  
北条司展・後期行ってきた🎵𓈒𓏸  めっちゃカッコイイ獠ちゃんに出会えた…❤  この冴羽獠めっちゃスキ❤  お皿、買っちゃったよねぇ(*´ ˘ `*)ｳﾌﾌ♡  今日はキャッツ多めでした  …また行こっと🎵𓈒𓏸  
https://pbs.twimg.com/media/GOLxze9bwAAjaPs?format=jpg&name=medium  
https://pbs.twimg.com/media/GOLx0FZaYAAz3lU?format=jpg&name=medium  
https://pbs.twimg.com/media/GOLx0WabkAAWNqq?format=jpg&name=900x900  

https://x.com/chiroru_special/status/1793274440773455949  
シティーハンター漫画全巻BOXの箱が良すぎる！！！名シーンだらけ🥰  単品購入できるから、箱だけもう一箱買おうかなー😆💕  
城市猎人漫画全集的包装盒太好了！ 满满的都是精彩画面🥰  你可以单独购买，我会为了这个盒子再买一盒😆💕  
https://pbs.twimg.com/media/GOL9l92akAAp6TT?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOL9l8PaYAABePL?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOL9mFoaQAA-j-x?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOL9mUHaYAAxtsZ?format=jpg&name=4096x4096  


https://x.com/chiroru_special/status/1793275962177765724  
キャー！！アクリルコースター最高！！😆💕  
卡 亚克力杯垫是最棒的！ 😆💕  
https://pbs.twimg.com/media/GOL---4a0AAo2qQ?format=jpg&name=4096x4096  


https://x.com/0824chieko/status/1793391071747305864  
久しぶりに投稿  シティーハンターが公開になってから毎日最低二回以上見てる  シティーハンター依存症の71歳鈴木亮平大好きおはばです  めっちゃカッコいいよね💕  


https://x.com/98AVmontaINGRAM/status/1793393687797637474  
片付けてたら大事なものが出てきたにょん  懸賞で当たった  神谷さんのサイン入りお年賀    
我在整理的时候，发现了一些重要的东西   
https://pbs.twimg.com/media/GONqDVYasAAb2wL?format=jpg&name=4096x4096  

https://x.com/saerikaduffy/status/1793399988405563427  
前期と後期の初日は同じように周り、カフェの壁は北条先生のサインが添えられていたり、エンジェルダスト用の書き下ろし獠のお出迎え😊宝探しみたい🥰ネトフリ版のサインポスター入れ替わりに、帰宅して気付く😅W獠ちゃんじゃないなって😆  
https://pbs.twimg.com/media/GONvyHVbwAAUXs_?format=jpg&name=medium  


https://x.com/ciitan126/status/1793433277954736140  
ついに！ついに！6/2(日)お昼頃観に行けます!!☆  娘も連れて行って布教活動もします(笑) #シティーハンター   #北条司展  楽しみ♪  
终于！终于！6/2(周日)中午左右可以去看！！☆带着女儿去做传教活动(笑)#城市猎人  #北条司展 我很期待♪  

https://x.com/KOHARU36991352/status/1793430885179797505  
昨日は後期初日行ってきました☘️  エンジェルハート早く見なきゃ…🥺  キャッツのロールケーキ美味しかった  オムとケーキのお皿｢XYZ…｣買った✌️  
我昨天去了後期的首日：☘️ 我迫不及待地想看到天使心...... 🥺 Cat's的蛋糕很好吃，我买了om和蛋糕盘｢XYZ…｣✌️    
https://pbs.twimg.com/media/GOOL36WbkAAzT9X?format=jpg&name=medium  
https://pbs.twimg.com/media/GOOL38QbkAAuAdZ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOOL4A9bYAAhNDp?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOOL4D6asAA_Kaz?format=jpg&name=small  

https://x.com/kikicks336/status/1793216994122379686  
デリ風な夕飯、でけたっ！  一枚目はスープ皿と獠の豆皿以外はゼノンで入手！（今日の台詞のお皿以外の小皿は閉まる前の骨董市で） 夫は量少ないから嫌だと小皿なしだけど（２枚目） ゼノンの空気のお裾分け、ありがとうございます！  
熟食式宵夜，搞定！ 第一个盘子是在 Zenon 买的，除了汤盘和獠的豆盘！(除了今天对话中的盘子，其他小盘子都是在古董市场关门前买的。） 我丈夫不喜欢，因为不多，所以没有小碟子（第二个）！   
https://pbs.twimg.com/media/GOLJWXkaYAAmCFD?format=jpg&name=900x900  
https://pbs.twimg.com/media/GOLJWXkaQAEBFOg?format=jpg&name=900x900  

https://x.com/yuzuki532/status/1793196319281922539  
まもちゃん…と思いながらもなんとか北条司展、後期も初日に滑り込んできた！！北条先生、今朝来てサインしてくださったそう☺️前回のサインが「コピー」という声があったらしく、そうならないように今回今日の日付を書かれたとのこと。貴重なお話も聞けて行けて良かったー😆✨  
まもちゃん......但还是成功地溜进了北条司展的第一天，也是展览的後期！ 今天上午，北条老师来签名了 ☺️，他说有人说他上次的签名是 "复制 "的，所以这次他写上了今天的日期，以防万一。 我很高兴能去听听他的珍贵故事😆✨  
https://pbs.twimg.com/media/GOK2i2RbUAAY85w?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOK2i2TawAAjyxn?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOK2i2RbAAAeznJ?format=jpg&name=900x900  
https://pbs.twimg.com/media/GOK2i2Ra0AAH1WQ?format=jpg&name=900x900  

https://x.com/lvxinxin/status/1793095456421196091  
再一次来到北条司展（下半场），再一次在留言板上留下来自纸片党的印记。 #北条司展  
https://pbs.twimg.com/media/GOJaz7kbgAEU1_J?format=jpg&name=4096x4096  

https://x.com/smiling_3flower/status/1793317858388357547  
Netflix版のサイン入りポスターも入れ替わり、3人ver.でした✨📢   あぁ色々読み返したいなぁ💖アニメも、ネトフリ版もまた観たい❣️時間がいくらあっても足りなそうっ🤣  
网飞版的签名海报也换了，而且是3人版✨📢  哦，我多么希望能回去再看一遍💖，我想再看一遍动画和 Netflix 版❣️，我想我的时间不够🤣  

https://x.com/lvxinxin/status/1793109228099649910  
《城市猎人》从开篇铺垫到最后的决战，獠和父亲海原的最终战，贡献过无数名场面。#北条司展  
https://pbs.twimg.com/media/GOJnVhdaIAAdKQb?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOJnVhhbEAAUiXM?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOJnVhgaMAEq_QE?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOJnVhfaEAAt7u1?format=jpg&name=4096x4096  


https://x.com/meimei12612149/status/1793166675493396655  
もっこりカレーは甘口しか食べない人は  辛く感じるかも。ロールケーキはレーズン？入ってた。宝石はどれ食べていいかダメか一瞬迷った笑    
只吃甜咖喱的人可能会觉得 Mokkori 咖喱很辣。 卷饼里有葡萄干？ 里面有。 我一时间不知道哪些宝石能吃，哪些不能吃，哈哈   
https://pbs.twimg.com/media/GOKblHkaIAARM1Y?format=jpg&name=4096x4096  

https://x.com/chibi_aya_728/status/1793164272048218246  
北条司展 後期へ！なんかもう、北条先生の絵が素晴らしすぎて心に響きすぎて泣いてしまった😭✨  こんなに素敵なものを見させていただいて本当にありがたいです💕  どの作品にも惹き込まれて、気づいたら1時間以上原画観てました😇  心が動きすぎてまだドキドキしてる！！ｷｬｰｰ!  
北条司展 後期！ 更重要的是，北条老师的画非常精彩，深深打动了我的心，让我流下了眼泪😭✨ 我非常感激能看到这么精彩的东西💕 我被每一幅作品吸引，发现自己看原画的时间超过了1小时😇 我的心还在怦怦直跳，因为它太让我感动了！ 哎呀  
https://pbs.twimg.com/media/GOKZZi4acAAW0wN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOKZZlobEAA4A0e?format=jpg&name=4096x4096  

https://x.com/xyz_mizu/status/1793152097531384196  
原画展、後期に来たわ～( *´艸｀)  
原画展、後期我在这里 - ～( *´艸｀)  
https://pbs.twimg.com/media/GOKOSncaIAACcbq?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOKOTgMaUAAQ0un?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOKOUpcaEAAlFhX?format=jpg&name=900x900  
https://pbs.twimg.com/media/GOKOU8XbUAAw431?format=jpg&name=medium  

https://x.com/ryu_ryu_ru/status/1793170753254510927  
北条司展 行きました😊  朝、具合悪かったけど、ちょっと頑張った(๑•̀ㅂ•́)و✧   原画展は良いね！キャラクターと作者の熱を感じるよ🤭  
我去看了北条刚的画展 😊 虽然早上生病了，但我还是努力了一下(๑̀ㅂ́)و✧ 原画展很棒！ 我能感受到人物和作者的热情🤭   
https://pbs.twimg.com/media/GOKfSVrakAAHaZt?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOKfSVrbwAAN_tt?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOKfSW_bkAAtnuc?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOKfSbdbkAEVyYP?format=jpg&name=4096x4096  

https://x.com/ryu_ryu_ru/status/1793173578252423413  
食べきった(￣▽￣;)  おなかいっぱい(º﹃º)  獠様がいっぱい❤️  

https://x.com/chibi_aya_728/status/1793174222753456240  
伝言板の前に四人のポスターぁぁぁぁーー！！！ｷｬｰｰ!ってなりました😇  (反射してたので画像の明るさ落としてます)  森田さんと安藤さんのにはサインも🥰嬉しいなぁ✨  @TheEdition88  さんのブログにも中の様子も出てましたね✨    
留言板前的四张海报啊！ 我太激动了😇！！！！ 我太激动了😇！  (由于反光，图片亮度有所降低）  森田和安藤的亲笔签名 🥰我好开心✨   @TheEdition88 的博客上看到了里面的内容✨  
https://pbs.twimg.com/media/GOKicWkbMAAyOOT?format=jpg&name=4096x4096  

https://x.com/hitoyuzuriha/status/1793192423906021501  
午後休取って行ってきたよー！お皿が珍しいもの(らしい)でした😊  
我下午请假去了那里！盘子很特别（显然）😊  
https://pbs.twimg.com/media/GOKzAIiakAAAukR?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOKzAI8bwAAj9qC?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOKzAIja4AA2-Tv?format=jpg&name=900x900  
https://pbs.twimg.com/media/GOKzAIkaEAArfcz?format=jpg&name=small  

https://x.com/chibi_aya_728/status/1793225644060684558  
もこオムカレーも美味しかったし、フロマージュロールケーキはさわやかで、グラスハートもキレイ✨  ロールケーキのお皿がキャッツのカードで嬉しかった💖  4枚目は遊んだやつ😂  もっこりをいただきに来られちゃうのは困る😇  (香ちゃんのプンプンパンケーキないの寂しいね😢)  
Mokkori煎蛋咖喱很美味，法式蛋卷蛋糕很清爽，GlassHeart也很漂亮✨  一盘卷饼是一张猫卡，让我很开心💖  第4张我玩了😂  我不想让他们来找我的麻烦 😇  (我会想念你的煎饼的，咔咔 😢）  
https://pbs.twimg.com/media/GOLRNtbaYAA5MN3?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOLRNoxbYAALs--?format=jpg&name=4096x4096  

https://x.com/meimei12612149/status/1793294241814495693  
ＣＥやＣＨの前半は原稿も修正が  あったと思うけど  以降は本当修正ないし  非常に綺麗でうっとりしちゃった、、、🩷  そういえば井上先生もアシさんだったから  どこか背景描かれてるんだよね。
CE和CH的前半部分修改了原稿  我想是有的  以后就不会真的修正了  非常漂亮，令人陶醉 说起来井上老师也是阿希  一定是画了背景吧。


https://x.com/xyz_mizu/status/1793238326755065932  
行く度に毎回メッセージを書かせて頂いているのですが、今日はちょっとイラストも入れてみました。絵を描くのはやっぱり好きだし、そして楽しい🥰  
每次去那里，我都会写上一句话，今天我又加了一幅小插图。我还是喜欢画画，这很有趣🥰。  
https://pbs.twimg.com/media/GOLcwSQawAAXuv5?format=jpg&name=4096x4096  


https://x.com/sherryzawander4/status/1793304035992506732  
北条司展 後期行ってきました！ADの描き下ろしの獠 カッコいい！！原画最高😆✨カレーがめちゃくちゃ美味しかった😋  
北条司展后期去了!描绘AD的獠好帅! !原画最高😆✨乱七八糟很美味的咖喱😋   
https://pbs.twimg.com/media/GOMYgh0bQAADspc?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOMYghxbsAEBqau?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GOMYghvaIAAra1i?format=jpg&name=4096x4096  


---  

## 前期  

https://www.xiaohongshu.com/discovery/item/662f189b000000001c009cad?xsec_token=CBe2A5KlVK09yIgOMWKil96N7WxRvScRvJm5Qvx3D8fmU=  
北条司画展！北条司的画风真的太喜欢了！！小时候最早是先看的猫眼三姐妹，后来看了成龙主演的城市猎人之后，又了解到了城市猎人。然后就是一发不可收拾  #北条司 #城市猎人 #猫眼三姐妹  
http://sns-webpic-qc.xhscdn.com/202411141551/aa2395736aed9297541910dca5a65955/1040g0083125lv3cp4o0g5n3l5j1k29694ngivr8!nd_dft_wgth_webp_3  

https://www.xiaohongshu.com/discovery/item/66777d24000000001c036982?xsec_token=CBHXy0bQRVdICoXMY3K4EnHFqYe_BXeCqyWHmYiXUbaRc=  
赶在最后一个周末去吉祥寺看北条司原画展 #原画展 #城市猎人 #北条司 #日本 #东京 #漫画 赶在开展最后一个周末，和朋友去看了吉祥寺的北条司原画展。（City Hunter城市猎人2025年将迎来连载40周年） 展厅虽然面积不大，但是可以看出布展很用心；旁边还有餐厅，有原画展联名餐品，价格还算实惠。展厅二楼的互动区可以看到很多看展人在留言本上的留言和涂鸦。真实地感受到日本人对漫画的感情很深厚。看到一个留言：喜欢了城市猎人了30多年，冴羽獠是我的初恋，而且我也很幸运地和一个名字叫做りょう（和獠同音）的男人结婚；现在两个孩子也很喜欢城市猎人，这次是和女儿一起来看展。 	
地点：ギャラリーゼノン  是今年新开张，漫画出版社コアミックス运营的漫画画廊。［後期］2024年5月22日(水）〜 6月23日(日）最后一天  GALLERY ZENON（東京都武蔵野市吉祥寺南町2−11−1）  
http://sns-webpic-qc.xhscdn.com/202411141542/85936cfa50b8a14e49ec3c24654a363e/1040g008314cc8640me004a18f2vloj5o3bsv468!nd_dft_wlteh_webp_3  
http://sns-webpic-qc.xhscdn.com/202411141542/5070787acff1f27e2131e0c2ea4a816d/1040g008314cc8640me0g4a18f2vloj5orp1s398!nd_dft_wgth_webp_3  
http://sns-webpic-qc.xhscdn.com/202411141542/2f1da9766f61f1d3d3e9928606e16120/1040g008314cc8640me404a18f2vloj5oa8uolao!nd_dft_wlteh_webp_3  
http://sns-webpic-qc.xhscdn.com/202411141542/98657df69b2ec928b6074dd9efeb62ad/1040g008314cc8640me5g4a18f2vloj5oeal285g!nd_dft_wlteh_webp_3  
http://sns-webpic-qc.xhscdn.com/202411141542/6db6bad54589c052a50d0bb18c611c91/1040g008314cc8640me604a18f2vloj5oh3hueo0!nd_dft_wlteh_webp_3  
http://sns-webpic-qc.xhscdn.com/202411141542/ba8d0d4e7bca18f7cb15996fbf1d12cb/1040g008314cc8640me6g4a18f2vloj5oip8res8!nd_dft_wlteh_webp_3  

https://www.xiaohongshu.com/discovery/item/666eb764000000001d01934d?xsec_token=CBBjwP2i9GqHgiBiCjgXGyvVE0EXTcxizrSAv4nVg_mVA=  
吉祥寺北条司展  买了些原稿回去学习一下  
http://sns-webpic-qc.xhscdn.com/202411141539/949b04930b015dc8d14e1709b19c1f95/1040g2sg3143q3ot4ho905p8s53d8sr7ql91mva8!nd_dft_wlteh_webp_3  


https://www.xiaohongshu.com/discovery/item/665c4cb5000000001401878e?xsec_token=CBPNX0yvu4FellunCStle5NO2yHGufMtLVPilo8GUpmNc=  
北条司先生的原作展，满满的回忆  吉祥寺的Gallery zenon 一个在铁道下面的屋子里，不大，但是动线设计合理转完一圈上二楼可以给北条司先生留言，据说他会看的好多粉丝画的特别好，门口有个漫画里的1：1的大锤子看有人拿起来拍照，很有意思  
http://sns-webpic-qc.xhscdn.com/202411141521/78289b6509ebc4ade425f7eddfa6989e/1040g2sg313hqihr0g4905ond7ajnqndv5oqgstg!nd_dft_wlteh_webp_3  
http://sns-webpic-qc.xhscdn.com/202411141521/0bc5cc01a692027da037b8846df956c0/1040g2sg313hqihr0g49g5ond7ajnqndvt6p714o!nd_dft_wlteh_webp_3  
http://sns-webpic-qc.xhscdn.com/202411141521/7265ce9ae0b0cd123d618f66d5462ed0/1040g2sg313hqihr0g4bg5ond7ajnqndvf6c4dag!nd_dft_wlteh_webp_3  

https://www.xiaohongshu.com/discovery/item/666cc3b1000000001d016c84?xsec_token=CB2_Szx3z4CNlttX-VfAdbA4E-XgWFDjtI6dVfm_r-cyU=  
http://sns-webpic-qc.xhscdn.com/202411141513/9d3794346238a5c8a3590e9d36f73805/1040g2sg3141t46kbicd05p6b5jv2pd532lgjm4g!nd_dft_wlteh_webp_3  
http://sns-webpic-qc.xhscdn.com/202411141513/4cbff932b2e7f2f9acaff5b61af810a5/1040g2sg3141t482h2ed05p6b5jv2pd535k00o1o!nd_dft_wlteh_webp_3

https://www.xiaohongshu.com/discovery/item/66599f35000000000f00f6da?xsec_token=CBFzF7H7zDUMgUS59kiAND3ae7MW2vzfYnIM7ff80EQbg=  
又来看北条司的展啦一直非常喜欢北条老师和他的城市猎人，画的真是太棒了。不过可能现在不流行这种画风了…但我就是喜欢，还给老师留了言，希望他能看到。地点：吉祥寺Gallery Zenon #日本生活 #东京生活 #画展 #漫画 #北条司 #城市猎人  
https://sns-webpic-qc.xhscdn.com/202411141454/5fe1383c558e121c2f76cba0fee200f7/1040g008313f6tb3dg41043c4vm568hru5htc49o!nd_dft_wlteh_webp_3  
https://sns-webpic-qc.xhscdn.com/202411141454/10b9cc12f56a19090f2db4255c9dd0ef/1040g008313f6tb3dg41g43c4vm568hrupllggro!nd_dft_wlteh_webp_3  
https://sns-webpic-qc.xhscdn.com/202411141454/bf34ca38bf48a8b95af2d7a18c535344/1040g008313f6tb3dg42043c4vm568hru133bhl8!nd_dft_wlteh_webp_3  
https://sns-webpic-qc.xhscdn.com/202411141454/21d41a81a949f23dae2774a992247728/1040g008313f6tb3dg45043c4vm568hru8q0qe8o!nd_dft_wlteh_webp_3  


https://www.facebook.com/SoulWriterH/posts/pfbid0FpWusoKtrQpAHCXS2X8LH34zXdRXWyxFqoWiXCtfUyVMGbHTVpg6bWexgogMFkKNl?__tn__=*F  

https://x.com/cityhunter100t/status/1340596590436859904  
[毎日CH扉絵 - Twitter](https://twitter.com/hashtag/毎日CH扉絵?f=live)  

https://x.com/Edition88art/status/1792115188398989326  
The Kimagure Orange Road campaign for overseas customers was so successful that we think we should do the same for City Hunter ....🥰  
Visit http://edition88.com on Monday, 20th at 18:00 Japan time  
https://pbs.twimg.com/media/GN7fIF1aoAAt4SF?format=jpg&name=4096x4096  

https://x.com/Yomigues/status/1792538628075196883  
https://pbs.twimg.com/media/GOBgBSOWUAA9-wZ?format=jpg&name=4096x4096  

https://x.com/shinsekaifr/status/1792587336372371800  
🔨Nouveauté!!🔨  City Hunter - Nicky Larson 1/8 ARTFXJ (Kotobukiya) est disponible sur le site !  La livraison est offerte ✨    
Pour commander 👉 https://shin-sekai.fr/city-hunter/5366-city-hunter-figurine-nicky-larson-18-artfxj-4934054007707.html  
https://pbs.twimg.com/media/GN14TOGbsAAyLy3?format=jpg&name=small  
https://pbs.twimg.com/media/GN14TOUagAA6jl8?format=jpg&name=small  
https://pbs.twimg.com/media/GN14TOUacAAMFp6?format=jpg&name=small  


https://x.com/5Rwph0EEiX83793/status/1791789557110968577  
息子氏、遂に自分で推しグッズを作りだした😂お気に入りのコマをコピーして色塗りして、100均で買ったオリジナルキーホルダーに入れて完成✨来週学校に持って行くんだって😊    
https://pbs.twimg.com/media/GN23GtYbcAARxoV?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN23G1Mb0AAqfqi?format=jpg&name=4096x4096  

https://www.instagram.com/p/C7MQKxWPMDU/?img_index=1  

https://x.com/GalleryZenon/status/1791341949217309060  
https://pbs.twimg.com/media/GNwgAaBa4AA7m9q?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNwgAaCbcAAyONj?format=jpg&name=4096x4096  

https://x.com/chiroru_special/status/1792070600296632483  
GALLERY ZENON前期最終日！やっぱりもう一度原画をリアルに見たくて現地へGO！！😆💕ぬい達と一緒に行けて幸せ〜❤️    
GALLERY ZENON前期最后一天!果然还是想再一次真实地看到原画去现场GO ! !😆💕和努伊们一起去很幸福~❤️  


https://x.com/smiling_3flower/status/1792110456322335128  
昨日デザフェス、今日は前期最終日のギャラリーゼノンへ❣️思いきって来て良かった〜💖入場特典のシークレット出たっ❣️嬉しすぎるーーー❣️最高だっ🥹💖昨天是desafes，今天是前期最后一天的galleryzennon❣️  
索性就来好了~💖入场特典的秘密出来了❣️太高兴了——❣️最高🥹💖  
https://pbs.twimg.com/media/GN7a9JrbwAI--AD?format=jpg&name=4096x4096  

https://x.com/chiroru_special/status/1792116835695128945  
GALLERY ZENONにて。シティーハンター30周年アニバーサリーのキャンパスをゲットできて嬉しいー😆💕クリアカード、ガラス越しのキスシーンはゲットできず残念•••💦でもかなり満足なシーンばかりでニヤニヤ🥰   
在GALLERY ZENON。城市猎人30周年纪念日的校园很高兴赢得-😆💕通过信用卡,玻璃的接吻场面是无法赢得遗憾•••💦也相当令人满意的场面只是笑着说:🥰
https://pbs.twimg.com/media/GN7gw48aMAAX1tN?format=jpg&name=4096x4096  

https://x.com/hB2I6h2vw266059/status/1792564274604507315  
北条先生の絵は本当に線とか色の塗り方がとてもキレイでした！  キラキラしてる描写とかで、絵の具が少し盛り上がってるところで、本物なんだなあって思いました…  繊細な作業プラスであのおもしろさ…すごすぎます  素敵でした本当に…何回でも見に行きたくなります！  
北条老师的画，线条和色彩都非常漂亮！那闪闪发光的描绘和颜料微微凸起的方式，让我以为那是真的...细腻的工作加上趣味......令人惊叹！真的很可爱......让人想回去再看一遍又一遍！  

https://x.com/Samantha_Newly/status/1792044948671635866  
https://pbs.twimg.com/media/GN6fW3jbsAA-bKw?format=jpg&name=4096x4096  

https://x.com/Samantha_Newly/status/1792042222126571805  
https://pbs.twimg.com/media/GN6c45GbEAAayia?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN6c45JbwAAravl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN6c45KbkAA0XhO?format=jpg&name=4096x4096  

https://x.com/Samantha_Newly/status/1792042939759366248  
https://pbs.twimg.com/media/GN6dip0aYAAe-YD?format=jpg&name=4096x4096  

https://x.com/Samantha_Newly/status/1792043197218390374  
https://pbs.twimg.com/media/GN6dxA1aQAAQfa-?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN6dxA4aQAAoqI1?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN6dxAzasAAOiHI?format=jpg&name=4096x4096  

https://x.com/Samantha_Newly/status/1792044360781169150  
https://pbs.twimg.com/media/GN6e01sbIAA20vy?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN6e01uagAAI7S-?format=jpg&name=4096x4096  

https://x.com/Samantha_Newly/status/1792044620425404446  
https://pbs.twimg.com/media/GN6fEKobEAAYLfH?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN6fEKnaMAAAaL-?format=jpg&name=4096x4096  

https://x.com/3939sakutyan/status/1792049631846859079  
前期の最終日だからー！横浜でTMの新聞買って吉祥寺の北条司展きましたー！最後にまた美しい先生の芸術品みられてよかった！  
今天是前期的最后一天！我在横滨买了一份 TM 报纸，然后去吉祥寺看了北条司展！我很高兴能在最后看到他的另一幅美丽的作品！  
https://pbs.twimg.com/media/GN6jow-bsAAJjPp?format=jpg&name=4096x4096  


https://x.com/mikawa1234567Tw/status/1792068076466516305  
ふーみんさんもお好きなんですね🏀✨  
https://pbs.twimg.com/media/GN60aTAbQAALe4V?format=jpg&name=4096x4096  

https://x.com/sa_dango/status/1792110763139833926  
北条司展  行ってきたよ  憧れのキャッツ・アイの生原稿〜！！どの原稿も立ち姿とか手足の描き方が特徴的な北条先生イズムが美しい✨  触っちゃダメ絵が楽しいw  近くで見れて楽しかった〜  後期にいくつか作品入れ替わるの楽しみ  
北条司展  我去过那里 我渴望已久的《猫眼》现场手稿✨！! 他绘制每张手稿的站姿和肢体的方式都是北条老师的特色✨ 不允许触摸的图画也很有趣 近距离欣赏它们很有趣~  我期待着下半年更换一些作品  
https://pbs.twimg.com/media/GN7bPSubkAAvBNc?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN7bPSrbkAAk41a?format=jpg&name=small  
https://pbs.twimg.com/media/GN7bPSvbUAALaEq?format=jpg&name=4096x4096  


https://x.com/3939sakutyan/status/1791811409992622411  
お花の並びがやばすぎるぅ😻💕  
花朵是如此的整齐😻💕  
https://pbs.twimg.com/media/GN3K-YIakAAJaxi?format=jpg&name=4096x4096  

https://x.com/MsJunction/status/1791821265877307621  
今回も素敵、素晴らしかったです🌹‼️他の方々のお花たちも一緒に、ちょっと狭い通路に勢揃いだったので、皆さん写真撮るのに大変だった笑💧  
同样，很可爱，和其他人的花在一起很美妙🌹‼️，而且它们都在一个相当狭窄的过道里，所以大家都很难拍照，笑💧   
https://pbs.twimg.com/media/GN3T8KQbgAAE-Qo?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN3T8KSacAAoh58?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN3T8KQawAAYxRM?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN3T8KPa4AA58gU?format=jpg&name=4096x4096  

https://x.com/d_tamako3/status/1791815153006326245  
北条司展前期リベンジ!もう色々素晴らし過ぎ!手仕事に圧倒され過ぎて放心状態。今まで恐れ多すぎてイラスト描いた事無かったけれど今回は先生宛のメッセージに香ちゃんを描いたり（なんかもうすみません!）。作品を生み出してくれた事に感謝しながら家族の元に帰宅。2024.5 #北条司展   
北条司展前期的复仇！ 已经有太多美好的东西了！ 我被这些手工作品深深地吸引住了，就这么放任自流了。 我以前从来没有画过插图，但这次我甚至画了一幅香子的画，作为给老师的留言（真是对不起！）。 我被工作压得喘不过气来，只好放手一搏。 我回到家里，感谢家人创作了我的作品。 2024.5 #北条司展   
https://pbs.twimg.com/media/GN3OYbZbkAAzLB-?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GN3OYbYbkAAijwL?format=jpg&name=900x900  
https://pbs.twimg.com/media/GN3OYbbb0AAqJUZ?format=jpg&name=4096x4096  

https://x.com/sherryzawander4/status/1791376923211485437  
北条司展 再び。この描き下ろしすごく好きだな☺️大好きなオープンカーの獠香 も見入っちゃうし、前期のみのTAXI DRIVERも見納めてきた😊   
又是北条司展。 我真的很喜欢这幅画。☺️ 我最喜欢的敞篷车 獠香，我也只在第一学期看过出租车司机 😊  
https://pbs.twimg.com/media/GNw_zyMbYAAYnTQ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNw_zyNbgAEEjtE?format=jpg&name=4096x4096  

https://x.com/Sasaki_Satoko/status/1791384731172692080  
本日の目的はギャラリーゼノン！やっと来れた！コラボメニューはきっと一度には食べられないと思って原画展の前にもっこり腹ごしらえ。お腹いっぱいで北条先生の絵を観る幸せよ☺️パンケーキとコーヒーで余韻に浸って眼福満腹😋  
今天的目标是Zenon Gallery！ 我终于来到了这里！ 我知道我不可能一口气吃完合作菜单，所以我在原画展前吃了点东西。 ☺️ 煎饼和咖啡的余韵和饱腹感😋  
https://pbs.twimg.com/media/GNxG5S0aQAEgamT?format=jpg&name=4096x4096  

https://twitter.com/GalleryZenon/status/1790925135014440976  
吉祥寺で開催中のリアル宝探しゲーム「狙われた天使のレクイエム」  に続き、謎解きに『キャッツアイ』が登場💕  1冊、税込￥1000-でギャラリー内物販コーナーにて販売中です！謎解きファンの皆様、キャッツアイファンの皆様、ぜひGETしてみては🐈💘  
在吉祥寺举办的现实寻宝游戏“被瞄准的天使的安魂曲”  之后,解谜出现💕《猫眼》  1本，含税￥1000-在画廊内商品贩售中!解谜球迷的大家,猫眼迷一定要试着get🐈💘  
https://pbs.twimg.com/media/GNqk6cwbsAASj4i?format=jpg&name=medium  

https://twitter.com/GalleryZenon/status/1791003014792413632  
人気のシティーハンターアクリルスタンド、アクリルフィギュアが入荷致しました！！また、人気商品のシティーハンタートートバッグは再び入荷待ちとなっております🙇  
⚫︎事前チケット購入は下記URLからお手続きをお願い致します。  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1   
⚫︎当日チケットも店頭販売しております！  
ぜひご来場お待ちいたしております。  
广受欢迎的《城市猎人》丙烯酸支架和丙烯酸模型现已到货！ 此外，深受欢迎的《城市猎人》手提袋也在等待您的光临🙇。 
⚫︎ 请访问以下网址提前购票：  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1   
⚫︎ 活动当天也可在店内购票！  
期待您的光临！  
https://pbs.twimg.com/media/GNrrvz1a8AAM22t?format=jpg&name=4096x4096  

https://twitter.com/takko79/status/1790938296174129317  
来たよー✨  
我在这里✨  
https://pbs.twimg.com/media/GNqw2G8b0AAdJMW?format=jpg&name=4096x4096  

https://twitter.com/meimei12612149/status/1790948982057050249  
今日は舐め回すようにじっくり見てきましたよー🩷  二枚目の布団の柄、トーンなんだ〜とか  先生は修正少ないなぁとか♪   スペシャルブレンドめっちゃ美味しかったのでドリップで出して欲しい😭  あとメッセージノート皆さんの楽しく拝見！ ゆきみつさんのイラストとても綺麗🩷  
今天我仔细看了一遍，舔了个遍 🩷我一直在想第二张被褥的图案，它的色调是怎样的，老师怎么不怎么修改 😭还有，我一直很喜欢看你的留言笔记！獠的插图太美了🩷   
https://pbs.twimg.com/media/GNq6lgCa0AAKOfc?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNq6lgJb0AAUzAw?format=jpg&name=4096x4096  

https://twitter.com/meimei12612149/status/1790962815626559856  
熊本復興のイラスト、プロで描き慣れてるからかもしれないけど  やや上からのアングルで四人描いてるのに  北条先生、アタリも描いてなかった気がしたんだが。また下書きだけで充分綺麗だし  成り立ってるし  本当さすがでございます先生、、、🙏  
熊本重建的插图，也许因为我是专业人士，习惯于画画。我从一个稍微高一点的角度画了四个人。北条老师，我觉得你连雅达利都没画。 此外，只画个草稿就够美了。而且很有型。老师，您真的很厉害......🙏还有，你的笔法又快又细致👼  

あとペン入れ早いのに細かい👼
https://twitter.com/kamikitakeiko/status/1790996839543796044  
COMPLEXの為に東京来たので  北条司展によって来ました  やっぱり手描き原稿は良い  デジタルだと見てもくるものがあまりない  ただの老害なのかもしれませんが…  
我是为 COMPLEX 来东京的  我去看了北条司展  手绘原稿毕竟是好东西  当我看到数字作品时，并没有太多的感觉  也许只是年纪大了......  
https://pbs.twimg.com/media/GNrmIPpacAAwwQm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNrmIQzbIAAzRbZ?format=jpg&name=4096x4096  

https://twitter.com/Risuke3K/status/1791006118766112946  
吉祥寺の #ギャラリーゼノン で  #北条司展 観てきました。原画がとても良い。黒板とスケッチブックに  絵を描いてきたから  3時間弱居たな。(⁠ ⁠ꈍ⁠ᴗ⁠ꈍ⁠)  
https://pbs.twimg.com/media/GNruiz8aAAAiqzS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNrui09b0AAYuOt?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNruiz8a4AIRkaa?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNrui07bEAAHuP_?format=jpg&name=4096x4096  

https://twitter.com/iganinja1192/status/1791035222940959223  
楽しかったです^_^綺麗な原画に感動✨
我很喜欢^_^。精美的原作给我留下了深刻的印象✨。  
https://pbs.twimg.com/media/GNsJCoeaMAEPWhs?format=jpg&name=4096x4096  

https://twitter.com/tenchotenchoooo/status/1791071261302374492  
初期の冴羽さんも魅力的。より、男の本能が  正直に描かれてた感じ。マドンナ達も色とりどり  様々な事情を持ち、且つ、魅力的。  
早期的 冴羽 也很有吸引力。更像是对男人本能的真实的写照。マドンナ也多姿多彩。环境不同，但也有吸引力。  
https://pbs.twimg.com/media/GNspz3MbIAAX1hS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNspz_2bYAAnDnu?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNsp0C1acAAI4dE?format=jpg&name=4096x4096  

https://twitter.com/fumionishimura/status/1791083352713568441  
https://pbs.twimg.com/media/GNs0yt_a0AAGsPu?format=jpg&name=900x900  

https://twitter.com/MonPanier12/status/1791085314473443721  
GALLERY ZENONオープン記念合同プレゼントキャンペーン第2弾の 北条司展チケット2枚当選しました！ありがとうございます😊  前期は2回行きましたが、後期展示も通います。今から楽しみです💕  
GALLERY ZENON开业纪念联合送礼活动第2弹的北条司展门票2张中奖了!谢谢😊  前期去过两次，后期展览也会去。从现在开始是乐趣💕  
https://pbs.twimg.com/media/GNs2mN0bIAEteQg?format=jpg&name=4096x4096  

https://twitter.com/moriyama_555/status/1791089245878440285  
ネトフリ版のイラスト、思ってたより小さな用紙だったので、このサイズで繊細に描かれていた事に驚いてしまった。個人的には初めて先生の連載をリアタイ出来たRASH‼︎が見れて嬉しい☺️展示に見入ってたらコラボの事を忘れて閉店時間...１時間半では足りなかった。次は日中に行こうかな  
我很惊讶 Netflix 版本的插图在这种尺寸下画得如此精致，因为纸张比我想象的要小。 作为个人来说，很高兴能看到第一次再现老师的连载的RASH !︎☺️  我沉浸在展览中，以至于忘记了合作的事，也到了该关门的时候了...... 一个半小时是不够的。 也许下次我会在白天去    
https://pbs.twimg.com/media/GNs6LFDaAAAnb6U?format=jpg&name=4096x4096  

https://twitter.com/Risuke3K/status/1791090203152798066  
CAT'S EYE 第一話の原稿観れるとは  思わなかったな。当時ジャンプで観て  アッ。この絵柄とキャラ好きっ🩷  ってハマったもんね。  メッセージにも描きました。 (⁠☆⁠▽⁠☆⁠)    
https://pbs.twimg.com/media/GNs7CZJbwAAz17U?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNs7CcKbAAAbRk8?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNs7ClDa0AAdPRA?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNs7CwDbUAIjkst?format=jpg&name=small  

https://twitter.com/Risuke3K/status/1791096592482865441  
凄く上手い絵を描く人がいるなあ。と思ってスケッチブックを観たら  プロ漫画家が混ざってたよ。(⁠*⁠´⁠ω⁠｀⁠*⁠)  
有人画得很好啊。我看了看速写本，里面混着职业漫画家。(⁠*⁠´⁠ω⁠`⁠*⁠)  

https://twitter.com/MsJunction/status/1791097485571887138  
(この頃、読んでたな…)とか、(この頃のタッチが好きだな…)とか、平日にも関わらず沢山のファンの皆さん、きっとみんな同じこと考えてたんだろうな笑  
(这段时间，在看啊…)之类的、(真喜欢这段时间的达也…)之类的、即使是平日也有很多的粉丝们，大家都在想同样的事情吧笑  
https://pbs.twimg.com/media/GNtBqd-bcAApJzZ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNtBqeAbcAAF4uA?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNtBqeAaEAAQzxN?format=jpg&name=4096x4096  

https://twitter.com/MsJunction/status/1791097918948343835  
さすが先生の絵、顔認識する。。←  
不愧是老师的画，面部识别。。 ←  
https://pbs.twimg.com/media/GNtCEApa0AAjdwb?format=jpg&name=4096x4096  

https://twitter.com/MsJunction/status/1791098327226069138  
こんなこと改めて言うことじゃないけど、本当に"巧い"作品ばかり。。😊  
这样的事不是要重申的，真是"巧"作品。。😊  
https://pbs.twimg.com/media/GNtCbtBbgAA3i4I?format=jpg&name=4096x4096  

https://twitter.com/MsJunction/status/1791098549020824046  
https://pbs.twimg.com/media/GNtCoo-bcAI9LM0?format=jpg&name=4096x4096  

https://twitter.com/MsJunction/status/1791099334160990581  
https://pbs.twimg.com/media/GNtDWLIawAAoiHP?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNtDWLJakAAm2WJ?format=jpg&name=4096x4096  

https://twitter.com/tenchotenchoooo/status/1791104550684029199  
https://pbs.twimg.com/media/GNtIFhCaoAAubOx?format=jpg&name=4096x4096  

https://twitter.com/08kujaku/status/1790174747323748475  
https://pbs.twimg.com/media/GNf6b_haEAAjDbL?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNf6cEMa8AE2gZl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNf6cBsaAAAmLST?format=jpg&name=900x900  
https://pbs.twimg.com/media/GNf6cENbEAAJDHv?format=jpg&name=900x900  

https://twitter.com/pMa5ZUrB0Z19prn/status/1790287143639843136  
歩き疲れとったせいか沢山パシャり📸したのに、ちょっとしか載せて無かったみたい💧  
因为走得很累，我拍了很多照片，但我想我只在网站上放了几张  
https://pbs.twimg.com/media/GNhgqJea8AAdjCY?format=jpg&name=small  
https://pbs.twimg.com/media/GNhgqQza4AAQ-3R?format=jpg&name=small  
https://pbs.twimg.com/media/GNhgqaebkAALIEt?format=jpg&name=small  
https://pbs.twimg.com/media/GNhgqi4bUAAj9ul?format=jpg&name=4096x4096  

https://twitter.com/tatewaki9no/status/1790363992420159979  
北条司展 再訪  「ネコまんまおかわり♡」の展示は前期だけだから、もう一度見ておきたかった  あと、え!?依頼人だったアイドルの松村渚ちゃん?な「TAXI DRIVER」も  「SPLASH!」シリーズなんかもそうだけど、あの頃読んでいたものは一言一句覚えてるなぁ   
重温 #北条司展  我想再看一遍"白猫少女"的展览，因为它只在前期展出过。  还有，什么？ 客户是偶像松村渚？ 还有 "出租车司机 "系列，“SPLASH！"系列也是一样，但我记得当时看的每一个字。 

https://twitter.com/tatewaki9no/status/1790374201444192715  
唯一無念だったのは前回揃わなかった入場特典  3人で入ればなんとかなるだろ…と思ってたら、まさかの全員同じ柄⚡  こんな事ある？と思ってたんだけど、カフェにいた他のお客さんと話をしたら…  その場にいた12人全員同じカードでしたとさ😅   
唯一的遗憾是，我们没有拿到上次的入場特典。我以为如果我们三个人一起进去，就能搞定......但结果是，我们的模式都一样。  这怎么可能呢？ 我正在想："这怎么可能呢？"但后来我和咖啡馆里的另一位顾客聊了起来，结果...那里的 12 个人都有同一张卡😅  


https://twitter.com/We_are_CH/status/1790646196073345509  
GALLERY ZENONの北条司展楽しかったー！配布カードはシティーハンターでした（前回はキャッツ）シークレットは出ないねぇ  展示スペースや2階のメッセージブースも空いていたのでゆっくりできました   次は後期。いつ行こうか🤔  もうみんな予約したのかな？  
我很喜欢在 GALLERY ZENON 举办的北条司展览！分发的卡片是《城市猎人》（上次是《猫》）  没有秘密卡片！  展览空间和二楼的留言亭都空着，我可以放松一下！  接下来是展览的後期。 我们什么时候去🤔？  大家都预订了吗？
https://pbs.twimg.com/media/GNmnOKHa0AAwNrB?format=jpg&name=4096x4096  

https://twitter.com/Ko_ichi_32/status/1790717022814568626  
ゼノンコミックス宣伝様より「北条司展」の招待券をいただきました♪ありがとうございます🙇🏻北条司ど真ん中世代としては、すごく楽しみです😆✨しかも、事前予約要らずで前後期どちらでもOKだなんて素敵すぎる💓#ゼノンコミックス宣伝 #北条司展  
非常感谢 Zenon Comics Advertising 邀请我参加「北条司展」。🙇🏻 作为北条司这一代的一员，我非常期待这次展览😆✨，而且不需要提前预约，可以在展览之前或之后来，这太好了💓   
https://pbs.twimg.com/media/GNnno-zakAAVZXr?format=jpg&name=4096x4096  

https://twitter.com/saigameLine/status/1789852393737207891  
推し活推し活  
要宣传和推广的活动  
https://pbs.twimg.com/media/GNbVNVLa8AAFzlX?format=jpg&name=4096x4096  

https://twitter.com/paokurin/status/1789904460975595863  
初 吉祥寺！  初 ZENON❣️昨日、夫とGALLERY ZENONオープン記念『北条司  展』に行ってきました😊ﾔｯﾄｲｹﾀﾖ~北条先生の繊細すぎる原画の美しさに感動しっぱなし😭✨Cafeのコラボメニューも美味しかったです🥰  
第一个吉祥寺！ 第一个 ZENON❣️昨天，为了庆祝 GALLERY ZENON开幕，我和丈夫去看了『北条司  展』😊Yat Iketayo ~北条老师精美绝伦的原创作品让我叹为观止😭✨咖啡馆的合作菜单也很美味 🥰   
https://pbs.twimg.com/media/GNcEnc-bIAA7HiQ?format=jpg&name=4096x4096  

https://twitter.com/15niconicola/status/1789952278998270142  
やっと北条司展に行かれました😭間近すぎる原画の繊細なタッチ＆色づかいを見つめ、写真もほぼOK。キャッツ・アイからジャンプ読者だったわたくし。ただただ感動😭頭上には先生の作品がオブジェの様に。伝言板もしっかり。こちらがこころ震えた日でした😂  
终于去了北条司展😭看着太近的原画纤细的笔触&配色，照片也差不多OK。我是从猫眼开始的跳跃读者。默默地感动😭头顶上是老师的作品。留言板也很好。这里是心颤抖的日😂  
https://pbs.twimg.com/media/GNcwG4WbcAAHW7w?format=jpg&name=4096x4096  

https://twitter.com/mariajamts/status/1789968936156405839  
吉祥寺で開催中の #北条司展 へ🐈‍⬛兄ふたりの影響で少年ジャンプ読者だったから、この時代の漫画は大体読んでいたし、原作は連載当時に読んだきりだったけれど、展示作品を見ているうちに当時の記憶がみるみる蘇ってきて、とても懐かしかった🚘  もっこりオムライスも食べられて大満足😋    
参观吉祥寺举办的#北条司展 🐈‍⬛受两个哥哥的影响，我喜欢看《SHONEN JUMP》，所以那个时代的漫画我看得最多，只有在连载时才会看原作，但看着展出的作品，我对那个时代的记忆涌上心头，感到非常怀念🚘。我对煎蛋饭也非常满意😋  

https://twitter.com/Zetton700/status/1789490062847316321  
北条司展  なんといっても描き下ろしの絵が素晴らしいです！！ミニクーパーと100tハンマー像もリアルでつい色んな角度から撮ってしまった笑  
北条司展  我还能说什么呢，新绘制的图片真是太棒了！Mini Cooper 和 100t Hammer 的雕像非常逼真，以至于我从不同角度拍了很多照片  
https://pbs.twimg.com/media/GNWLubqbIAAGLqf?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNWLubvbsAAlGGw?format=jpg&name=4096x4096  


https://twitter.com/pMa5ZUrB0Z19prn/status/1789515635598483738  
https://pbs.twimg.com/media/GNWi9gTbwAA7I_Y?format=jpg&name=4096x4096  


https://twitter.com/kaz_1090/status/1789558493110055340  
https://pbs.twimg.com/media/GNXJ6pBbwAA98RZ?format=jpg&name=4096x4096  

https://twitter.com/3939sakutyan/status/1789623424408805730  
九井 Q1Q1  って先生センスがヤバイです‼️    
九井 Q1Q1  老师您的感觉太糟糕了!  
https://pbs.twimg.com/media/GNYFBRrbYAAy0Z4?format=jpg&name=4096x4096  

https://twitter.com/KY_POKONYAN/status/1789654135446622488  
今日は吉祥寺のギャラリーゼノンで開催中の　#北条司展　に行ってきました😊 #シティーハンター　好きなら大満足間違いなし😆原画が間近で観られます。写真撮影🆗  北条先生宛のメッセージを書けるコーナーがあったんだけど、他のイラストのレベル高すぎて断念😅上手すぎるよ😊  
今天，我参观了吉祥寺 Zenon Gallery 的 #北条司展  如果你喜欢#城市猎人#，一定会非常满意😆  您可以近距离欣赏原画。 摄影🆗  本来有一个角落可以给北条老师写留言的，但因其他插画水平太高而放弃了😅，他太厉害了😊  
https://pbs.twimg.com/media/GNYg8s-bIAEPJxz?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNYg8s-agAAhrKf?format=jpg&name=4096x4096  


https://twitter.com/kanaminari/status/1789700304222728324  
旦那さんリクエストで久々に吉祥寺へ行ってきました。美しい原画の数々…眼福でした🥹ハンマー持って香になれるのいいね(笑)  
应丈夫的要求，我时隔多年后再次来到吉祥寺。精美的原画......真是大开眼界🥹可以拿着锤子变成香(笑)  


https://twitter.com/pinktanpopo34/status/1789702917387399516  
北条司展 のトートバッグ、ついに入手❣️三度目の正直〜ありがとうございます😆ギャラリーのスタッフさんに商工会館の場所を教えていただいてマンホールカードをもらって、マンホール巡りもしてきました！感謝☺️  
北条司展的手提袋，终于到手❣️第三次的正直~谢谢😆画廊的工作人员告诉了我工商会馆的位置，并拿到了井盖卡，还去寻访了井盖!感谢☺️  
https://pbs.twimg.com/media/GNZNUQGacAAwisH?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNZNUQIbAAA5JeT?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNZNUQFbUAAENAz?format=jpg&name=4096x4096  

https://twitter.com/yoyojambo/status/1789338892564566084  
西新宿でカクテル「XYZ」を飲みました🍸美味しさ格別でした☺️  
西新宿喝鸡尾酒「XYZ」了🍸味道特别好☺️   
https://pbs.twimg.com/media/GNUCPNZbsAAfCMe?format=jpg&name=medium  

https://twitter.com/scar_bullet/status/1789129833764761820  
北条司展 THE ROAD TO 『CITY HUNTER』40TH ANNIVERSARY 2025 〜LIMITED SPECIAL EXHIBITION〜 原画を見て。シティーハンターやキャッツ・アイが好きになったのが北条先生のタッチが好きだからと改めて感じた時間でした！出会えて良かった。  
北条司展 《城市猎人》THE ROAD TO 『CITY HUNTER』40TH ANNIVERSARY 2025 〜LIMITED SPECIAL EXHIBITION〜 原画欣赏。 这让我再次感受到，我之所以喜欢《城市猎人》和《猫眼》，是因为我喜欢北条老师的笔触！ 很高兴认识您。   
https://pbs.twimg.com/media/GNREF4gbEAEsPYU?format=jpg&name=4096x4096  

https://twitter.com/asoumi_persona/status/1789178165144985964  
北条司展 を満喫したところで、友人と待ち合わせなのだけど…マンホールは何処…？(汗)  
在欣赏了北条司展后，我与一位朋友相约......但窨井在哪里......？ （汗）  
https://pbs.twimg.com/media/GNRwDUNa8AA44VL?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNRwDUOb0AEzrtD?format=jpg&name=4096x4096  

https://twitter.com/iv9fgHWWxc7784/status/1789190339859034570  
最高でした！！  隅から隅まで堪能してきました🫶  グッズのところではアニメの時の懐かしいソングが聞けて幸せでした❤️  
太棒了！我很享受其中的点点滴滴🫶  我很高兴能在商品销售处听到动漫中的怀旧歌曲 ❤️  

https://twitter.com/p_accho/status/1789193537030164498  
コラボメニューも美味しくいただきました🌟  外のテラスには伝言板があって、チョークで書けるようになっていました✨祝い花もいっぱい！🌸みんなに愛されているなぁ～  
合作菜单非常美味🌟外面的露台上有一块留言板，可以用粉笔写字✨，还有很多喜庆的鲜花！ 🌸大家都非常爱你！  
https://pbs.twimg.com/media/GNR-CE2bQAAoTFc?format=jpg&name=4096x4096  

https://twitter.com/anikingerZ/status/1789195867091787984  
北条司展。やっぱり、最高の男は冴羽獠で決まり！  
北条司展。果然，最高的男人獠决定了!   
https://pbs.twimg.com/media/GNSAJYXaUAA3I4_?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNSAJV_bkAAMjRY?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNSAJX4aIAAZp-o?format=jpg&name=4096x4096  

https://twitter.com/suppy_651/status/1789253670359511422  
吉祥寺のGALLERY ZENONへ北条司展を見に行ってきた～  原画等撮影可ということでいろいろ撮影♪  
我去吉祥寺的 GALLERY ZENON 观看了北条司展~  允许拍摄原画和其他展品，因此拍了很多照片  

https://twitter.com/suppy_651/status/1789254640107733232  
CAT'S EYEの看板と瞳さんもいた♪  
还有一个 CAT'S EYE 标志和瞳  
https://pbs.twimg.com/media/GNS1mp2a0AAeMZ2?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNS1mwAbgAA52L_?format=jpg&name=medium  
https://pbs.twimg.com/media/GNS1m3MawAAlnp3?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNS1m_MaEAANWOM?format=jpg&name=medium  

https://twitter.com/suppy_651/status/1789255337586917847  
戦利品♪  
https://pbs.twimg.com/media/GNS2PkFa8AAxPCR?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNS2PoqaIAAOjsd?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNS2PyDaIAAXzde?format=jpg&name=4096x4096  

https://twitter.com/walkon7/status/1789271216072921302   
北条司展 に行って来た! 今回はデビュー作品から #エンジェル・ハート までの貴重な原画などの展示があったので細かく見ていたらあっという間に見学時間が終わってしまった。しかも今回も撮影OK。#北条司 先生、今回もファンにとっては幸せなイベントをありがとうございます。#シティーハンター  
https://pbs.twimg.com/media/GNTEq2daIAAvH9k?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNTEq3qbcAApYWs?format=jpg&name=medium  
https://pbs.twimg.com/media/GNTEq8lbAAEy8CE?format=jpg&name=4096x4096  

https://twitter.com/chibi_aya_728/status/1789278289619804289  
北条司展 のフォトスポットの注意書き、この場面なのを先日初めて気づいた😂  
美佐先生もわんちゃんたちも思い出すけど、「さ、パンツ脱いで♡」の肛門科の先生が脳内を占拠してくる😂  みんな気をつけてほしい  

https://twitter.com/in_eltona/status/1788924463201067190  
「猫の恩返し」で思い出したのは、週間ジャンプに「CityHunter」の北条司先生が描いた読み切りで、「しがないフリーカメラマンが一匹の白猫を助けて、白猫が女性に化けて恩返しに来る」といった内容の漫画です。少し切ない最後が印象的でした🥲  
「猫の恩返し」让我想起了《CityHunter》的北条司老师在《Weekly Jump》上发表的一部漫画：一个寒酸的自由摄影师救了一只白猫，白猫为了报恩变成了一个女人。 略带悲伤的结局让人印象深刻🥲  

https://twitter.com/lovegozan/status/1789073491255562289  
💗永遠の憧れ💗   
https://pbs.twimg.com/media/GNQQlZfaAAA0RqW?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNQQnrHaMAA_HWT?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNQQp_sagAA2WJ2?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNQQsM1acAADQZY?format=jpg&name=4096x4096  

https://twitter.com/usapon0808/status/1788813326002360586  
かっこよすぎて「XYZ」イヤリング買ってしまった💕  はぁ原画最高でした😭👏✨  ついでにマンホール巡りしてきたよ🔥🔫  
太酷了，我买了'XYZ'耳环💕，哈哈，原画是最好的😭👏✨，除此之外，我还去了趟訚井之旅🔥🔫。 
https://pbs.twimg.com/media/GNMkOmOboAAt6QZ?format=jpg&name=small  

https://twitter.com/4Wem473i6nk0ql1/status/1788766132054470865  
子どもの頃好きだった北条先生の原画展に行ってきた。シティハンターやキャッツアイの原画が見れて  また漫画読み直そう  新宿じゃないけど、掲示板があった  
我去看了小时候很喜欢的北条老师的原画展。 我看到了《城市猎人》和《猫眼》的原画。 我打算再看一遍漫画。 这里不是新宿，但有一个告示板 
https://pbs.twimg.com/media/GNL5SMzbsAAo5Ov?format=jpg&name=medium  

https://twitter.com/yukimitsu/status/1788874894895452571  
再び北条司展へ！ようやく香ちゃんのプンプンおしおきパンケーキを食べる事が出来ました🥞✨ めちゃんこかわいい💕 今日は自前の画材を持っていたので、冴羽さんｶｷｶｷ🖊 スケブがいっぱい置いてあって、みんな1P丸っと使っていました 笑　後期も楽しみです〜！  
又去看了北条司展！ 终于可以吃到香的pumppun oshi煎饼🥞✨ Mechanko可爱💕 今天我有自己的美术用品，所以我可以看到冴羽 kakikaki🖊 有很多素描展出，每个人都用了1p round lol 我也很期待後期~！   
https://pbs.twimg.com/media/GNNcOCnaIAAPJyz?format=jpg&name=small  
https://pbs.twimg.com/media/GNNcOEqaUAE7YeX?format=jpg&name=small  

https://twitter.com/187_kate/status/1788879302358081823  
GWの記録  吉祥寺の北条司展に行ってきた。絵がとても綺麗だった…眼福。  
GW record 我去吉祥寺看了北条司展。 那些画太美了...太养眼了。  
https://pbs.twimg.com/media/GNNgKajboAE28mW?format=jpg&name=small  
https://pbs.twimg.com/media/GNNgKahaEAA5586?format=jpg&name=small  

https://twitter.com/MonPanier12/status/1788922347971576097  
今日は前期2回目訪問！ひさびさ友達と会えて楽しかったし、お皿チョイス最高でした💕(是非販売を🙏🙏🙏)今回は👓持参したので、繊細な線を存分に堪能できました。後期も楽しみ😊  
今天是我前期的第2次参观！ 好久没见朋友了，很开心，盘子的选择也很棒💕(请出售🙏🙏🙏🙏)，这次我带了👓，可以充分欣赏精致的线条。 期待後期的到来😊  
https://pbs.twimg.com/media/GNOHYqlbMAAKq0L?format=jpg&name=4096x4096  

https://twitter.com/kochiraimaizumi/status/1788567522335461688  
後期展も楽しみ💖 久々に読み返す。 #北条司 #エンジェルハート #シティーハンター  
期待後期展览💖时隔多年再次阅读。    
https://pbs.twimg.com/media/GNJErXVaMAAJvJB?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GNJErXTa0AA4l4j?format=jpg&name=4096x4096  

https://twitter.com/vatesK/status/1788420873680207949  
後期も来るよ、絶対に！ #北条司展  (展示会観覧真っ最中に飛行機の欠航メールが来てびっくりしたけど何となく翌日無事帰国、又来ます。)  
我保证会回来看後期的 #北条司展 (我在观展途中突然收到航班取消的邮件，但第二天我还是安全返回了，我还会再来的。）  
https://pbs.twimg.com/media/GNG_TMwaYAAyu0M?format=jpg&name=900x900  
https://pbs.twimg.com/media/GNG_TMzbYAAA51s?format=jpg&name=small  

https://twitter.com/AoiTakarabako/status/1787951260311896400  
おパヨ～ございます🚘  
Payo ~ 是的  
https://pbs.twimg.com/media/GNAUMPVbcAQ76NY?format=jpg&name=4096x4096  

https://twitter.com/retour1979/status/1788023454270419053  
シティーハンター。個人的には冴羽獠は男の理想。語ると長くなるので一言。香ちゃんがいい女に見えたら大人の男。次、#セーラームーン　だったが、配布中止。#北条司展　見に行って板橋に向かうか。  
城市猎人 就我个人而言，冴羽獠是男人的理想。 说来话长，我只说几句。 如果香像个好女人，他就是个成年男人。 接下来是#SailorMoon#，但发行取消了。 我应该去#北条司展，然后前往板桥。
https://pbs.twimg.com/media/GNBV06ja8AAmfMO?format=jpg&name=900x900  
https://pbs.twimg.com/media/GNBV07KaYAAVV6T?format=jpg&name=900x900  
https://pbs.twimg.com/media/GNBV09AbsAEs9GH?format=jpg&name=900x900  

https://twitter.com/retour1979/status/1788031546068451600  
コミックス27巻の表紙が　#マンホールカード　になっている。  
https://pbs.twimg.com/media/GNBdNdHawAAeNhn?format=jpg&name=4096x4096  

https://twitter.com/cant_awake/status/1788150468897894847  
北条司展にこれまでに巡り会ったお姉さま方と行ってきた。これからも冴羽獠と結婚することは諦めません！(え？)  #北条司展  #GALLERYZENON   #CAFEZENON  
我和迄今为止见过的姐妹们一起去看了北条司展。 从现在起，我不会放弃与冴羽獠结婚的念头！ （诶？）   

https://twitter.com/tpbUsfQyuL66958/status/1787343775616094498  
シティーハンター、キャッツアイの原画が見れるなんて最高過ぎです✨🥰✨  
能看到《城市猎人》和《猫眼》的原画太好了✨🥰✨   
https://pbs.twimg.com/media/GM3ro7fagAAXNQX?format=jpg&name=900x900  


https://twitter.com/memeonmusic/status/1787486828200915323  
\#迷編親訪 #北条司 東京個展實在太有誠意了，不僅有《城市獵人》、《貓眼》等知名作品，還有出道作《我是男子漢》和以鈴木亮平飾演的冴羽獠為基礎繪製的新圖。近170 幅原稿，讓人再次感受到老師的畫功之高強！  
https://memeon-music.com/2024/05/06/hojo-tsukasa/  
@GalleryZenon  
https://pbs.twimg.com/media/GM5ttlbboAAdh0F?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GM5ttlaaoAAaG9L?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GM5ttlbaEAAIwHa?format=jpg&name=4096x4096  


https://twitter.com/hjVA9rC6NSBovln/status/1787491629886210251  
\#北条司 #シティーハンター  #エンジェルハート 18話、凄い。 北条先生の人間ドラマ最高です！
\#北条司 #城市猎人#  #天使心# 第18集，棒极了  北条先生最棒的人间戏剧！  
https://pbs.twimg.com/media/GM5yKhTa4AACjnb?format=jpg&name=medium  

https://twitter.com/GalleryZenon/status/1787291071065850206  
GWもあと1日となりました！ギャラリーに入ってすぐの所にあるミニクーパーがフォトスポットになっております🚗😎お一人の方もぜひスタッフにお声掛けいただければ記念撮影を撮らせていただきますので気兼ねなくお声掛けください📸  
▪︎本日は全時間帯の事前予約チケットに空きがございます。  
購入は下記URLからお手続きをお願い致します。  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1   
▪︎また、当日チケットも店頭でお買い求め頂けます！  
https://pbs.twimg.com/media/GM27ugWaYAE1RM0?format=jpg&name=medium  
お客様のご来場お待ちいたしております🤝   
我们的 GW 还剩一天！ 画廊内的 Mini Cooper 是一个拍照点 🚗😎😎，如果您是一个人，请不要犹豫，请工作人员为您拍照 📸  
▪︎ 当天所有时段的门票均可提前预订。  
如需购票，请访问以下网址。  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1   
▪︎ 您也可以在当天到商店购票！  
https://pbs.twimg.com/media/GM27ugWaYAE1RM0?format=jpg&name=medium  
我们期待在现场见到您🤝  

https://twitter.com/shiba_coco/status/1787318260154950080  
北条司展のコラボメニュー、他のメディアコラボにありがちな  ちんまりした量に申し訳程度のノベルティじゃなくて  結構ガチな量に色んな仕掛けがしてあって更にノベルティ付きなので  香ちゃんパンケーキ、食べきれんからお持ち帰りしたい、なんて人も居た(テイクアウトOK   #北条司展  
北条司展的合作菜单，这在其他媒体合作中很常见。这不仅仅是少量的食物和一些新奇的东西。这是一份相当严肃的食物，花样繁多，新颖别致。还有人因为吃不完而想把香-chan 煎饼带回家（外卖也行）。 #北条司展  


https://twitter.com/tpbUsfQyuL66958/status/1787388561928720723  
吉祥寺駅のシティーハンターのマンホールも見れました✨  最高です🎵😍🎵    
我还在吉祥寺站看到了城市猎人訚井✨ 这是最好的🎵😍🎵  
https://pbs.twimg.com/media/GM4UaJTbkAAAHFl?format=jpg&name=medium  

https://twitter.com/vv_butterfly_vv/status/1787430963775258856  
https://pbs.twimg.com/media/GM46_DIbYAAX6g-?format=jpg&name=4096x4096  

https://twitter.com/iv9fgHWWxc7784/status/1787490468512854376  
全部食べたい🤔  やはり後期も行くべきだな  #北条司#GALLERYZENON #北条司展 #シティハンター #シティーハンター  
我想把它们都吃掉🤔 我还是应该去看展览的後期    

https://twitter.com/pinktanpopo34/status/1787493188091232462  
ライブ後に2度目の北条司展。お目当てのトートバッグは今日の昼ごろ再び完売😭でも展示をじっくり見られてよかった❣️熊本国際漫画祭キービジュアル作成ビデオは繊細な線や彩色の美しさに見惚れて何回りも見ちゃった。CHもAHもKindleで持ってるけどやっぱり紙媒体で欲しいなぁ。#北条司展 #GalleryZenon  
音乐会后的第2次北条司展。 我想要的手提包今天中午又卖完了😭不过我很高兴能亲眼看到展览：❣️，我看了好几遍熊本国际漫画节的主要视觉创作视频，欣赏着细腻的线条和色彩之美。 #北条司展 #Gallery Zenon    
https://pbs.twimg.com/media/GM5zlAGbcAA2bPV?format=jpg&name=4096x4096  

https://twitter.com/hakumei/status/1787628165235638392  
吉祥寺 へ #北条司展 に行ってきました。展示数が少なめなので、後期もいったほうがよさげです。カラー原稿はよいものが多かったので、記念撮影しました。  
我去吉祥寺是为了参加 #北条司展。 展品数量不多，所以最好也参观一下展览的後期。 很多彩色手稿都很不错，所以我拍了一些纪念照。   
https://pbs.twimg.com/media/GM7uQ3YbUAAgtgL?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GM7uQ3fagAAbsfa?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GM7uQ3fasAAyF2-?format=jpg&name=4096x4096  

https://twitter.com/Ellie99639394/status/1787139127579959748  
やっぱりひとりでは解けませんでした😇108ピース目ないよーー！！  
我知道我一个人解决不了这个问题😇，我没有 108 件作品！   
https://pbs.twimg.com/media/GM0wuxNboAAX1I9?format=jpg&name=medium  

https://twitter.com/Saeba_Company/status/1787146657202897121  
北条先生関連がいっぱい笑  
很多与北条老师有关的问题，笑死我了   
https://pbs.twimg.com/media/GM04aQ2aAAEZS39?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GM04aQ3bIAAlnkD?format=jpg&name=4096x4096  

https://twitter.com/MAKOTOs0007/status/1787092058223866345  
こどもの日 #5月5日  こどもみたいな行動原理と無邪気さ と冷徹なプロ対応が混在してる　#シティハンター  #冴羽獠   これほど記憶に残るのは #北条司 さんの絵柄美麗さ故 その中でも、このリボルバー操作の手指の動き丁寧さ説得力は 作中というより日本劇画でも屈指の名シーンでは？ 実写版楽しみw  
儿童节 #5月5日  城市猎人 #冴羽獠 之所以如此令人难忘，是因为  #北条司 的画作非常精美。 其中，这不正是日本漫画中最著名的场景之一吗，不仅是作品，还有他操作左轮手枪时的手部动作所具有的说服力。我很期待真人版。  
https://pbs.twimg.com/media/GM0GwOcbgAATn8T?format=jpg&name=medium  


https://twitter.com/GalleryZenon/status/1786936767419228585  
皆さま、連日ご来場いただき誠にありがとうございます！ギャラリー内には展示や北条先生へのメッセージが書けるスペースを設けております📕  是非皆様の熱い想いを聞かせて下さい😎  事前予約チケット購入は下記URLからお手続きをお願い致します。  
非常感谢您每天光临我们的展览！ 在展厅的空白处，您可以给北条老师留言📕 请让我们听到您热情洋溢的心声📕 请访问以下网址提前购票。   
https://zizoya.hinori.jp/event/calendar  
また、当日チケットも店頭でお買い求め頂けます！  GWも残り2日間となりました。ぜひご来場お待ちいたしております🤝  
您也可以在当天到店购票！ GW 只剩下两天时间了。 我们期待在现场见到您🤝   
https://pbs.twimg.com/media/GMx5hKbbwAAScSK?format=jpg&name=4096x4096  

https://twitter.com/chiroru_special/status/1786944463362981955  
本日快晴☀  こんな日は喫茶キャッツ•アイに行ってアイスコーヒー飲みたいなぁ❤️そんなわけで、GALLERY ZENONに行ってきます✨ #シティーハンター   
今天阳光明媚 ☀ 在这样的天气里，我想去猫眼咖啡馆喝冰咖啡 ❤️ 这就是为什么我要去 GALLERY ZENON ✨ #CityHunter   

https://twitter.com/chiroru_special/status/1787029679540731921  
GALLERY ZENONのお宝❤️プランCで頂きたい〜🤭やっぱりリアルに原画を見られるのはとてもいい！！何度でも足を運んで見たい！！また行きます！！  
我想把它放在 GALLERY ZENON 的宝藏 ❤️ Plan C 🤭 在现实生活中看到原作还是非常不错的！ 我想再去看看！ 我会再去的！！   
https://pbs.twimg.com/media/GMzOBZSakAES3wZ?format=jpg&name=4096x4096  


https://twitter.com/chiroru_special/status/1787065857987989557  
GALLERY ZENONでグッズを購入！シティーハンター トレーディングクリアカードコレクションselection1とselection4を購入〜🥰今回はダブりなしでラッキー😆なんだけど、selection4のガラス越しのキスシーンのカードがほしくてたまらないー❤️  
在 GALLERY ZENON 买了一些东西！ 我买了 City Hunter Trading Clear Card Collection Select1 和 Select4 🥰我很幸运，这次没有买到重复的 😆但是我很想要 Select4 中那张有隔着玻璃接吻场景的卡片 ❤️  
https://pbs.twimg.com/media/GMzu7WaawAAkU0_?format=jpg&name=4096x4096  

https://twitter.com/konomingg/status/1786949188548501773  
さて❗今日はママ上にフードファイト✊してもらいます(笑)🤣💦  金💰は私が出すし😅💦  私の倍は食べれるから問題なし😤  私は…オムライスは…頑張る😅💦  
嗯❗，我今天要和我妈妈来一场食物大战✊🤣💦，我给你钱💰😅💦，你可以吃比我多一倍的量，没问题😤😤，我会......尽力😅💦吃欧姆饭的  

https://twitter.com/ado_mixten/status/1786963312057766305  
全てが素晴らしかったです。生きていてよかった。。。北条先生、ありがとうございます😊後期も来ます！これから、もっこりオムライスいただきまーす🩷    
一切都很美好。 我很高兴我还活着。 谢谢你，北条老师 😊 後期我还会来的！ 我现在去吃块状煎蛋🩷    

https://twitter.com/ado_mixten/status/1786997419454132256  
(ΦωΦ)ﾌﾌﾌ…  大満足。コレクションカードは海原バージョンだけ被りました。保存用にしておこう。  
(ΦΩΦ) 哼哼...我非常满意。 收藏卡上只有海原版。 我会把它保存起来的。  
https://pbs.twimg.com/media/GMywrmPagAAE5Mm?format=jpg&name=4096x4096  

https://twitter.com/konomingg/status/1787031812675649559  
サインはいいのぉ～🤗💕  
无需签名🤗💕  

https://twitter.com/ats0027/status/1787063783359496243  
心が震えた…  
我的心在颤抖...   
https://pbs.twimg.com/media/GMztCbzaUAApe_o?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMztCgWbsAAoFfb?format=jpg&name=900x900  

https://twitter.com/chibi_aya_728/status/1787073678968263084  
明日から仕事だー！！#北条司展 も行けて、原作もちょこちょこ読めて、#映画シティーハンター をたくさん観られて  ウォチパもアフタートークもあって大好きな #シティーハンター で皆で盛り上がれて  楽しい楽しいGWだったー！ありがとうございまぁぁぁす！！明日からもまた観ます！！😂    
我从明天开始工作！ 我去看了 #北条司展，读了一点原著故事，还看了很多 #CityHunter 电影。 我还参加了 #Wochipa 和会后谈话，大家都和我最喜欢的 #CityHunter 玩得很开心！ 这是一次有趣、好玩的 GW！ 非常感谢你们！ 我明天再看一遍!！ 😂      

https://twitter.com/xyz_mizu/status/1787089077768216753  
いいお天気なのにどこにも出かけないのもなんかもったいない気がしてひとり推し活へ。  
虽然天气很好，但我觉得不出去走走太可惜了，于是就一个人去参加了猜谜活动。  

https://twitter.com/meimei12612149/status/1787089267229167926  
やっぱり北条司展、変わる前に再度  じっくり見てこようかな！  
我还是觉得应该回去仔细看看北条司展，免得有什么变化！  

https://twitter.com/ats0027/status/1787112466654372121  
記帳してきた♪  
我记帐了♪  
https://pbs.twimg.com/media/GM0ZTmSbsAAVLfO?format=jpg&name=900x900  

https://twitter.com/Olivier_st/status/1787126350660477320  
GALLERY ZENONさんで開催中の北条司展に行ってきました！やはり何度見ても北条先生の原画は美し過ぎて、逆に印刷に見える…!!(⊃ Д)⊃≡ﾟ ﾟ  いろいろ感想つぶやきたいけど、とりあえず飲食物を。ARオモロイね😊  
我去参观了在 GALLERY ZENON 举办的北条司展！ 北条老师的原画我已经看过太多次了，看起来就像倒过来印的一样.....！ (⊃Д)⊃≡ﾟﾟ。 我想在推特上發表很多感想，但現在我只想吃吃喝喝😊 AR Omoroi 😊。   

https://twitter.com/3939sakutyan/status/1787171360034427096   
北条先生のトーンの削りかたが美しすぎる😻 #北条司展  
北条先生的tone的削方式太美了😻#北条司展
https://pbs.twimg.com/media/GM1O4WRaQAEcDn2?format=jpg&name=4096x4096  

https://twitter.com/3939sakutyan/status/1787171999187640514  
そーなんですね。と、しかいえない。  
是这样啊。只能这么说。  
https://pbs.twimg.com/media/GM1PdXKbIAAMPbl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GM1PdkTaYAA0kSJ?format=jpg&name=4096x4096  

https://twitter.com/esteechanart/status/1786563039082557542  
昨日の展示会のおかげで、昔読んでいた漫画を思い出した😲 主人公の女医がめちゃかっこいい〜 もう一度読んでみよかな👀✨  
昨天的展览让我想起了以前看过的一本漫画😲，主人公是一位女医生，非常酷，我应该再看一遍👀✨。   
https://pbs.twimg.com/media/GMslm-DbAAAMAvc?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMslm-CacAAMMVJ?format=jpg&name=4096x4096  

https://twitter.com/GalleryZenon/status/1786569123067261235  
本日『北条司展』マグカップ（集合）が入荷致しました☕️💫  
▪︎本日5月4日13時の回までの予約チケットは完売いたしました。  
▪︎当日券をご利用のお客様は店頭にてお買い求め頂けます。  
（※混雑状況により少々お待ち頂く可能性がございます）  
▪︎14時以降の予約チケット購入は下記URLからお手続きをお願い致します。  
お客様のご来場を心よりお待ちいたしております☺️  
北条司展览 "马克杯（集体）已于今天到货。☕️💫  
▪︎5月4日下午13:00前的预约票已售完。  
▪︎门票可在展览当天在商店购买。  
(根据会场的拥挤程度，您可能需要稍等片刻）  
▪︎请访问以下网址购买 14:00 之后的预约票。  
我们期待在展会上见到您。☺️  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1  
https://pbs.twimg.com/media/GMsrJYtaQAAobNm?format=jpg&name=4096x4096  

https://twitter.com/yasutaka0818/status/1786546317717897420  
はてなブログに投稿しました。今回は、前回に引き続き、GALLERY ZENON（ #ギャラリーゼノン ）で2024年6月23日（日）まで開催中の『 #北条司展 』［前期］を観てきた話について書いています。 #武蔵野市 #シティーハンター  #はてなブログ #はてなブログ  GALLERY ZENON（ギ…  
发表于 Hatena 博客。 这一次，我将继续上一篇文章，讲述我在#GALLERY ZENON举办的『 #北条司展 』［前期］的参观情况，展览将持续到2024年6月23日（周日）
https://yasubeblog.hatenablog.com/entry/2024/05/04/090000

https://twitter.com/yasutaka0818/status/1785489348789780722  
はてなブログに投稿しました。今回は、GALLERY ZENON（ #ギャラリーゼノン ）で2024年6月23日（日）まで開催中の『 #北条司展 』［前期］を観てきた話について書いています。 #武蔵野市 #シティーハンター #はてなブログ  GALLERY ZENON（ギャラリー ゼノン）で2024年6月23日…  
发表于 Hatena 博客。 这次我写的是我参观在#GALLERY ZENON举办的 『 #北条司展 』［前期］的情况，展览将持续到2024年6月23日（周日）。  
https://yasubeblog.hatenablog.com/entry/2024/05/01/110000  

https://twitter.com/jinmyoji/status/1786588782323814897  
この手の遊び溢れる注意書きが好き♪(*´ω｀*)#北条司展   
我喜欢这种俏皮的音符......#北条司展  
https://pbs.twimg.com/media/GMs9AxcacAAPyz_?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMs9AzFboAAJluQ?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMs9A-wbQAAO8ht?format=jpg&name=4096x4096  

https://twitter.com/jinmyoji/status/1786589048997601332  
シティーハンターといえばこれ(笑) #北条司展  
说到《城市猎人》，这个(笑) #北条司展  
https://pbs.twimg.com/media/GMs9RE9bsAAm84Z?format=jpg&name=medium  

https://twitter.com/jinmyoji/status/1786592183589720457  
原画原稿だけじゃなくて、こういう小道具あるのスゴくうれしい  #北条司展  
我很高兴有这样的道具，而不仅仅是原画原稿 #北条司展  
https://pbs.twimg.com/media/GMtAHVsbcAA7ieh?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMtAHV2bwAARVni?format=jpg&name=900x900  

https://twitter.com/jinmyoji/status/1786594153612378478  
ホワイトはこうやって入れるもんだって言うのは知ってますし入ってる原稿も沢山見てきましたが、やっぱりスゴいなぁって思うんですよ。#北条司展  
我知道白色应该这样放，我也见过很多手稿中都有白色，但我还是觉得很神奇。 #北条司展  
https://pbs.twimg.com/media/GMtB5i6bMAAQX7T?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMtB5jXaAAAi0xz?format=jpg&name=4096x4096  

https://twitter.com/jinmyoji/status/1786595989861842945  
【女子に100tハンマーで殴られる(模擬可)】の実績を解除しました #北条司展  
[被女孩用重达 100 吨的锤子殴打（可模拟）]成就已解锁#北条司展  

https://twitter.com/jinmyoji/status/1786600775982129408  
北条司展 のあとは吉祥寺駅周辺のデザイン #マンホール 巡り  https://city.musashino.lg.jp/kurashi_tetsuz  
北条司展结束后，在吉祥寺站附近进行了一次设计#manhole之旅  
https://pbs.twimg.com/media/GMtH4IYbEAA-98O?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMtH4KhbYAA5_E6?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMtH4RLbMAApTe2?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMtH4WhbAAE2WQM?format=jpg&name=900x900  

https://twitter.com/jinmyoji/status/1786606691272004068  
マンホールの続きともらってきた #マンホールカード それに #北条司展 の入場特典。シティーハンターも良いけどキャッツアイ派(・∀・)  
继续#manhole和我得到的#manhole卡片。 还有 #北条司展的入场特典。 《城市猎人》不错，但我更喜欢《猫眼》(・∀・)  
https://pbs.twimg.com/media/GMtNUEAa8AA3iWg?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMtNTzkbUAAHZFF?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMtNUE_asAAoJz-?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMtNUCcbAAAUkul?format=jpg&name=small  


https://twitter.com/inoyuki06/status/1786644287658270905  
北条司展 行ってきました！シティーハンター好きで伺いましたが、展示されている原画、作品じっくりじっとり見てきました。絵が細かくて、カラーの絵に関しては「これはどう塗っているんだろう…」と考えながらガン観。すごい。  「作品に触らないで！」が遊び心あって思わずニヤけた。好き。  
我去看了北条司展！ 我参观展览是因为我喜欢《城市猎人》，我仔细观看了展出的原画和作品。 这些图画非常细致，我在看彩色图画时发现自己在想 “他们是怎么画出这些的......”。 太神奇了  “别碰作品！“ 俏皮可爱，让我忍俊不禁。 我喜欢它。   

https://twitter.com/rG3Ms47fH3TuNmy/status/1786730408945811684  
当選してゼノンさんからいただきました！ありがとうございます☺️  北条先生の生み出したキャッツアイやシティーハンターその他全ての作品が美しく、優しく心温まるストーリーが大好きです❤️  綺麗な原画が見れる原画展まだまだ何度も行きます！  
我中奖了，并从 Zenon 收到了奖品！ 谢谢 ☺️  我喜欢北条老师创作的《猫眼》、《城市猎人》和其他所有作品，因为它们都有着美丽、温柔和温暖人心的故事： ❤️  我还会参加更多的展览，在那里我可以看到美丽的原画！   
https://pbs.twimg.com/media/GMu91RHbQAAuaFb?format=jpg&name=900x900  


https://twitter.com/chiroru_special/status/1785283285146538372  
神谷明さんのブログより  2000年の年賀状を北条司先生に書いていただいたとのこと。  これ、めちゃめちゃいいですねー   
从神谷明的博客上看到，他为北条司先生写了一张 2000 年的新年贺卡。 这真是太好了！    
https://pbs.twimg.com/media/GMaZr7GagAA_8Ui?format=jpg&name=large  

https://twitter.com/7GTOvYlvunmM2mu/status/1785565180744946040  
海坊主もきていたのかな  お土産はバーボンだろう  
我不知道海坊主是否也来过这里，我敢肯定它们一定带了波旁酒作为纪念品  
https://pbs.twimg.com/media/GMeaD1haoAAaH1z?format=jpg&name=medium  



https://twitter.com/AoiTakarabako/status/1785781022644383928  
おパヨ～ございます🌄  
https://pbs.twimg.com/media/GMheX0cawAAG-8T?format=jpg&name=medium  

https://twitter.com/5Rwph0EEiX83793/status/1785947015396073870  
まず紙にカラス＆トンボを描いて、それを油性ペンでトレースしました✨  大成功✨  雨で落ちなきゃいいなぁ…  
我先在纸上画了一只乌鸦和蜻蜓，然后用油性笔描摹✨，非常成功✨，希望它不会在雨中脱落...    

https://twitter.com/____goldenanime/status/1786392175682478278  
https://pbs.twimg.com/media/GMqKNTEXoAEIwii?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMqKNc9WUAA08YL?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMqKN27WEAA8aNi?format=jpg&name=4096x4096  

https://twitter.com/makomii/status/1786455302449283182  
真夜中 に #Netflix #CITYHUNTER  #シティーハンター  #お墓参り #多摩霊園  の 帰りにたまに  #深大寺 によって  #深大寺蕎麦 🥢ある時  #鈴木亮平 が #地元 ❓ #地元ロケ番組 でいた  めちゃくちゃ 口調も穏やかで  #優しさが滲み出る顔  でした❣️  
半夜# netflix # cityhunter #城市猎人去扫墓# #摩墓地的偶尔回来时# # #🥢深大寺荞麦面根据深大寺铃木亮平#当地❓#在当地拍摄外景的乱七八糟的节目  语气也很温和，脸上透着温柔❣️  
https://pbs.twimg.com/media/GMrDndWaQAI4Gof?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMrDndUasAQMYIm?format=jpg&name=small 
https://pbs.twimg.com/media/GMrDndWasAAdGOM?format=jpg&name=small  


https://twitter.com/lymreg/status/1786535619407016062  
シティーハンターNetflixの冴羽獠のアパートの外観コンセプト   
City Hunter Netflix 对冴羽獠公寓外观的构思  
https://pbs.twimg.com/media/GMsMpLFXkAAzaHt?format=jpg&name=4096x4096  

https://twitter.com/GalleryZenon/status/1786207016773734652  
本日グッズ各種入荷致しました😎大人気のシティーハンタートートバッグも再再入荷しております💫ぜひご来場お待ちいたしております🤝事前チケット購入は下記URLからお手続きをお願い致します。  
各种商品已于今日到货😎深受欢迎的城市猎人手提袋也已恢复库存💫我们期待在现场见到您🤝请访问以下网址提前购票。   
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=  
また、当日券は店頭でもお買い求め頂けます。スタッフまでお気軽にお声掛けくださいませ。  
您也可以在活动当天到店购票。 如需了解更多信息，请随时咨询工作人员。   
https://pbs.twimg.com/media/GMnh0AZbAAACCe1?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMnh0AZaEAAUNg-?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMnh0AYbQAAxLKX?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMnh0AaaMAARqxr?format=jpg&name=4096x4096  

https://twitter.com/kayoty2015/status/1785288528055943351  
北条司展の2回目、前回オムライスだったので、今回はパンケーキ😋  金平糖が思ってたより大きくて、口に含む→北条司ブレンド珈琲飲む、で程よい甘みを楽しんだものの、食べごたえあり過ぎて肝心のパンケーキが半分でお腹一杯に💦  金平糖、侮り難し〜  
北条司展的第2次,上次蛋包饭,所以这次是薄煎饼😋金平糖我想像大,包括→北条司混合咖啡喝,享受适当的甜味,但吃过很重要的薄煎饼一半一杯肚子💦金平糖,很难有人~  

https://twitter.com/esteechanart/status/1786312567545274729  
北条司 展✨  
https://pbs.twimg.com/media/GMpBzHBb0AAz1kw?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMpBzHBbMAEfct2?format=jpg&name=4096x4096  

https://twitter.com/alisa9121/status/1786324776581038363  
やっと行けた！  
终于去了!  
https://pbs.twimg.com/media/GMpM1tSaAAEXHbX?format=jpg&name=small  

https://twitter.com/chiroru_special/status/1786328372504912046  
コアミックス 公式オンラインショップ・ゼノンショップでは『#シティーハンター』全巻セットを買うと、このアクリルプレートが先着特典でついてくるー🥰どこで買うか悩むなぁー💦  
在 Coamix 官方在线商店和 Zenon 商店，如果您购买了整套 #城市猎人#，就可以获得这个亚克力盘，先到先得 🥰，不知道我会在哪里买到它💦。   

https://twitter.com/chiroru_special/status/1786344072338366470  
GALLERY ZENONでシティーハンターグッズがたくさん販売されていたから、ついつい買ってしまいました🥰缶バッチもコレクションカードも大好きな絵柄がきて嬉しい❤️  そして念願のマステゲットしましたー！！買いすぎ反省、でも幸せにょんw  
在 GALLERY ZENON 有很多《城市猎人》的商品在打折，所以我买了很多🥰 我很高兴，我喜欢的图案既有罐头徽章，也有收藏卡。❤️ 而且我还买到了心仪已久的杰作！ 对不起，我买得太多了，不过我很开心w   
https://pbs.twimg.com/media/GMped0MasAAUpNq?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMped4EaQAEVg9k?format=jpg&name=900x900  

https://twitter.com/GqAXhZyMYc1eRjV/status/1786188911892680752  
おはようございます。昨日は『北条司展』に行ってきました。良かった♪  
早上好。 昨天我去看了『北条司展』很不错 
https://pbs.twimg.com/media/GMnRWG9aAAAe4mF?format=jpg&name=4096x4096  

https://twitter.com/jinmyoji/status/1786277676829003895  
来ました(・∀・)  
我的天，他们来了(・∀・)  
https://pbs.twimg.com/media/GMoiEsvbwAArMQz?format=jpg&name=4096x4096  

https://twitter.com/jinmyoji/status/1786282229100097707  
ほんそれ。  
就是这样。  
https://pbs.twimg.com/media/GMomNn7b0AAVzPp?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMomNogbUAA_A67?format=jpg&name=4096x4096  

https://twitter.com/mxnanase/status/1786390211012026850  
吉祥寺のGALLERY ZENON（@GalleryZenon）で開催中の北条司(先生)展に行ってきました！ 北条先生のデビュー作からキャッツアイ、シティハンター他たくさん原画が展示されてました。原画は写真に撮っても良かったので写真も何枚か撮らせていただきました。このイラスト素敵でした。好き。 #北条司展    
我参观了位于吉祥寺的 GALLERY ZENON（@Gallery Zenon）举办的北条司（老师）作品展！那里展出了许多原画，从北条的处女作到《猫眼》、《城市猎人》等。 由于原画允许拍照，所以我也拍了一些照片。 这幅插图非常棒。 我很喜欢。#北条司展      
https://pbs.twimg.com/media/GMqHRIja8AALC_C?format=jpg&name=4096x4096  


https://twitter.com/cityhunterAA/status/1786479818583785640  
5/3 ライブ二本の後に吉祥寺GALLERY ZENON（@GalleryZenon）の北条司展へ  
北条さんの画はまさに芸術  
デビュー作から代表作や読み切り作品の原画を間近に凝視できる貴重な機会を堪能  
話題の実写版シティーハンターとあわせて楽しめ、北条ファンとしては嬉しい企画でした  
後期展示にも期待    
5/3 听完两场现场音乐会后，去吉祥寺 GALLERY ZENON（@Gallery ZENON）看了北条司展。 北条先生的画是真正的艺术！ 在这里，您可以近距离欣赏他的处女作原画、代表作和朗读作品，享受难得的机会。 对于北条的粉丝来说，这是一个很好的项目，他们可以与广受欢迎的真人版《城市猎人》一起欣赏。 期待後期的展览！   
https://pbs.twimg.com/media/GMrZ68aWoAATWrT?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMrZ68eXEAATkRS?format=jpg&name=4096x4096  


https://twitter.com/sa_dango/status/1785660883181740397  
北条司展  もう開催してるの分かってたけどGW進行でまだ前期行けてないんだよなぁ😂  あとは調子悪猫を誰かに見てもらう算段をつけねば...っ！！  
北条司展 我知道已经开放了，但是前期因为 GW 的进展一直没能去😂现在我只需要想办法找人帮我照看我那只状态不好的猫...... 我会找人来看我的猫的..！  

https://twitter.com/zkatsu19/status/1785890243943739623  
直行直帰で吉祥寺 #GALLERYZENON オープン記念企画 #北条司展 へ。私の初恋は #獠ちゃん だった✨毎週父と弟と70円ずつ出し合って #週刊少年ジャンプ を買って、#CITYHUNTER を奪い合うように読んだなぁ。懐かしすぎる。原画、生原稿、素晴らしかった、来てよかった…！帰ったらコミックス読み返そ。  
直接回家参加吉祥寺 #GALLERYZENON 开幕纪念项目 #北条司展 。 我的初恋是獠-chan ✨ 每个星期，我和父亲、哥哥都会每人花 70 日元购买 #Weekly Shonen Jump（《周刊少年 Jump》），像互相比赛一样阅读 #CITYHUNTER。 我太怀念它了。 原画、原稿，太神奇了，我很高兴我来到了这里.....！ 回家后我会再看漫画的    

https://twitter.com/pzp_ori/status/1785907151611011318  
小さい頃にキャッツアイ１話を読んで、当時の他の絵柄と比べて衝撃を受けた覚えがある。話も面白く、ファンになった。ジャンプを離れて以降追えていなかったが、久しぶりに作品に触れられて良かった。いつもならクリアファイルを買うところを、パンフレットを買ってしまった。嫁さんごめん。  
我记得小时候读过《猫眼》的第1话，与当时的其他作品相比，我感到非常震惊。 故事很有趣，我就成了它的粉丝。 离开《JUMP》后，我就没有再关注过这部作品，但能在很长时间后接触到这部作品，感觉还是很不错的。 我买了小册子，而我通常会买透明文件。 对不起，妻子。  

https://twitter.com/shiba_coco/status/1785907824608080301  
画力がパァァンしてる人ばかりで  
ワイみたいなニワカは寄せ書きなんて恐れ多くて書けましぇーん😂😂😂😂  
他们都很会画画。 
像我这样的宅男都不敢写😂😂😂😂    

https://twitter.com/shiba_coco/status/1785910940762026478  
遅い昼飯😋(尚女性でこれ頼んでる人ほぼ居なかったw  
晚间午餐😋（几乎没有女性点餐）  

https://twitter.com/shiba_coco/status/1785933732488393066  
いやぁよかった…非日常感満足ホクホクでしたありがとう。。あとでちまちま画像うｐるけど、画の展示はなるべく避けるぜ…同じ絵かきとして、生で見て欲しいから。  
我很高兴......这是一次非凡的经历，我非常高兴。 我稍后会发一些照片，但我会尽量避免展出这些画作......作为一名画家，我希望人们能亲眼看到它们。   

https://twitter.com/swallowataru/status/1785941288396951793  
オタ活してきました。描き下ろしの冴羽獠も昔の絵も美しいし、カッコよかったな  
我一直在宅。 新版的冴羽獠和旧版的画作都很美，很酷  
https://pbs.twimg.com/media/GMjwIZMaUAAlxCs?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMjwHFnbsAArCHX?format=jpg&name=4096x4096  


https://twitter.com/tokyobossa/status/1785948264963158517  
館内に入った途端、コヒーの「CITY HUNTER〜愛よ消えないで〜」が流れて気分は最高潮！ #北条司展  
一进入馆内，コヒー的《城市猎人--爱啊请不要消失》就会响起，心情也随之达到最高点！#北条司展   

https://twitter.com/shiba_coco/status/1785954642104619311  
これは展示のクーパーだけど、展示会場の数十m手前くらいに本物の赤いクーパーが路駐してあった。ファンの方のだろうけど道路マナーは守ろうぜ(:3_ヽ)_  でもお得な気分になったw  
这是一辆展出的 Cooper，但在展厅前几十米的路上停着一辆真正的红色 Cooper。 我知道这可能是车迷的，但我们还是要保持道路礼仪(:3_ヽ)_，不过我觉得我买到了好东西  

https://twitter.com/shiba_coco/status/1785955433167081863  
おさわり禁止。  
禁止触摸。  
https://pbs.twimg.com/media/GMj8_4Ab0AAlAjp?format=jpg&name=small  
https://pbs.twimg.com/media/GMj9AGPbQAAWY4A?format=jpg&name=small  

https://twitter.com/shiba_coco/status/1785955894410596434  
先生のRay-Ban。  
https://pbs.twimg.com/media/GMj9a9BaIAAl6bq?format=jpg&name=4096x4096  

https://twitter.com/jagaco_123/status/1785958034923311582  
パンケーキ1回くらい食べたいよな、、せめてパンケーキだけでも、行っちゃおうかな、でもちょい高いんだよな〜、 #北条司展  
我至少想吃一次煎饼，至少是煎饼，我会去的，但有点贵...... #北条司展    

https://twitter.com/yutamaru_92/status/1785986404771852594  
本日の推しごと報告① #北条司展 に行ってきました🙋‍♂️  デビュー前の読み切りから、キャッツ❤️アイ、シティーハンター等の原稿が展示されていましたが、少年ジャンプを一生懸命読んでいた頃の原稿に胸が躍りました🥰  繊細な筆致に目を奪われつつ、年を経るごとに豊かになるキャラの表情が印象的でした🤣  
今天的猜猜看报告（1）我去看了#北条司展 🙋‍♂️ 里面展出了他出道前的手稿，有《Cat's❤️ Eye》、《城市猎人》等，能看到自己当年苦读《SHONEN JUMP》时的手稿，我很激动🥰，细腻的笔触让人惊艳，人物的表情，随着年龄的增长，也让人印象深刻🤣。 细腻的笔触令人惊叹，人物的表情随着年龄的增长也变得越来越丰富，令人印象深刻🤣  
https://pbs.twimg.com/media/GMkUMMhbcAAEhym?format=jpg&name=4096x4096  

https://twitter.com/chibi_aya_728/status/1786061430170718484  
一枚の絵の、部分と全体の行き来をするのが好きなので空いてる時間帯に(邪魔にならないように☺️)。すーっと惹き込まれて、本当に時間を忘れて魅入ってしまい、周りに誰もいなくなってるなと思ってたら、いつの間にか次の回の人たちが周りにいて「ふぁっ？！」ってあわてた😂  
我喜欢在空闲时间（☺️，以免打扰您）在一幅画的局部和整体之间来回穿梭。 我被深深地吸引住了，我真的忘记了时间，被迷住了，我以为周围没有人，还没等我反应过来，下一轮人就围了上来，我就像 "哇什么？"惊呼道😂  
https://pbs.twimg.com/media/GMldZhca8AAYoGO?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMldZscbwAA67o9?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMldZ9kaQAAf3-q?format=jpg&name=4096x4096  

https://twitter.com/STEP2525/status/1785718553796284647  
https://pbs.twimg.com/media/GMgljv_agAAtSM7?format=jpg&name=large  

https://twitter.com/magu529/status/1785750025743786164  


https://twitter.com/GalleryZenon/status/1785483597144817708  
皆さま、連日ご来場いただき誠にありがとうございます！ギャラリー内にちらほらフォトスポットを設けております📸大きなメインビジュアルはもちろん、階段横にひっそりと設置されたこちらがスタッフおすすめのフォトスポットです😳💕ぜひ素敵なお写真を撮ってみてくださいね💫事前予約チケット購入は下記URLからお手続きをお願い致します。  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1  
また、当日チケットも店頭でお買い求め頂けます！  ぜひご来場お待ちいたしております🤝    
非常感谢您每天来访！ 我们在展厅内设置了几个拍照点 📸大型主视觉以及楼梯旁的这个安静的拍照点都是我们的工作人员推荐的 😳💕请尽量拍些好照片 💫请访问以下网址提前购票。 请访问以下网址提前购票。  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1  
您也可以在当天到店购票！ 我们期待您的光临🤝。   


https://twitter.com/GalleryZenon/status/1785484854265798757  
客様からよくお問い合わせを頂いております、「シティーハンター　トートバッグ」ですが、現在、在庫がまだございます！ぜひこの機会にGETしてくださいね😎事前予約チケット購入は下記URLからお手続きをお願い致します。  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1  
また、当日チケットも店頭でお買い求め頂けます！ご来場お待ちいたしております。    
https://pbs.twimg.com/media/GMdRAEhbQAAeRYE?format=jpg&name=4096x4096  
我们经常收到顾客询问的 City Hunter 手提包目前仍有存货！ 请趁此机会购买一个 😎请访问以下网址提前购票。  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1  
您也可以在活动当天到店购票！ 我们期待您的光临！   

https://twitter.com/sue_Ramone/status/1785314811628408852  
昨日行った #北条司展 では、伝言板があったのだけど、鈴木亮平さんの実写版 #シティーハンター の続編を求める声が多くあって、原作ファンからも愛されているなぁと感じた一コマ🤭   
在我昨天去的  #北条司展，有一个留言板上有很多人要求铃木亮平的《城市猎人》真人版拍续集，这让我感受到原作的粉丝们对这部电影的喜爱🤭  
https://pbs.twimg.com/media/GMa2W6GasAA-2dD?format=jpg&name=4096x4096  

https://twitter.com/kou_rocker333/status/1785605024263143732  
行ってきたぜ吉祥寺！昨今のシティハンターの流行に乗ってミーハー心から行ってみたけど、ものすごく素敵で楽しかった✨グッズも買えた♪  G.Wのお出かけにオススメ😊  あと実はスタッフさん達のお心遣いが丁寧で気兼ねなく楽しませてもらいましたありがとうございました  
我去了吉祥寺！ 我是跟着最近的《城市猎人》热潮而去的，但那里真的很不错，很有趣✨我还买了一些商品😊我推荐你去G.W.郊游😊  事实上，工作人员非常细心，让我毫不犹豫地尽情享受，非常感谢   
https://pbs.twimg.com/media/GMe-S39bEAEyJnC?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMe-S3-bwAA3a-A?format=jpg&name=4096x4096  


https://twitter.com/misadoala/status/1785589328997138679  
先月吉祥寺にopenしたGALLERY ZENON(めっちゃ間違えそうになるがZEONではない)の #北条司展  ストーリーもおもしろいけど北条司先生の描く絵(特に女性の)がめちゃくちゃ好きなので見に行けてよかった☺️楽しかった☺️  GALLERY ZENONさんの今後の企画にも期待☺️    
上个月在吉祥寺 GALLERY ZENON（不是 ZEON，虽然我差点搞错）举办的北条司展。 故事很有趣，但我真的很喜欢北条司的画作（尤其是女性），所以我很高兴去看了。☺️ 我很喜欢。☺️ 也期待 GALLERY ZENON 今后的项目。☺️    


https://twitter.com/tomaru02/status/1785565034195902715  
今日は #北条司展 にやって来た♪  チケットが時間指定制なのでゆっくり見られた😊  パンケーキ、甘さ控えめでうまし！。。。お昼食べて来ちゃったので、もっこりオムライスはまた今度食べたいw😋  
今天，我来到了 #北条司展 😊 因为门票是计时的，所以我可以慢慢来 😊 煎饼很好吃，而且不会太甜。 我吃了午饭，下次想尝尝大块煎蛋 😋   


https://twitter.com/dddash_saico426/status/1785617767439478906  
お気に入りの原画は絞りきれない位あるんですけども  
ほんとに眼福でした✨(2色原稿も好きです)  後期も絶対行きます!!!!!    
我甚至无法缩小我最喜欢的原画的范围。  
这真是让我大开眼界✨（我还喜欢双色手稿），我一定会回来看後期的展览!!!!!    
https://pbs.twimg.com/media/GMfJ499akAAB0wf?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMfJ5OZawAA1f_p?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMfJ5Y9aYAAxGA7?format=jpg&name=4096x4096  

https://twitter.com/chamiguri_43ome/status/1785624162159169678  
雨の吉祥寺、ギャラリーゼノンにて鑑賞。もうねえ、直撃世代なあたしゃにはたまらんよ♪デビュー作〜諸々短編迄、原画をガン観&写真撮りまくりですよ(喜)カフェで香のパンケーキ&北条先生ブレンドの珈琲をば。もう、気分はもっこりですぜ♪(笑) 後期展示もマストで行きますとも!! #北条司展  
https://pbs.twimg.com/media/GMfPs-ab0AAjU8J?format=jpg&name=4096x4096  


https://twitter.com/sa_dango/status/1785660883181740397  
北条司展  もう開催してるの分かってたけどGW進行でまだ前期行けてないんだよなぁ😂  あとは調子悪猫を誰かに見てもらう算段をつけねば...っ！！  
北条司展 我知道已经开放了，但是上学期因为 GW 的进展一直没能去😂现在我只需要想办法找人帮我照看我那只状态不好的猫...... 我会找人来看我的猫的..！   

https://twitter.com/tamamaro0317/status/1784981581679173695  
https://pbs.twimg.com/media/GMWHSadawAAX5t4?format=jpg&name=360x360  
https://pbs.twimg.com/media/GMWHSe2aEAAo599?format=jpg&name=small  


https://twitter.com/zuou_ankle/status/1784800449289412827  
水滴が本物みたいだし、オシャレな色彩バランスとか、使用画材なんなんだろうこれ〜アナログでこれかよ〜と楽しませていただきました。作品知らなくても楽しい。キャッツアイ看板を写ルンですで撮ったら存在し得るかんじになった。  
水滴看起来很真实，我喜欢它时尚的色彩平衡和所使用的艺术材料--模拟？ 即使你不了解作品，也会觉得很有趣。 当我用摄影镜头拍摄猫眼招牌时，它看起来就像真的一样。   
https://pbs.twimg.com/media/GMTii53aAAAHUiB?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMTii7racAAd4LU?format=jpg&name=4096x4096  

https://twitter.com/Erimasha5/status/1784812205114007787  
3時過ぎに新宿近くで待ち合わせなのでやって来たあ〜😆　亮平君ファンも　#シティーハンター　ファンも　#北条司  先生ファンも　もっこりファンにもここは堪らないと思う〜😍　#鈴木亮平　#GALLERYZENON  #吉祥寺  #北条司展  
https://pbs.twimg.com/media/GMTtOIRaYAAlVUD?format=jpg&name=900x900  


https://twitter.com/kawa0107dais/status/1784864040868557206  
北条司展行ってきた！！司先生のシティーハンターの絵まじで良かった！！  
我去看了北条司的画展！ 司先生的《城市猎人》画得非常好！   
https://pbs.twimg.com/media/GMUcXu5WcAAsbFW?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMUcXu5W8AAbszk?format=jpg&name=4096x4096  

https://twitter.com/chibi_aya_728/status/1784863218466496555  
何度観ても北条先生の生原画に心動かされます😭外を通る人が口々に「あ！シティーハンター！今ネトフリでやってるよね！」とか、「すごかった！」って言っていて、関心度も評価も高くて嬉しくなっちゃった！原作もネトフリ版も是非観て欲しい！    
无论看多少遍，我还是会被北条老师的现场原画所感动😭 外面路过的人都会说："啊！ 城市猎人！ 正在Netflix上播放呢！” “太棒了！” 看到大家这么感兴趣，这么喜欢，我真是太高兴了！ 希望大家能同时观看原版和 Netflix 版！     

https://twitter.com/chibi_aya_728/status/1784864861110813170  
もっこりしょにプンプンしてる香ちゃん😇  プンプンおしおきパンケーキと北条司ブレンド。  
https://pbs.twimg.com/media/GMUdHorWgAAEM8b?format=jpg&name=4096x4096

https://twitter.com/megenna_968/status/1784941181304201609
5/1分 #2D今週のオタ活報告 その1  #ラジオエフ  有楽町で5/12まで開催中の「#きまぐれオレンジロード40周年記念展」、吉祥寺で6/23まで開催中の「#北条司展」、渋谷で6/23まで開催中の「#村田蓮爾（ムラタレンジ）原画展」に行って来ました😆  （続く）  
5/1 #2D 本周御宅族活动报告 第一部分 #Radiof 我参加了在有乐町举办的 #Kimagure Orange Road 40 周年纪念展，展期至 5 月 12 日；在吉祥寺举办的 #Hojo Tsukasa 展，展期至 6 月 23 日；在涩谷举办的 #Renji Murata（村田系列）原创艺术展，展期至 6 月 23 日😆（续）    

https://twitter.com/megenna_968/status/1784941199431999846  
5/1分 #2D今週のオタ活報告 その3  #ラジオエフ  「#北条司展」は二期に分けて開催するそうですが、今期ではデビューから現在迄の生原稿が展示されていました。また、シティハンターのドラマが公開中という事で、素敵なイラストも展示されていましたよ😆  （続く）   
5/1 #2D 本周宅男报告 第三部分 #Radiof #北条司展将分两期举行，本期展出了他从出道到现在的原始手稿。 此外，目前正在播放的《城市猎人》电视剧也展出了一些精彩的插图😆（续）   
https://pbs.twimg.com/media/GMVijw7aEAA62VF?format=jpg&name=4096x4096  


https://twitter.com/masaaki_front/status/1784941490818682923  
ネトフリでシティーハンターも好評なので、武蔵野市で開催されている北条司展へ。WEBで予約制みたい。その分、混雑するエリアもあるけど、見るエリアを替えれば問題なし。  
Netflix 上的《城市猎人》也很受好评，所以我们去了武藏野市的北条司展，似乎需要通过网络预约。 有些地方因此人很多，但如果换一个想看的地方，就没问题了。   
https://twitter.com/suzumeradio/status/1784962169031041069  
今日は吉祥寺‼️チャイブレイクに行って、北条司展に行って、アトレとキラリナをぐるぐる歩きまわりました。  
今天，我去了吉祥寺 ‼️ Chai Break，参观了北条司展，并在 Atre 和 Kirarina 附近转了转。   


https://twitter.com/chiroru_special/status/1784555876294259094  
GALLERY ZENONに置いてあるお皿どれもステキすぎる〜😆販売してくれたら買いたいなぁ❤️  
GALLERY ZENON 的所有盘子都太漂亮了😆，如果出售的话，我想买下来❤️  
https://pbs.twimg.com/media/GMQEHKKaUAA3yQ0?format=jpg&name=4096x4096  


https://twitter.com/L5PRil43iYIL67z/status/1784379360331653607  
北条司展にも行ってきた！！シティハンターはもちろん、短編作品「桜の花咲くころ」が個人的に堪らなかった😇  画集買って、100tハンマーでモンハンポーズしてきた🔨  
我还去看了北条司展！ 当然是《城市猎人》，但我个人无法抗拒短篇故事《樱花盛开时》😇，我买了画册，还拿着 100 吨重的锤子摆了个姿势🔨  


https://twitter.com/ayuyu_0818/status/1784403122967945673  
北条司展17時からのだからそれまで暇だなー。映画も良い時間帯のやってないし🥺せっかくだからシティーハンターのマンホール見てこようかな。吉祥寺早めに行って。   


https://twitter.com/ayuyu_0818/status/1784426438961369241  
ねこまんまおかわりも確か凄い良い話だったよな。記憶が曖昧なんだけどジャンプでリアタイしてた気がする、、しっかりシティーハンターのトートバッグで行って来る✌️  見かけた人いたら是非お声掛けをば！    

https://twitter.com/akikonuno/status/1784443003496738967  
最っ高に幸せ〜😆💓  生遼サマを拝めるなんてぇぇぇ😭  とっても綺麗なギャラリーで、店員さんもとても優しくて、コラボカフェのスウィーツも美味しィィ✨  大好きな北条司先生の世界、そして愛する遼サマ達も拝めて、本当に本当に幸せすぎましたー❣️  
最开心的😆💓 能亲眼看到RyoSaeba，我真的好开心😭 画廊太美了，工作人员太亲切了，合作咖啡馆的甜点也很好吃✨ 能看到我最喜欢的北条司老师的世界和我最爱的RyoSaeba，我真的好开心好开心 ❣️    
https://pbs.twimg.com/media/GMOdbJ8bIAA5lGX?format=jpg&name=4096x4096  


https://twitter.com/KOHARU36991352/status/1784490006196949017  
北条司展行ってきました✨  もっこりオムライスめちゃ美味しかった😋小分けで使ったお皿売ってたら買った…。X情報でA3の硬質ケース必需ッ！って書いてたので持参したけどマジで持ってきてよかった😭    
我去看了北条司展✨蛋包饭太好吃了 😋如果有卖的话，我一定会买我用过的小份的盘子...... X 信息说你需要一个 A3 硬盒！ 我真的很高兴我带了它😭。    
https://pbs.twimg.com/media/GMPIK8pa4AAhkvr?format=jpg&name=4096x4096  


https://twitter.com/akikonuno/status/1784485400477913182  
オラの依頼はどれでしょう😆  
https://pbs.twimg.com/media/GMPEAiAbcAAf-jY?format=jpg&name=4096x4096  

https://twitter.com/dogdays_lion/status/1784491758564630616  
コラボメニュー全て頂きましたの！北条先生のスペシャルブレンドコーヒーもすっごく飲みやすくてビックリでしたわ☕   
我们吃了合作菜单上的所有菜品！ 我们还惊讶地发现，喝北条老师的特调咖啡☕竟然如此容易。    

https://twitter.com/bisco7777/status/1784514147629277387  
美しすぎました…✨  こんなに美しい漫画を描く北条司先生はやっぱり神様だと改めて実感しました(^^)  ゆっくり見られたし幸せな時間でした😊  また行こうっと😆  
太美了......✨ 我再一次意识到，画出如此美丽漫画的北条司，终究是个大神(^^) 我能够慢慢地看到它，那是一段快乐的时光😊 我们再来一次😆  
https://pbs.twimg.com/media/GMPeKMraYAAyNfr?format=jpg&name=4096x4096  


https://twitter.com/mnk_plus/status/1784521852322505198  
皆さま、このハンマー持てますよ‼️  
每个人都可以拥有这把锤子！！  
https://pbs.twimg.com/media/GMPlKmZbAAAzSbi?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMPlKmbbUAADZ_S?format=jpg&name=4096x4096  

https://twitter.com/KOHARU36991352/status/1784525825360158731  
北条司展 #シティーハンター   #マンホールカード  無事にマンホールカードGET✴️  入場特典描き下ろしのやつ当たった！  着いてきてくれた母ありがとう。  
北条司展 #城市猎人 #沙井卡 我成功拿到了井盖卡 ✴️，我赢得了入场奖励抽奖！ 感谢我妈妈的到来。   
https://pbs.twimg.com/media/GMPoxkuaQAAj4sW?format=jpg&name=4096x4096  


https://twitter.com/ayuyu_0818/status/1784545430094987542  
北条司展 行ってｷﾀ━(ﾟ∀ﾟ)━!たまらん空間でした🤤 #シティーハンター #キャッツアイ 好きな方は行くべし！前半はもうすぐ終わるけど、後半もあるらしいから又来なきゃ！！ちゃんと「喫茶キャッツアイ」の看板まで作ってあるこだわりよう。その2階には瞳の等身大    
我去看了北条司展！ 如果你喜欢 #城市猎人 # 猫眼，你一定要去！ 上半场快结束了，不过听说还有下半场，所以我还得再来！ 他们甚至还做了一个 “猫眼咖啡馆 ”的招牌。 二楼有一个真人大小的瞳雕像   
https://www.instagram.com/p/C6TbFWMy2KS/?igsh=c2R6ZmltNTE4ajFj  

https://twitter.com/Sunao_Love/status/1784555830123401509  
我が家に　#北条司　大先生の直筆のサインをお迎えすることにいたしました。家宝にさせていただきます。『みんな』も良かったのだけど、『まずはカオリン、そして、パイソンは獠の相棒』って思いが強く、そちらにしてみました。  
我决定将 #北条司 大师的亲笔签名带回家。 我们将把它作为传家宝。 我本想得到 “Minna”，但我想先得到 “獠的伙伴首先是Kaori，然后是”，所以就选了这个。   

https://twitter.com/505mimo/status/1784565245501169907  
Netflix版のシティハンターの書き下ろしイラストやキャッツ・アイの看板や100tハンマーなどが好みでした！やっぱりキャッツ・アイの原画がすごく良かったです！  
我喜欢新编的《城市猎人》网剧版插图、猫眼标志和 100t 锤！ 我还是非常喜欢《猫眼》的原画！   
https://pbs.twimg.com/media/GMQMoWLbMAApUWi?format=jpg&name=4096x4096  

https://twitter.com/jagaco_123/status/1784564481521307657  
ファンの方のメッセージ書く場所に色んなイラストがあって凄かった！皆さんサラサラと描いててすごい！#北条司展  
有这么多不同的插图，粉丝们可以在上面写下自己的信息！ 大家写得如此流畅，令人惊叹！  

https://twitter.com/da1104ht0603/status/1784564155854549385  
行ってきましたギャラリーゼノン🖼️😊カフェでお腹いっぱいになり😙展示作品で改めて北条先生の絵の細やかさに目を見張る👀✨  #galleryzenon #ギャラリーゼノン #北条司展 マンホールカードももらって、その後新宿も見てきましたよー🔫😄  
我去了Gallery Zenon 🖼️😊😊我在咖啡馆吃饱了😙展出的作品再次展现了北条的绘画细节👀✨ #galleryzenon #Gallery Zenon #北条司展我还买了一张井盖卡，然后去了新宿🔫😄  

https://twitter.com/jagaco_123/status/1784570598250971556  
母とふたりで行って特典が2人ともまさかのシークレットでした、、笑まぁでもすごい綺麗な絵なので満足😍あとファイルがやっぱり素敵！！#北条司展  
我和妈妈一起去的，我们俩都得到了秘密特典，笑死我了，不过这幅画很美，所以我很开心😍，文件也很不错！  
https://pbs.twimg.com/media/GMQRf89aUAALgJQ?format=jpg&name=4096x4096  


https://twitter.com/Botan_2015/status/1784577216602644497  
吉祥寺へ移動して  #北条司展 へ  カラー原稿のホワイト入れの所や  繊細な線が間近で見られるとは眼福💕  #冴羽獠 のもっこりオムライスはARに進化してるー！あと、北条司先生のオリジナルブレンドコーヒー☕️とても美味！   
彩色手稿的粉饰和近距离细腻的线条让人眼前一亮💕 #Saeha的大块煎蛋已经进化成了AR！ 另外，北条司的原味混合咖啡 ☕️ 也非常美味！    

https://twitter.com/SIN0079/status/1784720850446606705  
北条司先生の展示会へ。シティーハンターはもちろんキャッツアイや短編集の原画がたくさん展示されれて、ファンとしては幸せな空間だった！！実写版ポスターには鈴木亮平さんのサインも入っていたり、併設のカフェにはコラボフードもあった！後期もあるからまた行きたい！！  
去看了北条司的展览。 作为一名粉丝，这里展出了许多《城市猎人》的原画、《猫眼》和短篇小说，是一个令人开心的空间！ 铃木亮平在实景海报上签了名，展览附带的咖啡厅里还有合作美食！ 我还想再去，因为还有第二季！  

https://twitter.com/mktcova/status/1784015459811291381  
吉祥寺 の #GALLERYZENON で開催されている北条司展を見てきました  繊細で美しい画力なのに #もっこり  →#100tハンマー となるの対比も面白いですね♪  イラスト集を買って帰路へ  
我参观了位于东京吉祥寺的 #GALLERYZENON 的北条司展  精致美丽的艺术品与 #Mokkori → #100t 锤子之间的对比也很有趣♪我买了一本插图集就回家了   

https://twitter.com/sachi_park1979/status/1784028262399983789  
行ってきたー！！生原稿はもちろんだけど、注意書きが良い！全部同じじゃないし使い方が上手い！（笑）（写真のは地味目なやつ）それとこの眼鏡の男性が好みすぎる。あと、あと！藤原紀香さんからのお花が凄かった！あんな色の胡蝶蘭初めて見た！（胡蝶蘭だよね？）  
我也经历过！ 当然是原稿，但也有好的笔记！ 他们并不都是一样的，而且都用得很好！ (笑)(照片上的是个清醒的人。)而这个戴眼镜的人太对我的胃口了。还有！ 藤原纪香的花真漂亮！ 我从来没见过这种颜色的兰花！ (蝴蝶兰，对吧？）    
https://pbs.twimg.com/media/GMIkP62aUAA6qC3?format=jpg&name=4096x4096  


https://twitter.com/thelowtierchara/status/1784071618370912446  
今日はこれから吉祥寺にて #北条司展 です  前回の原画展から2年経ったことに驚きつつ、そのほとんどがキャッツアイに割かれていたので、今回はその名の通り全作品を網羅しているとしたら楽しみしかないです  時間が足りるか心配  
今天我们要去吉祥寺看#北条司展。 我很惊讶，距离上一次原画展已经过去两年了，其中大部分都是关于猫眼的，所以我只期待这次展览能像名字一样涵盖他所有的作品。 我担心我们是否有足够的时间   

https://twitter.com/Sunao_Love/status/1784072999391658259  
2度目の　#北条司展　オムライスとデザートをチョイス  食べ切れるかな（笑）    
第二次#北条司展：奥姆饭和甜点选择。 我不知道自己能否吃得完（笑）。   

https://twitter.com/pinktanpopo34/status/1784073146196533623  
本日のミッションその1 #北条司展 #ギャラリーゼノン #シティーハンター #原画が美しすぎて涙出る 後期も来ます！  
今天的任务 1 #北条司展 #Gallery Zenon #城市猎人 #原画太美了，我都要哭了，我还会回来看後期展览的！   

https://twitter.com/shiratama_shi/status/1784160476898935212  
シティーハンターだけでなく、「こもれ陽の下で…」も「RASH！！」も短編も大好きなので、原画を至近距離で拝見することができて胸熱。あのクオリティの絵を週刊連載で描いていたなんて、とてつもない。  
我不仅喜欢城市猎人，还喜欢「こもれ陽の下で…」和「RASH！！」 和短篇小说，所以能近距离看到原画真是令人激动。 他能在周刊连载中画出如此高质量的图画，真是了不起。   

https://twitter.com/sakananosurimin/status/1784204554994868356  
前期2回目！ゆっくり見れて嬉しい...後期もチケット取らな！藤原紀香さんからのお花あって2度見した🤣  
前期第二次来！ 很高兴能在闲暇时看到它...... 我得去买下半场的票！ 藤原纪香的花我看了两次🤣。   

https://twitter.com/yt_2hd/status/1784226005332488380  
原画を拝見できた、、北条先生の描く線は本当にきれい。作品は、わたしが生まれる以前の年代  だけど今見てもすごくロマンに溢れてておもしろい。大人っぽいタッチがすてき。最高です。GALLERY ZENONの今後の展開も楽しみ！落ち着いた空間ですごくよかった。    
我看到了原画，北条老师画的线条真的很美。 这些作品是在我出生之前创作的，但即使是现在看来，也非常浪漫有趣。 成熟的韵味非常美妙。 我期待着 GALLERY ZENON 今后的发展！ 空间很好，很安静。  
https://pbs.twimg.com/media/GMLYGBPbcAA8F5R?format=jpg&name=4096x4096  


https://twitter.com/shiratama_shi/status/1784170812641321385  
描き下ろしイラスト2点が素敵すぎたのではるばる足を運んで良かった！ うち1点はNetflix版を元にしたイラストだから冴羽獠なんだけど鈴木「獠」ちゃんなのよ。服の質感とか半端なくて本当に絵が上手い人なんだなぁと改めて実感。リアルで読んでいた懐かしいジャンプも！#シティーハンター #北条司展    
我很庆幸自己一路走来，因为有两幅新插图好得令人难以置信！其中一幅插图是根据 Netflix 版本创作的，所以是冴羽獠，但却是铃木「獠」。 衣服等的质感实在是太棒了，这让我再次意识到他的画技真的很棒。 我还怀念以前在现实生活中看过的Jump ！       
https://pbs.twimg.com/media/GMKl5NbaoAAbEDh?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMKl5NcbkAAlCOs?format=jpg&name=4096x4096  


https://twitter.com/sakananinaru/status/1784171460539638114  
吉祥寺GALLERY　ZENON『北条司展』俺、生粋の海坊主ファン。おとといNetflixのシティハンターも観たばかり。生原稿。美しいと言っては、感動でため息が出る。大好き過ぎてジンとくる。ありがとうございますじゃ言い足りない。#北条司展  
吉祥寺画廊 ZENON『北条司展』我是一个地地道道的海波迷。 前天刚在 Netflix 上看了《城市猎人》。 原稿。 我说太美了，感慨万千。 爱到心痛。 谢谢你还不够。  
https://pbs.twimg.com/media/GMKmeqAakAANtgc?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMKmeqNaMAEPOgR?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMKmesWbEAADxxd?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMKmes2bIAAq4q2?format=jpg&name=4096x4096  


https://twitter.com/hitoyuzuriha/status/1784231764707479764  
コピーじゃなくて写植✨  私も仕事で写植ペタペタ貼ってた。 あの大きな絵用の写植機、漫画家さんのお家にあったのかな🤔  てか、印画紙で表情だけ書き直してるのすごい!   
不是copy，而是复印✨ 我以前也把复印件平放在我的作品上。我不知道漫画家家里是否有复印大照片的机器🤔 🤔 嘿，他们就这样在相纸上改写了表情，真是太神奇了！    
https://pbs.twimg.com/media/GMLdVQnbAAAsWUv?format=jpg&name=4096x4096  


https://twitter.com/mnk_plus/status/1784253786384666815  
一昨日見てきました～😆  北条先生の絵はカッコイイのにどこか可愛くてセクシーで何年経っても魅了させられます。先生の絵をいつでも見られるように本は手に取れる場所に置いています📕✨  後期の展示も楽しみにしていますね🥰    
我前天才看到 😆北条老师的画很酷，但又莫名地可爱和性感，这么多年过去了，仍然让我着迷。 我把这本书放在一个随时可以拿起来的地方，这样我就可以随时欣赏他的画作📕✨，我很期待展览的後期🥰     

https://twitter.com/pinktanpopo34/status/1784249553077805480  
獠ちゃんをお持ち帰り☺️  #北条司展  #ギャラリーゼノン  #シティーハンター  2回目行く時にはトートあるといいなぁ…    
把獠带回家 ☺️ #北条司展 #Gallery Zenon #City Hunter 我希望第二次去的时候能有一个手提包...    
https://pbs.twimg.com/media/GMLtgoRaQAECLgg?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMLtgoObkAAy3KE?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMLtgoPaYAEYE6Y?format=jpg&name=900x900  


https://twitter.com/thelowtierchara/status/1784254110860263543  
北条司展 前期  いやいや、もうファンには堪らない空間でした  圧巻の原画に魅了され時間を忘れて長時間の滞在に  ギャラリー内を何周もしちゃいましたw  
北条司展 前期  我被铺天盖地的原画深深吸引，忘记了时间，久久不愿离去。 我在展厅里转了一圈又一圈   
https://pbs.twimg.com/media/GMLxp-haUAAh1Fb?format=jpg&name=900x900  
https://pbs.twimg.com/media/GMLxp-4a4AAoEFj?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMLxp_kbIAA_-rW?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMLxqAwbYAAdcSZ?format=jpg&name=4096x4096  


https://twitter.com/thelowtierchara/status/1784254119794077935  
北条司展 前期  同じ画を何度も見ていると、次々と発見や気づきが出てくるのが面白くて食い入るように見つめてしまいます  それが原画展の楽しみでもあるのでね…  
北条司展 前期 当你反复观看同一幅画时，你会发现一个又一个有趣的发现和领悟，这也是原画展的乐趣之一...    
https://pbs.twimg.com/media/GMLxqkcaoAAuHxS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMLxqllboAAQU3h?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMLxqnYbsAEMXsY?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMLxqo9bsAAMAQf?format=jpg&name=4096x4096  


https://twitter.com/thelowtierchara/status/1784254130124681514  
北条司展 前期  ガラスの十代の頃に戻っている気分で当時を懐かしみながらたっぷりと堪能できました  貴重な機会をありがとうございます  また後期お邪魔します  
北条司展 前期  我仿佛回到了我的玻璃少年时代，在回味那段时光的同时，我也非常喜欢这个展览。 感谢你们给我这次宝贵的机会  
https://pbs.twimg.com/media/GMLxrJeakAE5-_L?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMLxrKLaIAAgxWP?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMLxrLvbYAAt_hm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMLxrMSbsAAbwC9?format=jpg&name=4096x4096  



https://twitter.com/BShin5/status/1784368021299237337  
いざ、吉祥寺の#ギャラリーゼノン へ‼️原画展、楽しみや〜🤩 11:00〜11:59の回に  赤いキャップとパンツの輩が  いたらそれはあっしです🤭    
现在是时候去吉祥寺的 #GalleryZenon ‼️ 画展、我很期待 🤩，如果你在 11:00-11:59 的时段看到一个戴红帽子、穿红裤子的人，那就是我 🤭   

https://twitter.com/shiratama_shi/status/1784364620050059543  
入場特典のカード開封。この絵は個人的に嬉しすぎる。靴底の質感や立体感が凄い。そして靴底に萌える自分も大概頭おかしい。  
打开入场奖励卡。 这张照片让我个人太开心了。 鞋底的质感和立体感令人惊叹。 我也有点疯狂，喜欢在鞋底上蹭来蹭去。  
https://pbs.twimg.com/media/GMNWKClaUAARbPA?format=jpg&name=4096x4096  


https://twitter.com/xx_otaku_yxx/status/1783867066354962485  
漫画もアニメも映画もエンジェルハートも実写版も好きだから待ちに待ってた  
我一直在等待它，因为我喜欢漫画、动画、电影、天使心和真人版  
https://pbs.twimg.com/media/GMGRo_hbcAAMOoy?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMGRo_macAAFXqb?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMGRo_lbIAAMxvB?format=jpg&name=4096x4096  


https://twitter.com/KXV396ElYRrIIee/status/1783959141591216251  
CAT’S EYEで杏里さんのものまねをしました。エアロビ風の掛け声ワンチューワンチューをやりたかっただけなのに思いの外、杏里さんに似てしまったので驚いてます。ものまねの神に感謝します😌✨  #杏里  #キャッツアイ  #ものまね  ここをクリック↓  
https://dropbox.com/scl/fi/4vobuvn2bqni38zb8hptt/1-_1.mp3?rlkey=43vezkab8imotct6vfh1lk8gv&dl=0  
我模仿了 CAT'S EYE 的 杏里。 我只是想做一个有氧式的喊叫，但我很惊讶我出乎意料地像杏里先生/女士。 感谢上帝的😌✨模仿 #杏里 #キャッツアイ #ものまね 点击这里↓https://dropbox.com/scl/fi/4vobuvn2bqni38zb8hptt/1-_1.mp3?rlkey=43vezkab8imotct6vfh1lk8gv&dl=0      
https://dropbox.com/scl/fi/4vobuvn2bqni38zb8hptt/1-_1.mp3?rlkey=43vezkab8imotct6vfh1lk8gv&dl=0  
https://pbs.twimg.com/media/GMHlYRtaQAA5lPM?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMHlYSVaIAALHZP?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMHlYThbMAAaL52?format=jpg&name=4096x4096  


https://twitter.com/AoiTakarabako/status/1783965357260333177  
おパヨ～ございます😌  #シティーハンター  
https://pbs.twimg.com/media/GMHrB48bgAA-Quv?format=jpg&name=4096x4096  


https://twitter.com/yangwenli_hk/status/1783740698652766419  
https://pbs.twimg.com/media/GMEetL1bMAAfEwl?format=jpg&name=small  

https://twitter.com/SargassoSpace/status/1783638369093976437  
北条司先生展示にて撮影  
この展示、とてもオススメです。原画は本当に間近でみるほど素晴らしく、脇役キャラも｢美しい｣と思えます😊イラストのメイキング動画も、先生の作品愛が強烈に伝わってきます。  
摄于北条司先生展览  
我强烈推荐这个展览。 近距离观看原画真的很棒，连配角都很'美'😊制作插图的视频也有力地传达了老师对作品的热爱。   

https://twitter.com/shiro_r2u2/status/1783656078250827868  
幸せの時間でしかなかった　#北条司展  ホワイトの飛ばし方とか、ベタの塗り、トーンの削りなんて細かい仕事まで全て見えるの。本当に本当にありがとうございます…また展示内容変えて開催されるようなのですぐ行くね！  
这真是一段快乐的时光#北条司展，我看到了所有的细节工作，比如如何跳过白色，如何使用纯色，如何锐化色调。 非常感谢你们所做的一切......我很快就会回来的，因为展览将以不同的内容再次举办！   
https://pbs.twimg.com/media/GMDRs_XbQAAOaCD?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMDRs_cbAAARo-M?format=jpg&name=small  

https://twitter.com/crayon_lovekcal/status/1783700867117621437  
北条司展 に行ってきたよ  普段シティーハンターで騒いでいるけど、「桜の花 咲くころ」と「こもれ陽の下で…」が大好きなの展示されている原画が容赦なくて人の形を保つのに必死だった  
我去看了北条司展 平时都是城市猎人，但我喜欢《盛开的樱花》和《在阳光下》......因此，展出的原画让我不舍，我努力保持人形   

https://twitter.com/hana12190620/status/1783765065617088799  
今行かないと絶対逃す🤣GALLERY ZENONで開催されてる #北条司展 へ。SNS確認してないから、誰でも撮れるエリアの写真だけ。年明け？までカフェ＆飲食店だったので2019年PEの時にめちゃくちゃ通った場所。愛車のminiも残留🍻今回は前期後期で別れての開催で展示物が変わっちゃうかもなのでね。  
如果现在不去，就一定会错过 🤣去 GALLERY ZENON 观看 #北条司展 我没有查看社交媒体，所以只拍了这里的照片，任何人都可以在这里拍照。 直到今年年初，这里还是一家咖啡馆和餐馆？ 我在 2019 年 PE 期间经常去的一个地方，因为在那之前它还是一家咖啡馆和餐厅。 我心爱的MINI也留下来了🍻，因为这次展览会分上下学期，展品可能会有变化。   

https://twitter.com/Dory_happi/status/1783791076907917541  
GW本番前にtlで見た行きたい所に行く  今回のきっかけ #北条司展  Netflixでの実写版も公開され  #シティーハンター 界隈が賑わってる😆  100tハンマーに潰されたミニクーパー  一眼みたくチケットポチり🎫  LOギリでコラボメニューのオムライスを頂き(…後程)  実写版書き下ろしお出迎えされ店内堪能☺️  
去我想去的地方，这是我在 GW 展之前在 tl 上看到的  这次#北条司展  的原因  Netflix 上的真人版也已经上映，而且  #城市猎人#区域热闹非凡😆  Mini Cooper 被 100 吨重的锤子砸得粉碎。  像单反一样爆票🎫  在 LO Giri 享用了合作菜单中的蛋包饭（......稍后），并看到了新的真人版《城市猎人》☺️  
https://pbs.twimg.com/media/GMFMhfTbYAAsmRS?format=jpg&name=4096x4096  


https://twitter.com/TKD_ENMT/status/1783611258434208170  
あの馬もっこりをこんな風に使うなんて  
我真不敢相信他们会用这种方式来使用马皮  

https://twitter.com/hamavha/status/1783611393859825942  
アクションがすごく見応えがあったのでので、是非映画館でも上映してほしいです  
試写会行けなかった人も同じ気持ちの人いるんじゃないかなって思ってる(特に私)  
动作场面真的很壮观，所以我非常希望它能在电影院上映  我相信那些没能去看电影的人也会有同样的感受（尤其是我）。   


https://twitter.com/9ni5_j_keynote/status/1783614772359098616  
香役もすごいよかった…ちゃんと香だったよ…特にアクションのところの掛け合いがテンポ良くて楽しい  朝ドラのあの子と同じだなんて信じられない〜    
她饰演的香很棒......她真的是香......台词，尤其是獠的动作戏，节奏明快而有趣。 我不敢相信她和晨间剧里的那个女孩是同一个人......      


https://twitter.com/kze_anego/status/1783614953926340943  
このタグ🤣カオリのハンマーは  実写では再現出来ないよね？🤔   
这个标签🤣Kaori 的锤子无法在真人版中复制，对吗？ �  


https://twitter.com/8010cola/status/1783614965045440566
1人でパーティ  
独自狂欢  
https://pbs.twimg.com/media/GMCsWxcacAA067L?format=jpg&name=4096x4096  

https://twitter.com/KT_Exxx/status/1783616059637149859  
原作(漫画)ファンとして声を大にして言いたいのは鈴木亮平さんの目の表情が、めちゃくちゃ冴羽獠だったこと！！敵を見る目、香を見る目、依頼人を見る目、冴子を見る目、そして槇村を見る目、全部が原作で見た記憶があるような、そんなリョウちゃんだった😭😭😭  
作为原著（漫画）的粉丝，我想大声说，铃木亮平的眼神太像冴羽了！ 他看敌人的眼神，看香的眼神，看委托人的眼神，看冴子的眼神，看槇村的眼神，都是我记忆中原作里的那种獠😭😭😭  

https://twitter.com/AoiTakarabako/status/1783368692111376653
北条司展の前期、めっちゃ良かった😌🎶グッズ等は後期に来た時の楽しみにしておこう👝Netflixのポスターのヤツ、マジで写真みたい(笑)👏  
前期北条司展真的很不错😌🎶，等我回来的时候，我会期待後期的👝，Netflix 海报上的那张真的很像照片👏   

https://twitter.com/AA6585726651410/status/1783522961779294370  
最高の1日だった  
最美好的一天  
https://pbs.twimg.com/media/GMBYrfqacAAb0OU?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMBYrfobMAAQwTk?format=jpg&name=4096x4096  

https://twitter.com/Chm_069/status/1783304570585944268  
後期もいきたーい☺︎︎というかいきます(> ·̫ <⸝⸝)      
我也想去後期☺︎︎，或者说，我会去的(> ·̫ <⸝⸝)   
https://pbs.twimg.com/media/GL-SC9JakAACAC_?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GL-SC9Ia4AAC0qv?format=jpg&name=4096x4096  


https://twitter.com/chibi_aya_728/status/1783328055618121837  
小さい頃からずっと好きで、実写版まで観られて、もっこりで盛り上がって  北条先生の生原画に感動して  たくさん笑ってたくさん泣けて胸を熱くするなんて😭  シティーハンターを好きで本当によかったぁぁぁってなる😇  
我从小就喜欢这本书，甚至还看过真人版，非常有趣，也很有莫西干的味道  北条老师的现场原画让我感动不已  我笑了很多次，哭了很多次，让我心潮澎湃😭  我真的很高兴我喜欢《城市猎人》😇  
   

https://twitter.com/blackswamp0703/status/1783342682359927028  
めちゃくちゃ良かったです✨  私は世代的にWJ連載終了直前〜木漏れ日の下でを読んでましたね  ちょうどいい所に光がｗ😂パワーチャージできたので帰って家事頑張ります  
简直棒极了 ✨  我读的是我们这一代人的WJ连载～《阳光少女》的完结篇  正好是轻W的地方😂我已经充好电，回家做家务了  


https://twitter.com/1101RK1/status/1783376461971529847  
今日は  北条司展2回目行ってきたよ🤗初日は混んでたので今日改めて写真も沢山撮ってゆっくり堪能してきました🥰入場特典 シークレット めちゃ欲しかったやつがきたので嬉しい〜😆これから夕飯の買い物行って16時～PCの前でスタンバイする    
今天我去看了第二次北条司展 🤗第一天人很多，所以我拍了很多照片，今天又看了一次 🥰我很高兴，因为我得到了我非常想要的入场奖励秘籍 😆我要去买晚餐，从 16:00 开始在我的电脑前待命      

https://twitter.com/onchandesu187/status/1783378053428912447  
好きを補充してきました。吉祥寺、住みたすぎる。  
我补充了我的喜好。 吉祥寺，我太想住在那里了。   
https://pbs.twimg.com/media/GL_U2BAasAADssD?format=jpg&name=small  
https://pbs.twimg.com/media/GL_U2BHbcAAjSPY?format=jpg&name=4096x4096  


https://twitter.com/yukimitsu/status/1783378782570807458  
念願の北条司展へ行って来ました！北条先生の生原稿、美し過ぎた…… 展示用の描き下ろし冴羽さん＆香ちゃんもめちゃんこ良いですー！優しい色合いがステキ💖💕 後、先生の連載当時のネームを初めて見ました！これはレア過ぎる〜！  
我去看了期待已久的北条司展！ 北条老师的原始手稿太美了。...... 新为展览绘制的冴羽小姐和香子也非常不错！ 而且，我还是第一次看到北条老师连载漫画的故事！ 这太难得了！   
https://pbs.twimg.com/media/GL_VjRTakAAi4KQ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GL_VjTkbMAAm86B?format=jpg&name=4096x4096  

https://twitter.com/Cecil_Moonlight/status/1783381687445094563  
北条司展 映画公開前に行ってまいりました◥ (*´◒`*) ◤  シティーハンター劇場版1作目からハマった超新参なのですが、思わずこれを機にシティーハンター以外の北条司先生の作品にも触れてみたいって思うとても素敵な展示会でした…!!展示内容はもちろん、カフェのご飯やドリンクも、(本当にコラボカフェなの?!)ってレベルのクオリティで美味しすぎたし、ところどころにある小物や装飾まで凝っていて本当に愛情たっぷり最高の展示会でした!!!  #映画シティーハンター   このあと配信開始楽しみすぎる!!!    
我在电影上映前参加了北条司展◥(*´◒`*)◤我是从第一部电影开始接触《城市猎人》的超级新人◤◥★☆☉♀♂ ，但这次展览非常棒◤◥★☆☉♀♂ ，让我想借此机会接触一下北条司除了《城市猎人》之外的其他作品.....！ 不仅是展览的内容，还有咖啡馆里的食物和饮料（这真的是合作咖啡馆吗？） 咖啡馆里的食物和饮料都是如此优质美味，就连这里的小玩意儿和装饰品都是如此精心制作，这真是一场充满爱意的盛大展览！ #城市猎人 这部电影将于今年晚些时候发行，我非常期待！     

https://twitter.com/05051999senmomo/status/1783384486220382668  
素敵な企画を有難うございます✨  北条司展、気になりますね😆  北条先生の作品を感じられるのは幸せな時間ですね🍀  良いご縁でありますように😊    
感谢您的精彩项目✨   我对北条司展很感兴趣😆 能感受到北条先生的作品🍀，对我来说是一段幸福的时光  祝愿我们的合作愉快😊  
    

https://twitter.com/truewave0310/status/1783406840388505988  
２回目の北条司展に来館。Netflixも昨日入ったし、展示作品みて、気持ち盛り上げて夜に実写版拝見します！楽しみ😊   
我是第2次来参加北条司展览了，昨天Netflix来了，看了展览作品后，晚上要去看真人版，很兴奋！ 我很期待😊    


https://twitter.com/eijikita/status/1783474191993344045  
キングダムの原先生が  スラムダンクの井上先生のアシスタントをしていた事は知っていたけれど、その井上先生が北条司先生のアシスタントをしていたという事を今初めて知った。マジか！って声出た。28日に北条司展を観に行く！楽しみ過ぎる！    


https://twitter.com/yukimitsu/status/1783474786238390457  
なんか良い感じに撮れたギャラリーゼノンの看板  
Gallery Zenon 的标志，被拍得很美 
https://pbs.twimg.com/media/GMAs3axbUAAb1SK?format=jpg&name=medium  

https://twitter.com/Ko_ichi_32/status/1783485373655740564  
「北条司展」鈴木亮平主演のNetflix映画『シティーハンター』も爆当たり間違いなしの原作者でもあり、ジャンプ黄金期を飾った立役者と言っても過言ではない！そんな北条司先生の原画展、これは参加しないという選択肢はない🤔  
「北条司展」毫不夸张地说，北条司也是铃木亮平主演的 Netflix 电影《城市猎人》的作者，这部电影一定会很火爆，他还是《JUMP》黄金时代的关键人物！ 这样一位北条司先生的原画展，不参加不行🤔   

https://twitter.com/Ko_ichi_32/status/1783488470599766035  
「北条司展」鈴木亮平主演のNetflix映画『シティーハンター』も超話題👍🏻そして、ジャンプ黄金期を彩った立役者である北条司先生の原画展なんだよ🤩これは是が非でも観に行きたい！そりゃそうだよ😆  #北条司展 #GALLERYZENON #ゼノンコミックス宣伝    
铃木亮平主演的Netflix电影《城市猎人》的 「北条司展」也是超级热门话题👍🏻，是《JUMP》黄金时代的领军人物北条司的原画展🤩，无论如何我都要去看看！ 没错 😆#北条司展#GALLERYZENON#Zenon漫画宣传      

https://twitter.com/anmibear/status/1783495952667730081  
今日購入したものたち✨  
我今天买的东西✨  
https://pbs.twimg.com/media/GMBAHVRaoAAZpiR?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMBAHVQawAA0io5?format=jpg&name=4096x4096  

https://twitter.com/082ga181/status/1783517450669322430  
北条司展へ行ってきました‼️原画を見られて感涙です  服のシワ、髪の靡き方、ベタの艶やかさ、カラーの美しさ、書き込みの細かさ…書き出すとキリがないです  素晴らしかったです  後期も行きます  #シティーハンター  
我去看了北条司展‼️，看到原画时感动得热泪盈眶！ 衣服上的皱纹、头发的摆动、纯色的光泽、色彩的美感、文字的细节......我可以写的东西无穷无尽。 它太神奇了！ 我会回来看後期的 #城市猎人    
https://twitter.com/dareyame/status/1783602747797053541  
https://pbs.twimg.com/media/GMChNkrasAAQLHM?format=jpg&name=4096x4096  

https://twitter.com/dareyame/status/1783603238807449630  
\#シティーハンター ←これすごい‼️‼️『北条司展』おととい見に行ったけどもっかい行くぞー！ネトフリは日曜日にゆっくり観ます  慌てて観たくないの  #北条司展 #吉祥寺 #GALLRYZENON #シティーハンター #キャッツアイ象のハナコの近くのマンホールも見に来てね  
\#城市猎人←这太神奇了 ‼️‼️ 我前天去看了『北条司展』，但我还要再去！ 星期天我会慢慢看 Netflix。 我不想急着去看 来看看大象花子附近的沙井吧！   
https://pbs.twimg.com/media/GMChsOfasAEqdny?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMChsOhasAIlHvd?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GMChsOqa4AAeU1n?format=jpg&name=4096x4096  

https://twitter.com/chryokaori/status/1730250459942896067  
扉絵シール、閉店数日前に一度は在庫終了したのにすぐに補充してくれて、最終日まで残っているとは思わなかったので、おかげで記念になりました！  
门口的贴纸在关门前几天一度断货，但很快就补齐了，没想到直到最后一天还没卖完，多亏了他们，我才得以纪念这次活动！   
https://pbs.twimg.com/media/GAMVntibQAA5Xdu?format=jpg&name=900x900  


https://twitter.com/kime__suki/status/1776092080496107799  
ZENON SAKABAの改装もだいぶ進んだようどけど、表にケンシロウが立っていないと なんか変( ˘•ω•˘ )  
ZENON SAKABA的装修有了很大进展，但没有 Kenshiro 站在前面有点怪怪的( ˘•ω•˘ )  


https://twitter.com/kakitarepanda/status/1781948507055931424  
カフェゼノン　#シティーハンター　食べ終わったお皿にセリフがあるって聞いたのですが、これってシティーハンターじゃないよね🤔誰かご存知ありませんか？もう一個撮るの忘れたけど、時代劇っぽかったから花の慶次じゃないかなぁ？  
Cafe Zenon #城市猎人 听说吃完的盘子上有一行字，但这不是城市猎人🤔 有人知道吗？ 我忘了拍另一张，但看起来像一部年代剧，也许是《花之庆次》？   


https://twitter.com/GalleryZenon/status/1782948228692115964  
「北条司展」グッズ入荷情報💫  
▪︎シティーハンターアートタイル（car）¥990-税込  
▪︎シティーハンタークリアファイルセット¥1000-税込  
が本日入荷致しました！  ※予約チケットをお持ちの場合もグッズのお取り置きはできませんのでご了承ください。チケットのご購入はこちらから💁‍♀️  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1   
また、本日も当日券の販売を店頭にてお買い求め頂けますので、よろしければぜひご来場ください☺️お待ちいたしております。  
「北条司展」'商品有库存💫。 
▪︎ 城市猎人艺术瓷砖（汽车） ¥990-含税  
▪︎City Hunter Clear File Set ¥1000-含税  
今日有货！ 请注意，即使您持有预订的门票，也无法保留货物。 门票可在此处购买： 💁‍♀️  
https://zizoya.hinori.jp/event/calendar?openExternalBrowser=1   
此外，今天还可在店内购买演出当天的门票，欢迎您前来观看演出。☺️  
https://pbs.twimg.com/media/GL5N2pObQAAHX9w?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GL5N2pMakAAk7p5?format=jpg&name=4096x4096  


https://twitter.com/kaji1206tk/status/1782966696741130420  
北条司さん展  北条先生、素晴らしい😀楽しかったです♪さあ、お昼ごはん🍜🍚食べたら  横浜国際競技場へゆっくり向かう  
北条司的展览  北条先生，太棒了😀，很有趣......现在，吃完午饭🍜🍚  我将慢慢前往横滨国际体育馆  


https://twitter.com/yuka69680058/status/1782971427232317660  
パンケーキ頂いた後、こんな言葉が出て来ました‼️😊はい、昨夜の私は超ビビってました‼️😳   
拿到煎饼后，我脱口而出： ‼️😊 是的，我昨晚超级抓狂： ‼️😳   

https://twitter.com/jsano1206/status/1782983855156441422  
先日、北条司先生の原画展に行きました。素晴らしかった！特にカラーの原画は水彩で色付けされたのが紙のうねり具合に出ていて興奮しました✨もっこりオムライスはボリュームあって美味しかった😋🍴💕  外にはたくさんのお花！伝言板もあって『XYZ』だらけでした😆  
我最近参加了北条司老师的原画展。 展览非常精彩！ 我特别兴奋，因为我看到了彩色原画是如何用水彩颜料上色的，以及纸张是如何起伏的 ✨大块蛋包饭又香又好吃 😋🍴💕外面有很多花！ 到处都是留言板和 "XYZ"😆。   


https://twitter.com/teru_607/status/1783032040889401405  
北条司スペシャルブレンドコーヒーはとても飲みやすい好きな味！後期の開催日にまた注文します。ん？後期にもあるのかな？キャッツは三層ゼリーを綺麗に撮れなかった。おいしくなさそうに見える😭    
北条司特别的混合咖啡非常容易喝喜欢的味道!后期的举办日再订购。?后期也有吗?猫没有拍好三层果冻。看起来很好吃的😭  
https://pbs.twimg.com/media/GL6aL7OaEAAlogM?format=jpg&name=4096x4096  


https://twitter.com/yoshi_7_prv/status/1783045575270969428  
吉祥寺に行ってきました。  
https://pbs.twimg.com/media/GL6me19a8AA8fid?format=jpg&name=4096x4096  


https://twitter.com/dareyame/status/1783055490550989018  
キャッツ・アイ世代なんだけどめっちゃゃめちゃ良かった…あんなに絵が描けたら楽しそう😍原画じいいぃって見ちゃった  漫画家さん？たちがたくさんいらしてたみたいだけど、どなたかわからず…  オハナに置いとくのでよかったら見に来てください！  ネトフリも観よ！  
我是猫眼一代的，真的很棒......如果能像那样画画，一定很有趣 😍我看了原画  我看到了很多漫画家？ 那里好像有很多漫画家，但我不知道他们是谁......  我会把他们留在 Ohana，如果你想来看看，请一定要来！ 我也会看 Netflix 的！  
https://pbs.twimg.com/media/GL6vgHvbgAAuPEe?format=jpg&name=4096x4096  


https://twitter.com/unyanai/status/1783063293151866936  
北条司展 に行ってきました！生原画が綺麗すぎてめっちゃ近付いて凝視しました。本当に線一本一本が綺麗。ホワイトの使い方も凄い。その他描画手法が凄い。カラー原稿の執筆動画も流れていて、水彩の使い方がうわぁ〜！！！ってなった。写真撮影はOKなので全部撮りました！感動した。最高…！  
我去看了北条司展！ 原画太美了，我凑得很近，目不转睛地盯着它们看。 每根线条都非常漂亮。 白色的运用也令人惊叹。 其他绘画技巧也令人惊叹。 还有一段彩色手稿书写的视频，水彩的使用也非常棒！ 我当时就想，'哇！ 允许拍照，所以我拍了所有的照片！ 我印象深刻。 最棒的..！  
https://pbs.twimg.com/media/GL62m9NakAAcu5H?format=jpg&name=900x900  


https://twitter.com/Mariko05271019/status/1783078291135758689  
マジ私の原点！会場に近づくにつれて妙な動悸すら（不審者かw）大好きな『ねこまんまおかわり』原画が見られて感無量  もちろん後期も行きます‼︎  あと、サエバ商事の広告もあって嬉しかった。  
这真是我的起点！ 当我走近会场时，我甚至有种莫名的心悸（可疑吧？），看到我最喜欢的 "Nekomanma Okawari "的原画时，我非常感动！ 当然，下半年我还会再去的：‼︎ 另外，我很高兴看到 冴羽商事的广告。  
https://pbs.twimg.com/media/GL7EP2-bAAAvyDD?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GL7EP27b0AAypdc?format=jpg&name=4096x4096   


https://twitter.com/makkylucky/status/1783099880623439931  
やっと行ってきましたー！素敵な空間でした✨✨  描き下ろしはあまりにも素晴らしくてしばらく離れられなかった…   
我终于去了那里！ 那是一个可爱的空间✨✨   这些画太神奇了，让我久久不能离开...   
https://pbs.twimg.com/media/GL7X4p5aMAAqvxu?format=jpg&name=4096x4096  


https://twitter.com/chibi_aya_728/status/1783138621832507477  
今日は職場の後輩ちゃんが北条先生の原画展に一緒に行ってくれました🥰  CH原作は去年読んでくれていたのだけど、初めて触れる他の作品にもすごく感激して楽しんでくれて嬉しかった✨  4枚目はARの獠香ちゃんをなんとか一緒におさめる技術を後輩ちゃんが発見したとこ😂  
今天，我的一位同事陪我去看了北条老师的原画展🥰  她去年读过《CH》原作，但我很高兴她第一次接触其他作品✨，印象深刻，非常喜欢  第4张照片是一位大三学生发现了😂以某种方式将 AR 的 獠香 固定在一起的技术    
https://pbs.twimg.com/media/GL77HLsaYAAw423?format=jpg&name=4096x4096  


https://twitter.com/MangaHot_intl/status/1780468463368294414  
COAMIX's newest manga art exhibition space, @GalleryZenon   opens today! 🎊  Tsukasa Hojo-sensei is the first featured with works spanning his entire career!  If you're in Tokyo, don't miss the chance to see original Japanese manga artwork! (The exhibition is also in English)  
COAMIX 的最新漫画艺术展览空间 @GalleryZenon 今天开幕！北条司老师是第一位展出其整个职业生涯作品的漫画家！ 如果您在东京，千万不要错过欣赏日本漫画原作的机会！(展览还有英文版）  
https://pbs.twimg.com/media/GLV85sDacAAGh6C?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLV88hza8AACECa?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLV9BTJbAAI8wK6?format=jpg&name=4096x4096  


https://twitter.com/walkon7/status/1782772519638028535  
https://pbs.twimg.com/media/GL2uJ5jasAA3mkX?format=jpg&name=4096x4096  


https://twitter.com/ichigoremonmkan/status/1782656455021039854  
「木更津キャッツアイ」はやっぱり名作だなぁ。仲間との絆や夢に向かって生きる姿に元気をもらえる！ #キャッツアイ  
「木更津猫眼」至今仍是经典之作。 与朋友之间的羁绊以及他们为梦想而活的方式会让你振奋！   


https://twitter.com/mihohi1/status/1782619056727838773  
そうそう、コラボメニュー頼んだらARで撮影できるのすっかり忘れててお家でパシャリ📷もっこりオムライスの上で撮りたかった💦　#GALLERYZENON  
是的，我完全忘记了点合作菜单时可以用 AR 拍照，所以我想在家里快速拍一张📷 在我的大块煎蛋上💦 #GALLERYZENON    


https://twitter.com/mihohi1/status/1782620587992719647  
感動したのは、連載中は描かれてなかったリョウの靴底。ラバーソール の立体感！ 行ける環境の方はぜひこの目で焼き付けて #GALLERYZENON #北条司    
让我印象深刻的是獠的鞋底，这在连载中是没有画出来的。 橡胶鞋底具有立体效果！如果你有幸亲临现场，一定要亲眼看看！  
https://pbs.twimg.com/media/GL0j-ZabsAASM6K?format=jpg&name=4096x4096    


https://blog.livedoor.jp/summer_assassin1988/archives/40549286.html


https://twitter.com/tBv2D7pF9HsGkxc/status/1782502120761245991  
私用でお伺いすることが難しそうです😢ですがこつこつグッズ集めしてるので、今回も購入して自宅で展示会気分を味わいたいです🖼📗👜#北条司展　#GALLERYZENON  
由于个人原因，我将无法参观展览😢，但我一直在收集商品，我想再次购买，并在家欣赏展览🖼📗👜  
https://pbs.twimg.com/media/GLy30hvawAEqoiQ?format=jpg&name=4096x4096  


https://twitter.com/bookspringtokyo/status/1782645903616336264  
\#北条司展　に行ってきました！  もうすぐNetflix版のシティーハンターも  始まるので楽しみですね♪  #シティーハンター  
我去看了#北条司展  我很期待Netflix版的《城市猎人》即将开播  
https://pbs.twimg.com/media/GL06_5gagAA4D4u?format=jpg&name=4096x4096  


https://twitter.com/fumimaron/status/1782677333650145479  
先日、オープンした #GALLRYZENON に  #北条司展 (前期)を観に行ってきました✨   #シティーハンター や  #キャッツ・アイ 、初期の作品の原画などが観られてとても素敵な時間でした✨✨  
我去参观了最近开幕的#GALLERYZENON✨的北条司展 (前期)   能看到 #城市猎人#、#猫眼# 和他早期作品的原画✨✨，真是太棒了    
https://pbs.twimg.com/media/GL1XkqNbEAAM8nY?format=jpg&name=small  


https://twitter.com/erika_taitou/status/1782699088762597601  
北条司展に行ってきました…！！控えめに言って、最高過ぎました🥺‼️♥♥✨写真撮影OKとか太っ腹すぎやしませんか😂？！展示入れ替わったらまた行こー♥皆さんも是非行ってみて下さい  
我去看了北条司展.....！ 说真的，太精彩了🥺‼️♥♥✨他们太慷慨了，允许我们拍照😂？   
https://pbs.twimg.com/media/GL1rXo-aYAAaCfr?format=jpg&name=4096x4096  


https://twitter.com/hanako_fanks/status/1782724476783992977  
昨日は帰る前におかわりで吉祥寺へ！19日は見えにくかった神谷さんからのお花の立札が見やすくなってましたーTM NETWORK名義でお花とか出さないのかな？？ってちょっと期待しちゃいましたー  
19日很难看到的神谷先生的花摊，现在比较容易看到了--我在想，他是不是要以TM NETWORK的名义送花呢？   我有些激动！  


https://twitter.com/nin05ddd01e/status/1782748299067617612  
\#GALLERYZENON   #北条司展  特典はこちらでした｡:°ஐ..♡*   獠ちゃんもキャッツも仲良さそうなイラストが素晴らしい˙ᵕ˙  

https://twitter.com/nin05ddd01e/status/1782748886916039060  
\#GALLERYZENON  #北条司展  特典はこちらでした｡:°ஐ..♡*  獠ちゃんもキャッツも仲良さそうなイラストが素晴らしい˙ᵕ˙  


https://twitter.com/Chm_069/status/1782751083322061014  
数ある中でもこちらの原画が特にお気に入りでテンション爆上がりしていました(*ﾉω・*)  先生のサイン入りお迎えさせていただけますように(.› ‹.)    
在众多原画中，这幅画特别受欢迎，气氛紧张到爆炸（*ﾉω・*），请允许我带着老师的签名来迎接(.› ‹.)         
https://pbs.twimg.com/media/GL2aqMub0AAhF8C?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GL2aqMubMAAQ_pB?format=jpg&name=4096x4096    


https://twitter.com/Casabuuu/status/1782761745775522124  
北条司展に実写シティーハンターとまたシティーハンターが盛りがってて嬉しい❗️
冴羽 獠はやっぱりカッコいい❗️もっこりしててもカッコいい😆
どうか素敵なサインポスター当たりますよーに  
我很高兴城市猎人随着北条司展和真人版城市猎人的推出而再次令人激动❗️  
冴羽 獠雖然有點疙疙瘩瘩，但還是很酷的 ❗️ 😆  
请帮我赢得一张漂亮的签名海报！   


https://twitter.com/Casabuuu/status/1782752218258768292  
北条先生のサインポスター欲しい〜😆  実写のシティーハンターも盛り上がってるこの時期にプレゼントキャンペーンはアツすぎます‼️どうか素敵なサインポスター当たりますよーに🙏  
我想要一张北条老师的签名海报😆  请帮我赢得一张漂亮的签名海报🙏   


https://twitter.com/GFmxYtlKCYJRabD/status/1782318047531069613  
グリップ一時間で仕上げた(苦笑)おぢさんみたいな素人でもこんなに深みが出るとは(笑)オイルが良かったのか？(苦笑)   
https://pbs.twimg.com/media/GLwQziob0AAT4FS?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLwQzjFbIAAdD7p?format=jpg&name=900x900  


https://twitter.com/GFmxYtlKCYJRabD/status/1782347670247686525  
アニメからシティーハンターを知った人にはお馴染みのショルダーホルスター。正直、原作ファンのおぢさん的にはどうしようかなぁ(苦笑)原作では割りとショルダーより腰に直接INが多いのよ、獠ちゃん(苦笑)  
看过动画片《城市猎人》的人对肩上的枪套一定不会陌生。 说实话，作为原著粉，我也不知道该怎么处理它......（笑）在原著中，有很多情况下，枪套是直接放在腰间而不是肩上的  
https://pbs.twimg.com/media/GLwrwXZb0AATlNV?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLwrwYUbQAApWb-?format=jpg&name=4096x4096  


https://twitter.com/mihohi1/status/1782285350632858035  
そして吉祥寺で念願の北条司先生の原画を拝んで来ました！ほとんどホワイト無しの素晴らしい原稿…。国宝にしてくだされ🙏   
然后，我去吉祥寺看了我渴望已久的北条司的原画！ 几乎没有留白的精彩手稿... 请让它成为国宝🙏。  
https://pbs.twimg.com/media/GLvzDJ2b0AAsHIk?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLvzDJyasAAfyue?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLvzDJybYAAokIu?format=jpg&name=4096x4096  


https://twitter.com/shomatsurai317/status/1782344556509606086  
グッズも買ったよー🥰  北条司展の図録と旦那が気に入ってしまったキャッツアイのオードパルファム3個セット(今見たらAm〇zon激安でした…定価で買ったわwお、お布施よお布施！いいもん！😂)これ3つともいい香りで私は瞳のをもらってきた😊あとXYZのイヤリング。満足だけど、Ama〇onめ～😂  
我还买了一些东西🥰 🥰一本北条司展的画册和一套我丈夫喜欢的猫眼淡香水三件套（我刚才在Amazon上看到很便宜......我就按正价买了......呵呵，献礼，献礼！ 我不知道这是不是个好主意！ 😂)这三个味道都很好闻，我买了瞳那个😊和XYZ耳环。 我对它们很满意Amazon😂。   
https://pbs.twimg.com/media/GLwo7IabsAAoSHh?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLwo7IaaUAAM3uB?format=jpg&name=4096x4096  

https://twitter.com/We_are_CH/status/1782365688595964381  
昨日北条司展に行って来ました。シティーハンターのみならず北条先生の美麗な絵の変遷を感じられる作品の数々。個人的には大好きな読み切りの「ねこまんまおかわり♡」「TAXI DRIVER」を読めて感動しました。5月になったらもう一回観に行きたいと思います☺️  
我昨天去看了北条司展。 很多作品不仅让人感受到《城市猎人》的转变，也让人感受到北条老师的精美画作。就我个人而言，能够读到我最喜欢的读本 "白猫少女♡"和 "TAXI DRIVER "。让我很感动。我想在五月份再去看一次。☺️    
https://pbs.twimg.com/media/GLw8JEAbQAAx2NA?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLw8JEDaoAAfQAK?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLw8JD_aEAA5Idh?format=jpg&name=4096x4096  


https://twitter.com/hokkemint/status/1781567060293959990  
出版社関係のギャラリーなので、校正はしっかりしてほしかった…   
由于这是一个与出版商相关的画廊，我希望校对是扎实的......     
https://pbs.twimg.com/media/GLllyr6aIAAtUCg?format=jpg&name=4096x4096  


https://twitter.com/5Rwph0EEiX83793/status/1782258030954127636  
欲しかったシティーハンターのクリアファイル、アートタイル、トートバッグは入荷待ちでした😢  縁ですね。仕方なし🙂  ホワイトの使い方がすごくて…原画見に行ったんだから良し✨  いやぁ…感動しました…   
我想要的城市猎人Clear File、Art tiles和tote bag都在等待名单上😢 这是个优势。 别无选择 🙂 白色的运用太神奇了......还好我去看了原作 ✨ 嗯......我印象深刻...   
https://pbs.twimg.com/media/GLvaOe6asAAWZPS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLvaOnCasAAU0q2?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLvaOnEakAAotG0?format=jpg&name=4096x4096  


https://twitter.com/5Rwph0EEiX83793/status/1782271794227110040  
この熊本復興イラスト、震災当時に限定販売されてたの知らなかったな…欲しいな…  
我不知道这幅熊本重建插图在地震时限量出售......我想要一幅......  
https://pbs.twimg.com/media/GLvmvy2bQAA2SxN?format=jpg&name=4096x4096  


https://twitter.com/5Rwph0EEiX83793/status/1782326152243318881  
あ！コーヒーカップも売り切れてた！欲しかったのに〜！でもよい💕  欲しかったカードが手元に来てくれたから💕    
啊 咖啡杯也卖光了！ 我也想要💕！ 不过没关系💕，因为我买到了我想要的卡片💕     
https://pbs.twimg.com/media/GLwYMHQb0AAAyyF?format=jpg&name=medium  


https://twitter.com/toel_uru/status/1781452033272533403  
흑흑 이번 전시 기념해서 그린  뉴 일러스트 활용해서   굳즈 내줬으면 많이 샀을 것을  😭😭😭😭😭😭  #CITYHUNTER   #시티헌터    
为展览绘制的黑白插图  为展览绘制的新插图  如果你让我留着它，我会买很多东西  
https://pbs.twimg.com/media/GLj9Lm0aYAAM1JS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLj9LmyboAAqvHC?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLj9Lmwa4AE3hB_?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLj9Lm0bAAAACts?format=jpg&name=medium  


https://twitter.com/runruntomotomo/status/1781975648661275085  
推し様のプリントをしてくれるカフェに行ってきました🔫🚗🧡   
我去了一家可以打印你的猜测的咖啡馆   
https://pbs.twimg.com/media/GLrZaAyakAAEuuI?format=jpg&name=medium  


https://twitter.com/moyumoyuEXPO/status/1782259692204683280  
北条司展、原画がたっぷりで見応えあり！カフェでネトフリのCITY HUNTER予告してて、ひたすらゲワイのイントロ聞きながらのオムライス☕  
北条司展，有很多原画可看！在咖啡馆里做着《CITY HUNTER》预告，一边听着喧闹的前奏一边吃蛋包饭☕    
https://pbs.twimg.com/media/GLvbtnUagAAB1jf?format=jpg&name=large  


https://twitter.com/GalleryZenon/status/1782223228745953368  
「北条司展」に連日ご来場いただき誠にありがとうございます。本日から若干数の当日券の販売を開始致します！📍本日は14:00〜の回以降で当日券がお買い求め頂けます。(※13:00までの回は売り切れとなります)あいにくお天気ですが、お客様のご来場を心よりお待ちいたしております。  
非常感谢您每天参观「北条司展」。 从今天起开始发售少量当日门票！ 📍今天，14:00-场次之后可以购买当天的门票。 (13:00之前的展览门票已售完。   
https://pbs.twimg.com/media/GLu6kz1agAAEMGn?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLu6kz0bIAAF_wa?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLu6kz4boAAOwtL?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLu6kz2bcAAdAWg?format=jpg&name=4096x4096  


https://twitter.com/kakitarepanda/status/1781947090249400574  
\#北条司　#原画展　中身についてはあえて語らない。これこの値段で良いの？倍取っても良いんですよ？てか撮影自由って良いの？注意書きと冴羽商事とグッズ戦利品だけ載せとこ。なんの偶然か、後期にもライブと日程被るんだよね。ちゃんと体調整えてちゃんとどっちも行きたい😭  
\#北条司　#原画展 我不敢说里面有什么。 这个价钱好吗？ 我拿双倍可以吗？ 我是说，可以随意拍照吗？ 我只贴笔记、冴羽商事和战利品。 真巧，下半年的活动日程和现场演唱会也重合了。 我想调整好状态，两个都去😭。   


https://twitter.com/landotter_home/status/1781941187051729027  
こんなに胡蝶蘭贈られるなんて何処の名士かと思ったら、新宿スイーパーの作者様でした。ところでここは吉祥寺です。  
我不知道哪位名人会收到这样的兰花礼物，原来是新宿Sweeper的作者。 对了，这是吉祥寺。   


https://twitter.com/3939sakutyan/status/1781573917402075261  
にいってきましたぁ😊❤  #北条司　先生の美しすぎる作品と、初期の貴重な作品みられてよかったぁ💕  またいこうっと✨  
我很高兴能看到 #北条司 老师的漂亮的作品和他珍贵的早期作品💕  我要回去✨。   
https://pbs.twimg.com/media/GLlsB29awAAkBoo?format=jpg&name=medium  
https://pbs.twimg.com/media/GLlsB1ubQAA1-UX?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLlsB26aYAAur3j?format=jpg&name=4096x4096  


https://twitter.com/kakitarepanda/status/1781301931136393443  
北条司　#原画展　行ってきたのですが体力尽きたので報告は明日(多分) 戦利品だけどーーーーーん！   
我去看了北条司#原画展，但体力不支，所以我明天（可能）再报告！    
https://pbs.twimg.com/media/GLh0qfdbwAI6cvl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLh0qfcboAA7rb8?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLh0qfgbEAAp0vI?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLh0qfdbAAAUz9c?format=jpg&name=4096x4096   


https://twitter.com/philkun_jp/status/1780845792036872437  
1/2 Ce matin j´ai été voir rapidement "#HojoTsukasa Ten, The road to 『#CITYHUNTER』40th anniversary 2025〜Limited Special Exhibition〜" à @GalleryZenon
. Petite expo mais sympa ! #北条司 #tsukasahojo #manga #漫画 #nickylarson #冴羽獠 #catseye #キャッツアイ  
https://pbs.twimg.com/media/GLbVz2LaAAANswV?format=jpg&name=4096x4096  


https://twitter.com/lvxinxin/status/1780477625200435455  
老师画风的变化，还是很明显的，虽然女生都长的很像  
https://pbs.twimg.com/media/GLWG9SEaoAA0_pj?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLWG9SEaQAAFYTj?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLWG9SAacAARxSN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLWG9R-aEAAYWqH?format=jpg&name=4096x4096  

https://twitter.com/lvxinxin/status/1780476802068312378  
真开心啊，这个月还有其它展，要吃土了  
https://pbs.twimg.com/media/GLWGNZeacAAZEPS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLWGNZgacAA_e3v?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLWGNZgbUAAS3Y5?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLWGNZjaAAAzY1v?format=jpg&name=4096x4096  


https://twitter.com/yukiti6xp11/status/1780422589468582038  
https://pbs.twimg.com/media/GLVU5_SbIAAnPMa?format=jpg&name=medium  


https://twitter.com/vex1FXHl1u4671/status/1780097884236726551  
note.com/gifted_mango274/n/n39bc95701c23偽物リスト
hsgml大量偽物出品中！添付③本物ある方情報提供よろしくお願いします！ 
https://x.com/tiyu12sai/status/1714974204649763034?s=12&t=0D_gpU34GzYRUBxSgFg6qQ
https://x.com/kinokuniyausa/status/371363022557552640?s=12&t=0D_gpU34GzYRUBxSgFg6qQ  
全て宛先無し偽物！  通報拡散希望  #北条司 #シティーハンター #偽物 #ヤフオク #メルカリ  
https://pbs.twimg.com/media/GLQtk18bgAA7tHV?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLQtlKXaUAAb93y?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLQtleSbgAAkfvW?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLQtlvubcAAWqSo?format=jpg&name=4096x4096  


https://twitter.com/rsitejapan/status/1781995953739952596  
Get Wildのプレイリスト，97曲になりましたw  総再生時間9時間19分！！  
获取狂野播放列表，现在有 97 首歌曲！  总播放时间为 9 小时 19 分钟！   
https://pbs.twimg.com/media/GLrqx_la4AAr1rn?format=jpg&name=medium  


https://twitter.com/serow119/status/1781995494929289366  
\#GetWild をつくったよ  
という事で完成となります。原画展のミニは、ナンバーは伏せられているようですね。車内が良く見えるようにヘッドレストは着けませんでした。 実はアンテナは左右どちらかに逃げないと、🔨は載りません😄。  
これでおしまいです。  
https://pbs.twimg.com/media/GLrrdTYa4AAtYBQ?format=jpg&name=medium  


https://twitter.com/serow119/status/1781989741170418068  
\#GetWild をつくったよ  おまけ その２  完成すると見えにくいのですが、シートに載っているのは、｢週刊ジャンプ｣。始めはコミックスと思ったのですが、1/24だと１センチ足らずで小さすぎ、CAT'S♥EYEが表紙の頃のジャンプを作りました。  ナンバーは｢新宿｣で作成。  
https://pbs.twimg.com/media/GLrmOYNa4AAzRSw?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLrmOaNboAANZZ4?format=jpg&name=900x900  


https://twitter.com/serow119/status/1781980404968473087  
\#GetWild をつくったよ  おまけ その１  サンルーフを着けなかったのは、🔨これのため。でもどこで記憶がすり替わったのか、1→5と５倍になってしまった!(当社比)。Tが大文字しか無く残念。一応リバーシブル？です。半丸のプラ棒を巻いて、縁を作りました。  \#GetWild created.
号外，第一部分。  我没戴天窗🔨。 但我的记忆在哪里切换了，它从 1 变成了 5，还多了 5 倍！ (可惜 T 只有一个大写字母。 可逆的，你想知道吗？ 塑料棒是可翻转的。 我用一根半圆塑料棒包住它，做成了边缘。   
https://pbs.twimg.com/media/GLrduTragAA4GW4?format=jpg&name=small  
https://pbs.twimg.com/media/GLrdu_cbwAANfri?format=jpg&name=small  
https://pbs.twimg.com/media/GLrdu5lbMAA1ZVL?format=jpg&name=small  

https://twitter.com/pjhhXe2OtP3LjUG/status/1781946239577399797  
今回の話、香がかわいそう😅  守ってもらう美女が  香のフォローがなかったら  危なかったのに  目の敵にして病室から  追い出そうとしてくる☹️  
在这个故事里，我觉得香很可怜😅  一个美丽的女人需要被保护  如果不是香的跟进  就会有危险  她是他眼中的敌人，试图把他赶出病房  她想把我赶出病房 ☹️    

https://twitter.com/gamepressjp/status/1781928058947293282  
北条司先生直筆サイン入りA1ポスタープレゼントキャンペーン開催中🎁✨  GALLERY ZENONのオープンを記念して、コアミックスが素敵なプレゼントを用意しました！応募条件はたった2つ♪ 詳細は4月17日...  
由北条司签名的 A1 海报赠送活动。 为庆祝 GALLERY ZENON 开幕，Coamix 准备了一份精美的礼物！ 参加条件只有两个 🎁 详情将于 4 月 17 日公布...    

https://twitter.com/aitaina2525/status/1781601478693761373  
今ね  ビックリする事が‼️  なんと…  大好きな北条司先生の直筆サインが当選し送られてきました❣️  私の運を全て使い果たしたかも知れないですが、とても嬉しいです！  また中野ブロードウェイに行きたいと思います！  大切にします💖  ありがとうございました❣️  
现在   我有个惊喜给你： ‼️  哦，我的上帝...  我赢得了我最喜欢的作家北条司的亲笔签名，而且是寄给我的❣️  我可能已经用尽了我所有的运气，但我非常高兴！  我想再去一次中野百老汇！  我会好好珍惜的💖  非常感谢 ❣️  
https://pbs.twimg.com/media/GLmFGYhaoAAZEZM?format=jpg&name=large   


https://twitter.com/TOMOM0112358/status/1781539632045522956
展示と、カフェ行ってきました～  めちゃ良かったです  控えめに最高でした。  気になってる方は、是非～  特典はシクレでちょっとびっくりしたし、カフェの取り皿グッズとしてでないかな？  ありがとうございました～ #北条氏展  #シティーハンター #キャッツ・アイ  
我去看了展览，还去了咖啡馆--真是太棒了！  非常棒，低调的棒。 如果你感兴趣，请一定要来~  作为赠品的鸡尾酒让我有点意外，不知道会不会作为咖啡馆的盘中餐呢？  非常感谢  
https://pbs.twimg.com/media/GLlM2S6akAAsaz2?format=jpg&name=4096x4096  


https://twitter.com/shizuchryo0326/status/1781512079121293583  
City Hunter Sound Collection Y-Insertion Tracks http://tower.jp/item/1793085  #街中sophisticate #シティーハンター CD引っ張り出して聴こうっと☺️  
City Hunter Sound Collection Y-Insertion Tracks http://tower.jp/item/1793085 #sophisticate in the city #《城市猎人》 拿出 CD 听一听 ☺️  
https://pbs.twimg.com/media/GLky0hIaIAAlFr1?format=jpg&name=4096x4096  


https://twitter.com/RYO_usamame/status/1781493379408581022  
そういえば昨日GALLERY  ZENONで貰った入場特典(想像より小さくて貰ったことも忘れて鞄に入れっぱなしだった💦)シティーハンターだった🥰シークレットは何なのだろう？  
说到这里，我昨天在 GALLERY ZENON 得到的入场奖励（比我想象的要小，我忘了拿了，就放在包里💦）是《城市猎人》🥰，有什么秘密吗？   
https://pbs.twimg.com/media/GLkiyLHbgAAGlHa?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLkiyLGbAAABpyz?format=jpg&name=4096x4096   


https://twitter.com/tanusan1103/status/1781315386367254899  
やずやの2回目特典届いた  2人の缶バッチ　尊い💕  飲み始めて疲れが残りにくくなった気がします。おすすめですよ✨✨  #やずやのにんにく卵黄wild  
https://pbs.twimg.com/media/GLiA5uDbYAAU-Dg?format=jpg&name=medium   


https://twitter.com/hisa1974812/status/1781210924663427428  
初めての　やずや  シティーハンターコラボのにんにく卵黄  疲れが取れにくい年なので、期待感はあります👍  \#やずや #にんにく卵黄WILD #シティーハンター   
初来乍到  城市猎人合作的大蒜蛋黄  疲劳很难去除的一年,因此,期望👍  
https://pbs.twimg.com/media/GLgh5WwagAAD-fS?format=jpg&name=medium  


https://twitter.com/Nabe04148095/status/1780942953684550042  
トラベラーズノートのカスタマイズ💕  トラベラーズノートレフィル✖️シティーハンター ハガキコピー版を表紙貼り付け✖️100均クリアバックを切り貼り  
定制您的旅行笔记本💕    TRAVELER'S笔记本笔芯✖️City Hunter明信片复制版粘贴在封面✖️100剪贴透明背面    
https://pbs.twimg.com/media/GLcuKt3boAA7AF_?format=jpg&name=4096x4096  


https://twitter.com/TaThere/status/1780849912047165455  
届け…アダルトなキャスティングww 
https://pbs.twimg.com/media/GLbZjgqbEAAWn-f?format=jpg&name=medium  


https://twitter.com/Nabe04148095/status/1780786207175389574  
我が家の家計は常時冴羽商事並みですが、よく使う文具類がじょじょに増え、嬉しい💕  一部100均の力も借りて  
我们的家庭预算总是与冴羽商事看齐，但我们经常使用的文具数量却在逐渐增加，我们很高兴💕 在 100 日元商店的帮助下  
https://pbs.twimg.com/media/GLaflwdbkAAAsT7?format=jpg&name=4096x4096  


https://twitter.com/mikanoshidari/status/1780742861513949490  
先日、中野ブロードウェイに行ってきました！  欲しいと思っていたグッズ…完売のものもあったけれど  かわいいタオル、それからブラインド商品で  1番欲しいと思っていた  リョウと香をゲットできて嬉しい〜！  北条先生の美麗な原画の展示もありました！    
前几天，我去了中野百老汇！ 我想要的商品......有些已经卖完了，但是......  可爱的毛巾和盲人用品  是我最想要的  很高兴我买到了獠和香  还有北条老师的美丽原画展    
https://pbs.twimg.com/media/GLZ4LR7aQAAPadH?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLZ4L6zbAAA-IaX?format=jpg&name=small  


https://twitter.com/michanachan/status/1780591885842821547  
ゼノンカフェいーなー  原画展もいーなー  どーして先週東京行ってたんやろぉ  泣くぅーーーー  #シティーハンター  
我爱 Zenon Cafe！ 我喜欢原版展览！  为什么我上周要去东京？  我哭了     

https://twitter.com/sui1198838/status/1780590166031380915  
なんと言うか  
浦上親子エピは獠が珍しくところどころキュートでしたね ぽつねん獠は悲しかったですね セルフすまきは泣けましたね  付き添いを代わってくれって頼むのもどしたん話聞こかって感じでしたね  
たまにはこうして獠も揺れたらいいんですよ 読んでて一緒に疲れましたけどね #シティーハンター   


https://twitter.com/GalleryZenon/status/1781136206614741185  
【グッズ入荷情報】北条司展の新グッズ「シティーハンター クリアファイルセット」「TAXI DRIVER 複製原稿」「ネコまんま おかわり♡　複製原稿」「桜の花咲くころ複製原稿」「SPLASH!3 複製原稿」  が再入荷しました👍  ぜひお買い求めください  
北条司展新货《城市猎人 Clear File Set》、《出租车司机 Copy Manuscript》、《白猫少女♡ Copy Manuscript》、《樱花盛开时 Copy Manuscript》、《SPLASH！3 Copy Manuscript》现已有货！  3部现已恢复库存👍  欢迎购买！  
https://pbs.twimg.com/media/GLfc3DiaQAAaJ8J?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLfd74HaEAAW_IZ?format=jpg&name=4096x4096  


https://twitter.com/rirry_ro/status/1782188807447773469  
午後半休の記録・その1。4/17(水)、吉祥寺で「北条司展」を鑑賞🙂  CITY HUNTER・CATS EYEはもちろん、数々の作品を楽しみました😊  神谷明さんからのお花もあったっ  
4 月 17 日（周三），参观了吉祥寺 🙂 的北条司展。  欣赏了许多作品以及《城市猎人》和《猫眼》  还有神谷明的花！  
https://pbs.twimg.com/media/GLubRKMboAAKS7z?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLubRMsbwAAczxE?format=jpg&name=4096x4096  



https://twitter.com/natsuasa1988/status/1782167678502760758  
ブログ更新：吉祥寺の『GALLARY ZENON』で開催中の「北条司(先生)展」へ行ってきました！（１）  
博客更新：我去吉祥寺 GALLERY ZENON 观看了北条司（老师）的展览！ (1)    


https://twitter.com/ryosaeba357xyz1/status/1782059635652084118  
最高☕😊  XYZ ・・・ギャラリーゼノンにて待つ  写真は #北条司展 開催初日、吉祥寺GALLERY ZENONにて   キャッツ❤️アイの来生瞳です  
最好的☕😊  XYZ ・・・ 在泽农画廊等待 照片显示的是  #北条司展的第一天、在吉祥寺 GALLERY ZENON 举行  这是 Cat's❤Eye的来生瞳  
https://pbs.twimg.com/media/GLslxH4aoAAq1Ct?format=jpg&name=large  


https://twitter.com/yfdm0127abc/status/1782046212973384124  
行きました🙋‍♀️  展示もカフェも心から楽しめました🫶  外の伝言板のメッセージがいっぱいで書けなかったのが少し心残りですが、生原画に感動して じっくり近くで何度も何度も見ては  このシーンはあの時大好きだったなーෆ˚*とか思い出に浸っておりました🥺  ご縁がありますよーに(꒪˙꒳˙꒪ )    
我去了 🙋‍♀️  我非常喜欢这个展览和咖啡馆🫶  我有点失望，因为外面的留言板已经满了，我不能在上面写留言，但现场的原画给我留下了深刻的印象  我仔细地、近距离地看了一遍又一遍  我被原作深深打动，看了一遍又一遍  我希望你们有机会见到她   
https://pbs.twimg.com/media/GLsZlUgbYAAMB3Y?format=jpg&name=large  
https://pbs.twimg.com/media/GLsZlUhaUAASOWh?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLsZlUebkAAOIOz?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLsZlUeakAAw4lL?format=jpg&name=4096x4096  


https://twitter.com/yfdm0127abc/status/1782045822403936412  
展示もカフェも心から楽しめました🫶  外の伝言板のメッセージがいっぱいで書けなかったのが少し心残りですが、生原画に感動して  じっくり近くで何度も何度も見ては  このシーンはあの時大好きだったなーෆ˚*とか思い出に浸っておりました🥺  ご縁がありますよーに  
我非常喜欢这个展览和咖啡馆🫶  因为外面的留言板已经满了，我不能在上面写留言，这让我有点失望，但现场的原画给我留下了深刻的印象  我认真仔细地看了一遍又一遍。  我被原画深深地打动了，一遍又一遍地看着它，心想："当时我就喜欢这个场景ෆ˚*" 🥺  祝你好运！  


https://twitter.com/PEGASUSSEIYA13/status/1782019379758280892  
ギャラリーゼノン併設カフェでお昼ご飯🍴  
①獠のもっこりオムライス🍳🍚  
②③香のプンプン、おしおきパンケーキ&今宵あなたのハートをいただきに参りますドリンク🔨🥞🐱🥤  
④北条司スペシャルブレンド☕  
在 Zenon Gallery🍴附设的咖啡馆用午餐  
①獠的大块蛋包饭🍳🍚  
②③香的普姆蓬、惩罚煎饼和我今晚来取你的心饮料🔨🥞🐱🥤  
④北条司 Special Blend☕  
https://pbs.twimg.com/media/GLsBLkvaAAATSsO?format=jpg&name=large  
https://pbs.twimg.com/media/GLsBLlkaUAIcH3Z?format=jpg&name=large  
https://pbs.twimg.com/media/GLsBLl_awAAigYY?format=jpg&name=large  
https://pbs.twimg.com/media/GLsBLoSbQAA7PoJ?format=jpg&name=large  


https://twitter.com/PEGASUSSEIYA13/status/1782018515484246179  
4月20日①  北条司展行ってきました。  北条司先生の美麗原画とイラストは何度見ても飽きない🤗  
4月20日①  我去看了北条司的展览。  北条司的原创绘画和插图无论看多少遍都不会过时🤗   
https://pbs.twimg.com/media/GLsAZRObUAAxmv4?format=jpg&name=large  
https://pbs.twimg.com/media/GLsAZUrboAAjdto?format=jpg&name=large  
https://pbs.twimg.com/media/GLsAZXbbkAASyt3?format=jpg&name=large  
https://pbs.twimg.com/media/GLsAZV9bIAA5w66?format=jpg&name=4096x4096  


https://twitter.com/haruguma55/status/1782013911077953808  
北条司展に行ってきました。貴重な原画の美しさに感動しました。また、後期も行きます！   
我参观了北条司的画展。 珍贵的原画之美给我留下了深刻的印象。 我会再去看後期展览！    
https://pbs.twimg.com/media/GLr8NIIaYAAWMsQ?format=jpg&name=large  
https://pbs.twimg.com/media/GLr8NIGbgAAvc79?format=jpg&name=4096x4096   


https://twitter.com/retrofuture_08/status/1781979815857582576  
ようやく    行ってきました！  会場入って1枚目から、感動で泣きそうになって動けなく(動きたくなく)てどうしようかと思った……  カラーイラストも漫画原稿も美しくて、様々な画材(トーン含めて)使いこなされてるし、やっぱり先生の漫画は芸術品……   
终于去了    我去了！  从进入会场的第一张照片开始，我就被感动得几乎落泪，动弹不得（或不想动），不知如何是好......  他的彩色插图和漫画手稿非常漂亮，他使用了多种艺术材料（包括色调），他的漫画是一件艺术品......   
https://pbs.twimg.com/media/GLrdMcdaYAANAZ4?format=jpg&name=medium  
https://pbs.twimg.com/media/GLrdMiragAAxtsI?format=jpg&name=4096x4096    
https://pbs.twimg.com/media/GLrdMpTa4AA0zD0?format=jpg&name=medium  
あと、原画サイズで見ると、コミックや完全版やら画集より、表情も違った印象で感じられるのが良いね……🙏✨  (ADの特典冊子と新聞広告で見た時も思ったけど)  
此外，我还喜欢这样一个事实，那就是当你看到原版尺寸时，你对表情的印象会与漫画、完整版或画集中的不同。......🙏✨（当我在AD的特典冊子和报纸广告中看到它时，我也是这么想的）   

https://twitter.com/kinakonose/status/1781974933930906025  
ゼノン行ってきました！念願の北条司先生の生の絵を、細部まで近くで見ることが出来て幸せです！今回の図録と、（行けなかった）キャッツアイ展の図録を購入することができました。CHのクリアファイル、アートタイル、トートバッグは私が行ったときには売り切れでした。  
我去了Zenon！ 我很高兴能够近距离、细致地欣赏到我向往已久的北条司的现场绘画！ 我买到了这次展览和 "猫眼 "展览（我没去成）的画册；CH 透明文件、艺术瓷砖和手提袋在我去的时候已经卖完了。  
https://pbs.twimg.com/media/GLrYv3fagAEVW_J?format=jpg&name=large  
https://pbs.twimg.com/media/GLrYv4GakAAJSR9?format=jpg&name=large  


https://twitter.com/chibi_aya_728/status/1781971205354135953  
今日はお友達と一緒に北条司展へ🥰何度観ても美しくてため息が出るし涙ぐんじゃう…😢  本当にすごいなぁ。。  2階のテーブルにちょこっと展示物が増えてて嬉しい🤗(仏版サイン入りフライヤーも。)  ご飯も二人でシェアして美味しくいただきました✨   
今天，我和朋友一起去看了北条司展 🥰，它太美了，无论看多少次，都会让我感叹和流泪......😢，真的太神奇了。 我们很高兴在2楼的桌子上看到了更多的展品🤗（包括一张法文版的签名传单）。我们还一起分享了美味佳肴✨  
https://pbs.twimg.com/media/GLrVXBFbgAAPhDr?format=jpg&name=medium  
https://pbs.twimg.com/media/GLrVXHDagAAcIAg?format=jpg&name=medium  
https://pbs.twimg.com/media/GLrVXNvakAE6NoN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLrVXWrbIAAZwUD?format=jpg&name=medium  


https://twitter.com/ha43zu/status/1781968920922247397  
北条司展行って来ました〜😆  先生の超絶技術と美麗、繊細な絵の数々。作画VTRも見応えあって眼福🙏  ゼノンさんの店内の展示スペース、コンパクトですが導線もよく見易かったです😊  今日は予定ありで急ぎめで見て回ったので次回はゆっくりじっくり見るぞ。  
我去看了北条司的画展😆 老师高超的技艺和精美细腻的画作。 Zenon 店里的展览空间很紧凑，但只要人流顺畅，还是很容易看完的😊 今天因为有安排，所以只能匆匆看完，下次我会慢慢看，仔细看。   
https://pbs.twimg.com/media/GLrTOA0bwAAjzvw?format=jpg&name=large  
https://pbs.twimg.com/media/GLrTOA3aMAAUl_k?format=jpg&name=large  
https://pbs.twimg.com/media/GLrTOA4aMAApJlc?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLrTOA3a8AAfniq?format=jpg&name=4096x4096  


https://twitter.com/julyeveryday/status/1781952035132436919  
北条司展 前期  貴重な初期作品や大好きな「桜の花さく頃」の原画が見れて眼福でした。後期も行きたい。  
北条司展  前期  看到他珍贵的早期作品和我最喜欢的《樱花盛开时》的原画，真是大开眼界。 我还想去看后期。  
![](https://pbs.twimg.com/media/GLrD7CvacAAQgiW?format=jpg&name=large)  
![](https://pbs.twimg.com/media/GLrD7DubAAAWXtV?format=jpg&name=large)  
![](https://pbs.twimg.com/media/GLrD7FDa4AAf090?format=jpg&name=large)  
![](https://pbs.twimg.com/media/GLrD7IRa0AA7B5a?format=jpg&name=large)  


https://twitter.com/julyeveryday/status/1781952047572832580  
カフェてオムライスと北条先生のスペシャルブレンドを食しました。美味しかった。  
我在咖啡馆吃了蛋包饭和北条老师的特制调料。 味道很不错。   
https://pbs.twimg.com/media/GLrD7_9bwAAGbp1?format=jpg&name=large  
https://pbs.twimg.com/media/GLrD8GPa4AAJKci?format=jpg&name=large  


https://twitter.com/jollyrogersxxxx/status/1781951433367314715  
最近頻繁に吉祥寺に出没しておりますが  今週も出没です  吉祥寺の新名所『#北条司展』in GALLERY ZENONへ😆  当時ジャンプで読んでいた  \#シティーハンター や #キャッツアイ などの生原画がたくさん展示されている～  やはり原画は素晴らしい！！  
我最近经常去吉祥寺  这周我还会去  去吉祥寺的新地标 GALLERY ZENON 😆的『#北条司展』  当时我正在看《JUMP》上的《城市猎人》和《猫眼》  \#当时我在《JUMP》上读到的《城市猎人》和《猫眼》。 这里展示了很多原画。  这些原画仍然令人惊叹！！  
https://pbs.twimg.com/media/GLrCN40aMAAtiU3?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLrCYLhbcAAfda6?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLrCjFsbAAAbr4_?format=jpg&name=4096x4096  


https://twitter.com/hagitatu0321/status/1781944171395494206  
\#北条司展 in #GALLERYZENON へ  何時間でも見ていられるほどに美麗すぎる原画にただただため息しか出なかった  全然見飽きないし、見るたびに新たな感動が…  シティハンターやキャッツアイ他北条司先生の作品を知ってる人はもちろん、そこまで知らない人でも、あの原画を見たら感動すること間違いなし  
我只能感叹原画太美了，可以让我看上几个小时  我百看不厌，每看一次都有新的感受...  无论是了解《城市猎人》、《猫眼》和北条司其他作品的人，还是对北条司不甚了解的人，看到这些原画一定会印象深刻   
https://pbs.twimg.com/media/GLq8uXza4AAJVYX?format=jpg&name=large  
https://pbs.twimg.com/media/GLq8vNxaYAAfWxE?format=jpg&name=medium  
https://pbs.twimg.com/media/GLq8v_LawAAXEFa?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLq8wyFbsAArc7Z?format=jpg&name=medium  


https://twitter.com/senkouchoco0802/status/1781929075424346255  
最高でした。😭😭😭😭😭😭😭  
\#カフェゼノン #北条司展 #CITYHUNTER  
https://pbs.twimg.com/media/GLqu-KUaAAADgYK?format=jpg&name=large  
https://pbs.twimg.com/media/GLqu-KkbIAA_VnS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLqvBIJaQAAJLzL?format=jpg&name=large  
https://pbs.twimg.com/media/GLqvBIPb0AAN4Ak?format=jpg&name=large  


https://twitter.com/hagitatu0321/status/1781914735828750436  
【備忘録】  
\#galleryzenon で開催中の #北条司展 のカフェでコラボカフェメニュー(獠の…オムライス、香の…パンケーキ)を頼むときは、A3サイズの厚紙が入る(保護)ケースを持ってくること  
\在 #galleryzenon 的 #北条司展的咖啡厅点合作咖啡厅菜单（獠的......蛋包饭、香的......煎饼）时，请带上一个能装 A3 大小纸板的（保护）盒。   

https://twitter.com/ntkriu123/status/1781874564680798493  
\#北条司展 行ってきました〜〜🤗✨原画めっっちゃ素晴らしかった。1枚1枚がホントに繊細で美しいし絵がうますぎる。大変贅沢な時間を満喫させていただきました。カフェにも寄れたけど時間があっという間過ぎる😭💦 もう少し居たかったなぁって思いました。また行きたいなぁ〜☺️  
\#我去看了#北条司展～～🤗✨  原画太神奇了，每一幅都非常精致漂亮，画得也太好了。 我度过了一段非常奢侈的时光。 我们本可以在咖啡馆停留一下，但时间过得飞快 😭💦，真希望我们能多待一会儿。 我希望我能再回去。☺️  
https://pbs.twimg.com/media/GLp9d3abwAEPjsy?format=jpg&name=large  
https://pbs.twimg.com/media/GLp9d3Ma4AE27ID?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLp9d3LbwAEjpan?format=jpg&name=large  
https://pbs.twimg.com/media/GLp9d3LbIAAiKuM?format=jpg&name=4096x4096  


https://twitter.com/aquta/status/1781848123914297535  
17時回は18時閉館で実質1時間汗💦  カフェのラストオーダー17:30なので先が良いですよ、とスタッフさんからのアドバイスどおり、先にカフェへ。  獠ちゃんイラストA3シートもらえた！知らなくて、ビックリ嬉しい！  香ちゃんも可愛い！  入場特典もシクレで最高！  
17:00 的课程 18:00 就结束了，所以实际上是一个小时 汗💦  咖啡厅的最后点餐时间是 17:30，所以按照工作人员的建议，最好先去咖啡厅。  我拿到了一张 A3 纸，上面画着 Jyun-chan 的插图！ 我还不知道，真是又惊又喜！  香也很可爱！  入場特典也很丰厚！  
https://pbs.twimg.com/media/GLplaUnaYAAH8VZ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLplaXragAA9RLO?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLplalIa4AAqVTo?format=jpg&name=large  
https://pbs.twimg.com/media/GLpla2dbgAALF28?format=jpg&name=large  


https://twitter.com/aquta/status/1781815603478872569  
にいってきました。  建物の外装はあまり変わってないので、久しぶりのSAKABAにきた気分でした。  
我去了一家 SAKABA。  建筑的外观没有什么变化，所以我感觉自己很久以来第一次来到了 SAKABA。  
https://pbs.twimg.com/media/GLpH2M8aUAAcrdB?format=jpg&name=large  
https://pbs.twimg.com/media/GLpH2NLbcAAiic1?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLpH2DKbQAAD3N1?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLpH2RPbkAAyg-K?format=jpg&name=large  


https://twitter.com/UpperBodyCentau/status/1781699724753666340  
吉祥寺の新装GALLERY ZENONで開催中の #北条司展 行ってきました〜  週間連載を読んでいたストライク世代なので楽しかったー  ホントきれいな絵です。  好きな順番で展示やコラボカフェに行けるなかなか自由度が高い展示会でした。  最高。  今回前半って事で、どうやら後半展示もあるらしい。  
我去吉祥寺新装修的 GALLERY ZENON 观看了#北条司展。  我是曾经读过周刊連載的 "Strike "一代，所以很开心！  图片真的很美。  这是一个自由度很高的展览，你可以按照任何顺序去展览和合作咖啡馆。  这很棒。  这是展览的前半部分，显然还会有后半部分。  
https://pbs.twimg.com/media/GLnedLqbkAE3mhj?format=jpg&name=large  
https://pbs.twimg.com/media/GLnedNOagAALIs9?format=jpg&name=large  
https://pbs.twimg.com/media/GLnedLka0AA0NNl?format=jpg&name=large  
https://pbs.twimg.com/media/GLnedNQaIAAeNIk?format=jpg&name=4096x4096  


https://twitter.com/reigraft/status/1781686227500159042  
北条司先生の原画展へ行ってきました😊アナログ原稿の筆のタッチや細かい所も見れて楽しかった(≧∇≦)  併設のカフェでこちらを注文🍳🍅🍚  冴羽さんの素敵ボードは持ち帰り出来るのですが、A3！A3のファイルを持参することをおすすめします！！  
我去看了北条司先生的原画展😊我很喜欢看他的笔触和模拟手稿的细节(≧∇≦)  我在展览附设的咖啡馆点了这个🍳🍅🍚  冴羽的精彩画板可以带回家，但我建议带一张A3纸！  
https://pbs.twimg.com/media/GLnSLn8bkAAbOUL?format=jpg&name=large  


https://twitter.com/rG3Ms47fH3TuNmy/status/1781677315640217744  
えぇ盗まれたんですよゴッソリ...  でもまだまだ足りない...もっと見たいんですよ...  
是的，它被偷了......  但这还不够...... 我想看更多...   
https://pbs.twimg.com/media/GLnKEoQbAAAFgNm?format=jpg&name=large  



https://twitter.com/moccorihnm/status/1781663475879792813  
他の作品とのコラボ時のお皿なんだろうけど、冴羽オムライス食べ終わったら『そんなこと言わないでねっ。１時間だけお茶しよーよぉ！』の文字が出てきてむしろ冴羽にぴったりで笑った   
我知道这是与其他作品合作的菜单，但当我吃完冴羽煎蛋饭时，盘子上出现了 "别说了，我们喝茶吧，就一个小时！“的字样出现在盘子上，这与冴羽相当贴切，让我忍不住笑了起来。    


https://twitter.com/K_METAL355/status/1781663141245657136  
吉祥寺で『#北条司展』開催してた😃  
https://pbs.twimg.com/media/GLm9Lt1bIAACKj5?format=jpg&name=large  
https://pbs.twimg.com/media/GLm9Lt7bgAAqRqK?format=jpg&name=medium  


https://twitter.com/moccorihnm/status/1781659659079991801  
今日は北条司展前期へ行ってきましたー！  眼福すぎて食い入るように見てしまった...  更に先生の描き下ろしの２人の破壊力がすごすぎるから是非実際見て欲しいです！  
今天，我去看了前期北条司展！ 我高兴得目不转睛......  我想让你们亲眼看看，因为老师新画的这两幅画的破坏力实在太大了！  
https://pbs.twimg.com/media/GLm6ACEakAAniJ8?format=jpg&name=large  
https://pbs.twimg.com/media/GLm6AB5aIAAF78b?format=jpg&name=4096x4096  


https://twitter.com/moccorihnm/status/1781603525514137728  
サウザー冴羽、、、  
https://pbs.twimg.com/media/GLmG9eybUAAYZi-?format=jpg&name=large  


https://twitter.com/H_Specter_DA/status/1781645678751719763  
\#北条司展 に行ってきた！  大満足☺  やっぱ原画はいいなぁ…髪の質感の繊細さとかよく伝わる。  シティーハンター好きの方に超オススメ！！  
我要去#北条司展！  强烈推荐 ☺  毕竟原图不错...... 你真的能感受到头发质感的细腻。   超级推荐给喜欢城市猎人的人！  
https://pbs.twimg.com/media/GLmtS9wbAAAAJdg?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLmtTE2bUAASJOd?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLmtTHDaEAA0XdH?format=jpg&name=large  


https://twitter.com/aya77310/status/1781632392635224321  
行ってきました北条司展！  北条先生の美しいカラーや漫画原稿を舐める様に見て参りました…あまりにも綺麗でため息出たよ…  後半も行くから楽しみだー！  
我去看了北条司展！ 我看着北条先生美丽的色彩和漫画手稿，仿佛在舔舐它们......它们太美了，我感叹道  我也很期待去看展览的下半期！ 
https://pbs.twimg.com/media/GLmhNG9bkAAxMWN?format=jpg&name=large  
https://pbs.twimg.com/media/GLmhNHEakAE32Hm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLmhNIcaUAAWGW0?format=jpg&name=large  
https://pbs.twimg.com/media/GLmhNWMbUAELL5O?format=jpg&name=4096x4096


https://twitter.com/gomagomabou/status/1781621262697349575  
Replying to @gomagomabou  
ちなみに物販のクリアファイルは売切れでした🥹カフェ混雑時は待ちの列が出来ていたので、空いていたらすぐさま入りましょう！   肝心の展示に関しては、原画が美しすぎて感動😭😭😭3時間ほど滞在してしまいましたw  何度も足を運びたいです😤  
顺便说一句，出售的Clear File已经售罄，咖啡馆忙的时候还要排队等候，所以如果有空位，请马上进去！  至于展览，我被原画之美深深打动😭😭，在那里呆了 3 个小时😤 我想再来一次😤。   


https://twitter.com/3939sakutyan/status/1781580600694198377  
カフェのコラボメニューもおいしかったよー！  コラボメニューを頼むとついてくるQR  コード読み飲むと香ちゃんも瞳ちゃんがとびでてきたよ👀‼️  ファミリーコンポも好きなんだよなぁ。ファミリーコンポの原画もみたいな😍  
咖啡馆的合作菜单非常美味！  点合作菜单时附带的二维码！  看了二维码后喝了一口，香和瞳都跳了出来👀‼️  我也喜欢《Family Compo》。 我也想看看 Family Compo 的原画😍。  
https://pbs.twimg.com/media/GLlyGkYbkAALQKy?format=jpg&name=large  
https://pbs.twimg.com/media/GLlyGigbwAApdQt?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLlyGjHawAAXEju?format=jpg&name=4096x4096  


https://twitter.com/io_toc_ch_su_dn/status/1781580285697720539  
感涙  #galleryzenon #北条司展
https://pbs.twimg.com/media/GLlx03fbsAAuom3?format=jpg&name=medium  


https://twitter.com/hokkemint/status/1781576765745643866  
食べたのはパンケーキだけだけど、ステッカーはオムライスのものだった。  自分はいいんだけどさ…  ファンの中には怒る人もいるのでは…  
我只吃了煎饼，但贴纸是煎蛋的。  我觉得没问题......但我想有些粉丝可能会不高兴......  
https://pbs.twimg.com/media/GLlunsebQAExaCE?format=jpg&name=large  


https://twitter.com/3939sakutyan/status/1781576756077760595  
お二階にも展示が！！  おくの棚にグッズやノベルティの一部を飾ってました😆  持っているものもあったから、それもってるー！  ってなった💕  
二楼还有一个展览！ 一些商品和小玩意儿摆在奥的货架上 😆！ 我当时想💕！ 
https://pbs.twimg.com/media/GLlummVbQAAEqzl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLlumlLbsAAqp2A?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLlummSagAAb6RN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLlumnibUAAhYdF?format=jpg&name=4096x4096  


https://twitter.com/3939sakutyan/status/1781575251929027008  
お花もたくさんきてたよぉ😆🌷  
还送来了很多鲜花😆🌷   
https://pbs.twimg.com/media/GLltN9lbAAARk31?format=jpg&name=large  
https://pbs.twimg.com/media/GLltN91bsAEntn6?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLltOrPa0AAqWqH?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLltO2RaYAABRTE?format=jpg&name=4096x4096


https://twitter.com/CH_0326M/status/1781572293732528433  
やっと念願のGALLERY ZENONさん行ってきた！  描き下ろし本当に素敵だった💓💓  個人的には「こもれ陽の下で…」や「SPLASH!」の原稿拝見出来たのも嬉しかった〜圧巻でした！  行ける方は是非‼️  
我终于来到了向往已久的 GALLERY ZENON！ 那里的画真的很精彩💓💓。 就我个人而言，我很高兴能看到"阳光少女"和 "SPLASH!"的手稿，令人叹为观止！ 如果您能去，请一定要去 ‼️    
https://pbs.twimg.com/media/GLlqjNdbQAAvZky?format=jpg&name=large  
https://pbs.twimg.com/media/GLlqjNfbUAAdkMN?format=jpg&name=large


https://twitter.com/ougi_chs/status/1781563976264110378  
香のプンプン、おしおきパンケーキ🥞  （もっこりコラボ⛳）  ＆北条司ブレンド☕  
香的Punpun、Oshioki 煎饼🥞（Mokkori 合作⛳）＆北条司Blend ☕   
https://pbs.twimg.com/media/GLli_pmaMAAML5l?format=jpg&name=medium  


https://twitter.com/rabbit77625/status/1781537641600790815  
\#北条司展 に行ってきた！  先生ご自身からのお祝い花  ほぼ写真撮れるのはありがたい...！  TLのみんなさんの情報のおかげでA3硬質ケースを持参し、無事に獠ちゃんの紙ボードを回収  XYZ！  新宿駅から少し遠いけど(笑  
我去参加了#北条司展！ 老师亲自送上的祝贺花。 谢天谢地，我几乎可以拍照了..！ 多亏了 TL 大家提供的信息，我带着我的 A3 硬盒，安全地领取了獠chan 的纸板！  XYZ！ 虽然离新宿站有点远，但还是笑了   
https://pbs.twimg.com/media/GLlLCmyakAANoCS?format=jpg&name=large  
https://pbs.twimg.com/media/GLlLCmwb0AEk793?format=jpg&name=large  
https://pbs.twimg.com/media/GLlLCmvb0AADc3W?format=jpg&name=large  
https://pbs.twimg.com/media/GLlLCm0a4AALi0U?format=jpg&name=large  


https://twitter.com/rabbit77625/status/1781537651746893832  
香ちゃんかわいいな♪  もっこりと一緒に  撮りたかったワイ(ry   (後ろに怒ってる香が...  
香太可爱了......我想和 Mokkori 合影......（背景是生气的香.....  
https://pbs.twimg.com/media/GLlLDNAbcAEtCR6?format=jpg&name=large  
https://pbs.twimg.com/media/GLlLDM9bMAAbTrs?format=jpg&name=4096x4096
https://pbs.twimg.com/media/GLlLDM8aEAA-1r1?format=jpg&name=medium  
https://pbs.twimg.com/media/GLlLDM8aAAEBfiC?format=jpg&name=large  


https://twitter.com/hinatamiwa/status/1781321897617289326  
写真OKだけど写真じゃ絶対伝わらない美しさが生原稿にはあるので行ける人全員軽率に行った方がいい例えばコレまあ全部美しいんですけど個人的ツボは獠の足の下からの照り返しこれは生原稿じゃないと伝わらないてかそもそも週間連載であの作画カロリーと線の美しさ正確なデッサンまさに神のな   
https://pbs.twimg.com/media/GLiG0mCb0AApB21?format=jpg&name=4096x4096  


https://twitter.com/hinatamiwa/status/1781321872178893157  
生原稿しぬほど美しいので行ける人全員軽率に行った方がいいやっぱアナログ原稿っていいなー!!!!私シティーハンターが本当に大好きだったんですよガチでめちゃくちゃ模写した私が下手すぎて絵柄に全然残ってないけど2枚目写真左はこづかい貯めて買った当時の聖典です好きな原作の車はCR-Xで  
原稿太美了，能去的人都应该冒昧去看看.果然模拟原稿好啊!!!!我非常喜欢《城市猎人》。我在一张明信片上临摹了很多，因为我画得太烂了，完全没留下痕迹。第二张照片，左边是我积攒的零用钱买的那部经典。我最喜欢的原作车是CR-X。
https://pbs.twimg.com/media/GLiGy9RbcAAUgo0?format=jpg&name=large  
https://pbs.twimg.com/media/GLiGy9UaMAAj7Gi?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLiGy9TbcAAPSQg?format=jpg&name=large  
https://pbs.twimg.com/media/GLiGy9Sa0AELjnu?format=jpg&name=large  


https://twitter.com/ryosaeba357xyz1/status/1781278645820211249  
...  
https://pbs.twimg.com/media/GLhffMIbQAA485f?format=jpg&name=4096x4096


https://twitter.com/ryosaeba357xyz1/status/1781277255777607938  
...  
https://pbs.twimg.com/media/GLheM6obMAAp8E-?format=jpg&name=4096x4096  


https://twitter.com/ryosaeba357xyz1/status/1781276832568164567  
...  
https://twitter.com/ryosaeba357xyz1/status/1781959624096378885  
https://pbs.twimg.com/media/GLrK1aAaUAAaZjj?format=jpg&name=large  


https://twitter.com/ryosaeba357xyz1/status/1781276620155953275  
....  
https://pbs.twimg.com/media/GLhdpVvawAA_C9N?format=jpg&name=4096x4096  


https://twitter.com/ryosaeba357xyz1/status/1781274917293715717  
みんな、ギャラリーゼノンの帰りに井の頭公園スタバ向かいの「RYO」  で飲まない？☕😊  コーヒーもジュースも旨いよ・・・  最高☕😊  XYZ ・・・ギャラリーゼノンにて待つ  写真は #北条司展 開催初日、吉祥寺GALLERY ZENONにて  ・ 映画シティーハンター配信まであと6日❗❗❗❗❗❗❗❗❗❗❗❗❗❗・ XYZ ・・・配信を待つ  予告を見たら大変楽しみになりました  🎬(^o^)／  XYZ ・・・4月25日配信を待つ  実写シティーハンター大変楽しみにしております。  スタッフさん4年前からありがとう  ・・・・  下記全てシティーハンターの宣伝です  XYZ ・・・4月23日都内某所で待つ  XYZ ・・・4月25日配信を待つ  シティーハンター盛り上がってますね～❗❗❗❗❗❗❗❗❗❗🔫😎🕶️  ＼(^o^)／o(^-^o)(o^-^)o(ｏ^-^)尸  \#映画シティーハンター #CityHunterNetflix #中野ブロードウェイ3階 #墓場の画廊 にて行われた #シティーハンターのポップアップストア大賑わいでした❗❗❗❗❗❗＼(^o^)／＼(^o^)／  墓場の画廊はみんなが夢をみられるところなのさ・・・🕶️😎  中野ブロードウェイ3階墓場の画廊のみなさんお疲れ様でした❗❗❗❗❗❗❗  今回のイベント最高でした❗❗❗❗❗  ＼(^o^)／＼(^o^)／o(^-^o)(o^-^)o  最高のシティーハンターのイベントでした❗❗❗❗❗❗❗❗❗❗❗❗❗  盛り上がった～❗❗❗❗❗😊👏👏👏👏👏👏👏o(^-^o)(o^-^)o(ｏ^-^)尸  サイコーでした❗❗❗❗🎉🎁🎂😎  http://hakabanogarou.jp/archives/52409  会場の中野ブロードウェイ3階に  上がる階段の途中に大きなポスターがあり会館が閉まるギリギリまで見てたな～  🕶️😎お疲れ様でした～👏👏👏👏👏👏👏👏👏👏👏👏👏👏わあ～o(^-^o)(o^-^)o 🕶️😎😊  下記も全てシティーハンターの宣伝です   #新宿観光案内所 の皆様  以前のシティーハンターの冴羽獠像が置かれていた時は大変お世話になりました  3月26日は冴羽獠の誕生日、そしてヒロイン槙村香の誕生日が3月31日  またシティーハンターのイベント  http://hakabanogarou.jp/archives/52409…  を3月20日から中野ブロードウェイ3階墓場の画廊で行う  墓場の画廊オーナーUC SAYAMAさんの誕生日が3月28日@dokuro_uc  そしてNetflixの実写版シティーハンターに主演する鈴木亮平さんの誕生日が3月29日@ryoheiheisuzuki  さらにお酒🍸デュワーズとコラボした神戸三宮の  #バーエーソリューションズ  @BARASOLUTIONS2  の開店記念日が3月15日にあります  http://bar-a-solutions.amebaownd.com/pages/216327/p…  BAR A-SOLUTIONS  神戸市中央区下山手通2-17-10ライオンビル三宮館3階  3月はシティーハンター関連のバースデーがたくさんある月間なんです  よかったらまた新宿観光案内所に冴羽獠像を置きませんか？🙇  今からでも🙇  今さらになってすみませんm(_ _)m  3月にそちらに冴羽獠像を置く提案、すっかり遅くなってしまいました、よかったら今からでも置きませんか😅❔  僕の人生観を変えてくれたシティーハンター  下記も全てシティーハンターの宣伝です  YouTubeのボートレース下関チャンネル見て❗❗  シティーハンターみたいでカッコイイから❗❗❗❗❗❗❗❗❗❗❗❗❗  http://youtu.be/4QzsAANA7ho?si  ボートレース下関❗❗❗❗❗🚤🚤🚤  http://boatrace-shimonoseki.jp/sp/  、シティーハンター全国で盛り上がれ❗❗  全力で、睡眠を削り、仕事に支障をきたしても宣伝致します  🔨(^o^)🍺(^o^)🔨(^o^)🍺(^o^)🔨  もっこり美女のお姉さんありがとう❗❗❗❗❗❗❗❗❗❗❗❗❗❗  お酒のデュワーズのイベント楽しかった～  😍😍😍😍😍😍😍👏👏👏👏👏👏👏http://dewars-jp.com/cityhunter-mov…  #デュワーズ  劇場版シティーハンター大ヒット❗❗  全国で盛り上がれ❗❗❗❗❗❗❗❗   総武線に乗って  #聖地新宿 へ  #神谷明  #伊倉一恵  #玄田哲章 #一龍斎春水 #小山茉美 #堀内賢雄 #沢城みゆき  #鈴木亮平 #ClTYHUNTER   #北条司 #アニメ #マンガ\#キャッツアイ  キャッツ❤️アイ  #TMNETWORK #ゲットワイルド #GetWild  #小室哲哉 #劇場版シティーハンター #海坊主 #北九州総合観光案内所 #吉祥寺 #槙村秀幸 #GALLERYZENON #シティーハンター39周年  #シティーハンタープレミア #野上冴子 #冴羽獠 #槙村香 #ネトフリ #ネットフリックス #NetFlix #安藤政信 #木村文乃 #華村あすか #橋爪功 #杉本哲太 #水崎綾女 #迫田孝也 #ギャラリーゼノン  @moritamisato  @FUMINOKIMURA23  #家ついて行ってイイですか    に出た谷岡正規です   
鈴木亮平さん花が咲かれましたね  
大家，我在从Gallery Zenon回来的路上，井之头公园星巴克对面的「RYO」   不喝酒？ ☕😊  咖啡和果汁最好吃......  XYZ ・・・ 在泽农画廊等候  这张照片是在 #北条司展 活动的第一天拍摄的。  在吉祥寺GALLERY ZENON  ・距离电影《城市猎人》上映还有6天❗❗❗❗❗❗❗❗❗❗❗❗❗❗・ XYZ ・・・ 等待交货  
当我看到预告片时，我非常🎬(^o^)/兴奋  XYZ ・・・ 等待 4 月 25 日发布  我非常期待真人版《城市猎人》。  在过去的4年里，感谢你们，工作人员..  以下都是城市猎人的广告  XYZ ・・・ 4月23日 在东京某处等待  XYZ ・・・ 等待 4 月 25 日发布  城市猎人令人兴奋~❗❗❗❗❗❗❗❗❗❗🔫😎🕶️ (^o^)/o（^-^o）（o^-^）o（o^-^）尸  \#シティーハンター #映画シティーハンター #CityHunterNetflix #中野ブロードウェイ3階 #墓場の画廊快闪店人满为患❗❗❗❗❗❗ (^o^)/(^o^)/  墓地里的艺术画廊是每个人都可以梦想的地方...... 🕶️😎  感谢中野百老汇3楼墓地画廊的各位工作人员，感谢你们的辛勤工作❗❗❗❗❗❗❗  这个活动是最好的❗❗❗❗❗(^o^)/(^o^)/o（^-^o）（o^-^）o  这是最好的城市猎人活动❗❗❗❗❗❗❗❗❗❗❗❗❗  兴奋~ ❗❗❗❗❗😊👏👏👏👏👏👏o（^-^o）（o^-^）o（o^-^）尸 ❗❗❗❗🎉🎁🎂😎 太棒了  http://hakabanogarou.jp/archives/52409  在上楼梯到会场中野百老汇3楼的路上有一张大海报，一直看到大厅关门的最后一刻~🕶️😎谢谢你的辛苦~👏👏👏👏👏👏👏👏👏👏👏👏👏👏わあ~o（^-^o）（o^-^）o 🕶️😎😊  以下是所有 City Hunter 促销活动 #新宿観光案内所  我非常感激之前的城市猎人獠手办被放置时  3月26日是佐叶的生日，女主角槇村香的生日是3月31日  还将有一个城市猎人活动 http://hakabanogarou.jp/archives/52409...  将于3月20日在中野百老汇3楼的画廊举行  墓地画廊的老板佐山分校（UC SAYAMA）先生/女士的生日是3月28th@dokuro_uc日  出演 Netflix 真人版《城市猎人》的铃木良平先生/女士将于 3 月 29@ryoheiheisuzuki 日过生日  此外，神户三宫与清酒🍸杜瓦瓶合作的 #バーエーソリューションズ @BARASOLUTIONS2  3月15日是开店纪念日  http://bar-a-solutions.amebaownd.com/pages/216327/p......  BAR A-解决方案  神户市中央区下山手通2-17-10 狮子楼三宫馆3楼  三月是与城市猎人有关的生日的月份  如果你不介意，为什么不把獠的雕像再次放在新宿旅游信息中心呢？ 🙇  即使是🙇现在  对不起，现在更是如此 m(_ _)m  3月份在那里放置獠雕像的提议已被推迟。  如果你不介意，为什么不现在就放呢？ 😅❔  《城市猎人》改变了我的人生观  以下是City Hunter的所有广告  观看❗❗划船比赛下关频道  这就像一个城市猎人，很酷❗❗❗❗❗❗❗❗❗❗❗❗❗  
http://youtu.be/4QzsAANA7ho?si  划船比赛 下关 ❗❗❗❗❗🚤🚤🚤  http://boatrace-shimonoseki.jp/sp/ 、城市猎人：全国各地的盛况 ❗❗  我们会尽最大努力促进，即使它减少了睡眠并干扰了工作  🔨(^o^)🍺(^o^)🔨(^o^)🍺(^o^)🔨  Dewar的Event的活动很有趣~ 😍😍😍😍😍😍😍👏👏👏👏👏👏👏  http://dewars-jp.com/cityhunter-mov... #デュワーズ 城市猎人电影大片 ❗❗  全国各地的盛况❗❗❗❗❗❗❗❗   
https://pbs.twimg.com/media/GLhcFMebEAAeFpl?format=jpg&name=4096x4096  



https://twitter.com/ryosaeba357xyz1/status/1781270243224248810  
...  


https://twitter.com/masatoshi_s00/status/1781266892482879550  
お仕事半休とって北条司展へ。平日だったのでゆっくり見ることができました。やっぱり綺麗で繊細な絵だなぁ^ ^ 原画見てジャンプで読んでいた頃を思い出してノスタルジックな気分になってしまった。また来たいな。  #シティハンター #キャッツアイ #北条司展   
我请了半天假去参观北条司展。 因为是工作日，所以我可以慢慢参观。 我想起了以前在《JUMP》上阅读原画的时光，感到十分怀念。 我想再来一次。   
https://pbs.twimg.com/media/GLhUyjEbEAAz6JE?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLhUyjEagAAjUVS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLhUyjHbwAEFJqu?format=jpg&name=4096x4096  


https://twitter.com/shiropome828/status/1781253815913886021  
カフェにて🎵  ♥もっこり♥  
在咖啡馆 🎵 ♥Mokkori♥    
https://pbs.twimg.com/media/GLhI5gJbsAAnp9S?format=jpg&name=large  
https://pbs.twimg.com/media/GLhI5gOa0AA9fiF?format=jpg&name=large   
https://pbs.twimg.com/media/GLhI5jCaAAAzwu8?format=jpg&name=large  


https://twitter.com/hanako_fanks/status/1781251995418206592  
東京前乗りして行ってきましたー   
我提前去了东京！  
https://pbs.twimg.com/media/GLhHPrjbwAAxrOJ?format=jpg&name=large  
https://pbs.twimg.com/media/GLhHPrjacAAC11v?format=jpg&name=large  


https://twitter.com/0K8n3/status/1781251134918902097  
『#北条司展』in GALLERY ZENON  行ってきました〜♡！お花が沢山あってXYZ伝言板の前はいい匂いです💐ゆっくりじっくり観れて大変良かったです！スプラッシュ!3とゆう作品読んだ事がないので、是非読みたい( ◠‿◠ )  鈴木亮平さん冴羽も、もう少し！  
我去了那里♡！ 在 XYZ 留言板前有很多花，闻起来很香💐，能慢慢地四处看看非常好！ 我从来没有读过《Splash！3》，所以我真的很想读它(◠‿◠◠)  铃木亮平冴羽也快到了！   
https://pbs.twimg.com/media/GLhGV0BbgAAgMz9?format=jpg&name=large  
https://pbs.twimg.com/media/GLhGV0BbkAAq_1s?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLhGV0Ea0AAnL3C?format=jpg&name=large  


https://twitter.com/orange_chipmunk/status/1781241681456820323  
実は仕事で近くまではよく行っている。  でもまだ行けてない。  
事实上，我经常去附近工作。 但我还没能去那里。   
https://pbs.twimg.com/media/GLg93CAawAA_j1y?format=jpg&name=medium   



https://twitter.com/amane_00/status/1781238072040100073  
行って参りました、眼福でした☺️とあるカラー原画の先生コメントに崩れ落ちそうになっちゃった(良い意味でですよ)  後期も楽しみ♪   
我去看了，真是大开眼界☺️当老师评论其中一幅原色画时，我差点崩溃（是褒义）。我很期待後期    
https://pbs.twimg.com/media/GLg6jMjbMAAscLc?format=jpg&name=medium
https://pbs.twimg.com/media/GLg6joqakAA7ieY?format=jpg&name=large  
https://pbs.twimg.com/media/GLg6kLxaoAAVo5s?format=jpg&name=large  
https://pbs.twimg.com/media/GLg6kvobEAAVnPM?format=jpg&name=4096x4096  


https://twitter.com/riyoko_kisaki/status/1781236492352012601  
キャッツ参上🐱     
Cats 登台  
https://pbs.twimg.com/media/GLg5I_SbEAEcx3F?format=jpg&name=medium  
https://pbs.twimg.com/media/GLg5JOUaUAAhQ5t?format=jpg&name=large  
https://pbs.twimg.com/media/GLg5JaqbYAAe8l1?format=jpg&name=4096x4096    


https://twitter.com/RYO_usamame/status/1781235531025981944  
GALLERY  ZENONのカフェスペースはあまりスペースがなく席がギュッとされており食べ終わったら席を譲って下さいというお願いもあったので以前のようにゆったりゆっくりは出来ないです😅獠のもっこりオムライスを恥ずかしげもなく頼めるのは長年のファンだから？🤣ボリューム少なめでした💦  
GALLERY  ZENON的咖啡厅空间并不宽敞，座位挤得满满当当，吃完了还要让座😅😅我是老粉丝，可以肆无忌惮地要一份Mokkori蛋包饭😅😅，因为我是老粉丝？🤣份量很小💦  
https://pbs.twimg.com/media/GLg4Rfta4AAZuxl?format=jpg&name=large  


https://twitter.com/RYO_usamame/status/1781233311811289150   
GALLERY  ZENON 北条司展へ🎶新しい形のお店ですね☺️北条先生の原画を間近でじっくり見られるの幸せ💕前期後期に分けてるのでシティーハンターが沢山というイメージはなくキャッツアイ40周年原画展がCH存分に楽しめたので来年開催されるであろうCH40周年原画展に期待🤩あえてこの画像を😂    
GALLERY  ZENON 北条司展🎶这是一家新型的店🎶很开心能近距离看到北条的原画💕分为前期後期，所以没有很多城市猎人的形象💕猫眼40周年原画展我欣赏得很尽兴，所以我敢于期待明年即将举行的 CH 40 周年原画展🤩     
https://pbs.twimg.com/media/GLg2QZgbwAAtkmF?format=jpg&name=4096x4096  


https://twitter.com/riyoko_kisaki/status/1781231215816896881  
吉祥寺ギャラリーゼノンで開催中の #北条司展 へ。  シティーハンターやキャッツ・アイ、読切作品の原画を間近で拝見でき感無量…！撮影可SNS投稿常識内でOKというのもありがたいです。本編原稿の繊細さもさることながら、週刊連載でこれだけのカラーを…？という点でも驚き。想い溢れて胸いっぱい。  
参观吉祥寺Gallery Zenon 的 #北条司展。 能够近距离欣赏《城市猎人》、《猫眼》和短篇故事的原画，我非常感动.....！ 摄影可上传至SNS也是一件好事。 我对原稿的精致以及每周连载的色彩感到惊讶......？ 每周连载的色彩之丰富也令我惊讶。 我感慨万千，满心欢喜。   
https://pbs.twimg.com/media/GLg0VqgaUAAv7JS?format=jpg&name=medium  
https://pbs.twimg.com/media/GLg0V41bQAA1cLP?format=jpg&name=large  
https://pbs.twimg.com/media/GLg0WFubgAABDon?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLg0WJLaUAAr205?format=jpg&name=4096x4096  


https://twitter.com/Lucky_goddess_y/status/1781229475604345170  
た…楽しすぎる！後期も絶対遠征して出てこよう！  
…太开心了!后期也绝对要远征出来!  
https://pbs.twimg.com/media/GLgyuVnbMAAnPMl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLgyuVnbUAAylXq?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLgyuX6bIAA8_26?format=jpg&name=large  
https://twitter.com/i/status/1781229475604345170  



https://twitter.com/eijilove0329/status/1781226031602663688  
ダイエットやトレーニングが上手くいっているというのに、コンセプトカフェが大好き過ぎて、もっこりオムレツとおしおきパンケーキを両方食べるなんてアホだよね😁0.7キロ増えた💦大多数の方が両方召し上がっていましたのよ☺️  
尽管我的节食和训练进展顺利，但我太喜欢概念咖啡馆了，以至于我既吃了大块煎蛋，又吃了押切煎饼 😁 我的体重增加了 0.7 公斤 💦 大多数人都同时吃了这两种食物 💦 ☺️   


https://twitter.com/eijilove0329/status/1781219676447555998  
北条司展では基本的に原画の撮影はOKとなっていますが、ギャラリー内で流れる動画の撮影は禁止されています  北条先生が実際に描く動画を拝見して、益々原作者へのリスペクトは高まりましたし、原作者の意向、お気持ちを大切にして、映像作品が創られなければならないなと強く感じました  
在北条司展上，对原作拍照基本上是没问题的，但禁止拍摄展厅里播放的视频短片  看到北条老师实际绘制的视频片段，我对原作者更加肃然起敬，强烈地感受到视觉作品的创作必须考虑到原作者的意图和感受   
https://pbs.twimg.com/media/GLgp2Kzb0AAvGMD?format=jpg&name=4096x4096  



https://twitter.com/ryosaeba357xyz1/status/1781170113284350388  
XYZ ・・・ギャラリーゼノンにて待つ  写真は #北条司展 昨日開催初日、吉祥寺GALLERY ZENONにて  ギャラリーゼノンで🔫コルトパイソン357マグナム  と撮った写真です  スペシャリティ水出しアイスコーヒーは北条司ブレンド使用です☕😊/  
XYZ ・・・ 在 GALLERY ZENON等待  照片：昨天在吉祥寺 GALLERY ZENON 举办的 #北条司展的第一天  与 🔫 Colt Python 357 Magnum 在 Zenon 画廊合影  用北条司混合咖啡制成的特色水滴冰咖啡 ☕😊/  
https://pbs.twimg.com/media/GLf8v_EbYAASU7_?format=jpg&name=large  


https://twitter.com/mendesu1515/status/1781151468290265197  
仕事前に行ってきましたヽ(´▽｀)/  カフェが時間なくていけなかったからまたどこかで時間見て行こうかな😊  好きなシーンの原画あってドキドキしちゃった🤭    
我在上班前去了那里，真希望能亲临现场 😊 我发现了一幅我最喜欢的场景的原画，非常激动🤭。     
https://pbs.twimg.com/media/GLfrzPgaoAAMR4c?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLfrzPibgAAQ0jn?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLfrzPxaMAAqOF4?format=jpg&name=large  


https://twitter.com/ch_kaibarakai/status/1780954687589757060  
ひっそりお邪魔しました…幸せ空間でした…後期も楽しみです…  
我静静地参观着......这是一个快乐的空间......我期待着後期的到来...    
https://pbs.twimg.com/media/GLc416qasAAPwfI?format=jpg&name=4096x4096  


https://twitter.com/yume_sarakakera/status/1781966782636630132  
北条先生の原画展行ってきました😄  先生の原画を拝見できる幸せ空間、ありがとうございます！パンケーキもオムライスも美味しかったです♪ARも楽しいですね✨  
我去看了北条老师的原画展😄 谢谢你们给了我一个可以看到他原画的快乐空间！煎饼和蛋包饭很好吃✨ AR也很有趣✨。   
https://pbs.twimg.com/media/GLrRV1QbkAA4FFz?format=jpg&name=large  
https://pbs.twimg.com/media/GLrRV1RaAAAGezS?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLrRV1UaAAAiTey?format=jpg&name=900x900  


https://twitter.com/idumo/status/1780954118829568351  
ゼノンのテラス、色んな人からのお祝い花に囲まれてあの伝言板があったんだけれど、恐れ多くて何も書けなかった。写真撮っただけで胸いっぱい。2階のメッセージスケブにはお手紙書いてきた。上手なイラストは描けないから、本当にお手紙ね。  
在泽农的露台上，有一个留言板，周围摆满了许多人送来的祝贺鲜花，但我不敢写任何东西。 我在二楼的留言板上写了一封信。 我不会画插图，所以这真的是一封信。  
https://pbs.twimg.com/media/GLc4OJPboAAej89?format=jpg&name=large  
https://pbs.twimg.com/media/GLc4OJVasAAgl5D?format=jpg&name=large  
https://pbs.twimg.com/media/GLc4OJjaAAAkIfq?format=jpg&name=large  


https://twitter.com/idumo/status/1780954151801028719  
展示は本当に素敵だったんだけど、建物とか内装自体も素敵な感じで良かったよ。シャンデリアが漫画なの。凄くない?本物を加工したのかな。それともフェイクかな。面白いねぇ。  
展览确实不错，但建筑和内部装饰本身也很不错。 枝形吊灯是漫画书。 是不是很神奇？ 我想知道他们是否用了真品？还是赝品？ 很有趣。    
https://pbs.twimg.com/media/GLc4Vs8aAAAvvdJ?format=jpg&name=large  


https://twitter.com/CDG5fDJDaP1WZOw/status/1780937179419861231   
北条司先生が描く女性はどなたも美しくそして艶やかで…思春期の自分には刺激的でした。#北条司展 #GALLERYZENON  
北条司所画的女性都是美丽迷人的......这对青春期的我刺激很大。   


https://twitter.com/newgate72/status/1780929533606240367  
コラボメニューは、  
・獠のもっこりオムライス(¥1580)  
・今宵あなたのハートをいただきに参ります(¥1200)  
の2品を注文✨味もちゃんと美味しく、特典も充実！冴羽獠の厚紙がちゃんと厚いのは嬉しすぎる🤩  
合作菜单如下  
・獠的Mokkori 蛋包饭（1580 日元）  
・我今晚来取你的心（¥1200）  
菜单味道鲜美，还有很多额外的内容！ 我太开心了，冴羽獠的纸板厚得恰到好处🤩   
https://pbs.twimg.com/media/GLch-FAbwAA_aih?format=jpg&name=large  
https://pbs.twimg.com/media/GLch-FAasAAbVK2?format=jpg&name=large  
https://pbs.twimg.com/media/GLch-E9acAAEfbK?format=jpg&name=large  


https://twitter.com/newgate72/status/1780927802881257499  
展示とか細かい案内とかも良かったです✨    
我喜欢展览和详细的指导✨
https://pbs.twimg.com/media/GLcgZWfbkAAipkk?format=jpg&name=large  
https://pbs.twimg.com/media/GLcgZWjbIAAjW0-?format=jpg&name=large  
https://pbs.twimg.com/media/GLcgZWea0AAr8IK?format=jpg&name=large  
https://pbs.twimg.com/media/GLcgZWjacAATOG6?format=jpg&name=large  


https://twitter.com/Ko_ichi_32/status/1780923756699156861  
GALLERY ZENON オープン  記念合同プレゼントキャンペーン  🙌🏻👏🏻🙌🏻👏🏻🙌🏻👏🏻🙌🏻👏🏻  当時のジャンプ少年としては  キャッツ・アイからシティハンターと  本当に毎週が楽しみだった😊  🤨まさにジャンプ超黄金期の北条司の作品に出会えたことは  自分の宝物の一つだと思う👍🏻  
GALLERY ZENON开放  联合礼物纪念活动 🙌🏻👏🏻🙌🏻👏🏻🙌🏻👏🏻🙌🏻👏🏻  作为当时的Jump少年  从猫眼到城市猎人  我真的每周都很期待😊  那正是《JUMP》的超级黄金时代  能够接触到北条司的作品是  我认为这是我的珍宝之一。👍🏻   

https://twitter.com/yfdm0127abc/status/1780923311586984421  
先程北条司展へ行ってきました🚗  欲しかったものが売り切れていて少しショックでしたが本当に大満足でした🥹  複製原稿はこちらを購入しました  XYZピアスもお気に入り♡  カフェも素敵でしたよ☕*°  
我刚刚去看了北条司展🚗  我发现自己想要的作品都卖光了，这让我有点震惊，但我真的非常满意🥹  我在这里买了一份手稿  XYZ 耳环也是我的最爱♡  咖啡馆也很可爱☕*°   
https://pbs.twimg.com/media/GLccP6laQAAa4xx?format=jpg&name=large  
https://pbs.twimg.com/media/GLccP6mbYAAbJaM?format=jpg&name=large  
https://pbs.twimg.com/media/GLccP6nbQAAJ6D8?format=jpg&name=large  
https://pbs.twimg.com/media/GLccP6pasAA8zy-?format=jpg&name=large  


https://twitter.com/naora741529/status/1780915287891833252  
https://pbs.twimg.com/media/GLcVA_ra8AA8rRe?format=jpg&name=medium  
https://pbs.twimg.com/media/GLcVA_ob0AAxD5y?format=jpg&name=4096x4096


https://twitter.com/naora741529/status/1780914754548273583  
北条司先生の作品はどれも本当に素晴らしいくて最高です！😆💖✨  子供の頃からずっと大好きだから今回の展示会はとても嬉しいです♪💖  北条司サイン入りA1ポスター  当たるとすごく嬉しいです🥰  絶対に行きます！👍  これからも応援していきます！😊✨  
北条司先生的所有作品都非常棒，是最好的！ 😆💖✨  我从小就很喜欢他的作品，所以这次展览我非常高兴💖  北条司签名的 A1 海报  如果我获奖了，我会非常高兴🥰  我一定会去的！ 👍  我会继续支持你的！ 😊✨    


https://twitter.com/TheEdition88/status/1780900139810611582  
【#北条司展】大盛況‼️  完売&残りわずかな人気商品、#シティーハンター のトートバッグやアートタイル等、只今増産中💪💦  ＊オンラインショップでは5/20正午より販売スタート！  
【#北条司展】大获成功 ‼️  售罄，仅剩少量人气商品，如#城市猎人手提袋和tote bag，现正在生产中💪💦  网上商店将于 5/20 日中午开始销售！   
https://pbs.twimg.com/media/GLcGYMHacAAowz9?format=jpg&name=medium  
https://pbs.twimg.com/media/GLcGYMFbIAA1IjD?format=jpg&name=large  


https://twitter.com/aikawa_CH1987/status/1782176687645667495  
先生サインもめっちゃ大きくて最高です🥰🥰  
老师的牌子也非常大，非常棒 🥰 🥰   
https://pbs.twimg.com/media/GLuQP6ea8AA0CxM?format=jpg&name=large  


https://twitter.com/eijilove0329/status/1780899084183937526  
書く所に空きがなかった～😅   
没有地方写😅。  
https://pbs.twimg.com/media/GLcGR1ibcAAe5jy?format=jpg&name=large  


https://twitter.com/GalleryZenon/status/1780898706914681009  
ギャラリーゼノンのテラスには  あの⁉️「伝言板」がありまして、、  ご来場の方は自由に書くことができます📝✨  皆さんも是非😘  XYZ・・・  
在泽农画廊的露台上。  有一个 ⁉️"留言板"、、  游客可以自由地写下📝✨希望大家喜欢😘XYZ...  
https://pbs.twimg.com/media/GLcF73NaIAAGoa1?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLcF73ObcAAVtn0?format=jpg&name=4096x4096  


https://twitter.com/newgate72/status/1780892365680746525  
Netflixの実写シティハンター見る前に来られて良かった！！  めっちくちゃ！！カッコ良い！！🤩  #北条司展 #GALLERYZENON #キャッツアイ #シティーハンター  
很高兴我在观看 Netflix 上的真人版《城市猎人》之前就来了！  太酷了 太酷了 🤩🤩#北条司展 #GALLERYZENON#猫眼#城市猎人  
https://pbs.twimg.com/media/GLcAKbzbIAAKQs7?format=jpg&name=large  
https://pbs.twimg.com/media/GLcAKbrasAEocVo?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLcAKbqaMAATQEO?format=jpg&name=4096x4096


https://twitter.com/newgate72/status/1780891597238075586  
北条司展、行ってきましたよー✨  藤原紀香さんからお花！w  #北条司展 #GALLERYZENON #キャッツアイ #シティーハンター  
我去看了北条司展✨ 藤原纪香送的花！ w #北条司展 #GALLERYZENON #猫眼 #城市猎人  
https://pbs.twimg.com/media/GLb_dkFa4AA3Ebz?format=jpg&name=large  
https://pbs.twimg.com/media/GLb_dkEbcAANuSQ?format=jpg&name=large  
https://pbs.twimg.com/media/GLb_dkEaUAAnLmL?format=jpg&name=large  
https://pbs.twimg.com/media/GLb_dkQbwAAIf9A?format=jpg&name=large  


https://twitter.com/Take4Shimo/status/1780888628685902216  
北条先生のコメント  
デジタルも良いけど「手描きは紙との真剣勝負」  
ハンドメイド作業を趣味とする私（若い頃は絵も書いていましたし、ハンドでセル画も書いていました）には、とても心に響く言葉です  
そんな生原稿を見る機会は吉祥寺で！  
北条老师的评语  
数码固然好，但“手绘是与纸的较量”  
这句话对于爱好手工制作的我(年轻时也画画，也用手画赛璐珞)来说非常触动人心  
在吉祥寺能看到这样的原稿!  
https://pbs.twimg.com/media/GLb8xQ0bgAAT2hE?format=jpg&name=large  
https://pbs.twimg.com/media/GLb8xHVbEAATrva?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLb8xOTbEAA_6I4?format=jpg&name=large  
https://pbs.twimg.com/media/GLb8xO7bkAAtdWm?format=jpg&name=large  


https://twitter.com/chibi_aya_728/status/1780887816152744095  
昨日は原画だけをひたすらに浴びて仕事にダッシュだったので、今日はゆっくりとカフェも堪能しました🥰  お皿のセリフは一つ一つ違っていて、食べて美味しいし食べ進めるとまた楽しい、そんなお食事でした🤗  ボリューム満点です！  #北条司展  #GALLERYZENO  
昨天我匆匆忙忙去上班，只顾着沉浸于原画，所以今天我也慢慢享受咖啡馆的乐趣🥰  盘子里的每一道菜都不一样，吃起来很美味，吃下去又很有趣🤗  份量很足！  
https://pbs.twimg.com/media/GLb8BbfbYAAFKQl?format=jpg&name=medium  
https://pbs.twimg.com/media/GLb8BkzacAAa9GD?format=jpg&name=medium  
https://pbs.twimg.com/media/GLb8B1EbMAAif1N?format=jpg&name=medium


https://twitter.com/an_maagarin/status/1780883041784893523  
今日は吉祥寺、北条司さんの原画展(前期)にて  ゼノンの前形態をやめて、ギャラリーにしたというところでこけら落としになるのかな  ずっと観てても飽きないとはこのことで  ちなみに、桜の木の話がある短編集含め「こもれ陽の下で」で本格的から入った口なので  
今天在吉祥寺，举办了北条司的原创艺术展（前期）  不知道这是否会是他停止 Zenon 以前的形式并将其改造成画廊的地方的首次展览。  即使一直看，我也不会觉得无聊。  顺便一提，包括有樱花树故事的短篇集在内的《阳光少女》完整故事。  
https://pbs.twimg.com/media/GLb3rK4a0AAwirs?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLb3rN0bwAAtIiQ?format=jpg&name=large  
https://pbs.twimg.com/media/GLb3rM1bYAAuj3B?format=jpg&name=large  
https://pbs.twimg.com/media/GLb3rM1bMAAT52a?format=jpg&name=4096x4096  


https://twitter.com/Ritter_k333/status/1780874786857681125  
\#北条司展 行ってきました😊  
フードファイトしちゃいました😅オムライスとパンケーキ同じぐらいの高さだった💦水分もヤバかったけど金平糖とアラザンもヤバかった…甘い😱しかしながらご一緒させていただいたフォロワーさんのおかげで挫折せず楽しめました✨  
ありがとうございました🙏🏻✨   
https://pbs.twimg.com/media/GLbwLYwa8AAmy2v?format=jpg&name=large  
https://pbs.twimg.com/media/GLbwLYyaMAApTmx?format=jpg&name=large  


https://twitter.com/eijilove0329/status/1780871552822886778  
もっこりオムライスと、おしおきパンケーキをいただきました #北条司展   
我点了 Mokkori 煎蛋和煎饼。  
https://pbs.twimg.com/media/GLbtO0abQAAs6Eh?format=jpg&name=large  
https://pbs.twimg.com/media/GLbtO07acAAgEjF?format=jpg&name=large  


https://twitter.com/eijilove0329/status/1780870578309837187  
生原画、贅沢過ぎて夢のような世界でした☺️    
那是一个梦幻般的原创艺术世界，太奢侈了  
https://pbs.twimg.com/media/GLbsV1Mb0AAyhNl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLbsV2Ta0AA4xsS?format=jpg&name=large  


https://twitter.com/idumo/status/1780869644515180744  
行ってきたわ。 私にとって神様のような方の軌跡。生原画、生原稿達が本当に美しくてね…2階にも展示スペースがあるんだけど、スペース奥のガラスケースでテンション爆上がりしたよね。気になる人はご自分の目で確かめて来てください。もしくは、私にDMくださいw 画像送りますwww     
我去过那里。我去看了一个人的作品，他对我来说就像神一样。 原画和手稿真的很美......二楼有一个展览空间，但空间后面的玻璃柜让我非常激动。 如果你感兴趣，请亲自来看看。 或者给我发个 DM，我把图片发给你。     
https://pbs.twimg.com/media/GLbrf09awAAzkSb?format=jpg&name=large  
https://pbs.twimg.com/media/GLbrf07aIAA8TDA?format=jpg&name=large  
https://pbs.twimg.com/media/GLbrf07bwAAQDQs?format=jpg&name=4096x4096   
https://pbs.twimg.com/media/GLbrf0-aYAECx5m?format=jpg&name=large  


https://twitter.com/deshi40673303/status/1780860109389992234  
これはとても欲しい！！家族みんな好きだから、特に親にサプライズでプレゼントしたい！！  
我太想要这个了！ 我全家人都喜欢，尤其是我的父母，我想给他们一个惊喜！   


https://twitter.com/memn0ck/status/1780858099748032684  
\#北条司展 行ってきた。展示数は少なめ（展示スペース狭め）だけど、展示されてる原稿はポイント付いてて楽しめたかな。吉祥寺で近いのはよき https://prtimes.jp/main/html/rd/p/000000247.000124073.html  
我去看了#北条司展。 虽然展品数量不多（展览空间狭窄），但我很喜欢展出的手稿。 好在离吉祥寺很近。  
https://pbs.twimg.com/media/GLbg_eea8AAyNFn?format=jpg&name=large  
https://pbs.twimg.com/media/GLbg_gUaIAAv2Fx?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLbg_qLbQAAl0cn?format=jpg&name=large  
https://pbs.twimg.com/media/GLbg_rQb0AAKASS?format=jpg&name=large  


https://twitter.com/memn0ck/status/1780860652506194054  
https://twitter.com/memn0ck/status/1780860687222468616  
ついでに武蔵野市のデザインマンホールも回ってきた。吉祥寺北側にまとまってるから吉祥寺行きなれてれば歩いてでも20分くらいで回れるかな。シティーハンターとキャッツ🖤アイ、北斗の拳、花の慶次、週末のワルキューレ、よろしくメカドック、ワカコ酒  
我还绕着武藏野市的设计沙井转了一圈。 这些窨井都位于吉祥寺的北侧，所以如果你习惯去吉祥寺的话，步行大概 20 分钟就能走遍。 《城市猎人》、《猫眼》、《北斗神拳》、《花之姬》、《女武神周末》、《机甲码头》、《若子酒》   
https://pbs.twimg.com/media/GLbjTgYbMAAlUA8?format=jpg&name=large  
https://pbs.twimg.com/media/GLbjThdbkAAYGdC?format=jpg&name=large  
https://pbs.twimg.com/media/GLbjTotaUAAXHMF?format=jpg&name=large  
https://pbs.twimg.com/media/GLbjTpsaAAAILkJ?format=jpg&name=large  

 

https://twitter.com/gomamochi014/status/1780855475342569590  
最高すぎました🥲🫶🏻🫶🏻  
太好吃了🥲🫶🏻🫶🏻  
https://pbs.twimg.com/media/GLbemcDaUAAR1YU?format=jpg&name=large  
https://pbs.twimg.com/media/GLbemcAa8AAtaJw?format=jpg&name=large  


https://twitter.com/teru_607/status/1780833084478509464  
入場者特典で配布されたカードが3種類あることを忘れていたわ。  確認したらシークレットのカードだった。あの絵柄は現地で拝見してとても気に入ったからとても嬉しい🥰 #北条司展 #シティーハンター #キャッツアイ #GALLERYZENON  
我忘了有三张不同的卡片作为入场奖励。  我查了一下，原来是秘密卡。 我非常高兴，因为我在那里看到了那个图案，非常喜欢它🥰 #北条司展  #城市猎人 #猫眼 #GALLERYZENON  


https://twitter.com/Take4Shimo/status/1780821615376503158  
【おっさんユルユルライフ】vol.1380  今日のおでけけ先は吉祥寺  昨日オープンした画廊の「#北条司展」に行ってきました  手描き、ペン画、筆塗りで書き上げた生原稿、原画の数々  良いですねぇ  展示内容はこれから行かれる方も多いと思いますので伏せておきますね  20240418  老人的裕如生活] vol.1380  
今天的目的地是吉祥寺。  我去看了昨天开幕的画廊的「#北条司展」。  那里有很多手绘的笔墨画、用毛笔写成的原始手稿和原画。  非常不错！  展览的详细情况我就不说了，相信以后会有很多人前来参观。 20240418  


https://twitter.com/chiyo45/status/1780819753965326654  
素晴らしすぎました。直筆原画の迫力ってやっぱり凄い。力強くてしなやかで、目を疑う様な細かさに心を奪われて、全然見飽きず4周くらいした😂  吉祥寺はちょっと遠かったねえ  
这太美好了，不可能是真的。 手绘原稿的力量令人惊叹。 我被那些有力、柔美、让人瞠目结舌的细节深深吸引，转了四圈都没看腻😂。  吉祥寺有点远。  
https://pbs.twimg.com/media/GLa-IJCa4AAjyPK?format=jpg&name=large  
https://pbs.twimg.com/media/GLa-II_a8AALhVh?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLa-IJAaUAAcEw4?format=jpg&name=4096x4096  


https://twitter.com/taimama26/status/1780806367785460001  
うまく撮影できなかったけど、お仕置きパンケーキ⚒  #ギャラリーゼノン #北条司展  
没能拍到好照片，但煎饼很好吃⚒。  #画廊 Zenon #北条司展  


https://twitter.com/mangatoongakuto/status/1780776909275840983  
北条司展にて  観たかった原画  桜の花咲くころ  植物の細微な光をどのように仕上げているか小さい花々の線など  これらに興味があり  原画展に行ってびっくりしたのが  樹肌の凹凸表現‼️💦💦  光影、凹凸  想像世界を表現する為の限りない執着心  圧倒された😱💦  #北条司展 #GALLERYZENON  
在北条司展上  我想看的原画  樱花盛开时  我对他如何完成植物的微妙光线、小花的线条等很感兴趣。  我对这些东西很感兴趣。 当我去看展览时，我很惊讶。 树皮的凹凸不平‼️💦💦。 光与影，凹凸不平  表达想象世界的无尽执着  我被征服了😱💦#Hojo Tsukasa 展览#GALLERYZENON  
https://pbs.twimg.com/media/GLaXKJVa8AAP8nG?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLaXKJUaIAAeA0i?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLaXKJVaAAADX2V?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLaXKJRbMAAix0D?format=jpg&name=4096x4096  



https://twitter.com/mangatoongakuto/status/1780770855389401290  
展示会では  北条司先生による下記原画の制作映像が  流れるブースがある。  線の美しさにただただ驚くばかり。あの制作動画は会場でしっかり観てほしい。（制作動画だけは撮影禁止）  毛髪のハイライトのバランス加減も   カラーの濃淡も   一点の迷いもなく  仕上げられていく。#北条司展 #GALLERYZENON  
展览现场  播放北条司制作以下原画的视频  展示了北条司的作品。 线条之美令人惊叹。 请在展览现场观看制作视频。 (只有制作视频不得拍摄）头发高光的平衡。 色调的处理毫不拖泥带水。 #北条司展  #GALLERYZENON  
https://pbs.twimg.com/media/GLaRp0JaUAAW127?format=jpg&name=4096x4096  


https://twitter.com/may1025/status/1780707698281263456  
昨日オープンした🎉#GALLERYZENON で開催されている #北条司展 に行ってきました＼(^-^)／生原稿、堪能しましたっ🎶（撮影🆗のエリアがけっこうあるんです☺️）  
我去了昨天开幕🎉的 #GALLERYZENON 举行的 #北条司展（^-^）/我很喜欢🎶原始手稿（有☺️相当多的拍摄🆗区域）   
https://pbs.twimg.com/media/GLZYDeCa0AAJE_6?format=jpg&name=large  
https://pbs.twimg.com/media/GLZYLrsbYAAUWG7?format=jpg&name=large  
https://pbs.twimg.com/media/GLZYMeEaAAAY_S9?format=jpg&name=medium  
https://pbs.twimg.com/media/GLZYNrfaUAAME7Z?format=jpg&name=medium  


https://twitter.com/dgrain/status/1780699036699545847  
相変わらずAR操作が苦手デス🥺  リベンジせねば！ #北条司展  
我还不擅长AR操作，所以我🥺必须报仇！ #北条司展    
https://pbs.twimg.com/media/GLZQVkkaYAA9xLK?format=jpg&name=medium  
https://pbs.twimg.com/media/GLZQVlaa4AAewcu?format=jpg&name=medium  
https://pbs.twimg.com/media/GLZQVm0bAAADuvf?format=jpg&name=medium  


https://twitter.com/dgrain/status/1780694069284679915  
GALLERY ZENON 併設のカフェ' CAfE ZENON 'にてコラボメニュー😋そして、北条司スペシャルブレンドコーヒーお勧め☕ #北条司展  https://gallery-zenon.jp  
GALLERY ZENON 附设咖啡厅 "CAfE ZENON "的合作菜单😋和推荐的北条司特调咖啡☕ #北条司展 https://gallery-zenon.jp   
https://pbs.twimg.com/media/GLZL0SdbAAA8u9Z?format=jpg&name=large  
https://pbs.twimg.com/media/GLZL0VVbcAA7szt?format=jpg&name=large  
https://pbs.twimg.com/media/GLZL0X0agAAQDbG?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLZL0aCbMAA5uBO?format=jpg&name=large  



https://twitter.com/mogumog111/status/1780635323300733118  
吉祥寺の「GALLERY ZENON（ギャラリーゼノン）」本日初日の北条司展に行く。#シティーハンター や #キャッツアイ 等の原画に興奮！コラボメニューARキャラと撮影も可能で3種制覇。アナログの生原稿が美しすぎて鳥肌が立った！(ネタバレになる為原画はアップしませんが…)とにかく良かった！ #北条司展  
前往吉祥寺 GALLERY ZENON 观看今天开幕的北条司展 。 很高兴能看到 #城市猎人# 和 #猫眼# 的原画！ 合作菜单：3 种 AR 角色和拍照机会。 模拟原稿美得让我起鸡皮疙瘩！ (我不会上传原稿，因为那会破坏气氛......）总之，非常棒！#北条司展    
https://pbs.twimg.com/media/GLYWYk5a0AAdPwQ?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLYWYk9aMAAXgPw?format=jpg&name=small  
https://pbs.twimg.com/media/GLYWYk9akAE3OmV?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLYWYk7akAko6fa?format=jpg&name=small  


https://twitter.com/dgrain/status/1780694069284679915  
GALLERY ZENON 併設のカフェ' CAfE ZENON 'にてコラボメニュー😋そして、北条司スペシャルブレンドコーヒーお勧め☕ #北条司展  
合作菜单😋和北条司特制混合咖啡推荐☕ #北条司展在泽农画廊附属咖啡馆 "CAfE ZENON "举行  
https://pbs.twimg.com/media/GLZL0SdbAAA8u9Z?format=jpg&name=small  
https://pbs.twimg.com/media/GLZL0VVbcAA7szt?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLZL0X0agAAQDbG?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLZL0aCbMAA5uBO?format=jpg&name=900x900  


https://twitter.com/dgrain/status/1780691335072424112  
4/21(日)まで予約制 #北条司展  
仅限预约，4 月 21 日（周日）#北条司展  
https://pbs.twimg.com/media/GLZJVLBa4AAveOo?format=jpg&name=small  
https://pbs.twimg.com/media/GLZJVM2bUAAOFXK?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLZJVPhbUAA5ZOk?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLZJVSCbkAA42ez?format=jpg&name=small  


https://twitter.com/mogumog111/status/1780635323300733118
吉祥寺の「GALLERY ZENON（ギャラリーゼノン）」本日初日の北条司展に行く。#シティーハンター や #キャッツアイ 等の原画に興奮！コラボメニューARキャラと撮影も可能で3種制覇。アナログの生原稿が美しすぎて鳥肌が立った！(ネタバレになる為原画はアップしませんが…)とにかく良かった！  #北条司展  
前往吉祥寺 GALLERY ZENON 观看今天开幕的北条司展。 很高兴能看到 #城市猎人# 和 #猫眼# 的原画！ 合作菜单：3 种 AR 角色和拍照机会。 模拟原稿美得让我起鸡皮疙瘩！ (我不会上传原稿，因为那会剧透......）总之，非常棒！ #北条司展  


https://twitter.com/moritamisato/status/1780477991669436780
本日から開催の『#北条司展』にお邪魔してきました🔥  贅沢すぎる空間。亮平さん演じる冴羽さんポスターと、北条先生の描いた手書きの冴羽さんも並んでいました！グッズのXYZ帽子を被り記念にぱしゃり。なんと豪華な板挟み。  Netflix映画『シティーハンター』  いよいよ4月25日より配信です🎬  
我参观了今天开幕的『#北条司展』 🔥。 空间太豪华了。  这里有亮平扮演的冴羽的海报，还有北条老师手绘的冴羽！ 我戴上 XYZ 商品的帽子，拍了一张照片留作纪念。 好豪华的board。  Netflix 电影《城市猎人》。 终于要在 4 月 25 日开始发行了🎬。   
https://pbs.twimg.com/media/GLWHS20bcAA8tK2?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLWHS21a4AAW3tU?format=jpg&name=small  
https://pbs.twimg.com/media/GLWHS21aoAAv8vE?format=jpg&name=small  
https://pbs.twimg.com/media/GLWHS2wbUAADreU?format=jpg&name=4096x4096  


https://twitter.com/ayako0zy/status/1780611110774919640  
開催初日に行けました！北条先生の絵、やっぱり好きだなぁ～カフェは席待ちで結構待ちました。一人席があればもうちょっと回転率良くなるのでは？パンケーキ美味しかったけど、トッピングの金平糖がちょっと食べにくかったです（ガリガリ噛めない人なので）  
我在展览的第一天就去了！ 我还是很喜欢北条老师的画～我在咖啡馆里等了很久才等到一个座位。 如果有一人座的话，我想营业率会更好一些。煎饼很好吃，但上面的金平糖有点难吃（我不喜欢有嚼劲的）   


https://twitter.com/hamavha/status/1780603778804375939  
北条司展行ってきました！私は先生の作品をリアルタイムで触れていないので、どれも新しいもを観る感覚で楽しんできました  井上雄彦先生がCH完全版2巻の中で「職人」と言わしめる魅力や線の美しさを感じ取りました  原画展のために描き下ろした原画は幸せな気持ちになれます  
我去看了北条司展！ 我还没有真正接触过他的作品，所以我像欣赏新事物一样欣赏了他的所有作品  我感受到了《CH》全两卷中被井上雄彦先生称为"工匠"的线条的魅力和美感  为展览绘制的原画令我欣喜不已！  


https://twitter.com/hitoyuzuriha/status/1780602948378321110
オムライスの特典の大きさはA3サイズですーーー(」ﾟДﾟ)」  B4サイズのケースでは入りませんのでご注意ください！  
煎蛋饭的大小是A3大小。  请注意，它不适合 B4 尺寸的盒子！  


https://twitter.com/kao2784/status/1780599149832056840  
やっぱり手描きが好きだなぁ。会場内で観たムービーも感動しちゃった。生原画、、、( ᵒ̴̷͈ ⌑ ᵒ̴̶̷͈ )もう至福の時間だった✨とても快適に見られたし、カフェメニューもよかったし、また行こ。  
毕竟，我喜欢手绘。在会场看的电影也给我留下了深刻的印象。原画、、、 （ o̴̷͈ ⌑ o̴̶̷͈ ） 已经是一段幸福的时光了，✨看得很舒服，咖啡厅的菜单很好，我会再去的。  


https://twitter.com/ranto_boner0303/status/1780590098985435509  
北条司展、最高でした。  後半こそはcafeコーナーも行けたらいいな  
北条司展很棒。 我希望後半期还能参观cafe角  
https://pbs.twimg.com/media/GLXtQM-b0AA8e4k?format=jpg&name=large  
https://pbs.twimg.com/media/GLXtQM6boAAzP-9?format=jpg&name=medium  
https://pbs.twimg.com/media/GLXtQM-bwAAJrIG?format=jpg&name=large  
https://pbs.twimg.com/media/GLXtQM8acAAyfwy?format=jpg&name=4096x4096  


https://twitter.com/otom31/status/1780584604463960463  
今回の最大の目的は、 #GALLERYZENON の #北条司展 。大好きな #こもれ陽の下で… の展示も有り。ここから #ファミリーコンポ  辺りの画風が一番好き。光の表現とかは、感動ものでした    
这次展览的主要目的是在 #GALLERYZENON 举办的北条司展。 还有我最喜欢的阳光少女。 我最喜欢从这里到 #Family Compo 之间的绘画风格。 光的表达非常动人     
https://pbs.twimg.com/media/GLXoQcqagAE1mUX?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLXoQmuakAAXCTZ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLXoQtwawAAiQ5V?format=jpg&name=4096x4096  


https://twitter.com/jeanq0444/status/1780573793901220056  
さて #北条司展。美麗な原画の数々は眼福だったし何より先生が本展用に描き下ろされたというイラストにマジ泣いた✨ ソラゼノンもカフェゼノンもゼノン酒場もよく通った私的には吉祥寺を象徴する思い入れのあるこの場所をシティーハンターの世界を通して眺めることができた幸せに乾杯！  
嗯，#北条司展。 我经常去 Sola Zenon、Cafe Zenon 和 Zenon 酒吧，很高兴能通过《城市猎人》的世界看到这个象征着吉祥寺和我对吉祥寺的感情的地方！ 
https://pbs.twimg.com/media/GLXebZ2aIAAePxm?format=jpg&name=small  
https://pbs.twimg.com/media/GLXebZ0bMAAdhFE?format=jpg&name=4096x4096  


https://twitter.com/inuinuinu357/status/1780573203632640366  
美しい原画の数々！描き下ろしも展示されていました   
许多精美的原创图画！ 还展出了新的绘画作品  
https://pbs.twimg.com/media/GLXd4-vboAAdtiA?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLXd5Eda0AAU9J6?format=jpg&name=4096x4096  


https://twitter.com/chibi_aya_728/status/1780567928506868024  
夜通ったらとっても素敵な雰囲気でした✨  #ギャラリーゼノン のオープンと #北条司展 の初日、おめでとうございます🎉✨  北条先生の素晴らしい原画をたくさん観られてすごく幸せです！    
晚上经过这里真好 ✨ 祝贺 #Gallery Zenon 开幕和#北条司展的第一天 🎉✨ 我很高兴看到这么多北条的精彩原创作品！     
https://pbs.twimg.com/media/GLXZFnsbsAA_h0L?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLXZFoEbIAA2gHb?format=jpg&name=900x900  


https://twitter.com/apapane5688/status/1780547610664915405  
\#北条司展 を観に行った。2階には原画や原稿だけじゃなく大きなフィギュアもあったし、何よりメッセージを書けるスペースが嬉しかった。当時部屋に貼ってたキャッツアイのポスター原画が観れたのが最高に幸せ！懐かしい…。次はカフェも行くぞ！　#シティーハンター　#キャッツアイ　#ゼノン  
我去看了#北条司展展览，在二楼不仅有原画和手稿，还有大型手办，最重要的是，我很高兴看到了一个可以写留言的空间。 我很高兴看到我当时贴在房间里的《猫眼》原版海报！ 它勾起了我的回忆... 下次我也要去咖啡馆！　
https://pbs.twimg.com/media/GLXGnFYbgAAD8ob?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLXGmhnasAAZiDh?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLXGnSXbEAA-5X7?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLXGnZAaIAAGPrD?format=jpg&name=900x900  


https://twitter.com/MoonManiac666/status/1780546911378022735  
北条司展が開催されると知り、慌ててチケットを購入し初日に滑り込めた。原画を見れる機会はそうないので一枚一枚を目に焼き付け、いざ売店に立ち寄ったら‥は、版画が売ってる？！悩みに悩んだ結果、写真4右上を購入。北条先生の作品を手に入れる事が夢だったので嬉しすぎる😭  
得知要举办北条司展，我赶紧买了票，第一天就溜了进去。 由于没有机会看到原画，我把每一幅画都看在眼里！ 考虑再三，我买下了照片4 右上方的那幅。 我太高兴了，因为拥有一幅北条老师的作品是我的梦想😭   
https://pbs.twimg.com/media/GLXF9bvaEAAVGeH?format=jpg&name=small  
https://pbs.twimg.com/media/GLXF9cUbQAAjQfq?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLXF9cUagAEl5jl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLXF9cSbEAAxJRb?format=jpg&name=4096x4096  


https://twitter.com/sakananosurimin/status/1780522842255470674  
17時からのチケットについて  
入場と共にカフェ利用を聞かれて、「1730ラストオーダーだから、即カフェ列に並んでください。時間かかるので、1800閉館だから見れない可能性もあります。」とのこと  
私の場合、1710オーダー、1720パンケーキとコーヒー、急いで食べて1730くらいでした。  
关于 17:00 起的门票  
入馆时会问您是否要使用咖啡厅，我被告知："最后点餐时间是17:30，请立即到咖啡厅排队。因为博物馆 18:00 关门，所以您可能要花费一些时间才能看到它"。
就我而言，大约是17点10分点餐，17点20分吃煎饼和咖啡，17点30分快速吃完。   


https://twitter.com/sakananosurimin/status/1780522664014262350  
北条先生ブレンドコーヒー☕️飲んだ〜  苦味がなくて、飲みやすい。ごくごく飲めるタイプのコーヒー。香ちゃんパンケーキとの組み合わせが抜群だった😇  カードにno.1表記あると言うことは、ここで今後いろんな漫画家さんのブレンドのめるんだよね。楽しみだなぁ  
北条老师的混合咖啡☕️喝起来--没有苦味，容易入口。 非常好喝的一种咖啡。 与 香chan煎饼的搭配非常出色😇 卡片上有 No.1，那就意味着今后我们可以在这里喝到各种漫画家的混合咖啡了，对吗？ 我很期待。  
https://pbs.twimg.com/media/GLWv6Oba8AAY-O5?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLWv6ObbIAAP78B?format=jpg&name=small  
https://pbs.twimg.com/media/GLWv6OVacAAfI0j?format=jpg&name=4096x4096  


https://twitter.com/sherryzawander4/status/1780521986965590173  
北条司展 行ってきました！！原画が！動画か！！何もかもがスペシャルでした😆✨大好きなあの扉絵やあの作品の原画。幸せな空間でした。1度じゃ足りない😂また伺います😊入口の左上にあるこのCZの看板、先によく見てから店内へ、がオススメです😊  
我参加的北条司展！ 原画！ 视频！ 一切都很特别😆✨我喜欢的那幅扉絵的作品的原画。这是一个幸福的空间，一次不够😂我还会再来的😊建议大家进店前先看看入口左上角的这个CZ标志😊。   
https://pbs.twimg.com/media/GLWvTJAacAASw0a?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLWvTI-aEAAwIXH?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLWvTJEakAAHbvS?format=jpg&name=900x900  


https://twitter.com/xyz_mizu/status/1780520851982672350  
今日行きました✨  帰るのが惜しくて4時間くらいいました🤣  近くで見ると先生の絵の素晴らしさがまざまざとわかって、とにかくスゴいの一言。  ギャラリーオープン記念の先生の描きおろしまであるなんて✨キャー😍⤴️  サイン入りポスター欲しいです🥺  当たりますように  
我今天去了  我在那里呆了大约 4 个小时，因为我不想离开🤣  近距离欣赏他的画作，真是令人惊叹。 真不敢相信，为了庆祝画廊开幕，老师还画了一幅新画✨Ca 😍⤴️  我想要一张签名海报🥺  希望我能中奖！  
https://pbs.twimg.com/media/GLWuRrQb0AAhGct?format=jpg&name=medium  


https://twitter.com/nishiki135/status/1780520842193215775  
前期初日にお邪魔してきました！展示からカフェからグッズまで満喫できて楽しかったです！後期も初日に行きます！！！   
我们是在前期的第一天参观的！ 从展览到咖啡厅，再到商品，我都玩得很尽兴！後期的第一天我也会去的！  
https://pbs.twimg.com/media/GLWuQzoaYAAurN0?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLWuQzoaMAAmhae?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLWuQzoa8AApMU2?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLWuQzkbgAACF6p?format=jpg&name=900x900  


https://twitter.com/sakananosurimin/status/1780520495659860136  
初日に行ってきたよ〜！時間まで、吉祥寺のカフェでずっと仕事してた😂  やっぱ原画みれるのはいいね...綺麗でした。香ちゃんパンケーキは、金平糖込みの甘さ調節でふわふわでした。(ボリュームあったので夕飯とした)  トイレの下に冴羽商事チラシは笑った😂    
我是第一天去的！ 我在吉祥寺的一家咖啡馆工作，直到时间到了才回去😂  能看到原画真是太棒了... 太美了 香san煎饼松软，甜度适中，还加入了金平糖。 (厕所下面的冴羽商事传单逗笑了我😂。   


https://twitter.com/outa_vision0123/status/1780516366581379508  
行ってきた！ずっとトリハダ立ってた  
去了！ 我已经站了一整天了    

https://twitter.com/Danwei_pt/status/1780510495893778604  
今日のおでけけは吉祥寺でした！GALLERY ZENONさんで北条司展を拝見してきました✨  展示自体はゆっくり拝見できて嬉しかったです✨  物販とカフェは、そこばかり人が溜まっちゃって動線を考えないとなかなか大変な感じ😂  ネトフリでドラマも始まるしこれからも楽しみですね  
今天的 Odekeke 是在吉祥寺，我们参观了在 GALLERY ZENON✨举办的北条司展  展览本身就很让人开心✨，销售区和咖啡厅人山人海，所以必须考虑人流路线😂Netflix 上有一部电视剧开播了，所以我很期待看到更多的内容。   


https://twitter.com/kichinavikun/status/1780500837497987539  
吉祥寺に「GALLERY ZENON（ギャラリーゼノン）」さんが本日オープンしたので行ってきました🎉現在は北条司展が開催中。シティーハンターやキャッツアイなどの原画が観られたり、コラボメニューもいただけます✨グッズもあったりファンにはたまらない内容でした  
GALLERY ZENON 今天在吉祥寺开业，我去看了🎉北条司展。 您可以看到《城市猎人》、《猫眼》等作品的原画，还可以品尝到合作菜单✨，还有商品，粉丝们一定会忍不住的！   


https://twitter.com/oTx923Acdr0iUEq/status/1780499355998527715  
北条司展　行きたい✨✨  この先生の描く女性が綺麗💕  描き込みがスゴい原画を観たい  
我想去看北条司展✨✨  他画的女人很美💕  我想看看画得如此精美的原画   


https://twitter.com/hamavha/status/1780497631078478225  
吉祥寺のアトレの入口近くに北条司展のサイネージを見つけたので、記念に📸   
我在吉祥寺 Atre 的入口处发现了北条司展的标牌，以纪念这一事件 📸。    
https://pbs.twimg.com/media/GLWZJEfbkAAGR8V?format=jpg&name=900x900  


https://twitter.com/hitoyuzuriha/status/1780493150341181505  
行ってきました！全て美麗でした✨  今日の様子ですが混み具合によっての時間制なので、基本的にはずっといられます。  カフェは店員さんが席に案内するのでまずは並びましょう。パンケーキは大きめなのでフードファイトに近いです💦   ARちょっとやってみた  
我去了那里！ 一切都很美✨。  这是今天的样子，但它是定时的，取决于人多人少，所以你基本上可以一直呆在那里。 咖啡馆里有工作人员，他们会带你到你的座位，所以先排队吧。 煎饼很大，所以几乎就像一场食物大战💦 AR 我尝试了一下 AR。   


https://twitter.com/taka2380038/status/1780492818559254882   
ひとり参加なのでもっこりさせるの厳しかった…(｡•́ωก̀｡)…ｸﾞｽ  でも、おいしかった🎵 #シティーハンター #北条司展 #ギャラリーゼノン #冴羽獠 #吉祥寺  
因为是一个人参加，所以让人很难受…(。•́ωก̀。)……グス  不过,很好吃🎵  

https://twitter.com/truewave0310/status/1780492230324785478  
北条司展の開催おめでとうございます！待ちに待った本日、早速拝見しました。素敵な絵の数々に囲まれ幸せな空間でした🥰また、同じようにおひとりで来られてた方と意気投合し、一緒にCITYHUNTERについて語りながらカフェでご飯もでき楽しい時間でした。ありがとうございました  
祝贺北条司展开幕！ 我们期待已久，今天终于一睹为快。 我还和一个独自前来的人打了个招呼，我们在咖啡馆一起谈论了《CITYHUNTER》并共进晚餐，度过了一段愉快的时光。 非常感谢你们！ 


https://twitter.com/koko5075731/status/1780483572283195714  
北条先生の原画をじっくり見てきた。原画ってほんと生命を感じよね。とっても幸せな空間だったなぁ。原画展、たくさんの人に見てもらいたいからもっと色々な場所でやってくれたらいいのに。  
我仔细看过北条老师的原画。 原画真的很有生命力，不是吗？ 这是一个非常快乐的空间。 我希望他们能在更多的地方举办原画展览，因为我想让更多的人看到它们。   


https://twitter.com/fum91645774/status/1780480566305100089  
入場人数決まってるし、そんなに混む事もなく観やすかった。  
参观者人数固定，不那么拥挤，很容易观看。   
https://pbs.twimg.com/media/GLWJocKaoAAn9Ni?format=jpg&name=medium  


https://twitter.com/jeanq0444/status/1780480093439344714  
ギャラリーゼノンにて #北条司展 を堪能。初日でガチファンいっぱいw  
正在 Zenon Gallery欣赏 #北条司展。 今天是展览的第一天，到处都是宅男粉丝。   
https://pbs.twimg.com/media/GLWJMunaMAA5Yz3?format=jpg&name=4096x4096  



https://twitter.com/teru_607/status/1780473410218570050  
紀香さまからのお花もコメント最後の❤も素敵ですね。  
来自纪香的花和评语最后的❤也很棒呢。  （译注：因为藤原纪香前段时间出演猫眼相关的舞台剧。）  
https://pbs.twimg.com/media/GLWDH6jbIAAfJqJ?format=jpg&name=medium  


https://twitter.com/teru_607/status/1780470468098830348  
リニューアルオープンそして北条司展おめでとうございます💐ほとんどの展示物が撮影OK！太っ腹！それほど広いスペースでもないのにゆとりを感じたのは天井の高さ？内装の配色？推しのオードパルファム買って満足。また来週伺います  
祝贺重新开张和北条司展💐 大部分展品都可以拍照！ 空间不是很大，但感觉很宽敞。 室内的色彩搭配？ 我买了自己喜欢的淡香水，很满意。 下周我还会再来参观。   
https://pbs.twimg.com/media/GLWAci5bQAAfTE3?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLWAcpIbQAABbNW?format=jpg&name=4096x4096  


https://twitter.com/qafandch/status/1780466061873422673  
解説文にすごくうれしいニュースが書いてあった～‼️😍😭カフェの北条司ブレンドと水出しアイスコーヒーがすごく美味しい。是非食事のお供に飲んでほしい。ブラック派にはたまらない美味しさ。オムライス注文のかたは、A3ｻｲｽﾞを持ち帰れるように準備した方がよいです  
解说文字中有一个非常好的消息--‼️😍😭咖啡馆的北条司混合咖啡和水驱冰咖啡非常好喝。 我强烈推荐您在用餐时品尝。 对于喝黑咖啡的人来说，它非常美味。 如果您点了煎蛋，最好准备好把 A3 大小的带回家！   
https://pbs.twimg.com/media/GLV8cWba4AA76cY?format=jpg&name=900x900


https://twitter.com/kinakonose/status/1780460284198506660  
週末に行きます！皆さんのポスト見てソワソワする💛行くのを楽しみにして日常をがんばる！  
我周末要去！ 我很喜欢你的帖子💛 期待着去那里，在日常生活中尽我所能！  



https://twitter.com/deshi40673303/status/1780459952731029681  
これめっちゃ欲しい🔥🔥家族みんな好きで漫画全部あるけど、直筆サインとか無いからめちゃくちゃ欲しい！！！（実家に保管、家族で見てる！）当選したら実家にサプライズで持って行って貼る！  
我太想要这个了🔥🔥🔥 我太想要它了，因为我的家人都很喜欢它，有所有的漫画，但没有签名或任何东西！ (存放在我父母家，家人都看着呢！）。 如果我中奖了，我会把它带到我父母家，作为一个惊喜，并把它挂起来！   


https://twitter.com/chibi_aya_728/status/1780457893743013997  
北条先生の原画展最高でした…✨生原画を見た瞬間涙出た😭線や色、インクの厚み、筆の跡、表情とか、言葉が追いつかないほど美しい…各作品への気持ちも溢れてとにかく情緒が揺さぶられるし、何回も観に行きたい！まだ心臓ドキドキして痛い。呆然としてる…    
北条先生原画展是最棒的......✨看到现场原画的那一刻，我哭了😭线条、色彩、墨色的浓淡、笔触、表情，美得无法用语言来表达......对每一幅作品的感情都溢于言表，我简直被感动得想回去再看一遍又一遍！ 我的心还在怦怦直跳，隐隐作痛。 我惊呆了      


https://twitter.com/Mue0007/status/1780457413075767312  
オープンおめでとうございます！！スケジュール調整して、何とか行きたいです！！  
祝贺开幕！ 我希望能调整一下日程安排，设法去看看！  


https://twitter.com/yuzuki532/status/1780453620648927528  
神谷さんのお花もあった  
还有神谷先生的鲜花   
https://pbs.twimg.com/media/GLVxHw4akAA9Haz?format=jpg&name=4096x4096  


https://twitter.com/yuzuki532/status/1780447671586861080  
これから行く人は、オムライス頼むと貰えるこの青いボードがA3サイズはあるので、大きめのバッグ必須です！  
如果你要去，你需要一个大袋子，因为点一份煎蛋卷时得到的这块蓝色board是 A3 大小的！   


https://twitter.com/xyz_mizu/status/1780446164590166213  
入場待ちナウなんですが、原画展、物販、カフェあわせてなるべく90分でって言われたのだけど絶対無理な自信しかない。  
我现在正在等待入场，但有人告诉我，原展览、销售和咖啡馆加起来应该需要 90 分钟，但我只相信这绝对不可能。  


https://twitter.com/utulove/status/1780440416804016230  
北条司展 を見てから #TMNETWORK のライブに行くFANKS何人もおるんやろうな〜😁  
不知道有多少粉丝会在看完北条司展后去看 #TMNETWORK 演唱会😁。   
https://pbs.twimg.com/media/GLRcjNwaAAAiGTf?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLRck5QaUAAsIZu?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GLRcqGObMAAQxZz?format=jpg&name=900x900  
https://pbs.twimg.com/media/GLRg8aoaUAAzaQc?format=jpg&name=4096x4096  


https://twitter.com/takakishida1228/status/1780418697666138605  
昨日は「北条司展」の内覧会へ。原画が沢山！素晴らしかったです。北条先生、個展御目出度う御座います。そして、GALLERY ZENONオープン御目出度う御座います。  
昨天，我去参加了「北条司展」的预展。 有很多原画！ 太精彩了 祝贺你的个展，北条老师。 也祝贺泽农画廊开业。 
https://pbs.twimg.com/media/GLVRXKlaUAAs2xh?format=jpg&name=900x900  






 



-----  

<!--  
以下两种方式可获得最大分辨率的图片：  
https://pbs.twimg.com/media/GLrD7CvacAAQgiW?format=jpg&name=large  
https://pbs.twimg.com/media/GLrD7CvacAAQgiW.jpg?name=large  
name=4096x4096/large/medium/small/tiny  
https://twitter.com/search?q=(%23北条司展)%20until%3A2024-04-19%20since%3A2024-04-18&src=typed_query&f=live  
-->  