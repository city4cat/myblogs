sources:  
    - [北条司展 - Twitter](https://twitter.com/hashtag/北条司展?f=live)  
    - [北条司原画展 - Twitter](https://x.com/hashtag/北条司原画展?f=live)  
    - [北条司展 - Facebook](https://www.facebook.com/hashtag/北条司展)  
    - [北条司展 - Instagram](https://www.instagram.com/explore/tags/北条司展)  
    - [北条司展【公式】@Twitter](https://x.com/hojotsukasaten)  
    - [六本松蔦屋書店 - Twitter](https://x.com/hashtag/六本松蔦屋書店?f=live)  


https://www.instagram.com/p/DBd2-NVzGtr/  
初日に行ってきました。北条司展 IN 福岡✨ 動画撮影はNGですが、写真撮影はOKということでたくさん撮ってきたうちの一部を😃  pic 14  前売券購入特典3種類のうち自分で好きな絵柄が選べて、コミックの22巻表紙のものにしました。入場者特典は袋に入っているけど、自分でチョイスできて、まさかの被るという（笑）    
我第一天去了。北条司展IN福冈✨视频摄影是NG，照片摄影是OK，所以拍了很多照片😃pic 14预购券购入特典3种中可以选择自己喜欢的图案，漫画第22卷封面的图案。入场者特典装在袋子里，但可以自己选择，没想到会戴（笑）  
```  
https://scontent.cdninstagram.com/v/t51.29350-15/464263417_1296064014884678_1899232942009007124_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=102&_nc_ohc=PlMoXOFLf5AQ7kNvgHQ9yCJ&_nc_gid=e429b55fec704960a207eb04edf8369d&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NTE4MzQzODM1NjM4NTk4Mg%3D%3D.3-ccb7-5&oh=00_AYAIKFnw0c4vBIA3yrwsHK65niwzrPPYT-lYRLKvxByqFA&oe=6727A963&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/464207413_918403753499789_6460485170279673723_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=105&_nc_ohc=uP3tDDSidfQQ7kNvgG8DU4q&_nc_gid=e429b55fec704960a207eb04edf8369d&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NTE4MzQzODM3MzAwMTU4Ng%3D%3D.3-ccb7-5&oh=00_AYAE6KfLQMFgkrUusdsAxudELgEjADySz3FHpLhkIoz1Aw&oe=67277ECD&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/464467663_1075637897429414_3201220378018542223_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=102&_nc_ohc=qNBwaUmBoucQ7kNvgEKsOOa&_nc_gid=e429b55fec704960a207eb04edf8369d&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NTE4MzQzODQ5MDU1ODE3Ng%3D%3D.3-ccb7-5&oh=00_AYCbPzgkkLFDLuhm7g34N3vF7Fl75-3w5xnmH4eKQT_9dQ&oe=6727A7A6&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/464097574_1275950713824980_4038372738739743844_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=108&_nc_ohc=LOtonrRlJ5AQ7kNvgF0D8kM&_nc_gid=e429b55fec704960a207eb04edf8369d&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NTE4MzQzODM3MzEwNDcwMg%3D%3D.3-ccb7-5&oh=00_AYA3AZdjZmFfMDKYZNN93TMWQHvUA-McKGa_2jqoJT1jcQ&oe=6727A64D&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/464366268_1579127409348379_5174869126860837021_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=107&_nc_ohc=y6x7sNplGukQ7kNvgERqLmx&_nc_gid=e429b55fec704960a207eb04edf8369d&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NTE4MzQzODQ5MDY1ODcyOA%3D%3D.3-ccb7-5&oh=00_AYBRTuPpVAhCOKV8Yxmcq5FibpI-ikY751q72QbrdpJRLQ&oe=6727850A&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/464297093_1635608097032791_8995710237280565737_n.jpg?stp=dst-jpg_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=107&_nc_ohc=t77cbbG-jFAQ7kNvgF1nyzA&_nc_gid=e429b55fec704960a207eb04edf8369d&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NTE4MzQzODQ5OTAzOTUwOA%3D%3D.3-ccb7-5&oh=00_AYBVT2azA8dkITulcQkuhbqyVYBXP23NULBouhRt4d5Qwg&oe=6727A3B6&_nc_sid=10d13b  
```  

https://www.instagram.com/p/DBlCh2Vv9cY  
「北条司展IN福岡」  
```   
https://scontent.cdninstagram.com/v/t51.29350-15/464642101_1921789584954005_2841992451560090633_n.jpg?se=-1&stp=dst-jpegr_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMjE1eDE1MTguaGRyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=105&_nc_ohc=iHbMaDlv19kQ7kNvgEkTAQ0&_nc_gid=488452a054ca48f18b3e688e1c60338e&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NzIwNDU5NTcyNDAwODMzOA%3D%3D.3-ccb7-5&oh=00_AYDSXRJxjiDKXBX7G_4k8Flud5hmM1VJCY9dSGnxdb4Hcw&oe=67278BFB&_nc_sid=10d13b  
https://scontent.cdninstagram.com/v/t51.29350-15/464675494_954008060100760_7738149828028631224_n.jpg?se=-1&stp=dst-jpegr_e35&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMjE1eDE1MTguaGRyLmYyOTM1MC5kZWZhdWx0X2ltYWdlIn0&_nc_ht=scontent.cdninstagram.com&_nc_cat=104&_nc_ohc=UqCunpXhJ6EQ7kNvgE5Nps8&_nc_gid=488452a054ca48f18b3e688e1c60338e&edm=APs17CUBAAAA&ccb=7-5&ig_cache_key=MzQ4NzIwNDU5NTc5MTI5MDcwNA%3D%3D.3-ccb7-5&oh=00_AYCOL-8JAlRgFPnJQtLbQ0_BuePNx2MAII4RnGLNGg_5CA&oe=67277994&_nc_sid=10d13b  
```   


https://x.com/tarou6748/status/1849956257035366770  
行ってきました～！私の心撃たれたこの1枚が見られるとは…💥🔫ありがとうありがとう  写真撮影OK太っ腹ありがとう( ´•̥̥̥ω•̥̥̥`)  
去了~ ！能看到这张击中我心灵的照片…💥🔫谢谢谢谢  拍照OK  太っ腹ありがとう( ´•̥̥̥ω•̥̥̥`)  
https://pbs.twimg.com/media/GaxdaWnakAAKhuk?format=jpg&name=4096x4096  


https://x.com/hB2I6h2vw266059/status/1850787691090378813  
北条司展IN福岡、終わってしまいましたね😣  皆様の感想、お写真とても楽しかったです！素敵な空間の様子がわかりました。ありがとうございました☺  そして公式様の投稿も楽しく見させていただきました。お疲れさまでした。また次どこかで開催されることを願っています！！✨✨✨  
北条司展IN福冈，结束了😣大家的感想、照片非常快乐！我看到了一个漂亮的空间。谢谢☺并且官方的投稿也让我愉快地看了。您辛苦了。希望下次还能在什么地方举办！✨✨✨  


https://x.com/yukifukuoka/status/1850732754482438272  
やっと北条司展に伺えました！吉祥寺に行きたかったものが見れて嬉しかった〜！福岡開催ありがとうございます😊♥️🥰　35年以上前に、学校の文化祭で描いた絵も添付してみる😆😆😆#北条司展 #北条司 #シティーハンター #キャッツアイ #六本松蔦屋書店 #原画展 #北条司展IN福岡  
终于可以去看北条司展了！看到想去吉祥寺的东西很高兴~ ！福冈举办谢谢😊♥️🥰把35年前在学校文化祭上画的画也添加进去😆😆😆#北条司展#北条司#城市猎人#猫眼#六本松茑屋书店#原画展#北条司展IN福冈  


https://x.com/BardOkame2024_2/status/1850542069686923436  
詳しい原画展の詳細レポ、毎回、有難うございました！🙂‍↕️  お疲れ様でした🙇  関西での巡回展を期待しつつ😆その時までの参考にさせて頂きますね！😊  本当にお疲れ様＆有難うございました！😊  
详细的原画展的详细repo，每次，谢谢！🙂‍↕️辛苦了🙇一边期待关西的巡回展😆一边让我作为那个时候的参考吧！😊真的辛苦了&谢谢！😊  


https://x.com/mayoimymy0117/status/1850531250395898203  
条司展行ってきた🥲︎❣️  ほんとに素敵すぎて長らく  絵に見惚れてましたᐡ៸៸>  ̫ <៸៸ᐡ❤︎  沢山の人に愛されてるなぁーと改めて実感  先生にメッセージを  届けてもらえるということで  しっかり書いてきましたᯓ★  とりあえずとっても疲れた😲💦   グッズは あとからあげれたら💦  
去了条司展🥲︎❣️  真的太美了，太久了  看画入迷了ᐡ៸៸>̫<៸៸ᐡ❤︎  被很多人爱着啊  再次感受  给老师留言  可以送过来  写得很好ᯓ★  总之很累😲💦  周边等一下再送💦  
https://pbs.twimg.com/media/Ga5oXUdaoAA_oGX?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga5oXVhbYAAVdvH?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga5oXXhaIAEZjAK?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga5oXbtaMAAis6F?format=jpg&name=4096x4096  


https://x.com/Ryo_CHisa_ryo/status/1850490678301901182  
今回の原画展で好きだった原画  やっぱり素敵✨
这次原画展中喜欢的原画  果然很漂亮✨  
https://pbs.twimg.com/media/Ga5Dc-na0AEPfFy?format=jpg&name=4096x4096    
https://pbs.twimg.com/media/Ga5Dc-zaUAAacpm?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga5Dc-na8AA_TYZ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga5Dc-oaoAA0fiH?format=jpg&name=4096x4096  


https://x.com/Ryo_CHisa_ryo/status/1850489572217749775  
最終日……今日で終わりとか悲しい😭  でも、開催してくださったことに心より感謝(ㅅ´꒳` )  あ、ちなみに開催期間中にトンボとカラスが増えたそうですw  
最后一天……今天就结束了，很难过😭但是，衷心感谢大家的举办（ㅅ´꒳`）啊，顺便说一下，听说举办期间蜻蜓和乌鸦增加了w  
https://pbs.twimg.com/media/Ga5Cc4ybEAANb2y?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga5Cc4wboAAvncE?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga5Cc4vakAAOcG_?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga5Cc41aoAAh2Ja?format=jpg&name=4096x4096  


https://x.com/hinnahinna8/status/1850463737012015253  
本日最終日！ #北条司展 見納めに行ってきました！この2週間...福岡に先生の美麗原画が滞在されて、多くの方に見ていただいて、私もたくさん堪能できて、本当に感無量です✨北条先生！スタッフの皆様！ありがとうございました！！！4回見に行って特製カード綺麗にコンプできました😂最香🫶💕  
今天最后一天！去看了#北条司展！这两个星期…在福冈逗留了老师的美丽原画，很多的人看了，我也享受了很多，真的感慨无限✨北条老师！各位工作人员！谢谢大家！ ！ ！去看了四次特制卡片很漂亮😂最香🫶💕
https://pbs.twimg.com/media/Ga4q9Z8aQAEnQFD?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga4q9Z6bIAAMu7x?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga4q9Z8boAAbgjS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga4q9Z6bsAAA4De?format=jpg&name=4096x4096  


https://x.com/096k_Ue/status/1850441687988064435  
北条司展 行ってきましたー！！写真撮影OKだし、グッズも可愛いの多いし、何より原画のクオリティーが高くて行ってよかった！贔屓目なしに本当におすすめ！！！！それぞれの作品への北条先生のコメントも面白くて、楽しい時間だったわ☺️  
去了北条司展—！！拍照OK，周边也有很多可爱的东西，最重要的是原画的质量很高，去了真好！不偏不倚地真心推荐！！！！北条老师对每个作品的评论都很有趣，度过了快乐的时光☺️  
https://pbs.twimg.com/media/Ga4W4BpaMAAk8Ro?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga4W4BnaMAAOiQt?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga4W4BnaIAAUWJ0?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Ga4W4BpbMAAgBo_?format=jpg&name=4096x4096  


https://x.com/fanks_marine/status/1850341898860552655  
昨日の原画展でもらった来場者記念のポスター🥰以前ヨンマルでTM新聞が発行された時に保管するため買ったファイルがまさかの役に立ちました😇綺麗に保管できる🙌  
昨天在原画展上收到的来场者纪念海报🥰以前在yeon丸发行TM报纸的时候为了保管而买的文件没想到派上了用场😇可以保管得很漂亮🙌  
https://pbs.twimg.com/media/Ga28JYNbMAAnHEk?format=jpg&name=large  


https://x.com/ryosaeba357xyz1/status/1850295693912809517  
ＸＹＺ・・・ #北条司展 にて待つ  今日27日最終日❗️❗️  　　　　　　７時まで❗️❗️   福岡県福岡市 #六本松蔦屋書店 にて  写真はキャッツ❤アイ第一話で舞台の美術館のモデルとなった北九州市立美術館本館です
全国のSTAFFの皆様、今日27日、小倉の壁画近くで誕生日を迎えました  福岡の北条司展最終日が自身の誕生日で北条先生の地元  （北条先生の地元は小倉、原画展の開催は六本松）  でもあるので、福岡に来ました、壁画の前で誕生日を過ごし感無量です  これからも応援し続けますね  ＸＹＺ・・・北条先生の幸せな未来を待つ  ...  
https://pbs.twimg.com/media/Ga2SDIwaMAADTVM?format=jpg&name=large  


https://x.com/rumuu_miiro/status/1850072795918762010  
結構子供さん連れた方もいらしてました！明日最終日まで、無事の開催をお祈りしております！  
带孩子来的人也不少！到明天最后一天，祈祷顺利召开！  


https://x.com/azataku0102/status/1850051170339029204  
10/25北条司展in福岡に  行ってきました  連載読んでた頃の記憶が甦り  懐かしい気分で満喫しました
10/25去了福冈北条司展读连载的时候的记忆复苏  以怀念的心情充分享受了  
https://pbs.twimg.com/media/Gayzt0caAAEY0Li?format=jpg&name=small  
https://pbs.twimg.com/media/Gayzt0caAAEY0Li?format=jpg&name=small  
https://pbs.twimg.com/media/Gayzt0gaAAMg4B7?format=jpg&name=900x900  
https://pbs.twimg.com/media/Gayzt0eaAAMR17h?format=jpg&name=900x900  


https://x.com/tetsu28tfx/status/1850032425776165150  
ほんと、ライブの予定と重なったおかげで来れた。やばい。なにこの原稿の美しさ。先生、修正なさすぎてこえぇです。諸々あるから1時間で離脱したけど…（いやお前下手すると2時間とか普通に居りかねんだろ）トンボ最高  
真的，多亏和演唱会的预定重叠了才来的。糟了。这原稿的美。老师，您太不修正了，声音太大了。因为有很多事情，一个小时就离开了…（搞不好一般都待不了2个小时吧）蜻蜓最高  
https://pbs.twimg.com/media/GayirkOa4AA7EQf?format=jpg&name=900x900  
https://pbs.twimg.com/media/GayirkOaAAQkxNu?format=jpg&name=small  
https://pbs.twimg.com/media/GayirkQbMAA65jl?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GayirkPboAAqhkd?format=jpg&name=4096x4096  


https://x.com/chiroru_special/status/1850022197433446483  
北条司展 福岡ツアー振り返り  ちょっと振り返ってみたけど、やっぱりすごくいい！！吉祥寺と違ってこれもいい🥹  獠ちゃんが当日券売場横の壁に、反対側の横の壁にはGALLERY ZENONにたたずむふたりの姿が見える！なんという最香のお出迎え❤️シティーハンターコーナーの先陣は大好きな絵がいっぱい🥰  
北条司展 福冈巡回顾  稍微回头看了一下，果然非常好！ ！和吉祥寺不同这个也不错🥹  当天在售票处旁边的墙壁上，和在对面的墙壁上看到两个人伫立在GALLERY ZENON里的身影！多么香的迎接❤️城市猎人角的前头有很多喜欢的画🥰  
https://pbs.twimg.com/media/GayZYdlbAAA4qaI?format=jpg&name=900x900  
https://pbs.twimg.com/media/GayZYfrbcAAuinx?format=jpg&name=small  
https://pbs.twimg.com/media/GayZYgWaAAINb93?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GayZYmlbsAAuTns?format=jpg&name=4096x4096  


https://x.com/miki_sts/status/1850002593357160470  
『FANKSのみなさん、ライブ前にぜひ北条司展にお立ち寄りください‼️』  
“FANKS的各位，在演出前请一定去北条司展看看‼️”  
https://pbs.twimg.com/media/GaxzJdybgAABbjq?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaxzJbUboAEFdL6?format=jpg&name=4096x4096  


https://x.com/fanks_marine/status/1849994716215116072  
原画展、ゼノンカフェ以来2回目🥰  カラー絵は近くで見れば見るほど色使いもすごく繊細で細かくて  
原稿もコミックを何十回読んだかわからないけど、それでも実際の原稿を目にするとずっと見ていられる・・・🥰来れてよかった💖  
原画展，zenon cafe以来第2次🥰彩色画越靠近看颜色的使用也非常纤细细致  
原稿也不知道看了几十遍漫画，不过，看到实际的原稿就能一直看下去···🥰能来真是太好了💖  


https://x.com/oda08118004/status/1849740153608196192  
キャッツアイ の放送日に #北条司展 投稿190  
今回は「ネコまんまおかわり♡」   
頭上から落ちてきた猫を助けた貧乏写真家の主人公の元に翌日飼い主と名乗る美久がお礼に現れる  
料理がネコまんま、夜12時に必ず帰る美久だが2人にとって幸せの日々が続くが4枚目、何やら様子が…気になる方は読んでね  
猫眼 之放送日 #北条司展投稿190  这次是“再来一碗猫饭♡”  帮助了从头上掉下来的猫的贫穷摄影师的主人公，第二天自称饲主的美久出现在他面前表示感谢  料理是猫饭，晚上12点一定会回家的美久，对两人来说幸福的日子还在继续，但第4张，什么样子…感兴趣的人可以阅读哦  
https://pbs.twimg.com/media/GauY3PWaAAAIwsx?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GauY3PYa0AAWXsI?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GauY3PYbUAAbQY9?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GauY3PVaEAAC6Li?format=jpg&name=4096x4096  


https://x.com/zA69gM3gleFD1s8/status/1849604370712895902  
北条司展 IN 福岡  22日弾丸で朝夕拝見😌  久々に北条先生の美麗すぎる肉筆原画、ほんとに目の保養😌  何度観てもたまらんのです😁  欲しすぎた複製原画はじめグッズも入手できて最香✨  スタッフさんも優しい方ばかり、獠ちゃんパネルとも再会👍  会場の雰囲気もとてもお洒落✨  伺えてホントによかった😌
北条司展IN福冈  22日子弹朝夕见😌  好久没看到北条老师如此美丽的亲笔原画了，真是养眼😌  不管看多少遍都看不下去😁  想要的复制原画和周边也能入手最香✨
工作人员也很和善，和獠面板也重逢👍  会场的气氛也很时尚✨  能来真是太好了😌  
https://pbs.twimg.com/media/GasdXZ-aAAAqPQB?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GasdXcdaAAE1MWN?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GasdXdOboAAYgwr?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GasdXiKbIAAq-TZ?format=jpg&name=4096x4096  


https://x.com/TonTonToRiDe/status/1849491598385742169  
北条司展 IN福岡に行ってきた！なんてたって私が初めて読んだ漫画、シティーハンターですからね✌️美しい原画に惚れ惚れ…  
北条司展IN福冈去了！因为我第一次看的漫画是城市猎人✌️被美丽的原画迷住了…  
https://pbs.twimg.com/media/Gaq2yttaAAIo_Ym?format=jpg&name=small  
https://pbs.twimg.com/media/Gaq2zM7aYAAamuF?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Gaq2zkEaAAMbKLT?format=jpg&name=4096x4096  


https://x.com/hmtrivium87/status/1849450348253184158  
「北条司展in福岡」行ってきました  じっくり堪能できて幸せでした💕  原作読みたくなった😆  
去了“北条司展in福冈”能好好享受很幸福💕想看原作😆   
https://pbs.twimg.com/media/GaqRSdha4AAkOO4?format=jpg&name=small  
https://pbs.twimg.com/media/GaqRSdebMAA4kzm?format=jpg&name=small  


https://x.com/machagon0322/status/1849406030146629640
「来年40周年だから動きがあるとしたらシティーハンターだろう」とお話して下さったスタッフさん、ありがとうございます🙇  また楽しく生きる活力湧きました  頑張る力をありがとうございます。  
“明年就是40周年了，如果有什么动作的话，应该是城市猎人吧。”这么说的工作人员，谢谢🙇  快乐生活的活力再次涌现  谢谢你努力的力量。

https://x.com/machagon0322/status/1849405317307605352  
辛いときに支えてくれて、趣味に生きて良いんだと、好きな物は好きと言って良いんだと再認識させてくれた北条先生、ありがとうございます🙇  #北条司展  
辛苦的时候支持我，让我重新认识到可以按照兴趣生活，喜欢的东西就可以说喜欢的北条老师，谢谢🙇  #北条司展  

https://x.com/machagon0322/status/1849402548320714870  
尊い…最後の最後に見送ってくれた獠が尊い👼 今回は物販が図録しか買えなかったから次回は沢山買えるように頑張って働こう💪  
https://pbs.twimg.com/media/Gaplz9DbkAAaGlm?format=jpg&name=medium  

https://x.com/machagon0322/status/1849398607788794131  
この4点がずっと好きで、原画が見られて本当に良かった😍  来年はシティーハンター40周年。きっと何かが起きるはず😄  いや、起きて欲しい🙇🙇🙇スタッフさん、お願いします🙏  もっと大きな場所でもっと沢山の素敵な作品を拝見したいです。  
一直很喜欢这4幅作品，能看到原画真是太好了😍明年是城市猎人40周年。一定会发生什么事😄不，希望你醒醒🙇🙇🙇工作人员，拜托了🙏我想在更大的地方看到更多的好作品。  
https://pbs.twimg.com/media/GapiNseaoAEOuH2?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GapiN1dbEAAUYR_?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GapiOebaUAAOClo?format=jpg&name=small
https://pbs.twimg.com/media/GapiO0rbkAAnTf3?format=jpg&name=900x900  

https://x.com/machagon0322/status/1849397365343768869  
誤植見つけたの。正しくはけものへんなの。スタッフさん、修正して下さるって🙇  確認しに行きたいけど、閉幕までずっとお仕事で無理😢
我发现错字了。正确来说是野兽怪。工作人员说要修改一下🙇  虽然很想去确认，但是闭幕之前一直都在工作，不行😢  
https://pbs.twimg.com/media/GaphGilasAARMif?format=jpg&name=4096x4096  

https://x.com/machagon0322/status/1849396005994934656  
素晴らしい作品を見てきた。繊細なタッチで描かれているのが本当に分かるし、え、そんな事してたのこの表紙は😳😳😳と驚く事もあって、本当に行って良かった💞💞💞💞💞 #北条司展
我看到了很棒的作品。真的知道用纤细的笔触画的，诶，做了那样的事的这个封面😳😳😳吃惊的事也有，真的去了太好了💞💞💞💞💞 #北条司展

https://x.com/machagon0322/status/1849393286815096918  
このスクリーントーンとホワイトの使い方が最高だった。実物を見なきゃこの素晴らしさは分からない。  
这种屏幕色调和白色的使用是最棒的。不看实物就不知道有多好。  
https://pbs.twimg.com/media/GapdZIxbAAA4PNq?format=jpg&name=medium  


https://x.com/aitsuNOyomecyan/status/1849389169258422545  
シュガーボーイ複製原画買えず...(   ᷄ᾥ ᷅ )  でも行けて本当に良かった...
买不到sugar boy复制原画...(   ᷄ᾥ ᷅ ) 但是能去真的太好了…  
https://pbs.twimg.com/media/GapZdkwbkAAxe8a?format=jpg&name=small  
https://pbs.twimg.com/media/GapZdkyaoAA9EIH?format=jpg&name=medium  


https://x.com/Saeba_Company/status/1849328956371275897  
行ってきました✨  北条司展✨  もう最香☝️  グッズ買いすぎた😅  
我去了✨  北条司展✨  最香☝️  买太多商品了😅  
https://pbs.twimg.com/media/Gaoi37LaEAEUE9C?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Gaoi37KboAAxAQY?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Gaoi37KbUAAudgP?format=jpg&name=4096x4096  




https://x.com/BlueOce87930012/status/1842195309151330764  
劇場版シティハンター天使の涙 を観てから #シティハンター 熱が再燃してる  
剧场版城市猎人天使之泪 #城市猎人 热再燃  
https://pbs.twimg.com/media/GZDK3lAbgAAS_TL?format=jpg&name=4096x4096  

https://x.com/BlueOce87930012/status/1843856230789648556  
北条司展、ワクワク✨  
https://pbs.twimg.com/media/GZaxdx7bIAAazf0?format=jpg&name=4096x4096  


https://x.com/BlueOce87930012/status/1842182934016692476  
北条司展、楽しみすぎる✨  
北条司展，太期待了✨  
https://pbs.twimg.com/media/GZC_nQSbsAEfPrO?format=jpg&name=4096x4096   


https://x.com/islalaland1214/status/1846503288885412111  
新宿ハンズ購入品✨CH愛が止まらないよ〜😭❤️まだ新米ファンの25歳だけど  語れるお友達が欲しいのです…🥹🙏Xの皆さまぜひ繋がって下さい…😢😢  
新宿hands购入品✨CH爱不能停止~😭❤️我还是个25岁的新粉丝  我想要一个可以聊天的朋友…🥹🙏X的各位请一定要联系…😢😢  
https://pbs.twimg.com/media/GaAY6Z1agAAZJ0V?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaAY6Z0bYAEeuVJ?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaAY6Z0bgAARoQs?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaAY6Z3asAAj9SG?format=jpg&name=4096x4096  


https://x.com/kuning01062517/status/1847524173205361063  
北条司展に行ってきた～東京でやってた時にはまさか福岡に来てくれるなんて予想もしてなかったからホント嬉しい♡そしてグッズも手に入れ、ランダムなアクスタまさかの槇ちゃんカブるという奇跡…💧  
去看了北条司的展览~ 在东京做的时候，没想到会来福冈，真的很开心♡而且还得到了周边商品，随机的acsta竟然还出现了槙子唱歌的奇迹…💧  
https://pbs.twimg.com/media/GaO5cSAbUAMiSh_?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaO5cSBbUAI7RrD?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaO5cSDbUAM8g4u?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaO5cSCacAAwiTw?format=jpg&name=4096x4096  


https://x.com/oimotoika/status/1848837482684514778  
🔫#シティーハンター が大好きです😊💕約1年前に読売新聞に広告が載りました。🏠我が家は朝日を購読しているのでコチラは某オークションサイトで買いました✨  裏面には海原が載っていますがフレームの都合で冴羽さんのみを飾っています😍💘  
🔫#我爱城市猎人😊💕 大约一年前，读卖新闻刊登了一则广告。我家订阅了朝阳，这是在某拍卖网站买的✨  背面有大海，但由于边框的关系，只有冴羽做了装饰😍💘  
https://pbs.twimg.com/media/Gagky7RbEAAGmsy?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/Gagky7RbEAIJMBu?format=jpg&name=4096x4096  


https://x.com/ma2bon/status/1848960672618713170   
ジャンプに育ててもらったので北条司さんの原画展行ってよかった〜  
因为是jump培养的所以去了北条司先生的原画展真是太好了~  
https://pbs.twimg.com/media/GajT3kObEAA0WU0?format=jpg&name=small  
https://pbs.twimg.com/media/GajT3j4bEAMhxft?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GajT3kDa4AA9LrL?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GajT3kbbEAAEGMn?format=jpg&name=4096x4096  


https://x.com/_pwsu183/status/1849026265908711722  
We getchu! Mysterious girl~📸 @BankkGallery  
https://pbs.twimg.com/media/GakPlbkaEAAv1YN?format=jpg&name=4096x4096   
https://pbs.twimg.com/media/GakPlbkaUAAuQAZ?format=jpg&name=4096x4096  


https://x.com/babarts1979/status/1849287691420188903  
北条司展に行ってきた！北条司先生の線は、思ったより大胆だった。かっこいい線でした。そして何より色よ！なんて綺麗ななんだろう。いやー堪能しました！テンション上がった！  
去了北条司展！北条司老师的线条比想象中大胆。很酷的线条。最重要的是颜色！多么美丽啊。哎呀，我很满足！情绪高涨了！  
https://pbs.twimg.com/media/Gan9WdEa0AAHAvn?format=jpg&name=large  


https://x.com/waffle326331/status/1848966951470801278  
マメに投稿してくれるのも有り難いし、短い期間なのに決済方法追加など来場された方の声を拾い上げて改善してくれるの本当に嬉しいですよね♪  どうしても期間中行けない自分が悔しい😭😭😭うーー😭お近くの方、是非にー！！  
很感谢你经常投稿，在很短的时间内收集来场者的意见进行改善，比如追加结算方法等，真的很开心呢♪  怎么也不能去的自己很不甘心😭😭😭呜—😭附近的人，来吧！ ！  


https://x.com/yukifukuoka/status/1848715774736932967  
今日やっと北条司展に伺えました！吉祥寺に行きたかったものが見れて嬉しかった〜！福岡開催ありがとうございます😊♥️🥰　35年以上前に、学校の文化祭で描いた絵も添付してみる😆😆😆#北条司展 #北条司 #シティーハンター #キャッツアイ #六本松蔦屋書店 #原画展  
今天终于去看了北条司展！看到想去吉祥寺的东西很高兴~ ！福冈举办谢谢😊♥️🥰把35年前在学校文化祭上画的画也添加进去😆😆😆#北条司展#北条司#城市猎人#猫眼#六本松茑屋书店#原画展  
https://pbs.twimg.com/media/Gaf1MwqbEAIhaB2?format=jpg&name=900x900  
https://pbs.twimg.com/media/Gaf1MxoaAAACALM?format=jpg&name=900x900  


https://x.com/hinnahinna8/status/1848209611742216604  
ビジュがいい！！最高！！でも死にそう！！！悲しい！！！！生きて！！！！あぁでも伏せ目微笑水も滴るいい男セクシーすぎるサンキューいやでも死ィィィィ！！！！？？？？と混乱の最中10枚近く撮ってた。アニキは沼。※原画展で撮影したものです  
美女好！ ！最棒！ ！但是快死了！ ！ ！悲哀！！！！活着！！！！啊但是低垂的眼睛微笑滴水的男人太性感了谢谢即使不愿意也要去死！！！！？？？？
在混乱中拍了近10张照片。哥哥是沼泽。※原画展中拍摄的照片  
https://pbs.twimg.com/media/GaYo2Bea8AAvVSY?format=jpg&name=4096x4096  
https://x.com/mk_scratch/status/1848482844089454713  
僕もこれ見て、感動のあまり、しばらく硬直してたような気がします。印刷や画面越しでは感じ取れない、作者の息吹が伝わりますね。#シティーハンター #北条司展    
我看到这个，也感动得僵硬了好一阵子。 你无法在印刷品或屏幕上感受到它，但你可以感受到艺术家的气息。 #城市猎人 #北条司展


https://x.com/SaebaRyo0326/status/1848694680785182920  
当日券、吉祥寺はただのレシートだった気がする…  
当日券，吉祥寺感觉只是一张收据…  
https://pbs.twimg.com/media/GafiAq8bEAAEkvF?format=jpg&name=4096x4096  


https://x.com/BlueOce87930012/status/1848159558898000331  
複製原稿欲しい  原画展に行きたいという禁断症状が...きっと脳の快楽報酬系に入ったんだな苦笑
我想要复制原稿  想去原画展的禁断症状…一定是进入大脑的快乐报酬系统了吧苦笑  


https://x.com/BlueOce87930012/status/1848160673425227785  
北条司先生、故郷福岡での開催本当にありがとうございます。ふるさと開催を熱望し、ポストに応えていただき感謝です  
北条司老师，非常感谢您在故乡福冈举办。热切盼望故乡举办，感谢您的回应  
https://pbs.twimg.com/media/GaX8Veob0AA9sJV?format=jpg&name=large  


https://x.com/chiroru_special/status/1847953515379138796  
ZENONギャラリーとの違いをスタッフの方に確認！ZENONギャラリーよりも展示点数が少なくなっていますが、追加分はないとのことでした😊やっぱり最初は獠ちゃんの等身大がお出迎えでした🥹シティーハンターコーナーの途中の壁をよく見るとカラスが飛んでるし😆💕見つけられて嬉しい❤️  
向工作人员了解与ZENON画廊的不同之处！比ZENON画廊展示的数量少，但是没有追加的份😊果然一开始是獠的等身大来接我的🥹城市猎人专柜中间的墙壁仔细一看，发现乌鸦在飞很高兴找到了😆💕❤️  
https://pbs.twimg.com/media/GaU_7YKaEAIr-PM?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaU_7FoaQAA9FXc?format=jpg&name=small  
https://pbs.twimg.com/media/GaU_7b4bsAA1JT1?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaU_7YEbYAApPeR?format=jpg&name=4096x4096  


https://x.com/chiroru_special/status/1847948944380469668  
北条司展 IN福岡  TSUTAYA、墓場の画廊のグッズが揃ってる！！複製原画も揃っていて最香❤️  5000円以上で北条司展のポスター貰えるのが嬉しいです🥹とにかくスタッフの方が優しいし、シティーハンターへの愛を持って対応してくださるので嬉しいです🥰ほんとシティーハンターへの愛が溢れた空間です！！  
北条司展IN福冈  TSUTAYA，墓场的画廊的商品齐全！ ！复制原画也齐全最香❤️  5000日元以上就能拿到北条司展的海报很高兴🥹总之工作人员都很温柔，对城市猎人的爱也很高兴🥰真的是充满了对城市猎人的爱的空间！!    
https://pbs.twimg.com/media/GaU7s5KaYAAfnn4?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaU7tGUasAA0mG3?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaU7taobUAAV1dZ?format=jpg&name=4096x4096    


https://x.com/terayan_gti/status/1847916447126712830  
原画でも前後の背景と人物の距離があるものは別絵を重ねてるのは初めて知った。ほんとに線が美しいのよ北条先生。  
我第一次知道原画也会把前后背景和人物有距离的东西重叠成不同的画。线条真的很美啊北条老师。  
https://pbs.twimg.com/media/GaUeNnhbUAAf7WG?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaUeNuCbUAMtz2O?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaUeNwtbUAAFfd9?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaUeNyMbUAEQNhp?format=jpg&name=4096x4096  


https://x.com/SaebaRyo0326/status/1847820368389820684  
やっと来れた😂😂七隈線に乗ることないから、いつものように空港線の方に行ってしまった😇吉祥寺の方が断然良い←けど、地元でってのがまたいい😌✨先生大好き   TSUTAYAのグッスは今なら在庫まだありましたよー！  
终于来了😂😂因为没有坐七隈线，所以像往常一样去了机场线😇吉祥寺当然更好←不过还是本地的好😌✨老师很喜欢  TSUTAYA的文具现在还有库存哟！  
https://pbs.twimg.com/media/GaTG0_-bUAEiGMh?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaTG0_8bAAAH9Dy?format=jpg&name=small  


https://x.com/pinktanpopo34/status/1847670403113873650  
イラストレーションズ届いた❣️ #ギャラリーゼノン さんの #北条司展 で展示されていたSPLASH!３。他のSPLASH!も知りたくて！北条司スペシャルイラストレーションズにはカッコイイ先生の写真やインタビューに、作画のプロセスまで❣️載っていて嬉しい😆密林さんありがとう❣️また原画見たいなぁ💕  
插图收到了❣️#画廊zenon 先生的#北条司展被展示的SPLASH！3。其他的SPLASH！我也想知道！北条司Special Illustrations里有很Cool的老师的照片和采访，还有作画的过程❣️很高兴😆谢谢密林❣️还想看原画啊💕  
https://pbs.twimg.com/media/GaQ-b-sbUAUh3Bx?format=jpg&name=small  
https://pbs.twimg.com/media/GaQ-b-sagAAj1kp?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaQ-b-ubUAEGtGS?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaQ-b-sa0AA2fya?format=jpg&name=4096x4096  


https://x.com/waffle326331/status/1847641435555975436  
チケットや商品情報、日々の混雑状況から落とし物のお知らせまで‥お忙しい中で公式さんの細やかな投稿のおかげで、福岡までは行けないけれど現地の状況が分かり楽しませていただいてます☺️  
门票和商品信息，从每天的拥挤状况到失物通知…在百忙之中多亏了官方的细致投稿，虽然不能去福冈但是了解了当地的状况让我很开心☺️  


https://x.com/6RdjRXr7g5ZOq3H/status/1847593559920415214  
ようやく行けました✨  ☔だったためか混んではいなくて、ゆっくり見て回れました。1枚1枚から先生の情熱が伝わってくるようで( ˘ω˘)ｼﾞｰﾝ   
终于去了✨☔不知道是不是因为人多，可以慢慢地逛了逛。每一张都传达着老师的热情( ˘ω˘)ｼﾞｰﾝ     


https://x.com/Mika_fuk/status/1847545722650186210  
北条司展行ってきた  
中に椅子がないので(当たり前)足が痛くて  
じっくり見ることができなかったけど  
素晴らしい作品堪能できました  
去了北条司展  
因为里面没有椅子（当然）脚很疼  
虽然没能仔细看  
我欣赏了很棒的作品  
https://pbs.twimg.com/media/GaPNCrDbkAADokx?format=jpg&name=small  
https://pbs.twimg.com/media/GaPNCrDbUAQiKHg?format=jpg&name=4096x4096  


https://x.com/BlueOce87930012/status/1847485004500418670  
北条司展IN福岡でゲットしましたー  
我在北条司展IN福岡上得到了它  
https://pbs.twimg.com/media/GaOV0YqbUAQ8p1u?format=jpg&name=4096x4096  


https://x.com/BlueOce87930012/status/1847467985705062808  
北条司先生の原画は繊細で魂が入ってる  
北条司老师的原画细腻而有灵魂  
https://pbs.twimg.com/media/GaOGVhAbUAMhTLc?format=jpg&name=4096x4096  


https://x.com/BlueOce87930012/status/1847444685947883523  
北条司展 IN福岡、遂に来たよ  泣ける...  
北条司展IN福冈，终于来啦  好想哭…  
https://pbs.twimg.com/media/GaNxIw6bUAAet0X?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaNxIw-bUAAxKAd?format=jpg&name=4096x4096  


https://x.com/aya510/status/1847254081682989409  
めんたいこ県、到着してました🍜昨日、もらった獠ちゃんのカードと一緒に💥🔫  
明太湖县，到达了🍜和昨天收到的一张卡一起💥🔫  
https://pbs.twimg.com/media/GaLDy3YagAAbzDr?format=jpg&name=4096x4096  





https://x.com/BlueOce87930012/status/1847155264832807217  
リョウちゃんのアクリルスタンドほしい...  
我想要獠的亚克力台灯…  


https://x.com/aya510/status/1846846903725838806  
北条司展 in 福岡に行ってきました。イラスト見てたら、色んな思い出が一気にフラッシュバックして🥹ｳﾙｳﾙ  沢山写真撮ったけど、まだ始まったばかりだから、差し支えない物だけ📱✨  
去了北条司展in福冈。看了插画，各种各样的回忆一下子闪回🥹呜呜呜呜拍了很多照片，不过，因为才刚刚开始，所以只拍了没有影响的照片📱✨  
https://pbs.twimg.com/media/GaFRcaob0AM6Ojy?format=jpg&name=900x900  
https://pbs.twimg.com/media/GaFRcaub0AAN7w-?format=jpg&name=4096x4096  


https://x.com/k3mameco/status/1846778710361624783  
やべぇ！めちゃくちゃ長居してしまった🤣🤣🤣(1.5時間)  た、た、楽しかった〜！！ほぼ撮影OKとか優しすぎん！？接写で先生どんだけ描き込んでるんかわかるぅ！すごいよぉ  漫画家さんは体力おばけだよぉ😂  メッセージ迷ったけど…隙間に書かせて貰った！！感謝しかない！皆行こう💖
糟了！呆了很长时间🤣🤣🤣（1.5小时）好、好、好开心~ ! ！拍摄几乎OK，太温柔了！?我知道老师用近距画了多少了！好厉害啊漫画家是体力妖怪😂留言迷路了…我在空隙里写了！！只有感谢！大家走吧💖  
https://pbs.twimg.com/media/GaETchnb0AQ8OOi?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaETchrb0AY3pbA?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaETchnb0AMbrGH?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaETchpaUAA_R3Q?format=jpg&name=4096x4096  

https://x.com/k3mameco/status/1846783481831215181  
そう…ちなみに  シティーハンター以外では  あの吸血鬼のお話マジで好きなのよ！！！大好き！！！！  ハッピーエンド大好物だよ！💖  主人公も少しミック？リョウちゃん？に似ていてさ…でへへ。一見ダメそうな男子が実は優しいとか男前とかなると心がくすぐられてしまう…！！！（2次元限定やけどw)  
是吗…顺便一提  除了城市猎人以外  我真的很喜欢吸血鬼的故事！ ！ ！喜欢！！！！  我最喜欢快乐结局了！💖  主人公也有点像米克？獠？…呵呵。乍一看很没用的男生实际上变得很温柔很有男子气概  心痒痒的…！ ！ ！（2次元限定w）  

https://x.com/k3mameco/status/1846835122081091925  
本日の戦利品と描き込みが鬼えぐい神カラーのせちゃう。ホワイトの線が本当にすごいんだよなぁ…そりゃ1時間以上まじまじ見ちゃうよ！！😭感謝  ネックウォーマー、図録、複製原稿…と特典カードは神引きした💖(裏にパパ)複製原稿とか初めて買った…想像以上にでかい…毎週連載とか死んじゃうよっっ
今天的战利品和插图鬼神颜色。白色的线条真的很棒啊…看了一个多小时！ ！😭感谢  颈套，图录，复制原稿…和特典卡是有面额的💖（后面是パパ）复制原稿第一次买…比想象中还要大…每周连载都会死的  
https://pbs.twimg.com/media/GaFGwLib0AMGqSf?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaFGwLfb0AETOV_?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaFGwLdb0AI7Vwx?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaFGwLfb0AIrTH8?format=jpg&name=4096x4096  

https://x.com/k3mameco/status/1846836307873747456  
皆も行ける機会あったら  是非行って〜〜！！！！！この尊すぎる生原稿の山を  その目で見るんやーーーーー！！！それと特典カード裏面のパパも  載せとくね〜ちなみに  写真撮影OK(動画はダメ)  SNSあげも大丈夫  と説明もらいました〜ちょっと遠いけど…行こう！！(それしか言えんのか)  
如果大家都有机会去的话  一定要去~ ~！！！！！你的眼睛看这堆积如山的珍贵原稿！ ！ ！还有奖励卡背面的パパ  放上去~顺便说一下拍照OK（视频不行）上SNS也没问题  说明~虽然有点远…走吧！ ！（只能这么说吗？）  
https://pbs.twimg.com/media/GaFH1Rnb0AERqiO?format=jpg&name=4096x4096  

https://x.com/fuwarimoon/status/1846772865846464997  
北条司展に行ってきた！ グッズ色々欲しかったけど、展示された作品をまとめた図録と複製原稿を購入😊  
去了北条司展！虽然想要很多周边，但还是买了展示作品的图录和复制原稿😊  
https://pbs.twimg.com/media/GaEOFx9b0AQNQFD?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaEOGAVb0AAz-6T?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaEOGcab0AI710J?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GaEOGccb0AIDGwr?format=jpg&name=4096x4096  


https://x.com/Rumi_XYZ369/status/1846564344991195171
ずっとずっと楽しみにしてた北条司展！！幸せ空間でした🥹  
一直一直期待的北条司展！ ！幸福空间🥹  
https://pbs.twimg.com/media/GaBQeunbkAA0qzd?format=jpg&name=4096x4096  


https://x.com/miinemiine_ch/status/1846499853330960713  
福岡での原画展、開催おめでとうございます！🎉🎉🎉  TLで流れてくる様子を拝見するだけでワクワクしてきますねっ(  ˶'ᵕ'˶)  グッズも沢山あるみたいで羨ましい限りです。いいなぁいいなぁ( ܸ ⩌⩊⩌ ܸ )  
在福冈举办原画展！🎉🎉🎉  看到TL播放的样子就很兴奋呢っ(  ˶'ᵕ'˶)  周边好像也有很多，真是令人羡慕。很好很好( ܸ ⩌⩊⩌ ܸ )   


https://x.com/ryosaeba357xyz1/status/1847118967170912282  
https://x.com/ryosaeba357xyz1/status/1846772301087580218  
https://x.com/ryosaeba357xyz1/status/1846575877469212922  
https://x.com/ryosaeba357xyz1/status/1846486677814133049  


https://x.com/waffle326331/status/1846471915000787035  
開幕おめでとうございます㊗️今回は伺うことできず残念ですが一人でも多くの方が訪れ、先生の原画の素晴らしさ、繊細さに迫力といった本物を感じとっていただける機会になりますよう願っております✨福岡‥行けるものなら行きたい😭😭😭  
恭喜开幕㊗️很遗憾这次没能来拜访，希望能有更多的人来参观，能够有机会感受到老师原画的精彩、细腻和震撼力✨福冈…能去的话想去😭😭😭  


https://x.com/orinri060511/status/1846470048460034074  
北条司展へ初日に行ってきました✨  福岡で開催してくれるなんて嬉しい\(°∀° )/ ありがとうございます🎵 北条司先生の原画を見るのは2017年北九州漫画ミュージアム以来です💕あのときは写真撮れなかったので、今日は思う存分撮りまくってきました(笑)何周もしてしまった🥴  
第一天去了北条司展✨ 很高兴能在福冈举办\（°∀°）/谢谢🎵 2017年北九州漫画博物馆以来第一次看到北条司老师的原画💕那时候不能拍照，今天就尽情地拍了（笑）转了好几圈🥴  
https://pbs.twimg.com/media/GZ_6s8lb0AAviqs?format=jpg&name=small  
https://pbs.twimg.com/media/GZ_6s8ibEAArHVK?format=jpg&name=4096x4096  

https://x.com/Saeba_Company/status/1846458091149414405  
来週何処かで行きます！北条司展✨ジャンパー ジャケット コート どのＶｅｒ．で行くかな…🤔  
下周去什么地方！北条司展✨jumper jacket coat 穿哪个款式去呢…🤔   


https://x.com/ryosaeba357xyz1/status/1846442763203875291  


https://x.com/m527y/status/1846440285733376076  
六本松蔦屋書店 で今日からスタートした #北条司展 へ🚶‍♂️🎶  繊細でありつつ迫力満点の貴重な生原画の数々は一見の価値ありです✨✨  #漫画 #マンガ #原画展  
六本松蔦屋书店今天开始的#北条司展🚶‍♂️🎶细腻而又充满魄力的珍贵原始原画，值得一看✨✨  #漫画 #マンガ #原画展  
https://pbs.twimg.com/media/GZ_feJWbIAAQwAj?format=jpg&name=900x900  
https://pbs.twimg.com/media/GZ_feH-b0AAv8Kw?format=jpg&name=900x900  


https://x.com/A_L_XYZ/status/1846177687913500694  
念の為、お姉ちゃんに今週来週あたり福岡出張ないか聞いてみてる。そんな都合よくはいかないだろうけど〜複製原画欲しすぎて！☺️  
以防万一，我问姐姐这周下周会不会去福冈出差。虽然不会那么凑巧~不过我太想要复制原画了！☺️  


https://x.com/KyuFukuokaOtaku/status/1844816856877854890  
北条司展  
2024年10月16日(水)～27日(日)  
六本松 蔦屋書店  
デビュー作、連載作や読切等を網羅した北条ワールドと歴史を体感できる原画展  
▼展示作品 (予定)  
シティーハンター  
キャッツ♥アイ  
こもれ陽の下で…  
おれは男だ！  
エンジェル・ハート  
F.COMPO  
など  
https://fukuoka-otaku.net/hojo-tsukasa-ten/  





https://x.com/hojotsukasaten/status/1843591234155360641  
╭━━━━━━━━━╮  
  ✨グッズ購入特典✨  
╰━ｖ━━━━━━━╯  
北条司展IN福岡でグッズを税込5,000円以上ご購入いただいた方に、本展のポスター（A2サイズ）をプレゼントいたします👏✨  
北条先生の作品が大集合した美しいビジュアルポスターです❗️  
ぜひお気に入りのグッズをたくさん見つけて、ポスターをゲットしてください☺️  
※ポスター特典は無くなり次第終了となります  


https://x.com/hojotsukasaten/status/1842854780680769887  
╭━━━━━━━━╮  
　✨グッズ紹介✨  
╰━ｖ━━━━━━╯  
シティーハンター お守り　2種セット  
交通安全と恋愛成就のお守りです🚗❤️  
獠と香があなたの幸せを見守ります😎✨  
https://pbs.twimg.com/media/GZMiMR8bYAA2L4r?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GZMiNE8bQAA-E4i?format=jpg&name=4096x4096  


https://x.com/hojotsukasaten/status/1842487880218026240  
╭━━━━━━━━╮   
    ✨グッズ紹介 ✨  
╰━v━━━━━━╯  
名シーンプレート「お茶しよーよ」  
獠が美女に迫るおなじみの展開…😍  
そんなシーンのセリフがシンプルなお皿になりました🍽️  
盛り付け方次第では隠しメッセージみたいになって面白いアイテムですね🤭  
\#北条司展 #北条司 #シティーハンター   #原画展 #六本松蔦屋書店 #福岡   
公式HP  https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/  
北条司展チケット購入はこちら  https://ticketme.io/event/group/34  
https://pbs.twimg.com/media/GZHU9WpasAAXWkO?format=jpg&name=small  
https://pbs.twimg.com/media/GZHU9Wma8AANqY2?format=jpg&name=small  


https://x.com/hojotsukasaten/status/1842135457737724189  
＿人人人人人人人人人人人人人人人人人人人＿  
＞　北条司サイン入りポスターが当たる！　＜  
￣Y^Y^ Y^Y^Y^Y^Y^Y^Y Y^Y^ Y^Y^￣  
✨『北条司展 IN福岡』開催記念キャンペーン✨  
このアカウント（@hojotsukasaten  
）をフォロー 
↓  
この投稿をリポスト♻️  
↓  
応募完了❗️  
抽選で５名様に北条先生のサイン入りポスター（A2サイズ）をプレゼント🎁✨  
世界にたった５枚だけのポスターをゲットしよう❗️  
このチャンスをお見逃しなく👀  
※応募期間は2024/10/4(金)～10/14(月)まで  
※当選者の方にのみDMでご連絡させていただきます。当落のお問合せにはお答えできませんのでご了承ください  
👀公式HP  https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/  
🎫北条司展チケット購入はこちら  https://ticketme.io/event/group/34597257-d468-4cd2-8fc2-13cabefa5f28/9151d214-6a77-434e-98d0-970cc6d7d690  
\#北条司展　#北条司　#シティーハンター　#原画展　#六本松蔦屋書店　#福岡  
https://pbs.twimg.com/media/GZA_trrbAAQM5QX?format=jpg&name=medium  


https://x.com/hojotsukasaten/status/1841765241753108929  
╭━━━━━━━━╮  
✨グッズ紹介✨  
╰━ｖ━━━━━━╯  
シティーハンターのキャラクターたちがとってもかわいい  
刺繍ステッカーになりました♪  
あなたはどの組み合わせが気になりますか？  
もちろん全員集合も最高に素敵です‼️  
刺繍ステッカー（2枚セット）  
新宿正義セット　獠＆冴子  
喫茶キャッツ・アイセット　海坊主＆美樹  
槇村兄弟セット　秀幸＆香  
\#北条司展　#北条司　#シティーハンター　#原画展　#六本松蔦屋書店　#福岡  
👀公式HP  https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/  
🎫北条司展チケット購入はこちら  https://ticketme.io/event/group/34  
https://pbs.twimg.com/media/GY9DuXNbAAATDrN?format=jpg&name=4096x4096  


https://x.com/hojotsukasaten/status/1841406404076405183  
╭━━━━━━━━╮  
　✨グッズ紹介✨  
╰━ｖ━━━━━━╯  
メモ帳大集合‼️  
デスクに置けばいつでも一緒☺️  
キャッツカード付箋は悪用厳禁！  
皆さんはキャッツカードで何を頂きに行きたいですか⁉️  
シティーハンター ブロックメモ RYO&KAORI　 
シティーハンター メモ帳 トランプ柄  
キャッツカード 付箋 ２個セット  
\#北条司展　#北条司　#シティーハンター　#キャッツアイ　#原画展　#六本松蔦屋書店　#福岡  
👀公式HP  https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/  
🎫北条司展チケット購入はこちら  https://ticketme.io/event/group/34  
https://pbs.twimg.com/media/GY3GdHbaAAAkavy?format=jpg&name=4096x4096  


https://x.com/hojotsukasaten/status/1840330269469073576  
原画たちの身支度開始🖼️✨  
￣￣V￣￣￣￣￣￣￣￣￣  
 展示される予定の原画を額に入れて、美しく展示するための『額装』作業に入りました👀  
皆様が見に来てくださるのを心待ちにして、原画たちもおめかししています…✨  
\#北条司展　#北条司　#シティーハンター　#原画展　#六本松蔦屋書店　#福岡  
👀公式HP  https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/  
🎫北条司展チケット購入はこちら   https://ticketme.io/event/group/34  
https://pbs.twimg.com/media/GYoqnvEbkAEUbKn?format=jpg&name=4096x4096  
https://pbs.twimg.com/media/GYoqnvLaoAA1pYw?format=jpg&name=4096x4096  


https://x.com/hojotsukasaten/status/1839143616633360726  
北条司展 IN福岡のチラシができました✨  
￣￣V￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣  
アーティストカフェ福岡  
北九州市漫画ミュージアム  
六本松 蔦屋書店  
には既に置いていただいています😆  
お近くの方、福岡に遊びに行く方はぜひ探してみてください😎👍  
\#北条司展　#北条司　#シティーハンター　#原画展　#六本松蔦屋書店　#福岡  
👀公式HP  https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/  
🎫北条司展チケット購入はこちら  https://ticketme.io/account/kumamo  
https://pbs.twimg.com/media/GYXyyvLWMAEIXfd?format=jpg&name=medium  


https://x.com/hojotsukasaten/status/1838858742449999910  
／  
✨入場者特典✨  
北条司展特製カードプレゼント❣️  
＼  
どんな絵柄が出るかドッキドキ❤️‍🔥  
北条司展 IN福岡にご来場されたお客様全員に  
合計6種の特製カードの中から1枚をランダムにプレゼント✨  
ぜひゲットしてください😎  
👀公式HP  
https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/   
🎫チケット購入はこちら  
https://ticketme.io/account/kumamoto-manga  
\#北条司展　#北条司　#シティーハンター　#六本松蔦屋書店  
https://pbs.twimg.com/media/GYTwRv5akAQ2Bc4?format=jpg&name=900x900
https://pbs.twimg.com/media/GYTwRwAbwAAs4e0?format=jpg&name=small  
https://pbs.twimg.com/media/GYTwRv0boAAQz7_?format=jpg&name=small  
(Reply...)  
https://pbs.twimg.com/media/GZDAzM4bIAAh05h?format=jpg&name=medium  
https://pbs.twimg.com/media/Gadmn-ZbEAA1YEG?format=jpg&name=4096x4096  


(09.20) https://x.com/hojotsukasaten/status/1837039153625845888   
\━*＼北条司展 IN福岡　開催決定！／*━  
           2024年10月16日〜27日  
               📍六本松 蔦屋書店  
\━━━━━━━━━━━━━━━━━  
2025年2月に控える『シティーハンター』連載40周年に向け、漫画家北条司デビュー作から連載作品や読切等、ほぼ全ての作品を網羅したこれまでの北条ワールドと歴史を体感していただける原画展を開催します。  
9月17日よりチケット販売中！  
皆様のご来場を心よりお待ちしております🙇‍♀️  
👀北条司展 公式サイト  
https://gallery-zenon.jp/exhibitions/hojotsukasaten_fukuoka/   
 🎫チケット購入はこちら  
https://ticketme.io/account/kumamoto-manga  
\#北条司展　#北条司　#シティーハンター　#六本松蔦屋書店  
https://pbs.twimg.com/media/GX55XdVaUAAKNAw?format=jpg&name=4096x4096    