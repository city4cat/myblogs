海坊主虽然视力减退，但依然能辨识世界。这种能力是真实存在的。([hojocn上的链接](www.hojocn.com/bbs/viewthread.php?tid=96956))  

以下内容来自科普文章：  
[只需10周，你也能学会蒙眼识物的“超能力” ](./blind_hearing.md)  
[一些盲人其实能看见，而这种吓人的能力存在所有人脑中 ](./blind_sight.md)  

