
## 只需10周，你也能学会蒙眼识物的“超能力”

source: https://mp.weixin.qq.com/s/Ola9948uw0wUI8B2zgFGbQ

环球科学 2022-07-14  
撰文 | 栗子  
审校 | 二七  

![](img/blind_hearing_00.jpg)  
图片来源：askabiologist.asu.edu/echolocation

对拥有视力的人类说，回声定位技能或许不是最重要的。但对盲人来说，获得这种能力，可能意味着拥有出门的勇气。

自然界的许多动物，都有回声定位的技能。

其中，最有名的可能是蝙蝠。在大约900个蝙蝠物种里，超过一半都会使用回声定位。它们中的不少物种通过收缩喉部发出声波，也有一些依靠咋舌声，还有少数利用鼻腔发声：当这些声波遇上周遭的物体再弹回蝙蝠的两只耳朵里，蝙蝠便可从回声中推测物体的位置，并识别物体。

在黑暗的环境下，回声定位能够帮助蝙蝠锁定猎物、躲避障碍、寻找栖息地，是生存所需的必备技能。用这种方法探测到的物体，甚至可以细如发丝。

而在这项令人羡慕的技能背后，蝙蝠拥有特化的内耳结构，它们听到的声波频率，比人类能感知的范围（20-20000 Hz）要宽广得多。另外一些能够回声定位的动物如海豚和齿鲸，内耳当中也有和蝙蝠相似的特化耳蜗，为动物带来敏锐的听觉。

相比之下，人类的耳朵或许没有同样优异的性能，但这不代表我们就不能利用回声定位来探知周围的情况。研究表明，不论是盲人还是普通人，都可以通过短期训练，掌握回声定位的技巧。


## 用耳朵“看”到这个世界


来自美国加州的丹尼尔·基什（Daniel Kish）出生在1966年，刚满1岁的时候，他就因为患有一种罕见的儿童眼癌——视网膜母细胞瘤，而被摘除了两只眼球。

丹尼尔几乎从来没有看过这个世界的样子。但走在路上的时候，他依然能判断出前面有一辆车；当站在一棵姿态奇异的树旁边，他不光能感觉到那是树，还知道树长歪了——只在低处蜿蜒，长不高，跟普通的树形状很不一样。

![](img/blind_hearing_01.gif)  
图片来源：HDNet


他可以在城市里自如地行走，也能骑着自行车往来穿梭。假如只看丹尼尔骑车的画面，大家多半会以为这就是一个视力正常的普通人。但打开视频的声音之后，我们就会听见许多咔哒声，进一步仔细观察的话，还会发现声音是骑车人嘴里发出的。

每秒两到三下，这些短促而清脆的咔哒声，是丹尼尔弹舌时发出的，也是他在回声定位中用到的工具。从很小的时候他就感觉到，每当自己发出声响，周遭的世界都会给个回应。而且，不同的物体有不同的回应，比如树、车、门廊……每种物体在他脑海里都对应着各自的“形象”，只是这形象不是由图像表示，而是由回声来体现。



[](img/blind_hearing_02.mp4)  
视频来源：HDNet



从回声当中，丹尼尔虽然听不出物体在视觉上的细节，但能分辨出轮廓，了解物体的形状和大小，还有距离和方向。想知道物体离自己多远，可以依靠回声的频率（音高）和响度来判断，离得越近音调越高且越响；左右耳收到的不同回声，则有助于推测物体所在的方向。当物体位置变化的时候，回声也会随之改变，丹尼尔出行时就需要频繁发出咔哒声，不停地感受周围环境。

丹尼尔说，虽然自己弹舌的声音基本上每次都一样，但遇到物体后返回的声波，已经被物体表面的特性改变了。经过多年训练，他可以依靠回声来感受物体的质地，判断密度大小。这并不是什么精确的测量，但对丹尼尔来说能区分木栅栏和金属栅栏已经足够——木栅栏给他的回声更加沉闷一些。

在衣食住行中，对盲人最不友好的或许就是出行，一种对未知世界的恐惧，可能阻挡了许多盲人出门的脚步。而丹尼尔的“声纳”再加上手杖，除了满足日常出行需求，还能支持他远足的爱好——回声定位，仿佛就是他与世界谈判的资本。

![](img/blind_hearing_03.jpg)  
图片来源：BBC


当然，丹尼尔并不是人类中唯一的回声定位能力者。从18世纪中叶开始，人们就已经注意到了盲人用非接触的方式定位物体的能力。只是，当年的学者可能没有意识到听觉在其中的作用，更多地认为是物体给人脸施加了某种形式的“压力”，让盲人抓到一些规律，判断出面前有障碍物，这才会在撞墙之前停下脚步。

来到20世纪40年代，康奈尔大学的科学家邀请多位盲人参与实验，终于证明了盲人非接触式感知物体的能力并不是由“压力”驱动，而是靠声音和听觉成就的。

但直到如今，能在生活中熟练使用回声定位的盲人依然是少数，丹尼尔作为少数人中的一位，也常常收到采访和演讲邀约。他总是喜欢强调自己并不特别，回声定位也不是他独有的技能，而是任何人经过训练都能掌握的能力。

近年来，丹尼尔在把自己的技巧传授给更多的人。与此同时，一些科学家也自己的研究中证明，普通人想要拥有“声纳”，并不是一件太难的事。


## 三个月能训练到什么程度？


在2021年发表的一项研究中，来自英国杜伦大学的团队，征集了26位志愿者（年龄在21-79岁之间）来接受回声定位训练：其中12位是盲人，从童年起就被诊断为失明（矫正后视力较好的那只眼睛视力仍低于0.1），另外14位是拥有视力的人。

和丹尼尔用到的方法相似，志愿者们也要依靠口腔里发出的咔哒声来进行回声定位，具体训练内容包括区分物体大小、感知方向以及虚拟导航这几个小项。每周安排三次训练，一次时长大约2-3小时。训练期结束之后，志愿者要参加考试，并与使用回声定位超过10年的7位专业人士对比成绩。

第一项任务，区分物体大小。志愿者面前有一副架子，上面插着一大一小两个圆盘，可能大圆盘在小圆盘上方，也可能小圆盘插在大圆盘上方。人们需要站在离圆盘33厘米远的地方，通过发出咔哒声，来判断大小圆盘哪个在上哪个在下。

![](img/blind_hearing_04.jpg)  
图片来源：原论文


考试结果显示，经过10周训练后盲人志愿者判断大小的平均正确率达到74%，有视力志愿者的的平均正确率则有84%。两组志愿者的成绩都比训练开始前有了明显提升，只是还没有达到专业人士的水准（91%）。而专业人士在考核中，站在离圆盘100厘米远的地方，比志愿者的考试距离要远。

第二个任务是辨别方向。志愿者面前的架子上插着一块长方形木板，方向可能有四种：竖着（0°），往一边斜（45°），横着（90°），往另一边斜（135°）。人们需要从中选出正确的方向。

![](img/blind_hearing_05.jpg)  
图片来源：原论文


此项考核当中，盲人组最终的平均正确率是62%，视力组的平均正确率是76%，总体也都比训练前的成绩有所改善。但这一次，专业人士的正确率并不优于两组志愿者，虽然他们依然是站在比志愿者更远的位置考试的。

第三项任务是虚拟导航，或者说走虚拟迷宫。这里用的迷宫，是前人研究中已经建好的模型：科学家们在T形、U形、Z形等几种形状的实物迷宫里，录下了在每个位置发出的咔哒声，和双耳收到的回声，并把这些声音放进虚拟迷宫。考核中，志愿者需要在电脑上操作，根据提前录好的回声来判断对应的位置，在180秒内走到迷宫终点，而一旦撞墙就需要从原点重新开始。

![](img/blind_hearing_06.jpg)  
图片来源：原论文


在虚拟导航考试当中，科学家主要关心的是人们走出迷宫所用的时间，以及撞墙次数。训练后的志愿者们，走完迷宫的平均用时从119.30秒下降到了48.42秒，成效十分显著；志愿者的平均撞墙次数，也从最初的6.57次下降到3.64次。从用时上看，视力组的成绩优于盲人组，而专业人士的用时与盲人组之间并没有明显差距。

三项考试结束后，科学家感受到，普通人经过短期训练，也能让回声定位能力获得明显的进步，有时甚至可以超越常年使用回声定位的专业人士的表现。在后续随访中，盲人参与者们也表示，回声定位真的帮他们提升了活动能力。

并且，在12位盲人志愿者当中，有10位都认为这项技能可以让他们变得更独立，也在增强自己的幸福感——丹尼尔一直希望有越来越多的盲人朋友去接受回声定位训练，或许就是希望他们也能获得这样的感受。


敢于走出门，就是人生最大的一步。


原论文：  
https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0252330  


参考资料：  
https://linkinghub.elsevier.com/retrieve/pii/S0010027720300044  
https://askabiologist.asu.edu/echolocation  
https://web.archive.org/web/20020202195520/http://worldaccessfortheblind.org/thesis.txt  

