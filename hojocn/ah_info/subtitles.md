# Subtitles  
此处的"subtitles"指"每话的标题"。对于subtitles，其日文源自[mangahot](https://mangahot.jp/site/works/b_R0001), 其中文源自玉皇朝香港中文版。  

## Angel Heart (1st Season)  Vol 01  

### 第001话  運命に揺れる暗殺者   / 漂泊不定的暗殺者  Page001


### 第002话  絶叫する夢の記憶    / 大聲呼叫夢的記憶  Page051


### 第003话  あの人に会いたくて   / 想和那個人見面  Page075


### 第004话  新宿で大暴れ！     / 顛覆新宿!  Page097


### 第005话  思い出に導かれて    / 導入回憶  Page115


### 第006话  思い出との再会     / 回憶中的重逢  Page135


### 第007话  伝言板への助走     / 往留言板的助跑    Page153


### 第008话  出会いという名の再会  / 以約會為名的重逢   Page173


### 第009话  衝撃の邂逅           / 刺激的邂逅  Page191


### 第010话  こころとの対話     / 心靈的對話  Page209 


## Angel Heart (1st Season)  Vol 02  

### 第011话  事実への昂揚   / 對事實的高亢情緒 Page5  


### 第012话  衝撃を超えた真実 / 超越衝擊的事實 Page23  


### 第013话  緊迫する新宿 / 氣氛緊張的新宿 Page41  


### 第014话  衝撃のフラッシュバック / 衝擊的往事 Page61  


### 第015话  李兄弟との宿運 / 和李氏兄弟的宿緣 Page79  


### 第016话  出生の秘密 / 出生的秘密 Page97  


### 第017话  運命の対面 / 面對命運 Page115  


### 第018话  グラス・ハートのときめき / 玻璃心興奮的心跳 Page133  


### 第019话  ミステリアスな獠 / 神秘的獠 Page151  


### 第020话  宣戦布告!! / 在新宿宣戰!! Page169  


### 第021话  戦士の決意 / 戰士的决意 Page187  


### 第022话  生き続ける理由 / 繼續活下去的理由 Page207  


## Angel Heart (1st Season)  Vol03  

### 第023话  戦士たちの絆 / 戰士們的牽絆 Page5  


### 第024话  忘れていたもの / 被遺忘的回憶 Page23  


### 第025话  蘇った過去 / 甦醒的過去 Page41  


### 第026话  惜しみない命 / 今生無憾 Page59  


### 第027话  訓練生時代の想い出 / 訓練生時代的回憶 Page77  


### 第028话  命にかえても / 即使再活一次 Page95  


### 第029话  李大人の制裁 / 李大人的制裁 Page113  


### 第030话  失われた名前 / 被遺忘的名字 Page131  


### 第031话  最後の決着 / 最後的解决 Page151  


### 第032话  爸爸 / 爸爸 Page169  


### 第033话  一年ぶりの衝撃 / 闊别一年的衝擊 Page187  


## Angel Heart (1st Season)  Vol04


### 第034话  退院 / 出院 Page5  


### 第035话  パパと娘の初デート / 兩父女的初次約會 Page23  


### 第036话  船上の出会いと別れ / 船上的邂逅與分離 Page41


### 第037话  影の爸爸 / 替身爸爸 Page61  


### 第038话  おかえり / 歡迎歸來 Page79  


### 第039话  李大人からの贈り物 / 李大人送贈的禮物 Page97  


### 第040话  親不孝 / 不孝 Page115  


### 第041话  陳老人の店 / 陳老人的店 Page133  


### 第042话  おしゃれ / 打扮 Page151  


### 第043话  新宿の洗礼 / 新宿的洗禮 Page169  


### 第044话  依頼人### 第一号 / ### 第一個委訌  Page187  


## Angel Heart (1st Season)  Vol05

### 第045话  殺し屋の習性 / 殺手的習性 Page5  


### 第046话  獠とベンジャミンと女社長 / 獠、賓明和女社長 Page23  


### 第047话  パパを捜して!! / 尋找爸爸 Page41  


### 第048话  パパの似顔絵 / 爸爸的肖像畫 Page59  


### 第049话  ターニャの本当の笑顔 / 塔妮雅真心的笑容 Page77


### 第059话  狙撃準備完了 / 狙擊準備完畢 Page95  


### 第051话  風船のクマさん / 派送氣球的熊叔叔 Page113  


### 第052话  香との会話 / 和香的對話 Page131  


### 第053话  冴子の誕生日 / 冴子的生日 Page149  


### 第054话  夢の中の出会い / 夢中的邂逅 Page167  


### 第055话  槇村兄妹 / 稹村兄妹 Page185  


## Angel Heart (1st Season)  Vol06  

### 第056话  二匹の野良犬 / 兩隻野狗 Page1  


### 第057话  野良犬の告白 / 野狗的告白 Page23  


### 第058话  親子の絆 / 親子間的牽絆 Page41  


### 第059话  C・Hが犯人！？ / 城市獵人是兇手！？ Page59   


### 第060话  心の傷 / 心底的傷痛 Page77  


### 第061话  孤独な少女 / 孤獨的少女 Page95 


### 第062话  媽媽の心臓 / 媽媽的心臓 Page113  


### 第063话  夢を守る！ / 守護小夢 Page131  


### 第064话  突きつけられた真実 / 突然而來的真相 Page149  


### 第065话  香瑩の決意 / 香瑩的决心  Page167  


### 第066话  天使の心 / 天使的心 Page185  


## Angel Heart (1st Season)  Vol07  

### 第067话  不公平な幸せ / 不公平的幸福 Page5  


### 第068话  優しい心音 / 温柔的心跳聲 Page23  


### 第069话  笑顔の二人 / 重展歡颜的二人 Page43  


### 第070话  香瑩の変化 / 香瑩的改變 Page61  


### 第071话  依頼は殺し！？ / 殺死委訌人!? Page79  


### 第072话  伝わらぬ思い / 没傳達的心意 Page97  


### 第073话  不器用な男 / 笨拙的男人 Page115  


### 第074话  別れの五円玉 / 臨别的五圓硬幣 Page133   


### 第075话  命より大切な絆 / 比生命還重要的關係 Page151  


### 第076话  もう一度あの頃に / 再次回到那個時候 Page169  


### 第077话  本当の家族 / 真正的家人 Page187  


## Angel Heart (1st Season)  Vol08  

### 第078话  遺された指輪 / 被保留下來的戒指 Page5  


### 第079话  冴子からのXYZ / 來自冴子的XYZ Page23  


### 第080话  ターゲットは香瑩 / 瞄準香莹 Page41  


### 第081话  心臓(かおり)の涙 / 心氤的眼涙 Page61  


### 第082话  危険な匂い / 危險的氣味 Page79  


### 第083话  哀しき連続殺人犯 / 悲哀的連環殺人犯 Page97  


### 第084话  悪魔のエンディング / 惡魔的下場 Page115  


### 第085话  愛に飢えた悪魔 / 渴望愛的惡魔 Page133  


### 第086话  思い出の場所 / 回憶中的場所 Page151  


### 第087话  唯一の愛情 / 唯一的愛情 Page169  


### 第088话  あたたかい銃弾 / 温暖的槍彈 Page187  


## Angel Heart (1st Season)  Vol09  

### 第089话  遅れて来た幸せ / 遲來的幸福 Page5  


### 第090话  初めての感情 / 最初的愛情 Page23  


### 第091话  本当の表情(かお) / 真實的表情 Page41  


### 第092话  これが、恋？ / 這是戀愛嗎? Page59  


### 第093话  計り知れぬ想い / 比戀愛更深的感情 Page77  


### 第094话  父親が娘を想う気持ち / 父親的愛女之心 Page95  


### 第095话  私、恋してます / 我在戀愛 Page115  


### 第096话  遠い約束 / 遥遠的約定 Page133  


### 第097话  幸せな笑顔 / 幸福的笑臉 Page151  


### 第098话  初恋、涙の別れ / 揮告别初戀 169  


### 第099话  恋する人の気持ち / 戀愛者的心情 Page187  


## Angel Heart (1st Season)  Vol10  

### 第100话  妹を捜して / 找尋妹妹 Page1  


### 第101话  妹は幸せだった？ / 妹妹過得幸福嗎？ Page23  


### 第102话  心臓(かおり)の反応 / 心臟的回應 Page41  


### 第103话  新宿の天使 / 新宿的天使 Page59  


### 第104话  奇跡の適合性 / 互相適應的奇躓 Page77  


### 第105话  思い出の街 / 充滿回憶的城市 Page95  


### 第106话  小さな願い / 小小心願 Page113  


### 第107话  槇村の決意 / /稹村的决心 Page131  


### 第108话  C・Hの正体 / 城市獵人的真面目 Page149  


### 第109话  獠からの依頼 / 獠的委託 Page167  


### 第110话  一途(バカ)な男 / 一心一意的勇人 Page185  


## Angel Heart (1st Season)  Vol11  

### 第111话  早百合の旅立ち / 早百合的人生新旅程  Page1  


### 第112话  依頼人はひったくり！？ / 委訌个是路劫者!? Page23  


### 第113话  ベイビートラブル！ / 茶煲婴兒！ Page41  


### 第114话  恋人同士の誤解 / 戀人之間的誤會 Page59  


### 第115话  聖夜の奇跡 / 平安夜的奇跡 Page77  


### 第116话  運命の再会 / 命运的重逢 Page95  


### 第117话  二人の変化 / 二人的变化 Page113  


### 第118话  白蘭の任務 / 白蘭的任务 Page131  


### 第119话  スコープの中の真実 / 视爵内的真意 Page149  


### 第120话  神から授かりし子 / 神賜予的宝贝 Page167  


### 第121话  初めての愛情 / 初次的愛情  Page185  


## Angel Heart (1st Season)  Vol12  

### 第122话  白蘭の決意 / 白蘭的决心 Page5  


### 第123话  悲しみの決行日 / 悲哀的執行日 Page23  


### 第124话  生きる糧 / 生存之精神食糧 Page41  


### 第125话  受け継がれし命 / 繼承生命 Page59  


### 第126话  白蘭からの手紙 / 白蘭的來信 Page77  


### 第127话  心臓(こころ)の声を信じて / 相信心臟之音 Page95  


### 第128话  心臓(こころ)の共振 / 心臟的共嗚 Page113  


### 第129话  姉の想い / 姐姐的思念 Page131  


### 第130话  綾音の嘘 / 綾音的謊言 Page149  


### 第131话  高畑の嘘 / 高灿的謊言 Page167  


### 第132话  明かされた真実 / 真相大白 Page185  


## Angel Heart (1st Season)  Vol13  

### 第133话  心臓移植のリスク / 心臟移植的風險 Page5  


### 第134话  生きて・・・！ / 活下去...! Page23  


### 第135话  生きた証 / 活過的證據 Page41  


### 第136话  高畑のお守り / 高知的護身符 Page59  


### 第137话  冴子と謎の女の子 / 冴子與神秘女孩 Page77  


### 第138话  都会の座敷童 / 都市的座敷童子 Page95  


### 第139话  冴子のお返し / 冴子的回憶 Page113  


### 第140话  穏やかな夢 / 温馨的夢 Page131  


### 第141话  海坊主と座敷童 / 海坊主與座敷童子 Page149  


### 第142话  汚れなき心 / 純潔的心靈 Page167  


### 第143话  ママへの想い / 對媽媽的思念 Page185  


## Angel Heart (1st Season)  Vol14  

### 第144话  いつも一緒 / 時刻在一起 Page5  


### 第145话  暗闇に見えた夕陽 / 黑暗中見到的夕陽 Page23  


### 第146话  優しき店長(マスター) / 温情店長 Page41  


### 第147话  危険な大女優！ / 危險的超級女明星 Page59  


### 第148话  ジョイの事情 / 喬伊的隱情 Page77  


### 第149话  思い人はこの街に？ / 想念的人在這個城市嗎? Page95  


### 第150话  愛しき人の形見 / 心愛的人的遺物 Page115  


### 第151话  共同生活開始！ / 共同生活開始! Page131  


### 第152话  ミキの幸せ / 未來的幸福 Page149  


### 第153话  親子のように / 情同母女 Page167  


### 第154话  ジョイの行き先 / 喬伊的去向 Page185  


## Angel Heart (1st Season)  Vol15  

### 第155话  告白・・・！ / 表白 Page5  

 
### 第156话  家族の写真 / 家人的照片 Page23  


### 第157话  獠のケータイライフ / 獠的手機生活 Page41  


### 第158话  愛される理由 / 被愛的理由 Page59  


### 第159话  ラブ　サ・イ・ン / 我在戀愛 Page77  


### 第160话  ホレた男は謎だらけ！？ / 恋上的男人充滿秘密！? Page95  


### 第161话  恋の覚悟 / 戀愛的覺悟 Page113  


### 第162话  獠の大失敗 / 獠的大失策 Page131  


### 第163话  女同士の酒 / 女人之間的酒 Page149  


### 第164话  それぞれの場所 / 各自的位置 Page167  


### 第165话  楊からの依頼？ / 楊的委托？ Page185  


## Angel Heart (1st Season)  Vol16  

### 第166话  愛は地球を救う！？ / 愛心救地球!?  Page5   


### 第167话  楊芳玉の仕事 / 楊芳玉的工作 Page23  


### 第168话  天国と地獄！？ / 天堂與地獄!? Page41  


### 第169话  黒幕現る!! / 幕後黑手出現!! Page59  


### 第170话  楊、始動！ / 楊，出動! Page77  


### 第171话  再会・・・！ / 再會...!! Page95  


### 第172话  母なる思い / 母親的關懷 Page113  


### 第173话  アカルイミライ！？ / 光明的未來!? Page131  


### 第174话  麗泉の悩み / 麗泉的煩惱 Page149  


### 第175话  恋人達の未来 / 戀人們的未來 Page167  


### 第176话  悲しい二人 / 悲傷的二人 Page185   


## Angel Heart (1st Season)  Vol17  

### 第177话  男の信念 / 男人的信念 Page5  


### 第178话  透視の代償 / 透视的代價 Page23  


### 第179话  笑顔の未来 / 未來裡的笑臉 Page41  


### 第180话  引き寄せられる運命 / 被牽引的命運 Page59  


### 第181话  未来を変える男 / 改變未來的男人 Page77  


### 第182话  信宏が教えてくれた事 / 信宏的教晦 Page95  


### 第183话  麗子の未来 / 麗子的未來 Page113  


### 第184话  義父(ちち)はつらいよ！ / 難為了養父! Page131  


### 第185话  海坊主と先生 / 海坊主與老師 Page149  


### 第186话  変質者は海坊主！？ / 海坊主是變態佬!? Page167  


### 第187话  ミキが行方不明！？ / 未來下落不明!?  Page185  


## Angel Heart (1st Season)  Vol18  

### 第188话  海学級へ行こう！ / 去海邊旅行吧！ Page5  


### 第189话  変質者、現る！ / 變態佬出現！ Page23  


### 第190话  バスジャック！ / 騎劫巴士！ Page41  


### 第191话  恐怖のカーチェイス！ / 恐怖的飛車追逐！ Page59  


### 第192话  親子の電話 / 父女的電話 Page77  


### 第193话  怒りの一撃！ / 極怒的一擊！ Page95  


### 第194话  親子の覚悟 / 父女的覺悟 Page113  


### 第195话  夏休みの終わり / 暑假告终 Page131  


### 第196话  楊、再来!! / 楊、再來!! Page149  


### 第197话  指輪の記憶 / 戒指的記憶 Page167  


### 第198话  クリスマスプレゼント / 聖誕禮物 Page185  


## Angel Heart (1st Season)  Vol19  

### 第199话  異国からの求婚者！ / 從巽國來的求婚者！ Page5   


### 第200话  妄想超特急がやってきた！ / 妄想超特急來了！ Page23  


### 第201话  皇子と香瑩の初デート / 王子和香瑩的### 第一次約會 Page43  


### 第202话  昨日の敵は今日の恋人！ / 昨日的敵人是今日的戀人！ Page61  


### 第203话  マオの交渉 / 和馬奥的交涉 Page79  


### 第204话  家族の風景 / 家族的風景 Page97  


### 第205话  日常に潜むアクマ / 潛藏在日常中的惡魔 Page115  


### 第206话  皇子の苦悩 / 話王子的苦惱 Page133  


### 第207话  酒と泪と皇子と母親！？ / 酒、涙、王子和母親 Page151  


### 第208话  海辺の告白！ / 海邊的表白！ Page169  


### 第209话  迸る想い！ / 迸發的思念！ Page187  


## Angel Heart (1st Season)  Vol20  

### 第210话  マオの決断 / 馬奥的斷 Page5


### 第211话  男二人、車中にて / 兩個男人在車内 Page23  


### 第212话  長生きの代償 / 長壽的代價 Page41  


### 第213话  男の性(サガ)！ / 男人的本性！ Page59  


### 第214话  オペレーション　ナシクズシ！ / 漸進式戰術! Page77  


### 第215话  C・H資格テスト！ / C・H資格測試! Page93  


### 第216话  追う女、待つ女 / 追求的女人、等待的女人 Page113  


### 第217话  楊の生きる理由 / 楊的生存理由 Page131  


### 第218话  大幸運期到来!! / 大幸運期到來!! Page149  


### 第219话  私がパパです！  / 我是爸爸! Page167  


### 第220话  謎のお父さん！ / 神秘的爸爸! Page185  



## Angel Heart (1st Season)  Vol21  

### 第221话  歳はシゲキがお好き！？ / 18歲喜歡刺激! Page5  


### 第222话  カギは香瑩にあり！？ / 關鍵在香瑩那裎!? Page23  


### 第223话  紗世は名探偵！？ / 紗世是名偵探 Page41  


### 第224话  重ねる嘘、重なる罪 / 重叠的謊言、重叠的罪孽 Page59  


### 第225话  父のいる店 / 有爸爸在的店 Page77  


### 第226话  真実と紗世 / 真相與紗世 Page95  


### 第227话  香瑩、入学す！ / 香瑩入學！ Page113  


### 第228话  私達の学校 / 我們的學校 Page131  


### 第229话  想い、街に息づいて / 思念、在城市中嘆息 Page149  


### 第230话  ラブレター・パニック！ / 情信大恐慌! Page167  


### 第231话  僕ら、シャンイン親衛隊！ / 我們是香瑩親衛隊 Page185  


## Angel Heart (1st Season)  Vol22  

### 第232话  机男と親衛隊！ / 書桌男與親衛隊! Page5  


### 第233话  嫌な予感 / 不祥的預感 Page23  


### 第234话  いい子の行き着く場所 / 好孩子所去的地方 Page41  


### 第235话  肉弾戦 / 肉搏戰 Page61  


### 第236话  危険な出会い / 危險的相遇 Page79  


### 第237话  変色尤(カメレオン) / 變色龍 Page97  


### 第238话  花園学校の未来 / 花園學校的未來 Page115  


### 第239话  乙玲(イーリン)の姉 / 乙玲的姊姊 Page133  


### 第240话  変色尤(カメレオン)の誘惑 / 變色龍的誘盛 Page151  


### 第241话  狙撃 / 狙擊 Page169  


### 第242话  香瑩の答え / 香瑩的答案 Page187  


## Angel Heart (1st Season)  Vol23  

### 第243话  覚悟 / 覚悟 Page5  


### 第244话  恋はアフターで！ / 戀愛從陪客外出開始 Page23  


### 第245话  初アフターはストーカー付！ / 首次陪客外出遇上跟狂! Page41  


### 第246话  雲のように / 像雲一樣 Page61  


### 第247话  悲痛な願い / 悲痛的願望 Page59  


### 第248话  気づかぬ優しさ / 没察覺的温柔 Page77  


### 第249话  見えない景色 / 看不見的景色 Page95  


### 第250话  暖かい場所へ / 往温暖的地方 Page131  


### 第251话  意外な弱点 / 意外的缺點 Page149  


### 第252话  初めての経験 / 初次的經驗 Page167  


### 第253话  表への一歩 / 踏出「外面」的一步 Page185  


## Angel Heart (1st Season)  Vol24  

### 第254话  嫉妬 / 嫉妒 Page5  


### 第255话  仲間 / 伙伴 Page23  


### 第256话  新スポンサー / 新的贊助人 Page41  


### 第257话  陳さんの暴走 / 失控的陳先生 Page59  


### 第258话  及### 第点の答え / 合格的答案 Page77  


### 第259话  香瑩の怒り / 香瑩的憤怒 Page95  


### 第260话  消えない記憶 / 無法消失的記憶 Page113  


### 第261话  武道館決戦 / 武道館决戰 Page131  


### 第262话  仁志の涙 / 仁志的眼淤 Page149  


### 第263话  私の立ち位置 / 我站立的位置 Page167  


### 第264话  夏の再会 / 夏之再會 Page185  



## Angel Heart (1st Season)  Vol25

### 第265话  海坊主と葉月 / 海坊主與葉月 Page5 


### 第266话  誤解 / 誤解 Page23  


### 第267话  葉月の父 / 葉月的父親 Page41  


### 第268话  ファルコンのお守り / 隼人的護身符 Page59  


### 第269话  墓参り / 掃墓 Page77  


### 第270话  ファルコンの秘密 / 隼人的秘密 Page95  


### 第271话  キバナコスモス / 黄波斯菊 Page113  


### 第272话  大好きなんです / 最喜歡 Page131  


### 第273话  親子の愛 / 父女之愛 Page149  


### 第274话  葉月の悩み / 葉月的煩惱 Page167  


### 第275话  カメレオンの罠 / 變色龍的陷阱 Page185  



## Angel Heart (1st Season)  Vol26

### 第276话  招かれざる客 / 招惹不得的客人 Page5  


### 第277话  虎の尾 / 老虎尾巴 Page 23  


### 第278话  最凶悪危険人物 / 最兕狠的危險人物 Page41  


### 第279话  一億の勝負 / 一億的賭局 Page59  


### 第280话  母の想い / 母親的心情 Page77  


### 第281话  ぷるん / 軟綿綿 Page95  


### 第282话  カメレオン動く / 變色龍行動 Page113  


### 第283话  灯 / 燈 Page131  


### 第284话  ジンクス / 幸運徵兆 Page149  


### 第285话  不安な夜 / 不安之夜 Page167  


### 第286话  潮騒 / 潮騷 Page185  



## Angel Heart (1st Season)  Vol27

### 第287话  小さなXYZ / 小小的XYZ Page5  


### 第288话  家族のために / 為了家人 Page23  


### 第289话  ロスタイム / 補時 Page41  


### 第290话  損な役回り / 吃虧的差事 Page59  


### 第291话  罪悪感 / 内疚 Page77  


### 第292话  笑い泣き / 笑中有涙 Page95  


### 第293话  傲慢 / 傲慢 Page113  


### 第294话  懺悔と供養 / 懺悔與供養 Page131  


### 第295话  最期の言葉 / 臨終的話 Page149  


### 第296话  本当の姿 / 真正的樣子 Page167  


### 第297话  親子の時間 / 親子時間 Page185  



## Angel Heart (1st Season)  Vol28

### 第298话  幸せの涙 / 幸福的眼涙 Page5  


### 第299话  陰影 / 陰影 Page23  


### 第300话  恋の季節 / 戀之季節 Page41  


### 第301话  潜入 / 潜入 Page59  


### 第302话  身代わり / 替身 Page77


### 第303话  金木犀の香り / 丹桂的香味 Page94  


### 第304话  似た者同士 / 相似的兩人 Page113  


### 第305话  確かな鼓動 / 確切的鼓動 Page131   


### 第306话  幸せのベール / 幸福的頭紗 Page149


### 第307话  誓いの言葉 / 誓言 Page167  


### 第308话  新婚旅行 / 新婚旅行 Page185  



## Angel Heart (1st Season)  Vol28
### 第309话  離れていても / 即使分離 Page5  


### 第310话  真実(マコト) / 真實 Page23  


### 第311话  宣戦布告 / 宣戰通告 Page41  


### 第312话  影武者 / 影武者 Page59  


### 第313话  長い一日 / 漫長的一天 Page77  


### 第314话  怪我の功名 / 歪打正著 Page95  


### 第315话  仕組まれた黙秘 / 被逼緘默 Page113  


### 第316话  冴子の覚悟 / 冴子的覺悟 Page131  


### 第317话  現行犯逮捕 / 逮捕現行犯 Page149  


### 第318话  現場復帰 / 歸位 Page167  


### 第319话  黒幕 / 黑幕 Page185  


## Angel Heart (1st Season)  Vol29
### 第320话  制裁の行方 / 制裁的方向 Page5  


### 第321话  激昂 / 激昂 Page23  


### 第322话  カメレオンの遺産 / 變色龍的遺産 Page41  


### 第323话  怪訝なXYZ / 怪巽的XYZ Page59  


### 第324话  笑顔の理由 / 笑容的理由 Page77  


### 第325话  老人(かれ)の貴物(おもいで) / 老人的寶物 Page95  


### 第326话  対極の男 / 兩極的男人 Page113  


### 第327话  昼行灯 / 戇男 Page131  


### 第328话  敵情視察 / 视察敵情 Page149  


### 第329话  潜入！CH / 潜入！CH Page167  


### 第330话  人質交換 / 交換人質 Page185  



## Angel Heart (1st Season)  Vol30
### 第331话  只今参上！ / 馬上駕到！ Page5  


### 第332话  君だけのヒーロー / 只屬於你的英雄 Page23  


### 第333话  いつかの笑顔 / 似曾相識的笑容 Page41  


### 第334话  天使のサイモン / 天使西蒙 Page59  


### 第335话  カエルの子はカエル / 青蛙的女兒也是青蛙 Page77  


### 第336话  ふたつの命 / 兩條生命 Page95  


### 第337话  ちょっとの特別 / 有一點特别 Page113  


### 第338话  空のC・H / 天空的C・H Page131  


### 第339话  繋いだ手から / 牽著的手 Page149  


### 第340话  小さな依頼人 / 小小的委訌人 Page167


### 第341话  涙の理由 / 流涙的理由 Page185  



## Angel Heart (1st Season)  Vol31
### 第342话  偽りの再会 / 僞裝的重逢 Page5  


### 第343话  会いたい / 想見面 Page23  


### 第344话  ママを捜して・・・ / 找媽媽... Page41  


### 第345话  忘却の代償 / 忘御的代價 Page59  


### 第346话  嘘だと言って・・・ / 説是謊言 Page77  


### 第347话  優しい嘘 / 善意的謊言 Page95  


### 第348话  たくさんの月 / 有很多月亮 Page113  


### 第349话  希望の風 / 希望之風 Page131  


### 第350话  陳さんの策謀 / 陳先生的計謀 Page149  


### 第351话  薔薇と御曹司 / 玫瑰與貴公子 Page167  


### 第352话  巡る想い / 笑容的理由 Page185  


## Angel Heart (1st Season)  Vol31
### 第353话  恋の吊り橋効果 / 戀愛的吊橋 Page5  


### 第354话  今日限りの自由 / 只限今天的自由 Page23  


### 第355话  受け止める覚悟 / 理解的覺悟 Page41  


### 第356话  生きる目的 / 生存的目的 Page59  


### 第357话  陳老人暗躍 / 老陳的暗中活躍 Page77  


### 第358话  ギフト / 天賦 Page95  


### 第359话  愛の選択 / 愛的選擇 Page113  


### 第360话  父と息子 / 父與子 Page131  


### 第361话  二人分のギフト / 兩人的天賦 Page149  


### 第362话  乱暴な天使 / 莽撞的天使 Page167  


### 第363话  恋の行方 / 戀愛的去向 Page185  





