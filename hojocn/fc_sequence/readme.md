

# FC的分镜  （[hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96856)）    

本文借用电影构图(Cinematic Composition)分析漫画的分镜/分格(panel)。原因是电影构图兼具"摄影构图"和"叙事功能(传递故事情节相关信息、带动观众情绪)"。  

## 符号说明  
- "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)   
- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  


## [术语](./terms.md)    



----------------------------------------------------------

由于本文很长，所以拆为多个部分。  

目录： 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

----------------------------------------------------------
## 待整理

----------------------------------------------------------


## 参考资料  

[1] The Filmmakers Eye - Learning (and Breaking) the Rules of Cinematic Composition, 2010, Gustavo Mercado.  

[2] The Filmmakers Eye - the Language of the Lens, 2019, Gustavo Mercado. 

[3] Master Shots, 2009, Kenworthy Christopher.  

[4] Master Shots Vol2, 2011, Kenworthy Christopher.  

[5] Master Shots Vol3, 2013, Kenworthy Christopher.   
 
[6] Exploring Storyboarding, Wendy Tumminello, 中译版, 第35页.  

[7] Framed Ink, 2010, Marcos Mateu-Mestre,  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



