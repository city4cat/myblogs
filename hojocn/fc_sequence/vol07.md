
# FC的分镜

目录： 
[readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

--------------------------------------------------


## Vol07	CH043	page003		愿当养子  	Become My Son!

### 07_011,  07_012：  
紫苑抱怨爷爷，引出下一个镜头的爷爷。  
![](img/07_011.jpg)， 
![](img/07_012.jpg)  

### 07_014：  
两人对视时，分镜的视线也相对(在一条直线上)：  
![](img/07_014_1_2__mod.jpg)  
07_022两人没有对视时，分镜的视线也不相对(在一条直线上)：  
![](img/07_022_5_6__mod.jpg)  

### 07_016_4：  
场景里紫的面部涂黑(镜头右下方)，左侧显示紫的特写镜头。面部涂黑可能是暗示人物心情沉重（爷爷要收养雅彦一事）。    
![](img/07_016_4__.jpg)， 
![](img/07_016_4__crop0.jpg) 

### 07_027_5：  
用"左侧墙壁上雅彦的影子"表示雅彦偷听到了右侧两人的谈话：  
![](img/07_027_5.jpg)  

### 07_028_5：  
镜头有畸变，反映人物内心活动：  
![](img/07_028_5__.jpg)  


----------------------------------------------------------

## Vol07	CH044	page031		婚礼的秘密对策  	A Super Secret Plan  		  
### 07_041, 07_043:  
顺子的婚宴上，雅美出场时多个镜头推迟显示雅美的面目，显得神秘，也在暗示紫苑的计划很特别：  
![](img/07_041_4__.jpg) 
![](img/07_043_0__.jpg) 
![](img/07_043_1__.jpg) 
![](img/07_043_4__.jpg) 
![](img/07_043_6__.jpg)  

### 07_057_1，07_057_2，07_057_3，07_057_4(角色情感变化带有较长的镜头序列片段):  
镜头从远到近推进至角色：  
![](img/07_057_1.jpg) 
![](img/07_057_2.jpg) 
![](img/07_057_3.jpg) 
![](img/07_057_4.jpg)  


----------------------------------------------------------

## Vol07	CH045	page061		钟乳洞内的危险幽香  	The Smell Of Danger From Limestone Cave  

### 07_081:  
用水滴滴落来衔接镜头的切换。  
![](./img/07_081_3__.jpg) 
![](./img/07_081_4__.jpg)

### 07_082, 07_083:  
对于处于暗处的镜头(人物不着色)，这里用阴影遮盖某个人物，可能是为了突出另一个人物（的心理活动）：  
![](./img/07_082_3__.jpg) 
![](./img/07_082_7__.jpg) 
![](./img/07_083_1__.jpg) 
![](./img/07_083_2__.jpg)  
再后来07_084，甚至把紫苑完全涂黑了：  
![](./img/07_084_en.jpg)   
这几个镜头把紫苑完全涂黑。我猜，一方面可能为了更突出雅彦(的心理活动)，毕竟熄灯后的确是看不到对方的；另一方面，可能是为了对比后续溶洞里来电的镜头：  
![](./img/07_084_2__.jpg) 
![](./img/07_084_3__.jpg) 


----------------------------------------------------------

## Vol07	CH046	page089		恶梦般的露天温泉  	The Nightmare Otherdoor Bath  		  
### 07_110_0:  
雅彦的私处和齐藤西的眼部在同一位置，暗示齐藤西看到了雅彦的私处。构图巧妙。  
![](img/07_110_0__.jpg)  

### 07_127_0，07_127_1:  
叹气，紧接着惊讶：  
![](img/07_127_0__.jpg) 
![](img/07_127_1__.jpg)  

### 07_091, 07_092:  
有两个关联的镜头（蓝框所示）被分来了，而且是放在不同的页上。把这两镜头放在一起，对比一下效果。    
![](./img/07_091_092__en.jpg)， 
![](./img/07_091_6__.jpg) 
![](./img/07_092_0__.jpg)， 
![](./img/07_092_0__.jpg) 
![](./img/07_091_6__.jpg)  


### 07_096  
雅彦鼻尖差那么一点也不让破格。和07_058顺子这张图对比。[疑问]都是鼻尖差那么一点，为什么一个不破格、另一个破格？  
![](./img/07_096_1__.jpg)， 
![](./img/07_058_4__.jpg) 



----------------------------------------------------------

## Vol07	CH047	page115		两个雅彦  	The Two Masahikos  		  

### 07_122
汗滴破格.  
![](./img/07_122_5__.jpg)  

### 07_128_5:  
前中背景分明：    
前景：恶人、加阴影、表示在暗处或内心有邪念；  
中景：雅美；  
背景：路人被简化、楼；  
![](img/07_128_5__.jpg)  

### 07_125:  
一个画面被分为两个镜头。上下分割表示镜头向下摇：  
![](./img/07_125_1__.jpg)  

### 07_131:  
熏要拉雅美逃走。再拉手跑之前，先给熏一个镜头，我猜，这样镜头衔接比较自然，不突兀。  
![](./img/07_131_6__.jpg) 
![](./img/07_131_7__.jpg)  

### 07_134
汗滴破格   
![](./img/07_134_2__.jpg)  

### 07_137:  
把熏的手涂上阴影，[疑问]是否暗示这动作里有诡计？（后续熏借此强吻雅美）  
![](./img/07_137_7__.jpg)


----------------------------------------------------------

## Vol07	CH048	page143		"雅彦"的真正身份  	The Identity Of Masahiko   		  
### 07_158_0:  
打手的凶残和面无表情形成对比：  
![](img/07_158_0__.jpg) 
![](img/07_158_0__crop0.jpg) 

----------------------------------------------------------

## Vol07	CH049	page171		两父女  	Father And Child  		  
### 07_162_0，07_162_1:  
前一个镜头熏说带雅美走，后一个镜头已经到达目的地。很简洁。  
![](img/07_162_0__.jpg) 
![](img/07_162_1__.jpg)  

### 07_189_0, 07_189_1:  
雅美倒头便睡的镜头：  
![](img/07_189_0__.jpg) 
![](img/07_189_1__.jpg)  

### 07_191:  
蓝色框里的对白是雅彦的，但这时背景已经切换到熏的场景了。  
![](./img/07_191__cn.jpg)  
（电影电视里经常见到这种情况。我之前一直以为这是由于剪辑时声音和画面没有完全同步而导致的瑕疵）  

### 07_197:  
熏和辰巳的又一起冲突。老北没有直接画熏被打的画面（而是间接地画了熏倒地的画面）。  
![](./img/07_197_1__.jpg) 
![](./img/07_197_2__.jpg) 



----------------------------------------------------------

----------------------------------------------------------

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
