
## 术语    
- 关于镜头种类的命名，不同的书似乎不一致。例如下图。该图源自[6, p35]。图中右侧附上的是[1]里的镜头命名。镜头命名差异主要是Close Up, Medium Close Up, Medium Shot。本文采用[1]里的命名。  
![](img/shot_naming_illustration.jpg)  
- Macro Shot(微距镜头)[1, p119]：  
    比Extreme Close Up(E.C.U.)更放大物体、表现力度更大。例如，在角色A的瞳孔里看到角色B的肖像。  
- Extreme Close Up(E.C.U.)(极特写镜头/大特写镜头/极近距镜头)[1, p29]：  
    显示角色局部的小细节。让人觉得所显示的事物很重要或有某种意义。  
- Close Up(C.U.)(特写镜头/近距镜头)[1, p35]：  
    显示角色面部。能让人体会到角色的情感；可以预示某个重要时刻；或为了看清楚太小的事物。p37，为了不分散读者对于前景事物的注意力，所以让背景物虚化。headroom为负(眼睛高于上1/3)的构图，会有拥挤、不舒适的感觉。
- Medium Close Up(M.C.U.)(中距特写镜头/中近景镜头)[1, p41]：  
    显示从肩膀/胸到头顶。除了C.U.的特点之外，还能借助肩膀/胸传达部分肢体语言。因只能显示很小一部分背景，所以和角色的联系(比其他镜头)更紧密。  
- Medium Shot(中景镜头)[1, p47]：  
    显示角色从腰到头顶。  
- Medium Long Shot(中远景镜头)[1, p53]：  
    显示角色从膝盖到头顶。
- Long Shot(远景镜头)[1, p59]。  
    显示角色全身。  
- Extreme Long Shot(极远景镜头)[1,p65]。  
    强调环境。角色很小或不显示角色。  
- Over The Shoulder(O.T.S.)(过肩镜头)[1,p71]：  
    常用于在两个角色之间切换(比如，谈话)，该镜头常常成对出现。可以越过身体的其他部位(不限于肩膀)。面对镜头的角色，他的视线越直视镜头(视线与相机方向的夹角约小)，观众的代入感越强。
- Establishing Shot(E.S.)(定场镜头)[1,p77]：  
    展现情节发展的地点，可位于开始和结尾。多数情况下，其后紧跟的场景是和剧情联系更紧密的场景。E.S.也可紧随角色之后，去揭示某些信息。E.S.必须传递一些关于地点的信息，例如基调、和角色的关系、主题。  
- Subjective Shot(主观镜头)[1, p83]：  
    站在角色的角度去看剧情的发展，体会角色情感或身体上的感受。其他角色可以通过这种方式(例如，直视镜头)直接与观众互动。  
-  Two Shot(双人镜头)[1, p89]：  
    常见于对话场景.
- Group Shot(群体镜头)[1, p95]：  
    可用于表现角色之间的关系、角色和其环境的关系。该镜头一般放在开始，告诉观众角色之间的位置。若镜头里的角色按纵深排列，则可以表现画面的深度层次。也可表现特殊时刻的象征意义。  
- Canted Shot(倾斜镜头)[1, p101]：  
    可表现戏剧性的紧张感、心理上的不安定、迷惑、疯狂、药物致幻。也可表现角色的头脑不正常、群体的压抑、所处环境的不自然或不正常。倾斜角度约大(最大45度)，表示越不正常；倾斜很小的角度就能表现处不稳定。因为该镜头很引人注目，所以一般只用于局部场景，如果倾角不大的话，也可用于整个场景。不能使用太多。
- Emblematic Shot(寓意镜头)[1,p107]：  
    可出现在影片开始，奠定片子的基调；也可出现在结尾，为剧情作注解。也可出现在接近结局，告诉观众快要结尾了。
- Abstract Shot(抽象镜头)[1, p113]：  
    可赋予剧情额外的意义；可作为场景表演、角色意图的注解；有助于重现视觉主题。  
- Zoom Shot(变焦镜头)[1,p125]：
    让观众觉得镜头里的事物在靠近或远离。突然放缩镜头，会产生紧急、紧张、危险的感觉。总体上会让观众产生身临其境的感受。
- Pan Shot(横摇镜头)[1,p131]：  
    因为平移过程中不间断，所以让观众感到场景(或表演)的连续和完整；其间观众的情绪会一直加强(不被打断)。
- Tilt Shot(纵摇镜头)。可用作E.S.(定场镜头)[1,p137]：  
    可表现实时、空间、表演的完整，可让镜头富有叙事意义。
- Dolly Shot(推拉镜头)[1,p143]：  
    可用于揭示、掩盖、注解一个动作或情景。因有这种镜头有强烈的视觉和叙事效果，所以不能滥用该镜头，只为让观众强烈感受角色或场景时才使用。Dolly In(向前平移/拉近镜头)意味着有所发现或处于重要时刻。使得紧张、悬疑、戏剧性时刻实时地逐渐展开，使镜头具有静止镜头所不具备的叙事功能。Dolly Out(向后平移/拉远镜头)，常表现角色失去信心或力量、加剧孤独感或绝望。可揭示角色所经历的一个重要时刻。该镜头和Tracking Shot(跟随镜头)的区别在于，它不一定跟随物体而运动。  
- Dolly Zoom Shot(滑动变焦镜头)(也被称为“counter zoom”, “contra zoom", "trombone shot", "zolly", “Vertigo effect” shot）[1,p149]：  
    保持对象基本不变，但背景(的透视)变化, 显得背景拉近或拉远。因该镜头效果极为明显和不安，所以只应用在极为特殊意义的情况。可表现角色突然惊讶、突然意识到某事物、表现角色及其强烈的情感(例如：愤怒、沉迷、坠入爱河、妄想症、恐惧、药物至幻等)。背景快速变化时，表现强烈的情感；背景缓慢变化时，可意味着重要的事情要发生。但把焦点放在背景而不是角色上时，从而表现角色被周围事物包裹着的效果(因角色心情或(恐怖片里的)超自然力量所致)。  
- Tracking Shot(跟随镜头)[1,p155]：  
    跟随物体而运动。它和Dolly Shot的区别在于，后者不一定跟随物体而运动。反向跟随镜头能让观众更强烈地融入角色。  
- Steadicam Shot(斯坦尼康镜头/相机稳定器镜头)[1,p161]：  
    保持时间、空间、表演的完整性。代入感强，使观众身临其境。能产生紧张感、反预期。  
- Crane Shot(升降镜头/吊臂镜头)[1,p167]：  
    可在开场时逐渐展现场面和环境的宏大，即作为establishging shot(定场镜头)；也可从大场景逐渐缩小至角色。用于引入场景或角色时，可表现该事物有特殊的意义。  
- Sequence Shot(序列长镜头/段落镜头)[1,p173]：  
    就是常说的"长镜头"。展现对后续影片至关重要的一些事件。  