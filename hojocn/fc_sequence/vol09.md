
# FC的分镜

目录： 
[readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

--------------------------------------------------


## Vol09	CH057	page003		二人世界的生日  	Their Birthday  	

### 09_011:  
把一个画面分隔为两个镜头:  
![](./img/09_011_4_5__.jpg)
  
### 09_015_0，09_015_1:  
（江岛带叶子去酒店，被熏撞见并被揍了一顿）用手衔接两个镜头。  
![](img/09_015_0__.jpg) 
![](img/09_015_1__.jpg)  

### 09_027_3，09_027_4，09_027_5:  
(第二天，叶子在雅彦的卧室醒来)雅彦送叶子生日礼物，叶子接手。  
![](img/09_027_3__.jpg) 
![](img/09_027_4__.jpg) 
![](img/09_027_5__.jpg)  

<a name="09_029_5"></a>  

### 09_029_5  
之前，和雅彦的关系没有进展，叶子因此被同学说三道四。为此叶子有心结。[08_147_2](./vol08.md#08_147_2)镜头里叶子头顶高耸的云朵暗示叶子内心的不悦。  
![](./img/08_147_0_2__cn.jpg)  

这里，叶子与雅彦关系和好。09_029_5呼应上图。同时，叶子头顶的云朵消散，这暗示叶子内心释然。  
![](./img/09_029_3_5__cn.jpg)  

----------------------------------------------------------

## Vol09	CH058	page031		网上情人  	Mail Lover  	
### 09_045:  
紫苑在楼下等浩美。紫苑抬头看楼上，然后镜头转到楼上。这种镜头转换的方式很多见。  
![](./img/09_045_4_7__cn.jpg)  
之所以把古屋公弘的大门的镜头塞在本页的最后，我猜，这是因为下一页不适合放置该镜头。因为下一页的背景边框是黑色（通常背景边框是白色）。黑色背景这是为了表现整个房间很压抑，以及浩美进屋看到这一切后很震惊。  
![](./img/09_046__cn.jpg)  

### 09_049:  
有一个镜头层叠关系复杂。  
![](./img/09_049_1_5__cn.jpg)  

### 09_053_2:  
浩美伤心地离开古屋公弘的住处。这个Canted Shot(倾斜镜头)感觉很好.  
![](./img/09_053_2__.jpg)  
注：《The Filmmakers Eye - Learning (and Breaking) the Rules of Cinematic Composition》, 2010, Gustavo Mercado, p101, Canted Shot(倾斜镜头)。可表现戏剧性的紧张感、心理上的不安定、迷惑、疯狂、药物致幻。也可表现角色的头脑不正常、群体的压抑、所处环境的不自然或不正常。倾斜角度约大(最大45度)，表示越不正常；倾斜很小的角度就能表现处不稳定。因为该镜头很引人注目，所以一般只用于局部场景，如果倾角不大的话，也可用于整个场景。不能使用太多。  

### 09_049_1:  
左侧明亮对比右侧阴暗。（镜头倾斜暗示人物内心的情绪）  
![](img/09_049_1__.jpg)  
09_051_0，纯黑阴影和白色的阳光形成更强的反差，辅助情节表达人物的内心情绪升级。  
![](img/09_051_0__.jpg)  

### 09_053_3:  
浩美伤心地跑出网恋情人的家，紫苑看到后很惊讶。为人物加阴影可能是为了突显这种心情。  
![](img/09_053_3__.jpg)  


### 09_053_5，09_053_6:  
似乎在用角色的头发衔接两个镜头。  
![](img/09_053_5_6__.jpg)  

### 09_054_0:  
画面暗、人物大面积纯黑阴影，表示人物心情低落。人物比背景的阴影更重，可能突显了这种心情。  
![](img/09_054_0__.jpg)  

### 09_054_5:  
前景人物面部加阴影(该阴影的确由屋顶的光源产生)，和背景人物区分开来。  
![](img/09_054_5__.jpg)  


----------------------------------------------------------

## Vol09	CH059	page059		战胜劲敌  	Victory Pitch  		  
### 09_062_3:  
一般来说，把(远景)人物**眼部**涂黑，可表示其心理活动。这里把人物整个**面部**涂黑了。  
![](img/09_062_3__.jpg)  

### 09_070_1:  
两个角色头部的连线平行于背景里的电线。  
![](img/09_070_1__.jpg)  

### 09_070_2:  
用如下镜头进入回忆。  
![](img/09_070_2__.jpg) 

### 09_071_6:  
棒球手松下敏史向雅彦诉说伤心的回忆（他和紫苑之间的往事）。人物用黑色表示心情低落，这和明亮的背景形成反差。  
![](img/09_071_6__.jpg) 

### 09_072
本话是紫苑和棒球手松下敏史的故事。  
松下回忆里，他和紫苑在初中毕业前的那次比赛，紫苑穿校服。  
![](./img/09_072_3__.jpg)  

### 09_073_5，09_074_0:  
用棒球的上天、落地来衔接镜头，退出回忆。  
紫苑挡回了松下的球，这是回忆。  
![](./img/09_073_5__.jpg)  
球落地时，镜头从回忆里拉回现实。用球来衔接镜头、衔接回忆和现实，很巧妙：  
![](./img/09_074_0__.jpg)  

### 09_074_3:  
棒球手松下敏史面部加阴影，表示内心的情绪(低落)；这和雅彦面部明亮呈对比。同时这种对比体现出前景和背景。  
![](img/09_074_3__.jpg)  

### 09_078:  
比赛的这些镜头都是矩形边框：  
![](./img/09_078__cn.jpg)  

### 09_079_4:  
前景紫苑加阴影，背景雅彦未加阴影，这有助于区别前景和背景。紫苑的阴影(眼部无阴影)也表现她有心理活动。    
![](img/09_079_4__.jpg) 


### 09_083_2:  
（松下问：“紫苑...我...当了职业球员后，你会来为我打气吗！？”，紫苑：“那当然会来，我们曾是队友嘛！！”）  
紫苑加阴影，且有辉光。加阴影，可能是因为人物逆光，也可能表示她此时有心理活动。  
![](img/09_083_2__.jpg) 


----------------------------------------------------------

## Vol09	CH060	page085		辰已的心愿  	Tatsumi's Wish  		  
### 09_085_7，09_086_0:  
用打火机的光亮照亮阴暗处的人物，引出人物。  
![](img/09_085_7__.jpg) 
![](img/09_086_0__.jpg) 

### 09_094_3:  
若苗紫在为辰巳缝补裤子。其手部的虚线是茶的热蒸汽效果。（用茶的热气衔接两个镜头）    
![](./img/09_094_3__.jpg) 
![](./img/09_094_2__.jpg) 

### 09_099_5:  
没有显示熏的面部，反而更能突出他的心理活动。  
![](img/09_099_5__.jpg) 

### 09_100:  
这几个镜头，按照阅读顺序是从右至左。前一个镜头左边的人物，是后一个镜头右边的人物。这几个镜头连起来看就是"用人物来衔接镜头"。  
![](./img/09_100_6__.jpg)
![](./img/09_100_5__.jpg)
![](./img/09_100_4__.jpg)


### 09_105:  
前一个镜头出现了紫在打电话，后一个镜头即使紫的脸被遮住，也不影响。读者也知道这人是紫。  
![](./img/09_105_5__.jpg) 
![](./img/09_105_6__.jpg)  

### 09_106_1，09_106_2:  
连续两个镜头，背景由白变黑，形成反差，表示人物内心活动。  
![](img/09_106_1__.jpg) 
![](img/09_106_2__.jpg) 

### 09_106， 09_107:  
这两个连贯镜头被放在了不同页面上。  
![](./img/09_106_6__.jpg) 
![](./img/09_107_0__.jpg)  


### 09_110_5:  
辰巳震惊于若苗紫的秘密。用这种构图表示想到了某人。  
![](img/09_110_5__.jpg)  


----------------------------------------------------------

## Vol09	CH061	page111		辰已的表白  	Tatsumi's Confession  		
### 09_113_0:  
辰巳得知若苗紫的秘密后非常惊讶、失落，这里也把他的眼部涂黑来表示他的心情(心理活动?)。  
![](img/09_113_0__.jpg) 

### 09_122_7:  
用开门的方式引出角色。这种效果可能被门框内外的黑白对比所增强。  
![](img/09_122_7__.jpg) 

### 09_128_2:  
镜头倾斜，表示人物内心不平静（紧张）。  
![](img/09_128_2__.jpg) 

### 09_132_1:  
黑白高对比度，辅助表达紧张感。（背景显示镜头可能有畸变；也可能是车窗本来就是弯曲的）  
![](img/09_132_1__.jpg) 


----------------------------------------------------------

## Vol09	CH062	page139		母亲的印象  	Mother's Image  		

### 09_141:  
为了后续回忆的镜头由早纪过渡到熏。这里先给熏一个镜头，使得后续过渡的镜头不突兀：  
![](./img/09_141_3_5__cn.jpg)  
镜头从早纪的回忆过渡到熏的噩梦。从下面这个镜头开始过渡。这个镜头里，上面的台词是早纪的，下面的台词是熏的梦境：  
![](./img/09_144_2__.jpg) 
![](./img/09_144_2__crop0.jpg)  
小时候的熏最初很乖，但还是被早纪冷落。用这个反差营造读者的情绪：  
![](./img/09_144_6__cn.jpg)  
这是熏的噩梦，所以镜头的边框(背景)都是黑色，表示压抑：  
![](./img/09_146__cn.jpg)  
梦里，熏偷东西。这里不知道偷东西的原因。是饿？还是为了心理上得到关注？（从后续看，应该是后者）。  
熏从噩梦中惊醒。这个旋转镜头很赞！：  
![](./img/09_147_0_3__cn.jpg)  
我觉得，虽然早纪这个人物不讨人喜欢，但作者这些剧情和镜头很精彩。  
  
### 09_143_1，09_143_2，09_143_3:  
先出现泪滴，然后是辰巳惊讶，最后显示流泪的人是早纪。  
![](img/09_143_1__.jpg) 
![](img/09_143_2__.jpg) 
![](img/09_143_3__.jpg) 

### 09_145_0:  
门比年幼的熏高大，可能是暗示熏的弱小和无助。  
![](img/09_145_0__.jpg) 

### 09_145_2:  
熏和早纪的面部阴影。画面里的熏给我一种恐怖感，不知为何([疑问])。  
![](img/09_145_2__.jpg) 
![](img/09_145_2__crop0.jpg) 

### 09_146_2, 09_146_3:  
熏的梦境，背景是黑色，辅助表达内心不愉快（熏听到母亲的消息后，内心惊恐、焦虑）。    
![](img/09_146_2__.jpg) 
![](img/09_146_3__.jpg) 

### 09_146_2，09_146_3，09_147_0，09_152_2，09_154_0，09_155_0:  
多个倾斜镜头，暗示人物（熏）的内心不平静。  
![](img/09_146_2__.jpg) 
![](img/09_146_3__.jpg) 
![](img/09_147_0__.jpg) 
![](img/09_152_2__.jpg) 
![](img/09_154_0__.jpg) 
![](img/09_155_0__.jpg) 

### 09_152_2:  
人物在画面的下1/3处，可能表示人物内心的压抑。  
![](img/09_152_2__.jpg) 


### 09_154:  
用手来衔接镜头：  
![](./img/09_154_4__.jpg) 
![](./img/09_154_5__.jpg)  

### 09_161:  
右图的边框（和左图分割处）很特别，可理解为破格：  
![](./img/09_161_5_6__cn.jpg)  

### 09_165_3:  
熏很生气，把闹钟摔向早纪（顺带还有打火机？）：  
![](./img/09_165_3__.jpg)  
这里貌似有个讲究：闹钟砸向门, 分镜是画物体砸上之前？砸上时？还是砸上后？对比一下：  
![](./img/09_165_3__mod.jpg)  

### 09_165_6:  
背景黯淡，辅助表达人物心情不好。  
![](img/09_165_6__.jpg) 


----------------------------------------------------------

## Vol09	CH063	page167		早纪的诱惑  	Saki's Temptation  		  

### 09_169:  
镜头切换到熏之前，先让别人提到熏。这种切镜头的手法在FC里很常见。  
![](img/09_169_0_2__cn.jpg)  
    

----------------------------------------------------------

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
