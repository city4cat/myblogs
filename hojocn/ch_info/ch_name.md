
# 关于CH中部分角色的名字  

## 槇村/槙村  

**槙**：  

- dian1, 树梢[^2][^1]。  
- zhen3, 1.木理坚密。2.一种常绿乔木,亦称"罗汉松"。[^2]  
- zhen1, 古同“稹”[^2]。  

**稹**zhen3:  

- （草木）丛生[^3]。  
-  古通“缜zhen3”，细密[^3]。  

**槇**:  

- dian1, 同“槙”[^4][^1]。   


综上，汉语里，“槙zhen1”古同“稹zhen3”，但两者读音不同，不知为什么。日文里，槙的旧体是槇，这种情况下，在汉语里读音为dian1。  

另外，国内有版本把"槇村"翻译为“木村”。  



## 冴羽獠  

**冴**：  

- ya4，古同“讶”[^5][^6]。freezing; stopped up, closed off[^5]。   
- hu4, 古同“冱”。读“冱”时在日语主要表达寒冷彻骨、光色鲜明、声音清晰之意。  

> 注：iOS、Ubuntu系统的中文拼音输入法里只能用拼音"hu4"检索"冴"。Android系统的中文拼音输入法里可用拼音"ya4"和"hu4"检索"冴"。    

**獠**：  

- liao2：  
    - 面貌凶恶[^7]。  
    - 夜间打猎[^1][^7]。  

-  lao3[^7]：  
    - 即僚 [Lao nationality]。中国古族名。分布在今广东、广西、湖南、四川、云南、贵州等地区。亦泛指南方各少数民族。  
    - 詈词。古时北方人骂南方人的话 [fellow;guy]  
    - 另见 liao2.    

另:  
1. "冴羽獠"最早在大陆被翻译为“寒羽良”。我猜，这是将“寒”取为“冴hu4”在日文里的"寒冷"之意，而“良”的中文发音类似“獠”的发音。  
2. '獠'字有打猎的意思，这或许和"City Hunter"中的"Hunter"呼应。  


## 香  

据说槇村香的"香"源自北条司的好友堀江信彦的女儿的名字。  

### "香"在日本文化中的含义  

首先，[北条司官方微博](https://m.weibo.cn/detail/4867266644018206)提到：  
![](img/xiang_thumb.jpg) ![](img/xiang_crop0.jpg)  
"香香香クリアファイル　パンダの香香 - CityHunter 槇村香 - AngelHeart 香莹"  
(香香香ClearFile　Panda香香 - CityHunter 槇村香 - AngelHeart 香莹)  

其次，[中国大熊猫，如何在日本成了“顶流”？](https://mp.weixin.qq.com/s/7hyJZkPXhzmYe64kknbAfw)一文有如下信息：  
"熊猫外形与日本“可爱”的审美趣味相契合，所以熊猫在日本极为受欢迎。
2017年，中国大熊猫在日本上野动物园诞下幼崽。然后该动物园向民众征集姓名，并投票。最后在发布会上宣布熊猫幼崽的名字为“香香”，表示这个名字“像花朵盛开一样给人以明快印象”"。  
这些信息表明“香”在日本文化中是一个很受欢迎的名字，作为名字它表示“像花朵盛开一样给人以明快印象”。  

鉴于北条司及其官方微博将Panda香香与槇村香联系起来，所以有理由猜测：槇村香的"香"或许也有“像花朵盛开一样给人以明快印象”的含义。  


## 参考资料  
[^1]: [名字的秘密 - HTKJ](../zz_htkj_cn/ch/chname.md)，「槙」的旧体是「槇」  
[^2]: [槙 - 汉典](https://www.zdic.net/hans/槙)  
[^3]: [稹 - 汉典](https://www.zdic.net/hans/稹)  
[^4]: [槇 - 汉典](https://www.zdic.net/hans/槇)  
[^5]: [冴 - 汉典](https://www.zdic.net/hans/冴)  
[^6]: [冴 - 百度百科](https://baike.baidu.com/item/冴/5300246)  
[^7]: [獠 - 汉典](https://www.zdic.net/hans/獠)  


## Links

- https://ocr.space/  
- [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
- https://o-oo.net.cn/keyboard/

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
