# CityHunter Subtitles (English version) 
source: 
[URL1](https://mangahot.jp/site/works/e_R0007), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=e_R0007) 
(2024/12, it's updated to "98.Ryo's Story of Innocence ")  

1.The Inglorious Ten Count!

2.The BMW Devil

3.A Huge Mistake!

4.I'll Do the Dirty Work!!

5.Shooter from the Darkness!

6.Angel Dust of Fear  （注：我认为应译为"Fearful Angel Dust"）  

7.The Blacklist of Death

8.A Wonderful Partner!

9.A Smile for Striking Back!

10.Diamonds Before Swine!

11.The General's Trap!

12.The Brand of Death

13.The Undying Devil!

14.Don't Give Anything to Villains!

15.The Dangerous Tutor

16.I Hate Him!

17.The Little Minx's Vain Struggle!

18.The Unbalanced Gang

19.The Ever-Waiting Little Girl

20.The Hitman Double Cast

21.And Thus Exits the Little Girl

22.The Barefoot Actress

23.Panic on the Set

24.The Actress in Flames

25.A Selfish Bunch

26.Serious on Location

27.Quivering Heart

28.Shoot the Specter!

29.A Terrible Teacher

30.I Can't Tell You Why!

31.Never Escape!

32.The Scar of Memories

33.Truth of the Past

34.A Smile as Reward

35.The Bell Tolls for Fate!

36.Don't Mess With That Woman!

37.One of a Thousand

38.Can't Resist My Lower Half!

39.The Dangerous Harem Man

40.Just be Tenacious!!

41.The Laaame Secret Weapon!

42.The Terrifying Man Sensor

43.Two Shots of Tenacity!

44.Gambling Queen!

45.The Sorrowful Gambler

46.The Gambler Vampire

47.A Dangerous Victory!

48.The Prodigy's Miscalculation!

49.After Service of Love

50.Idol on the Run!

51.She's Got My Interest!

52.Mystery Miniskirt!

53.The Worst Thing Ever!

54.Love is Blind!

55.Wedding Announcement Panic!

56.Woman from a Dangerous Land!

57.The Cute Tutor!

58.False Start of Love

59.Laws of Emalia!

60.Sweet Betrayal!

61.At Love and Pain's End?

62.The Flying Tush!

63.The Nutty Professor!

64.Magic Trick!

65.Very, Veeery Good Job!

66.Sanchos' Revolt!

67.A Tulip Blooms in the Heart

68.An Angel's Smile

69.What is Love?

70.The Iron-Clad Date!!

71.Ryo the Kid's Love Lecture

72.No Doctor or Remedy for Love!!

73.First Love

74.Shore of Memories

75.Don't Call Me Daddy!

76.One Look at You!

77.Audition Canceled?!

78.Surprise Mastermind!!

79.Vivid Memories

80.The Stolen Bride

81.The Dangerous Antidote

82.Back in Action?

83.Kitagawa on the Move!

84.Perfect Revenge!

85.Setting Off

86.Don't Touch The Nurse

87.Rose-Tinted Hospital Stay?!

88.Kaori's Nurse Story

89.Would You Like an Apple?!

90.I'm Sorry...

91.Goodbye... And Hello!

92.One Night's Mistake

93.Watch Out Kaori-Cakes!

94.Operation Brassiere!!

95.Makimura's Unsettled Score

96.Ryo's Darkest Day

97.Operation: Decoy

98.Ryo's Story of Innocence

