"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


说明：  
  
- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  


# FC里某些角色的颜值  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96881))  

## 工具  
有些网站可以给人的相貌打分，这里测试一些FC里的人物。 本文仅供娱乐, 尽量不比较、不评价人物的相貌。  

-  [Betaface - Demo](https://www.betafaceapi.com/demo.html)  
    处理完照片后，可以搜索与照片里的人物的相貌类似的名人。  

- BAPA:  
 [이승철 성형 미학 [Dr Rhee BEAUTY AESTHETICS] 네이버 블로그](https://blog.naver.com/artprs)  
[naver - Dr Rhee - BAPA](https://blog.naver.com/artprs/221145354540)  
[Facebook - BAPA](https://www.facebook.com/BAPA-164618980217753/info)  
[BAPA (Balanced Angular and Proportional Analysis) ](http://bapa.co.kr)  (无法访问？)  

- [Viralemon - Face Beauty Analysis Test](https://viralemon.com/apps/post-3187)  
给相貌打分，并给出显著特征，例如，脸形，鼻子，瞳距，嘴唇。  

- [ToolPie - Beauty calculator](https://beauty.toolpie.com/)  
给相貌打分，并给出某些结果，例如，年龄，性别，脸型，种族，表情，是否戴眼镜。

- [PinkMirror](https://pinkmirror.com/faceupload/ChooseFile)  

- [BeautyScores](http://beautyscores.com/get-score-file.aspx)  
给相貌打分，并给出相似的明星。  

- [BeautyScoreTest - Beauty Score Calculator](https://www.beautyscoretest.com/)
给相貌打分, 并给出某些结果，例如，性别，年龄，表情，脸型。

- [PrettyScale](https://www.prettyscale.com/)  
手动标定mark点

- [Google image search with '&imgtype=face'](https://www.google.com/images)  

- [PicTriev](http://www.pictriev.com/)  (需科学上网)  

- [TinEye](https://tineye.com)  (需科学上网)   

- [PimEyes](https://pimeyes.com)  (需科学上网, 需注册并付费后才能访问搜索结果里的链接)    

- [DeepFace](https://www.deepface.me/)  
面部识别、搜索近似的人、搜索BTS里近似的人、搜索LOL角色里近似的人  

- [SNSmatch](https://www.snsmatch.com/)  

- [RealAML](https://app.realaml.com/sign-up)  

- [StarByFace](https://www.starbyface.com/)  

- [Bing - VisualSearch](https://www.bing.com/visualsearch)  

- [TwinFinders](https://www.twinfinders.com)  

- [Twinlets](https://www.twinlets.com/)  

- [Face Research](http://faceresearch.org/)
    
    
## 测试  
- 紫苑01_047_0  
![](img/shion/01_047_0__mod.jpg)  

    - Betaface:  
        - 相貌相似的名人：   Shannen Doherty(77.9%), Michelle Branch(76.8%), Vanessa Carlton(76%), Salma Hayek(75.8%), Ayumi Hamasaki(滨崎步)(75.8%), Katie Tunstall(75.5%), Namie muro(安室奈美惠)(75.4%), Megumi Hayashibara(75.1%)  
        
    - Viralemon:  
        - Beauty Score: 96.6  
        - Facial width：Oval, Lips:wide, Face size:Normal, Eyes Size:Normal  

    - ToolPie：  
        - Face beauty score: 70
        - Age: 23, Gender: female, Face Shape: heart, Expression: none, Race: yellow, Glasses: none
        
    - PinkMirror:  
        - 8.77 out of 10
        - The proportion of your nose width, lips, jaw line, chin are near perfect! Your eyes is a good feature. Overall you are very attractive.  
        - For this angle of your photo, your face shape is Heart. A stand out feature of a heart face shape is a wide forehead and cheekbones which becomes narrow when you follow the lines of your face downwards your jawline. Then it finishes off with a cute pointy chin.  

    - BeautyScores：  
        - Age: 21，Score: 90.558  
        - Dilraba Dilmurat（迪丽热巴）
        
    - BeautyScoreTest：
        - beauty score is 70 !
        - Gender: female， Age: 23 years old， Expression: none， Face shape: heart 
        
    - prettyscale
        - 47%, You are not bad! Prominent Features
            Good face shape
            Forehead too big
            Wide interocular distance
            Nose too narrow
            Long nose
            Small mouth for nose
            Big chin
            Bad face symmetry
        
- 紫苑01_150_7  
![](img/shion/01_150_7__mod.jpg) 

    - Betaface:  
        - 相貌相似的名人：  Namie muro(安室奈美惠)(82%), Gackt(79.4%), Ayumi Hamasaki(滨崎步)(79%), Michelle Branch(78.1%), Yamashita Tomohisa(77.8%), Nakama Yukie(77.6%), Jang Nara(77.1%), 

    - Viralemon:  
        - Beauty Score: 89.9 
        - Facial width：Oval, Lips:wide, Face size:Normal, Eyes Size:Normal  

    - ToolPie：  
        - Face beauty score: 73
        - Age: 24, Gender: female, Face Shape: heart, Expression: none, Race: yellow, Glasses: none
        
    - PinkMirror:  
        - 9.17 out of 10
        - The proportion of your eyes, lips, jaw line, chin are near perfect! Your nose width is a good feature. Overall you are exceptionally beautiful. For this angle of your photo, your face shape is Oval. You have wider cheekbones and it is the most prominent feature of their facial structure. The forehead and the jawline are slightly smaller than the cheekbones. The edges of the face are rounded, and the jawline is also curved around the edges. Your face exhibits no sharp or angular curves. It is considered to be the most ideal face type for all forms of accessories. 
        
    - BeautyScoreTest：  
        - beauty score is 73
        - Gender: female， Age: 24 years old， Expression: none， Face shape: heart 
        
    - deepface：  
        - Nion face recognition result:  
            - 27 years old male
            - male:83%, female:17%, smile:90%
        - Resemblance:  
            - 56%, Kim Taeyeon  
            - ![](http://imgnews.naver.net/image/5348/2021/03/29/29_712388_38060_20210329183221702.jpg)  
            - ![](http://imgnews.naver.net/image/5638/2021/02/16/0000031830_004_20210216172240550.jpg)   
            - ![](http://post.phinf.naver.net/MjAyMTAxMjdfMTU2/MDAxNjExNzM0NDM2MTE5.IHfo8QaNVSlvwzZvAlMVNoQHUYZsKuYUkGUb9LVpTgEg.7mlQOho8wdH7U_44G2kZ2rZj2thl3e2BEI5UsbJulE0g.JPEG/IOhCE3bAHl-rZ5_Hfg8QHaeGngng.jpg)  
            
        - Analysis:  
            - Appearance Score Male AI Assessment: Top 15% Appearance Rating with 85 points  
            - Beauty Score Women's AI Evaluation: 88 points, Top 12% Beauty
        
        
    - starbyface:  
        - Rose Byrne(38%), Winona Ryder(36%),  Pom Klementieff(36%)  
 
   
- 紫苑01_150_7  
![](img/shion/03_003_0__mod.jpg) 
![](img/shion/03_003_0__mod_gray.jpg)   

    - Betaface:  
        - 相貌相似的名人：  Lillian Gish(79%), Katie Tunstall(77%), Michelle Branch(75%), Vanessa Carlton(75%), Joely Richardson(75%), Clara Bow(75%)  
       
    - Viralemon:  
        - Beauty Score: 96.6  
        - Face size:Normal, Eyes Size:Normal, Eyes proportion: Excellent, Pupillary distance:Normal, Cheek bones:High,  
        
    - ToolPie：  
        - Face beauty score: 57
        - Age: 23, Gender: female, Face Shape: heart, Expression: none, Race: white, Glasses: none
        
        测试黑白照：  
        - Face beauty score: 63  
        - Age: 23, Gender: female, Face Shape: oval, Expression: none, Race: white, Glasses: none      
        
    - PinkMirror:  
        - 6 out of 10  
        - The proportion of your eyes, jaw line, chin are near perfect! Your nose width is a good feature. While looking at this photo, we feel the proportion of your Lips can be improved by makeup. For this angle of your photo, your face shape is Heart. A stand out feature of a heart face shape is a wide forehead and cheekbones which becomes narrow when you follow the lines of your face downwards your jawline. Then it finishes off with a cute pointy chin.  
        
        测试黑白照：  
        - 7.66 out of 10  
        - The proportion of your jaw line, chin are near perfect! Overall you are attractive. While looking at this photo, we feel the proportion of your Eyes, Nose Width, Lips can be improved by makeup. For this angle of your photo, your face shape is Round. The width of your face is mostly the same length of your face. Your jawline will also have a soft round shape rather than pointy or with an angle with edges. Round face shape is perfectly uniform with a petite look as typically seen in young children. Your cheekbones are the widest feature of your facial structure and you have full cheeks.  
        
    - BeautyScoreTest：  
        - beauty score is 57
        - Gender: female， Age: 23 years old， Expression: none， Face shape: heart 
        
        测试黑白照：    
        - beauty score is 63
        - Gender: female， Age: 23 years old， Expression: none， Face shape:  oval  
        
    - deepface：  
        - Nion face recognition result:  
            - 28 years old male
            - male:83%, female:17%, smile:90%  
        - Resemblance:  
            - 100%Manami  
        - skin analysis:  
            - Dark circles:83 points
            - Blemishes management:	98 points  
            - Acne management:	97 points  
            - Pore ​​elasticity:	37 points  
            - Wearing glasses:	Not wearing glasses  
            - Total skin score:   90 points  
            - Appearance Score Male AI Assessment:Top 28% Appearance Rating with 72 points  
            - Beauty Score Women's AI Evaluation: 72 points Top 28% Beauty  
            
    - starbyface:  
        - Rose Byrne(38%), Sophie Turner(34%),  Isabella Rossellini(33%),  Winona Ryder(32%) 
        
        
--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
