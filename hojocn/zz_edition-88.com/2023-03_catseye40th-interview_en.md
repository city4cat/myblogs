source:  
https://edition88.com/blogs/news/cat-s-eye-40th-anniversary-original-art-exhibit-special-interview-part1  
https://edition88.com/blogs/news/cat-s-eye-40th-anniversary-original-art-exhibit-special-interview-part-2


![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/articles/Cat_sEye_40thAnniversary_1800x800_outline_400x.jpg)  

March 04, 2023

# “Cat’s Eye” 40th Anniversary Original Art Exhibit Special Interview (PART 1)  
“Cat’s Eye” 40周年纪念原画展 专访（PART 1）  

![](https://cdn.shopify.com/s/files/1/0614/5123/9648/files/DSC04172_bcc67af0-e183-4950-a978-d76e1b566300_100x100.jpg)  
To commemorate the Original Art Exhibit, we talked with the artist Tsukasa Hojo and his editor on Cat’s ♥ Eye at the time, Nobuhiko Horie (current, CEO of Coamix Co., Ltd.)  
为了纪念原画展，我们采访了艺术家北条司和他当时Cat’s♥Eye的编辑堀江信彦（现任Coamix Co., Ltd.的CEO）。  

**In the Beginning: the 18th Tezuka Award Work, “Space Angel”**  
开始:第18届手冢奖作品“Space Angel”  

![](https://cdn.shopify.com/s/files/1/0614/5123/9648/files/DSC04138_100x100.jpg)  
**Hojo** - When I was a junior in college, a friend submitted his manga to the Tezuka Award. I wasn’t familiar with the award, but I heard that the prize was a whopping one million yen (approx. $4525 US, 1979)! So that got me interested.  
**北条** - 我上大三的时候，一个朋友向手冢奖提交了他的漫画。我对这个奖项并不熟悉，但我听说奖金高达一百万日元（约合4525美元，1979年）！这让我很感兴趣！   

At the time my experience in drawing manga was limited, but I was a college student with lots of time. So I began drawing one page per night on evenings after my summer job.  
当时我画漫画的经验很有限，但我是个大学生，有很多时间。所以我开始在暑假工作结束后的晚上，每晚画一页。  

“Space Angel” was the name of a squad appearing in an animation that my friends and I were working on at school. Based on that segment, I just drew what I wanted to into a manga each night. Looking back, though, it was far from being a readable manga.  
“太空天使”是我和朋友们在学校制作的动画中出现的一个小队的名字。在这个基础上，我每天晚上都把我想画的画进漫画。然而回过头来看，它远非一部可读的漫画。

That came out to about 20 pages, so I quickly added an ending to turn it into a 31-page manga, and submitted it to the Tezuka Award right before its deadline. Come to think of it now, it was totally absurd.  
这大约有20页，所以我迅速添加了一个结尾，把它变成了一本31页的漫画，并在截止日期前将其提交给了手冢奖。现在想起来，这太荒谬了。  

If you won, they notified you by phone. But I lived in an apartment without one, so I received a postcard. The postcard was from Horie-san, who later became my editor. I didn’t fully understand what editors in charge of a work did, but I do remember being surprised by the message on the postcard: “Let’s create thrilling manga together!!!”  
如果你赢了，他们会通过电话通知你。但我住在一个没有电话的公寓里，所以我收到了一张明信片。这张明信片是堀江先生寄来的，他后来成为我的编辑。我并不完全了解责任编辑是干什么的，但我确实记得对明信片上的信息感到惊讶： "让我们一起创造惊心动魄的漫画！！"  

**Horie** - The message on the postcard was just me, doing my job.  
**堀江** - 明信片上的信息只是我在做我的工作。  

I mean, it’s a line that a Weekly Shonen Jump editor should come up with, right? “Friendship, effort, and victory” is what the magazine is all about.  
我是说，这句话应该是《少年周刊》的编辑想出来的，对吧?“友谊、努力和胜利”是这本杂志的主旨。  

The Tezuka Award selection process was done in groups, and there used to be a rule that let the most junior editors choose an artist they wanted to work with.  
手冢奖的评选过程是分组进行的，过去有一条规则，让最初级的编辑选择他们想要合作的艺术家。  

I was the youngest, so I got to choose Tsukasa. While his submission piece was, as he acknowledges, rather unfinished, the award judges unanimously agreed that he was an artist with massive potential.  
我是最年轻的，所以我选择了北条。虽然他提交的作品，正如他所承认的那样，相当未完成，但评委们一致认为他是一位具有巨大潜力的艺术家。  

**1981: How the First Series Manga, Cat’s ♥ Eye Began**  
1981年：第一个系列漫画Cat’s♥Eye是如何开始的  

![](https://cdn.shopify.com/s/files/1/0614/5123/9648/files/IMG_3108_100x100.png)  
**Hojo** - During my four years at college, I created three one-shot pieces, of which two were published.  
**北条** - 在大学四年里，我创作了三个短篇作品，其中两篇发表。

The editors asked for more, but I begged them to wait, because I needed to focus on my graduation work and thesis.  
编辑们要求更多，但我恳求他们等待，因为我需要专注于我的毕业作品和论文。    

Cat’s ♥ Eye was the first piece I created after I completed my college work.  
Cat’s♥Eye是我完成大学学业后创作的第一部作品。

It was published in the Weekly Shonen Jump, and the readers seemed to like it, so Horie-san asked me to develop _a name_ (draft) of a sequel. However, when I drew up the _name_ that set Ai as the main character, he asked for another.  
这篇文章发表在《周刊少年跳跃》上，读者似乎很喜欢，所以堀山让我为续集拟定一个名字(草稿)。然而，当我拟好以小爱为主角的名字时，他又要了另一个。  

So I sent in another work. Then in June, three months after I graduated, he gave me a call, out of the blue, to tell me I should move to Tokyo immediately because I now have a series. I found out later that he used my drafts to pitch Cat’s ♥ Eye as a series at a Shonen Jump Serialization Meeting.  
所以我又送去了另一部作品。然后在六月，也就是我毕业三个月后，他突然给我打了个电话，告诉我应该马上搬到东京去，因为我现在有一个连载。后来我发现，他在少年Jump系列会议上用我的草稿推荐Cat’s♥Eye画连载。  

I remember thinking that having my series in a magazine was a rare opportunity, so I should grab it when I can and move to Tokyo.  
我记得当时我想，把我的连载刊登在杂志上是一个难得的机会，所以我应该在可能的时候抓住它，搬到东京去。  

The series began in the summer, but at first, I struggled to keep up and had to work like crazy. By late autumn, however, I think I got the hang of it.  
这个连载是在夏天开始的，但是一开始，我很难跟上，不得不疯狂地工作。然而，到了晚秋，我想我已经掌握了窍门。  

**Horie** - I believed that budding manga artists should finish school--high school, college, so forth--before making their debut, so I was willing to wait until Tsukasa graduated.  
**堀江** - 我认为刚出道的漫画家应该完成学业——高中、大学等等——然后才能出道，所以我愿意等到北条毕业。  

At the same time, I also knew he could hit the ground running, so the earlier his debut, the better. The schedule was to have Tsukasa draft up _names_ while still in school and begin his series immediately after he graduated, just like other students joining the workforce when they graduate. I didn’t plan any of this on my own… or at least I don’t think I did.  
同时，我也知道他可以一鸣惊人，所以他的出道越早越好。我们的计划是，让北条在学校时就拟好 _草稿_ ，毕业后立即开始他的连载作品，就像其他学生毕业时参加工作一样。 这些都不是我自己计划的......或者至少我认为我没有。  

At first, Tsukasa said he could only finish up to three pages per day because he still wasn’t used to the drawing process. That amounts to 21 pages per week. Shonen Jump’s serialized story manga is 19 pages per week. That leaves basically no time to rest or work on _names_; it must have been excruciating. Anyway, after four months, he said he could manage five pages a day, and things became smoother.    
起初，北条说他每天只能完成三页，因为他还不习惯画画的过程。这相当于每周21页。《少年Jump》的连载漫画是每周19页。这样基本上就没有时间休息或创作 _草稿_ 了；那一定很痛苦。不管怎样，四个月后，他说他可以每天写五页，事情变得更顺利了。  

**On Cat’s ♥ Eye**  
关于Cat’s♥Eye  

![](https://cdn.shopify.com/s/files/1/0614/5123/9648/files/IMG_3107_100x100.jpg)  
**Hojo** - I developed this as a one-shot manga, so I hadn’t thought through detailed settings like you usually would for a series. Suddenly, though, it was to be a series. So my first question was, now what do I do?  
**北条** - 我把它作为短篇漫画来画的，所以我没有像通常的连载漫画那样考虑过详细的设定。不过，突然间，它要成为一个连载。所以我的第一个问题是，现在我该怎么做？  

Horie-san surprised me after the series began by asking what drives the sisters as art thieves. I was like: now you ask me? Isn’t that something we should have discussed before the series started? I had a vague notion that the characters come from a family of thieves, but Horie-san kept pushing for a more compelling, exciting motivation.  
堀江先生在连载开始后问我，是什么驱使姐妹们盗窃艺术作品，这让我很吃惊。我当时想：现在你问我？这不是我们应该在连载开始前就讨论的问题吗？我有一个模糊的概念，即这些角色来自一个小偷家庭，但堀江先生一直在推动一个更令人信服、令人兴奋的动机。  

“That’s not quite _this_ story,” I thought but out of desperation, I blurted out an idea about the sisters hunting for a painting by their father. It was spur of the moment, so no-crying-over spilled whatever, but the idea stuck. So we fleshed it out with the Cranaff Syndicate and other aspects.  
"这不完全是 _这_ 个故事，"我想，但出于绝望，我冒出了一个关于姐妹们寻找她们父亲画作的想法。那是一时冲动，所以我不禁后悔，但这个想法坚持了下来。于是我们用Cranaff集团和其他方面来充实它。  

**Horie** - Tsukasa and his friends had fun and had already developed concepts for the serialization of Cat’s ♥ Eye, so we didn’t create the foundation together.  
**堀江** - 北条和他的朋友们玩得很开心，并且已经开发了Cat’s♥Eye的连载概念，所以我们没有一起创建基础。  

I asked why the sisters were thieves, simply because I didn’t know.  
我问那对姐妹为什么是小偷，只是因为我不知道。  

I did know that doing a series of stand-alone stories solely on the idea of thieves would become increasingly difficult to continue. On the other hand, an underlying story would facilitate the continuity of a series, so I thought we should incorporate why the characters turned into art thieves as the backbone of the narrative.  
但我知道，仅仅以小偷为主题创作一系列独立的故事将越来越难继续下去。另一方面，一个背后的故事将促进一个连载继续，所以我认为我们应该把为什么角色变成艺术小偷作为叙事的支柱。  

Maintaining a series through installments of one-shot stories is extremely challenging.  
通过分期的短篇故事来维持一个连载是极具挑战性的。  

**The Two Endings of Cat’s ♥ Eye**  
猫眼的两个结局

**Hojo** - Actually, I don’t know what happened between the ending of the series on Weekly Shonen Jump and the final one-shot, but the magazine probably had its reasons.  
**北条** - 实际上，我不知道在《周刊少年Jump》上的连载结局和最后一话之间发生了什么，但杂志可能有它的原因。  

I was mulling over what to do with the one-shot because the series finale already presented a good closure for me. That’s when Horie-san told me an interesting story.  
我正在考虑如何做这个短篇故事，因为连载的大结局对我来说已经是一个很好的结局了。就在那时，堀江给我讲了一个有趣的故事。  

He said, “This guy I know, his wife came down with a viral cold. She wakes up, sees him, and says, ‘Who the hell are you?’ It seems that the virus went to her brain, and she lost her memory.”  
他说："我认识的一个人，他的妻子得了病毒性感冒。她醒来看到他时，说：'你到底是谁？' 似乎病毒进入了她的大脑，她失去了记忆。"  

It was an intriguing story that I wanted to depict, and it became the final one-shot.  
这是一个耐人寻味的故事，我想把它画出来，并做为最后一话。  

**Horie** - Cat’s ♥ Eye didn’t end because of a lack of interest, so I felt we had unfinished business, and I wanted to do the work justice.  
**堀江** - Cat's ♥ Eye没有结束是因为缺乏兴趣，所以我觉得我们有未完成的工作，我想对得起这部作品。  

The amnesia story did really happen to a friend of mine, and Tsukasa and I frequently had conversations like these. We’d be shooting the breeze when inspiration would gel into a storyline. We talked about good movies, stuff that happened to our friends, and many other things. Having good conversations is also a part of an editor’s job.  
失忆症的故事确实发生在我的一个朋友身上，我和北条经常这样对话。我们在闲聊时，灵感就会凝聚成一个故事情节。我们谈论好的电影、发生在我们朋友身上的事情，以及许多其他事情。进行良好的对话也是编辑工作的一部分。  

_To be continued…_


---

---

---


![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/articles/cat_s-eye-tenji_blogbanner_400x.jpg)  

March 20, 2023

# “Cat’s Eye” 40th Anniversary Original Art Exhibit Special Interview (PART 2)
“Cat’s Eye” 40周年纪念原画展 专访（PART 2）  

To commemorate the Original Art Exhibit, we talked with the artist Tsukasa Hojo and his editor on Cat’s ♥ Eye at the time, Nobuhiko Horie (current, CEO of Coamix Co., Ltd.)
为了纪念原画展，我们采访了艺术家北条司和他当时Cat’s♥Eye的编辑堀江信彦（现任Coamix Co., Ltd.的CEO）。  

[_Click here for PART 1_](https://edition88.com/blogs/news/cat-s-eye-40th-anniversary-original-art-exhibit-special-interview-part1 "Tsukasa Hojo interview Part1")

**_PART2_**

**On to “City Hunter”**  
关于“City Hunter”

**![](https://cdn.shopify.com/s/files/1/0614/5123/9648/files/DSC04194_e80cb3ee-0426-4519-af37-aa04c1b4ad5b_600x600.jpg)**  
**Hojo** - There was the Weekly Shonen Jump Fans’ Award in those days, where artists could create one-shot works.  
**北条** - 在那些日子里，有一个《周刊少年Jump》粉丝奖，艺术家可以创作短篇作品。

It was the first City Hunter I draw for this.  
这是我为之画的第一个City Hunter。    

While serializing "Cat's ♥ Eye," I was drawing one-shot works of "City Hunter", one of which appeared in a special edition and was very popular. Even before Cat’s ♥ Eye ended, the editors seemed to think that City Hunter would be the follow-up.  
在连载《猫♥眼》的同时，我在画《城市猎人》的短篇作品，其中一个出现在特刊中，非常受欢迎。甚至在《猫♥眼》结束之前，编辑们似乎就认为《城市猎人》将是后续作品。  

Ryo Saeba is based on Kamiya (the Rat) from Cat’s ♥ Eye, but as I went through trial and error exercises creating the series, he turned into a different character.  
冴羽獠的原型是《猫眼》中的神谷（老鼠），但在我通过试错练习创作这个连载时，他变成了一个不同的角色。

**Horie** - I knew that City Hunter would be an enduring series from the earlier one-shot story days.  
**堀江** - 从早期的短篇故事时代开始，我知道《城市猎人》将是一个持久的连载。  

We always used to say that when Kamiya (the Rat) makes an entrance, he propels the plot, so the next series should be about him.  
我们当时总说，当神谷（老鼠）登场时，他推动了剧情的发展，所以下一个连载应该是关于他的。  

That’s why Cat’s ♥ Eye, followed by the City Hunter series, was a natural transition for me.   
这就是为什么《猫♥眼》之后的《城市猎人》系列对我来说是一个自然过渡。  

**On the 40th Anniversary**  
关于40周年纪念  

**![](https://cdn.shopify.com/s/files/1/0614/5123/9648/files/DSC04156_1024x1024.jpg)**  
**Hojo** - I never knew if I had the talent, but I kept going. When I look back and realize that it’s been more than 40 years, I don’t know how I managed, to be quite honest.  
**北条** - 我从来不知道我是否有这个天赋，但我一直在坚持。当我回首往事，发现已经过去40多年了，说实话，我不知道自己是怎么做到的。  

In the very beginning, Horie-san persuaded me to move to Tokyo, saying, “Give it five years.”  
在最开始的时候，堀江先生劝说我搬到东京，说"给它五年时间"。  

Frankly, I didn’t think I could amount to much in five years.  
坦率地说，我认为我在五年内不可能有什么成就。  

I felt that my taste was off the mainstream, beaten track. Things that I didn’t like became hits, or things I loved would quickly disappear. So I still don’t know how I lasted for 40 years.  
我觉得我的品味偏离了主流、不走寻常路。我不喜欢的东西会成为热门，或者我喜欢的东西会很快消失。所以我还是不知道我是怎么坚持了40年的。  

Of course, Horie-san played an important role.  
当然，堀山起了很重要的作用。  

Now, I want to draw a manga that feels old-school, fantastical...something that doesn’t need to be very real.  
现在，我想画一幅感觉老派的、奇幻的漫画……一些不需要非常真实的东西。  

**Horie** - Tsukasa’s had a great ride.  
**堀江** - 北条的职业生涯很顺利。  

He seems laid back and won’t let his drive or passion come to the surface, but that’s probably not how he is inside.  
他看起来很悠闲，不会让他的动力或激情流露出来，但这可能不是他的内心。  

His talent attracted many people and turned the last 40 years into an extraordinary journey.  
他的才华吸引了许多人，并将过去40年变成了一段非凡的旅程。  

During the series, we’d figure out each other’s thought processes as the manga artist and the editor and learned from each other. So we didn’t need to explain too much.  
在连载中，作为漫画家和编辑，我们会了解彼此的思维过程，并互相学习。所以我们不需要解释太多。  

**Last Question: On Each Other, or Any Messages for Each Other**  
最后一个问题:关于彼此，或向对方说些什么  

**Hojo** - I’ve already told him anything that needs to be said, so I don’t have much more to add…. But okay, Horie-san is like a geyser of ideas and passion, but there are times when I wish that perhaps he could read the room a little. Sometimes.  
**北条** - 我已经把需要说的都告诉他了，所以我没有更多的补充....但是，好吧，堀江先生就像一个思想和激情的喷泉，但是有的时候我希望也许他能稍微读懂这个房间。有时候。（译注：待校对）  

You know how you can be amazed when other people show you ideas and perspectives you didn’t have? Horie-san is brimming with them.  
你知道当别人向你展示你没有的想法和观点时，你会多么惊讶吗?堀山满脑子都是。  

**Horie** - I don’t have anything to add, either.  
**堀江** - 我也没有什么要补充的。  

Let me say this, though: I’m not going to congratulate his past. There’s a whole lot more we can still do for manga, like the company we’ve started, or nurturing young talent, etc.  
不过，让我说一句：我不打算祝贺他的过去。我们还可以为漫画做很多事情，比如我们创办的公司，或者培养年轻的人才，等等。   

I hope we can continue to work on these together.  
我希望我们能继续共同致力于这些工作。  

But Tsukasa has done everything that he possibly can, and more, so I do want to congratulate him on his well-deserved freedom to do anything he pleases.  
但是北条已经做了他能做的一切，甚至更多，所以我真的想祝贺他获得了应得的自由，可以做任何他喜欢的事情。  

Tsukasa’s Cat’s ♥ Eye and City Hunter are not just classics; new executions are being brought to life, such as depictions created by different artists, and live-action films. I hope Tsukasa can fully enjoy these as well.  
北条的《猫♥眼》和《城市猎人》不仅仅是经典之作；新的实施即将和大家见面，例如不同艺术家创作的描写，以及真人电影。我希望北条也能充分享受这些。  

![](https://cdn.shopify.com/s/files/1/0614/5123/9648/files/IMG_3099_600x600.jpg)  

[_See Tsukasa Hojo art print collection_](https://edition88.com/collections/tsukasa-hojo "Tsukasa Hojo Art print collection")  

