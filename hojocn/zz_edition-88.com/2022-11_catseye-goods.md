source: https://edition-88.com/pages/catseye-goods  

译注1：原文发表于2022年4月～5月期间。后因2022年11月在福冈县的再次画展而有所更改。本文将更改的内容标记出来。   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s-eyelogo_ecfc3efb-19c4-4c87-9b47-19ab01623a52_1000x.jpg)  

**版画**  

北条司先生の直筆サインが入る豪華な仕様となっているほか、全て原画原寸サイズで作られており、原画そのままの見た目を再現しています。  
エディション数は国内版200、インターナショナル版180、数量限定で販売いたします。  

版画一枚一枚に職人による手作業が施されています。  
キャッツアイの版画では、ホワイトの絵の具を使って一枚ごとに手彩色を施すことで、原画がもつ凹凸（マチエール）を表現することができました。  

※商品画像はイメージです。実際の商品とは異なる場合がございます。(译注：福冈展无这句话)    

-------------------  


**受注商品**(译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/キャッツ♥アイ))      

右側：「キャッツ♥アイ」版画1 （週刊少年ジャンプ 1983年 第36号 表紙より）  
左側： 「キャッツ♥アイ」版画2（週刊少年ジャンプ 1984年 第18号 表紙より）  
￥59.400（税込）  
●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦622×横471×厚さ20mm  
5/13（金）より通販でも受付開始予定。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_hanga01_800x.jpg?v=1650340444)  


------------------  


**受注商品**(译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/キャッツ♥アイ))      

下：「キャッツ♥アイ」 版画3（ジャンプ・コミックス 1985年 第14巻／表紙カバーより）  
右上：「キャッツ♥アイ」 版画4（ジャンプ・コミックス 1985年 第18巻／表紙カバーより）    
左上：「キャッツ♥アイ」 版画5（週刊少年ジャンプ 1983年 第46号 表紙より）  
￥44,000（税込）  
●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦525×横410×厚さ20mm  
5/13（金）より通販でも受付開始予定。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_hanga02_800x.jpg?v=1650341506)  

---------------------  


**受注商品**(译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/キャッツ♥アイ))      

「キャッツ♥アイ」 版画6（ジャンプ・コミックス1982年 第2巻／表紙カバーより）  
￥38,500（税込）  
●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦440×横364×厚さ20mm  
5/13（金）より通販でも受付開始予定。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_hanga03_800x.jpg?v=1650342199)   

---------------------------


**受注商品**(译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/キャッツ♥アイ))    

「キャッツ♥アイ」版画7（ジャンプ・コミックス 1983年 第5巻／表紙カバーより）  
￥33,000（税込）  
●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦395×横304×厚さ20mm  
5/13（金）より通販でも受付開始予定。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_hanga04_800x.jpg?v=1650342218)  


------------------------

**新作**(译注：福冈展新增)  

**「キャッツ♥アイ」 版画8（週刊少年ジャンプ 1983年 第1・2合併号／表紙カバーより）**  

￥44,000（税込）  

●仕様：北条司直筆サイン入り、木製額入り  
●総エディション数：380（国内版：200）  
※エディションナンバーはお選びいただけません。  
●額サイズ：縦525×横410×厚さ20mm  

**11/19（土）より通販で販売開始。**  
**会場では展示のみになります。**  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hanga-cats_400x.jpg?v=1666780833)  


------------------------

## GOODS

※商品ラインナップ、商品の仕様は予告なく変更になる場合がございます。その場合の詳細は決定次第、サイト内の商品ページやTwitter等にて発表します。  
※商品画像はイメージです。実際の商品とは異なる場合がございます。  
※商品の数には限りがあります。在庫に関するお問い合わせ（現在の在庫数や入荷予定等）にはご対応できません。あらかじめご了承ください。  
※会期前・会期中を問わず購入個数制限を変更する場合がありますのでご了承ください。  
※本会場での決済方法は、現金とクレジットカード（VISA、mastercard、AMERICANEXPRESS）のみご利用いただけます。(译注：福冈展无这句话)  


- image    
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/499ccc41625b1ac1b81d1c05622d422e.jpg)  

キャッツ♥アイ40周年原画展　図録  
¥2,200（税込）  
※図録付き入場券もございます。  

- image    
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_01.jpg?v=1651481782.jpg?v=1651481782)   

高級アートプリント1（B2）  
¥13,200（税込）  
※受注商品になります。  


- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_10.jpg?v=1651481782)  

高級アートプリント1（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_01.jpg?v=1651481782.jpg?v=1651481782)  

高級アートプリント2（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_02.jpg?v=1651481782)  

高級アートプリント3（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_03.jpg?v=1651481783)  
高級アートプリント4（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_04.jpg?v=1651481782.jpg?v=1651481782)  

高級アートプリント 5（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_05.jpg?v=1651481782.jpg?v=1651481782)  
高級アートプリント6（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_06.jpg?v=1651481782)  
高級アートプリント7（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_07.jpg?v=1651481782)  
高級アートプリント8（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_08.jpg?v=1651481782)  
高級アートプリント9（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_09.jpg?v=1651481782)  
高級アートプリント10（A4）  
¥1,540（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_01_47f72375-7629-489b-ab6e-f26077f21713.jpg?v=1651492833)  
複製原稿1（B2）  
¥4,400（税込）  
※受注商品になります。  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_02.jpg?v=1651492833)  
複製原稿2（B2）  
¥4,400（税込）  
※受注商品になります。  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_03.jpg?v=1651492833)  
複製原稿3（B2）  
¥4,400（税込）  
※受注商品になります。  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_04.jpg?v=1651492833)  
複製原稿4（B2）  
¥4,400（税込）  
※受注商品になります。  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_01.jpg?v=1651479980)  
複製原稿1（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_02.jpg?v=1651479980)  
複製原稿2（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga1.jpg?v=1650535780)  
複製原稿3（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_04.jpg?v=1651479980)  
複製原稿4（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga3.jpg?v=1650535780)  
複製原稿5（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_06.jpg?v=1651479980)  
複製原稿6（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_07.jpg?v=1651479980)  
複製原稿7（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_08.jpg?v=1651479980)  
複製原稿8（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga2.jpg?v=1650535781)  
複製原稿9（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_10.jpg?v=1651479980)  
複製原稿10（B4）  
¥1,100（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_11.jpg?v=1651479980)  
複製原稿11（B4）  
¥1,100（税込）  
  
- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_12.jpg?v=1651479980)  
複製原稿12（B4）  
¥1,100（税込）  

- image（译注：福冈展无）  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_acryl-stand_1.jpg?v=1651416593)  
アクリルスタンド（描き下ろし/全3種類）  
各¥1,760（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_acrylic.jpg?v=1651420768)  
トレーディング アクリルスタンドキーホルダー（全8種類）  
各¥880（税込）  
※コンプリートセットは受注になります。  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_57mmcanbadge.jpg?v=1650535780)  
トレーディング 缶バッジ（全13種類）  
各¥440（税込）  
※コンプリートセットは受注になります。  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cats_card_3.jpg?v=1651487895)  
アートトレーディングカード （全19種類）  
各¥330（税込）  
※コンプリートセットは受注になります。  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_4.jpg?v=1651416415)  
クリアファイル（描き下ろし）  
¥500（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_5.jpg?v=1651416415)  
クリアファイル（セクシーダイナマイトギャルズ）  
¥500（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_6.jpg?v=1651416415)  
クリアファイル（最後のビッグゲーム）  
¥500（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_1.jpg?v=1651416415)  
クリアファイル（Silhouette）  
¥500（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_2_67f6263f-99df-4584-b3e2-dc7665b1948f.jpg?v=1651477365)  
クリアファイル（Building）  
¥500（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_3.jpg?v=1651416414)  
クリアファイル（Window）  
¥500（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_coaster.jpg?v=1651420768)  
アートタイル（全10種類）  
各¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_2.jpg?v=1651416860)  
マグカップ（描き下ろし）  
¥1,650（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_1.jpg?v=1651416860)  
マグカップ（瞳）  
¥1,650（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_3.jpg?v=1651416860)  
マグカップ（セクシーダイナマイトギャルズ）  
¥1,650（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_4.jpg?v=1651416860)  
マグカップ（最後のビッグゲーム）  
¥1,650（税込）  


- 画像のサンプル  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_sticker.jpg?v=1650535780)  
ダイカットステッカー  
¥330（税込）  


- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_towel2.jpg?v=1651426744)  
ハンドタオル（描き下ろし）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_hand_towel.jpg?v=1650535781)  
ハンドタオル（瞳）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_big_towel-01.jpg?v=1651416802)  
ビッグタオル（描き下ろし）  
¥3,850（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_big_towel-02.jpg?v=1651416802)  
ビッグタオル（Jump）  
¥3,850（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_2.jpg?v=1651416888)  
メガネ拭き（描き下ろし）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_1.jpg?v=1651416888)  
メガネ拭き（Climbing）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_3.jpg?v=1651416888)  
メガネ拭き（Jump）  
¥880（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_pouch_1.jpg?v=1651416826)  
ポーチ（描き下ろし）  
¥1,650（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_tote_2.jpg?v=1651416738)  
トートバッグ（描き下ろし）  
¥2,200（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_tote_1.jpg?v=1651416738)  
トートバッグ（コレクション No.87）  
¥2,200（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_1.jpg?v=1651416946)  
Tシャツ（描き下ろし） M／L  
¥3,850（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_Tshirt2.jpg?v=1650535780)  
Tシャツ（セクシーダイナマイトギャルズ）M／L  
¥3,850（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_6.jpg?v=1651416946)  
Tシャツ（Cat'sEye）M／L  
¥3,850（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_3.jpg?v=1651416946)  
Tシャツ（最後のビッグゲーム）M／L  
¥3,850（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_Tshirt1.jpg?v=1650535780)  
Tシャツ（離南島からの招待状）M／L  
¥3,850（税込）  

- image  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_5.jpg?v=1651416947)  
Tシャツ（予告状の秘密）M／L  
¥3,850（税込）  
















