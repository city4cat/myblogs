source:  
https://edition88.com/blogs/news/cats-eye-original-art-exhibit-and-to-city-hunter-in-hakata


![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/articles/IMG_3110_100x.jpg)  

January 12, 2023

# Cat's eye 40th anniversary original art exhibit -and to city hunter in Hakata

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3139_100x100.jpg)  
Following the Tokyo exhibition, we held the exhibition in Fukuoka Prefecture, the hometown of Tsukasa Hojo!

Period: November 19 - 30, 2022

Place: Canal City Hakata, Fukuoka, Japan  
  
At the entrance of the venue, there were congratulatory flowers designed in the image of the three Kisugi sisters, Hitomi, Rui and Ai! It is very gorgeous.

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0985_100x100.jpg)

Tsukasa Hojo drew illustrations and messages for customers coming to the venue in Hakata.  
Thank you, Hojo Sensei!

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_100x100.jpg)

The luxurious frames with a strong presence make his beautiful originals stand out even more.

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3109_100x100.jpg)

The complete comics of Cat's Eye, rarely seen storyboards, art supplies and materials used by Tsukasa Hojo were displayed.

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3107_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3106_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3105_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_100x100.jpg)

Original colored drawings and raw manuscripts of Cat's Eye were exhibited.

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3127_100x100.jpg)

We exhibited a carefully selected number of original colored drawings, including rare raw manuscripts of City Hunter - XYZ-.  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3131_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_100x100.jpg)

Only here you can read the entire 83-page "Space Angel" in one sitting!

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_100x100.jpg)

Picture-taking spot in the exhibition hall

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3118_100x100.jpg)

This is the merchandising area.

In addition to various museum goods, art prints hand-signed by Tsukasa Hojo, duplicate manuscripts, and art books were on sale.

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0996_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0994_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_1004_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0989_100x100.jpg)

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0993_100x100.jpg)

If you are interested in art prints of Cat's Eye and City Hunter, please check our online store!

**[Tsukasa Hojo art print collection](https://edition88.com/collections/tsukasa-hojo "Tsukasa Hojo art collection (City Hunter and Cat's Eye)")**