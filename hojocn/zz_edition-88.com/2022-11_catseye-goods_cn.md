source: https://edition-88.com/pages/catseye-goods  

译注1：原文发表于2022年4月～5月期间。后因2022年11月在福冈县的再次画展而有所更改。本文将更改的内容标记出来。    
译注2：(2022年5月)当保存原网页时，会有如下版权信息提示。所以本译文不带原文；且只给出图片链接，而不显示图片。可以点击图片链接或直接访问原网页连接来查看图片。若仍有不妥，请告知；侵则删。  
** ** LEGAL NOTICE ** **  
All site content, including files, images, video, and written content is the property of Edition88.  
Any attempts to mimic said content, or use it as your own without the direct consent of Edition88 may result in LEGAL ACTION against YOU.  
Please exit this area immediately.  
译注3：(2022年11月)现在该网站取消了上述限制，所以本文恢复显示图片。  

----------------------



![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s-eyelogo_ecfc3efb-19c4-4c87-9b47-19ab01623a52_1000x.jpg)  

**版画**  

除了北条司先生的亲笔签名外，它们都是按原画原尺寸制作的，再现了原画的外观和感觉。  
版本数量为国内版200个，国际版180个，数量有限。   

每幅印刷品都是由工匠手工上色的。  
Cat's Eye的版画中，每幅都是用白色颜料手工上色的，这使得原画的凹凸不平（matiere）得到了表现。  

※商品图片仅用于说明目的。 实际的商品可能与之不同。(译注：福冈展无这句话)  

-------------------  


**受注商品**(译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/キャッツ♥アイ))  

右側：「Cat's♥Eye」版画1 （週刊少年Jump 1983年 第36号 封面）  
左側： 「Cat's♥Eye」版画2（週刊少年Jump 1984年 第18号 封面）  
￥59.400（含税）  
●规格：有北条司的亲笔签名，木质框架。  
●总版数：380（国内版：200）。  
※不能选择版本号。  
●框架尺寸：622（长）×471（宽）×20（厚）mm。  
也将从5月13日（星期五）起通过邮购提供。  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_hanga01_800x.jpg?v=1650340444)  


------------------  


**受注商品**(译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/キャッツ♥アイ))  

下：「Cat's♥Eye」 版画3（Jump・Comics 1985年 第14巻／封面）  
右上：「Cat's♥Eye」 版画4（Jump・Comics 1985年 第18巻／封面）  
左上：「Cat's♥Eye」 版画5（週刊少年Jump 1983年 第46号 封面）  
￥44,000（含税）  
●规格：有北条司亲笔签名，木质框架。  
●总版数：380（国内版：200）。  
※版本号不可选。  
●框架尺寸：525（长）×410（宽）×20（厚）mm。  
也将从5月13日（星期五）起通过邮购提供。  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_hanga02_800x.jpg?v=1650341506)  

---------------------  


**受注商品**(译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/キャッツ♥アイ))  

「Cat's♥Eye」 版画6（Jump・Comics 1982年 第2巻／封面）  
￥38,500（含税）  
●规格：有北条司亲笔签名，木质框架。  
●总版数：380（国内版：200）。  
※版本号不可选。  
●框架尺寸：440（长）×364（宽）×20（厚）mm。  
也将从5月13日（星期五）起通过邮购提供。  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_hanga03_800x.jpg?v=1650342199)   

---------------------------


**受注商品**(译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/キャッツ♥アイ))

「Cat's♥Eye」版画7（Jump・Comics 1983年 第5巻／封面）  
￥33,000（含税）  
●规格：有北条司亲笔签名，木质框架。  
●总版数：380（国内版：200）。  
※版本号不可选。  
●框架尺寸：395（长）x 304（宽）x 20（厚） mm。  
也将从5月13日（星期五）起通过邮购提供。  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_hanga04_800x.jpg?v=1650342218)  

------------------------


**新作**(译注：福冈展新增)  

**「Cat's♥Eye」 版画8（週刊少年Jump 1983年 第1・2合并号／封面）**  

￥44,000（含税）  

●规格：有北条司亲笔签名，木质框架。  
●总版数：：380（国内版：200）  
※版本号不可选。  
●框架尺寸：525（长）×410（宽）×20（厚）mm  

**从11月19日（星期六）开始邮购销售。**  
**只在会场展示。**  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hanga-cats_400x.jpg?v=1666780833)  


------------------------

**GOODS**

※产品阵容和产品规格如有变化，恕不另行通知。 在这种情况下，一旦决定，细节将在网站的产品页面和Twitter等处公布。  
※产品图片仅作说明之用。 它们可能与实际产品不同。  
※产品的数量是有限的。 我们无法回答有关库存的询问（例如，当前的库存数量、到货时间等）。 请事先了解这一点。  
※请注意，在展览前或展览期间，可购买的物品数量限制可能会有变化。  
※展会上只接受现金和信用卡（VISA、Mastercard、AMERICANEXPRESS）作为付款方式。(译注：福冈展无这句话)  


- image    
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/499ccc41625b1ac1b81d1c05622d422e.jpg)  

Cat's♥Eye 40周年原画展 目录  
¥2,200（含税）  
※还提供带目录的入场券。  

- image    
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_01.jpg?v=1651481782.jpg?v=1651481782)   

豪华艺术印刷品1（B2）  
¥13,200（含税）  
※该产品将按订单制作。  


- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_10.jpg?v=1651481782)  

豪华艺术印刷品1（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_01.jpg?v=1651481782.jpg?v=1651481782)  

豪华艺术印刷品2（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_02.jpg?v=1651481782)  

豪华艺术印刷品3（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_03.jpg?v=1651481783)  
豪华艺术印刷品4（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_04.jpg?v=1651481782.jpg?v=1651481782)  

豪华艺术印刷品 5（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_05.jpg?v=1651481782.jpg?v=1651481782)  
豪华艺术印刷品6（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_06.jpg?v=1651481782)  
豪华艺术印刷品7（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_07.jpg?v=1651481782)  
豪华艺术印刷品8（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_08.jpg?v=1651481782)  
豪华艺术印刷品9（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_09.jpg?v=1651481782)  
豪华艺术印刷品10（A4）  
¥1,540（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_01_47f72375-7629-489b-ab6e-f26077f21713.jpg?v=1651492833)  
複製原稿1（B2）  
¥4,400（含税）  
※该产品将按订单制作。  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_02.jpg?v=1651492833)  
複製原稿2（B2）  
¥4,400（含税）  
※该产品将按订单制作。  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_03.jpg?v=1651492833)  
複製原稿3（B2）  
¥4,400（含税）  
※该产品将按订单制作。  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_04.jpg?v=1651492833)  
複製原稿4（B2）  
¥4,400（含税）  
※该产品将按订单制作。  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_01.jpg?v=1651479980)  
複製原稿1（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_02.jpg?v=1651479980)  
複製原稿2（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga1.jpg?v=1650535780)  
複製原稿3（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_04.jpg?v=1651479980)  
複製原稿4（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga3.jpg?v=1650535780)  
複製原稿5（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_06.jpg?v=1651479980)  
複製原稿6（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_07.jpg?v=1651479980)  
複製原稿7（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_08.jpg?v=1651479980)  
複製原稿8（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga2.jpg?v=1650535781)  
複製原稿9（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_10.jpg?v=1651479980)  
複製原稿10（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_11.jpg?v=1651479980)  
複製原稿11（B4）  
¥1,100（含税）  
  
- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_12.jpg?v=1651479980)  
複製原稿12（B4）  
¥1,100（含税）  

- image（译注：福冈展无）  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_acryl-stand_1.jpg?v=1651416593)  
亚克力支架（新绘制/3种）  
各¥1,760（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_acrylic.jpg?v=1651420768)  
贸易亚克力支架钥匙架（全8種類）  
各¥880（含税）  
※完整的套装将按订单制作。  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_57mmcanbadge.jpg?v=1650535780)  
交易罐徽章（全13種類）  
各¥440（含税）  
※完整的套装将按订单制作。  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cats_card_3.jpg?v=1651487895)  
艺术品交易卡 （全19種類）  
各¥330（含税）  
※完整的套装将按订单制作。  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_4.jpg?v=1651416415)  
Clear file（新画的）  
¥500（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_5.jpg?v=1651416415)  
Clear file（性感的火爆女郎）  
¥500（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_6.jpg?v=1651416415)  
Clear file（最後のビッグゲーム）  
¥500（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_1.jpg?v=1651416415)  
Clear file（Silhouette）  
¥500（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_2_67f6263f-99df-4584-b3e2-dc7665b1948f.jpg?v=1651477365)  
Clear file（Building）  
¥500（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_3.jpg?v=1651416414)  
Clear file（Window）  
¥500（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_coaster.jpg?v=1651420768)  
Art Tile（全10種類）  
各¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_2.jpg?v=1651416860)  
Mug Cup（描き下ろし）  
¥1,650（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_1.jpg?v=1651416860)  
Mug Cup（瞳）  
¥1,650（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_3.jpg?v=1651416860)  
Mug Cup（セクシーダイナマイトギャルズ）  
¥1,650（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_4.jpg?v=1651416860)  
Mug Cup（最後のビッグゲーム）  
¥1,650（含税）  


- 画像のサンプル  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_sticker.jpg?v=1650535780)  
Die-cut Sticker  
¥330（含税）  


- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_towel2.jpg?v=1651426744)  
手巾（描き下ろし）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_hand_towel.jpg?v=1650535781)  
手巾（瞳）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_big_towel-01.jpg?v=1651416802)  
大毛巾（描き下ろし）  
¥3,850（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_big_towel-02.jpg?v=1651416802)  
大毛巾（Jump）  
¥3,850（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_2.jpg?v=1651416888)  
眼镜湿巾（描き下ろし）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_1.jpg?v=1651416888)  
眼镜湿巾（Climbing）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_3.jpg?v=1651416888)  
眼镜湿巾（Jump）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_pouch_1.jpg?v=1651416826)  
小袋（描き下ろし）  
¥1,650（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_tote_2.jpg?v=1651416738)  
手提袋（描き下ろし）  
¥2,200（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_tote_1.jpg?v=1651416738)  
手提袋（コレクション No.87）  
¥2,200（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_1.jpg?v=1651416946)  
T恤（描き下ろし） M／L  
¥3,850（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_Tshirt2.jpg?v=1650535780)  
T恤（セクシーダイナマイトギャルズ）M／L  
¥3,850（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_6.jpg?v=1651416946)  
T恤（Cat'sEye）M／L  
¥3,850（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_3.jpg?v=1651416946)  
T恤（最後のビッグゲーム）M／L  
¥3,850（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_Tshirt1.jpg?v=1650535780)  
T恤（離南島からの招待状）M／L  
¥3,850（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_5.jpg?v=1651416947)  
T恤（予告状の秘密）M／L  
¥3,850（含税）  
















