source:  
https://edition-88.com/blogs/blog/instagram-campaign15

2023年 3月 21日

# 「キャッツ♥アイ40周年記念原画展」名古屋開催 Instagramフォロー＆いいね キャンペーン  
「Cat's♥Eye40周年記念原画展」名古屋举行 Instagram Follow ＆ 点赞 Campaign  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/HT_nagoya_campaign-banner_instagram_short_1024x1024.jpg)  
「キャッツ♥アイ40周年記念原画展 ～そしてシティーハンターへ～」名古屋開催記念として、EDITION88公式Instagramアカウントをフォロー＆いいねで､抽選で2名様に「北条司直筆サイン入りポスター」が当たるプレゼントキャンペーンを実施いたします｡  
作为「Cat's♥Eye 40周年纪念原画展~然后向着CityHunter~」名古屋举办的纪念，关注EDITION88官方Instagram账号&点赞，将举办通过抽签2人获得“北条司亲笔签名海报”礼物的宣传活动。  

#### **【プレゼント商品】**  
Present商品  

#### **北条司先生 直筆サイン入りポスター**  
北条司先生 亲笔Sined Poster  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cats-nagoya_600x600.jpg)

松坂屋名古屋店にて開催される「キャッツ♥アイ40周年記念原画展～そしてシティーハンターへ～」のメインビジュアルを特製B3ポスターにしました。  
松坂屋名古屋店举办的「Cat's♥Eye 40周年纪念原画展~然后向着CityHunter~」的Main Visual是特制B3海报。  
こちらに北条司先生の直筆サインを入れたものをプレゼントします！  
这里有北条司老师的亲笔签名作为礼物!  

**【応募方法】**  
报名方法  

＜ステップ1＞  
<步骤1>  

EDITION88公式Instagramアカウント（[@theedition88](https://www.instagram.com/theedition88/))をフォロー  
关注EDITION88官方Instagram帐户([@theedition88](https：//www.instagram.com/theedition88/))  

＜ステップ２＞  
<步骤2>  

3月21日（火）10時配信の対象キャンペーン投稿を選び、「いいね！」するだけ  
选择3月21日(周二)10点发送的对象活动投稿，只需点赞!  

**【応募期間】**  
报名时间  

### 2023年3月21日（火）～2023年4月5日（水）23:59

※応募規約に同意いただいた上でご応募ください。  
※请在同意应征条款后应征。  

**【応募規約】**  
报名条款  

株式会社VISION8（以下、「当社」と言います）が主催する「EDITION88 公式Instagram プレゼントキャンペーン 」に応募いただく前に、本規約をよくお読みいただき、同意のうえ、ご応募ください。本キャンペーンに参加された場合、本規約にご同意いただいたものとみなします。  
在报名参加由VISION8有限公司主办的“EDITION88官方Instagram礼物活动”之前，请仔细阅读本条款，并在同意后报名。如果您参加了本活动，则视为您同意本条款。  

**【応募資格】**  
报名资格  

*   応募規約に同意していただいた方  
同意应征章程的人  
*   Instagramアカウントを有しており非公開設定されていない方  
拥有Instagram账号且非公开设定者  
*   EDITION88公式Instagramアカウント（[@theedition88](https://www.instagram.com/theedition88/)）をフォローしている方。プレゼント当選のご連絡はInstagramのDM（ダイレクトメッセージ）にて詳細をご連絡致しますので、公式アカウント「@theedition88」のフォローは外さないでください。  
关注EDITION88官方Instagram账户(@theedition88)。中奖的消息会通过Instagram的DM(直接留言)通知详细情况，请不要删除对官方账号“@theedition88”的关注。  
*   応募期間中に、この規約に同意され、ご応募いただいた方。  
在应征期间内，同意本章程并应征者。  
*   日本国内に在住し、郵便物・宅配物の受け取りが可能な方。  
住在日本国内，可以接收邮件、快递的人。  
*   未成年の方が応募されるには、保護者の方の同意が必要です。  
未成年的人应征的话，必须得到监护人的同意。  

**【当選発表】**  
宣布中奖  

*   応募期間終了後、厳正な抽選の上、当選者の方のみに当アカウント(@theedition88)よりDM差し上げます。  
报名时间结束后，经过严格的抽签，本账号(@theedition88)将向中奖者发送DM。  
*   当選時にフォローを外されている、非公開設定をされている方、受信拒否設定をしている方は当選の対象外となりますので、ご注意ください。  
请注意，当选时被取消关注的非公开设定的人、拒绝接收的设定的人不在当选的范围内。    

**【注意事項】**  

＜キャンペーンについて＞  
<关于宣传活动>  

*   キャンペーンの内容および賞品は、予告なく変更される場合があります。  
活动内容和奖品可能会更改，恕不另行通知。  

＜当選について＞  
关于中奖  

*   同一住所への賞品の発送は1点までとさせていただきます。  
同一地址最多只能发送1份奖品。  
*   当選通知受信後、指定の期限までに、ご連絡先、賞品お届け先等、必要事項を指定の方法でご連絡ください。7日以内にご連絡いただけない場合は当選を無効とさせていただきますのでご注意ください。      
收到中奖通知后，请在指定期限前以指定的方式联系您的联系方式、奖品目的地等必要事项。请注意，如果您不能在7天内与您联系，我们将使您的当选无效。  
*   住所・転居先が不明などでお届けできない場合は、当選を取り消させていただく場合がございます。  
地址、迁居地址不明等无法送达的情况下，可能会取消当选。  
*   当選者は、本権利を他人に譲渡したり、金銭と交換することはできません。  
当选人不得将本权利转让给他人或与金钱交换。  
*   応募方法や抽選方法、当選についてのお問い合わせは受け付けておりません。  
不接受关于应征方法、抽签方法、当选的咨询。  
*   キャンペーンの応募には、Instagramアカウントが必要です。アカウントをお持ちでない方は、Instagram公式サイトよりご取得ください。  
您需要Instagram帐户才能申请活动。如果您没有帐户，请从Instagram官网获取。  
*   該当の投稿を「いいね」していない場合は応募対象外となります。  
如果没有“赞”相应的投稿，则不在应征对象之外。  

＜Instagramについて＞  
<关于Instagram>  

*   Instagramのアカウントを削除したり、「@theedtion88」のフォローを解除すると、当選が無効となります。  
删除Instagram帐户或取消关注“@theedtion88”，中奖无效。
*   同一アカウントであることの確認が困難となるため、期間中にアカウント名を変更した場合は、抽選の対象外とさせていただきます。  
难以确认您是同一帐户，因此，如果您在此期间更改了帐户名称，则不包括抽奖。  
*   第三者がお客さまの書き込み内容を利用したことによって受けた損害や、お客さまが受けた損害については、一切の保証をいたしません。  
对因第三方使用您的书写内容而遭受的损害或您所遭受的损害不作任何保证。  
*   以下の場合、当選対象外になる可能性があります。  
在以下情况下，可能不在当选对象之外：  

*   プロフィール、ユーザー名などを設定していない場合  
如果您没有设置个人资料、用户名等  
*   アカウント作成後、メールアドレスが本人のものか確認をするプロセスを行っていない場合  
创建帐户后，如果没有进行确认电子邮件地址是否属于本人的过程  

**【禁止事項】**

本キャンペーンへの応募に際し、以下の行為を禁止します。  
在参加本活动时，禁止以下行为。  

*   応募規約に違反する行為  
违反应征章程的行为  
*   本キャンペーンの運営を妨げる行為  
妨碍本活动运营的行为  
*   他人に迷惑、不利益、損害または不快感を与える行為  
给他人带来麻烦、不利、损害或不愉快的行为  
*   他人を誹謗中傷し、またはその名誉若しくは信用を毀損する行為  
诽谤中伤他人或者损害他人名誉或者信用的行为  
*   他人の著作権その他の知的財産権を侵害する行為  
侵犯他人著作权或其他知识产权的行为  
*   営利を目的とした情報提供、広告宣伝もしくは勧誘行為  
以盈利为目的提供信息、广告宣传或劝诱行为  
*   当選した商品の転売、オークション等に出品する行為  
转卖、拍卖中奖商品的行为  
*   公序良俗に反する行為  
违反公序良俗的行为  
*   Instagramの利用規約・法令に違反する行為  
违反Instagram使用规章和法令的行为  
*   当社が本キャンペーンの趣旨に沿わないと判断する行為  
本公司判断为不符合本活动宗旨的行为  

**【個人情報の取り扱い】**  
处理个人信息  

応募者より個人情報を取得した場合、個人情報は賞品送付のみに使用し、それ以外には使用しません。  
从应征者那里获得个人信息的情况下，个人信息只用于发送奖品，除此之外不使用。  

**【免責事項】**  
免责声明  

本キャンペーンは「Instagram」を活用したキャンペーンです。Instagramに関連するアプリケーションの動作環境により発生するキャンペーン運営の中断または中止によって生じるいかなる損害についても、弊社が責任を負うものではありません。  
本活动是活用“Instagram”的活动。由于Instagram相关的应用程序的操作环境而导致的活动运营中断或终止所造成的任何损害，本公司并不负责。  

**【問い合わせ先】**  
咨询地址  

本キャンペーンに関するお問い合わせは、edition88@vision8.jpにて受け付けます。なお、抽選方法、当選、賞品についてのお問い合わせは受け付けておりません。  
关于本活动的咨询可以在edition88@vision8.jp上进行。另外，不接受关于抽奖方法、中奖、奖品的咨询。  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

