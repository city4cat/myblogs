source:  
https://edition88.com/collections/tsukasa-hojo

![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/collections/Cat_sEyey_1800x800-01_c050084a-9293-4a61-afaa-cd7d8c987e05_600x.webp)  

# Tsukasa Hojo （北条司）  

Born in Fukuoka Prefecture, Japan, Tsukasa Hojo has produced many hit works such as "Cat's Eye," "City Hunter," "F.COMPO," and "Angel Heart“. Currently, aside from training young manga artists, his delicate and dynamic drawing are also used as models by young these aspirant manga creators. His many fans, on the other hands, are fascinated by his witty, stylish, and rollicking storylines. His delicate and dynamic drawings are a model for young manga artists, and many fans are fascinated by his witty, stylish, and rollicking storylines.  
出生于日本福冈县的北条司，创作了"Cat's Eye"、"City Hunter"、"F.COMPO"和"Angel Heart“等多部热门作品。目前，除了培养年轻的漫画家外，他精致而充满活力的绘画也被这些有抱负的年轻漫画家们作为榜样。另一方面，他的许多粉丝都被他诙谐、时尚和欢乐的故事情节所吸引。他精致而充满活力的绘画是年轻漫画家的典范，许多粉丝都被他诙谐、时尚和欢乐的故事情节所吸引。  

In May 2022, "Cat's ♥ Eye 40th Anniversary Original Art Exhibit - And to City Hunter" was held in Tokyo, displaying a total of about 350 original drawings including rare color manuscripts. In addition, "Space Angel" was specially presented at this exhibition. The Space Angel, one of the roots of Mr. Hojo's creativity, was originally a Tezuka Award entry from 1979, and became a topic of great interest as it was completely redrawn for this exhibition with the composition and storyline remaining the same as they were originally 40 years ago.  
2022年5月，在东京举行了“Cat's♥Eye 40周年原创艺术展 - 然后向着City Hunter”，展出了包括罕见的彩色手稿在内的350多幅原创画作。此外，"Space Angel"也在本次展览中特别展出。Space Angel是北条先生创作的根源之一，它最初是1979年手冢奖的参赛作品，在这次展览中被完全重新绘制，构图和故事情节保持了40年前的原样，这引起了人们极大的兴趣。  

 [
![City Hunter, Art Print #6/ Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_city_02_01_100x.jpg) 
![City Hunter, Art Print #6/ Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_city_02_02_100x.jpg)
](https://edition88.com/products/cityhunter-hanga6)

## [City Hunter, Art Print #6/ Tsukasa Hojo](https://edition88.com/products/cityhunter-hanga6)

From ¥38,500 JPY

 [![City Hunter, Art Print #5/ Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_city_01_01_100x.jpg) ![City Hunter, Art Print #5/ Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_city_01_02_100x.jpg)](https://edition88.com/products/cityhunter-hanga5)

## [City Hunter, Art Print #5/ Tsukasa Hojo](https://edition88.com/products/cityhunter-hanga5)

From ¥38,500 JPY

 [![Cat's Eye, Art Print #10 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_cats_01_01_100x.jpg) ![Cat's Eye, Art Print #10 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_cats_01_02_100x.jpg)](https://edition88.com/products/catseye-hanga10)

## [Cat's Eye, Art Print #10 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga10)

From ¥38,500 JPY

 [![Cat's Eye, Art Print #9 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_cats_02_01_100x.jpg) ![Cat's Eye, Art Print #9 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_cats_02_12_100x.jpg)](https://edition88.com/products/catseye-hanga9-1)

## [Cat's Eye, Art Print #9 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga9-1)

¥59,400 JPY

 [![City Hunter, Art Print #4 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_city04_international_100x.jpg) ![City Hunter, Art Print #4 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_city04_sheet_100x.jpg)](https://edition88.com/products/cityhunter-hanga4)

## [City Hunter, Art Print #4 / Tsukasa Hojo](https://edition88.com/products/cityhunter-hanga4)

¥53,900 JPY

 [![Cat's Eye, Art Print #8 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat08_international_100x.jpg) ![Cat's Eye, Art Print #8 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat08_sheet_100x.jpg)](https://edition88.com/products/catseye-hanga8)

## [Cat's Eye, Art Print #8 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga8)

From ¥38,500 JPY

 [![Cat's Eye, Art Print #7 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat07_01_e8e98c70-7791-49fa-9010-7e400ebd14f5_100x.jpg) ![Cat's Eye Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CT07_45223b76-0017-4f23-b9e2-c0417d84d17f_100x.jpg)](https://edition88.com/products/catseye-hanga07)

## [Cat's Eye, Art Print #7 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga07)

From ¥29,150 JPY

 [![Cat's Eye, Art Print #6 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat06_01_fd230ecb-e4e1-4470-b168-63c28c8576e8_100x.jpg) ![Cat's Eye Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CT06_91a67625-09f2-4521-abbd-7f89815c73d2_100x.jpg)](https://edition88.com/products/catseye-hanga06)

## [Cat's Eye, Art Print #6 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga06)

From ¥34,100 JPY

 [![Cat's Eye, Art Print #5 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat05_01_807dd2ec-80af-4645-8ab9-979025e7418a_100x.jpg) ![Cat's Eye Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CT05_ac708d58-e05a-4035-b78a-f418edb0f60e_100x.jpg)](https://edition88.com/products/catseye-hanga05)

## [Cat's Eye, Art Print #5 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga05)

From ¥38,500 JPY

 [![Cat's Eye, Art Print #4 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat04_01_bd9d9d64-282e-4a3e-a436-0d50d1a1c8a9_100x.jpg) ![Cat's Eye Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CT04_ae0521c8-4e7c-4e71-b34c-f61e1ca57f1c_100x.jpg)](https://edition88.com/products/catseye-hanga04)

## [Cat's Eye, Art Print #4 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga04)

From ¥38,500 JPY

 [![Cat's Eye, Art Print #3 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat03_01_42288a5c-f2d1-466d-adae-e1516792953b_100x.jpg) ![Cat's Eye Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CT03_99b6d9eb-a93a-48e6-bbf4-eb989d141878_100x.jpg)](https://edition88.com/products/catseye-hanga03)

## [Cat's Eye, Art Print #3 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga03)

From ¥38,500 JPY

 [![Cat's Eye, Art Print #2 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat02_01_41235573-8ef8-48e9-9db7-03f87dc029c1_100x.jpg) ![Cat's Eye Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CT02_272a8f40-f141-45bf-8ce0-6d18041c27ae_100x.jpg)](https://edition88.com/products/catseye-hanga02)

## [Cat's Eye, Art Print #2 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga02)

From ¥53,900 JPY

 [![Cat's Eye, Art Print #1 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat01_01_1_ecfb73ca-f37e-461f-babd-d248af9aca46_100x.jpg) ![Cat's Eye Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/cats1_ddd00b2c-05cc-4f3c-9db6-4ef054a7f4fc_100x.jpg)](https://edition88.com/products/catseye-hanga01)

## [Cat's Eye, Art Print #1 / Tsukasa Hojo](https://edition88.com/products/catseye-hanga01)

From ¥53,900 JPY

 [![City Hunter, Art Print #3 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cty03_03_100x.jpg) ![City Hunter Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CH03_100x.jpg)](https://edition88.com/products/cityhunter-hanga3)

## [City Hunter, Art Print #3 / Tsukasa Hojo](https://edition88.com/products/cityhunter-hanga3)

¥53,900 JPY

 [![City Hunter, Art Print #2 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cty02_03_100x.jpg) ![City Hunter Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CH02_100x.jpg)](https://edition88.com/products/cityhunter-hanga2)

## [City Hunter, Art Print #2 / Tsukasa Hojo](https://edition88.com/products/cityhunter-hanga2)

¥53,900 JPY

 [![City Hunter, Art Print #1 / Tsukasa Hojo](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cty01_03_100x.jpg) ![City Hunter Art Prints | Edition88](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/CH01_100x.jpg)](https://edition88.com/products/cityhunter-hanga1)

## [City Hunter, Art Print #1 / Tsukasa Hojo](https://edition88.com/products/cityhunter-hanga1)

¥53,900 JPY

## Certificate of Authenticity （正品证明书）    

All of our prints have a Certificate of Authenticity as proof that the print is genuine.  
我们所有的印刷品都有正品证明书，以证明印刷品是正品。  

[Read more](https://edition88.com/pages/about-us)

![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/files/20220823115629_100x.jpg)
