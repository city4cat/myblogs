https://edition-88.com/pages/catseye-goods

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/cat_s-eyelogo_ecfc3efb-19c4-4c87-9b47-19ab01623a52_100x.jpg)  

## 版画

北条司先生の直筆サインが入る豪華な仕様となっているほか、全て原画原寸サイズで作られており、原画そのままの見た目を再現しています。  
エディション数は国内版200、インターナショナル版180、数量限定で販売いたします。  
除了有北条司老师亲笔签名的豪华规格外，全部都是以原画原尺寸制作的，再现了原画的外观。  
版数量为国内版200，国际版180，限量销售。  
  
版画一枚一枚に職人による手作業が施されています。  
キャッツアイの版画では、ホワイトの絵の具を使って一枚ごとに手彩色を施すことで、原画がもつ凹凸（マチエール）を表現することができました。  
每一幅版画都是工匠手工制作的。  
在猫眼版画中，通过使用白色颜料对每一张进行手工彩绘，可以表现出原画所具有的凹凸(matière)。 （译注：matière：质感）  
  
**新商品は3月21日（火）より名古屋会場およびEDITION88のオンラインストア（EC）にて販売致します。  
名古屋会場では、ここだけでしか購入できない期間限定商品として、版画の額装は全て会場限定仕様となっております。**  
新产品将于3月21日(周二)在名古屋会场和EDITION 88的在线商店(EC)销售。  
在名古屋会场，作为只有在那里才能购买的期间限定商品，版画的裱框全部为会场限定规格。  

### 名古屋会場限定販売  
名古屋会场限定销售  

## 新作

**「キャッツ♥アイ」 版画9（キャッツ♥アイ40周年記念原画展の描き下ろしイラストより）**  
「Cat's♥Eye」版画9(Cat's♥Eye40周年纪念原画展的新插画)  

**￥61,600（税込）**  
￥61600(含税)  

●仕様：北条司直筆サイン入り、木製額入り、Velvet（ブルー）仕様、金属プレート（キャッツカード）付  
式样:北条司亲笔签名、木框、天鹅绒垫子(Blue)式样、附带金属板(Cat'sCard)  
●総エディション数：380（国内版：200）  
总版数:380(国内版:200)  
※エディションナンバーはお選びいただけません。  
※不能选择版号。  
●額サイズ：640×490×厚さ16mm  
框Size:640×490×厚16mm  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/0213_200x.jpg)

### EC限定販売  
EC限量销售  

## 新作

**「キャッツ♥アイ」 版画9（キャッツ♥アイ40周年記念原画展の描き下ろしイラストより）**  
「Cat's♥Eye」版画9(根据Cat's♥Eye40周年纪念原画展的新插画)  

**￥61,600（税込）**  
￥61600(含税)  

●仕様：北条司直筆サイン入り、木製額入り、ベルベットマット（レッド）仕様、金属プレート（キャッツカード）付  
式样:北条司亲笔签名、木框、天鹅绒垫子(Red)式样、附带金属板(Cat'sCard)  
●総エディション数：380（国内版：200）  
总版数:380(国内版:200)  
※エディションナンバーはお選びいただけません。  
※不能选择版号。  
●額サイズ：640×490×厚さ16mm  
框Size:640×490×厚16mm  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/0213_d54e7766-bb37-47bc-95f2-46165856e9e2_200x.jpg)

## 新作

**「キャッツ♥アイ」 版画10（北条司ILLUSTRATIONS1991年ポスター（裏）より）**  
「Cat's♥Eye」版画10(北条司ILLUSTRATIONS 1991年海报(背面))  

**￥44,000（税込）**  
￥44,000（含税）  

●仕様：北条司直筆サイン入り、木製額入り、マット仕様（名古屋会場はダブルマット、EC販売はシングルマットとなります。）  
规格：北条司亲笔签名、木框、垫子规格(名古屋会场为双垫，EC销售为单垫。)  
●総エディション数：380（国内版：200）  
总版本：380(国内版：200)。  
※エディションナンバーはお選びいただけません。  
※不能选择版号。  
●額サイズ：525×410×厚さ20mm  
框尺寸：525×410×厚20mm  

**【名古屋会場限定】ダブルマット仕様　（２枚のマットを使用しています。下段に赤いマットを使用することで、絵を囲み赤いラインが見える仕様になっています。）**  
【名古屋会场限定】双垫规格　（使用2块垫子。下层使用红色垫子，可以看到包围画的红色线条。）  

**【EC限定】シングルマット仕様　（１枚のマットを使用し、すっきりとした印象の額装になっています。）**  
【EC限定】单垫规格　（使用1块垫子，装裱令人印象清爽。）  


![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/0213____1_200x.jpg)

## 会場限定版（マット断面カラー仕様）  (垫子截面颜色规格)  

左：**「キャッツ♥アイ」版画1 （週刊少年ジャンプ 1983年 第36号 表紙より）**  
左：「Cat's♥Eye」版画1(《周刊少年jump》1983年第36号封面)   

右：**「キャッツ♥アイ」版画2（週刊少年ジャンプ 1984年 第18号 表紙より）**  
右：「Cat's♥Eye」版画2(《周刊少年jump》1984年第18号封面)   

**各 ￥59,400（税込）**  
各 ￥59,400（含税）  

●仕様：北条司直筆サイン入り、木製額入り、マット断面カラー仕様  
规格：北条司亲笔签名、木框、垫子断面颜色规格  
●総エディション数：380（国内版：200）  
总版数：380(国内版：200)  
※エディションナンバーはお選びいただけません。  
※不能选择版本号  
●額サイズ：622×471×厚さ20mm  
框Size：622×471×厚20mm  

**＊ECではシングルマット（カラー無し）仕様の商品を只今販売中。**  
＊EC现在正在销售单垫(无彩色)规格的商品。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/cats-hanga1_7297b615-ce5f-438d-bbb3-3eea382d4b52_200x.png)

## 会場限定版（ダブルマット仕様）  
会場限定版（双垫规格）  

左上：**「キャッツ♥アイ」 版画3（ジャンプ・コミックス 1985年 第14巻／表紙カバーより）**  
左上：**「Cat's♥Eye」 版画3(Jump Comics 1985年 第14卷/封面)**  

右上：**「キャッツ♥アイ」 版画4（ジャンプ・コミックス 1985年 第18巻／表紙カバーより）**  
右上：「Cat's♥Eye」 版画4(Jump Comics 1985年 第18卷/封面)  

左下：**「キャッツ♥アイ」 版画5（週刊少年ジャンプ 1983年 第46号 表紙より）**  
左下：「Cat's♥Eye」 版画4(Jump Comics 1983年 第46卷/封面)  

右下：**「キャッツ♥アイ」版画8（週刊少年ジャンプ 1983年 第1・2合併号／表紙カバーより）**  
右下：「Cat's♥Eye」 版画4(Jump Comics 1983年 第1、2合并号/封面)  

**各 ￥44,000（税込）**  
各 ￥44,000（含税）  

●仕様：北条司直筆サイン入り、木製額入り、ダブルマット仕様  
规格：北条司亲笔签名，木框，双垫规格  
●総エディション数：380（国内版：200）  
总版数：380(国内版：200)  
※エディションナンバーはお選びいただけません。  
※不能选择版本号。  
●額サイズ：縦525×横410×厚さ20mm  
框Size：长525×宽410×厚20mm  

**＊ECではシングルマット仕様の商品を只今販売中。**   
＊EC现在正在销售单垫规格的商品。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/cats-hanga3_9b121f24-5dfc-4e78-8b7a-a3f0b60b0875_200x.jpg)

## 会場限定版

左：**「キャッツ♥アイ」 版画6（ジャンプ・コミックス1982年 第2巻／表紙カバーより）**  
左：「Cat's♥Eye」 版画6(Jump Comics 1982年 第2卷/封面)  

**￥38,500（税込）**  

右：**「キャッツ♥アイ」版画7（ジャンプ・コミックス 1983年 第5巻／表紙カバーより）**  
右：「Cat's♥Eye」 版画7(Jump Comics 1983年 第5卷/封面)

**￥33,000（税込）**  
￥33,000（含税）

●仕様：北条司直筆サイン入り、木製額入り、ダブルマット仕様  
规格：北条司亲笔签名，木框，双垫规格  
●総エディション数：380（国内版：200）  
总版数：380(国内版：200)  
※エディションナンバーはお選びいただけません。  
※不能选择版本号。  
●額サイズ  
　左：440×364×厚さ20mm  
　右：395×304×厚さ20mm  
框大小  
　左：440×364×厚20mm  
　右：395×304×厚20mm  

**＊ECではシングルマット仕様の商品を只今販売中。**  
＊EC现在正在销售单垫规格的商品。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/cats-hanga2_27264ab1-e17a-425e-af86-4b1a44eb4eed_200x.png)

## GOODS

※商品ラインナップ、商品の仕様は予告なく変更になる場合がございます。その場合の詳細は決定次第、サイト内の商品ページやTwitter等にて発表します。  
商品阵容、商品规格如有变更，恕不另行通知。这种情况下的详细情况一旦决定，就会在网站内的商品页面和Twitter等上发布。  
※商品画像はイメージです。実際の商品とは異なる場合がございます。  
商品图像仅是图像。可能与实际商品不同。  
※商品の数には限りがあります。在庫に関するお問い合わせ（現在の在庫数や入荷予定等）にはご対応できません。あらかじめご了承ください。  
商品数量有限。无法处理有关库存的查询(目前的库存数量和进货计划等)。请事先谅解。  
※会期前・会期中を問わず購入個数制限を変更する場合がありますのでご了承ください。  
会期前、会期期间都限制变更购买个数，敬请谅解。  


### 名古屋会場およびECにて販売予定 (名古屋会场及EC销售预定)  

## 新商品

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cats-kanban_200x.png)

実物大「喫茶キャッツアイ」 看板  
¥119,900（税込）  
サイズ：幅450×高さ870×奥行500（mm）  
**※受注商品／数量限定で販売予定**  
实物大小的「Cat'sEye咖啡屋」 招牌  
¥119,900(含税)  
尺寸:宽450×高870×进深500 (mm)  
**※接受订单商品/数量限定预定销售**  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cats-artprint1_200x.png)

高級アートプリント11（A4）  
¥1,540（税込）   
高级Art Print 11（A4）  
¥540(含税)  

### 名古屋会場およびECにて販売 (名古屋会场及EC销售)

## 通常商品

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/499ccc41625b1ac1b81d1c05622d422e_100x.jpg)

キャッツ♥アイ40周年原画展　図録  
¥2,200（税込）  
**※図録付き入場券もございます。**  
Cat's♥Eye 40周年原画展 图录  
¥2，200(含税)。  
※也有带图册的入场券。  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_01_100x.jpg)

高級アートプリント1（B2）  
¥13,200（税込）  
**※受注商品**  
※订单商品  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_10_100x.jpg)

高級アートプリント1（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_01_100x.jpg)

高級アートプリント2（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_02_100x.jpg)

高級アートプリント3（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_03_100x.jpg)

高級アートプリント4（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_04_100x.jpg)

高級アートプリント 5（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_05_100x.jpg)

高級アートプリント6（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_06_100x.jpg)

高級アートプリント7（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_07_100x.jpg)

高級アートプリント8（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_08_100x.jpg)

高級アートプリント9（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/A4_09_100x.jpg)

高級アートプリント10（A4）  
¥1,540（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_01_47f72375-7629-489b-ab6e-f26077f21713_100x.jpg)

複製原稿1（B2）  
¥4,400（税込）  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_02_100x.jpg)

複製原稿2（B2）  
¥4,400（税込）  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_03_100x.jpg)

複製原稿3（B2）  
¥4,400（税込）  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B2_04_100x.jpg)

複製原稿4（B2）  
¥4,400（税込）  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_01_100x.jpg)

複製原稿1（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_02_100x.jpg)

複製原稿2（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga1_100x.jpg)

複製原稿3（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_04_100x.jpg)

複製原稿4（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga3_100x.jpg)

複製原稿5（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_06_100x.jpg)

複製原稿6（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_07_100x.jpg)

複製原稿7（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_08_100x.jpg)

複製原稿8（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_fukuseigenga2_100x.jpg)

複製原稿9（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_10_100x.jpg)

複製原稿10（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_11_100x.jpg)

複製原稿11（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_12_100x.jpg)

複製原稿12（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_acrylic_100x.jpg)

トレーディング アクリルスタンドキーホルダー（全8種類）  
各¥880（税込）  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cats_card_3_100x.jpg)

アートトレーディングカード （全19種類）  
各¥330（税込）  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_57mmcanbadge_100x.jpg)

トレーディング 缶バッジ（全13種類）  
各¥440（税込）  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_5_100x.jpg)

クリアファイル（セクシーダイナマイトギャルズ）  
¥500（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_6_100x.jpg)

クリアファイル（最後のビッグゲーム）  
¥500（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_1_100x.jpg)

クリアファイル（Silhouette）  
¥500（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_2_67f6263f-99df-4584-b3e2-dc7665b1948f_100x.jpg)

クリアファイル（Building）  
¥500（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_clearfile_3_100x.jpg)

クリアファイル（Window）  
¥500（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_coaster_100x.jpg)

アートタイル（全10種類）  
各¥990（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_2_100x.jpg)

マグカップ（描き下ろし）  
¥1,650（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_1_100x.jpg)

マグカップ（瞳）  
¥1,650（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_3_100x.jpg)

マグカップ（セクシーダイナマイトギャルズ）  
¥1,650（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_mag_4_100x.jpg)

マグカップ（最後のビッグゲーム）  
¥1,650（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_seye_towel2_100x.jpg)

ハンドタオル（描き下ろし）  
¥880（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_hand_towel_100x.jpg)

ハンドタオル（瞳）  
¥880（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_big_towel-01_100x.jpg)

ビッグタオル（描き下ろし）  
¥3,850（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_big_towel-02_100x.jpg)

ビッグタオル（Jump）  
¥3,850（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_2_100x.jpg)

メガネ拭き（描き下ろし）  
¥990（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_1_100x.jpg)

メガネ拭き（Climbing）  
¥990（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_meganefuki_3_100x.jpg)

メガネ拭き（Jump）  
¥990（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_pouch_1_100x.jpg)

ポーチ（描き下ろし）  
¥1,650（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_tote_2_100x.jpg)

トートバッグ（描き下ろし）  
¥2,200（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_tote_1_100x.jpg)

トートバッグ（コレクション No.87）  
¥2,200（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_1_100x.jpg)

Tシャツ（描き下ろし） M／L  
¥3,850（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_Tshirt2_100x.jpg)

Tシャツ（セクシーダイナマイトギャルズ）M／L  
¥3,850（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_6_100x.jpg)

Tシャツ（Cat'sEye）M／L  
¥3,850（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_3_100x.jpg)

Tシャツ（最後のビッグゲーム）M／L  
¥3,850（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_cats_Tshirt1_100x.jpg)

Tシャツ（離南島からの招待状）M／L  
¥3,850（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cat_s_T-shirt_5_100x.jpg)

Tシャツ（予告状の秘密）M／L  
¥3,850（税込）