https://edition-88.com/pages/cityhunter-goods

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/cityhunterlogo_100x.jpg)  

## 版画

北条司先生の直筆サインが入る豪華な仕様となっているほか、全て原画原寸サイズで作られており、原画そのままの見た目を再現しています。  
エディション数は国内版200、インターナショナル版180、数量限定で販売いたします。  
除了北条司老师的亲笔签名为豪华规格以外，全部制作为原画原尺寸，再现原画的外观。  
版数国内版200、国际版180，限量销售。  
  
版画一枚一枚に職人による手作業が施されています。  
シティーハンターの版画では、一枚ごとにパール絵の具の粒子を吹き付けています。  
見る角度によって光沢が強調され、作品の奥行きが表現されています。  
每一张版画都是由工匠手工完成的。  
在CityHunter的版画中，每一幅都喷涂了珍珠颜料的粒子。  
根据观看角度强调光泽，表现作品的纵深。    

**新商品は3月21日（火）より名古屋会場およびEDITION88のオンラインストア（EC）にて販売致します。  
名古屋会場では、ここだけでしか購入できない期間限定商品として、版画の額装は全て会場限定仕様となっております。**  
新产品将于3月21日(周二)在名古屋会场和EDITION88的在线商店(EC)销售。  
在名古屋会场，作为只有在那里才能购买的期间限定商品，版画的裱框全部为会场限定规格。  

## 新作

**「シティーハンター」 版画5（週刊少年ジャンプ 1988年 第30号 表紙より）**  
「CityHunter」 版画5(周刊少年JUMP 1988年 第30期 封面)  

**￥44.000（税込）**  
￥44.000（含税）  

●仕様：北条司直筆サイン入り、木製額入り、ダブルマット仕様  
规格：北条司亲笔签名、木框、双垫规格  
●総エディション数：380（国内版：200）  
总版数：380(国内版：200)  
※エディションナンバーはお選びいただけません。  
※不能选择版本号。  
●額サイズ：525×410×厚さ20mm  
框Size：525×410×厚20mm   

**【名古屋会場限定】ダブルマット仕様　（２枚のマットを使用しています。下段にブルーのマットを使用することで、絵を囲み青いラインが見える仕様になっています。）**  
[名古屋会场限定]双垫规格 (使用2张垫子。下层使用蓝色垫子，可以看到包围画的蓝色线条。)  

**【EC限定】シングルマット仕様　（１枚のマットを使用し、すっきりとした印象の額装になっています。）**  
[EC限定]单垫规格(使用1张垫子，装裱令人印象清爽。)  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/city-hanga1_200x.png)  

## 新作  

**「シティーハンター」 版画6（週刊少年ジャンプ 1991年 第15号 表紙より）**　
「CityHunter」 版画6(周刊少年JUMP 1991年 第15期 封面)**  

**￥44.000（税込）**  
*￥44.000（含税）  

●仕様：北条司直筆サイン入り、木製額入り、ダブルマット仕様  
规格：北条司亲笔签名、木框、双垫规格  
●総エディション数：380（国内版：200）  
总版数：380(国内版：200)  
※エディションナンバーはお選びいただけません。  
※不能选择版本号。  
●額サイズ：410×525×厚さ20mm  
框Size：410×525×厚20mm  

**【名古屋会場限定】ダブルマット仕様　（２枚のマットを使用しています。下段にグリーンのマットを使用することで、絵を囲み緑のラインが見える仕様になっています。）**  
[名古屋会场限定]双垫规格(使用2张垫子。下层使用绿色的垫子，可以看到包围画的绿色线条。)  

**【EC限定】シングルマット仕様　（１枚のマットを使用し、すっきりとした印象の額装になっています。）**  
[EC限定]单垫规格(使用1张垫子，装裱令人印象清爽。)  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/city-hanga2_200x.png)  

## 会場限定版（ダブルマット仕様）  （双垫规格）   

上：**「シティーハンター」 版画1（週刊少年ジャンプ 1997年 特別編集　SUMMER SPECIAL/巻頭ポスター用イラストより）**  
上：「CityHunter」 版画1(周刊少年JUMP 1997年 特别编辑 SUMMER SPECIAL/卷首海报插图)  

下：**「シティーハンター」版画4（週刊少年ジャンプ 1987年 第11号 扉絵より）**  
下：「CityHunter」 版画4 (周刊少年JUMP 1987年 第11期 扉页)

**各 ￥59.400（税込）**  
各¥59.400(含税)  

●仕様：北条司直筆サイン入り、木製額入り、ダブルマット仕様  
规格：北条司亲笔签名、木框、双垫规格  
●総エディション数：380（国内版：200）  
总版数：380(国内版：200)  
※エディションナンバーはお選びいただけません。  
※不能选择版本号。  
●額サイズ：471×622×厚さ20mm  
框Size：471×622×厚20mm  

**＊ECではシングルマット仕様の商品を只今販売中。**  
＊EC现在正在销售单垫规格的商品。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/city-hanga3_200x.png)

## 会場限定版（ダブルマット仕様） （双垫规格）  

左：**「シティーハンター」 版画2（週刊少年ジャンプ 1989年 第50号 扉絵より）**　
左：「CityHunter」版画2 (周刊少年JUMP 1989年 第50号 扉页)  

右：**「シティーハンター」 版画3（北条司 ILLUSTRATIONS 1991年 付録ポスター用イラストより）**　
右：「CityHunter」版画3(北条司ILLUSTRATIONS 1991年 附录海报插图)    

**各 ￥59.400（税込）**  
各¥59.400(含税)  

●仕様：北条司直筆サイン入り、木製額入り、ダブルマット仕様  
规格：北条司亲笔签名、木框、双垫规格  
●総エディション数：380（国内版：200）  
总版数：380(国内版：200)  
※エディションナンバーはお選びいただけません。  
※不能选择版本号。  
●額サイズ：622×471×厚さ20mm  
框Size：622×471×厚20mm  

**＊ECではシングルマット仕様の商品を只今販売中。**  
＊EC现在正在销售单垫规格的商品。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/city-hanga4_200x.png)  

## GOODS

※商品ラインナップ、商品の仕様は予告なく変更になる場合がございます。その場合の詳細は決定次第、サイト内の商品ページやTwitter等にて発表します。  
商品阵容、商品规格如有变更，恕不另行通知。这种情况下的详细情况一旦决定，就会在网站内的商品页面和Twitter等上发布。  
※商品画像はイメージです。実際の商品とは異なる場合がございます。  
商品图像仅是图像。可能与实际商品不同。  
※商品の数には限りがあります。在庫に関するお問い合わせ（現在の在庫数や入荷予定等）にはご対応できません。あらかじめご了承ください。  
商品数量有限。无法处理有关库存的查询(目前的库存数量和进货计划等)。请事先谅解。  
※会期前・会期中を問わず購入個数制限を変更する場合がありますのでご了承ください。  
会期前、会期期间都限制变更购买个数，敬请谅解。  

### 名古屋会場およびECにて販売予定 (名古屋会场及EC销售预定)  

## 新商品

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cats-artprint2_200x.png)

高級アートプリント1（A4）  
¥1,540（税込）  
豪华Art Print 1(A4)  
¥1，540(含税)  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cats-artprint3_200x.png)

高級アートプリント2（A4）  
¥1,540（税込）  
豪华Art Print 2(A4)  
¥1，540(含税)  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/city-tote1_200x.png)

トートバッグ（bang）  
¥2,200（税込）  
手提包(Bang)  
¥2，200(含税)  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/city-tote2_200x.png)

トートバッグ（bouquet）  
¥2,200（税込）  
手提包（bouquet）  
¥2，200(含税)  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/city-meganefuki1_200x.png)

メガネ拭き（up）  
¥990（税込）  
眼镜布(Up)  
¥990(含税)  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/city-meganefuki2_200x.png)

メガネ拭き（coat）  
¥990（税込）  
眼镜布(coat)  
¥990(含税)  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/city-meganefuki3_200x.png)

メガネ拭き（aim）  
¥990（税込）  
眼镜布(aim)  
¥990(含税)  

### 名古屋会場およびECにて販売 (名古屋会场及EC销售)  

## 通常商品

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga1_100x.jpg)

複製原稿1（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_2_100x.jpg)

複製原稿2（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_3_100x.jpg)

複製原稿3（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_4_100x.jpg)

複製原稿4（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga2_100x.jpg)

複製原稿5（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_6_100x.jpg)

複製原稿6（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga3_100x.jpg)

複製原稿7（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_8_100x.jpg)

複製原稿8（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_9_100x.jpg)

複製原稿9（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_010_100x.jpg)

複製原稿10（B4）  
¥1,100（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_CF1_100x.jpg)

クリアファイル（XYZ/Green）  
¥500（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_CF2_100x.jpg)

クリアファイル（XYZ/Navy）  
¥500（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/city-coaster_100x.jpg)

アートタイル（全8種類）  
各¥990（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/CITY_mag_1_100x.jpg)

マグカップ（XYZ）  
¥1,650（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_hand_towel2_100x.jpg)

ハンドタオル（Darts）  
¥880（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_hand_towel1_100x.jpg)

ハンドタオル（Sniper）  
¥880（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_towel3_100x.jpg)

ハンドタオル（Building）  
¥880（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_megane1_100x.jpg)

メガネ拭き（Night view）  
¥990（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_megane-fuki_100x.jpg)

メガネ拭き（Building）  
¥990（税込）

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_megane3_100x.jpg)

メガネ拭き（Beach）  
¥990（税込）