
## About EDITION88
[About EDITION88, Printing method, PAPER FOR ART PRINTS, and etc](./about.md)


## Cat's Eye 40周年纪念原画展 ～然后向着City Hunter～  

### Links  
[Cat's ♥ Eye 40周年纪念原画展 ～ 并向City Hunter～ 展示内容公開！](https://edition-88.com/blogs/blog/catseye40th-exhibition)  
[「Cat's♥Eye」40周年纪念原画展 Special Interview](https://edition-88.com/blogs/blog/catseye40th-interview)  
[Cat'sEye商品](https://edition-88.com/pages/catseye-goods)  
[CityHunter商品](https://edition-88.com/pages/cityhunter-goods)  

Twitter Campaign:   
https://edition-88.com/blogs/blog/twitter-campaign06  
https://edition-88.com/blogs/blog/twitter-campaign07  

Report:  


### 2022.04，东京千代田展  
展示内容公開: 
[JAP](./2022-10_catseye40th-exhibition.md), 
[CN](./2022-10_catseye40th-exhibition_cn.md), 

Interview: 
[JAP&CN](./2022-04_catseye40th-interview.md), 
[CN(Official)](../zz_weibo.cn/2022-05_ce_40th_interview.md), 

Cat'sEye商品: 
[JAP](./2022-11_catseye-goods.md), 
[CN](./2022-11_catseye-goods_cn.md), 

CityHunter商品: 
[JAP](./2022-11_cityhunter-goods.md), 
[CN](./2022-11_cityhunter-goods_cn.md), 

Report: 
[JAP&CN](./2022-05_catseye40th-report.md), 

Campaign: 
2022-04 Twitter [JAP&CN](./2022-04_twitter-campaign.md), 
2022-05 Twitter [JAP&CN](./2022-05_twitter-campaign.md), 




### 2022.10，福冈博多展  
展示内容公開: 
[JAP](./2022-10_catseye40th-exhibition.md), 
[CN](./2022-10_catseye40th-exhibition_cn.md), 

Cat'sEye商品: 
[JAP](./2022-11_catseye-goods.md), 
[CN](./2022-11_catseye-goods_cn.md), 

CityHunter商品: 
[JAP](./2022-11_cityhunter-goods.md), 
[CN](./2022-11_cityhunter-goods_cn.md), 

Report: 
[JAP](./2022-11_catseye40th-report.md), 
[CN(official)](../zz_weibo.cn/catseye40th-report02(2022-11-29).md), 
[EN](./2022-11_catseye40th-report_en.md), 


### 2023.03，名古屋展  
展示内容公開: 
[JAP](./2023-03_catseye40th-exhibition.md), 
[CN](./2023-03_catseye40th-exhibition_cn.md), 

Interview: 
[EN&CN](./2023-03_catseye40th-interview_en.md), （注：和2022年的访谈信息不完全相同）  

Cat'sEye商品: 
[JAP&CN](./2023-03_catseye-goods.md), 

CityHunter商品: 
[JAP&CN](./2023-03_cityhunter-goods.md), 

International商品： 
[EN&CN](./2023-03_goods_international.md), 

Campaign : 
2023-03 Instagram [JAP&CN](./2023-03-21_instagram-campaign.md), 
2023-03 Twitter [JAP&CN](./2023-03-21_twitter-campaign.md), 

Report: 
(Not given)   






## Links  
- https://edition-88.com/collections/hojotsukasa  
- https://edition88.com/  
- [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
- [DeepL](https://www.deepl.com)  
- [腾讯翻译君](https://fanyi.qq.com/)  
- [有道翻译](https://fanyi.youdao.com)  
- [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

