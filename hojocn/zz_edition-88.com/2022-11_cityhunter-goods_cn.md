source: https://edition-88.com/pages/cityhunter-goods  

译注1：原文发表于2022年4月～5月期间。后因2022年11月在福冈县的再次画展而有所更改。本文将更改的内容标记出来。    
译注2：(2022年5月)当保存原网页时，会有如下版权信息提示。所以本译文不带原文；且只给出图片链接，而不显示图片。可以点击图片链接或直接访问原网页连接来查看图片。若仍有不妥，请告知；侵则删。  
** ** LEGAL NOTICE ** **  
All site content, including files, images, video, and written content is the property of Edition88.  
Any attempts to mimic said content, or use it as your own without the direct consent of Edition88 may result in LEGAL ACTION against YOU.  
Please exit this area immediately.  
译注3：(2022年11月)现在该网站取消了上述限制，所以本文恢复显示图片。  

----------------------



![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunterlogo_1000x.jpg)  


**版画**

除了北条司先生的亲笔签名外，它们都是按原画原尺寸制作的，再现了原画的外观和感觉。  
版本数量为国内版200个，国际版180个，数量有限。   

每件印刷品都是由工匠们手工制作的。  
在City Hunter的版画中，每幅版画都喷上了珍珠漆的颗粒。  
从不同的观察角度看，光泽被强调，这表达了艺术作品的深度。（译注：待校对）  


---------------

受注商品  (译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/シティーハンター))    

「City Hunter」 版画1（週刊少年Jump 1997年 特別編集　SUMMER SPECIAL/摘自封面海报的插图）（译注：待校对）　

￥59.400（含税）  

●规格：北条司亲笔签名，木质框架  
●总版数：380（国内版：200）  
※不能选择版本号  
●框架尺寸：长471 x 宽622 x 厚20 mm  

它还将从5月13日（星期五）起通过邮购方式提供。    

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/01_1_074c4a0a-369e-482a-a2f6-7f2c941da03f_800x.jpg?v=1650285232)  


-------------


受注商品  (译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/シティーハンター))    

「City Hunter」 版画2（週刊少年Jump 1989年 第50号 扉页）　

￥59.400（含税）  

●规格：北条司亲笔签名，木质框架  
●总版数：380（国内版：200）  
※不能选择版本号  
●框架尺寸：长622 × 宽471 × 厚20 mm  

它还将从5月13日（星期五）起通过邮购方式提供。  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/01_1_800x.jpg?v=1650285109)  

---------------


受注商品  (译注：福冈展时，此条目移至[View All Products](https://edition-88.com/collections/シティーハンター))    

「City Hunter」 版画3（北条司 ILLUSTRATIONS 1991年 来自附录海报的插图）　

￥59.400（含税）  

●规格：有北条司亲笔签名，木质框架  
●总版数：380（国内版：200）  
※不能选择版本号  
●框架尺寸：长622 × 宽471 × 厚20mm    

它还将从5月13日（星期五）起通过邮购方式提供。  

![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/03_1_800x.jpg?v=1650285192)  

--------------


新作 (译注：福冈展新增)

「CityHunter」 版画4（週刊少年Jump 1987年 第11号　扉页）

￥59.400（含税）

●规格：有北条司亲笔签名，木质框架
●总版数：380（国内版：200）  
※不能选择版本号  
●框架尺寸：长471×宽622×厚20mm

从11月19日（星期六）起可通过邮购获得。  
该展览只在会场展出。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/files/hanga-city_400x.jpg)  

--------------

**GOODS**

※产品阵容和产品规格如有变化，恕不另行通知。 在这种情况下，一旦决定，细节将在网站的产品页面和Twitter等处公布。  
※产品图片仅作说明之用。 它们可能与实际产品不同。  
※产品的数量是有限的。 我们无法回答有关库存的询问（例如，当前的库存数量、到货时间等）。 请事先了解这一点。  
※请注意，在展览前或展览期间，可购买的物品数量限制可能会有变化。  
※展会上只接受现金和信用卡（VISA、Mastercard、AMERICANEXPRESS）作为付款方式。(译注：福冈展无这句话)  


- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga1.jpg?v=1650535781)  
複製原稿1（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_2.jpg?v=1651475748)  
複製原稿2（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_3.jpg?v=1651475748)  
複製原稿3（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_4.jpg?v=1651475749)  
複製原稿4（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga2.jpg?v=1650535781)  
複製原稿5（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_6.jpg?v=1651475749)  
複製原稿6（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_fukuseigenga3.jpg?v=1650535781)  
複製原稿7（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_8.jpg?v=1651475749)  
複製原稿8（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_9.jpg?v=1651475749)  
複製原稿9（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/B4_cty_010.jpg?v=1651475752)  
複製原稿10（B4）  
¥1,100（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_CF1.jpg?v=1650535781)  
Clear File（XYZ/Green）  
¥500（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_CF2.jpg?v=1650535784)  
Clear File（XYZ/Navy）  
¥500（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/city-coaster.jpg?v=1651475727)  
Art Tile（全8種類）  
各¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/CITY_mag_1.jpg?v=1651425547)  
Mug Cup（XYZ）  
¥1,650（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_hand_towel2.jpg?v=1650535781)  
手巾（Darts）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_hand_towel1.jpg?v=1650535784)  
手巾（Sniper）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_towel3.jpg?v=1651426744)  
手巾（Building）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_megane1.jpg?v=1651426744)  
眼镜湿巾（Night view）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/houjouten_EC_shouhin-gazou_city_megane-fuki.jpg?v=1650535781)  
眼镜湿巾（Building）  
¥880（含税）  

- image  
![image](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/cityhunter_megane3.jpg?v=1651426744)  
眼镜湿巾（Beach）  
¥880（含税）  



























