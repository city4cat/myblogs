
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 人物右脸对应图片左侧，人物左脸对应图片右侧。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  



# FC的画风-面部-眉毛(Eyebrow)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96885))  

## 词义辨析  
考虑到参考资料7[^7]可信度更高，所以本文采纳其中的英文用词。  

- 当"眉毛"一词单独使用时，为单词"eyebrow"而非"brow"。  
- 当"眉毛"一词和其他词连用并组成术语时，有时使用"brow"，如：brow area, browbone；但有时使用"eyebrow"，如：eyebrow angle。  

## 如何描述眉毛  
先了解一下目前人们是如何表述眉毛的，之后再使用这些方法、术语来描述FC里角色的眉毛。

### 眉毛的Mark点  
有论文[^1]把眉毛放在眼部研究，给出了相应的mark点:    
![](img/BiometricStudyOfEyelidShape...Fig2.jpg)  
(Landmarks or fiducials for our photogrammetric analysis([1]里的Fig. 2))  

Rp, right pupil;  
Lp, left pupil;  
ML, most medial point of limbus;  
LL, most lateral point of limbus;  
Rzy, right zygion;  
Lzy, left zygion;  
en, endocanthion;  
ex, exocanthion;  
Ren, right endocanthion;  
Len, left endocanthion;  
pi, palpebrae inferius;  
ps, palpebrae superius;  
Uepi, midpoint between en and ps;  
Lepi, midpoint between en and pi;  
Ucan, midpoint between ps and ex;  
Lcan, midpoint between pi and ex;  
es, eyelid sulcus;  
br1, lowest meeting point of eyebrow to vertical line from en;  
br2, lowest meeting point of eyebrow to vertical line from Uepi;  
br3, lowest meeting point of eyebrow to vertical line from ps;  
br4, lowest meeting point of eyebrow to vertical line from Ucan;  
br5, lowest meeting point of eyebrow to vertical line from ex;  
al, most lateral point of alar curvature;  
Mb, most medial point of eyebrow;  
Hb, highest point of eyebrow;    
Lb, most lateral point of eyebrow  

从以上的mark点关系可以看出，"眼部的一些mark点"决定了"眉毛的多数mark点"：  
en/ps/ex决定了en/Uepi/ps/Ucan/ex，进而决定了br1/br2/br3/br4/br5  
    

### 眉毛的结构  
解剖学上，眉毛分为3部分[^7]：head（眉头）, body（眉腰）, tail（眉尾）。  
![](./img/Form-of-the-Head-and-Neck_brow-structure.jpg)  
其与上述mark点的对应关系如下：  

- head: Mb;  
- body: br1, br2, br3, br4;  
- tail: Hb, Lb;  

其他参考资料中有眉毛结构的其他分类方法[^6]。  

### 性别上的差异  
"The male and female eyebrows differ in both shape and position. The typical female eyebrow is positioned above the orbital rim; it is thinner and has a pointy tail. The male brow is flatter and fuller which runs over the orbital rim."[^7]  
(男性和女性的眉毛在形状和位置上都不同。典型的女性眉毛位于眼眶边缘之上;它更细，有一个尖尾巴。男性的眉毛更平更浓，超过了眼眶边缘。)  

#### 位置差异[^7]  
下图左列为女性，右列为男性：  
![](./img/eyebrow-female0.jpg) ![](./img/eyebrow-male0.jpg)  
![](./img/eyebrow-female1.jpg) ![](./img/eyebrow-male1.jpg)  
![](./img/eyebrow-female2.jpg) ![](./img/eyebrow-male2.jpg)  

#### 形状差异  
参考资料7[^7]中将眉毛分为如下形状：    
Female: Rounded, Curved, SoftAngled, Angled, Upward, Flat;     
Male: Rounded, Angled, Flat;     

![](./img/brow-female-rounded.jpg)
![](./img/brow-female-curved.jpg)
![](./img/brow-female-soft-angled.jpg)
![](./img/brow-female-angled.jpg)
![](./img/brow-female-upward.jpg)
![](./img/brow-female-flat.jpg)

![](./img/brow-male-rounded.jpg)
![](./img/brow-empty.jpg)
![](./img/brow-empty.jpg)
![](./img/brow-male-angled.jpg)
![](./img/brow-empty.jpg)
![](./img/brow-male-flat.jpg)

注：不同的参考资料眉毛形状的分类方法不同[^2] [^3] [^4] [^5]。


## 本文使用的方法  
- 相比其他参考资料，参考资料[7][^7]可能更权威，所以采用其眉毛形状分类标准和术语。  

- 将眉毛(不同部分)粗细程度分为5类：  
7(Extreme Thick/极粗) <- 6（Very Thick/非常粗）<- 5（Thick/粗）<- 4（Regular/普通粗细）-> 3（Thin/细）->2（Very Thin/非常细）->1(Extreme Thin/极细)。  

- 眉毛的记法的格式：  [Shape][HeadThickness][BodyThickness][TailThickness]
若某眉毛眉形为Flat，其眉头普通粗细、眉腰细、眉尾极细，则可将其记为： Flat431。  

- 因为比较角色的眉毛时如下难度：  
  1.角色常常有各种表情，眉毛随之变化；（或者，只处理无表情的角色镜头）；   
  2.该作品里经常出现的一种情况是：画面里某角色的左右眉毛不对称（甚至是左右脸的五官不对称）；  
所以，假设：角色的左侧眉和右侧眉对称。在此假设下，当角色的左、右眉都便于本文使用时，则选择其左眉（因为多数情况下角色发型向右脸偏，所以左侧眉毛完全被显露）；当其左眉不便使用时（例如，被部分遮挡），则将其右眉水平反转为左眉后再使用。（注：角色自身的左侧、右侧，即分别对应画面的右侧和左侧，以下同。）  
因为后续工作基于上述该假设，所以本文只能算是一个粗糙的比较。  


## 角色的眉毛  
- **雅彦(Masahiko)-(正面)眉**：  
    - basis（基准，以下同）:  
        - 图片：03_131_4，13_116_3，13_116_4，13_118_6，13_139_1，13_157_0，14_149_5，14_283_4， 
        - 特点：成组的图片数量最多。SoftAngled443。      
        - 这些图片叠加后的效果：  
        ![](img/brow_front_masahiko_basis.png)   

    - tail-thick:  
        - 图片：01_023_4, 01_081_5,02_063_4   
        - 特点：比basis组的眉尾粗。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_masahiko_tail-thick.png)  



- **紫苑(Shion)-(正面)眉**：  
    - basis:  
        - 图片：01_042_0, 01_047_0, 03_094_2, 08_096_2, 09_083_2,  14_282_1, 11_043_7
        - 特点：成组的图片数量最多。 Flat333。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shion_basis.png)   
        - 和雅彦basis比较，雅彦的眉粗。  

    - tail-low:  
        - 图片：02_119_6, 10_008_6, 12_104_1, 12_104_3, 14_281_6, 04_064_6, 11_133_2,     
        - 特点：比basis组的眉尾(稍微)低。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shion_tail-low.png)  

    - -Hb:  
        - 图片：03_003_0，07_097_0  
        - 特点：Hb点靠内。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shion_-Hb.png)  

    - curved-middle:  
        - 图片：03_003_0, 04_015_5,  
        - 特点：眉腰弯曲。眉型：Rounded。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shion_curved-middle.png)  


- **塩谷/盐谷(Shionoya, 简记为Shya)(紫苑男装)-(正面)眉**：  
盐谷多数带帽子，遮盖了眉毛。所以，可选择的镜头不多。所以，这里虽然作了比较，但可信度不高。  
    - basis:  
        - 图片：12_143_0，10_125_0，14_067_4，
        - 特点：成组的图片数量最多。 眉型：SoftAngled442。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_shya_basis.png)   
        - 该组(红色)和雅彦basis比较、和紫苑basis比较。  
        ![](img/brow_front_shya_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_shya_basis_vs_shion_basis.jpg) 


- **雅美(Masami)(雅彦女装)-(正面)眉**：  
选择正面、无表情镜头，各自差异大，没有成组的。  


- **若苗空(Sora)-正面**  
    - basis:  
        - 图片：06_135_2, 02_011_1
        - 特点：成组的图片数量最多。 眉型：Angled553。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_sora_basis.png)   
        - 该组(红色)和雅彦basis比较。（该组眉毛更粗、更直、眉腰更长）   
        ![](img/brow_front_sora_basis_vs_masahiko_basis.jpg)  
        - 该组(红色)和紫苑basis比较。（该组眉毛更粗、更直、眉腰更长，Hb点更高）  
        ![](img/brow_front_sora_basis_vs_shion_basis.jpg)  


- **若苗紫(Yukari)-正面**  
对比该角色的右脸眉毛（因为发型左偏分）。  
    - basis:  
        - 图片：10_057_0, 01_005_0, 05_042_1, 05_092_2, 05_183_6, 06_135_2, 10_035_3, 08_017_5,  
        - 特点：成组的图片数量最多。 眉型：Flat443。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_basis.png)   
        - 该组(红色)水平翻转并下移后，和雅彦basis比较（该组眉毛更粗、更直、眉腰更长）、和紫苑basis比较:   
        ![](img/brow_front_yukari_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_yukari_basis_vs_shion_basis.jpg)  

    - Hb+:  
        - 图片：07_059_3, 09_155_5, 10_020_4,   
        - 特点：比basis组的Hb点(眉峰)高。 眉型：Flat。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_Hb+.png)   

    - Hb-:  
        - 图片：08_011_5, 02_189_0，    
        - 特点：比basis组的Hb点(眉峰)低。 眉型：Flat。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_Hb-.png)  

    - Hb-_Lb-out:  
        - 图片：08_011_5, 02_189_0，    
        - 特点：比basis组的Hb点(眉峰)低，Lbd点突出(眉尖长)。 眉型：Flat。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_Hb-_Lb-out.png)  

    - angle:  
        - 图片：06_111_0, 01_025_4，02_004_4    
        - 特点：比basis组的眉峰尖角明显。 眉型：Flat。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_angle.png)  

    - thick:  
        - 图片：02_146_4，06_092_0，      
        - 特点：比basis组的眉粗。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_thick.png)  

    - thin:  
        - 图片：01_124_1，02_000a        
        - 特点：比basis组的眉细。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yukari_thin.png)  


- **浅冈叶子(Yoko)-正面**  
    - basis:  
        - 图片：13_172_0, 13_142_1, 08_152_6, 03_041_2,  
        - 特点：成组的图片数量最多。 眉型：SoftAngled332。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_basis.png)   
        - 该组(红色)平移后和雅彦basis比较、和紫苑basis比较。对比看出：该角色的眉毛细:   
        ![](img/brow_front_yoko_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_yoko_basis_vs_shion_basis.jpg)  

    - curve:  
        - 图片： 06_007_3, 05_160_2, 05_097_5,  
        - 特点： 眉型Angle。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_curve.png)   

    - -Hb:  
        - 图片： 02_092_5, 02_065_0  
        - 特点： Hb点低（Lb随之也低）。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_-Hb.png)   

    - +Hb:  
        - 图片： 10_119_3，05_142_1，13_116_3，     
        - 特点： Hb点高（Lb随之也高）。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_+Hb.png)   

    - -Lb:  
        - 图片： 10_119_3，05_142_1，13_116_3，     
        - 特点： Lb点低（Hb稍低）。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoko_-Lb.png)   


- **浩美(Hiromi)-正面**  
该角色很多镜头里眉毛被头发遮住了一半。采用右脸眉毛。    
    - basis:  
        - 图片：09_043_4, 05_163_0，04_091_2，    
        - 特点：成组的图片数量最多。 眉型：Rounded222。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_hiromi_basis.png)   
        - 该组(红色)水平翻转后和雅彦basis比较、和紫苑basis比较。对比看出：该角色眉毛细且上挑:   
        ![](img/brow_front_hiromi_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_hiromi_basis_vs_shion_basis.jpg)  


- **熏(Kaoru)-正面**  
采用右脸眉毛。眉毛边沿不整齐(有毛刺)。    
    - basis:  
        - 图片：08_062_2, 08_004_0, 08_036_5, 07_193_5,   
        - 特点：成组的图片数量最多。 眉型：SoftAngled333。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kaoru_basis.png)   
        - 该组(红色)水平翻转、上移后和雅彦basis比较、和紫苑basis比较。该角色眉毛稍细、稍上扬(Hb点高):   
        ![](img/brow_front_kaoru_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_kaoru_basis_vs_shion_basis.jpg)  


- **江岛(Ejima)-正面**  
没有可以成组的镜头。   


- **辰巳(Tatsumi)-正面**  
没有可以成组的镜头。  


- **导演-正面**  
    - basis:  
        - 图片：03_110_2, 03_066_4,   
        - 特点：成组的图片数量最多。 眉型：Rounded123(三角)。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_director_basis.png)   
        - 该组(红色)和雅彦basis比较。该角色眉毛短:   
        ![](img/brow_front_director_basis_vs_masahiko_basis.jpg)  


- **早纪(Saki)-正面**  
    - basis:  
        - 图片：09_175_1, 09_174_4, 09_152_3,     
        - 特点：成组的图片数量最多。 眉型：Rounded322。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_saki_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较。该角色眉毛细:   
        ![](img/brow_front_saki_basis_vs_masahiko_basis.jpg)  


- **真琴(Makoto)-正面**  
    - basis:  
        - 图片：11_035_0, 04_176_1, 06_005_6,     
        - 特点：成组的图片数量最多。 眉型：Rounded333。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_makoto_basis.png)   
        - 该组(红色)和雅彦basis比较、和紫苑basis比较。该角色眉毛细:   
        ![](img/brow_front_makoto_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_makoto_basis_vs_shion_basis.jpg)  

    - asymmetry:  
        - 图片：08_110_0, 07_072_0    
        - 特点：左脸眉毛与basis组一致。左右眉不对称。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_makoto_asymmetry.png)   


- **摄像师-正面**  
    - basis:  
        - 图片：07_104_1, 14_171_4, 12_148_0, 10_067_3     
        - 特点：成组的图片数量最多。 Angled245(三角)。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_cameraman_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较:   
        ![](img/brow_front_cameraman_basis_vs_masahiko_basis.jpg)  
    

- **浅葱(Asagi)-正面**  
采用右脸的眉毛（因为左脸眉毛被头发遮住一半）  
    - basis:  
        - 图片：14_250_1, 14_234_0, 14_123_0,      
        - 特点：成组的图片数量最多。 Rounded222。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_asagi_basis.png)   
        - 该组(红色)水平翻转、上移后，和雅彦basis比较、和紫苑basis比较、和紫basis(水平翻转)比较、和早纪basis比较。该角色眉毛细:   
        ![](img/brow_front_asagi_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_asagi_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_asagi_basis_vs_yukari_basis.jpg) 
        ![](img/brow_front_asagi_basis_vs_saki_basis.jpg)  

    - soft-angle:  
        - 图片：14_057_0, 14_039_0     
        - 特点：。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_asagi_soft-angle.png)   

    - Flat:  
        - 图片：14_185_2, 14_169_5     
        - 特点：。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_asagi_straight.png)   

    - 14_067_6，类似basis， 但眉头更粗、像蝌蚪。    
        ![](img/14_067_6__mod.jpg)   


- **和子(Kazuko)-正面**  
    - basis:  
        - 图片：08_109_3, 07_075_0，      
        - 特点：成组的图片数量最多。 SoftAngled443。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kazuko_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和空basis比较:   
        ![](img/brow_front_kazuko_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_kazuko_basis_vs_sora_basis.jpg)  

    - basis(man):  
        - 图片：08_120_0，08_120_0,        
        - 特点：男装时，成组的图片数量最多。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kazuko_basis(man).png)   
        - 该组(红色)上移后，和雅彦basis比较、和空basis比较。该组眉毛粗:   
        ![](img/brow_front_kazuko_basis(man)_vs_masahiko_basis.jpg) 
        ![](img/brow_front_kazuko_basis(man)_vs_sora_basis.jpg)  

    - 女装时比男装时眉毛稍细。但有一个例外：08_141_3。  
        ![](img/08_141_3__.jpg) 
        ![](img/08_141_3__crop0.jpg)  


- **爷爷-正面**  
待完成  


- **横田进(Susumu)-正面**  
采用右脸的眉毛（因为左脸眉毛被头发遮住一半）。  
    - basis:  
        - 图片：03_019_0, 03_012_5,     
        - 特点：成组的图片数量最多。 Flat332。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_susumu_basis.png)   
        - 该组(红色)翻转、右上移后，和雅彦basis比较、和空basis比较。该组眉毛稍细：     
        ![](img/brow_front_susumu_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_susumu_basis_vs_sora_basis.jpg)   


- **宪司(Kenji)-正面**  
    - basis:  
        - 图片：04_051_4, 04_035_1，04_014_1    
        - 特点：成组的图片数量多、表情较为平静。 Angled567。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kenji_basis.png)   
        - 该组(红色)翻转、右上移后，和雅彦basis比较、和空basis比较。该组眉毛稍粗：     
        ![](img/brow_front_kenji_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_kenji_basis_vs_sora_basis.jpg)   

    - anger:  
        - 图片：03_189_5，04_010_0，04_039_5，    
        - 特点：怒、喜。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_kenji_anger.png)  


- **顺子(Yoriko)-正面**  
    - basis:  
        - 图片：03_155_5, 04_039_0, 03_172_0,      
        - 特点：成组的图片数量最多。 Angled332。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoriko_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较、和紫basis比较。该组眉毛较细:   
        ![](img/brow_front_yoriko_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_yoriko_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_yoriko_basis_vs_yukari_basis.jpg)  

    - soft-angle:  
        - 图片：03_147_1, 10_173_3     
        - 特点： 眉型：soft-angle。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_yoriko_soft-angle.png)  


- **森(Mori)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：10_108_4, 10_102_1,      
        - 特点：成组的图片数量最多。 SoftAngled221。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_mori_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较、和紫basis比较。该组眉毛较细:   
        ![](img/brow_front_mori_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_mori_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_mori_basis_vs_yukari_basis.jpg)  
        - 该组(红色)上移后，和早纪basis比较、和浅葱basis比较：  
        ![](img/brow_front_mori_basis_vs_saki_basis.jpg) 
        ![](img/brow_front_mori_basis_vs_asagi_basis.jpg)  


- **叶子母-正面**  
    - basis:  
        - 图片：13_161_3, 13_136_4_        
        - 特点：成组的图片数量最多。 SoftAngled332。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_ykma_basis.png)   
        - 该组(红色)翻转、上移后，和雅彦basis比较、和紫苑basis比较、和紫basis比较。该组眉毛较细：  
        ![](img/brow_front_ykma_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_ykma_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_ykma_basis_vs_yukari_basis.jpg)  
        - 和叶子basis比较:   
        ![](img/brow_front_ykma_basis_vs_yoko_basis.jpg)  


- **理沙(Risa)-正面**  
采用右脸的眉毛（因为左脸眉毛被头发遮住一半）  
    - basis:  
        - 图片：12_024_6, 12_025_0     
        - 特点：成组的图片数量最多。 Rounded431。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_risa_basis.png)   
        - 该组(红色)水平翻转、上移后，和雅彦basis比较、和紫苑basis比较、和紫basis(水平翻转)比较:     
        ![](img/brow_front_risa_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_risa_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_risa_basis_vs_yukari_basis.jpg)  


- **美菜(Mika)-正面**  
    - basis:  
        - 图片：12_033_5, 12_008_6，     
        - 特点：成组的图片数量最多。 SoftAngled211。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_mika_basis.png)   
        - 该组(红色)和雅彦basis比较、和紫苑basis比较、和紫basis(水平翻转)比较。该组眉毛较细：  
        ![](img/brow_front_mika_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_mika_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_mika_basis_vs_yukari_basis.jpg)  


- **齐藤茜(Akane)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：06_163_4, 06_145_7, 06_144_5       
        - 特点：成组的图片数量最多。 Angled231或Flat231(三角)。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_akane_basis.png)   
        - 该组(红色)翻转、上移后，和雅彦basis比较、和紫苑basis比较、和紫basis比较:   
        ![](img/brow_front_akane_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_akane_basis_vs_shion_basis.jpg)  

    - asymmetry:  
        - 图片：06_157_1, 07_099_3      
        - 特点：两边的眉毛不对称。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_akane_asymmetry.png)   


- **仁科(Nishina)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：14_028_6, 14_008_7, 14_014_5,   
        - 特点：成组的图片数量最多。 SoftAngled332。   
        - 这些图片叠加后的效果：  
        ![](img/brow_front_nishina_basis.png)  
        - 该组(红色)翻转、上移后，和雅彦basis比较、和空basis比较。眉毛一样粗:    
        ![](img/brow_front_nishina_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_nishina_basis_vs_sora_basis.jpg)  

    - short-flat:  
        - 图片：12_077_0, 14_009_5  
        - 特点： 眉毛短、平、粗。   
        - 这些图片叠加后的效果：  
        ![](img/brow_front_nishina_short-flat.png)  


- **葵(Aoi)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：06_081_3, 09_101_6, 06_042_1,         
        - 特点：成组的图片数量最多。 Upward444。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_aoi_basis.png)   
        - 该组(红色)翻转、上移后，和雅彦basis比较:   
        ![](img/brow_front_aoi_basis_vs_masahiko_basis.jpg)  


- **奶奶-正面**  
未处理。  


- **文哉(Fumiya)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：12_040_3, 12_012_3, 12_010_3         
        - 特点：成组的图片数量最多。 Angled243或Flat243(三角)。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_fumiya_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和摄像师basis比较：  
        ![](img/brow_front_fumiya_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_fumiya_basis_vs_cameraman_basis.jpg)  


- **齐藤玲子(Reiko)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：01_163_3, 01_169_6,          
        - 特点：成组的图片数量最多。 Flat222。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_reiko_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较、和早纪basis(水平翻转)比较。该组眉毛较细：  
        ![](img/brow_front_reiko_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_reiko_basis_vs_shion_basis.jpg) 
        ![](img/brow_front_reiko_basis_vs_saki_basis.jpg)  


- **松下敏史(Matsushita松下 Toshifumi敏史, 简记为Matsu)-正面**  
采用角色左脸的眉毛。  
    - basis:  
        - 图片：09_072_5, 09_076_5,          
        - 特点：成组的图片数量最多。 愤怒。因为眉毛有动作，所以或许不适合使用。Angled456。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_matsu_basis.png)   
        - 该组(红色)翻转、下移后，和雅彦basis比较。该组眉毛较粗：  
        ![](img/brow_front_matsu_basis_vs_masahiko_basis.jpg)  


- **章子(Shoko)-正面**  
待完成  


- **阿透(Tooru)-正面**  
没有可以成组的镜头。12_012_3显示的眉型：Flat，形状是三角形。  
![](img/12_012_3__.jpg) 
![](img/12_012_3__crop0.jpg)  


- **京子(Kyoko)-正面**  
没有可以成组的镜头。  


- **一马(Kazuma)-正面**  
待完成  


- **一树(Kazuki)-正面**  
没有可以成组的镜头。 12_012_3和雅彦basis比较。基本一致，眉尾稍粗、稍长：  
![](img/brow_front_kazuki_12_012_3.png) 
![](img/brow_front_kazuki_12_012_3_vs_masahiko_basis.jpg)  


- **千夏(Chinatsu)-正面**  
采用右脸的眉毛。  
    - basis:  
        - 图片：12_141_0, 14_016_2,          
        - 特点：成组的图片数量最多。 Rounded321。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_chinatsu_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较。眉腰倾斜度大：  
        ![](img/brow_front_chinatsu_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_chinatsu_basis_vs_shion_basis.jpg)  


- **麻衣(Mai)-正面**  
    - basis:  
        - 图片：12_159_3, 12_141_0,          
        - 特点：成组的图片数量最多。 Rounded222。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_mai_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较、和早纪basis(水平翻转)比较。眉腰倾斜度大：  
        ![](img/brow_front_mai_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_mai_basis_vs_shion_basis.jpg)  


- **绫子(Aya)-正面**  
    - basis:  
        - 图片：02_177_3, 02_189_4,          
        - 特点：成组的图片数量最多。 Angled542。    
        - 这些图片叠加后的效果：  
        ![](img/brow_front_aya_basis.png)   
        - 该组(红色)上移后，和雅彦basis比较、和紫苑basis比较。眉腰倾斜度大：  
        ![](img/brow_front_aya_basis_vs_masahiko_basis.jpg) 
        ![](img/brow_front_aya_basis_vs_shion_basis.jpg)  


- **典子(Tenko)-正面**  
没有可以成组的镜头。  


- **古屋公弘(Kimihiro)-正面**  
没有可以成组的镜头。 09_051_6和雅彦basis比较。基本一致，眉尾稍粗、稍长：  
![](img/brow_front_kimihiro_09_051_6.png) 
![](img/brow_front_kimihiro_09_051_6_vs_masahiko_basis.jpg)  


- **古屋朋美(Tomomi)-正面**  
没有可以成组的镜头。  


- **八不(Hanzu)-正面**  
没有可以成组的镜头。   


- **哲(Te)-正面**  
没有可以成组的镜头。  


- 排列如下(角色左眉, 放大4倍):  
![](img/brow_front_x4/brow_front_masahiko.jpg) 
![](img/brow_front_x4/brow_front_masami.jpg) 
![](img/brow_front_x4/brow_front_shion.jpg) 
![](img/brow_front_x4/brow_front_shya.jpg) 
![](img/brow_front_x4/brow_front_sora.jpg) 
![](img/brow_front_x4/brow_front_yukari.jpg) 
![](img/brow_front_x4/brow_front_yoko.jpg) 
![](img/brow_front_x4/brow_front_hiromi.jpg) 
![](img/brow_front_x4/brow_front_kaoru.jpg) 
![](img/brow_front_x4/brow_front_ejima.jpg) 
![](img/brow_front_x4/brow_front_tatsumi.jpg) 
![](img/brow_front_x4/brow_front_makoto.jpg) 
![](img/brow_front_x4/brow_front_director.jpg) 
![](img/brow_front_x4/brow_front_saki.jpg) 
![](img/brow_front_x4/brow_front_asagi.jpg) 
![](img/brow_front_x4/brow_front_cameraman.jpg) 
![](img/brow_front_x4/brow_front_yoriko.jpg) 
![](img/brow_front_x4/brow_front_mori.jpg) 
![](img/brow_front_x4/brow_front_kazuko.jpg) 
![](img/brow_front_x4/brow_front_grandpa.jpg) 
![](img/brow_front_x4/brow_front_susumu.jpg) 
![](img/brow_front_x4/brow_front_kenji.jpg) 
![](img/brow_front_x4/brow_front_ykma.jpg) 
![](img/brow_front_x4/brow_front_risa.jpg) 
![](img/brow_front_x4/brow_front_mika.jpg) 
![](img/brow_front_x4/brow_front_akane.jpg) 
![](img/brow_front_x4/brow_front_nishina.jpg) 
![](img/brow_front_x4/brow_front_aoi.jpg) 
![](img/brow_front_x4/brow_front_grandma.jpg) 
![](img/brow_front_x4/brow_front_fumiya.jpg) 
![](img/brow_front_x4/brow_front_reiko.jpg) 
![](img/brow_front_x4/brow_front_matsu.jpg) 
![](img/brow_front_x4/brow_front_shoko.jpg) 
![](img/brow_front_x4/brow_front_tooru.jpg) 
![](img/brow_front_x4/brow_front_kyoko.jpg) 
![](img/brow_front_x4/brow_front_kazuma.jpg) 
![](img/brow_front_x4/brow_front_kazuki.jpg) 
![](img/brow_front_x4/brow_front_chinatsu.jpg) 
![](img/brow_front_x4/brow_front_mai.jpg) 
![](img/brow_front_x4/brow_front_aya.jpg) 
![](img/brow_front_x4/brow_front_tenko.jpg) 
![](img/brow_front_x4/brow_front_kimihiro.jpg) 
![](img/brow_front_x4/brow_front_tomomi.jpg) 
![](img/brow_front_x4/brow_front_hanzu.jpg) 
![](img/brow_front_x4/brow_front_te.jpg) 



[^1]: Biometric Study of Eyelid Shape and Dimensions of Different Races with References to Beauty. Seung Chul Rhee, Kyoung-Sik Woo, Bongsik Kwon. Aesth Plast Surg (2012), DOI 10.1007/s00266-012-9937-7   
[^2]: The Microblading Bible. 2016. Corinne Asch.  
[^3]: [https://www.eyebrowz.com](https://www.eyebrowz.com)   
[^4]: Making Faces. 1997. Kevyn Aucoin.  
[^5]: Makeup Artist Brow Charts. 2016. Gina M. Reyna.  
[^6]: [眉形综合篇！这是一份超全面超详细的眉形攻略 - 知乎](https://zhuanlan.zhihu.com/p/32484098)  
[^7]: Form of the Head and Neck, 2021, Uldis Zarins.  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
