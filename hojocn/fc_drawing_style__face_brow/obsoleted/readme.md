
- 资料[^6]中给出更细分的结构：眉头(Mb)、眉腰(br1/br2/br3/br4)、眉峰(Hb)、眉尾，眉尖(Lb)  
    ![](img/zhihu.00.mod.jpg)  
    各部分可能的变化：  
    眉头: 远(out)/近(in)；（眉间距远、近）  
    眉腰：直(line)/弯(curve)；  
    眉峰：高(high)/低(low)/靠里(in)/靠外(out)；  
    眉尾：短(short)/长(long)；  
    眉尖：钝(blunt)/尖(acute)；  

- 
    - [5] 列举了5种眉形： Straight, Round, Soft Angle, Sharp Angle, S-shape.    
        ![](img/MakeupArtistBrowCharts.Straight.jpg) 
        ![](img/MakeupArtistBrowCharts.Round.jpg) 
        ![](img/MakeupArtistBrowCharts.SoftAngle.jpg) 
        ![](img/MakeupArtistBrowCharts.SharpAngle.jpg) 
        ![](img/MakeupArtistBrowCharts.S-shape.jpg)  

    - [2]   
        - 把眉形分为5种： Straight, Curved, Naturally Thick, Arched, Thin.  
        ![](img/TheMicrobladingBible(2016).01.straight.jpg) 
        ![](img/TheMicrobladingBible(2016).01.curved.jpg) 
        ![](img/TheMicrobladingBible(2016).01.arched.jpg) 
        ![](img/TheMicrobladingBible(2016).01.thick.jpg) 
        ![](img/TheMicrobladingBible(2016).01.thin.jpg)  
 
        - 眉毛最高点的位置关系：  
        ![](img/TheMicrobladingBible(2016).02.jpg)  
        - Anatomy of a brow:  
        ![](img/TheMicrobladingBible(2016).03.jpg)  
        - 眉毛与合适的脸形：  
            - Oval faces ~ softly angled brows  
            - Heart-shaped faces ~ low rounded arches(a natural look) / high arch brow (elongate a short face)  
            - Round faces ~ high arches(elongating the face, adding angles)  
            - Square faces ~ soft curves(soften sharp angles)  
            - Diamond-shaped faces ~ medium high soft curves  
 
    - [3]  
        - 眉毛与合适的脸形：  
            - Oval-face ~ soft-angled brow,  
            - Heart-face ~ (low-arched/high-arched)round-brow,  
            - Long-Face ~ flat brow,  
            - Round-Face ~ high-arched brow,  
            - Square-Face ~ Angled brow / curved brow,  
            - Diamond-Face ~ curved brow / round-brow:  
            ![](img/eyebrowz.ovalshape.jpg) 
            ![](img/eyebrowz.heart.jpg) 
            ![](img/eyebrowz.long.jpg) 
            ![](img/eyebrowz.round.jpg) 
            ![](img/eyebrowz.square.jpg) 
            ![](img/eyebrowz.diamond.jpg)  

    - [4] 列举了7种眉形：  
        ![](img/MakingFaces.1999.audrey.jpg) 
        ![](img/MakingFaces.1999.brooke.jpg) 
        ![](img/MakingFaces.1999.clara.jpg) 
        ![](img/MakingFaces.1999.divine.jpg) 
        ![](img/MakingFaces.1999.elizabeth.jpg) 
        ![](img/MakingFaces.1999.marilyn.jpg) 
        ![](img/MakingFaces.1999.marlene.jpg)  


- 本文描述眉毛的方法：  
    - 使用[5]里的记法描述眉型：  
        - Straight:  
                ![](img/MakeupArtistBrowCharts.Straight.jpg) 
                ![](img/TheMicrobladingBible(2016).01.straight.jpg) 
    
        - Round:   
                ![](img/MakeupArtistBrowCharts.Round.jpg) 
                ![](img/TheMicrobladingBible(2016).01.curved.jpg) 
    
        - SoftAngle:  
                ![](img/MakeupArtistBrowCharts.SoftAngle.jpg) 
                ![](img/TheMicrobladingBible(2016).01.thin.jpg) 
    
        - SharpAngle:  
                ![](img/MakeupArtistBrowCharts.SharpAngle.jpg) 
                ![](img/TheMicrobladingBible(2016).01.arched.jpg) 
    
        - S-shape:  
                ![](img/MakeupArtistBrowCharts.S-shape.jpg) 
                ![](img/TheMicrobladingBible(2016).01.thick.jpg) 

    - 眉毛(不同部分)粗细程度分为5类： 1（细）<-- 2（稍细）<-- 3（中等）--> 4（稍粗）--> 5（粗）
    例如，一个Straight眉毛，眉头粗、眉腰中等粗细、眉尾细，可记为： Straight/3-2-1





## 总结

### 把类似的眉毛分组   
Flat/3-3-1： 雅彦basis ～紫苑basis～紫basis  
Flat/3-3-3： Sora basis ～ Aoi basis？（遮蔽）  
Flat/2-2-1： Susumu basis～Yoriko basis  
Flat/1-2-1(三角)：  Akane  
Flat/1-1-1: Reiko  
Rounded/2-1-1： Asagi basis～Saki basis  
Rounded/1-1-1： Hiromi basis ～Mori basis？（遮蔽）~ Mai basis  
SoftAngled/3-3-1： Shya basis  
Rounded/3-3-3： Kaoru basis？（遮蔽）
Rounded/2-2-2： Makoto basis  
Rounded/1-2-1,短(三角)： Director basis  
Rounded/4-4-1： Risa basis  
Rounded/3-2-1： Chinatsu basis  
SoftAngled/2-2-1： Yoko basis～ykma basis  
SoftAngled/1-1-1： Mika basis  
Angled/3-2-1： Aya basis  
Angled/3-3-3： Kazuko basis～Nishina basis  
Angled/5-5-5： Kenji basis  
Angled/4-4-4： Matsu basis  
Angled/1-3-1(三角)： Cameraman basis ～ Fumiya basis  

![](img/brow_front_x4/brow_front_masahiko.jpg) 
![](img/brow_front_x4/brow_front_shion.jpg) 
![](img/brow_front_x4/brow_front_yukari.jpg) 

![](img/brow_front_x4/brow_front_sora.jpg) 
![](img/brow_front_x4/brow_front_aoi.jpg) 

![](img/brow_front_x4/brow_front_susumu.jpg) 
![](img/brow_front_x4/brow_front_yoriko.jpg) 

![](img/brow_front_x4/brow_front_akane.jpg) 

![](img/brow_front_x4/brow_front_reiko.jpg) 

![](img/brow_front_x4/brow_front_asagi.jpg) 
![](img/brow_front_x4/brow_front_saki.jpg) 

![](img/brow_front_x4/brow_front_hiromi.jpg) 
![](img/brow_front_x4/brow_front_mori.jpg) 
![](img/brow_front_x4/brow_front_mai.jpg) 

![](img/brow_front_x4/brow_front_shya.jpg) 

![](img/brow_front_x4/brow_front_kaoru.jpg) 

![](img/brow_front_x4/brow_front_makoto.jpg) 

![](img/brow_front_x4/brow_front_director.jpg) 

![](img/brow_front_x4/brow_front_risa.jpg) 

![](img/brow_front_x4/brow_front_chinatsu.jpg) 

![](img/brow_front_x4/brow_front_yoko.jpg) 
![](img/brow_front_x4/brow_front_ykma.jpg) 

![](img/brow_front_x4/brow_front_mika.jpg) 

![](img/brow_front_x4/brow_front_aya.jpg) 

![](img/brow_front_x4/brow_front_kazuko.jpg) 
![](img/brow_front_x4/brow_front_nishina.jpg) 

![](img/brow_front_x4/brow_front_kenji.jpg) 

![](img/brow_front_x4/brow_front_matsu.jpg) 

![](img/brow_front_x4/brow_front_cameraman.jpg) 
![](img/brow_front_x4/brow_front_fumiya.jpg) 




### 合并眉毛粗细度(合并2,3,4)，从而减少分组  
Flat/3-3-1： 雅彦basis ～紫苑basis～紫basis **～～** Susumu basis～yoriko basis   
Flat/3-3-3： Sora basis ～ Aoi basis？（遮蔽）  
Flat/1-3-1(三角)：  Akane  
Flat/1-1-1: Reiko  
Rounded/3-1-1： Asagi basis～Saki basis  
Rounded/1-1-1： Hiromi basis ～Mori basis？（遮蔽）~ Mai basis  
SoftAngled/3-3-1： Shya basis  
Rounded/3-3-3： Kaoru basis？（遮蔽）～ Makoto basis  
Rounded/1-3-1,短(三角)： Director basis  
Rounded/3-3-1： Risa basis ～ Chinatsu basis  
SoftAngled/3-3-1： Yoko basis～Ykma basis  
SoftAngled/1-1-1： Mika basis  
Angled/3-3-1： Aya basis  
Angled/3-3-3： Kazuko basis～Nishina basis**～～**Matsu basis  
Angled/5-5-5： Kenji basis
Angled/1-3-1(三角)： Cameraman basis ～ Fumiya basis  

![](img/brow_front_x4/brow_front_masahiko.jpg) 
![](img/brow_front_x4/brow_front_shion.jpg) 

![](img/brow_front_x4/brow_front_yukari.jpg) 
![](img/brow_front_x4/brow_front_susumu.jpg) 
![](img/brow_front_x4/brow_front_yoriko.jpg) 

![](img/brow_front_x4/brow_front_sora.jpg) 
![](img/brow_front_x4/brow_front_aoi.jpg) 

![](img/brow_front_x4/brow_front_akane.jpg) 

![](img/brow_front_x4/brow_front_reiko.jpg) 

![](img/brow_front_x4/brow_front_asagi.jpg) 
![](img/brow_front_x4/brow_front_saki.jpg) 

![](img/brow_front_x4/brow_front_hiromi.jpg) 
![](img/brow_front_x4/brow_front_mori.jpg) 
![](img/brow_front_x4/brow_front_mai.jpg) 

![](img/brow_front_x4/brow_front_shya.jpg) 
![](img/brow_front_x4/brow_front_kaoru.jpg) 
![](img/brow_front_x4/brow_front_makoto.jpg) 

![](img/brow_front_x4/brow_front_director.jpg) 

![](img/brow_front_x4/brow_front_risa.jpg) 
![](img/brow_front_x4/brow_front_chinatsu.jpg) 

![](img/brow_front_x4/brow_front_yoko.jpg) 
![](img/brow_front_x4/brow_front_ykma.jpg) 

![](img/brow_front_x4/brow_front_mika.jpg) 

![](img/brow_front_x4/brow_front_aya.jpg) 

![](img/brow_front_x4/brow_front_kazuko.jpg) 
![](img/brow_front_x4/brow_front_nishina.jpg) 
![](img/brow_front_x4/brow_front_matsu.jpg) 

![](img/brow_front_x4/brow_front_kenji.jpg) 

![](img/brow_front_x4/brow_front_cameraman.jpg) 
![](img/brow_front_x4/brow_front_fumiya.jpg) 


### 进一步合并较为类似的眉毛，从而进一步减少分组  
Flat/3-3-1： 雅彦basis ～紫苑basis～紫basis ～～ Susumu basis～Yoriko basis **～～～**  Yoko basis～Ykma basis  
Flat/3-3-3： Sora basis ～ Aoi basis？（遮蔽） **～～～** Shya basis ～ Kaoru basis？（遮蔽）～ makoto basis  
Flat/1-1-1: Reiko  **～～～** Mika basis **～～～** Akane **～～～** Hiromi basis ～Mori basis？（遮蔽）~ Mai basis  
Rounded/3-1-1： Asagi basis～Saki basis  
Rounded/1-3-1,短(三角)： Director basis  
Rounded/3-3-1： Risa basis ～ Chinatsu basis  
Angled/3-3-1： Aya basis  
Angled/3-3-3： Kazuko basis～Nishina basis～～Matsu basis **～～～** Kenji basis  
Angled/1-3-1(三角)： Cameraman basis ～ Fumiya basis  

![](img/brow_front_x4/brow_front_masahiko.jpg) 
![](img/brow_front_x4/brow_front_shion.jpg) 
![](img/brow_front_x4/brow_front_yukari.jpg) 
![](img/brow_front_x4/brow_front_susumu.jpg) 
![](img/brow_front_x4/brow_front_yoriko.jpg) 
![](img/brow_front_x4/brow_front_yoko.jpg) 
![](img/brow_front_x4/brow_front_ykma.jpg) 

![](img/brow_front_x4/brow_front_sora.jpg) 
![](img/brow_front_x4/brow_front_aoi.jpg) 
![](img/brow_front_x4/brow_front_shya.jpg) 
![](img/brow_front_x4/brow_front_kaoru.jpg) 
![](img/brow_front_x4/brow_front_makoto.jpg) 

![](img/brow_front_x4/brow_front_reiko.jpg) 
![](img/brow_front_x4/brow_front_mika.jpg) 
![](img/brow_front_x4/brow_front_akane.jpg) 
![](img/brow_front_x4/brow_front_hiromi.jpg) 
![](img/brow_front_x4/brow_front_mori.jpg) 
![](img/brow_front_x4/brow_front_mai.jpg) 

![](img/brow_front_x4/brow_front_asagi.jpg) 
![](img/brow_front_x4/brow_front_saki.jpg) 

![](img/brow_front_x4/brow_front_director.jpg) 

![](img/brow_front_x4/brow_front_risa.jpg) 
![](img/brow_front_x4/brow_front_chinatsu.jpg) 

![](img/brow_front_x4/brow_front_aya.jpg) 

![](img/brow_front_x4/brow_front_kazuko.jpg) 
![](img/brow_front_x4/brow_front_nishina.jpg) 
![](img/brow_front_x4/brow_front_matsu.jpg) 
![](img/brow_front_x4/brow_front_kenji.jpg) 

![](img/brow_front_x4/brow_front_cameraman.jpg) 
![](img/brow_front_x4/brow_front_fumiya.jpg) 
