## 角色名字

<a name="sara"></a>
### (西九条)紗羅   
植物是本作的主题之一。所以，有理由猜测"紗羅"通"沙羅"。  

### 沙羅、沙羅树
"沙羅"可以指一种树名，即沙羅树。而“沙羅树”又名“娑羅樹”。[^Shorea_robusta_cn]   

[^Shorea_robusta_cn]: [娑羅樹 - Wikipedia](https://zh.wikipedia.org/wiki/娑羅樹)   

### 娑羅树
佛教中记载有娑羅双树（即两棵娑羅树）：  
> 後釋迦牟尼在拘尸那羅城外㕧賴拏伐底河（...）娑羅林（梵語：Śālavana，巴利語：Sālavana）雙樹下入滅。相傳釋迦牟尼入涅槃時，娑羅樹同時開花，林中一時變白......  
> 佛教視之為聖樹之一......在印度教儀式中常燃燒娑羅樹脂......  
（出处： [娑羅樹 - Wikipedia](https://zh.wikipedia.org/wiki/娑羅樹)）  

可以想象，娑羅树会随着佛教的传播而传播。但由于中国、日本、东南亚的气候不适合印度娑羅树，所以，这些地区的寺庙里使用其他树种代替印度娑羅树[^blog]，而仍称这些代替树木为娑羅树。  

[^blog]: [“沙羅双树”是什么是？“沙羅树”是什么树？花的颜色是什么？](./ref/shorea-robusta.md)  


[印度沙罗树的信息](https://en.wikipedia.org/wiki/Shorea_robusta)：  

> 学名：Shorea robusta   
> 本名：साल   [^Shorea_robusta_hi]  
> 英名：sal tree  
> 原产地：印度、馬來半島等南亞雨林[^Shorea_robusta_cn]  

[^Shorea_robusta_hi]: [Shorea robusta - Wikipedia(印度语)](https://hi.wikipedia.org/wiki/साल_(वृक्ष))   


[日本沙罗树的信息](./ref/shorea-robusta.md)：  
> 学名：Stewartia pseudocamellia [^Stewartia]  
> 本名：夏椿（ナツツバキ）  
> 原产地：日本  

[^Stewartia]:  
“It is called natsutsubaki(ナツツバキ, "summer camellia") in Japanese, and nogaknamu(노각나무, "overripe cucumber tree") in Korean.” source: [Stewartia pseudocamellia - wikipedia](https://en.wikipedia.org/wiki/Stewartia_pseudocamellia),    
"ナツツバキ（夏椿・沙羅、学名: Stewartia pseudocamellia）は、ツバキ科ナツツバキ属の落葉小高木・高木。別名でシャラノキ、シャラなどともよばれている。樹皮に美しい斑模様があり、初夏にツバキに似た白い花を咲かせる。" source: [ナツツバキ](https://ja.wikipedia.org/wiki/ナツツバキ)  


[东南亚国家的沙罗树的信息](./ref/shorea-robusta.md)：  
> 学名：Couroupia guianensis [^Couroupia]  
> 英名：Cannon ball tree  
> 中文名：砲彈樹 [^Couroupia]  
> 原产地：南美洲  

[^Couroupia]:  
“本種因其碩大球形果實，狀如中世紀發明的砲彈而得名，英、法語的俗稱即是砲彈樹：cannonball tree, boulet de canon。”出自：[砲彈樹 - Wikipedia](https://zh.wikipedia.org/wiki/砲彈樹)  
另见：  
[Couroupita guianensis - Wikipedia](https://en.wikipedia.org/wiki/Couroupita_guianensis),  
[ホウガンボク - Wikipedia](https://ja.wikipedia.org/wiki/ホウガンボク),  


[中国沙罗树的信息](https://zh.wikipedia.org/wiki/娑羅樹)：  
北方:  
> 学名：Aesculus chinensis  
> 中文名：七葉樹  
> 原产地：中国北方(河北、河南、山西、陕西等)  

南方:  
> 学名：Reevesia sinica  
> 中文名：梭罗树  
> 原产地：亚洲的亚热带及热带地区  




### 沙羅树与日本文化  
首先，佛教曾在7世纪传入日本；目前，日本佛教徒占比约69%。[^日本佛教]而佛教的圣树之一是沙羅树。[^Shorea_robusta_cn]  

其次，日本13世纪诞生了历史小说《平家物语》。其主要讲述煊赫一时的平氏家族的兴亡。该作以史书编年体为主，其中作者加入了对许多事件的看法，形成了以作者寻找平氏衰亡原因为主要线索的结构。该作的结尾更让人感受到盛者必衰之感。而这种无常观也是作者想要表达的重要观念之一。[^平家物语1]其与《源氏物语》并列为二大物语经典，影响极为深远。[^平家物语2]  
该作的开头提到沙羅树[^平家物语1]：  
> 祇園精舎の鐘の声  
諸行無常の響きあり  
娑羅双樹の花の色  
盛者必衰の理を顕す  
驕れる人も久しからず  
唯春の夜の夢の如し  
猛き者も終には亡びぬ  
偏に風の前の塵に同じ  

> 译文：  
祇园精舍的钟声，  
有诸行无常的声响，  
沙罗双树的花色，  
显盛者必衰的道理。  
骄奢者不久长，  
只如春夜的一梦，  
强梁者终败亡，  
恰似风前的尘土。  

[^日本佛教]: [日本佛教@Wikipedia](https://zh.wikipedia.org/wiki/日本佛教)  
[^平家物语1]: [平家物语@Wikipedia](https://zh.wikipedia.org/zh-hans/平家物语)  
[^平家物语2]: [平家物语@GoodReads](https://www.goodreads.com/book/show/43724230)  

由以上两点，有理由相信，沙羅树和"沙羅"一词在日本为多数人知晓。  


### (西九条)紗羅的英文名  
首先，该作品的官方英文版尚未出现。所以，保守地说，(官方版的)女主角紗羅的英文名暂未知。  

其次，网络流传的该作的英文版里，女主角紗羅的英文名为Sara。  



