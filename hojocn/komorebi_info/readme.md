# 《阳光少女》的相关信息    

《阳光少女》原作名：《こもれ陽の下で･･･》，连载于1993年～1994年週刊少年Jump，官方将其标记为短篇。[^official]可于[官方在线浏览](https://mangahot.jp/site/works/j_R0018)（前2话免费）。[^ol]    

[^official]: [北条司 Official Website](https://hojo-tsukasa.com/gallery)  
[^ol]: [Komorebi Online @mangahot](https://mangahot.jp/site/works/j_R0018)  


## [关于作品的社会背景](./komorebi_background.md)    
## [关于作品名。"こもれ陽"、"こもれひ"、"木漏れ日"  ](./komorebi_title.md)  
## [Subtitles](./komorebi_subtitles.md)  
## [角色名字](./komorebi_names.md)  





## Links  
[Komorebi @Wikipedia](https://ja.wikipedia.org/wiki/こもれ陽の下で…)  
https://ocr.space/  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  