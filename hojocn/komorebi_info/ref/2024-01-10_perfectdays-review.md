https://note.com/naohi/n/n6ea8d1dc4614

![](https://assets.st-note.com/production/uploads/images/127208010/rectangle_large_type_2_836c186b795a61b649312797268901f7.jpeg?width=100)    

# 映画『PERFECT DAYS』木漏れ日から漏れ出す光と影、小確幸、そして新しい一日について。
# 电影《PERFECT DAYS》关于透过树叶的阳光所透出的光与影、小确幸以及新的一天

naohi 2024年1月10日 04:59  

.  
『PERFECT DAYS』ヴィム・ヴェンダース  
《PERFECT DAYS》Wim Wenders    

ヴィム・ヴェンダースと言えば、その昔『ベルリン・天使の詩』（1988）を映画館で観て激しく感銘を受け、続く『時の翼にのって』（1994）、そして『ブエナ・ビスタ・ソシアル・クラブ』（1999）を観て以来だから、劇場で観るヴェンダース映画としてはなんと25年ぶりである。今回は映画の概要を知った段階で『これは絶対観なアカンやつや』と決めていたので、結構前からずっと楽しみにしていたのだ。  
说到Wim Wenders，以前在电影院看了《柏林天使之诗》（1988年）深受感动，接着看了《乘着时间的翅膀》（1994年）和《布埃纳维斯塔社交俱乐部》（1999年）。这是我25年来第一次在剧院看Wenders电影。这次在知道电影概要的阶段就决定‘这个绝对不能看的家伙’，所以从很久以前就很期待了。  

というわけで正月早々早速観に行ってきました！  
所以新年一开始就去看了！  

っていうか、正月休み中ということもあるのかもしれんが、思った以上の混雑ぶりである。うむむ、ヴェンダースって今でもこんなに人気あったのか。  
或许也有新年假期的原因吧，比想象中还要拥挤。嗯嗯，文德斯现在也这么受欢迎啊。  

～2時間経過～  
～2小时过去～  

いやあ、良かった。実に善きものを観せてもらったという感じである。  
哎呀，太好了。感觉真的看到了美好的东西。  

映画は東京都内で公衆トイレの清掃員として働く平山（役所広司）という男の日常を淡々と描くところから始まる。  
电影淡淡地描写了在东京都内作为公共厕所清洁工工作的平山（役所广司饰）的日常生活。  

（以下、若干のネタバレあり）  
（以下有若干剧透）  

早朝、近所の老婆が掃く箒の音で目覚め、布団をきちんとたたみ、歯を磨き、ヒゲを剃る。育てている植木に水をやり、車で仕事に出かける。車が大通りに入ったところでカセットテープで音楽を聴く。現場に着き、トイレ掃除の仕事を丁寧に行う。仕事が終わると銭湯に行き、いつもの居酒屋で飲んでから家路につく。寝る前にはなじみの古本屋で買った文庫本を読みながら眠りにつく。  
清晨，被附近老婆婆扫地的声音吵醒，叠好被褥，刷牙，刮胡子。给正在种植的树木浇水，开车去上班。汽车驶入大马路时，用卡式磁带听音乐。到达现场，认真地进行厕所清扫工作。下班后去澡堂，去常去的居酒屋喝酒，然后回家。睡觉前读着在常去的旧书店买的文库本入眠。  

これがずっと繰り返されるのである。こう書くとなんだか退屈に思えるかもしれないが、これが実に味わい深い映像になっているのである。それは平山が玄関を出たところで見上げる空の色であったり、カーステレオから流れる音楽であったり、その時車の窓から見える街の風景であったり、昼休みの公園の木漏れ日からこぼれる光と影だったりと、まさに村上春樹が良く言うところの『小確幸』（小さいけれども、確かな幸福）を感じさせるシーンが実に瑞々しくも詩情豊かに立ち現れてくるので、全く退屈せずに観ることが出来るのだ。平山が時折見せる静かな微笑みとかすかな戸惑いの表情もまた絶妙で、セリフがほとんど無い分、より心情がダイレクトに伝わってくる。このあたりが役所広司のカンヌ受賞に繋がったとも言えるのではないだろうか。映画ならではの魅力とは、つまりはこういうことなのである。  
这种情况一直在重复。这么写可能会觉得无聊，但这确实是意味深长的影像。那是平山走出玄关时仰望的天空的颜色，是汽车音响里播放的音乐，是此时透过车窗看到的街景，是午休时公园里木漏れ日洒落的阳光和光影，简直就是村上春树常说的“小确幸”（虽然小，但确实的幸福）的场景，生动而富有诗意地展现出来，观影完全不会无聊。平山偶尔露出的安静的微笑和微微困惑的表情也绝妙，几乎没有台词，更直接地传达了心情。或许可以说，正是这一点与役所广司在戛纳获奖息息相关吧。电影独特的魅力，就是这样的。  

ちなみに平山の部屋にはテレビも無く、スマホもネット環境も無い。あるのは本とラジカセとカセットテープのみである。観ながら『おお、このようなシンプルで質素な生活をいつかは我もしてみたいものだよのう』なんてちょっと憧れてしまったのだが、良く考えてみたら『いや、テレビやスマホは無くても良いがパソコンはやっぱり欲しい』とか『ラジカセじゃなくてレコードプレーヤーが良い』とか『文庫本よりハードカバーのほうが好きなんだよなあ』とか『銭湯はたまになら良いけど、毎日はちょっとなー』とか『コンビニ近所にあったっけ？』とかの邪念がふつふつと湧き出てしまい（笑）、結局自分にはこのような生活は無理かも、なんて思った次第だったりで。  
顺便一提，平山的房间里没有电视，也没有智能手机和网络环境。有的只有书、收录机和磁带。一边看一边憧憬着‘啊，总有一天我也要过这种简单朴素的生活啊’，但仔细一想‘不，没有电视和手机也可以，但还是想要电脑’、“比起文库本，我更喜欢精装书”、“澡堂偶尔泡一下还好，若每天的话就有点——”、“便利店附近有吗？”之类的邪念不断地涌出来（笑），结果觉得自己可能过不了这样的生活。  

まあそれはさておき、この僧侶のごとき質素な暮らし、そして小確幸を感じる日々、まさに『足るを知る』を地で行くような日常、それこそが『パーフェクトデイズ』ということかと思いきや、もちろんこの映画はそんな単純な話ではなく、後半はそのような繰り返しの日常が揺らぎはじめるような出来事が少しづつ起こり始める。  
这些暂且不提，像僧侣一样朴素的生活，以及感受小确幸的每一天，就像“知足”一样的日常生活，本以为这就是《Perfect Days》，但这部影片当然不是一个如此简单的故事，在影片的后半部分，一些事件开始一点一点地发生，开始动摇这种重复的日常生活。   

そこで平山が実は過去にかなりの紆余曲折というか、ある種の喪失感や痛みの記憶を抱えた人物であるということが示唆されるのだが、その時観客ははたと気づくのだ。平山の僧侶のような日常のルーティン、まるで華道や茶道の『所作』のごときものが、実は平山のそのような過去を封じ込める結界のような役割をしていたのではないだろうかと。しかしそのようなある意味閉じた世界にも次第に綻びが生じ、ついには決壊してしまう。そしてここでの役所広司の演技、これはもう圧巻の一言である（カメラマンが撮ってる途中で思わずもらい泣きをしてしまったらしい）。個人的には、おそらくこのような喪失感や痛みや挫折の記憶もまるごと引き受けた上で、それでも人生は素晴らしい、生きていくことは素晴らしいんだと自らに了承すること、自分なりの幸せを追い求めること、それらを含む全てが『パーフェクトデイズ』なのだとこのシーン、もしくはこの映画は言っているように感じた。この世界には全く同じ1日というものはなく、目の前には、自らの過去の全てが積み重なった『新しい1日』が常に開けているのである。  
由此暗示平山实际上是过去经历过相当曲折，或者说是怀有某种失落感和痛苦记忆的人物，这时观众才恍然大悟。平山的僧侣式的日常生活，就像花道和茶道的“礼仪”一样，实际上可能已经成为了容纳平山的过去的一种界限。但是在某种意义上封闭的世界也渐渐瓦解，最终崩溃。而且役所广司在这里的演技堪称一绝（听说摄影师在拍摄过程中情不自禁地跟着哭了）。就我个人而言，这个场景或者说这部电影也许表明，包括在全盘接受失落感、痛苦和挫折的记忆的基础上，承认自己的人生是美好的，活下去是美好的，追求属于自己的幸福。这就是“Perfect Days”的意义所在。我觉得这部电影在说这个世界上没有完全相同的一天，总有 “新的一天 ”摆在你的面前，在这一天里，你过去的一切都已积淀。   

観たあとに色々と考えてしまう映画であり、世界の見え方が昨日とほんの少し違って見えるような映画でもある。世界を揺るがす超大作、というわけではないが、間違いなく傑作であると思う。作品が作られた経緯に利権の匂いがすると批判的な意見もあるようだが、それとこれとは話が違う。映画はあくまでも映画として評価しなければいけない。  
这是一部看完之后会想很多的电影，一部会让你对世界的看法与昨天有些不同的电影。虽然不是震撼世界的大片，但毫无疑问是一部杰作。虽然也有批判的意见认为作品的创作过程带有利权的味道，但这是两码事。电影必须作为电影来评价。  

ちなみに自分的にはラストシーンはもう少し短くても良かったかな、とも思う。そのほうがより余韻に浸れたような気がするんだよなあ。ラストシーンの途中、一瞬我に返りそうになってしまったからね、オレ（笑）。    
顺便说一下，我觉得最后一幕再短一点也不错。那样更能沉浸在余韵中。最后一幕的中途，我一瞬间都要回过神来了呢（笑）。  
   
[#perfectdays](https://note.com/hashtag/perfectdays)    
[#パーフェクトデイズ](https://note.com/hashtag/パーフェクトデイズ)  
