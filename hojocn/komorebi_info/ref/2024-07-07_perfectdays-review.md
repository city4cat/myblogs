https://movie.douban.com/review/16041969/  


# “木漏れ日”：生活世界的意义在此刻澄明而涌现
[尘](https://www.douban.com/people/anthrosf/) 评论 《Perfect Days》， 2024-07-07 23:25:33  

> 这篇影评可能有剧透

在《完美的日子》中，生活世界的意义正是通过“木漏れ日”这一意象而澄明并进一步涌现出来的。在一个访谈之中，文德斯特别谈到了“木漏れ日”。他说，

> 日语中有一个专门的词来描述这些有时会突然出现的幻影：‘木漏れ日’：树叶在风中起舞，它坠落下来，影子落在你面前的墙上，这一切由宇宙中的一束光——太阳创造。”

“木漏れ日”特指阳光透过树叶之间的空隙投射下来的景象。在《完美的日子》里，“木漏れ日“的景象贯穿始终。这也是理解整部电影的一把钥匙。

## 意义在程式化的生活中消亡了吗？


电影中，平山的生活世界乃是高度程式化的。伴随着清晨打扫大街的清洁工的扫帚声而起床——刷牙——给植物喷水——上车前一定在自动贩卖机买一罐咖啡——选磁带，伴随着老摇滚乐上路——在厕所里细心地清理每一个细节——午饭在神社吃三明治——下班去澡堂洗澡——有时候去小书店买一本文库本小说——有时候去冲印店取冲好的胶片——去地下通道里的小餐馆吃晚餐——有时候去小酒馆喝一杯——睡前读书。电影不厌其烦地表现这些程式化的细节。文德斯的牛逼之处就在于不断重复这些细节，却让人丝毫不觉沉闷，反而对主人公平山的生活发生了浓厚的兴趣——他是谁？他为什么做这些？他究竟在想什么？

程式化以及不断重复的生活往往令人厌倦。对每天朝九晚五，两点一线的生活，大多数人留下的往往是单调而沉闷的印象。可是，文德斯想告诉我们的，却是这些程式化的另外一面。或者说，导演通过电影向我们提了一个问题：重复的、程式化的生活是否仅仅灰暗而单调？这样的生活是否可以充满意义(meaningful)？

## 木漏れ日：意义的涌现时刻
 

“木漏れ日”的意象在电影中不断地出现。比如在工作的间隙，当平山礼貌地在厕所外回避需要上厕所的路人时，他注意到墙上树影晃动。目光沿着光的方向回溯，他看见阳光穿过浓密的树叶。在他的脸上，不经意地浮现出一丝微笑。

在神社里吃午餐的时候，抬头瞥见阳光从树叶的缝隙中漏下来。平山拿出胶片相机，不经意地向斜上方拍摄，捕捉这转瞬即逝的光影瞬间。

在梦中，“木漏れ日”以黑白而朦胧摇曳的画面浮现。这些时刻，混着旧日的记忆，在蒙太奇中如幽暗的烛光般跃动。

在午后的光景，平山躺在榻榻米上，窗外的阳光照进来。录音机中播着Lou Reed的《Perfect Day》(点题片名！)。平山的眼前又浮现出“木漏れ日”的意象。

难得的休息日，平山冲洗好了照片。一张张地检视。留下那些“木漏れ日”的瞬间。

……

然而，究竟应该如何理解“木漏れ日”的意象呢？

本雅明在《机械复制时代的艺术作品》一文中首次提出”灵光/光晕(aura)“的概念。他写道，

> 我们将自然对象的光韵界定为在一定距离之外但感觉上如此贴近之物的独一无二的显现。在一个夏日的午后，一边休憩着一边凝视地平线上的一座连绵不断的山脉或一根在休憩者身上投下绿荫的树枝，那就是这座山脉或这根树枝的光韵在散发。“

在观影的过程中，本雅明“灵光/光晕”的概念不断在我脑海中闪现。在这里，“木漏れ日”与“灵光”显然有着某种紧密的联系。本雅明所举之例子，如一根投影咋在休憩者身上的树枝，如地平线上的远山，与穿过树叶而漏下的阳光有着异曲同工之妙。文德斯通过平山所捕捉的，正是那些转瞬即逝的“光晕”。

光晕之所以特别，正因为其独特的“质地”。这种质地显然无法通过机械复制来得到留存。“木漏れ日”所昭示的，正是那些无法被重复性、机械性的生活所遮蔽的“灵光”。这些“灵光”在那些树叶摇曳的光影中，洒漏而下，仿佛天启。

## 木漏れ日与偶发性（contingency）
 

与“灵光”一样，木漏れ日之所以迷人，是其充满了偶发性。我们无法确定什么时候会天晴，什么时候阳光会穿过云层，什么时候观测者又正好处在树荫之下，什么时候又恰好能看到穿过树叶的阳光，投射在墙上的光影。

是的，“偶发性”是帮助我们在看似机械的生活世界之中重获自由的落脚点。在平山看似一成不变的生活中，却有着许多很有意思的偶发事件。大一点的，比如外甥女离家出走而突然来访、搭档小哥突然不辞而别、酒馆妈妈桑的前夫来访而被平山不经意撞见。小一点的，比如某位陌生人在厕所留下的tic-tac-toe游戏、帮助躲进厕所生闷气的小朋友找到妈妈、在神社的树下发现一颗破土而出小树苗而小心翼翼地挖土装回等等不一而足。

正是这些偶发性构成了我们生活的斑点与色彩。这些偶发性事件正如那透过树叶间的光，跳跃着让人无法捕捉，无法预期，却又那么鲜活。

我们生活世界的意义从何而来，是从那些棱角分明的结构中来 （阶级、经济、文化观念等等），抑或是镶嵌于偶发性之中呢？抬头看一看木漏れ日的光晕，或许你已经有了答案。

## 属于“当下 （present）”的木漏れ日
 

木漏れ日是属于当下的。阳光穿过树叶的景象，每一秒都转瞬即逝。对抗源于重复性、程式化生活的关键即当下。

当平山和外甥女一起骑车。在桥上，外甥女看到了河流流向远方的大海，便问平山，“你要去那里吗？” 平山说，“下次吧。”外甥女女追问，“下次是什么时候？” 平山答道，“下次是下次。”外甥女继续问道，“那是什么时候？”平山答，“这次是这次，下次是下次。”

这是一段颇为禅意的对话，也是本片打动我的地方之一。是的，下次的事下次再算吧。我们只着眼现在。这或许就是当下的启示吧。

在另一次偶发事件之中，平山撞见了妈妈桑前夫前来造访她，并拥抱了她。平山心中对妈妈桑有微妙的情愫。很自然，这个场景让他心情郁结。独自买了香烟与啤酒，跑到河边。前夫哥也跟来了。原来，前夫哥得了癌症，命不久已。前夫哥托平山照顾好妈妈桑。二人话头忽然聊到影子。重叠的影子颜色会更深吗？前夫哥感慨，生命中有太多事还没明白就要结束了。平山于是建议二人来验证叠影是否更深。二人又借着夜晚的灯光玩起 了追影子的游戏。两个中年男人，忽然变得像两个小孩。

这个场景也是本片最为打动我的地方。如果生命中有许多事情没有完成，觉得需要以后才做，不如现在就行动起来，现在就做。未来是怎么样，明天会怎样，我们无法知道，我们唯有像小孩子一样，活在当下，这样才能感受“完美的日子”

阳光漏过夜间，那光影仅在当下。只有把握当下，才能在机械复制的生活中抓住那一闪而逝的灵光。

## 结语：意义的涌现
 

影片的最后，在一曲Feeling Good的的老歌中结束。

> It's a new dawn
> It's a new day
> It's a new life for me
> And I'm feeling good

在平山的脸上，微笑、伤感、感怀、沉思交替出现。是的，这就是生活。生活既不高深也不卑贱，生活就在“此处”。

附记：

很少写影评了。役所广司所饰演的平山让我想起一位刚刚过世不久的朋友。这位朋友从来没有漂亮的话语、没有大书特书的丰功伟绩。可是我却不断地想起他。当我想起他的时候，记起来的都是最为平常的生活细节。他劳作的样子、他的笑容、他所说过的俏皮话，他顽皮的玩笑。谨以此文怀念他吧。

© 本文版权归作者  [尘](https://www.douban.com/people/anthrosf/)  所有，任何形式转载请联系作者。

