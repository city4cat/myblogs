https://www.chicagobotanic.org/nature_and_wellness/komorebi

# Komorebi 木漏れ日

As the weather cools, you may find yourself taking a walk, perhaps in the tranquil Malott Japanese Garden, enjoying refreshing breezes and colorful leaves. If it’s sunny, the leaves above your head will appear to glow, creating a natural parasol that protects you from harsh rays. As you stroll beneath the trees swaying in the gentle winds, notice the angle of light emanating from the canopy, then cast your gaze downward and take a moment to observe the ever changing pattern of leaves dancing across the earth.

The dapples of light and leaf are caused by the pinhole effect—the same concept that allows a pinhole camera to work. Light passes through a small hole—or in this case, the gap between leaves—and projects an inverted image on the other side. This effect is especially notable at dawn or just before dusk, when one can observe a cascade of shimmering amber light. While the sight is familiar and nostalgic, there is no English word for this phenomenon. There is, however, a Japanese word: _komorebi_.

Komorebi (pronounced ko-mo-reh-bee) is most commonly written as 木漏れ日 in Japanese. Japanese has a pictograph-based writing system borrowed from and influenced by traditional Chinese, which allows for the creation of words that define more obscure concepts like komorebi. The meaning is easy to glean from reading, provided that you know what each character means. The word is made of three characters, or kanji, along with an additional character that denotes a verb stem. There are three important parts to this word: 木 (ko) meaning tree, 漏れ (more) meaning to escape from, and 日 (bi) meaning sun. Together, the characters mean something like “sunlight filtering through trees.”

The lit trees outside provide us respite from our busy lives and allow us a chance to take in the natural beauty around us, so that at least we might breathe. The frolic of light and shadow glimmering through the trees has remained the same for thousands of years, serving as a welcome reminder of the enchanting natural world that lies just outside the door.

Komorebi won a 2022 Emmy for Outstanding Crafts Achievement for Lighting.