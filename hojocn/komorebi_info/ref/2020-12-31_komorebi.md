https://word-dictionary.jp/posts/5519  

# 「木漏れ日」とは？意味や使い方を英語表現も含めてご紹介
# 「木漏れ日」是什么？介绍含义和使用方法，包括英语表达

「木漏れ日」という言葉から、どんなイメージが思い浮かぶでしょうか。日本人どうしなら意味を説明しなくても共通に使える言葉のようですが、英語圏の人に意味を説明するには少し難しいとされています。この記事では、「木漏れ日」について解説します。  
从「木漏れ日」这个词，你会想到什么呢？日本人之间即使不说明意思也能通用的词语，但是要向英语圈的人说明意思就有点困难了。本文将介绍「木漏れ日」。  

2020年12月31日公開  2020年12月31日更新  


## 「木漏れ日」の意味は？  
## 「木漏れ日」的意思是什么？

![](https://s3-ap-northeast-1.amazonaws.com/cdn.word-dictionary.jp/production/imgs/images/000/007/711/original.jpg)  
出典: [https://pixabay.com/photo-5461921/](https://pixabay.com/photo-5461921/)  

**「木漏れ日（こもれび）」**とは、森や林などで群がって生えている木々の間から、太陽の日差しが漏れている景色や眺めのことを言います。「木洩れ日（こもれび）」、「木漏れ陽」、「木洩れ陽」と書かれることもありますが、意味は同じです。  
所谓「木漏れ日（こもれび）」，指的是太阳从森林、树林等成群生长的树木间照射下来的景色和景色。也有写成「木洩れ日（こもれび）」、「木漏れ陽」、「木洩れ陽」，意思是一样的。    

「木漏れ日」は、晴れている日なら、木々のもとに行けば見ることができる光景です。雨が降ったあとにすぐに晴れたときには、雨上がりの水蒸気に木漏れ日が反射してカーテンのように輝く様子が美しく見えます。  
「木漏れ日」是晴天的话，走到树下就能看到的光景。下过雨马上放晴的时候，雨后的水蒸气反射着阳光，像窗帘一样闪闪发光的样子看起来很美。  


## 「木漏れ日」使い方  
## 「木漏れ日」用法  

*   私たちは**木漏れ日**を浴びながら、山の頂上を目指して歩き続けた。  
    我们沐浴着**木漏れ日**，继续朝着山顶走去。
*   公園の**木漏れ日**の中、幼い子どもと母親が鬼ごっこをしていた。  
    公园里**木漏れ日**中，年幼的孩子和母亲正在玩捉迷藏。
*   森林浴であふれる**木漏れ日**を浴びて、心も体もリフレッシュした。  
    沐浴着洒满森林的**木漏れ日**，我们的身心焕然一新。  

## 「木漏れ日」英語での表現  
## 「木漏れ日」的英语表达  

日本語の「木漏れ日」にそのまま対応する（直訳できる）英語での表現はないとされています。そのため、英語で「木漏れ日」を表現するときには次のような説明的な文章が必要です。  

*   **sunlight filtering down through the trees**（木々から降ってくる太陽の光）
*   **sunlight that filters through the leaves of trees**（木の葉から漏れる太陽の光）
*   **sunbeams streaming  through the leaves of trees**（木の葉から降り注ぐ太陽の光線）
*   **light that comes through the leaves of trees**（木の葉を通す光）


### 「OxfordBlog」で紹介された「Komorebi」
### 「OxfordBlog」牛津博客上的 “Komorebi”   

オックスフォード大学出版局のウエブサイトである『OxfordDictionaries』のコラム記事に、**「OxfordBlog」**があります。  
牛津大学出版社网站“OxfordDictionaries”的专栏文章中有“OxfordBlog”。  

「OxfordBlog」では、「Komorebi」を「15 Japanese words that English need（英語にも必要な日本語表現15個）」の最初の言葉として、次のように説明しています。  
在“OxfordBlog”中，“Komorebi”是“15 Japanese words that English need（英语中也需要的日语表达15个）”的第一句话是这样说明的。  

> This word refers  to the sunlight shining through the leaves of trees, creating a sort of dance between the light and the leaves.  
この言葉（木漏れ日）は、木の葉の間に輝く日光や、そこにつくり出されるダンスのようなものを指している。  


## 「木漏れ日」の形は？
## 「木漏れ日」的形状是什么？   

木々の間から地面に映る木漏れ日は、すべてが太陽と同じ円の形をしています。太陽の日差しが通り抜けてくる木々の隙間や葉の形にはまったく影響されることがなく、すべてが丸い形です。  
从树木之间投射到地面上的阳光，都是和太阳一样的圆形。阳光穿过树木的缝隙和叶子的形状完全不受影响，一切都是圆形的。  

通り抜ける隙間の形に関係なく光が丸いのは、木漏れ日が丸い太陽の像だからです。このように、小さな穴の中を光が通過し、平面に像を結ぶというピンホール現象は、ピンホールカメラの原理でもあります。  
光是圆形的，与穿过的缝隙形状无关，因为透过树林的阳光是圆形的太阳像。像这样，光穿过小孔，在平面上形成图像的针孔现象也是针孔相机的原理。  


### 日食のときの不思議な木漏れ日
### 日食时不可思议的木漏れ日

木漏れ日が丸いのは太陽が丸いからですが、太陽が丸くないときはどうなるのでしょうか。太陽と月、地球がほぼ一直線上に並び、月が太陽を隠してしまう日食のときには、丸くない太陽が見られます。  
木漏れ日之所以是圆的，是因为太阳是圆的，但是太阳不是圆的时候会怎么样呢？太阳、月亮和地球几乎在一条直线上，月亮遮住太阳的日食时，可以看到不圆的太阳。  

部分日食は、太陽が月によって隠されて、太陽の一部が欠けたり、太陽が三日月形になるときがあります。このときに観察される地面に映った木漏れ日の一つ一つは、欠けた太陽と同じ形です。  
日偏食是太阳被月亮遮住，太阳的一部分缺失，或者太阳变成月牙形的时候。此时观察到的映在地面上的木漏れ日，每一个都与缺角太阳的形状相同。  

また、金環日食では、太陽の中に月がすっぽりと入り込むため、太陽は細いリングのように見えます。このときの木漏れ日は、太陽の光と同じようにリング状になっています。
在日环食中，由于月亮完全进入太阳内部，所以太阳看起来像一个细细的圆环。这个时候木漏れ日和太阳光一样呈环状。  

### 木漏れ日は日食の観察方法のひとつ
### 木漏れ日树漏日是观察日食的一种方法  

日食で太陽の光が欠けていても、欠けていない部分の太陽の光の強さは普段と変わらず、とても強いものです。そのため、直接肉眼で太陽を見るのは危険です。    
日食导致太阳光缺失，但未缺失部分的太阳光强度和平时一样，非常强。因此，直接用肉眼观察太阳是危险的。  
  
そのような危険を避けるために、日食の観察には、ピンホールを利用したり、専用のグラスを利用したりする方法があります。また、上のような理由から、木漏れ日を見て日食を観察するのも、安全な方法として推奨されています。  
为了避免这种危险，日食观察可以使用针孔或专用玻璃。另外，基于上面的理由，通过看树叶漏日来观察日食也是一种推荐的安全方法。  

