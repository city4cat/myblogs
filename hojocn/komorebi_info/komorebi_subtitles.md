## Subtitles (source: [官方在线浏览](https://mangahot.jp/site/works/j_R0018))  

(以下中文标题源自：香港玉皇朝中文3卷版)  



### 第一幕 少女與樹之精靈  

第一場 少女と木の精      / 少女與樹之精靈       page5  


第二場 七年前の少女      / 七年前的少女        page53  


第三場 偲び花           / 思念里的小花        page79  


第四場 心の花           / 心裏的花朵         page101


第五場 呪いの樹         / 受詛咒的大樹       page127  


第六場 哀しき大樹       / 哀傷的大樹         page147  


第七場 花がとりもつ縁   / 鮮花結良緣         page166



### 第二幕 秋陽杲杲  

第八場 初恋 －幸福の光る花－ / 初戀 -發出幸福之光的小花-    page7  


第九場 小さな大冒険        / 小小大冒險                 page26  


第十場 光 満ちあふれて    / 萬點熒光                    page45  


第十一場 秋の陽のデジャヴー  / 秋陽杲杲                  page65  


第十二場 スパイ大作戦      / 狙擊間諜大作戰              page85  


第十三場 思い出づくり     / 留下買好回憶                page104  


第十四場 何時か何処かで    / 某時某地                  page123  


第十五場 BODY JACKER    / BODY JACKER               page143  


第十六場 Night crawler  / NIGHT CRAWLER             page166


第十七場 CRESCENDO －クレッシェンド－    / CRESCENDO -危機逼近-  page185  



### 第三幕 Heart and Soul  

第十八場 Bad Soul           / Bad Soul          page7   


第十九場 Heart and Soul     / Heart and Soul    page26  


第二十場 嵐のあと            / 暴風雨後           page46  


第二十一場 コルチカムの咲く家  / 水仙盛放的家園      page66


第二十二場 冬の嵐 with LOVE     / 寒風有情       page87  


第二十三場 友情の木          / 友誼之樹          page106  


第二十四場 いいだせなくて…   / 盡在不言中         page126  


第二十五場 ───終幕───      / 終幕              page146  

