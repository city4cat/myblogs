
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  



# FC的绘画 - 写实风格    

提到北条司的绘画，其"写实风格"似乎是一个绕不开的话题。虽然"写实风格"被朴素地理解为“画得逼真、画得像”，但在绘画领域里，该词似乎没有明确的定义。  


## 词汇辨析  

**写实**：  
>如实地描绘事物（write or paint realistically）。[1]  

**写实风格**：  
>"写实风格是相对抽象和表现而言的。完全抽象的叫抽象，写实基础上加变形的叫表现。其它都可以泛泛的称为写实风格。其特点是：  
>1、在图像上表达与具体事物相似的形象，越像越好，追求思维与存在的统一性，就是我们能够认识到事物真正的样子，在绘画上就是画得像  
>2、依靠光影和透视在二维上塑造三维空间，就是所谓的立体感。  
>3、在有限的空间内表现时间，写实主义就是重现那一瞬间的画面就可以了，写实主义是动中求静的。认识事物的方法就是静止的一瞬，比起动态的事物，画家更喜欢停止的事物好让他们观摩模仿。"[2]  
>"写实风格在设计时要求能充分表现产品的质感和真实感，例如斜面、浮雕、投影、模糊、柔化、发光、透明、描边等特征都要表达出来。例如漫画、动画、插画等是符合写实风格的，因为其平滑的线条。"[3]  
（以上内容原始出处未知，故其权威程度未知。）  

**Realism**:    
>- "Realism(现实主义) in the arts is generally the attempt to represent subject matter truthfully, without artificiality and avoiding speculative fiction and supernatural elements. The term is often used interchangeably with naturalism, even though these terms are not synonymous.  
>Realism ... refers to a specific art historical movement that originated in France in the aftermath of the French Revolution of 1848....realism was motivated by the renewed interest in the common man and the rise of leftist politics."[4]  
>Realism不仅包括Visual arts（如绘画）, 还包括Literature、Theatre、Cinema、Opera。[4]  

>- "realism, in the arts, the accurate, detailed, unembellished depiction of nature or of contemporary life. Realism rejects imaginative idealization in favour of a close observation of outward appearances."[9]    

>- "无论是面对真实存在的物体，还是想象出来的对象，绘画者总是在描述一个真实存在的物质而不是抽象的符号。这样的创作往往被统称为写实。"[10]  

**Naturalism**:  
>"Naturalism, as an idea relating to visual representation in Western art, seeks to depict objects with the least possible amount of distortion and is tied to the development of linear perspective and illusionism in Renaissance Europe.  
>In 19th-century Europe, Naturalism or the Naturalist school was somewhat artificially erected as a term representing a breakaway sub-movement of Realism, that attempted (not wholly successfully) to distinguish itself from its parent by its avoidance of politics and social issues, and liked to proclaim a quasi-scientific basis..."[4]   

**Classicism**:  
>"In its purest form, classicism is an aesthetic attitude dependent on principles based in the culture, art and literature of ancient Greece and Rome, with the emphasis on form, simplicity, proportion, clarity of structure, perfection, restrained emotion, as well as explicit appeal to the intellect.  
>The art of classicism typically seeks to be formal and restrained。  
>Classicism ... implies a canon of widely accepted ideal forms"[5]  

**Classical Realism**:  
>"Classical Realism is an artistic movement in the late-20th and early 21st century in which drawing and painting place a high value upon skill and beauty,...  
>Classical Realism is characterized by love for the visible world and the great traditions of Western art, including Classicism, Realism and Impressionism. The movement's aesthetic is Classical in that it exhibits a preference for order, beauty, harmony and completeness;  
>Artists in this genre strive to draw and paint from the direct observation of nature, and eschew the use of photography or other mechanical aids. In this regard, Classical Realism differs from the art movements of Photorealism and Hyperrealism.  
>Classical Realist artists attempt to revive the idea of art production as it was traditionally(译注：指古希腊、文艺复兴) understood"[6]  

**Photorealism**:  
>"Photorealism is a genre of art that encompasses painting, drawing and other graphic media, in which an artist studies a photograph and then attempts to reproduce the image as realistically as possible in another medium(译注：例如画布). ...  

>- The Photo-Realist uses the camera and photograph to gather information.  
>- The Photo-Realist uses a mechanical or semi-mechanical means to transfer the information to the canvas.  
>- The Photo-Realist must have the technical ability to make the finished work appear photographic.  
>- The artist must have exhibited work as a Photo-Realist by 1972 to be considered one of the central Photo-Realists.  
>- The artist must have devoted at least five years to the development and exhibition of Photo-Realist work.  

>Photorealist painting cannot exist without the photograph. In Photorealism, change and movement must be frozen in time which must then be accurately represented by the artist. Photorealists gather their imagery and information with the camera and photograph. Once the photograph is developed (usually onto a photographic slide) the artist will systematically transfer the image from the photographic slide onto canvases. Usually this is done either by projecting the slide onto the canvas or by using traditional grid techniques.The resulting images are often direct copies of the original photograph but are usually larger than the original photograph or slide. This results in the photorealist style being tight and precise, often with an emphasis on imagery that requires a high level of technical prowess and virtuosity to simulate, such as reflections in specular surfaces and the geometric rigor of man-made environs."[7]  

**Hyperrealism**:  
>"Hyperrealism is a genre of painting and sculpture resembling a high-resolution photograph. Hyperrealism is considered an advancement of photorealism by the methods used to create the resulting paintings or sculptures.  
>The hyperrealist style focuses much more of its emphasis on details and the subjects. Hyperreal paintings and sculptures are not strict interpretations of photographs, nor are they literal illustrations of a particular scene or subject. Instead, they use additional, often subtle, pictorial elements to create the illusion of a reality which in fact either does not exist or cannot be seen by the human eye. Furthermore, they may incorporate emotional, social, cultural and political thematic elements as an extension of the painted visual illusion; a distinct departure from the older and considerably more literal school of photorealism."[8]  

## 小结  
- Realism的绘画方法追求画面逼真，绘画内容关注平民生活。   
- Naturalism的绘画方法追求画面逼真，甚至诉诸于科学方法。  
- Classicism指古希腊、古罗马时期的审美风格：注重形(form)、简洁、比例、结构清晰、追求完美、无过多个人情感。  
- Classical Realism包含Classicism、Realism、Impressionism。绘画时直接用眼观察被画物体。  
- Photorealism的画家绘画时只看（放大的）照片。  
- Hyperrealism比Photorealism更关注细节。甚至会添加不存在的细节、人眼看不见的细节。  

FC的画面里经常用线条表现物体或角色的动感，这不同于上述"写实风格"的条目3。  
FC的画面追求逼真，但远未达到Photorealism级别的逼真，所以将其归类于Naturalism似乎更合理。  


## 参考资料 
1. [写实 - 汉典](https://www.zdic.net/hans/写实)  
2. [什么是写实风格？ - Zhihu](https://www.zhihu.com/question/279180650)  
3. [关于写实风格](http://www.scicat.cn/mm/20220412/243832.html)  
4. [Realism_(arts) - Wikipedia](https://en.wikipedia.org/wiki/Realism_(arts))  
5. [Classicism - Wikipedia](https://en.wikipedia.org/wiki/Classicism)  
6. [Classical_Realism - Wikipedia](https://en.wikipedia.org/wiki/Classical_Realism)  
7. [Photorealism - Wikipedia](https://en.wikipedia.org/wiki/Photorealism)  
8. [Hyperrealism - Wikipedia](https://en.wikipedia.org/wiki/Hyperrealism_(visual_arts))  
9. [realism -Britannica ](https://www.britannica.com/art/realism-art)  
10. [写实主义 - Baike](https://baike.baidu.com/item/写实主义/8026526)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
