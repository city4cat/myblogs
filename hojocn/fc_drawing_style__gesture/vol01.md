
# FC的画风-动作、姿态  

## 目录  
[Readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

按动作、姿态分类：  
[吃](./gesture_eat.md), 
[其他](./gesture_else.md), 
[跳](./gesture_jump.md), 
[跑](./gesture_run.md), 
[坐](./gesture_sit.md), 
[蹲](./gesture_squat.md), 
[站](./gesture_stand.md), 
[走](./gesture_walk.md), 


## Vol01  

- 01_006_3, 角色身上的蓝线是角色站立时的透视线(绘制方法参见[1])，绿线表示角色的动态。绿线和蓝线相比，可以看出角色的身体倾斜程度。  
![](img/01_006_3__mod_mark.jpg)  


- 01_077_1, 10_033_2, 13_053_2, 13_067_4. 紫苑跑步的姿态(如果一只脚落地的话，这只脚落在身体中线上（或落在中线另一侧）):  
![](img/01_077_1__mod.jpg) 
![](img/01_094_0__mod.jpg) 
![](img/10_033_2__.jpg) 
![](img/13_053_2__.jpg) 
![](img/13_067_4__.jpg)  


- 01_084_2, 鞋底弯曲程度越大，越能显示出力度：  
![](img/01_084_2__mod.jpg)  
但10_072_2的弯曲程度很小，不知为何：  
![](img/10_072_2__mod.jpg)  


- 01_125_0, 动作曲线很流畅。  
![](img/01_125_0__crop.jpg) 
![](img/01_125_0__mod.jpg)  


- 01_096_3, 眼睛向下看。  
![](img/01_096_3__mod.jpg) 
![](img/01_098_0__crop.jpg)  

- 01_097_4, (手里的热米饭让雅彦感到家的温暖，雅彦因此而流泪。紫和空很惊讶。)紫手部的动作辅助表情，显得更生动。去掉手，对比一下：   
![](img/01_097_4__mod.jpg)  
![](img/01_097_4__mod_no-hand.jpg)  


- 01_139_4, 腿部的动作曲线很流畅(腿部实际应该无交叉，但动作曲线是否可以画成交叉?)。  
![](img/01_139_4__mod.jpg) 
![](img/01_139_4__mod_force-line.jpg)  


- 01_139_4, 可能的动作曲线：  
![](img/01_147_2__mod.jpg) 
![](img/01_147_2__mod_force-line1.jpg) 
![](img/01_147_2__mod_force-line2.jpg) 
![](img/01_147_2__mod_force-line0.jpg)  


- 01_150_1, 动作曲线(重心在左脚)：  
![](img/01_150_1__mod.jpg) 
![](img/01_150_1__mod_force-line.jpg)  


- 01_164_8, 人的脖子的转角只能小于90度，该镜头里应该是大于90度（如果脖子要转这个角度，那么躯干肯定要相应旋转）：  
![](img/01_164_8__.jpg)  


- 01_177_4, 动作曲线：  
![](img/01_177_4__mod.jpg) 
![](img/01_177_4__mod_force-line.jpg)  


- 01_179_0, 动作曲线：  
![](img/01_179_0__mod.jpg) 
![](img/01_179_0__mod_force-line.jpg)  


- 01_190_0, 动作曲线：  
![](img/01_190_0__mod.jpg) 
![](img/01_190_0__mod_force-line.jpg)  


- 01_202_1, 动作曲线：  
![](img/01_202_1__mod.jpg) 
![](img/01_202_1__mod_force-line.jpg)  


- 01_079_6, 01_121_5, 紫不是单单的站立，而是腿上有小动作。这样似乎更生动。都说看北条司的漫画像是看电影，我觉得画面里角色的这些小动作对此起了一定作用。    
![](img/01_079_6__.jpg) 
![](img/01_121_5__.jpg)  
![](img/01_079_6__crop0.jpg) 
![](img/01_121_5__crop0.jpg) 


## 参考链接 
1. [FC的绘画 - 透视](../fc_drawing_style__perspective/readme.md)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处