

"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这2个分格/分镜编号分别为：01_003_0, 01_003_1。  
    - 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
    - 欢迎补充！欢迎指出错误！  



# FC的画风-动作、姿态  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96900))  
由于本文很长，所以拆为多个部分。  

按动作、姿态所在的卷分类：    
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

按动作、姿态类型分类：  
[吃](./gesture_eat.md)（[总结](./gesture_eat.md#总结)），  
[其他](./gesture_else.md)（[总结](./gesture_else.md#总结)），  
[跳](./gesture_jump.md)（[总结](./gesture_jump.md#总结)），  
[跑](./gesture_run.md)（[总结](./gesture_run.md#总结)），  
[坐](./gesture_sit.md)（[总结](./gesture_sit.md#总结)），  
[蹲](./gesture_squat.md)（[总结](./gesture_squat.md#总结)），  
[站](./gesture_stand.md)（[总结](./gesture_stand.md#总结)），  
[走](./gesture_walk.md)（[总结](./gesture_walk.md#总结)），  
 
--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



