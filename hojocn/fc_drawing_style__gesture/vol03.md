# FC的画风-动作、姿态  

## 目录  
[Readme.md](./readme.md), 
[vol01](./vol01.md), 
[vol02](./vol02.md), 
[vol03](./vol03.md), 
[vol04](./vol04.md), 
[vol05](./vol05.md), 
[vol06](./vol06.md), 
[vol07](./vol07.md), 
[vol08](./vol08.md), 
[vol09](./vol09.md), 
[vol10](./vol10.md), 
[vol11](./vol11.md), 
[vol12](./vol12.md), 
[vol13](./vol13.md), 
[vol14](./vol14.md), 

按动作、姿态分类：  
[吃](./gesture_eat.md), 
[其他](./gesture_else.md), 
[跳](./gesture_jump.md), 
[跑](./gesture_run.md), 
[坐](./gesture_sit.md), 
[蹲](./gesture_squat.md), 
[站](./gesture_stand.md), 
[走](./gesture_walk.md), 


## Vol03


- 03_020_3，横田进手拿烟的姿势很生动。  
![](img/03_020_3__.jpg) 
![](img/03_020_3__crop0.jpg)  


- 03_023_2,  
![](img/03_023_2__.jpg) 
![](img/03_023_2__mod_force-line.jpg)  


- 03_025_4,  
![](img/03_025_4__mod.jpg) 
![](img/03_025_4__mod_force-line.jpg)  


- 03_028_4,  
![](img/03_028_4__mod.jpg) 
![](img/03_028_4__mod_force-line.jpg)  


- 03_035_1,  
![](img/03_035_1__mod.jpg) 
![](img/03_035_1__mod_force-line.jpg)  


- 03_041_0,  
![](img/03_041_0__mod.jpg) 
![](img/03_041_0__mod_force-line.jpg)  


- 03_041_4, 03_042_0, 站姿各不相同。  
![](img/03_041_4__mod.jpg) 
![](img/03_042_0__mod.jpg)  


- 03_045_4，  
![](img/03_045_4__mod.jpg) 


- 03_049_0，03_049_1. 紫苑购物，提着东西走路。姿势很生动。  
![](img/03_049_1__mod.jpg) 
![](img/03_049_1__mod_force-line.jpg)  


- 03_053_6,  
![](img/03_053_6__mod.jpg) 
![](img/03_053_6__mod_force-line.jpg)  


- 03_054_6,  
![](img/03_054_1__mod.jpg) 
![](img/03_054_1__mod_force-line.jpg)  


- 03_060_2,  
![](img/03_060_2__mod.jpg)   

- 03_068_5, 03_083_0, 03_083_4, 叶子跑步的姿态。  
![](img/03_068_5__mod.jpg) 
![](img/03_083_0__mod.jpg) 
![](img/03_083_4__mod.jpg)  


- 03_069_5,  
![](img/03_069_5__mod.jpg) 
![](img/03_069_5__mod_force-line.jpg)  



- 03_109_1, 紫苑、雅美、横田进跑步的姿态。横田进的手臂左右摇摆。紫苑和雅美的摆臂相同，但前后脚相反，说明有一个人是“一顺腿”(是雅美)。    
![](img/03_109_1__mod.jpg) 


- 03_144_4, 紫苑手的姿势：   
![](img/03_144_4__mod.jpg)  
![](img/03_144_4__crop0.jpg) 
![](img/03_144_4__crop1.jpg) 

- 03_144_5,  
![](img/03_144_5__mod.jpg) 
![](img/03_144_5__mod_force-line.jpg)  


- 03_150_3, 同一个画面画了2个动作：   
![](img/03_150_3__mod.jpg)  


- 03_179_3,  
![](img/03_179_3__mod.jpg) 
![](img/03_179_3__mod_force-line.jpg)  


- 03_194_1,  
![](img/03_194_1__mod.jpg) 
![](img/03_194_1__mod_force-line.jpg)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处