

# 猫眼咖啡屋的建筑风格 ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96927))  

猫眼咖啡屋(Cat's Eye Cafe)在北条司的多部作品里出现过。以下三张图依次出自《猫眼》，《城市猎人》，《天使心》：  
![](img/ce_03_062_2.jpg) 
![](img/ch_18_112_0.jpg) 
![](img/ah_01_126_0.jpg) 


猫眼咖啡屋的建筑特点：  
1. 巨大的尖屋顶；  
2. 墙壁上有横向和竖向的(黑色)纹理；  
3. 屋顶侧面有窗户；  
4. 从[7]中的一张截图可以看出，咖啡屋墙壁上的(黑色)条纹是木材：  
![](img/ce_.jpg)  

从网上的资料来看，猫眼咖啡屋类似"都铎复兴建筑"或"日本民居"。以下是详细信息。  

## "都铎建筑"和"都铎复兴建筑"  

- 都铎建筑(Tudor architecture)[1][3]  
![](img/500px-Anne_Hathaways_Cottage_1_5662418953.jpg) 
![](img/500px-Churches_Mansion_left.jpg) 

- 都铎复兴建筑(Tudor Revival architecture (a.k.a mock Tudor))[2][3]  
![](img/tudor-revival-architectural-styles-america-europe_416841-670.jpg) 
![](img/342px-1Old_English_style_house_Mosman.jpg) 
![](img/440px-Builders_tudorbethan.jpg) 

"都铎复兴建筑风格"源于"都铎建筑风格", 其风格更简单。墙壁上的横向、竖向、斜向的深色条纹是木材，这被称为半木制结构(Half-timbering)[2]。斜条纹被称为columbage[8]。半木制结构属于木制结构(Timber framing)[3]的一种。德国有很多这种风格的建筑[3]。  

猫眼咖啡屋墙壁上的(黑色)纹理不密集，更接近"都铎复兴建筑风格"。  

猫眼咖啡屋与之相似之处：  

- 巨大的尖屋顶；  
- 横向和竖向的黑色纹理；  
- 屋顶侧面有窗户；  

猫眼咖啡屋与之不同之处：  

- 猫眼咖啡屋的墙壁上没有斜向(黑色)纹理；  
    

## 日本民居(Minka/[日]民家)[4]  

- Honmune:  
![](img/250px-Babake_house_2010.jpg)  

- Gasshō/Gasshou:  
![](img/250px-Shirakawago_Japanese_Old_Village_001.jpg)  

猫眼咖啡屋与之相似之处：  

- 巨大的尖屋顶；  
- 横向和竖向的黑色纹理；  

猫眼咖啡屋与之不同之处：  

- 屋顶侧面无窗户；  


## 其他  
在[7]看到，在日本有这样的建筑：  
    ![](img/-9lddQ8j0i-750iK26T1kSfe-9y.jpg)  


## 参考资料  
1. [Tudor architecture](https://www.wikiwand.com/en/Tudor_architecture), 
   [中文版](https://www.hisour.com/zh/tudor-architecture-29795)  
2. [Tudor Revival architecture](https://www.wikiwand.com/en/Tudor_Revival_architecture), 
   [中文版](https://www.hisour.com/zh/tudor-revival-architecture-29799)  
3. [Timber Framing](https://www.wikiwand.com/en/Timber_framing)  
4. [Minka](https://en.wikipedia.org/wiki/Minka)  
5. [List of architectural styles](https://en.wikipedia.org/wiki/List_of_architectural_styles)  
6. [Japanese architecture](https://en.wikipedia.org/wiki/Japanese_architecture)  
7. [《猫眼三姐妹》品读](https://g.nga.cn/read.php?tid=28185495)  
8. [Wattle and daub - Wikiwand](https://www.wikiwand.com/en/Wattle_and_daub)  
9. [The Most Adorable 28 Of Tutor Homes Ideas](https://jhmrad.com/the-most-adorable-28-of-tutor-homes-ideas)  
10. [25 Architectural Styles In America That Will Make You Happier](https://jhmrad.com/the-most-adorable-28-of-tutor-homes-ideas/)  
11. [art-print-pop-chart-lab-featuring-american-house](https://jhmrad.com/25-architectural-styles-in-america-that-will-make-you-happier/art-print-pop-chart-lab-featuring-american-house/)  
12. [most-popular-iconic-american-home-design-styles](https://jhmrad.com/25-architectural-styles-in-america-that-will-make-you-happier/most-popular-iconic-american-home-design-styles-96/)  
13. [tudor-revival-architectural-styles-america-europe](https://jhmrad.com/25-architectural-styles-in-america-that-will-make-you-happier/tudor-revival-architectural-styles-america-europe-15/)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处