## 和泉雅子 / Masako Izumi  
https://www.douban.com/personage/27231871/photo/2425654452/  
这张图和FC里若苗紫的一张图的动作类似：   
![](./img/masako_izumi_00.jpg) 
![](./img/illustration_033__20th[jp].jpg) 

由于这个动作很常见，所以，单单从这张照片推断"该影人可能是FC的角色若苗紫的一个模特"，这可信度并不高。  
