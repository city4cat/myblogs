## 南野洋子, Laura, 小泉今日子  
Twitter上有人说[ 
[1](https://twitter.com/yae_ch3/status/1527642080574849026), 
[2](https://twitter.com/yae_ch3/status/1527643879264391169) 
]南野洋子和Laura(劳拉)像泪；小泉今日子（こいずみ きょうこ/Kyoko Koizumi）像爱：  

> 20代の私の目が見るに、  
泪：南野陽子×ローラ  
瞳：工藤静香  
愛：小泉今日子  
だと思ってるんだけど似てません？  
(瞳ちゃんもっと似てる人いるかな…)  
> 译：  
正如我在二十多岁时看到的，  
泪：南野洋子 x Laura  
瞳：工藤静香  
爱：小泉今日子  
我认为她们相似，你觉得呢？  
(有没有人长得更像瞳...)  

> ローラの骨格がもろ泪さんだし、昔のキョンキョンが愛ちゃんすぎるんだよなぁ来生三姉妹リアルにいたらえらいべっぴんさんだよ  
> 译：Laura的骨架完全像泪，曾经的キョンキョン太像小爱了。来生三姐妹在现实生活中会非常漂亮。  
> 注：キョンキョン可能是Kyoko（即指小泉今日子）  

### 南野陽子(みなみの ようこ / Yoko Minamino)：  
![](img/rui_MinaminoYoko01.jpg) 
[![](img/rui_MinaminoYoko02.jpg)](https://thetv.jp/i/tl/000/0000/0000000603_r.jpg)  
    
### Laura:  
[![](img/rui_Laura.jpg)](https://pbs.twimg.com/media/FTNIAsnagAAyYtj?format=jpg)  
其相貌似乎和《北条司Illustrations》中来生泪的一幅插画(下图)插画相似：  
![](img/illustration_rui.jpg) 
![](img/illustration_rui_crop0.jpg) 
    
### 小泉今日子(こいずみ きょうこ/Kyoko Koizumi):  
[![](img/ai_KyokoKoizumi.jpg)](https://pbs.twimg.com/media/FTNIAsoaAAAlfH6?format=jpg)  