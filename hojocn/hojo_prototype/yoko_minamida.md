## 南田洋子 / Yôko Minamida  
![](img/YokoMinamida_00.jpg) 

### 部分参演电影和剧照  
#### 青い山脈 / 青色山脉 / 绿色的山脉 / 蓝山 / Aoi sanmyaku / Beyond The Green Hills, (1963)  
![](img/Beyond.The.Green.Hills_21.jpg) 
![](img/Beyond.The.Green.Hills_20.jpg) 
![](img/Beyond.The.Green.Hills_19.jpg) 

#### 散弾銃の男 / 持散弹枪的男人 / The Man with a Shotgun, (1961)  
![](img/The.Man.with.a.Shotgun.1961.06.jpg) 
![](img/The.Man.with.a.Shotgun.1961.07.jpg) 
![](img/The.Man.with.a.Shotgun.1961.08.jpg) 
![](img/The.Man.with.a.Shotgun.1961.09.jpg) 
![](img/The.Man.with.a.Shotgun.1961.10.jpg) 
![](img/The.Man.with.a.Shotgun.1961.11.jpg) 
![](img/The.Man.with.a.Shotgun.1961.12.jpg) 
![](img/The.Man.with.a.Shotgun.1961.13.jpg) 
![](img/The.Man.with.a.Shotgun.1961.14.jpg) 
![](img/The.Man.with.a.Shotgun.1961.15.jpg) 
![](img/The.Man.with.a.Shotgun.1961.16.jpg) 
![](img/The.Man.with.a.Shotgun.1961.17.jpg) 
![](img/The.Man.with.a.Shotgun.1961.18.jpg) 

### 更多照片  
- [南田洋子 - JapaneseClass](https://japaneseclass.jp/img/南田洋子?)  
- [南田洋子 - GenSun](https://gensun.org/?q=南田洋子)  
 
### 参考资料  
- [南田洋子 - douban](https://movie.douban.com/celebrity/1033711/)  
- [Yôko Minamida - IMDB](https://www.imdb.com/name/nm0590940/)  
- [Yôko Minamida - TMDB](https://www.themoviedb.org/person/132767-y-ko-minamida)  
- [青色山脉 - douban](https://movie.douban.com/subject/10355292/)  
- [青色山脉 - IMDB](https://www.imdb.com/title/tt0439119/)  
- [青色山脉 - TMDB](https://www.themoviedb.org/movie/360530-aoi-sanmyaku)  

