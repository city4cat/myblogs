## 吉永小百合 / Sayuri Yoshinaga  
![](img/SayuriYoshinaga.jpg) 
![](img/SayuriYoshinaga_00.jpg) 
![](img/SayuriYoshinaga_01.jpg) 
![](img/SayuriYoshinaga_02.jpg) 
![](img/SayuriYoshinaga_03.jpg) 
![](img/SayuriYoshinaga_04.jpg) 
![](img/SayuriYoshinaga_05.jpg) 

### 部分参演电影和剧照  
#### 青い山脈 / 青色山脉 / 绿色的山脉 / 蓝山 / Aoi sanmyaku / Beyond The Green Hills, (1963)  
![](img/Beyond.The.Green.Hills_05.jpg) 
![](img/Beyond.The.Green.Hills_06.jpg) 
![](img/Beyond.The.Green.Hills_10.jpg) 
![](img/Beyond.The.Green.Hills_13.jpg) 
![](img/Beyond.The.Green.Hills_14.jpg) 

### 更多照片  
- [吉永小百合 - GenSun](https://gensun.org/?q=吉永小百合)  
- [Sayuri Yoshinaga - GenSun](https://gensun.org/?q=Sayuri%20Yoshinaga)  

### 参考资料  
- [吉永小百合 - douban](https://movie.douban.com/celebrity/1033094/)  
- [吉永小百合 - IMDB](https://www.imdb.com/name/nm0949045/)  
- [吉永小百合 - TMDB](https://www.themoviedb.org/person/1006012-sayuri-yoshinaga)  
- [青色山脉 - douban](https://movie.douban.com/subject/10355292/)  
- [青色山脉 - IMDB](https://www.imdb.com/title/tt0439119/)  
- [青色山脉 - TMDB](https://www.themoviedb.org/movie/360530-aoi-sanmyaku)  