说明：  

- "CE"是北条司作品"Cat's Eye"的缩写，该作品的其他名称有: キャッツアイ / 貓眼三姐妹(大陸) / 貓之眼(港) / 貓眼(台)  
- "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭(大陆) / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  
- "AH"是北条司作品"Angel Heart"的缩写，该作品的其他名称有: エンジェルハート / 天使之心(港) / 天使心(台)  


# 北条司作品里某些人物的相貌原型(猜测)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96868))   

[AH Vol21的卷首语](../zz_preambles/ah_preambles.md)提到：  
"最近看了三、四歲時（大概是首次在電影院）看的電影DVD，令我不禁「咦？」的一聲。電影的女主角是我非常喜愛的女性類型，而壞蛋那邊的女角卻是我最討厭的類型（雖然兩人都有漂亮的面孔）"。  

由此，我猜测，北条司作品里的女性角色的相貌是否以这些影星为模特/原型？同时，考虑到作品和插画里角色的相貌有变化。所以，有理由猜测，角色的相貌原型可能不唯一。  

根据一些日本1950年代～1960年代的电影和影星(北条司3～4岁时，即1962～1963年)的信息，猜测部分角色的相貌原型可能包含如下人物：    

- 来生泪(CE)、若苗紫(FC)的相貌原型可能包含有：芦川泉。  
- 来生瞳(CE)的相貌原型可能包含有：吉永小百合。  
- 来生爱(CE)的相貌原型可能包含有：田代みどり。  
- AH Vol21的卷首语提到的电影可能是：《青い山脈》(1963)。进而，片中“壞蛋那邊的女角”可能是：南田洋子。  
    


## [芦川泉 / 芦川いづみ / Izumi Ashikawa](./izumi_ashikawa.md)  


## [吉永小百合 / Sayuri Yoshinaga](./sayuri_yoshinaga.md)  


## [田代みどり / たしろ みどり / Midori Tashiro](./midori_tashiro.md)  


如果上述猜测正确的话，那么《青い山脈》中的如下镜头就是猫眼三姐妹的相貌原型的同框（从左到右：芦川泉、田代みどり、吉永小百合）：  
![](img/Beyond.The.Green.Hills_13.jpg) 
![](img/Beyond.The.Green.Hills_14.jpg)  


## [南田洋子 / Yôko Minamida](./yoko_minamida.md)  


## 高峰秀子 / Hideko Takamine  


## [和泉雅子 / Masako Izumi](./masako_izumi.md)  


##[南野洋子, Laura, 小泉今日子](./yoko_laura_kyoko.md)  


## [Jeremy Brett](./jeremy_brett.md)  




待整理：  
[关于昭和女星的更多信息](https://space.bilibili.com/29246903/article)：  
[【2021年11月视频高清化记录】昭和群星篇 ](https://www.bilibili.com/read/cv14179170?spm_id_from=333.999.0.0)  
[【2020年终总结】昭和偶像篇 ](https://www.bilibili.com/read/cv9278027?spm_id_from=333.999.0.0)  
[ 【不作の83年組】昭和史上最弱的一年新人们 ](https://www.bilibili.com/read/cv5099121?spm_id_from=333.999.0.0)  
[【2019总结之昭和偶像】 ](https://www.bilibili.com/read/cv4286284?spm_id_from=333.999.0.0)
[ 【中森明菜】时代的歌姬 歌姬的时代 ](https://www.bilibili.com/read/cv4158497?spm_id_from=333.999.0.0)  
[【昭和老资料查找方法大全】 ](https://www.bilibili.com/read/cv3415990?spm_id_from=333.999.0.0)  
[【当现代遇上后现代】SUPREME与不思議 - 松田聖子与中森明菜 ](https://www.bilibili.com/read/cv2963809?spm_id_from=333.999.0.0)  
[【令和天皇的偶像】柏原芳恵 ](https://www.bilibili.com/read/cv2629550?spm_id_from=333.999.0.0)  
[【花の82年組代表人物介绍③】早見優 石川秀美 シブがき隊 ](https://www.bilibili.com/read/cv780525?spm_id_from=333.999.0.0)  
[ 【花の82年組代表人物介绍②】小泉今日子 ](https://www.bilibili.com/read/cv364850?spm_id_from=333.999.0.0)  
[ 【花の82年組代表性人物介绍①】松本伊代 堀ちえみ 三田寛子 ](https://www.bilibili.com/read/cv321739?spm_id_from=333.999.0.0)  
[ 【昭和故事-おニャン子クラブ】小猫俱乐部风云 ](https://www.bilibili.com/read/cv267370?spm_id_from=333.999.0.0)  














--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
