## 田代みどり / たしろ みどり / Midori Tashiro  
![](img/MidoriTashiro_00.jpg) 
![](img/MidoriTashiro_01.jpg) 
![](img/MidoriTashiro_02.jpg) 

### 部分参演电影和剧照  
#### 青い山脈 / 青色山脉 / 绿色的山脉 / 蓝山 / Aoi sanmyaku / Beyond The Green Hills, (1963)  
![](img/Beyond.The.Green.Hills_13.jpg) 
![](img/Beyond.The.Green.Hills_14.jpg) 
![](img/Beyond.The.Green.Hills_15.jpg) 
![](img/Beyond.The.Green.Hills_16.jpg) 
![](img/Beyond.The.Green.Hills_17.jpg) 
![](img/Beyond.The.Green.Hills_18.jpg) 

### 更多照片：  
- [田代みどり - JapaneseClass](https://japaneseclass.jp/img/田代みどり?)  
- [田代みどり - GenSun](https://gensun.org/?q=田代みどり)  
- [Midori Tashiro - GenSun](https://gensun.org/?q=Midori%20Tashiro)  

### 参考资料  
- [田代みどり - douban](https://ja.wikipedia.org/wiki/田代みどり)  
- [田代みどり - IMDB](https://www.imdb.com/name/nm0038972/)  
- [田代みどり - TMDB](https://www.themoviedb.org/person/1666258-midori-tashiro)  
- [Midori Tashiro](https://secondhandsongs.com/artist/118414)
- [青色山脉 - douban](https://movie.douban.com/subject/10355292/)  
- [青色山脉 - IMDB](https://www.imdb.com/title/tt0439119/)  
- [青色山脉 - TMDB](https://www.themoviedb.org/movie/360530-aoi-sanmyaku)  