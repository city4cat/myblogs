
## Jeremy Brett
[CH Vol33卷首语](../zz_preambles/ch_preambles.md)：  
"有读者来信问我，今次登场的海原与《Cat's Eye》中的海原是否同一个人。其实我是以一个自己非常喜欢的英国明星为蓝本，而且他的形象与今次的角色非常配合，所以就采用了。  
《Cat's Eye》及《City Hunter》本来就是两个不同世界的作品，所以我画的时候也没有当是同一个人，有这个感觉的读者不妨思考多一点，享受个中乐趣吧。"  

我猜，这个英国明星可能是《The Adventure of Sherlock Holmes(福尔摩斯探案全集)》(1984)里的男演员Jeremy Brett（本名Peter Jeremy Willian Huggins）。  
首先，剧里福尔摩斯喜欢用针管给自己注射某药物，这对应CH里海原神(Shin Kaibara)给自己注射天使毒品。  
其次，Jeremy Brett曾和男性有暧昧关系（参见["Jeremy had an affair with a man"](https://sherlockian-sherlock.com/jeremy-brett.php) ），这或许可以对应猫眼第十卷《离南岛来的邀请函》里海原神对俊夫说：“你正是我喜欢的类型...”。  

对比海原神和Jeremy Brett：  
![](./img/shin_kaibara_CH34_cover.jpg) 
![](./img/shin_kaibara_CH33_051_5.jpg) 
![](./img/shin_kaibara_CH33_168_0.jpg) 
![](./img/shin_kaibara_CH33_180_0.jpg) 
![](./img/shin_kaibara_CH33_199_0.jpg)  

![](./img/jeremy_brett_4-a.jpg) 
![](./img/jeremy_brett_4-7.jpg) 
![](./img/jeremy_brett_4-0.jpg) 
![](./img/jeremy_brett_4-2.jpg) 
![](./img/jeremy_brett_4-3.jpg) 
![](./img/jeremy_brett_4-4.jpg) 
![](./img/jeremy_brett_4-5.jpg) 
![](./img/jeremy_brett_4-6.jpg) 

![](./img/jeremy_brett_4-8.jpg) 
![](./img/jeremy_brett_4-9.jpg) 
![](./img/jeremy_brett_p647249738.jpg) 
![](./img/jeremy_brett_p647250380.jpg) 
![](./img/jeremy_brett_p741450377.jpg) 
