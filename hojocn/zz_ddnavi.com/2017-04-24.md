source:  
https://ddnavi.com/news/366911/a/

更多信息：  
https://ddnavi.com/person/6316  
https://ddnavi.com/person_tag/北条司/  

（借助在线翻译：[Google Translate](https://translate.google.cn)，[DeepL Translate](https://www.deepl.com/translator) ）

**凄腕の始末屋から父の顔へ「エンジェル・ハート 2ndシーズン」”家族愛”名シーン・ベスト3**  
《天使之心》第二季：三大家庭爱情场景  

　北条司氏の代表作である『シティーハンター』の世界観をベースに、壮大なパラレルワールドとして描かれたストーリー。それが、『エンジェル・ハート』だ。  
  以北条司的代表作《城市猎人》（City Hunter）的世界观而绘制的壮丽平行世界的故事。那就是《天使心》。  

　闇のスィーパーでシティハンターと呼ばれた、冴羽獠。ある日、最愛のパートナーだった槇村香が突如、交通事故に遭い死亡。彼女の心臓が移植されたのは、なんとマフィアに殺人マシンとして養成された香瑩（シャンイン）。獠と出会ってからの香瑩は徐々に人の心を取り戻し、やがて二人は父と子になっていく――。  
冴羽獠，一个黑暗的清道夫和城市猎人。 有一天，他心爱的搭档香在一场车祸中死亡。她的心脏被移植到香莹的体内，香莹被黑手党训练成一个杀人机器。 遇到獠后，香莹逐渐恢复了人心，两人最终成为父女。  

　『エンジェル・ハート 2ndシーズン（ゼノンコミックス）』（北条司/徳間書店）では、凄腕の始末屋として描かれる獠の姿もあるが、獠と香瑩、そして香瑩とともに生き続ける香という3人の親子愛も垣間見ることができる。その中で、特に獠が父親の表情になる名シーン、ベスト3を紹介しよう。  
　在《天使心 第二季》（北条司/徳間書店）中，我们看到獠是一个熟练的清洁工，但我们也可以看到獠、香莹和香（活在香莹体内）之间的亲情。 这里有三个最好的场景，展现了獠作为父亲的样子。  


**第3位：第11話「想い出の写真館」（3巻）**  
第三名：第11话“记忆的照片”（第3卷）  

　5年前、あの交通事故がなければ、2人で撮るはずだったウェディングドレス姿の記念写真。それをあの時と同じ写真館で、しかも獠と香瑩、そして香に変装した殺人者・カメレオンの3人で撮影するストーリー。  
　五年前，如果不是因为一场车祸，他们两个人就会穿着婚纱一起拍照。 这是在与那次相同的照相馆拍摄的故事，但獠、香莹和伪装成香的杀手变色龙。  

　香瑩の父である獠が、今回は香とともにタキシード姿で写る家族写真。幼い頃から香を知る写真館のご主人のおかげで、とびっきりの香瑩、いや香の笑顔を見ることができる。奇跡の3ショットは、ファンならずとも必見だ。  
　在这张家庭照片中，香瑩的父亲獠与香一起穿着燕尾服。 感谢摄影工作室的老板，他从小就认识香，我们可以看到她的笑脸。这三个奇迹般的镜头是所有粉丝必看的。  

![](img/39b708c90721145944b236c07a7fa3b64.jpg)  


**第2位：第61話「獠の手料理」（13巻）**  
第2名：第61话《獠的料理》（第13卷）  

　香が初めて獠に作った料理を、今度は獠が香瑩のために作るストーリー。  
　这是香第一次为獠做饭的故事，现在獠为香瑩做饭。  

　香の得意料理だった「塩辛とジャガイモのパスタ」を食べる獠。香が「おいしい？」と聞くと、「まずい！」と獠は即答。次の瞬間、香は思わず自分のパスタを皿ごと獠にぶちまける。  
　獠吃的是咸鱼和土豆的面条，这是香的特色。 香问："好吃吗？" ，"不好吃!” 獠立即回复。 接下来，她就把面条倒给獠。   

　同じ料理を獠が香瑩のために作ると、香瑩もなぜか勝手に体が動き出し、気がつけば香瑩も獠にパスタをぶちまける――という場面。  
 当他为香瑩做同样的菜时，她开始自己行动起来，在她意识到之前，香瑩已经向獠扔面条了。  

![](img/ANGELHEART2nd13_0271.jpg)  

香から獠、そして獠から香瑩へ。思い出のパスタが家庭の味として受け継がれていくシーンに、香の料理を通じて父と娘が本当の家族になっていく様子がわかる。  
从香到獠，从獠到香瑩。在令人难忘的意大利面作为家庭的味道被传下来的场景中，你可以看到父亲和女儿如何通过尚的烹饪成为一个真正的家庭。  

![](img/ANGELHEART2nd13_0371.jpg)  


**第1位：第74話「人生の彩り」(15巻）**  
第1名：第74话《生命的颜色》（第15卷）  

　ストーリーが始まった当初は15歳だった香瑩も、ついに二十歳！ 実父である李大人しか知らない香瑩の本当の誕生日をめぐり、二人の父親の愛が交錯する。そして香瑩も、殺人マシンとして死と共に生きてきた過去から、ようやく生と向き合い、ゆっくりと大人になっていくストーリーだ。  
　在故事开始时，香瑩15岁，但现在她已经20岁了！只有她的父亲李大人知道香瑩真正的生日，因为香瑩真正生日，两个父爱有了交集。  

　李大人とは、これで永遠の別れになるかもしれない――2人の父をもつ香瑩が、これまで誰にも言えなかった想いをこらえきれず、獠の胸で号泣するシーン。コードネーム「グラスハート」（ガラスの心臓）と呼ばれた香瑩が、自らの感情を隠すことなく父である獠に表現できるようになった瞬間だ。  
  　这个场景，香瑩按捺不住以前不能向别人倾诉的感情，而扑到獠怀里痛哭。  这是代号为"玻璃心"的香瑩能够不加掩饰地向父亲獠表达自己感情的时刻。  

![](img/c7fb13326b156d7708d9922a684137f71.jpg)  

『シティーハンター』の冴羽獠しか知らない人も、十分楽しめるストーリー。血のつながりを超えた「父と娘、家族の物語」として読むのも、おすすめだ。  
即使你只知道《城市猎人》中的冴羽獠，你仍然可以享受这个故事。我还建议把它作为一个超越血缘关系的"父女、家庭故事"来阅读。  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


