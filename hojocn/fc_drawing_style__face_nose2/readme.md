
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


说明：  

- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
- 人物右脸对应图片左侧，人物左脸对应图片右侧。  
- 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
- 如有错误，请指出，非常感谢！  


# FC的画风-面部-鼻(正面)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96887))  


## 词义辨析  
nasal adj. 鼻的. [^cd]  
phenotype n. 表现型（尤指生物体的可观测性状）. [^cd]  
ala n.  翼或翼状的解剖部分或突起. [^mw]  
alae n. ala的复数形式. [^mw]  
alar adj. 翼的;翼状的.   
alary adj. 同"alar". [^mw]  
alare n. 鼻翼上最外侧的一点. [^mw]  
alares n. alare的复数形式. [^mw]  

鼻翼： wing of the nose[^li], ala nasi[^li] [^qq] [^yd] [^dl], ala of the nose[^mw],   


[^cd]: [Cambridge词典](https://dictionary.cambridge.org/zhs/词典/英语-汉语-简体/)
[^li]: [Linguee词典](https://cn.linguee.com/中文-英语/)
[^qq]: [腾讯翻译](https://fanyi.qq.com/)
[^yd]: [有道翻译](https://fanyi.youdao.com/index.html)  
[^dl]: [DeepL翻译](https://www.deepl.com/)
[^bi]: [Being翻译](https://cn.bing.com/translator)
[^mw]: [Merriam-Webster词典](https://www.merriam-webster.com/dictionary/)


## 术语  
相关资料1[^1]、2[^2]、4[^4]的术语不一致。因为考虑到1、2可信度更高，所以本文采用资料1和2中的术语。  

### 资料1[^1]中术语  
[^1]中给出了正面面部mark点:  
![](../fc_drawing_style__face/img2/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig3.png)    
其中与鼻子相关的mark点:  

- 4: glabella (g), 眉间点  
- 5: sellion (se), 鼻梁点(最凹处)  
- 6/7: alare (al), 鼻翼点  
- 8: subnasale (sn), 鼻下点, (Il[^2])    

[^1]中给出了侧面面部mark点：  
![](../fc_drawing_style__face/img2/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig4.png)    
其中与鼻子相关的mark点:  

- glabella (g) （译注：眉间点）: the most prominent or anterior point of the forehead between the eyebrows,  
- sellion (se) （译注：鼻梁点）: the most concave point in the tissue overlying the area of the frontonasal suture,  
- pronasale (prn) （译注：鼻尖）: the most prominent or anterior projection point of the nose,  
- columella breakpoint (c) （译注：(鼻)小柱断点） the highest point of the columella or breakpoint of Daniel,  
- subnasale (sn) （译注：鼻下点）: the junctional point of the columella and the upper cutaneous lip, alar curvature point or alar  
crest point (ac) （译注：波峰点）: the most lateral point in the curved base line of each ala, indicating the facial insertion of the nasal wing base, 鼻翼根与面部连接处的曲线的最外侧点  

详细信息见[FC的画风-面部2(脸颊)](../fc_drawing_style__face/readme2.md)



### 资料2[^2]中的术语  

Parts of the nose:  
**Root**          (资料2[^2]中图所示的位置、该位置的命名，与多数资料[^1] [^4] [^5] [^6]不符，原图可能有误。)  
**Bridge**       （鼻梁(+)）  
**Lateral Wall**  （[外侧壁](https://www.imaios.com/en/e-anatomy/anatomical-structure/lateral-wall-of-nasal-cavity-1541213864)）  
**Dorsum **       ([(鼻)背](https://www.imaios.com/en/e-anatomy/anatomical-structure/dorsum-of-nose-1536888688))  
**Tip**           （Apex of nose(鼻尖[^5]) ）  
**Alar Groove**   （(鼻)翼沟(+)）  
**Ala**            （Ala or wing of nose[^5], (鼻)翼(+)）  
**Columella**      （(鼻)小柱(+)）  

注:(+)表示该词条的翻译为本文作者的猜测、没有明确的出处。以下同。  

Nasal skeleton:  
**Nb** : Nasal bones 猜测：鼻骨  
**Na** : Anterior nasal aperture (piriform aperture) (或 pyriform aperture[^5search] 梨状孔[^6])  
**Ans**: Anterior nasal spine ([鼻前棘](https://www.imaios.com/en/e-anatomy/anatomical-structure/anterior-nasal-spine-1536897012))  

**Ulc**: Upper lateral cartilage （上外侧软骨(+)）  
**S**  : Septal cartilage (鼻中隔软骨[^6])  
**Mja**: Major alar cartilage (大翼软骨[^6])  
**Mia**: Minor alar cartilage (小翼软骨[^6])  
**Af** : Alar fibro-fatty tissue ((鼻)翼纤维脂肪组织(+))  

Muscles of the nasal area:  
**P**  : Procerus （降眉间肌[^5search]）  
**Nt** : Nasalis (transverse portion) ([鼻肌](https://www.imaios.com/en/e-anatomy/anatomical-structure/nasalis-muscle-1541082016) (横向部分))  
**D**  : Dilator naris anterior （[鼻外侧肌](https://zhuanlan.zhihu.com/p/46628059)。直译：前鼻孔扩张肌）  
**C**  : [Compressor narium minor](https://anatomy.app/encyclopedia/compressor-narium-minor)  
**Ds** : Depressor septi nasi([降鼻中隔肌](https://www.imaios.com/en/e-anatomy/anatomical-structure/depressor-septi-nasi-1541081624))  
**Na** : Nasalis (alar portion) (鼻肌(鼻翼部分)(+))  
**L**  : Levator labii superioris alaeque nasi （提上唇鼻翼肌[^5search]）  

DETAILED ANALYSIS OF THE FORM OF THE NOSE：  
**F**  : Facet  （侧面(+)）  
**Ar** : Alar rim （鼻翼边缘(+)）   
**Ab** : Alar base（鼻翼底部(+)）  
**Cm** : Cranial margin（颅侧边缘(+)）  
**Ag** : Alar groove（(鼻)翼沟(+)）  
**N**  : Notch （[鼻切迹](https://www.imaios.com/en/e-anatomy/anatomical-structure/nasal-notch-1536896988)）  
**Dr** : Dorsum ([(鼻)背](https://www.imaios.com/en/e-anatomy/anatomical-structure/dorsum-of-nose-1536888688))  
**Lc** : Lateral crus (外侧脚(+))  
**D**  : Dome ((鼻)拱顶(+))  

**Ic** : Intermediate crus（中侧脚(+)）  
**Mc** : Medial crus（[内侧脚](https://www.imaios.com/en/e-anatomy/anatomical-structure/medial-crus-of-major-alar-cartilage-1536897996)）  
**At** : Alar tissue ((鼻)翼组织(+))  
**Cb** : Collumellar base（原文此处可能为笔误。若原义为"Columella(r) base"，则可译为"(鼻)小柱底"。）  
**Ns** : Nostril sill (鼻堤、鼻槛、鼻孔坎(+))  
**C**  : Collumella ((鼻)小柱(+))  
**T**  : Tip ((鼻)尖(+))  
**A**  : Ala（(鼻)翼(+)）  

[^5search]: [英文IMAIOS搜索](https://www.imaios.com/en/imaios-search/(search_text)/)或[中文IMAIOS搜索](https://www.imaios.com/cn/imaios-search/(search_text)/)  
[^6]: [医学百科](https://www.yixue.com/耳鼻咽喉外科/外鼻)  


侧面[^2]：  
Nasal profile of the nose[^2]:  
"The profile of the nose refers to the shape of the outline of the bridge of the nose. It is composed of the outline of the nasal bones, and the dorsum of upper lateral cartilages. The following points are important in analysis of the form of the nose:  
**Rhinion** (**Rh**) – the bony-cartilaginous junction.  
**Supratip area** (**Sa**) – the region above the tip.  
**Supratip lobule** (**Sl**) – portion from the supratip break point to the tip defining point (T).  
**Tip** (**T**) – the part of the nose furthest from the plane of the face.  
**Infratip Lobule** (**Il**) – Lowest portion of the nasal tip.  
**Anterior nasal spine** (**Ans**) – protrusion of the maxilla at the base.  
**Nla-nasolabial angle** – angle between columella and line tangent to philtrum. Typically 92–98 degrees in men and 95–105 degrees in women."  
（鼻子的轮廓是指鼻梁轮廓的形状。它由鼻骨的轮廓和上外侧软骨的背部组成。在分析鼻子的形状时，以下几点很重要:  
**鼻梁** (**Rh**) - 骨-软骨交界处。  
**尖端上区域** (**Sa**) - 尖端以上的区域。  
**上尖端小叶** (**Sl**) - 从上尖端断点到尖端定义点(T)的部分。  
**鼻尖** (**T**) - 鼻子离面部平面最远的部分。  
**下小叶** (**Il**)- 鼻尖的最低部分。  
**鼻前棘** (**Ans**) - 上颌骨底部的突出。  
**鼻唇角** - 鼻小柱与鼻中切线的夹角。通常男性为92-98度，女性为95-105度。）[^yd]  
![](./img/Form-of-the-Head-and-Neck_nasal-profile0.jpg)
![](./img/Form-of-the-Head-and-Neck_nasal-profile1.jpg)
![](./img/Form-of-the-Head-and-Neck_nasal-profile2.jpg)
![](./img/Form-of-the-Head-and-Neck_nasal-profile3.jpg)

鼻尖（The nasal tip）[^2]：  
"The nasal tip is composed entirely of cartilage. The cartilage which creates the tip of the nose is called the lower lateral cartilage (Llc). There are two lower lateral cartilages. One forms the right side of the nasal tip. The other one forms the left side of the nasal tip. The shape of each of these Llc, and their relation to each other, is what determines the shape of the tip of the nose. There is an endless amount of variation of these cartilages which explains why there is such a variety of nasal tip shapes."  
（鼻尖完全由软骨组成。形成鼻尖的软骨叫做下外侧软骨(Llc)。有两个下外侧软骨。一个形成鼻尖的右侧。另一个形成鼻尖的左侧。这些Llc的形状，以及它们之间的关系，决定了鼻尖的形状。这些软骨的变化是无穷无尽的，这就解释了为什么鼻尖的形状会有如此多的变化。）  
Broad or Boxy Tip （宽鼻尖或方鼻尖(+)）(下图左一)， Sharp tip（尖鼻尖(+)）(下图左二)：  
![](./img/Form-of-the-Head-and-Neck_nose-tip0.jpg) 
![](./img/Form-of-the-Head-and-Neck_nose-tip1.jpg) 

正面鼻子形状的对比：  
Major ethnic nose phenotypes（主要人种的鼻型）[^2]:  
正面：  
East Asian(下图左一), North European(下图左二), African (equatorial)(下图左三), Australian Indigenous(下图左四):  
![](./img/Form-of-the-Head-and-Neck_major-ethnic-nose-phenotypes.jpg)  

侧面：  
East Asian(下图左一), North European(下图左二), African (equatorial)(下图左三), Australian Indigenous(下图左四):  
![](./img/Form-of-the-Head-and-Neck_major-ethnic-nose-phenotypes10.jpg) 
![](./img/Form-of-the-Head-and-Neck_major-ethnic-nose-phenotypes11.jpg) 
![](./img/Form-of-the-Head-and-Neck_major-ethnic-nose-phenotypes12.jpg) 
![](./img/Form-of-the-Head-and-Neck_major-ethnic-nose-phenotypes13.jpg) 

鼻宽[^2]：  
Nose width corresponded to one quarter of the face width (the nasofacial canon)  
（鼻宽相当于脸宽的四分之一(鼻面标准)）  


## FC里鼻子(正面)的一些特点  
通过观察，FC里角色的鼻子(正面)有如下特点：  

- 除了身材魁梧的角色之外，大多数角色的鼻翼未画出。  

- 下图中鼻子处的红色曲线：  
  ![](./img/nose_middle.jpg)  
  我猜，这条线是Dorsum[^2]和Lateral Wall[^2]左侧的交界线，是阴影的交界线。在绝大多数画面里，其位于正面面部中垂线(上图蓝色竖线)的右侧。  
  注：Bridge(鼻梁)和Dorsum(鼻背)是不同位置。Bridge在Dorsum上方，Dorsum区域长，详见[^2]。它们和[^1]中的点4、点5不一致或稍微有些差异。    
  对比一下。下图左、中、右三张图，中图是左图和右图的叠加：  
  ![](./img/01_025_4__mod.jpg) 
  ![](./img/01_025_4__04_000a_0__.jpg) 
  ![](./img/04_000a_0__mod.jpg) 

- 下图中S区域：  
  ![](./img/nose_middle.jpg)  
  鼻侧处的阴影区域。有时该区域被涂上阴影。       

- 为了便于比较不同角色的鼻子的特点，本文使用以下mark点：  
  ![](./img/nose_mark.jpg)  
    

------
## 部分角色的鼻子（正面）

### 雅彦(Masahiko)  
- basis:  
    - 图片: 03_131_4, 11_129_3, 13_116_4, 13_118_6, 13_139_1,       
    - 特点：     
    - 叠加：  
        ![](./img/nose_front_masahiko_basis.png)  


### 雅美(Masami)(雅彦女装)  
- front:  
    - 图片: 12_054_4, 07_178_2, 07_048_0, 03_027_3,        
    - 特点：     
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_masami_front.png) 
        ![](./img/nose_mark_masami_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 14_246_3, 04_141_0, 03_114_0,        
    - 特点：镜头俯视鼻子。       
    - 叠加：  
        ![](./img/nose_front_masami_top.png)  


### 紫苑(Shion)  
- basis:  
    - 图片: 14_282_1, 12_104_3, 11_043_7, 09_083_2, 04_139_5, 03_003_0, 
    - 特点： 成组的图片数量最多。相比于雅彦basis，鼻孔小、鼻孔更朝下、鼻尖低(几乎看不到鼻尖)、鼻侧边线圆滑。   
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_shion_basis.png) 
        ![](./img/nose_mark_shion_vs_masahiko_basis.jpg)  

- tip:  
    - 图片: 07_188_0, 04_189_5，10_008_6，02_119_6
    - 特点： 鼻尖明显。和雅彦basis几乎一致，比雅彦basis鼻孔小。         
    - 叠加：  
        ![](./img/nose_front_shion_tip.png)  


### 塩谷/盐谷(Shionoya)(紫苑男装)  
- basis:  
    - 图片: 14_067_4, 12_151_4，12_150_4，  
    - 特点： 成组的图片数量最多。鼻尖明显。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_shya_basis.png) 
        ![](./img/nose_mark_shya_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 11_009_3, 14_025_0,  
    - 特点： 镜头俯视鼻子。       
    - 叠加：  
        ![](./img/nose_front_shya_top.png)  

- 对比12_150_4、12_143_0，鼻尖、鼻孔稍微抬起。  


### 若苗空(Sora)  
- basis:  
    - 图片: 08_050_2，01_152_0, 01_00a,  
    - 特点： 成组的图片数量最多。鼻尖低于雅彦basis，鼻孔间距宽于雅彦basis。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_sora_basis.png) 
        ![](./img/nose_mark_sora_vs_masahiko_basis.jpg)  

- down:  
    - 图片: 04_015_0, 02_191_3, 08_063_3,  
    - 特点： 镜头仰视鼻子。       
    - 叠加：  
        ![](./img/nose_front_sora_down.png)  


### 若苗紫(Yukari)  
- basis:  
    - 图片: 10_048_7,09_155_5，08_066_3，08_017_5，05_050_6，04_062_4，04_022_2，02_018_6，01_200_4，01_043_0，01_025_4，    
    - 特点： 成组的图片数量最多。鼻尖明显，鼻尖低于雅彦basis。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_yukari_basis.png) 
        ![](./img/nose_mark_yukari_vs_masahiko_basis.jpg)  

- straight:  
    - 图片: 05_183_6, 01_030_0
    - 特点： 鼻尖不明显。       
    - 叠加：  
        ![](./img/nose_front_yukari_straight.png)  

- mama:  
    - 图片: 01_025_4, 02_027_2,  
    - 特点： 雅彦的妈妈。Dorsum(鼻背)长。         
    - 叠加：  
        ![](./img/nose_front_yukari_mama.png)  


### 浅冈叶子(Yoko)  
- basis:  
    - 图片: 13_142_1，13_111_3, 08_160_3,06_007_3, 05_160_2,04_178_0,     03_041_2, 
    - 特点： 成组的图片数量最多。鼻孔间距比雅彦basis窄。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_yoko_basis.png) 
        ![](./img/nose_mark_yoko_vs_masahiko_basis.jpg)  

- down:  
    - 图片: 13_101_3, 13_116_3, 
    - 特点： 镜头仰视鼻子。       
    - 叠加：  
        ![](./img/nose_front_yoko_down.png)  

- short:  
    - 图片: 02_072_6, 08_149_2, 05_097_5,  
    - 特点： 鼻子短。         
    - 叠加：  
        ![](./img/nose_front_yoko_short.png)  


### 浩美(Hiromi)  

- top:  
    - 图片: 09_058_0, 09_036_0 
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_hiromi_top.png)  

- basis:  
    - 图片: 04_091_2, 07_106_1, 05_163_0, 05_130_2
    - 特点： 成组的图片数量最多。相比雅彦basis，鼻孔小、鼻尖稍低。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_hiromi_basis.png) 
        ![](./img/nose_mark_hiromi_vs_masahiko_basis.jpg)  

- down2:  
    - 图片: 09_034_7, 05_164_0 
    - 特点： 镜头仰视的程度很大。       
    - 叠加：  
        ![](./img/nose_front_hiromi_down2.png)  

- tilt:  
    - 图片: 03_008_3，01_191_5， 
    - 特点： 鼻侧线倾斜明显。       
    - 叠加：  
        ![](./img/nose_front_hiromi_tilt.png)  


### 熏(Kaoru)  
- top2:  
    - 图片: 13_011_1，08_173_2，08_035_5   
    - 特点： 镜头俯视程度大。         
    - 叠加：  
        ![](./img/nose_front_kaoru_top2.png)  

- top:  
    - 图片: 13_042_0__r，13_010_1__r，12_171_0__r，11_045_1，09_101_3， 08_062_2__r，08_036_5
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_kaoru_top.png)  

- basis:  
    - 图片: 08_127_0，07_152_3，07_193_5，08_013_4，11_105_3，
    - 特点： 镜头平视时，成组的图片数量最多。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_kaoru_basis.png) 
        ![](./img/nose_mark_kaoru_vs_masahiko_basis.jpg)  

- down:  
    - 图片: 13_018_1__r, 13_075_6__r   
    - 特点： 镜头仰视。       
    - 叠加：  
        ![](./img/nose_front_kaoru_down.png)  



### 江岛(Ejima)  
- down:  
    - 图片: 05_025_0，05_012_0，03_004_4
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_ejima_down.png)  

- basis:  
    - 图片: 10_075_5，12_163_6, 14_025_1
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻尖稍低。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_ejima_basis.png) 
        ![](./img/nose_mark_ejima_basis_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 12_140_5，10_128_6
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_ejima_top.png)  


### 辰巳(Tatsumi)  
- down2:  
    - 图片: 13_081_7，09_110_3，08_035_2,08_033_0__r,07_168_5,09_106_3__r,  
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_tatsumi_down2.png)  

- basis:  
    - 图片: 13_074_4，11_113_1, 09_134_6,09_097_2,09_093_7,07_184_0,06_041_0, 06_027_3,06_026_5, 04_139_4, 
    - 特点： 镜头平视时，成组的图片数量最多。鼻孔宽于雅彦basis。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_tatsumi_basis.png) 
        ![](./img/nose_mark_tatsumi_basis_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 06_072_6，06_028_4, 04_135_6, 03_136_3, 03_122_0,  
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_tatsumi_top.png)  

### 导演  
- down:  
    - 图片: 12_139_1, 03_110_2,03_066_4,  
    - 特点： 镜头稍微仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_director_down.png)  

- front:  
    - 图片: 03_099_1, 03_061_2,03_035_4,
    - 特点： 鼻子短，鼻孔宽于雅彦basis。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_director_front.png) 
        ![](./img/nose_mark_director_front_vs_masahiko_basis.jpg)  

### 早纪(Saki)  
- top:  
    - 图片: 10_019_2，11_114_0，10_011_2__r，10_010_1__r，
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_saki_top.png)  

- basis:  
    - 图片: 09_182_4，09_181_1, 
    - 特点： 镜头平视时，成组的图片数量最多。几乎和雅彦basis一致(鼻尖稍低)      
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_saki_basis.png) 
        ![](./img/nose_mark_saki_vs_masahiko_basis.jpg)  


### 真琴(Makoto)  
- no-nostril:  
    - 图片: 09_056_6
    - 特点： 鼻孔未显示。         
    - 叠加：  
        ![](./img/nose_front_makoto_no-nostril.png)  

- no-nostril2:  
    - 图片: 07_072_0，
    - 特点： 鼻孔未显示。         
    - 叠加：  
        ![](./img/nose_front_makoto_no-nostril2.png)  

- basis:  
    - 图片: 11_035_0,07_108_1，07_097_0，07_075_5，03_021_2， 
    - 特点： 镜头平视时，成组的图片数量最多。鼻孔间距稍小于雅彦basis。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_makoto_basis.png) 
        ![](./img/nose_mark_makoto_vs_masahiko_basis.jpg)  

- down:  
    - 图片: 07_112_5，06_103_2，06_005_6，05_163_0，03_013_0，
    - 特点： 镜头仰视。         
    - 叠加：  
        ![](./img/nose_front_makoto_down.png)  


### 摄像师  
- basis:  
    - 图片: 14_193_1, 14_171_4, 14_036_4, 12_163_6, 12_157_5, 12_148_0, 10_067_3_r, 07_104_1, 03_070_4, 
    - 特点： 镜头平视时，成组的图片数量最多。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_cameraman_basis.png) 
        ![](./img/nose_mark_cameraman_basis_vs_masahiko_basis.jpg)  

- down2:  
    - 图片: 14_194_6, 14_171_1, 10_063_1, (03_098_4), 
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_cameraman_down2.png)  


### 浅葱(Asagi)  
- basis:  
    - 图片: 14_074_2,14_073_7,14_057_0,14_039_0, 
    - 特点： 镜头平视时，成组的图片数量最多。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
        ![](./img/nose_front_asagi_basis.png) 
        ![](./img/nose_mark_asagi_vs_masahiko_basis.jpg)  

- down:  
    - 图片: 14_123_0，14_234_0,  
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_asagi_down.png)  


### 和子(Kazuko)  
- down2:  
    - 图片: 08_126_5, 07_051_1, 06_103_2, 05_163_0, 04_189_1, 03_021_2,  01_121_3, 
    - 特点： 镜头仰视的程度更大。         
    - 叠加：  
        ![](./img/nose_front_kazuko_down2.png)   

- down:  
    - 图片: 08_141_3, 08_113_4__r 
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_kazuko_down.png)   

- basis:  
    - 图片: 08_120_0_r, 08_120_3，08_091_5， 
    - 特点： 镜头平视时，成组的图片数量最多。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_kazuko_basis.png) 
        ![](./img/nose_mark_kazuko_basis_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 11_085_2，08_133_4,08_109_3，07_075_0，07_074_8，05_061_3，02_080_4,(01_069_0, 01_066_0), 01_052_2, 
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_kazuko_top.png)   

- top2:  
    - 图片: 10_163_2,  
    - 特点： 镜头俯视的程度更大。         
    - 叠加：  
        ![](./img/nose_front_kazuko_top2.png)   


### 爷爷  
待完成  


### 横田进(Susumu)  
- down2:  
    - 图片: 08_117_6, 08_135_2, 05_163_0, 06_103_2, 03_030_0,  
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_susumu_down2.png)   

- basis:  
    - 图片: 04_189_1, 03_094_1，03_021_2，(03_016_5), 03_014_5, 03_013_3, 03_012_5,  
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻尖稍高。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_susumu_basis.png) 
        ![](./img/nose_mark_susumu_basis_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 05_065_5, (08_050_0, 05_065_0), 03_019_0,  
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_susumu_top.png)   


### 宪司(Kenji)  
- down3:  
    - 图片: 07_026_1, 07_006_6, 03_175_4,   
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_kenji_down3.png)   

- down2:  
    - 图片: 04_082_4, 04_060_5, 04_011_1,  
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_kenji_down2.png)   

- down:  
    - 图片: 10_173_2, 07_007_2, 04_010_0 
    - 特点： 镜头仰视鼻子。         
    - 叠加：  
        ![](./img/nose_front_kenji_down.png)   

- basis:  
    - 图片: 04_035_1__r, 03_189_5, 03_187_2, 03_179_4,  
    - 特点： 镜头平视时，成组的图片数量最多。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_kenji_basis.png) 
        ![](./img/nose_mark_kenji_basis_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 04_060_0, 04_051_4, 04_039_5,04_038_2__r, 04_014_3, 04_014_1,   
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_kenji_top.png)   

- top2:  
    - 图片: 04_052_5,  
    - 特点： 镜头俯视鼻子。         
    - 叠加：  
        ![](./img/nose_front_kenji_top2.png)   


### 顺子(Yoriko)  
- basis:  
    - 图片: (06_187_0), 04_016_3, 04_005_5,03_172_0,03_155_5,
    - 特点： 镜头平视时，成组的图片数量最多。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_yoriko_basis.png) 
        ![](./img/nose_mark_yoriko_basis_vs_masahiko_basis.jpg)  

- front2:  
    - 图片: 10_173_3, 04_039_0 
    - 特点： 和basis组几乎一致，但鼻梁处的斜线有差异。和雅彦basis几乎一致。           
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_yoriko_front2.png) 
        ![](./img/nose_mark_yoriko_front2_vs_masahiko_basis.jpg)  


### 森(Mori)  
- basis:  
    - 图片: (10_107_3, 10_102_3, 02_074_2), 10_102_1, 10_095_0,
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔间距小、鼻孔小。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_mori_basis.png) 
        ![](./img/nose_mark_mori_basis_vs_masahiko_basis.jpg)  

- 2个不同侧面的镜头：10_093_4，10_109_6。  
        ![](./img/nose_front_mori_basis.png) 
        ![](./img/nose_front_mori_10_093_4.jpg) 
        ![](./img/nose_front_mori_10_109_6__r.jpg)  
        和basis一起组成鼻子转动的动画：  
        ![](./img/nose_front_mori_turn_anim.gif)  


### 叶子母  
- down:  
    - 图片: 13_136_4  
    - 特点： 镜头仰视。  
    - 叠加：  
        ![](./img/nose_front_ykma_down.png)  

- basis:  
    - 图片: 13_161_3,13_103_3
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔小。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
        ![](./img/nose_front_ykma_basis.png) 
        ![](./img/nose_mark_ykma_basis_vs_masahiko_basis.jpg)  




### 理沙(Risa)  
- basis:  
    - 图片: 11_147_1, 12_025_0, 
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔小、鼻尖略低。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_risa_basis.png) 
        ![](./img/nose_mark_risa_basis_vs_masahiko_basis.jpg)  


### 美菜(Mika)  
- basis:  
    - 图片: 12_033_5,12_024_4,12_012_1,12_008_6,
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔小、鼻孔间距小。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_mika_basis.png) 
        ![](./img/nose_mark_mika_basis_vs_masahiko_basis.jpg)  

- front2:  
    - 图片: 12_053_3, 12_055_3  
    - 特点： 镜头仰视。  
    - 叠加：相比basis组，鼻侧线偏右。    
        ![](./img/nose_front_mika_front2.png)  


### 齐藤茜(Akane)  
- basis:  
    - 图片: 07_099_3,06_145_7,06_144_5,06_157_1,
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻子稍短。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_akane_basis.png) 
        ![](./img/nose_mark_akane_basis_vs_masahiko_basis.jpg)  

- no-nostrils:  
    - 图片: 07_100_2, 06_159_2  
    - 特点： 未显示鼻孔。  
    - 叠加：相比basis组，鼻侧线偏右。    
        ![](./img/nose_front_akane_no-nostrils.png)  


### 仁科(Nishina)  
- down2:  
    - 图片: 14_027_4,10_147_6   
    - 特点： 镜头仰视。  
    - 叠加：  
        ![](./img/nose_front_nishina_down2.png)  

- basis:  
    - 图片: 14_028_6, 14_014_5, 14_008_7, 
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻尖稍低。       
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_nishina_basis.png) 
        ![](./img/nose_mark_nishina_basis_vs_masahiko_basis.jpg)  


### 葵(Aoi)  
- down2:  
    - 图片: 06_077_1, 06_067_1, 06_056_0, 06_039_0   
    - 特点： 镜头仰视。  
    - 叠加：  
        ![](./img/nose_front_aoi_down2.png)  

- down:  
    - 图片: 09_101_6,  
    - 特点： 镜头仰视。  
    - 叠加：  
        ![](./img/nose_front_aoi_down.png)  

- basis:  
    - 图片: 06_075_4, 06_073_3, 06_073_4, 06_080_6,  
    - 特点： 镜头平视时，成组的图片数量最多。相比于雅彦basis，鼻孔更朝下(没有雅彦basis鼻孔那么朝天)。  
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
        ![](./img/nose_front_aoi_basis.png) 
        ![](./img/nose_mark_aoi_basis_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 06_042_1   
    - 特点： 镜头俯视。  
    - 叠加：  
        ![](./img/nose_front_aoi_top.png)  

- 06_081_3角色稍微向左转头，能感觉到06_081_3里的鼻子和basis组里的鼻子稍微有些差异(转向)：  
    ![](./img/nose_front_aoi_basis.png) 
    ![](./img/nose_front_aoi_06_081_3_vs_aoi_basis__2.gif) 
    ![](./img/nose_front_aoi_06_081_3.png)  


### 奶奶  
未处理。  


### 文哉(Fumiya)  
- down2:  
    - 图片: 12_040_3, 12_035_7,  
    - 特点： 镜头仰视。  
    - 叠加：  
        ![](./img/nose_front_fumiya_down2.png)  

- basis:  
    - 图片: 12_012_3, 12_010_3,   
    - 特点： 镜头平视时，成组的图片数量最多。  
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
        ![](./img/nose_front_fumiya_basis.png) 
        ![](./img/nose_mark_fumiya_basis_vs_masahiko_basis.jpg)  


### 齐藤玲子(Reiko)  
- 01_169_6:  
    - 图片: 01_169_6,
    - 特点：年幼时的样子。相比于雅彦basis，鼻子稍短。  
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_reiko_01_169_6.png) 
        ![](./img/nose_mark_reiko_01_169_6_vs_masahiko_basis.jpg)  


### 松下敏史(Toshifumi)  
- top:  
    - 图片: 09_077_5,09_084_4,
    - 特点：镜头稍微俯视。相比于雅彦basis，鼻孔和鼻尖更朝下。  
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_matsu_top.png) 
        ![](./img/nose_mark_matsu_top_vs_masahiko_basis.jpg)  


### 章子(Shoko)  
待完成  


### 阿透(Tooru)  
- front:  
    - 图片: 12_038_3,
    - 特点：。  
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)，几乎一致：  
        ![](./img/nose_front_tooru_front.png) 
            ![](./img/nose_mark_tooru_front_vs_masahiko_basis.jpg)  


### 京子(Kyoko)  
- basis:  
    - 图片: 02_177_2,  
    - 特点： 镜头平视时，成组的图片数量最多。鼻头更明显(更圆)。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_kyoko_basis.png) 
        ![](./img/nose_mark_kyoko_basis_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 02_186_2_r,02_186_0_r,  
    - 特点： 镜头俯视。相比于雅彦basis，鼻子稍短。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_kyoko_top.png)  


### 一马(Kazuma)  
待完成  


### 一树(Kazuki)  
- front:  
    - 图片: 12_012_3,  
    - 特点： 相比于雅彦basis，鼻孔明显（更朝天）。该镜头里，因为右脸嘴角微笑，所以右脸鼻孔稍靠上（这种细致程度让人惊讶！）。  
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_kazuki_front.png) 
        ![](./img/nose_mark_kazuki_front_vs_masahiko_basis.jpg)  


### 千夏(Chinatsu)  
- basis:  
    - 图片: (14_016_2), 12_159_3, 12_141_0,  
    - 特点： 成组的图片数量最多。鼻侧线圆滑。相比于雅彦basis，鼻孔间距小。           
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_chinatsu_basis.png) 
        ![](./img/nose_mark_chinatsu_basis_vs_masahiko_basis.jpg)  


### 麻衣(Mai)  
- basis:  
    - 图片: 12_159_3, 12_141_0,  
    - 特点： 成组的图片数量最多。相比于雅彦basis，鼻孔间距小。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_mai_basis.png) 
        ![](./img/nose_mark_mai_basis_vs_masahiko_basis.jpg)  


### 绫子(Aya)  
- basis:  
    - 图片: 02_189_4, 02_180_5, 02_177_3_ 
    - 特点： 成组的图片数量最多。鼻子稍短，鼻头大，鼻翼明显。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_aya_basis.png) 
        ![](./img/nose_mark_aya_basis_vs_masahiko_basis.jpg)  


### 典子(Tenko)  
- basis:  
    - 图片: 02_180_6, 02_178_7,  
    - 特点： 成组的图片数量最多。鼻子稍短，鼻头大，鼻翼明显。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_tenko_basis.png) 
        ![](./img/nose_mark_tenko_basis_vs_masahiko_basis.jpg)  


### 古屋公弘(Kimihiro)  
- front:  
    - 图片: 09_051_6,  
    - 特点： 相比于雅彦basis，鼻孔更朝下。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色), 几乎一致：  
        ![](./img/nose_front_kimihiro_front.png) 
        ![](./img/nose_mark_kimihiro_front_vs_masahiko_basis.jpg)  


### 古屋朋美(Tomomi)  
- front:  
    - 图片: 09_047_1,  
    - 特点： 和雅彦basis几乎一致。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_tomomi_front.png) 
        ![](./img/nose_mark_tomomi_front_vs_masahiko_basis.jpg)  

- top:  
    - 图片: 09_042_6_r,  
    - 特点： 镜头俯视。         
    - 叠加：  
        ![](./img/nose_front_tomomi_top.png)  


### 八不(Hanzu)  
- top:  
    - 图片: 02_183_7,  
    - 特点： 镜头稍微俯视。         
    - 叠加。该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/nose_front_hanzu_top.png) 
        ![](./img/nose_mark_hanzu_top_vs_masahiko_basis.jpg)  


### 哲(Te)  
没有合适的图。  


------

## 排列如下(角色鼻子, 放大2倍)**:  
![](img/nose_front_x2/nose_front_masahiko.jpg) 
![](img/nose_front_x2/nose_front_masami.jpg) 
![](img/nose_front_x2/nose_front_shion.jpg) 
![](img/nose_front_x2/nose_front_shya.jpg) 
![](img/nose_front_x2/nose_front_sora.jpg) 
![](img/nose_front_x2/nose_front_yukari.jpg) 
![](img/nose_front_x2/nose_front_yoko.jpg) 
![](img/nose_front_x2/nose_front_hiromi.jpg) 
![](img/nose_front_x2/nose_front_kaoru.jpg) 
![](img/nose_front_x2/nose_front_ejima.jpg) 
![](img/nose_front_x2/nose_front_tatsumi.jpg) 
![](img/nose_front_x2/nose_front_makoto.jpg) 
![](img/nose_front_x2/nose_front_director.jpg) 
![](img/nose_front_x2/nose_front_saki.jpg) 
![](img/nose_front_x2/nose_front_asagi.jpg) 
![](img/nose_front_x2/nose_front_cameraman.jpg) 
![](img/nose_front_x2/nose_front_yoriko.jpg) 
![](img/nose_front_x2/nose_front_mori.jpg) 
![](img/nose_front_x2/nose_front_kazuko.jpg) 
![](img/nose_front_x2/nose_front_grandpa.jpg) 
![](img/nose_front_x2/nose_front_susumu.jpg) 
![](img/nose_front_x2/nose_front_kenji.jpg) 
![](img/nose_front_x2/nose_front_ykma.jpg) 
![](img/nose_front_x2/nose_front_risa.jpg) 
![](img/nose_front_x2/nose_front_mika.jpg) 
![](img/nose_front_x2/nose_front_akane.jpg) 
![](img/nose_front_x2/nose_front_nishina.jpg) 
![](img/nose_front_x2/nose_front_aoi.jpg) 
![](img/nose_front_x2/nose_front_grandma.jpg) 
![](img/nose_front_x2/nose_front_fumiya.jpg) 
![](img/nose_front_x2/nose_front_reiko.jpg) 
![](img/nose_front_x2/nose_front_matsu.jpg) 
![](img/nose_front_x2/nose_front_shoko.jpg) 
![](img/nose_front_x2/nose_front_tooru.jpg) 
![](img/nose_front_x2/nose_front_kyoko.jpg) 
![](img/nose_front_x2/nose_front_kazuma.jpg) 
![](img/nose_front_x2/nose_front_kazuki.jpg) 
![](img/nose_front_x2/nose_front_chinatsu.jpg) 
![](img/nose_front_x2/nose_front_mai.jpg) 
![](img/nose_front_x2/nose_front_aya.jpg) 
![](img/nose_front_x2/nose_front_tenko.jpg) 
![](img/nose_front_x2/nose_front_kimihiro.jpg) 
![](img/nose_front_x2/nose_front_tomomi.jpg) 
![](img/nose_front_x2/nose_front_hanzu.jpg) 
![](img/nose_front_x2/nose_front_te.jpg) 


## 把类似的鼻子分组  
同一{}的鼻子被认为是同样的。例如，在该合并分类中，可以认为masahiko和asagi的鼻子是一样的。  

- {masahiko, masami front, shya front, asagi, fumiya, tooru ， kazuki, ejima, nishina, susumu, aoi, matsu, kimihiro}  
  ![](img/nose_front_x2/nose_front_masahiko.jpg) 
  ![](img/nose_front_x2/nose_front_masami.jpg) 
  ![](img/nose_front_x2/nose_front_shya.jpg) 
  ![](img/nose_front_x2/nose_front_asagi.jpg) 
  ![](img/nose_front_x2/nose_front_fumiya.jpg) 
  ![](img/nose_front_x2/nose_front_tooru.jpg) 
  ![](img/nose_front_x2/nose_front_kazuki.jpg) 
  ![](img/nose_front_x2/nose_front_ejima.jpg) 
  ![](img/nose_front_x2/nose_front_nishina.jpg) 
  ![](img/nose_front_x2/nose_front_susumu.jpg) 
  ![](img/nose_front_x2/nose_front_aoi.jpg) 
  ![](img/nose_front_x2/nose_front_matsu.jpg) 
  ![](img/nose_front_x2/nose_front_kimihiro.jpg)  

- {shion, risa}  
  ![](img/nose_front_x2/nose_front_shion.jpg) 
  ![](img/nose_front_x2/nose_front_risa.jpg)  

- {yukari basis, hiromi, kaoru front}  
  ![](img/nose_front_x2/nose_front_yukari.jpg) 
  ![](img/nose_front_x2/nose_front_hiromi.jpg) 
  ![](img/nose_front_x2/nose_front_kaoru.jpg)   

- {sora}    
  ![](img/nose_front_x2/nose_front_sora.jpg)  

- {makoto, yoko, saki, mori, tomomi}   
  ![](img/nose_front_x2/nose_front_makoto.jpg)  
  ![](img/nose_front_x2/nose_front_yoko.jpg) 
  ![](img/nose_front_x2/nose_front_saki.jpg) 
  ![](img/nose_front_x2/nose_front_mori.jpg) 
  ![](img/nose_front_x2/nose_front_tomomi.jpg)  

- {yoriko, mika, chinatsu, mai}  
  ![](img/nose_front_x2/nose_front_yoriko.jpg) 
  ![](img/nose_front_x2/nose_front_mika.jpg) 
  ![](img/nose_front_x2/nose_front_chinatsu.jpg) 
  ![](img/nose_front_x2/nose_front_mai.jpg) 

- {ykma}  
  ![](img/nose_front_x2/nose_front_ykma.jpg)  

- {akane, reiko} (鼻子稍短)  
  ![](img/nose_front_x2/nose_front_akane.jpg) 
  ![](img/nose_front_x2/nose_front_reiko.jpg)  

- {kyoko}  
  ![](img/nose_front_x2/nose_front_kyoko.jpg)  

- {aya, tenko}  
  ![](img/nose_front_x2/nose_front_aya.jpg) 
  ![](img/nose_front_x2/nose_front_tenko.jpg)  

- {tatsumi}  
  ![](img/nose_front_x2/nose_front_tatsumi.jpg) 

- {director, cameraman}  
  ![](img/nose_front_x2/nose_front_director.jpg) 
  ![](img/nose_front_x2/nose_front_cameraman.jpg) 

- {kazuko, kenji} (比导演/摄像师的鼻子大)  
  ![](img/nose_front_x2/nose_front_kazuko.jpg) 
  ![](img/nose_front_x2/nose_front_kenji.jpg)  

- {hanzu}  
  ![](img/nose_front_x2/nose_front_hanzu.jpg) 
    

## 进一步减少分组  
同一{}的鼻子被认为是同样的。例如，在该合并分类中，可以认为masahiko和kyoko的鼻子是一样的。  

- {masahiko, masami front, shya front, asagi, fumiya, tooru ， kazuki, ejima, nishina, susumu, aoi, matsu, kimihiro, shion, risa, yukari basis, hiromi, kaoru front, yoko, saki, makoto, mori, tomomi, yoriko, mika, chinatsu, mai, ykma, sora, kyoko, akane, reiko}  
  ![](img/nose_front_x2/nose_front_masahiko.jpg) 
  ![](img/nose_front_x2/nose_front_masami.jpg) 
  ![](img/nose_front_x2/nose_front_shya.jpg) 
  ![](img/nose_front_x2/nose_front_asagi.jpg) 
  ![](img/nose_front_x2/nose_front_fumiya.jpg) 
  ![](img/nose_front_x2/nose_front_tooru.jpg) 
  ![](img/nose_front_x2/nose_front_kazuki.jpg) 
  ![](img/nose_front_x2/nose_front_ejima.jpg) 
  ![](img/nose_front_x2/nose_front_nishina.jpg) 
  ![](img/nose_front_x2/nose_front_susumu.jpg) 
  ![](img/nose_front_x2/nose_front_aoi.jpg) 
  ![](img/nose_front_x2/nose_front_matsu.jpg) 
  ![](img/nose_front_x2/nose_front_kimihiro.jpg) 
  ![](img/nose_front_x2/nose_front_shion.jpg) 
  ![](img/nose_front_x2/nose_front_risa.jpg) 
  ![](img/nose_front_x2/nose_front_yukari.jpg) 
  ![](img/nose_front_x2/nose_front_hiromi.jpg) 
  ![](img/nose_front_x2/nose_front_kaoru.jpg) 
  ![](img/nose_front_x2/nose_front_yoko.jpg) 
  ![](img/nose_front_x2/nose_front_saki.jpg) 
  ![](img/nose_front_x2/nose_front_makoto.jpg) 
  ![](img/nose_front_x2/nose_front_mori.jpg) 
  ![](img/nose_front_x2/nose_front_tomomi.jpg) 
  ![](img/nose_front_x2/nose_front_yoriko.jpg) 
  ![](img/nose_front_x2/nose_front_mika.jpg) 
  ![](img/nose_front_x2/nose_front_chinatsu.jpg) 
  ![](img/nose_front_x2/nose_front_mai.jpg) 
  ![](img/nose_front_x2/nose_front_ykma.jpg) 
  ![](img/nose_front_x2/nose_front_sora.jpg) 
  ![](img/nose_front_x2/nose_front_kyoko.jpg) 
  ![](img/nose_front_x2/nose_front_akane.jpg) 
  ![](img/nose_front_x2/nose_front_reiko.jpg)  

- {aya, tenko, director, cameraman, kazuko, kenji, }  
  ![](img/nose_front_x2/nose_front_aya.jpg) 
  ![](img/nose_front_x2/nose_front_tenko.jpg) 
  ![](img/nose_front_x2/nose_front_director.jpg) 
  ![](img/nose_front_x2/nose_front_cameraman.jpg) 
  ![](img/nose_front_x2/nose_front_kazuko.jpg) 
  ![](img/nose_front_x2/nose_front_kenji.jpg) 

- {tatsumi}  
  ![](img/nose_front_x2/nose_front_tatsumi.jpg) 

- {hanzu}  
  ![](img/nose_front_x2/nose_front_hanzu.jpg) 



## 参考资料  
[1] [Differences between Caucasian and Asian attractive faces](https://www.researchgate.net/publication/318548459), Seung Chul Rhee, 2017.  
[2] Form of the Head and Neck, 2021, Uldis Zarins.  
[3] [MurphAinmire's Nose Study - deviantart](https://www.deviantart.com/murphainmire/art/Nose-Study-691739426)  
[4] [How to Draw a Nose - tuts+]()  
[5] [IMAIOS英文](https://www.imaios.com/en/e-anatomy), [IMAIOS中文](https://www.imaios.com/cn/e-anatomy)  

[^1]: [Differences between Caucasian and Asian attractive faces](https://www.researchgate.net/publication/318548459), Seung Chul Rhee, 2017.  
[^2]: Form of the Head and Neck, 2021, Uldis Zarins.  
[^3]: [MurphAinmire's Nose Study - deviantart](https://www.deviantart.com/murphainmire/art/Nose-Study-691739426)  
[^4]: [How to Draw a Nose - tuts+]()  
[^5]: [IMAIOS英文](https://www.imaios.com/en/e-anatomy), [IMAIOS中文](https://www.imaios.com/cn/e-anatomy)  
[Nasal Cavity|IMAIOS](https://www.imaios.com/en/e-anatomy/4/cavita-nasale), [鼻腔|IMAIOS](https://www.imaios.com/cn/e-anatomy/4/3?mic=nose-nasal-cavity-illustrations) (需等待页面顶部的鼻部解剖图缓慢加载)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
