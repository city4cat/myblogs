- 鼻子的结构[^4]：  
    1. ball(鼻头?)  
    2. wings(鼻翼)  
    3. septum(鼻中隔)  
    4. bridge(鼻梁)  
    5. tip(鼻尖)。 鼻头上部。一般有高光。    
    6. sides(鼻侧)  
    7. root(鼻根)  
![](./img/how-to-draw-a-nose-4-6.jpg)  



- 男性、女性鼻子的差异[^4]：  
![](./img/how-to-draw-a-nose-5-1_002.jpg)  

- 正面鼻子的不同形状(鼻头、鼻孔的大小和比例不同)[^4]：  
![](./img/how-to-draw-a-nose-5-3_002.jpg)  

- 正面鼻子的一些变化[^3]：  
    - 正面鼻子的三部分及其变化：  
        - 鼻梁(bridge): Width((正面)宽度), Shape((侧面)形状)，Angle((侧面)角度)  
        - 鼻孔(nostrils): Size((正面)大小)，Height((正面)高度)，Tilt((侧面)倾斜度)， Flare((侧面)外翻?)  
        - 鼻尖(tip):  Shape((正面)形状)，Size((正面)大小)，Angle((侧面)角度)，Bulbousness(大蒜鼻)  
    - 正面鼻子，整体有长度和宽度的变化：  
        - Length(长度): Short(短), Medium(中), Long(长).  
        - Width(宽度): Narrow(窄), Medium(中), Wide(宽).   
    ![](./img/MurphAinmire_NoseStudy_deviantart.jpg)  
