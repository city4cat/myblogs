http://www.netlaputa.ne.jp/~arcadia/coopers.html


![实际照片](./image/mcooper.jpg)  
「Morris Mini Cooper 1275 S Mk.1」的Leopard车模型。  
这辆车的车顶已经从黑色重新漆成白色，但原来是黑色的车顶。  


![dualtone sheet](./image/in.jpg)  
其'60年代的内饰。


![也叫徽章](./image/morris.jpg)  
中间有一只牛的Morris帽徽。  