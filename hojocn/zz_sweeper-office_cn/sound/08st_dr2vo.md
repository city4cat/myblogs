http://www.netlaputa.ne.jp/~arcadia/sound/08st_dr2vo.html

> # ▼**CITY HUNTER**  
> dramatic master2  
>   
>   
> VOCAL MASTER  
>   
> No.1　**HOLD ME TIGHT**  
>   
> 作詩　Jaye Jaye、R.Keady／作曲・編曲　田代隆廣  
> 歌　Red Monster  
> 「♪I sow your face in a mirror tonight」  
> ドラマティックマスター2のみの収録曲。  
> [4:37]  
>   
>   
> **<SCENE No.1 Cat's Eye>**  
> 喫茶キャッツアイ店内。男の依頼から逃げ出し朝帰りしたリョウの事を、美樹や冴子たちに愚痴る香だが…。  
> No.2　**LONELY LULLABY**  
>   
> 作詩　島エリナ／作曲・編曲　矢野立美  
> 歌　神谷明  
> 「♪星もない夜空にそびえる」  
> [6:54]  
>   
>   
> No.3　**WITHOUT YOU**  
>   
> 作詩　Linda Hennrick／作曲　小倉良／編曲　大谷幸  
> 歌　Jennifer Cihi  
> 「♪Once in a lonely lifetime」  
> [4:34]  
>   
>   
> No.4　**砂のCASTLEのカサノヴァ**  
>   
> 作詩　只野菜摘／作曲・編曲　矢野立美  
> 歌　北代桃子  
> 「♪タールの夜を　サイレンがゆく」  
> [3:41]  
>   
>   
> No.5　**CITY HEAT**  
>   
> 作詩　北代桃子／作曲　小倉良／編曲　大谷幸  
> 歌　小倉良  
> 「♪Heat of the night」  
> [5:22]  
>   
>   
> **<SCENE No.2 Duel 1>**  
> リョウのマンションで乾杯をする一同。冴子とリョウは、モッコリをかけてシティーハンター関連しりとりで決闘することになる。  
> No.6　**COOL CITY**  
>   
> 作詩・作曲・編曲　国吉良一  
> 歌　The City Crackers  
> [7:50]  
>   
>   
> **<SCENE No.3 Duel 2>**  
> しりとり対決の続き。クールシティー→イクラどんぶり→（略）→スノーライトシャワー  
> No.7　**SNOW LIGHT SHOWER**  
>   
> 作詩　暮湖遊／作曲・編曲　矢野立美  
> 歌　伊倉一恵  
> 「♪パーティーカクテルに　白い雪が」  
> [5:03]  
>   
>   
> **<SCENE No.4 Duel 3>**  
> さらにしりとりは続く。いつのまにかCH関連から外れ、しかも香とリョウの対決になっていく…。海坊主たちはそんな二人に乾杯。  
> No.8　**熱くなれたら**  
>   
> 作詩　竹花いち子／作曲　千沢仁／編曲　国吉良一  
> 歌　鈴木聖美  
> 「♪また心いっぱい　地図を広げてる」  
> 「CITY HUNTER3 」1～13話＜最終話まで（通算115～127話）エンディングテーマ  
> [6:43]  
>   
>   
> No.9　**NAME OF THE GAME**  
>   
> 作詩　Linda Hennrick／作曲　小倉良／編曲　大谷幸  
> 歌　Jennifer Cihi  
> 「♪Sure as heaven up above」  
> [5:06]  
>   
>   
> No.10　**BLUE AIR MESSAGE**  
>   
> 作詩　江藤孝子／作曲　大内義昭／編曲　国吉良一  
> 歌　大内義昭  
> 「♪"What's the matter Such early morning?"」  
> [4:21]  
>   
>   
> No.11　**街中sophisticate**  
>   
> 作詩　乃未紫煙／作曲・編曲　矢野立美  
> 歌　神谷明　伊倉一恵  
> 「♪毒を仕込んで微笑んだ　おまえの」  
> [3:30]  
>   
>   
> No.12　**MAKE UP TONIGHT**  
>   
> 作詩　浅田有理／作曲　東郷昌和／編曲　大谷幸  
> 歌　河合夕子  
> 「♪熱いシャワー浴びて　眺めるtwilight」  
> ドラマティックマスター2のみの収録曲。  
> [4:01]  
>   
>   
> **<SCENE No.5 Battle>**  
> リョウと海坊主は、敵と銃撃戦を展開。二人の軽妙洒脱なやり取りは、戦闘の緊張感を感じさせないほどクール。  
> No.13　**RUNNING TO HORIZON**  
>   
> 作詩　小室みつ子／作曲・編曲　小室哲哉  
> 歌　小室哲哉  
> 「♪La la la la la la Step on the gas」  
> 「CITY HUNTER3 」1～13話＜最終話まで（通算115～127話）オープニングテーマ  
> [7:17]  
>   
>   
> No.14　**YOUR SECRETS**  
>   
> 作詩　北代桃子／作曲・編曲　矢野立美  
> 歌　北代桃子  
> 「♪Softly, you whisper my name」  
> [3:09]  
>   
>   
> No.15　**CHANCE**  
>   
> 作詩　遠藤明範／作曲・編曲　矢野立美  
> 歌　神谷明  
> 「（セリフ）おれの名は冴羽リョウ　都会に巣食う虫けらどもを...」  
> [4:09]  
>   
>   

EPIC/SONY RECORDS　1990.01　ESCB1029