http://www.netlaputa.ne.jp/~arcadia/sound/09st_dr2ins.html

> # ▼**CITY HUNTER**  
> dramatic master2  
>   
>   
> INSTRUMENTAL MASTER  
>   
> No.1　**DOMINO TARGET**  
>   
> 作曲・編曲　大谷幸／演奏　大谷幸プロジェクト  
> (Instrumental)  
> [4:23]  
>   
>   
> No.2　**PARADISE ALLEY**  
>   
> 作曲・編曲　矢野立美／演奏　矢野立美プロジェクト  
> (Instrumental)  
> [3:12]  
>   
>   
> no.3　**THE BALLAD OF SILVER BULLET**  
>   
> 作曲・編曲　国吉良一／演奏　国吉良一プロジェクト  
> (Instrumental)  
> [3:26]  
>   
>   
> No.4　**CRIME AND PASSION**  
>   
> 作曲・編曲　矢野立美／演奏　矢野立美プロジェクト  
> (Instrumental)  
> [5:33]  
>   
>   
> No.5　**JUST A HUNTER**  
>   
> 作曲・編曲　矢野立美／演奏　矢野立美プロジェクト  
> (Instrumental)  
> [3:34]  
>   
>   
> No.6　**BLOOD ON THE MOON**  
>   
> 作曲・編曲　国吉良一／演奏　国吉良一プロジェクト  
> (Instrumental)  
> [4:06]  
>   
>   
> No.7　**FANTASTIC SPLASH**  
>   
> 作曲・編曲　矢野立美／演奏　矢野立美プロジェクト  
> (Instrumental)  
> [4:04]  
>   
>   
> No.8　**HOLY NIGHT RHAPSODY**  
>   
> 作曲・編曲　大谷幸／演奏　大谷幸プロジェクト  
> (Instrumental)  
> [5:31]  
>   
>   
> No.9　**FIRE WITH FIRE**  
>   
> 作曲・編曲　矢野立美／演奏　矢野立美プロジェクト  
> (Instrumental)  
> [2:54]  
>   
>   
> No.10　**MIDNIGHT LIGHTNING**  
>   
> 作曲・編曲　国吉良一／演奏　国吉良一プロジェクト  
> (Instrumental)  
> [5:22]  
>   
>   
> No.11　**THE SHINING OF CAT'S EYE**  
>   
> 作曲・編曲　矢野立美／演奏　矢野立美プロジェクト  
> (Instrumental)  
> [4:30]  
>   
>   
> No.12　**SWEET TWILIGHT**  
>   
> 作曲・編曲　大谷幸／演奏　大谷幸プロジェクト  
> (Instrumental)  
> [5:30]  
>   
>   
> No.13　**WANT YOUR LOVE**  
>   
> 作詩　北代桃子／作曲・編曲　矢野立美  
> 演奏　矢野立美プロジェクトWITH北代桃子  
> [4:00]  
>   
>   
> No.14　**LADY IN THE DARK**  
>   
> 作曲・編曲　大谷幸／演奏　大谷幸プロジェクト  
> (Instrumental)  
> [5:57]  
>   
>   
> No.15　**FEREWELL MY HUNTER**  
>   
> 作曲・編曲　矢野立美／演奏　矢野立美プロジェクト  
> (Instrumental)  
> ドラマティックマスター2のみの収録曲。  
> [4:16]  
>   
>   

ブックレットには、アニメ127話までの放送日（キー局 大阪読売テレビ）・サブタイトル・脚本／構成・絵コンテ・演出・作画監督・ゲストの資料つき。  

EPIC/SONY RECORDS　1990.01　ESCB1030