http://www.netlaputa.ne.jp/~arcadia/sound/15st_kin.html

> # ▼**CITY HUNTER SPECIAL**  
> 「緊急生中継！？　凶悪犯 冴羽リョウの最期」  
> ORIGINAL SOUNDTRACK  
>   
>   
> No.1　**プロローグ1**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:28]  
>   
>   
> No.2　**illusion city (TV SIZE)**  
>   
> 作詞・作曲　Anchang／編曲　sex MACHINEGUNS  
> 歌　sex MACHINEGUNS  
> 「♪何かお探しですか？」  
> 「CITY HUNTER SPECIAL'99 緊急生中継！？　凶悪犯 冴羽リョウの最期」オープニングテーマ  
> [2:01]  
>   
>   
> No.3　**プロローグ2**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [0:46]  
>   
>   
> No.4　**指輪のテーマ**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [3:25]  
>   
>   
> No.5　**TV MUSIC**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [2:13]  
>   
>   
> No.6　**もっ×り**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:30]  
>   
>   
> No.7　**RYO & KAORI**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:27]  
>   
>   
> No.8　**歌舞伎町のテーマ**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:19]  
>   
>   
> No.9　**凶悪なリョウ**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [0:57]  
>   
>   
> No.10　**ESCAPE FROM DARKNESS**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:23]  
>   
>   
> No.11　**マッドドックのテーマ**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:25]  
>   
>   
> No.12　**サスペンス**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [2:01]  
>   
>   
> No.13　**BATTLE**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:17]  
>   
>   
> No.14　**ハードボイルド**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:55]  
>   
>   
> No.15　**愛のテーマ**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [3:45]  
>   
>   
> No.16　**DANGER ZONE**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [2:04]  
>   
>   
> No.17　**さやかのテーマ**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:56]  
>   
>   
> No.18　**サユリのテーマ1**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [2:54]  
>   
>   
> No.19　**サユリのテーマ2**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [3:04]  
>   
>   
> No.20　**ジャックのテーマ1**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:20]  
>   
>   
> No.21　**ジャックのテーマ2**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [2:58]  
>   
>   
> No.22　**ジャックのテーマ3**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [2:07]  
>   
>   
> No.23　**MAGNUM fire (2丁目VERSION)**  
>   
> 作詞・作曲　Anchang／編曲　sex MACHINEGUNS  
> 歌　sex MACHINEGUNS  
> 「♪昨日までの出来事が」  
> [4:04]  
>   
>   
> No.24　**END OF THE STORY**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [0:51]  
>   
>   
> No.25　**エピローグ**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [0:30]  
>   
>   

「CITY HUNTER SPECIAL'99 緊急生中継！？　凶悪犯 冴羽リョウの最期」（通算146話）のエンディングテーマは「GET WILD」。  

東芝EMI株式会社　1999.05　TYCY-10019