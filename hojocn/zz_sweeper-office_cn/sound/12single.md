http://www.netlaputa.ne.jp/~arcadia/sound/12single.html

> # ▼**Dance Dance Dance**  
> ジャッキー・チェン主演CH映画「城市猟人」イメージソング  
> SINGLE CD  
>   
>   
> No.1　**Dance Dance Dance**  
>   
> 作詞　三浦徳子／作曲　根本一文／編曲　西平彰  
> 歌　宇都宮隆  
> 「♪Dance Dance Dance 哀しいままで」  
> [4.43]  
>   
>   
> c/w　**Strange Guitar**  
>   
> 作詞　西尾佐栄子／作曲　宇都宮隆／編曲　葛城哲哉  
> 歌　宇都宮隆  
> 「♪あいつからのプレゼントは」  
> [4:20]  
>   
>   

EPIC/SONY RECORDS 1993.01　ESDB3359