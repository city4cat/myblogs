http://www.netlaputa.ne.jp/~arcadia/sound/05st_mov.html

> # ▼**CITY HUNTER ～A MAGNUM OF LOVE'S DESTINATION～**  
> MUSIC FROM ORIGINAL MOTION PICTURE SOUNDTRACK  
>   
>   
> No.1　**週末のソルジャー**  
>   
> 作詩・作曲・編曲　高橋研  
> 歌　金子美香  
> 「♪Soldier in the night」  
> 「CITY HUNTER 劇場版　愛と宿命のマグナム」オープニングテーマ  
> [5:18]  
>   
>   
> No.2　**ASPHALT DANDY**  
>   
> 作曲・編曲　矢野立美  
> (Intsrumental)  
> [5:43]  
>   
>   
> No.3　**ONE AND ONLY**  
>   
> 作曲・編曲　矢野立美  
> (Intsrumental)  
> [3:32]  
>   
>   
> No.4　**TROOP IN MIDNIGHT**  
>   
> 作曲・編曲　矢野立美  
> (Intsrumental)  
> [4:11]  
>   
>   
> No.5　**ONLY IN MY DREAMS**  
>   
> 作詩　北代桃子／作曲・編曲　矢野立美  
> 歌　国分友里恵  
> 「♪Only in my dreams, had I known the place」  
> [6:31]  
>   
>   
> No.6　**GET OUT!**  
>   
> 作曲・編曲　矢野立美  
> (Intsrumental)  
> [4:56]  
>   
>   
> No.7　**LOVE GAMES**  
>   
> 作詩　北代桃子／作曲・編曲　矢野立美  
> 歌　国分友里恵  
> 「♪Your taking to me」  
> [4:19]  
>   
>   
> No.8　**NINA**  
>   
> 作曲・編曲　矢野立美  
> (Intsrumental)  
> 「CITY HUNTER 劇場版　愛と宿命のマグナム」劇中のニーナ・シュテンベルグの曲  
> [3:14]  
>   
>   
> No.9　**WISH YOU'RE HERE**  
>   
> 作曲・編曲　矢野立美  
> (Intsrumental)  
> [3:45]  
>   
>   
> No.10　**十六夜（じゅうろくや）**  
>   
> 作詩・作曲　飛鳥涼／編曲　近藤敬三  
> 歌　高橋真梨子  
> 「♪月の灯り　飲み込んでる」  
> 「CITY HUNTER 劇場版　愛と宿命のマグナム」エンディングテーマ  
> [4:46]  
>   
>   

CDには「 A MUGNAM OF LOVE'S～」とあるが、MAGNUMの誤りと思われる。  

ビクター音楽産業株式会社　1989.07　VDR-1618