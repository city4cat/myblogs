http://www.netlaputa.ne.jp/~arcadia/sound/10st_bay.html

> # ▼**CITY HUNTER ORIGINAL SPECIAL**  
> \#2 BAYCITY WARS　#3 PLOT OF $1000000  
> MUSIC FROM THE ORIGINAL MOTION PICTURE SOUNDTRACK  
>   
>   
> No.1　**ROCK MY LOVE**  
>   
> 作詩　吉元由美／作曲・編曲　馬飼野康二  
> 歌　荻野目洋子  
> 「♪淋しさを指の先で」  
> 「CITY HUNTER ORIGINAL SPECIAL ベイシティウォーズ」テーマソング  
> [4:31]  
>   
>   
> No.2　**NIGHT MOVES**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [4:40]  
>   
>   
> No.3　**DEATHTRAP**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [6:28]  
>   
>   
> No.4　**彼女の想い**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [4:02]  
>   
>   
> No.5　**WOMAN IN THE DARK**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [4:34]  
>   
>   
> No.6　**EASY-GOING GUY**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [4:14]  
>   
>   
> No.7　**TAKE ME BACK**  
>   
> 作詩　北条司、オオヤマジュンコ／作曲　中村裕介／編曲　矢野立美  
> セリフ　神谷明／歌　中村裕介、山内洋子  
> 「（セリフ）あいつが倒したグラスから...」  
> [4:09]  
>   
>   
> No.8　**BAYCITY BATTLE**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [4:28]  
>   
>   
> No.9　**MORE MORE しあわせ**  
>   
> 作詩　川村真澄／作曲　MARK DAVIS／編曲　井上Brothers  
> 歌　荻野目洋子  
> 「♪月はきらいよ」  
> 「CITY HUNTER ORIGINAL SPECIAL 百万ドルの陰謀」テーマソング  
> [4:57]  
>   
>   
> No.10　**NEVER SAY GOODBYE**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [4:45]  
>   
>   

ビクター音楽産業株式会社　1990.09　VICL-55