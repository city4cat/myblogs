http://www.netlaputa.ne.jp/~arcadia/sound/13st_sec.html

> # ▼**CITY HUNTER SPECIAL**  
> THE SECRET SERVICE  
> ORIGINAL SOUNDTRACK  
>   
>   
> No.1　**otherwise**  
>   
> 作詞・作曲　KONTA／編曲　山口英次  
> 歌　KONTA  
> 「♪肩を寄せる　肩ほしさに」  
> 「CITY HUNTER SPECIAL'96 ザ・シークレット・サービス」オープニングテーマ  
> [4:15]  
>   
>   
> No.2　**HUNTER IN THE CITY**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [3:41]  
>   
>   
> No.3　**脅迫**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [0:58]  
>   
>   
> No.4　**ガイナムの秘密**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [2:53]  
>   
>   
> No.5　**We Can Make It**  
>   
> 作詞　Brian Peck／作曲　中村裕介／編曲　飯塚昌明  
> 歌　F.E.N.  
> 「♪Isn't it time we face it」  
> [6:21]  
>   
>   
> No.6　**運命の糸**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [0:52]  
>   
>   
> No.7　**美しき依頼人**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:22]  
>   
>   
> No.8　**CHASE! CHASE! CHASE!**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:12]  
>   
>   
> No.9　**IF YOU CAN'T SEE**  
>   
> 作詞　Brian Peck／作曲・編曲　中村裕介  
> 歌　中村裕介  
> 「♪If you can't see」  
> [4:10]  
>   
>   
> No.10　**BATTLE CRUISING**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [3:08]  
>   
>   
> No.11　**たったひとつの思い出**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [3:43]  
>   
>   
> No.12　**絆**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [1:02]  
>   
>   
> No.13　**怒りの銃弾**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [2:53]  
>   
>   
> No.14　**EASY LIFE AGAIN**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [0:53]  
>   
>   
> No.15　**WOMAN**  
>   
> 作詞　石川あゆ子／作曲　中崎英也／編曲　佐藤準  
> 歌　アン・ルイス  
> 「♪つわものどもが夢のあとだね」  
> 「CITY HUNTER SPECIAL'96 ザ・シークレット・サービス」エンディングテーマ  
> [5:17]  
>   
>   
> No.16　**otherwise (TV version)**  
>   
> 作詞・作曲　KONTA／編曲　山口英次  
> 歌　KONTA  
> [1:35]  
>   
>   

「CITY HUNTER SPECIAL'96 ザ・シークレット・サービス」は（劇場版及びオリジナルスペシャル2作を含み）通算144話。  

ビクターエンタテインメント　1996.01　VICL-731