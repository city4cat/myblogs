http://www.netlaputa.ne.jp/~arcadia/sound/14st_good.html

> # ▼**CITY HUNTER SPECIAL**  
> ～GOOD-BYE MY SWEET HEART  
> ORIGINAL SOUNDTRACK  
>   
>   
> No.1　**RIDE ON THE NIGHT**  
>   
> 作詞　古屋敏之／作曲　福山芳樹／編曲　HUMMING BIRD  
> 歌　HUMMING BIRD  
> 「♪星屑流れる夜に　夢見る少年のように」  
> 「CITY HUNTER SPECIAL'97 グッド・バイ・マイ・スイート・ハート」オープニングテーマ  
> [4:09]  
>   
>   
> No.2　**OPEN YOUR EYES**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [0:33]  
>   
>   
> No.3　**PROFESSOR MUTOH**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [2:17]  
>   
>   
> No.4　**JOSHIRYO**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [1:38]  
>   
>   
> No.5　**HURRY UP!**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [2:21]  
>   
>   
> No.6　**GOOD-BYE MY...**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [2:06]  
>   
>   
> No.7　**DEAD OR ALIVE**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [2:22]  
>   
>   
> No.8　**MY DEAR**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [0:37]  
>   
>   
> No.9　**WARLIKE**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [1:13]  
>   
>   
> No.10　**BLACK ROSES**  
>   
> 作詞　衛藤利恵／作曲・編曲　西田マサラ  
> 歌　DAWN MARIE MOORE  
> 「♪The night is falling」  
> [4:53]  
>   
>   
> No.11　**FEEL SO BLUE**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [0:49]  
>   
>   
> No.12　**BURST OUT DIVING**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [1:38]  
>   
>   
> No.13　**THE TRAP**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [1:45]  
>   
>   
> No.14　**CHASE ON THE BATTLEFIELD**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [3:08]  
>   
>   
> No.15　**MEMORY OF BLACK ROSES**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [3:16]  
>   
>   
> No.16　**FORCE POLLUTION**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [1:48]  
>   
>   
> No.17　**CRITICAL MOMENT**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [0:40]  
>   
>   
> No.18　**EMI**  
>   
> 作曲・編曲　西田マサラ  
> (Instrumental)  
> [2:28]  
>   
>   
> No.19　**GET WILD ～CITY HUNTER SPECIAL '97 VERSION**  
>   
> 作詞　小室みつ子／作曲　小室哲哉／編曲　上杉洋史  
> 歌　NAHO  
> 「♪アスファルト　タイヤを」  
> 「CITY HUNTER SPECIAL'97 グッド・バイ・マイ・スイート・ハート」エンディングテーマ  
> [3:47]  
>   
>   

CITY HUNTERアニメ化10周年（通算145話）ということで、Get Wildが新バージョンで復活。  

ビクターエンタテインメント　1997.05　VICL-60017