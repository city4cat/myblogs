http://www.netlaputa.ne.jp/~arcadia/sound/11st91.html

> # ▼**CITY HUNTER'91**  
> ORIGINAL ANIMATION SOUND TRACK  
>   
>   
> No.1　**DOWN TOWN GAME**  
>   
> 作詩　松井五郎／作曲　崩場将夫／編曲　清水信之  
> 歌　GWINKO  
> 「♪ピアスで決めたり　派手なMINIを捜したり」  
> 「CITY HUNTER'91 」1～13話＜最終話まで（通算128～140話）オープニングテーマ  
> [4:55]  
>   
>   
> No.2　**may be rich**  
>   
> 作詩・作曲　Atsusi "KONTA" Kondo／編曲　Achilles C'Domigos  
> 歌　Atsusi "KONTA" Kondo  
> 「♪あばずれ Baby　ベッドから声かけんな」  
> [4:04]  
>   
>   
> No.3　**お願いKISS ME AGAIN**  
>   
> 作詩　Naoko Shimada／作曲　Yosuke Sugiyama／編曲　Soich Terada  
> 歌　NAOKO  
> 「♪Silent Night あなたが隣りにいるの」  
> [5:28]  
>   
>   
> No.4　**Spring Breeze**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [5:41]  
>   
>   
> No.5　**You never know my heart**  
>   
> 作詩　Mototaka Yamashita／作曲　Tatsuya Shinbo／編曲　Tatsuya Shinbo、Manabu Takahashi  
> 歌　橘真由美  
> 「♪危険なDirty Guy　ひょうの様に」  
> [4:23]  
>   
>   
> No.6　**熱帯魚の景色**  
>   
> 作曲・編曲　塩谷哲  
> (Instrumental)  
> [4:08]  
>   
>   
> No.7　**Bay in the night**  
>   
> 作曲・編曲　矢野立美  
> (Instrumental)  
> [5:40]  
>   
>   
> No.8　**Sometimes, Get My Love**  
>   
> 作詩　池上久(JETZT)／作曲　Shugo Shishikura(JETZT)／編曲　JETZT  
> 歌　JETZT  
> 「♪We're going to walking-down from 45 up to 42」  
> [4:25]  
>   
>   
> No.9　**Hold Me Slowly**  
>   
> 作詩・作曲・編曲　Hiromi Ito  
> 歌　野口郁子、伊藤洋  
> 「♪Listen, my baby　Do you remember?」  
> [4:19]  
>   
>   
> No.10　**SMILE&SMILE**  
>   
> 作詩　REDS／作曲　竜巻きのPIE／編曲　AURA & 佐藤宣彦  
> 歌　AURA  
> 「♪Yes, I'm lonely　都会はいつもと」  
> 「CITY HUNTER'91 」1～13話＜最終話まで（通算128～140話）エンディングテーマ  
> [5:39]  
>   
>   
> No.11　**Asphalt Moon**  
>   
> 作曲・編曲　塩谷哲  
> (Instrumental)  
> [4:04]  
>   
>   

初回限定 北条司オリジナルスーパーピクチャー・レーベルは冴子の図柄  
  

EPIC SONY RECORDS　1991.06　ESCB1148