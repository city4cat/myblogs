http://www.netlaputa.ne.jp/~arcadia/about.html

**「Sweeper Office」Contents調査報告書。**（Introduction？)

> 创建这个HomePage相当出人意料，About说明相当奇怪。  
> Macintosh/ Netscape Navigator4.04, Internet Explorer4.01/ 640*480  
> 在Windows/ Netscape Navigator4.5, Internet Explorer4.0/ 800*600下确认操作。
> 
> ---
> 
> ---
> 
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・点击TopPage上的「What's neW?」可以跳转到[FIGHT1发 更新記録](./what.md)（What's neW?），据说没有写在TopPage上的细微更新也会写在这里。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[CocktailＸＹＺ](./cocktail.md)（Cocktail-X.Y.Z.）是一个角落，解释了CityHunter世界的狂热一面，未来将有越来越多的Data加入。 「Lucky Story？」部分告诉你今天的Lucky Story。 它也可以从Top的天堂蜻蜓中访问。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[Angel☆Dust](./dust.md)（Angel Dust）是一个One Phrase的插图Gallary。 这是一个带有插图的Phrase集。 插图是一种奖励，所以希望你能忍受它们，即使它们和并不像。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[Hammer Tanma！](./tanma.md)（Stop a Hammer!）留下了墙纸和纪念图片。 其实，不能用来做壁纸的图片和不被欣赏的纪念图片都是安排好的，只是不想让你心烦意乱，围着Hammer晃悠而已。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[しゅーてぃんぐ★れんじ](./shoot/shooting.md)（Shooting Range）是射击Game的Corner。 据说即使你取得了好成绩，也不会得到任何奖品。 难度的高低似乎取决于所用电脑的性能。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[Midnight☆Hunting](http://cgi.netlaputa.ne.jp/~arcadia/cgi-bin/hunt/chat.cgi)（Midnight Hunting）是一个显而易见的CGIChat工具。 它的功能相当多，但与其说是Chat，倒不如说是用来自言自语的。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[もっこり★ほてる](./chat.md)（Couples Hotel）是使用JavaApplet的Chat。它似乎是CGI负载大时的备用品。因为是以MIDI的「STILL LOVE HER」为BGM，所以可以代替Listening Room使用，但其真正的目的似乎是从其他Chat搭讪Mokkori。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[うぉんてっどＢＢＳ](./bbs.md)（Wanted-B.B.S.）似乎是非常普通的公告栏。上面写着「裏の世界の情報ウォンテッド」，不过这只是一句装饰语，希望人们发布任何他们想要的东西。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[OneHole Shot](./link.md)（Onehole Shots）似乎是Link集。以「GET WILD」为BGM，可以进行CH系列的Site巡游。除了CH系以外，似乎只有美女的HP才能被链接。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[プロふぃ～る](./profile.md)（Profile）似乎可以窥见Sweeper Office管理员的一面。但是结果似乎什么都不明白。  
>   
> ![極楽蜻蜓♪](./image/tombow.gif)・・・[ＩＣＱMessage Panel](./icq.md)（ICQ-Panel）似乎Panel可以轻松地向管理员发送Request和寒暄等。但是有时ICQServer不稳定，Message收不到的情况似乎也很少见。  
>   

![ばなー？](./image/sweep.gif) ![ばなー？](./image/sweep01.jpg)  
Sweeper Office似乎是LinkFree。反转动画的Logo的Banner、CH2的后期OP似乎是原段子。（译注：待校对）

---

---

**>** [Office](./index.md) **<**