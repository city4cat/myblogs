source:  
[http://www.netlaputa.ne.jp/~arcadia/cocktail.html](http://www.netlaputa.ne.jp/~arcadia/cocktail.html)  

▲[Corner Top](./cocktail/cocktail2.md)  
▲Angel Heart在[这里](./ah/index.md)  
---

- CITY HUNTER是什么？  
    - ◆[Story](./cocktail/story.md)  
    　└原作、动画的解说  
    - ◆[Character](./cocktail/chara.md)  
    　└主要登场人物  
    - ◆[Music](./cocktail/music.md)  
    　└全部SoundTrack  

- 分类解说(次回更新)  
    - ◇[Automobile](./cocktail/automobile.md)  
    　└Cooper等汽车  
    - ◇[Cocktail](./cocktail/liquor.md)  
    　└Cocktail etc.  
    - ◇[Drug](./cocktail/drug.md)  
    　└Angel Dust etc.  
    - ◇[Fashion](./cocktail/fashion.md)  
    　└Fashion Item  
    - ◇[Gun](./cocktail/gun.md)  
    　└Python和其他枪械  

- 地图  
    - ○[新宿PHOTO Map](./shinjuku/map.md)  
　   └CH舞台的照片之旅  

- 各种Data  
    - ●[信息 来源](./cocktail/info.md) up  
    　└CH的最新信息  
    - ●[CH人名辞典](./cocktail/name.md)  
    　└登場人物List  
    - ●[CH年表](./cocktail/history.md)  
    　└CH的事件和现实  
    - ●[CH豆知識](./cocktail/bits.md)  
    　└一点小知识  
    - ●[CH関連書籍](./cocktail/book.md) up  
    　└Comics及其他  
    - ●[CH関連書籍2](./cocktail/book2.md)  
    　└新潮社出版Comics及其他  
    - ●[CH関連書籍3](./cocktail/book3.md)  
    　└Coamix出版Comics和其他  
    - ●[CH関連書籍4](./cocktail/book4.md) new!  
    　└徳間書店CH完全版  
    - ●[CH関連商品](./cocktail/video.md)  
    　└Video, LD, Game  
    - ●[Main Staff](./cocktail/staffm.md)  
    　└Anime的Main Staff  
    - ●[各話Staff](./cocktail/staff.md)  
    　└Anime的各話Staff  
    - ●[原作Anime対应表](./cocktail/aotable.md)  
    　└Anime化快速対应表  

- Mystery(準備中)  
    - ■[CH之谜](./cocktail/mystery.md)  
    　└CH的神秘和奇妙之处  
    - ■[原作与Anime的差异](./cocktail/difference.md)  
    　└原作和Anime之间有什么区别？ 

- Keyword(工事中)  
    - ▼[CH encyclopedia](./cocktail/uc.md)  
    　└CH百科辞典  

- 其他  
    - ★[About Mini](./mini0.md)  
    　└Mini和Cooper的历史  

- 赠品  
[[Lucky Story?](http://cgi.netlaputa.ne.jp/~arcadia/cgi-bin/lucky/kuji.cgi)]  
　└占卜  

---  
[ [Office](./index.md) ]  

（译注：原网页右侧的搜索页面如下）  
[cocktail/cocktail2.html](./cocktail/cocktail2.md)

