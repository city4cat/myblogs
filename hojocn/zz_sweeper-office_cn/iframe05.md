http://www.netlaputa.ne.jp/~arcadia/iframe05.html

**→[2006年的信息](./iframe06.md)**  
  
**▼2005年的信息**  
  
■12/22(木)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第197話  
　【指輪の記憶】  
　▽2006年春、AH动画DVD发售開始決定   
  
■12/19(月) 读卖TV/24:59～  
■01/03(火) 日本TV/26:04～  
　动画**Angel Heart** #12  
　「船上の出会いと別れ」  
　→下回「李大人からの贈り物」  
  
■12/16(金)  
　Comic Bunch新年3号(12月30日号)发售  
　「**Angel Heart**」第196話  
　【楊(ヤン)、再来！！】  
  
■12/15(木)  
　「**北条司**漫画家25周年記念  
　　自选Illustration 100幅」发售  
　→定价2940日元  
　▽新绘制的香莹封面・新绘制的AH Poster  
　▽ 专用 QUO Card Present  
  
■12/12(月) 读卖TV/25:59～  
■12/20(火) 日本TV/25:25～  
　动画**Angel Heart** #11  
　「父娘(おやこ)の時間」  
　→下回「船上の出会いと別れ」  
  
■12/09(金)  
　Comic Bunch増刊「**Angel Heart**総集編  
　动画＆Comics Special 」发售  
　→定价390日元  
　★Bunch Comics  
　「**Angel Heart**」第17巻 发售  
　▽封面是Comic Bunch第21号封面的图案  
  
■12/05(月) 读卖TV/25:09～  
■12/13(火) 日本TV/25:55～  
　动画**Angel Heart** #10  
　「Angel  スマイル」  
　→下回「父娘(おやこ)の時間」  
  
■12/02(金)  
　Comic Bunch新年1・2合并特刊(12月16日・23日号)发售  
　「**Angel Heart**」第195話/封面  
　【夏休みの終わり】  
　▽特別附录・AH Christmas Card  
　▽AH邮票表・Present申请要項  
　▽动画AH News、第10話提要和要点  
  
■11/28(月) 读卖TV/25:09～  
■12/06(火) 日本TV/25:35～  
　动画**Angel Heart** #9  
　「香瑩～失われた名前～」  
　→下回「Angel  スマイル」  
  
■11/25(金)  
　Comic Bunch第52号(12月9日号)发售  
　「**Angel Heart**」暂停。  
　▽动画AH News、第9話提要和要点  
　▽下期(12/2发售)AH Christmas Card特別附录  
  
■11/21(月) 读卖TV/25:19～  
■11/29(火) 日本TV/25:35～  
　动画**Angel Heart** #8  
　「真実(ホント)の仲間」  
　→下回「香瑩～失われた名前～」  
  
■11/18(金)  
　Comic Bunch第51号(12月2日号)发售  
　「**Angel Heart**」第194話  
　【親子の覚悟】  
　▽动画AHNews、第8話提要和要点  
　▽新年1・2合并特刊上AH Special 附录的预定  
■11/14(月) 读卖TV/25:28～  
■11/22(火) 日本TV/25:55～  
　动画**Angel Heart** #7  
　「俺の愛すべき街」  
　→下回「真実(ホント)の仲間」  
  
■11/11(金)  
　Comic Bunch第50号(11月25日号)发售  
　「**Angel Heart**」暂停。  
　▽涵盖从CE到AH的北条司自选插图 集  
　→徳間書店将于12月中旬发售決定  
　▽动画AH News、第7話提要和要点  
  
■11/07(月) 读卖TV/24:59～  
■11/08(火) 日本TV/放送休止  
⇒11/15(火) 日本TV/25:40～  
　动画**Angel Heart** #6  
　「再会」  
　→下回「俺の愛すべき街」  
  
■11/04(金)  
　Comic Bunch第49号(11月18日号)发售  
　「**Angel Heart**」第193話  
　【怒りの一撃！】  
　▽Angel Heart総集編 12/09(金)发售決定  
　▽动画AH News、第6話提要和要点  
  
■10/31(月) 读卖TV/24:59～  
■11/01(火) 日本TV/26:25～  
　动画**Angel Heart** #5  
　「永別(さよなら) …カオリ」  
　→下回「再会」  
  
■10/28(金)  
　Comic Bunch第48号(11月11日号)发售  
　「**Angel Heart**」第192話  
　【親子の電話】  
　▽动画AH News、第5話提要和要点  
　→「誰かが君を想ってる」CDPresent(30名)  
  
■10/24(月) 读卖TV/25:14～  
■10/25(火) 日本TV/25:25～  
　动画**Angel Heart** #4  
　「さまようHEART」  
　→下回「永別(さよなら) …カオリ」  
  
■10/21(金)  
　Comic Bunch第47号(11月4日号)发售  
　「**Angel Heart**」第191話  
　【恐怖のカーチェイス！】  
　▽动画AH News、第4話提要和要点  
　→「誰かが君を想ってる」CD 11/9(水)发售  
  
■10/17(月) 读卖TV/25:13～  
■10/18(火) 日本TV/25:34～  
　动画**Angel Heart** #3  
　「XYZの街」  
　→下回「さまようHEART」  
  
■10/14(金)  
　Comic Bunch第46号(10月28日号)发售  
　「**Angel Heart**」第190話  
　【バスジャック！】  
　▽动画AHNews、第3話提要和要点  
  
■10/10(月) 读卖TV/25:28～  
■10/11(火) 日本TV/25:49～  
　动画**Angel Heart** #2  
　「香が帰ってきた」  
　→下回「XYZの街」  
  
■10/07(金)  
　Comic Bunch第45号(10月21日号)发售  
　「**Angel Heart**」第189話/封面  
　【変質者、現る！】  
　▽动画CColor特集  
　OP/ED曲・Artist's Comment  
  
■10/03(月) 读卖TV/25:34～  
■10/04(火) 日本TV/26:25～  
　动画Angel Heart放送開始  
　动画**Angel Heart** #1  
　「ガラスの心臓 グラス・ハート」  
  
■09/30(金)  
　Comic Bunch第44号(10月14日号)发售  
　「**Angel Heart**」第188話/封面  
　【海学級へ行こう！】  
　▽动画巻頭Color特集／1話提要和要点etc.  
　→OP曲「Finally」by Sowelu  
　→ED曲「誰かが君を想ってる」by Skoop On Somebody  
　▽动画放映記念附录「CD型卓上Calendar」  
　▽「香瑩等身大Poster」全員Service申请要項  
  
■09/22(木)  
　Comic Bunch第43号(10月7日号)发售  
　「**Angel Heart**」第187話  
　【ミキが行方不明！？】  
　▽下期(44号)是2大特典付き・巻頭Color特集  
  
■09/16(金)  
　Comic Bunch第42号(9月30日号)  
　「**Angel Heart**」暂停。  
　▽动画AH放送日決定  
　读卖TV・10月3日(月)~~24:58～~~  
　日本TV・10月4日(火)~~25:25～~~  
  
■09/09(金)  
　Comic Bunch第41号(9月23日号)发售  
　「**Angel Heart**」第186話/封面  
　【変質者是海坊主！？】  
　▽动画AH News  
　→首版・人物对照表  
  
■09/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第16巻 发售  
　▽封面是Comic Bunch新年4・5合并特刊封面的图案  
  
■09/02(金)  
　Comic Bunch第40号(9月16日号)发售  
　「**Angel Heart**」第185話  
　【海坊主と先生】  
　▽潜入动画AH制作发布会  
　▽决定每周播出日  
　读卖TV・每周一24:58～  
　日本TV・每周二25:25～  
  
■08/26(金)  
　Comic Bunch第39号(9月9日号)发售  
　「**Angel Heart**」第184話  
　【義父(ちち)是つらいよ！】  
  
■08/12(金)  
　Comic Bunch第37・38合并特刊 发售  
　「**Angel Heart**」第183話  
　【麗子の未来】  
　▽CColor・动画AH速報  
　→读卖TV・日本TV今秋播出  
　→AH Digest DVD Present(100名)  
　▽13日～在银座举行CH展览  
　▽CAT'S EYE完全版・计划在10月登场  
  
■08/05(金)  
　Comic Bunch第36号(8月19日号)发售  
　「**Angel Heart**」第182話  
　【信宏が教えてくれた事】  
  
■07/29(金)  
　Comic Bunch第35号(8月12日号)  
　「**Angel Heart**」暂停。  
　▽CH COMPLETE DVD BOX Color广告  
　▽动画AH News・Poster用插图   
　▽令人兴奋的宝島宝島in天保山「**Heart of City**」  
　→7/31(日) 神谷明和川崎真央登場  
　　详情是http://www.ytv.co.jp/wakuwaku/  
　▽THE **CITY HUNTER**展 Vol.1 銀座  
　→DVD BOX・AH动画化記念／原画大公開・企划齐全  
　　(8/13～8/21 Sony大厦8楼)详见第37、38期合并号  
  
■07/22(金)  
　Comic Bunch第34号(8月5日号)发售  
　「**Angel Heart**」第181話  
　【未来を変える男】  
  
■07/14(木)  
　Comic Bunch第33号(7月29日号)发售  
　「**Angel Heart**」第180話  
　【引き寄せられる運命】  
  
■07/08(金)  
　Comic Bunch第32号(7月22日号)发售  
　「**Angel Heart**」第179話  
　【笑顔の未来】  
  
■07/01(金)  
　Comic Bunch第31号(7月15日号)发售  
　「**Angel Heart**」第178話  
　【透視の代償】  
  
■06/24(金)  
　Comic Bunch第30号(7月8日号)发售  
　「**Angel Heart**」第177話  
　【男の信念】  
  
■06/17(金)  
　Comic Bunch第29号(7月1日号)发售  
　「**Angel Heart**」第176話  
　【悲しい二人】  
  
■06/15(水)  
　「**CITY HUNTER** 完全版」  
　別巻特別短編集[Z] 由德间书店发售  
　→[Get Wild] 带特别附录CD   
　▽Color色紙Present(抽签200名)  
  
■06/10(金)  
　Comic Bunch第28号(6月24日号)发售  
　「**Angel Heart**」第175話/封面  
　【恋人達の未来】  
　▽CColor特集「A・Hを構築する3つの世界」（译注：构建A·H的3个世界）    
　→Angel BOX的申请券  
  
■06/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第15巻 发售  
　▽封面是Comic Bunch第28号封面的图案  
　→封面画18枚Set「Angel BOX」Present申请券  
  
  
■06/03(金)  
　Comic Bunch第27号(6月17日号)  
　「**Angel Heart**」暂停。  
　▽下期(28号)是AH封面＆Center Color特集  
  
■05/27(金)  
　Comic Bunch第26号(6月10日号)发售  
　「**Angel Heart**」第174話  
　【麗泉(れいせん)の悩み】  
  
■05/20(金)  
　Comic Bunch第25号(6月3日号)发售  
　「**Angel Heart**」第173話  
　【アカルイミライ！？】  
  
■05/14(土)  
　「**CITY HUNTER** 完全版」  
　別巻插画集[Y]由德间书店发售  
　▽Color色紙Present(抽签200名)  
  
■05/12(木)  
　Comic Bunch第24号(5月27日号)发售  
　「**Angel Heart**」第172話  
　【母なる思い】  
　▽北斗の拳・CH的Sticker＆走运千社札带  
  
■04/28(木)  
　Comic Bunch第22・23合并特大号 发售  
　「**Angel Heart**」第171話  
　【再会…！！】  
　▽下期是5月12日(木)发售  
  
■04/22(金)  
　Comic Bunch第21号(5月6日号)发售  
　「**Angel Heart**」第170話/封面  
　【楊(ヤン)、始動！】  
　▽下期是4月28日(木)发售  
  
■04/15(金)  
　Comic Bunch第20号(4月29日号)发售  
　「**Angel Heart**」第169話  
　【黒幕現る！！】  
  
■04/15(金)  
　「**CITY HUNTER** 完全版」  
　VOL.32 (完結) 由徳間書店发售  
　▽Complete BOX全員Service応募方法  
　▽獠＆香QUO card Present
　→详情见书带  
  
■04/08(金)  
　Comic Bunch第19号(4月22日号)发售  
　「**Angel Heart**」第168話  
　【天国と地獄！？】  
　▽动画香瑩役、川崎真央的直撃Interview  
  
■04/03(日)23時～NACK5  
■04/02(土)24時～FM愛知・FM大阪  
　ラジオ番組「**HEART OF ANGEL**」内Corner  
　「XYZ Ryo's Bar」放送スタート  
　→神谷明和美女Guest的TalkCorner  
　▽第一回目のGuest是麻上洋子和伊倉一恵  

■04/01(金)  
　Comic Bunch第18号(4月15日号)  
　▽动画AH声優Audition発表  
　→香瑩役声優：川崎真央(18岁)  
　「**Angel Heart**」暂停。  
  
■03/25(金)  
　Comic Bunch第17号(4月8日号)发售  
　「**Angel Heart**」第167話  
　【楊芳玉(ヤンファンユィ)の仕事】  
　▽下期(18号)、动画AH大特集  
　→香瑩役声優大公布/本篇暂停  
  
■03/17(木)  
　Comic Bunch第16号(4月1日号)发售  
　「**Angel Heart**」第166話  
　【愛は地球を救う！？】  
　▽动画AH News  
　潜入香瑩役声優Auditionに！  
　香瑩＆獠 线画设定定稿！  
  
　▽动画放送日時  
　日本TV系 4月22日(金)27時8分～  
　读卖TV系4月18日(月)24時58分～  
  
■03/15(火)  
　「**CITY HUNTER** 完全版」  
　VOL.30、VOL.31 徳間書店より发售  
　▽3巻連続 愛読者W感謝企画スタート  
　→詳しくは帯にて  
  
■03/11(金)  
　Comic Bunch第15号(3月25日号)发售  
　「**Angel Heart**」第165話  
　【楊(ヤン)からの依頼？】  
　▽Anime AH News  
　　配上天使之翼logo决定！  
　　主要Cast和CH一样！  
  
■03/04(金)  
　Comic Bunch第14号(3月18日号)发售  
　「**Angel Heart**」第164話  
　【それぞれの場所】  
  
■02/25(金)  
　Comic Bunch第13号(3月11日号)发售  
　「**Angel Heart**」第163話  
　【女同士の酒】  
　▽CColor/手机AH Game「SameBunch」攻略留言板  
　▽北斗の拳Raoh外传・2006年春改编为电影  
　→新角色悲伤女战士「Reina」角色由北条先生设计 
  
■02/18(金)  
　Comic Bunch第12号(3月4日号)发售  
　「**Angel Heart**」第162話  
　【獠の大失敗】  
  
■02/18(金)  
　「**CITY HUNTER** 完全版」  
　VOL.28、VOL.29 由徳間書店发售  
  
■02/10(木)  
　Comic Bunch第11号(2月25日号)  
　「**Angel Heart**」暂停。  
  
■02/09(水)  
　★Bunch Comics  
　「**Angel Heart**」第14巻 发售  
　▽封面是Comic Bunch'03年第52号封面  
  
■02/04(金)  
　Comic Bunch第10号(2月18日号)发售  
　「**Angel Heart**」第161話  
　【恋の覚悟】  
  
■01/28(金)  
　Comic Bunch第9号(2月11日号)发售  
　「**Angel Heart**」第160話  
　【ホレた男是謎だらけ！？】  
　▽今春TV放映決定！动画AH速報第二弾  
　　CenterColor・香瑩役声優Audition详情  
  
■01/21(金)  
　Comic Bunch第8号(2月4日号)发售  
　「**Angel Heart**」第159話  
　【私、恋してます！】  
  
■01/15(土)  
　「**CITY HUNTER** 完全版」  
　VOL.26、VOL.27 由徳間書店发售  
  
■01/14(金)  
　Comic Bunch第7号(1月28日号)发售  
　「**Angel Heart**」第158話  
　【愛される理由】  
　▽香瑩役声優Audition详情系第9期  
  
■01/07(金)  
　Comic Bunch新年6号(1月21日号)发售  
　「**Angel Heart**」第157話  
　【獠のケータイライフ】  
　▽香瑩役声優Audition開催決定  
  
**→[2004年的信息](./iframe04.md)**