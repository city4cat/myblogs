http://www.netlaputa.ne.jp/~arcadia/ah/ahname_ma.html

Angel Heart 人名辞典

---

ま行  
|　[ま](./ahname_ma.md#ma)　|　[み](./ahname_ma.md#mi)　|　[む](./ahname_ma.md#mu)　|　[め](./ahname_ma.md#me)　|　[も](./ahname_ma.md#mo)　|

> ま  
>   
> まきむら-かおり【槇村香】  
> リョウのパートナーだったが交通事故で命を落とし、その心臓はグラス・ハートに移植される。  
> →初登場[第2話](./ahstory1.md#2)？1話？  
>   
>   
> み  
>   
> - Nothing -  
>   
>   
> む  
>   
> - Nothing -  
>   
>   
> め  
>   
> - Nothing -  
>   
>   
> も  
>   
> もちやま-ひでお【餅山秀夫】  
> 根掘会系暴力団の若頭。39歳。XYZの暗号とGHについて知っていたため、GHに殺されかける。  
> →初登場[第4話](./ahstory1.md#4)  
>   

---

ver.010619

● [back](./ahname.md) ●