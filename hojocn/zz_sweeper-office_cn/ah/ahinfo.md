http://www.netlaputa.ne.jp/~arcadia/ah/ahinfo.html

AH最新信息

---

●Information  
  
# Section 1 (译注)  

## 2010年（译注）  

### ●2010年10月25日(月)  
Coamix出版了**『月刊 COMIC ZENON (ComicZenon)』**的新创刊号。  
官网… http://www.comic-zenon.jp/  
**『Angel Heart』2nd Season**在<font color=#ff0000>COMIC ZENON连载开始！！</font>  
  
定价为650日元（创刊号为800日元），可从德间书店购买。  
  
◆在25日的杂志创刊号上，该活动是一个大事件（10.23~10.25）  
→23日、24日，创刊0号和传单将在Café Zenon和Book Ruhe商店（吉祥寺）分发。    
・獠 & 香莹的真人大小的pop-ups也提前展示    
→在25日这一天…  
・在Café Zenon的所有食物和饮料都是半价!    
・在Café Zenon购买月刊Zenon，所有商品可享受70%的折扣（前30名顾客）  
・在Book Ruhe购买Zenon月刊时，可免费获得高级限量版嘎查（如AH限量版锡章） (译注：待校对)  
  
◆「Cat's Eye Special Cocktails」现在可以在Café Zenon买到(10.25～)  
→北条先生参加了品酒会! 三款鸡尾酒的灵感来自于愛、瞳、泪的形象（爱的版本不含酒精）  
・数量有限，你可以得到一个Cat's Card杯垫（背面可以作为通告信）。  
・在柜台点餐时，食品和饮料可享受20%的折扣。  
・Cat's Card形状的Café Zenon邮票卡现已上市。  
  
◆Free paper，免费发放Zenon创刊０号(10.10～)  
→对AH 1st Season的摘要回顾。 还刊登了创刊2号的附录。（译注：待校对）  
◎分发地址  
・清洲城的歌舞伎节（在清洲城活动中限量发行5000份。10.10）  
・2010年名古屋节（名古屋市长、原哲夫和堀江信彦的三人谈。10.16）  
・原哲夫「戦国傾奇個展」（名古屋荣三越。 10.13～10.18）  
・吉祥寺Café Zenon(10.10～)  
・在Zenon官方Twitter上申请的杂志monitor(10.12～10.14)  
・涩谷Q-front 7F「TSUTAYA 涩谷店」(同时展出AH复制原画。10.13～11.05)  
・渋谷Ｑ-front 6F「WIRED CAFE」(10.13～)  
・吉祥寺Books Ruhe店面(10.23～)  
・iPad、iPhone App（App Store免费下载。10.14～）  
  
※Comic Zenon也将作为一个基于商店的应用程序提供（但不包含增刊）  
  
◆「去战斗！ Pants」以外的创刊号附录还包括「漢(おとこ)の安心２大保証」（连续2期、3期）（译注：待校对）  
・獠和健次郎将保护我们所有的读者!  
→如果你购买了Zenon并在手机上免费注册，在发生紧急情况时，你将获得高达10万日元的慰问金。  
・獠解决了「跟踪狂受害」和「个人信息泄露」，「汉与女的纠纷保证」（译注：待校对）  
  
◆创刊2号的附录有「北条司mini画集」  
→另外还有北条司致敬画册第3弹  
  
▽参加人士  
村田雄介、久保ミツロウ、大島司、佐原ミズ、小野洋一郎、ヤマザキマリ（敬称略）  
  
◆Comic Zenon编辑部blog启动(10.01～)  
→AH 2nd Season 第1话卷中的一个彩页的掠影。  
AH是「北条司出道30周年的巅峰之作！」  
  
◆手机网站「Zenon Land」的初步开放  
→「AHきせかえ」作为创刊纪念礼物，在有限时间内赠送给所有注册用户(10.25～)  
  
◆Zenon Selection 至AH第4卷（计划22卷）  
・第4卷收录第49话～65话(10.22)  
→附有2nd Season的特制海报  
→ZENON创刊号连动！有AH 四卡的参赛表格（译注：待校对）    
  
◆「北条司 Ｘ いってん」致敬review展，决定举办!(渋谷LOFT。11.01～11.19）  
・北条司30周年纪念 & Comic Zenon创刊纪念的致敬review展!  
→Café Zenon和POPBOX（=由FEWMANY制作的creator bazaar）之间的联系项目。  
※「いってん」是FEWMANY的一个双月展。 
  
▽参加的艺术家  
彩、EVILROBOTS、emico、オオタニヨシミ、久保田ミホ、小石川ユキ、こなつ、sioux、白玉、  
末吉陽子、ソネアユミ、ソノベナミコ、タケヤマ・ノリヤ、チバサトコ、てりィ、東京Aリス、  
Tokyo Guns、NICO、feebee、mamico、ミキワカコ、ミヤモトヨシコ、Himiko Rose、mikish、  
宮川雄一、照紗、yucachin、Benicco、ヨシカワナオヒデ、ワカマツカオリ（敬称略）  
  
### ●2010年10月02日(土)  
**『Angel Heart』 活动舞台**、吉祥寺Theater举办！  
  
**「ANGEL HEART ～向着展翅的人～」**  
→仅限1天! 电影和舞蹈合作  
  
▽出演者  
杉本有美　坂崎愛　市道真央　鈴木つかさ　高柳将太　福島慎之介　高瀬秀芳　鍵山由佳　及川奈央（敬称略）  
  
北条司 作家生活30周年記念Project  
 在官方的网站上 UP  
http://www.hojo-tsukasa.com/index.php  
  
### ●2010年09月28日(火)～10月11日(月)  
**「北条司30周年 meets atre吉祥寺」**  
北条司30周年与atre吉祥寺开业的合作活动举办！  
  
▽**「Angel Heart」Dance Performance event **  
→为了纪念个展和舞台的举办，迎接Angel Heart dance team、杉本有美、及川奈央的示范表演。  
时间：9月28日(火)17:00～ 19:00～  
地点：atre吉祥寺、ゆらぎの広場  
  
▽**北条司「Angel Heart」個展**  
→展示所有AH Comics封面复制原画。  
期間：9月29日(水)～10月11日(月)まで  
場所：atre吉祥寺、ゆらぎの広場  
  
▽**「Stamp Rally 2010」「吉祥寺限定AH Gacha」**  
Stamp Rally→收集并应征，就会抽取老师亲笔签名的复制原画！   
GachaGacha → 吉祥寺限定的AH徽章33种。  
时间：9月29日(水)～10月11日(月)  
地点：atre吉祥寺ゆらぎの広場、CAFE ZENON、OLD CROW、FUNKY、LEMON DROP这4家商店。  
  
上述详情请参见… http://www.hojo-tsukasa.com/2010/09/kichijoji2010.html#more  
  
### ●**Zenon Selection「Angel Heart」第2巻**  
→9月24日(金)发售！定价480日元（含税）    
▽1st Season 重新收录的便利店版Comics  
→毎月第2、第4个周五发售  
  
### ●2010年09月11日(土)～11月07日(日)  
在北条先生的母校九州产业大学的美術館举办了10位校友漫画家的原画展!  
老师历代连载作品的原画多数参展！  
  
**50周年纪念事业特别展  毕业生--专业人士的世界─vol.2  
キュウサン☆Manga☆Chronicle**（译注：待校对）  
  
会期：2010年09月11日(土)～11月07日(日)  
時間：10:00～17:30(入场到17:15为止)  
→入场费：一般200日元、学生100日元  
※周一闭馆（11月1日开放）  
  
▽相关事宜   
Coamix社長 堀江信彦的讲座「与北条司的30年」  
举办时间：2010年09月25日(土)14：00～  
举办地点：15号馆2层15201号教室  
  
九州産業大学美術館 官方网站  
http://www.kyusan-u.ac.jp/ksumuseum/  
  
九州産業大学美術館Web日誌  
http://blog.livedoor.jp/ksumuseum/archives/cat_50046866.html  
  
### ●**Zenon Selection「Angel Heart」第1巻**  
→9月10日(金)发售！定价480日元（含税）  
▽1st Season 重新收录的便利店版漫画，现在由Coamix出版!    
→从现在起，每月第2、第4个周五发售。  
  
### ●**Bunch Comics「Angel Heart」第33巻**  
→9月9日(木)发售！定价540日元（含税）  
▽封面是Comic Bunch第20号封面的图案  
→1stSeason最終巻  
  
### ●2010年08月27日(金)  
**『週刊Comic Bunch』**从今天停止出版。 →该杂志将于2011年1月21日作为月刊Comic『＠Bunch』重新发行。  
**『Angel Heart』2nd Season**预定连载的**『月刊Comic Zenon』**于2010年10月25日发表新创刊。  
  
纸质杂志『Comic Zenon』、空间杂志**『Cafe Zennon』**(武藏野市吉祥寺)、电子杂志**『ZENONLAND』**(手机网站=预定)合作的三位一体的娱乐。  
主编是Coamix社长堀江信彦。杂志的theme写道「倾」  
  
Coamix2010年8月27日发布的News  
http://www.coamix.co.jp/2010/08/zenon0827.html  
  
### ●2010年08月20日(金)  
信息解禁Day。『週刊Comic Bunch』将于下周在杂志上通知停刊。  
网上透露，**『Angel Heart』**将转移到新杂志**『Comic Zenon』**，由Coamix推出。  
  
▽官方预告视频  
「Angel Heart移动到Comic Zenon（自画像ver）」  
http://www.youtube.com/watch?v=vZ_G9WUjTCk  
「Angel Heart移动到Comic Zenon（公告板ver）」  
http://www.youtube.com/watch?v=9EbF2yDQOMg  
  
参考：Comic Zenon官方视频集  
http://www.youtube.com/user/COMICZENON  
  
### ●2010年08月13日(金)・14日(土)  
**「吉祥寺Charity Auction」**举办！  
以「Art之街・吉祥寺」为关键词，与街道相关的艺术家合作举办了Charity Auction。  
  
▽北条司先生的展出作品  
・「Angel Heart Giclee（高精細复制原画）签名」  
・「France Japan Expro 2010官方小册子<非卖品>签名彩纸」  
  
举办时间：8月13日(金)・14日(土) 每天10:00～17:00  
举办地点：武蔵野商工会館4楼「Zero One Hall」（JR吉祥寺站中央出口，步行5分钟）
→免费入场，进出自由  
  
查询：产经Living新闻社 TEL0422(21)2531  
 详情在官方网站上  
http://www.lcomi.ne.jp/tie-up/t_10charity/  
  
### ●2010年08月06日(金)  
**『Angel Heart』1st Season**、『週刊Comic Bunch』的最終回！  
2nd Season信息在北条司官网和官方Twiter上。  
  
此外，Coamix新杂志**『Comic Zenon』**的官方网站预开放&官方Twitter启动！  
▽10月25日0時倒计时  
  
http://www.comic-zenon.jp/  
http://twitter.com/zenon_official/  
  
新潮社漫画事业部也正式启动Twitter！  
『Angel Heart』Comics新刊信息在这里。  
http://twitter.com/Comic_Shincho  
   
### ●2010年08月01日(日)  
**「北条司 OFFICIAL WEB SITE」**renewal！  
http://www.hojo-tsukasa.com/index.php  
  
### ●2010年07月01日(木)～04日(日)  
France**「Japan Expo 2010」**，北条司先生参加！  
→此事在@hojo_official的官方Twitter和官方移动网站「Mobile Bunch」上 
  
▽嘉宾包括动画CH的导演児玉兼嗣  
  
Japan Expo URL  
http://www.japan-expo.com/  
  
### ●2010年06月29日(火)  
**北条司's Official** Twitter Start @hojo_official  
http://twitter.com/hojo_official  
  
### ●2010年06月25日(金)  
Coamix株式会社其『週刊Comic Bunch』编辑业务与新潮社的合同期满，<font color=#ff0000>将在8月27日发售号上结束</font>。  

▽借此机会，Coamix将在今年秋天推出一本新的杂志，由北条司、原哲夫、次原隆二、一个新鲜而有前途的创作团队，以及株式会社North Stars Pictures。  
▽「Comic Bunch」也将继续由新潮社内部编辑，并将于今年晚些时候重新推出。  
  
向各位读者致意（译注：待校对）  
http://www.coamix.co.jp/2010/06/comicbunch.html  
  
### ●2010年05月14日(金)～<s>27日(木)</s>06月3日(木)  
**「北条司最新彩色原画＆tribute pinup展」**举办  
  
▽第1弾、第2弾Pin-up & Angel Heart最新插画展！！  
  
举办时间：5月14日(金)起<s>2周</s>（※21日(金)、23日(土)的17時后除外）→6月3日(木)为止（因好评而延长期限）  
举办地点：吉祥寺「CAFE ZENON」Loft space 
→免费入场  
  
详见Bunch或Café Zenon的官方网站  
http://www.comicbunch.com/news_release/hojo-exhibition2.php  
http://cafe-zenon.jp/news/?p=526  
  
### ●**Bunch Comics「Angel Heart」第32巻**  
→3月9日(火)发售！定价540日元（含税）  
▽封面是Comic Bunch'09年第39号封面的图案  
  
## 2009年（译注）  

### ●2009年12月24日(木)～12月<s>27日(日)</s>30日(水)  
**「北条司和朋友们Surprise原画展」**  
作为出道30周年联动企划紧急举办。  
举办时间：12月24日(木)17:00から、<s>直到27日（周日）25:00的4天</s>→至1月30日（周三）15:00为止（因好评而延长期限）  
举办地点：吉祥寺「CAFE ZENON」的2楼特别展览floor  
→免费入场  
  
▽除了北条刚的精华原画，还有一个特别展览，是由Comic Bunch 「北条司tribute pinup」企划的著名艺术家阵容新绘制的原画特别展览。 
→携带Comic Bunch 2010新年4/5合并版的前200名观众将获赠北条司的特别postcard present。  
  
详见Bunch或Café Zenon的官方网站  
http://www.comicbunch.com/news_release/hojo-exhibition.php  
http://cafe-zenon.jp/news/?p=240  

### ●**Bunch Comics「Angel Heart」第31巻**  
→11月9日(月)发售！定价540日元（含税）  
▽封面是Comic Bunch第14号封面的图案  
  
### ●**MINI freak No108**(2009年10月号・Natsume社)  
「我问了CityHunter的冴羽獠！」  
→8月21日(金)发售！定价1300日元（含税）  
  
▽特集「对你来说，Mini是什么？」中獠登場  
→附带北条先生的comment  
  
### ●**Bunch Comics「Angel Heart」第30巻**  
→7月9日(木)发售！定价540日元（含税）  
▽封面是Comic Bunch新年3号封面的图案  
→初次限定特典！带纪念明信片(共3种)  
  
### ●**Bunch Comics「Angel Heart」第29巻**  
→3月9日(月)发售！定价540日元（含税）  
▽封面是Comic Bunch'08年第43号封面的图案  
  
## 2008年（译注）  

### ●**Bunch Comics「Angel Heart」第28巻**  
→12月9日(火)发售！定价540日元（含税）  
▽封面是Comic Bunch第39号Color扉页的图案  
  
### ●**Bunch Comics「Angel Heart」第27巻**  
→9月9日(火)发售！定价540日元（含税）  
  
▽封面是Comic Bunch第32号 封面的图案  
  
### ●**Bunch Comics「Angel Heart」公式Guidebook**  
→5月16日(金)发售！定价700日元（含税）  
▽封面是Comic Bunch第23号 封面的图案  
  
・ Episode guide（ Episode 34話収録）  
・Character Guide（对所有124个人物的完整介绍）
・北条先生 long Interview  
・夢の鼎談、北条司×井上雄彦×梅澤春人  
・合作，北条司创作的漫画「A・H」× 大沢在昌写的小说「新宿鮫」  
etc...
  
### ●2008年05月16日(金)～05月18日(日)  
「Angel Heart」Comics累計1500万部突破記念Art Gallery、  
**『Memory of X・Y・Z』**于5/15～21之间、JR新宿站举行。  
  
举办时间：5月16日～18日、各日13:00～20:00  
举办地点：JR新宿站 Alps广场  
［电照板张贴期间:5月15日(木)～21日(水)］  
  
活动内容  
1.新宿站XYZ留言板的复活　→通过电照板恢复XYZ留言板。可以委托CH。  
2.Mini Cooper＆1500万t Hammer 巨大模型的展示→根据为活动新绘制的插图制作的巨大模型。  
3.「AH」巨型Comic Gallery→在一张电照板上展示AH的整个情节。  
4.「AH」精华的复制原画的展示→从AH的彩色和单色原稿中复制精华进行展示。（译注：待校对）  
5.特制原画post card present  →Bunch最新号的每日前100名。  
6.特制AH生态袋→向17日、18日到场的前1500名参观者分发。  
  
 详见… http://www.comicbunch.com/AH1500/ 
  
### ●2008年05月17日(土)  
**『北条司先生sign会』**決定！  
举办日期和时间：2008年5月17日(土) 13:00～14:00  
举办場所：紀伊國屋新宿本店Forest 2层特設会場  
▽5/9(周五)上午10点起，在新宿本店Forest 2楼漫画柜台，向购买AH26卷的前150名发放编号的门票。  
→仅在同一本书中签名。 
  
### ●2008年05月14日(水)～07月31日(木)17点前接受预订
AH**『Giclee印刷品系列』**限量销售。  
▽所有6个标题都有北条老师的签名和序号。 每本100份。 装框的窗口尺寸为535mm x 420mm。  
→期間限定、1点48000日元(含税)  
要求提供目录… http://www.shinchosha.co.jp/angel  
Bunch Mobile Shop… http://bunch-shop.jp/  
  
### ●**Bunch Comics「Angel Heart」第26巻**  
→5月9日(金)发售！定价530日元（含税）  
▽封面是Comic Bunch2007年第24号 封面的图案  
  
▽该杂志・Comics联动「AH fan 感恩节」第2弾！  
「AHOriginal Pins ＆专用frame」申请者全員Service  
「Comics26巻オビの申请券」＋「Comic Bunch23号(5/9发售)、24号(5/16发售)、25号(5/23发售)いずれかの申请用纸」＋「2000日元分の郵便定額小為替」を一組にして申请。  
→截止日期、2008年06月09日(月)<font color=#ff0000>必须送达</font>。详情见腰封。  

### ●2008年05月09日(金)～06月09日(月)上午10点为止 
手机Bunch申请者全員免费present！  
▽注册成为会员并使用手机 Bunch Top page的表格申请。  
→你将收到一个特制的AH手机防偷窥贴纸。  
  
### ●**Bunch Comics「Angel Heart」第25巻**  
→2月9日(土)发售！定价530日元（含税）  
▽封面是Comic Bunch2007年第50号 封面的图案  
  
▽该杂志・Comics联动「AH fan 感恩节」開始  
第一弾、「AH特制镊子5件套装」申请者全員service  
「Comic Bunch10号(2/8发售)、11号(2/15发售)、12号(2/22发售)任何一个申请用纸」＋「Comics25巻腰封的申请券」＋「1000日元的邮政定额小汇」为一组申请。  
→截止日期、2008年03月10日(月)<font color=#ff0000>必须送达</font>。详情见腰封。
  
## 2007年（译注）  

### ●**Bunch Comics「Angel Heart」第24巻**  
→11月9日(金)发售！定价530日元（含税）  
▽封面是Comic Bunch第30号 巻頭Color扉页的图案  
  
▽赠送一张印有北条老师笔迹的个性化纸张present，用于回答调查问卷/可通过电脑或手机进行输入。  
→截止日期、2007年12月10日(木)午夜0時/详见Comics腰封&第202页。  
    
### ●**Bunch Comics「Angel Heart」第23巻**  
→8月9日(木)发售！定价530日元（含税）  
▽封面是Comic Bunch新年1・2合并特刊 巻頭Color扉页的图案  
  
### ●**Bunch Comics「Angel Heart」第22巻**  
→5月9日(水)发售！定价530日元（含税）  
▽封面是Comic Bunch新年5・6合并特刊 封面的图案  
  
▽该杂志・Comics联动企划「AH DVD Premium Box Vol.4 present」／抽取15名  
用Comic Bunch第24期（5/11发售）&第25期（5/18发售）的申请券中的任意2张，和Comic第22卷腰封的1张申请券，共计2张作为一组进行申请。  
→截止日期、2007年06月04日(月)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
###「Angel Heart」×「Yahoo! JAPAN」联动协作企划  
→Yahoo! 动画中、AH单独的特集page**『Angel Heart  Special 』**OPEN！  
… http://streaming.yahoo.co.jp/special/anime/angelheart/  
→动画AH在互联网上首次出现（免费观看第1話）  
→OP曲、ED曲的免费分发  
  
▽Charity Auction超目玉大型企划「原寸大100t Hammer 」：从3月9日(周五)开始  
▽Yahoo!動画限定！「MotionComics」にAH第1話が登場  
▽北条老师也参加「AH Baton」  
▽登场人物提问「AH智囊」  
▽从原作、Anime中摘取「Original壁紙Download」  
其他、「AH测试」、手机免费待办活动etc..  
  
### ●**Bunch Comics「Angel Heart」第21巻**  
→1月9日(火)发售！定价・通常版530日元、包含特制腰封的初版限量版780元（含税）  
▽封面是Comic Bunch'06年第47号封面的图案  
→两个最重要的腰封是「しら乌鸦」&「100t Hammer」（译注：待校对）  
  
▽该杂志・Comics联动企划「AH DVD Premium Box Vol.3 present」／抽取15名  
Comic Bunch5・6合并号(12/28发售)＆7号(1/12发售)＆8号(1/19发售)＆9号(1/26发售)的申请券任选2张、Comics 21巻腰封的申请券1枚、共3张成一组申请。  
→截止日期、2007年02月05日(金)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
## 2006年（译注）  

### ●2006年09月25日(月)  
动画AH、迎接最終回节目的Present  
・DVD Premium Box Vol.2 (5名)  
・Original Soundtrack CD (20名)  
  
▽通过手机·PC申请… Access http://angelheart.tv   
→报名截止日期：2006年10月31日(火)  
  
▽通过明信片申请… 〒530-8056 大阪中央郵便局私書箱 1212号  
DVD希望 → 读卖TV「 Angel DVD present」栏目  
CD希望 → 读卖TV「 Angel CD present」栏目  
请将您的姓名、地址、年龄和对节目的反馈发送到相应的地址  
→申请截止日期：2006年10月31日(火)当日邮戳有効  
    
### ●动画AH官方手机网站上的「连续3周的问答活动」  
→2006年09月13日(水)～10月31日(火)  
▽测验活动，赢得免费的待机图像和其他节目的礼物  
→回答正确，可获得免费的待机图像present  
→仅限会员，从连续3周正确回答所有问题的人中抽出，由节目组赠送的特別present  
  
▽动画AH官方手机网站・Access方法  
(1)从menu上看  
**NTT DoCoMo**  
i-MENU→menu list→待机画面/i-应用待机/frame→animation/manga/→Angel Heart  
**au**  
top menu→ カテゴリで探す→画像･Character→Anime･Comic→Angel Heart  
**SoftBank Mobile**(vodafone)  
menu list→壁紙・待受→Anime・manga→Angel Heart  
(2)从URL  
http://angelheart.tv  
  
### ●**Bunch Comics「Angel Heart」第20巻**  
→9月9日(土)发售！定价530日元（含税）  
▽封面是Comic Bunch第34号 封面的图案  
  
### ●**Bunch Comics「Angel Heart」第19巻**  
→6月9日(金)发售！定价530日元（含税）  
▽封面是Comic Bunch第22・23合并特刊 封面的图案  
  
▽该杂志・Comics联动企划「AH DVD Premium Box Vol.2他 present」／抽取150名  
（DVD15名・AH T-shirt9名・Original Key 环126名）  
Comics腰封的申请券、Comic Bunch28号(6/9发售)＆29号(6/16发售)の申请券、共3张成一组申请。  
→截止日期、2006年06月30日(金)<font color=#ff0000>必须送达</font>。详情见腰封。   
  
### ●**Bunch Comics「Angel Heart」第18巻**  
→3月9日(木)发售！定价530日元（含税）  
▽封面是Comic Bunch第8号 巻頭Color扉页的图案  
  
▽该杂志・Comics联动企划「AH DVD Premium Box Vol.1 present」／抽取15名  
Comics带的报名券和Comic Bunch15号(3/10发售)＆16号(3/17发售)の申请券，共3张成一组申请。  
→截止日期、2006年03月31日(金)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
### ●动画AH手机网站开放   
2006年<s>03月06日(月)</s>→推迟到4月以后  
**『Angel Heart』（月額315日元・含税）**  
→NTT DoCoMo的i-Mode内容(※au、Vodafone将于4月以后依次开设)    
▽Access方法  
(1) i-MENU→menu list→等待画面/i应用程序等待/frames→animation/manga/→Angel Heart  
(2)http://angelheart.tv   
  
## 2005年（译注）  

### ●**Bunch Comics「Angel Heart」第17巻**  
→12月9日(金)发售！定价530日元（含税）  
▽封面是Comic Bunch第21号 封面的图案  
  
### ●动画**『Angel Heart 』**放送開始  
读卖TV→10月03日(月)起、每周一：24時(深夜0時)58分～  
日本TV→10月04日(火)起、每周二：25時(深夜1時)25分～  
  
\#1「ガラスの心臓 グラス・ハート」读卖TV10/03(月)25:34～／日本TV10/04(火)26:25～  
\#2「香が帰ってきた」读卖TV10/10(月)25:28～／日本TV10/11(火)25:49～  
\#3「XYZの街」读卖TV10/17(月)25:13～／日本TV10/18(火)25:34～  
\#4「さまようHEART」读卖TV10/24(月) 25:14～／日本TV10/25(火) 25:25～  
\#5「永別(さよなら) …カオリ」读卖TV10/31(月) 24:59～／日本TV11/01(火) 26:25～  
\#6「再会」读卖TV11/07(月) 24:59～／日本TV11/15(火)  
\#7「俺の愛すべき街」读卖TV11/14(月) 25:28～／日本TV11/22(火) 25:55～  
\#8「真実(ホント)の仲間」读卖TV11/21(月) 25:19～／日本TV11/29(火) 25:35～  
\#9「香瑩～失われた名前～」读卖TV11/28(月) 25:09～／日本TV12/06(火) 25:35～  
\#10「Angel Smile」读卖TV12/05(月) 25:09～／日本TV12/13(火) 25:55～  
\#11「父娘(おやこ)の時間」读卖TV12/12(月) 25:59～／日本TV12/20(火) 25:25～  
\#12「船上の出会いと別れ」读卖TV12/19(月) 24:59～／日本TV01/03(火) 26:04～  
\#13「李大人からの贈り物」读卖TV01/09(月) 25:50～／日本TV01/10(火) 25:55～  
\#14「復活Ｃ・Ｈ！」读卖TV01/16(月) 24:59～／日本TV01/17(火) 25:25～  
\#15「パパを捜して」读卖TV01/23(月) 24:59～／日本TV01/24(火) 25:25～  
\#16「Ｃ・Ｈとしての資格」读卖TV01/30(月) 24:59～／日本TV01/31(火) 25:40～  
\#17「夢の中の出会い」读卖TV02/06(月) 27:03～／日本TV02/07(火) 26:00～  
\#18「親子の絆」读卖TV02/13(月) 25:03～／日本TV02/14(火) 25:25～  
\#19「陳老人の店」读卖TV02/20(月) 25:03～／日本TV02/21(火) 25:25～  
\#20「宿命のプレリュード」读卖TV02/27(月) 25:03～／日本TV02/28(火) 25:55～  
\#21「哀しき守護者(ガーディアン)」读卖TV03/06(月) 25:03～／日本TV03/07(火) 25:25～  
\#22「不公平な幸せ」读卖TV03/13(月) 25:03～／日本TV03/14(火) 25:25～  
\#23「出発(たびだち)のメロディー」读卖TV03/20(月) 25:29～／日本TV03/21(火) 25:25～  
\#24「鼓動と共に…」读卖TV03/27(月) 25:03～／日本TV03/28(火) 26:25～  
\#25「死にたがる依頼者」读卖TV04/03(月) 25:30～／日本TV04/04(火) 25:49～  
\#26「もう一度あの頃に」读卖TV04/10(月) 25:30～／日本TV04/11(火) 25:29～  
\#27「私、恋してる！？」读卖TV04/17(月) 25:25～／日本TV04/18(火) 25:29～  
\#28「約束」读卖TV04/24(月) 24:55～／日本TV04/25(火) 25:29～  
\#29「私の妹…香」读卖TV05/01(月) 24:55～／日本TV05/02(火) 25:29～  
\#30「この街は私の全て」读卖TV05/08(月) 24:55～／日本TV05/09(火) 25:59～  
\#31「最後の夜に見た奇跡」读卖TV05/15(月) 24:55～／日本TV05/16(火) 25:29～  
\#32「組織から来た女」读卖TV05/22(月) 24:55～／日本TV05/23(火) 25:29～  
\#33「神から授かりし子」读卖TV05/29(月) 24:55～／日本TV05/30(火) 25:29～  
\#34「二人の決意」读卖TV06/05(月) 24:55～／日本TV06/06(火) 25:29～  
\#35「未来へ…」读卖TV06/12(月) 24:55～／日本TV06/13(火) 25:29～  
\#36「幸せを運ぶ女の子」读卖TV06/19(月) 25:16～／日本TV06/20(火) 25:29～  
\#37「汚れのない心」读卖TV06/26(月) 24:55～／日本TV06/27(火) 25:29～  
\#38「オレの目になってくれ」读卖TV07/03(月) 25:25～／日本TV07/04(火) 25:29～  
\#39「依頼者は大女優」读卖TV07/10(月) 25:25～／日本TV07/11(火) 25:29～  
\#40「ミキの隠された秘密」读卖TV07/17(月) 25:04～／日本TV07/18(火) 25:38～  
\#41「自分の居場所」读卖TV07/24(月) 25:09～／日本TV07/25(火) 25:38～  
\#42「二人だけのSign 」读卖TV07/31(月) 25:00～／日本TV08/01(火) 25:29～  
\#43「私が生きる日常」读卖TV08/07(月) 25:14～／日本TV08/08(火) 25:29～  
\#44「俺たちの子供のために」读卖TV08/14(月) 24:55～／日本TV08/15(火) 25:29～  
\#45「人間核弾頭、楊(ヤン)」读卖TV08/21(月) 25:10～／日本TV08/22(火) 26:14～  
\#46「マザー・ハート」读卖TV08/28(月) 25:25～／日本TV08/29(火) 25:29～  
\#47「アカルイミライ！？」读卖TV09/04(月) 24:55～／日本TV09/05(火) 26:59～  
\#48「引き寄せられる運命」读卖TV09/11(月) 24:55～／日本TV09/12(火) 25:29～  
\#49「Get My Life」读卖TV09/18(月) 25:25～／日本TV09/19(火) 25:29～  
\#50「Last Present」(最終話) 读卖TV09/25(月) 25:25～／日本TV09/26(火) 25:59～  
  
★Opening theme「**Finally**」(第2～24話)  
歌：Sowelu  
作詞：Shoko Fujibayashi  
作曲：Kosuke Morimoto  
編曲：Takuya Harada  
(DefSTAR RECORDS)  
→2006年2/1(水)发售  
  
★Opening theme「**Lion**」(第25～38話)  
歌：玉置浩二  
作詞：松井五郎  
作曲：玉置浩二  
(Sony Music Records)  
→2006年3/15(水)发售  
  
★Opening theme「**Battlefield of Love**」(第39～50話)  
歌：伊沢麻未  
作詞・作曲：伊沢麻未  
編曲：DJ CLAZZIQUAI  
(SME Records)  
→2006年8/30(水)发售 ：初回特典・ワイドラベル仕様（Angel Heart图案）  
  
★Ending theme「**誰かが君を想ってる**」(第2～12・14～19・24・50話)  
歌：Skoop On Somebody  
作詞：SOS  
作曲：SOS＋土肥真生  
編曲：土肥真生＋SOS  
(SME Records)  
→2005年11/9(水)发售  
  
★Special・Ending theme「**Daydream Tripper**」(第13・20～23話)  
歌：U_WAVE  
作詞：森 雪之丞  
作曲：石井妥師  
編曲：土橋安騎夫＆石井妥師  
(M-TRES)  
→2006年4/26(水)发售  
  
★Ending theme「**My Destiny**」(第25～41話)  
歌：Cannon  
作詞・作曲・編曲：Cannon  
(Sony Music Japan International)  
→2006年5/24(水)发售：最初特典规范・wide cap sticker  
  
★Ending theme「**哀しみのAngel**」(第42～46話）  
(→第24・28話の挿入歌）  
歌：稲垣潤一  
作詞：Satomi  
作曲：羽場仁志  
編曲：水島康貴  
(Aniplex)  
  
★Ending theme「**FEEL ME**」(第47～49話)  
歌：中西圭三  
作詞・作曲：中西圭三  
編曲：上野圭市  
(Aniplex)  
  
★挿入歌「**Gloria**」(第1・2・16・50話)  
歌：Cannon  
作詞・作曲：Cannon  
編曲・Produce：大谷 幸  
(Sony Music Japan International)  
  
★挿入歌「**Guardian Star**」(第7話)  
歌：尾崎亜美  
作詞・作曲・編曲：尾崎亜美  
(Columbia Music Entertainment)  
  
★挿入歌「**More Than You Know**」(第7・10・12・15話)  
歌：ＴＯＫＵ  
作詞：MAYUMI  
作曲：松本俊明  
編曲：ＴＯＫＵ  
(Sony Music Japan International)  
  
★挿入歌「**Wings of Love**」(第10・12・41話)  
歌：TOKU  
作詞：MAYUMI  
作曲：松本俊明  
編曲：TOKU／Febian Reza Pane  
(Sony Music Japan International)  
  
★挿入曲「**愛の夢　第３番**」(第11話)  
演奏：Balazs Szokolay  
作曲：Franz Liszt  
  
★挿入歌「**そばに**」(第12・36・41話)  
歌：牧伊織  
作詞：笛田彩葉  
作曲：西岡和哉  
編曲：鈴木正人  
(The Music Council)  
  
★挿入歌「**Rebirth**」(第12話)  
歌：kitago-yama  
作詞：安岡 優  
作曲：北山陽一　多胡 淳  
編曲：多胡 淳　北山陽一  
(Ki/oon Records)  
  
★挿入歌「**A Brand New Day**」(第19・42話)  
歌：Christy & Clinton  
作詞：Christy & Clinton  
作曲：谷脇仁美  
編曲：安部 潤  
(Aniplex)  
  
★挿入歌「**哀しみのAngel**」(第24・28話）  
歌：稲垣潤一  
作詞：Satomi  
作曲：羽場仁志  
編曲：水島康貴  
(Aniplex)  
  
★挿入歌「**Angel tears**」(第24・27・38話）  
歌：籐子  
作詞：籐子  
作曲：Tohko Project  
編曲：Tohko Project  
(Aniplex)  
  
★挿入歌「**Back To The City**」(第32話）  
歌：Cro-magnon feat. Kyoko  
作詞：Kyoko  
作曲：小菅 剛　大竹重寿　金子 巧  
編曲：Cro-magnon  
(Aniplex)  
  
★挿入歌「**Serenade**」(第33話）  
歌：Cannon  
作詞・作曲・編曲：Cannon  
(Sony Music Japan International)  
  
★挿入歌「**海の色**」「**sound collage #1**」(第35話）  
歌：岩男潤子  
作詞：岩男潤子  
作曲・編曲：山本はるきち  
  
★挿入歌「**parallel －パラレル－**」(第42話）  
歌：shungo.  
作詞：shungo.  
作曲：shungo. & Serio  
編曲：shungo. & Serio  
(Aniplex)  
  
★挿入歌「**Fri Day!**」(第45話）  
歌：LiL'G  
作詞・作曲：LiL'G  
(Aniplex)  
  
→全国也陆续决定放映  
・札幌TV放送：10/17(月) 25:25～  
・山形放送：10/07(金) 25:25～  
・中京TV放送：10/12(水) 25:59～  
・福岡放送：10/16(日) 25:25～  
・西日本放送：10/17(月) 25:50～  
・鹿児島読売TV：10/21(金) 25:25～  
・静岡第一TV：2006/01/11(水) 26:10分～<font color=#ff0000>*</font>来自chikka的信息。  
  
### ●**Bunch Comics「Angel Heart」第16巻**  
→9月9日(金)发售！定价530日元（含税）  
  
▽封面是Comic Bunch新年4・5合并特刊 封面的图案  
  
### ●**Bunch Comics「Angel Heart」第15巻**  
→6月9日(木)发售！定价530日元（含税）  
  
▽封面是Comic Bunch第28号封面的图案  
  
▽漫画累计突破1000万册纪念「Angel BOX」present  
Comics第1～15卷的封面画复制品/18张一套  
→B4尺寸（高364x 宽257mm）/装在带有序号的豪华礼盒中  
※腰封上的入场券和Comic Bunch第28期（6月10日发售）上的入场券都是必需的/1000名获奖者将被抽出 
→截止日期为6月30日（周四），以当天邮戳为准，详见腰封    
  
### ●**Bunch Comics「Angel Heart」第14巻**  
→2月9日(水)发售！定价530日元（含税）  
  
▽封面是Comic Bunch'03年第52号封面的图案  
  
## 2004年（译注）  

### ●**Bunch Comics「Angel Heart」第13巻**  
→11月9日(火)发售！定价530日元（含税）  
  
▽封面是Comic Bunch第40号封面的图案  
  
### ●**Bunch Comics「Angel Heart」第12巻**  
→9月9日(木)发售！定价530日元（含税）  
  
▽封面是Comic Bunch第27号扉页画的图案  
  
### ●**Bunch Comics「Angel Heart」第11巻**  
→6月9日(水)发售！定价530日元（含税）  
  
▽封面是Comic Bunch第27号封面的图案  
  
### ●**Bunch Comics「Angel Heart」第10巻**  
→3月9日(火)发售！定价505日元+税  
  
▽封面是Comic Bunch'03年第52号扉页画  
  
### ●**Da Vinci 2月号**  
→1月6日(火)发售／定价450日元  
▽Comics Da Vinci 「北条 司 その尽きない魅力」（译注：「北条司 无尽的魅力」）  
  
### ●**日経Entertainment！2004年2月号**  
→1月5日(月)发售／定价500日元  
▽【対談】北条司×飯島愛  
「Cat's Eye」「City Hunter」  
「Angel Heart」誕生秘話  
  
### 2003年（译注）  

### ●**Bunch Comics「Angel Heart」第9巻**  
→12月9日(火)发售！定价505日元+税  
  
▽封面是Comic Bunch第31号扉页画  
  
### ●**Bunch Comics「Angel Heart」第8巻**  
→9月9日(火)发售！定价505日元+税  
  
▽封面是Comic Bunch第19号封面  
→超豪华! 有装裱的彩色纸质present  
北条先生签名的彩色纸（有获奖者的名字）25名/失望奖明信片套装500名   
截止日期为10月8日（周三）；详情见単行本帯  
  
### ●**Bunch Comics「Angel Heart」第7巻**  
→6月9日(月)发售！定价505日元+税  
  
▽封面是Comic Bunch第10号封面  
→累計500万部突破記念present  
A奖「作者亲笔签名的报刊」50人/B奖「Clear File Set」1000人（译注：待校对）  
截止日期7月8日(星期二)·详情请参阅单行本带（译注：待校对） 
     
### ●Bunch公式i-mode Site 「ｉBunch」  
→4月7日(月)OPEN  
▽AH的shooting game「Mokkori Hunter 」和待机画面  
Access方法：iMenu→menu list→TV/广播/杂志→(5)杂志→ｉBunch  
月額：300日元  
  
### ●**Bunch Comics「Angel Heart」第6巻**  
→3月8日(土)发售！定价505日元+税  
  
▽封面是Comic Bunch2002年第44号封面  
  
## 2002年（译注） 

### ●**Bunch Comics「Angel Heart」第5巻**  
→11月9日(土)发售！定价505日元+税  
  
▽封面是Comic Bunch第31号封面  
  
### ●09月/上旬～11月/最后一天为止  
**新宿「MY CITY」6楼的山下書店设立了[「XYZ 伝言板BOX」](./xyzbox.md)**  
▽征集想让CH解决的独特委托、有趣委托  
▽优秀的委托被作品采用，还有亲笔签名的单行本Present  
▽用配备的特制Postcard投递  
→ Card系Bunch第22・23合并特刊封面的图案  
  
### ●07/16(火)～08/31(土)  
**吉祥寺[BOOKS  Ruhe](http://www.books-ruhe.co.jp/)原哲夫／北条司原画展**  
▽在楼梯平台的空间里  
▽通过抽奖向CH和AH等漫画购买者赠送签名彩纸和Original Goods的Present  
  
### ●**Bunch Comics「Angel Heart」第4巻**  
→7月9日(火)发售！定价505日元+税  
  
▽封面是Comic Bunch第18号封面  
  
### ●**Bunch Comics「Angel Heart」第3巻**  
→3月8日(金)发售！定价505日元+税  
  
▽封面是Comic Bunch新年7・8合并特刊AH扉页画  
  
## 2001年（译注）

### ●**Bunch Comics「Angel Heart」第2巻**  
→12月8日(土)发售！定价505日元+税  
  
▽封面是Comic Bunch第20号封面  
  
### ●**Bunch Comics「Angel Heart」第1巻**  
→10月9日(火)发售！定价505日元+税  
  
▽封面是新画的（GH＆獠的侧脸）→[Coamix HP](http://www.coamix.co.jp/)or[北条司公式Homepage](http://www.hojo-tsukasa.com/)GO！  
▽Comics初版的腰封、以及Comic Bunch No.22&No.23/24合并号上的申请券，收集其中3张、<font color=#ff0000>抽选</font>1000名AH**听漫的Drama CD**！！  
→申请10月31日(火)<font color=#ff0000>必须送达</font>  
  
※AH等、Bunch Comics介绍**免费小冊子**（内封是AH的Comic封面）也在书店等地分发！<font color=#0000ff>\*</font>    
(\* 从タカ收到的信息)  
  

# Section 2 (译注)  

## 2010年（译注）  
### ●2010/08/06(金)
■ComicsBunch第36・37合并号(8月20日・27日号)发售  
　「**Angel Heart**」第363話  
　【恋の行方】  
　▽1stSeason／完  
　→2ndSeason情報在北条司官方Site及Twitter上  
>   
> 
>   
>   
> 
### ●2010/07/30(金)
■Comic Bunch第35号(8月13日号)发售  
　「**Angel Heart**」系暂停。  
  ▽下周系AH 1st.Season最終話  
>   
> 
>   
>   
> 
### ●2010/07/23(金)
■Comic Bunch第34号(8月6日号)发售  
　「**Angel Heart**」系隔周连载。  
　→这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/07/16(金)
■Comic Bunch第33号(7月30日号)发售  
　「**Angel Heart**」第362話  
　【乱暴な天使】  
>   
> 
>   
>   
> 
### ●2010/07/09(金)
■Comic Bunch第32号(7月23日号)发售  
　「**Angel Heart**」系暂停。 
>   
> 
>   
>   
> 
### ●2010/07/02(金)
■　Comic Bunch第31号(7月16日号)发售  
　「**Angel Heart**」系暂停。  
>   
> 
>   
>   
> 
### ●2010/06/25(金)
■Comic Bunch第30号(7月9日号)发售  
　「**Angel Heart**」第361話/封面  
　【二人分のギフト】  
>   
> 
>   
>   
> 
### ●2010/06/18(金)
■Comic Bunch第29号(7月2日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/06/11(金)
■Comic Bunch第28号(6月25日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/06/04(金)
■Comic Bunch第27号(6月18日号)发售  
　「**Angel Heart**」第360話  
　【父と息子】  
>   
> 
>   
>   
> 
### ●2010/05/28(金)
■Comic Bunch第26号(6月11日号)发售  
　「**Angel Heart**」系暂停。  
>   
> 
>   
>   
> 
### ●2010/05/21(金)
■Comic Bunch第25号(6月4日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/05/14(金)
■Comic Bunch第24号(5月28日号)发售  
　「**Angel Heart**」第359話/封面  
　【愛の選択】  
　▽Bunch9周年・北条司30周年記念号  
　・北条司Tribute Pinup第2弾  
　▽北条司最新Color原画＆Tribute Pinup展举办  
　→「CAFE ZENON」从5/14～27为止  
>   
> 
>   
>   
> 
### ●2010/05/07(金)
> 
> ■Comic Bunch第23号(5月21日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
> ▽下期（14日发售）系、北条司Debut30周年・Bunch創刊9周年記念号  
> →豪華作家阵容集結 北条司Tribute Pinup第2弾  
> （小畑友紀・芹沢直樹・高橋留美子・次原隆二・冨樫義博・渡辺航）  
>   
> 
>   
>   
> 
### ●2010/04/23(金)
> 
> ■ComicsBunch第21・22合并号(5月7日・14日号)发售  
> 「Angel Heart」第358話  
> 【ギフト】  
>   
> 
>   
>   
> 
### ●2010/04/16(金)
> 
> ■ComicsBunch第20号(4月30日号)发售  
> 「Angel Heart」第357話/封面  
> 【陳老人暗躍】  
>   
> 
>   
>   
> 
### ●2010/04/09(金)
> 
> ■ComicsBunch第19号(4月23日号)发售  
> 「**Angel Heart**」系隔号连载。  
>　→这周号上没有刊登 
>   
> 
>   
>   
> 
### ●2010/04/02(金)
> 
> ■ComicsBunch第18号(4月16日号)发售  
> 「Angel Heart」第356話  
> 【生きる目的】  
>   
> 
>   
>   
> 
### ●2010/03/26(金)
> 
> ■ComicsBunch第17号(4月9日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/03/19(金)
> 
> ■ComicsBunch第16号(4月2日号)发售  
> 「Angel Heart」第355話  
> 【受け止める覚悟】  
>   
> 
>   
>   
> 
### ●2010/03/12(金)
> 
> ■ComicsBunch第15号(3月26日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/03/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第32巻 发售  
> ▽封面是Comic Bunch'09年第39号封面的图案  
>   
> 
>   
>   
> 
### ●2010/03/05(金)
> 
> ■ComicsBunch第14号(3月19日号)发售  
> 「Angel Heart」第354話  
> 【今日限りの自由】  
>   
> 
>   
>   
> 
### ●2010/02/26(金)
> 
> ■ComicsBunch第13号(3月12日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/02/19(金)
> 
> ■ComicsBunch第12号(3月5日号)发售  
> 「Angel Heart」第353話/封面  
> 【恋の吊り橋】  
>   
> 
>   
>   
> 
### ●2010/02/12(金)
> 
> ■ComicsBunch第11号(2月26日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/02/05(金)
> 
> ■ComicsBunch第10号(2月19日号)发售  
> 「Angel Heart」第352話  
> 【笑顔の理由】  
>   
> 
>   
>   
> 
### ●2010/01/29(金)
> 
> ■ComicsBunch第9号(2月12日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/01/22(金)
> 
> ■ComicsBunch第8号(2月5日号)发售  
> 「Angel Heart」第351話  
> 【薔薇と御曹司】  
>   
> 
>   
>   
> 
### ●2010/01/15(金)
> 
> ■ComicsBunch新年7号(1月29日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2010/01/08(金)
> 
> ■ComicsBunch新年6号(1月22日号)发售  
> 「Angel Heart」第350話  
> 【陳さんの策謀】  
> ▽2010新春全作家阵容亲笔插画Present  
>   
> 
>   
>   

## 2009年（译注）  
### ●2009/12/24(木)
> 
> ■ComicsBunch新年4・5合并号(1月15日・20日号)发售  
> 「Angel Heart」第349話/封面  
> 【希望の風】  
> ▽北条司Debut30周年企划・第1弾  
> ・北条作品Tribute Pinup  
> →由豪华漫画家阵容绘制的新插画(袋とじ附录)（译注：待校对）  
> ・北条司mini原画展举办決定  
>   
> 
>   
>   
> 
### ●2009/12/18(金)
> 
> ■ComicsBunch新年3号(1月8日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
> ▽下期（24日(木)发售）北条司30周年特別企划  
> →由知名艺术家创作的北条世界人物的精湛插图集一举刊登  
> （朝基まさし・井上雄彦・梅澤春人・新條まゆ・のりつけ雅春・原哲夫）  
>   
> 
>   
>   
> 
### ●2009/12/04(金)
> 
> ■ComicsBunch新年1・2合并特刊(1月1日・5日号) 发售  
> 「Angel Heart」第348話  
> 【たくさんの月】  
>   
> 
>   
>   
> 
### ●2009/11/27(金)
> 
> ■ComicsBunch第52号(12月11日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2009/11/20(金)
> 
> ■ComicsBunch第51号(12月4日号)发售  
> 「Angel Heart」第347話  
> 【優しい嘘】  
>   
> 
>   
>   
> 
### ●2009/11/13(金)
> 
> ■ComicsBunch第50号(11月27日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2009/11/09(月)
> 
> ■Bunch Comics  
> 「Angel Heart」第31巻 发售  
> ▽封面是Comic Bunch第14号封面的图案  
>   
> 
>   
>   
> 
### ●2009/11/06(金)
> 
> ■ComicsBunch第49号(11月20日号)发售  
> 「Angel Heart」第346話/封面  
> 【嘘だと言って…】  
>   
> 
>   
>   
> 
### ●2009/10/30(金)
> 
> ■ComicsBunch第48号(11月13日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2009/10/23(金)
> 
> ■ComicsBunch第47号(11月6日号)发售  
> 「Angel Heart」第345話  
> 【忘却の代償】  
>   
> 
>   
>   
> 
### ●2009/10/16(金)
> 
> ■ComicsBunch第46号(10月30日号)发售  
> 「Angel Heart」系隔号连载。  
> →这周号上没有刊登  
>   
> 
>   
>   
> 
### ●2009/10/09(金)
> 
> ■ComicsBunch第45号(10月23日号)发售  
> 「Angel Heart」第344話/封面  
> 【ママを捜して…】  
>   
> 
>   
>   
> 
### ●2009/10/02(金)
> 
> ■ComicsBunch第44号(10月16日号)发售  
> 「描線の流儀」北条司 Interview 後編 （译注：「线条的风格」北条司Interview 后篇）  
> ▽从本期起「Angel Heart」每隔一期连载  
> 本周没有连载「Angel Heart」。  
>   
> 
>   
>   
> 
### ●2009/09/18(金)
> 
> ■ComicsBunch第42・43合并特刊(10月2日・9日号)发售  
> 「Angel Heart」第343話  
> 【会いたい】  
> ▽「描線の流儀」北条司 Interview 前編（译注：「线条的风格」北条司Interview 前篇）   
>   
> 
>   
>   
> 
### ●2009/09/11(金)
> 
> ■ComicsBunch第41号(9月25日号)发售  
> 「Angel Heart」第342話  
> 【偽りの再会】  
>   
> 
>   
>   
> 
### ●2009/09/04(金)
> 
> ■ComicsBunch第40号(9月18日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2009/08/28(金)
> 
> ■ComicsBunch第39号(9月11日号)发售  
> 「Angel Heart」第341話/封面  
> 【涙の理由】  
>   
> 
>   
>   
> 
### ●2009/08/21(金)
> 
> ■mini・Freak No108(2009年10月号・Natsume社)发售  
> 「City Hunterの冴羽獠に聞きました！」  
> ▽特集「对你来说，Mini是什么？」中獠登場  
> →附带北条先生的comment  
>   
> 
>   
>   
> 
### ●2009/08/21(金)
> 
> ■ComicsBunch第38号(9月4日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2009/08/07(金)
> 
> ■ComicsBunch第36・37合并特刊(8月21日・28日号)发售  
> 「Angel Heart」第340話  
> 【小さな依頼人】  
>   
> 
>   
>   
> 
### ●2009/07/31(金)
> 
> ■ComicsBunch第35号(8月14日号)发售  
> 「Angel Heart」第339話  
> 【繋いだ手から】  
>   
> 
>   
>   
> 
### ●2009/07/24(金)
> 
> ■ComicsBunch第34号(8月7日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2009/07/17(金)
> 
> ■ComicsBunch第33号(7月31日号)发售  
> 「Angel Heart」第338話  
> 【空のＣ・Ｈ(シティ Hunter )】  
>   
> 
>   
>   
> 
### ●2009/07/10(金)
> 
> ■ComicsBunch第32号(7月24日号)发售  
> 「Angel Heart」第337話  
> 【ちょっとの特別】  
>   
> 
>   
>   
> 
### ●2009/07/09(木)
> 
> ■Bunch Comics  
> 「Angel Heart」第30巻 发售  
> ▽封面是Comic Bunch新年3号封面的图案  
> →初回限定特典！記念postcard(共3种)  
>   
> 
>   
>   
> 
### ●2009/07/03(金)
> 
> ■ComicsBunch第31号(7月17日号)发售  
> 「Angel Heart」第336話  
> 【ふたつの命】  
>   
> 
>   
>   
> 
### ●2009/06/26(金)
> 
> ■ComicsBunch第30号(7月10日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2009/06/19(金)
> 
> ■ComicsBunch第29号(7月3日号)发售  
> 「Angel Heart」第335話  
> 【カエルの子はカエル】  
>   
> 
>   
>   
> 
### ●2009/06/12(金)
> 
> ■ComicsBunch第28号(6月26日号)发售  
> 「Angel Heart」第334話/封面  
> 【天使のサイモン】  
>   
> 
>   
>   
> 
### ●2009/06/05(金)
> 
> ■ComicsBunch第27号(6月19日号)发售  
> 「Angel Heart」第333話  
> 【いつかの笑顔】  
>   
> 
>   
>   
> 
### ●2009/05/29(金)
> 
> ■ComicsBunch第26号(6月12日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2009/05/22(金)
> 
> ■ComicsBunch第25号(6月5日号)发售  
> 「Angel Heart」第332話  
> 【君だけのヒーロー】  
>   
> 
>   
>   
> 
### ●2009/05/15(金)
> 
> ■ComicsBunch第24号(5月29日号)发售  
> 「Angel Heart」第331話  
> 【只今参上！】  
> ▽Bunch8周年記念特別Column  
> STORY Digest・作家陣Special Message  
> →全作品同時刊登  
>   
> 
>   
>   
> 
### ●2009/05/08(金)
> 
> ■ComicsBunch第23号(5月22日号)发售  
> 「Angel Heart」第330話  
> 【人質交換】  
>   
> 
>   
>   
> 
### ●2009/04/24(金)
> 
> ■ComicsBunch第21・22合并特刊(5月8日・15日号)发售  
> 「Angel Heart」第329話  
> 【潜入！ ＣＨ】  
>   
> 
>   
>   
> 
### ●2009/04/17(金)
> 
> ■ComicsBunch第20号(5月1日号)发售  
> 「Angel Heart」第328話  
> 【敵情視察】  
>   
> 
>   
>   
> 
### ●2009/04/10(金)
> 
> ■ComicsBunch第19号(4月24日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2009/04/03(金)
> 
> ■ComicsBunch第18号(4月17日号)发售  
> 「Angel Heart」第327話  
> 【昼行灯(ひるあんどん)】  
>   
> 
>   
>   
> 
### ●2009/03/27(金)
> 
> ■ComicsBunch第17号(4月10日号)发售  
> 「Angel Heart」第326話  
> 【対極の男】  
>   
> 
>   
>   
> 
### ●2009/03/19(木)
> 
> ■ComicsBunch第16号(4月3日号)发售  
> 「Angel Heart」第325話  
> 【老人(かれ)の貴物(おもいで)】  
>   
> 
>   
>   
> 
### ●2009/03/13(金)
> 
> ■ComicsBunch第15号(3月27日号)发售  
> 「Angel Heart」第324話  
> 【笑顔の理由】  
> ▽第16号的发售日是3月19日(木)  
>   
> 
>   
>   
> 
### ●2009/03/09(月)
> 
> ■Bunch Comics  
> 「Angel Heart」第29巻 发售  
> ▽封面是Comic Bunch'08年第43号封面的图案  
>   
> 
>   
>   
> 
### ●2009/03/06(金)
> 
> ■ComicsBunch第14号(3月20日号)发售  
> 「Angel Heart」第323話/封面  
> 【怪訝なＸＹＺ】  
>   
> 
>   
>   
> 
### ●2009/02/27(金)
> 
> ■ComicsBunch第13号(3月13日号)发售  
> 「Angel Heart」暂停。  
> ▽下期出现在封面上，新篇章Start  
>   
> 
>   
>   
> 
### ●2009/02/20(金)
> 
> ■ComicsBunch第12号(3月6日号)发售  
> 「Angel Heart」第322話  
> 【カメレオンの遺産】  
>   
> 
>   
>   
> 
### ●2009/02/13(金)
> 
> ■ComicsBunch第11号(2月27日号)发售  
> 「Angel Heart」第321話/封面  
> 【激昂】  
>   
> 
>   
>   
> 
### ●2009/02/06(金)
> 
> ■ComicsBunch第10号(2月20日号)发售  
> 「Angel Heart」第320話  
> 【制裁の行方】  
>   
> 
>   
>   
> 
### ●2009/01/30(金)
> 
> ■ComicsBunch第9号(2月13日号)发售  
> 「Angel Heart」第319話  
> 【黒幕】  
>   
> 
>   
>   
> 
### ●2009/01/23(金)
> 
> ■ComicsBunch第8号(2月6日号)发售  
> 「Angel Heart」第318話  
> 【現場復帰】  
>   
> 
>   
>   
> 
### ●2009/01/16(金)
> 
> ■ComicsBunch第7号(1月30日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   


### ●2008/01/09(金)（译注：应为2009/01/09(金)）
> 
> ■ComicsBunch新年6号(1月23日号)发售  
> 「Angel Heart」第317話  
> 【現行犯逮捕】  
> ▽2009新春、全作家阵容・亲笔Goods Present  
> →Sign 色紙＆ Message扇子  
>   
> 
>   
>   

## 2008年（译注）  
### ●2008/12/26(金)
> 
> ■ComicsBunch新年4・5合并特刊(1月16日・21日号)发售  
> 「Angel Heart」第316話  
> 【冴子の覚悟】  
>   
> 
>   
>   
> 
### ●2008/12/19(金)
> 
> ■ComicsBunch新年3号(1月9日号)发售  
> 「Angel Heart」第315話/封面  
> 【仕組まれた黙秘】  
> ▽特別付録・AH特製Book Cover  
>   
> 
>   
>   
> 
### ●2008/12/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第28巻 发售  
> ▽封面是Comic Bunch第39号Color扉页的图案  
>   
> 
>   
>   
> 
### ●2008/12/05(金)
> 
> ■ComicsBunch新年1・2合并特刊(1月1日・4日号)发售  
> 「Angel Heart」第314話  
> 【怪我の功名】  
> ▽特別付録2009年Bunch特製Calendar  
> →11月・12月系AH  
>   
> 
>   
>   
> 
### ●2008/11/28(金)
> 
> ■ComicsBunch第52号(12月12日号)发售  
> 「Angel Heart」第313話  
> 【長い一日】  
> ▽AH Original Zippo(一次2种)  
> 　Bunch在移动SHOP中确定发售  
> →详见12/9、在网站上发布  
>   
> 
>   
>   
> 
### ●2008/11/21(金)
> 
> ■ComicsBunch第51号(12月5日号)发售  
> 「Angel Heart」第312話  
> 【影武者】  
>   
> 
>   
>   
> 
### ●2008/11/14(金)
> 
> ■ComicsBunch第50号(11月28日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2008/11/07(金)
> 
> ■ComicsBunch第49号(11月21日号)发售  
> 「Angel Heart」第311話  
> 【宣戦布告】  
>   
> 
>   
>   
> 
### ●2008/10/31(金)
> 
> ■ComicsBunch第48号(11月14日号)发售  
> 「Angel Heart」第310話  
> 【真実(マコト)】  
>   
> 
>   
>   
> 
### ●2008/10/24(金)
> 
> ■ComicsBunch第47号(11月7日号)发售  
> 「Angel Heart」第309話  
> 【離れていても】  
>   
> 
>   
>   
> 
### ●2008/10/17(金)
> 
> ■ComicsBunch第46号(10月31日号)发售  
> 「Angel Heart」第308話  
> 【新婚旅行】  
>   
> 
>   
>   
> 
### ●2008/10/10(金)
> 
> ■ComicsBunch第45号(10月24日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2008/10/03(金)
> 
> ■ComicsBunch第44号(10月17日号)发售  
> 「Angel Heart」第307話  
> 【誓いの言葉】  
>   
> 
>   
>   
> 
### ●2008/09/26(金)
> 
> ■ComicsBunch第43号(10月10日号)发售  
> 「Angel Heart」第306話/封面  
> 【幸せのベール】  
> ▽「Cat's Eye」Pachislo特集  
>   
> 
>   
>   
> 
### ●2008/09/19(金)
> 
> ■ComicsBunch第42号(10月3日号)发售  
> 「Angel Heart」第305話  
> 【確かな鼓動】  
>   
> 
>   
>   
> 
### ●2008/09/12(金)
> 
> ■ComicsBunch第41号(9月26日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2008/09/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第27巻 发售  
> ▽封面是Comic Bunch第32号封面的图案  
>   
> 
>   
>   
> 
### ●2008/09/05(金)
> 
> ■ComicsBunch第40号(9月19日号)发售  
> 「Angel Heart」第304話/封面  
> 【似た者同士】  
>   
> 
>   
>   
> 
### ●2008/08/29(金)
> 
> ■ComicsBunch第39号(9月12日号)发售  
> 「Angel Heart」第303話/封面/巻頭Color  
> 【金木犀の香り】  
>   
> 
>   
>   
> 
### ●2008/08/22(金)
> 
> ■ComicsBunch第38号(9月5日号)发售  
> 「Angel Heart」第302話  
> 【身代わり】  
>   
> 
>   
>   
> 
### ●2008/08/08(金)
> 
> ■ComicsBunch第36・37合并特刊(8月22日・29日号)发售  
> 「Angel Heart」第301話  
> 【潜入】  
>   
> 
>   
>   
> 
### ●2008/08/01(金)
> 
> ■ComicsBunch第35号(8月15日号)发售  
> 「Angel Heart」第300話  
> 【恋の季節】  
>   
> 
>   
>   
> 
### ●2008/07/25(金)
> 
> ■ComicsBunch第34号(8月8日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2008/07/18(金)
> 
> ■ComicsBunch第33号(8月1日号)发售  
> 「Angel Heart」第299話  
> 【陰影】  
>   
> 
>   
>   
> 
### ●2008/07/11(金)
> 
> ■ComicsBunch第32号(7月25日号)发售  
> 「Angel Heart」第298話/封面  
> 【幸せの涙】  
>   
> 
>   
>   
> 
### ●2008/07/04(金)
> 
> ■ComicsBunch第31号(7月18日号)发售  
> 「Angel Heart」第297話  
> 【親子の時間】  
>   
> 
>   
>   
> 
### ●2008/06/27(金)
> 
> ■ComicsBunch第30号(7月11日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2008/06/20(金)
> 
> ■ComicsBunch第29号(7月4日号)发售  
> 「Angel Heart」第296話  
> 【本当の姿】  
>   
> 
>   
>   
> 
### ●2008/06/06(金)
> 
> ■ComicsBunch第28号(6月27日号)发售  
> 「Angel Heart」第295話  
> 【最期の言葉】  
> ▽AH Event 報告→委托内容的部分介绍  
>   
> 
>   
>   
> 
### ●2008/06/06(金)
> 
> ■ComicsBunch第27号(6月20日号)发售  
> 「Angel Heart」第294話  
> 【懺悔と供養】  
> ▽AH1500万部突破記念 Event 報告  
>   
> 
>   
>   
> 
### ●2008/05/30(金)
> 
> ■ComicsBunch第26号(6月13日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2008/05/23(金)
> 
> ■ComicsBunch第25号(6月6日号)发售  
> 「Angel Heart」第293話  
> 【傲慢】  
> ▽Comics連動・AH特製Frame申请用紙  
>   
> 
>   
>   
> 
### ●2008/05/16(金)
> 
> ■ComicsBunch第24号(5月30日号)发售  
> 「Angel Heart」第292話  
> 【笑い泣き】  
> ▽Comics連動・AH特製Frame申请用紙  
>   
> ▽創刊7周年記念・Bunch複製原画集Present  
>   
> 
>   
>   
> 
### ●2008/05/09(金)
> 
> ■Bunch Comics  
> 「Angel Heart」第26巻 发售  
> ▽封面是Comic Bunch'07年第24号封面的图案  
> ▽本誌連動AH Fan 感恩节・第2弾  
> →「AH Pins ＆特製Frame」申请券  
>   
> 
>   
>   
> 
### ●2008/05/09(金)
> 
> ■ComicsBunch第23号(5月23日号)发售  
> 「Angel Heart」番外編/封面  
> 【一夜の友情】  
> →「新宿鮫」大沢在昌Specialコラボ  
> ▽AH累計1500万部突破記念号  
> ▽TOP3 CROSS TALK  
> →北条司×井上雄彦×梅澤春人  
> ▽特別付録・AH特製插画Book  
> ▽ Event 「Memory of X・Y・Z」詳細  
> ▽Comics連動・AH特製Frame申请用紙  
> ▽手机Bunch申请者全員免费Present  
> →AH专用手机防偷窥贴纸  
> ▽AH Giclee版画Collection限量版销售  
> 
>   
>   
> 
### ●2008/04/25(金)
> 
> ■ComicsBunch第21・22合并特刊(5月9日・16日号)发售  
> 「Angel Heart」第291話  
> 【罪悪感】  
> ▽北条司先生sign会決定(5/17)  
> ▽5月中旬开始在新宿举行活动  
> →「Memory of X・Y・Z」  
> ▽下期(5/9发售)AH Comics  
> 累計1500万部突破記念号  
> →北条司大特集 SPECIAL2大企划  
>   
> 
>   
>   
> 
### ●2008/04/18(金)
> 
> ■ComicsBunch第20号(5月2日号)发售  
> 「Angel Heart」第290話  
> 【損な役回り】  
>   
> 
>   
>   
> 
### ●2008/04/11(金)
> 
> ■ComicsBunch第19号(4月25日号)发售  
> 「Angel Heart」第289話/封面  
> 【ロスタイム】  
>   
> 
>   
>   
> 
### ●2008/04/04(金)
> 
> ■ComicsBunch第18号(4月18日号)发售  
> 「Angel Heart」第288話  
> 【家族のために】  
>   
> 
>   
>   
> 
### ●2008/03/28(金)
> 
> ■ComicsBunch第17号(4月11日号)发售  
> 「Angel Heart」第287話  
> 【小さなＸＹＺ】  
>   
> 
>   
>   
> 
### ●2008/03/21(金)
> 
> ■ComicsBunch第16号(4月4日号)发售  
> 「Angel Heart」暂停。  
> ▽下期、新的发展正在到来   
>   
> 
>   
>   
> 
### ●2008/03/14(金)
> 
> ■ComicsBunch第15号(3月28日号)发售  
> 「Angel Heart」第286話  
> 【潮騒】  
>   
> 
>   
>   
> 
### ●2008/03/07(金)
> 
> ■ComicsBunch第14号(3月21日号)发售  
> 「Angel Heart」第285話  
> 【不安な夜】  
>   
> 
>   
>   
> 
### ●2008/02/29(金)
> 
> ■ComicsBunch第13号(3月14日号)发售  
> 「Angel Heart」第284話  
> 【ジンクス】  
>   
> 
>   
>   
> 
### ●2008/02/22(金)
> 
> ■ComicsBunch第12号(3月7日号)发售  
> 「Angel Heart」暂停。  
> ▽第25巻連動AH Fan 感恩节・申请用紙  
>   
> 
>   
>   
> 
### ●2008/02/15(金)
> 
> ■ComicsBunch第11号(2月29日号)发售  
> 「Angel Heart」第283話  
> 【灯(あかり)】  
> ▽第25巻連動AH Fan 感恩节・申请用紙  
>   
> 
>   
>   
> 
### ●2008/02/09(土)
> 
> ■Bunch Comics  
> 「Angel Heart」第25巻 发售  
> ▽封面是Comic Bunch'07年第50号封面的图案  
> ▽本誌連動AH Fan 感恩节・特製 Pins 申请券  
>   
> 
>   
>   
> 
### ●2008/02/08(金)
> 
> ■ComicsBunch第10号(2月22日号)发售  
> 「Angel Heart」第282話/封面  
> 【カメレオン動く】  
> ▽AH Fan 感恩节・申请者全員Service  
> →第25巻連動、AH特製 Pins 5件套装  
>   
> 
>   
>   
> 
### ●2008/02/01(金)
> 
> ■ComicsBunch第9号(2月15日号)发售  
> 「Angel Heart」第281話  
> 【ぷるん】  
>   
> 
>   
>   
> 
### ●2008/01/25(金)
> 
> ■ComicsBunch第8号(2月8日号)发售  
> 「Angel Heart」第280話  
> 【母の想い】  
>   
> 
>   
>   
> 
### ●2008/01/18(金)
> 
> ■ComicsBunch新年7号(2月1日号)发售  
> 「Angel Heart」第279話  
> 【一億の勝負】  
> ▽Bunch全作品T-shirt新春Present  
> →在design garden自由创作  
>   
> 
>   
>   
> 
### ●2008/01/10(木)
> 
> ■ComicsBunch新年6号(1月31日号)发售  
> 「Angel Heart」第278話  
> 【最凶悪危険人物】  
> ▽Signed彩色纸和其他新年礼物   
>   
> 
>   
>   

## 2007年（译注）  
### ●2007/12/27(木)
> 
> ■ComicsBunch新年4・5合并特刊(1月21日・25日号)发售  
> 「Angel Heart」第277話/封面  
> 【虎の尾】  
> ▽北条司総力特集Special Interview  
> ▽特別付録illustrated.Book「ライアのまど」（译注：ライア：Lyra/Riah。まど：Mado/窗户）    
> ▽CH JOURNAL：Pachislo CH  
> →围板、Poster等Present  
>   
> 
>   
>   
> 
### ●2007/12/21(金)
> 
> ■ComicsBunch新年3号(1月14日号)发售  
> 「Angel Heart」暂停。  
> ▽下期27日发售、北条司特集  
> → Interview＆特別付録「ライアのまど」  
>   
> 
>   
>   
> 
### ●2007/12/07(金)
> 
> ■ComicsBunch新年1・2合并特刊(1月1日・4日号)发售  
> 「Angel Heart」第276話  
> 【招かれざる客】  
> ▽CH単巻Release記念・CH JOURNAL  
> ▽下期暂停。4・5合并号系北条司全方位专题  
> illustrated.Book「ライアのまど」Present  
>   
> 
>   
>   
> 
### ●2007/11/30(金)
> 
> ■ComicsBunch第53号(12月14日号)发售  
> 「Angel Heart」第275話  
> 【カメレオンの罠】  
>   
> 
>   
>   
> 
### ●2007/11/22(木)
> 
> ■ComicsBunch第52号(12月7日号)发售  
> 「Angel Heart」第274話  
> 【葉月の悩み】  
>   
> 
>   
>   
> 
### ●2007/11/16(金)
> 
> ■ComicsBunch第51号(11月30日号)发售  
> 「Angel Heart」第273話  
> 【親子の愛】  
>   
> 
>   
>   
> 
### ●2007/11/09(金)
> 
> ■ComicsBunch第50号(11月23日号)发售  
> 「Angel Heart」第272話/封面  
> 【大好きなんです】  
>   
> 
>   
>   
> 
### ●2007/11/09(金)
> 
> ■Bunch Comics  
> 「Angel Heart」第24巻 发售  
> ▽封面是Comic Bunch第30号 巻頭Color扉页的图案  
>   
> 
>   
>   
> 
### ●2007/11/02(金)
> 
> ■ComicsBunch第49号(11月16日号)发售  
> 「Angel Heart」暂停。  
> ▽下期系AH封面  
>   
> 
>   
>   
> 
### ●2007/10/26(金)
> 
> ■ComicsBunch第48号(11月9日号)发售  
> 「Angel Heart」第271話  
> 【キバナコスモス】  
>   
> 
>   
>   
> 
### ●2007/10/19(金)
> 
> ■ComicsBunch第47号(11月2日号)发售  
> 「Angel Heart」第270話  
> 【ファルコンの秘密】  
>   
> 
>   
>   
> 
### ●2007/10/12(金)
> 
> ■ComicsBunch第46号(10月26日号)发售  
> 「Angel Heart」第269話  
> 【墓参り】  
> ▽CH単巻DVD发售決定  
> →12/19起毎月Release计划(全26巻)  
>   
> 
>   
>   
> 
### ●2007/10/05(金)
> 
> ■ComicsBunch第45号(10月19日号)发售  
> 「Angel Heart」第268話/封面  
> 【ファルコンのお守り】  
>   
> 
>   
>   
> 
### ●2007/09/28(金)
> 
> ■ComicsBunch第44号(10月12日号)发售  
> 「Angel Heart」第267話  
> 【葉月の父】  
>   
> 
>   
>   
> 
### ●2007/09/21(金)
> 
> ■ComicsBunch第43号(10月5日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2007/09/14(金)
> 
> ■ComicsBunch第42号(9月28日号)发售  
> 「Angel Heart」第266話  
> 【誤解】  
>   
> 
>   
>   
> 
### ●2007/09/07(金)
> 
> ■ComicsBunch第41号(9月21日号)发售  
> 「Angel Heart」第265話  
> 【海坊主と葉月】  
>   
> 
>   
>   
> 
### ●2007/08/31(金)
> 
> ■ComicsBunch第40号(9月14日号)发售  
> 「Angel Heart」第264話  
> 【夏の再会】  
>   
> 
>   
>   
> 
### ●2007/08/24(金)
> 
> ■ComicsBunch第39号(9月7日号)发售  
> 「Angel Heart」第263話  
> 【私の立ち位置】  
>   
> 
>   
>   
> 
### ●2007/08/10(金)
> 
> ■ComicsBunch第37・38合并特刊(8月24・31日号)发售  
> 「Angel Heart」第262話  
> 【仁志の涙】  
> ▽Bunch創刊300号記念Present  
> →AHS ign 色紙・北条先生推荐的红葡萄酒&白葡萄酒   
>   
> 
>   
>   
> 
### ●2007/08/09(木)
> 
> ■Bunch Comics  
> 「Angel Heart」第23巻 发售  
> ▽封面是Comic Bunch新年1・2合并号 巻頭Color扉页的图案  
>   
> 
>   
>   
> 
### ●2007/08/03(金)
> 
> ■ComicsBunch第36号(8月17日号)发售  
> 「Angel Heart」第261話  
> 【武道館決戦】  
>   
> 
>   
>   
> 
### ●2007/07/27(金)
> 
> ■ComicsBunch第35号(8月10日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2007/07/20(金)
> 
> ■ComicsBunch第34号(8月3日号)发售  
> 「Angel Heart」第260話  
> 【消えない記憶】  
>   
> 
>   
>   
> 
### ●2007/07/13(金)
> 
> ■ComicsBunch第33号(7月27日号)发售  
> 「Angel Heart」第259話  
> 【香瑩の怒り】  
>   
> 
>   
>   
> 
### ●2007/07/06(金)
> 
> ■ComicsBunch第32号(7月20日号)发售  
> 「Angel Heart」第258話  
> 【及第点の答え】  
>   
> 
>   
>   
> 
### ●2007/06/29(金)
> 
> ■ComicsBunch第31号(7月13日号)发售  
> 「Angel Heart」第257話  
> 【陳さんの暴走】  
>   
> 
>   
>   
> 
### ●2007/06/22(金)
> 
> ■ComicsBunch第30号(7月6日号)发售  
> 「Angel Heart」第256話/封面/巻頭Color  
> 【新スポンサー】  
>   
> 
>   
>   
> 
### ●2007/06/15(金)
> 
> ■ComicsBunch第29号(6月29日号)发售  
> 「Angel Heart」暂停。  
> ▽下周30号系AH封面＆巻頭Color  
>   
> 
>   
>   
> 
### ●2007/06/08(金)
> 
> ■ComicsBunch第28号(6月22日号)发售  
> 「Angel Heart」第255話  
> 【仲間】  
>   
> 
>   
>   
> 
### ●2007/06/01(金)
> 
> ■ComicsBunch第27号(6月15日号)发售  
> 「Angel Heart」第254話  
> 【嫉妬】  
>   
> 
>   
>   
> 
### ●2007/05/25(金)
> 
> ■ComicsBunch第26号(6月8日号)发售  
> 「Angel Heart」第253話  
> 【表への一歩】  
>   
> 
>   
>   
> 
### ●2007/05/18(金)
> 
> ■ComicsBunch第25号(6月1日号)发售  
> 「Angel Heart」暂停。  
> ▽Comics連動、DVD BOX4 Present申请券(15名)  
>   
> 
>   
>   
> 
### ●2007/05/11(金)
> 
> ■ComicsBunch第24号(5月25日号)发售  
> 「Angel Heart」第252話/封面  
> 【初めての経験】  
> ▽Comics連動、DVD BOX4 Present申请券(15名)  
>   
> 
>   
>   
> 
### ●2007/05/09(水)
> 
> ■Bunch Comics  
> 「Angel Heart」第22巻 发售  
> ▽封面是Comic Bunch新年5・6合并号封面的图案  
> →DVD BOX4 Present申请券(本誌連動/15名)  
>   
> 
>   
>   
> 
### ●2007/04/27(金)
> 
> ■ComicsBunch第22・23合并特刊(5月11日・18日号)发售  
> 「Angel Heart」第251話  
> 【意外な弱点】  
>   
> 
>   
>   
> 
### ●2007/04/20(金)
> 
> ■ComicsBunch第21号(5月4日号)发售  
> 「Angel Heart」第250話  
> 【暖かい場所へ】  
>   
> 
>   
>   
> 
### ●2007/04/13(金)
> 
> ■ComicsBunch第20号(4月27日号)发售  
> 「Angel Heart」第249話  
> 【見えない景色】  
>   
> 
>   
>   
> 
### ●2007/04/06(金)
> 
> ■ComicsBunch第19号(4月20日号)发售  
> 「Angel Heart」第248話  
> 【気づかぬ優しさ】  
>   
> 
>   
>   
> 
### ●2007/03/30(金)
> 
> ■ComicsBunch第18号(4月13日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2007/03/23(金)
> 
> ■ComicsBunch第17号(4月6日号)发售  
> 「Angel Heart」第247話  
> 【悲痛な願い】  
>   
> 
>   
>   
> 
### ●2007/03/16(金)
> 
> ■ComicsBunch第16号(3月30日号)发售  
> 「Angel Heart」第246話  
> 【雲のように】  
>   
> 
>   
>   
> 
### ●2007/03/09(金)
> 
> ■ComicsBunch第15号(3月23日号)发售  
> 「Angel Heart」第245話  
> 【初アフターはストーカー付！】  
> ▽Yahoo AH連動企划  
> →实物大小100tHammer、Yahoo Auction出品  
>   
> 
>   
>   
> 
### ●2007/03/02(金)
> 
> ■ComicsBunch第14号(3月16日号)发售  
> 「Angel Heart」第244話  
> 【恋はアフターで！】  
> ▽Yahoo AH連動企划→什么是智囊？  
>   
> 
>   
>   
> 
### ●2007/02/23(金)
> 
> ■ComicsBunch第13号(3月9日号)发售  
> 「Angel Heart」第243話  
> 【覚悟】  
> ▽Yahoo 連動企划・AH Baton活用術 (译注：Baton：接力棒)   
>   
> 
>   
>   
> 
### ●2007/02/16(金)
> 
> ■ComicsBunch第12号(3月2日号)发售  
> 「Angel Heart」第242話  
> 【香瑩の答え】  
> ▽AH×Yahoo!JAPAN連動企划  
> Angel Heart Special OPEN  
> →http://streaming.yahoo.co.jp/special/anime/angelheart/  
>   
> 
>   
>   
> 
### ●2007/02/09(金)
> 
> ■ComicsBunch第11号(2月23日号)发售  
> 「Angel Heart」暂停。  
> ▽AH×Yahoo!JAPAN連動企划  
> →2/16,Yahoo!動画AH特集page登場  
> 1.Yahoo! Auction Surprise Item出品←那个巨大的物品是实物尺寸的  
> 2.AH第1话可以在话题的新Media上阅读  
> 3.Yahoo!人気Corner也有北条先生参加  
>   
> 
>   
>   
> 
### ●2007/02/02(金)
> 
> ■ComicsBunch第10号(2月16日号)发售  
> 「Angel Heart」第241話  
> 【狙撃】  
> ▽Angel Heart × Yahoo! JAPAN 绝密项目进行中，详情见下期   
>   
> 
>   
>   
> 
### ●2007/01/26(金)
> 
> ■ComicsBunch第9号(2月9日号)发售  
> 「Angel Heart」第240話  
> 【カメレオンの誘惑】  
> ▽Comics連動、DVD BOX3 Present申请券(15名)  
>   
> 
>   
>   
> 
### ●2007/01/19(金)
> 
> ■ComicsBunch第8号(2月2日号)发售  
> 「Angel Heart」第239話  
> 【乙玲(イーリン)の姉】  
> ▽Comics連動、DVD BOX3 Present申请券(15名)  
>   
> 
>   
>   
> 
### ●2007/01/12(金)
> 
> ■ComicsBunch新年7号(1月31日号)发售  
> 「Angel Heart」第238話  
> 【花園学校の未来】  
> ▽Comics連動、DVD BOX3 Present申请券(15名)  
>   
> 
>   
>   
> 
### ●2007/01/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第21巻 发售  
> ▽封面是Comic Bunch'06年第47号封面的图案  
> ▽包含书带的初版限量版780日元  
> →しらけカラス＆100t Hammer  
> ▽DVD BOX3 Present申请券(该杂志連動/15名)  
>   
> 
>   
>   

## 2006年（译注）  
### ●2006/12/28(木)
> 
> ■ComicsBunch新年5・6合并特刊(1月22日・26日号)发售  
> 「Angel Heart」第237話/封面  
> 【カメレオン】  
> ▽卷头Color企划・有奖的AH测试  
> ▽特別附录・AH Comics Cover   
> 第21卷于1月9日发售→特制腰封包括在初版限量版中  
> ▽Comics联动、DVD BOX3 present申请券(15名)  
>   
> 
>   
>   
> 
### ●2006/12/22(金)
> 
> ■ComicsBunch新年4号(1月19日号)发售  
> 「Angel Heart」第236話  
> 【危険な出会い】  
> ▽下期5・6合并号的发售系12月28日(木)  
> →下期系AH封面、AH Comics Cover 付録＆AH审定召开  
>   
> 
>   
>   
> 
### ●2006/12/15(金)
> 
> ■ComicsBunch新年3号(1月12日号)发售  
> 「Angel Heart」第235話  
> 【肉弾戦】  
>   
> 
>   
>   
> 
### ●2006/12/01(金)
> 
> ■ComicsBunch新年1・2合并特刊(1月1日・5日号)发售  
> 「Angel Heart」第234話/巻頭Color  
> 【いい子の行き着く場所】  
>   
> 
>   
>   
> 
### ●2006/11/24(金)
> 
> ■ComicsBunch第52号(12月8日号)发售  
> 「Angel Heart」暂停。  
> ▽下期、新年1・2合并号AH巻頭Color  
>   
> 
>   
>   
> 
### ●2006/11/17(金)
> 
> ■ComicsBunch第51号(12月1日号)发售  
> 「Angel Heart」第233話  
> 【嫌な予感】  
>   
> 
>   
>   
> 
### ●2006/11/10(金)
> 
> ■ComicsBunch第50号(11月24日号)发售  
> 　「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2006/11/02(木)
> 
> ■ComicsBunch第49号(11月17日号)发售  
> 「Angel Heart」第232話  
> 【机男と親衛隊(シンエータイ)！】  
>   
> 
>   
>   
> 
### ●2006/10/27(金)
> 
> ■ComicsBunch第48号(11月10日号)发售  
> 「Angel Heart」第231話  
> 【ボクら、シャンイン親衛隊！？】  
>   
> 
>   
>   
> 
### ●2006/10/20(金)
> 
> ■ComicsBunch第47号(11月3日号)发售  
> 「Angel Heart」第230話/封面  
> 【ラブレター・パニック！】  
>   
> 
>   
>   
> 
### ●2006/10/13(金)
> 
> ■ComicsBunch第46号(10月27日号)发售  
> 「Angel Heart」第229話  
> 【想い、街に息づいて】  
>   
> 
>   
>   
> 
### ●2006/10/06(金)
> 
> ■ComicsBunch第45号(10月20日号)发售  
> 「Angel Heart」第228話  
> 【私たちの学校】  
>   
> 
>   
>   
> 
### ●2006/09/29(金)
> 
> ■ComicsBunch第44号(10月13日号)发售  
> 「Angel Heart」第227話  
> 【香瑩、入学す！】  
> ▽动画AH News、Chief P 諏訪的评论  
>   
> 
>   
>   
> 
### ●2006/09/25(月)
> 
> ■09/25(月) 读卖TV/25:25～  
> ■09/26(火) 日本TV/25:59～  
> 动画Angel Heart #50  
> 「Last Present」  
> →最終回  
> ▽DVD BOX2, Soundtrack  CD Present  
>   
> 
>   
>   
> 
### ●2006/09/22(金)
> 
> ■ComicsBunch第43号(10月6日号)发售  
> 「Angel Heart」第226話  
> 【真実と紗世】  
> ▽动画AH News、最終話提要和要点  
>   
> 
>   
>   
> 
### ●2006/09/18(月)
> 
> ■09/18(月) 读卖TV/25:25～  
> ■09/19(火) 日本TV/25:29～  
> 动画Angel Heart #49  
> 「Get My Life」  
> →下回「Last Present」  
>   
> 
>   
>   
> 
### ●2006/09/15(金)
> 
> ■ComicsBunch第42号(9月29日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2006/09/11(月)
> 
> ■09/11(月) 读卖TV/24:55～  
> ■09/12(火) 日本TV/25:29～  
> 动画Angel Heart #48  
> 「引き寄せられる運命」  
> →下回「Get My Life」  
>   
> 
>   
>   
> 
### ●2006/09/09(土)
> 
> ■Bunch Comics  
> 「Angel Heart」第20巻 发售  
> ▽封面是Comic Bunch第34号封面的图案  
>   
>   
> 
>   
>   
> 
### ●2006/09/08(金)
> 
> ■ComicsBunch第41号(9月22日号)发售  
> 「Angel Heart」第225話/封面  
> 【父のいる店】  
>   
> 
>   
>   
> 
### ●2006/09/04(月)
> 
> ■09/04(月) 读卖TV/24:55～  
> ■09/05(火) 日本TV/26:59～  
> 动画Angel Heart #47  
> 「アカルイミライ！？」  
> →下回「引き寄せられる運命」  
>   
> 
>   
>   
> 
### ●2006/09/01(金)
> 
> ■ComicsBunch第40号(9月15日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2006/08/28(月)
> 
> ■08/28(月) 读卖TV/25:25～  
> ■08/29(火) 日本TV/25:29～  
> 动画Angel Heart #46  
> 「マザー・ハート」  
> →下回「アカルイミライ！？」  
>   
> 
>   
>   
> 
### ●2006/08/25(金)
> 
> ■ComicsBunch第39号(9月8日号)发售  
> 「Angel Heart」第224話  
> 【重ねる嘘、重なる罪】  
>   
> 
>   
>   
> 
### ●2006/08/21(月)
> 
> ■08/21(月) 读卖TV/25:10～  
> ■08/22(火) 日本TV/26:14～  
> 动画Angel Heart #45  
> 「人間核弾頭、楊(ヤン)」  
> →下回「マザー・ハート」  
>   
> 
>   
>   
> 
### ●2006/08/14(月)
> 
> ■08/14(月) 读卖TV/24:55～  
> ■08/15(火) 日本TV/25:29～  
> 动画Angel Heart #44  
> 「俺たちの子供のために」  
> →下回「人間核弾頭、楊(ヤン)」  
>   
> 
>   
>   
> 
### ●2006/08/11(月)
> 
> ■ComicsBunch第37・38合并特刊(8月15日・9月1日号)发售  
> 「Angel Heart」第223話  
> 【紗世は名探偵！？】  
> ▽动画AH News、第44話简介  
> ▽7/29 天保山AH Event 的情况  
>   
> 
>   
>   
> 
### ●2006/08/07(月)
> 
> ■08/07(月) 读卖TV/25:14～  
> ■08/08(火) 日本TV/25:29～  
> 动画Angel Heart #43  
> 「私が生きる日常」  
> →下回「俺たちの子供のために」  
>   
> 
>   
>   
> 
### ●2006/08/04(金)
> 
> ■ComicsBunch第36号(8月18日号)发售  
> 「Angel Heart」第222話  
> 【カギは香瑩にあり！？】  
> ▽动画AH News、第43話提要和要点  
> →新OP ThemeCDPresent(20名)  
>   
> 
>   
>   
> 
### ●2006/07/31(月)
> 
> ■07/31(月) 读卖TV/25:00～  
> ■08/01(火) 日本TV/25:29～  
> 动画Angel Heart #42  
> 「二人だけのSign 」  
> →下回「私が生きる日常」  
>   
> 
>   
>   
> 
### ●2006/07/28(金)
> 
> ■ComicsBunch第35号(8月11日号)发售  
> 「Angel Heart」第221話  
> 【１８歳はシゲキがお好き！？】  
> ▽动画AH News、第42話提要和要点  
> ▽令人兴奋的宝岛AH活动通知   
>   
> 
>   
>   
> 
### ●2006/07/24(月)
> 
> ■07/24(月) 读卖TV/25:09～  
> ■07/25(火) 日本TV/25:38～  
> 动画Angel Heart #41  
> 「自分の居場所」  
> →下回「二人だけのSign 」  
>   
> 
>   
>   
> 
### ●2006/07/21(金)
> 
> ■ComicsBunch第34号(8月4日号)发售  
> 「Angel Heart」第220話/封面  
> 【謎のお父さん！】  
> ▽紧急通知・令人兴奋的宝岛AH活动  
> →7/29(土)13:00～  
> 由一位出色的音乐家现场演奏&  
> 举办川崎真央Talk Show  
>   
> 
>   
>   
> 
### ●2006/07/17(月)
> 
> ■07/17(月) 读卖TV/25:04～  
> ■07/18(火) 日本TV/25:38～  
> 动画Angel Heart #40  
> 「ミキの隠された秘密」  
> →下回「自分の居場所」  
>   
> 
>   
>   
> 
### ●2006/07/14(金)
> 
> ■ComicsBunch第33号(7月28日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2006/7/10(月)
> 
> ■07/10(月) 读卖TV/25:25～  
> ■07/11(火) 日本TV/25:29～  
> 动画Angel Heart #39  
> 「依頼者は大女優」  
> 新OP Theme「Battlefield of Love」  
> →下回「ミキの隠された秘密」  
>   
> 
>   
>   
> 
### ●2006/07/07(金)
> 
> ■ComicsBunch第32号(7月21日号)发售  
> 「Angel Heart」第219話  
> 【私がパパです！】  
> ▽动画AH News、第39話提要和要点  
>   
> 
>   
>   
> 
### ●2006/07/03(月)
> 
> ■07/03(月) 读卖TV/25:25～  
> ■07/04(火) 日本TV/25:29～  
> 动画Angel Heart #38  
> 「オレの目になってくれ」  
> →下回「依頼者は大女優」  
>   
> 
>   
>   
> 
### ●2006/06/30(金)
> 
> ■ComicsBunch第31号(7月14日号)发售  
> 「Angel Heart」第218話  
> 【大幸運期到来！！】  
>   
> 
>   
>   
> 
### ●2006/6/26(月)
> 
> ■06/26(月) 读卖TV/24:55～  
> ■06/27(火) 日本TV/25:29～  
> 动画Angel Heart #37  
> 「汚れのない心」  
> →下回「オレの目になってくれ」  
>   
> 
>   
>   
> 
### ●2006/06/23(金)
> 
> ■ComicsBunch第30号(7月7日号)发售  
> 「Angel Heart」第217話  
> 【楊の生きる理由】  
> ▽动画AH News、第37話提要和要点  
> ▽天宝山10天令人兴奋的宝岛2006的AH活动   
> →7/28(金)～8/6(日)  
>   
> 
>   
>   
> 
### ●2006/06/19(月)
> 
> ■06/19(月) 读卖TV/25:16～  
> ■06/20(火) 日本TV/25:29～  
> 动画Angel Heart #36  
> 「幸せを運ぶ女の子」  
> →下回「汚れのない心」  
>   
> 
>   
>   
> 
### ●2006/06/16(金)
> 
> ■ComicsBunch第29号(6月30日号)发售  
> 「Angel Heart」第216話  
> 【追う女、待つ女】  
> ▽动画AH News、第36話简介  
> →Comics連動・DVD等Present(150名)申请券  
>   
> 
>   
>   
> 
### ●2006/06/12(月)
> 
> ■06/12(月) 读卖TV/24:55～  
> ■06/13(火) 日本TV/25:29～  
> 动画Angel Heart #35  
> 「未来へ…」  
> →下回「幸せを運ぶ女の子」  
>   
> 
>   
>   
> 
### ●2006/06/09(金)
> 
> ■ComicsBunch第28号(6月23日号)发售  
> 「Angel Heart」第215話/封面  
> 【Ｃ・Ｈ資格テスト！】  
> ▽巻頭Color企划、形成AH应援团  
> →Comics連動・DVD等Present(150名)申请券  
> ▽动画AH News、第35話简介  
>   
> 
>   
>   
> 
### ●2006/06/09(金)
> 
> ■Bunch Comics  
> 「Angel Heart」第19巻 发售  
> ▽封面是Comic Bunch第22・23合并号封面的图案  
> →AH DVD BOX2等Present(150名)申请券  
>   
> 
>   
>   
> 
### ●2006/06/05(月)
> 
> ■06/05(月) 读卖TV/24:55～  
> ■06/06(火) 日本TV/25:29～  
> 动画Angel Heart #34  
> 「二人の決意」  
> →下回「未来へ…」  
>   
> 
>   
>   
> 
### ●2006/06/02(金)
> 
> ■ComicsBunch第27号(6月16日号)发售  
> 「Angel Heart」第214話  
> 【オペレーション ナシクズシ！】  
>   
> 
>   
>   
> 
### ●2006/05/29(月)
> 
> ■05/29(月) 读卖TV/24:55～  
> ■05/30(火) 日本TV/25:29～  
> 动画Angel Heart #33  
> 「神から授かりし子」  
> →下回「二人の決意」  
>   
> 
>   
>   
> 
### ●2006/05/26(金)
> 
> ■ComicsBunch第26号(6月9日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2006/05/22(月)
> 
> ■05/22(月) 读卖TV/24:55～  
> ■05/23(火) 日本TV/25:29～  
> 动画Angel Heart #32  
> 「組織から来た女」  
> →下回「神から授かりし子」  
>   
> 
>   
>   
> 
### ●2006/05/19(金)
> 
> ■ComicsBunch第25号(6月2日号)发售  
> 「Angel Heart」第213話  
> 【男の性(サガ)！】  
> ▽动画AH News、第32話提要和要点  
>   
> 
>   
>   
> 
### ●2006/05/15(月)
> 
> ■05/15(月) 读卖TV/24:55～  
> ■05/16(火) 日本TV/25:29～  
> 动画Angel Heart #31  
> 「最後の夜に見た奇跡」  
> →下回「組織から来た女」  
>   
> 
>   
>   
> 
### ●2006/05/12(金)
> 
> ■ComicsBunch第24号(5月26日号)发售  
> 「Angel Heart」第212話  
> 【長生きの代償】  
> ▽創刊5周年企划・这部Bunch漫画太精彩了!  
> →AH最新动画改编信息＆令人难忘的委托Best5   
> ▽动画AH News、第30話提要和要点  
>   
> 
>   
>   
> 
### ●2006/05/08(月)
> 
> ■05/08(月) 读卖TV/24:55～  
> ■05/09(火) 日本TV/25:59～  
> 动画Angel Heart #30  
> 「この街は私の全て」  
> →下回「最後の夜に見た奇跡」  
>   
> 
>   
>   
> 
### ●2006/05/1(月)
> 
> ■05/01(月) 读卖TV/24:55～  
> ■05/02(火) 日本TV/25:29～  
> 动画Angel Heart #29  
> 「私の妹…香」  
> →下回「この街は私の全て」  
>   
> 
>   
>   
> 
### ●2006/04/28(金)
> 
> ■ComicsBunch第22・23合并特刊(5月12日・19日号)发售  
> 「Angel Heart」第211話/封面  
> 【男二人、車中にて】  
> ▽特别附录AH袋装premium postcard  
> ▽动画AH News、第29話提要和要点  
>   
> 
>   
>   
> 
### ●2006/04/24(月)
> 
> ■04/24(月) 读卖TV/24:55～  
> ■04/25(火) 日本TV/25:29～  
> 动画Angel Heart #28  
> 「約束」  
> →下回「私の妹…香」  
>   
> 
>   
>   
> 
### ●2006/04/21(金)
> 
> ■ComicsBunch第21号(5月5日号)发售  
> 「Angel Heart」第210話  
> 【マオの決断】  
> ▽动画AH News、第28話提要和要点  
>   
> 
>   
>   
> 
### ●2006/04/17(月)
> 
> ■04/17(月) 读卖TV/25:25～  
> ■04/18(火) 日本TV/25:29～  
> 动画Angel Heart #27  
> 「私、恋してる！？」  
> →下回「約束」  
>   
> 
>   
>   
> 
### ●2006/04/14(金)
> 
> ■ComicsBunch第20号(4月28日号)发售  
> 「Angel Heart」第209話  
> 【迸る想い！】  
> ▽动画AH News、第27話简介  
> ▽玉置浩二 Special Interview  
> →新OP「Lion」CD Present(20名)  
>   
> 
>   
>   
> 
### ●2006/04/10(月)
> 
> ■04/10(月) 读卖TV/25:30～  
> ■04/11(火) 日本TV/25:29～  
> 动画Angel Heart #26  
> 「もう一度あの頃に」  
> →下回「私、恋してる！？」  
>   
> 
>   
>   
> 
### ●2006/04/07(金)
> 
> ■ComicsBunch第19号(4月21日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2006/04/03(月)
> 
> ■04/03(月) 读卖TV/25:30～  
> ■04/04(火) 日本TV/25:49～  
> 动画Angel Heart #25  
> 「死にたがる依頼者」  
> →下回「もう一度あの頃に」  
>   
> 
>   
>   
> 
### ●2006/03/31(金)
> 
> ■ComicsBunch第18号(4月14日号)发售  
> 「Angel Heart」第208話  
> 【海辺の告白！】  
> ▽动画AH News、第25話简介  
> ▽宇都宮隆×川崎真央 特別対談  
> →Signed AH特製Clear File Present  
>   
> 
>   
>   
> 
### ●2006/03/27(月)
> 
> ■03/27(月) 读卖TV/25:03～  
> ■03/28(火) 日本TV/26:25～  
> 动画Angel Heart #24  
> 「鼓動と共に…」  
> →下回「死にたがる依頼者」  
>   
> 
>   
>   
> 
### ●2006/03/24(金)
> 
> ■ComicsBunch第17号(4月7日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2006/03/20(月)
> 
> ■03/20(月) 读卖TV/25:29～  
> ■03/21(火) 日本TV/25:25～  
> 动画Angel Heart #23  
> 「出発(たびだち)のメロディー」  
> →下回「鼓動と共に…」  
>   
> 
>   
>   
> 
### ●2006/03/17(金)
> 
> ■ComicsBunch第16号(3月31日号)发售  
> 「Angel Heart」第207話  
> 【酒と泪(なみだ)と皇子と母親！？】  
> ▽动画AH News、第23話简介  
> ▽Comics連動企划・DVD BOX Present申请券  
>   
> 
>   
>   
> 
### ●2006/03/13(月)
> 
> ■03/13(月) 读卖TV/25:03～  
> ■03/14(火) 日本TV/25:25～  
> 动画Angel Heart #22  
> 「不公平な幸せ」  
> →下回「出発(たびだち)のメロディー」  
>   
> 
>   
>   
> 
### ●2006/03/10(金)
> 
> ■ComicsBunch第15号(3月24日号)发售  
> 「Angel Heart」第206話  
> 【皇子の苦悩】  
> ▽动画AH News、第22話简介  
> ▽Comics連動企划・DVD BOX Present申请券  
>   
> 
>   
>   
> 
### ●2006/03/09(木)
> 
> ■Bunch Comics  
> 「Angel Heart」第18巻 发售  
> ▽封面是Comic Bunch第8号 巻頭Color扉页的图案  
>   
> ▽本刊·漫画联动企划「AH DVD Premium Box Vol.1 present」/15名抽奖者  
> Comics带的报名券和Comic Bunch15号(3/10发售)&16号(3/17发售)的报名券，共3张成一组申请。  
> →截止日期是2006年3月31日（周五）<font color=#ff0000>必须到达</font>。 详情请见腰封。  
>   
> 
>   
>   
> 
### ●2006/03/06(月)
> 
> ■03/06(月) 读卖TV/25:03～  
> ■03/07(火) 日本TV/25:25～  
> 动画Angel Heart #21  
> 「哀しき守護者(ガーディアン)」  
> →下回「不公平な幸せ」  
>   
> 
>   
>   
> 
### ●2006/03/03(金)
> 
> ■ComicsBunch第14号(3月17日号)发售  
> 「Angel Heart」第205話  
> 【日常に潜むアクマ】  
> ▽动画AH News、第21話提要和要点  
> ▽<s>3/6(月)</s>、动画AH携帯 Site Open  
> →推迟到4月上旬  
> 
>   
>   
> 
### ●2006/02/27(月)
> 
> ■02/27(月) 读卖TV/25:03～  
> ■02/28(火) 日本TV/25:55～  
> 动画Angel Heart #20  
> 「宿命のプレリュード」  
> →下回「哀しき守護者(ガーディアン)」  
>   
> 
>   
>   
> 
### ●2006/02/24(金)
> 
> ■ComicsBunch第13号(3月10日号)发售  
> 「Angel Heart」第204話  
> 【家族の風景】  
> ▽动画AH News、第20話提要和要点  
>   
> 
>   
>   
> 
### ●2006/02/20(月)
> 
> ■02/20(月) 读卖TV/25:03～  
> ■02/21(火) 日本TV/25:25～  
> 动画Angel Heart #19  
> 「陳老人の店」  
> →下回「宿命のプレリュード」  
>   
> 
>   
>   
> 
### ●2006/02/17(金)
> 
> ■ComicsBunch第12号(3月3日号)发售  
> 「Angel Heart」第203話  
> 【マオの交渉】  
> ▽动画AH News、第19話提要和要点  
>   
> 
>   
>   
> 
### ●2006/02/13(月)
> 
> ■02/13(月) 读卖TV/25:03～  
> ■02/14(火) 日本TV/25:25～  
> 动画Angel Heart #18  
> 「親子の絆」  
> →下回「陳老人の店」  
>   
> 
>   
>   
> 
### ●2006/02/10(金)
> 
> ■ComicsBunch第11号(2月24日号)发售  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2006/02/06(月)
> 
> ■02/06(月) 读卖TV/27:03～  
> ■02/07(火) 日本TV/26:00～  
> 动画Angel Heart #17  
> 「夢の中の出会い」  
> →下回「親子の絆」  
>   
> 
>   
>   
> 
### ●2006/02/03(金)
> 
> ■ComicsBunch第10号(2月17日号)发售  
> 「Angel Heart」第202話  
> 【昨日の敵は今日の恋人！】  
> ▽动画AH News、第17話提要和要点  
>   
> 
>   
>   
> 
### ●2006/01/30(月)
> 
> ■01/30(月) 读卖TV/24:59～  
> ■01/31(火) 日本TV/25:40～  
> 动画Angel Heart #16  
> 「Ｃ・Ｈとしての資格」  
> →下回「夢の中の出会い」  
>   
> 
>   
>   
> 
### ●2006/01/27(金)
> 
> ■ComicsBunch第9号(2月10日号)发售  
> 「Angel Heart」第201話  
> 【皇子と香瑩の初デート(はぁと)】  
> ▽动画AH News、第16話提要和要点  
> →AH DVD Premium BOX 发售日大決定  
>   
> 
>   
>   
> 
### ●2006/01/23(月)
> 
> ■01/23(月) 读卖TV/24:59～  
> ■01/24(火) 日本TV/25:25～  
> 动画Angel Heart #15  
> 「パパを捜して」  
> →下回「Ｃ・Ｈとしての資格」  
>   
> 
>   
>   
> 
### ●2006/01/20(月)
> 
> ■ComicsBunch第8号(2月3日号)发售  
> 「Angel Heart」第200話/封面/巻頭Color  
> 【妄想超特急がやってきた！】  
> ▽动画AH News、第15話提要和要点  
>   
> 
>   
>   
> 
### ●2006/01/16(月)
> 
> ■01/16(月) 读卖TV/24:59～  
> ■01/17(火) 日本TV/25:25～  
> 动画Angel Heart #14  
> 「復活Ｃ・Ｈ！」  
> →下回「パパを捜して」  
>   
> 
>   
>   
> 
### ●2006/01/13(金)
> 
> ■ComicsBunch第7号(1月27日号)发售  
> 「Angel Heart」第199話  
> 【異国からの求婚者！】  
> ▽动画AH News、第14話提要和要点  
> →「Finally」CD 20名にPresent  
>   
> 
>   
>   
> 
### ●2006/01/09(月)
> 
> ■01/09(月) 读卖TV/25:50～  
> ■01/10(火) 日本TV/25:55～  
> 动画Angel Heart #13  
> 「李大人からの贈り物」  
> →下回「復活Ｃ・Ｈ！」  
>   
> 
>   
>   
> 
### ●2006/01/06(金)
> 
> ■ComicsBunch新年6号(1月20日号)发售  
> 「Angel Heart」第198話  
> 【クリスマスPresent】  
> ▽动画AH News、第13話提要和要点  
> →OP曲「Finally」CD 2/1(水) DefSTAR RECORDSより发售  
>   
> 
>   
>   

## 2005年（译注）  
### ●2005/12/22(木)
> 
> ■ComicsBunch新年4・5合并特刊(1月6日・13日号)发售  
> 「Angel Heart」第197話  
> 【指輪の記憶】  
> ▽2006年春、AH动画DVD发售開始決定  
>   
> 
>   
>   
> 
### ●2005/12/19(月)
> 
> ■12/19(月) 读卖TV/24:59～  
> ■01/03(火) 日本TV/26:04～  
> 动画Angel Heart #12  
> 「船上の出会いと別れ」  
> →下回「李大人からの贈り物」  
>   
> 
>   
>   
> 
### ●2005/12/16(金)
> 
> ■ComicsBunch新年3号(12月30日号)发售  
> 「Angel Heart」第196話  
> 【楊(ヤン)、再来！！】  
>   
> 
>   
>   
> 
### ●2005/12/15(木)
> 
> ■「北条司漫画家25周年記念  
> 　自選插画レーション100」发售  
> →定价2940日元  
> ▽新绘制的香莹封面・新绘制的AH Poster    
> ▽特製Quo Card Present  
>   
> 
>   
>   
> 
### ●2005/12/12(月)
> 
> ■12/12(月) 读卖TV/25:59～  
> ■12/20(火) 日本TV/25:25～  
> 动画Angel Heart #11  
> 「父娘(おやこ)の時間」  
> →下回「船上の出会いと別れ」  
>   
> 
>   
>   
> 
### ●2005/12/09(金)
> 
> ■Bunch Comics  
> 「Angel Heart」第17巻 发售  
> ▽封面是Comic Bunch第21号封面的图案  
>   
> ■ComicsBunch増刊「Angel Heart总集篇  
> 动画＆Comics Special」发售  
> →定价390日元  
>   
> 
>   
>   
> 
### ●2005/12/05(月)
> 
> ■12/05(月) 读卖TV/25:09～  
> ■12/13(火) 日本TV/25:55～  
> 动画Angel Heart #10  
> 「Angel Smile」  
> →下回「父娘(おやこ)の時間」  
>   
> 
>   
>   
> 
### ●2005/12/02(金)
> 
> ■ComicsBunch新年1・2合并特刊(12月16日・23日号)发售  
> 「Angel Heart」第195話/封面  
> 【夏休みの終わり】  
> ▽特別付録・AH Christmas card  
> ▽AH邮票表・Present申请要項  
> ▽动画AH News、第10話提要和要点  
>   
> 
>   
>   
> 
### ●2005/11/28(月)
> 
> ■11/28(月) 读卖TV/25:09～  
> ■12/06(火) 日本TV/25:35～  
> 动画Angel Heart #9  
> 「香瑩～失われた名前～」  
> →下回「Angel Smile」  
>   
> 
>   
>   
> 
### ●2005/11/25(金)
> 
> ■ComicsBunch第52号(12月9日号)发售  
> 「Angel Heart」暂停。  
> ▽动画AH News、第9話提要和要点  
> ▽下期(12/2发售)AH Christmas card特別付録  
>   
> 
>   
>   
> 
### ●2005/11/21(月)
> 
> ■11/21(月) 读卖TV/25:19～  
> ■11/29(火) 日本TV/25:35～  
> 动画Angel Heart #8  
> 「真実(ホント)の仲間」  
> →下回「香瑩～失われた名前～」  
>   
> 
>   
>   
> 
### ●2005/11/18(金)
> 
> ■ComicsBunch第51号(12月2日号)发售  
> 「Angel Heart」第194話  
> 【親子の覚悟】  
> ▽动画AH News、第8話提要和要点  
> ▽新年1、2合并号上AH特别附录的计划  
>   
> 
>   
>   
> 
### ●2005/11/14(月)
> 
> ■11/14(月) 读卖TV/25:28～  
> ■11/22(火) 日本TV/25:55～  
> 动画Angel Heart #7  
> 「俺の愛すべき街」  
> →下回「真実(ホント)の仲間」  
>   
> 
>   
>   
> 
### ●2005/11/11(金)
> 
> ■ComicsBunch第50号(11月25日号)发售  
> 「Angel Heart」暂停。  
> ▽涵盖从CE到AH的北条司自选插图集 
> →徳間書店将于12月中旬发售決定  
> ▽Anime AH News、第7話提要和要点  
>   
> 
>   
>   
> 
### ●2005/11/07(月)
> 
> ■11/07(月) 读卖TV/24:59～  
> ■11/08(火) 日本TV/放送休止  
> ⇒11/15(火) 日本TV/25:40～  
> 动画Angel Heart #6  
> 「再会」  
> →下回「俺の愛すべき街」  
>   
> 
>   
>   
> 
### ●2005/11/04(金)
> 
> ■ComicsBunch第49号(11月18日号)发售  
> 「Angel Heart」第193話  
> 【怒りの一撃！】  
> ▽Angel Heart総集編 12/09(金)发售決定  
> ▽动画AH News、第6話提要和要点  
>   
> 
>   
>   
> 
### ●2005/10/31(月)
> 
> ■10/31(月) 读卖TV/24:59～  
> ■11/01(火) 日本TV/26:25～  
> 动画Angel Heart #5  
> 「永別(さよなら) …カオリ」  
> →下回「再会」  
>   
> 
>   
>   
> 
### ●2005/10/28(金)
> 
> ■ComicsBunch第48号(11月11日号)发售  
> 「Angel Heart」第192話  
> 【親子の電話】  
> ▽动画AH News、第5話提要和要点  
> →「誰かが君を想ってる」CD Present(30名)  
>   
> 
>   
>   
> 
### ●2005/10/24(月)
> 
> ■10/24(月) 读卖TV/25:14～  
> ■10/25(火) 日本TV/25:25～  
> 动画Angel Heart #4  
> 「さまようHEART」  
> →下回「永別(さよなら) …カオリ」  
>   
> 
>   
>   
> 
### ●2005/10/21(金)
> 
> ■ComicsBunch第47号(11月4日号)发售  
> 「Angel Heart」第191話  
> 【恐怖のカーチェイス！】  
> ▽动画AH News、第4話提要和要点  
> →「誰かが君を想ってる」CD 11/9(水)发售  
>   
> 
>   
>   
> 
### ●2005/10/17(月)
> 
> ■10/17(月) 读卖TV/25:13～  
> ■10/18(火) 日本TV/25:34～  
> 动画Angel Heart #3  
> 「XYZの街」  
> →下回「さまようHEART」  
>   
> 
>   
>   
> 
### ●2005/10/14(金)
> 
> ■ComicsBunch第46号(10月28日号)发售  
> 「Angel Heart」第190話  
> 【バスジャック！】  
> ▽动画AH News、第3話简介  
>   
> 
>   
>   
> 
### ●2005/10/10(月)
> 
> ■10/10(月) 读卖TV/25:28～  
> ■10/11(火) 日本TV/25:49～  
> 动画Angel Heart #2  
> 「香が帰ってきた」  
> →下回「XYZの街  
>   
> 
>   
>   
> 
### ●2005/10/07(金)
> 
> ■ComicsBunch第45号(10月21日号)发售  
> 「Angel Heart」第189話/封面  
> 【変質者、現る！】  
> ▽动画CColor特集  
> OP/ED曲・Artist's Comment  
>   
> 
>   
>   
> 
### ●2005/10/03(月)
> 
> ■10/03(月) 读卖TV/25:34～  
> ■10/04(火) 日本TV/26:25～  
> 动画Angel Heart放送開始  
> 动画Angel Heart #1  
> 「ガラスの心臓 グラス・ハート」  
>   
> 
>   
>   
> 
### ●2005/09/30(金)
> 
> ■ComicsBunch第44号(10月14日号)发售  
> 「Angel Heart」第188話/封面  
> 【海学級へ行こう！】  
> ▽动画巻頭Color特集／1話提要和要点etc.  
> →OP曲「Finally」by Sowelu  
> →ED曲「誰かが君を想ってる」by Skoop On Somebody  
> ▽动画放映記念付録「CD型卓上Calendar」  
> ▽「香瑩等身大Poster」全員Service申请要項  
>   
> 
>   
>   
> 
### ●2005/09/22(木)
> 
> ■ComicsBunch第43号(10月7日号)发售  
> 「Angel Heart」第187話  
> 【ミキが行方不明！？】  
> ▽下期(44号)有2大特典付き・巻頭Color特集  
>   
> 
>   
>   
> 
### ●2005/09/16(金)
> 
> ■ComicsBunch第42号(9月30日号)  
> 「Angel Heart」暂停。  
> ▽动画AH放送日決定  
> 读卖TV・10月3日(月)~~24:58～~~  
> 日本TV・10月4日(火)~~25:25～~~  
>   
> 
>   
>   
> 
### ●2005/09/09(金)
> 
> ■ComicsBunch第41号(9月23日号)发售  
> 「Angel Heart」第186話/封面  
> 【変質者は海坊主！？】  
> ▽动画AH News  
> →首版・人物对照表  
>   
> 
>   
>   
> 
### ●2005/09/09(金)
> 
> ■Bunch Comics  
> 「Angel Heart」第16巻 发售  
>   
> ▽封面是Comic Bunch新年4・5合并特刊 封面的图案  
>   
> 
>   
>   
> 
### ●2005/09/02(金)
> 
> ■ComicsBunch第40号(9月16日号)发售  
> 「Angel Heart」第185話  
> 【海坊主と先生】  
> ▽动画AH製作発表会に潜入  
> ▽放送曜日決定  
> 读卖TV・毎週月曜24:58～  
> 日本TV・毎週火曜25:25～  
>   
> 
>   
>   
> 
### ●2005/08/26(金)
> 
> ■ComicsBunch第39号(9月9日号)发售  
> 「Angel Heart」第184話  
> 【義父(ちち)はつらいよ！】  
>   
> 
>   
>   
> 
### ●2005/08/12(金)
> 
> ■ComicsBunch第37・38合并特刊(8月26日・9月2日号)发售  
> 「Angel Heart」第183話  
> 【麗子の未来】  
> ▽CColor・动画AH速報  
> →读卖TV・日本TV今秋播出  
> →AH Digest DVD Present(100名)  
> ▽13日～銀座にてCH展開催  
> ▽CAT'S EYE完全版・计划在10月登场  
>   
> 
>   
>   
> 
### ●2005/08/05(金)
> 
> ■ComicsBunch第36号(8月19日号)发售  
> 「Angel Heart」第182話  
> 【信宏が教えてくれた事】  
>   
> 
>   
>   
> 
### ●2005/07/29(金)
> 
> ■ComicsBunch第35号(8月12日号)  
> 「Angel Heart」暂停。  
> ▽CH COMPLETE DVD BOX Color广告  
> ▽动画AH News・Poster用插画  
>   
> ▽令人兴奋的宝島in天保山「Heart of City」  
> →7/31(日) 神谷明和川崎真央登場  
> 　詳細はhttp://www.ytv.co.jp/wakuwaku/  
>   
> ▽THE CITY HUNTER展 Vol.1 銀座  
> →DVD BOX・AH动画化記念／原画大公開・企划齐全  
> 　(8/13～8/21 Sony大厦8楼)详见第37、38期合并号  
>   
> 
>   
>   
> 
### ●2005/07/22(金)
> 
> ■ComicsBunch第34号(8月5日号)发售  
> 「Angel Heart」第181話  
> 【未来を変える男】  
>   
> 
>   
>   
> 
### ●2005/07/14(木)
> 
> ■ComicsBunch第33号(7月29日号)发售  
> 「Angel Heart」第180話  
> 【引き寄せられる運命】  
>   
> 
>   
>   
> 
### ●2005/07/08(金)
> 
> ■ComicsBunch第32号(7月22日号)发售  
> 「Angel Heart」第179話  
> 【笑顔の未来】  
>   
> 
>   
>   
> 
### ●2005/07/01(金)
> 
> ■ComicsBunch第31号(7月15日号)发售  
> 「Angel Heart」第178話  
> 【透視の代償】  
>   
> 
>   
>   
> 
### ●2005/06/24(金)
> 
> ■ComicsBunch第30号(7月8日号)发售  
> 「Angel Heart」第177話  
> 【男の信念】  
>   
> 
>   
>   
> 
### ●2005/06/17(金)
> 
> ■ComicsBunch第29号(7月1日号)发售  
> 「Angel Heart」第176話  
> 【悲しい二人】  
>   
> 
>   
>   
> 
### ●2005/06/10(金)
> 
> ■ComicsBunch第28号(6月24日号)发售  
> 「Angel Heart」第175話/封面  
> 【恋人達の未来】  
> ▽CColor特集「A・Hを構築する3つの世界」（译注：构建A·H的3个世界）  
> →Angel BOX的申请券  
>   
> 
>   
>   
> 
### ●2005/06/09(木)
> 
> ■Bunch Comics  
> 「Angel Heart」第15巻 发售  
> ▽封面是Comic Bunch第28号封面的图案  
>   
> ▽Comics累計1000万部突破記念「AngelBOX」Present  
> Comics 1～15巻封面画的复制品／18张一套  
> →B4尺寸（高364x 宽257mm）/装在带有序号的豪华礼盒中  
> ※腰封上的入场券和Comic Bunch第28期（6月10日发售）上的入场券都是必需的/1000名获奖者将被抽出 
> →截止日期为6月30日（周四），以当天邮戳为准，详见腰封    
>   
> 
>   
>   
> 
### ●2005/06/03(金)
> 
> ■ComicsBunch第27号(6月17日号)  
> 「Angel Heart」暂停。  
> ▽下期(28号)系AH封面＆Center Color特集  
>   
> 
>   
>   
> 
### ●2005/05/27(金)
> 
> ■ComicsBunch第26号(6月10日号)发售  
> 「Angel Heart」第174話  
> 【麗泉(れいせん)の悩み】  
>   
> 
>   
>   
> 
### ●2005/05/20(金)
> 
> ■ComicsBunch第25号(6月3日号)发售  
> 「Angel Heart」第173話  
> 【アカルイミライ！？】  
>   
> 
>   
>   
> 
### ●2005/05/12(木)
> 
> ■ComicsBunch第24号(5月27日号)发售  
> 「Angel Heart」第172話  
> 【母なる思い】  
> ▽包括北斗の拳、CH贴纸&走运千社札带  
>   
> 
>   
>   
> 
### ●2005/04/28(木)
> 
> ■ComicsBunch第22・23合并特刊(5月13日・20日号)发售  
> 「Angel Heart」第171話  
> 【再会…！！】  
> ▽下期系5月12日(木)发售  
>   
> 
>   
>   
> 
### ●2005/04/22(金)
> 
> ■ComicsBunch第21号(5月6日号)发售  
> 「Angel Heart」第170話/封面  
> 【楊(ヤン)、始動！】  
> ▽下期系4月28日(木)发售  
>   
> 
>   
>   
> 
### ●2005/04/15(金)
> 
> ■ComicsBunch第20号(4月29日号)发售  
> 「Angel Heart」第169話  
> 【黒幕現る！！】  
>   
> 
>   
>   
> 
### ●2005/04/08(金)
> 
> ■ComicsBunch第19号(4月22日号)发售  
> 「Angel Heart」第168話  
> 【天国と地獄！？】  
> ▽动画香瑩役、川崎真央的直接 Interview  
>   
> 
>   
>   
> 
### ●2005/04/02-03(土-日)
> 
> ■04/03(日)23時～NACK5  
> ■04/02(土)24時～FM愛知・FM大阪  
> 广播节目「HEART OF ANGEL」内corner  
> 「XYZ Ryo's Bar」放送Start  
> →神谷明和美女Guest的talk corner  
> ▽第一批的Guest是麻上洋子和伊倉一恵  
>   
> 
>   
>   
> 
### ●2005/04/01(金)
> 
> ■ComicsBunch第18号(4月15日号)  
> ▽动画AH声優Audition発表  
> →香瑩役声優：川崎真央(18岁)   
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2005/03/25(金)
> 
> ■ComicsBunch第17号(4月8日号)发售  
> 「Angel Heart」第167話  
> 【楊芳玉(ヤン Fan ユィ)の仕事】  
> ▽下期（第18期）将有一个关于动漫AH的主要专题   
> →香瑩役声優大公布／本篇暂停  
>   
> 
>   
>   
> 
### ●2005/03/17(木)
> 
> ■ComicsBunch第16号(4月1日号)发售  
> 「Angel Heart」第166話  
> 【愛は地球を救う！？】  
> ▽动画AH News  
> 潜入香莹役声优Audition！  
> 香莹&獠 线画设定定稿!  
>   
> ▽Anime放送日時  
> 日本TV系 4月22日(金)27時8分～  
> 读卖TV系4月18日(月)24時58分～  
>   
> 
>   
>   
> 
### ●2005/03/11(金)
> 
> ■ComicsBunch第15号(3月25日号)发售  
> 「Angel Heart」第165話  
> 【楊(ヤン)からの依頼？】  
> ▽动画AH News  
> 　带有天使之翼的logo决定了!  
> 　主要的演员阵容与CH相同！  
>   
> 
>   
>   
> 
### ●2005/03/04(金)
> 
> ■ComicsBunch第14号(3月18日号)发售  
> 「Angel Heart」第164話  
> 【それぞれの場所】  
>   
> 
>   
>   
> 
### ●2005/02/25(金)
> 
> ■ComicsBunch第13号(3月11日号)发售  
> 「Angel Heart」第163話  
> 【女同士の酒】  
> ▽C-color/手机 AH Game「Same Bunch」攻略留言板  
> ▽北斗の拳Raoh外传，2006年春季改编为电影  
> →新角色悲伤女战士「Reina」角色由北条先生设计 
> 
>   
>   
> 
### ●2005/02/18(金)
> 
> ■ComicsBunch第12号(3月4日号)发售  
> 「Angel Heart」第162話  
> 【獠の大失敗】  
>   
> 
>   
>   
> 
### ●2005/02/10(木)
> 
> ■ComicsBunch第11号(2月25日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2005/02/09(水)
> 
> ■Bunch Comics  
> 「Angel Heart」第14巻 发售  
>   
> ▽封面是Comic Bunch'03年第52号封面的图案  
>   
> 
>   
>   
> 
### ●2005/02/04(金)
> 
> ■ComicsBunch第10号(2月18日号)发售  
> 「Angel Heart」第161話  
> 【恋の覚悟】  
>   
> 
>   
>   
> 
### ●2005/01/28(金)
> 
> ■ComicsBunch第9号(2月11日号)发售  
> 「Angel Heart」第160話  
> 【ホレた男は謎だらけ！？】  
> ▽今春决定TV转播! 动画AH速报第2弹  
> 　Center Color 香莹役声优Audition详情  
>   
> 
>   
>   
> 
### ●2005/01/21(金)
> 
> ■ComicsBunch第8号(2月4日号)发售  
> 「Angel Heart」第159話  
> 【私、恋してます！】  
>   
> 
>   
>   
> 
### ●2005/01/14(金)
> 
> ■ComicsBunch第7号(1月28日号)发售  
> 「Angel Heart」第158話  
> 【愛される理由】  
> ▽第9期中香莹役声优Audition细节 
>   
> 
>   
>   
> 
### ●2005/01/07(金)
> 
> ■ComicsBunch新年6号(1月21日号)发售  
> 「Angel Heart」第157話  
> 【獠の手机ライフ】  
> ▽决定举办香莹役声优Audition  
>   
> 
>   
>   

## 2004年（译注）  
### ●2004/12/24(金)
> 
> ■ComicsBunch新年4・5合并特刊(1月11日・14日号)发售  
> 「Angel Heart」第156話/封面  
> 【家族の写真】  
> ▽TV动画化速報第一弾＆AH特製Calendar  
>   
> 
>   
>   
> 
### ●2004/12/16(木)
> 
> ■ComicsBunch新年3号(1月7日号)发售  
> 「Angel Heart」第155話  
> 【告白…！】  
> ▽第4、5期合并号在卷首彩画上有一个关于TV动画改编的重要预告专题  
> 
>   
>   
> 
### ●2004/12/03(金)
> 
> ■ComicsBunch新年1・2合并特刊(12月24日・31日号)发售  
> 「Angel Heart」第154話  
> 【ジョイの行き先】  
> ▽下期(新年3号)系12月16日(木)发售  
>   
> 
>   
>   
> 
### ●2004/11/26(金)
> 
> ■ComicsBunch第52号(12月10日号)发售  
> 「Angel Heart」第153話  
> 【親子のように】  
>   
> 
>   
>   
> 
### ●2004/11/19(金)
> 
> ■ComicsBunch第51号(12月3日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2004/11/12(金)
> 
> ■ComicsBunch第50号(11月26日号)发售  
> 「Angel Heart」第152話  
> 【ミキの幸せ】  
>   
> 
>   
>   
> 
### ●2004/11/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第13巻 发售  
>   
> ▽封面是Comic Bunch第40号封面的图案  
>   
> 
>   
>   
> 
### ●2004/11/05(金)
> 
> ■ComicsBunch第49号(11月19日号)发售  
> 「Angel Heart」第151話  
> 【共同生活開始！】  
> ▽AH TV动画化決定  
>   
> 
>   
>   
> 
### ●2004/10/29(金)
> 
> ■ComicsBunch第48号(11月12日号)发售  
> 「Angel Heart」第150話  
> 【愛しき人の形見】  
>   
> 
>   
>   
> 
### ●2004/10/22(金)
> 
> ■ComicsBunch第47号(11月5日号)发售  
> 「Angel Heart」第149話  
> 【思い人はこの街に？】  
>   
> 
>   
>   
> 
### ●2004/10/15(金)
> 
> ■ComicsBunch第46号(10月29日号)发售  
> 「Angel Heart」第148話  
> 【ジョイの事情】  
>   
> 
>   
>   
> 
### ●2004/10/07(木)
> 
> ■ComicsBunch第45号(10月22日号)发售  
> 「Angel Heart」第147話  
> 【"危険"な大女優！】  
>   
> 
>   
>   
> 
### ●2004/10/01(金)
> 
> ■ComicsBunch第44号(10月15日号)发售  
> 「Angel Heart」第146話  
> 【優しき店長(マスター)】  
>   
> 
>   
>   
> 
### ●2004/09/24(金)
> 
> ■ComicsBunch第43号(10月8日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2004/09/16(木)
> 
> ■ComicsBunch第42号(10月1日号)发售  
> 「Angel Heart」第145話  
> 【暗闇に見えた夕陽】  
> ▽下期、43号暂停  
>   
> 
>   
>   
> 
### ●2004/09/10(金)
> 
> ■ComicsBunch第41号(9月24日号)发售  
> 「Angel Heart」第144話  
> 【いつも一緒】  
>   
> 
>   
>   
> 
### ●2004/09/09(木)
> 
> ■Bunch Comics  
> 「Angel Heart」第12巻 发售  
>   
> ▽封面是Comic Bunch第27号扉页画的图案  
>   
> 
>   
>   
> 
### ●2004/09/03(金)
> 
> ■ComicsBunch第40号(9月17日号)发售  
> 「Angel Heart」第143話/封面  
> 【ママへの想い】  
>   
> 
>   
>   
> 
### ●2004/08/27(金)
> 
> ■ComicsBunch第39号(9月10日号)发售  
> 「Angel Heart」第142話  
> 【汚れなき心】  
>   
> 
>   
>   
> 
### ●2004/08/19(木)
> 
> ■ComicsBunch第38号(9月3日号)发售  
> 「Angel Heart」第141話  
> 【海坊主と座敷童】  
>   
> 
>   
>   
> 
### ●2004/08/06(金)
> 
> ■ComicsBunch第36・37合并特刊(8月20日・27日号)发售  
> 「Angel Heart」第140話  
> 【穏やかな夢】  
>   
> 
>   
>   
> 
### ●2004/07/30(金)
> 
> ■ComicsBunch第35号(8月13日号)发售  
> 「Angel Heart」第139話  
> 【冴子のお返し】  
>   
> 
>   
>   
> 
### ●2004/07/23(金)
> 
> ■ComicsBunch第34号(8月6日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2004/07/15(木)
> 
> ■ComicsBunch第33号(7月30日号)发售  
> 「Angel Heart」第138話  
> 【都会の座敷童】  
> ▽下周、34号暂停  
>   
> 
>   
>   
> 
### ●2004/07/09(金)
> 
> ■ComicsBunch第32号(7月23日号)发售  
> 「Angel Heart」第137話  
> 【冴子と謎の女の子】  
>   
> 
>   
>   
> 
### ●2004/07/02(金)
> 
> ■ComicsBunch第31号(7月16日号)发售  
> 「Angel Heart」第136話  
> 【高畑のお守(まも)り】  
>   
> 
>   
>   
> 
### ●2004/06/25(金)
> 
> ■ComicsBunch第30号(7月9日号)发售  
> 「Angel Heart」第135話  
> 【生きた証】  
>   
> 
>   
>   
> 
### ●2004/06/18(金)
> 
> ■ComicsBunch第29号(7月2日号)发售  
> 「Angel Heart」第134話  
> 【生きて…！】  
>   
> 
>   
>   
> 
### ●2004/06/11(金)
> 
> ■ComicsBunch第28号(6月25日号)发售  
> 「Angel Heart」第133話  
> 【心臓移植のリスク】  
>   
> 
>   
>   
> 
### ●2004/06/09(水)
> 
> ■Bunch Comics  
> 「Angel Heart」第11巻 发售  
>   
> ▽封面是Comic Bunch第27号封面的图案  
>   
> 
>   
>   
> 
### ●2004/06/04(金)
> 
> ■ComicsBunch第27号(6月18日号)发售  
> 「Angel Heart」第132話/封面＆巻頭Color  
> 【明かされた真実】  
>   
> 
>   
>   
> 
### ●2004/05/28(金)
> 
> ■ComicsBunch第26号(6月11日号)  
> 「Angel Heart」暂停。  
> ▽27号系封面＆巻頭Color  
>   
> 
>   
>   
> 
### ●2004/05/21(金)
> 
> ■ComicsBunch第25号(6月4日号)发售  
> 「Angel Heart」第131話  
> 【高畑の嘘】  
> ▽26号系暂停。27号封面＆巻頭Color  
>   
> 
>   
>   
> 
### ●2004/05/14(金)
> 
> ■ComicsBunch第24号(5月28日号)发售  
> 「Angel Heart」第130話  
> 【綾音(あーや)の嘘】  
>   
> 
>   
>   
> 
### ●2004/05/06(木)
> 
> ■ComicsBunch第23号(5月21日号)发售  
> 「Angel Heart」第129話  
> 【姉(さおり)の想い】  
>   
> 
>   
>   
> 
### ●2004/04/23(金)
> 
> ■ComicsBunch第21・22合并特刊(5月7日・14日号)发售  
> 「Angel Heart」第128話  
> 【心臓(こころ)の共振】  
>   
> 
>   
>   
> 
### ●2004/04/16(金)
> 
> ■ComicsBunch第20号(4月30日号)发售  
> 「Angel Heart」第127話  
> 【心臓(こころ)の声を信じて】  
>   
> 
>   
>   
> 
### ●2004/04/09(金)
> 
> ■ComicsBunch第19号(4月23日号)发售  
> 「Angel Heart」第126話  
> 【白蘭からの手紙】  
>   
> 
>   
>   
> 
### ●2004/04/02(金)
> 
> ■ComicsBunch第18号(4月16日号)发售  
> 「Angel Heart」第125話  
> 【受け継がれし命】  
>   
> 
>   
>   
> 
### ●2004/03/26(金)
> 
> ■ComicsBunch第17号(4月9日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2004/03/19(金)
> 
> ■ComicsBunch第16号(4月2日号)发售  
> 「Angel Heart」第124話  
> 【生きる糧】  
>   
> 
>   
>   
> 
### ●2004/03/12(金)
> 
> ■ComicsBunch第15号(3月26日号)发售  
> 「Angel Heart」第123話  
> 【悲しみの決行日】  
>   
> 
>   
>   
> 
### ●2004/03/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第10巻 发售  
>   
> ▽封面是Comic Bunch'03年第52号扉页画  
>   
> 
>   
>   
> 
### ●2004/03/05(金)
> 
> ■ComicsBunch第14号(3月19日号)发售  
> 「Angel Heart」第122話  
> 【白蘭の決意】  
>   
> 
>   
>   
> 
### ●2004/02/27(金)
> 
> ■ComicsBunch第13号(3月12日号)发售  
> 「Angel Heart」第121話  
> 【初めての愛情】  
>   
> 
>   
>   
> 
### ●2004/02/20(金)
> 
> ■ComicsBunch第12号(3月5日号)发售  
> 「Angel Heart」第120話  
> 【神から授かりし子】  
>   
> 
>   
>   
> 
### ●2004/02/13(金)
> 
> ■ComicsBunch第11号(2月27日号)发售  
> 「Angel Heart」第119話/封面  
> 【スコープの中の真意】  
>   
> 
>   
>   
> 
### ●2004/02/06(金)
> 
> ■ComicsBunch第10号(2月20日号)发售  
> 「Angel Heart」第118話  
> 【白蘭の任務】  
>   
> 
>   
>   
> 
### ●2004/01/30(金)
> 
> ■ComicsBunch第9号(2月13日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2004/01/23(金)
> 
> ■ComicsBunch第8号(2月6日号)发售  
> 「Angel Heart」第117話  
> 【二人の変化】  
>   
> 
>   
>   
> 
### ●2004/01/16(金)
> 
> ■ComicsBunch第7号(1月30日号)发售  
> 「Angel Heart」第116話  
> 【運命の再会】  
>   
> 
>   
>   
> 
### ●2004/01/08(木)
> 
> ■ComicsBunch新年6号(1月23日号)发售  
> 「Angel Heart」第115話  
> 【聖夜の奇跡】  
>   
> 
>   
>   
> 
### ●2004/01/06(火)
> 
> ■Da Vinci 2月号 发售  
> →定价450日元  
> ▽Comics Da Vinci 「北条 司 その尽きない魅力」（译注：「北条司 无尽的魅力」）  
>   
> 
>   
>   
> 
### ●2004/01/05(月)
> 
> ■日経Entertainment！2月号 发售  
> →定价500日元  
> ▽【対談】北条司×飯島愛  
> 「Cat's Eye」「City Hunter」  
> 「Angel Heart」誕生秘話  
>   
> 
>   
>   

## 2003年（译注）
### ●2003/12/25(木)
> 
> ■ComicsBunch新年4・5合并特刊(1月13日・1月16日号)发售  
> 「Angel Heart」第114話/封面  
> 【恋人同士の誤解】  
> ▽Premium付録・AH特製Comics Cover   
> ▽下期(新年6号)系1月8日发售  
>   
> 
>   
>   
> 
### ●2003/12/19(金)
> 
> ■ComicsBunch新年3号(1月9日号)发售  
> 「Angel Heart」第113話  
> 【ベイビートラブル！】  
> ▽下期(12/25发售)、AH特製Book Cover(B6)付録  
>   
> 
>   
>   
> 
### ●2003/12/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第9巻 发售  
>   
> ▽封面是Comic Bunch第31号扉页画  
>   
> 
>   
>   
> 
### ●2003/12/05(金)
> 
> ■ComicsBunch新年1・2合并特刊(12月26日・1月2日号)发售  
> 「Angel Heart」第112話  
> 【依頼人はひったくり！？】  
> ▽下期(新年3号)系12月19日发售  
>   
> 
>   
>   
> 
### ●2003/11/28(金)
> 
> ■ComicsBunch第52号(12月12日号)发售  
> 「Angel Heart」第111話/封面＆巻頭Color  
> 【早百合の旅立ち】  
>   
> 
>   
>   
> 
### ●2003/11/20(木)
> 
> ■ComicsBunch第51号(12月5日号)  
> 「Angel Heart」暂停。  
> ▽下期(52号)系AH封面＆巻頭Color  
>   
> 
>   
>   
> 
### ●2003/11/14(金)
> 
> ■ComicsBunch第50号(11月28日号)发售  
> 「Angel Heart」第110話  
> 【一途(バカ)な男】  
>   
> 
>   
>   
> 
### ●2003/11/07(金)
> 
> ■ComicsBunch第49号(11月21日号)发售  
> 「Angel Heart」第109話  
> 【獠からの依頼】  
>   
> 
>   
>   
> 
### ●2003/10/30(木)
> 
> ■ComicsBunch第48号(11月14日号)发售  
> 「Angel Heart」第108話/封面  
> 【Ｃ・Ｈの正体】  
>   
> 
>   
>   
> 
### ●2003/10/24(金)
> 
> ■ComicsBunch第47号(11月7日号)发售  
> 「Angel Heart」第107話  
> 【槇村の決意】  
>   
> 
>   
>   
> 
### ●2003/10/17(金)
> 
> ■ComicsBunch第46号(10月31日号)发售  
> 「Angel Heart」第106話  
> 【小さな願い】  
>   
> 
>   
>   
> 
### ●2003/10/09(木)
> 
> ■ComicsBunch第45号(10月24日号)发售  
> 「Angel Heart」第105話  
> 【思い出の街】  
>   
> 
>   
>   
> 
### ●2003/10/03(金)
> 
> ■ComicsBunch第44号(10月17日号)发售  
> 「Angel Heart」第104話  
> 【奇跡の適合性】  
>   
> 
>   
>   
> 
### ●2003/09/26(金)
> 
> ■ComicsBunch第43号(10月10日号)发售  
> 「Angel Heart」第103話  
> 【新宿の天使】  
>   
> 
>   
>   
> 
### ●2003/09/19(金)
> 
> ■ComicsBunch第42号(10月3日号)发售  
> 「Angel Heart」第102話  
> 【心臓(かおり)の反応】  
>   
> 
>   
>   
> 
### ●2003/09/11(木)
> 
> ■ComicsBunch第41号(9月26日号)发售  
> 「Angel Heart」第101話  
> 【妹は幸せだった？】  
>   
> 
>   
>   
> 
### ●2003/09/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第8巻 发售  
> ▽封面是Comic Bunch第19号封面  
>   
> →超豪華！額装色紙Present  
> 北条先生签名的彩色纸（有获奖者的名字）25名/失望奖明信片套装500名  
> 截止日期为10月8日（周三）；详情见単行本帯 
>   
>   
>   
> 
>   
>   
> 
### ●2003/09/05(金)
> 
> ■ComicsBunch第40号(9月19日号)发售  
> 「Angel Heart」第100話/封面・巻頭Color  
> 【妹を捜して！】  
> ▽AH Quo Card ・T-shirt Present etc...  
>   
> 
>   
>   
> 
### ●2003/08/29(金)
> 
> ■ComicsBunch第39号(9月12日号)  
> 「Angel Heart」暂停。  
> ▽下期系AH封面＆巻頭Color  
>   
> 
>   
>   
> 
### ●2003/08/22(金)
> 
> ■ComicsBunch第38号(9月5日号)发售  
> 「Angel Heart」第99話  
> 【恋する人の気持ち】  
> ▽下期系暂停。40号系新展開巻頭Color  
>   
> 
>   
>   
> 
### ●2003/08/08(金)
> 
> ■ComicsBunch第36・37合并特刊(8月22日・29日号)发售  
> 「Angel Heart」第98話  
> 【初恋、涙の別れ】  
> ▽豪華特別付録・AH第1話Full Color収録  
>   
> 
>   
>   
> 
### ●2003/08/01(金)
> 
> ■ComicsBunch第35号(8月15日号)发售  
> 「Angel Heart」第97話/封面  
> 【幸せな笑顔】  
> ▽下期Bunch、AH第1話48P以Full Color完全収録  
>   
> 
>   
>   
> 
### ●2003/07/25(金)
> 
> ■ComicsBunch第34号(8月8日号)发售  
> 「Angel Heart」第96話  
> 【遠い約束】  
>   
> 
>   
>   
> 
### ●2003/07/17(木)
> 
> ■ComicsBunch第33号(8月1日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2003/07/11(金)
> 
> ■ComicsBunch第32号(7月25日号)发售  
> 「Angel Heart」第95話  
> 【私、恋してます】  
> ▽下期系AH暂停。96話第34号  
>   
> 
>   
>   
> 
### ●2003/07/04(金)
> 
> ■ComicsBunch第31号(7月18日号)发售  
> 「Angel Heart」第94話/巻頭Color  
> 【父親が娘を想う気持ち】  
>   
> 
>   
>   
> 
### ●2003/06/27(金)
> 
> ■ComicsBunch第30号(7月11日号)发售  
> 「Angel Heart」第93話  
> 【恋より深い感情】  
>   
> 
>   
>   
> 
### ●2003/06/20(金)
> 
> ■ComicsBunch第29号(7月4日号)发售  
> 「Angel Heart」第92話  
> 【これが、恋？】  
>   
> 
>   
>   
> 
### ●2003/06/13(金)
> 
> ■ComicsBunch第28号(6月27日号)发售  
> 「Angel Heart」第91話  
> 【本当の表情(かお)】  
>   
> 
>   
>   
> 
### ●2003/06/09(月)
> 
> ■Bunch Comics  
> 「Angel Heart」第7巻 发售  
> ▽封面是Comic Bunch第10号封面  
>   
> →累計500万部突破記念Present  
> A奖「作者亲笔签名的报刊」50人/B奖「Clear File Set」1000人（译注：待校对）  
> 截止日期7月8日(星期二)·详情请参阅单行本带（译注：待校对）  
>   
> 
>   
>   
> 
### ●2003/06/06(金)
> 
> ■ComicsBunch第27号(6月20日号)发售  
> 「Angel Heart」第90話/封面  
> 【初めての感情】  
>   
> 
>   
>   
> 
### ●2003/05/30(金)
> 
> ■ComicsBunch第26号(6月13日号)发售  
> 「Angel Heart」第89話  
> 【遅れて来た幸せ】  
>   
> 
>   
>   
> 
### ●2003/05/23(金)
> 
> ■ComicsBunch第25号(6月6日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2003/05/16(金)
> 
> ■ComicsBunch第24号(5月30日号)发售  
> 「Angel Heart」第88話/封面  
> 【あたたかい銃弾】  
> ▽下期系AH暂停。89话将在第26期发表。  
>   
> 
>   
>   
> 
### ●2003/05/09(金)
> 
> ■ComicsBunch第23号(5月23日号)发售  
> 「Angel Heart」第87話  
> 【唯一の愛情】  
> ▽AH2周年纪念明信片2种  
>   
> 
>   
>   
> 
### ●2003/04/25(金)
> 
> ■ComicsBunch第21・22合并特刊(5月9日・16日号)发售  
> 「Angel Heart」第86話  
> 【思い出の場所】  
> ▽AH Original・tapestry、QUO Card、图书 Card Present  
>   
> 
>   
>   
> 
### ●2003/04/18(金)
> 
> ■ComicsBunch第20号(5月2日号)发售  
> 「Angel Heart」第85話  
> 【愛に飢えた悪魔】  
> ▽下周Bunch签名的AH Original Goods Present  
>   
> 
>   
>   
> 
### ●2003/04/11(金)
> 
> ■ComicsBunch第19号(4月25日号)发售  
> 「Angel Heart」第84話/封面  
> 【悪魔のエンディング】  
>   
> 
>   
>   
> 
### ●2003/04/07(月)
> 
> ■Bunch公式i-mode Site 「ｉBunch」OPEN  
> ▽AH的shooting game「Mokkori Hunter」和待机画面  
> 进入方法：iMenu→menu list→TV/广播/杂志→(5)杂志→iBunch  
> 每月：300日元  
>   
> 
>   
>   
> 
### ●2003/04/04(金)
> 
> ■ComicsBunch第18号(4月18日号)发售  
> 「Angel Heart」第83話  
> 【哀しき連続殺人犯】  
>   
> 
>   
>   
> 
### ●2003/03/28(金)
> 
> ■ComicsBunch第17号(4月11日号)发售  
> 「Angel Heart」第82話  
> 【危険な匂い】  
>   
> 
>   
>   
> 
### ●2003/03/20(木)
> 
> ■ComicsBunch第16号(4月3日号)发售  
> 「Angel Heart」第81話  
> 【心臓(かおり)の涙】  
>   
> 
>   
>   
> 
### ●2003/03/14(金)
> 
> ■ComicsBunch第15号(3月28日号)发售  
> 「Angel Heart」第80話/巻頭Color  
> 【ターゲットは香瑩】  
>   
> 
>   
>   
> 
### ●2003/03/08(土)
> 
> ■Bunch Comics  
> 「Angel Heart」第6巻 发售  
> ▽封面是Comic Bunch 2002年第44号封面  
>   
> 
>   
>   
> 
### ●2003/03/07(金)
> 
> ■ComicsBunch第14号(3月21日号)发售  
> 「Angel Heart」第79話/封面  
> 【冴子からのＸＹＺ】  
> ▽下周系AH巻頭Color  
>   
> 
>   
>   
> 
### ●2003/02/28(金)
> 
> ■ComicsBunch第13号(3月14日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2003/02/21(金)
> 
> ■ComicsBunch第12号(3月7日号)发售  
> 「Angel Heart」第78話  
> 【遺された指輪】  
> ▽第13号 AH暂停  
>   
> 
>   
>   
> 
### ●2003/02/14(金)
> 
> ■ComicsBunch第11号(2月28日号)发售  
> 「Angel Heart」第77話  
> 【本当の家族】  
>   
> 
>   
>   
> 
### ●2003/02/07(金)
> 
> ■ComicsBunch第10号(2月21日号)发售  
> 「Angel Heart」第76話/封面  
> 【もう一度あの頃に…】  
>   
> 
>   
>   
> 
### ●2003/01/31(金)
> 
> ■ComicsBunch第9号(2月14日号)发售  
> 「Angel Heart」第75話  
> 【命より大切な絆】  
>   
> 
>   
>   
> 
### ●2003/01/24(金)
> 
> ■ComicsBunch第8号(2月7日号)发售  
> 「Angel Heart」第74話  
> 【別れの五日元玉】  
>   
> 
>   
>   
> 
### ●2003/01/17(金)
> 
> ■ComicsBunch第7号(1月31日号)发售  
> 「Angel Heart」第73話  
> 【不器用な男】  
>   
> 
>   
>   
> 
### ●2003/01/10(金)
> 
> ■ComicsBunch新年6号(1月24日号)发售  
> 「Angel Heart」第72話  
> 【伝わらぬ思い】  
> ▽新春特典2003年版AH Original Calendar（1～2月）  
> →Calendar的图案是Comic Bunch2002年第36/37期合并特刊的封面  
>   
> 
>   
>   

## 2002年（译注）
### ●2002/12/27(金)
> 
> ■ComicsBunch新年4・5合并特刊(1月13日・17日号)发售  
> 「Angel Heart」第71話/封面  
> 【依頼は殺し！？】  
> ▽下期系1月10日，并附有全彩色的超大Calendar   
>   
> 
>   
>   
> 
### ●2002/12/20(金)
> 
> ■ComicsBunch新年3号(1月10日号)发售  
> 「Angel Heart」第70話  
> 【香瑩の変化】  
>   
> 
>   
>   
> 
### ●2002/12/06(金)
> 
> ■ComicsBunch新年1・2合并特刊(1月1日・3日号)发售  
> 「Angel Heart」第69話  
> 【笑顔の二人】  
> ▽下期系12月20日发售  
>   
> 
>   
>   
> 
### ●2002/11/29(金)
> 
> ■ComicsBunch第52号(12月13日号)发售  
> 「Angel Heart」第68話/巻頭Color/封面  
> 【優しい心音(おと)】  
>   
> 
>   
>   
> 
### ●2002/11/22(金)
> 
> ■ComicsBunch第51号(12月6日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2002/11/15(金)
> 
> ■ComicsBunch第50号(11月29日号)发售  
> 「Angel Heart」第67話  
> 【不公平な幸せ】  
> ▽下期系AH暂停→在第52号中恢复封面人物  
>   
> 
>   
>   
> 
### ●2002/11/09(土)
> 
> ■Bunch Comics  
> 「Angel Heart」第5巻 发售  
> ▽封面是Comic Bunch第31号封面  
>   
> 
>   
>   
> 
### ●2002/11/08(金)
> 
> ■ComicsBunch第49号(11月22日号)发售  
> 「Angel Heart」第66話  
> 【天使の心】  
>   
> 
>   
>   
> 
### ●2002/11/01(金)
> 
> ■ComicsBunch第48号(11月15日号)发售  
> 「Angel Heart」第65話/封面  
> 【香瑩の決意】  
>   
> 
>   
>   
> 
### ●2002/10/25(金)
> 
> ■ComicsBunch第47号(11月8日号)发售  
> 「Angel Heart」第64話  
> 【突きつけられた真実】  
>   
> 
>   
>   
> 
### ●2002/10/18(金)
> 
> ■ComicsBunch第46号(11月1日号)发售  
> 「Angel Heart」第63話  
> 【夢を守る！】  
>   
> 
>   
>   
> 
### ●2002/10/11(金)
> 
> ■ComicsBunch第45号(10月25日号)发售  
> 「Angel Heart」第62話  
> 【媽媽の心臓】  
>   
> 
>   
>   
> 
### ●2002/10/04(金)
> 
> ■ComicsBunch第44号(10月18日号)发售  
> 「Angel Heart」第61話/封面  
> 【孤独な少女】  
>   
> 
>   
>   
> 
### ●2002/09/27(金)
> 
> ■ComicsBunch第43号(10月11日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2002/09/20(金)
> 
> ■ComicsBunch第42号(10月4日号)发售  
> 「Angel Heart」第60話  
> 【心の傷】  
>   
> 
>   
>   
> 
### ●2002/09/13(金)
> 
> ■ComicsBunch第41号(9月27日号)发售  
> 「Angel Heart」第59話/封面  
> 【C・Hが犯人！？】  
>   
> 
>   
>   
> 
### ●2002/09月/上旬～11月末为止 
> 
> ■新宿「MY CITY」 6楼的山下书店设立了[「XYZ 伝言板BOX」](http://www.netlaputa.ne.jp/~arcadia/ah/xyzbox.html)  
> ▽征集想让CH解决的独特委托、有趣委托  
> ▽优秀的委托被作品采用，还有亲笔签名的单行本礼物  
> ▽用配备的特制Postcard投递  
> →Card图案是以Bunch的第22/23号合并特刊封面为蓝本  
>   
> 
>   
>   
> 
### ●2002/09/06(金)
> 
> ■ComicsBunch第40号(9月20日号)发售  
> 「Angel Heart」第58話  
> 【親子の絆】  
> ▽特別付録CH等手机用贴纸   
>   
> 
>   
>   
> 
### ●2002/08/30(金)
> 
> ■ComicsBunch第39号(9月13日号)发售  
> 「Angel Heart」第57話  
> 【押しかけC・H】  
>   
> 
>   
>   
> 
### ●2002/08/23(金)
> 
> ■ComicsBunch第38号(9月6日号)发售  
> 「Angel Heart」第56話/巻頭Color  
> 【二匹の野良犬】  
> ▽特別付録AH手机用贴纸    
>   
> 
>   
>   
> 
### ●2002/08/09(金)
> 
> ■ComicsBunch第36・37合并特刊(8月23日・8月30日号)发售  
> 「Angel Heart」第55話/封面  
> 【槇村兄妹】  
> ▽下期系8月23日发售（有AH封面人物和手机贴纸） （译注：待校对）    
>   
> 
>   
>   
> 
### ●2002/08/02(金)
> 
> ■ComicsBunch第35号(8月16日号)发售  
> 「Angel Heart」第54話  
> 【夢の中の出会い】  
>   
> 
>   
>   
> 
### ●2002/07/26(金)
> 
> ■ComicsBunch第34号(8月9日号)发售  
> 「Angel Heart」第53話  
> 【冴子の誕生日】  
> ▽Color企划「北条司 ANIME EXPO 2002 Report in L.A.」  
>   
> 
>   
>   
> 
### ●2002/07/19(金)
> 
> ■ComicsBunch第33号(8月2日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2002/07/16(火)～08/31(土)
> 
> ■吉祥寺[BOOKS  Ruhe](http://www.books-ruhe.co.jp/)で原哲夫／北条司原画展  
> ▽楼梯平台的空间  
> ▽为CH、AH和其他购买漫画书的人抽选签名和Original Goods的Present     
>   
> 
>   
>   
> 
### ●2002/07/12(金)
> 
> ■ComicsBunch第32号(7月26日号)发售  
> 「Angel Heart」第52話  
> 【香との会話】  
>   
> 
>   
>   
> 
### ●2002/07/09(火)
> 
> ■Bunch Comics  
> 「Angel Heart」第4巻 发售  
> ▽封面是Comic Bunch第18号封面  
>   
> 
>   
>   
> 
### ●2002/07/05(金)
> 
> ■ComicsBunch第31号(7月19日号)发售  
> 「Angel Heart」第51話/封面  
> 【風船のクマさん】  
>   
> 
>   
>   
> 
### ●2002/06/28(金)
> 
> ■ComicsBunch第30号(7月12日号)发售  
> 「Angel Heart」第50話  
> 【狙撃準備完了】  
>   
> 
>   
>   
> 
### ●2002/06/21(火)
> 
> ■ComicsBunch第29号(7月5日号)发售  
> 「Angel Heart」第49話  
> 【ターニャの本当の笑顔】  
>   
> 
>   
>   
> 
### ●2002/06/14(火)
> 
> ■ComicsBunch第28号(6月28日号)发售  
> 　「Angel Heart」第48話  
> 　【パパの似顔絵】  
>   
> 
>   
>   
> 
### ●2002/06/07(金)
> 
> ■ComicsBunch第27号(6月21日号)发售  
> 「Angel Heart」第47話/封面  
> 【パパを捜して！！】  
>   
> 
>   
>   
> 
### ●2002/05/31(金)
> 
> ■ComicsBunch第26号(6月14日号)发售  
> 「Angel Heart」第46話  
> 【獠とベンジャミンと女社長】  
>   
> 
>   
>   
> 
### ●2002/05/24(金)
> 
> ■ComicsBunch第25号(6月7日号)发售  
> 「Angel Heart」第45話  
> 【殺し屋の習性】  
>   
> 
>   
>   
> 
### ●2002/05/17(金)
> 
> ■ComicsBunch第24号(5月31日号)发售  
> 「Angel Heart」第44話  
> 【依頼人第一号】  
> ▽从今天起，Comic Bunch<font color=#ff0000>每周五</font>发售
> 
>   
>   
> 
### ●2002/04/30(火)
> 
> ■ComicsBunch第22・23合并特刊(5月14日・5月21日号)发售  
> 「Angel Heart」第43話/封面  
> 【新宿の洗礼】  
> ▽下期起，Comics Bunch每周五发售  
> 
>   
>   
> 
### ●2002/04/23(火)
> 
> ■ComicsBunch第21号(5月7日号)发售  
> 「Angel Heart」第42話  
> 【おしゃれ】  
>   
> 
>   
>   
> 
### ●2002/04/16(火)
> 
> ■ComicsBunch第20号(4月30日号)发售  
> 「Angel Heart」第41話  
> 【陳老人の店】  
> ▽CenterColor特別企划「在新宿捕获GH！！」  
>   
> 
>   
>   
> 
### ●2002/04/09(火)
> 
> ■ComicsBunch第19号(4月23日号)发售  
> 「Angel Heart」第40話  
> 【親不孝】  
>   
> 
>   
>   
> 
### ●2002/04/02(火)
> 
> ■ComicsBunch第18号(4月16日号)发售  
> 「Angel Heart」第39話  
> 【李大人からの贈り物】  
>   
> 
>   
>   
> 
### ●2002/03/26(火)
> 
> ■ComicsBunch第17号(4月9日号)发售  
> 「Angel Heart」第38話  
> 【おかえり】  
>   
> 
>   
>   
> 
### ●2002/03/19(日)
> 
> ■ComicsBunch第16号(4月2日号)发售  
> 「Angel Heart」第37話  
> 【影のパーパ】  
>   
> 
>   
>   
> 
### ●2002/03/12(火)
> 
> ■ComicsBunch第15号(3月26日号)发售  
> 「Angel Heart」第36話/巻頭Color  
> 【船上の出会いと別れ】  
>   
> 
>   
>   
> 
### ●2002/03/08(金)
> 
> ■Bunch Comics  
> 「Angel Heart」第3巻 发售  
> ▽封面是Comic Bunch新年7・8合并特刊AH扉页画  
>   
> 
>   
>   
> 
### ●2002/02/26(火)
> 
> ■Comics Bunch第13号(3月12日号)发售  
> 「Angel Heart」第35話  
> 【パパと娘の初デート】  
> ▽…下期（14号）系AH暂停  
>   
> 
>   
>   
> 
### ●2002/02/19(火)
> 
> ■Comics Bunch第12号(3月5日号)发售  
> 「Angel Heart」第34話/封面  
> 【退院】  
>   
> 
>   
>   
> 
### ●2002/02/12(火)
> 
> ■Comics Bunch第11号(2月26日号)发售  
> 「Angel Heart」第33話  
> 【一年ぶりの衝撃】  
> ▽…下期（12号）系AH封面  
>   
> 
>   
>   
> 
### ●2002/02/05(火)
> 
> ■Comics Bunch第10号(2月19日号)发售 「Angel Heart」第32話  
> 【パーパ】（译注：帕帕。待校对）  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2002/01/29(火)
> 
> ■Comics Bunch第9号(2月12日号)发售  
> 「Angel Heart」第31話  
> 【最後の決着(ケリ)】  
>   
> 
>   
>   
> 
### ●2001/01/15(火)
> 
> ■ComicsBunch新年7・8合并特刊(1月29日・2月5日号)发售  
> 「Angel Heart」第30話/巻頭Color  
> 【失われた名前】  
>   
> 
>   
>   
> 
### ●2002/01/04(金)
> 
> ■ComicsBunch新年5・6合并特刊(1月16日・1月22日号)发售  
> 「Angel Heart」第29話  
> 【李大人の制裁】  
> ▽下期，AH将是巻頭Color  
>   
> 
>   
>   
## 2001年（译注）
### ●2001/12/18(火)
> 
> ■ComicsBunch新年3・4合并特刊(1月10日・1月15日号)发售  
> 「Angel Heart」第28話  
> 【命にかえてでも】  
>   
> 
>   
>   
> 
### ●2001/12/16(日)～2002/01/31(木)
> 
> ■吉祥寺[BOOKS  Ruhe](http://www.books-ruhe.co.jp/)でCoamix原画展  
> ▽在楼梯平台的空间里（译注：待校对）  
> ▽「Angel Heart」等、「蒼天の拳」「251」等 
>   
> 
>   
>   
> 
### ●2001/12/11(火)
> 
> ■ComicsBunch新年2号(1月8日号)发售  
> 「Angel Heart」第27話  
> 【訓練生時代の想い出】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/12/08(土)
> 
> ■Bunch Comics  
> 「Angel Heart」第2巻 发售。定价505日元+税  
> ▽封面是Comic Bunch第20号封面  
>   
> 
>   
>   
> 
### ●2001/12/04(火)
> 
> ■ComicsBunch新年1号(1月1日号)发售  
> 「Angel Heart」第26話  
> 【惜しみない命】  
>   
> 
>   
>   
> 
### ●2001/11/27(火)
> 
> ■ComicsBunch第29号(12月11日号)发售  
> 「Angel Heart」第25話  
> 【甦った過去】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/11/20(火)
> 
> ■ComicsBunch第28号(12月4日号)  
> 「Angel Heart」暂停。  
>   
> 
>   
>   
> 
### ●2001/11/13(火)
> 
> ■ComicsBunch第27号(11月27日号)发售  
> 「Angel Heart」第24話  
> 【忘れていたもの】  
> ▽下回系第29号  
>   
> 
>   
>   
> 
### ●2001/11/06(火)
> 
> ■ComicsBunch第26号(11月20日号)发售  
> 「Angel Heart」第23話  
> 【戦士たちの絆】  
>   
> 
>   
>   
> 
### ●2001/10/30(火)
> 
> ■ComicsBunch第25号(11月13日号)发售  
> 「Angel Heart」第22話  
> 【生き続ける理由】  
>   
> 
>   
>   
> 
### ●2001/10/16(火)
> 
> ■ComicsBunch第23・24合并特刊(10月30日・11月6日号)发售  
> 「Angel Heart」第21話/巻頭Color  
> 【戦士の決意】  
> ▽附带AH特别宽幅海报  
> 　→AH漫画第1卷的封面图案  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
>   
> 
>   
>   
> 
### ●2001/10/09(火)
> 
> ■Bunch Comics「Angel Heart」第1巻 发售。定价505日元+税  
>   
> ■ComicsBunch第22号(10月23日号)发售  
> 「Angel Heart」第20話  
> 【新宿で宣戦布告！！】  
> ▽下周的合并号有opening color和宽幅彩色海报（译注：待校对）  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
>   
> 
>   
>   
> 
### ●2001/10/02(火)
> 
> ■ComicsBunch第21号(10月16日号)发售  
> 「Angel Heart」第19話  
> 【ミステリアスな獠】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/09/25(火)
> 
> ■ComicsBunch第20号(10月9日号)发售  
> 「Angel Heart」第18話  
> 【グラスハートのときめき】  
> ▽「听漫」Drama CD、Comics书带的应征券等Present  
> ▽[Coamix HP](http://www.coamix.co.jp/)＆[北条司公式Homepage](http://www.hojo-tsukasa.com/)、Comics发售情報更新  
>   
> 
>   
>   
> 
### ●2001/09/18(火)
> 
> ■ComicsBunch第19号(10月2日号)  
> 「Angel Heart」暂停  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/09/11(火)
> 
> ■ComicsBunch第18号(9月25日号)发售  
> 「Angel Heart」第17話  
> 【運命の対面】  
> ▽Comics第1巻→10月9日(火)发售決定  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/09/04(火)
> 
> ■ComicsBunch第17号(9月18日号)发售  
> 「Angel Heart」第16話  
> 【出生の秘密】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/08/28(火)
> 
> ■ComicsBunch第16号(9月11日号)发售  
> 「Angel Heart」第15話  
> 【李兄弟との宿運】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/08/21(火)
> 
> ■ComicsBunch第15号(9月4日号)发售  
> 「Angel Heart」第14話  
> 【衝撃のフラッシュバック】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/08/07(火)
> 
> ■ComicsBunch第13・14合并特刊(8月21日・28日号)发售  
> 「Angel Heart」第13話/巻頭Color  
> 【緊迫する新宿】  
> ▽AH手机的待机图像在测验中 免费DL（8月20日止） 
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/08/02(木)
> 
> ■「Angel Heart」总集篇 发售決定  
> ▽收录第1話～第9話  
> ▽定价290日元  
>   
> 
>   
>   
> 
### ●2001/07/31(火)
> 
> ■ComicsBunch第12号(8月14日号)发售  
> 「Angel Heart」第12話  
> 【衝撃を超えた真実】  
> ▽下期13+14合并号系AH巻頭Color  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/07/28(土)
> 
> ・NHK BS2 (22時15分～23時09分)『[週刊BookReview](http://www.nhk.or.jp/book/)』  
> ▽关于Coamix的漫画制作文件的mini专题片。  
> ▽有对北条司的Interview。  
>   
> 
>   
>   
> 
### ●2001/07/24(火)
> 
> ■ComicsBunch第11号(8月7日号)发售  
> 「Angel Heart」第11話  
> 【事実への昂揚】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/07/17(火)
> 
> ■ComicsBunch第10号(7月31日号)发售  
> 「Angel Heart」第10話  
> 【こころとの対話】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/07/10(火)
> 
> ■ComicsBunch第9号(7月24日号)发售  
> 「Angel Heart」第9話  
> 【衝撃の邂逅】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/07/03(火)
> 
> ■ComicsBunch第8号(7月17日号)发售  
> 「Angel Heart」第8話/封面  
> 【出会いと言う名の再会】  
> ▽[Coamix HP](http://www.coamix.co.jp/)＆[北条司公式Homepage](http://www.hojo-tsukasa.com/)、网站在中午时分重新启动!    
>   
> 
>   
>   
> 
### ●2001/06/26(火)
> 
> ■ComicsBunch第7号(7月10日号)发售  
> 「Angel Heart」第7話/巻頭Color  
> 【掲示板への助走】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/06/25(月)
> 
> ・新潮社より发售の『波』7月号（100日元）に「ComicsBunch Interview」が。  
> ▽根岸忠氏、原哲夫氏、北条司氏の Interview掲載。  
> ▽还可查看[新潮社website【Ｗｅｂ新潮】](http://www.webshincho.com/)。  
> （[Mary](mailto:rosemary@badgirl.co.jp)提供的信息。谢谢你m(__)m）  
>   
> 
>   
>   
> 
### ●2001/06/24(日)
> 
> ・在朝日电视台系「Scoop 21」复活的名作漫画的特辑中，推出Coamix如何创刊Comic Bunch的纪录片。由神谷明旁白，北条氏评论。  
>   
> 
>   
>   
> 
### ●2001/06/19(火)
> 
> ■ComicsBunch第6号(7月3日号)发售  
> 「Angel Heart」第6話  
> 【思い出との再会】の巻  
> ▽下周，AH将成为巻頭Color！  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> 
>   
>   
> 
### ●2001/06/12(火)
> 
> ■ComicsBunch第5号(6月26日号)发售  
> 「Angel Heart」封面/第5話・巻頭Color  
> 【思い出に導かれて】の巻  
>   
> 
>   
>   
> 
### ●2001/06/05(火)
> 
> ■ComicsBunch第4号(6月19日号)发售  
> 「Angel Heart」第4話  
> 【新宿で大暴れ！】の巻  
>   
> ▽下周，AH封面&卷首彩页！  
> ▽可以在6月14日之前在[Coamix HP](http://www.coamix.co.jp/)收听「听漫」！  
>   
> 
>   
>   
> 
### ●2001/05/31(木)
> 
> ・朝日新聞的文化・娱乐版关于Comic Bunch等里的Hero复活的話題。  
> 　「…漫画Hero归来」为题、「**Angel Heart**」中獠说「今后会扮演重要的角色」。 
>   
> 
>   
>   
> 
### ●2001/05/29(火)
> 
> ■ComicsBunch創刊3号(6月12日号)发售  
> 「Angel Heart」封面/第3話  
> 【あの人に会いたくて】の巻  
>   
> 
>   
>   
> 
### ●2001/05/26(土)
> 
> ・读卖新闻晚间版有一篇介绍Comic Bunch等新漫画杂志的文章。 「『City Hunter』的真的复活」。  
>   
> 
>   
>   
> 
### ●2001/05/22(火)
> 
> ■ComicsBunch創刊2号(6月5日号)发售→54万部完売  
> 「Angel Heart」第2話  
> 【絶叫する夢の記憶】の巻  
>   
> 
>   
>   
> 
### ●2001/05/15(火)
> 
> ■週刊ComicsBunch創刊！(5月29日号)→72万部完売  
> 北条司新連載「Angel Heart」第1話  
> 【運命に揺れる暗殺者】の巻  
>   
> ▽新潮社Information 03-3269-4800，神谷明「听漫」的介绍。 「蒼天の拳」的有声剧（至18日）。  
> ▽「Comics Bunch」TV CM放映中！
> 
>   
>   
> 
### ●2001/04/下旬
> 
> ■週刊Comics Bunch創刊0号  
> ・在书店和便利店等地开始分发(免费)。  
>   
> ▽在评论「Angel Heart」时，北条说「~这个美丽的主人公将如何行动，她将来会做什么？ 这是我真正期待的事情。 我可能会被意想不到的发展吓一跳（笑）。」  
>   
> 
>   
>   
> 
### ●2001/04/11(水)
> 
> ■新連載は「Angel Heart」！  
> （Bunch World B6版 Comics「CITY HUNTER Vol.8 [気になるあいつ！編]」从通知中得知）  
>   
> 
>   
>   
> 
### ●2001/04/04(水)
> 
> ■週刊ComicsBunchは5月15日周二創刊！  
> （Bunch World B6版 Comics「CAT'S EYE Vol.1 [セクシーダイナマイトギャルズ編]」从通知中得知）  
>   
> 
>   
>   

## 2000年（译注） 
### ●2000/12/26(火)
> 
> ■[「北条司公式Homepage」](http://www.hojo-tsukasa.com/)Open！  
>   
> 
>   
>   
> 
### ●2000/09/12(火)
> 
> ・新连载的内容是City Hunter这一点是肯定的，编辑部正在考虑各种适合新连载的题材。  
>   
> 感谢Coamix的编辑部的持田様先生给我们写信提供这些信息。m(__)m  
>   
> 
>   
>   
> 
### ●2000/09/11(月)
> 
> ■新潮社的新Comics杂志的信息  
> ・根据新潮社Comics编辑部的答复，北条的连载已经决定，是「City Hunter」的现代的、大人版。第一期于2001年5月出版。  
> （[うぉんてっどBBS(http://www.tcup5.com/533/arcadia.html)](../cgi/533/readme.md)泄漏的信息？）  
>   
> 
>   
>   
> 
### ●2000/08/08(火)
> 
> ・据悉，新潮社正计划在明年春天之前推出一本漫画周刊，该杂志以「Coamix」为基础，由前「周刊少年Jump」主编堀江信彦、漫画家原哲夫、北条司、声优神谷明共同创立。 这本新的漫画杂志将在8月底前命名，预计创刊时发行量约为50万册。  
> （摘自《产经新闻》文章）  
>   
> 
>   
>   
>   
> 
> 如果您有任何与AH相关的最新消息，请务必告诉我们。
> 
> お名前(译注：尊姓大名)　　　　  
> メールアドレス(译注：MailAdress)  
> ●情報● (译注：信息) 

---

ver.010619

● [back](./jmc2.md) ●
