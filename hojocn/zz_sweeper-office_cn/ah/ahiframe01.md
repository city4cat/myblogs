http://www.netlaputa.ne.jp/~arcadia/ah/ahiframe01.html

**→[最新的信息](./ahiframe.md)**（译注：2009年～2013年1月的信息）  
  
（译注：以下是2001年5月～2008年的信息）  

**▼2001年～2008年的信息**  
  
## 2008年（译注） 

■12/26(金)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第316話  
　【冴子の覚悟】  
  
■12/19(金)  
　Comic Bunch新年3号(1月9日号)发售  
　「**Angel Heart**」第315話/封面  
　【仕組まれた黙秘】  
　▽特別附录・AH 专用 Book Cover  
  
■12/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第28巻 发售  
　▽封面是Comic Bunch第39号Color扉页的图案  
  
■12/05(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第314話  
　【怪我の功名】  
　▽特別附录2009年Bunch 专用 Calendar  
　→11月・12月是AH  
  
■11/28(金)  
　Comic Bunch第52号(12月12日号)发售  
　「**Angel Heart**」第313話  
　【長い一日】  
　▽AH Original Zippo(一次2種)  
　　Bunch在移动SHOP中确定发售  
　→详见12/9、Site上发布  
  
■11/21(金)  
　Comic Bunch第51号(12月5日号)发售  
　「**Angel Heart**」第312話  
　【影武者】  
  
■11/14(金)  
　Comic Bunch第50号(11月28日号)发售  
　「**Angel Heart**」暂停。  
  
■11/07(金)  
　Comic Bunch第49号(11月21日号)发售  
　「**Angel Heart**」第311話  
　【宣戦布告】  
  
■10/31(金)  
　Comic Bunch第48号(11月14日号)发售  
　「**Angel Heart**」第310話  
　【真実(マコト)】  
  
■10/24(金)  
　Comic Bunch第47号(11月7日号)发售  
　「**Angel Heart**」第309話  
　【離れていても】  
  
■10/17(金)  
　Comic Bunch第46号(10月31日号)发售  
　「**Angel Heart**」第308話  
　【新婚旅行】  
  
■10/10(金)  
　Comic Bunch第45号(10月24日号)发售  
　「**Angel Heart**」暂停。  
  
■10/03(金)  
　Comic Bunch第44号(10月17日号)发售  
　「**Angel Heart**」第307話  
　【誓いの言葉】  
  
■09/26(金)  
　Comic Bunch第43号(10月10日号)发售  
　「**Angel Heart**」第306話/封面  
　【幸せのベール】  
　▽「Cat's Eye」Pachislo特集  
  
■09/19(金)  
　Comic Bunch第42号(10月3日号)发售  
　「**Angel Heart**」第305話  
　【確かな鼓動】  
  
■09/12(金)  
　Comic Bunch第41号(9月26日号)发售  
　「**Angel Heart**」暂停。  
  
■09/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第27巻 发售  
　▽封面是Comic Bunch第32号封面的图案  
  
■09/05(金)  
　Comic Bunch第40号(9月19日号)发售  
　「**Angel Heart**」第304話/封面  
　【似た者同士】  
  
■08/29(金)  
　Comic Bunch第39号(9月12日号)发售  
　「**Angel Heart**」第303話/封面/巻頭Color  
　【金木犀の香り】  
  
■08/22(金)  
　Comic Bunch第38号(9月5日号)发售  
　「**Angel Heart**」第302話  
　【身代わり】  
  
■08/08(金)  
　Comic Bunch第36・37合并特刊 发售  
　「**Angel Heart**」第301話  
　【潜入】  
  
■08/01(金)  
　Comic Bunch第35号(8月15日号)发售  
　「**Angel Heart**」第300話  
　【恋の季節】  
  
■07/25(金)  
　Comic Bunch第34号(8月8日号)发售  
　「**Angel Heart**」暂停。  
  
■07/18(金)  
　Comic Bunch第33号(8月1日号)发售  
　「**Angel Heart**」第299話  
　【陰影】  
  
■07/11(金)  
　Comic Bunch第32号(7月25日号)发售  
　「**Angel Heart**」第298話/封面  
　【幸せの涙】  
  
■07/04(金)  
　Comic Bunch第31号(7月18日号)发售  
　「**Angel Heart**」第297話  
　【親子の時間】  
  
■06/27(金)  
　Comic Bunch第30号(7月11日号)发售  
　「**Angel Heart**」暂停。  
  
■06/20(金)  
　Comic Bunch第29号(7月4日号)发售  
　「**Angel Heart**」第296話  
　【本当の姿】  
  
■06/13(金)  
　Comic Bunch第28号(6月27日号)发售  
　「**Angel Heart**」第295話  
　【最期の言葉】  
　▽AH Event報告→委托内容的部分介绍  
  
■06/06(金)  
　Comic Bunch第27号(6月20日号)发售  
　「**Angel Heart**」第294話  
　【懺悔と供養】  
　▽AH1500万部突破記念Event報告  
  
■05/30(金)  
　Comic Bunch第26号(6月13日号)发售  
　「**Angel Heart**」暂停。  
  
■05/23(金)  
　Comic Bunch第25号(6月6日号)发售  
　「**Angel Heart**」第293話  
　【傲慢】  
　▽Comics联动・AH 专用 frame申请用紙  
  
■05/16(金)  
　Comic Bunch第24号(5月30日号)发售  
　「**Angel Heart**」第292話  
　【笑い泣き】  
　▽Comics联动・AH 专用 frame申请用紙  
　▽創刊7周年記念・Bunch複製原画集Present  
  
■05/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第26巻 发售  
　▽封面是Comic Bunch'07年第24号封面的图案  
　▽该杂志联动AH Fan 感恩节・第2弾  
　→「AH Pins ＆ 专用 frame」申请券  
  
■05/09(金)  
　Comic Bunch第23号(5月23日号)发售  
　「**Angel Heart**」番外編/封面  
　【一夜の友情】  
　→「新宿鮫」大沢在昌Special 合作  
　▽AH累計1500万部突破記念号  
　▽TOP3 CROSS TALK  
　→北条司×井上雄彦×梅澤春人  
　▽特別附录・AH 专用 图册  
　▽Event「Memory of X・Y・Z」详情  
　▽Comics联动・AH 专用 frame申请用紙  
　▽手机Bunch申请者全員無料Present  
　→AH 专用 手机防偷窥贴纸  
　▽AH Giclee 版画 Collection限量版销售  
  
■04/25(金)  
　Comic Bunch第21・22合并特刊 发售  
　「**Angel Heart**」第291話  
　【罪悪感】  
　▽北条司先生Sign会決定(5/17)  
　▽5月中旬开始在新宿举行Event  
　→「Memory of X・Y・Z」  
　▽下期(5/9发售)是AH Comics  
　累計1500万部突破記念号  
　→北条司大特集 SPECIAL2大企划  
  
■04/18(金)  
　Comic Bunch第20号(5月2日号)发售  
　「**Angel Heart**」第290話  
　【損な役回り】  
  
■04/11(金)  
　Comic Bunch第19号(4月25日号)发售  
　「**Angel Heart**」第289話/封面  
　【ロスタイム】  
  
■04/04(金)  
　Comic Bunch第18号(4月18日号)发售  
　「**Angel Heart**」第288話  
　【家族のために】  
  
■03/28(金)  
　Comic Bunch第17号(4月11日号)发售  
　「**Angel Heart**」第287話  
　【小さなＸＹＺ】  
  
■03/21(金)  
　Comic Bunch第16号(4月4日号)发售  
　「**Angel Heart**」暂停。  
　▽下期、新的发展正在到来  
  
■03/14(金)  
　Comic Bunch第15号(3月28日号)发售  
　「**Angel Heart**」第286話  
　【潮騒】  
  
■03/07(金)  
　Comic Bunch第14号(3月21日号)发售  
　「**Angel Heart**」第285話  
　【不安な夜】  
  
■02/29(金)  
　Comic Bunch第13号(3月14日号)发售  
　「**Angel Heart**」第284話  
　【ジンクス】  
  
■02/22(金)  
　Comic Bunch第12号(3月7日号)发售  
　「**Angel Heart**」暂停。  
　▽第25巻联动AH Fan 感恩节・申请用紙  
  
■02/15(金)  
　Comic Bunch第11号(2月29日号)发售  
　「**Angel Heart**」第283話  
　【灯(あかり)】  
　▽第25巻联动AH Fan 感恩节・申请用紙  
  
■02/09(土)  
　★Bunch Comics  
　「**Angel Heart**」第25巻 发售  
　▽封面是Comic Bunch'07年第50号封面的图案  
　▽该杂志联动AH Fan 感恩节・ 专用  Pins 申请券  
  
■02/08(金)  
　Comic Bunch第10号(2月22日号)发售  
　「**Angel Heart**」第282話/封面  
　【カメレオン動く】  
　▽AH Fan 感恩节・申请者全員Service  
　→第25巻联动、AH 专用Pins 5件套装  
  
■02/01(金)  
　Comic Bunch第9号(2月15日号)发售  
　「**Angel Heart**」第281話  
　【ぷるん】  
  
■01/25(金)  
　Comic Bunch第8号(2月8日号)发售  
　「**Angel Heart**」第280話  
　【母の想い】  
  
■01/18(金)  
　Comic Bunch新年7号(2月1日号)发售  
　「**Angel Heart**」第279話  
　【一億の勝負】  
　▽Bunch全部作品T-shirt新春Present  
　→在Design Garden自由创作  
  
■01/10(木)  
　Comic Bunch新年6号(1月31日号)发售  
　「**Angel Heart**」第278話  
　【最凶悪危険人物】  
　▽Sign彩纸等、新年礼物  
  
## 2007年（译注）  

■12/27(木)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第277話/封面  
　【虎の尾】  
　▽北条司全方位特集Special Interview  
　▽特別附录illustrated.Book「ライアのまど」（译注：ライア：Lyra/Riah。まど：Mado/窗户）  
　▽CH JOURNAL：Pachislo CH  
　→围板、Poster等Present  
  
■12/21(金)  
　Comic Bunch新年3号(1月14日号)发售  
　「**Angel Heart**」暂停。  
　▽下期是27日发售、北条司特集  
　→Interview＆特別附录「ライアのまど」  
  
■12/07(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第276話  
　【招かれざる客】  
　▽CH単巻Release記念・CH JOURNAL  
　▽下期休載。4・5合并特刊是北条司全方位专题  
　illustrated.Book「ライアのまど」Present  
  
■11/30(金)  
　Comic Bunch第53号(12月14日号)发售  
　「**Angel Heart**」第275話  
　【カメレオンの罠】  
  
■11/22(木)  
　Comic Bunch第52号(12月7日号)发售  
　「**Angel Heart**」第274話  
　【葉月の悩み】  
  
■11/16(金)  
　Comic Bunch第51号(11月30日号)发售  
　「**Angel Heart**」第273話  
　【親子の愛】  
  
■11/09(金)  
　Comic Bunch第50号(11月23日号)发售  
　「**Angel Heart**」第272話/封面  
　【大好きなんです】  
  
■11/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第24巻 发售  
　▽封面是Comic Bunch第30号Color扉页的图案  
　→问卷调查中有北条先生亲笔彩纸Present  
  
■11/02(金)  
　Comic Bunch第49号(11月16日号)发售  
　「**Angel Heart**」暂停。  
　▽下期是AH封面  
  
■10/26(金)  
　Comic Bunch第48号(11月9日号)发售  
　「**Angel Heart**」第271話  
　【キバナコスモス】  
  
■10/19(金)  
　Comic Bunch第47号(11月2日号)发售  
　「**Angel Heart**」第270話  
　【ファルコンの秘密】  
  
■10/12(金)  
　Comic Bunch第46号(10月26日号)发售  
　「**Angel Heart**」第269話  
　【墓参り】  
　▽CH単巻DVD发售決定  
　→12/19起毎月计划Release(全26巻)  
  
■10/05(金)  
　Comic Bunch第45号(10月19日号)发售  
　「**Angel Heart**」第268話/封面  
　【ファルコンのお守り】  
  
■09/28(金)  
　Comic Bunch第44号(10月12日号)发售  
　「**Angel Heart**」第267話  
　【葉月の父】  
  
■09/21(金)  
　Comic Bunch第43号(10月5日号)发售  
　「**Angel Heart**」暂停。  
  
■09/14(金)  
　Comic Bunch第42号(9月28日号)发售  
　「**Angel Heart**」第266話  
　【誤解】  
  
■09/07(金)  
　Comic Bunch第41号(9月21日号)发售  
　「**Angel Heart**」第265話  
　【海坊主と葉月】  
  
■08/31(金)  
　Comic Bunch第40号(9月14日号)发售  
　「**Angel Heart**」第264話  
　【夏の再会】  
  
■08/24(金)  
　Comic Bunch第39号(9月7日号)发售  
　「**Angel Heart**」第263話  
　【私の立ち位置】  
  
■08/10(金)  
　Comic Bunch第37・38合并特刊 发售  
　「**Angel Heart**」第262話  
　【仁志の涙】  
　▽Bunch創刊300号記念Present  
　→AHSign彩纸・北条先生推荐的红葡萄酒&白葡萄酒  
  
■08/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第23巻 发售  
　▽封面是Comic Bunch新年1・2合并特刊扉絵的图案  
  
■08/03(金)  
　Comic Bunch第36号(8月17日号)发售  
　「**Angel Heart**」第261話  
　【武道館決戦】  
  
■07/27(金)  
　Comic Bunch第35号(8月10日号)发售  
　「**Angel Heart**」暂停。  
  
■07/20(金)  
　Comic Bunch第34号(8月3日号)发售  
　「**Angel Heart**」第260話  
　【消えない記憶】  
  
■07/13(金)  
　Comic Bunch第33号(7月27日号)发售  
　「**Angel Heart**」第259話  
　【香瑩の怒り】  
  
■07/06(金)  
　Comic Bunch第32号(7月20日号)发售  
　「**Angel Heart**」第258話  
　【及第点の答え】  
  
■06/29(金)  
　Comic Bunch第31号(7月13日号)发售  
　「**Angel Heart**」第257話  
　【陳さんの暴走】  
  
■06/22(金)  
　Comic Bunch第30号(7月6日号)发售  
　「**Angel Heart**」第256話/封面/巻頭Color  
　【新スポンサー】  
  
■06/15(金)  
　Comic Bunch第29号(6月29日号)发售  
　「**Angel Heart**」暂停。  
　▽下周30号是AH封面＆巻頭Color  
  
■06/08(金)  
　Comic Bunch第28号(6月22日号)发售  
　「**Angel Heart**」第255話  
　【仲間】  
  
■06/01(金)  
　Comic Bunch第27号(6月15日号)发售  
　「**Angel Heart**」第254話  
　【嫉妬】  
  
■05/25(金)  
　Comic Bunch第26号(6月8日号)发售  
　「**Angel Heart**」第253話  
　【表への一歩】  
  
■05/18(金)  
　Comic Bunch第25号(6月1日号)发售  
　「**Angel Heart**」暂停。  
　▽Comics联动、DVD BOX4 Present申请券(15名)  
  
■05/11(金)  
　Comic Bunch第24号(5月25日号)发售  
　「**Angel Heart**」第252話/封面  
　【初めての経験】  
　▽Comics联动、DVD BOX4 Present申请券(15名)  
  
■05/09(水)  
　★Bunch Comics 　「**Angel Heart**」第22巻 发售  
　▽封面是Comic Bunch新年5・6合并特刊封面的图案  
　→DVD BOX4Present申请券(该杂志联动/15名)  
  
■04/27(金)  
　Comic Bunch第22・23合并特刊 发售  
　「**Angel Heart**」第251話  
　【意外な弱点】  
  
■04/20(金)  
　Comic Bunch第21号(5月4日号)发售  
　「**Angel Heart**」第250話  
　【暖かい場所へ】  
  
■04/13(金)  
　Comic Bunch第20号(4月27日号)发售  
　「**Angel Heart**」第249話  
　【見えない景色】  
  
■04/06(金)  
　Comic Bunch第19号(4月20日号)发售  
　「**Angel Heart**」第248話  
　【気づかぬ優しさ】  
  
■03/30(金)  
　Comic Bunch第18号(4月13日号)发售  
　「**Angel Heart**」暂停。  
  
■03/23(金)  
　Comic Bunch第17号(4月6日号)发售  
　「**Angel Heart**」第247話  
　【悲痛な願い】  
  
■03/16(金)  
　Comic Bunch第16号(3月30日号)发售  
　「**Angel Heart**」第246話  
　【雲のように】  
  
■03/09(金)  
　Comic Bunch第15号(3月23日号)发售  
　「**Angel Heart**」第245話  
　【初アフター是ストーカー付！】  
　▽Yahoo AH联动企划  
　→实物大小100t Hammer、Yahoo Auction出品  
  
■03/02(金)  
　Comic Bunch第14号(3月16日号)发售  
　「**Angel Heart**」第244話  
　【恋是アフターで！】  
　▽Yahoo AH联动企划→什么是智囊？  
  
■02/23(金)  
　Comic Bunch第13号(3月9日号)发售  
　「**Angel Heart**」第243話  
　【覚悟】  
　▽Yahoo 联动企划・AH Baton活用術 (译注：Baton：接力棒)   
  
■02/16(金)  
　Comic Bunch第12号(3月2日号)发售  
　「**Angel Heart**」第242話  
　【香瑩の答え】  
　▽AH×Yahoo!JAPAN联动企划  
　Angel Heart  Special OPEN  
  
■02/09(金)  
　Comic Bunch第11号(2月23日号)发售  
　「**Angel Heart**」暂停。  
　▽AH×Yahoo!JAPAN联动企划  
　→2/16,Yahoo!動画AH特集Page登場  
　1.Yahoo!Auction Surprise Item出品  
　2.AH第1話、話題的新Media上阅读  
　3.Yahoo!人气Corner也有北条先生参加  
  
■02/02(金)  
　Comic Bunch第10号(2月16日号)发售  
　「**Angel Heart**」第241話  
　【狙撃】  
　▽绝密Project進行中・详情见下期    
  
■01/26(金)  
　Comic Bunch第9号(2月9日号)发售  
　「**Angel Heart**」第240話  
　【カメレオンの誘惑】  
　▽Comics联动、DVD BOX3 Present申请券(15名)  
  
■01/19(金)  
　Comic Bunch第8号(2月2日号)发售  
　「**Angel Heart**」第239話  
　【乙玲(イーリン)の姉】  
　▽Comics联动、DVD BOX3 Present申请券(15名)  
  
■01/12(金)  
　Comic Bunch新年7号(1月31日号)发售  
　「**Angel Heart**」第238話  
　【花園学校の未来】  
　▽Comics联动、DVD BOX3 Present申请券(15名)  
  
■01/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第21巻 发售  
　▽封面是Comic Bunch'06年第47号封面的图案  
　▽包含书带的初版限量版780日元  
　→しら乌鸦＆１００ｔ Hammer  
　▽DVD BOX3Present申请券(该杂志联动/15名)  
  
## 2006年（译注）

■12/28(木)  
　Comic Bunch新年5・6合并特刊 发售  
　「**Angel Heart**」第237話/封面  
　【カメレオン】  
　▽巻頭Color企划・有奖的AH测试  
　▽特別附录・AH Comics Cover   
　第21巻是1/9发售→初版限定版 附有专用书带   
　▽Comics联动、DVD BOX3 Present申请券(15名)  
  
■12/22(金)  
　Comic Bunch新年4号(1月19日号)发售  
　「**Angel Heart**」第236話  
　【危険な出会い】  
　▽下期5・6合并特刊的发售是12月28日(木)  
　→下期是AH封面、AH Comics Cover 附录  
　＆AH审定召开  
  
■12/15(金)  
　Comic Bunch新年3号(1月12日号)发售  
　「**Angel Heart**」第235話  
　【肉弾戦】  
  
■12/01(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第234話/巻頭Color  
　【いい子の行き着く場所】  
  
■11/24(金)  
　Comic Bunch第52号(12月8日号)发售  
　「**Angel Heart**」暂停。  
　▽下期、新年1・2合并特刊是AH巻頭Color  
  
■11/17(金)  
　Comic Bunch第51号(12月1日号)发售  
　「**Angel Heart**」第233話  
　【嫌な予感】  
  
■11/10(金)  
　Comic Bunch第50号(11月24日号)发售  
　「**Angel Heart**」暂停。  
  
■11/02(木)  
　Comic Bunch第49号(11月17日号)发售  
　「**Angel Heart**」第232話  
　【机男と親衛隊(シンエータイ)！】  
  
■10/27(金)  
　Comic Bunch第48号(11月10日号)发售  
　「**Angel Heart**」第231話  
　【ボクら、シャンイン親衛隊！？】  
  
■10/20(金)  
　Comic Bunch第47号(11月3日号)发售  
　「**Angel Heart**」第230話/封面  
　【ラブレター・パニック！】  
  
■10/13(金)  
　Comic Bunch第46号(10月27日号)发售  
　「**Angel Heart**」第229話  
　【想い、街に息づいて】  
  
■10/06(金)  
　Comic Bunch第45号(10月20日号)发售  
　「**Angel Heart**」第228話  
　【私たちの学校】  
  
■09/29(金)  
　Comic Bunch第44号(10月13日号)发售  
　「**Angel Heart**」第227話  
　【香瑩、入学す！】  
　▽动画AH News、Chief P 諏訪的评论  
  
■09/25(月) 读卖TV/25:25～  
■09/26(火) 日本TV/25:59～  
　动画**Angel Heart** #50  
　「Last Present」  
　→最終回  
　▽DVD BOX2, Soundtrack CD Present  
  
■09/22(金)  
　Comic Bunch第43号(10月6日号)发售  
　「**Angel Heart**」第226話  
　【真実と紗世】  
　▽动画AH News、最終話提要和要点  
  
■09/18(月) 读卖TV/25:25～  
■09/19(火) 日本TV/25:29～  
　动画**Angel Heart** #49  
　「Get My Life」  
　→下回「Last Present」  
  
■09/15(金)  
　Comic Bunch第42号(9月29日号)发售  
　「**Angel Heart**」暂停。  
  
■09/11(月) 读卖TV/24:55～  
■09/12(火) 日本TV/25:29～  
　动画**Angel Heart** #48  
　「引き寄せられる運命」  
　→下回「Get My Life」  
  
■09/09(土)  
　★Bunch Comics  
　「**Angel Heart**」第20巻 发售  
　▽封面是Comic Bunch第34号封面的图案  
  
■09/08(金)  
　Comic Bunch第41号(9月22日号)发售  
　「**Angel Heart**」第225話/封面  
　【父のいる店】  
  
■09/04(月) 读卖TV/24:55～  
■09/05(火) 日本TV/26:59～  
　动画**Angel Heart** #47  
　「アカルイミライ！？」  
　→下回「引き寄せられる運命」  
  
■09/01(金)  
　Comic Bunch第40号(9月15日号)发售  
　「**Angel Heart**」暂停。  
  
■08/28(月) 读卖TV/25:25～  
■08/29(火) 日本TV/25:29～  
　动画**Angel Heart** #46  
　「マザー・ハート」  
　→下回「アカルイミライ！？」  
  
■08/25(金)  
　Comic Bunch第39号(9月8日号)发售  
　「**Angel Heart**」第224話  
　【重ねる嘘、重なる罪】  
  
■08/21(月) 读卖TV/25:10～  
■08/22(火) 日本TV/26:14～  
　动画**Angel Heart** #45  
　「人間核弾頭、楊(ヤン)」  
　→下回「マザー・ハート」  
  
■08/14(月) 读卖TV/24:55～  
■08/15(火) 日本TV/25:29～  
　动画**Angel Heart** #44  
　「俺たちの子供のために」  
　→下回「人間核弾頭、楊(ヤン)」  
  
■08/11(金)  
　Comic Bunch第37・38合并特刊 发售  
　「**Angel Heart**」第223話  
　【紗世是名探偵！？】  
　▽动画AH News、第44話简介  
　▽7/29 天保山AH Event的情况   
  
■08/07(月) 读卖TV/25:14～  
■08/08(火) 日本TV/25:29～  
　动画**Angel Heart** #43  
　「私が生きる日常」  
　→下回「俺たちの子供のために」  
  
■08/04(金)  
　Comic Bunch第36号(8月18日号)发售  
　「**Angel Heart**」第222話  
　【カギ是香瑩にあり！？】  
　▽动画AH News、第43話提要和要点  
　→新OP Theme CD Present(20名)  
  
■07/31(月) 读卖TV/25:00～  
■08/01(火) 日本TV/25:29～  
　动画**Angel Heart** #42  
　「二人だけのSign」  
　→下回「私が生きる日常」  
  
■07/28(金)  
　Comic Bunch第35号(8月11日号)发售  
　「**Angel Heart**」第221話  
　【１８歳是シゲキがお好き！？】  
　▽动画AHNews、第42話提要和要点  
　▽令人兴奋的宝岛AH活动通知  
  
■07/24(月) 读卖TV/25:09～  
■07/25(火) 日本TV/25:38～  
　动画**Angel Heart** #41  
　「自分の居場所」  
　→下回「二人だけのSign」  
  
■07/21(金)  
　Comic Bunch第34号(8月4日号)发售  
　「**Angel Heart**」第220話/封面  
　【謎のお父さん！】  
　▽紧急通知・令人兴奋的宝岛 AH Event  
　→7/29(土)13:00～  
　由一位出色的音乐家现场演奏＆  
　川崎真央Talk Show举办  
  
■07/17(月) 读卖TV/25:04～  
■07/18(火) 日本TV/25:38～  
　动画**Angel Heart** #40  
　「ミキの隠された秘密」  
　→下回「自分の居場所」  
  
■07/14(金)  
　Comic Bunch第33号(7月28日号)发售  
　「**Angel Heart**」暂停。  
  
■07/10(月) 读卖TV/25:25～  
■07/11(火) 日本TV/25:29～  
　动画**Angel Heart** #39  
　「依頼者是大女優」  
　→下回「ミキの隠された秘密」  
  
■07/07(金)  
　Comic Bunch第32号(7月21日号)发售  
　「**Angel Heart**」第219話  
　【私がパパです！】  
　▽动画AHNews、第39話提要和要点  
  
■07/03(月) 读卖TV/25:25～  
■07/04(火) 日本TV/25:29～  
　动画**Angel Heart** #38  
　「オレの目になってくれ」  
　→下回「依頼者是大女優」  
  
■06/30(金)  
　Comic Bunch第31号(7月14日号)发售  
　「**Angel Heart**」第218話  
　【大幸運期到来！！】  
  
■06/26(月) 读卖TV/24:55～  
■06/27(火) 日本TV/25:29～  
　动画**Angel Heart** #37  
　「汚れのない心」  
　→下回「オレの目になってくれ」  
  
■06/23(金)  
　Comic Bunch第30号(7月7日号)发售  
　「**Angel Heart**」第217話  
　【楊の生きる理由】  
　▽动画AHNews、第37話提要和要点  
　▽天宝山10天令人兴奋的宝島AH Event  
　→7/28(金)～8/6(日)  
  
■06/19(月) 读卖TV/25:16～  
■06/20(火) 日本TV/25:29～  
　动画**Angel Heart** #36  
　「幸せを運ぶ女の子」  
　→下回「汚れのない心」  
  
■06/16(金)  
　Comic Bunch第29号(6月30日号)发售  
　「**Angel Heart**」第216話  
　【追う女、待つ女】  
　▽动画AH News、第36話提要  
　→Comics联动・DVD等Present(150名)申请券  
  
■06/12(月) 读卖TV/24:55～  
■06/13(火) 日本TV/25:29～  
　动画**Angel Heart** #35  
　「未来へ…」  
　→下回「幸せを運ぶ女の子」  
  
■06/09(金)  
　Comic Bunch第28号(6月23日号)发售  
　「**Angel Heart**」第215話/封面  
　【Ｃ・Ｈ資格テスト！】  
　▽巻頭Color企划、結成AH応援団  
　→Comics联动・DVD等Present(150名)申请券  
　▽动画AH News、第35話提要  
  
■06/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第19巻 发售  
　▽封面是Comic Bunch第22・23合并特刊封面的图案  
　→AH DVD BOX2等Present(150名)申请券  
  
■06/05(月) 读卖TV/24:55～  
■06/06(火) 日本TV/25:29～  
　动画**Angel Heart** #34  
　「二人の決意」  
　→下回「未来へ…」  
  
■06/02(金)  
　Comic Bunch第27号(6月16日号)发售  
　「**Angel Heart**」第214話  
　【オペレーション ナシクズシ！】  
  
■05/29(月) 读卖TV/24:55～  
■05/30(火) 日本TV/25:29～  
　动画**Angel Heart** #33  
　「神から授かりし子」  
　→下回「二人の決意」  
  
■05/26(金)  
　Comic Bunch第26号(6月9日号)发售  
　「**Angel Heart**」暂停。  
  
■05/22(月) 读卖TV/24:55～  
■05/23(火) 日本TV/25:29～  
　动画**Angel Heart** #32  
　「組織から来た女」  
　→下回「神から授かりし子」  
  
■05/19(金)  
　Comic Bunch第25号(6月2日号)发售  
　「**Angel Heart**」第213話  
　【男の性(サガ)！】  
　▽动画AH News、第32話提要和要点  
  
■05/15(月) 读卖TV/24:55～  
■05/16(火) 日本TV/25:29～  
　动画**Angel Heart** #31  
　「最後の夜に見た奇跡」  
　→下回「組織から来た女」  
  
■05/12(金)  
　Comic Bunch第24号(5月26日号)发售  
　「**Angel Heart**」第212話  
　【長生きの代償】  
　▽創刊5周年企划・这部Bunch漫画太精彩了！  
　→AH最新动画化情報＆令人难忘的委托Best5  
　▽动画AH News、第30話提要和要点  
  
■05/08(月) 读卖TV/24:55～  
■05/09(火) 日本TV/25:59～  
　动画**Angel Heart** #30  
　「この街是私の全て」  
　→下回「最後の夜に見た奇跡」  
  
■05/01(月) 读卖TV/24:55～  
■05/02(火) 日本TV/25:29～  
　动画**Angel Heart** #29  
　「私の妹…香」  
　→下回「この街是私の全て」  
  
■04/28(金)  
　Comic Bunch第22・23合并特刊 发售  
　「**Angel Heart**」第211話/封面  
　【男二人、車中にて】  
　▽特別附录AHAH袋装Premium Postcard  
　▽动画AH News、第29話提要和要点  
  
■04/24(月) 读卖TV/24:55～  
■04/25(火) 日本TV/25:29～  
　动画**Angel Heart** #28  
　「約束」  
　→下回「私の妹…香」  
  
■04/21(金)  
　Comic Bunch第21号(5月5日号)发售  
　「**Angel Heart**」第210話  
　【マオの決断】  
　▽动画AH News、第28話提要和要点  
  
■04/17(月) 读卖TV/25:25～  
■04/18(火) 日本TV/25:29～  
　动画**Angel Heart** #27  
　「私、恋してる！？」  
　→下回「約束」  
  
■04/14(金)  
　Comic Bunch第20号(4月28日号)发售  
　「**Angel Heart**」第209話  
　【迸る想い！】  
　▽动画AH News、第27話提要  
　▽玉置浩二 Special Interview  
　→新OP「Lion」CD Present(20名)  
  
■04/10(月) 读卖TV/25:30～  
■04/11(火) 日本TV/25:29～  
　动画**Angel Heart** #26  
　「もう一度あの頃に」  
　→下回「私、恋してる！？」  
  
■04/07(金)  
　Comic Bunch第19号(4月21日号)发售  
　「**Angel Heart**」暂停。  
  
■04/03(月) 读卖TV/25:30～  
■04/04(火) 日本TV/25:49～  
　动画**Angel Heart** #25  
　「死にたがる依頼者」  
　→下回「もう一度あの頃に」  
  
■03/31(金)  
　Comic Bunch第18号(4月14日号)发售  
　「**Angel Heart**」第208話  
　【海辺の告白！】  
　▽动画AHNews、第25話提要  
　▽宇都宮隆×川崎真央 特別対談  
　→Signed AH 专用 Clear File Present  
  
■03/27(月) 读卖TV/25:03～  
■03/28(火) 日本TV/26:25～  
　动画**Angel Heart** #24  
　「鼓動と共に…」  
　→下回「死にたがる依頼者」  
  
■03/24(金)  
　Comic Bunch第17号(4月7日号)发售  
　「**Angel Heart**」暂停。  
  
■03/20(月) 读卖TV/25:29～  
■03/21(火) 日本TV/25:25～  
　动画**Angel Heart** #23  
　「出発(たびだち)のメロディー」  
　→下回「鼓動と共に…」  
  
■03/17(金)  
　Comic Bunch第16号(3月31日号)发售  
　「**Angel Heart**」第207話  
　【酒と泪(なみだ)と皇子と母親！？】  
　▽动画AH News、第23話提要  
　▽Comics联动企划・DVD BOX Present申请券  
  
■03/13(月) 读卖TV/25:03～  
■03/14(火) 日本TV/25:25～  
　动画**Angel Heart** #22  
　「不公平な幸せ」  
　→下回「出発(たびだち)のメロディー」  
  
■03/10(金)  
　Comic Bunch第15号(3月24日号)发售  
　「**Angel Heart**」第206話  
　【皇子の苦悩】  
　▽动画AH News、第22話提要  
　▽Comics联动企划・DVD BOX Present申请券  
  
■03/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第18巻 发售  
　▽封面是Comic Bunch第8号Color扉页的图案  
　→AH DVD BOX Present(15名)  
  
■03/06(月) 读卖TV/25:03～  
■03/07(火) 日本TV/25:25～  
　动画**Angel Heart** #21  
　「哀しき守護者(ガーディアン)」  
　→下回「不公平な幸せ」  
  
■03/03(金)  
　Comic Bunch第14号(3月17日号)发售  
　「**Angel Heart**」第205話  
　【日常に潜むアクマ】  
　▽动画AH News、第21話提要和要点  
　▽3/6(月)、动画AH手机Site Open  
　→推迟到4月以后  
  
■02/27(月) 读卖TV/25:03～  
■02/28(火) 日本TV/25:55～  
　动画**Angel Heart** #20  
　「宿命のプレリュード」  
　→下回「哀しき守護者(ガーディアン)」  
  
■02/24(金)  
　Comic Bunch第13号(3月10日号)发售  
　「**Angel Heart**」第204話  
　【家族の風景】  
　▽动画AH News、第20話提要和要点  
  
■02/20(月) 读卖TV/25:03～  
■02/21(火) 日本TV/25:25～  
　动画**Angel Heart** #19  
　「陳老人の店」  
　→下回「宿命のプレリュード」  
  
■02/17(金)  
　Comic Bunch第12号(3月3日号)发售  
　「**Angel Heart**」第203話  
　【マオの交渉】  
　▽动画AH News、第19話提要和要点  
  
■02/13(月) 读卖TV/25:03～  
■02/14(火) 日本TV/25:25～  
　动画**Angel Heart** #18  
　「親子の絆」  
　→下回「陳老人の店」  
  
■02/10(金)  
　Comic Bunch第11号(2月24日号)发售  
　「**Angel Heart**」暂停。  
  
■02/06(月) 读卖TV/27:03～  
■02/07(火) 日本TV/26:00～  
　动画**Angel Heart** #17  
　「夢の中の出会い」  
　→下回「親子の絆」  
  
■02/03(金)  
　Comic Bunch第10号(2月17日号)发售  
　「**Angel Heart**」第202話  
　【昨日の敵是今日の恋人！】  
　▽动画AH News、第17話提要和要点  
  
■01/30(月) 读卖TV/24:59～  
■01/31(火) 日本TV/25:40～  
　动画**Angel Heart** #16  
　「Ｃ・Ｈとしての資格」  
　→下回「夢の中の出会い」  
  
■01/27(金)  
　Comic Bunch第9号(2月10日号)发售  
　「**Angel Heart**」第201話  
　【皇子と香瑩の初デート(是ぁと)】  
　▽动画AH News、第16話提要和要点  
　→AH DVD Premium BOX 发售日大決定  
  
■01/23(月) 读卖TV/24:59～  
■01/24(火) 日本TV/25:25～  
　动画**Angel Heart** #15  
　「パパを捜して」  
　→下回「Ｃ・Ｈとしての資格」  
  
■01/20(金)  
　Comic Bunch第8号(2月3日号)发售  
　「**Angel Heart**」第200話/封面/巻頭Color  
　【妄想超特急がやってきた！】  
　▽动画AH News、第15話提要和要点  
  
■01/16(月) 读卖TV/24:59～  
■01/17(火) 日本TV/25:25～  
　动画**Angel Heart** #14  
　「復活Ｃ・Ｈ！」  
　→下回「パパを捜して」  
  
■01/13(金)  
　Comic Bunch第7号(1月27日号)发售  
　「**Angel Heart**」第199話  
　【異国からの求婚者！】  
　▽动画AH News、第14話提要和要点  
　→「Finally」CD 20名Present  
  
■01/09(月) 读卖TV/25:50～  
■01/10(火) 日本TV/25:55～  
　动画**Angel Heart** #13  
　「李大人からの贈り物」  
　→下回「復活Ｃ・Ｈ！」  
  
■01/06(金)  
　Comic Bunch新年6号(1月20日号)发售  
　「**Angel Heart**」第198話  
　【クリスマスPresent】  
　▽动画AH News、第13話提要和要点  
　→OP曲「Finally」CD 2/1(水)发售  
  
## 2005年（译注）

■12/22(木)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第197話  
　【指輪の記憶】  
　▽2006年春、AH动画DVD发售開始決定  
■12/19(月) 读卖TV/24:59～  
■01/03(火) 日本TV/26:04～  
　动画**Angel Heart** #12  
　「船上の出会いと別れ」  
　→下回「李大人からの贈り物」  
  
■12/16(金)  
　Comic Bunch新年3号(12月30日号)发售  
　「**Angel Heart**」第196話  
　【楊(ヤン)、再来！！】  
  
■12/15(木)  
　「**北条司**漫画家25周年記念  
　　自选Illustration 100幅」发售  
　→定价2940日元  
　▽新绘制的香莹封面・新绘制的AH Poster  
　▽ 专用 QUO Card Present  
  
■12/12(月) 读卖TV/25:59～  
■12/20(火) 日本TV/25:25～  
　动画**Angel Heart** #11  
　「父娘(おやこ)の時間」  
　→下回「船上の出会いと別れ」  
  
■12/09(金)  
　Comic Bunch増刊「**Angel Heart**総集編  
　动画＆Comics Special 」发售  
　→定价390日元  
　★Bunch Comics  
　「**Angel Heart**」第17巻 发售  
　▽封面是Comic Bunch第21号封面的图案  
  
■12/05(月) 读卖TV/25:09～  
■12/13(火) 日本TV/25:55～  
　动画**Angel Heart** #10  
　「Angel  スマイル」  
　→下回「父娘(おやこ)の時間」  
  
■12/02(金)  
　Comic Bunch新年1・2合并特刊(12月16日・23日号)发售  
　「**Angel Heart**」第195話/封面  
　【夏休みの終わり】  
　▽特別附录・AH Christmas Card  
　▽AH邮票表・Present申请要項  
　▽动画AH News、第10話提要和要点  
  
■11/28(月) 读卖TV/25:09～  
■12/06(火) 日本TV/25:35～  
　动画**Angel Heart** #9  
　「香瑩～失われた名前～」  
　→下回「Angel  スマイル」  
  
■11/25(金)  
　Comic Bunch第52号(12月9日号)发售  
　「**Angel Heart**」暂停。  
　▽动画AH News、第9話提要和要点  
　▽下期(12/2发售)AH Christmas Card特別附录  
  
■11/21(月) 读卖TV/25:19～  
■11/29(火) 日本TV/25:35～  
　动画**Angel Heart** #8  
　「真実(ホント)の仲間」  
　→下回「香瑩～失われた名前～」  
  
■11/18(金)  
　Comic Bunch第51号(12月2日号)发售  
　「**Angel Heart**」第194話  
　【親子の覚悟】  
　▽动画AHNews、第8話提要和要点  
　▽新年1・2合并特刊上AH Special 附录的预定  
■11/14(月) 读卖TV/25:28～  
■11/22(火) 日本TV/25:55～  
　动画**Angel Heart** #7  
　「俺の愛すべき街」  
　→下回「真実(ホント)の仲間」  
  
■11/11(金)  
　Comic Bunch第50号(11月25日号)发售  
　「**Angel Heart**」暂停。  
　▽涵盖从CE到AH的北条司自选插图 集  
　→徳間書店将于12月中旬发售決定  
　▽动画AH News、第7話提要和要点  
  
■11/07(月) 读卖TV/24:59～  
■11/08(火) 日本TV/放送休止  
⇒11/15(火) 日本TV/25:40～  
　动画**Angel Heart** #6  
　「再会」  
　→下回「俺の愛すべき街」  
  
■11/04(金)  
　Comic Bunch第49号(11月18日号)发售  
　「**Angel Heart**」第193話  
　【怒りの一撃！】  
　▽Angel Heart総集編 12/09(金)发售決定  
　▽动画AH News、第6話提要和要点  
  
■10/31(月) 读卖TV/24:59～  
■11/01(火) 日本TV/26:25～  
　动画**Angel Heart** #5  
　「永別(さよなら) …カオリ」  
　→下回「再会」  
  
■10/28(金)  
　Comic Bunch第48号(11月11日号)发售  
　「**Angel Heart**」第192話  
　【親子の電話】  
　▽动画AH News、第5話提要和要点  
　→「誰かが君を想ってる」CDPresent(30名)  
  
■10/24(月) 读卖TV/25:14～  
■10/25(火) 日本TV/25:25～  
　动画**Angel Heart** #4  
　「さまようHEART」  
　→下回「永別(さよなら) …カオリ」  
  
■10/21(金)  
　Comic Bunch第47号(11月4日号)发售  
　「**Angel Heart**」第191話  
　【恐怖のカーチェイス！】  
　▽动画AH News、第4話提要和要点  
　→「誰かが君を想ってる」CD 11/9(水)发售  
  
■10/17(月) 读卖TV/25:13～  
■10/18(火) 日本TV/25:34～  
　动画**Angel Heart** #3  
　「XYZの街」  
　→下回「さまようHEART」  
  
■10/14(金)  
　Comic Bunch第46号(10月28日号)发售  
　「**Angel Heart**」第190話  
　【バスジャック！】  
　▽动画AHNews、第3話提要和要点  
  
■10/10(月) 读卖TV/25:28～  
■10/11(火) 日本TV/25:49～  
　动画**Angel Heart** #2  
　「香が帰ってきた」  
　→下回「XYZの街」  
  
■10/07(金)  
　Comic Bunch第45号(10月21日号)发售  
　「**Angel Heart**」第189話/封面  
　【変質者、現る！】  
　▽动画CColor特集  
　OP/ED曲・Artist's Comment  
  
■10/03(月) 读卖TV/25:34～  
■10/04(火) 日本TV/26:25～  
　动画Angel Heart放送開始  
　动画**Angel Heart** #1  
　「ガラスの心臓 グラス・ハート」  
  
■09/30(金)  
　Comic Bunch第44号(10月14日号)发售  
　「**Angel Heart**」第188話/封面  
　【海学級へ行こう！】  
　▽动画巻頭Color特集／1話提要和要点etc.  
　→OP曲「Finally」by Sowelu  
　→ED曲「誰かが君を想ってる」by Skoop On Somebody  
　▽动画放映記念附录「CD型卓上Calendar」  
　▽「香瑩等身大Poster」全員Service申请要項  
  
■09/22(木)  
　Comic Bunch第43号(10月7日号)发售  
　「**Angel Heart**」第187話  
　【ミキが行方不明！？】  
　▽下期(44号)是2大特典付き・巻頭Color特集  
  
■09/16(金)  
　Comic Bunch第42号(9月30日号)  
　「**Angel Heart**」暂停。  
　▽动画AH放送日決定  
　读卖TV・10月3日(月)~~24:58～~~  
　日本TV・10月4日(火)~~25:25～~~  
  
■09/09(金)  
　Comic Bunch第41号(9月23日号)发售  
　「**Angel Heart**」第186話/封面  
　【変質者是海坊主！？】  
　▽动画AH News  
　→首版・人物对照表  
  
■09/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第16巻 发售  
　▽封面是Comic Bunch新年4・5合并特刊封面的图案  
  
■09/02(金)  
　Comic Bunch第40号(9月16日号)发售  
　「**Angel Heart**」第185話  
　【海坊主と先生】  
　▽潜入动画AH制作发布会  
　▽决定每周播出日  
　读卖TV・每周一24:58～  
　日本TV・每周二25:25～  
  
■08/26(金)  
　Comic Bunch第39号(9月9日号)发售  
　「**Angel Heart**」第184話  
　【義父(ちち)是つらいよ！】  
  
■08/12(金)  
　Comic Bunch第37・38合并特刊 发售  
　「**Angel Heart**」第183話  
　【麗子の未来】  
　▽CColor・动画AH速報  
　→读卖TV・日本TV今秋播出  
　→AH Digest DVD Present(100名)  
　▽13日～在银座举行CH展览  
　▽CAT'S EYE完全版・计划在10月登场  
  
■08/05(金)  
　Comic Bunch第36号(8月19日号)发售  
　「**Angel Heart**」第182話  
　【信宏が教えてくれた事】  
  
■07/29(金)  
　Comic Bunch第35号(8月12日号)  
　「**Angel Heart**」暂停。  
　▽CH COMPLETE DVD BOX Color广告  
　▽动画AH News・Poster用插图   
　▽令人兴奋的宝島宝島in天保山「**Heart of City**」  
　→7/31(日) 神谷明和川崎真央登場  
　　详情是http://www.ytv.co.jp/wakuwaku/  
　▽THE **CITY HUNTER**展 Vol.1 銀座  
　→DVD BOX・AH动画化記念／原画大公開・企划齐全  
　　(8/13～8/21 Sony大厦8楼)详见第37、38期合并号  
  
■07/22(金)  
　Comic Bunch第34号(8月5日号)发售  
　「**Angel Heart**」第181話  
　【未来を変える男】  
  
■07/14(木)  
　Comic Bunch第33号(7月29日号)发售  
　「**Angel Heart**」第180話  
　【引き寄せられる運命】  
  
■07/08(金)  
　Comic Bunch第32号(7月22日号)发售  
　「**Angel Heart**」第179話  
　【笑顔の未来】  
  
■07/01(金)  
　Comic Bunch第31号(7月15日号)发售  
　「**Angel Heart**」第178話  
　【透視の代償】  
  
■06/24(金)  
　Comic Bunch第30号(7月8日号)发售  
　「**Angel Heart**」第177話  
　【男の信念】  
  
■06/17(金)  
　Comic Bunch第29号(7月1日号)发售  
　「**Angel Heart**」第176話  
　【悲しい二人】  
  
■06/10(金)  
　Comic Bunch第28号(6月24日号)发售  
　「**Angel Heart**」第175話/封面  
　【恋人達の未来】  
　▽CColor特集「A・Hを構築する3つの世界」（译注：构建A·H的3个世界）    
　→Angel BOX的申请券  
  
■06/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第15巻 发售  
　▽封面是Comic Bunch第28号封面的图案  
　→封面画18枚Set「Angel BOX」Present申请券  
  
■06/03(金)  
　Comic Bunch第27号(6月17日号)  
　「**Angel Heart**」暂停。  
　▽下期(28号)是AH封面＆Center Color特集  
  
■05/27(金)  
　Comic Bunch第26号(6月10日号)发售  
　「**Angel Heart**」第174話  
　【麗泉(れいせん)の悩み】  
  
■05/20(金)  
　Comic Bunch第25号(6月3日号)发售  
　「**Angel Heart**」第173話  
　【アカルイミライ！？】  
  
■05/12(木)  
　Comic Bunch第24号(5月27日号)发售  
　「**Angel Heart**」第172話  
　【母なる思い】  
　▽北斗の拳・CH的Sticker＆走运千社札带  
  
■04/28(木)  
　Comic Bunch第22・23合并特刊(5月13日・20日号)发售  
　「**Angel Heart**」第171話  
　【再会…！！】  
　▽下期是5月12日(木)发售  
  
■04/22(金)  
　Comic Bunch第21号(5月6日号)发售  
　「**Angel Heart**」第170話/封面  
　【楊(ヤン)、始動！】  
　▽下期是4月28日(木)发售  
  
■04/15(金)  
　Comic Bunch第20号(4月29日号)发售  
　「**Angel Heart**」第169話  
　【黒幕現る！！】  
  
■04/08(金)  
　Comic Bunch第19号(4月22日号)发售  
　「**Angel Heart**」第168話  
　【天国と地獄！？】  
　▽动画香瑩役、川崎真央的直撃Interview  
  
■04/03(日)23時～NACK5  
■04/02(土)24時～FM愛知・FM大阪  
　ラジオ番組「**HEART OF ANGEL**」内Corner  
　「XYZ Ryo's Bar」放送スタート  
　→神谷明和美女Guest的TalkCorner  
　▽第一回目のGuest是麻上洋子和伊倉一恵  
  
■04/01(金)  
　Comic Bunch第18号(4月15日号)  
　▽动画AH声優Audition発表  
　→香瑩役声優：川崎真央(18岁)  
　「**Angel Heart**」暂停。  
  
■03/25(金)  
　Comic Bunch第17号(4月8日号)发售  
　「**Angel Heart**」第167話  
　【楊芳玉(ヤン Fan ユィ)の仕事】  
　▽下期(18号)、动画AH大特集  
　→香瑩役声優大公布/本篇暂停  
  
■03/17(木)  
　Comic Bunch第16号(4月1日号)发售  
　「**Angel Heart**」第166話  
　【愛是地球を救う！？】  
　▽动画AH News  
　潜入香瑩役声優Auditionに！  
　香瑩＆獠 线画设定定稿！  
  
　▽动画放送日時  
　日本TV系 4月22日(金)27時8分～  
　读卖TV系4月18日(月)24時58分～  
  
■03/11(金)  
　Comic Bunch第15号(3月25日号)发售  
　「**Angel Heart**」第165話  
　【楊(ヤン)からの依頼？】  
　▽动画AHNews  
　　带有天使之翼的logo决定！  
　　主要的演员阵容与CH相同！  
  
■03/04(金)  
　Comic Bunch第14号(3月18日号)发售  
　「**Angel Heart**」第164話  
　【それぞれの場所】  
  
■02/25(金)  
　Comic Bunch第13号(3月11日号)发售  
　「**Angel Heart**」第163話  
　【女同士の酒】  
　▽CColor/手机AH Game「SameBunch」攻略留言板  
　▽北斗の拳Raoh外传・2006年春改编为电影  
　→新角色悲伤女战士「Reina」角色由北条先生设计   
  
■02/18(金)  
　Comic Bunch第12号(3月4日号)发售  
　「**Angel Heart**」第162話  
　【獠の大失敗】  
  
■02/10(木)  
　Comic Bunch第11号(2月25日号)  
　「**Angel Heart**」暂停。  
  
■02/09(水)  
　★Bunch Comics  
　「**Angel Heart**」第14巻 发售  
　▽封面是Comic Bunch'03年第52号封面  
  
■02/04(金)  
　Comic Bunch第10号(2月18日号)发售  
　「**Angel Heart**」第161話  
　【恋の覚悟】  
  
■01/28(金)  
　Comic Bunch第9号(2月11日号)发售  
　「**Angel Heart**」第160話  
　【ホレた男是謎だらけ！？】  
　▽今春TV放映決定！动画AH速報第二弾  
　　CenterColor・香瑩役声優Audition详情  
  
■01/21(金)  
　Comic Bunch第8号(2月4日号)发售  
　「**Angel Heart**」第159話  
　【私、恋してます！】  
  
■01/14(金)  
　Comic Bunch第7号(1月28日号)发售  
　「**Angel Heart**」第158話  
　【愛される理由】  
　▽香瑩役声優Audition详情系第9期  
  
■01/07(金)  
　Comic Bunch新年6号(1月21日号)发售  
　「**Angel Heart**」第157話  
　【獠の手机ライフ】  
　▽香瑩役声優Audition開催決定  
  
## 2004年（译注）  

■12/24(金)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第156話/封面  
　【家族の写真】  
　▽TV动画化速報第一弾＆AH 专用 Calendar  
  
■12/16(木)  
　Comic Bunch新年3号(1月7日号)发售  
　「**Angel Heart**」第155話  
　【告白…！】  
　▽4・5合并特刊号在巻頭Color上有TV动画化的预告大专题    
  
■12/03(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第154話  
　【ジョイの行き先】  
　▽下期(新年3号)是12月16日(木)发售  
  
■11/26(金)  
　Comic Bunch第52号(12月10日号)发售  
　「**Angel Heart**」第153話  
　【親子のように】  
  
■11/19(金)  
　Comic Bunch第51号(12月3日号)  
　「**Angel Heart**」暂停。  
  
■11/12(金)  
　Comic Bunch第50号(11月26日号)发售  
　「**Angel Heart**」第152話  
　【ミキの幸せ】  
  
■11/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第13巻 发售  
　▽封面是Comic Bunch第40号封面  
  
■11/05(金)  
　Comic Bunch第49号(11月19日号)发售  
　「**Angel Heart**」第151話  
　【共同生活開始！】  
　▽AH TV动画化決定  
  
■10/29(金)  
　Comic Bunch第48号(11月12日号)发售  
　「**Angel Heart**」第150話  
　【愛しき人の形見】  
  
■10/22(金)  
　Comic Bunch第47号(11月5日号)发售  
　「**Angel Heart**」第149話  
　【思い人是この街に？】  
  
■10/15(金)  
　Comic Bunch第46号(10月29日号)发售  
　「**Angel Heart**」第148話  
　【ジョイの事情】  
  
■10/07(木)  
　Comic Bunch第45号(10月22日号)发售  
　「**Angel Heart**」第147話  
　【"危険"な大女優！】  
  
■10/01(金)  
　Comic Bunch第44号(10月15日号)发售  
　「**Angel Heart**」第146話  
　【優しき店長(マスター)】  
  
■09/24(金)  
　Comic Bunch第43号(10月8日号)  
　「**Angel Heart**」暂停。  
  
■09/16(木)  
　Comic Bunch第42号(10月1日号)发售  
　「**Angel Heart**」第145話  
　【暗闇に見えた夕陽】  
　▽下期、43号暂停  
  
■09/10(金)  
　Comic Bunch第41号(9月24日号)发售  
　「**Angel Heart**」第144話  
　【いつも一緒】  
  
■09/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第12巻 发售  
　▽封面是Comic Bunch第27号扉絵  
  
■09/03(金)  
　Comic Bunch第40号(9月17日号)发售  
　「**Angel Heart**」第143話/封面  
　【ママへの想い】  
  
■08/27(金)  
　Comic Bunch第39号(9月10日号)发售  
　「**Angel Heart**」第142話  
　【汚れなき心】  
  
■08/19(木)  
　Comic Bunch第38号(9月3日号)发售  
　「**Angel Heart**」第141話  
　【海坊主と座敷童】  
  
■08/06(金)  
　Comic Bunch第36・37合并特刊 发售  
　「**Angel Heart**」第140話  
　【穏やかな夢】  
  
■07/30(金)  
　Comic Bunch第35号(8月13日号)发售  
　「**Angel Heart**」第139話  
　【冴子のお返し】  
  
■07/23(金)  
　Comic Bunch第34号(8月6日号)  
　「**Angel Heart**」暂停。  
  
■07/15(木)  
　Comic Bunch第33号(7月30日号)发售  
　「**Angel Heart**」第138話  
　【都会の座敷童】  
　▽下周、34号暂停  
  
■07/09(金)  
　Comic Bunch第32号(7月23日号)发售  
　「**Angel Heart**」第137話  
　【冴子と謎の女の子】  
  
■07/02(金)  
　Comic Bunch第31号(7月16日号)发售  
　「**Angel Heart**」第136話  
　【高畑のお守(まも)り】  
  
■06/25(金)  
　Comic Bunch第30号(7月9日号)发售  
　「**Angel Heart**」第135話  
　【生きた証】  
  
■06/18(金)  
　Comic Bunch第29号(7月2日号)发售  
　「**Angel Heart**」第134話  
　【生きて…！】  
  
■06/11(金)  
　Comic Bunch第28号(6月25日号)发售  
　「**Angel Heart**」第133話  
　【心臓移植のリスク】  
  
■06/09(水)  
　★Bunch Comics  
　「**Angel Heart**」第11巻 发售  
　▽封面是Comic Bunch第27号封面  
  
■06/04(金)  
　Comic Bunch第27号(6月18日号)发售  
　「**Angel Heart**」第132話/封面＆巻頭Color  
　【明かされた真実】  
  
■05/28(金)  
　Comic Bunch第26号(6月11日号)  
　「**Angel Heart**」暂停。  
　▽27号是封面＆巻頭Color  
  
■05/21(金)  
　Comic Bunch第25号(6月4日号)发售  
　「**Angel Heart**」第131話  
　【高畑の嘘】  
　▽26号暂停。27号是封面＆巻頭Color  
  
■05/14(金)  
　Comic Bunch第24号(5月28日号)发售  
　「**Angel Heart**」第130話  
　【綾音(あーや)の嘘】  
  
■05/06(木)  
　Comic Bunch第23号(5月21日号)发售  
　「**Angel Heart**」第129話  
　【姉(さおり)の想い】  
  
■04/23(金)  
　Comic Bunch第21・22合并特刊 发售  
　「**Angel Heart**」第128話  
　【心臓(こころ)の共振】  
  
■04/16(金)  
　Comic Bunch第20号(4月30日号)发售  
　「**Angel Heart**」第127話  
　【心臓(こころ)の声を信じて】  
  
■04/09(金)  
　Comic Bunch第19号(4月23日号)发售  
　「**Angel Heart**」第126話  
　【白蘭からの手紙】  
  
■04/02(金)  
　Comic Bunch第18号(4月16日号)发售  
　「**Angel Heart**」第125話  
　【受け継がれし命】  
  
■03/26(金)  
　Comic Bunch第17号(4月9日号)  
　「**Angel Heart**」暂停。  
  
■03/19(金)  
　Comic Bunch第16号(4月2日号)发售  
　「**Angel Heart**」第124話  
　【生きる糧】  
  
■03/12(金)  
　Comic Bunch第15号(3月26日号)发售  
　「**Angel Heart**」第123話  
　【悲しみの決行日】  
  
■03/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第10巻 发售  
　▽封面是Comic Bunch'03年第52号扉絵  
  
■03/05(金)  
　Comic Bunch第14号(3月19日号)发售  
　「**Angel Heart**」第122話  
　【白蘭の決意】  
  
■02/27(金)  
　Comic Bunch第13号(3月12日号)发售  
　「**Angel Heart**」第121話  
　【初めての愛情】  
  
■02/20(金)  
　Comic Bunch第12号(3月5日号)发售  
　「**Angel Heart**」第120話  
　【神から授かりし子】  
  
■02/13(金)  
　Comic Bunch第11号(2月27日号)发售  
　「**Angel Heart**」第119話/封面  
　【スコープの中の真意】  
  
■02/06(金)  
　Comic Bunch第10号(2月20日号)发售  
　「**Angel Heart**」第118話  
　【白蘭の任務】  
  
■01/30(金)  
　Comic Bunch第9号(2月13日号)  
　「**Angel Heart**」暂停。  
  
■01/23(金)  
　Comic Bunch第8号(2月6日号)发售  
　「**Angel Heart**」第117話  
　【二人の変化】  
  
■01/16(金)  
　Comic Bunch第7号(1月30日号)发售  
　「**Angel Heart**」第116話  
　【運命の再会】  
  
■01/08(木)  
　Comic Bunch新年6号(1月23日号)发售  
　「**Angel Heart**」第115話  
　【聖夜の奇跡】  
  
■01/06(火)  
　Da Vinci  2月号 发售（定价450日元）  
　▽Comics Da Vinci  「北条 司 その尽きない魅力」（译注：「北条司 无尽的魅力」）  
  
■01/05(月)  
　日経Entertainment！2月号 发售（定价500日元）  
　▽【対談】北条司×飯島愛  
　「Cat's Eye」「**City Hunter**」  
　「**Angel Heart**」誕生的秘密故事  
  
## 2003年 （译注）  

■12/25(木)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第114話/封面  
　【恋人同士の誤解】  
　▽Premium附录・AH 专用 Comics Cover   
　▽下期(新年6号)是1月8日发售  
  
■12/19(金)  
　Comic Bunch新年3号(1月9日号)发售  
　「**Angel Heart**」第113話  
　【ベイビートラブル！】  
　▽下期(12/25发售)、AH 专用 Book Cover(B6)附录  
  
■12/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第9巻 发售  
　▽封面是Comic Bunch第31号扉絵  
  
■12/05(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第112話  
　【依頼人是ひったくり！？】  
　▽下期(新年3号)是12月19日发售  
  
■11/28(金)  
　Comic Bunch第52号(12月12日号)发售  
　「**Angel Heart**」第111話/封面＆巻頭Color  
　【早百合の旅立ち】  
  
■11/20(木)  
　Comic Bunch第51号(12月5日号)  
　「**Angel Heart**」暂停。  
　▽下期(52号)是AH封面＆巻頭Color  
  
■11/14(金)  
　Comic Bunch第50号(11月28日号)发售  
　「**Angel Heart**」第110話  
　【一途(バカ)な男】  
  
■11/07(金)  
　Comic Bunch第49号(11月21日号)发售  
　「**Angel Heart**」第109話  
　【獠からの依頼】  
  
■10/30(木)  
　Comic Bunch第48号(11月14日号)发售  
　「**Angel Heart**」第108話/封面  
　【Ｃ・Ｈの正体】  
  
■10/24(金)  
　Comic Bunch第47号(11月7日号)发售  
　「**Angel Heart**」第107話  
　【槇村の決意】  
  
■10/17(金)  
　Comic Bunch第46号(10月31日号)发售  
　「**Angel Heart**」第106話  
　【小さな願い】  
  
■10/09(木)  
　Comic Bunch第45号(10月24日号)发售  
　「**Angel Heart**」第105話  
　【思い出の街】  
  
■10/03(金)  
　Comic Bunch第44号(10月17日号)发售  
　「**Angel Heart**」第104話  
　【奇跡の適合性】  
  
■09/26(金)  
　Comic Bunch第43号(10月10日号)发售  
　「**Angel Heart**」第103話  
　【新宿の天使】  
  
■09/19(金)  
　Comic Bunch第42号(10月3日号)发售  
　「**Angel Heart**」第102話  
　【心臓(かおり)の反応】  
  
■09/11(木)  
　Comic Bunch第41号(9月26日号)发售  
　「**Angel Heart**」第101話  
　【妹是幸せだった？】  
  
■09/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第8巻 发售  
　▽封面是Comic Bunch第19号封面  
　→超豪華！有装裱的彩纸Present  
  
■09/05(金)  
　Comic Bunch第40号(9月19日号)发售  
　「**Angel Heart**」第100話/封面・巻頭Color  
　【妹を捜して！】  
　▽AH QUO Card T-shirt Present etc...  
  
■08/29(金)  
　Comic Bunch第39号(9月12日号)  
　「**Angel Heart**」暂停。  
　▽下期是AH封面＆巻頭Color  
  
■08/22(金)  
　Comic Bunch第38号(9月5日号)发售  
　「**Angel Heart**」第99話  
　【恋する人の気持ち】  
　▽下期暂停。40号是新展開巻頭Color  
  
■08/08(金)  
　Comic Bunch第36・37合并特刊 发售  
　「**Angel Heart**」第98話  
　【初恋、涙の別れ】  
　▽豪華特別附录・AH第1話Full Color収録  
  
■08/01(金)  
　Comic Bunch第35号(8月15日号)发售  
　「**Angel Heart**」第97話/封面  
　【幸せな笑顔】  
　▽下期Bunch、AH第1話48页以Full Color完全収録  
  
■07/25(金)  
　Comic Bunch第34号(8月8日号)发售  
　「**Angel Heart**」第96話  
　【遠い約束】  
  
■07/17(木)  
　Comic Bunch第33号(8月1日号)  
　「**Angel Heart**」暂停。  
  
■07/11(金)  
　Comic Bunch第32号(7月25日号)发售  
　「**Angel Heart**」第95話  
　【私、恋してます】  
　▽下期是AH暂停。96話是34号  
  
■07/04(金)  
　Comic Bunch第31号(7月18日号)发售  
　「**Angel Heart**」第94話/巻頭Color  
　【父親が娘を想う気持ち】  
  
■06/27(金)  
　Comic Bunch第30号(7月11日号)发售  
　「**Angel Heart**」第93話  
　【恋より深い感情】  
  
■06/20(金)  
　Comic Bunch第29号(7月4日号)发售  
　「**Angel Heart**」第92話  
　【これが、恋？】  
  
■06/13(金)  
　Comic Bunch第28号(6月27日号)发售  
　「**Angel Heart**」第91話  
　【本当の表情(かお)】  
  
■06/09(月)  
　★Bunch Comics  
　「**Angel Heart**」第7巻 发售  
　▽封面是Comic Bunch第10号封面  
　→累計500万部突破記念Present  
  
■06/06(金)  
　Comic Bunch第27号(6月20日号)发售  
　「**Angel Heart**」第90話/封面  
　【初めての感情】  
  
■05/30(金)  
　Comic Bunch第26号(6月13日号)发售  
　「**Angel Heart**」第89話  
　【遅れて来た幸せ】  
  
■05/23(金)  
　Comic Bunch第25号(6月6日号)  
　「**Angel Heart**」暂停。  
  
■05/16(金)  
　Comic Bunch第24号(5月30日号)发售  
　「**Angel Heart**」第88話/封面  
　【あたたかい銃弾】  
　▽下期是AH暂停。89話系26号发表。  
  
■05/09(金)  
　Comic Bunch第23号(5月23日号)发售  
　「**Angel Heart**」第87話  
　【唯一の愛情】  
　▽AH2周年記念Postcard 2種  
  
■04/25(金)  
　Comic Bunch第21・22合并特刊 发售  
　「**Angel Heart**」第86話  
　【思い出の場所】  
　▽AH Original ・Tapestry、QUO Card、图书Card Present  
  
■04/18(金)  
　Comic Bunch第20号(5月2日号)发售  
　「**Angel Heart**」第85話  
　【愛に飢えた悪魔】  
　▽下周Bunch带Sign的AH Original Goods Present  
  
■04/11(金)  
　Comic Bunch第19号(4月25日号)发售  
　「**Angel Heart**」第84話/封面  
　【悪魔のエンディング】  
  
■04/07(月)  
　Bunch公式i-modeSite「ｉBunch」OPEN  
　▽**AH**のGameや待受画像など  
　Access方法：iMenu→menu list→TV/广播/雑誌→(5)雑誌→ｉBunch  
　每月：300日元  
  
■04/04(金)  
　Comic Bunch第18号(4月18日号)发售  
　「**Angel Heart**」第83話  
　【哀しき連続殺人犯】  
  
■03/28(金)  
　Comic Bunch第17号(4月11日号)发售  
　「**Angel Heart**」第82話  
　【危険な匂い】  
  
■03/20(木)  
　Comic Bunch第16号(4月3日号)发售  
　「**Angel Heart**」第81話  
　【心臓(かおり)の涙】  
  
■03/14(金)  
　Comic Bunch第15号(3月28日号)发售  
　「**Angel Heart**」第80話/巻頭Color  
　【ターゲット是香瑩】  
  
■03/08(土)  
　★Bunch Comics  
　「**Angel Heart**」第6巻 发售  
　▽封面是Comic Bunch'02年第44号封面  
  
■03/07(金)  
　Comic Bunch第14号(3月21日号)发售  
　「**Angel Heart**」第79話/封面  
　【冴子からのＸＹＺ】  
　▽下周是AH巻頭Color  
  
■02/28(金)  
　Comic Bunch第13号(3月14日号)  
　「**Angel Heart**」暂停。  
  
■02/21(金)  
　Comic Bunch第12号(3月7日号)发售  
　「**Angel Heart**」第78話  
　【遺された指輪】  
　▽第13号AH休載  
  
■02/14(金)  
　Comic Bunch第11号(2月28日号)发售  
　「**Angel Heart**」第77話  
　【本当の家族】  
  
■02/07(金)  
　Comic Bunch第10号(2月21日号)发售  
　「**Angel Heart**」第76話/封面  
　【もう一度あの頃に…】  
  
■01/31(金)  
　Comic Bunch第9号(2月14日号)发售  
　「**Angel Heart**」第75話  
　【命より大切な絆】  
  
■01/24(金)  
　Comic Bunch第8号(2月7日号)发售  
　「**Angel Heart**」第74話  
　【別れの五日元玉】  
  
■01/17(金)  
　Comic Bunch第7号(1月31日号)发售  
　「**Angel Heart**」第73話  
　【不器用な男】  
  
■01/10(金)  
　Comic Bunch新年6号(1月24日号)发售  
　「**Angel Heart**」第72話  
　【伝わらぬ思い】  
　▽新春特典2003年版AH Original Calendar（1～2月）  
  
## 2002年（译注）

■12/27(金)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第71話/封面  
　【依頼是殺し！？】  
　▽下期系1月10日，并附有Calendar  
  
■12/20(金)  
　Comic Bunch新年3号(1月10日号)发售  
　「**Angel Heart**」第70話  
　【香瑩の変化】  
  
■12/06(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第69話  
　【笑顔の二人】  
　▽下期是12月20日发售  
  
■11/29(金)  
　Comic Bunch第52号(12月13日号)发售  
　「**Angel Heart**」第68話/巻頭Color/封面  
　【優しい心音(おと)】  
  
■11/22(金)  
　Comic Bunch第51号(12月6日号)  
　「**Angel Heart**」暂停。  
  
■11/15(金)  
　Comic Bunch第50号(11月29日号)发售  
　「**Angel Heart**」第67話  
　【不公平な幸せ】  
　▽下期AH休載→第52号巻頭Color重新开始  
  
■11/09(土)  
　★Bunch Comics  
　「**Angel Heart**」第5巻 发售  
　▽封面是Comic Bunch第31号封面  
  
■11/08(金)  
　Comic Bunch第49号(11月22日号)发售  
　「**Angel Heart**」第66話  
　【天使の心】  
  
■11/01(金)  
　Comic Bunch第48号(11月15日号)发售  
　「**Angel Heart**」第65話/封面  
　【香瑩の決意】  
  
■10/25(金)  
　Comic Bunch第47号(11月8日号)发售  
　「**Angel Heart**」第64話  
　【突きつけられた真実】  
  
■10/18(金)  
　Comic Bunch第46号(11月1日号)发售  
　「**Angel Heart**」第63話  
　【夢を守る！】  
  
■10/11(金)  
　Comic Bunch第45号(10月25日号)发售  
　「**Angel Heart**」第62話  
　【媽媽の心臓】  
  
■10/04(金)  
　Comic Bunch第44号(10月18日号)发售  
　「**Angel Heart**」第61話/封面  
　【孤独な少女】  
  
■09/27(金)  
　Comic Bunch第43号(10月11日号)  
　「**Angel Heart**」暂停。  
  
■09/20(金)  
　Comic Bunch第42号(10月4日号)发售  
　「**Angel Heart**」第60話  
　【心の傷】  
  
■09/13(金)  
　Comic Bunch第41号(9月27日号)发售  
　「**Angel Heart**」第59話/封面  
　【C・Hが犯人！？】  
  
■09月/上旬～11月/最后一天为止  
　新宿「MY CITY」6楼的山下書店设立了「**XYZ 伝言板BOX**」  
　▽征集想让CH解决的独特委托、有趣委托  
　▽优秀的委托被作品采用，还有亲笔签名的单行本Present  
　▽用配备的特制Postcard投递  
　→ Card系Bunch第22・23合并特刊封面的图案  
  
■09/06(金)  
　Comic Bunch第40号(9月20日号)发售  
　「**Angel Heart**」第58話  
　【親子の絆】  
　▽特別附录CH等手机用Seal  
  
■08/30(金)  
　Comic Bunch第39号(9月13日号)发售  
　「**Angel Heart**」第57話  
　【押しかけC・H】  
  
■08/23(金)  
　Comic Bunch第38号(9月6日号)发售  
　「**Angel Heart**」第56話/巻頭Color  
　【二匹の野良犬】  
　▽特別附录AH手机用Seal  
  
■08/09(金)  
　Comic Bunch第36・37合并特刊(8月23日・8月30日号)发售  
　「**Angel Heart**」第55話/封面  
　【槇村兄妹】  
　▽下期是8月23日发售（附有AH巻頭Color・手机Seal）  
  
■08/02(金)  
　Comic Bunch第35号(8月16日号)发售  
　「**Angel Heart**」第54話  
　【夢の中の出会い】  
  
■07/26(金)  
　Comic Bunch第34号(8月9日号)发售  
　「**Angel Heart**」第53話  
　【冴子の誕生日】  
　▽Color企划「北条司 ANIME EXPO 2002 Report in L.A.」  
  
■07/19(金)  
　Comic Bunch第33号(8月2日号)  
　「**Angel Heart**」暂停。  
  
■07/16(火)～08/31(土)  
　吉祥寺[BOOKS Ruhe](http://www.books-ruhe.co.jp/)原哲夫／**北条司**原画展  
　▽楼梯平台的空间  
　▽为CH、AH等Comics购买者抽选Sign彩纸和Original Goods的Present  
  
■07/12(金)  
　Comic Bunch第32号(7月26日号)发售  
　「**Angel Heart**」第52話  
　【香との会話】  
  
■07/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第4巻 发售  
　▽封面是Comic Bunch第18号封面  
  
■07/05(金)  
　Comic Bunch第31号(7月19日号)发售  
　「**Angel Heart**」第51話/封面  
　【風船のクマさん】  
  
■06/28(金)  
　Comic Bunch第30号(7月12日号)发售  
　「**Angel Heart**」第50話  
　【狙撃準備完了】  
  
■06/21(金)  
　Comic Bunch第29号(7月5日号)发售  
　「**Angel Heart**」第49話  
　【ターニャの本当の笑顔】  
  
■06/14(金)  
　Comic Bunch第28号(6月28日号)发售  
　「**Angel Heart**」第48話  
　【パパの似顔絵】  
  
■06/07(金)  
　Comic Bunch第27号(6月21日号)发售  
　「**Angel Heart**」第47話/封面  
　【パパを捜して！！】  
  
■05/31(金)  
　Comic Bunch第26号(6月14日号)发售  
　「**Angel Heart**」第46話  
　【獠とベンジャミンと女社長】  
  
■05/24(金)  
　Comic Bunch第25号(6月7日号)发售  
　「**Angel Heart**」第45話  
　【殺し屋の習性】  
  
■05/17(金)  
　Comic Bunch第24号(5月31日号)发售  
　「**Angel Heart**」第44話  
　【依頼人第一号】  
　▽今日起Comic Bunch是每周五发售  
  
■04/30(火)  
　Comic Bunch第22・23合并特刊 发售  
　「**Angel Heart**」第43話/封面  
　【新宿の洗礼】  
  
■04/23(火)  
　Comic Bunch第21号(5月7日号)发售  
　「**Angel Heart**」第42話  
　【おしゃれ】  
  
■04/16(火)  
　Comic Bunch第20号(4月30日号)发售  
　「**Angel Heart**」第41話  
　【陳老人の店】  
　▽CenterColor特別企划「在新宿捕获GH！！」  
  
■04/09(火)  
　Comic Bunch第19号(4月23日号)发售  
　「**Angel Heart**」第40話  
　【親不孝】  
  
■04/02(火)  
　Comic Bunch第18号(4月16日号)发售  
　「**Angel Heart**」第39話/封面  
　【李大人からの贈り物】  
  
■03/26(火)  
　Comic Bunch第17号(4月9日号)发售  
　「**Angel Heart**」第38話  
　【おかえり】  
  
■03/19(火)  
　Comic Bunch第16号(4月2日号)发售  
　「**Angel Heart**」第37話  
　【影のパーパ】  
  
■03/12(火)  
　Comic Bunch第15号(3月26日号)发售  
　「**Angel Heart**」第36話/巻頭Color  
　【船上の出会いと別れ】  
  
■03/08(金)  
　★Bunch Comics 　「**Angel Heart**」第3巻 发售  
　▽封面是Comic Bunch新年7・8合并特刊AH扉絵  
  
■02/26(火)  
　Comic Bunch第13号(3月12日号)发售  
　「**Angel Heart**」第35話  
　【パパと娘の初デート】  
　▽…下期（14号）AH暂停  
  
■02/19(火)  
　Comic Bunch第12号(3月5日号)发售  
　「**Angel Heart**」第34話/封面  
　【退院】  
  
■02/12(火)  
　Comic Bunch第11号(2月26日号)发售  
　「**Angel Heart**」第33話  
　【一年ぶりの衝撃】  
　▽…下期（12号）是AH封面  
  
■02/05(火)  
　Comic Bunch第10号(2月19日号)发售  
　「**Angel Heart**」第32話  
　【パーパ】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■01/29(火)  
　Comic Bunch第9号(2月12日号)发售  
　「**Angel Heart**」第31話  
　【最後の決着(ケリ)】  
  
■01/15(火)  
　Comic Bunch新年7・8合并特刊 发售  
　「**Angel Heart**」第30話/巻頭Color  
　【失われた名前】  
  
■01/04(金)  
　Comic Bunch新年5・6合并特刊 发售  
　「**Angel Heart**」第29話  
　【李大人の制裁】  
　▽下回是AH巻頭Color  
  
## 2001年（译注）

■12/18(火)  
　Comic Bunch新年3・4合并特刊 发售  
　「**Angel Heart**」第28話  
　【命にかえてでも】  
  
■12/16(日)～2002/01/31(木)  
　吉祥寺[BOOKS Ruhe](http://www.books-ruhe.co.jp/)でCoamix 原画展  
　▽階在楼梯平台的空间里  
　▽「Angel Heart」等、「蒼天の拳」「251」等  
  
■12/11(火)  
　Comic Bunch新年2号(1月8日号)发售  
　「**Angel Heart**」第27話  
　【訓練生時代の想い出】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■12/08(土)  
　★Bunch Comics  
　「**Angel Heart**」第2巻 发售  
　▽封面是Comic Bunch第20号封面  
  
■12/04(火)  
　Comic Bunch新年1号(1月1日号)发售  
　「**Angel Heart**」第26話  
　【惜しみない命】  
　  
■11/27(火)  
　Comic Bunch第29号(12月11日号)发售  
　「**Angel Heart**」第25話  
　【甦った過去】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■11/20(火)  
　Comic Bunch第28号(12月4日号)  
　「**Angel Heart**」暂停。  
  
■11/13(火)  
　Comic Bunch第27号(11月27日号)发售  
　「**Angel Heart**」第24話  
　【忘れていたもの】  
　▽下回是第29号  
  
■11/06(火)  
　Comic Bunch第26号(11月20日号)发售  
　「**Angel Heart**」第23話  
　【戦士たちの絆】  
  
■10/30(火)  
　Comic Bunch第25号(11月13日号)发售  
　「**Angel Heart**」第22話  
　【生き続ける理由】  
  
■10/16(火)  
　Comic Bunch第23・24合并特刊发售  
　「**Angel Heart**」第21話/巻頭Color  
　【戦士の決意】  
　▽附带AH特別Wide Poster  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■10/09(火)  
　Comic Bunch第22号(10月23日号)发售  
　「**Angel Heart**」第20話  
　【新宿で宣戦布告！！】  
　▽下周的合并号有巻頭Color＆WideColorPoster  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
　★Bunch Comics「**Angel Heart**」第1巻 发售  
  
■10/02(火)  
　Comic Bunch第21号(10月16日号)发售  
　「**Angel Heart**」第19話  
　【ミステリアスな獠】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■09/25(火)  
　Comic Bunch第20号(10月9日号)发售  
　「**Angel Heart**」第18話/封面  
　【グラスハートのときめき】  
　▽「聴コミ」Drama CD、Comics初版书帯的申请券等[Present](http://www.netlaputa.ne.jp/~arcadia/ah/ahinfoin.html)（译注：「聴コミ」：「听漫」）  
  
■09/18(火)  
　Comic Bunch第19号(10月2日号)  
　「**Angel Heart**」暂停。  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
　▽[Coamix HP](http://www.coamix.co.jp/)＆[北条司公式HomePage](http://www.hojo-tsukasa.com/)、Comics发售情報更新  
  
■09/11(火)  
　Comic Bunch第18号(9月25日号)发售  
　「**Angel Heart**」第17話  
　【運命の対面】  
　▽Comics第1巻→10月9日(火)发售決定  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■09/04(火)  
　Comic Bunch第17号(9月18日号)发售  
　「**Angel Heart**」第16話  
　【出生の秘密】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■08/28(火)  
　Comic Bunch第16号(9月11日号)发售  
　「**Angel Heart**」第15話  
　【李兄弟との宿運】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■08/21(火)  
　Comic Bunch第15号(9月4日号)发售  
　「**Angel Heart**」第14話  
　【衝撃のフラッシュバック】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■08/07(火)  
　Comic Bunch第13・14合并特刊 发售  
　「**Angel Heart**」第13話/巻頭Color  
　【緊迫する新宿】  
　▽测试AH手机待机图像 免费DL(8月20日まで)  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■08/02(木)  
　「**Angel Heart**」総集編 发售　定价290日元  
　▽第1話～第9話被収録  
  
■07/31(火)  
　Comic Bunch第12号(8月14日号)发售  
　「**Angel Heart**」第12話  
　【衝撃を超えた真実】  
　▽下期13+14合并特刊是AH巻頭Color  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■07/28(土) 22:15～23:09  
　NHK BS2『[週刊Book Review](http://www.nhk.or.jp/book/)』  
　▽mini特集中播放Coamix的漫画制作Document。  
　▽北条司的Interview。  
  
■07/24(火)  
　Comic Bunch第11号(8月7日号)发售  
　「**Angel Heart**」第11話  
　【事実への昂揚】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■07/17(火)  
　Comic Bunch第10号(7月31日号)发售  
　「**Angel Heart**」第10話  
　【こころとの対話】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■07/10(火)  
　Comic Bunch第9号(7月24日号)发售  
　「**Angel Heart**」第9話  
　【衝撃の邂逅】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■07/03(火)  
　Comic Bunch第8号(7月17日号)发售  
　「**Angel Heart**」第8話/封面  
　【出会いと言う名の再会】  
　▽[Coamix HP](http://www.coamix.co.jp/)＆[北条司公式HomePage](http://www.hojo-tsukasa.com/)、正午时Site Renewal
  
■06/26(火)  
　Comic Bunch第7号(7月10日号)发售  
　「**Angel Heart**」第7話/巻頭Color  
　【掲示板への助走】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■06/25(月)  
　新潮社出版的『波』7月号（100日元）中的「Comic BunchInterview」。  
　▽根岸忠氏、原哲夫氏、北条司氏的Interview刊登。  
　▽[新潮社HomePage【Ｗｅｂ新潮】](http://www.webshincho.com/)可查看。  
　（[Mary](mailto:rosemary@badgirl.co.jp)提供的信息。谢谢m(__)m）  
  
■06/24(日)  
　TV朝日系「Scoop21」复活的名作漫画的特辑中，推出Coamix如何创刊Comic Bunch的纪录片。由神谷明旁白，北条氏评论。    
  
■06/19(火)  
　Comic Bunch第6号(7月3日号)发售  
　「**Angel Heart**」第6話  
　【思い出との再会】の巻  
　▽下周是AH巻頭Color！  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■06/12(火)  
　Comic Bunch第5号(6月26日号)发售  
　「**Angel Heart**」封面/第5話・巻頭Color  
　【思い出に導かれて】の巻  
  
■06/05(火)  
　Comic Bunch第4号(6月19日号)发售  
　「**Angel Heart**」第4話  
　【新宿で大暴れ！】の巻  
　▽下周是AH封面＆巻頭Color！  
　▽「聴コミ」在[Coamix HP](http://www.coamix.co.jp/)收听，系6月14日之前！  
  
■05/31(木)  
　朝日新聞的文化・娱乐版关于Comic Bunch等里的Hero复活的話題。  
　「…漫画Hero归来」为题、「**Angel Heart**」中獠说「今后会扮演重要的角色」。  
  
■05/29(火)  
　Comic Bunch創刊3号(6月12日号)发售  
　「**Angel Heart**」封面/第3話  
　【あの人に会いたくて】の巻  
  
■05/26(土)  
　读卖新闻晚间版有一篇介绍Comic Bunch等新漫画杂志的文章。 「『City Hunter』的真的复活」。  
  
■05/22(火)  
　Comic Bunch創刊2号(6月5日号)发售  
　「**Angel Heart**」第2話  
　【絶叫する夢の記憶】の巻  
  
■05/15(火)  
　週刊Comic Bunch創刊！(5月29日号)  
　北条司新連載「**Angel Heart**」第1話  
　【運命に揺れる暗殺者】の巻
