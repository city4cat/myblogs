http://www.netlaputa.ne.jp/~arcadia/ah/ahname_na.html

Angel Heart 人名辞典

---

な行  
|　[な](./ahname_na.md#na)　|　[に](./ahname_na.md#ni)　|　[ぬ](./ahname_na.md#nu)　|　[ね](./ahname_na.md#ne)　|　[の](./ahname_na.md#no)　|

> な  
>   
> - Nothing -  
>   
>   
> に  
>   
> - Nothing -  
>   
>   
> ぬ  
>   
> - Nothing -  
>   
>   
> ね  
>   
> - Nothing -  
>   
>   
> の  
>   
> のがみ-さえこ【野上冴子】  
> 警視庁新宿西警察署の警察官。署長であるらしいが、その色気はとても警察官には思えない。  
> →初登場[第1話](./ahstory1.md#1)  
>   

---

ver.010715

● [back](./ahname.md) ●