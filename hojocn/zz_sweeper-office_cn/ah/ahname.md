http://www.netlaputa.ne.jp/~arcadia/ah/ahname.html

Angel Heart人名辞典

---

-   本节涵盖了出现在Angel Heart中的人名。 (目前有11项)  
-   对于姓氏不明确的人，只登载名字。  

> ●[あ行](./ahname_a.md)（海坊主）  
>   
> ●[か行](./ahname_ka.md)（金）  
>   
> ●[さ行](./ahname_sa.md)（ 冴羽獠）  
>   
> ●[た行](./ahname_ta.md)（張～智）  
>   
> ●[な行](./ahname_na.md)（野上冴子）  
>   
> ●[は行](./ahname_ha.md)（Nothing）  
>   
> ●[ま行](./ahname_ma.md)（槇村香～餅山秀夫）  
>   
> ●[や行](./ahname_ya.md)（Nothing）  
>   
> ●[ら行](./ahname_ra.md)（李大人～林）  
>   
> ●[わ行](./ahname_wa.md)（Nothing）  
>   

---

ver.010804

● [back](./jmc2.md) ●