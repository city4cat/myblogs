http://www.netlaputa.ne.jp/~arcadia/ah/ahstorydiff.md

本誌掲載時と総集編1（8月16日増刊号）の違い

---

<TABLE BORDER="1">
<TR><TD ALIGN="CENTER">話数</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493"><B>本杂志</B></FONT></TD><TD ALIGN="CENTER">本杂志Page</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493"><B>总集篇1</B></FONT></TD><TD ALIGN="CENTER">总集篇1 Page</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#1">第1話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">新宿中央公园文字的颜色(红色)</FONT></TD><TD ALIGN="CENTER">p61</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→黒色</FONT></TD><TD ALIGN="CENTER">p3</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#1">第1話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">Subtitle的「の巻」可以得到</FONT></TD><TD ALIGN="CENTER">p63</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→所有后续的Subties</FONT></TD><TD ALIGN="CENTER">p5</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#1">第1話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">Parachute下降的 Scene</FONT></TD><TD ALIGN="CENTER">p88</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→添加到线图中？</FONT>（译注：待校对）</TD><TD ALIGN="CENTER">p30</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#1">第1話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">「Yamete」的位置（p93，第4 分格）</FONT></TD><TD ALIGN="CENTER">p93</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→偏左</FONT></TD><TD ALIGN="CENTER">p35</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#1">第1話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">ナゼ…？悪夢ヲ見セルノ…</FONT></TD><TD ALIGN="CENTER">p93</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">ナゼ…？コンナ悪夢ヲ見セルノ…</FONT></TD><TD ALIGN="CENTER">p35</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#1">第1話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">JMC的文字的颜色（黒）</FONT></TD><TD ALIGN="CENTER">p102</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→留白</FONT></TD><TD ALIGN="CENTER">p44</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#2">第2話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">「！！」的字母的大小</FONT></TD><TD ALIGN="CENTER">p51</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→变大</FONT></TD><TD ALIGN="CENTER">p53</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#2">第2話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">ドナー心臓は輸送中、臓器は（p55、3分格）</FONT></TD><TD ALIGN="CENTER">p55</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">ドナー心臓は輸送中、臓器コ</FONT></TD><TD ALIGN="CENTER">p57</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#2">第2話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">おい…　何なんだよ？</FONT>（译注：喂……到底是怎么回事？）</TD><TD ALIGN="CENTER">p57</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">おい…香ィ　何なんだよ？</FONT>（译注：喂……香 到底是怎么回事？）</TD><TD ALIGN="CENTER">p59</TD></TR>

<!--
<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#2">第2話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">こんなこと今まで味わったことのない感覚……</FONT>（译注：这种事是我从来没有尝过的感觉……）</TD><TD ALIGN="CENTER">p61</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">（Font的变化）</FONT></TD><TD ALIGN="CENTER">p63</TD></TR>
-->

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#2">第2話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">泣くこたぁないだろうォ</FONT>（译注：没有必要哭）</TD><TD ALIGN="CENTER">p62</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">泣くこたぁないだろうぉ</FONT></TD><TD ALIGN="CENTER">p64</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#2">第2話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">「始末」の強調の点</FONT></TD><TD ALIGN="CENTER">p67</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→没有点</FONT>（译注：待校对）</TD><TD ALIGN="CENTER">p69</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#2">第2話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">生きてくれ…！俺たちのためだ！！</FONT>（译注：活下去吧…！为了我们！！）</TD><TD ALIGN="CENTER">p72</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">生きてくれ…！俺たちのために！！</FONT></TD><TD ALIGN="CENTER">p74</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">心电图、脑电图的反应</FONT></TD><TD ALIGN="CENTER">p25</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">心电图、脑电图无反应</FONT></TD><TD ALIGN="CENTER">p77</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">別名シティーハンター</FONT>（译注：又名CityHunter）</TD><TD ALIGN="CENTER">p31</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">別名シティ・ハンター</FONT></TD><TD ALIGN="CENTER">p83</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">どうやら…大した心臓だったようだな……</FONT>(译注：看来…好像是个了不起的心脏啊……)</TD><TD ALIGN="CENTER">p34</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">やはり…大した心臓だったようだな……</FONT>(译注：果然…好像是个了不起的心脏啊……)</TD><TD ALIGN="CENTER">p86</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">いい？獠私なりに</FONT></TD><TD ALIGN="CENTER">p37</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">いい？獠　私なりに</FONT></TD><TD ALIGN="CENTER">p89</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">只有手出镜的海坊主的手的颜色</FONT></TD><TD ALIGN="CENTER">p38</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→颜色是黑色的</FONT></TD><TD ALIGN="CENTER">p90</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">Coffee的热气</FONT></TD><TD ALIGN="CENTER">p38</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→海子的手部、右下冴子的杯子、左下角的獠的杯子都是画出来的，为画面增色不少</FONT></TD><TD ALIGN="CENTER">p90</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">女性全員に…！</FONT>（译注：所有女人…！）</TD><TD ALIGN="CENTER">p39</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">女性全員に……！</FONT></TD><TD ALIGN="CENTER">p91</TD></TR>
<!--
<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">い～や（「～」的Font）</FONT></TD><TD ALIGN="CENTER">p42</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→Font变化</FONT></TD><TD ALIGN="CENTER">p94</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#3">第3話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">ピンポ～ン（「～」のFont）</FONT></TD><TD ALIGN="CENTER">p42</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→Font变化</FONT></TD><TD ALIGN="CENTER">p94</TD></TR>
-->
<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#4">第4話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">お…おお？（「？」的大小）</FONT></TD><TD ALIGN="CENTER">p27</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→普通大小</FONT></TD><TD ALIGN="CENTER">p103</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#4">第4話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">中国語のルビ「シェンモ」</FONT>（译注：中文的Ruby“什么”）</TD><TD ALIGN="CENTER">p37</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→「センモ」</FONT>（译注：发音类似“森么”）</TD><TD ALIGN="CENTER">p113</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#4">第4話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">え？あなッ</FONT></TD><TD ALIGN="CENTER">p37</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">え？あはっ</FONT></TD><TD ALIGN="CENTER">p113</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#4">第4話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">「シトー」（俄语的Ruby）</FONT></TD><TD ALIGN="CENTER">p37</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→ルビ欠落</FONT>（译注：缺少Ruby）</TD><TD ALIGN="CENTER">p113</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#4">第4話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">CHE（意大利语）的Ruby「ケ」</FONT></TD><TD ALIGN="CENTER">p37</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">→「シェ」</FONT></TD><TD ALIGN="CENTER">p113</TD></TR>

<TR><TD ALIGN="CENTER"><A HREF="ahstory1.md#8">第8話</A></TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">むやみに捜し廻っても無駄だ・・・・考えろ・・・・何か手掛りがあるはずだ・・・・</FONT>（译注：随便找也没用···想一想···应该有什么线索···）</TD><TD ALIGN="CENTER">p32</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">何をやってるんだ俺は……？香が戻ったってどういうことだ　誰を捜しているんだ俺は…！？</FONT>（译注：我在做什么……？香回来了是怎么回事  我在找谁啊！？）</TD><TD ALIGN="CENTER">p178</TD></TR>

<!--
<TR><TD ALIGN="CENTER">話</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">-----</FONT></TD><TD ALIGN="CENTER">BunchPage</TD><TD ALIGN="CENTER"><FONT COLOR="#FF1493">++++++</FONT></TD><TD ALIGN="CENTER">p</TD></TR>
-->

</TABLE>

---

ver.010805

● [back](./ahstory.md) ●