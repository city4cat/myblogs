http://www.netlaputa.ne.jp/~arcadia/ah/ahname_sa.html

Angel Heart 人名辞典

---

さ行  
|　[さ](./ahname_sa.md#sa)　|　[し](./ahname_sa.md#si)　|　[す](./ahname_sa.md#su)　|　[せ](./ahname_sa.md#se)　|　[そ](./ahname_sa.md#so)　|

> さ  
>   
> さえば-りょう【冴羽リョウ】  
> シティーハンターと呼ばれる闇のスイーパー。  
> →初登場[第1話](./ahstory1.md#1)  
>   
>   
> し  
>   
> - Nothing -  
>   
>   
> す  
>   
> - Nothing -  
>   
>   
> せ  
>   
> - Nothing -  
>   
>   
> そ  
>   
> - Nothing -  
>   

---

ver.010619

● [back](./ahname.md) ●