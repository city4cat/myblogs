http://www.netlaputa.ne.jp/~arcadia/ah/jmc2.html


![J.M.C.](../image/jmc1.gif)  
AH的全部Jump・mokkori・catch _! ?_  
  

![はぁと](./image/aheart3.gif)
本页将讨论关于「Angel Heart」的最新信息
![はぁと](./image/aheart3.gif)

  
  

▲北条司「**Angel Heart**」在  
新世纪新人青年杂志「**周刊Comic Bunch**」(2001年5月15日创刊)上开始震撼连载！  
▲**1st.Season**于2010年8月6日发售号结束。  
▲**2nd.Season**在「**月刊Comic Zenon**」(2010年10月25日Coamix创刊·每月25日发售)上连载！！  
→详情请参阅[Zenon官方HP]](http://www.comic-zenon.jp/)  
  
▲週刊Comic Bunchは、<s>'02年5月17日起每周**周五**发售，定价250日元→280日元(发行新潮社·Coamix组合)</s> 2010年8月27日该出版物停刊。  
  
▲Zenon Comics「**Angel Heart** 2ndSeason」第**5**巻、**'13年01月19日(土)**发售！！  
→定价600日元。new!  
  
▲杂志出版时的彩页完全再現！Zenon Comics DX「**Angel Heart** 1stSeason」开始发行。  
第**21、22**巻、**'13年01月19日(土)**发售！！  
→定价680日元。毎月出版2巻、计划出版24巻。new!（译注：待校对）  
  
▲Bunch Comics「**Angel Heart**（1stSeason）」第**33**巻（最終巻）、**'10年09月09日(木)**发售！！  
  
▲「**Angel Heart官方Guidebook**」、**'08年05月16日(金)**发售！！  
  
▲1stSeason再收录的便利店版Comics！Zenon Selection「**Angel Heart**」第**22**巻（最終巻）は、**'11年07月22日(金)**发售！！  
→定价480日元。全22巻。  
  
▲**至'12年10月中旬**、北九州市漫画Museum举办『**北条司原画展**』！  
→详见[官方信息](http://www.hojo-tsukasa.com/2012/09/exhibition.html#more)new!  
  
▲**'12年03月24日(土)**、『**北条司先生Sign会**』举办！  
喜久屋書店 漫画館仙台店、AH2nd第3巻购买者・前150名  
→详见[官方信息](http://www.hojo-tsukasa.com/2012/02/sign.html#more)  
  
▲**'11年10月25日(火)**、Comic Zenon官方邮购Site开业！  
→[『**Comic Zenon Online Store**』](http://store.comic-zenon.jp/)  
  
▲用报纸制作的漫画杂志「**ECO Zenon**」**北条司クロニクル～叙情詩(なみだ)の軌跡～**、**'11年03月09日(水)**发售！！（译注：北条司Chronicle～叙情詩(なみだ)的軌跡～）  
  
▲**'11年03月23日(水)より**、吉祥寺「CAFE ZENON」にて『**Zenon Comics創刊記念原画展**』が举办！  
  
▲**'11年02月26日(土)**、吉祥寺「CAFE ZENON」由“CityHunter冴羽獠主持”『**结缘びPartyーXYZ**』举办！（译注：结缘PartyXYZ）  
→详见[ Zenon官方HP](http://www.comic-zenon.jp/)  
  
▲**'10年11月25日(木)～12月05日(月)**、吉祥寺「CAFE ZENON」『**北条司30周年記念Tribute原画展 第3弾**』举办！（译注：北条司30周年纪念致敬原画展 第3弾）  
  
▲**'10年10月25日(月)**、Comic Zenon创刊当天将在吉祥寺Café  Zenon、BooksRuhe举办「**大优惠活动**」（前一天发传单）  
→详见[編辑部blog](http://www.comic-zenon.jp/blog/)  
  
▲**'10年09月28日(火)～10月11日(月)**、『**北条司30周年和atre吉祥寺Open的Collaboration event**』举办！  
9月28日(火)、atre吉祥寺ゆらぎの広場、AH Dance Performance Event  
→详见[官方](http://www.hojo-tsukasa.com/2010/09/kichijoji2010.html#more)  
  
▲**'10年10月02日(土)**、于吉祥寺Theater举办为期一天的『**Angel Heart Event舞台**』！Ticket发售于9月5日开始！  
→详见[官方](http://www.hojo-tsukasa.com/index.php)  
  
▲**'10年09月11日(土)～11月07日(日)**、毕业生 ─专业人士的世界─ 之原画展『**建校50周年記念事业特別展 キュウサン☆Manga☆Chronicle**』于北条先生的母校九州产业大学的美術館举办！  
→[官方信息](http://www.kyusan-u.ac.jp/ksumuseum/tennji/tenjitop.html)  
  
▲**'10年8月13日(金)・14日(土)**、以「Art之街・吉祥寺」为关键词举办「**吉祥寺Charity Auction**」！北条先生参展！  
→[官方信息](http://www.lcomi.ne.jp/tie-up/t_10charity/)  
  
▲**'10年7月1日(木)～4日(日)**、France Paris举办「**[Japan Expo](http://www.japan-expo.com/)**」、北条先生参加！  
→此事出现在[北条司official](http://twitter.com/hojo_official)Twitter＆官方手机Site「Mobile Bunch」上  
  
▲「**北条司最新Color原画＆Tribute Pinup展**」，**'10年5月14日(金)～<s>5月27日(木)</s>6月3日(木)＜因好评而延长期限＞**期間、吉祥寺「CAFE ZENON」Loft space举办！！  
→[官方信息](http://cafe-zenon.jp/news/?p=526)  
  
▲北条司30周年記念「**北条司和朋友们 Surprise原画展**」、**'09年12月24日(木)～12月~~27日(日)~~30日(水)＜因好评而延长期限＞**期限、吉祥寺「CAFE ZENON」Loft space举办！！  
→[官方信息](http://cafe-zenon.jp/news/?p=240)  
  
▲AH1500万部突破記念Art Gallery「**Memory of X・Y・Z**」が、**'08年05月16日(金)～5月18日(日)**之间、在新宿站举办。17日（周六）在紀伊國屋书店举行Sign会！！  
→[官方信息](http://www.comicbunch.com/AH1500/)  
▽[ Event Report](http://www.netlaputa.ne.jp/~arcadia/ah/ah/AH1500/shinjuku_xyz1.html)  
  
▲Bunch増刊「**Angel Heart総集編**」**Anime＆Comic Special**、**'05年12月09日(金)**发售！！  
  
▲读卖TV・日本TV系'05年秋Anime放送！  
◎读卖TV 第50話(終)**'06年09月25日(月)**25時25分～  
◎日本TV 第50話(終)**'06年09月26日(火)**25時59分～  
→[Anime官方HP](http://www.angelheart.tv)  
  
▲「**Angel Heart DVD Premium BOX**」**Vol.4**、**'07年02月28日(水)**发售！！  
▲「**Angel Heart Vocal Collection**」**Vol.2**、**'06年12月20日(水)**发售！！  
▲「**Angel Heart Original Soundtrack**」、**'06年09月20日(水)**发售！！  
→[DVD官方HP](http://www.angelheart-dvd.com/)  
  
▲Yahoo! JAPAN × Angel Heart联动协作企划「**Angel Heart Special**」、**'07年02月16日(金)**OPEN！！  
→[Yahoo! 動画AH特集Page](http://streaming.yahoo.co.jp/special/anime/angelheart/)  
  
▲AH DVD-BOX ＆ 3月的Ending曲CD发售** Event**、**'06年4月26日(水)**举办  
→[官方信息](http://www.sonymusic.co.jp/Animation/top/uwave.html)  
▲天保山Harbor Village わくわく宝島Main Stage**AH Event**、**'06年07月29日(土)13:00～**  
→[官方信息](http://www.ytv.co.jp/wakuwaku/)

  

## **+Information**

（译注：本文以下信息与[/ah/ahiframe.md](./ahiframe.md)中的内容相同：2009年～2013年1月的信息）

## 2013 (译注)  

■01/25(金)  
　月刊Comic Zenon2013年3月号发售  
　「**Angel Heart** 2ndSeason」第28話/封面  
　【わくわくする秘密】  
　▽巻頭Color特別企画「漫画の描き方」座談会（译注：「漫画的画法」）  
　→北条司×保谷伸・モリコロス・日笠優  
  
■01/19(土)  
　★Zenon Comics  
　「**Angel Heart** 2ndSeason」第5巻 发售  
　▽封面Cover待ち受け・登録者全員Present（译注：待校对）  
　・可以享受从封面画的草稿到完成的过程  
　→详见腰封的QRCode（译注：QRCode即二维码）  
　▽TSUTAYA限定特別附录「北条司の漫画の読み方」（译注：「北条司的漫画的阅读方法」）    
　→新人3作家的座谈会收录・合作海报  
  
■01/19(土)  
　★Zenon ComicsDX  
　「**Angel Heart** 1stSeason」第21・22巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
## 2012 (译注)  

■12/25(火)  
　月刊Comic Zenon 2013年2月号发售  
　「**Angel Heart** 2ndSeason」第27話  
　【未熟な足長おじさん】  
　▽年终大谢礼（通过抽签共抽出400名获奖者）  
　→AH特製ZIPPO和香的Coffee Cup等都是奖品  
  
■12/20(木)  
　★Zenon Comics DX  
　「**Angel Heart** 1stSeason」第19・20巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
■11/24(土)  
　月刊Comic Zenon 2013年1月号发售  
　「**Angel Heart** 2ndSeason」第26話  
　【本当の再会】  
  
■11/20(火)  
　★Zenon Comics DX  
　「**Angel Heart** 1stSeason」第17・18巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
■10/25(木)  
　月刊Comic Zenon2012年12月号发售  
　「**Angel Heart** 2ndSeason」第25話  
　【理不尽な対価】  
　▽赠送有亲笔签名的生原稿Present应征券  
　→創刊2周年特別Present(11/23邮戳有效)  
　・连载作家阵容新画／各1枚  
　・2周年之际的Silent漫画  
  
■10/20(土)  
　★Zenon Comics DX  
　「**Angel Heart** 1stSeason」第15・16巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
■09/25(火)  
　月刊Comic Zenon 2012年11月号发售  
　「**Angel Heart** 2ndSeason」第24話  
　【もうひとつの瞳】  
　▽「ラーメン侍 ―初代熱風録―」後編  
　・北条司先生制作的首部作品  
  
■09/20(木)  
　★Zenon Comics DX  
　「**Angel Heart** 1stSeason」第13・14巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
■08/25(土)  
　月刊Comic Zenon 2012年10月号发售  
　「**Angel Heart** 2ndSeason」第23話/封面  
　【瞳の魔力】  
　▽最新第4巻发售記念・AH巻頭Color特集  
　▽「香のCoffee Cup」Present Campaign   
　→Comics联动到50人(9/30邮戳有效)  
　※以下对象商品上的应征券，需要任意2张  
　「月刊Comic Zenon10月号」  
　「Angel Heart 2ndSeason」第4巻  
　「Angel Heart 1stSeason」第11・12巻  
　▽AH特設Site Digital Accessory 大Present  
　　http://www.comic-zenon.jp/ah4/  
　・PC&智能手机特制壁纸或保温杯衬纸、  
　　Twitter Icon, Screensaver  
　・本刊限定Zenon封面壁纸  
　→8/20～9/30之前  
　▽手机官方 ZenonLandでデコメパック免费Present（译注：待校对）  
　・デコメ絵文字、デコメPicture、iconなど4点Set （译注：待校对）  
　→12/31之前  
　▽Cat's Eye舞台化記念  
　・Cat's Eye Seven 签名 T-shirt Present  
　→抽选3名获奖者(9/24邮戳有效)  
　▽「ラーメン侍 ―初代熱風録―」前編  
　・北条司先生制作的首部作品  
　→Center Color／漫画 田中克樹先生  
　▽ Zenon Online Store期間限定特別価格  
　・Anime fair関西出展記念  
　→8/25～9/24之前  
  
■08/20(月)  
　★Zenon Comics  
　「**Angel Heart** 2ndSeason」第4巻 发售  
　▽「香のCoffee Cup」Present Campaign   
　→本刊联动50名(9/30邮戳有效)  
　※以下对象商品上的应征券，需要任意2张  
　「月刊Comic Zenon10月号」  
　「Angel Heart 2ndSeason」第4巻  
　「Angel Heart 1stSeason」第11・12巻  
　▽AH特設Site Digital Accessory 大Present  
　　http://www.comic-zenon.jp/ah4/  
　・PC&智能手机特制壁纸或保温杯衬纸、  
　　Twitter Icon, Screensaver  
　・Comics腰封的折返AccessCode  
　→8/20～9/30之前  
■08/20(月)  
　★Zenon Comics DX  
　「**Angel Heart** 1stSeason」第11・12巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
　▽「香のCoffee Cup」Present Campaign   
　→本誌連動で50名に(9/30邮戳有效)  
　※以下对象商品上的应征券，需要任意2张  
　「月刊Comic Zenon10月号」  
　「Angel Heart 2ndSeason」第4巻  
　「Angel Heart 1stSeason」第11・12巻  
　▽AH特設Site Digital Accessory 大Present  
　　http://www.comic-zenon.jp/ah4/  
　・PC&智能手机特制壁纸或保温杯衬纸、  
　　Twitter Icon, Screensaver  
　・Comics腰封的折返AccessCode  
　→8/20～9/30之前  
  
■07/25(水)  
　月刊Comic Zenon2012年9月号发售  
　「**Angel Heart** 2ndSeason」第22話  
　【天使のくれた時間】  
　▽ Pachinko CR CityHunter～亮点是XYZ～特集（译注：待校对）  
　→读者限定Present应征券(8/24邮戳有效)  
　A.特製非卖品Poster2種類Set 2名  
　B.特制有点小巧的葡萄酒瓶型折叠伞 5名  
　▽Cat's Eye舞台化決定  
　　劇団「Geki-Halo」(Berryz工房×℃-ute)  
　・東京公演9/22～／大阪公演10/13～  
　→公演Ticket 3組6名にPresent(8/24邮戳有效)  
　・舞台化記念的Original T-shirt 发售決定  
　→ Zenon Online Store数量限定  
　▽第2回Smile Zenon「AH＋持枪女子」結果発表  
　→特别合作环节是吉原基貴先生画的香莹  
　▽下期预告  
　・8/20(月)AH 2ndSeason最新4巻发售記念  
　→連動Present Campaign 启动  
  
■07/20(金)  
　★Zenon ComicsDX  
　「**Angel Heart** 1stSeason」第9・10巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
■06/25(月)  
　月刊Comic Zenon2012年8月号发售  
　「**Angel Heart** 2ndSeason」第21話  
　【時の止まった店】  
　▽ Pachinko CR CityHunter～合言葉はXYZ～CM  
　“小室哲哉×北条司”合作企划「Mana」的楽曲  
　→「SUPERNATURAL」「GET WILD」Mana ver.  
　▽第1回Smile Zenon結果発表  
　→次回themeは「AH＋持枪女子」  
　（特别合作环节将是吉原基貴先生画的香莹）  
  
■06/20(水)  
　★Zenon Comics DX  
　「**Angel Heart** 1stSeason」第7・8巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
■06/05(火)  
　插画投稿Site pixiv“pixiv Comic”Start  
　http://comic.pixiv.net  
　综合性的漫画网站，你可以在这里阅读新的出版物和电子漫画。  
　▽作为Start纪念，Zenon全部连载作品的1话免费  
　→「**Angel Heart** 2ndSeason」第1話和其它  
  
■05/25(金)  
　月刊Comic Zenon 2012年7月号发售  
　「**Angel Heart** 2ndSeason」第20話  
　【ここに居る理由】  
　▽“北条司×小室哲哉”Special対談  
　→第一次见面的两人的Collaboration企划开始了  
　・Full CG Virtual Artist「Mana」  
　→北条司先生创作的角色唱21世紀的「GetWild」  
　▽新企画Smile Zenon笑容插画征集  
　→用笑容表达对喜欢的角色的想法的环节  
　・第2回theme「AH＋持枪女子」(6/24邮戳有效)  
　※也接受插画投稿Site TINAMI  
　▽手机 Zenon Land「Mana」的信息尽早公开  
  
■05/19(土)  
　★Zenon ComicsDX  
　「**Angel Heart** 1stSeason」第5・6巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
■04/25(水)  
　月刊Comic Zenon2012年6月号发售  
　「**Angel Heart** 2ndSeason」第19話  
　【裏切りと誤解】  
　▽3/24喜久屋書店 北条司Sign会リポート＠仙台  
　▽手机 Zenon Land AH 2nd Postcard Present  
　→3月的北条先生Sign会发放 Card Set  
　・Zenon Land会員限定50名(5/24截止)  
　▽新企画Smile Zenon笑容插画征集  
　→用笑容表达对喜欢的角色的想法的环节  
　・第2回theme 「AH＋持枪女子」(6/24邮戳有效)  
　※也接受插画投稿Site TINAMI  
　▽下一期，重要公告 
　→北条司×BIG Artist Special Project 启动  
  
■04/20(金)  
　★Zenon ComicsDX  
　「**Angel Heart** 1stSeason」第3・4巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
  
■03/24(土)  
　**北条司先生**Sign会举办  
　▽喜久屋書店 漫画館仙台店  
　→在该店的AH2nd第3巻購入者180名  
　▽这是国内第2次Sign会  
  
■03/24(土)  
　月刊Comic Zenon 2012年5月号发售  
　「**Angel Heart** 2ndSeason」第18話/封面  
　【40年前から来た男】  
　▽巻頭Color AH 2ndSeason特集  
　・可在手机上收听「香の声」获得密码(～8/31为止)  
　→伊倉一恵录下来  
　(AH2nd第12話的“香の伝言Memo”完全再現)  
　・香的微笑 複製原画Present  
　→伊倉一恵与她的签名・抽选3人(4/24邮戳有效)  
　・Zenon Online Store上CH＆AH的新商品  
　→100t Hammer， Necklace， Zenon T-shirt 等  
　・AH 1st第10話中登場的“香のCoffee Cup”製作決定  
　→北条司先生 全面监督，非常有限  
　▽手机 Zenon Land上有香的铃声  
　→AH待受けも配信中（译注：待校对）  
　▽在参与活动的8家连锁店中的任何一家购买该杂志，可获得以下2大优惠附录 
　「新宿鮫×AH」合作短編小冊子＋Original Postcard   
　→ Postcard 共8种(每个店的设计都不同)  
  
■03/19(月)  
　★Zenon Comics  
　「**Angel Heart** 2ndSeason」第3巻 发售  
　▽超美麗Color扉絵Gallery収録  
　▽手机上听到「香の声」 Campaign   
　→结合本杂志( Zenon5月号)完成“香的留言Memo”  
　▽官方Site上的公告动画  
　→Motion Comics剪裁  
　▽静电驱动的AH特制书签免费分发  
　→使用大日本印刷的Filmo技术的书签，第1次在实际中使用。  
　※在9个目标文教堂书店、Café  Zenon（3/27～）分发（译注：待校对）  
  
■03/19(月)  
　★Zenon Comics DX  
　「**Angel Heart** 1stSeason」第1・2巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷  
　▽手机上听到「香の声」 Campaign   
　→购买第1 or 2卷不寻常的「香の声」铃声时赠送语音（译注：待校对）  
  
■03/19(月)  
　Zenon Comics創刊一周年Fair  
　以“用漫画带来微笑”为Motto的微笑Fair  
　▽购买符合条件的Comics \* 可当场获得一张Postcard   
　→每本Comics 1册1张  
　（ 共15张不同的Postcard/许多是由Zenon连载阵容绘制的）  
　\* 「**Angel Heart** 2ndSeason」第1・2巻等  
  
■02/25(土)  
　月刊Comic Zenon2012年4月号发售  
　「**Angel Heart** 2ndSeason」第17話  
　【宝物と約束】  
　▽北条司先生Sign会情報  
　・3/24(土)喜久屋書店 漫画館仙台店里  
　→在该店购买AH2nd第3巻、前150名  
　▽下期预告  
　・5月号系AH大特集  
　→最新刊发售記念、封面＆巻頭Color  
　・为来电Voice声音呈现「香の声」（译注：待校对）  
　※详情请参照以下单行本(全部3/19发售)的书带  
　→AH2nd第3巻、Zenon Comics DX版AH1st第1 or 2巻  
  
■01/25(水)  
　月刊Comic Zenon2012年3月号发售  
　「**Angel Heart** 2ndSeason」第16話  
　【二人の父親】  
  
## 2011年（译注）  

■12/24(土)  
　月刊Comic Zenon2012年2月号发售  
　「**Angel Heart** 2ndSeason」第15話  
　【息子の門出と親心】  
　▽新春 全作家阵容亲笔Sign彩纸Present  
　・附上获奖者的姓名，各1名  
　▽手机官方 Zenon Land Present情報  
　・装着超级稀有物品的福袋15名  
　→袋内物品包括「CH Paperweight」  
　▽ Zenon Online Store Χmas特別 Campaign   
　・AH silver ring 、AH Zippo特价  
　→时间12/23～12/25  
  
■11/25(金)  
　月刊Comic Zenon 2012年1月号发售  
　「**Angel Heart** 2ndSeason」第14話  
　【出世しないタイプ】  
　▽手机官方 Zenon Land Present情報  
　・冴羽獠勝負Pants Black Version  
　→只限付费会员5人  
　▽ Zenon Online Store新商品  
　・北条先生亲笔Sign的AH Canvas复制原画  
　→用过的笔尖   
  
■10/25(火)  
　月刊Comic Zenon2011年12月号发售  
　「**Angel Heart** 2ndSeason」第13話  
　【期待のホープ】  
　☆創刊一周年記念特別号  
　▽特別付録 冴羽獠“勝負 Pants”  
　→PEACH JOHN× Zenonの合作  
　▽官方邮购Site「 Zenon Online Store」OPEN  
　→不同颜色・ High-Grade版“勝負Pants”限量销售  
　▽Gravure特集「道端Angelica」（译注：Gravure：凹版）  
　→道端Angelica×冴羽獠勝負Pants  
　▽CityHunter Seoul通知  
　▽Pachinko「Cat's Eye－恋ふたたび」情報（译注：パチスロ/Pachinko：柏青哥/弹子机店的老虎机）  
　→Bath Towel Present应征券  
  
■09/24(土)  
　月刊Comic Zenon2011年11月号发售  
　「**Angel Heart** 2ndSeason」第12話/封面  
　【香の声】  
　▽特別付録 AH×CH Original・Clear File   
　※在参与活动的书店购买该杂志，可获得两项额外的特别优惠  
　→AH Postcard ＆小冊子「獠のProposal編」  
　▽手机官方「Zenon Land」AH SubChar大选  
　▽Mobage AHで「エゴノキの花」 Event  （译注：エゴノキ/Mobage：梦宝谷社交游戏。エゴノキ：野茉莉）
　▽韓流Drama「CityHunter in Seoul」  
　→10/29(土)在日本「KNTV」开始播放  
　▽10/9(日)HOME MADE 家族×北条司　家族节日2011  
　→印有北条老师的插画Poster/会场内AH特设展位  
　▽下期预告  
　・12月号系Comic Zenon創刊一周年  
　「冴羽獠勝負 Pants」的特別付録  
　→女性内衣制造商PEACH JOHN与Zenon合作   

■09/20(火)  
　★Zenon Comics  
　「**Angel Heart** 2ndSeason」第2巻 发售  
　▽超美麗Color扉絵Gallery収録  
　▽AH图书 Card Present Campaign   
　→9月刊的相关Comics的书带上也有应征券  
  
■08/25(木)  
　月刊Comic Zenon2011年10月号发售  
　「**Angel Heart** 2ndSeason」第11話  
　【想い出の写真館】  
　▽杏里Original Select Wine 情報  
　・Main Label 是北条司先生的新画  
　→9月的Concert会场内的销售数量有限  
　▽下期预告  
　・11月号AH封面＆巻頭Color  
　→9/20(火)Comics第2巻 发售記念  
　・特別付録 AH×CH Original Clear File   
  
■07/25(月)  
　月刊Comic Zenon2011年9月号发售  
　「**Angel Heart** 2ndSeason」第10話  
　【特別なCoffee Cup】  
　▽8/24(水)ZENON NIGHT vol.3举办  
　「吉祥寺 Miss Cats愛」Café  Zenonで決定  
　→Special Guest是川嶋あい  
  
■07/22(金)  
　★Zenon Selection  
　「**Angel Heart**」第22巻(完) 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■07/08(金)  
　★Zenon Selection  
　「**Angel Heart**」第21巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■06/25(土)  
　月刊Comic Zenon2011年8月号发售  
　「**Angel Heart** 2ndSeason」第9話  
　【エゴノキの香り】  
　▽特别报道 CH已经被雕刻成了一个逼真的人物  
　→「Figure王」出張・号外版（译注：待校对）  
　▽XYZ结缘Couple后续企划  
　→对成立Couple的一組进行实际采访  
　▽手机游戏Game Mobage AH通讯  
  
■06/24(金)  
　Figure王 No.161发售  
　▽1/6Scale、獠・冴子的Figure介绍  
　→Hot Toys生产・预计2011年12月发售  
　▽**CityHunter**大特集  
　→北条司先生Interview和作品解说等  
  
■06/24(金)  
　★Zenon Selection  
　「**Angel Heart**」第20巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■06/10(金)  
　★Zenon Selection  
　「**Angel Heart**」第19巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■05/27(金)  
　★Zenon Selection  
　「**Angel Heart**」第18巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■05/25(水)  
　月刊Comic Zenon2011年7月号发售  
　「**Angel Heart** 2ndSeason」第8話  
　【桜の足あと】  
　▽手机游戏Game Mobage AH综合专题  
　→5月下旬Open  
　▽韓国Drama CityHunter  
　→5/25起在韩国播出  
　▽下期预告  
　・结缘后续报道，再次由CityHunter发动  
　→Party之后的漫画化（译注：待校对）  
  
■05/13(金)  
　★Zenon Selection  
　「**Angel Heart**」第17巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■04/25(月)  
　月刊Comic Zenon2011年6月号发售  
　「**Angel Heart** 2ndSeason」第7話  
　【桜炎上…！】  
　▽韓国Drama CityHunter速報  
  
■04/22(金)  
　★Zenon Selection  
　「**Angel Heart**」第16巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■04/08(金)  
　★Zenon Selection  
　「**Angel Heart**」第15巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■03/26(土)  
　月刊Comic Zenon2011年5月号发售  
　「**Angel Heart** 2ndSeason」第6話/封面  
　【桜に重ねる想い】  
　▽封面插画与AH2nd Comics第1卷联动  
　→手机游戏 Zenon Land 注册者的3D Postcard Present  
　▽特別付録「AH 2ndSeason替代Cover」（译注：待校对）  
　→AH2nd第1巻用“獠＆香”Version的BookCover  
　▽Zenon Comics联动·图书 Card Present  
　→AH和Cat's・愛的2枚Set(带豪华衬纸)  
　▽Comics发售記念「AH 2ndSeason特集記事」  
　→1巻的内容大公开  
　▽潜入！结缘PartyXYZ  
　→完全实况报道2部漫画  
　▽AH2nd等、Comics創刊記念的原画展举办  
　→3/23起在Café  Zenon  
　▽书店店面及び Zenon官方Site发布ComicsCM视频  
　→獠和香瑩Navigate  
　→AH版CM「心は憶えてる」3/22起开始发售  
  
■03/25(金)  
　★Zenon Selection  
　「**Angel Heart**」第14巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■03/22(火)  
　★Zenon Comics創刊(大地震のため19日より延期)  
　「**Angel Heart** 2ndSeason」第1巻 发售  
　▽封面Cover是新绘制的  
　→ Zenon5月号連動・AH图书 Card Present  
　▽1stSeason的全封面用Full Color收录  
　▽书店也免费发放Zenon Comics特别号  
  
■03/11(金)  
　★Zenon Selection  
　「**Angel Heart**」第13巻 发售  
　▽1stSeason重新收录的便利店版Comics  
　▽3月創刊Zenon Comics的Digest特別収録  
　→AH 2ndSeason等，4部作品的第1话试读 
  
■03/09(水)  
　★用报纸制作的漫画杂志「ECO Zenon」发售  
　「北条司Chronicle～叙情詩(なみだ)の軌跡～」  
　▽北条先生30年来轨迹的Digest  
　→**AH**1st第1,2話、**AH**2nd第1,2話、**CH**最终话收录  
  
■02/25(金)  
　月刊Comic Zenon2011年4月号发售  
　「**Angel Heart** 2ndSeason」第5話  
　【桜と家族】  
　▽婚姻企划联动【獠のProposal】和二本立（译注：待校对）  
　→1stSeason人气Episode的加筆修正読切（译注：待校对）  
　▽结缘Party“XYZ”Café  Zenon于2/26举办  
　▽下期预告  
　→下一期是Zenon Comics創刊記念号  
　・AH特製Comics Cover付録  
　・Comics联动 AH图书 Card Present  
　・结缘Party实况转播  
  
■02/25(金)  
　★Zenon Selection  
　「**Angel Heart**」第12巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■02/10(木)  
　★Zenon Selection  
　「**Angel Heart**」第11巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■01/28(金)  
　★Zenon Selection  
　「**Angel Heart**」第10巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■01/25(火)  
　月刊Comic Zenon2011年3月号发售  
　「**Angel Heart** 2ndSeason」第4話  
　【人生の忘れ物】  
　▽CH冴羽獠主办「结缘PartyXYZ」  
　→漫画界首次婚姻 Event的举办・征集参与者  
　（2/26于吉祥寺Café  Zenon／免费参加）  
　▽下期预告  
　结缘企画連動「獠のProposal」特别出版  
　→AH1stSeason人气Episode的大幅加筆読切（译注：待校对）  
　（本篇和W刊载的豪华两本书）  
  
■01/14(金)  
　★Zenon Selection  
　「**Angel Heart**」第9巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
##  2010年（译注）
 
■12/25(土)  
　月刊Comic Zenon2011年2月号(創刊3号)发售  
　「**Angel Heart** 2ndSeason」第3話  
　【本当に好きな人】  
　▽AH特製DVD「杉本有美×AH」付録  
　→AH Event舞台、Gravure Making  
　　AH第1話Motion Comic等収録  
　▽創刊3号连续「漢の二大安心保証」  
　▽下期预告  
　结缘企划「CH主办 Zenon婚姻Party」  
　→开始征集参与者  
  
■12/24(金)  
　★Zenon Selection  
　「**Angel Heart**」第8巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■12/10(金)  
　★Zenon Selection  
　「**Angel Heart**」第7巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■11/26(金)  
　★Zenon Selection  
　「**Angel Heart**」第6巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■11/25(木)  
　月刊Comic Zenon創刊2号 发售  
　「**Angel Heart** 2ndSeason」第2話/封面  
　【封印していた想い】  
　▽特別付録「北条司30周年記念特製画集」  
　→附带豪華作家阵容Tribute Pinup第3弾  
　　※Café  Zenonの原画展和联动  
　　→期間11/25(木)～12/5(日)为止  
　▽作家Atelier探訪→Guest#01 北条司  
　▽創刊3号連続「漢の二大安心保証」  
　▽下期预告  
　下一期附录为AH特製DVD→ Event舞台完全収録  
  
■11/12(金)  
　★Zenon Selection  
　「**Angel Heart**」第5巻 发售  
　▽1stSeason重新收录的便利店版Comics  
  
■10/25(月)  
　月刊Comic Zenon創刊号 发售  
　「**Angel Heart** 2ndSeason」第1話/巻中Color  
　【運命のペンダント時計】  
　▽增加Volume 40P  
　▽特別付録「漢の二大安心保証」  
　→免费注册遇到Trouble时慰问金  (译注：待校对)
　▽AH特製QUO Card Present応募券  
　→Zenon SelectionAH第4巻联动  
　▽「XYZ伝言板」Café  Zenon复活  
　→名人是什么委托？「那个人和XYZ」Start (译注：待校对)  
　▽獠＆香瑩的等身大POP在东京的书店里出现  
　▽Café Zenon和BooksRuhe創刊 Campaign   
　▽北条司Spin-off「Cat's愛」連載開始etc...(译注：spin-off：衍生品)    
  
■10/22(金)  
　★Zenon Selection  
　「**Angel Heart**」第4巻 发售  
　▽1stSeason重新收录的便利店版Comics  
　→2ndSeason特製Poster付き  
　▽Comic Zenon創刊号和联动  
　→AH特制QUO Card Present应征用紙  
  
■10/08(金)  
　★Zenon Selection  
　「**Angel Heart**」第3巻 发售  
　▽1stSeason重新收录的便利店版Comics  
　→毎月第2、第4个周五  
  
■09/24(金)  
　★Zenon Selection  
　「**Angel Heart**」第2巻 发售  
　▽1stSeason重新收录的便利店版Comics  
　→毎月第2、第4个周五  
  
■09/10(金)  
　★Zenon Selection  
　「**Angel Heart**」第1巻 发售  
　▽1stSeason重新收录的便利店版Comics  
　▽定价480日元(税込み)  
　→今後、毎月第2、第4个周五发售  
  
■09/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第33巻 发售  
　▽封面系Comic Bunch第20号封面的图案  
　→1stSeason最終巻  
  
■08/27(金)  
　Comic Bunch第39号(9月10日号)发售  
　▽週刊Comic Bunch将在本期停刊  
　→明年1/21月刊＠Bunch新創刊  
　「**Angel Heart**」计划搬到Comic Zenon  
  
■08/20(金)  
　Comic Bunch第38号(9月3日号)发售  
　「**Angel Heart**」将以8/6发售号发行1stSeason完结  
　→这周号上没有刊登  
  
■08/06(金)  
　Comic Bunch第36・37合并号 发售  
　「**Angel Heart**」第363話  
　【恋の行方】  
　▽1stSeason／完  
　→2ndSeason情報は北条司官方Site及びTwitterで  
  
■07/30(金)  
　Comic Bunch第35号(8月13日号)发售  
　「**Angel Heart**」系暂停。  
  ▽下周系AH 1st.Season最終話  
  
■07/23(金)  
　Comic Bunch第34号(8月6日号)发售  
　「**Angel Heart**」系隔周连载。  
　→这周号上没有刊登  
  
■07/16(金)  
　Comic Bunch第33号(7月30日号)发售  
　「**Angel Heart**」第362話  
　【乱暴な天使】  
  
■07/09(金)  
　Comic Bunch第32号(7月23日号)发售  
　「**Angel Heart**」系暂停。  
  
■07/02(金)  
　Comic Bunch第31号(7月16日号)发售  
　「**Angel Heart**」系暂停。  
  
■06/25(金)  
　Comic Bunch第30号(7月9日号)发售  
　「**Angel Heart**」第361話/封面  
　【二人分のギフト】  
  
■06/18(金)  
　Comic Bunch第29号(7月2日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■06/11(金)  
　Comic Bunch第28号(6月25日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■06/04(金)  
　Comic Bunch第27号(6月18日号)发售  
　「**Angel Heart**」第360話  
　【父と息子】  
  
■05/28(金)  
　Comic Bunch第26号(6月11日号)发售  
　「**Angel Heart**」系暂停。  
  
■05/21(金)  
　Comic Bunch第25号(6月4日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■05/14(金)  
　Comic Bunch第24号(5月28日号)发售  
　「**Angel Heart**」第359話/封面  
　【愛の選択】  
　▽Bunch9周年・北条司30周年記念号  
　・北条司Tribute Pinup第2弾  
　▽北条司最新Color原画＆Tribute Pinup展举办  
　→「CAFE ZENON」从5/14～27为止  
  
■05/07(金)  
　Comic Bunch第23号(5月21日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
　▽次号(14日发售)は北条司30周年Bunch9周年記念号  
　→北条司Tribute Pinup第2弾  
　（小畑友紀／芹沢直樹／高橋留美子／次原隆二／冨樫義博／渡辺航）  
  
■04/23(金)  
　Comic Bunch第21・22合并号 发售  
　「**Angel Heart**」第358話  
　【ギフト】  
  
■04/16(金)  
　Comic Bunch第20号(4月30日号)发售  
　「**Angel Heart**」第357話/封面  
　【陳老人暗躍】  
  
■04/09(金)  
　Comic Bunch第19号(4月23日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■04/02(金)  
　Comic Bunch第18号(4月16日号)发售  
　「**Angel Heart**」第356話  
　【生きる目的】  
  
■03/26(金)  
　Comic Bunch第17号(4月9日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■03/19(金)  
　Comic Bunch第16号(4月2日号)发售  
　「**Angel Heart**」第355話  
　【受け止める覚悟】  
  
■03/12(金)  
　Comic Bunch第15号(3月26日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■03/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第32巻 发售  
　▽封面はComic Bunch'09年第39号封面的图案  
  
■03/05(金)  
　Comic Bunch第14号(3月19日号)发售  
　「**Angel Heart**」第354話  
　【今日限りの自由】  
  
■02/26(金)  
　Comic Bunch第13号(3月12日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■02/19(金)  
　Comic Bunch第12号(3月5日号)发售  
　「**Angel Heart**」第353話/封面  
　【恋の吊り橋】  
  
■02/12(金)  
　Comic Bunch第11号(2月26日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■02/05(金)  
　Comic Bunch第10号(2月19日号)发售  
　「**Angel Heart**」第352話  
　【笑顔の理由】  
  
■01/29(金)  
　Comic Bunch第9号(2月12日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■01/22(金)  
　Comic Bunch第8号(2月5日号)发售  
　「**Angel Heart**」第351話  
　【薔薇と御曹司】  
  
■01/15(金)  
　Comic Bunch新年7号(1月29日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■01/08(金)  
　Comic Bunch新年6号(1月22日号)发售  
　「**Angel Heart**」第350話  
　【陳さんの策謀】  
　▽2010新春全作家阵容亲笔插画Present  
  
## 2009年（译注）  

■12/24(木)  
　Comic Bunch新年4・5合并号 发售  
　「**Angel Heart**」第349話/封面  
　【希望の風】  
　▽北条司Debut30周年企画・第1弾  
　・北条作品Tribute Pinup  
　→由豪华漫画家阵容绘制的新插画(袋とじ附录)（译注：待校对）  
　・北条司mini原画展举办決定  
　→「CAFE ZENON」在12/24～27期间限定  
　→应大众要求，延长至30日(周三)Lunch Time  
  
■12/18(金)  
　Comic Bunch新年3号(1月8日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
　▽下期（24日发售）北条司30周年特別企画  
　→由著名作家绘制的北条World的角色  
  
■12/04(金)  
　Comic Bunch新年1・2合併特大号 发售  
　「**Angel Heart**」第348話  
　【たくさんの月】  
  
■11/27(金)  
　Comic Bunch第52号(12月11日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■11/20(金)  
　Comic Bunch第51号(12月4日号)发售  
　「**Angel Heart**」第347話  
　【優しい嘘】  
  
■11/13(金)  
　Comic Bunch第50号(11月27日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■11/09(月)  
　★Bunch Comics  
　「**Angel Heart**」第31巻 发售  
　▽封面はComic Bunch第14号封面的图案  
  
■11/06(金)  
　Comic Bunch第49号(11月20日号)发售  
　「**Angel Heart**」第346話/封面  
　【嘘だと言って…】  
  
■10/30(金)  
　Comic Bunch第48号(11月13日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■10/23(金)  
　Comic Bunch第47号(11月6日号)发售  
　「**Angel Heart**」第345話  
　【忘却の代償】  
  
■10/16(金)  
　Comic Bunch第46号(10月30日号)发售  
　「**Angel Heart**」系隔号连载。  
　→这周号上没有刊登  
  
■10/09(金)  
　Comic Bunch第45号(10月23日号)发售  
　「**Angel Heart**」第344話/封面  
　【ママを捜して…】  
  
■10/02(金)  
　Comic Bunch第44号(10月16日号)发售  
　「描線の流儀」北条司Interview 後編（译注：「线条的风格」北条司Interview 后篇）  
　▽本期起「**Angel Heart**」系隔号連載  
　本周没有「Angel Heart」的连载。  
  
■09/18(金)  
　Comic Bunch第42・43合併特大号 发售  
　「**Angel Heart**」第343話  
　【会いたい】  
　▽「描線の流儀」北条司Interview 前編（译注：「线条的风格」北条司Interview 前篇）   
  
■09/11(金)  
　Comic Bunch第41号(9月25日号)发售  
　「**Angel Heart**」第342話  
　【偽りの再会】  
  
■09/04(金)  
　Comic Bunch第40号(9月18日号)发售  
　「**Angel Heart**」系暂停。  
  
■08/28(金)  
　Comic Bunch第39号(9月11日号)发售  
　「**Angel Heart**」第341話/封面  
　【涙の理由】  
  
■08/21(金)  
　MINI freak No108(2009年10月号・Natsume公司)  
　「我问了**CityHunter**的冴羽獠！」  
　▽特集「对你来说，Mini是什么？」  
　中獠登場  
　→附带北条先生的comment  
  
■08/21(金)  
　Comic Bunch第38号(9月4日号)发售  
　「**Angel Heart**」系暂停。  
  
■08/07(金)  
　Comic Bunch第36・37合併特大号 发售  
　「**Angel Heart**」第340話  
　【小さな依頼人】  
  
■07/31(金)  
　Comic Bunch第35号(8月14日号)发售  
　「**Angel Heart**」第339話  
　【繋いだ手から】  
  
■07/24(金)  
　Comic Bunch第34号(8月7日号)发售  
　「**Angel Heart**」系暂停。  
  
■07/17(金)  
　Comic Bunch第33号(7月31日号)发售  
　「**Angel Heart**」第338話  
　【空のＣ・Ｈ(CityHunter)】  
  
■07/10(金)  
　Comic Bunch第32号(7月24日号)发售  
　「**Angel Heart**」第337話  
　【ちょっとの特別】  
  
■07/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第30巻 发售  
　▽封面はComic Bunch新年3号封面的图案  
　→初回限定特典 記念 Postcard (全3種)  
  
■07/03(金)  
　Comic Bunch第31号(7月17日号)发售  
　「**Angel Heart**」第336話  
　【ふたつの命】  
  
■06/26(金)  
　Comic Bunch第30号(7月10日号)发售  
　「**Angel Heart**」系暂停。  
  
■06/19(金)  
　Comic Bunch第29号(7月3日号)发售  
　「**Angel Heart**」第335話  
　【カエルの子はカエル】  
  
■06/12(金)  
　Comic Bunch第28号(6月26日号)发售  
　「**Angel Heart**」第334話/封面  
　【天使のサイモン】  
  
■06/05(金)  
　Comic Bunch第27号(6月19日号)发售  
　「**Angel Heart**」第333話  
　【いつかの笑顔】  
  
■05/29(金)  
　Comic Bunch第26号(6月12日号)发售  
　「**Angel Heart**」系暂停。  
  
■05/22(金)  
　Comic Bunch第25号(6月5日号)发售  
　「**Angel Heart**」第332話  
　【君だけのヒーロー】  
  
■05/15(金)  
　Comic Bunch第24号(5月29日号)发售  
　「**Angel Heart**」第331話  
　【只今参上！】  
　▽Bunch8周年記念特別Column  
　STORY Digest・作家陣Special Message  
　→全作品同時刊登  
  
■05/08(金)  
　Comic Bunch第23号(5月22日号)发售  
　「**Angel Heart**」第330話  
　【人質交換】  
  
■04/24(金)  
　Comic Bunch第21・22合併特大号 发售  
　「**Angel Heart**」第329話  
　【潜入！ ＣＨ】  
  
■04/17(金)  
　Comic Bunch第20号(5月1日号)发售  
　「**Angel Heart**」第328話  
　【敵情視察】  
  
■04/10(金)  
　Comic Bunch第19号(4月24日号)发售  
　「**Angel Heart**」系暂停。  
  
■04/03(金)  
　Comic Bunch第18号(4月17日号)发售  
　「**Angel Heart**」第327話  
　【昼行灯(ひるあんどん)】  
  
■03/27(金)  
　Comic Bunch第17号(4月10日号)发售  
　「**Angel Heart**」第326話  
　【対極の男】  
  
■03/19(木)  
　Comic Bunch第16号(4月3日号)发售  
　「**Angel Heart**」第325話  
　【老人(かれ)の貴物(おもいで)】  
  
■03/13(金)  
　Comic Bunch第15号(3月27日号)发售  
　「**Angel Heart**」第324話  
　【笑顔の理由】  
　▽第16号的发售日系3月19日(木)  
  
■03/09(月)  
　★Bunch Comics  
　「**Angel Heart**」第29巻 发售  
　▽封面系Comic Bunch'08年第43号封面的图案  
  
■03/06(金)  
　Comic Bunch第14号(3月20日号)发售  
　「**Angel Heart**」第323話/封面  
　【怪訝なＸＹＺ】  
  
■02/27(金)  
　Comic Bunch第13号(3月13日号)发售  
　「**Angel Heart**」系暂停。  
　▽下期在封面上出现新篇章Start  
  
■02/20(金)  
　Comic Bunch第12号(3月6日号)发售  
　「**Angel Heart**」第322話  
　【カメレオンの遺産】  
  
■02/13(金)  
　Comic Bunch第11号(2月27日号)发售  
　「**Angel Heart**」第321話/封面  
　【激昂】  
  
■02/06(金)  
　Comic Bunch第10号(2月20日号)发售  
　「**Angel Heart**」第320話  
　【制裁の行方】  
  
■01/30(金)  
　Comic Bunch第9号(2月13日号)发售  
　「**Angel Heart**」第319話  
　【黒幕】  
  
■01/23(金)  
　Comic Bunch第8号(2月6日号)发售  
　「**Angel Heart**」第318話  
　【現場復帰】  
  
■01/16(金)  
　Comic Bunch第7号(1月30日号)发售  
　「**Angel Heart**」系暂停。  
  
■01/09(金)  
　Comic Bunch新年6号(1月23日号)发售  
　「**Angel Heart**」第317話  
　【現行犯逮捕】  
　▽2009新春、全作家阵容・亲笔Goods Present  
　→Sign彩纸＆ Message扇子  
  
**→[过去的信息](./ahiframe01.md)**（译注：2001年5月～2008年的信息）  

> ◎拒绝◎  
> 
> -   「Angel Heart」缩写为「AH」。
> -   「Glass Heart」有时被缩写为「GH」。
> -   「獠」是表外漢字，所以用「獠」来表示。
> -   请允许我省略敬称。
> -   JMC的名字的由来是第1话的獠的秘技。
> 
> - Page内操索 -  
>  AND OR

  

> ※如果有需要注意的地方，请发[mail](mailto:xyz@sweeper.office.ne.jp)或BBS。  
> 　另外，本Page是个人Page，与北条司先生、新潮社、Coamix以及实际存在的各关联公司、团体等完全没有关系。

_by [Saeba Ryo](mailto:xyz@sweeper.office.ne.jp)_

---

ver.010624

● [_Sweeper Office_](../index.md) ●




