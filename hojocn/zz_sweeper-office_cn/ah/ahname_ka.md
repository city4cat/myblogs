http://www.netlaputa.ne.jp/~arcadia/ah/ahname_ka.html

Angel Heart 人名辞典

---

か行  
|　[か](./ahname_ka.md#ka)　|　[き](./ahname_ka.md#ki)　|　[く](./ahname_ka.md#ku)　|　[け](./ahname_ka.md#ke)　|　[こ](./ahname_ka.md#ko)　|

> か  
>   
> - Nothing -  
>   
>   
> き  
>   
> きむ【金】  
> "組織"の構成員。先輩の林と共にグラス・ハートの病室を監視する任務を負っていたが…。"組織"では新入りらしい。  
> （名前は[聴コミ](./ahstorycc.md)のキャストにより判明）  
> →初登場[第1話](./ahstory1.md#1)  
>   
>   
> く  
>   
> - Nothing -  
>   
>   
> け  
>   
> - Nothing -  
>   
>   
> こ  
>   
> - Nothing -  
>   

---

ver.010619

● [back](./ahname.md) ●