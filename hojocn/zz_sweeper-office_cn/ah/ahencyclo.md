http://www.netlaputa.ne.jp/~arcadia/ah/ahencyclo.html

Angel Heart 百科辞典　～为了更深入地品味AH？～

---

超级暂定版！  
・只要Keyword数量超过一定数量，页面就会被分为五十音顺序和连载顺序。    
・故事上的设定的说明用<font color=#00aa00>这个颜色</font>标记。  
  
第1話～第12話→　[ [1](./ahencyclo.md#1) | [2](./ahencyclo.md#2) | [3](./ahencyclo.md#3) | [4](./ahencyclo.md#4) | [5](./ahencyclo.md#5) | [6](./ahencyclo.md#6) | [7](./ahencyclo.md#7) | [8](./ahencyclo.md#8) | [9](./ahencyclo.md#9) | [10](./ahencyclo.md#10) | [11](./ahencyclo.md#11) | [12](./ahencyclo.md#12) ]

> ▼第1話  
>   
> しんじゅく ちゅうおうこうえん  
> 【新宿中央公园】<A NAME="新宿中央公園"></A>  
> <font color=#00aa00>是「Angel Heart」开篇的舞台的地点。</font>这是被高层建筑群包围的公园，曾经的淀桥净水厂旧址。1968年4月由区政府设立。8.2公顷的园内约有8万棵树木，有露天舞台、水广场、草坪广场等休闲设施。公园北部还有一个45米高的富士见台(观景台)。<font color=#00aa00>Glass Heart在这个公园清理第50个Target，作为“组织”的“工作”。</font>新宿站步行15分钟。(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
>   
> コルト パイソン 357マグナム  
> 【Colt Python .357Magnum】<A NAME="Colt Python .357Magnum"></A>  
> <font color=#00aa00>耳熟能详的獠的爱枪。 在第1话的扉页中，Glass Heart拿着的似乎也是Python。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)扉页、[第5話](./ahstory1.md#5)其他  
> ⇒[CocktailＸＹＺ](../cocktail.md)的分类解说参照  
>   
> D208 ハイパー  
> 【D208 HYPER】<A NAME="D208 HYPER"></A>  
> NTT DoCoMo的Digital mova移动电话。<font color=#00aa00>Glass Heart用于和“组织”联系。铃声Normal</font>，和弦可以设定「Alps的少女Heidi」和「StarWars」等。它的特点是能够创建大约71亿个不同的肖像，于1999年11月推出。 三菱制造。不支持i-mode。<font color=#00aa00>在“组织”打来的电话中(通话时间约41秒左右)，本机被从大楼跳下来的Glass Heart扔掉。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒[NTT DoCoMo](./ahencyclo.md#NTT DoCoMo)  
>   
> NTT ドコモ  
> 【NTT DoCoMo】<A NAME="NTT DoCoMo"></A>  
> 目前是日本最大的Share移动运营商，<font color=#00aa00>Glass Heart持的移动电话机也是DoCoMo</font>。DoCoMo是**Do C**ommunications **O**ver The **MO**bile Network Inc.的缩写。移动通信部门于1992年从日本电报电话公社私有化的NTT中分离出来，成为NTT移动通信网络NTT DoCoMo。 当时，最初的费用包括10万日元的押金、4万5800日元的新订费和1万7000日元的基本使用费（包括设备租金）。(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒[D208 HYPER](./ahencyclo.md#D208 HYPER)  
> ⇒[NTT DoCoMo 代代木大厦](./ahencyclo.md#NTT DoCoMo 代代木大厦)  
> ⇒[D502i HYPER](./ahencyclo.md#D502i HYPER)  
> ⇒[D101 HYPER](./ahencyclo.md#D101 HYPER)  
>   
> ぞうきいしょく Coordinator  
> <A NAME="臓器移植Coordinator">【臓器移植Coordinator】</A>  
> 器官捐献候选人出现时，信息从医院联系日本器官移植Network，该Network的Block Center派遣的是器官移植Coordinator。主要工作是向提供候选人的家属说明器官捐献，并向家属确认承诺的意愿。与进行移植的医院联系，安排器官的医学检查，运输器官等。<font color=#00aa00>在「新宿 Donor心臓強奪事件」中，一名器官移植协调员在运送心脏时被枪杀，心脏被盗走。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒[Donor制](./ahencyclo.md#Donor制)  
> ⇒[新宿 Donor心臓強奪事件](./ahencyclo.md#新宿 Donor心臓強奪事件)  
>   
> Donor せい  
> <A NAME="Donor制">【Donor制】</A>  
> 「器官移植相关法律」从1997年10月开始实施，在此之前，除了心脏停止后可以移植的肾脏和角膜之外，还可以移植心脏、肺、肝脏、小肠等。心脏、肺、肝脏在心脏停止后血液不再循环时，状态恶化，因此以脑死亡移植为条件。器官捐献者被称为Donor，他们必须于死前在「器官捐献意愿指示Card」上以书面形式表明其捐献意愿(该Card也可以表明捐献者不愿意捐献)。与此相对，器官捐赠的接受者则被称为Recipient。意愿指示Card在市政厅、邮局、保健所、驾驶执照考试场和Convenience Store都有。另外，即使被判定为脑死亡的Donor候选人拥有填写了提供器官的意愿的Card，如果没有家人的同意，也不能进行移植。<font color=#00aa00>香的情况，据信獠作为家庭成员同意了。</font>在心脏移植的Case中，必须在摘除后的约4小时内进行移植，重新开始血流，能够进行移植手术的设施也有限，因此，目前移植的Chance较少(该法实施后的移植病例为10例左右)。另外，器官移植法从2000年10月开始进入了重新评估的时期。在其修正案中，认为脑死亡一律为人的死亡，即使成为脑死亡的本人没有意志表示Card，也认为自己决定为器官移植而死，只要家人的同意就可以进行移植。(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒[新宿 Donor心臓強奪事件](./ahencyclo.md#新宿 Donor心臓強奪事件)  
>   
> たいわん  
> <A NAME="台湾">【台湾】</A>  
> 该岛位于冲绳主岛西南偏西约600公里处。它由台湾本岛、澎湖群岛和其他附属岛屿组成。它从明末清初开始成为中国的领土，当时大量的汉族人移民到岛上，但由于中日战争，这里成为日本的领土，第二次世界大战中日本战败后回归中国，蒋介石政权于1949年迁入这里。其名称在其宪法中是「中華民国（英語名 Republic of China）」、在联合国会员国中是「Taiwan」（译注：现在(2023年)其在联合国的文献中为"Taiwan Province of China"，来源：[China - Country Profile](https://data.un.org/CountryProfile.aspx/_Images/CountryProfile.aspx?crName=China)）、在国际竞技会中是「Chinese Taipei」。气候属于亚热带，普遍炎热多雨。官方语言是汉语普通话，但在日常生活中也使用台湾话（福建话）。一些地区也使用客家话（粤语）。 许多在战争期间接受日本教育的人仍然讲日语。汽车靠右行驶。与日本的时差为-1小时。<font color=#00aa00>教育Glass Heart的基地（其中一个"组织"？）就在台湾。Glass Heart被移植了心脏，在台湾的一所房子里接受治疗，没有恢复意识...。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒[台北市](./ahencyclo.md#台北市)  
>   
> すざく ぶたい  
> <A NAME="朱雀部隊">【朱雀部隊】</A>  
> <font color=#00aa00>“组织”的杀人部队。它由从不到5岁就只受过杀人术的部队人员组成，可以说是「殺人Machine」。受训儿童要接受如此严酷的训练，以至于20人中只有一人能够存活下来，然后在最后的测试中，通过互相残杀来选拔正式成员。Glass Heart就是朱雀部队Elite中的Elite，“组织”曾试图让心脏破裂的Glass Heart实施移植手术而苏醒。</font>顺便提一下，朱雀是四神之一，是中国传说中保护南方的鸟（东方的是青龙、西方的是白虎、北方的是玄武）。 在台湾，诸如「在朱雀的方向...」这样的短语仍然被使用。(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒[台湾](./ahencyclo.md#台湾)  
>   
> スミス＆ウェッソン M586  
> <A NAME="S&W M586">【S&W M586】</A>  
> <font color=#00aa00>在“组织”的射击场训练生们用于射击训练的手枪中的一种。小时候的Glass Heart似乎也用这种手枪训练。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> ベレッタ M84  
> <A NAME="Beretta M84">【Beretta M84】</A>  
> <font color=#00aa00>在“组织”的手枪组装的训练中Glass Heart组装的手枪。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> グロック 17  
> <A NAME="Glock 17">【Glock 17】</A>  
> <font color=#00aa00>“组织”的教官拿着的手枪。没能在时间内组装手枪的训练生，就这样毫不留情地被枪杀了。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> ヘッケラー＆コッホ MP5A5  
> <A NAME="H&K MP5A5">【H&K MP5A5】</A>  
> <font color=#00aa00>Glass Heart在最后的 "组织 "试验中使用的Sub-Machine Gun（译注：冲锋枪）。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> Glass Heart  
> <A NAME="Glass Heart">【Glass Heart】</A>  
> <font color=#00aa00>拥有准备充分、无懈可击的暗杀技术的主人公的Code Name。因为他她的方法敏感而机警，甚至不向对手暴露自己的真实面目。这个名字显然来自于她被"组织"敬畏且嫉妒地称为"Glass之心"（即懦夫），因为她有强烈的谨慎意识。</font>(2001.06.19)（译注：待校对）  
> →[第1話](./ahstory1.md#1)  
>   
> ファブリック・ナショナル／フシー・オートマチック・レガーL1A1  
> <A NAME="FN FAL L1A1">【FN FAL L1A1】</A>  
> <font color=#00aa00>在Glass Heart所做噩梦中的回忆Scene中，GH在Parachute降落的夜间狙击中开枪的Rifle。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> ヘッケラー＆コッホ PSG-1  
> <A NAME="H&K PSG-1">【H&K PSG-1】</A>  
> <font color=#00aa00>Glass Heart所做噩梦中的回想Scene，躲在喷泉中进行狙击时用到的Rifle。</font>(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> Dormicum  
> <A NAME="Dormicam">【Dormicam】</A>  
> Benzodiazepine系（作用于大脑中的Benzodiazepine受体）的鎮静剤、Midazolam的商品名。水溶性、广泛用于麻醉的诱导。<font color=#00aa00>没有恢复意识的Glass Heart，不知为何脉搏数飙升“发作”时，“组织”的医疗班使用的也是Dormicum。几个月来，这种发作似乎每天都在发生。</font>这些药物具有高度成瘾性，并有低血压和行为异常的副作用，在日本被指定为麻醉品和精神药物管制法中的精神药物。(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
>   
> フィンキール  
> <A NAME="Finkeel">【Finkeel】</A>  
> 龙的脊梁和船的脊梁被称为龙骨，而像鲨鱼背鳍一样从船体上突出来防止船被侧翻的突起被称为Finkeel。<font color=#00aa00>在新宿的会员制女性Athletic Club的室内Pool中，一边游泳一边物色对方的獠，作为“Finkeel”而为人熟知的人气人物(?)似乎是受欢迎的。</font>(2001.06.19)（译注：待校对）  
> →[第1話](./ahstory1.md#1)  
>   
> バサロ  
> <A NAME="Vasallo">【Vasallo】</A>  
> 仰卧着玩Dolphin Kick的游泳方法。<font color=#00aa00>为了用室内Pool来物色对方，他在Vasallo游泳。</font>Vasallo是一种在水中行进的游泳方式，比仰泳在水面上游泳阻力小，速度更快。首尔奥运会时，铃木大地选手在Vasallo上夺得金牌而闻名。(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
>   
> ジャンピング Mokkori キャッチ  
> <A NAME="JMC">【JMC】</A>  
> <font color=#00aa00>从水中Jump，Mokkori(爱的)救生圈Catch了良的秘技。这是常人无法模仿的招数。</font>这也是这个页面的名字的由来。(2001.06.19)（译注：JMC是"Jump Mokkori Catch"的缩写）  
> →[第1話](./ahstory1.md#1)  
>   
> いっぱつ おおかみ  
> <A NAME="一発狼">【一发狼】</A>  
> <font color=#00aa00>獠的自称。「我是"新宿的一发狼"」「我不把自己当回事...」。理由是「卡米桑吵死了」～～～～～～。它被认为是"一枪 "和"一炮"的双关语。</font>（什么？）(2001.06.19)  
> →[第1話](./ahstory1.md#1)  
>   
> ▼第2話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> しんじゅく Donorしんぞう ごうだつじけん  
> <A NAME="新宿 Donor心臓強奪事件">【新宿 Donor心脏抢劫事件】</A>  
> <font color=#00aa00>「2000年5月12日，牧村香(28岁)交通事故死亡…在确认脑死亡后(21:58)，因为被登记为捐赠者，她的心脏被切除...其他器官严重受损...切除心脏的工作被放弃。 Donor心脏在运输过程中，协调员被枪杀，捐赠者的心脏被抢走。 两周后，在横滨市Apart区发现一具男性自杀尸体。 同室内有手枪和染血的T-Shirt...T-Shirt的血被证明是Coordinator的血。手枪也与指纹相符。 枪手被确定为肇事者。 虽然没有证据表明与心脏有关，但此案已经结案」（警視庁新宿西警察署／署長室Database）</font>(2001.06.19)  
> →[第2話](./ahstory1.md#2)  
> ⇒[Donor制](./ahencyclo.md#Donor制)  
> ⇒[臓器移植Coordinator](./ahencyclo.md#臓器移植Coordinator)  
>   
> ドコモ よよぎ ビル  
> <A NAME="NTT DoCoMo 代代木大厦">【NTT DoCoMo 代代木大厦】</A>  
> <font color=#00aa00>从公园（新宿御苑？）可以看到的建筑，獠和香在那里见面。</font>在新宿南口2000年10月竣工的大楼，与Empire State Building非常相似。27层楼，高239米。大楼顶部载有十几架Parabolic Antenna，兼作NTT DoCoMo的Office和通讯基地。(2001.06.19)  
> →[第2話](./ahstory1.md#2)  
> ⇒[NTT DoCoMo](./ahencyclo.md#NTT DoCoMo)  
>   
> タキシード  
> <A NAME="Tuxedo">【Tuxedo】</A>  
> 男子的夜会简式礼服，代替燕尾服。1886年，New York州的Tuxedo Park俱乐部的正装舞会，一名男子穿着鲜红的Tailless Coat(没有尾巴的衣服)出场，而他本来是穿着燕尾服参加舞会的。这是那个名字的由来。在日本，大约在20世纪60年代，白色上下两层的Tuxedo Frill的Shirt变得非常流行，成为新郎的标准服装。<font color=#00aa00>同意和香拍摄纪念照时，獠最终决定穿上它。</font>顺便说一下，婚礼纪念照可以从4万到5万日元起拍，包括租用服装和其他物品。(2001.06.19)（译注：待校对）  
> →[第2話](./ahstory1.md#2)  
>   
> ▼第3話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> そうさく  
> <A NAME="操索">【操索】</A>  
> <font color=#00aa00>觉醒的Glass Heart在“组织”的Database中检索了「saeba ryo」。“组织”的Database的System当然是中文，检索的Button中有「搜索」的文字。獠的File是「FILE-2001」。</font>(2001.06.19)  
> →[第3話](./ahstory1.md#3)  
>   
> CityHunter  
> <A NAME="CITY HUNTER">【CITY HUNTER】</A>  
> <font color=#00aa00>"组织"Database中关于獠的文件包含一张从某个角度隐蔽拍摄的獠的照片，以及对CityHunter的描述。「黑暗的杀手(Swepper)...精通所有武器...年龄经历不明......以新宿为据点...」。</font>(2001.06.19)  
> →[第3話](./ahstory1.md#3)  
>   
> C-4  
> <A NAME="Composition C-4">【Composition C-4】</A>  
> 俗称Plastic炸弹。 非常坚固，由Nitrotoluene、Dinitrotoluene、trinitrotoluene（TNT）、tetryl、Nitrocellulose、wax等的油性混合物与Trimethylenetrinitramine混合制成。 它像粘土一样灵活，可以变成任何形状。 它绝对需要一个导火索来引爆。 在正常状态下，它在点燃时不会爆炸，可以作为固体燃料的替代品。 <font color=#00aa00>Glass Heart在逃离台湾的 "组织 "宅邸时，使用了在宅邸的武器和炸药库中发现的C-4和其他武器，彻底摧毁了该宅邸。</font>顺便说一下，Trimethylenetrinitramine是第二次世界大战期间世界各国最致力于大量生产的军用炸药。(2001.06.19)  
> →[第3話](./ahstory1.md#3)  
>   
> きっさ Cat'sアイ  
> <A NAME="COFFEE HOUSE CAT'S EYE">【COFFEE HOUSE CAT'S EYE】</A>  
> <font color=#00aa00>海坊主做Master的熟悉的咖啡店。营业时间为上午10：00~晚上8：00。</font>(2001.06.19)  
> →首次登场[第3話](./ahstory1.md#3)  
>   
> とうきょう600まんにん  
> <A NAME="東京六〇〇万人">【東京六〇〇万人】</A>  
> <font color=#00aa00>为了找到胸部有手术疤痕的女性，「我要看东京所有女性的胸部」獠称。 冴子很担心「但是如果这家伙有可能做的话...东京总共有600万名女性...！」。</font>根据截至2000年10月1日的人口普查，东京都女性人口为6，032，953人，自大正9年调查开始以来，女性人数首次超过男性(男性6，026，284人)。(2001.06.19)  
> →[第3話](./ahstory1.md#3)  
>   
> ミニ クーパー  
> <A NAME="MINI COOPER">【MINI COOPER】</A>  
> <font color=#00aa00>獠的愛車。</font>(2001.06.19)  
> →首次登场[第3話](./ahstory1.md#3)  
> ⇒[CocktailＸＹＺ](../cocktail.md)的分类解说参照  
>   
> ▼第4話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> Vz61 スコーピオン  
> <A NAME="BRNO Vz61 SCORPION">【BRNO Vz61 SCORPION】</A>  
> <font color=#00aa00>在第4集的扉页图中，Glass Heart插在腰带上的Sub-Machine Gun。</font>(2001.06.19)  
> →[第4話](./ahstory1.md#4)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> エックス ワイ ゼット  
> <A NAME="XYZ">【XYZ】</A>  
> <font color=#00aa00>Glass Heart从十字路口的Scramble的斑马线突然联想到X。当然，是用来和CityHunter联系的密码。</font>(2001.06.19)  
> →[第4話](./ahstory1.md#4)  
> ⇒[CITY HUNTER](./ahencyclo.md#CITY HUNTER)  
> ⇒[XYZ?](./ahencyclo.md#XYZ?)  
> ⇒[伝言板](./ahencyclo.md#伝言板)  
>   
> ベンツ 500SEL  
> <A NAME="MERCEDES BENZ 500SEL">【MERCEDES BENZ 500SEL】</A>  
> SClass Benz。<font color=#00aa00>根掘会系暴力团伙年轻头的车。似乎没有实施防弹规格等处理。Tokarev的7.62毫米子弹贯穿Gasoline Tank时，很容易被烧毁。</font>(2001.06.19)  
> →[第4話](./ahstory1.md#4)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> バヨネット ナイフ  
> <A NAME="Bayonet Knife">【Bayonet Knife】</A>  
> <font color=#00aa00>大闹新宿十字路口的Glass Heart所携带的刀像刺刀规格的Bayonet Knife（类似于Buck公司生产的Phrobis M9？ ）。</font>Bayonet Knife是一把刀，安装在Rifle等的前端，可以作为刺刀使用。(2001.06.19)  
> →[第4話](./ahstory1.md#4)  
>   
> Tokarev TT33  
> <A NAME="TOKAREV TT33">【TOKAREV TT33】</A>  
> <font color=#00aa00>若頭带着一把手枪，GlassHeart拿着手枪射中了汽车的Gasoline Tank，使奔驰车着火。</font>在日本该地区流通的大多数Tokarevs是Norinco公司生产的54式手枪（M54），是一种仿制产品，尽管其中大多数是在中国根据许可证生产的。(2001.06.19)  
> →[第4話](./ahstory1.md#4)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> XYZって  
> <A NAME="XYZ?">【XYZ?】</A>  
> <font color=#00aa00>一个年轻的中国人脱口而出「我不懂日语～」，「什么是XYZ？（XYZ为何物？"）」 Glass Heart立即用中文问道。如果他说自己是「韓国人」，她会用韩语问「ムオッXYZ?」然后用几种语言问一系列关于「XYZ」是什么的问题。「シトーXYZ?（Russia語）」、「XYZ itu apa?（\* Indonesia語）」、「Qu'est-ce que c'est XYZ?（France語）」、「XYZ,CHE?（Italy語）」、「What is XYZ?（英語）」etc...。Glass Heart似乎精通多国语言。</font>  
> （\*因为发音是「アパ」，所以是Indonesia语。如果发音为「アプ」，则为Malaysia语。）(2001.06.19)  
> →[第4話](./ahstory1.md#4)  
> ⇒[XYZ](./ahencyclo.md#XYZ)  
>   
> ▼第5話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> ブローニング ハイパワー マーク3  
> <A NAME="FN BROWNING HI-POWER Mk.3">【FN BROWNING HI-POWER Mk.3】</A>  
> <font color=#00aa00>Comic Bunch第5号(6月28日号)封面，Glass Heart拥有的手枪。</font>(2001.06.19)  
> →[第5話](./ahstory1.md#5)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> デザート イーグル  
> <A NAME="IMI DESERT EAGLE">【IMI DESERT EAGLE】</A>  
> <font color=#00aa00>第5話的扉页图中，GlassHeart左手拿着的手枪。</font>(2001.06.19)  
> →[第5話](./ahstory1.md#5)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> New-Nanbu M60  
> <A NAME="New-Nanbu M60">【New-Nanbu M60】</A>  
> 日本警察使用的代表性手枪。<font color=#00aa00>赶到新宿一个十字路口的汽车爆炸和大火现场的警察，都在第一时间将他们的New-Nanbu转向一个持有Tokarev手枪的年轻人。</font>(2001.06.19)（译注：待校对）  
> →[第5話](./ahstory1.md#5)  
> ⇒详细数据请参照各领域解说(即将发布)  
>   
> ▼第6話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> たいぺい し  
> <A NAME="台北市">【台北市】</A>  
> 台湾的中心城市，1949年成为中华民国的临时首都（宪法规定的首都是南京）。它位于台北盆地的中心，是政府机构、银行和企业的政治、经济和文化中心。台北不是台湾省的一部分，但被认为是行政院的 "管辖城市"，与台湾省的地位相同。<font color=#00aa00>李成人和"组织"的高级成员在台北市一栋高层建筑的一个房间里开会，追踪GH的逃亡。</font>(2001.06.26)  
> →[第6話](./ahstory1.md#6)  
> ⇒[台湾](./ahencyclo.md#台湾)  
>   
> Recipient  
> <A NAME="recipient">【recipient】</A>  
> 在器官移植中，器官提供者称为Donor(Donor)，接受者(患者)称为Recipient。(2001.06.26)  
> →[第6話](./ahstory1.md#6)  
> ⇒[Donor制](./ahencyclo.md#Donor制)  
>   
> げんし  
> <A NAME="幻視">【幻視】</A>  
> 实际上可以看到不存在的东西。<font color=#00aa00>据参与“组织”GH心脏移植的一名医生说，在接受心脏移植的患者中，有些人的脑海中浮现了以前器官主人的记忆。病人可能会做梦或经历幻視，引发他本不应该知道的Donor的记忆。</font>确实有许多这样的案例被报道过。 然而，在科学上，这个谜团还没有被解开。 记忆是由各种大脑物质储存和回忆的，最近的研究表明，参与记忆的神经递质「神经肽（Neuropeptide）」也存在于各种器官的神经细胞中，包括心脏。 这意味着记忆可能不仅储存在大脑中，而且也储存在其他器官中。 人类的情绪是来自神经元的电信号，当Neuropeptide附着在（Receptor）上时，这些信号会得到促进，所以Neuropeptide可能因此让Recipient继承Donor的情绪 \* 。  
> 　此外，心脏器官不是由大脑驱动，而是由其自身的神经元组驱动。 有这样的假说，这是一个与大脑功能相同的System，它储存自己的记忆，这些记忆可能通过移植传递给Recipient。  
> （ \* 说到底是个假说）  
> （↓興味のある方は↓）  
> Claire Sylvia, William Novak『记忆的心脏』（～一位心脏移植患者的手记～　角川書店、1998）1500日元  
> Pearsall,Paul. _The Heart's Code: Tapping the Wisdom and Power of Our Heart Energy.'98_ Broadway Books,1998.(2001.06.26)  
> →[第6話](./ahstory1.md#6)  
>   
> ▼第7話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> かおりのCup  
> <A NAME="香のCup">【香のCup】</A>  
> <font color=#00aa00>[第6話](./ahstory1.md#6)，GH来到店里，海坊主为她倒咖啡的Cup。咖啡屋「CAT'S EYE」中好像放着香专用的Cup</font>(2001.07.01)  
> →[第7話](./ahstory1.md#7)  
>   
> でんごんばん  
> <A NAME="伝言板">【伝言板】</A>  
> <font color=#00aa00>GH听到香的幻象说「一次就行了XYZ留言板满了...」她为脑海中浮现的留言板形象感到困扰。 看来，如果你在新宿站东口检票口前的留言板上写下XYZ--已经没有时间了，帮帮我--你的愿望就会实现（仅限女性），这个传言不仅在城市里传开了，而且在乡下也有。</font>(2001.07.01)  
> →[第7話](./ahstory1.md#7)  
> <font color=#00aa00>在[第8集](./ahstory1.md#8)中，留言板由于移动电话等的普及而没有人使用，去年(=2000年)被撤去了。</font>另外，留言板本来就是**不存在的**。(2001.07.08)  
> →[第8話](./ahstory1.md#8)  
> ⇒[XYZ](./ahencyclo.md#XYZ)  
>   
> めいさいふく  
> <A NAME="迷彩服">【迷彩服】</A>  
> 带有迷彩图案的服装，以使其在交火或其他战斗情况下难以被敌人发现。 其目的是与周围的背景融为一体，迷惑敌人的识别。 适合的图案因地点不同而不同，如Woodland Pattern用于在森林中作战，Desert Pattern用于在沙漠中作战，等等。 世界上第一件迷彩服是由德军在第二次世界大战期间开发的。<font color=#00aa00>[第3話](./ahstory1.md#3)，GH在新宿所穿的迷彩服与从 "组织 "的库中拿的迷彩服**不同**。 在新宿的一个十字路口发生的汽车火灾中，警方正在寻找「穿迷彩服的女人」作为嫌疑人...</font>迷彩图案，现在也被纳入了一般的Fashion中。(2001.07.01++07.08加筆)  
> ⇒[UNIKURA](./ahencyclo.md#UNIKURA)  
> →[第7話](./ahstory1.md#7)  
>   
<!--
<FONT SIZE="+1" COLOR="#FF1493">しんじゅく 3ちょうめ<BR>【新宿3丁目】</FONT><BR>説明<FONT COLOR="#008B8B">設定</FONT><BR>→<A HREF="ahstory1.html#7">第7話</A><BR><BR>
-->
> ユニクラ  
> <A NAME="UNIKURA">【UNIKURA】</A>  
> <font color=#00aa00>被警察追赶的GH闯入一家服装店，换下她的迷彩服。「UNIQLO」的Parody。</font>(2001.07.01)  
> →[第7話](./ahstory1.md#7)  
> ⇒[迷彩服](./ahencyclo.md#迷彩服)  
>   
> に ちょうめ  
> <A NAME="二丁目">【二丁目】</A>  
> <font color=#00aa00>追捕GH的警察们将搜索范围扩展到二丁目地区。</font>说到新宿二丁目，是日本最大的同性恋区域。很多俱乐部和酒吧都是针对那些人的。(2001.07.01)  
> →[第7話](./ahstory1.md#7)  
>   
> かんばんもち  
> <A NAME="看板持ち">【看板持ち】</A>  
> 在繁华街道的十字路口等地，拿着店的招牌站着的人。<font color=#00aa00>向坐在小巷里的GH打招呼，告诉他XYZ留言板的位置(为教学而做的吗？)老人拿着可以读作「～Health～～Renge」的招牌。招牌上</font>**似乎**经常<font color=#00aa00>贴着打折券</font>。<font color=#00aa00>虽然是蛇足，但是招牌上写的店名的全貌难道不能读成“「メレンゲ」吗？</font>(2001.07.01)  
> →[第7話](./ahstory1.md#7)  
> （<font color=#00aa00>→[第8話](./ahstory1.md#8)，「Fashion Health　\* メレンゲ」和发现。</font>）  
> （\* メレンゲ（Meringue）：搅拌好的蛋清）(2001.07.08)  
> →[第8話](./ahstory1.md#8)  
> ⇒[Health](./ahencyclo.md#Health)  
>   
> へるす  
> <A NAME="Health">【Health】</A>  
> 性产业的一种。根据「娱乐和娱乐业相关业务的监管和适当经营法」，像Fashion Health这样的風俗店只允许「从日出到午夜」开业。<font color=#00aa00>拿着招牌的老人把拿着的三明治递给GH，那可能是他打算在商店开张前吃的自己用的早餐。</font>(2001.07.01)  
> →[第7話](./ahstory1.md#7)  
> ⇒[看板持ち](./ahencyclo.md#看板持ち)  
>   
> マイシティー  
> <A NAME="MY CITY">【MY CITY】</A>　[新旧Logo比較写真](./myc.md)  
> JR新宿站东口的车站大楼。正式名称为「株式会社新宿Station Building」。这座大楼是根据新宿民众站设立计划建造的，该计划是在国铁车站上用民间资本建造大楼，并将其打造成Shopping Center。从1964年开业，从地下2楼到地上8楼有250个Tenant。屋顶还有夏季限定的Beer-garden和会员制的Tennis School等。  
> 「MY CITY」的Logo于1997年换新、<font color=#00aa00>与CH连载时相比，有了微妙的变化。GH匆匆赶到东口检票口前的留言板，尽管这是第一次来的地方，但对这座大楼有似曾相识的感觉。</font>  
> 另外，在该大楼上设置有可以[从WEB上操作的Camera](www.x-zone.canon.co.jp/WebView/)，从那里可以24小时地观看Studio Alta周边的影像。(2001.07.01)  
> →[第7話](./ahstory1.md#7)  
> ⇒[新宿Studio Alta](./ahencyclo.md#新宿Studio Alta)  
> →[e-MYCITY](http://www.e-mycity.co.jp/)（公式）  
>   
> しんじゅくえき ひがしぐち こうばん  
> <A NAME="新宿駅東口交番">【新宿站东口派出所】</A>  
> MY CITY车站大楼拐角处的派出所。也会被用作约会的标志等。<font color=#00aa00>在这个派出所附近，GH看到幻象的香回首…。</font>(2001.07.01)  
> →[第7話](./ahstory1.md#7)  
> ⇒[MY CITY](./ahencyclo.md#MY CITY)  
>   
> ▼第8話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> しんじゅく Studio Alta  
> <A NAME="新宿 STUDIO ALTA">【新宿 STUDIO ALTA】</A>　[新旧比較写真](./alta.md)  
> 「ALTERNATIVE（创造新事物）」作为词源诞生于1980年。在1楼的小广场周末进行Event等，从地下2楼到地上6楼有60多家Tenant。著名的「笑っていいとも！」是在7楼的Studio中录制的。Alta的场所是作为三越新宿分店的综合食品店独立的「二幸」的所在地，从那时起就与有乐町的「\* そごう」入口、涩谷站的「八公」前一起成为典型的碰头地点。  
> 墙面的「Alta Vision」于2001年3月15日更新为世界上第一个大型街头Digital Hi-Vision(约600英寸)。从传统的Panasonic生产替换为MITSUBISHI生产。时钟(温度计)从Analogue式变成Digital等、<font color=#00aa00>从CH连载到现在，外观有很大的变化。[第8話](./ahstory1.md#8)里，隔着站在新宿站东口的入口(香和)GH就能看到。</font>  
> （\* 有楽町そごう于2000年9月24日关门。现在BigCamera正在开放。）(2001.7.8)  
> →[第8話](./ahstory1.md#8)  
> ⇒[MY CITY](./ahencyclo.md#MY CITY)  
> →[Studio Alta](http://www.studio-alta.co.jp/)（公式）  
>   
> からす  
> <A NAME="烏・鴉">【烏・鴉】</A>  
> Suzume目Karasu科Karasu属的鸟。原来是东南Asian的Jungle鸟。在日本，主要生活着「HashibutoGarasu」和「HashibosoGarasu」两种。城市地区最多的是HashibutoGarasu，有一个厚厚的喙和突出的额头。生活在城市地区的Karasu的数量在过去的十年里急剧增加。1985年开始调查时有7000只，2001年超过了25000只(根据都市鸟研究会的调查)。随之而来的是糟蹋垃圾、吓跑人的「Karasu公害」成为社会问题。原因是，城市有很多这样的路边的树木和电线杆，这是一个易于筑巢的“城市Jungle”，而且使用半透明的垃圾袋，里面的东西看得见，这些垃圾袋已经被用来处理垃圾，这丰富了Karasu的食物。虽然天生是草食性的，但是市中心的Karasu是杂食性。清晨的垃圾场是Karasu的重要粮源。<font color=#00aa00>在黎明的新宿，为了寻找香的线索而四处奔走的獠的旁边，也有大量捕捞垃圾的Karasu的身影。</font>  
> Karasu通常被作为厄运的象征而受到厌恶，但它也有作为一种以害虫为食的益鸟的一面。<font color=#00aa00>在CH中，Sub-Character的「Karasu」与「極楽蜻蜓」经常出现。</font>(2001.7.8)  
> →[第8話](./ahstory1.md#8)  
> ⇒[極楽蜻蜓](./ahencyclo.md#極楽蜻蜓)  
>   
> はじめての キス  
> <A NAME="初吻">【初吻】</A>  
> <font color=#00aa00>当獠寻找关于香的线索时，他和香的记忆又出现在他的脑海中。 的獠和香的初吻是在冬天即将到来的黎明小巷里。 每当獠喝醉了，总是在那个地方睡着，当香来接他时，他梦游般地吻了她。 他抱住香说「我爱你～～」，吻完后就梦游（假装？） 了的獠的「最差初吻」。在那条小巷里，[第7話](./ahstory1.md#7)徘徊在新宿的GH坐在那里，就是那个地方。</font>(2001.7.8)  
> →[第8話](./ahstory1.md#8)  
> ⇒[看板持ち](./ahencyclo.md#看板持ち)  
>   
> ひがしぐち かいさつ まえ  
> <A NAME="东口检票口前">【东口检票口前】</A>　[写真](./kaisatsu.md)  
> <font color=#00aa00>与CityHunter联系的「XYZ留言板」所在的地方。GH在香的幻象的指引下到达这里，终于找到留言板，但留言板本身也是幻象…。</font>(2001.7.8)  
> →[第8話](./ahstory1.md#8)  
> ⇒[XYZ](./ahencyclo.md#XYZ)  
> ⇒[伝言板](./ahencyclo.md#伝言板)  
>   
> ▼第9話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> はなぞの しんりょうじょ  
> <A NAME="花园诊疗所">【花园诊疗所】</A>  
> <font color=#00aa00>GH在新宿车站晕倒后，獠把她背到的诊所。 那里的医生是那个背着招牌的老人，他告诉了GH留言板的位置。 这个老人被獠称为「医生」，他以前在晚上做兼职工作，负责看牌子。 獠似乎已经认识大夫和护士「智」很长时间了。 诊所所在的大楼（3F）的位置可以推测是在新宿一町目附近。</font>现在的新宿1丁目的北半部，曾经被称为「花園町」。<font color=#00aa00>在同一栋大楼的1F里，有正宗手打荞麦面乌冬面的「もなめや」和Coffee&Restaurant的店。4F是「韩国美容满月」和「SHOT BAR ARGUS(YOGASU)」。</font>顺便说一下，诊所和医院的区别是基于它是否没有住院设施或少于19张床位。(2001.7.15++07.23加筆)  
> →[第9話](./ahstory1.md#9)  
> ⇒[看板持ち](./ahencyclo.md#看板持ち)  
>   
> ED  
> <A NAME="Erectile Dysfunction">【Erectile Dysfunction】</A>  
> Erectile(组织充血的结果，起立) Dysfunction(功能的障碍)缩写为「ED」。为了不充分Mokkori，或者不能维持充分的Mokkori，该术语用于定义令人满意的一发变得似乎不可能的状态。<font color=#00aa00>即使把GH抱在手上也什么都感觉不到的獠，声称成了ED，想让医生和护士智去检查。…。</font>  
> ED的原因可以分为由于心理原因造成的功能障碍和由于身体疾病造成的气质障碍；ED的发病率随着年龄的增长而增加，半数以上的40～70岁的男性被认为受ED困扰。(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
> ⇒[花园诊疗所](./ahencyclo.md#花园诊疗所)  
>   
> PengYou  
> <A NAME="PengYou・朋友">【PengYou・朋友】</A>  
> Comic Bunch连载的「蒼天の拳」中常用的台词。「朋友」在中国是「朋友」的意思。在上海语、北京语中发音为「PengYou」，在广东语中发音为「PanYao」。在日本，古代是「BaoYu」。<font color=#00aa00>诊治所旁边的大楼是「～炭火焼肉  朋友」，斜对面的大楼有一个「Reach麻雀 朋友」的招牌。</font>正如「強敵」一词被读作「とも」一样，它在「蒼天の拳」中的使用似乎超出了其原始含义的细微差别。(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
> ⇒[花园诊疗所](./ahencyclo.md#花园诊疗所)  
>   
> D502i ハイパー  
> <A NAME="D502i HYPER">【D502i HYPER】</A>  
> NTT DoCoMo的Digital mova移动电话。<font color=#00aa00>这是獠所使用的机型。铃声是Normal。在花园诊疗所里的Scene中出现，是冴子打给獠的电话。</font>  
> 三菱制造。iMode对应。2000年1月上市，业界第3款推出的「256色Color液晶」手机。利用DDLink(简易红外线通信)功能，可以向D208发送记忆拨号或来电Melody等信息。另外，和D208一样，可以制作71亿种肖像画。Color有Lilac pearl、Metallic Rose、Pure White和Diamond Silver等。(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
> ⇒[D208 HYPER](./ahencyclo.md#D208 HYPER)  
> ⇒[NTT DoCoMo](./ahencyclo.md#NTT DoCoMo)  
> ⇒[090 2X41 X00X](./ahencyclo.md#090 2X41 X00X)  
>   
> 090 2X41 X00X  
> <A NAME="090 2X41 X00X">【090 2X41 X00X】</A>  
> <font color=#00aa00>獠的移动电话来电显示、冴子的电话号码。</font>就移动电话而言，如果号码的第4位是「2」，第6位是「4」，那么你加入的是移动电话会社；第5位是'0'的情况为「関西Cellular」、是'1-7'的情况为「NTT DoCoMo」、是'8'的情况为「J-PHONE東北」、是'9'的情况为「IDO関東」。<font color=#00aa00>由此推断，冴子的移动电话也是「NTT DoCoMo」的可能性很高。</font>(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
> ⇒[D502i HYPER](./ahencyclo.md#D502i HYPER)  
>   
> ごくらく 蜻蜓  
> <A NAME="極楽蜻蜓">【極楽蜻蜓】</A>  
> <font color=#00aa00>在CH上熟悉的Sub-Character。第9话首次登场。</font>(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
>   
> PillCase  
> <A NAME="Pillcase">【Pillcase】</A>  
> 用于携带日常药物等的Case，而不是用来放那个「Pill」的，这一点不要弄错。 在江户时代，密封箱被用作PillCase。  
> <font color=#00aa00>在被送进花园诊疗所的GH的唯一携带品--PillCase中，发现装有免疫抑制剂「Prograf」的药片。</font>(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
> ⇒[Prograf](./ahencyclo.md#Prograf)  
>   
> Prograf  
> <A NAME="Prograf">【Prograf】</A>  
> Tacrolimus水合物的商品名(Tacrolimus，1984年在筑波山麓土壤中放线菌中发现的生物活性物质)。藤泽药品工业开发的免疫抑制剂，开发编号“FK506”。主要用于预防肝移植和肾移植排斥反应，以及器官移植中其他免疫抑制剂用于治疗无效排斥反应。在日本批准之前，在其他国家被广泛使用。关于心脏移植时的排斥反应的抑制效果，在肺、胰、小肠移植的同时，在日本国内2000年2月申请了适应的扩大的认可。剂型有Capsule0.5 mg(识别编号F607)、1 mg(识别编号<font color=#00aa00>F617</font>)、5 mg(识别编号F657)、注射液、颗粒等。作为副作用，必须特别注意糖尿病，肾功能障碍，高纯血症，四肢颤抖，高脂血症等。Grapefruit增加了药物的血药浓度，因此严禁摄取。<font color=#00aa00>医生发现Prograf F617的药片放在GH的唯一携带的PillCase中，看到GH胸前的一个大疤痕，想到了一件事…。</font>(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
> ⇒[免疫抑制剤](./ahencyclo.md#免疫抑制剤)  
>   
> めんえき よくせいざい  
> <A NAME="免疫抑制剤">【免疫抑制剤】</A  
> 人体具有免疫机制，具有排斥外部异物的功能，如病原体和移植器官(不包括同卵双胞胎之间的移植)。用于抑制这种排斥反应的是免疫抑制剂。心脏移植时的免疫抑制疗法一般使用「Ciclosporin或Prograf \* 」、「Azathioprine或Mycophenolate mofetil」、「以Predonin为代表的Steroid」三剂并用疗法。只要移植器官继续发挥作用，免疫抑制剂就必须服用。免疫被抑制也意味着对病原菌的抵抗力变弱，需要充分注意感染症。 
> （ \* Prograf效果是Ciclosporin的10-100倍。严禁联合使用Ciclosporin和Prograf）(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
> ⇒[Prograf](./ahencyclo.md#Prograf)  
>   
> マッキントッシュ  
> <A NAME="Macintosh">【Macintosh】</A>  
> 美国Apple公司的Personal Computer。<font color=#00aa00>在花园诊疗所，医生在调查「日本的心脏移植者List~」时使用的电脑好像是「Power Macintosh 8000 or 9000Series（1994年～1997年生産）」。Display是SONY的Trinitron Monitor。Printer是ALPS的MDSeries</font>(目前，ALPS公司已经退出Printer的销售)。20世纪90年代当时，医疗关系和Design关系中特别使用Mac的情况很多(现在呢？)。日本的心脏移植在1968年的初次移植手术造成了很多问题，直到1998年才重新开始。(2001.7.15)  
> →[第9話](./ahstory1.md#9)  
> ⇒[花园诊疗所](./ahencyclo.md#花园诊疗所)  
>   
> ▼第10話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> ゆうれい かっ  
> <A NAME="幽霊かっ">【幽霊かっ】</A>  
> 鬼魂是死人的灵魂，或未能成佛而出现在这个世界上的人。<font color=#00aa00>在海坊主的建议下莫名其妙地寻找香的獠问道「你说香回来了是怎么回事！？」并在当晚冲进了Cat's。</font>(2001.7.23)（译注：待校对）  
> →[第10話](./ahstory1.md#10)  
>   
> 27 ばん  
> <A NAME="27号">【27号】</A>  
> <font color=#00aa00>GH的童年的代号。GH没有名字，在“组织”进行训练的时候也用代号来称呼。</font>(2001.7.23)  
> →[第1話](./ahstory1.md#1)  
> →[第10話](./ahstory1.md#10)  
>   
> D101 ハイパー  
> <A NAME="D101 HYPER">【D101 HYPER】</A>  
> NTT DoCoMo的Digital mova移动电话。<font color=#00aa00>医生使用的机型。在花园诊疗所的病房里，GH正在和镜子里的香对话的时候，从门外站着听着她的医生的移动电话响了起来。那个电话是来自獠的，发信者的名字是「冴羽獠」(獠的片假名)。</font>三菱製。1995年12月发售。「带语音标签的记忆拨号」、「配备来电Vibrator功能」是当时的卖点。Flip式。(2001.7.23)  
> →[第10話](./ahstory1.md#10)  
> ⇒[NTT DoCoMo](./ahencyclo.md#NTT DoCoMo)  
>   
> ▼第11話　[　[PageTop](./ahencyclo.md#0)　]  
>   
<!--
<FONT SIZE="+1" COLOR="#FF1493">Brioni<BR><A NAME="">【VRIONI】</A></FONT><BR>Greekの寝具Maker。<FONT COLOR="#008B8B">GH从诊疗所逃了出来，在半夜里，她试图对着街上橱窗里他的影子叫出香。 橱窗上写着品牌名称。</FONT><FONT SIZE="-1" COLOR="#C2C2CD">(2001.7.29)</FONT><BR>→<A HREF="ahstory1.html#11">第11話</A><BR><BR>
-->  
> しんじゅく グランド ホテル  
> <A NAME="SINJUKU GRAND HOTEL">【SINJUKU GRAND HOTEL】</A>  
> <font color=#00aa00>秘密来到日本的李大人，在根堀会系的暴力团年轻头目饼山，用来听GH在新宿大闹的时候的事件的事的酒店。在谈话过程中，李大人从外面被什么人狙击了。饼山说，那个房间半径1公里以内没有可以狙击的地方，所以狙击者是从1公里以外的地方瞄准的。</font>(2001.7.29)  
> →[第11話](./ahstory1.md#11)  
> ⇒[1公里](./ahencyclo.md#1公里)  
>   
> パン  
> <A NAME="幇">【幇】</A>  
> 在中国和台湾，具有强烈地方色彩的黑社会被称为「幇（Bang）」（基于经济实力的黑社会，如在经济特区，被称为「会」或「门」）。<font color=#00aa00>根堀会系暴力团若头的饼山，提议请秘密来到日本的李大人与新宿帮派的领导人一起干杯...... 在这种情况下，饼山可能以中国人的方式称呼自己的黑帮组织，即「新宿帮」。 饼山的黑社会组织（干部？） 似乎欠李大人某种恩情。</font>(2001.7.29)  
> →[第11話](./ahstory1.md#11)  
>   
> 1公里  
> <A NAME="1公里">【1公里】</A>  
> 1公里＝1000米。高性能Scope的高精度Rifle也是勉强的最大有效射程距离。所谓狙击，随着距离越来越远，风向、天气、温度和湿度、光量、子弹本身的精度等因素会错综复杂地交织在一起。在远距离射击中，枪所要求的性能自不必说，狙击手自身的心脏跳动和呼吸的Rhythm也会对结果产生影响。如果1公里以外的话，打到Target是极其困难的招数。实际上，专业人员也很少有600 Yard(549m)以上的狙击。<font color=#00aa00>狙击了「SINJUKU GRAND HOTEL」房间里的李大人的狙击手，一定是超一流的本领吧。隔着Glass的狙击</font>更难做到，因为子弹的轨道在穿过Glass时会偏离。(2001.7.29)  
> →[第11話](./ahstory1.md#11)  
> ⇒[SINJUKU GRAND HOTEL](./ahencyclo.md#SINJUKU GRAND HOTEL)  
>   
> ▼第12話　[　[PageTop](./ahencyclo.md#0)　]  
>   
> チェンダオフェイ  
> <A NAME="正道会">【正道会】</A>  
> <font color=#00aa00>训练GH成为杀手的"组织"的名称。 新宿区控制着歌舞伎町。 台湾最大的黑幇（Mafia），李大人就是正龙头(大Boss)。 「朱雀部隊」作为一个杀人部队而存在。</font>(2001.8.4)  
> →[第12話](./ahstory1.md#12)  
> ⇒[朱雀部隊](./ahencyclo.md#朱雀部隊)  
>   
<!--
<FONT SIZE="+1" COLOR="#FF1493">たんご<BR><A NAME="">【単語】</A></FONT><BR>説明<FONT COLOR="#008B8B">設定</FONT><FONT SIZE="-1" COLOR="#C2C2CD">(2001.0.0)</FONT><BR>→<A HREF="ahstory1.html#">第話</A><BR><BR>
-->

---

ver.010730

● [back](./jmc2.md) ●