http://www.netlaputa.ne.jp/~arcadia/ah/ahstory.html

Story

---

**◆Story**

> ◆**Angel Heart ?**  
> 　2001年5月开始，在新潮社创刊的周刊Comic Bunch上连载。据说是北条司的代表作「CITY HUNTER」(1985年～1991年/集英社·周刊少年JUMP连载)的事实上的续篇。破坏、创造、构建…。这部历经5年构想的作品是漫画家职业生涯20周年的集大成，是一部令人震惊的作品。「<font color=#ff0000>\*</font>什么是生命！什么是爱情！以男和女的激烈剧情」而开始的「Angel Heart」。目前还不清楚「CITY HUNTER」的舞台设定在多大程度上得到了保留。  
> （ <font color=#ff0000>\*</font>参考：[Coamix HP](http://www.coamix.co.jp/)）  
>   
>   
> ◆**Outline**  
> 　一个代号为GlassHeart的女孩被一个 "组织"训练成杀手，她再也无法忍受被该组织操纵去杀人，试图自杀。 经过一年的沉睡，GlassHeart苏醒过来，在内心声音的指引下......她心中的香的意识......来到命运之城新宿。新宿是捐献心脏抢劫案发生的地方。在新宿，她找到了一直在寻找在捐献者心脏抢劫事件中被夺去的“香的心跳”的冴羽良的身影。究竟等待他们的命运是什么！？  
>   
>   
> ◆**Story Digest**  
> 　Angel Heart 各Episode内容提要  
>   
> 　◆「Angel Heart」1stSeason(調整中)  
>   
> 　◆「Angel Heart」2ndSeason(調整中)  
>   
> 　◆[「Angel Heart」听漫](./ahstorycc.md)  
>   
> 　◆[本刊刊载时与总集篇1的区别](./ahstorydiff.md)new  
>   
>   
>   
> ◆**备注**  
> 　CityHunter的动画第25集中，出现了名为「Angel Heart」的金发美女Sweeper，但似乎与本作品没有任何关系。  
> 
> ---
> 
>   

---

ver.010619

● [back](./jmc2.md) ●