http://www.netlaputa.ne.jp/~arcadia/ah/ahbook_bc.html

Angel Heart 相关书籍

---

> BUNCH COMICS  

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="1">Angel Heart（エンジェル・ハート）第1巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771001-7</TD></TR>
<TR><TD>第1話 運命に揺れる暗殺者<BR>第2話 絶叫する夢の記憶<BR>第3話 あの人に会いたくて<BR>第4話 新宿で大暴れ！<BR>第5話 思い出に導かれて<BR>第6話 思い出との再会<BR>第7話 伝言板への助走<BR>第8話 出会いと言う名の再会<BR>第9話 衝撃の邂逅（かいこう）<BR>第10話 こころとの対話</TD><TD ALIGN="CENTER">2001年10月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="2">Angel Heart（エンジェル・ハート）第2巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771012-2</TD></TR>
<TR><TD>第11話 事実への昂揚<BR>第12話 衝撃を超えた真実<BR>第13話 緊迫する新宿<BR>第14話 衝撃のフラッシュバック<BR>第15話 李兄弟との宿運<BR>第16話 出生の秘密<BR>第17話 運命の対面<BR>第18話 グラスハートのときめき<BR>第19話 ミステリアスなリョウ<BR>第20話 宣戦布告！！<BR>第21話 戦士の決意<BR>第22話 生き続ける理由</TD><TD ALIGN="CENTER">2001年12月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="3">Angel Heart（エンジェル・ハート）第3巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771026-2</TD></TR>
<TR><TD>第23話 戦士たちの絆<BR>第24話 忘れていたもの<BR>第25話 甦った過去<BR>第26話 惜しみない命<BR>第27話 訓練生時代の想い出<BR>第28話 命にかえても<BR>第29話 李大人の制裁<BR>第30話 失われた名前<BR>第31話 最後の決着(ケリ)<BR>第32話 パーパ<BR>第33話 一年ぶりの衝撃</TD><TD ALIGN="CENTER">2002年3月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="4">Angel Heart（エンジェル・ハート）第4巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771045-9</TD></TR>
<TR><TD>第34話 退院<BR>第35話 パパと娘の初デート<BR>第36話 船上の出会いと別れ<BR>第37話 影のパーパ<BR>第38話 おかえり<BR>第39話 李大人からの贈り物<BR>第40話 親不孝<BR>第41話 陳(チン)老人の店<BR>第42話 おしゃれ<BR>第43話 新宿の洗礼<BR>第44話 依頼人第一号</TD><TD ALIGN="CENTER">2002年7月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="5">Angel Heart（エンジェル・ハート）第5巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771065-3</TD></TR>
<TR><TD>第45話 殺し屋の習性<BR>第46話 リョウとベンジャミンと女社長<BR>第47話 パパを捜して！！<BR>第48話 パパの似顔絵<BR>第49話 ターニャの本当の笑顔<BR>第50話 狙撃準備完了<BR>第51話 風船のクマさん<BR>第52話 香との会話<BR>第53話 冴子の誕生日<BR>第54話 夢の中の出会い<BR>第55話 槇村兄妹</TD><TD ALIGN="CENTER">2002年11月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="6">Angel Heart（エンジェル・ハート）第6巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771080-7</TD></TR>
<TR><TD>第56話 二匹の野良犬<BR>第57話 野良犬の告白<!--原題「押しかけC・H 」--><BR>第58話 親子の絆<BR>第59話 C・Hが犯人！？<BR>第60話 心の傷<BR>第61話 孤独な少女<BR>第62話 媽媽（マーマ）の心臓<BR>第63話 夢を守る！<BR>第64話 突きつけられた真実<BR>第65話 香瑩の決意<BR>第66話 天使の心</TD><TD ALIGN="CENTER">2003年3月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="7">Angel Heart（エンジェル・ハート）第7巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771096-3</TD></TR>
<TR><TD>第67話 不公平な幸せ<BR>第68話 優しい心音(おと)<BR>第69話 笑顔の二人<BR>第70話 香瑩の変化<BR>第71話 依頼は殺し！？<BR>第72話 伝わらぬ思い<BR>第73話 不器用な男<BR>第74話 別れの五日元玉<BR>第75話 命より大切な絆<BR>第76話 もう一度あの頃に<!--原題「もう一度あの頃に… 」--><BR>第77話 本当の家族</TD><TD ALIGN="CENTER">2003年6月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="8">Angel Heart（エンジェル・ハート）第8巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771113-7</TD></TR>
<TR><TD>第78話 遺された指輪<BR>第79話 冴子からのＸＹＺ<BR>第80話 ターゲットは香瑩<BR>第81話 心臓(かおり)の涙<BR>第82話 危険な匂い<BR>第83話 哀しき連続殺人犯<BR>第84話 悪魔のエンディング<BR>第85話 愛に飢えた悪魔<BR>第86話 思い出の場所<BR>第87話 唯一の愛情<BR>第88話 あたたかい銃弾</TD><TD ALIGN="CENTER">2003年9月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="9">Angel Heart（エンジェル・ハート）第9巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771125-0</TD></TR>
<TR><TD>第89話 遅れて来た幸せ<BR>第90話 初めての感情<BR>第91話 本当の表情(かお)<BR>第92話 これが、恋？<BR>第93話 計り知れぬ想い<!--原題「恋より深い感情」 --><BR>第94話 父親が娘を想う気持ち<BR>第95話 私、恋してます<BR>第96話 遠い約束<BR>第97話 幸せな笑顔<BR>第98話 初恋、涙の別れ<BR>第99話 恋する人の気持ち</TD><TD ALIGN="CENTER">2003年12月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="10">Angel Heart（エンジェル・ハート）第10巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771139-0</TD></TR>
<TR><TD>第100話 妹を捜して<!--原題「妹を捜して！」 --><BR>第101話 妹は幸せだった？<BR>第102話 心臓(かおり)の反応<BR>第103話 新宿の天使<BR>第104話 奇跡の適合性<BR>第105話 思い出の街<BR>第106話 小さな願い<BR>第107話 槇村の決意<BR>第108話 Ｃ・Ｈ(シティーハンター)の正体<BR>第109話 リョウからの依頼<BR>第110話 一途(バカ)な男</TD><TD ALIGN="CENTER">2004年3月15日<BR>505日元+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="11">Angel Heart（エンジェル・ハート）第11巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771154-4</TD></TR>
<TR><TD>第111話 早百合の旅立ち<BR>第112話 依頼人はひったくり！？<BR>第113話 ベイビートラブル！<BR>第114話 恋人同士の誤解<BR>第115話 聖夜の奇跡<BR>第116話 運命の再会<BR>第117話 二人の変化<BR>第118話 白蘭の任務<BR>第119話 スコープの中の真実<!-- 原題「スコープの中の真意」 --><BR>第120話 神から授かりし子<BR>第121話 初めての愛情</TD><TD ALIGN="CENTER">2004年6月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="12">Angel Heart（エンジェル・ハート）第12巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771173-0</TD></TR>
<TR><TD>第122話 白蘭の決意<BR>第123話 悲しみの決行日<BR>第124話 生きる糧<BR>第125話 受け継がれし命<BR>第126話 白蘭からの手紙<BR>第127話 心臓(こころ)の声を信じて<BR>第128話 心臓(こころ)の共振<BR>第129話 姉(さおり)の想い<BR>第130話 綾音(あーや)の嘘<BR>第131話 高畑の嘘<BR>第132話 明かされた真実</TD><TD ALIGN="CENTER">2004年9月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="13">Angel Heart（エンジェル・ハート）第13巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771184-6</TD></TR>
<TR><TD>第133話 心臓移植のリスク<BR>第134話 生きて…！<BR>第135話 生きた証<BR>第136話 高畑のお守(まも)り<BR>第137話 冴子と謎の女の子<BR>第138話 都会の座敷童<BR>第139話 冴子のお返し<BR>第140話 穏やかな夢<BR>第141話 海坊主と座敷童<BR>第142話 汚れなき心<BR>第143話 ママへの想い</TD><TD ALIGN="CENTER">2004年11月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="14">Angel Heart（エンジェル・ハート）第14巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771200-1</TD></TR>
<TR><TD>第144話 いつも一緒<BR>第145話 暗闇に見えた夕陽<BR>第146話 優しき店長(マスター)<BR>第147話 &quot;危険&quot;な大女優！<BR>第148話 ジョイの事情<BR>第149話 思い人はこの街に？<BR>第150話 愛しき人の形見<BR>第151話 共同生活開始！<BR>第152話 ミキの幸せ<BR>第153話 親子のように<BR>第154話 ジョイの行き先</TD><TD ALIGN="CENTER">2005年2月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="15">Angel Heart（エンジェル・ハート）第15巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771220-6</TD></TR>
<TR><TD>第155話 告白…！<BR>第156話 家族の写真<BR>第157話 リョウのケータイライフ<BR>第158話 愛される理由<BR>第159話 <!-- 原題「私、恋してます！」 -->ラブ サ・イ・ン<BR>第160話 ホレた男は謎だらけ！？<BR>第161話 恋の覚悟<BR>第162話 リョウの大失敗<BR>第163話 女同士の酒<BR>第164話 それぞれの場所<BR>第165話 楊(ヤン)からの依頼？</TD><TD ALIGN="CENTER">2005年6月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="16">Angel Heart（エンジェル・ハート）第16巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771236-2</TD></TR>
<TR><TD>第166話 愛は地球を救う！？<BR>第167話 楊芳玉(ヤンファンユィ)の仕事<BR>第168話 天国と地獄！？<BR>第169話 黒幕現る！！<BR>第170話 楊、始動！<BR>第171話 再会…！！<BR>第172話 母なる思い<BR>第173話 アカルイミライ！？<BR>第174話 麗泉(れいせん)の悩み<BR>第175話 恋人達の未来<BR>第176話 悲しい二人</TD><TD ALIGN="CENTER">2005年9月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="17">Angel Heart（エンジェル・ハート）第17巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771252-4</TD></TR>
<TR><TD>第177話 男の信念<BR>第178話 透視の代償<BR>第179話 笑顔の未来<BR>第180話 引き寄せられる運命<BR>第181話 未来を変える男<BR>第182話 信宏が教えてくれた事<BR>第183話 麗子の未来<BR>第184話 義父(ちち)はつらいよ！<BR>第185話 海坊主と先生<BR>第186話 変質者は海坊主！？<BR>第187話 ミキが行方不明！？</TD><TD ALIGN="CENTER">2005年12月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="18">Angel Heart（エンジェル・ハート）第18巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771267-2</TD></TR>
<TR><TD>第188話 海学級へ行こう！<BR>第189話 変質者、現る！<BR>第190話 バスジャック！<BR>第191話 恐怖のカーチェイス！<BR>第192話 親子の電話<BR>第193話 怒りの一撃！<BR>第194話 親子の覚悟<BR>第195話 夏休みの終わり<BR>第196話 楊(ヤン)、再来！！<BR>第197話 指輪の記憶<BR>第198話 クリスマスプレゼント</TD><TD ALIGN="CENTER">2006年3月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="19">Angel Heart（エンジェル・ハート）第19巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771277-X</TD></TR>
<TR><TD>第199話 異国からの求婚者！<BR>第200話 妄想超特急がやってきた！<BR>第201話 皇子と香瑩の初デート<BR>第202話 昨日の敵は今日の恋人！<BR>第203話 マオの交渉<BR>第204話 家族の風景<BR>第205話 日常に潜むアクマ<BR>第206話 皇子の苦悩<BR>第207話 酒と泪と皇子と母親！？<BR>第208話 海辺の告白！<BR>第209話 迸る想い！</TD><TD ALIGN="CENTER">2006年6月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="20">Angel Heart（エンジェル・ハート）第20巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-10-771295-8</TD></TR>
<TR><TD>第210話 マオの決断<BR>第211話 男二人、車中にて<BR>第212話 長生きの代償<BR>第213話 男の性(サガ)！<BR>第214話 オペレーション ナシクズシ！<BR>第215話 Ｃ・Ｈ(シティーハンター)資格テスト！<BR>第216話 追う女、待つ女<BR>第217話 楊(ヤン)の生きる理由<BR>第218話 大幸運期到来！！<BR>第219話 私がパパです！<BR>第220話 謎のお父さん！</TD><TD ALIGN="CENTER">2006年9月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">※[ ]内为初次限量版。附带「しらけ乌鸦」&「100t Hammer」两大道具的挂件。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF1493"><A NAME="21">Angel Heart（エンジェル・ハート）第21巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771313-1<BR>※[978-4-10-771308-7]</TD></TR>
<TR><TD>第221話 １８歳はシゲキがお好き！？<BR>第222話 カギは香瑩にあり！？<BR>第223話 紗世は名探偵！？<BR>第224話 重ねる嘘、重なる罪<BR>第225話 父のいる店<BR>第226話 真実と紗世<BR>第227話 香瑩、入学す！<BR>第228話 私たちの学校<BR>第229話 想い、街に息づいて<BR>第230話 ラブレター・パニック！<BR>第231話 僕ら、シャンイン親衛隊！<!-- 原題「ボクら、シャンイン親衛隊！？」 --></TD><TD ALIGN="CENTER">2007年1月15日<BR>530日元（含税）<BR><BR>※[780日元（含税）]</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="22">Angel Heart（エンジェル・ハート）第22巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771334-6</TD></TR>
<TR><TD>第232話 机男と親衛隊(シンエータイ)！<BR>第233話 嫌な予感<BR>第234話 いい子の行き着く場所<BR>第235話 肉弾戦<BR>第236話 危険な出会い<BR>第237話 カメレオン<BR>第238話 花園学校の未来<BR>第239話 乙玲(イーリン)の姉<BR>第240話 カメレオンの誘惑<BR>第241話 狙撃<BR>第242話 香瑩の答え</TD><TD ALIGN="CENTER">2007年5月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="23">Angel Heart（エンジェル・ハート）第23巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771350-6</TD></TR>
<TR><TD>第243話 覚悟<BR>第244話 恋はアフターで！<BR>第245話 初アフターはストーカー付！<BR>第246話 雲のように<BR>第247話 悲痛な願い<BR>第248話 気づかぬ優しさ<BR>第249話 見えない景色<BR>第250話 暖かい場所へ<BR>第251話 意外な弱点<BR>第252話 初めての経験<BR>第253話 表への一歩</TD><TD ALIGN="CENTER">2007年8月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="24">Angel Heart（エンジェル・ハート）第24巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771367-4</TD></TR>
<TR><TD>第254話 嫉妬<BR>第255話 仲間<BR>第256話 新スポンサー<BR>第257話 陳さんの暴走<BR>第258話 及第点の答え<BR>第259話 香瑩の怒り<BR>第260話 消えない記憶<BR>第261話 武道館決戦<BR>第262話 仁志の涙<BR>第263話 私の立ち位置<BR>第264話 夏の再会</TD><TD ALIGN="CENTER">2007年11月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="25">Angel Heart（エンジェル・ハート）第25巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771381-0</TD></TR>
<TR><TD>第265話 海坊主と葉月<BR>第266話 誤解<BR>第267話 葉月の父<BR>第268話 ファルコンのお守り<BR>第269話 墓参り<BR>第270話 ファルコンの秘密<BR>第271話 キバナコスモス<BR>第272話 大好きなんです<BR>第273話 親子の愛<BR>第274話 葉月の悩み<BR>第275話 カメレオンの罠</TD><TD ALIGN="CENTER">2008年2月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="26">Angel Heart（エンジェル・ハート）第26巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771397-1</TD></TR>
<TR><TD>第276話 招かれざる客<BR>第277話 虎の尾<BR>第278話 最凶悪危険人物<BR>第279話 一億の勝負<BR>第280話 母の想い<BR>第281話 ぷるん<BR>第282話 カメレオン動く<BR>第283話 灯(あかり)<BR>第284話 ジンクス<BR>第285話 不安な夜<BR>第286話 潮騒(しおさい)</TD><TD ALIGN="CENTER">2008年5月15日<BR>530日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="27">Angel Heart（エンジェル・ハート）第27巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771420-6</TD></TR>
<TR><TD>第287話 小さなＸＹＺ<BR>第288話 家族のために<BR>第289話 ロスタイム<BR>第290話 損な役回り<BR>第291話 罪悪感<BR>第292話 笑い泣き<BR>第293話 傲慢<BR>第294話 懺悔と供養<BR>第295話 最期の言葉<BR>第296話 本当の姿<BR>第297話 親子の時間</TD><TD ALIGN="CENTER">2008年9月15日<BR>540日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="28">Angel Heart（エンジェル・ハート）第28巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771444-2</TD></TR>
<TR><TD>第298話 幸せの涙<BR>第299話 陰影<BR>第300話 恋の季節<BR>第301話 潜入<BR>第302話 身代わり<BR>第303話 金木犀(きんもくせい)の香り<BR>第304話 似た者同士<BR>第305話 確かな鼓動<BR>第306話 幸せのベール<BR>第307話 誓いの言葉<BR>第308話 新婚旅行</TD><TD ALIGN="CENTER">2008年12月15日<BR>540日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">※在目录页中，巧妙的名称</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF1493"><A NAME="29">Angel Heart（エンジェル・ハート）第29巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771468-8</TD></TR>
<TR><TD>第309話 離れていても<BR>第310話 真実(マコト)<BR>第311話 宣戦布告<BR>第312話 影武者<BR>第313話 長い一日<BR>第314話 怪我の功名 <FONT SIZE="-1" COLOR="RED">※</FONT><BR>第315話 仕組まれた黙秘<BR>第316話 冴子の覚悟<BR>第317話 現行犯逮捕<BR>第318話 現場復帰<BR>第319話 黒幕</TD><TD ALIGN="CENTER">2009年3月15日<BR>540日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="30">Angel Heart（エンジェル・ハート）第30巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771496-1</TD></TR>
<TR><TD>第320話 制裁の行方<BR>第321話 激昂<BR>第322話 カメレオンの遺産<BR>第323話 怪訝なＸＹＺ<BR>第324話 笑顔の理由<BR>第325話 老人(かれ)の貴物(おもいで)<BR>第326話 対極の男<BR>第327話 昼行灯(ひるあんどん)<BR>第328話 敵情視察<BR>第329話 潜入！ ＣＨ<BR>第330話 人質交換</TD><TD ALIGN="CENTER">2009年7月15日<BR>540日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="31">Angel Heart（エンジェル・ハート）第31巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771530-2</TD></TR>
<TR><TD>第331話 只今参上！<BR>第332話 君だけのヒーロー<BR>第333話 いつかの笑顔<BR>第334話 天使のサイモン<BR>第335話 カエルの子はカエル<BR>第336話 ふたつの命<BR>第337話 ちょっとの特別<BR>第338話 空のＣ・Ｈ(シティハンター)<BR>第339話 繋いだ手から<BR>第340話 小さな依頼人<BR>第341話 涙の理由</TD><TD ALIGN="CENTER">2009年11月15日<BR>540日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="32">Angel Heart（エンジェル・ハート）第32巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771554-8</TD></TR>
<TR><TD>第342話 偽りの再会<BR>第343話 会いたい<BR>第344話 ママを捜して…<BR>第345話 忘却の代償<BR>第346話 嘘だと言って…<BR>第347話 優しい嘘<BR>第348話 たくさんの月<BR>第349話 希望の風<BR>第350話 陳(ちん)さんの策謀<BR>第351話 薔薇と御曹司<BR>第352話 巡る想い<!-- 原題「笑顔の理由」 --></TD><TD ALIGN="CENTER">2010年3月15日<BR>540日元（含税）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="GB">Angel Heart（エンジェル・ハート）公式GuideBook</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">978-4-10-771398-8</TD></TR>
<TR><TD>第1章 Character Guide<BR>第2章 新宿Town Map<BR>第3章 Special Interview<BR>第4章 Illustration Gallery<BR>第5章 Story Digest<BR>第6章 Angel Heart×新宿鮫<BR>『新宿鮫』特別編 大沢在昌<BR>『Angel Heart』番外編「一夜の友情」北条司<BR><BR>Column<BR>・<BR>・</TD><TD ALIGN="CENTER">2008年5月25日<BR>700日元（含税）</TD></TR>
</TABLE>

<!--
<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF1493"><A NAME="">Angel Heart（エンジェル・ハート）第-巻</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[]</TD><TD ALIGN="CENTER">4-10-</TD></TR>
<TR><TD>目次</TD><TD ALIGN="CENTER">2002年月日<BR>505日元+税</TD></TR>
</TABLE>

<CAPTION ALIGN="LEFT" VALIGN="BOTTOM"><FONT COLOR="RED"></FONT></CAPTION>
-->


---

ver.011009

● [back](http://www.netlaputa.ne.jp/~arcadia/ah/ahbook.html) ●