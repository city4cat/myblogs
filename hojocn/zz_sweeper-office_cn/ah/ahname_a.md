http://www.netlaputa.ne.jp/~arcadia/ah/ahname_a.html

Angel Heart 人名辞典

---

あ行  
|　[あ](./ahname_a.md#a)　|　[い](./ahname_a.md#i)　|　[う](./ahname_a.md#u)　|　[え](./ahname_a.md#e)　|　[お](./ahname_a.md#o)　|

> あ  
>   
> - Nothing -  
>   
>   
> い  
>   
> - Nothing -  
>   
>   
> う  
>   
> うみぼうず【海坊主】  
> 元スイーパーで、喫茶店「CAT'S EYE」を営むマスター。目は見えないが、その分カンが鋭い。  
> →初登場[第3話](./ahstory1.md#3)  
>   
>   
> え  
>   
> - Nothing -  
>   
>   
> お  
>   
> - Nothing -  
>   

---

ver.010619

● [back](./ahname.md) ●