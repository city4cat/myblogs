http://www.netlaputa.ne.jp/~arcadia/ah/ahname_ta.html

Angel Heart 人名辞典

---

た行  
|　[た](./ahname_ta.md#ta)　|　[ち](./ahname_ta.md#ti)　|　[つ](./ahname_ta.md#tu)　|　[て](./ahname_ta.md#te)　|　[と](./ahname_ta.md#to)　|

> た  
>   
> - Nothing -  
>   
>   
> ち  
>   
> チョウ【張】  
> "組織"＝台湾最大のマフィア「正道会」の幹部（青龍＝チンロン）。李大人に一番信頼のあるスキンヘッドの男。  
> →初登場[第6話](./ahstory.md#6)  
>   
>   
> つ  
>   
> - Nothing -  
>   
>   
> て  
>   
> - Nothing -  
>   
>   
> と  
>   
> ドク【ドク】  
> 花園診療所の医師。夜は新宿の繁華街で看板持ちのバイトをやっている。GHに伝言板の場所を教えた人物。  
> →初登場[第7話](./ahstory1.md#7)  
>   
> ともちゃん【智ちゃん】  
> ドクが医師をする花園診療所の看護婦。リョウにEDを診てくれと言われ…。  
> →初登場[第9話](./ahstory1.md#9)  
>   

---

ver.010804

● [back](./ahname.md) ●