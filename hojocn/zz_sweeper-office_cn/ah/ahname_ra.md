http://www.netlaputa.ne.jp/~arcadia/ah/ahname_ra.html

Angel Heart 人名辞典

---

ら行  
|　[ら](./ahname_ra.md#ra)　|　[り](./ahname_ra.md#ri)　|　[る](./ahname_ra.md#ru)　|　[れ](./ahname_ra.md#re)　|　[ろ](./ahname_ra.md#ro)　|

> ら  
>   
> - Nothing -  
>   
>   
> り  
>   
> リー-たいじん【李大人】  
> "組織"＝台湾最大のマフィア「正道会」の正龍頭（大ボス）。自殺を図ったグラス・ハートを生かし続け、覚醒した彼女を使って何かを企むが。  
> →初登場[第1話](./ahstory1.md#1)  
>   
> リン【林】  
> "組織"の構成員。後輩の金と共にグラスハートの病室の監視の任務を負う。内部事情に詳しい。  
> →初登場[第1話](./ahstory1.md#1)  
>   
>   
> る  
>   
> - Nothing -  
>   
>   
> れ  
>   
> - Nothing -  
>   
>   
> ろ  
>   
> - Nothing -  
>   

---

ver.010804

● [back](./ahname.md) ●