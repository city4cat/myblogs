http://www.netlaputa.ne.jp/~arcadia/ah/AH1500/shinjuku_xyz2.html



「Memory of X・Y・Z」ReportPage(2/7)

* * *

電照Sheet，留言板和Comic Gallary合在一起，一共10面。  
  
  
  

![](./image/dens01.jpg)  
Colorful的许多电照Sheet围绕着Event会场。
  
  
![](./image/dens02.jpg)  
Angel Heart・Character Introduction中登场人物的介绍。  
  
  
![](./image/dens03.jpg)  
在介绍文字的下面，还附有北条先生的Comment。  
  
  
![](./image/dens04.jpg)  
介绍文字和Comment与16日发售的GuideBook上的内容相同。  
  
  
![](./image/dens05.jpg)  
如果你有兴趣，不要错过Angel Heart官方GuideBook。  
  
  
![](./image/dens06.jpg)  
除了AH的新潮社，还有CH完整版的德间书店、  
Anime AH放映中的Kids Station也有其名字。
  
  
![](./image/dens07.jpg)  
Comics也是熟悉的插图。
  
  
![](./image/dens08.jpg)  
横向的电照Sheet也非常有力。 
  
  
![](./image/dens09.jpg)  
仔细一看，黑色背景也成了Comic的一格一格。  
（※为清晰起见，我们对图像进行了增强处理。）  
  
  

[**1** ←BACK](shinjuku_xyz1.md)≪

≫[复活的留言板见→ **Page3**](shinjuku_xyz3.md)

* * *

ver.080522

● [Back](javascript:history.go(-1)) ●