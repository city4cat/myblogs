http://www.netlaputa.ne.jp/~arcadia/ah/AH1500/shinjuku_xyz7.html



「Memory of X・Y・Z」ReportPage(7/7)

* * *

在Special Event期间，会场分发了AH特制Eco-Tote和3种1Set的特制原画Postcard。  
  
  
  

![](./image/ecobag.jpg)  
AH特製Eco-Tote Bag。  
17日、18日到会場的前1500名。  
  
  
![](./image/nakami.jpg)  
里面的是CH完整版的传单(背面是Cat‘s Eye)和、  
Anime AH放映中的Kids Station的节目表等。  
  
  
![](./image/postcard.jpg)  
最新号Bunch赠送的明信片（每天前100名）。  
试着把它摆在实车上作为纪念。。。  
  
  

[**6** ←BACK](shinjuku_xyz6.md)≪

结束　≫[返回最初→ **Page1**](shinjuku_xyz1.md)

* * *

ver.080522

● [Back](javascript:history.go(-1)) ●