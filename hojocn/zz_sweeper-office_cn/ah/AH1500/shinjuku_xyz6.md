http://www.netlaputa.ne.jp/~arcadia/ah/AH1500/shinjuku_xyz6.html



「Memory of X・Y・Z」ReportPage(6/7)

* * *

以Event用新画插图为基础制作的Mini Cooper & 1500万t Hammer巨大Figure。  
  
  
  

![](./image/mini01.jpg)  
正面的Up。  
  
  
![](./image/mini02.jpg)  
右侧斜前方。  
  
  
![](./image/mini03.jpg)  
左侧斜前方。  
  
  
![](./image/mini04.jpg)  
埋在车顶上，突破1500万部纪念的“1500万t Hammer”。 
  
  
![](./image/mini05.jpg)  
左边的侧面。  
  
  
![](./image/mini06.jpg)  
从左斜后方。Window上也出现了裂缝。  
  
  
![](./image/mini07.jpg)  
在里面看到的Emblem和Bonnet的一样，  
Rover時代的「MINI Emblem」的Design。  
  
  
![](./image/mini08.jpg)  
右边的侧面。  
  
  
![](./image/mini09.jpg)  
内饰似乎也是仿照老款Mini的AirBag规格。  
  
  
![](./image/mini10.jpg)  
该车在车身下的细节得到了很好的再现。  
  
  

[**5** ←BACK](shinjuku_xyz5.md)≪

≫[查看Event中分发的Goods→ **Page7**](shinjuku_xyz7.md)

  
  
※这个巨大的Figure和复制原画，Event结束后也会被好好保管，也许总有一天会出现在舞台上。  
  

* * *

ver.080522

● [Back](javascript:history.go(-1)) ●