http://www.netlaputa.ne.jp/~arcadia/ah/AH1500/shinjuku_xyz1.html



「Memory of X・Y・Z」ReportPage(1/7)

* * *

「Angel Heart」Comics累計1500万部突破記念Art Gallary  
**「Memory of X・Y・Z」**在JR新宿站举行。  
  
举行时间：2008年5月16日(金)～18日(日)、每天13:00～20:00  
举行場所：JR新宿站内 Alps广场  
［電照Sheet张贴时间:5月15日(木)～21日(水)］  
  
Event内容  
1.**新宿站XYZ留言板之复活**　→電照Sheet的XYZ留言板的复活。可以委托CH。  
2.**Mini Cooper＆1500万t Hammer巨大Figure的展示**　→以Event用的新画插图为基础制作的巨大Figure。（译注：Figure意思是"手办"。）  
3.**「AH」巨大Comic Gallary**ー　→電照Sheet上张贴整个AH。  
4.**「AH」珍贵的复制原画的展示**　→AH的Color和单色原稿中复制展示珍贵的Cut。（译注：待校对）  
5.**特製原画Postcard Present**　→Bunch最新号的每日前100名。  
6.**特製AH Eco-Bag的分发**　→17日、18日参观会场的前1500名。  
  
详细信息请参见… http://www.comicbunch.com/AH1500/  
  
17日(周六)在紀伊国屋新宿总店，北条司先生的(国内首个！)Sign会也举行了。  
这些活动在ComicBunch的编辑博客上进行了直播。
URL… http://blog.comicbunch.com/  
  

![](./image/Alps01.jpg)  
从东口检票口方向前往Alps广场。
  
  
![](./image/Alps02.jpg)  
Event会场有很多人前来参观。  
  
  
![](./image/Alps03.jpg)  
位于人墙对面的是Mini Cooper巨大Figure。  
  
  
![](./image/Alps04.jpg)  
在其斜对面，有Information的受理Counter。  
  
  
![](./image/Alps05.jpg)  
从这个位置看，巨大Figure是如此的繁忙，你甚至看不到它!（译注：待校对）  
  
  
![](./image/Alps06.jpg)  
巨大電照Sheet也吸引了人们的目光。  
  
  

≫[查看更多電照Sheet→ **Page2**](shinjuku_xyz2.md)

* * *

ver.080522

● [Back](javascript:history.go(-1)) ●