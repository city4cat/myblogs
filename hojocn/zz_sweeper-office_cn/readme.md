
[Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/)是一个日文的北条司粉丝站，主要关注CH和AH。这里是一份它的中译文。    


## 总目录  
为方便查阅，整理了文件目录。    

- [About](./about.md)(译注：各板块简介）  
- [What's neW](./what.md)  
- [Index & Index2](./index.md)(译注：待完善)  
    - AH 1500万部突破記念「Memory of X・Y・Z」（2008年5月）: 
        [1](./ah/AH1500/shinjuku_xyz1.md), 
        [2](./ah/AH1500/shinjuku_xyz2.md), 
        [3](./ah/AH1500/shinjuku_xyz3.md), 
        [4](./ah/AH1500/shinjuku_xyz4.md), 
        [5](./ah/AH1500/shinjuku_xyz5.md), 
        [6](./ah/AH1500/shinjuku_xyz6.md), 
        [7](./ah/AH1500/shinjuku_xyz7.md)  
    - 历史信息： 
        [2015](./iframe.md), 
        [2013](./iframe13.md), 
        [2012-2009](./iframe12.md), 
        [2008](./iframe08.md), 
        [2007](./iframe07.md), 
        [2006](./iframe06.md), 
        [2005](./iframe05.md), 
        [2004](./iframe04.md), 
        [2003](./iframe03.md), 
        [2002](./iframe02.md), 
        [2001](./iframe01.md)   
    - [Cocktail-X.Y.Z.](./cocktail.md)(CH的abc到xyz)  
        - CITY HUNTER是什么？  
            - [Story](./cocktail/story.md)(原作、动画的解説)  
                - [「CITY HUNTER」・原作全55集](./cocktail/storyc.md)（译注：待完善）  
                - [CITY HUNTER・短篇故事全2集](./cocktail/storyb.md)（译注：待完善）  
                - [CITY HUNTER・小说共8集](./cocktail/storyn.md)  
                - [动画「CITY HUNTER」全51話](./cocktail/story1.md)（译注：待完善）  
                - [动画「CITY HUNTER2」全63話](./cocktail/story2.md)（译注：待完善）  
                - [动画「CITY HUNTER3」全13話](./cocktail/story3.md)（译注：待完善）  
                - [动画「CITY HUNTER'91」全13話](./cocktail/story91.md)（译注：待完善）  
                - [动画・CITY HUNTER劇場版・Special 共6部](./cocktail/storysp.md)（译注：待完善）  
            - [Character](./cocktail/chara.md)(主要登场人物)  
            - [Music](./cocktail/music.md)(全部音乐列表)  
                - [CITY HUNTER soundtrack](./sound/01st1_1.md)  
                - [CITY HUNTER soundtrack Vol.2](./sound/02st1_2.md)  
                - [CITY HUNTER2 soundtrack Vol.1](./sound/03st2_1.md)  
                - [CITY HUNTER2 soundtrack Vol.2](./sound/04st2_2.md)  
                - [CITY HUNTER劇場版「愛和宿命的连发枪」soundtrack](./sound/05st_mov.md)  
                - [CITY HUNTER3 soundtrack](./sound/06st3.md)  
                - [CH Dramatic Master](./sound/07st_dr1.md)  
                - [CH Dramatic Master Vol.2 Vocal版](./sound/08st_dr2vo.md)  
                - [CH Dramatic Master Vol.2 Instrumental版](./sound/09st_dr2ins.md)  
                - [CITY HUNTER Original Special soundtrack](./sound/10st_bay.md)  
                - [CITY HUNTER'91 soundtrack](./sound/11st91.md)  
                - [成龙电影「城市猟人」Image Song](./sound/12single.md)  
                - [CH '96sp.「The Secret Service」soundtrack](./sound/13st_sec.md)  
                - [CH '97sp.「Goodby My Sweetheart」soundtrack](./sound/14st_good.md)  
                - [CH '99sp.「紧急直播！？ 暴徒冴羽獠的最后日子」soundtrack](./sound/15st_kin.md)  
                - [CD Book 2个](./cocktail/book_cd.md#cd)  
        - 分野別解説(次回更新)  
            - [Automobile](./cocktail/automobile.md)(Cooper等车)  
                - [写真](./coopers.md)  
                - [汇总](./mini.md)    
                    - [About Mini](./mini0.md)  
                    - [Mini的誕生](./mini1.md)　  
                    - [Mini Cooper和Mini Cooper S的到来](./mini2.md)　  
                    - [Mini Cooper S的成功](./mini3.md)  
                    - [Mk.2的時代](./mini4.md)  
                    - [Mini系列，到Mk.3](./mini5.md)  
                    - [Rover Mini](./mini6.md)  
                    - [Mini Cooper复活](./mini7.md)  
                    - [新型Mini？](./mini8.md)  
                    - [Mini的Dress Up](./mini9.md)  
                    - [自Mk2以来的主要外观变化](./mini10.md)  
                    - [目录数据](./mini11.md)  
            - [Cocktail](./cocktail/liquor.md)(鸡尾酒和酒精饮料的解读)  
            - [Drug](./cocktail/drug.md)(作品中相关毒品的解读)  
            - [Fashion](./cocktail/fashion.md)(与时尚有关的解读)  
                - [Belt Buckle Knife写真](./buckle.md)
            - [Gun](./cocktail/gun.md)(对枪支的解读)  
                - [Colt Python .357Magnum写真](./python.md)  
        - 地图  
            - [新宿PHOTO Map](./shinjuku/map.md)  
                - [Saeba公寓](./shinjuku/apartment.md)  
                - [高层建筑区](./shinjuku/building.md)  
                - [新宿东口站前](./shinjuku/eastside.md)  
                - [歌舞伎町](./shinjuku/kabuki.md)  
                - [Hotel街](./shinjuku/loveho.md)  
                - [新宿2丁目](./shinjuku/nichome.md)  
                - [新宿中央公園](./shinjuku/park.md)  
                - [新宿站](./shinjuku/station.md)  
                - [首都政府大楼](./shinjuku/tocho.md)  
        - 各种Data  
            - [信息/来源](./cocktail/info.md)(CH的最新信息)  
                - [应季的信息](./cocktail/info.md#旬)
                - [Schedule](./cocktail/info.md#スケジュール)
                - [番外編](./cocktail/info.md#番外編)
                - [记录](./cocktail/info.md#記録)
            - [XYZ BOX](./cocktail/xyzbox.md)  
            - [CH人名辞典](./cocktail/name.md)(登場人物List)  
                - [あ行](./cocktail/name_a.md)（青猿～オールマン）（译注：待完善）  
                - [か行](./cocktail/name_ka.md)（海音字さゆり～権藤）（译注：待完善）  
                - [さ行](./cocktail/name_sa.md)（佐伯～千寿院）（译注：待完善）  
                - [た行](./cocktail/name_ta.md)（タイガーヘッド～ドラゴン）（译注：待完善）  
                - [な行](./cocktail/name_na.md)（中田万堂～典子）（译注：待完善）  
                - [は行](./cocktail/name_ha.md)（バカロ～ボンダル大使）（译注：待完善）  
                - [ま行](./cocktail/name_ma.md)（真風笑美～森脇美鈴）（译注：待完善）  
                - [や行](./cocktail/name_ya.md)（八九三～予備校生）（译注：待完善）  
                - [ら行](./cocktail/name_ra.md)（雷王～龍雲）（译注：待完善）  
                - [わ行](./cocktail/name_wa.md)（稚姫～王）（译注：待完善）  
            - [CH年表](./cocktail/history.md)(CH相关事件、现实事件、流行语)  
            - [CH小知识](./cocktail/bits.md)（CH 4格漫画、北条先生选择的书籍）  
            - [CH関連書籍](./cocktail/book.md)  
                - [JUMP COMICS](./cocktail/book_jc.md)  
                - [集英社文庫COMIC版](./cocktail/book_bunko.md)  
                - [JUMP COMICS DELUXE](./cocktail/book_dx.md)  
                - [JUMP j BOOKS](./cocktail/book_jb.md)  
                - [Cassette・CDBook](./cocktail/book_cd.md)  
                - [JUMP GOLD SELECTION Anime Mook](./cocktail/book_mook.md)  
                - [JUMP COMICS SELECTION Anime Comics](./cocktail/book_anime.md)  
                - [愛蔵版Comics](./cocktail/book_aizo.md)  
                - [SC ALLMAN](./cocktail/book_sc.md)  
                - [SC ALLMAN愛蔵版](./cocktail/book_scaizo.md)  
                - [別冊宝島](./cocktail/book_takara.md)（宝島社）  
            - [CH関連書籍2](./cocktail/book2.md)（新潮社出版的comics等）  
                - [BUNCH WORLD B6尺寸Comic](./cocktail/book_bw.md)  
                - [BUNCH COMICS](./cocktail/book_bc.md)  
            - [CH関連書籍3](./cocktail/book3.md)（Coamix出版的comics等）  
                - [RAIJIN COLLECTION B6尺寸漫画](./cocktail/book_rc.md)  
            - [CH関連書籍4](./cocktail/book4.md)（徳間書店CH完全版）  
                - [CUTY HUNTER 《COMPLETE EDITION》](./cocktail/book_pf.md)  
            - [CH関連商品](./cocktail/video.md)（Video，LD，Games）  
                - [Video](./cocktail/video_vi.md)  
                - [Laser Disc](./cocktail/video_ld.md)  
                - [Game](./cocktail/video_ga.md)  
            - [Main Staff](./cocktail/staffm.md)（Main Anime Staff）  
            - [各話Staff](./cocktail/staff.md)（动画各話Staff）  
            - [原作动画対应表](./cocktail/aotable.md)（动画化対应简表）  
        - Mystery(準備中)  
            - [CH之谜](./cocktail/mystery.md)（CH之谜和不可思议）  
            - [原作和动画的不同之处](./cocktail/difference.md)  
        - Keywords(建设中)  
            - [CH encyclopedia](./cocktail/uc.md)（CH百科辞典）  
        - 其他  
            - [About Mini](./mini0.md)（Mini和Cooper的历史）  
        - 奖励  
            - [Lucky Story?](./cgi/lucky_kuji.md)（抽签）  
    - [Angel Dust](./dust.md)(译注：一些画作。未整理)  
    - [Stop a Hammer !](./tanma.md)（不含墙纸和纪念图片）(译注：一些画作。未整理)  
    - [Shooting Range](./shoot/shooting.md)（Java Applet的射击Game!）(译注：未整理)  
    - [Midnight Hunting](./cgi/hunter_index.md)（所谓的正常聊天…）（译注：聊天室？）  
    - [Couples Hotel](./chat.md)（带BGM和Love的Java Chat!）（译注：另一个聊天室？）  
    - [Wanted-B.B.S.](./bbs.md)  
    - [OneHole Shot](./link.md)（译注：CH相关链接。）  
    - [Profile](./profile.md)（译注：站长的个人信息。）  
    - [ICQ Message panel](./icq.md)（译注：向站长发消息。）  
    - [J.M.C.](./ah/index.md)（**AH板块**）  
        - JMC  
            - [JMC Top](./ah/jmc2.md)（JMC2。一些信息+2009年～2013年1月的信息）  
                - [2001年5月～2008年的信息](./ah/ahiframe01.md)  
                - [2009年～2013年1月的信息](./ah/ahiframe.md)  
            - [更新状況](./ah/ahwhat.md)  
        - Angel Heart是什么？  
            - [Story](./ah/ahstory.md)  
                - [「Angel Heart」听漫](./ah/ahstorycc.md)  
                - [本刊刊载时与总集篇1的区别](./ah/ahstorydiff.md)  
            - [Character](./ah/ahchara.md)（主要登场人物）（译注：待完善）  
        - BBS  
            - [AH感想BBS](./cgi/ah_bbs.md)  
                - [BBS概要](./cgi/ah_bbs_about.md)  
                - [2014-2023年的帖子](./cgi/ah_bbs_post14-23.md)(建议按时间先后顺序阅读以下BBS Posts)  
                - [2013年的帖子](./cgi/ah_bbs_post13.md)  
                - [2012年的帖子](./cgi/ah_bbs_post12.md)  
                - [2011年的帖子](./cgi/ah_bbs_post11.md)  
                - [2010年的帖子](./cgi/ah_bbs_post10.md)  
                    - [关于Comic Zenon创刊](./cgi/ah_bbs_post10.md#BunchStop6)；  
                    - [关于Bunch休刊5](./cgi/ah_bbs_post10.md#BunchStop5)；  
                    - [关于Bunch休刊4](./cgi/ah_bbs_post10.md#BunchStop4)；  
                    - [关于Bunch休刊3](./cgi/ah_bbs_post10.md#BunchStop3)；  
                    - [关于Bunch休刊2](./cgi/ah_bbs_post10.md#BunchStop2)；  
                    - [关于Bunch休刊1](./cgi/ah_bbs_post10.md#BunchStop1)；  
                    - [关于Bunch休刊0](./cgi/ah_bbs_post10.md#BunchStop0)；  
                - [2009年的帖子](./cgi/ah_bbs_post09.md)  
                    - 「描線の流儀」访谈的简介：[后篇](./cgi/ah_bbs_post09.md#interview1)；  
                    - 「描線の流儀」访谈的简介：[前篇](./cgi/ah_bbs_post09.md#interview0)；  
                    - CAFE ZENON[报道1](./cgi/ah_bbs_post09.md#cafe_zenon1)；  
                    - CAFE ZENON[报道0](./cgi/ah_bbs_post09.md#cafe_zenon0)；  
                - [2008年的帖子](./cgi/ah_bbs_post08.md)  
                    - [北条司画的拉姆（《福星小子》）](./cgi/ah_bbs_post08.md#Ram)
                - [2007年的帖子](./cgi/ah_bbs_post07.md)  
                - [2006年的部分帖子](./cgi/533/2006.md)  
                - [2005年的部分帖子](./cgi/533/2005.md)  
                - [2004年的部分帖子](./cgi/533/2004.md)  
                - [2003年的部分帖子](./cgi/533/2003.md)  
                    - 本网站访问量突破20万；  
                    - 海坊主Bobsapp；
                    - 忠犬八公现在被制成标本，在上野的博物馆里；
                    - 据说朝日新闻曾将Smith&Wesson写成Smith&Western；
                    - CH的音乐被做成铃声；  
                    - 纪伊国屋的Comic House销售JC版CH；  
                    - 神谷明主持电台节目；
                - [2002年的部分帖子](./cgi/533/2002.md)  
                    - “危险的家庭教师(动画)”里獠的衣服好像是文科科目老师兼生活指导；  
                - [2001年的部分帖子](./cgi/533/2001.md)  
                    - 设定本论坛是可剧透；创建「Angel Heart 感想BBS」；
                    - AH総集編； 
                    - 「週刊Book Review」上出现了北条；  
                    - 纽约曼哈顿的Bunch销售情况；  
                - [2000年的部分帖子](./cgi/533/2000.md)  
                    - 在Jump小说中，有一部分是用彩色刊载的小海的婚礼场景。Mick没有出现；  
                    - 在新宿、歌舞伎町一带特别能看到Mini；新宿的红白色Mini确实很多；  
                    - 北条司老师的官方主页已经建立起来了。留言板上还有神谷明本人的留言，他的暱称是｢あっくん｣；
                - [1999年的部分帖子](./cgi/533/1999.md)  
        - Data
            - [AH人名辞典](./ah/ahname.md)  
                - [あ行](./ah/ahname_a.md)（海坊主）（译注：待完善）  
                - [か行](./ah/ahname_ka.md)（金）（译注：待完善）  
                - [さ行](./ah/ahname_sa.md)（冴羽獠）（译注：待完善）  
                - [た行](./ah/ahname_ta.md)（張～智）（译注：待完善）  
                - [な行](./ah/ahname_na.md)（野上冴子）（译注：待完善）  
                - [は行](./ah/ahname_ha.md)（Nothing）（译注：待完善）  
                - [ま行](./ah/ahname_ma.md)（槇村香～餅山秀夫）（译注：待完善）  
                - [や行](./ah/ahname_ya.md)（Nothing）（译注：待完善）  
                - [ら行](./ah/ahname_ra.md)（李大人～林）（译注：待完善）  
                - [わ行](./ah/ahname_wa.md)（Nothing）（译注：待完善）  
            - [AH信息·来源](./ah/ahinfo.md)  
            - [AH相关书籍](./ah/ahbook.md) up  
                - [BUNCH COMICS](./ah/ahbook_bc.md)  
        - Keyword　
            - [AH encyclopedia](./ah/ahencyclo.md)（AH百科辞典）  
                - [新宿站东口检票口前写真](./ah/kaisatsu.md)  
                - [MyCity新旧Logo比較写真](./ah/myc.md)  
                - [Alta新旧比較写真](./ah/alta.md)  
        - Mysterious?　
            - [CH-AH的不同](./ah/ahdiff.md)  
        - Other  
            - [AH Link](./cgi/ah_ahlink_autolink.md)（AH相关的Link）  



- 其他未展示的网页:      
    - THE CITY HUNTER展（2005年8月，銀座）Report Page:   
        - [Page1](./ginza/ginza_ch1.md);  
        - [Page2](./ginza/ginza_ch2.md);  
        - [Page3](./ginza/ginza_ch3.md);  
        - [Page4](./ginza/ginza_ch4.md);  
        - [Page5](./ginza/ginza_ch5.md);  


## 关于中译文  

### 动机  

首先，[Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/)和[北条司研究序説(HTKJ)](http://chiakik.la.coocan.jp/htkj/xyz.htm)([中文翻译](../zz_htkj_cn/readme.md))似乎是现存唯二的北条司粉丝日文站。相比HTKJ，Sweeper Office虽然有与其重复的内容，但也有很多不同的内容(例如[CH年表](./cocktail/history.md)等)。所以，对于进一步了解北条司及其作品，Sweeper Office有它自身的价值。    

其次，虽然每个人都可以借助翻译工具去阅读SweeperOffice的内容，但对于机翻不通顺的句子，每个人的理解可能各有不同。所以想提供一个地方，集众人所长来不断地完善该中译文。  

最后，翻译非常耗时；一人翻译，众人受益；Sweeper Office几乎不更新，所以该翻译是一劳永逸。  

### 关于版权  
虽然该中译文暂未得到Sweeper Office的授权，但我尊重Sweeper Office的版权信息。若今后该中译文遭到Sweeper Office反对，我将删除该译文。    
中译文遵循许可"CC BY-NC-SA 4.0"。  

### 关于翻译的一些约定  
一方面想忠于原文，另一方面个人精力有限。所以需要有取舍，便有了如下翻译约定：  

- 只翻译日文。原因：1）大部分读者有英文阅读能力。2）机翻"日译英"的结果常常比"日译中"的结果好。    
- 以下内容未作翻译或改动：  
    - 保留日文标点符号，例如，『』，「」，等。  
    - 保留日文专有名词或书名里的"・"。  
    - 常见的、易理解的繁体字词未翻译。  
    - 对于英文单词的日文发音，绝大多数直接翻译为英文。例如，"シティーハンター"翻译为"City Hunter"。    
- 暂时舍弃原文里某些段落的格式、表格的格式。待以后完善时再加上这些格式。  


### 其他  

有部分章节暂时未翻译。暂将其标记为“待完善”。  

在翻译过程中，对于我觉得翻译不准确的地方，在其段落末尾标有译注(例如：“（译注：待校对）”)，便于后续更有针对性地去完善。    

本中译版未将中译文附在日文之后，而是做成独立于日文的单独文件目录。这既有优点也有缺点。缺点是，校对译文时需要另外访问日文原文网址。  

记录北条司相关的历史活动的章节有四处：  
- [2015](./iframe.md)（根目录）  
- [信息/来源 ](./cocktail/info.md)（CH）  
- [JMC Top](./ah/jmc2.md)（AH）  
- [AH信息·来源](./ah/ahinfo.md)（AH）  
这四处有大量重复信息，但内容或组织方式不尽相同。我猜，[2015](./iframe.md)可能是比较全面的汇总。  

如有任何问题，欢迎来此讨论： BBS-[北条司中文网](www.hojocn.com/bbs)， 帖子-[Sweeper Office(中文翻译)]()。    

个人感受：  

- [AH感想BBS](./cgi/ah_bbs.md)中有2007年10月至今的帖子。这些帖子由站长和Fans发布。帖子的译文略粗糙，未能全部遵守上述翻译约定。    
- 很遗憾，[AH BBS](./cgi/ah_bbs.md)无法追溯2007年9月以前的帖子；CH BBS([1](http://www.tcup5.com/533/arcadia.html)和[2](http://533.teacup.com/arcadia/bbs))的网络供应商于2022年关闭服务，所以其帖子无法看到；[从web.archive.org上只获得了部分数据](./cgi/533/readme.md)。  
- 阅读BBS的帖子，我的感受是：  
    - 仿佛自己穿越到了当年。比如，读到AH越来越频繁的休载、关于Bunch停刊、AH第二季等等这些消息。
    - 感受到日本Fans追AH连载的日常：期待、购买、阅读、感想、期待下一期。
    - 对于AH越来越频繁的休载，帖子中透露出Fans对作者北条司的理解和祝福。    





### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处


<font color=#ff0000></font>
<a name="cd"></a>  
<s></s>