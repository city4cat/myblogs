http://www.netlaputa.ne.jp/~arcadia/iframe13.html

**←[最新的信息](./iframe.md)**  

## 2013年（译注）  

■01/25(金)　　　/[新信息在这里](./cocktail/info.md)/  
　ミッ月刊Comic Zenon2013年3月号发售  
　「**Angel Heart** 2ndSeason」第28話/封面  
　【わくわくする秘密】  
　▽巻頭Color特別企画「漫画の描き方」座談会（译注：「漫画的画法」）  
　→北条司×保谷伸・モリコロス・日笠優  
  
■01/19(土)  
　★Zenon Comics  
　「**Angel Heart** 2ndSeason」第5巻 发售  
　▽封面Cover待ち受け・登録者全員Present（译注：待校对）  
　・可以享受从封面画的草稿到完成的过程  
　→详见书带的QRCode（译注：QRCode即二维码）   
　▽TSUTAYA限定特別付録「北条司の漫画の読み方」（译注：「北条司的漫画的阅读方法」）   
　→新人3作家的座談会収録・合作海报    
  
■01/19(土)  
　★Zenon Comics DX  
　「**Angel Heart** 1stSeason」第21・22巻 发售  
　▽杂志刊登时的Color完全再现&新装帧  
　→每月出版2卷，计划全部24卷   
  
**→[2012年的信息](./iframe12.md)**