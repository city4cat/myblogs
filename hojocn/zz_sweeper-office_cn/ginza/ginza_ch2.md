source:  
[http://www.netlaputa.ne.jp/~arcadia/ginza/ginza_ch2.html](http://www.netlaputa.ne.jp/~arcadia/ginza/ginza_ch2.html)  

THE CITY HUNTER展 Vol.1 銀座（→2005年8月13日～8月21日为止）ReportPage2

* * *

Case中的展品。现在很少见(？)。许多宝藏Goods。
  
  
  

![](image/goods01.jpg)  
视频，甚至是LD。  
  
  
![](image/goods02.jpg)  
Character Goods，如Campen和Note。  
  
  
![](image/goods03.jpg)  
Game、AnimeComic、T-shirt etc...。  
  
  
![](image/goods04.jpg)  
甚至Skateboard也是CityHunter！！ 
  
  
![](image/goods06.jpg)  
Record或CD。  
  
  
![](image/goods05.jpg)  
甚至还有珍贵~提供的物品！！
  
  

[**1** ←BACK](ginza_ch1.md)≪

≫[提供的物品见→ **Page3**](ginza_ch3.md)

* * *

ver.050826

● [Back](javascript:history.go(-1)) ●