source:  
[http://www.netlaputa.ne.jp/~arcadia/ginza/ginza_ch1.html](http://www.netlaputa.ne.jp/~arcadia/ginza/ginza_ch1.html)  


THE CITY HUNTER展 Vol.1 銀座（→2005年8月13日～8月21日为止）ReportPage1

* * *

会場／銀座 Sony大厦8F(SOMIDO Hall)  
時間／月～金：12時～20時、土日：12時～19時  
  
CITY HUNTER COMPLETE DVD BOX发售＆Angel Heart Anime化記念、 Anime CH相关的Event举办。  
  

> _○Anone人选择的「CityHunter」Best Selection一举上映！  
> ○原作者・北条司先生的原画大公開！  
> ○"CITY HUNTER COMPLETE"的实物大小Sample终于登场了！  
> ○Memorial CD Album可试听！  
> ○展示令人怀念的「CityHunter」相关商品！如Video Package和Character Goods_

（以上，摘自官方信息页面）  
而且，要看到这一切不容易（？） 内容是很华丽的。（译注：待校对）
  

![](image/ch_ginza01.jpg)  
在会场门口，立即迎接獠的到来。
  
  
![](image/ch_ginza02.jpg)  
进入里面，侧面整齐地排列着北条老师的CH Color原画。。  
  
  
![](image/ch_ginza03.jpg)  
在里面的Theater中，Best Selection的2话(每日交替)交替上映。
  
  
![](image/ch_ginza04.jpg)  
到访的人们会看到Case上展出的许多贵重物品。  
  
  
![](image/ch_ginza06.jpg)  
这是大家期待已久的DVD BOX！的实物大小Sample。  
  
  
![](image/ch_ginza05.jpg)  
新推出的Memorial CD试听Corner。  
（与实际发售的东西不同。Event用的特别编辑。）  
  
  

≫[展示品见→ **Page2**](ginza_ch2.md)

在会场入口，每天10人的DVD BOX“非卖品Poster”抽签。  
（运气不好的人没有非卖品Sticker）（译注：待校对）  
另外，Event第一天，在第二次放映会后，有诹访Producer和植田Producer(当时Sunrise)的
Surprise Talk Show

* * *

ver.050826

● [Back](javascript:history.go(-1)) ●