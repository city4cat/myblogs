http://www.netlaputa.ne.jp/~arcadia/shinjuku/tocho.html  

首都政府大楼  
  

![首都政府大楼](photo/tocho01.jpg)

'91年3月启用的新首都政府大楼。 该建筑让人联想到「后现代风格」的哥特式神庙。  

  
  
  

![从中央公园看](photo/tocho02.jpg)

从新宿中央公园往上看首都政府大楼。

  
  
  

![在CH2的时候正在建设中](photo/tocho03.jpg)

在CH2结束的后期，新首都政府大楼仍在建设中。

  
  
  

* * *

[返回map](map.md)