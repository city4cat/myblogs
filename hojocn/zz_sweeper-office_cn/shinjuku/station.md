http://www.netlaputa.ne.jp/~arcadia/shinjuku/station.html  

新宿站  
  

![新宿。](photo/station01.jpg)

新宿站。从山手线出站台。

  
  
  

![山手线出站](photo/station02.jpg)

圆形的绿色山手线。 在高峰期非常拥挤。

  
  
  

![检票口](photo/station03.jpg)

东出口检票口。

  
  
  

![东口付近](photo/station04.jpg)

东出口检票口外的区域。用于委托的留言板就设置在这附近？

  
  
  

![留言板！？](photo/station05.jpg)

找到留言板了！! 不是，只是一个广告... (留言板不是真的)。


  
  
  

![MY CITY入口](photo/station06.jpg)

来这里，在MYCITY购物。

  
  
  

* * *

[返回map](map.md)
