http://www.netlaputa.ne.jp/~arcadia/shinjuku/building.html  

高层建筑区  
  

![](photo/building01.jpg)

原作中经常出现的高层建筑区。

  
  
  

![](photo/building02.jpg)

左边的建筑是55层的新宿三井大厦。 绰号 「55大厦」。 半镜面外观。

  
  
  

![](photo/building03.jpg)

动画片结局中的一个熟悉场景。

  
  
  

![](photo/building04.jpg)

从獠的公寓楼顶看出去的景色是这样的吗？








  
  
  

* * *

[返回map](./map.md)
