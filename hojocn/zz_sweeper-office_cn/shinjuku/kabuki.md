http://www.netlaputa.ne.jp/~arcadia/shinjuku/kabuki.html  

歌舞伎町  
  

![歌舞伎町一番街入口](photo/kabuki01.jpg)

歌舞伎町一番街入口・・・。

  
  
  

![歌舞伎町](photo/kabuki02.jpg)

歌舞伎町，獠晚上出没的地方。  

  
  
  

![各种商店](photo/kabuki04.jpg)

各种各样的商店... 他们在做什么？

  
  
  

![劇場街](photo/kabuki03.jpg)

劇場街。各种剧院聚集在一起。  

  
  
  

* * *

[返回map](map.md)
