http://www.netlaputa.ne.jp/~arcadia/shinjuku/eastside.html  

新宿东口站前  
  

![MYCITY](photo/eastside01.jpg)

熟悉的MYCITY，现在的标志与连载时略有不同。

  
  
  

![新宿站的入口处](photo/eastside02.jpg)

在新宿站的入口附近。 经常有活动在这里举行。

  
  
  

![车站广场](photo/eastside04.jpg)

这里是阿香散发传单的地方。

  
  
  

![ALTA工作室](photo/eastside03.jpg)

ALTA工作室的大屏幕。 这里是阿香向银狐宣战的地方，还是獠注视美女的地方？

  
  
  

![新宿大街](photo/eastside05.jpg)

从车站广场看新宿大街。

  
  
  

* * *

[返回map](map.md)
