http://www.netlaputa.ne.jp/~arcadia/shinjuku/map.html

City Hunter的舞台 - 新宿

---

点击红色区域可以看到獠和他的朋友们活动的地点的照片。  
  

![新宿マップ](../shinjuku/map.gif)

译注：  

- [サエバアパート？](./apartment.md)（译注：Saeba公寓）  
- [高層ビル街](./building.md)（译注：高层建筑区）  
- [新宿東口駅前](./eastside.md)（译注：新宿东口站前）  
- [歌舞伎町](./kabuki.md)  
- [ホテル街](./loveho.md)（译注：Hotel街）  
- [新宿2丁目](./nichome.md)  
- [新宿中央公園](./park.md)  
- [新宿駅](./station.md)（译注：新宿站）  
- [新都庁舍](./tocho.md)（译注：首都政府大楼）  

---

---

**[** [Office](../index.md) **]**