http://www.netlaputa.ne.jp/~arcadia/shinjuku/park.html  

新宿中央公园  
  

![新宿中央公园](photo/park01.jpg)

新宿中央公园是一个创建于'68年的市政公园。

  
  
  

![长椅](photo/park02.jpg)

现在的长椅有突起，以防止无家可归者在上面睡觉。

  
  
  

![公园的绿色植物](photo/park03.jpg)

这就是獠与将军们战斗的那个公园吗？

  
  
  

![水之广场](photo/park04.jpg)

水之广场。后面有瀑布和其他功能。

  
  
  

![公園的阶梯](photo/park05.jpg)

当长椅坐满时，一些人坐在楼梯上休息・・・。

  
  
  

* * *

[返回map](map.md)
