http://www.netlaputa.ne.jp/~arcadia/iframe07.html

**→[2008年的信息](./iframe08.md)**  
  
**▼2007年的信息**  
  
■12/27(木)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第277話/封面  
　【虎の尾】  
　▽北条司全方位特集Special Interview  
　▽特別附录illustrated.Book「ライアのまど」（译注：ライア：Lyra/Riah。まど：Mado/窗户）  
　▽CH JOURNAL：Pachislo CH  
　→围板、Poster等Present  
  
■12/21(金)  
　Comic Bunch新年3号(1月14日号)发售  
　「**Angel Heart**」暂停。  
　▽下期是27日发售、北条司特集  
　→Interview＆特別附录「ライアのまど」  
  
■12/07(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第276話  
　【招かれざる客】  
　▽CH単巻Release記念・CH JOURNAL  
　▽下期休載。4・5合并特刊是北条司全方位专题  
　illustrated.Book「ライアのまど」Present  
  
■11/30(金)  
　Comic Bunch第53号(12月14日号)发售  
　「**Angel Heart**」第275話  
　【カメレオンの罠】  
  
■11/22(木)  
　Comic Bunch第52号(12月7日号)发售  
　「**Angel Heart**」第274話  
　【葉月の悩み】  
  
■11/16(金)  
　Comic Bunch第51号(11月30日号)发售  
　「**Angel Heart**」第273話  
　【親子の愛】  
  
■11/09(金)  
　Comic Bunch第50号(11月23日号)发售  
　「**Angel Heart**」第272話/封面  
　【大好きなんです】  
  
■11/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第24巻 发售  
　▽封面是Comic Bunch第30号Color扉页的图案  
　→问卷调查中有北条先生亲笔彩纸Present  
  
■11/02(金)  
　Comic Bunch第49号(11月16日号)发售  
　「**Angel Heart**」暂停。  
　▽下期是AH封面  
  
■10/26(金)  
　Comic Bunch第48号(11月9日号)发售  
　「**Angel Heart**」第271話  
　【キバナコスモス】  
  
■10/19(金)  
　Comic Bunch第47号(11月2日号)发售  
　「**Angel Heart**」第270話  
　【ファルコンの秘密】  
  
■10/12(金)  
　Comic Bunch第46号(10月26日号)发售  
　「**Angel Heart**」第269話  
　【墓参り】  
　▽CH単巻DVD发售決定  
　→12/19起毎月计划Release(全26巻)  
  
■10/05(金)  
　Comic Bunch第45号(10月19日号)发售  
　「**Angel Heart**」第268話/封面  
　【ファルコンのお守り】  
  
■09/28(金)  
　Comic Bunch第44号(10月12日号)发售  
　「**Angel Heart**」第267話  
　【葉月の父】  
  
■09/21(金)  
　Comic Bunch第43号(10月5日号)发售  
　「**Angel Heart**」暂停。  
  
■09/14(金)  
　Comic Bunch第42号(9月28日号)发售  
　「**Angel Heart**」第266話  
　【誤解】  
  
■09/07(金)  
　Comic Bunch第41号(9月21日号)发售  
　「**Angel Heart**」第265話  
　【海坊主と葉月】  
  
■08/31(金)  
　Comic Bunch第40号(9月14日号)发售  
　「**Angel Heart**」第264話  
　【夏の再会】  
  
■08/24(金)  
　Comic Bunch第39号(9月7日号)发售  
　「**Angel Heart**」第263話  
　【私の立ち位置】  
  
■08/10(金)  
　Comic Bunch第37・38合并特刊 发售  
　「**Angel Heart**」第262話  
　【仁志の涙】  
　▽Bunch創刊300号記念Present  
　→AHSign彩纸・北条先生推荐的红葡萄酒&白葡萄酒 
  
■08/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第23巻 发售  
　▽封面是Comic Bunch新年1・2合并特刊扉絵的图案  
  
■08/03(金)  
　Comic Bunch第36号(8月17日号)发售  
　「**Angel Heart**」第261話  
　【武道館決戦】  
  
■07/27(金)  
　Comic Bunch第35号(8月10日号)发售  
　「**Angel Heart**」暂停。  
  
■07/20(金)  
　Comic Bunch第34号(8月3日号)发售  
　「**Angel Heart**」第260話  
　【消えない記憶】  
  
■07/13(金)  
　Comic Bunch第33号(7月27日号)发售  
　「**Angel Heart**」第259話  
　【香瑩の怒り】  
  
■07/06(金)  
　Comic Bunch第32号(7月20日号)发售  
　「**Angel Heart**」第258話  
　【及第点の答え】  
  
■06/29(金)  
　Comic Bunch第31号(7月13日号)发售  
　「**Angel Heart**」第257話  
　【陳さんの暴走】  
  
■06/22(金)  
　Comic Bunch第30号(7月6日号)发售  
　「**Angel Heart**」第256話/封面/巻頭Color 
　【新スポンサー】  
  
■06/15(金)  
　Comic Bunch第29号(6月29日号)发售  
　「**Angel Heart**」暂停。  
　▽下周30号是AH封面＆巻頭Color  
  
■06/08(金)  
　Comic Bunch第28号(6月22日号)发售  
　「**Angel Heart**」第255話  
　【仲間】  
  
■06/01(金)  
　Comic Bunch第27号(6月15日号)发售  
　「**Angel Heart**」第254話  
　【嫉妬】  
  
■05/25(金)  
　Comic Bunch第26号(6月8日号)发售  
　「**Angel Heart**」第253話  
　【表への一歩】  
  
■05/18(金)  
　Comic Bunch第25号(6月1日号)发售  
　「**Angel Heart**」暂停。  
　▽Comics联动、DVD BOX4 Present申请券(15名)  
  
■05/11(金)  
　Comic Bunch第24号(5月25日号)发售  
　「**Angel Heart**」第252話/封面  
　【初めての経験】  
　▽Comics联动、DVD BOX4 Present申请券(15名)  
  
■05/09(水)  
　★Bunch Comics 　
　「**Angel Heart**」第22巻 发售  
　▽封面是Comic Bunch新年5・6合并特刊封面的图案  
　→DVD BOX4Present申请券(该杂志联动/15名)  
  
■04/27(金)  
　Comic Bunch第22・23合并特大号 发售  
　「**Angel Heart**」第251話  
　【意外な弱点】  
  
■04/20(金)  
　Comic Bunch第21号(5月4日号)发售  
　「**Angel Heart**」第250話  
　【暖かい場所へ】  
  
■04/13(金)  
　Comic Bunch第20号(4月27日号)发售  
　「**Angel Heart**」第249話  
　【見えない景色】  
  
■04/06(金)  
　Comic Bunch第19号(4月20日号)发售  
　「**Angel Heart**」第248話  
　【気づかぬ優しさ】  
  
■03/30(金)  
　Comic Bunch第18号(4月13日号)发售  
　「**Angel Heart**」暂停。  
  
■03/23(金)  
　Comic Bunch第17号(4月6日号)发售  
　「**Angel Heart**」第247話  
　【悲痛な願い】  
  
■03/16(金)  
　Comic Bunch第16号(3月30日号)发售  
　「**Angel Heart**」第246話  
　【雲のように】  
  
■03/09(金)  
　Comic Bunch第15号(3月23日号)发售  
　「**Angel Heart**」第245話  
　【初アフター是ストーカー付！】  
　▽Yahoo AH联动企划  
　→实物大小100t Hammer、Yahoo Auction出品  
  
■03/02(金)  
　Comic Bunch第14号(3月16日号)发售  
　「**Angel Heart**」第244話  
　【恋是アフターで！】  
　▽Yahoo AH联动企划→什么是智囊？ 
  
■02/23(金)  
　Comic Bunch第13号(3月9日号)发售  
　「**Angel Heart**」第243話  
　【覚悟】  
　▽Yahoo 联动企划・AH Baton活用術 (译注：Baton：接力棒)   
  
■02/16(金)  
　Comic Bunch第12号(3月2日号)发售  
　「**Angel Heart**」第242話  
　【香瑩の答え】  
　▽AH×Yahoo!JAPAN联动企划  
　Angel Heart  Special OPEN  
  
■02/09(金)  
　Comic Bunch第11号(2月23日号)发售  
　「**Angel Heart**」暂停。  
　▽AH×Yahoo!JAPAN联动企划  
　→2/16,Yahoo!動画AH特集Page登場  
　1.Yahoo!Auction Surprise Item出品  
　2.AH第1話、話題的新Media上阅读  
　3.Yahoo!人气Corner也有北条先生参加  
  
■02/02(金)  
　Comic Bunch第10号(2月16日号)发售  
　「**Angel Heart**」第241話  
　【狙撃】  
　▽绝密Project進行中・详情见下期    
  
■01/26(金)  
　Comic Bunch第9号(2月9日号)发售  
　「**Angel Heart**」第240話  
　【カメレオンの誘惑】  
　▽Comics联动、DVD BOX3 Present申请券(15名)  
  
■01/19(金)  
　Comic Bunch第8号(2月2日号)发售  
　「**Angel Heart**」第239話  
　【乙玲(イーリン)の姉】  
　▽Comics联动、DVD BOX3 Present申请券(15名)  
  
■01/12(金)  
　Comic Bunch新年7号(1月31日号)发售  
　「**Angel Heart**」第238話  
　【花園学校の未来】  
　▽Comics联动、DVD BOX3 Present申请券(15名)  
  
■01/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第21巻 发售  
　▽封面是Comic Bunch'06年第47号封面的图案  
　▽包含书带的初版限量版780日元  
　→しら乌鸦＆１００ｔ Hammer  
　▽DVD BOX3Present申请券(该杂志联动/15名)  
  
**→[2006年的信息](./iframe06.md)**