http://www.netlaputa.ne.jp/~arcadia/iframe03.html

**→[2004年的信息](./iframe04.md)**  
  
**▼2003年的信息**  
  
■12/25(木)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第114話/封面  
　【恋人同士の誤解】  
　▽Premium附录・AH 专用 Comics Cover   
　▽下期(新年6号)是1月8日发售  
  
■12/19(金)  
　Comic Bunch新年3号(1月9日号)发售  
　「**Angel Heart**」第113話  
　【ベイビートラブル！】  
　▽下期(12/25发售)、AH 专用 Book Cover(B6)附录 
  
■12/15(月)  
　「**CITY HUNTER** 完全版」  
　VOL.01、VOL.02 由徳間書店发售  
　定价・各980日元  
　▽発刊記念 特製电话卡＆Postcard Present  
  
■12/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第9巻 发售  
　▽封面是Comic Bunch第31号扉絵  
  
■12/05(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第112話  
　【依頼人是ひったくり！？】  
　▽下期(新年3号)是12月19日发售  
  
■11/28(金)  
　Comic Bunch第52号(12月12日号)发售  
　「**Angel Heart**」第111話/封面＆巻頭Color  
　【早百合の旅立ち】  
  
■11/20(木)  
　Comic Bunch第51号(12月5日号)  
　「**Angel Heart**」暂停。  
　▽下期(52号)是AH封面＆巻頭Color  
  
■11/14(金)  
　Comic Bunch第50号(11月28日号)发售  
　「**Angel Heart**」第110話  
　【一途(バカ)な男】  
  
■11/07(金)  
　Comic Bunch第49号(11月21日号)发售  
　「**Angel Heart**」第109話  
　【リョウからの依頼】  
  
■10/30(木)  
　Comic Bunch第48号(11月14日号)发售  
　「**Angel Heart**」第108話/封面  
　【Ｃ・Ｈの正体】  
  
■10/24(金)  
　Comic Bunch第47号(11月7日号)发售  
　「**Angel Heart**」第107話  
　【槇村の決意】  
  
■10/17(金)  
　Comic Bunch第46号(10月31日号)发售  
　「**Angel Heart**」第106話  
　【小さな願い】  
  
■10/09(木)  
　Comic Bunch第45号(10月24日号)发售  
　「**Angel Heart**」第105話  
　【思い出の街】  
  
■10/03(金)  
　Comic Bunch第44号(10月17日号)发售  
　「**Angel Heart**」第104話  
　【奇跡の適合性】  
  
■09/26(金)  
　Comic Bunch第43号(10月10日号)发售  
　「**Angel Heart**」第103話  
　【新宿の天使】  
  
■09/19(金)  
　Comic Bunch第42号(10月3日号)发售  
　「**Angel Heart**」第102話  
　【心臓(かおり)の反応】  
  
■09/11(木)  
　Comic Bunch第41号(9月26日号)发售  
　「**Angel Heart**」第101話  
　【妹は幸せだった？】  
  
■09/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第8巻 发售  
　▽封面是Comic Bunch第19号封面  
　→超豪華！有装裱的彩纸Present  
  
■09/05(金)  
　Comic Bunch第40号(9月19日号)发售  
　「**Angel Heart**」第100話/封面・巻頭Color  
　【妹を捜して！】  
　▽AH QUO Card T-shirt Present etc...  
  
■08/29(金)  
　Comic Bunch第39号(9月12日号)  
　「**Angel Heart**」暂停。  
　▽下期是AH封面＆巻頭Color 
  
■08/22(金)  
　Comic Bunch第38号(9月5日号)发售  
　「**Angel Heart**」第99話  
　【恋する人の気持ち】  
　▽下期暂停。40号是新展開巻頭Color 
  
■08/08(金)  
　Comic Bunch第36・37合并特大号 发售  
　「**Angel Heart**」第98話  
　【初恋、涙の別れ】  
　▽豪華特別付録・AH第1話オールColor収録  
  
■08/01(金)  
　Comic Bunch第35号(8月15日号)发售  
　「**Angel Heart**」第97話/封面  
　【幸せな笑顔】  
　▽下期Bunch、AH第1話48页以Full Color完全収録  
  
■07/25(金)  
　Comic Bunch第34号(8月8日号)发售  
　「**Angel Heart**」第96話  
　【遠い約束】  
  
■07/17(木)  
　Comic Bunch第33号(8月1日号)  
　「**Angel Heart**」暂停。  
  
■07/11(金)  
　Comic Bunch第32号(7月25日号)发售  
　「**Angel Heart**」第95話  
　【私、恋してます】  
　▽下期是AH暂停。96話是34号  
  
■07/04(金)  
　Comic Bunch第31号(7月18日号)发售  
　「**Angel Heart**」第94話/巻頭Color  
　【父親が娘を想う気持ち】  
  
■06/27(金)  
　Comic Bunch第30号(7月11日号)发售  
　「**Angel Heart**」第93話  
　【恋より深い感情】  
  
■06/20(金)  
　Comic Bunch第29号(7月4日号)发售  
　「**Angel Heart**」第92話  
　【これが、恋？】  
  
■06/13(金)  
　Comic Bunch第28号(6月27日号)发售  
　「**Angel Heart**」第91話  
　【本当の表情(かお)】  
  
■06/09(月)  
　★Bunch Comics  
　「**Angel Heart**」第7巻 发售  
　▽封面是Comic Bunch第10号封面  
　→累計500万部突破記念Present  
  
■06/06(金)  
　Comic Bunch第27号(6月20日号)发售  
　「**Angel Heart**」第90話/封面  
　【初めての感情】  
  
■05/30(金)  
　Comic Bunch第26号(6月13日号)发售  
　「**Angel Heart**」第89話  
　【遅れて来た幸せ】  
  
■05/23(金)  
　Comic Bunch第25号(6月6日号)  
　「**Angel Heart**」暂停。  
  
■05/16(金)  
　Comic Bunch第24号(5月30日号)发售  
　「**Angel Heart**」第88話/封面  
　【あたたかい銃弾】  
　▽下期是AH暂停。89話系26号发表。
  
■05/09(金)  
　Comic Bunch第23号(5月23日号)发售  
　「**Angel Heart**」第87話  
　【唯一の愛情】  
　▽AH2周年記念Postcard 2種  
  
■04/25(金)  
　Comic Bunch第21・22合并特大号 发售  
　「**Angel Heart**」第86話  
　【思い出の場所】  
　▽AH Original ・Tapestry、QUO Card、图书Card Present  
  
■04/18(金)  
　Comic Bunch第20号(5月2日号)发售  
　「**Angel Heart**」第85話  
　【愛に飢えた悪魔】  
　▽下周Bunch带Sign的AH Original Goods Present  
  
■04/11(金)  
　Comic Bunch第19号(4月25日号)发售  
　「**Angel Heart**」第84話/封面  
　【悪魔のエンディング】  
  
■04/07(月)  
　Bunch公式i-modeSite「ｉBunch」OPEN  
　▽**AH**のGameや待受画像など  
　Access方法：iMenu→menu list→TV/广播/雑誌→(5)雑誌→ｉBunch  
　每月：300日元  
  
■04/04(金)  
　Comic Bunch第18号(4月18日号)发售  
　「**Angel Heart**」第83話  
　【哀しき連続殺人犯】  
  
■03/28(金)  
　Comic Bunch第17号(4月11日号)发售  
　「**Angel Heart**」第82話  
　【危険な匂い】  
  
■03/20(木)  
　Comic Bunch第16号(4月3日号)发售  
　「**Angel Heart**」第81話  
　【心臓(かおり)の涙】  
  
■03/14(金)  
　Comic Bunch第15号(3月28日号)发售  
　「**Angel Heart**」第80話/巻頭Color  
　【ターゲットは香瑩】  
  
■03/08(土)  
　★Bunch Comics  
　「**Angel Heart**」第6巻 发售  
　▽封面是Comic Bunch'02年第44号封面  
  
■03/07(金)  
　Comic Bunch第14号(3月21日号)发售  
　「**Angel Heart**」第79話/封面  
　【冴子からのＸＹＺ】  
　▽下周是AH巻頭Color  
  
■02/28(金)  
　Comic Bunch第13号(3月14日号)  
　「**Angel Heart**」暂停。  
  
■02/21(金)  
　Comic Bunch第12号(3月7日号)发售  
　「**Angel Heart**」第78話  
　【遺された指輪】  
　▽第13号AH休載  
  
■02/14(金)  
　Comic Bunch第11号(2月28日号)发售  
　「**Angel Heart**」第77話  
　【本当の家族】  
  
■02/07(金)  
　Comic Bunch第10号(2月21日号)发售  
　「**Angel Heart**」第76話/封面  
　【もう一度あの頃に…】  
  
■01/31(金)  
　Comic Bunch第9号(2月14日号)发售  
　「**Angel Heart**」第75話  
　【命より大切な絆】  
  
■01/24(金)  
　Comic Bunch第8号(2月7日号)发售  
　「**Angel Heart**」第74話  
　【別れの五日元玉】  
  
■01/17(金)  
　Comic Bunch第7号(1月31日号)发售  
　「**Angel Heart**」第73話  
　【不器用な男】  
  
■01/10(金)  
　Comic Bunch新年6号(1月24日号)发售  
　「**Angel Heart**」第72話  
　【伝わらぬ思い】  
　▽新春特典2003年版AH Original Calendar（1～2月）  
  
■01/07(火)  
　Raijin Collection  
　「北条司短編集 Vol.1 天使の贈りもの」发售  
　▽**CityHunter**收录2个短篇故事  
　→定价286日元＋税  
  
■01/06(月)  
　英語版漫画雑誌  
　RAIJIN COMICS（ライジンコミックス）vol.4（2月号）发售  
　「**CITY HUNTER**（英語版）」  
　"EPISODE5: A SNIPER IN THE DARK"  
　▽特別付録Listening CD（CH和北斗の拳）  
  
**→[2002年的信息](./iframe02.md)**