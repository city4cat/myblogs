http://www.netlaputa.ne.jp/~arcadia/mini5.html

[戻る](./mini4.md)

>   
> Mini系列，到Mk.3  
>   
> 　随着1969年向Mk3的发展，Mini Cooper消失了，只有Mini Cooper S（1970年的BL Mini Cooper Mk3）上市。 标志也与轿车型迷你汽车共享，因此Cooper的个性较少，Cooper和Cooper S车型过去所具有的魅力和特殊性也非常低。 格式也被改为ADO20。 轴距延长了10mm，汽车变得稍微重了些。 石油冲击和竞争对手汽车日益增长的性能最终导致Cooper系列车型在1971年7月停产。  
>   
> 　Mk3时代的Mini车被俗称为BL Mini车，Austin和Morris品牌也不复存在。  
>   
> 　最有力的说法是，Mini Mk3指的是到1980年为止的车型。 事实上，只有Cooper S被制造商正式命名为Mk3，因此有各种说法认为Mk3时代一直持续到1971年Cooper S生产结束，或直到1975年尾灯从2种颜色变为3种颜色，但制造商从未推出Mk4，所以其区别是模糊的。  
>   
> 　许多车迷大声说，真正的Mini车是在Mk3时代才出现的。  
>   
> 　在日本，Austin型Mini车由Capital Enterprises（后来的Yanase）进口和销售，Maurice型Mini车由Nichibei Jidosha进口，但由于不符合排放规定等原因，Mini车的正式进口在1976年停止。 然而，Mini车的受欢迎程度仍然不减，1981年，在经过约5年的空白期后，Mini1000日英自动车再次由日北治道社进口。 当时的车型是一种名为Mini 1000 HL（High Line）的规格。    
>   
>   

[次へ](./mini6.md)

---

---

[あばうとミニへ](./mini0.md)