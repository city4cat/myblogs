http://www.netlaputa.ne.jp/~arcadia/iframe06.html

**→[2007年的信息](./iframe07.md)**  
  
**▼2006年的信息**  
  
■12/28(木)  
　Comic Bunch新年5・6合并特刊 发售  
　「**Angel Heart**」第237話/封面  
　【カメレオン】  
　▽巻頭Color企划・有奖的AH测试  
　▽特別附录・AH Comics Cover   
　第21巻是1/9发售→初版限定版 附有专用书带   
　▽Comics联动、DVD BOX3 Present申请券(15名)  
  
■12/22(金)  
　Comic Bunch新年4号(1月19日号)发售  
　「**Angel Heart**」第236話  
　【危険な出会い】  
　▽下期5・6合并特刊的发售是12月28日(木)  
　→下期是AH封面、AH Comics Cover 附录  
　＆AH审定召开  
  
■12/15(金)  
　Comic Bunch新年3号(1月12日号)发售  
　「**Angel Heart**」第235話  
　【肉弾戦】  
  
■12/01(金)  
　Comic Bunch新年1・2合并特刊 发售  
　「**Angel Heart**」第234話/巻頭Color  
　【いい子の行き着く場所】 
  
■11/24(金)  
　Comic Bunch第52号(12月8日号)发售  
　「**Angel Heart**」暂停。  
　▽下期、新年1・2合并特刊是AH巻頭Color  
  
■11/17(金)  
　Comic Bunch第51号(12月1日号)发售  
　「**Angel Heart**」第233話  
　【嫌な予感】  
  
■11/10(金)  
　Comic Bunch第50号(11月24日号)发售  
　「**Angel Heart**」暂停。 
  
■11/02(木)  
　Comic Bunch第49号(11月17日号)发售  
　「**Angel Heart**」第232話  
　【机男と親衛隊(シンエータイ)！】  
  
■10/27(金)  
　Comic Bunch第48号(11月10日号)发售  
　「**Angel Heart**」第231話  
　【ボクら、シャンイン親衛隊！？】  
  
■10/20(金)  
　Comic Bunch第47号(11月3日号)发售  
　「**Angel Heart**」第230話/封面  
　【ラブレター・パニック！】  
  
■10/13(金)  
　Comic Bunch第46号(10月27日号)发售  
　「**Angel Heart**」第229話  
　【想い、街に息づいて】  
  
■10/06(金)  
　Comic Bunch第45号(10月20日号)发售  
　「**Angel Heart**」第228話  
　【私たちの学校】  
  
■09/29(金)  
　Comic Bunch第44号(10月13日号)发售  
　「**Angel Heart**」第227話  
　【香瑩、入学す！】  
　▽动画AH News、Chief P 諏訪的评论  
  
■09/25(月) 读卖TV/25:25～  
■09/26(火) 日本TV/25:59～  
　动画**Angel Heart** #50  
　「Last Present」  
　→最終回  
　▽DVD BOX2, Soundtrack CD Present 
  
■09/22(金)  
　Comic Bunch第43号(10月6日号)发售  
　「**Angel Heart**」第226話  
　【真実と紗世】  
　▽动画AH News、最終話提要和要点  
  
■09/18(月) 读卖TV/25:25～  
■09/19(火) 日本TV/25:29～  
　动画**Angel Heart** #49  
　「Get My Life」  
　→下回「Last Present」  
  
■09/15(金)  
　Comic Bunch第42号(9月29日号)发售  
　「**Angel Heart**」暂停。  
  
■09/11(月) 读卖TV/24:55～  
■09/12(火) 日本TV/25:29～  
　动画**Angel Heart** #48  
　「引き寄せられる運命」  
　→下回「Get My Life」   
  
■09/09(土)  
　★Bunch Comics  
　「**Angel Heart**」第20巻 发售  
　▽封面是Comic Bunch第34号封面的图案  
  
■09/08(金)  
　Comic Bunch第41号(9月22日号)发售  
　「**Angel Heart**」第225話/封面  
　【父のいる店】  
  
■09/04(月) 读卖TV/24:55～  
■09/05(火) 日本TV/26:59～  
　动画**Angel Heart** #47  
　「アカルイミライ！？」  
　→下回「引き寄せられる運命」  
  
■09/01(金)  
　Comic Bunch第40号(9月15日号)发售  
　「**Angel Heart**」暂停。  
  
■08/28(月) 读卖TV/25:25～  
■08/29(火) 日本TV/25:29～  
　动画**Angel Heart** #46  
　「マザー・ハート」  
　→下回「アカルイミライ！？」  
  
■08/25(金)  
　Comic Bunch第39号(9月8日号)发售  
　「**Angel Heart**」第224話  
　【重ねる嘘、重なる罪】  
  
■08/21(月) 读卖TV/25:10～  
■08/22(火) 日本TV/26:14～  
　动画**Angel Heart** #45  
　「人間核弾頭、楊(ヤン)」  
　→下回「マザー・ハート」  
  
■08/14(月) 读卖TV/24:55～  
■08/15(火) 日本TV/25:29～  
　动画**Angel Heart** #44  
　「俺たちの子供のために」  
　→下回「人間核弾頭、楊(ヤン)」 
  
■08/11(金)  
　Comic Bunch第37・38合并特刊 发售  
　「**Angel Heart**」第223話  
　【紗世是名探偵！？】  
　▽动画AH News、第44話简介  
　▽7/29 天保山AH Event的情况 
  
■08/07(月) 读卖TV/25:14～  
■08/08(火) 日本TV/25:29～  
　动画**Angel Heart** #43  
　「私が生きる日常」  
　→下回「俺たちの子供のために」 
  
■08/04(金)  
　Comic Bunch第36号(8月18日号)发售  
　「**Angel Heart**」第222話  
　【カギ是香瑩にあり！？】  
　▽动画AH News、第43話提要和要点  
　→新OP Theme CD Present(20名)  
  
■07/31(月) 读卖TV/25:00～  
■08/01(火) 日本TV/25:29～  
　动画**Angel Heart** #42  
　「二人だけのSign」  
　→下回「私が生きる日常」  
  
■07/28(金)  
　Comic Bunch第35号(8月11日号)发售  
　「**Angel Heart**」第221話  
　【１８歳是シゲキがお好き！？】  
　▽动画AHNews、第42話提要和要点  
　▽令人兴奋的宝岛AH活动通知  
  
■07/24(月) 读卖TV/25:09～  
■07/25(火) 日本TV/25:38～  
　动画**Angel Heart** #41  
　「自分の居場所」  
　→下回「二人だけのSign」  
  
■07/21(金)  
　Comic Bunch第34号(8月4日号)发售  
　「**Angel Heart**」第220話/封面  
　【謎のお父さん！】  
　▽紧急通知・令人兴奋的宝岛 AH Event  
　→7/29(土)13:00～  
　由一位出色的音乐家现场演奏＆  
　川崎真央Talk Show举办  
  
■07/17(月) 读卖TV/25:04～  
■07/18(火) 日本TV/25:38～  
　动画**Angel Heart** #40  
　「ミキの隠された秘密」  
　→下回「自分の居場所」  

■07/14(金)  
　Comic Bunch第33号(7月28日号)发售  
　「**Angel Heart**」暂停。  
  
■07/10(月) 读卖TV/25:25～  
■07/11(火) 日本TV/25:29～  
　动画**Angel Heart** #39  
　「依頼者是大女優」  
　→下回「ミキの隠された秘密」  
  
■07/07(金)  
　Comic Bunch第32号(7月21日号)发售  
　「**Angel Heart**」第219話  
　【私がパパです！】  
　▽动画AHNews、第39話提要和要点  
  
■07/03(月) 读卖TV/25:25～  
■07/04(火) 日本TV/25:29～  
　动画**Angel Heart** #38  
　「オレの目になってくれ」  
　→下回「依頼者是大女優」 
  
■06/30(金)  
　Comic Bunch第31号(7月14日号)发售  
　「**Angel Heart**」第218話  
　【大幸運期到来！！】  
  
■06/26(月) 读卖TV/24:55～  
■06/27(火) 日本TV/25:29～  
　动画**Angel Heart** #37  
　「汚れのない心」  
　→下回「オレの目になってくれ」 
  
■06/23(金)　　　/[情報Gen](./cocktail/info.md)/  
　Comic Bunch第30号(7月7日号)发售  
　「**Angel Heart**」第217話  
　【楊の生きる理由】  
　▽动画AHNews、第37話提要和要点  
　▽天宝山10天令人兴奋的宝島AH Event  
　→7/28(金)～8/6(日)  
  
■06/19(月) 读卖TV/25:16～  
■06/20(火) 日本TV/25:29～  
　动画**Angel Heart** #36  
　「幸せを運ぶ女の子」  
　→下回「汚れのない心」  
  
■06/16(金)  
　Comic Bunch第29号(6月30日号)发售  
　「**Angel Heart**」第216話  
　【追う女、待つ女】  
　▽动画AH News、第36話提要  
　→Comics联动・DVD等Present(150名)申请券  
  
■06/12(月) 读卖TV/24:55～  
■06/13(火) 日本TV/25:29～  
　动画**Angel Heart** #35  
　「未来へ…」  
　→下回「幸せを運ぶ女の子」 
  
■06/09(金)  
　Comic Bunch第28号(6月23日号)发售  
　「**Angel Heart**」第215話/封面  
　【Ｃ・Ｈ資格テスト！】  
　▽巻頭Color企划、結成AH応援団  
　→Comics联动・DVD等Present(150名)申请券  
　▽动画AH News、第35話提要  
  
■06/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第19巻 发售  
　▽封面是Comic Bunch第22・23合并特刊封面的图案  
　→AH DVD BOX2等Present(150名)申请券  
  
■06/05(月) 读卖TV/24:55～  
■06/06(火) 日本TV/25:29～  
　动画**Angel Heart** #34  
　「二人の決意」  
　→下回「未来へ…」  
  
■06/02(金)  
　Comic Bunch第27号(6月16日号)发售  
　「**Angel Heart**」第214話  
　【オペレーション ナシクズシ！】  
  
■05/29(月) 读卖TV/24:55～  
■05/30(火) 日本TV/25:29～  
　动画**Angel Heart** #33  
　「神から授かりし子」  
　→下回「二人の決意」  
  
■05/26(金)  
　Comic Bunch第26号(6月9日号)发售  
　「**Angel Heart**」暂停。  
  
■05/22(月) 读卖TV/24:55～  
■05/23(火) 日本TV/25:29～  
　动画**Angel Heart** #32  
　「組織から来た女」  
　→下回「神から授かりし子」  
  
■05/19(金)  
　Comic Bunch第25号(6月2日号)发售  
　「**Angel Heart**」第213話  
　【男の性(サガ)！】  
　▽动画AH News、第32話提要和要点  
  
■05/15(月) 读卖TV/24:55～  
■05/16(火) 日本TV/25:29～  
　动画**Angel Heart** #31  
　「最後の夜に見た奇跡」  
　→下回「組織から来た女」  
  
■05/12(金)  
　Comic Bunch第24号(5月26日号)发售  
　「**Angel Heart**」第212話  
　【長生きの代償】  
　▽創刊5周年企划・这部Bunch漫画太精彩了！  
　→AH最新动画化情報＆令人难忘的委托Best5  
　▽动画AH News、第30話提要和要点  
  
■05/08(月) 读卖TV/24:55～  
■05/09(火) 日本TV/25:59～  
　动画**Angel Heart** #30  
　「この街是私の全て」  
　→下回「最後の夜に見た奇跡」  
  
■05/01(月) 读卖TV/24:55～  
■05/02(火) 日本TV/25:29～  
　动画**Angel Heart** #29  
　「私の妹…香」  
　→下回「この街是私の全て」  
  
■04/28(金)　　　/[情報Gen](./cocktail/info.md)/  
　Comic Bunch第22・23合并特刊 发售  
　「**Angel Heart**」第211話/封面  
　【男二人、車中にて】  
　▽特別附录AHAH袋装Premium Postcard  
　▽动画AH News、第29話提要和要点  
  
■04/24(月) 读卖TV/24:55～  
■04/25(火) 日本TV/25:29～  
　动画**Angel Heart** #28  
　「約束」  
　→下回「私の妹…香」  
  
■04/21(金)  
　Comic Bunch第21号(5月5日号)发售  
　「**Angel Heart**」第210話  
　【マオの決断】  
　▽动画AH News、第28話提要和要点  
  
■04/17(月) 读卖TV/25:25～  
■04/18(火) 日本TV/25:29～  
　动画**Angel Heart** #27  
　「私、恋してる！？」  
　→下回「約束」  
  
■04/14(金)  
　Comic Bunch第20号(4月28日号)发售  
　「**Angel Heart**」第209話  
　【迸る想い！】  
　▽动画AH News、第27話提要  
　▽玉置浩二 Special Interview  
　→新OP「Lion」CD Present(20名)  
  
■04/10(月) 读卖TV/25:30～  
■04/11(火) 日本TV/25:29～  
　动画**Angel Heart** #26  
　「もう一度あの頃に」  
　→下回「私、恋してる！？」  
  
■04/07(金)  
　Comic Bunch第19号(4月21日号)发售  
　「**Angel Heart**」暂停。  
  
■04/03(月) 读卖TV/25:30～  
■04/04(火) 日本TV/25:49～  
　动画**Angel Heart** #25  
　「死にたがる依頼者」  
　→下回「もう一度あの頃に」  
  
■03/31(金)  
　Comic Bunch第18号(4月14日号)发售  
　「**Angel Heart**」第208話  
　【海辺の告白！】  
　▽动画AHNews、第25話提要  
　▽宇都宮隆×川崎真央 特別対談  
　→Signed AH 专用 Clear File Present  
  
■03/27(月) 读卖TV/25:03～  
■03/28(火) 日本TV/26:25～  
　动画**Angel Heart** #24  
　「鼓動と共に…」  
　→下回「死にたがる依頼者」  
  
■03/24(金)  
　Comic Bunch第17号(4月7日号)发售  
　「**Angel Heart**」暂停。  
  
■03/20(月) 读卖TV/25:29～  
■03/21(火) 日本TV/25:25～  
　动画**Angel Heart** #23  
　「出発(たびだち)のメロディー」  
　→下回「鼓動と共に…」  
  
■03/17(金)  
　Comic Bunch第16号(3月31日号)发售  
　「**Angel Heart**」第207話  
　【酒と泪(なみだ)と皇子と母親！？】  
　▽动画AH News、第23話提要  
　▽Comics联动企划・DVD BOX Present申请券  
  
■03/13(月) 读卖TV/25:03～  
■03/14(火) 日本TV/25:25～  
　动画**Angel Heart** #22  
　「不公平な幸せ」  
　→下回「出発(たびだち)のメロディー」  
  
■03/10(金)  
　Comic Bunch第15号(3月24日号)发售  
　「**Angel Heart**」第206話  
　【皇子の苦悩】  
　▽动画AH News、第22話提要  
　▽Comics联动企划・DVD BOX Present申请券  
  
■03/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第18巻 发售  
　▽封面是Comic Bunch第8号Color扉页的图案  
　→AH DVD BOX Present(15名)  
  
■03/06(月) 读卖TV/25:03～  
■03/07(火) 日本TV/25:25～  
　动画**Angel Heart** #21  
　「哀しき守護者(ガーディアン)」  
　→下回「不公平な幸せ」  
  
■03/03(金)  
　Comic Bunch第14号(3月17日号)发售  
　「**Angel Heart**」第205話  
　【日常に潜むアクマ】  
　▽动画AH News、第21話提要和要点  
　▽3/6(月)、动画AH手机Site Open  
　→推迟到4月以后  
  
■02/27(月) 读卖TV/25:03～  
■02/28(火) 日本TV/25:55～  
　动画**Angel Heart** #20  
　「宿命のプレリュード」  
　→下回「哀しき守護者(ガーディアン)」  
  
■02/24(金)  
　Comic Bunch第13号(3月10日号)发售  
　「**Angel Heart**」第204話  
　【家族の風景】  
　▽动画AH News、第20話提要和要点  
  
■02/20(月) 读卖TV/25:03～  
■02/21(火) 日本TV/25:25～  
　动画**Angel Heart** #19  
　「陳老人の店」  
　→下回「宿命のプレリュード」  
  
■02/17(金)  
　Comic Bunch第12号(3月3日号)发售  
　「**Angel Heart**」第203話  
　【マオの交渉】  
　▽动画AH News、第19話提要和要点  
  
■02/13(月) 读卖TV/25:03～  
■02/14(火) 日本TV/25:25～  
　动画**Angel Heart** #18  
　「親子の絆」  
　→下回「陳老人の店」  
  
■02/10(金)  
　Comic Bunch第11号(2月24日号)发售  
　「**Angel Heart**」暂停。  
  
■02/06(月) 读卖TV/27:03～  
■02/07(火) 日本TV/26:00～  
　动画**Angel Heart** #17  
　「夢の中の出会い」  
　→下回「親子の絆」  
  
■02/03(金)  
　Comic Bunch第10号(2月17日号)发售  
　「**Angel Heart**」第202話  
　【昨日の敵是今日の恋人！】  
　▽动画AH News、第17話提要和要点  
  
■01/30(月) 读卖TV/24:59～  
■01/31(火) 日本TV/25:40～  
　动画**Angel Heart** #16  
　「Ｃ・Ｈとしての資格」  
　→下回「夢の中の出会い」  
  
■01/27(金)  
　Comic Bunch第9号(2月10日号)发售  
　「**Angel Heart**」第201話  
　【皇子と香瑩の初デート(是ぁと)】  
　▽动画AH News、第16話提要和要点  
　→AH DVD Premium BOX 发售日大決定  
  
■01/23(月) 读卖TV/24:59～  
■01/24(火) 日本TV/25:25～  
　动画**Angel Heart** #15  
　「パパを捜して」  
　→下回「Ｃ・Ｈとしての資格」  
  
■01/20(金)  
　Comic Bunch第8号(2月3日号)发售  
　「**Angel Heart**」第200話/封面/巻頭Color  
　【妄想超特急がやってきた！】  
　▽动画AH News、第15話提要和要点  
  
■01/16(月) 读卖TV/24:59～  
■01/17(火) 日本TV/25:25～  
　动画**Angel Heart** #14  
　「復活Ｃ・Ｈ！」  
　→下回「パパを捜して」  
  
■01/13(金)  
　Comic Bunch第7号(1月27日号)发售  
　「**Angel Heart**」第199話  
　【異国からの求婚者！】  
　▽动画AH News、第14話提要和要点  
　→「Finally」CD 20名Present  
  
■01/09(月) 读卖TV/25:50～  
■01/10(火) 日本TV/25:55～  
　动画**Angel Heart** #13  
　「李大人からの贈り物」  
　→下回「復活Ｃ・Ｈ！」  
  
■01/06(金)  
　Comic Bunch新年6号(1月20日号)发售  
　「**Angel Heart**」第198話  
　【クリスマスPresent】  
　▽动画AH News、第13話提要和要点  
　→OP曲「Finally」CD 2/1(水)发售  
  
**→[2005年的信息](./iframe05.md)**