http://www.netlaputa.ne.jp/~arcadia/iframe08.html

**→[2009年的信息](./iframe.md)**  
  
**▼2008年的信息**  
  
■12/26(金)  
　Comic Bunch新年4・5合并特大号 发售  
　「**Angel Heart**」第316話  
　【冴子の覚悟】  
  
■12/19(金)  
　Comic Bunch新年3号(1月9日号)发售  
　「**Angel Heart**」第315話/封面  
　【仕組まれた黙秘】  
　▽特別附录・AH 专用 Book Cover 
  
■12/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第28巻 发售  
　▽封面是Comic Bunch第39号Color扉页的图案  
  
■12/05(金)  
　Comic Bunch新年1・2合并特大号 发售  
　「**Angel Heart**」第314話  
　【怪我の功名】  
　▽特別附录2009年Bunch 专用 Calendar  
　→11月・12月是AH  
  
■11/28(金)  
　Comic Bunch第52号(12月12日号)发售  
　「**Angel Heart**」第313話  
　【長い一日】  
　▽AH Original Zippo(一次2種)  
　　Bunch在移动SHOP中确定发售  
　→详见12/9、Site上发布  
  
■11/21(金)  
　Comic Bunch第51号(12月5日号)发售  
　「**Angel Heart**」第312話  
　【影武者】  
  
■11/14(金)  
　Comic Bunch第50号(11月28日号)发售  
　「**Angel Heart**」暂停。  
  
■11/07(金)  
　Comic Bunch第49号(11月21日号)发售  
　「**Angel Heart**」第311話  
　【宣戦布告】  
  
■10/31(金)  
　Comic Bunch第48号(11月14日号)发售  
　「**Angel Heart**」第310話  
　【真実(マコト)】  
  
■10/24(金)  
　Comic Bunch第47号(11月7日号)发售  
　「**Angel Heart**」第309話  
　【離れていても】  
  
■10/17(金)  
　Comic Bunch第46号(10月31日号)发售  
　「**Angel Heart**」第308話  
　【新婚旅行】  
  
■10/10(金)  
　Comic Bunch第45号(10月24日号)发售  
　「**Angel Heart**」暂停。  
  
■10/03(金)  
　Comic Bunch第44号(10月17日号)发售  
　「**Angel Heart**」第307話  
　【誓いの言葉】  
  
■09/26(金)  
　Comic Bunch第43号(10月10日号)发售  
　「**Angel Heart**」第306話/封面  
　【幸せのベール】  
　▽「Cat's Eye」Pachislo特集  
  
■09/19(金)  
　Comic Bunch第42号(10月3日号)发售  
　「**Angel Heart**」第305話  
　【確かな鼓動】  
  
■09/12(金)  
　Comic Bunch第41号(9月26日号)发售  
　「**Angel Heart**」暂停。  
  
■09/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第27巻 发售  
　▽封面是Comic Bunch第32号封面的图案  
  
■09/05(金)  
　Comic Bunch第40号(9月19日号)发售  
　「**Angel Heart**」第304話/封面  
　【似た者同士】  
  
■08/29(金)  
　Comic Bunch第39号(9月12日号)发售  
　「**Angel Heart**」第303話/封面/巻頭Color  
　【金木犀の香り】  
  
■08/22(金)  
　Comic Bunch第38号(9月5日号)发售  
　「**Angel Heart**」第302話  
　【身代わり】  
  
■08/08(金)  
　Comic Bunch第36・37合并特大号 发售  
　「**Angel Heart**」第301話  
　【潜入】  
  
■08/01(金)  
　Comic Bunch第35号(8月15日号)发售  
　「**Angel Heart**」第300話  
　【恋の季節】  
  
■07/25(金)  
　Comic Bunch第34号(8月8日号)发售  
　「**Angel Heart**」暂停。  
  
■07/18(金)  
　Comic Bunch第33号(8月1日号)发售  
　「**Angel Heart**」第299話  
　【陰影】  
  
■07/11(金)  
　Comic Bunch第32号(7月25日号)发售  
　「**Angel Heart**」第298話/封面  
　【幸せの涙】  
  
■07/04(金)  
　Comic Bunch第31号(7月18日号)发售  
　「**Angel Heart**」第297話  
　【親子の時間】  
  
■06/27(金)  
　Comic Bunch第30号(7月11日号)发售  
　「**Angel Heart**」暂停。  
  
■06/20(金)  
　Comic Bunch第29号(7月4日号)发售  
　「**Angel Heart**」第296話  
　【本当の姿】  
  
■06/13(金)  
　Comic Bunch第28号(6月27日号)发售  
　「**Angel Heart**」第295話  
　【最期の言葉】  
　▽AH Event報告→委托内容的部分介绍  
  
■06/06(金)  
　Comic Bunch第27号(6月20日号)发售  
　「**Angel Heart**」第294話  
　【懺悔と供養】  
　▽AH1500万部突破記念Event報告  
  
■05/30(金)  
　Comic Bunch第26号(6月13日号)发售  
　「**Angel Heart**」暂停。  
  
■05/23(金)  
　Comic Bunch第25号(6月6日号)发售  
　「**Angel Heart**」第293話  
　【傲慢】  
　▽Comics联动・AH 专用 frame申请用紙 
  
■05/16(金)  
　Comic Bunch第24号(5月30日号)发售  
　「**Angel Heart**」第292話  
　【笑い泣き】  
　▽Comics联动・AH 专用 frame申请用紙  
　▽創刊7周年記念・Bunch複製原画集Present 
  
■05/09(金)  
　★Bunch Comics  
　「**Angel Heart**」第26巻 发售  
　▽封面是Comic Bunch'07年第24号封面的图案  
　▽该杂志联动AH Fan 感恩节・第2弾  
　→「AH Pins ＆ 专用 frame」申请券  
  
■05/09(金)  
　Comic Bunch第23号(5月23日号)发售  
　「**Angel Heart**」番外編/封面  
　【一夜の友情】  
　→「新宿鮫」大沢在昌Special 合作  
　▽AH累計1500万部突破記念号  
　▽TOP3 CROSS TALK  
　→北条司×井上雄彦×梅澤春人  
　▽特別附录・AH 专用 图册  
　▽Event「Memory of X・Y・Z」详情  
　▽Comics联动・AH 专用 frame申请用紙  
　▽手机Bunch申请者全員無料Present  
　→AH 专用 手机防偷窥贴纸  
　▽AH Giclee 版画 Collection限量版销售  
  
■04/25(金)  
　Comic Bunch第21・22合并特刊 发售  
　「**Angel Heart**」第291話  
　【罪悪感】  
　▽北条司先生Sign会決定(5/17)  
　▽5月中旬开始在新宿举行Event  
　→「Memory of X・Y・Z」  
　▽下期(5/9发售)是AH Comics  
　累計1500万部突破記念号  
　→北条司大特集 SPECIAL2大企划  
  
■04/18(金)  
　Comic Bunch第20号(5月2日号)发售  
　「**Angel Heart**」第290話  
　【損な役回り】  
  
■04/11(金)  
　Comic Bunch第19号(4月25日号)发售  
　「**Angel Heart**」第289話/封面  
　【ロスタイム】  
  
■04/04(金)  
　Comic Bunch第18号(4月18日号)发售  
　「**Angel Heart**」第288話  
　【家族のために】  
  
■03/28(金)  
　Comic Bunch第17号(4月11日号)发售  
　「**Angel Heart**」第287話  
　【小さなＸＹＺ】  
  
■03/21(金)  
　Comic Bunch第16号(4月4日号)发售  
　「**Angel Heart**」暂停。  
　▽次号、新展開突入  
  
■03/14(金)  
　Comic Bunch第15号(3月28日号)发售  
　「**Angel Heart**」第286話  
　【潮騒】  
  
■03/07(金)  
　Comic Bunch第14号(3月21日号)发售  
　「**Angel Heart**」第285話  
　【不安な夜】  
  
■02/29(金)  
　Comic Bunch第13号(3月14日号)发售  
　「**Angel Heart**」第284話  
　【ジンクス】  
  
■02/22(金)  
　Comic Bunch第12号(3月7日号)发售  
　「**Angel Heart**」暂停。  
　▽第25巻联动AH Fan 感恩节・申请用紙  
  
■02/15(金)  
　Comic Bunch第11号(2月29日号)发售  
　「**Angel Heart**」第283話  
　【灯(あかり)】  
　▽第25巻联动AH Fan 感恩节・申请用紙  
  
■02/09(土)  
　★Bunch Comics  
　「**Angel Heart**」第25巻 发售  
　▽封面是Comic Bunch'07年第50号封面的图案  
　▽该杂志联动AH Fan 感恩节・ 专用  Pins 申请券  
  
■02/08(金)  
　Comic Bunch第10号(2月22日号)发售  
　「**Angel Heart**」第282話/封面  
　【カメレオン動く】  
　▽AH Fan 感恩节・申请者全員Service  
　→第25巻联动、AH 专用Pins 5件套装  
  
■02/01(金)  
　Comic Bunch第9号(2月15日号)发售  
　「**Angel Heart**」第281話  
　【ぷるん】  
  
■01/25(金)  
　Comic Bunch第8号(2月8日号)发售  
　「**Angel Heart**」第280話  
　【母の想い】  
  
■01/18(金)  
　Comic Bunch新年7号(2月1日号)发售  
　「**Angel Heart**」第279話  
　【一億の勝負】  
　▽Bunch全部作品T-shirt新春Present  
　→在Design Garden自由创作  
  
■01/10(木)  
　Comic Bunch新年6号(1月31日号)发售  
　「**Angel Heart**」第278話  
　【最凶悪危険人物】  
　▽Sign彩纸等、新年礼物  
  
**→[2007年的信息](./iframe07.md)**