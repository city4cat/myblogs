source:  
http://www.netlaputa.ne.jp/~arcadia/what.html


**[** [Office](./index.md) **]**

**[** [Contents調査報告書](./about.md) **]**

[joho-Gen](./cocktail/info.md)

## _FIGHT1发_ 更新记录

千分之一的更新，做起来都是浪费时间？（译注：待校对）

![](./image/clear.gif)

  
  

June 28, 2004

[OneHole Shot](./link.md)更新。

September 17, 2002

[OneHole Shot](./link.md)、美女☆LINK追加一件。

August 4, 2001

[OneHole Shot](./link.md)更新。

July 9, 2001

[Midnight☆Hunting](./cgi/hunter_index.md)Renewal。

July 1st, 2001

[JMC](./ah/index.md)更新。→今后随时更新。更新部分在JMC内的「更新状況」中。

June 27, 2001

[CocktailＸＹＺ](./cocktail.md)的CH相关书籍2。

June 26, 2001

[JMC](./ah/index.md)更新。

June 19, 2001

开设Angel Heart的Page[JMC](./ah/index.md)！

May 28, 2001

总之，目前，在所有的事情上面（？）[Angel Heart 感想BBS](./cgi/ah_bbs.md)是新成立的！

May 25, 2001

目前正在准备恢复更新。

October 05, 2000

因为管理员住院，所以暂时请假了更新。

June 30, 2000

3万Hit記念！？　[CocktailＸＹＺ](./cocktail.md)Renewal。  
[OneHole Shot](./link.md)、CH LINK追加一件。

May 31, 2000

在[CocktailＸＹＺ](./cocktail.md)中添加[新宿PHOTOMap](./shinjuku/map.md)。

May 23, 2000

增加「[Lucky Story?](./cgi/lucky_kuji.md)」的Data数据。共计250。

May 22, 2000

在[CocktailＸＹＺ](./cocktail.md)中追加[Story Digest](./cocktail.md#storyd)。

April 21, 2000

[CocktailＸＹＺ](./cocktail.md)添加CityHunter SoundTrack全部List。

April 15, 2000

增加像Banner一样的东西。

April 9, 2000

在[CocktailＸＹＺ](./cocktail.md)中追加「[Lucky Story?](./cgi/lucky_kuji.md)」（抽签）。也可以从Top的極楽蜻蜓那里访问它。  
[OneHole Shot](./link.md)、美女☆LINK追加一件。

April 6, 2000

[CocktailＸＹＺ](./cocktail.md)内[「About Mini」](./mini0.md)的目录中添加Mini的图片。(过去的使用方式)

April 1st, 2000

[CocktailＸＹＺ](./cocktail.md)内[「About Mini」](./mini0.md)2000年・Mini停产方面，部分加笔修正。  
[OneHole Shot](./link.md)、修正目标Link。

March 31, 2000

追加[「FIGHT1発更新記録」](./what.md)（本Page）和[「Contents調査報告書」](./about.md)的Page。

March 27, 2000

Top的Layout、若干变更中。

March 26, 2000

[No Flash Top](http://www.netlaputa.ne.jp/~arcadia/index2.html)(译注：整合到[index.md](./index.md))新作MIDI「ANGEL NIGHT～天使のいる場所～」的BGM Up。

March 20, 2000

Cat's Eye完成版捐赠给りさんちに（99年2月），由于M.T.F.C的关闭，撤回到[HammerTanma!］(./tanma.md)。(译注：待校对)

March 15, 2000

Top恢复到正常。

March 4, 2000

[Angel☆Dust](./dust.md)中的「嫌い帰れも好きのうち！？」Up。Top也临时Top。

January 31, 2000

真是个惊喜！ 集英社・Comic文庫編集部给我们写了关于GuideBook的信。 (这与更新无关＾＾；）

January 19, 2000

[HammerTanma！](./tanma.md)稍微更新一下

January 13, 2000

[Angel☆Dust](./dust.md)中「美樹ちゃん危機一発！？」（←不是错别字）Up。
[Midnight☆Hunting](./cgi/hunter_index.md)修复日期Y2K问题。[OneHole Shot](./link.md)更新。

January 1st, 2000

Top只需加上一点新年的气息。

![](./image/clear.gif)

**>** [Officeへ](./index.md) **<**