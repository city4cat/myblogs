http://www.netlaputa.ne.jp/~arcadia/mini6.html

[戻る](./mini5.md)

>   
> # Rover Mini  
>   
> 　1982年，公司名称「Austin Rover」再次恢复，这是一个令人怀念的Mini名称。 在日本，「Austin Rover Japan」于1983年5月成立，1985年，Nichibei Jidosha成为日英自动车 ARJ的成员。  
>   
> 　1982年10月，Mini系列中增加了一个 「Mayfair」型号。 Mayfair有一个非常现代的Mini车的外观，用螺丝固定的塑料盖子。 从那时起，Mayfair成为Mini的主要车型。 Mayfair这个名字是伦敦Hyde公园以东的一个高级住宅区的名字，也是伦敦社交场合的用语。  
>   
> 　从1984年开始，轮胎的轮毂尺寸也增加了。 (从10英寸到12英寸。 )，以便为前制动器配备更大直径的盘式制动器。  
>   
> 　事实上，那一年也是Cooper恢复的一年，尽管不是作为目录车型。 这一切都始于John Cooper本人向Rover公司提出复兴Cooper的建议。 然而，当时Rover公司没有能力将该计划付诸实施，它被断然拒绝。 然后，日本的特殊商店「Mini Maruyama」出现在现场。 Mini Maruyama把当时经销商的一辆车，做成了一辆怀旧的Cooper，并把它带给了John Cooper。 John Cooper对这辆车印象深刻，但回答说，对于现代Cooper来说，调教水平太高了，现代Cooper车型必须更加精致。 然而，这个完成度被采纳了，项目被独立进行，John Cooper本人负责发动机，Mini Maruyama负责内饰和外观。 结果是「Mini John Cooper」。
>   
> 　Mini John Cooper在全世界引起了轰动，甚至得到了Rover的称赞。 新的Cooper车型是1000cc车型，功率为62马力，但在1987年，Rover注意到了62马力的设定，决定开发1300cc的Cooper车型。    
>   
>   

[次へ](./mini7.md)

---

---

[あばうとミニへ](./mini0.md)