
# 原网址无法访问  
- http://www.tcup5.com/533/arcadia.html网址无法访问:  
The domain Tcup5.com may be for sale. Click here to inquire about this domain.  

- http://533.teacup.com/arcadia/bbs网址无法访问:  
teacup. byGMOのサービス終了のお知らせ  
teacup.サービスをご利用のみなさまへ。  
長年にわたりご愛顧いただきましたteacup.ですが、2022年8月1日(月)13:00をもちまして、サービスを終了させていただきました。  
これまでteacup.をご利用いただいた皆様には、心より感謝申し上げます。  
今後とも、GMOメディア株式会社のサービスをよろしくお願いいたします。  
2022年8月1日  
(teacup. byGMO终止服务的通知  
teacup.给使用teacup服务的各位。  
长年承蒙惠顾的teacup.，将于2022年8月1日(星期一)13:00停止服务。  
衷心感谢至今为止使用teacup.的各位。  
今后GMO媒体股份有限公司的服务也请多多关照。  
2022年8月1日  )  

- http://533.teacup.com/arcadia/bbs2


# 原网址在web.archive.org上的备份数据  

![](https://web.archive.org/web/19991023042109im_/http://www.netlaputa.ne.jp/~arcadia/image/bbstitle.jpg)  
・・・血と硝煙はオレの生き様、裏の世界の情報をウォンテッド！

[ [teacup. Level-1](https://web.archive.org/web/19991023042109/http://www.tcup.com/l1.html) ]　[ [BBS Town](https://web.archive.org/web/19991023042109/http://www.tcup.com/town.html) ]　[ [BOSHU!!](https://web.archive.org/web/19991023042109/http://www65.tcup.com/6504/flifli.html) ]　[ [CHAT!](https://web.archive.org/web/19991023042109/http://www.tcup.com/chat.html) ]　[ [前ページへ](javascript:history.go(-1)) ]

[PR]　[【120時間無料】さらに、クレジットカード不要!!のプロバイダはここ。チャンス!!](https://web.archive.org/web/19991023042109/http://www.arwin.com/cgi-localbin/adj02.cgi)

  

* * *

投稿者<input type="text" name="name" size="20" value="" maxlength="24">   
メール<input type="text" name="email" size="30" value="">   
題名<input type="text" name="subject" size="30">
<input type="submit" value="     投稿     ">
<input type="reset" value="消す">

内容_（記入通りに記録しますので適当に改行を入れてください。[利用可能タグ一覧](https://web.archive.org/web/19991023042109/http://www.tcup.com/bbs/tags.html)）  
<textarea name="value" rows="5" cols="82"></textarea>  
ＵＲＬ（リンクを入れたい場合はここに記入します）  
<input type="text" name="page" size="70" value="http://">

* * *

_新しい記事から表示します。最高70件の記事が記録され、それを超えると古い記事から削除されます。  
１回の表示で10件を越える場合は、下のボタンを押すことで次の画面の記事を表示します。_

* * *

- [2006年的部分数据](./2006.md)  
- [2005年的部分数据](./2005.md)
（[出现page list](https://web.archive.org/web/20050317040708/http://533.teacup.com/arcadia/bbs2)但无备份数据）    
    - CH完全版第30卷、第31卷，封面画给人“清爽”的感觉；  
- [2004年的部分数据](./2004.md)  
    - Da Vinci杂志采访北条；
    - 眠狂四郎 vs CH；
    - 新宿御苑也是CH的景点;
    - 完全版的BOX的应募;
    - CH Perfect Guide Book发售，网站相应更新；  
    - AH动画化决定
    - 12月8日播出的《Toribia之泉》中，神谷出演VTR;
    - Yahoo!Comics(提供在线欣赏的漫画服务)，发布CE。
- [2003年的部分数据](./2003.md)  
    - 本网站访问量突破20万；  
    - 海坊主Bobsapp；
    - 忠犬八公现在被制成标本，在上野的博物馆里；
    - 据说朝日新闻曾将Smith&Wesson写成Smith&Western；
    - CH的音乐被做成铃声；  
    - 纪伊国屋的Comic House销售JC版CH；  
    - 神谷明主持电台节目；
- [2002年的部分数据](./2002.md)  
    - “危险的家庭教师(动画)”里獠的衣服好像是文科科目老师兼生活指导；  
- [2001年的部分数据](./2001.md)  
    - 设定本论坛是可剧透；创建「Angel Heart 感想BBS」；
    - AH総集編； 
    - 「週刊Book Review」上出现了北条；  
    - 纽约曼哈顿的Bunch销售情况；  
- [2000年的部分数据](./2000.md)  
    - 在Jump小说中，有一部分是用彩色刊载的小海的婚礼场景。Mick没有出现；  
    - 在新宿、歌舞伎町一带特别能看到Mini；新宿的红白色Mini确实很多；  
    - 北条司老师的官方主页已经建立起来了。留言板上还有神谷明本人的留言，他的暱称是｢あっくん｣；
- [1999年的部分数据](./1999.md)  

* * *

_以上は、現在登録されている新着順1番目から10番目までの記事です。_

* * *

**インターネット無料診断でマイカーの価値がわかっちゃいます！**  
[![愛車の無料診断](https://web.archive.org/web/19991023042109im_/http://www100.tcup.net/images/jac.gif)](https://web.archive.org/web/19991023042109/http://jac.p-club.com/access/8911516/)  
[[恋人を見つけよう！](https://web.archive.org/web/19991023042109/http://p-club.com/o-net/access/93/)]　[[ＩＳＤＮの申し込み](https://web.archive.org/web/19991023042109/http://www.arwin.com/ISDN/)]　[[業界トップのＪＡＣで安心無料診断！](https://web.archive.org/web/19991023042109/http://jac.p-club.com/access/8911516/)]

[![lycos homepage](https://web.archive.org/web/19991023042109im_/http://www.lycos.co.jp/images/logo_link3.gif)](https://web.archive.org/web/19991023042109/http://www.lycos.co.jp/?FROM=teacup)      

ウェブを検索：<input type="text" name="query" size="30"><input type="image" width="70" height="20" alt="検索" border="0">

<form method="post" action="https://web.archive.org/web/19991023042109/http://www.tcup5.com/cgi-localbin/533/arcadia.cgi">
<input type="hidden" name="page" value="10">
<input type="submit" value="次のページ"></form>
<form><input type="button" value="ホームページへ" onclick="top.location.href='https://web.archive.org/web/19991023042109/http://www.netlaputa.ne.jp/~arcadia/'"></form>

[ [teacup. Level-1](https://web.archive.org/web/19991023042109/http://www.tcup.com/l1.html) ]　[ [BBS Town](https://web.archive.org/web/19991023042109/http://www.tcup.com/town.html) ]　[ [BOSHU!!](https://web.archive.org/web/19991023042109/http://www65.tcup.com/6504/flifli.html) ]　[ [CHAT!](https://web.archive.org/web/19991023042109/http://www.tcup.com/chat.html) ]　[ [前ページへ](javascript:history.go(-1)) ]

* * *

パスワード  <input type="text" name="pwd" size="10"> 
<input type="submit" value="管理者メニュー">

* * *

管理者：[xyz@sweeper.office.ne.jp](https://web.archive.org/web/19991023042109/mailto:xyz@sweeper.office.ne.jp)

[MiniBBS v7 + tcup](https://web.archive.org/web/19991023042109/http://www.tcup.com/l0.html)









