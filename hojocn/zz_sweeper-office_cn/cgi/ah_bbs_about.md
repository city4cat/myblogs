http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi?a=about

![Angel Heart 感想BBS](http://www.netlaputa.ne.jp/~arcadia/ah/ahbbs.jpg)  
[[返回掲示板](./ah_bbs.md)]  

当你使用它时  

*   这个掲示板(译注：BBS/论坛)是为包含「Angel Heart」最新话等**剧透的**感想Talk而设的掲示板。  
*   所以，不想被剧透的人，千万不要往下看。  
*   CityHunter的话题、其他信息等，请访问[うぉんてっどBBS(http://www.tcup5.com/533/arcadia.html)](./533/readme.md)。  

  

这个掲示板的特征  

**(功能)**  

　这个公告栏是i mode/PC/EZweb/J-sky组合的公告栏，功能如下。  

*   使用Cooki。发布一次投稿后，会存储投稿人的姓名和MailAddress、URL以及删除Password，从第二次开始就会自动输入。但是，i mode不支持。
*   您可以删除或修改输入的文章。
*   您可以从文章中搜索特定单词。
*   自动检测i mode/PC/EZweb/J-sky并提供适当的Interface。i mode提供C-HTML，PC提供HTML3.2，EZweb提供HDML 3.0，J-sky提供MML兼容HTML。
*   从PC、EZweb、J-sky，你也可以看到i mode的图形文字。

**(限制)**  

　Tag(HTML，HDML)不可用。

**(其他)**  

　对于发布的文章，将自动将半角假名转换为全角。  
　要从PC/EZweb/J-sky输入图形文字Code，请输入？x(CODE)；(16进制码)或？(CODE)；(对于10进制码)。或者，您可以直接输入SHIFT JISCode作为字符。  
　管理员认为严重不利的文章或诽谤他人的文章恕不另行通知，可能会被删除。  


管理员：[さいばーりょう](mailto:xyz@sweeper.office.ne.jp)

* * *

[Ir-i-BBS version 1.85b](http://www.irao.com/irao/ir-i-bbs/)  
Copyright (c) 1999 by Irao![](http://cgi.netlaputa.ne.jp/~arcadia/ah/aha.cgi)