http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi


![Angel Heart 感想BBS](http://www.netlaputa.ne.jp/~arcadia/ah/ahbbs.jpg)

パンパカパーン！你是这个地方建成以来第412018个美女～～!  

![はぁと](image/aheart2.gif)

  
★caution!★  

> *   这个掲示板(译注：BBS/论坛)是为包含「Angel Heart」最新话等**剧透的**感想Talk而设的掲示板。  
> *   所以，不想被剧透的人，千万不要往下看。  
> *   Tag不能使用。  
> *   手机等也可以Access・写入。→[将URL发送到手机](mailto:@?body=http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi)  
> *   CityHunter的话题、其他信息等，请访问[うぉんてっどBBS(http://533.teacup.com/arcadia/bbs)](./533/readme.md)。  

_● [Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/) ●_ 
[ [这个掲示板的概要](./ah_bbs_about) ] 
[ [JMC](../ah/index.md) ]  
公式Page→ 
[ [北条司公式HomePage](http://www.hojo-tsukasa.com/) ] 
[ [週刊Comic Bunch/COAMIX](http://www.coamix.co.jp/) ]  

* * *

<form action="http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi" method="POST">
<input type="hidden" name="kan" value="漢字">
<input type="hidden" name="a" value="regist"><tt>投稿者 <input type="text" size="20" name="name" maxlength="24"><br>
Mail <input type="text" size="30" name="email" maxlength="40"><br>
标题　<input type="text" size="30" maxlength="40" name="subject"></tt>
<input type="submit" value="     投稿     "><input type="reset" value="删除"><p>
削除Password<input type="password" size="10" maxlength="8" name="pass">（删除和修改帖子时需要）</p><p>
内容<br>
<textarea name="message" rows="5" cols="82" style="width: 300px; height: 20px;"></textarea></p><p>
URL（想加入HP的链接时请填写）<br><input type="text" size="20" name="webpage" maxlength="60"></p><p>
</p></form>
<form action="http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi" method=POST>
<select name=a><option value=find>操索
<option value=delete>删除<option value=select>修正<option value=logmode>Log</select>
<input type=hidden name=start value=0>
<input type=submit value="执行"></form>
---

BBS Posts:（建议按时间先后顺序阅读）  

- [2014-2023年的帖子](./ah_bbs_post14-23.md)  
- [2013年的帖子](./ah_bbs_post13.md)  
- [2012年的帖子](./ah_bbs_post12.md)  
- [2011年的帖子](./ah_bbs_post11.md)  
- [2010年的帖子](./ah_bbs_post10.md)  
    - [关于Comic Zenon创刊](./ah_bbs_post10.md#BunchStop6)；  
    - [关于Bunch休刊5](./ah_bbs_post10.md#BunchStop5)；  
    - [关于Bunch休刊4](./ah_bbs_post10.md#BunchStop4)；  
    - [关于Bunch休刊3](./ah_bbs_post10.md#BunchStop3)；  
    - [关于Bunch休刊2](./ah_bbs_post10.md#BunchStop2)；  
    - [关于Bunch休刊1](./ah_bbs_post10.md#BunchStop1)；  
    - [关于Bunch休刊0](./ah_bbs_post10.md#BunchStop0)；  
- [2009年的帖子](./ah_bbs_post09.md)  
    - 「描線の流儀」访谈的简介：[后篇](./ah_bbs_post09.md#interview1)；  
    - 「描線の流儀」访谈的简介：[前篇](./ah_bbs_post09.md#interview0)；  
    - CAFE ZENON：[报道1](./ah_bbs_post09.md#cafe_zenon1)；  
    - CAFE ZENON：[报道0](./ah_bbs_post09.md#cafe_zenon0)；  
- [2008年的帖子](./ah_bbs_post08.md)  
    - [北条司画的拉姆](./ah_bbs_post08.md#Ram)  
- [2007年的帖子](./ah_bbs_post07.md)  

(译注：[1999～2006年的部分帖子](./533/readme.md)  )  


---

按新到顺序1～10件／全459件
　　
<form action="http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi#list" method="POST">　　<input type="submit" name="pos:0" value="更新">　　<input type="submit" name="pos:10" value="下一页"></form>

[Ir-i-BBS version 1.85b](http://www.irao.com/irao/ir-i-bbs/)
Copyright (c) 1999 by Irao

[管理人Mode](http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi?a=ch_init)

[JMC](../ah/index.md)