http://www.netlaputa.ne.jp/~arcadia/mini.html

>   
> # Mini的誕生　～汽车工程的奇迹～  
>   
> 　* 代号为ADO15的Mini于1959年8月26日从 * BMC首次亮相，在1956年9月的苏伊士危机（埃及的纳赛尔上校封锁了苏伊士运河。 石油危机（停止了石油供应）导致英国实行汽油配给制，从德国进口的被称为Bubble车的小型汽车占领了英国。 被称为ADO15的小型汽车开发项目是为了扭转局势而设立的，BMC主席Leonard Lord将该项目委托给天才工程师 * Alec Issigonis。    
>   
> 　ADO 15所要求的概念是「能够毫无困难地容纳四个成年人，而且速度要快。 使用BMC现有的发动机，我们想创造一个比以前的任何房车都小的房车」。    
>   
> 　然而，可用于BMC的发动机是来自一款名为Morris Minor或Austin A35的汽车的发动机，它的长度为92cm，根本无法按原样安装。 革命性的想法是将发动机横向放置，并将变速器直接放在曲轴下面，在它们之间有一些齿轮。 简单地说，它是一个带有发动机和变速器的两层结构。 以这种方式建造的发动机和其他传动系统部件被极好地安置在整体式车身的副车架上。  
>   
> 　因此，ADO15是一款小型轿车，车身尺寸紧凑，长度只有3050mm，乘客+行李箱的空间占80%，动力占20%，这也是Issigonis的最初构想。 
>   
> 　最初，发动机为两层结构，是一个848cc的液冷直列式OHV发动机，缸径冲程为62.94 x 62.28mm。34马力，配有一个SU * 化油器。 最大扭矩为6.08kg-m，转速为2900rpm。变速器为4速。  
>   
> 　带有横向安装的发动机的FWD（Front Wheel Drive，FF是日语中的英语）对后来的小型车产生了深远的影响，因为它完全颠覆了小型车的后轮驱动设计，在那之前，后轮驱动被认为是最合理的。 今天，全轮驱动是小型车的标准，但几乎可以肯定的是，它是由Mini车普及的。  
>   
> 　这并不是Mini汽车被认为是创新的小型汽车的唯一原因。 轮胎正好位于车身4个角的布局和4轮独立悬挂；开发和采用10英寸车轮的轮胎所提供的低重心和稳定性。 悬架是橡胶锥型的，利用橡胶的反作用力。 这些以前从未有人尝试过的新挑战，催生了Mini车，它甚至被称为汽车工程的奇迹。  
>   
> 　顺便说一句，Mini汽车对10英寸轮毂轮胎的普及促进了日本正在进行的小排量「小型汽车」标准的发展。  
>   
> 　只有9个人参与了Mini车的生产：Issigonis、2名设计师、4名绘图员和2名兼职学生工人。 Issigonis更喜欢与少数精英合作的方法。  
>   
> 　Mini车的设计很独特，直线和弧线结合得很好，正如英国人所描述的「方与圆」，但每个侧面的圆弧面不仅仅是为了设计的原因，也是为了提高内部的刚性和安全容量，这是一个重要的因素。  
>   
> 　车身的一个特别特征是焊接空间。 通常情况下，它们会被藏在后面，但对于在Mini，它们被特意放在外面，以简化工作过程。 采用这种面板配置的原因是，如果某个面板损坏、生锈或腐蚀，只需更换该面板就可以轻松修复。  
>   
> 　1959年9月2日，Mini在100多个展厅同时推出。  
>   
> 　1周前，在8月26日的报纸上，在新闻发布会之后，该报纸写道「我们以前从未见过这样的小汽车。 而且它有前轮驱动和4轮独立悬挂。 耗油量只有50mpg（1加仑汽油可以开50英里）。 最高时速为70mph（换算为约110km/h）。 它只有10英尺长，但内部空间却比800磅以上的轿车要大。 而Austin Seven的含税价格却不到500英镑。 它一路滑行：所有4个轮子上的橡胶悬架都很顺滑舒适。 悬挂系统很紧凑，没有牺牲任何内部空间。 由于采用了前轮驱动，它的重量很轻，可以像跑车一样过弯！」文章说。  
>   
> 　Mini车由Austin和Morris销售，这两个品牌拥有广泛的英国小型汽车销售网络，采用的是现在所谓的徽章工程（所谓的双子车）。 Austin Mini被命名为Austin Seven，继承了战前大作的名字。 另一方面，Morris Mini车被命名为Morris Mini 小型车，以当时流行的小型车Morris小车为名。 直到1962年，「Mini」这个名字才被用于该车。（译注：待校对）  
>   
> 　它首次亮相时有2个等级：basic saloon 和 deluxe。  
>   
> 　车身颜色包括格子呢红、冲浪蓝、烟灰色、古英国白、杏仁绿、岛屿蓝、毛呢灰和栗色。  
>   
> *BMC代表British Motor Corporation(译注：英国汽车公司)，是当时欧洲最大的汽车公司。1939年，几家公司联合起来成立了Nuffield集团。 这家由Morris领导的公司在1952年增加了Austin，并扩展为BMC。 合并后的公司的车型在这个庞大的组织内作为品牌存活下来，保留了它们以前的名字，即使在新公司成立后也几乎完全按照以前的方式销售。1966年还吸收了Jaguar和Daimler，品牌范围从大众汽车到豪华汽车。 顺便说一下，Morris和Austin的竞争关系类似于日本的丰田和日产之间的竞争。  
>   
> *ADO是Austin Drawing Office的缩写，其15号车就是Mini。 它最初是Austin使用的代号，但即使在与Nuffield集团合并后，ADO仍被用于新车的设计。  
>   
> *Alec Issigonis后来被英国皇室授予「Sir（爵士）」头衔，以表彰他在开发Mini方面的成就。  
>   
> *Carburetor（化油器）：发动机中的一台机器，它将燃料汽油吹入由气缸内活塞上下运动产生的负压所吸入的空气混合物中。 雾化的汽油在气缸中被压缩、点燃并燃烧。  
>   
>   
> # Mini Cooper和Mini Cooper S的到来 ～Mini系列Mk.1的时代～  
>   
> 　当Mini上市时，正如许多汽车爱好者所期望的那样，它立即被引入了赛车领域：在1960年4月的日内瓦拉力赛上，它已经取得了横扫1升级别第1名和第2名的壮举。 而人们对拥有更强大发动机的运动车型的渴望也逐渐开始增长。  
>   
> 　Mini Cooper的完成就是对这一呼吁的回应。  
>   
> 　John Newton Cooper是当时F1的顶级竞争者和著名的工程师，他与Issigonis是好朋友，他通过赛车认识了Issigonis，当Mini的原型车建成后，他立即借来驾驶，并很早就意识到，如果进行改装，它的速度会大大增加。 然后，他说服了正在制造Mini的BMC公司，创造出Mini的高性能版本，即Mini Cooper。  
>   
> 　Cooper系列被称为ADO50，与basic Mini分开考虑。  
>   
> 　以Cooper为名的车型于1961年10月加入BMC阵容，即在Mini车首次亮相的2年后。虽然外观没有变化，但它采用了997cc的发动机，是原车型848cc发动机的放大版，用双SU的1-1/4化油器进行调校，功率从34马力增加到55马力，并采用了交叉变速器。 它甚至还配备了7英寸的盘式制动器。  
>   
> 　1962年，也就是Mini Cooper首次亮相的次年，Mini Cooper在全球取得了153次胜利。 Mini Cooper在拉力赛中的表现尤其令人印象深刻，它的出现在当时的汽车运动领域引起了巨大的轰动。  
>   
> 　速度更快的Mini Cooper成为城里的改装店展示其技术的最佳项目，其中最引人注目的是Daniel Richmond领导的Downton Engineering公司的改装Mini Cooper。 这种可以轻松达到160km/h的改装技术引起了BMC设计部门的注意，该部门受到了Mini Cooper销售成功的鼓舞，并最终决定生产下一款Mini Cooper。 这就是Mini Cooper S。  
>   
> 　Mini Cooper和Mini Cooper S之间的区别是，Mini Cooper是一个纯粹的系列车型，而Mini Cooper S则被设计成一个可供比赛使用的车型。Cooper S于1963年推出，刚公布时只有1071cc车型，但次年，即1964年，增加了970cc和 1275cc的Cooper S车型。 排量上令人眼花缭乱的变化是由于赛车中的等级划分，制造商自己为参加比赛的车手准备了适合每个等级的车型。  
>   
> 　Mini Cooper是由普通的Mini缸体调校而成的，而Mini Cooper S则完全不同，它使用的是由John Cooper从缸体上新设计的发动机。 似乎是为了证实它的起源，Mini Cooper S发动机有许多特征，使专家们一眼就能认出它是Mk1库珀S发动机，例如热交换器盖后部印有菱形标记。 曲轴是锻造的这一事实也是Mk1 Cooper S独有的特征。 甚至头部的螺柱螺栓数量也不同。 970cc的功率为65马力，1071cc的功率为67马力，1275cc的功率为75马力。 刹车系统也得到了加强，有7.5英寸的刹车盘。 化油器是SU1-1/2双缸，这3种类型的化油器都很常见。 970cc的最高速度为148m/h，1071cc的最高速度为152m/h，特别是1275cc是前所未有的超小型车，最高速度为160Km/h。 1965年后只有1275cc仍然在生产。 (更准确地说，970cc车型一直生产到1965年1月）  
>   
>   
> # Mini Cooper S的成功 ～在传奇的Monte Carlo拉力赛中4连胜～  
>   
> 　Mini Cooper S Mk1一直生产到1967年，当时Mini系列被移交给Mk2，并因其许多赛车运动的成就而闻名。 最著名的例子是它在Monte Carlo拉力赛中的成功。 其压倒性的实力至今仍是传奇。 1964年，被称为 "Mini车手之神 "的Paddy Hopkirk驾驶该车取得了胜利，随后是1965年的T. Makinen，1967年的A. Altonen。 Cooper S在1966年赢得了比赛，尽管它因违反照明规定而被取消了资格（被指责大灯是不能调光的类型）。  
>   
> 　尽管竞争对手包括Lancia Fulvia HF、Cortina Lotus、Ford Falcon和保时捷-911，所有这些都是更强大的汽车，但Cooper S获胜的事实真正证明了Cooper S的制造水平。 当时赢得Monte Carlo拉力赛比现在赢得Paris Dakar拉力赛更有声望。  
>   
> 　在日本，许多人看到一辆普通的Mini车就称它为Mini Cooper，这一方面是由于Mini Cooper因其成功而受到欢迎，另一方面是由于在当时的Mini系列进口车中，Mini Cooper的比例很高。  
>   
> 　与普通的Mini一样，Mini Cooper S从一开始就有两个品牌--Morris和Austin，但只有Mk1采用了不同的格栅设计以及徽章。 当然，它们基本上是同一车型，只是品牌不同。 在性能上没有区别。 英国人是一个以不更换品牌为荣的民族。  
>   
> 　然而，渐渐地，工业不景气开始冲击整个英国，迫使BMC再次进行合并和吸收。 1967年，当Mini车变成Mk2时，BMC将1934年成立的Leyland集团和Rover公司加在一起，组成了一家名为British Leyland的新公司。 最初，该公司被称为BLMC，与BMC很相似。  
>   
>   
> # Mk.2的時代  
>   
> 　1967年10月，Mini系列经历了微小的变化，成为Mk2。 与此同时，Mini Cooper和Mini Cooper S也发展成为Mk2，但这是一个机会，Mk1 Cooper S所用的材料从半超标改为正常。 虽然看不见，但就Cooper S而言，这是Mk1和Mk2的最大区别。 在外部，散热器格栅从Mk1的轻度锥形格栅（被称为小胡子格栅等）改成了稍有棱角的格栅，采用了更大的尾部透镜，后窗区域也被扩大了。  
>   
> 　另一个重要特征是，在发展到Mk2的同时，Morris和Austin的格栅变得完全相同，两者之间唯一的区别是徽章。  
>   
> 　为普通轿车型迷你车推出了1000cc的型号。 (1000cc车型根据发动机分级，850cc为基本型，1000cc为超级豪华型。1000cc车型的生产一直持续到1992年5月，并进行了一系列的小改动)  
>   
> 　内饰也经历了相当大的变化。 座椅与框架不同，被更厚的座椅所取代，方向盘和转向灯也不同。 Mk2的另一个特点是变速器，过去有一个非同步的一档，现在是一个完全同步的4档变速器。  
>   
>   
> # Mini系列，到Mk.3  
>   
> 　随着1969年向Mk3的发展，Mini Cooper消失了，只有Mini Cooper S（1970年的BL Mini Cooper Mk3）上市。 标志也与轿车型迷你汽车共享，因此Cooper的个性较少，Cooper和Cooper S车型过去所具有的魅力和特殊性也非常低。 格式也被改为ADO20。 轴距延长了10mm，汽车变得稍微重了些。 石油冲击和竞争对手汽车日益增长的性能最终导致Cooper系列车型在1971年7月停产。  
>   
> 　Mk3时代的Mini车被俗称为BL Mini车，Austin和Morris品牌也不复存在。  
>   
> 　最有力的说法是，Mini Mk3指的是到1980年为止的车型。 事实上，只有Cooper S被制造商正式命名为Mk3，因此有各种说法认为Mk3时代一直持续到1971年Cooper S生产结束，或直到1975年尾灯从2种颜色变为3种颜色，但制造商从未推出Mk4，所以其区别是模糊的。  
>   
> 　许多车迷大声说，真正的Mini车是在Mk3时代才出现的。  
>   
> 　在日本，Austin型Mini车由Capital Enterprises（后来的Yanase）进口和销售，Maurice型Mini车由Nichibei Jidosha进口，但由于不符合排放规定等原因，Mini车的正式进口在1976年停止。 然而，Mini车的受欢迎程度仍然不减，1981年，在经过约5年的空白期后，Mini1000日英自动车再次由日北治道社进口。 当时的车型是一种名为Mini 1000 HL（High Line）的规格。  
>   
>   
> # Rover Mini  
>   
> 　1982年，公司名称「Austin Rover」再次恢复，这是一个令人怀念的Mini名称。 在日本，「Austin Rover Japan」于1983年5月成立，1985年，Nichibei Jidosha成为日英自动车 ARJ的成员。  
>   
> 　1982年10月，Mini系列中增加了一个 「Mayfair」型号。 Mayfair有一个非常现代的Mini车的外观，用螺丝固定的塑料盖子。 从那时起，Mayfair成为Mini的主要车型。 Mayfair这个名字是伦敦Hyde公园以东的一个高级住宅区的名字，也是伦敦社交场合的用语。  
>   
> 　从1984年开始，轮胎的轮毂尺寸也增加了。 (从10英寸到12英寸。 )，以便为前制动器配备更大直径的盘式制动器。  
>   
> 　事实上，那一年也是Cooper恢复的一年，尽管不是作为目录车型。 这一切都始于John Cooper本人向Rover公司提出复兴Cooper的建议。 然而，当时Rover公司没有能力将该计划付诸实施，它被断然拒绝。 然后，日本的特殊商店「Mini Maruyama」出现在现场。 Mini Maruyama把当时经销商的一辆车，做成了一辆怀旧的Cooper，并把它带给了John Cooper。 John Cooper对这辆车印象深刻，但回答说，对于现代Cooper来说，调教水平太高了，现代Cooper车型必须更加精致。 然而，这个完成度被采纳了，项目被独立进行，John Cooper本人负责发动机，Mini Maruyama负责内饰和外观。 结果是「Mini John Cooper」。
>   
> 　Mini John Cooper在全世界引起了轰动，甚至得到了Rover的称赞。 新的Cooper车型是1000cc车型，功率为62马力，但在1987年，Rover注意到了62马力的设定，决定开发1300cc的Cooper车型。  
>   
>   
> # Mini Cooper复活  
>   
> 　1989年，Rover集团被卖给了英国宇航公司，这是一家航空和其他公司，这两家公司曾被划分为负责乘用车的「Austin Rover Group」和负责四轮驱动车的「Land Rover Group」，并合并为一家名为「Rover Cars」的公司。 自1989年以来，Mini车一直由Rover Cars公司销售。 这导致日本公司「Austin Rover Japan」也改名为「Rover Japan」。  
>   
> 　然后在1990年，Mini Cooper被Rover公司恢复了。 最初作为2000辆的限量版推出（600辆分配给日本），它很快就在全球范围内销售一空，Rover在次年的1991年将Cooper作为目录车型推出。  
>   
> 　今天复活的Rover Mini Cooper1.3仅仅是一款配备了顶级Metro1.3发动机、白色车身顶棚和Mk2型镀铬前格栅的Mayfair。 底盘和悬挂与1000cc版本基本相同，使其成为区别于以前的Mini Cooper S的车型。 换句话说，它是一个只重现Cooper S氛围的车型。 自然，它没有Cooper S的个性，包括全轮驱动汽车所固有的驾驶困难。 这是一款以实用性能为主要考虑的车型。    
>   
> 　1992年，Mini系列改用电子喷射技术，采用计算机化的气体控制，取代了长期使用的SU化油器。  
>   
> 　Cooper 1.3也发展成了Cooper 1.3i，其中i是指喷射的i。 Mini和Mini Cooper的发动机是通过喷射进行电子控制的，比老式的Mini车更容易操作。 毕竟，只需轻点油门就可以启动发动机（这在现代汽车中是可以期待的）。 (以前，驾驶室操作的Mini车在启动发动机时，必须拉动「扼流圈」来调整油门开度）。  
>   
> 　然而，那些认为模拟质量是使Mini车卓越的原因的人，往往对喷射技术望而却步。 肯定有一些顽固的粉丝认为，当你称自己为Cooper时，你必须有两个化油器。    
>   
>   
> # 新型Mini？  
>   
> 　1994年2月，BMW公司收购了Rover集团，BMW公司总经理Bernd Pischetsrieder是Alec Issigonis的远房亲戚，显然是Mini车的铁杆支持者。  
>   
> 　1997年9月8日，新的Mini在法兰克福亮相。 有传言说，新Mini将从2000年底开始销售。 为什么突然在3年前宣布，这仍然是一个谜。  
>   
> 　<s>有一个例子是，同属Rover集团的Land Rover公司在25年后全面重新设计Range Rover时，将老款车型与Range Rover一起出售，称其为「经典车型」，那么，目前的Mini车型是否有可能继续生产？</s>  
>   
> 2000年1月31日，Rover（英国）正式宣布，从2000年9月起停止生产Mini Classic（目前的Mini）。  
>   
>   
> # Mini的Dress Up  
>   
> 　Dress up、剪裁或恢复到旧式被认为是Mini车的时尚。 60年代的Mini车比现在的Mini车的细节更优雅、更漂亮、更别致，大规模生产降低了各种零部件的成本。 以下是Mk1时代的迷你车的特征清单。  
>   
> ## ■**Mk1的特征**  
>   
> ●前格栅：带有圆形模具，不以任何方式干扰引擎盖。 一些非常早期的车型被涂成白色。 在Cooper车型上，Morris由7根粗条组成。 据说这个边条的底端是倒三角形或圆形，这取决于年份。 Austin-Cooper式格栅有11条细边条。  
>   
> ●徽章：Austin的徽章是典型的英国纹章，而Morris的徽章是奶牛图案。 后备箱盖上只有字母。 Austin为手写体，Morris为印刷体。 在Cooper S上，字母S被添加到引擎盖的徽章上。  
>   
> ●尾灯：尾灯有独立的指示灯和刹车。 倒车灯没有集成。  
>   
> ●车门：通过向下旋转车门把手打开。 有些有一个电缆释放系统，从里面开锁是通过拉动电缆完成的。 外门的铰链薄得令人吃惊。  
>   
> ●门把手：仅在驾驶员一侧有门锁的钥匙孔。 乘客一侧只能从内部锁定；从1966年1月的车型开始，把手的末端被弯曲，在门板一侧安装了一个安全凸起，即圆盘。  
>   
> ●Boot handle：形状像船锚（Mk2也是）。  
>   
> ●翼形后视镜：后视镜不是标准设备，而是可选设备。（译注：待校对）  
>   
> ●推拉式侧窗：有几种不同类型的轨道来容纳它们。 塞子配件的形状和孔的位置也因年份不同而略有不同。  
>   
> ●配额车窗：条形铰链，车窗周围有镀铬饰条；从Mk2到Mk3，饰条变得越来越薄了。  
>   
> ●雨刮器的安装位置：更靠近中心。 雨刮器可以从任何一侧安装，以方便出口时的左手驾驶版本。 (到Mk 2为止）  
>   
> ●Remote control housing：最初的Mk1有一个异常长的直接换挡杆，但从1961年开始采用Cooper的遥控装置。  
>   
> ●低档（1档）：非同步。  
>   
> ●轮胎：10英寸车轮轮胎（Cooper配备了适合10英寸车轮的7英寸盘式制动器，与Lockheed合作完成。 而在Cooper S上，7.5英寸盘式制动器，据说是10英寸车轮的极限。 (纳入了冷却机制，甚至制动伺服系统）。  
>   
> ●液压悬架：Mini车的悬架通常是橡胶锥悬架，但从1964年9月开始，一些Mini车的橡胶锥被一种通过特殊的加压液体吸收冲击的悬架所取代。 这有点像人们熟悉的F1赛车的原始主动悬挂。 然而，由于价格和空间问题，从Mk 3开始，它又恢复了橡胶锥。  
>   
> ●油箱：双油箱（25升+25升）最初是作为Cooper S的选配件提供的，但从1965年11月起成为Cooper 1275S的标准装备。  
>   
> ●大灯：Lucas三点式大灯。 (现在每个价格超过80,000日元) 
>   
> ●牌照灯：'60年代正式进口到日本的Mini车安装了托架，提高了牌照灯的基础高度。
>   
> ●内饰：由于没有车门衬里，使得内部空间比目前的Mini车型要宽敞得多。 考虑到出口因素，无论方向盘的位置如何，都会使用中央仪表和中央钥匙。 中央仪表一直延续到Mk3。  
>   
> ●3重中心仪表：正式名称为oval instrument nacelle。 只配备在saloon系列的顶级车型上，但在Cooper系列上作为标准配置。3中心仪表：从左到右：水温、速度和油温表。 (从79年起，Minis上的方形仪表。 最初，它们是没有转速表的2连杆类型，直到80年代末，它们才变成我们今天看到的方形3连杆类型）  
>   
> ●转向：工厂原厂是一个大直径的手柄，直径为40厘米cm。 这种类型一直到Mk3都有。 齿条和小齿轮系统，很重。 像公共汽车一样以一定角度安装。 转向柱上只安装了一个杠杆，用于启动转向灯。 在杠杆的末端是一个与闪烁器相连的灯。  
>   
> ●启动器：非常早期的Mk1车型不是通过转动点火钥匙来启动发动机的，而是通过按下驾驶员和乘客座椅之间从地板上伸出的一个按钮来启动。 因此，钥匙仅仅是一个电流的on/off开关。  
>   
> ●Dipper开关：通过用脚按下加热器下方地板上的一个开关，在大灯的远近光之间进行切换；从Mk2开始，与闪烁器杆共用。  
>   
> ●座椅：Cooper系列的座椅有一种金色或银色的浮雕装饰，称为锦缎。 这些只有蓝色和红色的组合座椅可以使用。 双色的颜色。 没有靠背装置。 没有头枕。  
>   
> ●烟灰缸：位于中央仪表正上方的烟灰缸最多只能容纳5个烟头。  
>   
> ●AT汽车于1964年9月推出。 然而，Cooper系列只提供手动变速器。  
>   
>   
> # 自Mk2以来的主要外观变化  
>   
> ■**Mk2的变化**  
> ●前格栅设计（有角度的外框，而不是以前的圆形，模子的上半部分连接到引擎盖一侧）  
> ●徽章的设计（品牌之间的区别较小，区别仅限于徽章内的颜色和品牌名称的使用）    
> ●尾灯的形状（改为大长方形类型）  
> ●门把手（Mk1时代的外门把手有薄而尖的尖端，可能会伤害到手指）  
> ●加大的后窗  
> ●牌照灯的位置和形状  
> ●内饰（Cooper系列有一个黑色的内饰，而不是以前的双色方案。 它还有一个眨眼器杆，可作为喇叭和拨片的开关）。 
>   
> ■**Mk3的变化**  
> ●前格栅设计（Mk2的方形模具被改道，并采用了条数更多的格栅。 所有车型的设计都是统一的。 基本上，目前的Mini也采用Mk3的格栅。 Cooper系列则类似于Mk2的格栅）  
> ●徽章设计（由于该品牌已不再存在，所有车型的徽章也是一致的。 同样的形状一直使用到1992年的Mini1000）  
> ●门把手（从通过转动来解锁变成了一个按钮系统。 从车里面开门和关门现在是由门的前面来完成）  
> ●侧窗现在是卷起的，而不是滑动的（门的内侧被衬里挡住，以容纳窗户）  
> ●取消外门铰链（改为内门铰链。 (这也促成了门的厚度增加）  
> ●门的形状（铰链一侧的下线，从圆形到角形）  
> ●后备箱盖的形状  
> ●Boot handle（过渡到较厚的手柄）  
> ●牌照灯（安装在牌照上方的平板灯）  
> ●安装了侧标（日本法规要求前翼必须有与闪烁器配合的灯，因此这些灯在1986年左右成为进口车的标准装备）  
> ●尾灯（从1977年起也采用了带倒车灯的三合一类型）  
> ●雨刷安装位置  
>   
> ■**后来对Mini的更改**  
> ●1980年，发动机为「A+」型，细节也得到了改进。  
> ●在'80年代早期，立柱杆的数量增加到2个（闪烁器和雨刷）。 早期的右驾汽车的闪烁器在右侧，左驾汽车的闪烁器在左侧，但由于左驾汽车的产量很大，可能是由于零件的统一，闪烁器杆在后来的某个时候也被放在了右驾汽车（像现在的汽车）的左侧，与左驾汽车版本相同。  
> ●汽油箱从25升增加到34升。  
> ●在12英寸轮毂之后，配备中央仪表的车型消失了，所有车型都配备了偏置的仪表板仪表。  
> ●从'80年代末开始，泊车灯也是用树脂而不是玻璃制成的。 形状也是圆柱形和平面。 镀金的轮辋也消失了。  
> ●1992年的喷射改装。 所有车型改装为1300cc。  
> ●1997年，Mini车在车门内安装了侧撞门梁，以增加车门的强度和驾驶员的安全气囊，使该车符合现代安全标准。  
>   
>   
> ■**Mini Cooper 1.3i目录和数据**  
<TABLE BORDER="2" cellspacing="1" cellpadding="2">
<TR><TD ALIGN=CENTER>总长度</TD><TD ALIGN=CENTER>3075mm</TD></TR>
<TR><TD ALIGN=CENTER>总高度</TD><TD ALIGN=CENTER>1330mm</TD></TR>
<TR><TD ALIGN=CENTER>总宽度</TD><TD ALIGN=CENTER>1440mm</TD></TR>
<TR><TD ALIGN=CENTER>轴距</TD><TD ALIGN=CENTER>2035mm</TD></TR>
<TR><TD ALIGN=CENTER>front tread</TD><TD ALIGN=CENTER>1235mm</TD></TR>
<TR><TD ALIGN=CENTER>rear tread</TD><TD ALIGN=CENTER>1200mm</TD></TR>
<TR><TD ALIGN=CENTER>wheel</TD><TD ALIGN=CENTER>4.5×12inch</TD></TR>
<TR><TD ALIGN=CENTER>车辆重量</TD><TD ALIGN=CENTER>720kg</TD></TR>
<TR><TD>　</TD><TD>　</TD></TR>
<TR><TD ALIGN=CENTER>Engine type</TD><TD ALIGN=CENTER>XN12A</TD></TR>
<TR><TD ALIGN=CENTER>排气量</TD><TD ALIGN=CENTER>1271cc</TD></TR>
<TR><TD ALIGN=CENTER>bore</TD><TD ALIGN=CENTER>70.60mm</TD></TR>
<TR><TD ALIGN=CENTER>stroke</TD><TD ALIGN=CENTER>81.20mm</TD></TR>
<TR><TD ALIGN=CENTER>圧縮比</TD><TD ALIGN=CENTER>10.10</TD></TR>
<TR><TD ALIGN=CENTER>最大输出</TD><TD ALIGN=CENTER>62PS/5700rpm</TD></TR>
<TR><TD ALIGN=CENTER>最大扭矩</TD><TD ALIGN=CENTER>9.60kg-m/3900rpm</TD></TR></TABLE>

>   

---

---

[カクテルXYZへ](http://www.netlaputa.ne.jp/~arcadia/cocktail.html)