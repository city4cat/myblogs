http://www.netlaputa.ne.jp/~arcadia/mini11.html

# Mini Cooper 1.3i目录和数据

> ■**Mini Cooper 1.3i目录和数据**  
<TABLE BORDER="2" cellspacing="1" cellpadding="2">
<TR><TD ALIGN=CENTER>总长度</TD><TD ALIGN=CENTER>3075mm</TD></TR>
<TR><TD ALIGN=CENTER>总高度</TD><TD ALIGN=CENTER>1330mm</TD></TR>
<TR><TD ALIGN=CENTER>总宽度</TD><TD ALIGN=CENTER>1440mm</TD></TR>
<TR><TD ALIGN=CENTER>轴距</TD><TD ALIGN=CENTER>2035mm</TD></TR>
<TR><TD ALIGN=CENTER>front tread</TD><TD ALIGN=CENTER>1235mm</TD></TR>
<TR><TD ALIGN=CENTER>rear tread</TD><TD ALIGN=CENTER>1200mm</TD></TR>
<TR><TD ALIGN=CENTER>wheel</TD><TD ALIGN=CENTER>4.5×12inch</TD></TR>
<TR><TD ALIGN=CENTER>车辆重量</TD><TD ALIGN=CENTER>720kg</TD></TR>
<TR><TD>　</TD><TD>　</TD></TR>
<TR><TD ALIGN=CENTER>Engine type</TD><TD ALIGN=CENTER>XN12A</TD></TR>
<TR><TD ALIGN=CENTER>排气量</TD><TD ALIGN=CENTER>1271cc</TD></TR>
<TR><TD ALIGN=CENTER>bore</TD><TD ALIGN=CENTER>70.60mm</TD></TR>
<TR><TD ALIGN=CENTER>stroke</TD><TD ALIGN=CENTER>81.20mm</TD></TR>
<TR><TD ALIGN=CENTER>圧縮比</TD><TD ALIGN=CENTER>10.10</TD></TR>
<TR><TD ALIGN=CENTER>最大输出</TD><TD ALIGN=CENTER>62PS/5700rpm</TD></TR>
<TR><TD ALIGN=CENTER>最大扭矩</TD><TD ALIGN=CENTER>9.60kg-m/3900rpm</TD></TR></TABLE>


[あばうとミニへ](./mini0.md)

---

---

[カクテルXYZへ](./cocktail.md)