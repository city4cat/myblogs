http://www.netlaputa.ne.jp/~arcadia/mini3.html

[戻る](./mini2.md)

>   
> # Mini Cooper S的成功 ～在传奇的Monte Carlo拉力赛中4连胜～  
>   
> 　Mini Cooper S Mk1一直生产到1967年，当时Mini系列被移交给Mk2，并因其许多赛车运动的成就而闻名。 最著名的例子是它在Monte Carlo拉力赛中的成功。 其压倒性的实力至今仍是传奇。 1964年，被称为 "Mini车手之神 "的Paddy Hopkirk驾驶该车取得了胜利，随后是1965年的T. Makinen，1967年的A. Altonen。 Cooper S在1966年赢得了比赛，尽管它因违反照明规定而被取消了资格（被指责大灯是不能调光的类型）。  
>   
> 　尽管竞争对手包括Lancia Fulvia HF、Cortina Lotus、Ford Falcon和保时捷-911，所有这些都是更强大的汽车，但Cooper S获胜的事实真正证明了Cooper S的制造水平。 当时赢得Monte Carlo拉力赛比现在赢得Paris Dakar拉力赛更有声望。  
>   
> 　在日本，许多人看到一辆普通的Mini车就称它为Mini Cooper，这一方面是由于Mini Cooper因其成功而受到欢迎，另一方面是由于在当时的Mini系列进口车中，Mini Cooper的比例很高。  
>   
> 　与普通的Mini一样，Mini Cooper S从一开始就有两个品牌--Morris和Austin，但只有Mk1采用了不同的格栅设计以及徽章。 当然，它们基本上是同一车型，只是品牌不同。 在性能上没有区别。 英国人是一个以不更换品牌为荣的民族。  
>   
> 　然而，渐渐地，工业不景气开始冲击整个英国，迫使BMC再次进行合并和吸收。 1967年，当Mini车变成Mk2时，BMC将1934年成立的Leyland集团和Rover公司加在一起，组成了一家名为British Leyland的新公司。 最初，该公司被称为BLMC，与BMC很相似。  
>   
>   

[次へ](./mini4.md)

---

---

[至About Mini](./mini0.md)