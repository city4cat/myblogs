http://www.netlaputa.ne.jp/~arcadia/iframe04.html

**→[2005年的信息](./iframe05.md)**  
  
**▼2004年的信息**  
  
■12/24(金)  
　Comic Bunch新年4・5合并特刊 发售  
　「**Angel Heart**」第156話/封面  
　【家族の写真】  
　▽TV动画化速報第一弾＆AH 专用 Calendar  
  
■12/16(木)  
　Comic Bunch新年3号(1月7日号)发售  
　「**Angel Heart**」第155話  
　【告白…！】  
　▽4・5合并特刊号在巻頭Color上有TV动画化的预告大专题 
  
■12/15(水)  
　「**CITY HUNTER** 完全版」  
　VOL.24、VOL.25 由徳間書店发售  
  
■12/03(金)  
　Comic Bunch新年1・2合并特大号 发售  
　「**Angel Heart**」第154話  
　【ジョイの行き先】  
　▽下期(新年3号)是12月16日(木)发售  
  
■11/26(金)  
　Comic Bunch第52号(12月10日号)发售  
　「**Angel Heart**」第153話  
　【親子のように】  
  
■11/19(金)  
　Comic Bunch第51号(12月3日号)  
　「**Angel Heart**」暂停。  
  
■11/15(月)  
　「**CITY HUNTER** 完全版」  
　VOL.22、VOL.23 由徳間書店发售  
  
■11/12(金)  
　Comic Bunch第50号(11月26日号)发售  
　「**Angel Heart**」第152話  
　【ミキの幸せ】  
  
■11/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第13巻 发售  
　▽封面是Comic Bunch第40号封面  
  
■11/05(金)  
　Comic Bunch第49号(11月19日号)发售  
　「**Angel Heart**」第151話  
　【共同生活開始！】  
　▽AH TV动画化決定 
  
■10/29(金)  
　Comic Bunch第48号(11月12日号)发售  
　「**Angel Heart**」第150話  
　【愛しき人の形見】  
  
■10/22(金)  
　Comic Bunch第47号(11月5日号)发售  
　「**Angel Heart**」第149話  
　【思い人はこの街に？】  
  
■10/15(金)  
　「**CITY HUNTER** 完全版」  
　VOL.20、VOL.21 由徳間書店发售  
  
　Comic Bunch第46号(10月29日号)发售  
　「**Angel Heart**」第148話  
　【ジョイの事情】  
  
■10/07(木)  
　Comic Bunch第45号(10月22日号)发售  
　「**Angel Heart**」第147話  
　【"危険"な大女優！】  
  
■10/01(金)  
　Comic Bunch第44号(10月15日号)发售  
　「**Angel Heart**」第146話  
　【優しき店長(マスター)】  
  
■09/24(金)  
　Comic Bunch第43号(10月8日号)  
　「**Angel Heart**」暂停。  
  
■09/16(木)  
　Comic Bunch第42号(10月1日号)发售  
　「**Angel Heart**」第145話  
　【暗闇に見えた夕陽】  
　▽次号、43号暂停  
  
■09/15(水)  
　「**CITY HUNTER** 完全版」  
　VOL.19、別巻插画集X 由徳間書店发售  
  
■09/10(金)  
　Comic Bunch第41号(9月24日号)发售  
　「**Angel Heart**」第144話  
　【いつも一緒】  
  
■09/09(木)  
　★Bunch Comics  
　「**Angel Heart**」第12巻 发售  
　▽封面是Comic Bunch第27号扉絵  
  
■09/03(金)  
　Comic Bunch第40号(9月17日号)发售  
　「**Angel Heart**」第143話/封面  
　【ママへの想い】  
  
■08/27(金)  
　Comic Bunch第39号(9月10日号)发售  
　「**Angel Heart**」第142話  
　【汚れなき心】  
  
■08/19(木)  
　Comic Bunch第38号(9月3日号)发售  
　「**Angel Heart**」第141話  
　【海坊主と座敷童】  
  
■08/14(土)  
　「**CITY HUNTER** 完全版」  
　VOL.17、VOL.18 由徳間書店发售  
  
■08/06(金)  
　Comic Bunch第36・37合并特大号 发售  
　「**Angel Heart**」第140話  
　【穏やかな夢】  
  
■07/30(金)  
　Comic Bunch第35号(8月13日号)发售  
　「**Angel Heart**」第139話  
　【冴子のお返し】  
  
■07/23(金)  
　Comic Bunch第34号(8月6日号)  
　「**Angel Heart**」暂停。  
  
■07/15(木)  
　Comic Bunch第33号(7月30日号)发售  
　「**Angel Heart**」第138話  
　【都会の座敷童】  
　▽下周，34号暂停  
  
■07/15(木)  
　「**CITY HUNTER** 完全版」  
　VOL.15、VOL.16 由徳間書店发售  
　▽Complete BOX全員Service  
　→申请细节在书带上  
  
■07/09(金)  
　Comic Bunch第32号(7月23日号)发售  
　「**Angel Heart**」第137話  
　【冴子と謎の女の子】  
  
■07/02(金)  
　Comic Bunch第31号(7月16日号)发售  
　「**Angel Heart**」第136話  
　【高畑のお守(まも)り】  
  
■06/25(金)  
　Comic Bunch第30号(7月9日号)发售  
　「**Angel Heart**」第135話  
　【生きた証】  
  
■06/18(金)  
　Comic Bunch第29号(7月2日号)发售  
　「**Angel Heart**」第134話  
　【生きて…！】  
  
■06/15(火)  
　「**CITY HUNTER** 完全版」  
　VOL.13、VOL.14 由徳間書店发售  
　▽Complete BOX全員Service  
　→第1弾Start  
  
■06/11(金)  
　Comic Bunch第28号(6月25日号)发售  
　「**Angel Heart**」第133話  
　【心臓移植のリスク】  
  
■06/09(水)  
　★Bunch Comics  
　「**Angel Heart**」第11巻 发售  
　▽封面是Comic Bunch第27号封面  
  
■06/04(金)  
　Comic Bunch第27号(6月18日号)发售  
　「**Angel Heart**」第132話/封面＆巻頭Color  
　【明かされた真実】  
  
■05/28(金)  
　Comic Bunch第26号(6月11日号)  
　「**Angel Heart**」暂停。  
　▽27号是封面＆巻頭Color  
  
■05/21(金)  
　Comic Bunch第25号(6月4日号)发售  
　「**Angel Heart**」第131話  
　【高畑の嘘】  
　▽26号暂停。27号是封面＆巻頭Color  
  
■05/15(土)  
　「**CITY HUNTER** 完全版」  
　VOL.11、VOL.12 由徳間書店发售  
  
■05/14(金)  
　Comic Bunch第24号(5月28日号)发售  
　「**Angel Heart**」第130話  
　【綾音(あーや)の嘘】  
  
■05/06(木)  
　Comic Bunch第23号(5月21日号)发售  
　「**Angel Heart**」第129話  
　【姉(さおり)の想い】  
  
■04/23(金)  
　Comic Bunch第21・22合并特大号 发售  
　「**Angel Heart**」第128話  
　【心臓(こころ)の共振】  
  
■04/16(金)  
　Comic Bunch第20号(4月30日号)发售  
　「**Angel Heart**」第127話  
　【心臓(こころ)の声を信じて】  
  
■04/15(木)  
　「**CITY HUNTER** 完全版」  
　VOL.09、VOL.10 由徳間書店发售  
　▽100万部突破企画  
　4枚組Postcard Present1000名  
  
■04/09(金)  
　Comic Bunch第19号(4月23日号)发售  
　「**Angel Heart**」第126話  
　【白蘭からの手紙】  
  
■04/02(金)  
　Comic Bunch第18号(4月16日号)发售  
　「**Angel Heart**」第125話  
　【受け継がれし命】  
  
■04/01(木)～  
　新宿「MY CITY」6楼的山下書店举行的CH完全版 发售記念企画  
　▽「**CITY HUNTER**」的复制原画公开  
　▽以北条先生亲笔为基础的original paper作为礼物   
　→购买完全版的前300名的想要的人  
  
■03/26(金)  
　Comic Bunch第17号(4月9日号)  
　「**Angel Heart**」暂停。  
  
■03/19(金)  
　Comic Bunch第16号(4月2日号)发售  
　「**Angel Heart**」第124話  
　【生きる糧】  
  
■03/15(月)  
　「**CITY HUNTER** 完全版」  
　VOL.07、VOL.08 由徳間書店发售  
　定价・各980日元  
  
■03/12(金)  
　Comic Bunch第15号(3月26日号)发售  
　「**Angel Heart**」第123話  
　【悲しみの決行日】  
  
■03/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第10巻 发售  
　▽封面是Comic Bunch'03年第52号扉絵  
  
■03/05(金)  
　Comic Bunch第14号(3月19日号)发售  
　「**Angel Heart**」第122話  
　【白蘭の決意】  
  
■02/27(金)  
　Comic Bunch第13号(3月12日号)发售  
　「**Angel Heart**」第121話  
　【初めての愛情】  
  
■02/20(金)  
　Comic Bunch第12号(3月5日号)发售  
　「**Angel Heart**」第120話  
　【神から授かりし子】  
  
■02/14(土)  
　「**CITY HUNTER** 完全版」  
　VOL.05、VOL.06 由徳間書店发售  
　定价・各980日元  
  
■02/13(金)  
　Comic Bunch第11号(2月27日号)发售  
　「**Angel Heart**」第119話/封面  
　【スコープの中の真意】  
  
■02/06(金)  
　Comic Bunch第10号(2月20日号)发售  
　「**Angel Heart**」第118話  
　【白蘭の任務】  
  
■01/30(金)  
　Comic Bunch第9号(2月13日号)  
　「**Angel Heart**」暂停。  
  
■01/23(金)  
　Comic Bunch第8号(2月6日号)发售  
　「**Angel Heart**」第117話  
　【二人の変化】  
  
■01/16(金)  
　Comic Bunch第7号(1月30日号)发售  
　「**Angel Heart**」第116話  
　【運命の再会】  
  
■01/15(木)  
　「**CITY HUNTER** 完全版」  
　VOL.03、VOL.04 由徳間書店发售  
　定价・各980日元  
  
■01/08(木)  
　Comic Bunch新年6号(1月23日号)发售  
　「**Angel Heart**」第115話  
　【聖夜の奇跡】  
  
■01/06(火)  
　Da Vinci  2月号 发售（定价450日元）  
　▽Comics Da Vinci  「北条 司 その尽きない魅力」（译注：「北条司 无尽的魅力」）  
  
■01/05(月)  
　日経Entertainment！2月号 发售（定价500日元）  
　▽【対談】北条司×飯島愛  
　「Cat's Eye」「**City Hunter**」  
　「**Angel Heart**」誕生的秘密故事  
  
**→[2003年的信息](./iframe03.md)**