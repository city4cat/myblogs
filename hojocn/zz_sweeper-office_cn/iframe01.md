http://www.netlaputa.ne.jp/~arcadia/iframe01.html

**→[2002年的信息](./iframe02.md)**  
  
**▼2001年的信息**  
  
■12/19(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.25  
　[飛ぶのが怖い！？編]」发售  
　▽Book Guide on Bunch！第17回 小鷹信光「探偵物語」Series  
  
■12/18(火)  
　Comic Bunch新年3・4合并特大号 发售  
　「**Angel Heart**」第28話  
　【命にかえてでも】  
  
■12/16(日)～2002/01/31(木)  
吉祥寺[BOOKS Ruhe](http://www.books-ruhe.co.jp/)有Coamix原画展  
▽在楼梯平台的空间里  
▽「Angel Heart」等、「蒼天の拳」「251」等  
  
■12/11(火)  
　Comic Bunch新年2号(1月8日号)发售  
　「**Angel Heart**」第27話  
　【訓練生時代の想い出】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■12/08(土)  
　★Bunch Comics  
　「**Angel Heart**」第2巻 发售  
　▽封面是Comic Bunch第20号封面  
  
■12/05(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.24  
　[ふりかえったO・SHI・RI編]」发售  
　▽Book Guide on Bunch！第16回 柴田よしき「For Dear Life」  
  
■12/04(火)  
　Comic Bunch新年1号(1月1日号)发售  
　「**Angel Heart**」第26話  
　【惜しみない命】  
  
■11/27(火)  
　Comic Bunch第29号(12月11日号)发售  
　「**Angel Heart**」第25話  
　【甦った過去】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■11/21(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.23  
　[セーラー服パニック！編]」发售  
　▽Book Guide on Bunch！第15回 Raymond Chandler「Playback」等  
  
■11/20(火)  
　Comic Bunch第28号(12月4日号)  
　「**Angel Heart**」暂停。  
  
■11/13(火)  
　Comic Bunch第27号(11月27日号)发售  
　「**Angel Heart**」第24話  
　【忘れていたもの】  
　▽下回系第29号   
  
■11/07(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.22  
　[ビル街のコールサイン編]」发售  
　▽Book Guide on Bunch！第14回 Lilian J. Brown「猫は殺しをかぎつける」  
  
■11/06(火)  
　Comic Bunch第26号(11月20日号)发售  
　「**Angel Heart**」第23話  
　【戦士たちの絆】  
  
■10/30(火)  
　Comic Bunch第25号(11月13日号)发售  
　「**Angel Heart**」第22話  
　【生き続ける理由】  
  
■10/24(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.21  
　[再び空飛ぶオシリ！編]」发售  
　▽Book Guide on Bunch！第13回 Dashiell Hammett「Malta之鷹」  
  
■10/16(火)  
　Comic Bunch第23・24合并特大号发售  
　「**Angel Heart**」第21話/巻頭Color  
　【戦士の決意】  
　▽附带AH特别宽幅海报   
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■10/10(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.20  
　[哀しい天使編]」发售  
　▽Book Guide on Bunch！第12回 鳴海章「撃つ」  
  
■10/09(火)  
　Comic Bunch第22号(10月23日号)发售  
　「**Angel Heart**」第20話  
　【新宿で宣戦布告！！】  
　▽下周的合并号有opening color和宽幅彩色海报（译注：待校对） 
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
　★Bunch Comics「**Angel Heart**」第1巻发售  
  
■10/02(火)  
　Comic Bunch第21号(10月16日号)发售  
　「**Angel Heart**」第19話  
　【ミステリアスな獠】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■10/01(月)  
　来自[Coamix HP](http://www.coamix.co.jp/)**CH的A3Calendar**开始接受  
　（桌面版本定于10月中旬）  
  
■09/26(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.19  
　[海坊主(ファルコン)にゾッコン！！編]」发售  
　▽Book Guide on Bunch！第11回 Sue Grafton 「alibiのA」（Kinsey Millhone Series）  
  
■09/25(火)  
　Comic Bunch第20号(10月9日号)发售  
　「**Angel Heart**」第18話/封面  
　【グラスハートのときめき】  
　▽「听漫」Drama CD、Comics书带的应征券等Present  
  ▽[Coamix HP](http://www.coamix.co.jp/)＆[北条司公式Homepage](http://www.hojo-tsukasa.com/)、Comics发售情報更新    
  
■09/18(火)  
　Comic Bunch第19号(10月2日号)  
　「**Angel Heart**」暂停。  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■09/12(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.18  
　[やってきた春編]」发售  
　▽Book Guide on Bunch！第10回 乃南アサ「凍える牙」（女刑事・音道貴子Series）  
  
■09/11(火)  
　Comic Bunch第18号(9月25日号)发售  
　「**Angel Heart**」第17話  
　【運命の対面】  
　▽Comicス第1巻→10月9日(火) 发售決定  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■09/04(火)　  
　Comic Bunch第17号(9月18日号)发售  
　「**Angel Heart**」第16話  
　【出生の秘密】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■08/29(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.17  
　[恋人はCityHunter編]」发售  
　▽Book Guide on Bunch！第9回 田中芳樹「薬師寺涼子の怪奇事件簿」Series  
  
■08/28(火)  
　Comic Bunch第16号(9月11日号)发售  
　「**Angel Heart**」第15話  
　【李兄弟との宿運】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
　▽CH「On-demand Personal Calendar」预计将于10月1日开始发售  
  
■08/22(水) 判明情報  
　「On-demand Personal Calendar」  
　来自[Coamix ](http://www.coamix.co.jp/)发售決定  
　▽A3＆桌上（兼Postcard）  
　▽CH等7种图像可供选择  
  
■08/21(火)  
　Comic Bunch第15号(9月4日号)发售  
　「**Angel Heart**」第14話  
　【衝撃のフラッシュバック】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■08/08(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.16  
　[TOKYOデート・スクランブル編] 」发售  
　▽Book Guide on Bunch！第8回 Sara Paretsky 「Summertime Blues」  
  
■08/07(火)  
　Comic Bunch第13・14合并特大号 发售  
　「**Angel Heart**」第13話/巻頭Color  
　【緊迫する新宿】  
　▽AH手机的待机图像在测验中 免费DL（8月20日止）   
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■08/02(木)  
　「**Angel Heart**」总集篇 发售　定价290日元  
　▽收录第1話～第9話  
  
■07/31(火)  
　Comic Bunch第12号(8月14日号)发售  
　「**Angel Heart**」第12話  
　【衝撃を超えた真実】  
　▽下期13+14合并号系AH巻頭Color  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■07/28(土) 22:15～23:09  
　NHK BS2『[週刊週刊BookReview](http://www.nhk.or.jp/book/)』  
　▽关于Coamix的漫画制作文件的mini专题片。  
  ▽有对北条司的Interview。。  
  
■07/25(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.15  
　[越してきた女（ひと）編] 」发售  
　▽Book Guide on Bunch！第7回 南里征典「新宿欲望探偵」  
  
■07/24(火)  
　Comic Bunch第11号(8月7日号)发售  
　「**Angel Heart**」第11話  
　【事実への昂揚】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■07/17(火)  
　Comic Bunch第10号(7月31日号)发售  
　「**Angel Heart**」第10話  
　　【こころとの対話】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
　▽中午「CITY HUNTER」ZIPPO Online网上销售开始  
　→限量200个即日售罄  
  
■07月14日(土) 15時～  
　东京Auction House的Guest talk show有神谷明等Guest。
　▽Auction有「**CityHunter**」冴羽獠的Cell画（Signed）等。  
　▽举办地点：東京Auction House　港区麻布十番1-10-10 Jules A座4F  
　▽詳情见→http://www.auctionhouse.co.jp  
（这是OMO先生的信息。非常感谢）  
  
■07/11(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.14  
　[海坊主からの依頼編] 」发售  
  
■07/10(火)  
　Comic Bunch第9号(7月24日号)发售  
　「**Angel Heart**」第9話  
　【衝撃の邂逅】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
■07/03(火)  
　Comic Bunch第8号(7月17日号)发售  
　「**Angel Heart**」第8話/封面  
　【出会いと言う名の再会】  
　▽[Coamix HP](http://www.coamix.co.jp/)＆[北条司公式HomePage](http://www.hojo-tsukasa.com/)、正午时分Site Renewal  
　▽「CITY HUNTER」ZIPPO、7月17日起[Online销售](http://www.hojo-tsukasa.com/)開始決定！  
　价格8500日元（运费全国一律500日元，不含消费税）  
  
■06/27(水) 　
  Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.13  
　[トラブル・スクープ！編] 」发售  
  
■06/26(火)  
　Comic Bunch第7号(7月10日号)发售  
　「**Angel Heart**」第7話/巻頭Color  
　【掲示板への助走】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
  
　・文艺春秋社发售的『TITLE』8月号（530日元）的Comic介绍Page，Italy空前的Manga·Boom的报道。  
　▽「在日本风靡一时的『City Hunter』」「现在『Family Compo』很受欢迎」  
（mizue提供的信息。 谢谢）  
  
■06/25(月)  
　新潮社发售的**『波』7月号**(100日元)中有「Comic Bunch Interview」。  
　▽刊登根岸忠、原哲夫、北条司的Interview。  
　▽[新潮社HomePage【Ｗｅｂ新潮】](http://www.webshincho.com/)也可以查看。  
　（[マリィさん](mailto:rosemary@badgirl.co.jp)提供的信息。非常感谢）  
  
■06/24(日)  
　在朝日电视台系「Scoop 21」复活的名作漫画的特辑中，推出Coamix如何创刊Comic Bunch的纪录片。由神谷明旁白，北条氏评论。  
  
■06/19(火) 　
  Comic Bunch第6号(7月3日号)发售  
　「**Angel Heart**」第6話  
　【思い出との再会】の巻  
　▽下周，AH将成为巻頭Color！   
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新   
　▽「CITY HUNTER」ZIPPO将于7月开始在网上销售。 最终价格为8,500日元（不包括运费和消费税）  
  
■06/13(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.12  
　[槇村の忘れもの編] 」发售  
  
■06/12(火)  
　Comic Bunch第5号(6月26日号)发售  
　「**Angel Heart**」封面/第5話・巻頭Color  
　【思い出に導かれて】の巻  
  
■06/05(火)  
　Comic Bunch第4号(6月19日号)发售  
　「**Angel Heart**」第4話  
　【新宿で大暴れ！】の巻  
　▽下周，AH封面&卷首彩页！  
　▽可以在6月14日之前在[Coamix HP](http://www.coamix.co.jp/)收听「听漫」！    
  
■05/31(木)  
　朝日新聞的文化・娱乐版关于Comic Bunch等里的Hero复活的話題。  
　「…漫画Hero归来」为题、「**Angel Heart**」中獠说「今后会扮演重要的角色」。   
  
■05/30(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.11  
　[看護婦には手を出すな！編] 」发售  
  
■05/29(火)  
　Comic Bunch創刊3号(6月12日号)发售  
　「**Angel Heart**」封面/第3話  
　【あの人に会いたくて】の巻  
  
■05/26(土)  
　读卖新闻晚间版有一篇介绍Comic Bunch等新漫画杂志的文章。 「『City Hunter』的真的复活」。   
  
■05/22(火)  
　Comic Bunch創刊2号(6月5日号)发售  
　「**Angel Heart**」第2話  
　【絶叫する夢の記憶】の巻  
  
■05/16(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.10  
　[掠奪してきたひとりの花嫁編]」发售  
  
■05/15(火)  
　週刊Comic Bunch創刊！(5月29日号)  
　北条司新連載「**Angel Heart**」第1話  
　【運命に揺れる暗殺者】の巻  
  
※关于早期和其他详细的信息，请参见「[CocktailＸＹＺ](./cocktail.md)」内的「信息来源」。