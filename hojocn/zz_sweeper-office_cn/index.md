source:  
http://www.netlaputa.ne.jp/~arcadia/index.html  
http://www.netlaputa.ne.jp/~arcadia/index2.html  



- [Cocktail-X.Y.Z.](./cocktail.md)  
CH之abc至xyz！  
- [Angel Dust](./dust.md)  
一个画廊  
- [Stop a Hammer !](./tanma.md)  
不包含墙纸和纪念图片  
- [Shooting Range](./shoot/shooting.md)  
Java applet 射击游戏!    
- [Midnight Hunting](http://cgi.netlaputa.ne.jp/%7Earcadia/cgi-bin/hunter/index.html)  
所谓的普通聊天…  
- [Couples Hotel](./chat.md)  
带BGM和爱的Java chat!！？  
- [Wanted-B.B.S.](./bbs.md)  
这叫留言板吗？  
- [Onehole Shots](./link.md)  
这是一个带有BGM的链接页面  
- [Profile](./profile.md)  
嗯…当你看到它时，你会后悔的，嗯？  
- [ICQメッセージパネル](./icq.md)  
- [J.M.C.](./ah/index.md)  
AH之全搜罗！？  
- [AH感想BBS](http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi)  


# Common Data on pages [index](http://www.netlaputa.ne.jp/~arcadia/index.html) and [index2](http://www.netlaputa.ne.jp/~arcadia/index2.html)  

[極楽蜻蜓](http://cgi.netlaputa.ne.jp/~arcadia/cgi-bin/lucky/kuji.cgi)
▲[What's neW?](./what.md)
▼Comic Zenon連載「Angel Heart」的页面
[ＪＭＣ](./ah/index.md)≪
[AH 感想BBS](http://cgi.netlaputa.ne.jp/~arcadia/ah/bbs.cgi)  

+Info. 		----.  
■现在正在Maintenance。  
■10/09(金) （译注：可能为2015年）  
　★Drama化記念 Special Edition  
　「**Angel Heart**」廉价版Comics 发售  
　▽A5尺寸、668Page、价格为800日元。全国便利店有售。  
　・该剧主演上川隆也和北条先生的対談（5P）  
　→故事刊载了1stSeason的第1話至第33話    
→[过去的信息](./iframe13.md)  


# Special Data on page [index](http://www.netlaputa.ne.jp/~arcadia/index.html)  

●新年快乐。今年也请多多关照●
恭賀新年

●'15/10/11 日本TV、「真人版Drama『Angel Heart』」开始播放 →[Drama公式HP](http://www.ntv.co.jp/angelheart/)  
・10月11日(周日)10点Start(第1集扩大30分钟)2集之后每周日10点30分播出  

2015年是CityHunter诞生30周年記念→[『CityHunter』30周年特設Site](http://www.hojo-tsukasa.com/ch30/)  
'15/10/20 CityHunter XYZ edition 第7巻、第8巻→[全书(12卷)有购买优惠](http://www.hojo-tsukasa.com/ch30/)  

'12/07/23 Pachinko「CR CityHunter ～看点是XYZ～」登場→[HEIWA](http://www.heiwanet.co.jp/latest/cr_cityhunter/index.html)  
北条司先生×小室哲哉氏Collaboration企画「Mana」PV公開→[avex](http://avexnet.or.jp/mana/index.html)  
iPhone App「コミコメ『CityHunter』」日／英／法语版发布中→[ComiCom.me](http://comicom.me/)  

「Angel Heart」2ndSeason在月刊Comic Zenon('10/10/25创刊)上开始连载→[公式情報](http://www.comic-zenon.jp/)  
'11/10/25 Comic Zenon官方邮购Site Open→[Comic Zenon Online Store](http://store.comic-zenon.jp/)  

[GREE Game](http://mpf.gree.jp/42)上有「CityHunter 100万人のSweeper」／[北条司Official](http://twitter.com/hojo_official)Twitter  

'09/3月登場[HEIWA](http://www.heiwanet.co.jp/)CR CityHunter／'08/5/16 新宿鮫とのコラボあり AH公式Guide Book  
'06/12/20 AH Vocal Collection Vol.2／'07/2/28 AH DVD Premium BOX Vol.4→[公式情報](http://www.angelheart-dvd.com/)  
'07/12/19(水)CH TV Series単巻DVD（全26巻）Sell and Rent同時Release開始→[公式情報](http://www.cityhunter-dvd.com/)  

'08/5/16～18 AH 1500万部突破記念「Memory of X・Y・Z」→[公式情報](http://www.comicbunch.com/AH1500/)／[Event Report](./ah/AH1500/shinjuku_xyz1.md)  

[CH完全版](http://www.tokuma.jp/)別巻[Z] (完結) ／[CH展(銀座)Repo](./ginza/ginza_ch1.md) ／'15/10/20(火)AH2nd第12巻  
'05/12/9(金)Bunch増刊号AH总集篇 ／12/15(木) 北条司25周年記念 [自选Illustration 100](http://www.tokuma.jp/)  
12/21(水)CH Memorial CD Album[X],[Y],[Z]／12/21(水)CH COMPLETE DVD BOX→[公式情報](http://www.cityhunter-dvd.com/)  

●日本TV・读卖TV上、Anime AH放送 →[Anime 公式HP](http://www.angelheart.tv/)  
・读卖TV ：毎週月曜２４時５８分～ ／第50話は、9月25日(月)25:25～  
・日本TV ：毎週火曜２５時２５分～／第50話は、9月26日(火)25:59～  
第50話[最終回]「Last Present」(スゥチン編)  

●广播节目「HEART OF ANGEL」内Corner「**XYZ 香瑩's Cafe**」  
・FM愛知・FM大阪：每周六２４時～ ／・NACK5：每周日２３時～  
→9月23日(土)／24日(日)最終回   


★每周五 23:00起 [Midnight Hunting](http://cgi.netlaputa.ne.jp/~arcadia/cgi-bin/hunter/index.html) 在例行Chat「古城祭」举办！★  


※要显示此TopPage，Browser需要[Shockwave Flash](http://www-asia.macromedia.com/shockwave/download/download.cgi?Lang=Japanese&INSERT_QUERY_STRING_HERE)的Plug-In。  
CITY HUNTER的著作权 ©北条司先生／集英社・読売TV・Sunrise・Coamix・新潮社  


# Special Data on page [index2](http://www.netlaputa.ne.jp/~arcadia/index2.html)  

![Top](./image/python.jpg)  

★每周五 PM11点起 [Midnight Hunting](http://cgi.netlaputa.ne.jp/~arcadia/cgi-bin/hunter/index.html) (renewal) 在例行Chat中★  

意义不明Contents炸裂中的！(内容)日本最轻的HomePage！（译注：待校对）   
另外，CITY HUNTER的著作权在北条司先生／集英社・読売TV・Sunrise・Coamix・新潮社。  




"xyz@sweeper.office.ne.jp"  
[![ばなー？](./image/sweep01.jpg)](./about.md)	  
since Nov.17,1998　  
by SaebaRyo　  
		
		




