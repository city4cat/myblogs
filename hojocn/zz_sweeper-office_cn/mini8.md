http://www.netlaputa.ne.jp/~arcadia/mini8.html

[戻る](./mini7.md)

>   
> # 新型Mini？  
>   
> 　1994年2月，BMW公司收购了Rover集团，BMW公司总经理Bernd Pischetsrieder是Alec Issigonis的远房亲戚，显然是Mini车的铁杆支持者。  
>   
> 　1997年9月8日，新的Mini在法兰克福亮相。 有传言说，新Mini将从2000年底开始销售。 为什么突然在3年前宣布，这仍然是一个谜。  
>   
> 　<s>有一个例子是，同属Rover集团的Land Rover公司在25年后全面重新设计Range Rover时，将老款车型与Range Rover一起出售，称其为「经典车型」，那么，目前的Mini车型是否有可能继续生产？</s>  
>   
> 2000年1月31日，Rover（英国）正式宣布，从2000年9月起停止生产Mini Classic（目前的Mini）。     

[次へ](./mini9.md)

---

---

[あばうとミニへ](./mini0.md)