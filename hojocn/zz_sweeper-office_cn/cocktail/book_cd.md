http://www.netlaputa.ne.jp/~arcadia/cocktail/book_cd.html


# **CITY HUNTER 关联书籍**

---

> Cassette Book

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">这个故事几乎与原作<A HREF="storyc.html#4">＃４</A>恐怖的Angel Dust 相当。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（
シティーハンター）</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>吻你的搭档…</TD><TD ALIGN="CENTER">4-08-901021-7</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">1989年6月28日<BR>1210日元</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">这个故事几乎与原作<A HREF="storyc.html#30">＃３０</A>悲伤天使 相当。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>悲伤天使</TD><TD ALIGN="CENTER">4-08-901029-2</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">1990年8月29日<BR>1500日元</TD></TR>
</TABLE><BR><BR>



<a name="cd"></a>  
> CDBook  

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">这个故事几乎与原作<A HREF="storyc.html#4">＃４</A>恐怖的Angel Dust 相当。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>吻你的搭档…</TD><TD ALIGN="CENTER">4-08-901047-0</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">1991年10月28日<BR>2000日元</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">这个故事几乎与原作<A HREF="storyc.html#30">＃３０</A>悲伤天使 相当。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>悲伤天使</TD><TD ALIGN="CENTER">4-08-901030-6</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">1990年8月29日<BR>2000日元</TD></TR>
</TABLE><BR><BR><BR>

> 
>   
>   
>   
> ■配音  
>   
> ▽吻你的搭档…  
> 冴羽獠・・・・・・・・神谷明  
> 槇村香・・・・・・・・伊倉一恵  
> 槇村・・・・・・・・・田中秀幸  
> 将軍（General）・・・水島鉄夫  
> 長老（Mayor）・・・・加藤治  
> Club Owner・・・・・中多和宏  
> 流氓・・・・・・・・・梅津秀行  
> PCP患者・・・・・・・梁田清之  
> 爆弾男・・・・・・・・広森信吾  
> 女Ａ・・・・・・・・・林玉緒  
>   
>   
> ▽悲伤天使  
> 冴羽獠・・・・・・・・神谷明  
> 槇村香・・・・・・・・伊倉一恵  
> 海坊主・・・・・・・・玄田哲章  
> 西九条紗羅・・・・・・杉山佳寿子  
> 麻上亜紀子・・・・・・藤田淑子  
> 西九条定光・・・・・・岸野一彦  
> 執事・・・・・・・・・掛川裕彦  
> 部下Ａ・・・・・・・・梁田清之  
> 部下Ｂ・・・・・・・・坂東尚樹  
> 店内Announce・・・・林玉緒  
>   

---

---

**[** [返回](./book.md) **]**
