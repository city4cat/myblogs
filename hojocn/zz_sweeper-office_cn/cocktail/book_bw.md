http://www.netlaputa.ne.jp/~arcadia/cocktail/book_bw.html

**CITY HUNTER 相关书籍*

---

> BUNCH WORLD（バンチワールド）B6尺寸Comic

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">（↑初版应写在p12留言板上的XYZ地的文字漏掉了）</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600"><A NAME="1">CITY HUNTER（シティーハンター）Vol.1</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[ステキな相棒！編]</TD><TD ALIGN="CENTER">4-10-770001-1</TD></TR>
<TR><TD>・栄光なきテンカウント！<BR>
・BMWの悪魔<BR>
・闇からの狙撃者！<BR>
・恐怖のエンジェルダスト<BR>
・ステキな相棒！<BR>
・特製携帯ストラップ・プレゼント応募要項<BR>
・コラム[スペシャルPhotoシリーズ]　渡辺克巳の新宿ストーリー</TD><TD ALIGN="CENTER">2001年1月1日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="2">CITY HUNTER（シティーハンター）Vol.2</A> </FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[危険な家庭教師編]</TD><TD ALIGN="CENTER">4-10-770003-8</TD></TR>
<TR><TD>・将軍の罠！<BR>
・危険な家庭教師<BR>
・特製携帯ストラップ・プレゼント応募要項<BR>
・コラム[スペシャルPhotoシリーズ]　渡辺克巳の新宿ストーリー</TD><TD ALIGN="CENTER">2001年1月15日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="3">CITY HUNTER（シティーハンター）Vol.3</A> </FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[裸足の女優編]</TD><TD ALIGN="CENTER">4-10-770005-4</TD></TR>
<TR><TD>・アンバランスギャング<BR>
・裸足の女優<BR>
・撮影所パニック！<BR>
・ゆれる心<BR>
・亡霊を撃て！<BR>
・特製携帯ストラップ・プレゼント応募要項<BR>
・コラム[スペシャルPhotoシリーズ]　渡辺克巳の新宿ストーリー（完）</TD><TD ALIGN="CENTER">2001年2月1日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="4">CITY HUNTER（シティーハンター）Vol.4 </A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[その女に手を出すな！編]</TD><TD ALIGN="CENTER">4-10-770007-0</TD></TR>
<TR><TD>・その女に手を出すな！<BR>
・ワン・オブ・サウザンド<BR>
・なんたって執念！！<BR>
・恐怖の男センサー<BR>
・危険な国から来た女！</TD><TD ALIGN="CENTER">2001年2月15日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="5">CITY HUNTER（シティーハンター）Vol.5</A> </FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[空とぶオシリ！編]</TD><TD ALIGN="CENTER">4-10-770009-7</TD></TR>
<TR><TD>・危険な国からきた女！－続<BR>
・空飛ぶオシリ！<BR>
・とんでる博士！<BR>
・[新登場特別コラム] 必殺！武器カタログ　コルトパイソン357</TD><TD ALIGN="CENTER">2001年3月1日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="6">CITY HUNTER（シティーハンター）Vol.6</A> </FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[待ちつづける少女編]</TD><TD ALIGN="CENTER">4-10-770011-9</TD></TR>
<TR><TD>・心に咲いたチューリップ<BR>
・待ちつづける少女<BR>
・とんだティーチャー<BR>
・[特別コラム] 必殺！武器カタログ　コルトローマンMK3 ＆ ワン・オブ・サウザンドS&W41マグナム</TD><TD ALIGN="CENTER">2001年3月15日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">↑有Jump Comics未收录剪辑。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600"><A NAME="7">CITY HUNTER（シティーハンター）Vol.7</A> </FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[ギャンブルクイーン！編]</TD><TD ALIGN="CENTER">4-10-770013-5</TD></TR>
<TR><TD>JC未収録カット<BR>
 ・思い出のキズ跡<BR>
 ・鐘とともに運命が！<BR>
 ・ギャンブルクイーン！<BR>
 ・哀愁のギャンブラー<BR>
・[特別コラム] 必殺！武器カタログ　M16 ＆ M60軽機関銃</TD><TD ALIGN="CENTER">2001年4月1日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="8">CITY HUNTER（シティーハンター）Vol.8 </A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[気になるあいつ！編]</TD><TD ALIGN="CENTER">4-10-770017-8</TD></TR>
<TR><TD>・天才の誤算！<FONT COLOR="RED">JC未収録扉</FONT><BR>
 ・気になるあいつ！<BR>
 ・恋は盲目！<BR>
・  [特別コラム] 必殺！武器カタログ　バズーカ（M20A1） ＆ グレネードランチャー（RPG-7）</TD><TD ALIGN="CENTER">2001年4月25日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="9">CITY HUNTER（シティーハンター）Vol.9</A> </FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[天使のほほえみ編]</TD><TD ALIGN="CENTER">4-10-770021-6</TD></TR>
<TR><TD>・天使のほほえみ<BR>
・愛ってなんですか<BR>
・リョウちゃんの恋愛講座<BR>
・お医者様でも草津の湯でも！！<BR>
・初恋<BR>
・思い出の渚<BR>
・[新登場特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第1回 大沢在昌「新宿鮫」シリーズ　大多和伴彦</TD><TD ALIGN="CENTER">2001年5月9日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="10">CITY HUNTER（シティーハンター）Vol.10</A> </FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[掠奪してきたひとりの花嫁編]</TD><TD ALIGN="CENTER">4-10-770025-9</TD></TR>
<TR><TD>・ひと目あなたに！<FONT COLOR="RED">JC未収録扉</FONT><BR>
・掠奪してきたひとりの花嫁<BR>
・アブナイ解毒薬<BR>
・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第2回 馳星周「不夜城」シリーズ　大多和伴彦</TD><TD ALIGN="CENTER">2001年5月30日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="11">CITY HUNTER（シティーハンター）Vol.11 </A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[看護婦には手を出すな！編]</TD><TD ALIGN="CENTER">4-10-770029-1</TD></TR>
<TR><TD>・復讐は完璧に！<BR>
・看護婦には手を出すな！<BR>
・さよなら…そしてコンニチハ！<BR>
・ある夜のマチガイ<BR>
・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第3回 菊地秀行「鬼獣伝」　大多和伴彦</TD><TD ALIGN="CENTER">2001年6月13日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="12">CITY HUNTER（シティーハンター）Vol.12 </A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[槇村の忘れもの編]</TD><TD ALIGN="CENTER">4-10-770033-X</TD></TR>
<TR><TD>・香チャン気をつけて！<BR>
・ブラジャー大作戦<BR>
・槇村の忘れもの<BR>
・リョウ…その受難の日<BR>
・リョウの純情物語<BR>
・美人キャスターの実力<BR>
・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第4回 ミッキー・スピレイン「裁くのは俺だ」　大多和伴彦</TD><TD ALIGN="CENTER">2001年6月27日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="13">CITY HUNTER（シティーハンター）Vol.13</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[トラブル・スクープ！編]</TD><TD ALIGN="CENTER">4-10-770037-2</TD></TR>
<TR><TD>・トラブル・スクープ！<BR>・ニュース速報・礼子の場合<BR>・出逢って恋して占って<BR>・シンデレラの見る夢は…<BR>・暗闇でボッキリ！<BR>・はい、チーズ！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第5回 船戸与一「新宿・夏の死」　大多和伴彦</TD><TD ALIGN="CENTER">2001年7月11日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="14">CITY HUNTER（シティーハンター）Vol.14 </A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[海坊主からの依頼編]</TD><TD ALIGN="CENTER">4-10-770041-0</TD></TR>
<TR><TD>・占いなんて大嫌い！？<BR>・海坊主からの依頼<BR>・スネーク現る！<BR>・ラスト・コンサート<BR>・香ちゃん狙撃さる！？<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第6回 イーヴリン・E・スミス「ミス・メルヴィル」シリーズ　大多和伴彦</TD><TD ALIGN="CENTER">2001年7月25日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="15">CITY HUNTER（シティーハンター）Vol.15</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[越してきた女（ひと）編]</TD><TD ALIGN="CENTER">4-10-770045-3</TD></TR>
<TR><TD>・SAYONARA…<BR>・がんばれ！香ちゃん!!<BR>・越してきた女（ひと）<BR>・動き出した犯人<BR>・ラブラブ大作戦<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第7回 南里征典「新宿欲望探偵」　大多和伴彦</TD><TD ALIGN="CENTER">2001年8月8日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">↑Jump Comics未收录的几个扉页。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600"><A NAME="16">CITY HUNTER（シティーハンター）Vol.16</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[TOKYO（トーキョー）デート・スクランブル編]</TD><TD ALIGN="CENTER">4-10-770049-6</TD></TR>
<TR><TD>・ジェラシー寸前<BR>・ハーレム地獄<BR>・TOKYO（トーキョー）デート・スクランブル<BR>・王女誘拐!?<BR>・告白のエアポート<BR>・越してきたおとなりさん<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第8回 サラ・パレツキー「サマータイム・ブルース」</TD><TD ALIGN="CENTER">2001年8月22日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="17">CITY HUNTER（シティーハンター）Vol.17</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[恋人はシティーハンター編]</TD><TD ALIGN="CENTER">4-10-770053-4</TD></TR>
<TR><TD><BR>・舞子のマイ・ボディーガード<BR>・本能のおもむくままに！？<BR>・守ってあげたい！？<BR>・サクセスは遠く！？<BR>・恋人はシティーハンター<BR>・ドア越しの告白<BR>・秘密のバイト君！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第9回 田中芳樹「薬師寺涼子の怪奇事件簿」シリーズ</TD><TD ALIGN="CENTER">2001年9月12日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="18">CITY HUNTER（シティーハンター）Vol.18</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[やってきた春編]</TD><TD ALIGN="CENTER">4-10-770057-7</TD></TR>
<TR><TD>・懲りない葉子サン！<BR>・暁のMEMORY<BR>・やってきた春<BR>・香ちゃんの初デート<BR>・危険なふたり！<BR>・許してくれ…<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第10回 乃南アサ「凍える牙」（女刑事・音道貴子シリーズ）</TD><TD ALIGN="CENTER">2001年9月26日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="19">CITY HUNTER（シティーハンター）Vol.19</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[海坊主(ファルコン)にゾッコン！！編]</TD><TD ALIGN="CENTER">4-10-770061-5</TD></TR>
<TR><TD>・墓地のナンパニスト<BR>・あねさんは女子大生！<BR>・悲しき未亡人<BR>・花火をあげろ！盛大に！！<BR>・白いキャンバス<BR>・海坊主(ファルコン)にゾッコン！！<BR>・史上最大の作戦！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第11回 スー・グラフトン「アリバイのA」（キンジー・ミルホーン・シリーズ）</TD><TD ALIGN="CENTER">2001年10月10日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="20">CITY HUNTER（シティーハンター）Vol.20</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[哀しい天使編]</TD><TD ALIGN="CENTER">4-10-770065-8</TD></TR>
<TR><TD>・海坊主の愛情<BR>・海坊主が愛した女（ひと）<BR>・哀しい天使<BR>・すねた紗羅ちゃん<BR>・勇気をください！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第12回 鳴海章「撃つ」</TD><TD ALIGN="CENTER">2001年10月24日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="21">CITY HUNTER（シティーハンター）Vol.21</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[再び空飛ぶオシリ！編]</TD><TD ALIGN="CENTER">4-10-770069-0</TD></TR>
<TR><TD>・再び空飛ぶオシリ！<BR>・人質になったもっこり<BR>・泥棒勝負開始！<BR>・鏡の秘密<BR>・憂いのMy Sister(マイシスター)<BR>・姉妹(きょうだい）のきずな<BR>・香ちゃんのトラバーユ！？<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第13回 ダシール・ハメット「マルタの鷹」</TD><TD ALIGN="CENTER">2001年11月7日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="22">CITY HUNTER（シティーハンター）Vol.22</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[ビル街のコールサイン編]</TD><TD ALIGN="CENTER">4-10-770073-9</TD></TR>
<TR><TD>・指輪に秘めた夢<BR>・リョウさん、出番です！！<BR>・さよならの向こう側…<BR>・ビル街のコールサイン<BR>・夢の中の憧れの男(ひと)<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第14回 リリアン・J・ブラウン「猫は殺しをかぎつける」</TD><TD ALIGN="CENTER">2001年11月21日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">↑有Jump Comics未收录的扉页。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600"><A NAME="23">CITY HUNTER（シティーハンター）Vol.23</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[セーラー服パニック！編]</TD><TD ALIGN="CENTER">4-10-770077-1</TD></TR>
<TR><TD>・セーラー服パニック！<BR>・プロポーズ狂想曲(ラプソディ)！？<BR>・嫌い帰れも好きのうち！？<BR>・怒りのマグナム！<BR>・17年目のおばあちゃん<BR>・お嬢さんにパイソンを！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第15回 レイモンド・チャンドラー「プレイバック」他</TD><TD ALIGN="CENTER">2001年12月5日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="24">CITY HUNTER（シティーハンター）Vol.24</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[ふりかえったO・SHI・RI編]</TD><TD ALIGN="CENTER">4-10-770081-X</TD></TR>
<TR><TD>・ホテル街の007<BR>・天下無敵の女探偵誕生！？<BR>・ふりかえったO・SHI・RI<BR>・涙のバースデー<BR>・夜明けの告白<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第16回 柴田よしき「フォー・ディア・ライフ」</TD><TD ALIGN="CENTER">2001年12月19日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="25">CITY HUNTER（シティーハンター）Vol.25</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[飛ぶのが怖い！？]</TD><TD ALIGN="CENTER">4-10-770085-2</TD></TR>
<TR><TD>・女心を知るものは…<BR>・愛は銃よりも強く！<BR>・明日へのリバイバル<BR>・飛ぶのが怖い！？<BR>・飛べないスイーパー<BR>・Fright, Flight（恐怖の飛行）<BR>・エアポート'89<BR>・大空の告白<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第17回 小鷹信光「探偵物語」シリーズ</TD><TD ALIGN="CENTER">2002年1月2日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="26">CITY HUNTER（シティーハンター）Vol.26</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[天使の落としもの！？編]</TD><TD ALIGN="CENTER">4-10-770089-5</TD></TR>
<TR><TD>・天使の落としもの！？<BR>・憧れの後ろ姿<BR>・香、努力する！！<BR>・決闘！蝙蝠（こうもり）v.s.リョウ！！<BR>・背中にさよなら！！<BR>・依頼人は幽霊！？<BR>・ふたつの顔をもつ女！？<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第18回 柘植久慶「傭兵」シリーズ</TD><TD ALIGN="CENTER">2002年1月30日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="27">CITY HUNTER（シティーハンター）Vol.27</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[美女と野獣！？編]</TD><TD ALIGN="CENTER">4-10-770093-3</TD></TR>
<TR><TD>・暗闇でドッキリ・デート<BR>・鏡の中のもうひとりの私！？<BR>・炎の中のピアニスト！！<BR>・Play it again, Mami !－あの曲をもう一度！－<BR>・美女と野獣！？<BR>・港の決闘！！<BR>・涙の街頭募金<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第19回 赤川次郎「セーラー服と機関銃」</TD><TD ALIGN="CENTER">2002年2月13日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="28">CITY HUNTER（シティーハンター）Vol.28</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[突然の出会い！！編]</TD><TD ALIGN="CENTER">4-10-770097-6</TD></TR>
<TR><TD>・突然の出会い！！<BR>・引き裂かれた心<BR>・強く、堅い絆<BR>・マイ・フェア・リョウちゃん！？<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第20回 谷 恒生　警視庁歌舞伎町分室「新宿暴力街」</TD><TD ALIGN="CENTER">2002年2月27日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="29">CITY HUNTER（シティーハンター）Vol.29</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[都会のシンデレラ！！編]</TD><TD ALIGN="CENTER">4-10-770102-6</TD></TR>
<TR><TD>・香が水着に着がえたら！？<BR>・美しき標的！！<BR>・コートの秘密！？<BR>・華麗なる脱出！！<BR>・都会のシンデレラ！！－前編－<BR>・都会のシンデレラ！！－後編－<BR>・殺したい男！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第21回 戸梶圭太「ご近所探偵TOMOE」</TD><TD ALIGN="CENTER">2002年3月13日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="30">CITY HUNTER（シティーハンター）Vol.30</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[祖父、現る！？編]</TD><TD ALIGN="CENTER">4-10-770112-3</TD></TR>
<TR><TD>・暁の決断！！<BR>・過去の傷跡<BR>・賽は投げられた！！<BR>・勝敗の行方！！<BR>・賢者の贈り物！！<BR>・祖父、現る！？<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第22回 森 詠「横浜狼犬（ハウンドドッグ）」</TD><TD ALIGN="CENTER">2002年3月27日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="31">CITY HUNTER（シティーハンター）Vol.31</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[冴子のお見合い！！編]</TD><TD ALIGN="CENTER">4-10-770116-6</TD></TR>
<TR><TD>・狙われたリョウの許嫁！？<BR>・誘拐された<FONT SIZE="-1">じいさん付</FONT>下着！！<BR>・卑劣な罠（トラップ）！？<BR>・ふたりのシティーハンター<BR>・伊集院隼人氏の平穏な一日<BR>・冴子のお見合い！！<BR>・アニキの残像！！<BR>・探りを入れろ！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第23回 高部正樹「傭兵の誇り」</TD><TD ALIGN="CENTER">2002年4月10日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="32">CITY HUNTER（シティーハンター）Vol.32</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[命懸けのパートナー編]</TD><TD ALIGN="CENTER">4-10-770120-4</TD></TR>
<TR><TD>・変えられた台本（シナリオ）！！<BR>・命懸けのパートナー<BR>・失われた過去！！<BR>・死を呼ぶ暗示！！<BR>・思い出を消して…<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第24回 吉村達也「キラー通り殺人事件」</TD><TD ALIGN="CENTER">2002年4月24日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="33">CITY HUNTER（シティーハンター）Vol.33</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[ハートマークの逃がし屋！？編]</TD><TD ALIGN="CENTER">4-10-770124-7</TD></TR>
<TR><TD>・ハートマークの逃がし屋！？<BR>・恐怖の運送！！<BR>・暗号を歌う女！？<BR>・あきれた大逃走！！<BR>・あなたをパートナーに！！<BR>・リョウと恐るべき似た者姉妹！！<BR>・父、来襲す！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第25回 山口雅也「垂里冴子のお見合いと推理」</TD><TD ALIGN="CENTER">2002年5月8日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="34">CITY HUNTER（シティーハンター）Vol.34</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[もっこり十発の陰謀！？編]</TD><TD ALIGN="CENTER">4-10-770128-X</TD></TR>
<TR><TD>・卑怯者！！<BR>・ふたりでひとりの心！！<BR>・もっこり十発の陰謀！？<BR>・消えたもっこり！！<BR>・おかしなふたり！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第26回 霧舎 巧「四月は霧の00（ラブラブ）密室」</TD><TD ALIGN="CENTER">2002年5月29日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="35">CITY HUNTER（シティーハンター）Vol.35</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[写真を巡る思い出！！編]</TD><TD ALIGN="CENTER">4-10-770132-8</TD></TR>
<TR><TD>・本気と本音<BR>・写真を巡る思い出！！<BR>・悲しき旅立ち！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第27回 園 子温「自殺サークル」</TD><TD ALIGN="CENTER">2002年6月12日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="36">CITY HUNTER（シティーハンター）Vol.36</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[涙のペンダント編]</TD><TD ALIGN="CENTER">4-10-770136-0</TD></TR>
<TR><TD>・涙のペンダント<BR>・嵐の前…<BR>・地獄への出航！！<BR>・愛と憎しみと…！！<BR>・ペンダントの記憶！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第28回 森村誠一「砂漠の駅（ステーション）」</TD><TD ALIGN="CENTER">2002年6月26日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="37">CITY HUNTER（シティーハンター）Vol.37</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[ヒーローの怪我！？編]</TD><TD ALIGN="CENTER">4-10-770140-9</TD></TR>
<TR><TD>・息子よ！<BR>・地獄からの生還！！<BR>・ヒーローの怪我！？<BR>・ワンワンスイーパー！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第29回 初野晴「水の時計」</TD><TD ALIGN="CENTER">2002年7月10日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="38">CITY HUNTER（シティーハンター）Vol.38</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[にせC・H登場！！編]</TD><TD ALIGN="CENTER">4-10-770144-1</TD></TR>
<TR><TD>・にせC・H登場！！<BR>・かみあうふたり！！<BR>・素直な気持ちに！！<BR>・[特別コラム] シティーファンに贈るブックガイド・オン・バンチ！…もうひとつの「シティーハンター」第30回（最終回） 北方謙三「風樹の剣」</TD><TD ALIGN="CENTER">2002年7月24日<BR>286円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="39">CITY HUNTER（シティーハンター）Vol.39</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[最終巻 FOREVER, CITY HUNTER!!編]</TD><TD ALIGN="CENTER">4-10-770148-4</TD></TR>
<TR><TD>・ウェディング・ベル！<BR>・FOREVER, CITY HUNTER!!<BR>・特別読切 シティーハンター ―XYZ―<BR>・特別読切 シティーハンター ―ダブル-エッジ―</TD><TD ALIGN="CENTER">2002年8月7日<BR>286円+税</TD></TR>
</TABLE>

<!--
<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="">CITY HUNTER（シティーハンター）Vol.</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>[編]</TD><TD ALIGN="CENTER">4-10-</TD></TR>
<TR><TD>目次</TD><TD ALIGN="CENTER">2002年月日<BR>286円+税</TD></TR>
</TABLE>

<CAPTION ALIGN="LEFT" VALIGN="BOTTOM"><FONT COLOR="RED"></FONT></CAPTION>
-->

---

---

**[** [返回](./book2.md) **]**