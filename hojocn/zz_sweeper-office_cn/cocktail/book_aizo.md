http://www.netlaputa.ne.jp/~arcadia/cocktail/book_aizo.html

**CITY HUNTER 関連書籍**

---

> 愛蔵版Comics

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="perfect">CITY HUNTER（シティーハンター）Perfect Guidebook</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-782038-6</TD></TR>
<TR><TD>・CITY HUNTER ILLUSTRATION GALLERY<BR>・WHAT'S CITY HUNTER?<BR>・HEROINE WHO'S WHO<BR>・香＆獠 ONE AND ONLY<BR>・EPISODE FILE<BR>・北条司 long interview<BR>・北条司 TAKE OUT<BR>・VARIETY SHOOT1-3<BR>・GAG BREAK1-3<BR>・CITY HUNTER 新宿 MAP<BR>・文庫版 Comics版 Episode对应表</TD><TD ALIGN="CENTER">2000年1月25日<BR>781円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">在采访中，他谈到了CITY HUNTER的连载背景，以及在新杂志上的连载「我要画什么，这是最高机密。 我只会说「这不是『城市猎人』！我就这么说吧（笑）」。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600"><A NAME="ill">HOJO TSUKASA 20th ANIVERSARY ILLUSTRATIONS</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>北条司 漫画家20周年記念 イラストレーションズ</TD><TD ALIGN="CENTER">4-08-782598-1</TD></TR>
<TR><TD>・WORK1<BR>COMPLETE<BR>F.Compo<BR><BR>・WORK2<BR>SELECTION<BR>City Hunter , Cat's Eye , & other works<BR>SPLASH! 5<BR><BR>・HOJO TSUKASA interview</TD><TD ALIGN="CENTER">2000年12月25日<BR>2400円+税</TD></TR>
</TABLE>

---

---

**[** [戻る](./book.md) **]**