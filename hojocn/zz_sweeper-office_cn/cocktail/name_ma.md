http://www.netlaputa.ne.jp/~arcadia/cocktail/name_ma.html

CityHunter人名辞典

---

ま行  
|　[ま](./name_ma.md#ma)　|　[み](./name_ma.md#mi)　|　[む](./name_ma.md#mu)　|　[め](./name_ma.md#me)　|　[も](./name_ma.md#mo)　|

<a name="ma"></a>  
> ま  
>   
> まいける-がーらんと【マイケル・ガーラント】  
> →ガーラントの項を参照。  
>   
> まかぜ-えみ【真風笑美】  
> 新宿歌劇団の花形男役スター。プロフェッサー、武藤武明の妹。22歳。  
> →动画[グッド・バイ・マイ・スイート・ハート](./storysp.md#5)（声：岩男潤子）  
>   
> まきの-ようこ【牧野陽子】  
> 鬼英会のカジノで働く、特殊な能力を持ったディーラー。  
> →原作[＃１０](./storyc.md#10)　→动画[第9話](./story1.md#9)（声：川浪葉子）  
>   
> まきはら-こずえ【牧原こずえ】  
> 空想好きな12歳の車椅子の少女。望遠鏡で街を眺める。  
> →原作[＃３３](./storyc.md#33)　→动画[第98、99話](./story2.md#98)（声：山野さと子）  
>   
> まきはら-ゆか【牧原友佳】  
> こずえの姉。リョウの愛人と間違われる。  
> →原作[＃３３](./storyc.md#33)　→动画[第98、99話](./story2.md#98)（声：おざわかおる）  
>   
> まきむら-かおり【槇村香】  
> リョウのパートナー。槇村の妹。  
> →原作[＃３](./storyc.md#3)他　→动画[第4話](./story1.md#4)他（声：伊倉一恵）  
>   
> まきむら-ひでゆき【槇村秀幸】  
> リョウの親友で元刑事。香の兄。  
> →原作[＃１](./storyc.md#1)他　→动画[第1話](./story1.md#1)他（声：田中秀幸）  
>   
> まくがいあ,じぇーむす【ジェームス・マクガイア】  
> ガイナム共和国の次期大統領候補。シークレットサービスの新庄安奈の父。  
> →动画[ザ・シークレット・サービス](./storysp.md#4)（声：大塚明夫）  
>   
> まさ【政】  
> 関西から来た暴力団、権三の子分。  
> →动画[第80話](./story2.md#80)（声：山寺宏一）  
>   
> まさいち【政一】  
> 関東吉祥組の組員のアニキ分。何かあるとすぐに指つめで責任をとろうとする。  
> →原作[＃２８](./storyc.md#28)　→动画[第96、97話](./story2.md#96)（声：戸谷公次）  
>   
> まさき-えいぞう【正木瑛三】  
> 実業家。由加里の義父。子連れの公子と結婚した。  
> →动画[第36話](./story1.md#36)（声：原田一夫）  
>   
> まさき-きみこ【正木公子】  
> 由加里の母。娘を見守るようリョウに依頼を。  
> →动画[第36話](./story1.md#36)（声：花形恵子）  
>   
> まさき-ゆかり【正木由加里】  
> 実業家の正木瑛三の義理の娘。フランス語を学ぶ女子大生。  
> →动画[第36話](./story1.md#36)（声：松井菜桜子）  
>   
> ましば-けんいちろう【真柴憲一郎】  
> 裏の世界では伝説的な闇のガンスミス。由加里の父。  
> →原作[＃３８](./storyc.md#38)  
>   
> ましば-しおり【真柴しおり】  
> 由加里の娘。銃のにおいがあると落ち着く赤ん坊。  
> →原作[＃３８](./storyc.md#38)  
>   
> ましば-ゆかり【真柴由加里】  
> しおりの母。名ガンスミスだった憲一郎の技術を受け継ぐ二代目。  
> →原作[＃３８](./storyc.md#38)  
>   
> ますたー【マスター】  
> 同伴スナックやりたい放題(动画では「FASHION CAFE MELODY」)のマスター。リョウの情報提供者。  
> →原作[＃２](./storyc.md#2)　→动画[第1](./story1.md#1)、[51話](./story1.md#51)（声：青森伸）  
>   
> まちだ【町田】  
> 悪徳宝石商。鬼怒川大造のダイヤを盗む。  
> →动画[第40話](./story1.md#40)（声：田原アルノ）  
>   
> まつい【松井】  
> 大物政治家の天地宗造の第一秘書。  
> →原作[＃６](./storyc.md#6)　→动画[第7話](./story1.md#7)（声：西尾徳）  
>   
> まつおか【松岡】  
> 拓也の父。JPI通信の記者。政府内部の機密文書を反乱軍に渡そうとした疑いでエマリア政府に捕らわれた。  
> →原作[＃１２](./storyc.md#12)　→动画[第12話](./story1.md#12)（声：塚田正昭）  
>   
> まつおか-たくや【松岡拓也】  
> ク－デタ－中のエマリア共和国から家庭教師の川田温子に連れられて逃げて来た少年。  
> →原作[＃１２](./storyc.md#12)　→动画[第12話](./story1.md#12)（声：佐々木望）  
>   
> まっど-どっく【マッド・ドック】  
> メガシティーTVのダグラスの部下。ノベルではマッドドッグ。本名ケン・ベルナルド・バルバロスで、サユリの弟。  
> →动画[凶悪犯 冴羽リョウの最期](./storysp.md#6)（声：森川智之）  
>   
> まつなが【松永】  
> 新興暴力団「集英会」の組長。  
> →ノベル[＃７](./storyn.md#7)  
>   
> まつのうち【松之内】  
> 超一流のブランド品、リッチャーウールの偽物業者。  
> →动画[第60話](./story2.md#60)（声：徳丸完）  
>   
> まつむら-なぎさ【松村渚】  
> 15歳の人気アイドル歌手。熱狂的なファンに狙われていた。  
> →原作[＃１１](./storyc.md#11)　→动画[第14話](./story1.md#14)（声：渕崎ゆり子）  
>   
> まもら【マモラ】  
> パメラ共和国のジメチル将軍が雇ったテロリスト。相棒のラバートと革命指導者の娘、美佐・ウィリアムスを狙う。  
> →动画[第68話](./story2.md#68)（声：桜井敏治）  
>   
> まりぃー【マリィー】  
> 裏の世界ではブラッディー・マリィーと呼ばれる、リョウの元パートナー。  
> →原作[＃３６](./storyc.md#36)　→动画[第112～114話](./story2.md#112)（声：高坂真琴）  
>   
> まりこ【マリ子】  
> 女スリ。香の高校時代の友人。大学で犯罪心理学を学んでいた。  
> →动画[第46話](./story1.md#46)（声：神代智恵）  
>   
> まるきど-しょうぐん【マルキド将軍】  
> フェリダン王国の将軍。アンジェラ王女暗殺を企む。  
> →动画[第82、83話](./story2.md#82)（声：原田一夫）  
>   
> まるちねす,ろーざ【ローザ・マルチネス】  
> ガイナム共和国のマクガイア大統領候補の秘書。マクガイアとは反政府運動時代からの仲間。  
> →动画[ザ・シークレット・サービス](./storysp.md#4)（声：戸田恵子）  
>   
> まるめす-だいじん【マルメス大臣】  
> セリジナ公国の大臣。アルマ王女を暗殺し、セリジナ公国を手中におさめようとした。  
> →原作[＃２４](./storyc.md#24)　→动画[第61、62話](./story2.md#61)（声：石森達幸）  
>   
<a name="mi"></a>  
> み  
>   
> みか【美加】  
> そば屋の白子屋を母から継いだ女主人。  
> →动画[第87話](./story2.md#87)（声：皆口裕子）  
>   
> みき【美樹】  
> 海坊主のパートナー。喫茶「CAT'S EYE」を経営。  
> →原作[＃２９](./storyc.md#29)他　→动画[第90話](./story1.md#90)他（声：小山茉美／オリジナルスペシャル2,3では伊藤美紀）  
>   
> みさ-うぃりあむす【美佐・ウイリアムス】  
> →ウイリアムス,美佐の項を参照。  
>   
> みさき-あや【三崎彩】  
> 保険会社に雇われた女スイーパー。美人で切れ者。[第40話](./story1.md#40)（声：山田栄子）
> 
> みさわ【三沢】  

香港マフィア日本支部のボス、江神の手下。  
→动画[第84、85話](./story2.md#84)（声：山寺宏一）  
  
みた【三田】  
プロボクサー。日本Jr.ミドル級チャンピオン。  
→原作[＃１](./storyc.md#1)　→动画[第3話](./story1.md#3)（声：大塚芳忠）  
  
みっきーいしかわ【ミッキー石川】  
マジシャン。石川マジックの団長。ライバルである岡部ナオミを殺そうとして殺し屋を雇う。  
→动画[第111話](./story2.md#111)（声：納谷六郎）  
  
みっく-えんじぇる【ミック・エンジェル】  
→エンジェルの項を参照。  
  
みながわ【皆川】  
由緒正しきお金持ち。娘の由貴を溺愛する父親。  
→原作[＃１４](./storyc.md#14)　→动画[第26話](./story1.md#26)（声：阪脩）  
  
みながわふじん【皆川夫人】  
由貴の母。  
→原作[＃１４](./storyc.md#14)　→动画[第26話](./story1.md#26)（声：梨羽由記子）  
  
みながわ-ゆき【皆川由貴】  
箱入り娘の18歳のお嬢様。ファーストフード店でアルバイトを。  
→原作[＃１４](./storyc.md#14)　→动画[第26話](./story1.md#26)（声：岡本麻弥）  
  
みなみ-おおやまねこ【南大山猫】  
沖ノ光島の動物。島の地上げ屋一味撃退に協力？する。  
→动画[第58話](./story2.md#58)（声：神代智恵）  
  
みむら【三村】  
元銀行強盗。婦警の倉橋聖子に撃たれた事を逆恨みし、出所後も聖子を狙う。  
→动画[第72話](./story2.md#72)（声：木原正二郎）  
  
<a name="mu"></a>  
む  
  
むじか【ムジカ】  
日本製兵器密輸組織の一味。相棒はジャムカ。  
→动画[第25話](./story1.md#25)（声：島香裕）  
  
むとう-たけあき【武藤武明】  
プロフェッサーと呼ばれるテロリスト。外人部隊出身。真風笑美の義理の兄。  
→动画[グッド・バイ・マイ・スイート・ハート](./storysp.md#5)（声：山寺宏一）  
  
むらこし-さおり【村越さおり】  
絵本作家。3年の期限の約束で家族のもとを離れ上京。  
→动画[第29話](./story1.md#29)（声：高島雅羅）  
  
むーん,ろーずまりぃー【ローズマリィー・ムーン】  
マリィーの表の仕事のモデルの時の名前。→マリィーの項を参照。  
  
<a name="me"></a>  
め  
  
めいよーる【メイヨール（長老）】  
麻薬組織ユニオン・テオーペの長老。海原神。→海原神の項を参照。  
→原作[＃５](./storyc.md#5)(CD／カセットブックの声：加藤治)  
  
めらるど-こくおう【メラルド国王】  
メラルド王国の国王。東京で開催されたメラルド王国秘宝展で白石リカと出会い婚約。  
→动画[第139話](./story91.md#139)（声：堀内賢雄）  
  
<a name="mo"></a>  
も  
  
もさだ【モサダ】  
アンジェラ王女を狙う暗殺団のリーダー。  
→动画[第82、83話](./story2.md#82)（声：山寺宏一）  
  
もーとん-だいとうりょう【モートン大統領】  
ラトアニア共和国の大統領。訪日中に国でクーデターが起き、帰国できなくなる。  
→原作[＃５４](./storyc.md#54)  
  
もはめど【モハメド】  
清水博士の去勢細菌を狙う武器供給業者。  
→动画[第2話](./story1.md#2)（声：亀井三郎）  
  
もらん【モラン】  
フェリダン王国のゲリラ「人民解放機構」のメンバー。代表、ガウスの部下。  
→动画[第83話](./story2.md#83)（声：田口昂）  
  
もりい-かずえ【森居和枝】  
アイドル歌手、松村渚の所属する森居プロダクションの社長。  
→原作[＃１１](./storyc.md#11)　→动画[第14話](./story1.md#14)（声：泉晶子）  
  
もりむら-さくら【森村さくら】  
不良集団「パイレーツ」の一員の16歳。鮎原ひろみの婚約者の妹。  
→动画[第84、85話](./story2.md#84)（声：坂本千夏）  
  
もりむら-たかお【森村孝夫】  
印刷の原版作りの技術者。香港マフィアに偽ドルの原版を作らされる。鮎原ひろみの婚約者で、さくらの兄。  
→动画[第84話](./story2.md#84)（声：関俊彦）  
  
もりわき-みすず【森脇美鈴】  
ルビリア大使館の臨時職員。ルビリアの自然を守るため、募金活動も。  
→动画[第115話](./story3.md#115)（声：林原めぐみ）

---

---

**[** [戻る](./name.md) **]**