source:  
[http://www.netlaputa.ne.jp/~arcadia/cocktail/story.html](http://www.netlaputa.ne.jp/~arcadia/cocktail/story.html)  

ストーリー  

------

# Story

##    CITY HUNTER ?
原作・北条司，1983年的短篇故事，1985年～1991年在周刊少年Jump（集英社）上连载。  
JUMP COMICS（以下简称JC）全35巻、文庫版Comic全18巻。  
有4个动画剧，有3部剧场版，目前在NTV周五路演时段有3部特别节目。  
北条司的其他知名作品包括「CAT'S EYE」(1981年～1984年)、「F.COMPO」(1996年～2000年/连载于集英社ALLMAN）。  


##    Outline
City Hunter 冴羽獠是一个一流的清道夫，他接受了各种各样的工作，从保护美女到杀人。 他的搭档牧村香是他已故好友的妹妹，常常阻止獠对委托人的冲动。 两人表面上斗智斗勇，但在内心深处，他们深深地信任对方，并解决了无数棘手的案件…。(译注：待校对)    


##    Story Digest
<font color=#ff0000>▼CITY HUNTER 完整的故事梗概（剧透警告）</font>   

- [「CITY HUNTER」・原作全55集](./storyc.md)  
- [CITY HUNTER・短篇故事全2集](./storyb.md)  
- [CITY HUNTER・小说共8集](./storyn.md)  
- [动画「CITY HUNTER」全51話](./story1.md)  
- [动画「CITY HUNTER2」全63話](./story2.md)  
- [动画「CITY HUNTER3」全13話](./story3.md)  
- [动画「CITY HUNTER'91」全13話](./story91.md)  
- [动画・CITY HUNTER劇場版・Special 共6部](./storysp.md)  



■注释   
还有一部由成龙改编的电影「城市猟人」（1993年3月）。 电影中的开头和结尾是由北条司绘制的插图。  


[ [Office](../index.md) ]