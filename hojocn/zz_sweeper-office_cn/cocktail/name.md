http://www.netlaputa.ne.jp/~arcadia/cocktail/name.html

CityHunter人名辞典

---

-   CityHunter中登场人物的名字的几乎完整列表。（514项）  
-   对于那些没有透露姓氏的人，只列出他们的名字。  
-   对于非日本人的名字，以姓氏为标准。  

> ●[あ行](./name_a.md)（青猿～オールマン）  
>   
> ●[か行](./name_ka.md)（海音字さゆり～権藤）  
>   
> ●[さ行](./name_sa.md)（佐伯～千寿院）  
>   
> ●[た行](./name_ta.md)（タイガーヘッド～ドラゴン）  
>   
> ●[な行](./name_na.md)（中田万堂～典子）  
>   
> ●[は行](./name_ha.md)（バカロ～ボンダル大使）  
>   
> ●[ま行](./name_ma.md)（真風笑美～森脇美鈴）  
>   
> ●[や行](./name_ya.md)（八九三～予備校生）  
>   
> ●[ら行](./name_ra.md)（雷王～龍雲）  
>   
> ●[わ行](./name_wa.md)（稚姫～王）  
>   

---

---

**[** [Office](../index.md) **]**