http://www.netlaputa.ne.jp/~arcadia/cocktail/name_ha.html


CityHunter人名辞典

---

は行  
|　[は](./name_ha.md#ha)　|　[ひ](./name_ha.md#hi)　|　[ふ](./name_ha.md#hu)　|　[へ](./name_ha.md#he)　|　[ほ](./name_ha.md#ho)　|

<a name="ha"></a>  
> は  
>   
> ばかろ【バカロ】  
> ファウンデ王国の新国王、ナリオの叔父。王位略奪を企み、ナリオを殺そうとする。部下はシュミット。  
> →动画[第110話](./story2.md#110)（声：石森達幸）  
>   
> はぎお-なおゆき【萩尾直行】  
> 天地宗造のひき逃げ事故の罪をかぶった運転手。道子の父。  
> →原作[＃６](./storyc.md#6)　→动画[第7話](./story1.md#7)（声：清川元夢）  
>   
> はぎお-みちこ【萩尾道子】  
> 萩尾の一人娘。父の帰りを待っている。  
> →原作[＃６](./storyc.md#6)　→动画[第7話](./story1.md#7)（声：江森浩子）  
>   
> はたおか【畠岡】  
> 日本アクションスタントの及川優希のマネージャー。原作と动画では顔が違う。  
> →原作[＃４８](./storyc.md#48)　→动画[第130話](./story91.md#130)（声：田原アルノ）  
>   
> ばっふぁろー【バッファロー】  
> 暴走族ブルーオイスターの牛次に付けられた名前。  
> →原作[＃５](./storyc.md#5)　→动画[第10話](./story1.md#10)  
>   
> はーと,えんじぇる【エンジェル・ハート】  
> 金髪の美人スイーパー。コンピューターの「ミスターロバーツ」が相棒。  
> →动画[第25話](./story1.md#25)（声：吉田理保子）  
>   
> はな-ひめ【花姫】  
> 黒熊谷に隠れ住む、平家の血をひくお姫様。18歳。  
> →动画[第20話](./story1.md#20)（声：平野文）  
>   
> はらの-くろいつ【ハラノ・クロイツ】  
> →クロイツの項を参照。  
>   
> はりすん,ろばーと【ロバート・ハリスン】  
> アメリカNo.1のスイーパー。リョウの恩人で親友だった。愛用銃はワルサーP38。  
> →动画[第82、83話](./story2.md#82)、[第126、127話](./story3.md#126)（声：堀秀行）  
>   
> はるひこ【春彦】  
> 片岡優子の通う武蔵文化大学の学生。  
> →原作[＃８](./storyc.md#8)  
>   
> ばろん【バロン（男爵）】  
> ユニオン日本史部の資金源のひとつのカジノ、カフェバー「GREEN GU」のオーナー。  
> →原作[＃５](./storyc.md#5)  
>   
> はんす-じーくふりーと【ハンス・ジークフリート】  
> →ジークフリートの項を参照。  
>   
> はんちょう【班長】  
> 警視庁の特捜班付きで新設された、シークレットサービスの班長。  
> →动画[ザ・シークレット・サービス](./storysp.md#4)（声：KONTA）  
>   
> はんにゃの-かえで【般若のかえで】  
> 服部半蔵13代の正統伊賀流忍者。リョウに挑戦。  
> →动画[第75話](./story2.md#75)（声：丸尾和子）  
>   
<a name="hi"></a>  
> ひ  
>   
> ぴえーる-るるーしゅ【ピエール・ルルーシュ】  
> →ルルーシュの項を参照。  
>   
> ひかげ【日影】  
> 竜神会の一員。ゴンザレスと手を組み麻薬を密輸しようとする。  
> →动画[ザ・シークレット・サービス](./storysp.md#4)（声：市川治）  
>   
> ひぐま【ヒグマ（羆）】  
> ギャラリーHIGUMAの社長。画家、藤野和友の元弟子で、絵画の贋作を造っている。  
> →动画[第57話](./story2.md#57)（声：キートン山田）  
>   
> ひさいし-かおり【久石香】  
> 香が槇村家の養子になる以前の名前。  
> →原作[＃３２](./storyc.md#32)　→动画[第103話](./story2.md#103)  
>   
> ひさいし-じゅんいち【久石純一】  
> 槇村の警官だった父親が追跡中に事故死させた殺人犯。香と立木さゆりの実の父。  
> →原作[＃３２](./storyc.md#32)　→动画[第103話](./story2.md#103)  
>   
> ひとよし-いんちょう【人吉院長】  
> 孤児院「くすのきハウス」の院長。大河内巌に弱味があり土地をだまし取られた。  
> →动画[第88、89話](./story2.md#88)（声：村松康雄）  
>   
> ぴーと【ピート】  
> マクガイア大統領候補の反政府活動時代の同志。ローザが特別な感情を抱いていた。  
> →动画[ザ・シークレット・サービス](./storysp.md#4)  
>   
> ひなこ【日奈子】  
> リョウがビラ配りの最中にナンパしようとした美女。  
> →动画[百万ドルの陰謀](./storysp.md#3)（声：冬馬由美）  
>   
> ひむろ-たけし【氷室剛司】  
> 海坊主の元上官で恩人。真希の父で、ヴァイオリンの名手。  
> →原作[＃２１](./storyc.md#21)　→动画[第27、28話](./story1.md#27)  
>   
> ひむろ-まき【氷室真希】  
> 天才ヴァイオリニスト。氷室剛司の娘。20歳。  
> →原作[＃２１](./storyc.md#21)　→动画[第27、28話](./story1.md#27)（声：島本須美）  
>   
> ひゅうが-としお【日向敏夫】  
> 人気絶頂のアイドル歌手。映画「思い出の渚」の主役に選ばれていた。映画宣伝のため、社長と狂言を仕組む。  
> →原作[＃１５](./storyc.md#15)　→动画[第19話](./story1.md#19)（声：堀川亮）  
>   
> ひらお【平尾】  
> 牧野陽子がカジノのディーラーとして働く鬼英会の若頭。  
> →原作[＃１０](./storyc.md#10)　→动画[第9話](./story1.md#9)（声：屋良有作）  
>   
> ひらた-あきら【平田アキラ】  
> 脱獄囚。親分の暮林虎吉とともに脱獄を。  
> →动画[第87話](./story2.md#87)（声：山寺宏一）  
>   
> ひらやま-きみこ【平山希美子】  
> リョウに海坊主のガードを頼むため、美樹が変装した時の偽名。  
> →原作[＃５１](./storyc.md#51)  
>   
<a name="hu"></a>  
> ふ  
>   
> ふぁるこん【ファルコン】  
> →伊集院隼人の項を参照。  
>   
> ふぃーるど,けにー【ケニー・フィールド】  
> リョウのアメリカにいた頃のパートナー。ソニアの父。No.1の賞金稼ぎだったが、リョウと決闘し死す。  
> →原作[＃４４](./storyc.md#44)　→动画[第135話](./story91.md#135)（声：柴田秀勝）  
>   
> ふぃーるど,そにあ【ソニア・フィールド】  
> リョウの元パートナーのケニーの娘。リョウを父の仇と思い、復讐しようとする。  
> →原作[＃４４](./storyc.md#44)　→动画[第135話](./story91.md#135)（声：松本梨香）  
>   
> ふかまち【深町】  
> 射撃のオリンピック金メダリストの警部。暴力団と癒着していた黒川警視正の仲間。  
> →原作[＃２３](./storyc.md#23)　→动画[第42話](./story1.md#42)（声：山寺宏一）  
>   
> ふじおか-こうじ【藤岡浩司】  
> 26歳のフリーカメラマン。恋人の加納絵里をモデルに写真を撮る。  
> →ノベル[＃７](./storyn.md#7)  
>   
> ふじの-かずとも【藤野和友】  
> 早世の画家。遥の父。「わが娘」を描いた。  
> →动画[第57話](./story2.md#57)  
>   
> ふじの-はるか【藤野遥】  
> 画家で、リョウのデッサンをした。藤野和友の娘。  
> →动画[第57話](./story2.md#57)（声：富沢美智恵）  
>   
> ふしみ【伏見】  
> ミュージカル狂で有名な伏見財閥の当主。次原舞子のお守りに隠されていた、窃盗犯に盗まれたダイヤの持ち主。  
> →原作[＃２５](./storyc.md#25)　→动画[第108話](./story2.md#108)（声：鈴木泰明）  
>   
> ふぶき-きくのすけ【吹雪菊之介】  
> 吹雪団十郎の娘、吹雪菊之。18歳の女任侠。  
> →动画[第37、38話](./story1.md#37)（声：松岡ミユキ）  
>   
> ふぶき-だんじゅうろう【吹雪団十郎】  
> 関東花吹雪組の組長で、最後の任侠といわれる。菊之介の父。  
> →动画[第37、38話](./story1.md#37)（声：渡辺猛）  
>   
> ぶらっくあーみーのぼす【ブラックアーミーのボス】  
> 謎の国際的テロリスト集団、ブラックアーミーのボス。特殊訓練を受けた現地の女性を見せ掛けのボス「セイラ」に仕立て作戦名とするのが常套手段。  
> →动画[第101話](./story2.md#101)（声：池水通洋）  
>   
> ぶらっでぃ-まりぃー【ブラッディ・マリィー】  
> →マリィーの項を参照。  
>   
> ふりつ-らんぐう【不律乱寓】  
> 自称、世界一の超常現象研究家。研究の資金のために美術品の泥棒もやる。  
> →原作[＃１３](./storyc.md#13)　→动画[第11話](./story1.md#11)（声：大木民雄）  
>   
> ぷろふぇっさー【プロフェッサー】  
> →武藤武明の項を参照。  
>   
<a name="he"></a>  
> へ  
>   
> へいわーど【ヘイワード】  
> 海坊主の元パートナー。キャサリンの父。キャサリンをダンディ・ジャックの犯罪組織に誘拐され、背中から銃弾を浴びる。  
> →动画[第66話](./story2.md#66)  
>   
> へいわーど,きゃさりん【キャサリン・ヘイワード】  
> 18歳の金髪美女で、海坊主の元パートナーの娘。  
> →动画[第66、67話](./story2.md#66)（声：鶴ひろみ）  
>   
> べてぃー【ベティー】  
> 高円会の若頭、高宮の女だが、オカマだった。  
> →原作[＃１０](./storyc.md#10)  
>   
> へるぜん,よはん【ヨハン・フリードリッヒ・フォン・ヘルゼン】  
> 東ガリエラ情報部大佐。赤い死神と呼ばれる。ニーナの父。  
> →动画[愛と宿命のマグナム](./storysp.md#1)（声：木原正二郎）  
>   
<a name="ho"></a>  
> ほ  
>   
> ほうじょう【北條】  
> 金持ちのぼっちゃん。結婚を前に愛人整理をし、応じなかった女を殺害。  
> →原作[＃４１](./storyc.md#41)  
>   
> ほしの-ありさ【星野アリサ】  
> ファッションモデル。四井信一の恋人。  
> →动画[第22話](./story1.md#22)（声：高田由美）  
>   
> ほりこし-ちえみ【堀越ちえみ】  
> ファンクラブもある、美人スチュワーデス。香の高校時代の友人。  
> →动画[第16話](./story1.md#16)（声：坂本千夏）  
>   
> ぼんだる-たいし【ボンダル大使】  
> 東ガリエラ駐日大使。本国の指示で、極東における破壊工作の資料を奪回しようとする。部下はギュンター。  
> →动画[愛と宿命のマグナム](./storysp.md#1)（声：加藤正之）  
>   

---

---

**[** [戻る](./name.md) **]**