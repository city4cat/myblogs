http://www.netlaputa.ne.jp/~arcadia/cocktail/book.html

**CITY HUNTER 相关书籍**

---

-   CITY HUNTER的comics和相关书籍的Pick-ups。
-   除非另有说明，否则均由集英社出版。

表格阅读方法    
<TABLE BORDER="1">
<TR><TD><FONT COLOR="#FF6600">Title</FONT></TD><TD ALIGN="CENTER">著者</TD></TR>
<TR><TD>Subtitle</TD><TD ALIGN="CENTER">ISBN</TD></TR>
<TR><TD>目录</TD><TD ALIGN="CENTER">发行年月日<BR>初版价格</TD></TR>
</TABLE>

> ●[JUMP COMICS（ジャンプコミックス）](./book_jc.md)  
>   
> ●[集英社文庫COMIC版](./book_bunko.md)  
>   
> ●[JUMP COMICS DELUXE](./book_dx.md)  
>   
> ●[JUMP j BOOKS](./book_jb.md)  
>   
> ●[Cassette・CDBook](./book_cd.md)  
>   
> ●[JUMP GOLD SELECTION Anime Mook](./book_mook.md)  
>   
> ●[JUMP COMICS SELECTION Anime Comics](./book_anime.md)  
>   
> ●[愛蔵版Comics](./book_aizo.md)  
>   
> ●[SC ALLMAN（SCオールマン）](./book_sc.md)  
>   
> ●[SC ALLMAN愛蔵版（SCオールマン愛蔵版）](./book_scaizo.md)  
>   
> ●[別冊宝島](./book_takara.md)（宝島社） new  
>   

---

---

**[** [Office](../index.md) **]**