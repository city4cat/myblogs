http://www.netlaputa.ne.jp/~arcadia/cocktail/history.html

<A NAME="top"></A>
**CITY HUNTER 年表**

---

- 本节以CityHunter世界的事件为背景，回顾了20世纪的最后一段时期。  
- 故事情节基本以原著为基础。  

  
  

| [1950年代](./history.md#50) | [1960年代](./history.md#60) | [1980年代](./history.md#80) | [1990年代](./history.md#90) | [2000年](./history.md#2000) |

  
  
如何阅读年表  
<TABLE BORDER="1">
<TR>
<TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">年</FONT></TD>
<TD BGCOLOR="DodgerBlue">与CH有关的事件</TD>
</TR>
<TR><TD BGCOLOR="RED">故事中的事件</TD></TR>
<TR><TD>世界上发生的事情</TD></TR>
<TR><TD>流行语，其他</TD></TR>
</TABLE>

译注： 为了便于查看，修改原蓝色背景为浅蓝色；为了便于添加注释，修改了原表格。  

<TABLE BORDER="1">
<TR>
<TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK"><A NAME="50">1955年</A><BR>昭和30年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">（美国COLT公司推出了Python）</TD>
</TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・5月，紫雲丸沉没事件。<BR>・6月，首次发行1日元硬币。</TD></TR>
<TR><TD>・三件神器（电冰箱、电动洗衣机、电视）</TD></TR>
</TABLE>  

<TABLE BORDER="1">
<TR>
<TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1956年<BR>昭和31年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">-</TD>
</TR>
<TR><TD BGCOLOR="RED">・槇村秀幸出生。</TD></TR>
<TR><TD>・7月，气象局成立。<BR>・12月，日苏恢复邦交。</TD></TR>
<TR><TD>・「もはや戦後ではない」　・ドライ、ウェット</TD></TR>
<TR><TD ALIGN="CENTER">～</TD><TD ALIGN="CENTER">　</TD></TR>
</TABLE>  
译注：  

- 「もはや戦後ではない」（译注：「不再是战后」）：  
“1956年《经济白皮书》序言中的一段话。 它作为战后重建结束的象征性宣言，成为一个流行语。 作者是后藤英之助。 当时，日本正处于战后重建时期，日本经济正在稳步恢复到战前水平，部分原因是来自韩国的特殊采购。 '通过恢复实现的增长已经结束。 未来的增长将得到现代化的支持"，序言中的一句话表明，人们对恢复到战前水平的成就感和对未来增长的忧虑交织在一起。”  
出处：[もはや戦後ではない](https://kotobank.jp/word/もはや戦後ではない-159610)<BR>  
- ドライ、ウェット（译注：干、湿）：  
“干（年轻人分裂的思维和行为方式）。”  
出处：[1956年（昭和31年）流行語](https://nendai-ryuukou.com/1950/1956.html)  

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1959年<BR>昭和34年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・3月5日（周四），北条司先生出生于福冈县小仓市（现北九州市）。</TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・8月，英国BMC公司名车「Mini」诞生。<BR>・9月，伊势湾台风。</TD></TR>
<TR><TD>・カミナリ族<BR>
</TD></TR>
<TR><TD ALIGN="CENTER">～</TD><TD ALIGN="CENTER">　</TD></TR>
</TABLE>  
译注：  

- カミナリ族（译注：暴走族）：  
“年轻人驾驶摩托车，有爆炸声”  
出处：[1959年（昭和34年）流行語](https://nendai-ryuukou.com/1950/1959.html)  

<TABLE BORDER="1">
<TR><TD ALIGN="CENTER">　</TD><TD ALIGN="CENTER"><A HREF="#top">转到页面顶部</A></TD></TR>
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK"><A NAME="60">1965年</A><BR>昭和40年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">-</TD></TR>
<TR><TD BGCOLOR="RED">・3月31日(水)，久石香出生。</TD></TR>
<TR><TD>・4月，警视厅成立了机动搜查队。<BR>・6月，日韩基本条约签署。</TD></TR>
<TR><TD>・フィーリング　・11PM
</TD></TR>
<TR><TD ALIGN="CENTER">～</TD><TD ALIGN="CENTER">　</TD></TR>
</TABLE>
译注：  

- フィーリング（译注：Feeling）：  
“用来表示感觉或心情的词语”  
出处：[1965年（昭和40年）流行語](https://nendai-ryuukou.com/1960/1965.html)  
- 11PM：  
可能指[电影《11pm》(1965)](https://www.imdb.com/title/tt1163369/)  

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK"><A NAME="80">1982年</A><BR>昭和57年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">-</TD></TR>
<TR><TD BGCOLOR="RED">・同年3月26日（周五），在她只有16岁的时候，她初次见到獠。</TD></TR>
<TR><TD>・2月，日航飞机羽田海域坠毁事故。<BR>・4月，发行500日元硬币。<BR>・10月，Compact Disc出现。</TD></TR>
<TR><TD>・エアロビクス　・「ウッソー ホントー カワイイー」　・逆噴射<BR>
</TD></TR>
</TABLE>  
译注：  
  
- エアロビクス（译注：Aerobics/有氧运动）  
- 「ウッソー ホントー カワイイー」（译注：“伍索”、“洪托”、“卡哇伊”）：  
“三语族(用“伍索”、“洪托”、“卡哇伊”三个词来表示所有感情的年轻女性”。  
“感叹词。像ホントー、ウッソー这样单词的长音化、促音化现象较多，并逐渐成为女性专用的表达感情的感叹词”。  
出处：[1982年（昭和57年）流行・出来事／年代流行](https://nendai-ryuukou.com/1980/1982.html)、[日语“女性语”研究综述](http://www.xueshut.com/yydili/29398.html)  
- 逆噴射（译注：反向推力）：  
“来自日本航空公司飞机在羽田附近坠毁的原因”  
出处：[1982年（昭和57年）流行語](https://nendai-ryuukou.com/1980/1982.html)  

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1983年<BR>昭和58年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・4月、「CityHunter-XYZ-」的短篇故事发表在JUMP上。<BR>（7月，「Cat's Eye」动画改编第1季开始）</TD></TR>
<TR><TD BGCOLOR="RED">（研制去势细菌的清水博士自杀了）</TD></TR>
<TR><TD>・4月，东京Disneyland开业。<BR>・9月，大韩航空飞机被击落事件。<BR>・10月，三宅岛火山喷发。</TD></TR>
<TR><TD>・おしんのしんは辛抱のしん　・積みきくずし<BR>
</TD></TR>
</TABLE>  
译注：  

- おしんのしんは辛抱のしん  
"おしんのしんは辛抱のしん"是NHK电视剧《阿信》中的台词。意思是：'おしん(阿信)'中的'しん'也是'辛抱'的'しん'。日语‘辛抱’的意思是：忍耐。  
出处：[okwave.jp](https://okwave.jp/qa/q525938.html)  
- "積みきくずし"可能通”積木くずし“，后者是1983年高收视率的电视剧。  
出处：[1983年（昭和58年）高視聴率テレビドラマ](https://nendai-ryuukou.com/1980/1983.html)    

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1984年<BR>昭和59年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・1月、「CityHunter-Double Edge-」的短篇故事在Fresh Jump上发表。<BR>（10月，「Cat's Eye」动画第2期開始）<BR>（10月、「Cat's Eye」本杂志连载结束）</TD></TR>
<TR><TD BGCOLOR="RED">・连环杀人魔「开膛手Jack事件」。</TD></TR>
<TR><TD>・1月，「ロス疑惑」騒動。<BR>・3月，グリコ森永事件。<BR>・7月，Los Angeles Olympics。<BR>

</TD></TR>
<TR><TD>・エリマキトカゲ　・まる金、まるビ<BR>
</TD></TR>
</TABLE>  
译注：  

- ロス疑惑  
出处：[流行語大賞からの現代史回想～1984年](https://timesteps.net/archives/ad1984japan.html)  
- グリコ・森永事件  
出处：[流行語大賞からの現代史回想～1984年](https://timesteps.net/archives/ad1984japan.html)  
- エリマキトカゲ(译注：エリマキlizard)：    
- まる金、まるビ（译注：富裕型、贫穷型）：  
“流行的职业分为两类：富裕型'まる金'和贫穷型'まるビ'”  
出处：[1984年（昭和59年）流行語](https://nendai-ryuukou.com/1980/1984.html)  


<TABLE BORDER="1">
<TR><TD ALIGN="CENTER">　</TD><TD ALIGN="CENTER"><A HREF="#top">转到页面顶部</A></TD></TR>

<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1985年<BR>昭和60年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・2月，「CITY HUNTER」开始在周刊少年Jump上连载。</TD></TR>
<TR><TD BGCOLOR="RED">・2月，日本Jr. Middle级冠军赛三田VS稻垣。<BR>・3月，「新宿無差別女性暴行殺人事件」<BR>・3月26日(周二)，獠和香再次见面。<BR>・3月31日(周日)，香的20岁生日。槇村死于乐生会之手（28歳或29歳）。<BR>・片冈优子，片冈集团的继承人。</TD></TR>
<TR><TD>・3月，「科学万博つくば博'85」举办。<BR>・8月，日航巨型飞机在御巢鹰山坠毁。<BR>・丰田商事事件。</TD></TR>
<TR><TD>・新人類　・「FFされる」（由Focus和Friday拍摄）<BR>
</TD></TR>
</TABLE>  
译注：  

- 新人類：  
“1960年后出生的年轻人”  
出处：[1985年（昭和60年）流行語](https://nendai-ryuukou.com/1980/1985.html)
- 「FFされる」：  
“被拍丑闻照片”  
出处：[1985年（昭和60年）流行語](https://nendai-ryuukou.com/1980/1985.html)

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1986年<BR>昭和61年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">-</TD></TR>
<TR><TD BGCOLOR="RED">・偶像歌手松村渚，专注于穿衣打扮。<BR>・在艾玛利亚共和国，反对独裁政府的政变取得成功。<BR>・怪盗305号引起了公众骚动。<BR>・Saeba公寓右边的「Donald Mag」开业。<BR>・为电影「思い出の渚」的女主角进行公开试镜。<BR>・名取和江成为教授的助理。</TD></TR>
<TR><TD>・1月，Space Shuttle「Challenger」发射失败，发生爆炸。<BR>・2月，Marcos总统在Philippine革命期间逃离了该国。<BR>・4月，苏联切尔诺贝利核电站爆炸。<BR>・5月，英国的查尔斯王子和戴安娜公主访问日本。。<BR>・11月，三原山大噴火。</TD></TR>
<TR><TD>・ぷっつん　・家庭内離婚　・あぶない刑事（10月～）<BR>
</TD></TR>
</TABLE>  
译注：  

- ぷっつん：  
“1.紧绷的线等断裂的声音。2.超出忍耐的极限，失去自制力。 失去自制力；失去做出正确判断的能力。”  
出处：[ぷっつん](https://www.hujiang.com/jpciku/je381b7e381a3e381a4e38293/)  
- 家庭内離婚:  
"一种没有夫妻关系的状态，但在表面上他们是以夫妻的形式存在的。"  
出处：[1986年（昭和61年）流行語](https://nendai-ryuukou.com/1980/1986.html)  
- あぶない刑事:  
"一部以横滨为背景的喜剧悬疑动作片。当年的高收视率电视剧。"  
出处：[1986年（昭和61年）高視聴率テレビドラマ](https://nendai-ryuukou.com/1980/1986.html)   

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1987年<BR>昭和62年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・4月、「CityHunter」动画化。<BR>・11月，动画「CityHunter」的视频化開始。</TD></TR>
<TR><TD BGCOLOR="RED">・JBS TV的报道揭露了政府官员和暴力团伙的勾结。<BR>・氷室真希Vienna留学。<BR>・香、銀狐対決。<BR>・「RN侦探所」开在Saeba公寓的左边。<BR>・塞里迪纳公国的阿尔玛公主访问日本。獠若无其事地表演Michael Jackson's Moonwalk。<BR>・次原舞子，在纽约的舞蹈队。</TD></TR>
<TR><TD>・4月，国铁私有化，成为JR。<BR>・9月，Michael Jackson来日本<BR>・10月，纽约股市最大的股价大跌，黑色星期一。<BR>・11月，大韓航空機事件。</TD></TR>
<TR><TD>・バブル　・ボディコン　・朝シャン　・サラダ記念日<BR>  
</TD></TR>
</TABLE>  
译注：  

- バブル（译注：bubble/泡沫）：  
“人们在预期价格上涨的情况下投资于股票或土地，导致市场价格飙升的现象”  
转自：[1987年（昭和62年）流行語](https://nendai-ryuukou.com/1980/1987.html)  
- ボディコン（译注：Boddikon）：  
- 朝シャン（译注：早晨洗发）：  
“morning shampoo。"晨间洗发"是资生堂电视广告中"晨间洗发水"的缩写，该广告的主角是斉藤由貴。以前不常见的早晨洗发的习俗变得很普遍，特别是在高中女生中，而且这种习俗一直延续到现在，没有收敛。宽敞的洗头台也在这一时期开始增加。”  
转自：[新語部門 表現賞](https://kw-note.com/marketing/1987-shingo-ryukogo-taisho/)  
- サラダ記念日(译注：Salad記念日)：  
“俵万智さん的首部歌集「Salad記念日」成为畅销书，售出280万册。 作为歌集是异乎寻常的热门，可以说对短歌爱好者的获得也做出了贡献。标题中使用的歌曲"'这个味道很好'你说，所以7月6日是沙拉周年纪念日"，成为一个非常有名的短语。”  
转自：[新語部門 表現賞](https://kw-note.com/marketing/1987-shingo-ryukogo-taisho/)  


<TABLE BORDER="1">  
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1988年<BR>昭和63年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・4月，动画「CityHunter2」放映開始。<BR>・7月，「CityHunter」的杰作选LD发行。</TD></TR>
<TR><TD BGCOLOR="RED">・柏木圭子是一名大政治家因贩毒而受审的证人。<BR>・在西荻会的一次纵火袭击中，吉祥組的一个公园被烧成了灰烬。<BR>・美樹的「CAT'S EYE咖啡屋」開業。<BR>・西九条Concern的西九条重信被毒死。（译注：待校对）<BR>・獠、身穿紧身衣的小霞一起训练成为小偷。（译注：待校对）<BR>・成功取出导致牧原小梢瘫痪的子弹的手术。<BR>・香的亲姐姐立木小百合，去了Weekly News的纽约总部。</TD></TR>
<TR><TD>・3月，东京Dome建成。<BR>・6月，Recruit疑惑。<BR>・7月，两伊战争停火。<BR>・7月，渔船「第一富士丸」与潜艇「なだしお」在浦贺水道相撞。<BR>・9月，汉城奥运会。<BR>
</TD></TR>
<TR><TD>・ペレストロイカ　・ハナモク　・おたく族<BR>
</TD></TR>
</TABLE>  
译注：  

- “Recruit疑惑”：  
据说是战后最大的丑闻，涉及一些政治家，对政界和商界产生了重大影响，造成公众对政治的不信任。  
详见：https://leisurego.jp/archives/297073  
- ペレストロイカ（译注：perestroika/苏联政治体制改革）：  
“由当时的苏共总书记戈尔巴乔夫倡导并实施的政治改革运动。在长期一党专政、政府僵化、公众日益不满的背景下，他努力引导国家向民主方向发展，包括裁军、暂停对其他国家的干预以及在信息公开的基础上引入部分市场经济。”  
转自：[新語部門 金賞](https://kw-note.com/marketing/1988-shingo-ryukogo-taisho/)  
- ハナモク（译注：花之星期四的简称）：  
“「ハナキン」（花之星期五的简称）指的是星期五下班后喝酒和玩耍，那时人们有两天的周末，明天是他们的休息日。相比之下，「ハナモク」（花之星期四）是传播这样一种观念：最好利用星期五进行休闲活动，如出国旅游或滑雪，而利用星期四来玩。”  
转自：[新語部門 銀賞](https://kw-note.com/marketing/1988-shingo-ryukogo-taisho/)  
- おたく族（译注：御宅族）：  
“沉迷于动漫、电脑等的年轻人，独来独往，难以形成人际关系。”  
转自：[1988年（昭和63年）流行語](https://nendai-ryuukou.com/1980/1988.html)  

<TABLE BORDER="1">
<TR>
<TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1989年<BR>平成元年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・6月，劇場版「愛与宿命的连发枪」发布。<BR>・8月，动画「CityHunter2」开始视频化<BR>・10月，动画「CityHunter3」放映開始。</TD>
</TR>
<TR><TD BGCOLOR="RED">・模特Rose Mary Moon访问日本。<BR>・1月15日(周日)，麻生霞二十岁。<BR>・神村侦探事务所的酒店重建计划将被曝光。<BR>・塞斯纳飞机撞上了Saeba公寓楼六层。<BR>・真柴由加里和她的孩子到一个在加拿大经营农场的熟人那里。<BR>・9月4日(周一)，若叶艺术大学纵火连环路人被捕。<BR>・伍岛梓，被选为日美非洲调查团成员。<BR>・浦上まゆ子的眼部手术成功。<BR>・獠和香成为圣诞泳装秀上「ERI KITAHARA」的专属模特。</TD></TR>
<TR><TD>・1月，昭和天皇崩御。<BR>・4月，消费税将被引入。（税率3％）<BR>・6月，天安門事件。<BR>・11月，柏林墙倒塌。</TD></TR>
<TR><TD>・24時間戦えますか　・オバタリアン　・セクハラ　・ハナコさん</TD></TR>
</TABLE>  
译注：  

- 24時間戦えますか（译注：你能战斗24小时吗）：  
“来自三共（现在的第一三共保健公司）的营养饮料Regain的电视广告中使用的口头禅 "你能战斗24小时吗？"。片中的演员時任三郎被描绘成一个商人，在世界各地的商业中打拼。在广告中，他唱了一首名为 "勇气的标志"的歌曲，这首歌后来在1989年11月22日以"牛若丸三郎太"的名字作为单曲发行时成为热门”。“上世纪七八十年代，日本人把“像战士一样工作”视为美德，24小时营业模式也逐步在便利店行业深入人心，令整个行业得到了极大发展"。  
转自：[流行語部門 銅賞](https://kw-note.com/marketing/1989-shingo-ryukogo-taisho/)、[日本人引以为傲的“24小时营业”，正被便利店巨头们放弃](https://new.qq.com/rain/a/20191116A027LX00)  
- オバタリアン：  
“「オバタリアン」一词是为了讽刺恼人的中年妇女而创造的，并获得1989年日本新词和流行语大奖。它也是Sunrise公司的第一部纯搞笑的作品。人物原作简单平淡的风格被完整地复制了出来。”  
转自：[オバタリアン](http://sunrise-world.net/titles/pickup_040.php)  
- セクハラ（译注：性骚扰）：  
“"性骚扰"第一次在日本引起关注是在30年前。 1989年，第一个关于性骚扰问题的法庭案件在福冈提出，'性骚扰'在当年的琉球年度词汇奖的新词类别中获得金奖。 这是一个机会，让性骚扰这个词在社会上广泛传播。”  
转自：[特集セクハラ（１）平成がのこした宿題　日本初の“セクハラ”裁判を振り返る](https://www.nhk.or.jp/heart-net/article/140/)  
- ハナコさん：  
"「ハナコさん」是由「Hanako」杂志创造的。它指的是20多岁的女性，她们拥有力所能及的奢侈品，不以事业为导向，没有结婚的愿望。"  
转自：[ハナコさん](https://bubble-go.com/1989/hanako.html)  

<TABLE BORDER="1">
<TR><TD ALIGN="CENTER">　</TD><TD ALIGN="CENTER"><A HREF="#top">转到页面顶部</A></TD></TR>
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK"><A NAME="90">1990年</A><BR>平成2年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・3月，Sun Denshi推出PC引擎软件「CityHunter」。<BR>・4月，动画杰作选「Super Magnum Version」开始发售。<BR>・动画「CityHunter2」的LD化开始。<BR>・8月，「Bay City Wars」、「百万美元的阴谋」剧场版上映。<BR>・11月，动画「CityHunter3」视频化开始。。</TD></TR>
<TR><TD BGCOLOR="RED">・索尼娅菲尔德访问日本。<BR>・3月31日（周六），獠和海坊主在墓地决斗。<BR>・神宫寺道彦前来寻找他的孙子（当时3岁），他在1963年的一次飞机失事中失踪。<BR>・海坊主第一次独自打理「CAT'S EYE」店铺。<BR>・6月，冴子、北尾裕貴警部補有一场包办婚姻。<BR>・阿利那米亚的Yuki Grace，继承王位。。<BR>・南加西亚特工袭击Saeba公寓。<BR>・北野由香的最新作品「警视厅的女豹刑警冴子」超级畅销。（译注：待校对）</TD></TR>
<TR><TD>・2月，黑人运动领袖Nelson Mandela在南非关押28年后获释。<BR>・4月，大阪举办「花之世博会」。<BR>・6月，礼宮文仁亲王和川岛纪子公主成婚。<BR>・10月，东德西德统一。</TD></TR>
<TR><TD>・ティラミス  ・アッシー君　・おやじギャル　・バブル崩壊　・ファジー<BR>
</TD></TR>
</TABLE>  
译注：   

- ティラミス(译注：提拉米苏)。  
- アッシー君：  
“一个只要你打电话给他，他就会来送你回家的人。”  
转自：[1990年（平成2年）流行・出来事／年代流行](https://nendai-ryuukou.com/1990/1990.html)  
- おやじギャル：  
“一个20多岁的女人，"性格强硬，喜欢做她父亲做的事情，而且有男人的精力和生活技能。”  
转自：[1990年（平成2年）流行・出来事／年代流行](https://nendai-ryuukou.com/1990/1990.html)  
- バブル崩壊：  
“泡沫经济破灭，股票暴跌”  
转自：[1990年（平成2年）出来事](https://nendai-ryuukou.com/1990/1990.html)  
- ファジー（译注：Fuzzy)：  
“荣获1990年流行語大奖金奖。Fuzzy是一种算法和理论，通过将复杂系统视为 "模糊的 "来优化其控制。我们大多数人当时的理解是，它是宽松和适当的。我还记得电饭煲上的模糊烹调功能呢！”  
转自：[ファジー](https://bubble-go.com/1990/fazzy.html)  


<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1991年<BR>平成3年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・3月，「北条司Illustrations」发售<BR>・4月，动画「CityHunter'91」开始播出。<BR>・10月，动画「CityHunter」系列的完整LD化开始<BR>・「CityHunter Anime Mook」发售。<BR>・11月，「CITY HUNTER」在Jump杂志上的连载结束。<BR>・12月，动画「CityHunter3」LD BOX发售。</TD></TR>
<TR><TD BGCOLOR="RED">・獠最好的朋友，米克-安吉尔，抵达日本。<BR>・獠和海原神在船上对决。<BR>・国际刑警组织将朝着破坏乐生会的方向发展。<BR>・雀丘大学医院第一外科半数医生因与黑社会勾结事件而被捕。<BR>・ラトアニア共和国军事政变失败。<BR>・海坊主和美纪在奥多摩的一个教堂里结婚。</TD></TR>
<TR><TD>・1月，东京的区号现在是四位数。<BR>・1月，海湾战争爆发。<BR>・3月，新宿新东京都政府大楼启用。<BR>・5月，云仙·普贤岳发生火灾。<BR>・8月，苏联解体。</TD></TR>
<TR><TD>・バツイチ　・多国籍軍　・地球にやさしい　・じゃあ～りませんか<BR>
</TD></TR>
</TABLE>  
译注：  

- バツイチ：  
”离婚人士“  
转自：[1991年（平成3年）流行語](https://nendai-ryuukou.com/1990/1991.html)  
- 多国籍軍(译注：多国部队）  
- 地球にやさしい（译注：对地球友好）：  
“20世纪90年代初，公众开始意识到，全球环境问题和"人类生活与自然的和谐"问题成为未来的一个重要问题。在此背景下，讲谈社以"地球友好"为主题，对这一问题采取了全面的立场。”  
转自：[7/4のhitomi dictionary...地球にやさしい（1991年）](http://blog.fmk.fm/charmy-old/2014/07/74hitomi-dictionary1991.php)  
- じゃあ～りませんか（译注：啊～不是吗？）：  
“吉本興業的喜剧演员查理-哈玛的搞笑词。这句话在《新纪元》中已经使用了一段时间，但在同年播出的三得利酒业的电视广告中使用了「是口袋妖怪，啊~不是吗？」这句话，这句话就成了一个流行语。”  
转自：[1991年の日本新語・流行語大賞](https://kw-note.com/marketing/1991-shingo-ryukogo-taisho/)  

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1992年<BR>平成4年	</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・2月，动画「CityHunter'91」的视频开始发售。<BR>・3月，插图小说「明日ある復讐」在jump novel vol.2发表。<BR>・8月，插图小说「薔薇と拳銃」在jump novel vol.3发表。</TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・6月，在巴西里约热内卢举行的地球峰会。<BR>・7月，巴塞罗那奥林匹克运动会举行。<BR>・10月，国会议员金丸信因佐川献金丑闻而辞职。</TD></TR>
<TR><TD>・ジュリアナ東京　・冬彦さん　・ほめ殺し<BR>
</TD></TR>
</TABLE>  
译注：  

- ジュリアナ東京（译注：Juliana东京）：  
“1991年，在土地和股票价格异常飙升的推动下，泡沫经济一下子破裂了。 与此同时，Juliana东京诞生了。 这家由被称为"海滨"的运河边仓库改造而成的迪斯科舞厅，每天都吸引着穿着短裤和紧身衣的女性，她们在舞池中翩翩起舞，就像一朵预示着泡沫经济结束的花朵。...该俱乐部于1994年8月关闭。被警方指出公共道德受到干扰。1993年秋天，泡沫完全破灭。”  
转自：[平成３年、ジュリアナ東京とバブル崩壊](https://www.sanspo.com/article/20181128-BKE6D3GBFFO3TA34XQYQVKG6GQ/)<BR>  
- 冬彦さん（译注：冬彦桑）：  
“电视剧「ずっとあなたが好きだった」中桂田冬彦(冬彦桑)的角色。”  
转自：[1992 流行語部門](https://kw-note.com/marketing/1992-shingo-ryukogo-taisho/)<BR>  
- ほめ殺し：  
“这个词指的是一边批评、诽谤或骚扰某人，一边貌似赞美他们的做法。原意是指在成长过程中赞美一个人而毁掉他或她，等等，但这个词在今年的政治中被频繁使用。”  
转自：[1992 新語部門](https://kw-note.com/marketing/1992-shingo-ryukogo-taisho/)<BR>  

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1993年<BR>平成5年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・3月，成龙电影「城市猟人」上映。<BR>・4月，JUMP j BOOKS「CITY HUNTER」发售。<BR>・动画「CITY HUNTER '91」LD BOX发售。</TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・5月，J-League Soccer誕生。<BR>・7月，北海道西南冲地震。<BR>・8月，细川联合内阁成立。</TD></TR>
<TR><TD>・規制緩和　・清貧　・コギャル<BR>
</TD></TR>
</TABLE>  
译注：  

- 規制緩和：  
“自20世纪70年代以后，日本政府为摆脱石油危机和财政赤字，削减财政支出，力图通过规制缓和，放宽政府对企业实行的多种限制，为增强企业的开放度，给民间经济主体以更多的自由选择权，使日本企业特别是中小企业在竞争中增强了活力。”  
转自：[关于日本的"规制缓和"](https://www.docin.com/p-1659518871.html)  
- 清貧。  
- コギャル：  
“喜欢健康的玩耍和其他活动的十几岁女孩，同时保持她们的少女气质”  
转自：[1993年（平成5年）流行語](https://nendai-ryuukou.com/1990/1993.html)<BR>  


<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1994年<BR>平成6年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">-<!-- なし--></TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・4月，中华航空飞机在名古屋坠毁起火。<BR>・6月，松本沙林毒气事件。<BR>・9月，关西国际机场启用。<BR>・大米不足。各地都有创纪录的酷暑。</TD></TR>
<TR><TD>・一郎效应　・価格破壊　・Young妈妈　・ゴーマニズム<BR>
</TD></TR>
</TABLE>  
译注：  

- 一郎效应：  
“一郎的成功，让被J联赛打败的整个职业棒球恢复了活力。”  
转自：[1994年（平成6年）流行語](https://nendai-ryuukou.com/1990/1994.html)  
- 価格破壊：  
“「Daiei-松下战争」是Daiei和松下电器工业公司（现在的松下公司）之间关于销售价格的冲突，是价格破坏的象征，导致了一场法庭诉讼，今年已经解决。Daiei创始人中内功さん的政策是 "由Daiei决定以任何价格销售产品，制造商根本不允许抱怨"，这导致了折扣销售，但这遭到了制造商的反对，松下电器产业公司采取了暂停向Daiei发货的措施。这场对峙从1964年持续到1994年，被称为30年战争。"价格破坏 "一词本身并不是在这个时候产生的，而是已经存在了一段时间。 ”  
转自：[1994 トップテン入賞](https://kw-note.com/marketing/1994-shingo-ryukogo-taisho/)  
- Young妈妈：  
“这个词之所以引起关注，是因为拥有年轻、张扬的时尚和棕色头发的母亲数量迅速增加，这与过去的母亲形象不同。”  
转自：[1994 トップテン入賞](https://kw-note.com/marketing/1994-shingo-ryukogo-taisho/)<BR>  
- ゴーマニズム：  
“以「おぼっちゃまくん」等漫画而闻名的小林よし的「ゴーマニズム宣言」已成为一部流行漫画。 ゴーマニズム是由傲慢这个词创造出来的，是一部描述小林よし对社会和政治问题的论点和意见的意识形态漫画。 简称为「ゴー宣」。”  
转自：[1994 審査員特選造語賞](https://kw-note.com/marketing/1994-shingo-ryukogo-taisho/)<BR>  


<TABLE BORDER="1">
<TR><TD ALIGN="CENTER">　</TD><TD ALIGN="CENTER"><A HREF="#top">到页面顶部</A></TD></TR>
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1995年<BR>平成7年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・12月，JUMP j BOOKS「CITY HUNTER SPECIAL」发售。</TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・1月，阪神大地震。<BR>・3月，地铁沙林毒气事件。<BR>・12月，快中子增殖反应堆Monju的钠泄漏事故。</TD></TR>
<TR><TD>・NOMO　・無党派　・lifeline　・Internet<BR>
</TD></TR>
</TABLE>  
<BR>
译注：  

- NOMO：  
“日本职业棒球运动员野茂英雄（Hideo Nomo）与美国职业棒球大联盟的洛杉矶Dodgers队签约，他在该队七年来的第一个区冠军中发挥了重要作用，创下了16次三振出局的记录，并取得了日本大联盟球员历史上第一次全场胜利。截至2016年，日本棒球运动员进入大联盟已不再稀奇，但在这个时代，在日本职业棒球中打球是很平常的事，野毛是31年来第二个这样做的日本大联盟球员。”  
转自[1995 大賞](https://kw-note.com/marketing/1995-shingo-ryukogo-taisho/)  
- 無党派：  
“独立人士青島幸男和横山ノック分别在当年举行的东京都和大阪府的省长选举中获胜。隶属于政党的候选人的失败引起了人们对「無党派」的关注，这些选民（投票的人）要么没有政党支持，要么对政治兴趣不大。吸引这些「無党派」的支持的重要性在选举活动中再次得到认可。”  
转自[1995 大賞](https://kw-note.com/marketing/1995-shingo-ryukogo-taisho/)  
- lifeline：  
“这个词本身翻译为「生命线」或 「救生绳」，但它也意味着对人类生活至关重要的各种意义上的供应路线，如水、电、电话、煤气和其他生活基础设施，以及食品运输路线和分销路线。这些都在阪神淡路大地震中被切断，这些生命线的重要性得到了认可，修复工作将它们放在了首位。从那时起，「lifeline」一词开始被用于更广泛的含义。当7-11用直升机空运盒装午餐，在地震发生后立即在路障中维持食物的生命线时，它也成为一个热门话题。”  
转自[1995 トップテン入賞](https://kw-note.com/marketing/1995-shingo-ryukogo-taisho/)  
- Internet：  
“当年发布的Windows 95因其增强的网络功能和即插即用功能而成为热门话题，即使是初学者也可以连接和使用外围设备，从而将互联网的使用推广到普通大众以及截至那时的互联网核心用户。该奖项的获得者是被称为 "互联网先生 "的庆应义塾大学教授村井淳，他被认为是为日本互联网奠定了技术基础。”  
转自[1995 トップテン入賞](https://kw-note.com/marketing/1995-shingo-ryukogo-taisho/)  


<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1996年<BR>平成8年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・1月，电视特别节目「The Secret Service」播出。<BR>・1月，Anime Comics「The Secret Service前后篇」发布。<BR>・2月，「The Secret Service」视频和LD发行。<BR>・6月，集英社文库Comic版「CITY HUNTER」开始发售。<BR>・10月，Computer game「The Secret Service」发售。</TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・7月，Atlanta Olympics举办。<BR>・致病性大肠杆菌O-157肆虐。<BR>・12月，日本驻秘鲁大使馆被武装游击队占领。</TD></TR>
<TR><TD>・チョベリバ　・ルーズソックス　・メークドラマ　・プリクラ<BR>
</TD></TR>
</TABLE>  
译注：  

- チョベリバ：  
“チョベリバ是「超ベリー・バッド」(超very bad)的缩写，意思是「最糟糕的！」。 它在追求时尚的高中女生中开始流行，她们穿着迷你裙、宽松的袜子和棕色的头发，被称为コギャル（译注：该词见1993年流行语），但在几年内，它就成了死词之一。”  
转自：[1996 トップテン入賞](https://kw-note.com/marketing/1996-shingo-ryukogo-taisho/)  
- ルーズソックス（译注：Loose socks）：  
“厚厚的白色高筒袜，在腿部做得很宽松，松弛地穿在腿上。这个名字来源于宽松的袜子，意思是 "马虎 "或 "邋遢"，因为它们在没有被拉长的情况下穿起来显得很邋遢。它们在高中女生中非常流行，因为它们与校服（好看）兼容，而且后来成为一种标准。”  
转自：[1996 トップテン入賞](https://kw-note.com/marketing/1996-shingo-ryukogo-taisho/)  
- メークドラマ（译注：MAKE DRAMA）：  
“这个词是由时任读卖巨人职业棒球队经理的長嶋茂雄创造的，在1995年，即五角大楼比赛的前一年开始使用，意思是巨人队（现在处于低迷状态）将大举复出，赢得冠军。第二年，即1996年，该队在冠军赛中获得第三名，但在第二年，他们在落后排名第一的广岛东洋鲤鱼多达11.5场的情况下，实现了大逆转，赢得了冠军，实现了MAKE DRAMA。”  
转自：[1996 大賞](https://kw-note.com/marketing/1996-shingo-ryukogo-taisho/)  
- プリクラ（译注：Purikura，即"大头照"）：  
“プリント倶楽部的缩写”。“第一台purinto kurabu（プリント倶楽部）机器于1995年7月在日本首次亮相，由位于东京的游戏软件公司Atlus开发。它们一开始是相当基本的机器，只是简单地拍摄你的照片，并在其周围放置一个时尚的框架，如鲜花等。没过多久，其他游戏公司如世嘉也开始开发机器，它们被统称为purikura。尽管它们在商场和游乐中心非常受欢迎，但它们真正起飞是在1997年，当时它们与令人难以置信的流行日本乐队SMAP一起出现在电视上。虽然从技术上讲，purikura的流行在90年代末达到顶峰，但自其诞生以来，它们一直是青年文化中无处不在的一部分。”。“从1997年到1999年成为一个大热，主要是在初中和高中女生中，增加了一些有趣的元素，如可以在笔板上自由书写，可以在框架上添加花卉和其他图案，以及可以与机器上记录的名人一起拍照。 据说繁荣的原因是可以轻松实现团结和亲近感”。  
转自：[1996年（平成8年）流行語](https://nendai-ryuukou.com/1990/1996.html)、[All About Purikura!](https://allabout-japan.com/en/article/6943/)、[プリント倶楽部](https://kotobank.jp/word/プリント倶楽部-178439)  

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1997年<BR>平成9年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・3月，插图小说「囮」于jump novel vol.12发表。。<BR>・4月，电视特别节目「Good Bye My Sweet Heart」放映。<BR>・4月，JUMP j BOOKS「CITY HUNTER 2」发售。<BR>・6月，「Good Bye My Sweet Heart」LD发售。<BR>・7月，「Good Bye My Sweet Heart」视频发售<BR>・8月，Anime Comic「Good Bye My Sweet Heart」发售。</TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・2月，日本神户的学童被杀。<BR>・4月，5%的消费税。<BR>・7月，香港回归中国。<BR>・8月，Diana王妃在巴黎死于车祸。</TD></TR>
<TR><TD>・失楽園　・たまごっち　・パパラッチ　・マイブーム<BR>
</TD></TR>
</TABLE>  
译注：  

- 失楽園：  
“1995年至1996年在《日本经济新闻》上连载的一部浪漫小说，1997年被拍成电影，然后被拍成电视剧。”  
转自：[1997 大賞](https://kw-note.com/marketing/1997-shingo-ryukogo-taisho/)  
- たまごっち：  
“万代公司于1996年11月23日发布的一款蛋形手机游戏，用户在屏幕上给'玉兔'角色喂食，清理它的粪便等，并在与它交流的同时养育它。 在养育玉兔的过程中，它们的外观发生了变化，变成了被称为「おやじっち」的角色，等等。たまごっち这个名字是由'Tamago'（塔玛戈）和'Watch'（手表）组合而成的，英文版本写的是'Tamagotchi'。这股热潮一旦被点燃，就变得非常受欢迎，可以说是一种社会现象，一大早就在玩具店和其他有产品到货信息的商店排起了大队。”  
转自：[1997 トップテン入賞](https://kw-note.com/marketing/1997-shingo-ryukogo-taisho/)  
- パパラッチ（译注：狗仔队）：  
“用来描述跟踪名人的人，如演员、运动员和名人，拍摄私人照片并出售给杂志。 他们通常独立行动，不隶属于某个媒体或杂志。这个名字来源于意大利电影《甜蜜生活》中一位新闻摄影师的名字，并取自意大利方言中的"蚊子"之意。这个名字直到现在才在日本为人所知。在法国巴黎，戴安娜王妃和她的情人多迪-阿尔费德被狗仔队追赶，在高速上迎面撞上了隧道的中间地带，多迪死亡，戴安娜重伤（后来死亡），但在场的狗仔队没有提供帮助就拍下了事件。 由于没有提供急救就继续拍照，许多狗仔队在国际上受到了严厉的批评，这一事件导致了这一说法广为人知。”  
转自：[1997 トップテン入賞](https://kw-note.com/marketing/1997-shingo-ryukogo-taisho/)  
- マイブーム（译注：My Boom）：  
“指只在自己的头脑中流行（Boom）的东西，而不考虑世界上普遍的繁荣，或者是一种生活方式，即试图拥有自己的个人繁荣。”  
转自：[1997 トップテン入賞](https://kw-note.com/marketing/1997-shingo-ryukogo-taisho/)


<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1998年<BR>平成10年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・9月，CityHunter和Cat's Eyed 角色作为奖品出现在game center。</TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・1月，美国总统婚外恋涉嫌销毁报告。<BR>・6月，在法国世界杯足球赛，日本第一次出场<BR>・7月，和歌山的咖喱中掺有砷，4人死亡。<BR>・8月，美国大使馆爆炸。</TD></TR>
<TR><TD>・だっちゅーの　・環境ホルモン　・ハマの大魔人　・キレる<BR>
</TD></TR>  
</TABLE>  
译注：  

- だっちゅーの：  
“这是由喜剧二人组Pirates的一个插曲，她们用手臂夹住自己的乳房以强调自己的乳沟，并说了一句「だっちゅーの」。这个搞笑节目在富士电视台的「ボキャブラ天国」节目中表演，逐渐成为人们谈论的话题，并在综艺节目中大获成功。”  
转自：[1998 大賞](https://kw-note.com/marketing/1998-shingo-ryukogo-taisho/)  
- 環境ホルモン（译注：环境hormone）：  
“进入21世纪，环境问题被提出来，二恶英、杀虫剂、DDT和多氯联苯等化学物质显然会对人体和动物生态系统产生严重影响。这些物质被称为内分泌干扰物或内分泌干扰化学品，为了使这个极其难懂的名字更容易被大众理解，日本广播公司（NHK）和Yasusen Iguchi先生发明了一个普通的名字 "环境hormone"。此后，这一名称经常出现在大众媒体上。”  
转自：[1998 トップテン入賞](https://kw-note.com/marketing/1998-shingo-ryukogo-taisho/)  
- ハマの大魔人：  
“横滨湾明星队投手佐佐木正弘的昵称，他作为一名拦网手发挥了重要作用。他在那一年赢得了许多奖项，包括连续第四年的最佳救援投手奖、日本救球得分记录、最佳投手、MVP和Shoriki Matsutaro奖，以及为球队38年来首次获得中央联赛冠军做出了巨大贡献。他的名字来自于他与日本电影特效时代剧系列中的守护神相像。”  
转自：[ハマの大魔神 - 1998 大賞](https://kw-note.com/marketing/1998-shingo-ryukogo-taisho/)  
- キレる：  
“愤怒突然爆发，不能抑制自己的情绪”  
转自：[1998年（平成10年）流行語](https://nendai-ryuukou.com/1990/1998.html)  

<TABLE BORDER="1">
<TR><TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK">1999年<BR>平成11年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・3月，GW电视台播放的电视特辑中的部分小说在Jump Novel Vol.15中发表。<BR>・4月，JUMP j BOOKS「CITY HUNTER SPECIAL 2」发售。<BR>・4月，电视特别节目「緊急生中継！？凶悪犯冴羽リョウの最期」放映。<BR>・5月，「緊急生中継！？凶悪犯冴羽リョウの最期」LD＆视频发售。<BR>・5月，「Good Bye My Sweet Heart」视频低价再发售。</TD></TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・7月，全日空飞机被劫持，机长被刺死。<BR>・9月，台湾大地震<BR>・9月，东海村发生的首次临界事故。。<BR>・神奈川县警察局发生了一系列丑闻。<BR>・11月，新宿的回忆横町发生火灾。</TD></TR>
<TR><TD>・カリスマ　・だんご三兄弟　・Y2K　・ミレニアム<BR>

</TD></TR>
</TABLE>  
译注：  

- カリスマ（译注：charisma）：  
“涩谷已经成为一个时尚中心，据说涩谷109号是GAL时尚的圣地，有Egoist、Kokoruru、Mijane和Jassie等流行品牌店。 在那里工作的店员成为时尚领袖，对年轻女性产生了巨大影响，她们被称为 "charisma店员"。这些 "charisma店员 "所穿的衣服卖得很火，她们对销售产生了重大影响。后来，"charisma"一词被用来描述对各种行业有影响的人，如 "charisma理发师 "和 "charisma主持人"，以及店员等。”  
转自：[1999 トップテン入賞](https://kw-note.com/marketing/1999-shingo-ryukogo-taisho/)<BR>  
- だんご三兄弟：  
“这首歌于1999年1月作为NHK教育电视节目「おかあさんといっしょ」的 "月度歌曲 "发行，因其爽快的探戈式旋律和流行歌词而在儿童中流行。这首だんご三兄弟由当时的 "歌唱家 "早见健太郎和 "歌唱家 "重森步美演唱。这张期待已久的CD于3月3日发行，并成为爆炸性的热门歌曲，在发行的第三天出货量就超过了250万张，赢得了众多奖项，包括1999年Oricon年度单曲榜的第一名，第41届日本唱片奖的特别奖和日本金唱片奖。”  
转自：[1999 トップテン入賞](https://kw-note.com/marketing/1999-shingo-ryukogo-taisho/)<BR>  
- Y2K：  
“许多老式计算机为了节省空间而使用年份的最后两位数字，当到达2000年时，这可能会与1900年相混淆，造成故障。 这也被称为 "Y2K问题 "或千年虫。”  
转自：[西暦２０００年問題 - 1999 トップテン入賞](https://kw-note.com/marketing/1999-shingo-ryukogo-taisho/)<BR>  
- ミレニアム（译注：millennium，千禧年）：  
“世界各地将举行千年倒计时”  
转自：[1999年（平成11年）出来事](https://nendai-ryuukou.com/1990/1999.html)<BR>  


<TABLE BORDER="1">
<TR>
<TD ALIGN="CENTER">　</TD><TD ALIGN="CENTER"><A HREF="#top">到页面顶部</A></TD>
</TR>
<TR>
<TD ROWSPAN="4" ALIGN="CENTER" BGCOLOR="YELLOW"><FONT COLOR="BLACK"><A NAME="2000">2000年</A><BR>平成12年</FONT></TD>
<TD BGCOLOR="DodgerBlue" WIDTH="1000">・1月，集英社文庫comics版「北条司 短編集1CityHunter -XYZ-」等发售。<BR>・1月，「CityHunter Perfect Guidebook」发售。</TD>
</TR>
<TR><TD BGCOLOR="RED">-</TD></TR>
<TR><TD>・1月，失踪女孩在9年后被找到。<BR>・2月，每400年一次的闰年。<BR>・3月，日比谷线脱轨事件。<BR>・3月，有珠山喷发。<BR>・5月，劫持公共汽车事件。</TD></TR>
<TR><TD>・プレステ2　・ひきこもり<BR>
</TD></TR>
</TABLE>
译注：  

- プレステ2：PlayStation2  
- ひきこもり(译注：Hikikomori，Social Withdrawal，家里蹲)：  
“"戒断"或"社会戒断"不是一种疾病或诊断。 它是一个术语，用来描述年轻人在停止上学或找不到工作后，仍然被限制在家中数年的情况。”  
转自：[ひきこもり](https://kotobank.jp/word/ひきこもり-186722)<BR>


---

---

**[** [Office](http://www.netlaputa.ne.jp/~arcadia/index.html) **]**