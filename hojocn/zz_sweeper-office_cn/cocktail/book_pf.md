http://www.netlaputa.ne.jp/~arcadia/cocktail/book_pf.html

**CITY HUNTER 関連書籍**

---

> CUTY HUNTER 《COMPLETE EDITION》（CUTY HUNTER完全版）  
>   
> ●所有卷的封面插图都是新绘制的  
> ●杂志连载时的彩页全部再现  
> ●以前漫画版本中未收录的页面被重新收录  
> （Size：A5尺寸）

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="1">CITY HUNTER（シティーハンター）VOLUME:01</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780213-7</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第1話</FONT> 栄光なきテンカウント！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> BMWの悪魔<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第3話</FONT> 大きなヘマ！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第4話</FONT> 手を汚すのは おれだ！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第5話</FONT> 闇からの狙撃者！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第6話</FONT> 恐怖のエンジェルダスト<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第7話</FONT> 死のブラックリスト<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第8話</FONT> ステキな相棒！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第9話</FONT> 反撃の微笑！<FONT SIZE="-2">の巻</FONT><BR>
☆初代担当編集者が語る CITY HUNTER誕生秘話
</TD><TD ALIGN="CENTER">2004年1月20日<BR>933円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="2">CITY HUNTER（シティーハンター）VOLUME:02</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香）</TD><TD ALIGN="CENTER">4-19-780214-5</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第10話</FONT> ブタにはダイヤ！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第11話</FONT> 将軍の罠！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第12話</FONT> 死の烙印！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第13話</FONT> 不死身の悪魔！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第14話</FONT> 悪党には なにもやるな！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第15話</FONT> 危険な家庭教師<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第16話</FONT> 憎い あいつ！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第17話</FONT> 小悪魔の悪あがき！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第18話</FONT> アンバランスギャング<FONT SIZE="-2">の巻</FONT><BR>
☆井上雄彦が語る「師匠・北条 司から学んだこと」
</TD><TD ALIGN="CENTER">2004年1月20日<BR>933円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="3">CITY HUNTER（シティーハンター）VOLUME:03</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／海坊主）</TD><TD ALIGN="CENTER">4-19-780219-6</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第19話</FONT> 待ちつづける少女<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第20話</FONT> 殺し屋ダブルキャスト<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第21話</FONT> そして少女は消えた<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第22話</FONT> 裸足の女優<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第23話</FONT> 撮影所パニック<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第24話</FONT> 炎の女優<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第25話</FONT> 勝手なやつら<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第26話</FONT> 本気でロケーション<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第27話</FONT> 揺れる心<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第28話</FONT> 亡霊を撃て！<FONT SIZE="-2">の巻</FONT><BR>
☆シティーハンター７つの謎
</TD><TD ALIGN="CENTER">2004年2月20日<BR>933円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="4">CITY HUNTER（シティーハンター）VOLUME:04</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／冴子）</TD><TD ALIGN="CENTER">4-19-780220-X</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第29話</FONT> とんだティーチャー<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第30話</FONT> いいだせなくて！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第31話</FONT> ネバーエスケープ！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第32話</FONT> 思い出のキズ跡<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第33話</FONT> 過去の真実<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第34話</FONT> 笑顔の報酬<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第35話</FONT> 鐘とともに運命が！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第36話</FONT> その女に手を出すな！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第37話</FONT> ワン・オブ・サウザンド<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第38話</FONT> 下半身にはかなわない！<FONT SIZE="-2">の巻</FONT><BR>
☆「北条 司 100問100答 Part1」
</TD><TD ALIGN="CENTER">2004年2月20日<BR>933円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="5">CITY HUNTER（シティーハンター）VOLUME:05</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780224-2</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第39話</FONT> ハーレムの危険な野郎<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第40話</FONT> なんたって執念！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第41話</FONT> くさ～～い秘密兵器！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第42話</FONT> 恐怖の男センサー<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第43話</FONT> 二発の執念！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第44話</FONT> ギャンブルクイーン！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第45話</FONT> 哀愁のギャンブラー<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第46話</FONT> ギャンブラーバンパイア<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第47話</FONT> 危険な勝利！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第48話</FONT> 天才の誤算！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「冴羽 リョウのLethal Weapons」
</TD><TD ALIGN="CENTER">2004年3月20日<BR>933円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="6">CITY HUNTER（シティーハンター）VOLUME:06</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香）</TD><TD ALIGN="CENTER">4-19-780225-0</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第49話</FONT> 恋のアフターサービス<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第50話</FONT> アイドルは走る！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第51話</FONT> 気になるあいつ！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第52話</FONT> 謎のミニスカート！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第53話</FONT> 最悪最低のできごと！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第54話</FONT> 恋は盲目！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第55話</FONT> 結婚宣言大パニック！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第56話</FONT> 危険な国からきた女！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第57話</FONT> かわいい家庭教師！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第58話</FONT> 恋のフライング<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
☆「北条 司 100問100答 もっと過激に Part 2」
</TD><TD ALIGN="CENTER">2004年3月20日<BR>933円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="7">CITY HUNTER（シティーハンター）VOLUME:07</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780230-7</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第59話</FONT> エマリアの掟！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第60話</FONT> 甘い裏切り！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第61話</FONT> 愛と苦しみの果て？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第62話</FONT> 空とぶオシリ！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第63話</FONT> とんでる博士！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第64話</FONT> マジカルトリック！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第65話</FONT> とてもとってもご苦労さん！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第66話</FONT> サンチョスの反乱！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第67話</FONT> 心に咲いたチューリップ<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第68話</FONT> 天使のほほえみ<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「冴羽 リョウの心に響くキメ台詞集！！ Part 1」
</TD><TD ALIGN="CENTER">2004年4月15日<BR>933円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="8">CITY HUNTER（シティーハンター）VOLUME:08</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／冴子）</TD><TD ALIGN="CENTER">4-19-780231-5</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第69話</FONT> 愛ってなんですか？<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第70話</FONT> 鋼鉄のデート！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第71話</FONT> リョウちゃんの恋愛講座<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第72話</FONT> お医者様でも草津の湯でも！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第73話</FONT> 初恋<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第74話</FONT> 思い出の渚<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第75話</FONT> パパと呼ばないで！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第76話</FONT> ひと目あなたに！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第77話</FONT> オーディション中止！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第78話</FONT> 意外な黒幕！！<FONT SIZE="-2">の巻</FONT><BR>
☆「冴羽 リョウのもっこり美女ランキング（はぁと）　Best 10」
</TD><TD ALIGN="CENTER">2004年4月15日<BR>933円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="9">CITY HUNTER（シティーハンター）VOLUME:09</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780236-6</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第79話</FONT> 思い出は鮮やかに<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第80話</FONT> 掠奪してきたひとりの花嫁<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第81話</FONT> アブナイ解毒薬<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第82話</FONT> 復活なるか？<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第83話</FONT> 喜多川、動く！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第84話</FONT> 復讐は完璧に！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第85話</FONT> 旅立ち<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第86話</FONT> 看護師（ナース）には手を出すな！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第87話</FONT> バラ色の入院生活！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第88話</FONT> 香の看護師（ナース）物語<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「シティーハンター」キャラクター占い
</TD><TD ALIGN="CENTER">2004年5月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="10">CITY HUNTER（シティーハンター）VOLUME:10</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香）</TD><TD ALIGN="CENTER">4-19-780237-4</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第89話</FONT> 林檎たべませんか！？<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第90話</FONT> ごめんなさい…<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第91話</FONT> さよなら…そしてコンニチハ！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第92話</FONT> ある夜のマチガイ<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第93話</FONT> 香チャン気をつけて！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第94話</FONT> ブラジャー大作戦！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第95話</FONT> 槇村の忘れもの<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第96話</FONT> リョウ…その受難の日<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第97話</FONT> 囮大作戦<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第98話</FONT> リョウの純情物語<FONT SIZE="-2">の巻</FONT><BR>
☆「冴羽 リョウのスペシャルドリンクで乾杯！？」
</TD><TD ALIGN="CENTER">2004年5月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="11">CITY HUNTER（シティーハンター）VOLUME:11</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780242-0</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第99話</FONT> 美人キャスターの実力<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第100話</FONT> リョウのレポーター修行<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第101話</FONT> やさしさ猛吹雪！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第102話</FONT> なんて素敵にパートナー<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第103話</FONT> トラブル・スクープ！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第104話</FONT> ニュース速報・礼子の場合<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第105話</FONT> 出逢って恋して占って<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第106話</FONT> シンデレラの見る夢は…<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第107話</FONT> 怪人Ｘの正体<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第108話</FONT> 暗闇でボッキリ！<FONT SIZE="-2">の巻</FONT><BR>
☆「［スクープ！週刊冴羽芸能］ 消えた１億円の謎！？」
</TD><TD ALIGN="CENTER">2004年6月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="12">CITY HUNTER（シティーハンター）VOLUME:12</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／海坊主）</TD><TD ALIGN="CENTER">4-19-780243-9</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第109話</FONT> はい、チーズ！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第110話</FONT> 占いなんて大嫌い！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第111話</FONT> 海坊主からの依頼<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第112話</FONT> 海坊主の置き土産<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第113話</FONT> やさしさ入道雲！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第114話</FONT> 海坊主とバイオリン<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第115話</FONT> スネーク現わる！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第116話</FONT> ラストコンサート<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第117話</FONT> 香チャン狙撃さる！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第118話</FONT> 不器用な告白<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「北条 司 100問100答 ラスト・クエスチョン Part 3」
</TD><TD ALIGN="CENTER">2004年6月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="13">CITY HUNTER（シティーハンター）VOLUME:13</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香）</TD><TD ALIGN="CENTER">4-19-780247-1</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第119話</FONT> 槇村なら・・・<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第120話</FONT> ＳＡＹＯＮＡＲＡ・・・<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第121話</FONT> がんばれ！香ちゃん！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第122話</FONT> 越してきた女<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第123話</FONT> ただ金のためでなく・・・<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第124話</FONT> 動き出した犯人<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第125話</FONT> 日本一軽い男の本領発揮<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第126話</FONT> ラブラブ大作戦！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第127話</FONT> ジェラシー寸前<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第128話</FONT> ハーレム地獄<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第129話</FONT> ＴＯＫＹＯデート・スクランブル<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
☆「香ちゃんの家計簿大公開！」<BR>
</TD><TD ALIGN="CENTER">2004年7月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="14">CITY HUNTER（シティーハンター）VOLUME:14</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780248-X</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第130話</FONT> もっこり度チェック<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第131話</FONT> 愛は誤解の中に！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第132話</FONT> 王女誘拐！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第133話</FONT> 告白のエアポート<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第134話</FONT> 越してきたおとなりさん<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第135話</FONT> 舞子のマイ・ボディーガード<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第136話</FONT> ザ・オーディション<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第137話</FONT> 本能のおもむくままに！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第138話</FONT> 守ってあげたい！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第139話</FONT> サクセスは遠く！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第140話</FONT> 恋人はシティーハンター<FONT SIZE="-2">の巻</FONT><BR>
☆「香がリョウを公開セクハラ裁判」<BR>
</TD><TD ALIGN="CENTER">2004年7月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="15">CITY HUNTER（シティーハンター）VOLUME:15</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780252-8</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第141話</FONT> 勇者と腰抜け<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第142話</FONT> ドア越しの告白<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第143話</FONT> 秘密のバイト君！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第144話</FONT> 懲りない葉子サン！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第145話</FONT> 仮面の男<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第146話</FONT> 暁のＭＥＭＯＲＹ<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第147話</FONT> やってきた春<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第148話</FONT> 眠れぬ夜のために<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第149話</FONT> 香ちゃんの初デート！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第150話</FONT> 危険なふたり！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第151話</FONT> ショック・ショック！<FONT SIZE="-2">の巻</FONT><BR>
☆「報道カメラマン 冬野葉子の現場ルポ」<BR>
</TD><TD ALIGN="CENTER">2004年8月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="16">CITY HUNTER（シティーハンター）VOLUME:16</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／海坊主）</TD><TD ALIGN="CENTER">4-19-780253-6</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第152話</FONT> 許してくれ・・・<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第153話</FONT> 墓地のナンパニスト<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第154話</FONT> あねさんは女子大生！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第155話</FONT> 愛が見える風景<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第156話</FONT> 悲しき未亡人<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第157話</FONT> 花火をあげろ！盛大に！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第158話</FONT> 白いキャンバス<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第159話</FONT> 海ちゃんたら色男！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第160話</FONT> 海坊主にゾッコン！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第161話</FONT> 史上最大の作戦！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第162話</FONT> 赤ちゃんスクランブル<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「リョウちゃんのパートナー適性チェック」<BR>
</TD><TD ALIGN="CENTER">2004年8月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="17">CITY HUNTER（シティーハンター）VOLUME:17</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／美樹）</TD><TD ALIGN="CENTER">4-19-780257-9</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第163話</FONT> 海坊主の愛情<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第164話</FONT> 海坊主が愛した女<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第165話</FONT> スーパー乳母と哀しい天使<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第166話</FONT> 前門の美少女、後門の虎！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第167話</FONT> やっぱり素敵な冴羽さん？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第168話</FONT> すねた紗羅ちゃん<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第169話</FONT> 亜紀子さんの恋心<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第170話</FONT> 勇気をください！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第171話</FONT> 再び空とぶオシリ！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第172話</FONT> リョウ、敗北す！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第173話</FONT> 人質になったもっこり<FONT SIZE="-2">の巻</FONT><BR>
☆「CITY HUNTERの棲む街　新宿ガイドマップ」<BR>
</TD><TD ALIGN="CENTER">2004年9月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="18">CITY HUNTER（シティーハンター）VOLUME:18</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780258-7</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第174話</FONT> プロのプライド！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第175話</FONT> 泥棒勝負開始！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第176話</FONT> 鏡の秘密<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第177話</FONT> ビル街のコールサイン<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第178話</FONT> ふたりの探偵さん<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第179話</FONT> 夢の家へようこそ！<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">（JC未収録扉）</FONT><BR>
・<FONT SIZE="-2">第180話</FONT> こずえの大作戦<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第181話</FONT> こずえの夢<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第182話</FONT> 夢の中の憧れの男<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第183話</FONT> 憂いのＭｙ Ｓｉｓｔｅｒ<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第184話</FONT> 姉妹のきずな<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「冴羽 リョウのLethal Weapons part2」<BR>
</TD><TD ALIGN="CENTER">2004年9月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="19">CITY HUNTER（シティーハンター）VOLUME:19</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香）</TD><TD ALIGN="CENTER">4-19-780262-5</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第185話</FONT> 香ちゃんのトラバーユ！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第186話</FONT> 指輪に秘めた夢<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第187話</FONT> リョウさん、出番です！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第188話</FONT> さよならの向こう側…<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第189話</FONT> セ－ラ－服パニック！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第190話</FONT> プロポーズ狂想曲！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第191話</FONT> 疑惑のデート<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第192話</FONT> 嫌い帰れも好きのうち！？<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第193話</FONT> 武田老人ついに語る<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第194話</FONT> 怒りのマグナム！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第195話</FONT> １７年目のおばあちゃん<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「シティーハンター愛車名鑑」<BR>
</TD><TD ALIGN="CENTER">2004年10月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="X">CITY HUNTER（シティーハンター）VOLUME:X</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>Illustrations 1（表紙／リョウ＆香）</TD><TD ALIGN="CENTER">4-19-780263-3</TD></TR>
<TR><TD>
・<FONT SIZE="-2">Chapter: 1</FONT> ＣＯＶＥＲ<FONT SIZE="-2"> (5colors)*Illustrations</FONT><BR>
・<FONT SIZE="-2">Chapter: 2</FONT> ＯＰＥＮＩＮＧ<FONT SIZE="-2"> (4colors)*Illustrations</FONT><BR>
・<FONT SIZE="-2">Chapter: 3</FONT> ＯＰＥＮＩＮＧ<FONT SIZE="-2"> (2colors)*Illustrations</FONT><BR>
・<FONT SIZE="-2">Chapter: 4</FONT> ＯＰＥＮＩＮＧ<FONT SIZE="-2"> (1color)*Illustrations</FONT><BR>
・<FONT SIZE="-2">Chapter: 5</FONT> ＦＡＮ ＳＱＵＡＲＥ<FONT SIZE="-2"> (1color)*Interview & Favorite</FONT><BR>
・<FONT SIZE="-2">Chapter: 6</FONT> ＳＰＥＣＩＡＬ<FONT SIZE="-2"> (5colors)*Poster & Goods</FONT><BR>
＋Mr.神谷 明'S Treasured Illustration<BR>
</TD><TD ALIGN="CENTER">2004年10月15日<BR>定価800円（本体762円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="20">CITY HUNTER（シティーハンター）VOLUME:20</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／マリィー）</TD><TD ALIGN="CENTER">4-19-780268-4</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第196話</FONT> ふりかえったＯ・ＳＨＩ・ＲＩ<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第197話</FONT> 遠い昔にジャングルで…<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第198話</FONT> 涙のバースデー<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第199話</FONT> デーティングインフェルノ！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第200話</FONT> 夜明けの告白<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第201話</FONT> 女心を知るものは…<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第202話</FONT> 愛は銃よりも強く！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第203話</FONT> 明日へのリバイバル<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第204話</FONT> お嬢さんにパイソンを！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第205話</FONT> 嗚呼、地獄の特訓<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第206話</FONT> ホテル街の００７<FONT SIZE="-2">の巻</FONT><BR>
☆「香ちゃんのLethal Weapons」<BR>
</TD><TD ALIGN="CENTER">2004年11月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="21">CITY HUNTER（シティーハンター）VOLUME:21</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香）</TD><TD ALIGN="CENTER">4-19-780269-2</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第207話</FONT> おじさんの秘密<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第208話</FONT> 秘密のお守り<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第209話</FONT> 天下無敵の女探偵誕生！？<FONT SIZE="-2">の巻</FONT><FONT COLOR="RED">JC未収録扉</FONT><BR>
・<FONT SIZE="-2">第210話</FONT> 飛ぶのが怖い！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第211話</FONT> 香とリョウの危ない一夜<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第212話</FONT> 飛べないスイーパー<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第213話</FONT> Ｆｒｉｇｈｔ，Ｆｌｉｇｈｔ（恐怖の飛行）<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第214話</FONT> エアポート’８９<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第215話</FONT> 大空の告白<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第216話</FONT> 天使の落としもの！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第217話</FONT> あたしがママよ！？<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「冴羽商事完全見取り図」<BR>
</TD><TD ALIGN="CENTER">2004年11月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="22">CITY HUNTER（シティーハンター）VOLUME:22</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780273-0</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第218話</FONT> 涙の母娘再会！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第219話</FONT> 憧れのうしろ姿<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第220話</FONT> 香、努力する！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第221話</FONT> 決闘！蝙蝠Ｖ.Ｓ.リョウ！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第222話</FONT> 背中にさよなら！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第223話</FONT> 依頼人は幽霊！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第224話</FONT> ふたつの顔をもつ女！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第225話</FONT> 暗闇でドッキリ・デート<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第226話</FONT> 鏡の中のもうひとりの私！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第227話</FONT> 炎の中のピアニスト！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第228話</FONT> Ｐｌａｙ ｉｔ ａｇａｉｎ, Ｍａｍｉ！―あの曲をもう一度！―<FONT SIZE="-2">の巻</FONT><BR>
☆「冴羽 リョウ直伝 エンターティナー養成講座」<BR>
</TD><TD ALIGN="CENTER">2004年12月20日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="23">CITY HUNTER（シティーハンター）VOLUME:23</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香）</TD><TD ALIGN="CENTER">4-19-780274-9</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第229話</FONT> リョウの「愛は地球を救う」！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第230話</FONT> 美女と野獣！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第231話</FONT> 愛と悲しみの誘拐犯<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第232話</FONT> 港の決闘！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第233話</FONT> 涙の街頭募金<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第234話</FONT> 突然の出会い！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第235話</FONT> 狙われた少女<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第236話</FONT> 揺れる乙女心！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第237話</FONT> おかしな大追跡！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第238話</FONT> 引き裂かれた心<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第239話</FONT> 山荘の悲劇！！<FONT SIZE="-2">の巻</FONT><BR>
☆「シティーハンターウンチク用語辞典」<BR>
</TD><TD ALIGN="CENTER">2004年12月20日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="24">CITY HUNTER（シティーハンター）VOLUME:24</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香(都会のシンデレラ)）</TD><TD ALIGN="CENTER">4-19-780278-1</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第240話</FONT> 強く、堅い絆<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第241話</FONT> あたしをナンパした女！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第242話</FONT> マイ・フェア・リョウちゃん！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第243話</FONT> 香が水着に着がえたら！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第244話</FONT> 美しき標的！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第245話</FONT> コートの秘密！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第246話</FONT> 華麗なる脱出！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第247話</FONT> 生きるためのファッション！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第248話</FONT> 都会のシンデレラ！！<FONT SIZE="-2">の巻（前編）</FONT><BR>
・<FONT SIZE="-2">第249話</FONT> 都会のシンデレラ！！<FONT SIZE="-2">の巻（後編）</FONT><BR>
・<FONT SIZE="-2">第250話</FONT> 殺したい男！！<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「冴羽 リョウ解体親書」<BR>
</TD><TD ALIGN="CENTER">2005年1月20日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="25">CITY HUNTER（シティーハンター）VOLUME:25</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／海坊主）</TD><TD ALIGN="CENTER">4-19-780279-x</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第251話</FONT> 心に秘められた刃！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第252話</FONT> 曉の決断！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第253話</FONT> 過去の傷跡<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第254話</FONT> 賽は投げられた！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第255話</FONT> リョウを信じろ！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第256話</FONT> 死の気配！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第257話</FONT> 勝敗の行方！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第258話</FONT> 賢者の贈り物！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第259話</FONT> 祖父、現る！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第260話</FONT> もっこりの血族！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第261話</FONT> 狙われたリョウの許嫁！？<FONT SIZE="-2">の巻</FONT><BR>
☆「冴羽 リョウのコスプレランキングBEST 5」<BR>
</TD><TD ALIGN="CENTER">2005年1月20日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="26">CITY HUNTER（シティーハンター）VOLUME:26</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／冴子）</TD><TD ALIGN="CENTER">4-19-780283-8</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第262話</FONT> 誘拐された<FONT SIZE="-1">じいさん付</FONT>下着！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第263話</FONT> 卑劣な罠！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第264話</FONT> ふたりのシティーハンター<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第265話</FONT> Ｉ ｍｕｓｔ ｃｈａｎｇｅ ｍｙ ｌｉｆｅ．<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第266話</FONT> 伊集院隼人氏の平穏な一日<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第267話</FONT> 冴子のお見合い！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第268話</FONT> アニキの残像！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第269話</FONT> 探りを入れろ！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第270話</FONT> すれちがいの中の脱出！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第271話</FONT> 変えられた台本！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第272話</FONT> 命懸けのパートナー<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「香ちゃんのパートナーへの道」<BR>
</TD><TD ALIGN="CENTER">2005年2月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="27">CITY HUNTER（シティーハンター）VOLUME:27</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780284-6</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第273話</FONT> 失われた過去！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第274話</FONT> 死を呼ぶ暗示！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第275話</FONT> 渚のプリンセス！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第276話</FONT> 意外な暗示伝達者！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第277話</FONT> 奇跡の生還！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第278話</FONT> 思い出を消して…<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第279話</FONT> ハートマークの逃がし屋！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第280話</FONT> 恐怖の運送！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第281話</FONT> 密室の恐怖！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第282話</FONT> 暗号を歌う女！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第283話</FONT> あきれた大逃走！！<FONT SIZE="-2">の巻</FONT><BR>
☆「香の『生活豆知識』 医学・救助編」<BR>
</TD><TD ALIGN="CENTER">2005年2月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="28">CITY HUNTER（シティーハンター）VOLUME:28</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780287-0</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第284話</FONT> あなたをパートナーに！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第285話</FONT> もっこり十発の陰謀！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第286話</FONT> 美樹ちゃん、危機一発！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第287話</FONT> 消えたもっこり！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第288話</FONT> リョウと恐るべき似た者姉妹！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第289話</FONT> あなたの全てを知りたい！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第290話</FONT> 父、来襲す！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第291話</FONT> 疑惑の関係！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第292話</FONT> 卑怯者！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第293話</FONT> 二人で一人の心！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第294話</FONT> リョウの“マブダチ”！！<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「冴羽流 夜ばい指南 虎の巻 十ヵ条」<BR>
</TD><TD ALIGN="CENTER">2005年3月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="29">CITY HUNTER（シティーハンター）VOLUME:29</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／ミック）</TD><TD ALIGN="CENTER">4-19-780288-9</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第295話</FONT> おかしな二人！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第296話</FONT> 揺れる男心と女心！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第297話</FONT> 本気と本音！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第298話</FONT> ホントのパートナー！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第299話</FONT> 愛を捕まえたい！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第300話</FONT> おまえへの…<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第301話</FONT> 写真を巡る思い出！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第302話</FONT> 悩めるシュガーボーイ<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第303話</FONT> 優しくて、あったかい背中！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第304話</FONT> 兄の分身！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第305話</FONT> 悲しき旅立ち！！<FONT SIZE="-2">の巻</FONT><BR>
☆「お蔵出し！ 幻の『シティーハンター』４コマ」<BR>
</TD><TD ALIGN="CENTER">2005年3月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="30">CITY HUNTER（シティーハンター）VOLUME:30</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ）</TD><TD ALIGN="CENTER">4-19-780293-5</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第306話</FONT> 涙のペンダント<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第307話</FONT> 狂気の男！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第308話</FONT> 戦いの前夜…<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第309話</FONT> 嵐の前…<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第310話</FONT> 地獄への出航！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第311話</FONT> 魔船の意外な虜！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第312話</FONT> 悪魔のショー！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第313話</FONT> 愛と憎しみと…！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第314話</FONT> ペンダントの記憶！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第315話</FONT> 決着！！<FONT SIZE="-2">の巻</FONT><BR>
☆「Complete Edition Illustration Gallery」<BR>
☆「お蔵出し！ 幻の『シティーハンター』特別マンガ Part1」<BR>
</TD><TD ALIGN="CENTER">2005年4月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="31">CITY HUNTER（シティーハンター）VOLUME:31</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／香）</TD><TD ALIGN="CENTER">4-19-780294-3</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第316話</FONT> 生きる約束！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第317話</FONT> 息子よ！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第318話</FONT> 地獄からの生還！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第319話</FONT> ヒーローの怪我！？<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第320話</FONT> かわいい天使！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第321話</FONT> 本筋から外れた攻防戦！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第322話</FONT> 飲まれた鍵！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第323話</FONT> ワンワンスイーパー！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第324話</FONT> にせＣ・Ｈ登場！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第325話</FONT> 香の家出！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第326話</FONT> Ｗもっこり大暴れ！！<FONT SIZE="-2">の巻</FONT><BR>
☆「お蔵出し！ 幻の『シティーハンター』特別マンガ Part2」<BR>
</TD><TD ALIGN="CENTER">2005年4月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="32">CITY HUNTER（シティーハンター）VOLUME:32</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／リョウ＆香）</TD><TD ALIGN="CENTER">4-19-780298-6</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第327話</FONT> Ｗ夜ばい作戦発動！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第328話</FONT> 疑惑の香り！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第329話</FONT> ミックの苛立ち！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第330話</FONT> 暗殺決行！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第331話</FONT> かみあう二人！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第332話</FONT> 素直な気持ちに！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第333話</FONT> ウェディング・ベル！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第334話</FONT> 血染めのブーケ！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第335話</FONT> 何が何でも…！！<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第336話</FONT> ＦＯＲＥＶＥＲ，ＣＩＴＹ ＨＵＮＴＥＲ！！<FONT SIZE="-2">の巻</FONT><BR>
☆「The Partner's Story ～CITY HUNTER名場面集～」<BR>
</TD><TD ALIGN="CENTER">2005年5月15日<BR>定価980円（本体933円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="Y">CITY HUNTER（シティーハンター）VOLUME:Y</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>Illustrations 2（表紙／リョウ＆香）</TD><TD ALIGN="CENTER">4-19-780302-8</TD></TR>
<TR><TD>
・<FONT SIZE="-2">Chapter: 1</FONT> ＣＯＶＥＲ<FONT SIZE="-2"> (5colors)*Illustrations</FONT><BR>
・<FONT SIZE="-2">Chapter: 2</FONT> ＯＰＥＮＩＮＧ<FONT SIZE="-2"> (4colors)*Illustrations</FONT><BR>
・<FONT SIZE="-2">Chapter: 3</FONT> ＯＰＥＮＩＮＧ<FONT SIZE="-2"> (2colors)*Illustrations</FONT><BR>
・<FONT SIZE="-2">Chapter: 4</FONT> ＯＰＥＮＩＮＧ<FONT SIZE="-2"> (1color)*Illustrations</FONT><BR>
・<FONT SIZE="-2">Chapter: 5</FONT> ＦＡＮ ＳＱＵＡＲＥ<FONT SIZE="-2"> (1color)*Interview & Favorite</FONT><BR>
・<FONT SIZE="-2">Chapter: 6</FONT> ＳＰＥＣＩＡＬ<FONT SIZE="-2"> (5colors)*Goods & Cards</FONT><BR>
＋北条 司 Self-Portrait Gallery: (1)(2)(3)(4)<BR>
＋完全版未収録 Special Illustration [ I・II・III ]<BR>
＋声優：伊倉一恵's Treasured Illustration<BR>
</TD><TD ALIGN="CENTER">2005年6月15日<BR>定価800円（本体762円）</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="Z">CITY HUNTER（シティーハンター）VOLUME:Z</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>Complete Story 特別短編集（表紙／リョウ・ねずみ）</TD><TD ALIGN="CENTER">4-19-780306-0</TD></TR>
<TR><TD>
・<FONT SIZE="-2">Story: 1</FONT> シティーハンター―ＸＹＺ―<BR>
・<FONT SIZE="-2">Story: 2</FONT> シティーハンター―ダブル-エッジ―<BR>
・<FONT SIZE="-2">Story: 3</FONT> <FONT SIZE="-1">「冴羽 リョウの原点」(1)</FONT> 泥棒紳士登場<FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">Story: 4</FONT> <FONT SIZE="-1">「冴羽 リョウの原点」(2)</FONT> しつこいねずみ<FONT SIZE="-2"> の巻</FONT><BR>
＋特別企画 ANIMATION of CITY HUNTER<BR>
＋特別付録 CD [Get Wild]<BR>
</TD><TD ALIGN="CENTER">2005年7月15日<BR>定価900円（本体857円）</TD></TR>
</TABLE>

<!--
<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="*">CITY HUNTER（シティーハンター）VOLUME:2*</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-（表紙／）</TD><TD ALIGN="CENTER">4-19-</TD></TR>
<TR><TD>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
・<FONT SIZE="-2">第2話</FONT> <FONT SIZE="-2">の巻</FONT><BR>
☆「冴羽 リョウ」<BR>
</TD><TD ALIGN="CENTER">2005年*月20日<BR>定価980円（本体933円）</TD></TR>
</TABLE>
 -->


---

---

**[** [戻る](./book2.md) **]**
