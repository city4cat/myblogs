http://www.netlaputa.ne.jp/~arcadia/cocktail/automobile.html

关于汽车和其他车辆的说明。  

---

・关于原作、动画设定的说明用<font color=#ff0000>红色</font>标记。  
  
**M**

> **Morris Mini Cooper 1275 S Mk.1**　　　[写真](../coopers.md)  
> 　<font color=#ff0000>原作中，从「爱为何物」之卷（JC第8卷/文库版第4卷）开始，Mini Cooper S经常作为獠的最爱出现。</font>  
> 　Mini由1959年英国的BMC推出，Mini Cooper和Mini Cooper S是该车的加强版。当时F2和F1的顶级赛车制造师John Newton Cooper已经注意到了Mini汽车的潜力，并调整了其发动机，这是Cooper这个名字的由来。 尤其是Cooper S的开发是为了在拉力赛和其他赛车活动中取得胜利。    
> 　Mini系列的所有车型（基本上）都有相同的外观，根据生产年份的不同，有*Mk1、Mk2、Mk3等区别。  
> 　BMC的新销售方式是以「Austin」和 「Maurice」两个品牌销售Mini车，BMC是以Maurice为中心的Nuffield集团与Austin合并的结果，合并后公司继续利用其现有的销售渠道。 这就是该公司被称为BMC的原因。（译注：待校对）  
>   
> 　*读作Mark1、2、3。 原本是用罗马数字写的，但由于机器上的字符，这里用的是算术数字。  
> 　  
> 　<font color=#ff0000>Leopard的Cooper S是「Morris Mini Cooper S Mk1」</font> 1275cc Cooper S是Cooper系列中最强大的版本，于1964年加入阵容。   
> 　Cooper S从一开始就展示了它的高性能，赢得了它的首场比赛，但最传奇的也许是它在Monte Carlo拉力赛中的连续胜利。 尽管有诸如Works Porsche 911和Lancia等更大排量的对手，Cooper S几乎横扫了世界，连续四次赢得Monte Carlo拉力赛。    
> 　Cooper 1275 S Mk1一直生产到1967年秋季Mini系列被改造成Mk2，其中只有38辆（Morris品牌）通过经销商正式进口到日本。  
> 　顺便说一句，Cooper当时在日本的销售价格超过160万日元。 当时，新员工的起薪约为10,000日元。  
>   
> 　<font color=#ff0000>獠的Cooper S的外观几乎保持了'60年代的原始状态。从门把手的形状来判断，认为是'66年或'67年车型。颜色是黑色的车顶和Tartan红的车身。车顶似乎安装了一个手动帆布顶。</font>现在Mini Cooper的颜色一般是红色车身和白色车顶，但这最初是BMC Worksteam参加拉力赛时的 "工作色"，当时真正的制造商颜色是红色车身和黑色车顶的组合。（Tartan红是Stabilo Pen 68/50的颜色）（译注：待校对）  
> 　<font color=#ff0000>保险杠上安装了超车架和超车杆（也称为角管），这是Cooper车型的标准配置，而油箱则是左右双人的拉力赛规格。</font>  
> 　<font color=#ff0000>后部没有安装倒车灯的唯一原因是当时的英国风格？</font>（应该是按照日本的规定加装的）  
> 　<font color=#ff0000>轮胎是「Cosmic Mk1」轮毂，</font>这是如今非常罕见的替代品（尽管Cooper S本身也很罕见......）。<font color=#ff0000>轮胎是Dunlop SP3</font>。  
> 　<font color=#ff0000>此外，还安装了一个拉力赛底板，以保护车辆底部的油箱。</font>  
>   
> 　<font color=#ff0000>另一方面，尽管内饰的外观，似乎得到了大量的工作。 方向盘已经被一个Motrita风格的方向盘所取代，并且在3重中心表的右侧安装了一个转速计（转速表）。</font>センターメーターの構成は、左から水温計、速度計、油圧計で、その真下のスイッチパネルは、同じく左からヒーターファンノブ、ワイパースイッチ、イグニッションキー、ヘッドランプスイッチ、チョークノブである。   
> 　<font color=#ff0000>它还配备了现代化的设施，例如在中央仪表的左侧有一个内置面板，可以捕捉发射器的无线电信号并显示其位置，还有一个车载电话。  
> 　座椅已被带头枕的座椅所取代，而且似乎可以躺下。  
> 　横向滑开和关闭的侧窗仍在原位，但车门衬里与原版不同。 内饰的红银双色方案与60年代的相同。</font>  
>   
> 　Cooper系列的生产在1971年MK3时代停止了，直到1990年由Rover公司恢复生产，期间出现了19年的空白期。 尽管如此，Mini Cooper在日本非常受欢迎，任何具有Mini形状的汽车都被称为 "Mini Cooper"。 (诞生了Mini的BMC公司被反复兼并、吸收和分离，销售Mini的公司依次为→BLMC→Austin Rover→Rover）    
> 　然而，Mini Cooper实际上是在20世纪80年代恢复的。 这是当「Mini丸山」--一个在日本历史悠久的迷你特殊商店--与John Cooper合作，创造和销售一个特殊的Mini。 虽然不是经销商的车型，但这款特殊的Cooper吸引了全世界的关注，甚至还说服了最初不愿意复制Cooper车型的Rover公司。    
> 　<font color=#ff0000>令人惊讶的是，「Mini丸山」还在电影「爱与宿命的连发枪」上映之际合作播放了一个特别节目。  
>   
> 　与原作相比，不幸的是，Cooper在动画中的形象是相当粗糙的。  
> 　在CH「2」系列之前，Mk1并没有被忠实地描绘出来，因为前格栅和徽章可以在同一个故事中改变，尽管它是一个右舵版本，这是英国汽车的典型特征。    
> 特别是，甚至有一个场景，徽章是Mk3时代的一个大特写，通常被称为「BL Mini」。  
> 然而，其他部分只有Mk 1。 这可能是因为动画制作人员中没有人熟悉Cooper。  
> 　从「3」系列开始，包括影院发行的特辑，Cooper车变得更加奇怪了。 这可能是为了使它看起来更像一辆外国车。 该车现在是左侧驾驶，后视镜也从翼子板后视镜变成了铬制门镜。 前面的扰流板、厚重的过度翼和2套雾灯使该车的风格更加激进。 标志不再与Mk1有关。  
> 　如果硬要解释的话，它可以被描述为带有Mk3或更高底盘的Mini Cooper，在一些地方被 "打扮成Mk2"的样子，但整体上有一个现代风的变化。  
> 　连接在车顶上的车顶天线是一个经典特征。</font>  
>   
> ■**Catalogue data**（Mini Cooper S Mk1）  
<TABLE BORDER="2" cellspacing="1" cellpadding="2">
<TR><TD ALIGN=CENTER>全長度</TD><TD ALIGN=CENTER>3050mm</TD></TR>
<TR><TD ALIGN=CENTER>总高度</TD><TD ALIGN=CENTER>1350mm</TD></TR>
<TR><TD ALIGN=CENTER>总宽度</TD><TD ALIGN=CENTER>1410mm</TD></TR>
<TR><TD ALIGN=CENTER>Wheel base</TD><TD ALIGN=CENTER>2030mm</TD></TR>
<TR><TD ALIGN=CENTER>Front tread</TD><TD ALIGN=CENTER>1234mm</TD></TR>
<TR><TD ALIGN=CENTER>Rear tread</TD><TD ALIGN=CENTER>1201mm</TD></TR>
<TR><TD ALIGN=CENTER>Wheel</TD><TD ALIGN=CENTER>4.5×10inch</TD></TR>
<TR><TD ALIGN=CENTER>车辆重量</TD><TD ALIGN=CENTER>635kg</TD></TR>
<TR><TD>　</TD><TD>　</TD></TR>
<TR><TD ALIGN=CENTER>Engine type</TD><TD ALIGN=CENTER>12FA</TD></TR>
<TR><TD ALIGN=CENTER>排气量</TD><TD ALIGN=CENTER>1275cc</TD></TR>
<TR><TD ALIGN=CENTER>内孔</TD><TD ALIGN=CENTER>70.61mm</TD></TR>
<TR><TD ALIGN=CENTER>Stroke</TD><TD ALIGN=CENTER>81.33mm</TD></TR>
<TR><TD ALIGN=CENTER>压缩比</TD><TD ALIGN=CENTER>9.75</TD></TR>
<TR><TD ALIGN=CENTER>最大输出</TD><TD ALIGN=CENTER>76PS/6000rpm</TD></TR>
<TR><TD ALIGN=CENTER>最大扭矩</TD><TD ALIGN=CENTER>10.93kg-m/3000rpm</TD></TR></TABLE>


> ■**备注**  
> 　Cooper S Mk1现在自然只能作为二手车出售，价格在250万日元～500万日元以上。  
> 　要了解更多关于Mini和Mini Cooper以及Mini Cooper S的信息，请点击这里，[About Mini](../mini0.md)。  
>   
> 
> ---
> 
>   

---

---

**[** [Office](../index.html) **]**