http://www.netlaputa.ne.jp/~arcadia/cocktail/video_ga.html

**CITY HUNTER 関連商品**

---

Game  
  

> ★<font color=#ff0000>City Hunter（PC Engine Soft） </font>   
>   
> ■解説  
> 主人公獠收到了参与案件的委托，独自一人拿着枪进入了敌人的藏身处。 故事发生在一个由建筑物和工厂组成的迷宫里。 在这个侧面滚动的动作射击游戏中，玩家上下楼梯，跑过长长的走廊，在几个房间中搜索，获得信息、武器和物品，并追捕敌人的老板。 故事由4话组成；完成三3个委托就可以进入最后的故事情节。  
>   
> 
> ![](./game_ryo.jpg)  
>   
> 
> > ▼委托之1「和美女一起离去」  
> >   
> > 一周前，接到在Huge Electronics工作的恋人秀樹的电话，说："我开发了一个疯狂的东西"。 从那时起，他就一直没有消息。 Huge Electronics是一家巨大的公司，但在幕后我听到了很多不好的传言。  
> > 我想，也许秀树在做一些危险的工作・・・  
> > 求你了，冴羽先生！ 秀树到底是怎么了？  
> > 能否请你去调查一下？by 美幸  
> >   
> >   
> > ▼委托之2「被猫吓到的海坊主」  
> >   
> > 冴羽，你知道科纳动物研究所在开发食用动物的新物种的背后，还开发了生物武器，这一点你知道吗？  
> > 我是为了委托才潜入那里的，但失败了，我的搭档真希被夺走了。拜托了，帮我救真希吧！by 海坊主<font color=#ff0000>（应该是美樹，弄错了吧？）</font>  
> >   
> >   
> > ▼委托之3「爆炸! 獠和冴子的逃亡」  
> >   
> > 被传为走私枪支的上月組发现，他们其实并不是在走私枪支，而是自己制造枪支。  
> > 而他们正在与一家公司合作，这家公司不仅在制造枪支，而且还在制造更强大的武器。 这家公司非常强大，以至于警察拿他们没办法・・・  
> > 求你了！我希望你把工厂破坏到留下证据的程度，把幕后黑手之出来！  
> >   
> >   
> > ▼最終Stage  
> >   
> > 冴羽獠。  
> > 哼哼......。 我，是Huge Electronics公司的总裁，是科纳动物研究所的所长，也是幕后操纵上月組的人・・・。  
> > 这一次你把我的计划都搞砸了。 我很无语。（译注：待校对）  
> > 礼尚往来，我已经邀请你的伙伴，香，到我的船上。  
> > 如果你不早点来接他们，他们可能根本就不会回来・・・  
> > 胡......胡哈哈哈哈哈......。by 敌人的老板  
> >   
> 
> ![](./game_sun.jpg)  
> 它看起来就像Bill街的Call sign的卷中的游戏!  
>   
> ![](./game_kai.jpg)  
> 在一个场景中恢复力量・・・。(译注：待校对)  
>   
> 
> ■销售机构  
> SAN電子株式会社  
>   
> ■发布日期等  
> 1990年3月2日发布、6300日元。型号SS90001  
>   
> 
> ●
> 
>   
>   
> ★<font color=#ff0000>City Hunter　The Secret Service（Windows）</font>  
>   
> ■解説  
> 基于TV Special的The Secret Service的冒险游戏。 玩家扮演獠，与Secret Service特工安奈的神秘袭击作斗争。 充满了动画场景，充分利用了高度详细的图形；电视广播版本中没有使用的multiple ending。  
>   
> 
> ![](./game_sec.jpg)  
>   
> 
> ■销售机构   
> (株)Yutaka/Bandai Visual  
>   
> ■操作环境  
> Windows/CPU486以上、内存8MB以上  
>   
> ■发布日期等  
> 1996年10月25日发售、9800日元  
>   

---

---

**[** [戻る](./video.md) **]**