http://www.netlaputa.ne.jp/~arcadia/cocktail/music.html

动画中使用的音乐
---

**Music**

> **Soundtracks**  
> ▼CITY HUNTER的全部音乐列表。  
>   
> ●CITY HUNTER soundtrack [収录曲list](../sound/01st1_1.md)  
>   
> ●CITY HUNTER soundtrack Vol.2　[収录曲list](../sound/02st1_2.md)  
>   
> ●CITY HUNTER2 soundtrack Vol.1　[収录曲list](../sound/03st2_1.md)  
>   
> ●CITY HUNTER2 soundtrack Vol.2　[収录曲list](../sound/04st2_2.md)  
>   
> ●CITY HUNTER劇場版「愛和宿命的连发枪」soundtrack　[収录曲list](../sound/05st_mov.md)  
>   
> ●CITY HUNTER3 soundtrack　[収录曲list](../sound/06st3.md)  
>   
> ●CH Dramatic Master　[収录曲list](../sound/07st_dr1.md)  
>   
> ●CH Dramatic Master Vol.2 Vocal版　[収录曲list](../sound/08st_dr2vo.md)  
>   
> ●CH Dramatic Master Vol.2 Instrumental版　[収录曲list](../sound/09st_dr2ins.md)  
>   
> ●CITY HUNTER Original Special soundtrack　[収录曲list](../sound/10st_bay.md)  
>   
> ●CITY HUNTER'91 soundtrack　[収录曲list](../sound/11st91.md)  
>   
> ●成龙电影「城市猟人」[Image Song](../sound/12single.md)  
>   
> ●CH '96sp.「The Secret Service」soundtrack　[収录曲list](../sound/13st_sec.md)  
>   
> ●CH '97sp.「Goodby My Sweetheart」soundtrack　[収录曲list](../sound/14st_good.md)  
>   
> ●CH '99sp.「紧急直播！？ 暴徒冴羽獠的最后日子」soundtrack　[収录曲list](../sound/15st_kin.md)  
>   
>   
> ■**备注**  
> 所有数据都在CD上；存在一些同名的磁带和唱片。  
> CD还包括[CD Book 2个](./book_cd.md#cd)「吻你的搭档...」和「悲伤天使」。  
>   
> 
> ---
> 
>   

---

---

**[** [Office](http://www.netlaputa.ne.jp/~arcadia/index.html) **]**