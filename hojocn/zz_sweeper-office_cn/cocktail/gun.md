http://www.netlaputa.ne.jp/~arcadia/cocktail/gun.html

对枪支的解读。

---

・关于原作、动画设定的说明用<font color=#ff0000>红色</font>标记。    
  
**C**

> **Colt Python .357Magnum**　　　[写真](../python.md)  
> 　<font color=#ff0000>獠最常使用的手枪是一支4英寸枪管的Python。</font>  
> 　Python是美国Colt公司于1955年推出的一种 * Double action（译注：双动)左轮手枪。  
> 口径为.357Magnum，可发射.357Magnum和.38Spl.子弹（即38Special）。  
> 手枪的Magnum弹是由Smith&Wesson公司和Winchester公司联合开发的实际包，.357Magnum弹在1935年完成。  
> 　然而，Colt公司直到1954年才推出其第一支大口径左轮手枪「Trooper」，不幸的是，Trooper无法超越当时已经上市的S&W大口径左轮手枪的变化和商业吸引力。  
> 　因此，Colt公司计划并开发了一把具有自己公司声望的左轮手枪杰作，以便将其在 * SAA中建立的信任延续到Magnum枪上。 Python就是这样的结果。  
> 　Python在枪架设计上与Trooper相似，但有一个延伸到muzzle（=枪口）的弹射杆护罩和带冷却孔的 * ventilated ribs（译注：散热肋条），使其成为有史以来最豪华和最强大的左轮手枪。  
> 用Colt Prestige来形容Colt的豪华程度真是再恰当不过了。
> 　Python整体抛光良好，它以Colt Royal Blue为底色。  
> 然而，枪身顶部的视线和枪管上的肋条顶部是经过亚光处理的（即消光），以防止光线的漫反射。  
> 　边缘也大多被磨圆，但平坦的枪口尖端和ventilated ribs顶部仍然锋利。  
> 人手没有直接接触到的地方也保留了锋利，协调了直线和曲线的关系。    
> 　 因为不能降低制造成本，所以价格也很高。4英寸型号的价格约为10万日元。 以一把Python的价格可以买到2把相同口径的S&W枪。  
> 　从样说来，Python枪甚至被称为手枪中的Rolls Royce。  
>   
> 　* Double action：一种只需扣动扳机就能扣动击锤并发射的机制。  
> 　* SAA：Single Action Army的缩写，这种枪在西部电影中很有名，1873年推出。  
> 　* ventilated ribs：安装在被枪火加热的枪管上的散热器，以防止其遮挡和扭曲瞄准器。  
>   
> 　自1955年推出「First Production Model」以来，Python经历了不少于5次的小改动。  
>  特别是'60年代的Second Models，被称为「vintage class(译注：复古级)」。  
> 　<font color=#ff0000>獠使用的Python被认为是Third Model，也被称为70年代的「Best-Selling Model」，由传奇枪械师、已故的真柴憲一郎调整。</font>  
> 　实际的Python在行动上并没有得到良好的声誉。  
>  对Python的平均评价是，它的枪管精度很好，但在机制方面有问题。  
> 　话说回来，某枪械杂志的读者专栏曾经挤满了这样的评论「一个以杀人为生的人，用一把行动拙劣的Python，不管它在漫画里有多好，很奇怪它能连续射击一百次」。  
> 当然，也有很多粉丝为CH辩护，这场 「CH争论」愈演愈烈，在CH'91播出的时候，动画工作人员也参与其中。  
>   
> 　Python机制基本上有100多年的历史。在Double Action射击时，在两个点上有强烈的摩擦运动：扳机和击锤支杆（即扣动击锤的部分），以及回弹杆（即使击锤回位的部分）和手（即旋转枪筒的部分）。  
> 内部使用的弹簧是leaf弹簧（=板状弹簧）。  
> 以上是使Python手枪成为「最难操作的Double Action左轮手枪」的一些主要因素，但当扳机全部扣下时，它比S&W左轮手枪更顺畅。  
> (在缓慢扣动扳机时，使用板簧击锤的枪支比使用螺旋弹簧的枪支更难操作，因为张力会逐渐增加。 此外，Python也很难把握住松开的时机（即击锤张力达到峰值的时刻=枪锤落下的时刻），因为枪膛旋转结束和击锤落下的时机是在同一时间）  
> 　Python的bore diameters（即枪管直径）小于0.355英寸，比弹头直径0.357英寸小0.002英寸。 据说这0.002英寸的差异会转化为枪管精度的差异，这并不全是好事。 它还会导致功率损失。  
> 此外，Python的枪膛间隙（即枪膛与枪管之间的间隙）很大，射击时可以使更多的火药燃烧气体从间隙中逸出，爆炸时子弹被枪管中的膛线刨出的金属粉末击中射手的面部和手腕。  
> <font color=#ff0000>短篇故事「CityHunter　XYZ」的开篇，在洛杉矶郊外的一个射击场的场景中，獠用一把从枪店买来的Python射击，在他旁边射击的清水美津子抱怨说「火药屑溅到她脸上，弄疼了她」。</font>  
>   
> ■**其他要点**  
> ●弹射杆护罩是一个平衡砝码。  
> ●散热肋条在正常使用中并不像预期的那样有效。  
> ●框架尺寸被归类为「I-框架」。  
> ●宽击锤顶纹（称为快速上膛。 它是一种可以快速扣动的大击锤，是一种单一的动作）。  
> ●在较新和较老的Python型号之间，击锤的形状也有细微差别。  
> ●锯齿状坡道（=锯齿状坡度）前视镜。 战斗类型。  
> ●front sight(译注：前视镜)是用2个销钉固定的。 最近的型号是用1个销钉固定的。  
> ●Rear sight（译注：后视镜）可微调风向（左和右）和仰角（高度）。(还有一种带红灯的前视镜。 在这种情况下，后视镜有一条白线）  
> ●Firing pin（译注：撞针）是一种浮动式的，与击锤分离。  
> ●Grip（译注：握把）是棋盘式的胡桃木（'90年代的橡胶握把）。  
> ●Rifling(译注：膛线)是每个左旋的6条线。 螺距为每圈14英寸。 
> ●从射手角度看，Cylinder（译注：弹筒）的旋转方向是顺时针。  
> ●Cylinder latches（译注：弹筒闩）是向后拉的（在第三个型号之前，手指碰到的地方没有凹槽）。  
> ●枪管左侧刻的是 「PYTHON.357  ★.357MAGNUM-CTG.★」，其中CTG代表子弹。 
> ●枪管右侧刻的是「COLT'S PT. F.A. MFG. CO. HARTFORD, CT. U.S.A.」，意思是Colt Patent Firearms Manufacturing Company, Hartford, Connecticut（译注：美国Connecticut州，Hartford的Colt专利火器制造公司）。  
> ●'80年代初(?)，刻的是「COLT'S PT. F.A. MFG. CO. HARTFORD, CONN. U.S.A.」  
> ●80年代末以后的型号，在枪管右侧的雕刻开始和结束时，还另外画了一条横线。  
> ●除了蓝色型号外，还有一个不锈钢型号。 (不锈钢型号是在'85年推出的）。  
> ●枪管长度包括2.5英寸、3英寸、4英寸、6英寸和8英寸，其中3英寸型号被命名为「Combat Python」，带瞄准镜的8英寸型号被命名为「Python Hunter」。  
> ●自98年以来，大规模生产已经停止，只以「Python Elite」的名义订做，但热爱Python的枪店总是有库存。  
>   
> ■**归档数据**（4inch Blue model/带真正的木质手柄）  
<TABLE BORDER="2" cellspacing="1" cellpadding="2">
<TR><TD ALIGN=CENTER>总长</TD><TD ALIGN=CENTER>24.1cm</TD></TR>
<TR><TD ALIGN=CENTER>总高</TD><TD ALIGN=CENTER>14.0cm</TD></TR>
<TR><TD ALIGN=CENTER>总宽</TD><TD ALIGN=CENTER>3.94cm</TD></TR>
<TR><TD ALIGN=CENTER>重量</TD><TD ALIGN=CENTER>1092g</TD></TR>
<TR><TD ALIGN=CENTER>枪管长度</TD><TD ALIGN=CENTER>10.1cm</TD></TR>
<TR><TD ALIGN=CENTER>适用的弹药</TD><TD ALIGN=CENTER>.357Mag./.38Spl.</TD></TR>
<TR><TD ALIGN=CENTER>子弹的数量</TD><TD ALIGN=CENTER>6</TD></TR>
<TR><TD ALIGN=CENTER>Trigger pull double-action（译注：双动模式的扳机压力）</TD><TD ALIGN=CENTER>4.8kg</TD></TR>
<TR><TD ALIGN=CENTER>Trigger pull single-action（译注：单动模式的扳机压力）</TD><TD ALIGN=CENTER>1.8kg</TD></TR></TABLE><BR>


> ■**备注**  
> 　<font color=#ff0000>根据CH3的CD收录的「A LOVE NO ONE CAN CHANGE」，獠使用的.357 Magnum子弹的重量为10.2克，显然是158 grain(喱)的子弹。</font>  
> 　在同一首歌中，他说「初始速度，376米」，但初始速度（=子弹刚从枪口射出后的速度，代表子弹在1秒钟内前进的距离）在每次测量同一颗子弹和同一支枪时，都会有1~5%的差异。  
> 　'99年10月，Colt公司宣布它打算清算其手枪生产系列。  
>   
> 
> ---
> 
>   

---

---

**[** [Office](../index.md) **]**