http://www.netlaputa.ne.jp/~arcadia/cocktail/book_anime.html

**CITY HUNTER 関連書籍**

---

> JUMP COMICS SELECTION Anime Comics 　发行／Home社　发售／集英社


<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">City Hunter The Secret Service 前編</FONT></TD><TD ALIGN="CENTER">週刊少年Jump編集部・編</TD></TR>
<TR><TD>The Secret Service　前編</TD><TD ALIGN="CENTER">4-8342-1414-1</TD></TR>
<TR><TD>・描きおろしスペシャルポスター<BR>・CHARACTERS SECRET DATA<BR>・「The Secret Service」前編アニメシアター<BR>・OPENING & ENDING COLLECTION</TD><TD ALIGN="CENTER">1996年1月24日<BR>690円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">City Hunter The Secret Service 後編</FONT></TD><TD ALIGN="CENTER">週刊少年Jump編集部・編</TD></TR>
<TR><TD>The Secret Service　後編</TD><TD ALIGN="CENTER">4-8342-1415-X</TD></TR>
<TR><TD>・描きおろしスペシャルポスター<BR>・REVIEW<BR>・「The Secret Service」後編アニメシアター<BR>スタッフ＆キャスト・オールデータ</TD><TD ALIGN="CENTER">1996年1月24日<BR>690円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER SPECIAL（シティーハンタースペシャル）</FONT></TD><TD ALIGN="CENTER">週刊少年Jump編集部・編</TD></TR>
<TR><TD>～グッド・バイ・マイ・スイート・ハート～</TD><TD ALIGN="CENTER">4-8342-1526-1</TD></TR>
<TR><TD>・かきおろしスペシャルポスター<BR>・CHARACTER FILE<BR>・～グッド・バイ・マイ・スイート・ハート～アニメシアター<BR>・ORIGINAL OPENING DIGEST<BR>スタッフ＆キャスト・オールデータ</TD><TD ALIGN="CENTER">1997年8月24日<BR>1000円</TD></TR>
</TABLE>

---

---

**[** [戻る](./book.md) **]**