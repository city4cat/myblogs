http://www.netlaputa.ne.jp/~arcadia/cocktail/aotable.html

**CITY HUNTER 原作・动画对应简表**

---

动画化故事的简表。

<TABLE BORDER="1">
<TR><TD ALIGN="CENTER">原作</TD><TD ALIGN="CENTER">动画</TD></TR>
<TR><TD><A HREF="storyb.md#1">読切り＃１</A>City Hunter -ＸＹＺ-</TD><TD><A HREF="story1.md#2"> 第2話</A>　私を殺して！！　美女に照準は似合わない</TD></TR>
<TR><TD><A HREF="storyb.md#2">読切り＃２</A>City Hunter -Double-Edge-</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#1">＃１</A>栄光なきテンカウント！</TD><TD><A HREF="story1.md#3"> 第3話</A>　愛よ消えないで！　明日へのテンカウント</TD></TR>
<TR><TD><A HREF="storyc.md#2">＃２</A>BMW（ベーエムベー）の悪魔</TD><TD><A HREF="story1.md#1"> 第1話</A>　粋なスイーパー　ＸＹＺは危険なカクテル</TD></TR>
<TR><TD><A HREF="storyc.md#3">＃３</A>闇からの狙撃者！</TD><TD><A HREF="story1.md#4"> 第4話</A>　美女蒸発！！　ブティックは闇への誘い</TD></TR>
<TR><TD><A HREF="storyc.md#4">＃４</A>恐怖のAngel Dust</TD><TD><A HREF="story1.md#5"> 第5話</A>　グッバイ槇村　雨の夜に涙のバースデー</TD></TR>
<TR><TD><A HREF="storyc.md#5">＃５</A>危険な家庭教師</TD><TD><A HREF="story1.md#10"> 第10話</A>　危険な家庭教師？　女子高生(スケバン)に愛の手料理</TD></TR>
<TR><TD><A HREF="storyc.md#6">＃６</A>待ちつづける少女</TD><TD><A HREF="story1.md#7"> 第7話</A>　心ふるえる銃声　悲しきロンリーガール</TD></TR>
<TR><TD><A HREF="storyc.md#7">＃７</A>裸足の女優</TD><TD><A HREF="story1.md#6"> 第6話</A>　恋しない女優　希望へのラストショット</TD></TR>
<TR><TD><A HREF="storyc.md#8">＃８</A>鐘とともに運命が！</TD><TD><A HREF="story1.md#15"> 第15話</A>　リョウが女子大講師？　麗しのお嬢サマを守れ</TD></TR>
<TR><TD><A HREF="storyc.md#9">＃９</A>その女に手をだすな！</TD><TD><A HREF="story1.md#8"> 第8話</A>　美人に百発百中？！　女刑事には手を出すな</TD></TR>
<TR><TD><A HREF="storyc.md#10">＃１０</A>哀愁のギャンブラー</TD><TD><A HREF="story1.md#9"> 第9話</A>　ギャンブルクイーン　華麗なる恋の賭け</TD></TR>
<TR><TD><A HREF="storyc.md#11">＃１１</A>気になるあいつ！</TD><TD><A HREF="story1.md#14"> 第14話</A>　１６歳結婚宣言！　アイドルに熱いキッス</TD></TR>
<TR><TD><A HREF="storyc.md#12">＃１２</A>危険な国からきた女！</TD><TD><A HREF="story1.md#12"> 第12話</A>　子供は得だね！　危険な国のモッコリ美人</TD></TR>
<TR><TD><A HREF="storyc.md#13">＃１３</A>空とぶオシリ！</TD><TD><A HREF="story1.md#11"> 第11話</A>　レオタード美女は　チューリップがお好き</TD></TR>
<TR><TD><A HREF="storyc.md#14">＃１４</A>天使のほほえみ</TD><TD><A HREF="story1.md#26"> 第26話</A>　愛ってなんですか？　リョウの正しい恋愛講座</TD></TR>
<TR><TD><A HREF="storyc.md#15">＃１５</A>思い出の渚</TD><TD><A HREF="story1.md#19"> 第19話</A>　思い出の渚　オーディションは危険がいっぱい</TD></TR>
<TR><TD><A HREF="storyc.md#16">＃１６</A>掠奪してきたひとりの花嫁</TD><TD><A HREF="story1.md#23"> 第23話</A>　毒バチブンブン！！　空から花嫁降ってきた</TD></TR>
<TR><TD><A HREF="storyc.md#17">＃１７</A>看護婦には手を出すな！</TD><TD><A HREF="story1.md#24"> 第24話</A>　バラ色の入院生活？　狙われた白衣の天使</TD></TR>
<TR><TD><A HREF="storyc.md#18">＃１８</A>槇村の忘れもの</TD><TD><A HREF="story1.md#13"> 第13話</A>　オレの敵は美女？　史上最大の美女地獄！！</TD></TR>
<TR><TD><A HREF="storyc.md#19">＃１９</A>トラブル・スクープ！</TD><TD><A HREF="story1.md#35"> 第35話</A>　突撃美人キャスター　リョウの秘モッコリ取材</TD></TR>
<TR><TD><A HREF="storyc.md#20">＃２０</A>シンデレラの見る夢は…</TD><TD><A HREF="story2.md#52"> 第52-53話</A>　リョウは許嫁？！　出会って恋して占います！（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#21">＃２１</A>ラスト・コンサート</TD><TD><A HREF="story1.md#27"> 第27-28話</A>　リョウと海坊主の　純情足ながおじさん伝説（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#22">＃２２</A>がんばれ！香ちゃん！！</TD><TD><A HREF="story2.md#54"> 第54-55話</A>　狙われた香！！ 　愛の言葉はさようなら（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#23">＃２３</A>越してきた女（ひと）</TD><TD><A HREF="story1.md#41"> 第41-42話</A>　冴子の妹は女探偵(前後編) ―翔んだ女の大胆秘密＆翔んだ女の大捕物帳</TD></TR>
<TR><TD><A HREF="storyc.md#24">＃２４</A>告白のエアポート</TD><TD><A HREF="story2.md#61"> 第61話</A>　モッコリ殺し？！　王女の高貴なオーラ（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#25">＃２５</A>舞子のマイ・ボディーガード</TD><TD><A HREF="story2.md#107"> 第107-108話</A>　新宿サクセス物語　お隣さんは美人ダンサー（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#26">＃２６</A>恋人はCity Hunter</TD><TD><A HREF="story2.md#73"> 第73-74話</A>　標的はリョウ！　激写美人は危険が大好き！（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#27">＃２７</A>危険なふたり！</TD><TD><A HREF="story2.md#76"> 第76-77話</A>　狙われた証人！　男装美女と危険な二人（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#28">＃２８</A>あねさんは女子大生！</TD><TD><A HREF="story2.md#96"> 第96-97話</A>　19歳の未亡人！　スケッチ美人に心の恋人（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#29">＃２９</A>海坊主（ファルコン）にゾッコン！！</TD><TD><A HREF="story2.md#90"> 第90-91話</A>　海ちゃん色男！　美人スイーパー美樹が肉迫（前編）</TD></TR>
<TR><TD><A HREF="storyc.md#30">＃３０</A>哀しい天使</TD><TD><A HREF="story2.md#92"> 第92-93話</A>　モッコリだらけ！　リョウの心と超能力少女（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#31">＃３１</A>再び空飛ぶオシリ！</TD><TD><A HREF="story2.md#94"> 第94-95話</A>　リョウは恋泥棒！　魔境に秘めた愛の行方（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#32">＃３２</A>さよならの向こう側…</TD><TD><A HREF="story2.md#103"> 第103-104話</A>　20年目の再会！　冴羽さん妹をよろしく（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#33">＃３３</A>ビル街のコールサイン</TD><TD><A HREF="story2.md#98"> 第98-99話</A>　夢みるように恋したい　１２歳天使の作戦（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#34">＃３４</A>プロポーズ狂想曲（ラプソディ）！？</TD><TD><A HREF="story2.md#105"> 第105-106話</A>　17歳のプロポーズ　疑惑のデートパニック（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#35">＃３５</A>お嬢さんにパイソンを！</TD><TD><A HREF="story3.md#118"> 第118-119話</A>　危ない探偵ごっこ！　お嬢さんにパイソンを（前後編）</TD></TR>
<TR><TD><A HREF="storyc.md#36">＃３６</A>明日へのリバイバル</TD><TD><A HREF="story2.md#112"> 第112-114話</A>　グッドラック　マイスイーパー　二人のシティーストリート（前中後編）</TD></TR>
<TR><TD><A HREF="storyc.md#37">＃３７</A>大空の告白</TD><TD><A HREF="story91.md#128"> 第128話</A>　迷コンビ大復活！　空から舞いおりた美女</TD></TR>
<TR><TD><A HREF="storyc.md#38">＃３８</A>天使の落としもの！？</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#39">＃３９</A>Play it again, Mami! -あの曲をもう一度-</TD><TD><A HREF="story91.md#132"> 第132-133話</A>　恐怖！　新宿怪談！！　さまよえる美女の魂 ＆ 別れのレクイエム　あの面影をもう一度</TD></TR>
<TR><TD><A HREF="storyc.md#40">＃４０</A>美女と野獣！？</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#41">＃４１</A>突然の出会い！！</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#42">＃４２</A>香が水着に着替えたら！？</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#43">＃４３</A>都会のシンデレラ！！</TD><TD><A HREF="story91.md#137"> 第137話</A>　今夜だけこの愛を…　都会のシンデレラ物語</TD></TR>
<TR><TD><A HREF="storyc.md#44">＃４４</A>賢者の贈り物！！</TD><TD><A HREF="story91.md#135"> 第135-136話</A>　復讐の美女！　リョウに哀しみのブルースを ＆ 硝煙の行方…　City Hunter暁に死す！</TD></TR>
<TR><TD><A HREF="storyc.md#45">＃４５</A>ふたりのCity Hunter</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#46">＃４６</A>伊集院隼人氏の平穏な一日</TD><TD><A HREF="story91.md#134"> 第134話</A>　あの伊集院隼人氏の極めて平穏な一日</TD></TR>
<TR><TD><A HREF="storyc.md#47">＃４７</A>冴子のお見合い！！</TD><TD><A HREF="story91.md#129"> 第129話</A>　さらば香！　City Hunter逮捕指令</TD></TR>
<TR><TD><A HREF="storyc.md#48">＃４８</A>思い出を消して…</TD><TD><A HREF="story91.md#130"> 第130話</A>　危険を買う美女！　想い出は光の彼方に</TD></TR>
<TR><TD><A HREF="storyc.md#49">＃４９</A>暗号を歌う女！？</TD><TD><A HREF="story91.md#131"> 第131話</A>　恋もＡ級ライセンス　美人逃がし屋参上！</TD></TR>
<TR><TD><A HREF="storyc.md#50">＃５０</A>ふたりでひとりの心！！</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#51">＃５１</A>もっこり十発の陰謀！？</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#52">＃５２</A>涙のペンダント</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#53">＃５３</A>ワンワンスイーパー！！</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#54">＃５４</A>素直な気持ちに！！</TD><TD>なし</TD></TR>
<TR><TD><A HREF="storyc.md#55">＃５５</A>FOREVER, CITY HUNTER!!</TD><TD>なし</TD></TR>

</TABLE>

---

---

**[** [Office](../index.md) **]**