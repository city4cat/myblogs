http://www.netlaputa.ne.jp/~arcadia/cocktail/fashion.html

与时尚有关的解读。  

---

・关于原作、动画设定的说明用<font color=#ff0000>红色</font>标记。  
  
**B**

> **Belt Buckle Knife**　　　[写真](../buckle.md)  
> 　<font color=#ff0000>獠腰带扣里面有一把小刀，刀尖的设计是为了能够解开手铐。</font>  
> 　这把腰带扣刀是由美国Gerber（=GERBER LEGENDARY BLADES）制造的，目前已不再生产。  
> 这把刀不是很大，总长度为11cm，刀刃长度为3.4cm。 刀片是由不锈钢制成的。  
> 有些型号的刀柄上有turkey或hunting dock图案。  
> 这些刀是由一个叫Blackie Collins的设计师设计的。  
>   
> ■**备注**  
> 　Gerber是一家1939年成立于Oregon Portland的制造商。 它是世界上最著名的刀具制造商之一，生产具有现代设计和各种理念的运动刀具。  
> 猎刀借鉴了早期生产的厨房刀具的技术，受到了专业猎人和向导的高度赞扬。  
>   
> 
> ---
> 
>   

---

---

**[** [Office](../index.md) **]**