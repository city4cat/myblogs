http://www.netlaputa.ne.jp/~arcadia/cocktail/book_jc.html

**CITY HUNTER 関連書籍**

---

> JUMP COMICS（ジャンプコミックス）


<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">↑離南島からの招待状の巻に、东洋财团总帅、海原神登場。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">キャッツアイ（CAT'S EYE）第10巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>離南島(ルナンとう)からの招待状の巻</TD><TD ALIGN="CENTER">4-08-851460-2</TD></TR>
<TR><TD>・離南島(ルナンとう)からの招待状の巻<BR>・おいしい生活の巻<BR>・キャッツ風犯人料理法の巻</TD><TD ALIGN="CENTER">1984年7月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">↑重いトリガーの巻、拿枪的场景里说「像CityHunter」的台词。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">キャッツアイ（CAT'S EYE）第13巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>危険な迷いの巻</TD><TD ALIGN="CENTER">4-08-851463-7</TD></TR>
<TR><TD>・夏の夜の悪夢の巻<BR>・危険な迷いの巻<BR>・九月の白昼夢の巻<BR>・カセットレターの巻<BR>・あなたにだけ愛情こめての巻<BR>・子供じゃないもんの巻<BR>・30億円のつまみぐいの巻<BR>・飛べない小鳥の巻<BR>・重いトリガーの巻</TD><TD ALIGN="CENTER">1985年2月15日<BR>360円</TD></TR>
</TABLE><BR><BR>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第1巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>恐怖のエンジェルダストの巻</TD><TD ALIGN="CENTER">4-08-852381-4</TD></TR>
<TR><TD>・栄光なきテンカウント！の巻<BR>・BMWの悪魔の巻<BR>・闇からの狙撃者！の巻<BR>・恐怖のエンジェルダストの巻<BR>・ステキな相棒！の巻</TD><TD ALIGN="CENTER">1986年1月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第2巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>将軍の罠！の巻</TD><TD ALIGN="CENTER">4-08-852382-2</TD></TR>
<TR><TD>・将軍の罠！の巻<BR>・悪党にはなにもやるな！の巻<BR>・危険な家庭教師の巻</TD><TD ALIGN="CENTER">1986年4月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第3巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>裸足の女優の巻</TD><TD ALIGN="CENTER">4-08-852383-0</TD></TR>
<TR><TD>・アンバランスギャングの巻<BR>・待ちつづける少女の巻<BR>・裸足の女優の巻<BR>・撮影所パニック！の巻</TD><TD ALIGN="CENTER">1986年6月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第4巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>鐘とともに運命が！の巻</TD><TD ALIGN="CENTER">4-08-852384-9</TD></TR>
<TR><TD>・ゆれる心の巻<BR>・亡霊を撃て！の巻<BR>・とんだティーチャーの巻<BR>・思い出のキズ跡の巻<BR>・鐘とともに運命が！の巻</TD><TD ALIGN="CENTER">1986年9月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第5巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>ワン・オブ・サウザンドの巻</TD><TD ALIGN="CENTER">4-08-852385-7</TD></TR>
<TR><TD>・その女に手をだすな！の巻<BR>・ワン・オブ・サウザンドの巻<BR>・なんたって執念!!の巻<BR>・恐怖の男センサーの巻<BR>・ギャンブルクィーン！の巻</TD><TD ALIGN="CENTER">1986年12月10日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第6巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>哀愁のギャンブラーの巻</TD><TD ALIGN="CENTER">4-08-852386-5</TD></TR>
<TR><TD>・哀愁のギャンブラーの巻<BR>・気になるあいつ！の巻</TD><TD ALIGN="CENTER">1987年2月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第7巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>危険な国からきた女！の巻</TD><TD ALIGN="CENTER">4-08-852387-3</TD></TR>
<TR><TD>・恋は盲目！の巻<BR>・危険な国からきた女！の巻<BR>・空飛ぶオシリ！の巻</TD><TD ALIGN="CENTER">1987年4月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第8巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>天使のほほえみの巻</TD><TD ALIGN="CENTER">4-08-852388-1</TD></TR>
<TR><TD>・とんでる博士！の巻<BR>・心に咲いたチューリップの巻<BR>・天使のほほえみの巻<BR>・愛ってなんですかの巻<BR>・リョウちゃんの恋愛講座の巻</TD><TD ALIGN="CENTER">1987年6月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第9巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>思い出の渚の巻</TD><TD ALIGN="CENTER">4-08-852389-X</TD></TR>
<TR><TD>・お医者様でも草津の湯でも!!の巻<BR>・初恋の巻<BR>・思い出の渚の巻<BR>・掠奪してきたひとりの花嫁の巻</TD><TD ALIGN="CENTER">1987年8月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第10巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>看護婦には手を出すな！の巻</TD><TD ALIGN="CENTER">4-08-852390-3</TD></TR>
<TR><TD>・アブナイ解毒薬の巻<BR>・看護婦には手を出すな！の巻</TD><TD ALIGN="CENTER">1987年10月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第11巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>槇村の忘れもの の巻</TD><TD ALIGN="CENTER">4-08-852391-1</TD></TR>
<TR><TD>・さよなら…そしてコンニチハ！の巻<BR>・ある夜のマチガイの巻<BR>・ブラジャー大作戦!!の巻<BR>・槇村の忘れもの の巻<BR>・リョウ…その受難の日の巻<BR>・リョウの純情物語の巻</TD><TD ALIGN="CENTER">1987年12月9日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第12巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>トラブル・スクープの巻</TD><TD ALIGN="CENTER">4-08-852392-X</TD></TR>
<TR><TD>・美人キャスターの実力の巻<BR>・トラブル・スクープの巻<BR>・ニュース速報 礼子の場合の巻<BR>・出逢って恋して占っての巻<BR>・シンデレラの見る夢は…の巻</TD><TD ALIGN="CENTER">1988年2月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第13巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>海坊主と足ながおじさんの巻</TD><TD ALIGN="CENTER">4-08-852393-8</TD></TR>
<TR><TD>・暗闇でボッキリ！の巻<BR>・はい、チーズ!!の巻<BR>・占いなんて大嫌い！？の巻<BR>・海坊主からの依頼の巻<BR>・スネーク現る！の巻<BR>・ラスト・コンサートの巻</TD><TD ALIGN="CENTER">1988年4月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第14巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>がんばれ！ 香ちゃん!!の巻</TD><TD ALIGN="CENTER">4-08-852394-6</TD></TR>
<TR><TD>・香ちゃん狙撃さる！？の巻<BR>・SAYONARA…の巻<BR>・がんばれ！香ちゃん!!の巻<BR>・越してきた女(ひと)の巻<BR>・動き出した犯人の巻</TD><TD ALIGN="CENTER">1988年6月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第15巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>告白のエアポートの巻</TD><TD ALIGN="CENTER">4-08-852395-4</TD></TR>
<TR><TD>・ラブラブ大作戦！の巻<BR>・ジェラシー寸前の巻<BR>・ハーレム地獄の巻<BR>・TOKYOデート・スクランブルの巻<BR>・王女誘拐！？の巻<BR>・告白のエアポートの巻<BR>・越してきたおとなりさんの巻</TD><TD ALIGN="CENTER">1988年8月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第16巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>恋人はシティーハンターの巻</TD><TD ALIGN="CENTER">4-08-852396-2</TD></TR>
<TR><TD>・舞子のマイ・ボディーガードの巻<BR>・本能のおもむくままに！？の巻<BR>・守ってあげたい！？の巻<BR>・サクセスは遠く！？の巻<BR>・恋人はシティーハンターの巻<BR>・ドア越しの告白の巻<BR>・秘密のバイト君！の巻</TD><TD ALIGN="CENTER">1988年10月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第17巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>暁のメモリーの巻</TD><TD ALIGN="CENTER">4-08-852397-0</TD></TR>
<TR><TD>・懲りない葉子サン！の巻<BR>・暁のMEMORYの巻<BR>・やってきた春の巻<BR>・香ちゃんの初デートの巻<BR>・危険なふたり！の巻<BR>・許してくれ…の巻<BR>・墓地のナンパニストの巻</TD><TD ALIGN="CENTER">1988年12月11日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第18巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>海坊主にゾッコン!!の巻</TD><TD ALIGN="CENTER">4-08-852398-9</TD></TR>
<TR><TD>・あねさんは女子大生！の巻<BR>・悲しき未亡人の巻<BR>・花火をあげろ！盛大に!!の巻<BR>・白いキャンバスの巻<BR>・海坊主にゾッコン!!の巻<BR>・史上最大の作戦！の巻<BR>・海坊主の愛情の巻</TD><TD ALIGN="CENTER">1989年2月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第19巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>哀しい天使の巻</TD><TD ALIGN="CENTER">4-08-852399-7</TD></TR>
<TR><TD>・海坊主が愛した女(ひと)の巻<BR>・哀しい天使の巻<BR>・すねた紗羅ちゃんの巻<BR>・勇気をください！の巻<BR>・再び空飛ぶオシリ！の巻<BR>・人質になったもっこりの巻</TD><TD ALIGN="CENTER">1989年4月15日<BR>370円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第20巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>さよならの向こう側の巻</TD><TD ALIGN="CENTER">4-08-852400-4</TD></TR>
<TR><TD>・泥棒勝負開始!!の巻<BR>・鏡の秘密の巻<BR>・憂いのMy Sisterの巻<BR>・姉妹のきずなの巻<BR>・指輪に秘めた夢の巻<BR>・リョウさん、出番です!!の巻<BR>・さよならの向こう側…の巻</TD><TD ALIGN="CENTER">1989年6月15日<BR>370円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第21巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>ビル街のコールサインの巻</TD><TD ALIGN="CENTER">4-08-852612-0</TD></TR>
<TR><TD>・ビル街のコールサインの巻<BR>・夢の中の憧れの男(ひと)の巻<BR>・セーラー服パニック！の巻<BR>・プロポーズ狂想曲！？の巻<BR>・嫌い帰れも好きのうち！？の巻</TD><TD ALIGN="CENTER">1989年8月15日<BR>370円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第22巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>お嬢さんにパイソンを！の巻</TD><TD ALIGN="CENTER">4-08-852613-9</TD></TR>
<TR><TD>・怒りのマグナム！の巻<BR>・17年目のおばあちゃんの巻<BR>・お嬢さんにパイソンを！の巻<BR>・天下無敵の女探偵誕生！？の巻<BR>・ふりかえったO・SHI・RIの巻</TD><TD ALIGN="CENTER">1989年10月15日<BR>370円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第23巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>明日へのリバイバルの巻</TD><TD ALIGN="CENTER">4-08-852614-7</TD></TR>
<TR><TD>・涙のバースデーの巻<BR>・女心を知るものは…の巻<BR>・愛は銃よりも強く！の巻<BR>・明日へのリバイバルの巻<BR>・飛ぶのが怖い！？の巻<BR>・飛べないスイーパーの巻</TD><TD ALIGN="CENTER">1989年12月10日<BR>370円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第24巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>大空の告白の巻</TD><TD ALIGN="CENTER">4-08-852615-5</TD></TR>
<TR><TD>・Fright,Flight(恐怖の飛行)の巻<BR>・エアポート'89の巻<BR>・大空の告白の巻<BR>・天使の落としもの！？の巻<BR>・憧れのうしろ姿の巻<BR>・香、努力する!!の巻<BR>・決闘！蝙蝠V.S.リョウ!!の巻<BR>・背中にさよなら!!の巻</TD><TD ALIGN="CENTER">1990年2月15日<BR>370円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第25巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>Play it again, Mami! -あの曲をもう一度-の巻</TD><TD ALIGN="CENTER">4-08-852616-3</TD></TR>
<TR><TD>・依頼人は幽霊！？の巻<BR>・ふたつの顔をもつ女！？の巻<BR>・暗闇でドッキリ・デートの巻<BR>・鏡の中のもうひとりの私！？の巻<BR>・炎の中のピアニスト!!の巻<BR>・Play it again, Mami! -あの曲をもう一度-の巻<BR>・美女と野獣！？の巻<BR>・港の決闘!!の巻</TD><TD ALIGN="CENTER">1990年4月15日<BR>370円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第26巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>突然の出会い!!の巻</TD><TD ALIGN="CENTER">4-08-852617-1</TD></TR>
<TR><TD>・涙の街頭募金の巻<BR>・突然の出会い!!の巻<BR>・引き裂かれた心の巻<BR>・強く、堅い絆の巻<BR>・マイ・フェア・リョウちゃん！？の巻</TD><TD ALIGN="CENTER">1990年6月15日<BR>370円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第27巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>都会のシンデレラの巻</TD><TD ALIGN="CENTER">4-08-852618-X</TD></TR>
<TR><TD>・香が水着に着がえたら！？の巻<BR>・美しき標的!!の巻<BR>・コートの秘密！？の巻<BR>・華麗なる脱出!!の巻<BR>・都会のシンデレラ!!の巻-前編-<BR>・都会のシンデレラ!!の巻-後編-<BR>・殺したい男!!の巻<BR>・暁の決断!!の巻</TD><TD ALIGN="CENTER">1990年8月15日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第28巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>勝敗の行方!!の巻</TD><TD ALIGN="CENTER">4-08-852619-8</TD></TR>
<TR><TD>・過去の傷跡の巻<BR>・賽は投げられた!!の巻<BR>・勝敗の行方!!の巻<BR>・賢者の贈り物!!の巻<BR>・祖父、現る！？の巻<BR>・狙われたリョウの許嫁！？の巻<BR>・誘拐された<FONT SIZE="-2">じいさん付き</FONT>下着!!の巻</TD><TD ALIGN="CENTER">1990年10月15日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第29巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>伊集院隼人氏平穏な一日の巻</TD><TD ALIGN="CENTER">4-08-852620-1</TD></TR>
<TR><TD>・卑劣な罠(トラップ)！？の巻<BR>・ふたりのシティーハンターの巻<BR>・伊集院隼人氏の平穏な一日の巻<BR>・冴子のお見合い!!の巻<BR>・アニキの残像!!の巻<BR>・変えられた台本(シナリオ）!!の巻<BR>・命懸けのパートナーの巻</TD><TD ALIGN="CENTER">1990年12月9日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第30巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>思い出を消して…の巻</TD><TD ALIGN="CENTER">4-08-852191-9</TD></TR>
<TR><TD>・失われた過去!!の巻<BR>・死を呼ぶ暗示!!の巻<BR>・思い出を消して…の巻<BR>・ハートマークの逃がし屋！？の巻<BR>・恐怖の運送!!の巻<BR>・暗号を歌う女！？の巻</TD><TD ALIGN="CENTER">1991年2月15日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第31巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>ふたりでひとりの心!!の巻</TD><TD ALIGN="CENTER">4-08-852192-7</TD></TR>
<TR><TD>・あきれた大逃走!!の巻<BR>・あなたをパートナーに!!の巻<BR>・リョウと恐るべき似た者姉妹!!の巻<BR>・父、来襲す!!の巻<BR>・卑怯者!!の巻<BR>・ふたりでひとりの心!!の巻<BR>・もっこり十発の陰謀！？の巻<BR>・消えたもっこり!!の巻</TD><TD ALIGN="CENTER">1991年5月15日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第32巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>おかしなふたり!!の巻</TD><TD ALIGN="CENTER">4-08-852193-5</TD></TR>
<TR><TD>・おかしなふたり!!の巻<BR>・写真を巡る思い出!!の巻</TD><TD ALIGN="CENTER">1991年8月12日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第33巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>地獄への出航!!の巻</TD><TD ALIGN="CENTER">4-08-852194-3</TD></TR>
<TR><TD>・悲しき旅立ち!!の巻<BR>・涙のペンダントの巻<BR>・嵐の前…の巻<BR>・地獄への出航!!の巻<BR>・愛と憎しみと…!!の巻<BR>・ペンダントの記憶!!の巻</TD><TD ALIGN="CENTER">1991年12月8日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第34巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>にせＣ・Ｈ(シティーハンター)登場!!の巻</TD><TD ALIGN="CENTER">4-08-852195-1</TD></TR>
<TR><TD>・息子よ!! の巻<BR>・地獄からの生還!!の巻<BR>・ヒーローの怪我！？の巻<BR>・ワンワンスイーパー!!の巻<BR>・にせＣ・Ｈ(シティーハンター)登場!!の巻</TD><TD ALIGN="CENTER">1992年2月15日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）第35巻</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>FOREVER, CITY HUNTER!!の巻</TD><TD ALIGN="CENTER">4-08-852196-X</TD></TR>
<TR><TD>・かみあうふたり!!の巻<BR>・素直な気持ちに!!の巻<BR>・ウェディング・ベル！の巻<BR>・FOREVER, CITY HUNTER!!の巻</TD><TD ALIGN="CENTER">1992年4月15日<BR>390円</TD></TR>
</TABLE><BR><BR>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">天使の贈りもの</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>北条司短編集1</TD><TD ALIGN="CENTER">4-08-871268-4</TD></TR>
<TR><TD>・天使の贈りもの<BR>・おれは男だ！<BR>・ネコまんまおかわり<BR>・シティーハンター -XYZ-<BR>・シティーハンター -ダブル-エッジ-</TD><TD ALIGN="CENTER">1988年11月15日<BR>360円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">↑尽管设定不同，但西九条紗羅和松村渚出现了。 第Saeba公寓和獠也出现了。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">桜の花咲くころ</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>北条司短編集2</TD><TD ALIGN="CENTER">4-08-871340-0</TD></TR>
<TR><TD>・桜の花 咲くころ<BR>・ファミリー・プロット<BR>・TAXI DRIVER<BR>・少女の季節 -サマードリーム-</TD><TD ALIGN="CENTER">1993年5月15日<BR>390円</TD></TR>
</TABLE><BR><BR>


<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">这个系列的主人公是一个拥有神秘力量的女孩，西九条紗羅。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">こもれ陽の下で…第1幕</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>少女と木の精</TD><TD ALIGN="CENTER">4-08-871355-9</TD></TR>
<TR><TD>・第一場　少女と木の精<BR>・第二場　七年前の少女<BR>・第三場　偲び花<BR>・第四場　心の花<BR>・第五場　呪いの樹<BR>・第六場　哀しき大樹<BR>・第七場　花がとりもつ縁</TD><TD ALIGN="CENTER">1994年3月9日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">こもれ陽の下で…第2幕</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>秋の陽のデジャヴー</TD><TD ALIGN="CENTER">4-08-871356-7</TD></TR>
<TR><TD>・第八場　初恋 -幸福の光る花-<BR>・第九場　小さな大冒険<BR>・第十場　光 満ちあふれて<BR>・第十一場　秋の陽のデジャヴー<BR>・第十二場　スパイ大作戦!!<BR>・第十三場　思い出つくり<BR>・第十四場　何時か何処かで<BR>・第十五場　BODY JACKER<BR>・第十六場　Night crawler<BR>・第十七場　CRESCENDO -クレッシェンド-</TD><TD ALIGN="CENTER">1994年5月7日<BR>390円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">こもれ陽の下で…第3幕</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>Heart and Soul</TD><TD ALIGN="CENTER">4-08-871357-5</TD></TR>
<TR><TD>・第十八場　Bad Soul<BR>・第十九場　Heart and Soul<BR>・第二十場　嵐のあと<BR>・第二十一場　コルチカムの咲く家<BR>・第二十二場　冬の嵐 with LOVE<BR>・第二十三場　友情の木<BR>・第二十四場　いいだせなくて…<BR>・第二十五場　-終幕-</TD><TD ALIGN="CENTER">1994年7月9日<BR>390円</TD></TR>
</TABLE>

---

---

**[** [戻る](./book.md) **]**