http://www.netlaputa.ne.jp/~arcadia/cocktail/xyzbox.html

**XYZ BOX**（→2002年11月30日为止）

---

新宿站东口「MY CITY」6楼的「山下書店」，「XYZ BOX」登场。  
请CH(獠和香莹)解决的独特的委托、有趣的委托正在受理中！！  
  
委托的方式是填写所提供的问卷卡（明信片），并将其投进BOX。  
※卡片图案是以Bunch的第22/23号合并特刊封面为蓝本  
优秀的委托被作品采用，还有亲笔签名的单行本礼物！！  
  

![](../ah/image/xyzbox01.jpg)  
MY CITY的6楼。獠和香莹的招牌吸引了人们的注意。  
  
  
![](../ah/image/xyzbox02.jpg)  
这就是传闻中的「XYZ留言板BOX」。  
  
  
![](../ah/image/xyzbox03.jpg)  
企画的启动是从AH4卷开始的。 时间延长到11月底。  
  
  
![](../ah/image/xyzbox04.jpg)  
有趣的委托创意可以反映在作品中！？  
或者，可以在「Comic Bunch」杂志上介绍！？  
  
![](../ah/image/xyzbox05.jpg)  
问卷调查的卡片，企画的原始明信片！！  
  
  
![](../ah/image/xyzbox06.jpg)  
似乎不乏有美女要求委托!  
(为了保护隐私，有些照片被打了马赛克^^;)  
  

※当然，这只是一个有趣的企画，所以「不是对CITY HUNTER的正式委托」。  

---

ver.020923

**[** Back **]**