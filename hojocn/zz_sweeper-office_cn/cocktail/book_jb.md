http://www.netlaputa.ne.jp/~arcadia/cocktail/book_jb.html

**CITY HUNTER 関連書籍**

---

> JUMP j BOOKS

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）</FONT></TD><TD ALIGN="CENTER">北条司・外池省二</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-703005-9</TD></TR>
<TR><TD>・SuperComic・Prologue<BR>・真実への回線 The access to the truth<BR>・明日ある復讐 The revenge for tomorrow<BR>・薔薇と拳銃 A gun and a rose</TD><TD ALIGN="CENTER">1993年4月30日<BR>760円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER SPECIAL（シティーハンタースペシャル）</FONT></TD><TD ALIGN="CENTER">北条司・天羽沙夜</TD></TR>
<TR><TD>The Secret Service（ザ・シークレット・サービス）</TD><TD ALIGN="CENTER">4-08-703042-3</TD></TR>
<TR><TD>・City Hunter SPECIAL<BR>・あとがき</TD><TD ALIGN="CENTER">1995年12月20日<BR>760円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）2</FONT></TD><TD ALIGN="CENTER">北条司・稲葉稔</TD></TR>
<TR><TD>OTORI</TD><TD ALIGN="CENTER">4-08-703058-X</TD></TR>
<TR><TD>・囮<BR>・罠</TD><TD ALIGN="CENTER">1997年4月29日<BR>780円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER SPECIAL（シティーハンタースペシャル）2</FONT></TD><TD ALIGN="CENTER">北条司・岸間信明</TD></TR>
<TR><TD>緊急生中継！？凶悪犯冴羽リョウの最期</TD><TD ALIGN="CENTER">4-08-703082-2</TD></TR>
<TR><TD>・CITY HUNTER SPECIAL2<BR>・あとがき</TD><TD ALIGN="CENTER">1999年4月7日<BR>780円</TD></TR>
</TABLE>

---

---

**[** [戻る](./book.md) **]**