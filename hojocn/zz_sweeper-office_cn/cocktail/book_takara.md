http://www.netlaputa.ne.jp/~arcadia/cocktail/book_takara.html

**CITY HUNTER 関連書籍**

---

> 別冊宝島

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">City Hunter最強読本</FONT></TD><TD ALIGN="CENTER">別冊宝島862</TD></TR>
<TR><TD>TV series 全140話＋ special 版全6話 digest </TD><TD ALIGN="CENTER">4-7966-3504-1（雑誌65996-15）</TD></TR>
<TR><TD>＜巻頭 color 特集＞<BR>●City Hunter名 scene 集<BR>ACTION<BR>LOVE<BR>MOKKORI<BR><BR>●主要 character 紹介<BR>冴羽リョウ<BR>槇村香<BR>槇村秀幸<BR>野上冴子<BR>野上麗香<BR>ファルコン（海坊主）<BR>美樹<BR>麻生かすみ<BR>●特選!　想い出のヒロイン<BR>● special イラスト集<BR><BR>City Hunter1＜全51話＞<BR>City Hunter2＜全63話＞<BR>City Hunter3＜全13話＞<BR>City Hunter'91＜全13話＞<BR>＜巻末 special ＞劇場版＆ special 版全6作品徹底紹介<BR><BR>・INTERVIEW<BR>監督　こだま兼嗣<BR>Producer 　植田益朗<BR>作画監督　北原健雄<BR><BR>・Column<BR>美女よりお子ちゃまに大モテなヒーロー!?<BR> Anime とテーマ曲のコラボレーションだ!<BR>『City Hunter』を飾るゲストキャラたち<BR>まるで夫婦漫才!?　ハンマーが結ぶ２人の恋物語<BR>あらためて見よう。 Anime 『City Hunter』の歩み<BR><BR>●新宿MAP<BR>●『City Hunter』の名銃たち<BR>●Staff Credit一覧</TD><TD ALIGN="CENTER">2003年9月28日<BR>952円＋税</TD></TR>
</TABLE>

---

---

**[** [戻る](./book.md) **]**