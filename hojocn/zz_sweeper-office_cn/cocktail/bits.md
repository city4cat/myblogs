http://www.netlaputa.ne.jp/~arcadia/cocktail/bits.html

**CITY HUNTER 小知识**

---

CITY HUNTER相关的小知识。  
  
  

> ★CITY HUNTER的4格漫画  
>   
> 　周刊少年 Jump经常在黄金周和年底组织特别活动，出版4格连载的漫画。CITY HUNTER也有几个4格系列。  
>   
> ・「夏休みスイカ割り大会CITY HUNTERチーム編」（译注：暑假劈西瓜比赛CITY HUNTERteam篇）  
> 　<font color=#ff0000>獠蒙住自己的眼睛，尝试着用手劈开西瓜。だがリョウは、スイカの代わりに！？</font> （译注：待校对）   
>   
> ・「X'masのパノラマリョウちゃん」（译注：X'mas之全景獠）  
> 　<font color=#ff0000>獠试图用流行的全景相机拍摄的东西・・・！？</font>  
>   
> ・「それぞれのゴールデンウィーク」（译注：每个Golden Week）  
> 　<font color=#ff0000>如何度过每一个GW--海坊主和美樹的情况，伢子和とーちゃん的情况，槇村和ミック的情况，一如既往的两个人的情况，漫画家的情况。</font>...etc.  
>   
>   
> ★（源自jump novel Vol.1）北条先生选择的书籍　BEST 5 作品  
>   
> ・Flowers for Algernon（Daniel Keyes著）  
> ・人はなぜ助平になったか（戸川幸夫著）  
> ・Spencer Series（Robert B. Parker著）（译注：中文名可能是《斯宾塞系列》）  
> ・The End of Childhood (Arthur C. Clarke著）  
> ・七瀬シリーズ（家族八景、七瀬ふたたび、エディプスの恋人）（筒井康隆著）（译注：七瀬系列（家族八景、又见七瀬、Oedipus的恋人））  
>   
> 　※排名并非如此  
>   
> <font color=#ff0000>（简单解说：Spencer系列的主人公是一个硬汉私家侦探。他很时尚，是个专业厨师。他的对手和朋友Hawk是一个大块头的黑人光头。两人之间的互动也相当轻松和时尚。 七濑系列的主人公是一个天生具有心灵感应能力的女子。）</font>

---

---

**[** [Office](../index.html) **]**