http://www.netlaputa.ne.jp/~arcadia/cocktail/info.html

# CH最新信息 2010年10月24日版

---

獠：「你有什么好的mokkori视频吗？」  
源さん：「…スキだねぇ～獠也是…」 （译注：待校对）  
  
[ [应季的信息](./info.md#旬) | [Schedule](./info.md#スケジュール) | [番外編](./info.md#番外編)11年02月近日up予定 | [记录](./info.md#記録) ]  
  
  
<a name="旬"></a>
## 应季的信息！（译注：待校对）

  
### ●2010年10月25日(月)  
Coamix出版了**『月刊 COMIC ZENON (ComicZenon)』**的新创刊号。  
官网… http://www.comic-zenon.jp/  
**『Angel Heart』2nd Season**在<font color=#ff0000>COMIC ZENON连载开始！！</font>  
  
定价为650日元（创刊号为800日元），可从德间书店购买。  
  
◆在25日的杂志创刊号上，该活动是一个大事件（10.23~10.25）  
→23日、24日，创刊0号和传单将在Café Zenon和Book Ruhe商店（吉祥寺）分发。    
・獠 & 香莹的真人大小的pop-ups也提前展示    
→在25日这一天…  
・在Café Zenon的所有食物和饮料都是半价!    
・在Café Zenon购买月刊Zenon，所有商品可享受70%的折扣（前30名顾客）  
・在Book Ruhe购买Zenon月刊时，可免费获得高级限量版嘎查（如AH限量版锡章） (译注：待校对)   
  
◆「Cat's Eye Special Cocktails」现在可以在Café Zenon买到(10.25～)  
→北条先生参加了品酒会! 三款鸡尾酒的灵感来自于愛、瞳、泪的形象（爱的版本不含酒精）  
・数量有限，你可以得到一个Cat's Card杯垫（背面可以作为通告信）。  
・在柜台点餐时，食品和饮料可享受20%的折扣。  
・Cat's Card形状的Café Zenon邮票卡现已上市。  
  
◆Free paper，免费发放Zenon创刊０号(10.10～)  
→对AH 1st Season的摘要回顾。 还刊登了创刊2号的附录。（译注：待校对）  
◎分发地址  
・清洲城的歌舞伎节（在清洲城活动中限量发行5000份。10.10）  
・2010年名古屋节（名古屋市长、原哲夫和堀江信彦的三人谈。10.16）  
・原哲夫「戦国傾奇個展」（名古屋荣三越。 10.13～10.18）  
・吉祥寺Café Zenon(10.10～)  
・在Zenon官方Twitter上申请的杂志monitor(10.12～10.14)  
・涩谷Q-front 7F「TSUTAYA 涩谷店」(同时展出AH复制原画。10.13～11.05)  
・渋谷Ｑ-front 6F「WIRED CAFE」(10.13～)  
・吉祥寺Books Ruhe店面(10.23～)  
・iPad、iPhone App（App Store免费下载。10.14～）  
  
※Comic Zenon也将作为一个基于商店的应用程序提供（但不包含增刊）  
  
◆「去战斗！ Pants」以外的创刊号附录还包括「漢(おとこ)の安心２大保証」（连续2期、3期）（译注：待校对）  
・獠和健次郎将保护我们所有的读者!  
→如果你购买了Zenon并在手机上免费注册，在发生紧急情况时，你将获得高达10万日元的慰问金。  
・獠解决了「跟踪狂受害」和「个人信息泄露」，「汉与女的纠纷保证」（译注：待校对）  
  
◆创刊2号的附录有「北条司mini画集」  
→另外还有北条司致敬画册第3弹  
  
▽参加人士  
村田雄介、久保ミツロウ、大島司、佐原ミズ、小野洋一郎、ヤマザキマリ（敬称略）  
  
◆Comic Zenon编辑部blog启动(10.01～)  
→AH 2nd Season 第1话卷中的一个彩页的掠影。  
AH是「北条司出道30周年的巅峰之作！」  
  
◆手机网站「Zenon Land」的初步开放  
→「AHきせかえ」作为创刊纪念礼物，在有限时间内赠送给所有注册用户(10.25～)  
  
◆Zenon Selection 至AH第4卷（计划22卷）  
・第4卷收录第49话～65话(10.22)  
→附有2nd Season的特制海报  
→ZENON创刊号连动！有AH 四卡的参赛表格（译注：待校对）    
  
◆「北条司 Ｘ いってん」致敬review展，决定举办!(渋谷LOFT。11.01～11.19）  
・北条司30周年纪念 & Comic Zenon创刊纪念的致敬review展!  
→Café Zenon和POPBOX（=由FEWMANY制作的creator bazaar）之间的联系项目。  
※「いってん」是FEWMANY的一个双月展。 
  
▽参加的艺术家  
彩、EVILROBOTS、emico、オオタニヨシミ、久保田ミホ、小石川ユキ、こなつ、sioux、白玉、  
末吉陽子、ソネアユミ、ソノベナミコ、タケヤマ・ノリヤ、チバサトコ、てりィ、東京Aリス、  
Tokyo Guns、NICO、feebee、mamico、ミキワカコ、ミヤモトヨシコ、Himiko Rose、mikish、  
宮川雄一、照紗、yucachin、Benicco、ヨシカワナオヒデ、ワカマツカオリ（敬称略）  
  
### ●2010年10月02日(土)  
**『Angel Heart』 活动舞台**、吉祥寺Theater举办！  
  
**「ANGEL HEART ～向着展翅的人～」**  
→仅限1天! 电影和舞蹈合作  
  
▽出演者  
杉本有美　坂崎愛　市道真央　鈴木つかさ　高柳将太　福島慎之介　高瀬秀芳　鍵山由佳　及川奈央（敬称略）  
  
北条司 作家生活30周年記念Project  
 在官方的网站上 UP  
http://www.hojo-tsukasa.com/index.php  
  
### ●2010年09月29(水)  
globe best album**『15YEARS -BEST HIT SELECTION-』**发售!  
▽这张best album中收录了City Hunter的ED theme「Get Wild」的cover。  
  
→Cover design是北条司先生  
chikka提供了[详细信息](./globe_15years_best.md)。谢谢m(__)m  
  
### ●2010年09月28日(火)～10月11日(月)  
**「北条司30周年 meets atre吉祥寺」**  
北条司30周年与atre吉祥寺开业的合作活动举办！  
  
▽**「Angel Heart」Dance Performance event **  
→为了纪念个展和舞台的举办，迎接Angel Heart dance team、杉本有美、及川奈央的示范表演。  
时间：9月28日(火)17:00～ 19:00～  
地点：atre吉祥寺、ゆらぎの広場  
  
▽**北条司「Angel Heart」個展**  
→展示所有AH Comics封面复制原画。  
期間：9月29日(水)～10月11日(月)まで  
場所：atre吉祥寺、ゆらぎの広場  
  
▽**「Stamp Rally 2010」「吉祥寺限定AH Gacha」**  
Stamp Rally→收集并应征，就会抽取老师亲笔签名的复制原画！   
GachaGacha → 吉祥寺限定的AH徽章33种。  
时间：9月29日(水)～10月11日(月)  
地点：atre吉祥寺ゆらぎの広場、CAFE ZENON、OLD CROW、FUNKY、LEMON DROP这4家商店。  
  
上述详情请参见… http://www.hojo-tsukasa.com/2010/09/kichijoji2010.html#more  
  
### ●2010年09月24日(金)  
<font color=#ff0000>Zenon Selection </font>**『Angel Heart』[第2巻](./book_bc.md#z02)** 发售  
→定价480日元（含税）  
▽1st Season 重新收录的便利店版Comics  
→毎月第2、第4个周五发售  
  
### ●2010年09月11日(土)～11月07日(日)  
在北条先生的母校九州产业大学的美術館举办了10位校友漫画家的原画展!  
老师历代连载作品的原画多数参展！  
  
**50周年纪念事业特别展  毕业生--专业人士的世界─vol.2  
キュウサン☆Manga☆Chronicle**（译注：待校对）  
  
会期：2010年09月11日(土)～11月07日(日)  
時間：10:00～17:30(入场到17:15为止)  
→入场费：一般200日元、学生100日元  
※周一闭馆（11月1日开放）  
  
▽相关事宜   
Coamix社長 堀江信彦的讲座「与北条司的30年」  
举办时间：2010年09月25日(土)14：00～  
举办地点：15号馆2层15201号教室  
  
九州産業大学美術館 官方网站  
http://www.kyusan-u.ac.jp/ksumuseum/  
  
九州産業大学美術館Web日誌  
http://blog.livedoor.jp/ksumuseum/archives/cat_50046866.html  
  
### ●2010年09月10日(金)  
<font color=#ff0000>Zenon Selection </font>**『Angel Heart』[第1巻](./book_bc.md#z01)** 发售  
→定价480日元（含税）  
▽1st Season 重新收录的便利店版漫画，现在由Coamix出版!    
→从现在起，每月第2、第4个周五发售。 
  
### ●2010年09月09日(木)  
Bunch Comics**『Angel Heart』[第33巻](./book_bc.md#33)** 发售  
→定价540日元（含税）  
▽封面是Comic Bunch第20号封面的图案  
→1st Season 最終巻  
  
### ●2010年08月27日(金)  
**『週刊Comic Bunch』**从今天停止出版。 →该杂志将于2011年1月21日作为月刊Comic『＠Bunch』重新发行。  
**『Angel Heart』2nd Season**预定连载的**『月刊Comic Zenon』**于2010年10月25日发表新创刊。  
  
纸质杂志『Comic Zenon』、空间杂志**『Cafe Zennon』**(武藏野市吉祥寺)、电子杂志**『ZENONLAND』**(手机网站=预定)合作的三位一体的娱乐。  
主编是Coamix社长堀江信彦。杂志的theme写道「倾」  
  
Coamix2010年8月27日发布的News  
http://www.coamix.co.jp/2010/08/zenon0827.html  
  
### ●2010年08月20日(金)  
信息解禁Day。『週刊Comic Bunch』将于下周在杂志上通知停刊。  
网上透露，**『Angel Heart』**将转移到新杂志**『Comic Zenon』**，由Coamix推出。  
  
▽官方预告视频  
「Angel Heart移动到Comic Zenon（自画像ver）」  
http://www.youtube.com/watch?v=vZ_G9WUjTCk  
「Angel Heart移动到Comic Zenon（公告板ver）」  
http://www.youtube.com/watch?v=9EbF2yDQOMg  
  
参考：Comic Zenon官方视频集  
http://www.youtube.com/user/COMICZENON  
  
### ●2010年08月13日(金)・14日(土)  
**「吉祥寺Charity Auction」**举办！  
以「Art之街・吉祥寺」为关键词，与街道相关的艺术家合作举办了Charity Auction。  
  
▽北条司先生的展出作品  
・「Angel Heart Giclee（高精細复制原画）签名」  
・「France Japan Expro 2010官方小册子<非卖品>签名彩纸」  
  
举办时间：8月13日(金)・14日(土) 每天10:00～17:00  
举办地点：武蔵野商工会館4楼「Zero One Hall」（JR吉祥寺站中央出口，步行5分钟）
→免费入场，进出自由  
  
查询：产经Living新闻社 TEL0422(21)2531  
 详情在官方网站上  
http://www.lcomi.ne.jp/tie-up/t_10charity/  
  
### ●2010年08月06日(金)  
**『Angel Heart』1st Season**、『週刊Comic Bunch』的最終回！  
2nd Season信息在北条司官网和官方Twiter上。  
  
此外，Coamix新杂志**『Comic Zenon』**的官方网站预开放&官方Twitter启动！  
▽10月25日0時倒计时  
  
http://www.comic-zenon.jp/  
http://twitter.com/zenon_official/  
  
新潮社漫画事业部也正式启动Twitter！  
『Angel Heart』Comics新刊信息在这里。  
http://twitter.com/Comic_Shincho  
  
### ●2010年08月01日(日)  
**「北条司 OFFICIAL WEB SITE」**renewal！  
http://www.hojo-tsukasa.com/index.php  
  
### ●2010年07月05日(月)  
作为SNS「GREE」向的社交游戏的第一弹，株式会社Enters Pia开始提供**「City Hunter 100万人的Swepper」**(6月29日起)   
http://entersphere.co.jp/press/press_release_2010_07_05.html  
  
・用户成为初出茅庐的Swepper、解决各种事件的游戏。  
・与獠、香、海坊主和冴子为搭档，在City Hunter的城市中以swepper的身份生活。  
・如果你赢了其他用户，你在其世界的名气就会增加，会收到美女的委托。  
・还可以召集朋友，享受夜生活。  
・基本的游戏是免费的，道具是收费的。  
  
▽支持的设备  
au：　与WIN兼容的型号  
NTT Docomo：　90Xi Series、70Xi Series、STYLE Series、PRIME Series、SMART Series、PRO Series中的SH终端  
SoftBank：　与movie shamail兼容的3G机型(※X Series、iPhone 3G不可用)
  
→需要在GREE注册会员  
http://mpf.gree.jp/42  
  
### ●2010年07月01日(木)～04日(日)  
France**「Japan Expo 2010」**，北条司先生参加！  
→此事在@hojo_official的官方Twitter和官方移动网站「Mobile Bunch」上 
  
▽嘉宾包括动画CH的导演児玉兼嗣  
  
Japan Expo URL  
http://www.japan-expo.com/  
  
### ●2010年06月29日(火)  
**北条司's Official** Twitter Start @hojo_official  
http://twitter.com/hojo_official  
  
### ●2010年06月25日(金)  
Coamix株式会社其『週刊Comic Bunch』编辑业务与新潮社的合同期满，<font color=#ff0000>将在8月27日发售号上结束</font>。  

▽借此机会，Coamix将在今年秋天推出一本新的杂志，由北条司、原哲夫、次原隆二、一个新鲜而有前途的创作团队，以及株式会社North Stars Pictures。  
▽「Comic Bunch」也将继续由新潮社内部编辑，并将于今年晚些时候重新推出。  
  
向各位读者致意（译注：待校对）  
http://www.coamix.co.jp/2010/06/comicbunch.html  
  
### ●2010年05月14日(金)～<s>27日(木)</s>06月3日(木)  
**「北条司最新彩色原画＆tribute pinup展」**举办  
  
▽第1弾、第2弾Pin-up & Angel Heart最新插画展！！  
  
举办时间：5月14日(金)起<s>2周</s>（※21日(金)、23日(土)的17時后除外）→6月3日(木)为止（因好评而延长期限）  
举办地点：吉祥寺「CAFE ZENON」Loft space 
→免费入场  
  
 详见Bunch或Café Zenon的官方网站  
http://www.comicbunch.com/news_release/hojo-exhibition2.php  
http://cafe-zenon.jp/news/?p=526  
  
### ●2010年03月09日(火)  
Bunch Comics**『Angel Heart』[第32巻](./book_bc.md#32)** 发售  
→定价540日元（含税）  
▽封面是Comic Bunch'09年第39号封面的图案  
  
### ●2009年12月24日(木)～12月<s>27日(日)</s>30日(水)  
**「北条司和朋友们Surprise原画展」**  
作为出道30周年联动企划紧急举办。  
举办时间：12月24日(木)17:00から、<s>直到27日（周日）25:00的4天</s>→至1月30日（周三）15:00为止（因好评而延长期限）  
举办地点：吉祥寺「CAFE ZENON」的2楼特别展览floor  
→免费入场  
  
▽除了北条刚的精华原画，还有一个特别展览，是由Comic Bunch 「北条司tribute pinup」企划的著名艺术家阵容新绘制的原画特别展览。 
→携带Comic Bunch 2010新年4/5合并版的前200名观众将获赠北条司的特别postcard present。  
  
 详见Bunch或Café Zenon的官方网站  
http://www.comicbunch.com/news_release/hojo-exhibition.php  
http://cafe-zenon.jp/news/?p=240  
  
### ●2009年11月09日(月)  
Bunch Comics**『Angel Heart』[第31巻](./book_bc.md#31)** 发售  
→定价540日元（含税）  
▽封面是Comic Bunch第14号封面的图案  
  
### ●2009年08月21日(金)  
汽车Mini的专业杂志**『MINI freak(ミニ・フリーク)』**No.108号（2009年10月号） 发售  
→Natsume社・定价1300日元（含税）  
▽特集「对你来说，Mini是什么？」中獠登場  
→「我问了CityHunter的冴羽獠！」  
▽附带北条先生的comment  
  
### ●2009年07月09日(木)  
Bunch Comics**『Angel Heart』[第30巻](./book_bc.md#30)** 发售  
→定价540日元（含税）  
▽封面是Comic Bunch新年3号封面的图案  
→初次限定特典！带纪念明信片(共3种)  
  
### ●2009年03月09日(月)  
Bunch Comics**『Angel Heart』[第29巻](./book_bc.md#29)** 发售  
→定价540日元（含税）  
▽封面是Comic Bunch'08年第43号封面的图案  
  
### ●2008年12月09日(火)  
Bunch Comics**『Angel Heart』[第28巻](./book_bc.md#28)** 发售  
→定价540日元（含税）  
▽封面是Comic Bunch第39号扉页的图案  
  
### ●2008年09月09日(火)  
Bunch Comics**『Angel Heart』[第27巻](./book_bc.md#27)** 发售  
→定价540日元（含税）  
▽封面是Comic Bunch第32号 封面的图案  
  
### ●2008年05月16日(金)  
Bunch Comics**『Angel Heart』[公式Guidebook](./book_bc.md#GB)** 发售  
→定价700日元（含税）  
▽封面是Comic Bunch第23号 封面的图案  
  
・ Episode guide（ Episode 34話収録）  
・Character Guide（对所有124个人物的完整介绍）
・北条先生 long Interview  
・夢の鼎談、北条司×井上雄彦×梅澤春人  
・合作，北条司创作的漫画「A・H」× 大沢在昌写的小说「新宿鮫」  
etc...  
  
### ●2008年05月16日(金)～05月18日(日)  
「Angel Heart」Comics累計1500万部突破記念Art Gallery、  
**『Memory of X・Y・Z』**于5/15～21之间、JR新宿站举行。  
  
举办时间：5月16日～18日、各日13:00～20:00  
举办地点：JR新宿站 Alps广场  
［电照板张贴期间:5月15日(木)～21日(水)］  
  
活动内容  
1.新宿站XYZ留言板的复活　→通过电照板恢复XYZ留言板。可以委托CH。  
2.Mini Cooper＆1500万t Hammer 巨大模型的展示→根据为活动新绘制的插图制作的巨大模型。  
3.「AH」巨型Comic Gallery→在一张电照板上展示AH的整个情节。  
4.「AH」精华的复制原画的展示→从AH的彩色和单色原稿中复制精华进行展示。（译注：待校对）  
5.特制原画post card present  →Bunch最新号的每日前100名。  
6.特制AH生态袋→向17日、18日到场的前1500名参观者分发。  
  
 详见… http://www.comicbunch.com/AH1500/  
  
### ●2008年05月17日(土)  
**『北条司先生sign会』**決定！  
举办日期和时间：2008年5月17日(土) 13:00～14:00  
举办場所：紀伊國屋新宿本店Forest 2层特設会場  
▽5/9(周五)上午10点起，在新宿本店Forest 2楼漫画柜台，向购买AH26卷的前150名发放编号的门票。  
→仅在同一本书中签名。  
  
### ●2008年05月14日(水)～07月31日(木)17点前接受预订
AH**『Giclee印刷品系列』**限量销售。  
▽所有6个标题都有北条老师的签名和序号。 每本100份。 装框的窗口尺寸为535mm x 420mm。  
→期間限定、1点48000日元(含税)  
要求提供目录… http://www.shinchosha.co.jp/angel  
Bunch Mobile Shop… http://bunch-shop.jp/  
  
### ●2008年05月09日(金)  
Bunch Comics**『Angel Heart』[第26巻](./book_bc.md#26)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch2007年第24号 封面的图案  
  
▽该杂志・Comics联动「AH fan 感恩节」第2弾！  
「AHOriginalピンズ＆专用frame」応募者全員サービス  
「Comics26巻オビの申请券」＋「Comic Bunch23号(5/9发售)、24号(5/16发售)、25号(5/23发售)いずれかの申请用纸」＋「2000日元分の郵便定額小為替」を一組にして応募。  
→截止日期、2008年06月09日(月)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
### ●2008年05月09日(金)～06月09日(月)上午10点为止 
手机Bunch応募者全員無料present！  
▽注册成为会员并使用手机 Bunch Top page的表格申请。  
→你将收到一个特制的AH手机防偷窥贴纸。  
  
### ●2008年02月09日(土)  
Bunch Comics**『Angel Heart』[第25巻](./book_bc.md#25)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch2007年第50号 封面的图案  
  
▽该杂志・Comics联动「AH fan 感恩节」開始  
第一弾、「AH特制镊子5件套装」応募者全員service  
「Comic Bunch10号(2/8发售)、11号(2/15发售)、12号(2/22发售)任何一个申请用纸」＋「Comics25巻腰封的申请券」＋「1000日元的邮政定额小汇」为一组申请。  
→截止日期、2008年03月10日(月)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
### ●2007年12月19日(水)より毎月  
**『City Hunter 単巻DVD』**计划发行 
→全26巻  
  
### ●2007年11月09日(金)  
Bunch Comics**『Angel Heart』[第24巻](./book_bc.md#24)** 发售  
→定价530日元（含税）  
▽封面是封面是Comic Bunch第30号 巻首彩色扉页的图案  
  
▽赠送一张印有北条老师笔迹的个性化纸张present，用于回答调查问卷/可通过电脑或手机进行输入。  
→截止日期、2007年12月10日(木)午夜0時/详见Comics腰封&第202页。  
  
### ●2007年08月09日(木)  
Bunch Comics**『Angel Heart』[第23巻](./book_bc.md#23)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch新年1・2合并特刊 巻首彩色扉页的图案  
  
### ●2007年05月09日(水)  
Bunch Comics**『Angel Heart』[第22巻](./book_bc.md#22)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch新年5・6合并特刊 封面的图案  
  
▽该杂志・Comics联动企划「AH DVD Premium Box Vol.4 present」／抽取15名  
用Comic Bunch第24期（5/11发售）&第25期（5/18发售）的申请券中的任意2张，和Comic第22卷腰封的1张申请券，共计2张作为一组进行申请。  
→截止日期、2007年06月04日(月)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
### ●2007年02月16日(金)～05月01日(火)  
「Angel Heart」×「Yahoo! JAPAN」联动协作企划  
→Yahoo! 动画中、AH単单独的特集page**『Angel Heart  Special 』**OPEN！  
… http://streaming.yahoo.co.jp/special/anime/angelheart/  
→动画AH在互联网上首次出现（免费观看第1話）  
→OP曲、ED曲的免费分发  
  
▽チャリティーオークション超目玉大型企画「原寸大１００t Hammer 」：開始は3月9日(金)から  
▽Yahoo!動画限定！「モーションComics」にAH第1話が登場  
▽北条先生も参加する「AHバトン」  
▽登場人物から質問「AH知恵袋」  
▽原作、Animeからピックアップ「Original壁紙ダウンロード」  
その他、「AH検定」、携帯無料待受キャンペーンなどetc..  
  
### ●2007年01月09日(火)  
Bunch Comics**『Angel Heart』[第21巻](./book_bc.md#21)** 发售  
→定价・通常版530日元、包含特制腰封的初版限量版780元（含税）  
▽封面是Comic Bunch'06年第47号封面的图案  
→两个最重要的腰封是「しら乌鸦」&「100t Hammer」（译注：待校对）  
  
▽该杂志・Comics联动企划「AH DVD Premium Box Vol.3 present」／抽取15名  
Comic Bunch5・6合并号(12/28发售)＆7号(1/12发售)＆8号(1/19发售)＆9号(1/26发售)的申请券任选2张、Comics 21巻腰封的申请券1枚、共3张成一组申请。  
→截止日期、2007年02月05日(金)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
### ●2006年09月25日(月)  
动画AH、节目最終回时的present  
・DVD Premium Box Vol.2 (5名)  
・Original・Soundtrack CD (20名)  
  
▽通过手机·PC申请… Access http://angelheart.tv   
→报名截止日期：2006年10月31日(火)  
  
▽通过明信片申请… 〒530-8056 大阪中央郵便局私書箱 1212号  
DVD希望 → 读卖TV「 Angel DVD present」栏目  
CD希望 → 读卖TV「 Angel CD present」栏目  
请将您的姓名、地址、年龄和对节目的反馈发送到相应的地址  
→申请截止日期：2006年10月31日(火)当日邮戳有効  
  
### ●2006年09月13日(水)～10月31日(火)  
动画AH官方手机网站上的「连续3周的问答活动」  
▽测验活动，赢得免费的待机图像和其他节目的礼物  
→回答正确，可获得免费的待机图像present  
→仅限会员，从连续3周正确回答所有问题的人中抽出，由节目组赠送的特別present  
  
▽动画AH官方手机网站・Access方法  
(1)从menu上看  
**NTT DoCoMo**  
i-MENU→menu list→待机画面/i-应用待机/frame→animation/manga/→Angel Heart  
**au**  
top menu→ カテゴリで探す→画像･Character→Anime･Comic→Angel Heart  
**SoftBank Mobile**(vodafone)  
menu list→壁紙・待受→Anime・manga→Angel Heart  
(2)从URL  
http://angelheart.tv  
  
### ●2006年09月09日(土)  
Bunch Comics**『Angel Heart』[第20巻](./book_bc.md#20)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch第34号 封面的图案  
  
### ●2006年06月09日(金)  
Bunch Comics**『Angel Heart』[第19巻](./book_bc.md#19)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch第22・23合并特刊 封面的图案  
  
▽该杂志・Comics联动企划「AH DVD Premium Box Vol.2他 present」／抽取150名  
（DVD15名・AH T-shirt9名・Original Key 环126名）  
Comics腰封的申请券、Comic Bunch28号(6/9发售)＆29号(6/16发售)の申请券、共3张成一组申请。  
→截止日期、2006年06月30日(金)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
### ●2006年03月09日(木)  
Bunch Comics**『Angel Heart』[第18巻](./book_bc.md#18)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch第8号 巻首彩色扉页的图案  
  
▽该杂志・Comics联动企划「AH DVD Premium Box Vol.1 present」／抽取15名  
Comics带的报名券和Comic Bunch15号(3/10发售)＆16号(3/17发售)の申请券，共3张成一组申请。  
→截止日期、2006年03月31日(金)<font color=#ff0000>必须送达</font>。详情见腰封。  
  
### ●2006年<s>03月06日(月)</s>→推迟到4月以后  
动画AH手机网站开放  
→NTT DoCoMo的i-Mode内容(※au、Vodafone将于4月以后依次开设)  
**『Angel Heart』（月額315日元・含税）**  
▽Access方法  
(1) i-MENU→menu list→等待画面/i应用程序等待/frames→animation/manga/→Angel Heart  
(2)http://angelheart.tv  
  
### ●2005年12月09日(金)  
Bunch Comics**『Angel Heart』[第17巻](./book_bc.md#17)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch第21号 封面的图案  
  
### ●2005年11月02日(水)  
TM NETWORK**『GET WILD』**的封面歌曲，由Sony Music发行  
（小室哲也亲自编排了这首歌，它将被转化为一个完整的舞曲）  
▽被称为「Gundam歌姫」的玉置成実演唱 
→价格：1223元（含税）  
（信息由chikka(Eastern Shizuoka)提供。谢谢。m(__)m）  
  
### ●2005年10月03日(月)  
### ●2005年10月04日(火)  
动画**『Angel Heart Angel Heart』**放送開始  
读卖TV→每周一：24時(深夜0時)58分～  
日本TV→每周二：25時(深夜1時)25分～  
  
\#1「ガラスの心臓 グラス・ハート」读卖TV10/03(月)25:34～／日本TV10/04(火)26:25～  
\#2「香が帰ってきた」读卖TV10/10(月)25:28～／日本TV10/11(火)25:49～  
\#3「XYZの街」读卖TV10/17(月)25:13～／日本TV10/18(火)25:34～  
\#4「さまようHEART」读卖TV10/24(月) 25:14～／日本TV10/25(火) 25:25～  
\#5「永別(さよなら) …カオリ」读卖TV10/31(月) 24:59～／日本TV11/01(火) 26:25～  
\#6「再会」读卖TV11/07(月) 24:59～／日本TV11/15(火) 25:40～  
\#7「俺の愛すべき街」读卖TV11/14(月) 25:28～／日本TV11/22(火) 25:55～  
\#8「真実(ホント)の仲間」读卖TV11/21(月) 25:19～／日本TV11/29(火) 25:35～  
\#9「香莹～失われた名前～」读卖TV11/28(月) 25:09～／日本TV12/06(火) 25:35～  
\#10「 Angel  Smile」读卖TV12/05(月) 25:09～／日本TV12/13(火) 25:55～  
\#11「父娘(おやこ)の時間」读卖TV12/12(月) 25:59～／日本TV12/20(火) 25:25～  
\#12「船上の出会いと別れ」读卖TV12/19(月) 24:59～／日本TV01/03(火) 26:04～  
\#13「李大人からの贈り物」读卖TV01/09(月) 25:50～／日本TV01/10(火) 25:55～  
\#14「復活Ｃ・Ｈ！」读卖TV01/16(月) 24:59～／日本TV01/17(火) 25:25～  
\#15「パパを捜して」读卖TV01/23(月) 24:59～／日本TV01/24(火) 25:25～  
\#16「Ｃ・Ｈとしての資格」读卖TV01/30(月) 24:59～／日本TV01/31(火) 25:40～  
\#17「夢の中の出会い」读卖TV02/06(月) 27:03～／日本TV02/07(火) 26:00～  
\#18「親子の絆」读卖TV02/13(月) 25:03～／日本TV02/14(火) 25:25～  
\#19「陳老人の店」读卖TV02/20(月) 25:03～／日本TV02/21(火) 25:25～  
\#20「宿命のプレリュード」读卖TV02/27(月) 25:03～／日本TV02/28(火) 25:55～  
\#21「哀しき守護者(ガーディアン)」读卖TV03/06(月) 25:03～／日本TV03/07(火) 25:25～  
\#22「不公平な幸せ」读卖TV03/13(月) 25:03～／日本TV03/14(火) 25:25～  
\#23「出発(たびだち)のメロディー」读卖TV03/20(月) 25:29～／日本TV03/21(火) 25:25～  
\#24「鼓動と共に…」读卖TV03/27(月) 25:03～／日本TV03/28(火) 26:25～  
\#25「死にたがる依頼者」读卖TV04/03(月) 25:30～／日本TV04/04(火) 25:49～  
\#26「もう一度あの頃に」读卖TV04/10(月) 25:30～／日本TV04/11(火) 25:29～  
\#27「私、恋してる！？」读卖TV04/17(月) 25:25～／日本TV04/18(火) 25:29～  
\#28「約束」读卖TV04/24(月) 24:55～／日本TV04/25(火) 25:29～  
\#29「私の妹…香」读卖TV05/01(月) 24:55～／日本TV05/02(火) 25:29～  
\#30「この街は私の全て」读卖TV05/08(月) 24:55～／日本TV05/09(火) 25:59～  
\#31「最後の夜に見た奇跡」读卖TV05/15(月) 24:55～／日本TV05/16(火) 25:29～  
\#32「組織から来た女」读卖TV05/22(月) 24:55～／日本TV05/23(火) 25:29～  
\#33「神から授かりし子」读卖TV05/29(月) 24:55～／日本TV05/30(火) 25:29～  
\#34「二人の決意」读卖TV06/05(月) 24:55～／日本TV06/06(火) 25:29～  
\#35「未来へ…」读卖TV06/12(月) 24:55～／日本TV06/13(火) 25:29～  
\#36「幸せを運ぶ女の子」读卖TV06/19(月) 25:16～／日本TV06/20(火) 25:29～  
\#37「汚れのない心」读卖TV06/26(月) 24:55～／日本TV06/27(火) 25:29～  
\#38「オレの目になってくれ」读卖TV07/03(月) 25:25～／日本TV07/04(火) 25:29～  
\#39「依頼者は大女優」读卖TV07/10(月) 25:25～／日本TV07/11(火) 25:29～  
\#40「ミキの隠された秘密」读卖TV07/17(月) 25:04～／日本TV07/18(火) 25:38～  
\#41「自分の居場所」读卖TV07/24(月) 25:09～／日本TV07/25(火) 25:38～  
\#42「二人だけのサイン」读卖TV07/31(月) 25:00～／日本TV08/01(火) 25:29～  
\#43「私が生きる日常」读卖TV08/07(月) 25:14～／日本TV08/08(火) 25:29～  
\#44「俺たちの子供のために」读卖TV08/14(月) 24:55～／日本TV08/15(火) 25:29～  
\#45「人間核弾頭、楊(ヤン)」读卖TV08/21(月) 25:10～／日本TV08/22(火) 26:14～  
\#46「マザー・ハート」读卖TV08/28(月) 25:25～／日本TV08/29(火) 25:29～  
\#47「アカルイミライ！？」读卖TV09/04(月) 24:55～／日本TV09/05(火) 26:59～  
\#48「引き寄せられる運命」读卖TV09/11(月) 24:55～／日本TV09/12(火) 25:29～  
\#49「Get My Life」读卖TV09/18(月) 25:25～／日本TV09/19(火) 25:29～  
\#50「Last Present」(最終話) 读卖TV09/25(月) 25:25～／日本TV09/26(火) 25:59～  
  
★ Opening theme「**Finally**」(第2～24話)  
歌：Sowelu  
作詞：Shoko Fujibayashi  
作曲：Kosuke Morimoto  
編曲：Takuya Harada  
(DefSTAR RECORDS)  
→2006年2/1(水)发售  
  
★ Opening theme「**Lion**」(第25～38話)  
歌：玉置浩二  
作詞：松井五郎  
作曲：玉置浩二  
(Sony Music Records)  
→2006年3/15(水)发售  
  
★ Opening theme「**Battlefield of Love**」(第39～50話)  
歌：伊沢麻未  
作詞・作曲：伊沢麻未  
編曲：DJ CLAZZIQUAI  
(SME Records)  
→2006年8/30(水)发售：初回特典・Wide label规格（Angel Heart图案）  
  
★ Ending theme「**誰かが君を想ってる**」(第2～12・14～19・24・50話)  
歌：Skoop On Somebody  
作詞：SOS  
作曲：SOS＋土肥真生  
編曲：土肥真生＋SOS  
(SME Records)  
→2005年11/9(水)发售  
  
★ Special ・ Ending theme「**Daydream Tripper**」(第13・20～23話)  
歌：U_WAVE  
作詞：森 雪之丞  
作曲：石井妥師  
編曲：土橋安騎夫＆石井妥師  
(M-TRES)  
→2006年4/26(水)发售  
  
★ Ending theme「**My Destiny**」(第25～41話)  
歌：Cannon  
作詞・作曲・編曲：Cannon  
(Sony Music Japan International)  
→2006年5/24(水)发售：最初特典规范・wide cap sticker
  
★ Ending theme「**哀しみのAngel**」(第42～46話）  
(→第24・28話の挿入歌）  
歌：稲垣潤一  
作詞：Satomi  
作曲：羽場仁志  
編曲：水島康貴  
(Aniplex)  
  
★ Ending theme「**FEEL ME**」(第47～49話)  
歌：中西圭三  
作詞・作曲：中西圭三  
編曲：上野圭市  
(Aniplex)  
  
★挿入歌「**Gloria**」(第1・2・16・50話)  
歌：Cannon  
作詞・作曲：Cannon  
編曲・Produce：大谷 幸  
(Sony Music Japan International)  
  
★挿入歌「**Guardian Star**」(第7話)  
歌：尾崎亜美  
作詞・作曲・編曲：尾崎亜美  
(Columbia Music Entertainment)  
  
★挿入歌「**More Than You Know**」(第7・10・12・15話)  
歌：ＴＯＫＵ  
作詞：MAYUMI  
作曲：松本俊明  
編曲：ＴＯＫＵ  
(Sony Music Japan International)  
  
★挿入歌「**Wings of Love**」(第10・12・41話)  
歌：TOKU  
作詞：MAYUMI  
作曲：松本俊明  
編曲：TOKU／Febian Reza Pane  
(Sony Music Japan International)  
  
★挿入曲「**愛の夢　第３番**」(第11話)  
演奏：Balazs Szokolay  
作曲：Franz Liszt  
  
★挿入歌「**そばに**」(第12・36・41話)  
歌：牧伊織  
作詞：笛田彩葉  
作曲：西岡和哉  
編曲：鈴木正人  
(The Music Council)  
  
★挿入歌「**Rebirth**」(第12話)  
歌：kitago-yama  
作詞：安岡 優  
作曲：北山陽一　多胡 淳  
編曲：多胡 淳　北山陽一  
(Ki/oon Records)  
  
★挿入歌「**A Brand New Day**」(第19・42話)  
歌：Christy & Clinton  
作詞：Christy & Clinton  
作曲：谷脇仁美  
編曲：安部 潤  
(Aniplex)  
  
★挿入歌「**哀しみのAngel**」(第24・28話）  
歌：稲垣潤一  
作詞：Satomi  
作曲：羽場仁志  
編曲：水島康貴  
(Aniplex)  
  
★挿入歌「**Angel tears**」(第24・27・38話）  
歌：籐子  
作詞：籐子  
作曲：Tohko Project  
編曲：Tohko Project  
(Aniplex)  
  
★挿入歌「**Back To The City**」(第32話）  
歌：Cro-magnon feat. Kyoko  
作詞：Kyoko  
作曲：小菅 剛　大竹重寿　金子 巧  
編曲：Cro-magnon  
(Aniplex)  
  
★挿入歌「**Serenade**」(第33話）  
歌：Cannon  
作詞・作曲・編曲：Cannon  
(Sony Music Japan International)  
  
★挿入歌「**海の色**」「**sound collage #1**」(第35話）  
歌：岩男潤子  
作詞：岩男潤子  
作曲・編曲：山本はるきち  
  
★挿入歌「**parallel －パラレル－**」(第42話）  
歌：shungo.  
作詞：shungo.  
作曲：shungo. & Serio  
編曲：shungo. & Serio  
(Aniplex)  
  
★挿入歌「**Fri Day!**」(第45話）  
歌：LiL'G  
作詞・作曲：LiL'G  
(Aniplex)  
  
→全国也陆续决定放映  
・札幌TV放送：10/17(月) 25:25～  
・山形放送：10/07(金) 25:25～  
・中京TV放送：10/12(水) 25:59～  
・福岡放送：10/16(日) 25:25～  
・西日本放送：10/17(月) 25:50～  
・鹿児島読売TV：10/21(金) 25:25～  
・静岡第一TV：2006/01/11(水) 26:10分～<font color=#ff0000>*</font>来自chikka的信息。  
  
### ●2005年09月09日(金)  
Bunch Comics**『Angel Heart』[第16巻](./book_bc.md#16)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch新年4・5合并特刊 封面的图案  
  
### ●2005年06月15日(水)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[別巻VOLUME:Z（特別短編集）](./book_pf.md#Z)** 发售  
☆[Get Wild] 附有特别的附录CD   
→定价900日元（含税）  
▽有彩色纸张赠送/抽出200名获奖者  
  
### ●2005年06月09日(木)  
Bunch Comics**『Angel Heart』[第15巻](./book_bc.md#15)** 发售  
→定价530日元（含税）  
▽封面是Comic Bunch第28号封面的图案  
  
▽漫画累计突破1000万册纪念「Angel BOX」present  
Comics第1～15卷的封面画复制品/18张一套  
→B4尺寸（高364x 宽257mm）/装在带有序号的豪华礼盒中  
※腰封上的入场券和Comic Bunch第28期（6月10日发售）上的入场券都是必需的/1000名获奖者将被抽出 
→截止日期为6月30日（周四），以当天邮戳为准，详见腰封    
  
### ●2005年05月14日(土)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[別巻VOLUME:Y（イラストレーションズ2）](./book_pf.md#Y)** 发售  
→定价800日元（含税）  
▽有彩色纸张赠送/抽出200名获奖者  
  
### ●2005年04月15日(金)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:32](./book_pf.md#32)** 发售  
→定价980日元（含税）  
  
◯连续3卷的爱读者W感謝企画  
▽每期抽奖：特殊的 character quo card 
　32巻「リョウ＆香」  
▽30・31・32巻 全卷compaign：Complete BOX  
　CH完全版17～32+独立卷Y、Z可以收纳！！ / 申请者全员service  
→详情参见腰封   
  
### ●2005年03月15日(火)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:30](h./book_pf.md#30)、[VOLUME:31](./book_pf.md#31)** 同时发售  
→定价980日元（含税）  
  
◯连续3卷的爱读者W感謝企画start  
▽每期抽奖：特制character quocard  
　30巻「リョウ」、31巻「香」、32巻 「？」的三种／合计600名  
▽30・31・32巻 全卷campaign：Complete BOX  
　CH完全版17～32+独立卷Y、Z可以收纳！！ / 申请者全员service第2弹  
→详情参见腰封  
  
### ●2005年02月18日(金)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:28](./book_pf.md#28)、[VOLUME:29](./book_pf.md#29)** 同时发售  
→定价980日元（含税）  
  
### ●2005年02月09日(水)  
Bunch Comics**『Angel Heart』[第14巻](./book_bc.md#14)** 发售  
→定价530日元（含税）  
  
▽封面是Comic Bunch'03年第52号封面的图案  
  
### ●2005年01月15日(土)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:26](./book_pf.md#26)、[VOLUME:27](./book_pf.md#27)** 同时发售  
→定价980日元（含税）  
  
### ●2004年12月15日(水)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:24](./book_pf.md#24)、[VOLUME:25](./book_pf.md#25)** 同时发售  
→定价980日元（含税）  
  
### ●2004年11月15日(月)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:22](./book_pf.md#22)、[VOLUME:23](./book_pf.md#23)** 同时发售  
→定价980日元（含税）  
  
### ●2004年11月09日(火)  
Bunch Comics**『Angel Heart』[第13巻](./book_bc.md#13)** 发售  
→定价530日元（含税）  
  
▽封面是Comic Bunch第40号封面的图案  
  
### ●2004年10月15日(金)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:20](./book_pf.md#20)、[VOLUME:21](./book_pf.md#21)** 同时发售  
→定价980日元（含税）  
  
### ●2004年09月15日(水)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:19](./book_pf.md#19)、[別巻VOLUME:X（イラストレーションズ1）](./book_pf.md#X)** 同时发售  
→定价980日元、插图集800日元（含税）  
  
### ●2004年09月09日(木)  
Bunch Comics**『Angel Heart』[第12巻](./book_bc.md#12)** 发售  
→定价530日元（含税）  
  
▽封面是Comic Bunch第27号扉页画的图案  
  
### ●2004年08月14日(土)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:17](./book_pf.md#17)、[VOLUME:18](./book_pf.md#18)** 同时发售  
→定价980日元（含税）  
  
### ●2004年07月15日(木)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:15](./book_pf.md#15)、[VOLUME:16](./book_pf.md#16)** 同时发售  
→定价980日元（含税）  
  
◯豪华特制「Complete BOX」全員service 第1弾start（附报名券）  
▽可以收纳CH完全版17册  
▽豪华特制BOX上有新绘制的封面插图  
剪下第13卷的报名表，将第14、15、16卷的报名票粘贴在上面  
需要 - 1400日元的汇票（含运费）  
→截止日期为8月31日（星期二），以当天的邮戳为准，详见腰封    
  
### ●2004年06月15日(火)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:13](./book_pf.md#13)、[VOLUME:14](./book_pf.md#14)** 同时发售  
→定价980日元（含税）  
  
◯豪华特制「Complete BOX」全员service第1弹Start(附报名券)  
▽CH完全版可以整齐地收纳起来  
▽豪华特制BOX上有新绘制的封面插图  
→申请方法在第15、16卷的腰封中给出    
  
### ●2004年06月09日(水)  
Bunch Comics**『Angel Heart』[第11巻](./book_bc.md#11)** 发售  
→定价530日元（含税）  
  
▽封面是Comic Bunch第27号封面的图案  
  
### ●2004年05月15日(土)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:11](./book_pf.md#11)、[VOLUME:12](./book_pf.md#12)** 同时发售  
→定价980日元（含税）  
  
### ●2004年04月15日(木)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:09](./book_pf.md#9)、[VOLUME:10](./book_pf.md#10)** 同时发售  
→定价980日元（含税）  
  
◯100万部突破記念Campaign  
▽漫画第9、10卷的顺位的2张入场券可以用来填写问卷，以赢得一套4张豪华明信片，共1000人。  
→提交申请截止至2004年5月31日(月)<font color=#ff0000>邮戳有効</font>  
  
详情参见[徳間書店的Homepage](http://www.tokuma.jp)。  
  
### ●2004年04月01日(木)～  
新宿「MY CITY」6楼的山下書店举行的CH完全版 发售記念企画  
▽CITY HUNTER的复制原画公开  
▽购买完全版的前300名，以北条先生亲笔为基础的original paper作为礼物 
  
### ●2004年03月15日(月)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:07](./book_pf.md#7)、[VOLUME:08](./book_pf.md#8)** 同时发售  
→定价933日元（不含税）  
  
### ●2004年03月09日(火)  
Bunch Comics**『Angel Heart』[第10巻](./book_bc.md#10)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch'03年第52号扉页画  
  
### ●2004年02月14日(土)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:05](./book_pf.md#5)、[VOLUME:06](./book_pf.md#6)** 同时发售  
→定价933日元（不含税）  
  
### ●2004年01月15日(木)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:03](./book_pf.md#3)、[VOLUME:04](./book_pf.md#4)** 同时发售  
→定价933日元（不含税）  
  
### ●2004年01月06日(火)  
Da Vinci 2月号 发售（定价450日元）  
▽Comics Da Vinci「北条 司 その尽きない魅力」（译注：「北条司 无尽的魅力」）  
※上个月预告的title是「工匠北条司的作品」  
  
### ●2004年01月05日(月)  
日経Entertainment！2月号 发售  
→定价500日元  
▽【対談】北条司×飯島愛  
「Cat's Eye」「City Hunter」  
「Angel Heart」誕生秘話  
  
### ●2003年12月15日(月)  
德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:01](./book_pf.md#1)、[VOLUME:02](./book_pf.md#2)** 同时发售  
→定价933日元（不含税）  
●所有卷的新封面插图  
●在杂志上发表的彩页完全再现  
●以前的漫画版本中未收集的页面被重新收录  
  
▽「发刊纪念 campaign！！」**特制电话卡&海报**present！！   
▽漫画1、2巻的腰封的入场券2个，特制电话卡1000名、海报2000名。（译注：待校对）  
→申请截至于2004年1月31日(星期六)<font color=#ff0000>邮戳有效</font>  
  
详情参见[徳間書店的Homepage](http://www.tokuma.jp)。  
  
### ●2003年12月09日(火)  
Bunch Comics**『Angel Heart』[第9巻](./book_bc.md#9)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch第31号扉页画  
  
### ●2003年09月09日(火)  
Bunch Comics**『Angel Heart』[第8巻](./book_bc.md#8)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch第19号封面  
→超豪华! 有装裱的彩色纸质present  
北条先生签名的彩色纸（有获奖者的名字）25名/失望奖明信片套装500名   
截止日期为10月8日（周三）；详情见単行本帯  
  
### ●2003年08月28日(木)  
別冊宝岛862**[『City Hunter最强读本』](./book_takara.md)** 发售  
→定价952日元（不含税）  
  
### ●2003年06月09日(月)  
Bunch Comics**『Angel Heart』[第7巻](./book_bc.md#7)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch第10号封面  
→累計500万部突破記念present  
A奖「作者亲笔签名的报刊」50人/B奖「Clear File Set」1000人（译注：待校对）  
截止日期7月8日(星期二)·详情请参阅单行本带（译注：待校对） 
  
### ●2003年04月07日(月)  
Bunch官方i-mode网站「i-bunch 」OPEN  
▽AH的shooting game「Mokkori Hunter」和待机画面  
Access方法：iMenu→menu list→TV/广播/杂志→(5)杂志→iBunch  
每月：300日元   
  
### ●2003年03月08日(土)  
Bunch Comics**『Angel Heart』[第6巻](./book_bc.md#6)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch2002年第44号封面  
  
### ●2003年01月07日(火)  
Raijin Collection  
**『北条司短編集 Vol.1 [天使的礼物](./book_rc.md#短編1)』**发售  
▽City Hunter的2个短篇故事收录在内  
→定价286日元＋税  
  
### ●2003年01月06日(月)  
英語版漫画雑誌  
**RAIJIN COMICS（ライジンComics）vol.4（2月号）**发售  
「CITY HUNTER（英語版）」  
"EPISODE5: A SNIPER IN THE DARK"  
▽特別附录 Listening CD（CH和北斗の拳）  
  
### ●2002年12月05日(木)  
英語版漫画雑誌  
**RAIJIN COMICS（ライジンComics）2003年1月号** 发售  
「CITY HUNTER（英語版）」  
"EPISODE4: THE DEVIL IN THE BMW part 3"  
▽番外篇「爆发了 "mokkori"大争论！！」  
→对50名母语人士和英语语言专家进行深入采访  
▽特別附录 Listening CD（CH和北斗の拳）  
▽CH海报作为50名问卷调查礼物  
→店面销售定价880日元（含税）  
  
### ●2002年11月09日(土)  
Bunch Comics**『Angel Heart』[第5巻](./book_bc.html#5)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch第31号封面  
  
### ●2002年11月05日(火)  
英語版漫画雑誌  
**RAIJIN COMICS（ライジンComics）創刊2号（12月号）**发售  
「CITY HUNTER（英語版）」/封面  
"EPISODE3: THE DEVIL IN THE BMW part 2"  
▽特別附录 Listening CD（CH和北斗の拳）  
▽CH海报作为50名问卷调查礼物  
→店面销售定价880日元（含税）  
  
### ●2002年10月05日(土)  
英语版漫画杂志 创刊  
**RAIJIN COMICS（ライジンComics）創刊号（11月号）**发售  
「CITY HUNTER（英語版）」  
"EPISODE2: THE DEVIL IN THE BMW part 1"  
▽特別附录 Listening CD（CH和北斗の拳）  
▽CH海报作为100名问卷调查礼物  
→毎月5日发售／店内销售880日元（含税）  
  
### ●2002年09月/上旬～11月/最后一天为止  
新宿「MY CITY」6楼的山下書店设立了[「XYZ 伝言板BOX」](./xyzbox.md)  
▽征集想让CH解决的独特委托、有趣委托  
▽优秀的委托被作品采用，还有亲笔签名的单行本Present  
▽用配备的特制Postcard投递  
→ Card系Bunch第22・23合并特刊封面的图案  
  
### ●2002年08月30日(金)<font color=#ff0000>までに</font>  
通过预购英文版漫画杂志「RAIJIN COMICS」的年度订阅  
「CITY HUNTER/高級giclee印刷品和Primagraphy」赠品 （译注：待校对） 
▽可选择3种不同的模式  
▽尺寸157mm×116mm  
▽海洋堂「北斗の拳」还可选择肯健次郎的人物 
  
<s>每年（A course）550日元×48本=26400日元 运费4800日元 总计31200日元</s>  
→决定将其定为月刊，年费为6600日元（包括邮费）  
  
### ●2002年07月26日(金)  
「RAIJIN COMICS」预创刊号**City Hunter・ Special **发售  
→定价350日元（含税）  
  
▽特别2个主要附录：特别CD（CH有声剧、屏幕保护程序、RAIJIN Junk English摘要）&CH特别鼠标垫  
▽7-11、イトーヨーカドー、紀伊国屋書店、文教堂、旭屋書店、三省堂、ジュンク堂書店和其他书店发售  
  
### ●2002年07月16日(火)～08月31日(土)  
吉祥寺[BOOKS Ruhe](http://www.books-ruhe.co.jp/)原哲夫／北条司原画展  
▽在楼梯平台的空间里  
▽通过抽奖向CH和AH等漫画购买者赠送签名彩纸和Original Goods的Present  
  
### ●2002年07月09日(火)  
Bunch Comics**『Angel Heart』[第4巻](./book_bc.md#4)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch第18号封面  
  
### ●2002年05月17日(金)  
▽从今天起，Comic Bunch每周五发售  
  
英文漫画杂志**「RAIJIN COMICS」创刊0号**发售  
「CITY HUNTER（英語版）」  
"EPISODE1: TEN COUNT WITH NO GLORY"  
▽配有10cm的健次郎模型  
▽在7-11独家销售/300日元（含税）  
→后来免费发行的这本杂志没有模型  
  
### ●2002年05月13日(月)～  
(工作日)从24:55开始  
在TBS电台（和其他27个国家网络/27：00～※）   
**「RAIJIN（雷神） Junk English」**放送中  
▽通过CITY HUNTER和其他漫画的对话来学习英语  
▽由Coamix提供的广播节目，与「RAIJIN COMICS」连动  
▽Personality是Thane Camus  
　※大阪电台在23:42播出    
  
### ●2002年04月01日(月)  
**「CITY HUNTER 3D Art Crystal Paperweight 」**  
▽限量220个，从中午开始[网上销售](http://www.coamix.co.jp/)  
▽ Hammer 、蜻蜓、乌鸦的3种类型可选  
→价格4500日元（不含税）+邮费500日元（不含税）  
  
### ●2002年03月08日(金)  
Bunch Comics**『Angel Heart』[第3巻](./book_bc.md#3)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch新年7・8合并特刊AH扉页  
  
### ●2002年02月06日(水)  
** Lawson ・Bunch World Selection 「CITY HUNTER」**  Lawson 发售  
→定价495日元＋税  
「がんばれ！香ちゃん！！編」  
「海坊主にゾッコン！！編」  
  
### ●2002年01月08日(火)  
**『北条司 Episode 1』**发售  
→定价320日元  
▽包括CITY HUNTER在内的北条司作品的连载第1話，共5部  
▽CITY HUNTER×Cat's Eye的大海报一幅  
  
### ●2001年12月16(日)～2002年01月31(木)  
吉祥寺[BOOKS Ruhe](http://www.books-ruhe.co.jp/)有Coamix原画展  
▽在楼梯平台的空间里  
▽「Angel Heart」等、「蒼天の拳」「251」等  
  
### ●2001年12月08日(土)  
Bunch Comics**『Angel Heart』[第2巻](./book_bc.md#2)** 发售  
→定价505日元（不含税）  
  
▽封面是Comic Bunch第20号封面  
  
### ●2001年10月09日(火)  
Bunch Comics**『Angel Heart』[第1巻](./book_bc.md#1)** 发售  
→定价505日元（不含税）  
  
▽封面是新画的（GH＆獠的侧脸）→[Coamix HP](http://www.coamix.co.jp/)or[北条司公式Homepage](http://www.hojo-tsukasa.com/)GO！  
▽Comics初版的腰封、以及Comic Bunch No.22&No.23/24合并号上的申请券，收集其中3张、<font color=#ff0000>抽选</font>1000名AH**听漫的Drama CD**！！  
→申请10月31日(火)<font color=#ff0000>必须送达</font>  
  
※AH等、Bunch Comics介绍**免费小冊子**（内封是AH的Comic封面）也在书店等地分发！    
  
### ●2001年10月01日(月)  
**「On-demand Personal Calendar」**  
发售来自[Coamix HP](http://www.coamix.co.jp/)  
▽**A3 Calendar**／高420mm×宽297mm（A3纵向）  
→价格3500日元（含邮费）+税  
▽**桌面日历兼明信片**／縦104mm×横205mm  
→价格2000日元（含邮费）+税  
  
<font color=#ff0000>现在也有桌面版本</font>  
※包括CH在内的7种类型的图像（桌面的8种类型）可以从一系列准备好的图像中自由选择    
※你可以把名字放在封面上  
※可以设定自己的纪念日  
※可以从任何月份开始（2002年1月～）  
  
### ●2001年8月20日(月)<font color=#ff0000>截止</font>  
Comic Bunch最新一期的测试，**AH手机待机图片**，免费DL  
▽备用图片与Comic Bunch第3期的封面图片相同  
  
### ●2001年8月02日(木)  
**「Angel Heart」总集篇1** 发售　定价290日元  
▽第1話～第9話被收录  
▽Original T-shirt（仅L尺寸） present企画  
→单行本Comics第1卷预计将于10月发售    
  
### ●2001年7月17日(火) 从中午开始  
**「CITY HUNTER」ZIPPO**、「[北条司公式Homepage](http://www.hojo-tsukasa.com/)」开始在网上销售  
<font color=#ff0000>限量200个即日售罄</font>  
价格8500日元(运费全国一律500日元·消费不含税)←合计9450日元(含税)  
  
### ●2001年6月26日(火)  
文艺春秋社发售的**『TITLE』8月号**（530日元）的漫画介绍页(p.11)上，意大利空前的漫画・热潮的报道。   
▽「在日本风靡一时的『City Hunter』」「现在『Family Compo』很受欢迎」  
（mizue提供的信息。 谢谢m(__)m）  
  
### ●2001年6月25(月)  
新潮社发售的**『波』7月号**(100日元)中有「Comic Bunch Interview」。（p.6）  
▽刊登根岸忠氏、原哲夫氏、北条司氏（p.10）的Interview。  
（[マリィさん](mailto:rosemary@badgirl.co.jp)提供的信息。非常感谢m(__)m）  
  

<a name="スケジュール"></a>
## 最新Schedule！

  
<TABLE BORDER="1"><TR><TD BGCOLOR="#191970"><FONT COLOR = #FFFFFF>●BUNCH WORLD B6尺寸漫画<BR><B>「CITY HUNTER」series</B><BR>　隔週水曜日发售。286日元+税。</FONT></TD><TD BGCOLOR="#ffffff"><FONT COLOR="#000000"><!-- 最新BW -->Vol.39 [最終巻FOREVER, CITY HUNTER!!編] 发售中<BR>
→<FONT COLOR="RED"><!-- 下期BW -->完結</FONT></FONT></TD></TR><TR><TD BGCOLOR="#ffffff"><FONT COLOR="#000000">●「Angel Heart」连载的<B>「周刊Comic Bunch」</B><BR>（发行 新潮社・编辑Coamix）<BR>　每周五发售。
</FONT></TD><TD BGCOLOR="#191970"><FONT COLOR = #FFFFFF>
<!-- 最新号CB -->第36・37合并号(8月20日・27日号)发售／280日元。／AH 1st Season最終話。<BR>
→下期は<FONT COLOR="RED"><!-- 下期CB -->8月20日(金)发售／280日元。</FONT></FONT></FONT></TD></TR><TR><TD BGCOLOR="#191970"><FONT COLOR = #FFFFFF>●英語版漫画雑誌<B>「RAIJIN COMICS（ライジンComics）」</B><BR>英語版CITY HUNTER连载・附Listening CD<BR>毎月5日发售。880日元（含税）。</FONT></TD><TD BGCOLOR="#ffffff"><FONT COLOR="#000000"><!-- 最新RC -->vol.12（10月号）发售中<BR>
→<FONT COLOR="RED"><!-- 下期RC -->vol.12停刊。</FONT></FONT></TD></TR><TR><TD BGCOLOR="#ffffff"><FONT COLOR="#000000">●YTV（Yomiuri(译注：读卖）电视广播）<B>动画「City Hunter2」</B>现正重播中！<BR>只有关西地区？
</FONT></TD><TD BGCOLOR="#191970"><FONT COLOR = #FFFFFF>
<!-- 最新放送 -->
<FONT COLOR="RED">放送时间<!-- 下回 --></FONT><BR>每周六　　：深夜2:30～<BR>第1＆第3周日：深夜1:35～<BR>第2＆第4周日：深夜2:30～</FONT></FONT></TD></TR><TR><TD BGCOLOR="#191970">
<FONT COLOR = #FFFFFF>●CS放送 <A HREF="http://www.skyperfectv.co.jp/" TARGET="_top">SKY Perfec TV!</A> <B>动画「City Hunter」</B>放映中！<FONT COLOR="RED">*</FONT><BR>ch.276「キッズステーション」<BR>ch.724「アニマックス」</FONT></TD><TD BGCOLOR="#ffffff"><FONT COLOR="#000000">
→下集播出<BR>
参见<A HREF="http://www.skyperfectv.co.jp/" TARGET="_top">SKY Perfec TV!</A>的网站。
<!-- 362 日19:30-20:00 再 木07:30-08:00 金18:00-18:30 -->
<!-- 362 end -->

<!-- 276 月19:30～20:00 金20:00-20:30 土21:00-21:30 日16:00-16:30 -->
<!--
<FONT COLOR="RED">2月05日(火)19:00～19:30(ch.276)</FONT><BR>
「City Hunter第22話 愛的Cupid　Diamond乾杯！！ 」<BR>
 -->

<!-- 276 end -->
<!--
<FONT COLOR="RED">7月13日(金)～7月20(金)各21:30、24:30、27:30(ch.147)</FONT><BR>
City Hunter Special <BR>『愛和宿命的连发枪』<BR>『Bay City Wars』<BR>『百万美元的阴谋』
<FONT COLOR="RED">8月25日(土)13:11～16:30(ch.724)</FONT><BR>
『Thanks! Sunrise!』<BR>
City Hunter的劇場版作品「愛和宿命的连发枪」介紹
-->
</FONT></TD></TR></TABLE>

<!-- <FONT COLOR="RED">0月00日(-)00:00～00:00(ch.000)</FONT><BR> -->
<FONT COLOR=#0000ff>\*</FONT>タカ提供的信息。 谢谢。m(__)m<BR>
<FONT COLOR=#ff0000>\*</FONT>收到了一些人的信息。 非常感谢。m(__)m<BR><BR>

  

<a name="番外編"></a>
## ●番外篇信息！  

  
### ●2009年秋天，宇都宮隆举行了他5年来的首次个人演唱会**「TAKASHI UTSUNOMIYA　CONCERT　TOUR 2009　SMALL　NETWORK　F.O.D」**。new!  
→[更多信息](./tm_utsu.md#1)  
  
### ●TM NETWORK single collection 第2弾**「TM NETWORK THE SINGLES 2」**2009年9月30日由Sony Music Direct发售。new!  
【初次规格】2张带bonus disc的CD组合  产品编号：MHCL1587～8  价格：3150日元(含税)  
【普通版】1张CD  编号：MHCL 1589  价格：\2520日元(含税)  
→[更多信息](./tm_utsu.md#2)  
  
### ●讲述了小室哲哉被判缓刑的5亿日元诈骗案的逮捕剧和审判、至今为止的音乐活动等的随笔**「罪与音乐」**(幻冬舍、1365日元)于2009年9月15日发售。new!  
与他有关的歌曲的销售和发行也在8月22日恢复了。  
→[更多信息](./tm_utsu.md#3)  
  
### ●由于各种原因，以下「Get Wild」的翻唱歌曲已被取消销售。
（2008年11月4日）  
  
### <s>●globe的2年半来的首支单曲将于2008年11月26日（周三）发售。  
这首歌是对TM NETWORK的**「Get Wild」**的翻唱。  
由avex发售，价格为1260日元（含税）。</s>  
  
### ●下面介绍的特效hero节目**「超光战士Chancellion」**将于2008年6月7日(周六)在CS Family剧场播出。  
→每周六12：30～13：00（重播于每周二深夜2：00～2：30）  
▽作品中时隐时现CH的故事。  
▽主角侦探 「涼村暁」，从各方面来看，是以冴羽獠为原型。  
  
### ●2008年春、**TM NETWORK 全国巡演**  
全国7都市14公演 TM NETWORK 期待已久的巡回赛决定！  
→[更多信息](./tm_tour.md)  
  
### ●去年12月3日TM NETWORK在日本武道馆的现场表演的DVD于4月2日（周三）发售。  
**「TM NETWORK -REMASTER- at NIPPON BUDOKAN 2007」**  
价格5500日元（含税）。  
→[更多信息](./tm_dvd.md)  
  
### ●在CS放送的TBS频道中，从2008年元旦开始的6日期间，从深夜2点到4点播出**「90‘s Live Collection I love band」**  
在这个节目中，动画CH2前期OP曲&插入歌中熟悉的「PSY・S」，和同样演唱后期OP曲的「FENCE OF DEFENSE」出现在这个节目中。  
→「FENCE OF DEFENSE」于1月4日(周五)登场，「PSY・S」于1月5日(周六)登场。  
  
### ●TM NETWORK的3年来的第一张专辑**「SPEEDWAY」**由、YOSHIMOTO　R and C．LTD自2007年12月5日(水)发售。  
价格为3060日元（含税）。  
▽标题的「SPEEDWAY」是TM NETWORK的之前的乐队名。  
▽10月31日发行的新歌「WELCOME　BACK　2」和coupling曲目「N43」（但都是1983年编辑），以及其他11首歌曲被收录。  
这张专辑的宣传文案是「致那些我为之疯狂的日子。 11首来自我们3人的怀旧、新的、尽全力的歌曲。」  
  
↓12月23日(日)的**「みゅーじん～音遊人～」**（TV东京每周日10:54播出的音乐信息节目，时长30分钟），由TM NETWORK主持。  
该节目播出了11月和12月的现场表演和幕后故事。  
  
### ●小室哲哉氏、宇都宮隆氏、木根尚登所属的TM NETWORK时隔3年再次结成，发行了新曲**「WELCOME BACK ２」**(作词·作曲：小室哲哉)！  
YOSHIMOTO　R and C．LTD于2007年10月31日（周三）发售。  
价格为1260日元（含税）。  
▽coupling曲是「N43」（作詞・作曲：木根尚登）。  
▽音乐会信息→11月2日和3日（Pacific 横浜），11月26日和27日（C.C.LEMON Hall），12月3日（日本武道館）。  
  
### ●圣饥魔II的主唱·戴蒙小暮阁下(自称10万44岁)Cover女性摇滚歌手的歌曲的专辑**「GIRLS' ROCK」**，由Avex于2007年1月24日发售。  
▽收录了小比類巻Kaharu演唱的「CITY HUNTER~爱啊不要消失~」  
首批限量版为3990日元，标准版为3150日元（均含税）。  
  
### ●CH的动画的脚本staff之一井上敏树的小说**「仮面ライダーファイズ正伝－異形の花々－」**将于2004年8月18日开始由讲谈社发售。  
定价933日元（不含税）。  
▽这是去年播出的「假面骑士555」的小说版，但内容与电视版完全不同  
（硬要说的话，井上真正想写的「假面骑士555」的故事，它消除了电视的限制）。（译注：待校对）  
▽据井上介绍**这是「一个关于把子弹装进枪里，一枪打中目标」的故事 **。
（井上写了剧本的「剧场版假面骑士Blade - Missing Ace -」将于9月11日在全国的东映影院上映）。  
  
### ●曾执笔CH的动画的剧本的遠藤明範的小说**「真牡丹灯篭」**正在发售。  
来自学研M文库。定价590日元。  
  
### ●CH的动画的脚本staff之一井上敏树脚本的特效hero节目**「超光战士Changerion」**的DVD，将于2003年6月21日发售。  
▽被称为「特效版CITY HUNTER」，这是有一种「CH的味道」的作品。    
▽该片还将从3月起在SkyPerfect TV的东映channel播出。    
※井上先生为目前在朝日电视台播出的「假面骑士555」写剧本，据说「超光战士Changerion」是平成版假面骑士（クウガ・アギト・龍騎・555）的起源。  
  
（所有的番外篇都是由chikka(Eastern Shizuoka)提供的。 非常感谢。m(__)m）  
  

<a name="記録"></a>
## ●最新信息的纪录  

### ●2010年08月06日(金)  
> Comic Bunch第36・37合并号(8月20日・27日号)发售  
> 「Angel Heart」第363話  
> 【恋の行方】  
> ▽1st Season／完  
> →2nd Season信息可在北条司官方网站及Twitter上获得  
>   
### ●2010年07月30日(金)  
> Comic Bunch第35号(8月13日号)发售  
> 「Angel Heart」暂停。  
> ▽下周AH 1st.Season最終話  
>   
### ●2010年07月23日(金)  
> Comic Bunch第34号(8月6日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年07月16日(金)  
> Comic Bunch第33号(7月30日号)发售  
> 「Angel Heart」第362話  
> 【乱暴な天使】  
>   
### ●2010年07月09日(金)  
> Comic Bunch第32号(7月23日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2010年07月02日(金)  
> Comic Bunch第31号(7月16日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2010年06月25日(金)  
> Comic Bunch第30号(7月9日号)发售  
> 「Angel Heart」第361话/封面  
> 【二人分のギフト】  
>   
### ●2010年06月18日(金)  
> Comic Bunch第29号(7月2日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年06月11日(金)  
> Comic Bunch第28号(6月25日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年06月04日(金)  
> Comic Bunch第27号(6月18日号)发售  
> 「Angel Heart」第360話  
> 【父と息子】  
>   
### ●2010年05月28日(金)  
> Comic Bunch第26号(6月11日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2010年05月21日(金)  
> Comic Bunch第25号(6月4日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年05月14日(金)  
> Comic Bunch第24号(5月28日号)发售  
> 「Angel Heart」第359话/封面  
> 【愛の選択】  
> ▽Bunch9周年・北条司30周年記念号  
> ・北条司tribute pinup第2弾  
> ▽北条司最新Color原画＆tribute pinup展举办  
> →「CAFE ZENON」5/14～27  
>   
### ●2010年05月07日(金)  
> Comic Bunch第23号(5月21日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
> ▽下期（14日发售）、北条司debut 30周年・Bunch創刊9周年記念号  
> →华丽作家阵容集结 北条司tribute pinup第2弾  
> （小畑友紀・芹沢直樹・高橋留美子・次原隆二・冨樫義博・渡辺航）  
>   
### ●2010年04月23日(金)  
> Comic Bunch第21・22合并号(5月7日・14日号)发售  
> 「Angel Heart」第358話  
> 【ギフト】  
>   
### ●2010年04月16日(金)  
> Comic Bunch第20号(4月30日号)发售  
> 「Angel Heart」第357话/封面  
> 【陳老人暗躍】  
>   
### ●2010年04月09日(金)  
> Comic Bunch第19号(4月23日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年04月02日(金)  
> Comic Bunch第18号(4月16日号)发售  
> 「Angel Heart」第356話  
> 【生きる目的】  
>   
### ●2010年03月26日(金)  
> Comic Bunch第17号(4月9日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年03月19日(金)  
> Comic Bunch第16号(4月2日号)发售  
> 「Angel Heart」第355話  
> 【受け止める覚悟】  
>   
### ●2010年03月12日(金)  
> Comic Bunch第15号(3月26日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年03月09日(火)  
> Bunch Comics**『Angel Heart』[第32巻](./book_bc.md#32)** 发售  
> →定价540日元（含税）  
> ▽封面是Comic Bunch'09年第39号封面的图案  
>   
### ●2010年03月05日(金)  
> Comic Bunch第14号(3月19日号)发售  
> 「Angel Heart」第354話  
> 【今日限りの自由】  
>   
### ●2010年02月26日(金)  
> Comic Bunch第13号(3月12日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年02月19日(金)  
> Comic Bunch第12号(3月5日号)发售  
> 「Angel Heart」第353话/封面  
> 【恋の吊り橋】  
>   
### ●2010年02月12日(金)  
> Comic Bunch第11号(2月26日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年02月05日(金)  
> Comic Bunch第10号(2月19日号)发售  
> 「Angel Heart」第352話  
> 【笑顔の理由】  
>   
### ●2010年01月29日(金)  
> Comic Bunch第9号(2月12日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年01月22日(金)  
> Comic Bunch第8号(2月5日号)发售  
> 「Angel Heart」第351話  
> 【薔薇と御曹司】  
>   
### ●2010年01月15日(金)  
> Comic Bunch新年7号(1月29日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2010年01月08日(金)  
> Comic Bunch新年6号(1月22日号)发售  
> 「Angel Heart」第350話  
> 【陳さんの策謀】  
> ▽2010新春全作家阵容亲笔插画present  
>   
### ●2009年12月24日(木)  
> Comic Bunch新年4・5合并号(1月15日・20日号)发售  
> 「Angel Heart」第349话/封面  
> 【希望の風】  
> ▽北条司debut 30周年企画・第1弾  
> ・北条作品tribute pinup  
> →由豪华漫画家阵容绘制的新插画(袋とじ附录)（译注：待校对）  
> ・北条司mini原画展将举办  
>   
### ●2009年12月18日(金)  
> Comic Bunch新年3号(1月8日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
> ▽下期（24日(木)发售）北条司30周年特別企画  
> →由知名艺术家创作的北条世界人物的精湛插图集一举刊登  
> （朝基まさし・井上雄彦・梅澤春人・新條まゆ・のりつけ雅春・原哲夫）  
>   
### ●2009年12月04日(金)  
> Comic Bunch新年1・2合并特刊(1月1日・5日号) 发售  
> 「Angel Heart」第348話  
> 【たくさんの月】  
>   
### ●2009年11月27日(金)  
> Comic Bunch第52号(12月11日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2009年11月20日(金)  
> Comic Bunch第51号(12月4日号)发售  
> 「Angel Heart」第347話  
> 【優しい嘘】  
>   
### ●2009年11月13日(金)  
> Comic Bunch第50号(11月27日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2009年11月09日(月)  
> Bunch Comics**『Angel Heart』[第31巻](./book_bc.md#31)** 发售  
> →定价540日元（含税）  
> ▽封面是Comic Bunch第14号封面的图案  
>   
### ●2009年11月06日(金)  
> Comic Bunch第49号(11月20日号)发售  
> 「Angel Heart」第346话/封面  
> 【嘘だと言って…】  
>   
### ●2009年10月30日(金)  
> Comic Bunch第48号(11月13日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2009年10月23日(金)  
> Comic Bunch第47号(11月6日号)发售  
> 「Angel Heart」第345話  
> 【忘却の代償】  
>   
### ●2009年10月16日(金)  
> Comic Bunch第46号(10月30日号)发售  
> 「Angel Heart」每隔一期连载。  
> →不在本周刊号中  
>   
### ●2009年10月09日(金)  
> Comic Bunch第45号(10月23日号)发售  
> 「Angel Heart」第344话/封面  
> 【ママを捜して…】  
>   
### ●2009年10月02日(金)  
> Comic Bunch第44号(10月16日号)发售  
> 「描線の流儀」北条司インタビュー 後編（译注：「线条的风格」北条司Interview 后篇）  
> ▽从本期起「Angel Heart」每隔一期连载  
> 本周没有连载「Angel Heart」。  
>   
### ●2009年09月18日(金)  
> Comic Bunch第42・43合并特刊(10月2日・9日号)发售  
> 「Angel Heart」第343話  
> 【会いたい】  
> ▽「描線の流儀」北条司インタビュー 前編 （译注：「线条的风格」北条司Interview 前篇）  
>   
### ●2009年09月11日(金)  
> Comic Bunch第41号(9月25日号)发售  
> 「Angel Heart」第342話  
> 【偽りの再会】  
>   
### ●2009年09月04日(金)  
> Comic Bunch第40号(9月18日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2009年08月28日(金)  
> Comic Bunch第39号(9月11日号)发售  
> 「Angel Heart」第341话/封面  
> 【涙の理由】  
>   
### ●2009年08月21日(金)  
> 汽车Mini的专业杂志**『MINI Freak(ミニ・フリーク)』**No.108号（2009年10月号） 发售  
> →Natsume社・定价1300日元（含税）  
> ▽特集「对你来说，Mini是什么？」中獠登場  
> →「我问了CityHunter的冴羽獠!」  
> ▽附带北条先生的comment  
>   
### ●2009年08月21日(金)  
> Comic Bunch第38号(9月4日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2009年08月07日(金)  
> Comic Bunch第36・37合并特刊(8月21日・28日号)发售  
> 「Angel Heart」第340話  
> 【小さな依頼人】  
>   
### ●2009年07月31日(金)  
> Comic Bunch第35号(8月14日号)发售  
> 「Angel Heart」第339話  
> 【繋いだ手から】  
>   
### ●2009年07月24日(金)  
> Comic Bunch第34号(8月7日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2009年07月17日(金)  
> Comic Bunch第33号(7月31日号)发售  
> 「Angel Heart」第338話  
> 【空のＣ・Ｈ(シティハンター)】  
>   
### ●2009年07月10日(金)  
> Comic Bunch第32号(7月24日号)发售  
> 「Angel Heart」第337話  
> 【ちょっとの特別】  
>   
### ●2009年07月09日(木)  
> Bunch Comics**『Angel Heart』[第30巻](./book_bc.md#30)** 发售  
> →定价540日元（含税）  
> ▽封面是Comic Bunch新年3号封面的图案  
> →初回限定特典！記念postcard(共3种)  
>   
### ●2009年07月03日(金)  
> Comic Bunch第31号(7月17日号)发售  
> 「Angel Heart」第336話  
> 【ふたつの命】  
>   
### ●2009年06月26日(金)  
> Comic Bunch第30号(7月10日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2009年06月19日(金)  
> Comic Bunch第29号(7月3日号)发售  
> 「Angel Heart」第335話  
> 【カエルの子はカエル】  
>   
### ●2009年06月12日(金)  
> Comic Bunch第28号(6月26日号)发售  
> 「Angel Heart」第334话/封面  
> 【天使のサイモン】  
>   
### ●2009年06月05日(金)  
> Comic Bunch第27号(6月19日号)发售  
> 「Angel Heart」第333話  
> 【いつかの笑顔】  
>   
### ●2009年05月29日(金)  
> Comic Bunch第26号(6月12日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2009年05月22日(金)  
> Comic Bunch第25号(6月5日号)发售  
> 「Angel Heart」第332話  
> 【君だけのヒーロー】  
>   
### ●2009年05月15日(金)  
> Comic Bunch第24号(5月29日号)发售  
> 「Angel Heart」第331話  
> 【只今参上！】  
> ▽Bunch8周年記念特別Column  
> STORY Digest・作家陣 Special Message  
> →全作品同時刊登  
>   
### ●2009年05月08日(金)  
> Comic Bunch第23号(5月22日号)发售  
> 「Angel Heart」第330話  
> 【人質交換】  
>   
### ●2009年04月24日(金)  
> Comic Bunch第21・22合并特刊(5月8日・15日号)发售  
> 「Angel Heart」第329話  
> 【潜入！ ＣＨ】  
>   
### ●2009年04月17日(金)  
> Comic Bunch第20号(5月1日号)发售  
> 「Angel Heart」第328話  
> 【敵情視察】  
>   
### ●2009年04月10日(金)  
> Comic Bunch第19号(4月24日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2009年04月03日(金)  
> Comic Bunch第18号(4月17日号)发售  
> 「Angel Heart」第327話  
> 【昼行灯(ひるあんどん)】  
>   
### ●2009年03月27日(金)  
> Comic Bunch第17号(4月10日号)发售  
> 「Angel Heart」第326話  
> 【対極の男】  
>   
### ●2009年03月19日(木)  
> Comic Bunch第16号(4月3日号)发售  
> 「Angel Heart」第325話  
> 【老人(かれ)の貴物(おもいで)】  
>   
### ●2009年03月13日(金)  
> Comic Bunch第15号(3月27日号)发售  
> 「Angel Heart」第324話  
> 【笑顔の理由】  
> ▽第16号的发售日是3月19日(木)  
>   
### ●2009年03月09日(月)  
> Bunch Comics**『Angel Heart』[第29巻](./book_bc.md#29)** 发售  
> →定价540日元（含税）  
> ▽封面是Comic Bunch'08年第43号封面的图案  
>   
### ●2009年03月06日(金)  
> Comic Bunch第14号(3月20日号)发售  
> 「Angel Heart」第323话/封面  
> 【怪訝なＸＹＺ】  
>   
### ●2009年02月27日(金)  
> Comic Bunch第13号(3月13日号)发售  
> 「Angel Heart」暂停。  
> ▽下期出现在封面上，新篇章Start  
>   
### ●2009年02月20日(金)  
> Comic Bunch第12号(3月6日号)发售  
> 「Angel Heart」第322話  
> 【カメレオンの遺産】  
>   
### ●2009年02月13日(金)  
> Comic Bunch第11号(2月27日号)发售  
> 「Angel Heart」第321话/封面  
> 【激昂】  
>   
### ●2009年02月06日(金)  
> Comic Bunch第10号(2月20日号)发售  
> 「Angel Heart」第320話  
> 【制裁の行方】  
>   
### ●2009年01月30日(金)  
> Comic Bunch第9号(2月13日号)发售  
> 「Angel Heart」第319話  
> 【黒幕】  
>   
### ●2009年01月23日(金)  
> Comic Bunch第8号(2月6日号)发售  
> 「Angel Heart」第318話  
> 【現場復帰】  
>   
### ●2009年01月16日(金)  
> Comic Bunch第7号(1月30日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2009年01月09日(金)  
> Comic Bunch新年6号(1月23日号)发售  
> 「Angel Heart」第317話  
> 【現行犯逮捕】  
> ▽2009新春、全作家阵容・亲笔Goods present  
> →sign彩纸 & message扇子  
>   
### ●2008年12月26日(金)  
> Comic Bunch新年4・5合并特刊(1月16日・21日号)发售  
> 「Angel Heart」第316話  
> 【冴子の覚悟】  
>   
### ●2008年12月19日(金)  
> Comic Bunch新年3号(1月9日号)发售  
> 「Angel Heart」第315话/封面  
> 【仕組まれた黙秘】  
> ▽特別附录・AH特製Book Cover   
>   
### ●2008年12月09日(火)  
> Bunch Comics**『Angel Heart』[第28巻](./book_bc.md#28)** 发售  
> →定价540日元（含税）  
> ▽封面是Comic Bunch第39号扉页的图案  
>   
### ●2008年12月05日(金)  
> Comic Bunch新年1・2合并特刊(1月1日・4日号)发售  
> 「Angel Heart」第314話  
> 【怪我の功名】  
> ▽特別附录2009年Bunch特製Calendar  
> →11月・12月系AH  
>   
### ●2008年11月28日(金)  
> Comic Bunch第52号(12月12日号)发售  
> 「Angel Heart」第313話  
> 【長い一日】  
> ▽AH Original Zippo(一次2种)  
> 　Bunch在移动SHOP中确定发售  
> →详见12/9、在网站上发布  
>   
### ●2008年11月21日(金)  
> Comic Bunch第51号(12月5日号)发售  
> 「Angel Heart」第312話  
> 【影武者】  
>   
### ●2008年11月14日(金)  
> Comic Bunch第50号(11月28日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2008年11月07日(金)  
> Comic Bunch第49号(11月21日号)发售  
> 「Angel Heart」第311話  
> 【宣戦布告】  
>   
### ●2008年10月31日(金)  
> Comic Bunch第48号(11月14日号)发售  
> 「Angel Heart」第310話  
> 【真実(マコト)】  
>   
### ●2008年10月24日(金)  
> Comic Bunch第47号(11月7日号)发售  
> 「Angel Heart」第309話  
> 【離れていても】  
>   
### ●2008年10月17日(金)  
> Comic Bunch第46号(10月31日号)发售  
> 「Angel Heart」第308話  
> 【新婚旅行】  
>   
### ●2008年10月10日(金)  
> Comic Bunch第45号(10月24日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2008年10月03日(金)  
> Comic Bunch第44号(10月17日号)发售  
> 「Angel Heart」第307話  
> 【誓いの言葉】  
>   
### ●2008年09月26日(金)  
> Comic Bunch第43号(10月10日号)发售  
> 「Angel Heart」第306话/封面  
> 【幸せのベール】  
> ▽「Cat's Eye」Pachislo特集  
>   
### ●2008年09月19日(金)  
> Comic Bunch第42号(10月3日号)发售  
> 「Angel Heart」第305話  
> 【確かな鼓動】  
>   
### ●2008年09月12日(金)  
> Comic Bunch第41号(9月26日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2008年09月09日(火)  
> Bunch Comics**『Angel Heart』[第27巻](./book_bc.md#27)** 发售  
> →定价540日元（含税）  
> ▽封面是Comic Bunch第32号 封面的图案  
>   
### ●2008年09月05日(金)  
> Comic Bunch第40号(9月19日号)发售  
> 「Angel Heart」第304话/封面  
> 【似た者同士】  
>   
### ●2008年08月29日(金)  
> Comic Bunch第39号(9月12日号)发售  
> 「Angel Heart」第303话/封面/封面画  
> 【金木犀の香り】  
>   
### ●2008年08月22日(金)  
> Comic Bunch第38号(9月5日号)发售  
> 「Angel Heart」第302話  
> 【身代わり】  
>   
### ●2008年08月08日(金)  
> Comic Bunch第36・37合并特刊(8月22日・29日号)发售  
> 「Angel Heart」第301話  
> 【潜入】  
>   
### ●2008年08月01日(金)  
> Comic Bunch第35号(8月15日号)发售  
> 「Angel Heart」第300話  
> 【恋の季節】  
>   
### ●2008年07月25日(金)  
> Comic Bunch第34号(8月8日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2008年07月18日(金)  
> Comic Bunch第33号(8月1日号)发售  
> 「Angel Heart」第299話  
> 【陰影】  
>   
### ●2008年07月11日(金)  
> Comic Bunch第32号(7月25日号)发售  
> 「Angel Heart」第298话/封面  
> 【幸せの涙】  
>   
### ●2008年07月04日(金)  
> Comic Bunch第31号(7月18日号)发售  
> 「Angel Heart」第297話  
> 【親子の時間】  
>   
### ●2008年06月27日(金)  
> Comic Bunch第30号(7月11日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2008年06月20日(金)  
> Comic Bunch第29号(7月4日号)发售  
> 「Angel Heart」第296話  
> 【本当の姿】  
>   
### ●2008年06月13日(金)  
> Comic Bunch第28号(6月27日号)发售  
> 「Angel Heart」第295話  
> 【最期の言葉】  
> ▽AH event 报告→委托内容的部分介绍  
>   
### ●2008年06月06日(金)  
> Comic Bunch第27号(6月20日号)发售  
> 「Angel Heart」第294話  
> 【懺悔と供養】  
> ▽AH1500万部突破記念 event 報告  
>   
### ●2008年05月30日(金)  
> Comic Bunch第26号(6月13日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2008年05月23日(金)  
> Comic Bunch第25号(6月6日号)发售  
> 「Angel Heart」第293話  
> 【傲慢】  
> ▽Comics联动・AH专用frame申请用纸  
>   
### ●2008年05月16日(金)  
> Comic Bunch第24号(5月30日号)发售  
> 「Angel Heart」第292話  
> 【笑い泣き】  
> ▽Comics联动・AH专用frame申请用纸  
> ▽創刊7周年記念・Bunch复制原画集present  
>   
### ●2008年05月09日(金)  
> Bunch Comics**『Angel Heart』[第26巻](./book_bc.md#26)** 发售  
> ▽封面是Comic Bunch'07年第24号封面的图案  
> ▽该杂志联动AH fan 感恩节・第2弾  
> →「AH pins & 专用frame」申请券  
>   
### ●2008年05月09日(金)  
> Comic Bunch第23号(5月23日号)发售  
> 「Angel Heart」番外編/封面  
> 【一夜の友情】  
> →「新宿鮫」大沢在昌 Special 合作  
> ▽AH累計1500万部突破記念号  
> ▽TOP3 CROSS TALK  
> →北条司×井上雄彦×梅澤春人  
> ▽特別附录・AH特制图册  
> ▽ event 「Memory of X・Y・Z」詳細  
> ▽Comics联动・AH特製frame申请用纸  
> 手机Bunch所有申请者免费present  
> →AH专用手机防偷窥贴纸  
> ▽AH Giclee 版画 Collection限量版销售  
>   
### ●2008年04月25日(金)  
> Comic Bunch第21・22合并特刊(5月9日・16日号)发售  
> 「Angel Heart」第291話  
> 【罪悪感】  
> ▽北条司先生sign会決定(5/17)  
> ▽5月中旬开始在新宿举行活动  
> →「Memory of X・Y・Z」  
> ▽下期(5/9发售)AH Comics  
> 累計1500万部突破記念号  
> →北条司大特集 SPECIAL2大企划  
>   
### ●2008年04月18日(金)  
> Comic Bunch第20号(5月2日号)发售  
> 「Angel Heart」第290話  
> 【損な役回り】  
>   
### ●2008年04月11日(金)  
> Comic Bunch第19号(4月25日号)发售  
> 「Angel Heart」第289话/封面  
> 【ロスタイム】  
>   
### ●2008年04月04日(金)  
> Comic Bunch第18号(4月18日号)发售  
> 「Angel Heart」第288話  
> 【家族のために】  
>   
### ●2008年03月28日(金)  
> Comic Bunch第17号(4月11日号)发售  
> 「Angel Heart」第287話  
> 【小さなＸＹＺ】  
>   
### ●2008年03月21日(金)  
> Comic Bunch第16号(4月4日号)发售  
> 「Angel Heart」暂停。  
> ▽下期、新的发展正在到来  
>   
### ●2008年03月14日(金)  
> Comic Bunch第15号(3月28日号)发售  
> 「Angel Heart」第286話  
> 【潮騒】  
>   
### ●2008年03月07日(金)  
> Comic Bunch第14号(3月21日号)发售  
> 「Angel Heart」第285話  
> 【不安な夜】  
>   
### ●2008年02月29日(金)  
> Comic Bunch第13号(3月14日号)发售  
> 「Angel Heart」第284話  
> 【ジンクス】  
>   
### ●2008年02月22日(金)  
> Comic Bunch第12号(3月7日号)发售  
> 「Angel Heart」暂停。  
> ▽第25巻联动AH fan 感恩节・申请用纸  
>   
### ●2008年02月15日(金)  
> Comic Bunch第11号(2月29日号)发售  
> 「Angel Heart」第283話  
> 【灯(あかり)】  
> ▽第25巻联动AH fan 感恩节・申请用纸  
>   
### ●2008年02月09日(土)  
> Bunch Comics**『Angel Heart』[第25巻](./book_bc.md#25)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch2007年第50号 封面的图案  
>   
> ▽该杂志・Comics联动「AH fan感恩节」開始  
> 第一弾、「AH特制镊子5件套装」応募者全員service  
> 「Comic Bunch10号(2/8发售)、11号(2/15发售)、12号(2/22发售)任何一个申请用纸」＋「Comics25巻腰封的申请券」＋「1000日元的邮政定额小汇」为一组申请。  
> →截止日期、2008年03月10日(月)<font color=#ff0000>必须送达</font>。详情见腰封。  
>   
### ●2008年02月08日(金)  
> Comic Bunch第10号(2月22日号)发售  
> 「Angel Heart」第282话/封面  
> 【カメレオン動く】  
> ▽AH fan 感恩节・申请者全员service  
> →第25巻联动、AH特制镊子5件套装  
>   
### ●2008年02月01日(金)  
> Comic Bunch第9号(2月15日号)发售  
> 「Angel Heart」第281話  
> 【ぷるん】  
>   
### ●2008年01月25日(金)  
> Comic Bunch第8号(2月8日号)发售  
> 「Angel Heart」第280話  
> 【母の想い】  
>   
### ●2008年01月18日(金)  
> Comic Bunch新年7号(2月1日号)发售  
> 「Angel Heart」第279話  
> 【一億の勝負】  
> ▽Bunch所有作品T-shirt新春present  
> →在design garden自由创作  
>   
### ●2008年01月10日(木)  
> Comic Bunch新年6号(1月31日号)发售  
> 「Angel Heart」第278話  
> 【最凶悪危険人物】  
> ▽Signed彩色纸和其他新年礼物  
>   
### ●2007年12月27日(木)  
> Comic Bunch新年4・5合并特刊(1月21日・25日号)发售  
> 「Angel Heart」第277话/封面  
> 【虎の尾】  
> ▽北条司全方位专题 Special Interview  
> ▽特別附录illustrated.Book「ライアのまど」（译注：ライア：Lyra/Riah。まど：Mado/窗户）  
> ▽CH JOURNAL：Pachislo CH  
> →围板、海报等present  
>   
### ●2007年12月21日(金)  
> Comic Bunch新年3号(1月14日号)发售  
> 「Angel Heart」暂停。  
> ▽下期27日发售、北条司特集  
> →Interview ＆特別附录「ライアのまど」  
>   
### ●2007年12月07日(金)  
> Comic Bunch新年1・2合并特刊(1月1日・4日号)发售  
> 「Angel Heart」第276話  
> 【招かれざる客】  
> ▽CH单卷release纪念・CH JOURNAL  
> ▽下期休載。4・5合并号系北条司全方位专题  
> illustrated.Book「ライアのまど」present  
>   
### ●2007年11月30日(金)  
> Comic Bunch第53号(12月14日号)发售  
> 「Angel Heart」第275話  
> 【カメレオンの罠】  
>   
### ●2007年11月22日(木)  
> Comic Bunch第52号(12月7日号)发售  
> 「Angel Heart」第274話  
> 【葉月の悩み】  
>   
### ●2007年11月16日(金)  
> Comic Bunch第51号(11月30日号)发售  
> 「Angel Heart」第273話  
> 【親子の愛】  
>   
### ●2007年11月09日(金)  
> Comic Bunch第50号(11月23日号)发售  
> 「Angel Heart」第272话/封面  
> 【大好きなんです】  
>   
### ●2007年11月09日(金)  
> Bunch Comics**『Angel Heart』[第24巻](./book_bc.md#24)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch第30号 巻首彩色扉页的图案  
>   
### ●2007年11月02日(金)  
> Comic Bunch第49号(11月16日号)发售  
> 「Angel Heart」暂停。  
> ▽下期AH封面  
>   
### ●2007年10月26日(金)  
> Comic Bunch第48号(11月9日号)发售  
> 「Angel Heart」第271話  
> 【キバナコスモス】  
>   
### ●2007年10月19日(金)  
> Comic Bunch第47号(11月2日号)发售  
> 「Angel Heart」第270話  
> 【ファルコンの秘密】  
>   
### ●2007年10月12日(金)  
> Comic Bunch第46号(10月26日号)发售  
> 「Angel Heart」第269話  
> 【墓参り】  
> ▽CH单卷DVD发售決定  
> →12月19日起计划每月发布(全26巻)  
>   
### ●2007年10月05日(金)  
> Comic Bunch第45号(10月19日号)发售  
> 「Angel Heart」第268话/封面  
> 【ファルコンのお守り】  
>   
### ●2007年09月28日(金)  
> Comic Bunch第44号(10月12日号)发售  
> 「Angel Heart」第267話  
> 【葉月の父】  
>   
### ●2007年09月21日(金)  
> Comic Bunch第43号(10月5日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2007年09月14日(金)  
> Comic Bunch第42号(9月28日号)发售  
> 「Angel Heart」第266話  
> 【誤解】  
>   
### ●2007年09月07日(金)  
> Comic Bunch第41号(9月21日号)发售  
> 「Angel Heart」第265話  
> 【海坊主と葉月】  
>   
### ●2007年08月31日(金)  
> Comic Bunch第40号(9月14日号)发售  
> 「Angel Heart」第264話  
> 【夏の再会】  
>   
### ●2007年08月24日(金)  
> Comic Bunch第39号(9月7日号)发售  
> 「Angel Heart」第263話  
> 【私の立ち位置】  
>   
### ●2007年08月10日(金)  
> Comic Bunch第37・38合并特刊(8月24・31日号)发售  
> 「Angel Heart」第262話  
> 【仁志の涙】  
> ▽Bunch创刊300号纪念present  
> →AH signed 彩纸，北条先生推荐的红葡萄酒&白葡萄酒  
>   
### ●2007年08月09日(木)  
> Bunch Comics**『Angel Heart』[第23巻](./book_bc.md#23)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch新年1・2合并特刊 巻首彩色扉页的图案  
>   
### ●2007年08月03日(金)  
> Comic Bunch第36号(8月17日号)发售  
> 「Angel Heart」第261話  
> 【武道館決戦】  
>   
### ●2007年07月27日(金)  
> Comic Bunch第35号(8月10日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2007年07月20日(金)  
> Comic Bunch第34号(8月3日号)发售  
> 「Angel Heart」第260話  
> 【消えない記憶】  
>   
### ●2007年07月13日(金)  
> Comic Bunch第33号(7月27日号)发售  
> 「Angel Heart」第259話  
> 【香莹の怒り】  
>   
### ●2007年07月06日(金)  
> Comic Bunch第32号(7月20日号)发售  
> 「Angel Heart」第258話  
> 【及第点の答え】  
>   
### ●2007年06月29日(金)  
> Comic Bunch第31号(7月13日号)发售  
> 「Angel Heart」第257話  
> 【陳さんの暴走】  
>   
### ●2007年06月22日(金)  
> Comic Bunch第30号(7月6日号)发售  
> 「Angel Heart」第256话/封面/封面画  
> 【新スポンサー】  
>   
### ●2007年06月15日(金)  
> Comic Bunch第29号(6月29日号)发售  
> 「Angel Heart」暂停。  
> ▽下周30号AH封面＆卷首彩页  
>   
### ●2007年06月08日(金)  
> Comic Bunch第28号(6月22日号)发售  
> 「Angel Heart」第255話  
> 【仲間】  
>   
### ●2007年06月01日(金)  
> Comic Bunch第27号(6月15日号)发售  
> 「Angel Heart」第254話  
> 【嫉妬】  
>   
### ●2007年05月25日(金)  
> Comic Bunch第26号(6月8日号)发售  
> 「Angel Heart」第253話  
> 【表への一歩】  
>   
### ●2007年05月18日(金)  
> Comic Bunch第25号(6月1日号)发售  
> 「Angel Heart」暂停。  
> ▽Comics联动、DVD BOX4 present申请券(15名)  
>   
### ●2007年05月11日(金)  
> Comic Bunch第24号(5月25日号)发售  
> 「Angel Heart」第252话/封面  
> 【初めての経験】  
> ▽Comics联动、DVD BOX4 present申请券(15名)  
>   
### ●2007年05月09日(水)  
> Bunch Comics**『Angel Heart』[第22巻](./book_bc.md#22)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch新年5・6合并特刊 封面的图案  
>   
> ▽该杂志・Comics联动企划「AH DVD Premium Box Vol.4 present」／抽取15名  
> 用Comic Bunch第24期（5/11发售）&第25期（5/18发售）的申请券中的任意2张，和Comic第22卷腰封的1张申请券，共计2张作为一组进行申请。  
> →截止日期、2007年06月04日(月)<font color=#ff0000>必须送达</font>。详情见腰封。  
>   
### ●2007年04月27日(金)  
> Comic Bunch第22・23合并特刊(5月11日・18日号)发售  
> 「Angel Heart」第251話  
> 【意外な弱点】  
>   
### ●2007年04月20日(金)  
> Comic Bunch第21号(5月4日号)发售  
> 「Angel Heart」第250話  
> 【暖かい場所へ】  
>   
### ●2007年04月13日(金)  
> Comic Bunch第20号(4月27日号)发售  
> 「Angel Heart」第249話  
> 【見えない景色】  
>   
### ●2007年04月06日(金)  
> Comic Bunch第19号(4月20日号)发售  
> 「Angel Heart」第248話  
> 【気づかぬ優しさ】  
>   
### ●2007年03月30日(金)  
> Comic Bunch第18号(4月13日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2007年03月23日(金)  
> Comic Bunch第17号(4月6日号)发售  
> 「Angel Heart」第247話  
> 【悲痛な願い】  
>   
### ●2007年03月16日(金)  
> Comic Bunch第16号(3月30日号)发售  
> 「Angel Heart」第246話  
> 【雲のように】  
>   
### ●2007年03月09日(金)  
> Comic Bunch第15号(3月23日号)发售  
> 「Angel Heart」第245話  
> 【初アフターはストーカー付！】  
> ▽ Yahoo AH联动企划  
> →实物大小100t Hammer 、Yahoo Auction出品  
>   
### ●2007年03月02日(金)  
> Comic Bunch第14号(3月16日号)发售  
> 「Angel Heart」第244話  
> 【恋はアフターで！】  
> ▽ Yahoo AH联动企划→什么是智囊？  
>   
### ●2007年02月23日(金)  
> Comic Bunch第13号(3月9日号)发售  
> 「Angel Heart」第243話  
> 【覚悟】  
> ▽ Yahoo 联动企划・AH Baton活用术 (译注：Baton：接力棒)   
>   
### ●2007年02月16日(金)  
> Comic Bunch第12号(3月2日号)发售  
> 「Angel Heart」第242話  
> 【香莹の答え】  
> ▽AH×Yahoo!JAPAN联动企划  
> Angel Heart  Special OPEN  
> →http://streaming.yahoo.co.jp/special/anime/angelheart/  
>   
### ●2007年02月09日(金)  
> Comic Bunch第11号(2月23日号)发售  
> 「Angel Heart」暂停。  
> ▽AH×Yahoo!JAPAN联动企划  
> →2/16,Yahoo!动画AH特集page登场  
>  scoop 1. Yahoo! auction surprise item出品 ←那个巨大的item以实物大小登场！？  
>  scoop 2. AH第1话可以在话题的新Media上阅读  
>  scoop 3. Yahoo! JAPAN的人气corner也有北条先生参加  
> 其他人气contents将被染成angel的颜色  
>   
### ●2007年02月02日(金)  
> Comic Bunch第10号(2月16日号)发售  
> 「Angel Heart」第241話  
> 【狙撃】  
> ▽Angel Heart × Yahoo! JAPAN 绝密项目进行中，详情见下期  
>   
### ●2007年01月26日(金)  
> Comic Bunch第9号(2月9日号)发售  
> 「Angel Heart」第240話  
> 【カメレオンの誘惑】  
> ▽Comics联动、DVD BOX3 present申请券(15名)  
>   
### ●2007年01月19日(金)  
> Comic Bunch第8号(2月2日号)发售  
> 「Angel Heart」第239話  
> 【乙玲(イーリン)の姉】  
> ▽Comics联动、DVD BOX3 present申请券(15名)  
>   
### ●2007年01月12日(金)  
> Comic Bunch新年7号(1月31日号)发售  
> 「Angel Heart」第238話  
> 【花園学校の未来】  
> ▽Comics联动、DVD BOX3 present申请券(15名)  
>   
### ●2007年01月09日(火)  
> Bunch Comics**『Angel Heart』[第21巻](./book_bc.md#21)** 发售  
> →定价・通常版530日元、包含特制腰封的初版限量版780元（含税）  
> ▽封面是Comic Bunch'06年第47号封面的图案  
>   
> →两个最重要的腰封是「しら乌鸦」&「100t Hammer」（译注：待校对）  
>   
> ▽该杂志・Comics联动企划「AH DVD Premium Box Vol.3 present」／抽取15名  
> Comic Bunch5・6合并号(12/28发售)＆7号(1/12发售)＆8号(1/19发售)＆9号(1/26发售)的申请券任选2张、Comics 21巻腰封的申请券1枚、共3张成一组申请。  
> →截止日期、2007年02月05日(金)<font color=#ff0000>必须送达</font>。详情见腰封。  
>   
### ●2006年12月28日(木)  
> Comic Bunch新年5・6合并特刊(1月22日・26日号)发售  
> 「Angel Heart」第237话/封面  
> 【カメレオン】  
> ▽卷头Color企划・有奖的AH测试  
> ▽特別附录・AH Comics Cover   
> 第21卷于1月9日发售→特制腰封包括在初版限量版中  
> ▽Comics联动、DVD BOX3 present申请券(15名)  
>   
### ●2006年12月22日(金)  
> Comic Bunch新年4号(1月19日号)发售  
> 「Angel Heart」第236話  
> 【危険な出会い】  
> ▽下期5・6合并号发售于12月28日(木)  
> →下期AH封面、AH Comics Cover 附录  
> ＆AH审定召开  
>   
### ●2006年12月15日(金)  
> Comic Bunch新年3号(1月12日号)发售  
> 「Angel Heart」第235話  
> 【肉弾戦】  
>   
### ●2006年12月01日(金)  
> Comic Bunch新年1・2合并特刊(1月1日・5日号)发售  
> 「Angel Heart」第234話/封面画  
> 【いい子の行き着く場所】  
>   
### ●2006年11月24日(金)  
> Comic Bunch第52号(12月8日号)发售  
> 「Angel Heart」暂停。  
> ▽下期、新年1・2合并号AH巻首Color  
>   
### ●2006年11月17日(金)  
> Comic Bunch第51号(12月1日号)发售  
> 「Angel Heart」第233話  
> 【嫌な予感】  
>   
### ●2006年11月10日(金)  
> Comic Bunch第50号(11月24日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2006年11月02日(木)  
> Comic Bunch第49号(11月17日号)发售  
> 「Angel Heart」第232話  
> 【机男と親衛隊(シンエータイ)！】  
>   
### ●2006年10月27日(金)  
> Comic Bunch第48号(11月10日号)发售  
> 「Angel Heart」第231話  
> 【ボクら、シャンイン親衛隊！？】  
>   
### ●2006年10月20日(金)  
> Comic Bunch第47号(11月3日号)发售  
> 「Angel Heart」第230话/封面  
> 【ラブレター・パニック！】  
>   
### ●2006年10月13日(金)  
> Comic Bunch第46号(10月27日号)发售  
> 「Angel Heart」第229話  
> 【想い、街に息づいて】  
>   
### ●2006年10月06日(金)  
> Comic Bunch第45号(10月20日号)发售  
> 「Angel Heart」第228話  
> 【私たちの学校】  
>   
### ●2006年09月29日(金)  
> Comic Bunch第44号(10月13日号)发售  
> 「Angel Heart」第227話  
> 【香莹、入学す！】  
> ▽Anime AH News、 Chief P 諏訪的评论  
>   
### ●2006年09月25日(月) 读卖TV/25:25～  
### ●2006年09月26日(火) 日本TV/25:59～  
> 动画Angel Heart #50  
> 「Last Present」  
> →最終回  
> ▽DVD BOX2,  soundtrack CD present  
>   
### ●2006年09月22日(金)  
> Comic Bunch第43号(10月6日号)发售  
> 「Angel Heart」第226話  
> 【真実と紗世】  
> ▽Anime AH News、最終話提要和要点  
>   
### ●2006年09月18日(月) 读卖TV/25:25～  
### ●2006年09月19日(火) 日本TV/25:29～  
> 动画Angel Heart #49  
> 「Get My Life」  
> →下回「Last Present」  
>   
### ●2006年09月15日(金)  
> Comic Bunch第42号(9月29日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2006年09月11日(月) 读卖TV/24:55～  
### ●2006年09月12日(火) 日本TV/25:29～  
> 动画Angel Heart #48  
> 「引き寄せられる運命」  
> →下回「Get My Life」  
>   
### ●2006年09月09日(土)  
> Bunch Comics**『Angel Heart』[第20巻](./book_bc.md#20)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch第34号 封面的图案  
>   
### ●2006年09月08日(金)  
> Comic Bunch第41号(9月22日号)发售  
> 「Angel Heart」第225话/封面  
> 【父のいる店】  
>   
### ●2006年09月04日(月) 读卖TV/24:55～  
### ●2006年09月05日(火) 日本TV/26:59～  
> 动画Angel Heart #47  
> 「アカルイミライ！？」  
> →下回「引き寄せられる運命」  
>   
### ●2006年09月01日(金)  
> Comic Bunch第40号(9月15日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2006年08月28日(月) 读卖TV/25:25～  
### ●2006年08月29日(火) 日本TV/25:29～  
> 动画Angel Heart #46  
> 「マザー・ハート」  
> →下回「アカルイミライ！？」  
>   
### ●2006年08月25日(金)  
> Comic Bunch第39号(9月8日号)发售  
> 「Angel Heart」第224話  
> 【重ねる嘘、重なる罪】  
>   
### ●2006年08月21日(月) 读卖TV/25:10～  
### ●2006年08月22日(火) 日本TV/26:14～  
> 动画Angel Heart #45  
> 「人間核弾頭、楊(ヤン)」  
> →下回「マザー・ハート」  
>   
### ●2006年08月14日(月) 读卖TV/24:55～  
### ●2006年08月15日(火) 日本TV/25:29～  
> 动画Angel Heart #44  
> 「俺たちの子供のために」  
> →下回「人間核弾頭、楊(ヤン)」  
>   
### ●2006年08月11日(金)  
> Comic Bunch第37・38合并特刊(8月15日・9月1日号)发售  
> 「Angel Heart」第223話  
> 【紗世は名探偵！？】  
> ▽Anime AH News、第44话简介  
> ▽7/29 天保山AH event 的情况  
>   
### ●2006年08月07日(月) 读卖TV/25:14～  
### ●2006年08月08日(火) 日本TV/25:29～  
> 动画Angel Heart #43  
> 「私が生きる日常」  
> →下回「俺たちの子供のために」  
>   
### ●2006年08月04日(金)  
> Comic Bunch第36号(8月18日号)发售  
> 「Angel Heart」第222話  
> 【カギは香莹にあり！？】  
> ▽Anime AH News、第43話提要和要点  
> →新OP theme CD present(20名)  
>   
### ●2006年07月31日(月) 读卖TV/25:00～  
### ●2006年08月01日(火) 日本TV/25:29～  
> 动画Angel Heart #42  
> 「二人だけのサイン」  
> →下回「私が生きる日常」  
>   
### ●2006年07月28日(金)  
> Comic Bunch第35号(8月11日号)发售  
> 「Angel Heart」第221話  
> 【１８歳はシゲキがお好き！？】  
> ▽Anime AH News、第42話提要和要点  
> ▽令人兴奋的宝岛AH活动通知  
>   
### ●2006年07月24日(月) 读卖TV/25:09～  
### ●2006年07月25日(火) 日本TV/25:38～  
> 动画Angel Heart #41  
> 「自分の居場所」  
> →下回「二人だけのサイン」  
>   
### ●2006年07月21日(金)  
> Comic Bunch第34号(8月4日号)发售  
> 「Angel Heart」第220话/封面  
> 【謎のお父さん！】  
> ▽紧急通知・令人兴奋的宝岛AH活动  
> →7/29(土)13:00～  
> 由一位出色的音乐家现场演奏&  
> 举办川崎真央Talk Show  
>   
### ●2006年07月17日(月) 读卖TV/25:04～  
### ●2006年07月18日(火) 日本TV/25:38～  
> 动画Angel Heart #40  
> 「ミキの隠された秘密」  
> →下回「自分の居場所」  
>   
### ●2006年07月14日(金)  
> Comic Bunch第33号(7月28日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2006年07月10日(月) 读卖TV/25:25～  
### ●2006年07月11日(火) 日本TV/25:29～  
> 动画Angel Heart #39  
> 「依頼者は大女優」  
> 新OP theme「Battlefield of Love」  
> →下回「ミキの隠された秘密」  
>   
### ●2006年07月07日(金)  
> Comic Bunch第32号(7月21日号)发售  
> 「Angel Heart」第219話  
> 【私がパパです！】  
> ▽Anime AH News、第39話提要和要点  
>   
### ●2006年07月03日(月)读卖TV/25:25～  
### ●2006年07月04日(火) 日本TV/25:29～  
> 动画Angel Heart #38  
> 「オレの目になってくれ」  
> →下回「依頼者は大女優」  
>   
### ●2006年06月30日(金)  
> Comic Bunch第31号(7月14日号)发售  
> 「Angel Heart」第218話  
> 【大幸運期到来！！】  
>   
### ●2006年06月26日(月) 读卖TV/24:55～  
### ●2006年06月27日(火) 日本TV/25:29～  
> 动画Angel Heart #37  
> 「汚れのない心」  
> →下回「オレの目になってくれ」  
>   
### ●2006年06月23日(金)  
> Comic Bunch第30号(7月7日号)发售  
> 「Angel Heart」第217話  
> 【楊の生きる理由】  
> ▽Anime AH News、第37話提要和要点  
> ▽天宝山10天令人兴奋的宝岛2006的AH活动  
> →7/28(金)～8/6(日)  
>   
### ●2006年06月19日(月) 读卖TV/25:16～  
### ●2006年06月20日(火) 日本TV/25:29～  
> 动画Angel Heart #36  
> 「幸せを運ぶ女の子」  
> →下回「汚れのない心」  
>   
### ●2006年06月16日(金)  
> Comic Bunch第29号(6月30日号)发售  
> 「Angel Heart」第216話  
> 【追う女、待つ女】  
> ▽Anime AH News、第36话简介  
> →Comics联动・DVD等present(150名)申请券  
>   
### ●2006年06月12日(月)读卖TV/24:55～  
### ●2006年06月13日(火) 日本TV/25:29～  
> 动画Angel Heart #35  
> 「未来へ…」  
> →下回「幸せを運ぶ女の子」  
>   
### ●2006年06月09日(金)  
> Comic Bunch第28号(6月23日号)发售  
> 「Angel Heart」第215话/封面  
> 【Ｃ・Ｈ資格テスト！】  
> ▽卷首Color企划，形成AH应援团  
> →Comics联动・DVD等present(150名)申请券  
> ▽Anime AH News、第35话简介  
>   
### ●2006年06月09日(金)  
> Bunch Comics**『Angel Heart』[第19巻](./book_bc.md#19)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch第22・23合并特刊 封面的图案  
>   
> ▽该杂志・Comics联动企划「AH DVD Premium Box Vol.2等Present」／抽取150名  
> （DVD15名・AH T-shirt9名・Original Key 环126名）  
> Comics腰封的申请券、Comic Bunch28号(6/9发售)＆29号(6/16发售)の申请券、共3张成一组申请。  
> →截止日期、2006年06月30日(金)<font color=#ff0000>必须送达</font>。详情见腰封。  
>   
### ●2006年06月05日(月)读卖TV/24:55～  
### ●2006年06月06日(火) 日本TV/25:29～  
> 动画Angel Heart #34  
> 「二人の決意」  
> →下回「未来へ…」  
>   
### ●2006年06月02日(金)  
> Comic Bunch第27号(6月16日号)发售  
> 「Angel Heart」第214話  
> 【オペレーション ナシクズシ！】  
>   
### ●2006年05月29日(月) 读卖TV/24:55～  
### ●2006年05月30日(火) 日本TV/25:29～  
> 动画Angel Heart #33  
> 「神から授かりし子」  
> →下回「二人の決意」  
>   
### ●2006年05月26日(金)  
> Comic Bunch第26号(6月9日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2006年05月22日(月) 读卖TV/24:55～  
### ●2006年05月23日(火) 日本TV/25:29～  
> 动画Angel Heart #32  
> 「組織から来た女」  
> →下回「神から授かりし子」  
>   
### ●2006年05月19日(金)  
> Comic Bunch第25号(6月2日号)发售  
> 「Angel Heart」第213話  
> 【男の性(サガ)！】  
> ▽Anime AH News、第32話提要和要点  
>   
### ●2006年05月15日(月) 读卖TV/24:55～  
### ●2006年05月16日(火) 日本TV/25:29～  
> 动画Angel Heart #31  
> 「最後の夜に見た奇跡」  
> →下回「組織から来た女」  
>   
### ●2006年05月12日(金)  
> Comic Bunch第24号(5月26日号)发售  
> 「Angel Heart」第212話  
> 【長生きの代償】  
> ▽創刊5周年企画・这部Bunch漫画太精彩了!  
> →AH最新动画改编信息＆令人难忘的委托Best5  
> ▽Anime AH News、第30話提要和要点  
>   
### ●2006年05月08日(月) 读卖TV/24:55～  
### ●2006年05月09日(火) 日本TV/25:59～  
> 动画Angel Heart #30  
> 「この街は私の全て」  
> →下回「最後の夜に見た奇跡」  
>   
### ●2006年05月01日(月) 读卖TV/24:55～  
### ●2006年05月02日(火) 日本TV/25:29～  
> 动画Angel Heart #29  
> 「私の妹…香」  
> →下回「この街は私の全て」  
>   
### ●2006年04月28日(金)  
> Comic Bunch第22・23合并特刊(5月12日・19日号)发售  
> 「Angel Heart」第211话/封面  
> 【男二人、車中にて】  
> ▽特别附录AH袋装premium postcard  
> ▽Anime AH News、第29話提要和要点  
>   
### ●2006年04月24日(月) 读卖TV/24:55～  
### ●2006年04月25日(火) 日本TV/25:29～  
> 动画Angel Heart #28  
> 「約束」  
> →下回「私の妹…香」  
>   
### ●2006年04月21日(金)  
> Comic Bunch第21号(5月5日号)发售  
> 「Angel Heart」第210話  
> 【マオの決断】  
> ▽Anime AH News、第28話提要和要点  
>   
### ●2006年04月17日(月) 读卖TV/25:25～  
### ●2006年04月18日(火) 日本TV/25:29～  
> 动画Angel Heart #27  
> 「私、恋してる！？」  
> →下回「約束」  
>   
### ●2006年04月14日(金)  
> Comic Bunch第20号(4月28日号)发售  
> 「Angel Heart」第209話  
> 【迸る想い！】  
> ▽Anime AH News、第27话简介  
> ▽玉置浩二 Special Interview  
> →新OP「Lion」CD present(20名)  
>   
### ●2006年04月10日(月) 读卖TV/25:30～  
### ●2006年04月11日(火) 日本TV/25:29～  
> 动画Angel Heart #26  
> 「もう一度あの頃に」  
> →下回「私、恋してる！？」  
>   
### ●2006年04月07日(金)  
> Comic Bunch第19号(4月21日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2006年04月03日(月) 读卖TV/25:30～  
### ●2006年04月04日(火) 日本TV/25:49～  
> 动画Angel Heart #25  
> 「死にたがる依頼者」  
> →下回「もう一度あの頃に」  
>   
### ●2006年03月31日(金)  
> Comic Bunch第18号(4月14日号)发售  
> 「Angel Heart」第208話  
> 【海辺の告白！】  
> ▽Anime AH News、第25话简介  
> ▽宇都宮隆×川崎真央 特別対談  
> →Signed AH特制clear file present(3名)  
>   
### ●2006年03月27日(月) 读卖TV/25:03～  
### ●2006年03月28日(火) 日本TV/26:25～  
> 动画Angel Heart #24  
> 「鼓動と共に…」  
> →下回「死にたがる依頼者」  
>   
### ●2006年03月24日(金)  
> Comic Bunch第17号(4月7日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2006年03月20日(月) 读卖TV/25:29～  
### ●2006年03月21日(火) 日本TV/25:25～  
> 动画Angel Heart #23  
> 「出発(たびだち)のメロディー」  
> →下回「鼓動と共に…」  
>   
### ●2006年03月17日(金)  
> Comic Bunch第16号(3月31日号)发售  
> 「Angel Heart」第207話  
> 【酒と泪(なみだ)と皇子と母親！？】  
> ▽Anime AH News、第23话简介  
> ▽Comics联动企划・DVD BOX present申请券  
>   
### ●2006年03月13日(月) 读卖TV/25:03～  
### ●2006年03月14日(火) 日本TV/25:25～  
> 动画Angel Heart #22  
> 「不公平な幸せ」  
> →下回「出発(たびだち)のメロディー」  
>   
### ●2006年03月10日(金)  
> Comic Bunch第15号(3月24日号)发售  
> 「Angel Heart」第206話  
> 【皇子の苦悩】  
> ▽Anime AH News、第22话简介  
> ▽Comics联动企划・DVD BOX present报名券  
>   
### ●2006年03月09日(木)  
> Bunch Comics**『Angel Heart』[第18巻](./book_bc.md#18)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch第8号 巻首彩色扉页的图案  
>   
> ▽本刊·漫画联动企划「AH DVD Premium Box Vol.1 present」/15名抽奖者  
> Comics带的报名券和Comic Bunch15号(3/10发售)&16号(3/17发售)的报名券，共3张成一组申请。  
> →截止日期是2006年3月31日（周五）<font color=#ff0000>必须到达</font>。 详情请见腰封。  
>   
### ●2006年03月06日(月) 读卖TV/25:03～  
### ●2006年03月07日(火) 日本TV/25:25～  
> 动画Angel Heart #21  
> 「哀しき守護者(ガーディアン)」  
> →下回「不公平な幸せ」  
>   
### ●2006年03月03日(金)  
> Comic Bunch第14号(3月17日号)发售  
> 「Angel Heart」第205話  
> 【日常に潜むアクマ】  
> ▽Anime AH News、第21話提要和要点  
> ▽<s>3/6(月)</s>、动画AH手机site open  
> →推迟到4月以后  
>   
### ●2006年02月27日(月) 读卖TV/25:03～  
### ●2006年02月28日(火) 日本TV/25:55～  
> 动画Angel Heart #20  
> 「宿命のプレリュード」  
> →下回「哀しき守護者(ガーディアン)」  
>   
### ●2006年02月24日(金)  
> Comic Bunch第13号(3月10日号)发售  
> 「Angel Heart」第204話  
> 【家族の風景】  
> ▽Anime AH News、第20話提要和要点  
>   
### ●2006年02月20日(月) 读卖TV/25:03～  
### ●2006年02月21日(火) 日本TV/25:25～  
> 动画Angel Heart #19  
> 「陳老人の店」  
> →下回「宿命のプレリュード」  
>   
### ●2006年02月17日(金)  
> Comic Bunch第12号(3月3日号)发售  
> 「Angel Heart」第203話  
> 【マオの交渉】  
> ▽Anime AH News、第19話提要和要点  
>   
### ●2006年02月13日(月) 读卖TV/25:03～  
### ●2006年02月14日(火) 日本TV/25:25～  
> 动画Angel Heart #18  
> 「親子の絆」  
> →下回「陳老人の店」  
>   
### ●2006年02月10日(金)  
> Comic Bunch第11号(2月24日号)发售  
> 「Angel Heart」暂停。  
>   
### ●2006年02月06日(月) 读卖TV/27:03～  
### ●2006年02月07日(火) 日本TV/26:00～  
> 动画Angel Heart #17  
> 「夢の中の出会い」  
> →下回「親子の絆」  
>   
### ●2006年02月03日(金)  
> Comic Bunch第10号(2月17日号)发售  
> 「Angel Heart」第202話  
> 【昨日の敵は今日の恋人！】  
> ▽Anime AH News、第17話提要和要点  
>   
### ●2006年01月30日(月) 读卖TV/24:59～  
### ●2006年01月31日(火) 日本TV/25:40～  
> 动画Angel Heart #16  
> 「Ｃ・Ｈとしての資格」  
> →下回「夢の中の出会い」  
>   
### ●2006年01月27日(金)  
> Comic Bunch第9号(2月10日号)发售  
> 「Angel Heart」第201話  
> 【皇子と香莹の初デート(はぁと)】  
> ▽Anime AH News、第16話提要和要点  
> →AH DVD Premium BOX 发售日大決定  
>   
### ●2006年01月23日(月) 读卖TV/24:59～  
### ●2006年01月24日(火) 日本TV/25:25～  
> 动画Angel Heart #15  
> 「パパを捜して」  
> →下回「Ｃ・Ｈとしての資格」  
>   
### ●2006年01月20日(金)  
> Comic Bunch第8号(2月3日号)发售  
> 「Angel Heart」第200话/封面/封面画  
> 【妄想超特急がやってきた！】  
> ▽Anime AH News、第15話提要和要点  
>   
### ●2006年01月16日(月) 读卖TV/24:59～  
### ●2006年01月17日(火) 日本TV/25:25～  
> 动画Angel Heart #14  
> 「復活Ｃ・Ｈ！」  
> →下回「パパを捜して」  
>   
### ●2006年01月13日(金)  
> Comic Bunch第7号(1月27日号)发售  
> 「Angel Heart」第199話  
> 【異国からの求婚者！】  
> ▽Anime AH News、第14話提要和要点  
> →「Finally」CD 20名获奖者  
>   
### ●2006年01月09日(月) 读卖TV/25:50～  
### ●2006年01月10日(火) 日本TV/25:55～  
> 动画Angel Heart #13  
> 「李大人からの贈り物」  
> →下回「復活Ｃ・Ｈ！」  
>   
### ●2006年01月06日(金)  
> Comic Bunch新年6号(1月20日号)发售  
> 「Angel Heart」第198話  
> 【クリスマスpresent】  
> ▽Anime AH News、第13話提要和要点  
> →OP曲「Finally」CD 2/1(水) 由DefSTAR RECORDS发售  
>   
### ●2005年12月22日(木)  
> Comic Bunch新年4・5合并特刊(1月6日・13日号)发售  
> 「Angel Heart」第197話  
> 【指輪の記憶】  
> ▽2006年春、AH动画DVD发售開始決定  
>   
### ●2005年12月19日(月) 读卖TV/24:59～  
### ●2006年01月03日(火) 日本TV/26:04～  
> 动画Angel Heart #12  
> 「船上の出会いと別れ」  
> →下回「李大人からの贈り物」  
>   
### ●2005年12月16日(金)  
> Comic Bunch新年3号(12月30日号)发售  
> 「Angel Heart」第196話  
> 【楊(ヤン)、再来！！】  
>   
### ●2005年12月15日(木)  
> 「北条司漫画家25周年記念  
> 　自选illustration100幅」发售  
> →定价2940日元  
> ▽新绘制的香莹封面・新绘制的AH Poster  
> ▽特制Quo Card Present  
>   
### ●2005年12月12日(月) 读卖TV/25:59～  
### ●2005年12月20日(火) 日本TV/25:25～  
> 动画Angel Heart #11  
> 「父娘(おやこ)の時間」  
> →下回「船上の出会いと別れ」  
>   
### ●2005年12月09日(金)  
> Comic Bunch増刊「Angel Heart总集篇  
> Anime & Comics Special」发售  
> →定价390日元  
>   
### ●2005年12月09日(金)  
> Bunch Comics**『Angel Heart』[第17巻](./book_bc.md#17)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch第21号 封面的图案  
>   
### ●2005年12月05日(月) 读卖TV/25:09～  
### ●2005年12月13日(火) 日本TV/25:55～  
> 动画Angel Heart #10  
> 「 Angel  Smile」  
> →下回「父娘(おやこ)の時間」  
>   
### ●2005年12月02日(金)  
> Comic Bunch新年1・2合并特刊(12月16日・23日号)发售  
> 「Angel Heart」第195话/封面  
> 【夏休みの終わり】  
> ▽特别附录・AH Christmas card  
> ▽AH邮票表和present的申请要求  
> ▽Anime AH News、第10話提要和要点  
>   
### ●2005年11月28日(月) 读卖TV/25:09～  
### ●2005年12月06日(火) 日本TV/25:35～  
> 动画Angel Heart #9  
> 「香莹～失われた名前～」  
> →下回「 Angel  Smile」  
>   
### ●2005年11月25日(金)  
> Comic Bunch第52号(12月9日号)发售  
> 「Angel Heart」暂停。  
> ▽Anime AH News、第9話提要和要点  
> ▽下期(12/2发售)AH Christmas card 特别附录  
>   
### ●2005年11月21日(月) 读卖TV/25:19～  
### ●2005年11月29日(火) 日本TV/25:35～  
> 动画Angel Heart #8  
> 「真実(ホント)の仲間」  
> →下回「香莹～失われた名前～」  
>   
### ●2005年11月18日(金)  
> Comic Bunch第51号(12月2日号)发售  
> 「Angel Heart」第194話  
> 【親子の覚悟】  
> ▽Anime AH News、第8話提要和要点  
> ▽新年1、2合并号上AH特别附录的计划  
>   
### ●2005年11月14日(月) 读卖TV/25:28～  
### ●2005年11月22日(火) 日本TV/25:55～  
> 动画Angel Heart #7  
> 「俺の愛すべき街」  
> →下回「真実(ホント)の仲間」  
>   
### ●2005年11月11日(金)  
> Comic Bunch第50号(11月25日号)发售  
> 「Angel Heart」暂停。  
> ▽涵盖从CE到AH的北条司自选插图集 
> →徳間書店将于12月中旬发售決定  
> ▽Anime AH News、第7話提要和要点  
>   
### ●2005年11月07日(月) 读卖TV/24:59～  
### ●2005年11月08日(火) 日本TV/放送休止  
> ⇒11/15(火) 日本TV/25:40～  
> 动画Angel Heart #6  
> 「再会」  
> →下回「俺の愛すべき街」  
>   
### ●2005年11月04日(金)  
> Comic Bunch第49号(11月18日号)发售  
> 「Angel Heart」第193話  
> 【怒りの一撃！】  
> ▽Angel Heart总集篇 12/09(金)发售決定  
> ▽Anime AH News、第6話提要和要点  
>   
### ●2005年10月31日(月) 读卖TV/24:59～  
### ●2005年11月01日(火) 日本TV/26:25～  
> 动画Angel Heart #5  
> 「永別(さよなら) …カオリ」  
> →下回「再会」  
>   
### ●2005年10月28日(金)  
> Comic Bunch第48号(11月11日号)发售  
> 「Angel Heart」第192話  
> 【親子の電話】  
> ▽Anime AH News、第5話提要和要点  
> →「誰かが君を想ってる」CD present(30名)  
>   
### ●2005年10月24日(月) 读卖TV/25:14～  
### ●2005年10月25日(火) 日本TV/25:25～  
> 动画Angel Heart #4  
> 「さまようHEART」  
> →下回「永別(さよなら) …カオリ」  
>   
### ●2005年10月21日(金)  
> Comic Bunch第47号(11月4日号)发售  
> 「Angel Heart」第191話  
> 【恐怖のカーチェイス！】  
> ▽Anime AH News、第4話提要和要点  
> →「誰かが君を想ってる」CD 11/9(水)发售  
>   
### ●2005年10月17日(月) 读卖TV/25:13～  
### ●2005年10月18日(火) 日本TV/25:34～  
> 动画Angel Heart #3  
> 「XYZの街」  
> →下回「さまようHEART」  
>   
### ●2005年10月14日(金)  
> Comic Bunch第46号(10月28日号)发售  
> 「Angel Heart」第190話  
> 【バスジャック！】  
> ▽Anime AH News、第3话提要和要点  
>   
### ●2005年10月10日(月) 读卖TV/25:28～  
### ●2005年10月11日(火) 日本TV/25:49～  
> 动画Angel Heart #2  
> 「香が帰ってきた」  
> →下回「XYZの街」  
>   
### ●2005年10月07日(金)  
> Comic Bunch第45号(10月21日号)发售  
> 「Angel Heart」第189话/封面  
> 【変質者、現る！】  
> ▽动画C Color特集  
> OP/ED曲・Artist's Comment  
>   
### ●2005年10月03日(月) 读卖TV/25:34～  
### ●2005年10月04日(火) 日本TV/26:25～  
> 动画Angel Heart放送開始  
> 动画Angel Heart #1  
> 「ガラスの心臓 グラス・ハート」  
>   
### ●2005年09月30日(金)  
> Comic Bunch第44号(10月14日号)发售  
> 「Angel Heart」第188话/封面  
> 【海学級へ行こう！】  
> ▽动画卷首色彩的特辑/第1话简介、要点etc  
> →OP曲「Finally」by Sowelu  
> →ED曲「誰かが君を想ってる」by Skoop On Somebody  
> ▽动画放映纪念附录「CD型台历」  
> ▽「香莹等身大海报」全员service报名要点  
> →需要填写本刊中的报名表和700元的邮政汇票。以10月17日（星期一）邮戳为准  
>   
### ●2005年09月22日(木)  
> Comic Bunch第43号(10月7日号)发售  
> 「Angel Heart」第187話  
> 【ミキが行方不明！？】  
> ▽下一期（第44期）有2个特别专题・一个卷首彩色专题  
>   
### ●2005年09月16日(金)  
> Comic Bunch第42号(9月30日号)  
> 「Angel Heart」暂停。  
> ▽动画AH放送日決定  
> 读卖TV・10月3日(月)<s>24:58～</s>  
> 日本TV・10月4日(火)<s>25:25～</s>  
>   
### ●2005年09月09日(金)  
> Comic Bunch第41号(9月23日号)发售  
> 「Angel Heart」第186话/封面  
> 【変質者は海坊主！？】  
> ▽Anime AH News  
> →首版・人物对照表  
>   
### ●2005年09月09日(金)  
> Bunch Comics**『Angel Heart』[第16巻](./book_bc.md#16)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch新年4・5合并特刊 封面的图案  
>   
### ●2005年09月02日(金)  
> Comic Bunch第40号(9月16日号)发售  
> 「Angel Heart」第185話  
> 【海坊主と先生】  
> ▽潜入动画AH制作发布会  
> ▽决定每周播出日  
> 读卖TV・每周一24:58～  
> 日本TV・每周二25:25～  
>   
### ●2005年08月26日(金)  
> Comic Bunch第39号(9月9日号)发售  
> 「Angel Heart」第184話  
> 【義父(ちち)はつらいよ！】  
>   
### ●2005年08月12日(金)  
> Comic Bunch第37・38合并特刊(8月26日・9月2日号)发售  
> 「Angel Heart」第183話  
> 【麗子の未来】  
> ▽C-Color 动画AH速报  
> →读卖TV/日本TV今秋播出  
> →AH Digest DVD present(100名)  
> ▽13日～在银座举行CH展览  
> ▽CAT'S EYE完全版・计划在10月登场  
>   
### ●2005年08月05日(金)  
> Comic Bunch第36号(8月19日号)发售  
> 「Angel Heart」第182話  
> 【信宏が教えてくれた事】  
>   
### ●2005年07月29日(金)  
> Comic Bunch第35号(8月12日号)  
> 「Angel Heart」暂停。  
> ▽CH COMPLETE DVD BOX 彩色广告  
> ▽Anime AH News・海报用插图  
>   
> ▽令人兴奋的宝島in天保山「Heart of City」  
> →7/31(日) 神谷明和川崎真央登場  
> 　详见http://www.ytv.co.jp/wakuwaku/  
>   
> ▽THE CITY HUNTER展 Vol.1 銀座  
> →DVD BOX・AH动画化纪念/原画大公开 企划齐全  
> 　(8月13日～8月21日，Sony大厦8楼)详见第37、38期合并号  
>   
### ●2005年07月22日(金)  
> Comic Bunch第34号(8月5日号)发售  
> 「Angel Heart」第181話  
> 【未来を変える男】  
>   
### ●2005年07月14日(木)  
> Comic Bunch第33号(7月29日号)发售  
> 「Angel Heart」第180話  
> 【引き寄せられる運命】  
>   
### ●2005年07月08日(金)  
> Comic Bunch第32号(7月22日号)发售  
> 「Angel Heart」第179話  
> 【笑顔の未来】  
>   
### ●2005年07月01日(金)  
> Comic Bunch第31号(7月15日号)发售  
> 「Angel Heart」第178話  
> 【透視の代償】  
>   
### ●2005年06月24日(金)  
> Comic Bunch第30号(7月8日号)发售  
> 「Angel Heart」第177話  
> 【男の信念】  
>   
### ●2005年06月17日(金)  
> Comic Bunch第29号(7月1日号)发售  
> 「Angel Heart」第176話  
> 【悲しい二人】  
>   
### ●2005年06月15日(水)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[別巻VOLUME:Z（特別短編集）](./book_pf.md#Z)** 发售  
> ☆[Get Wild] 附有特别的附录CD  
> →定价900日元（含税）  
> ▽有彩色纸张赠送/抽出200名获奖者  
>   
### ●2005年06月10日(金)  
> Comic Bunch第28号(6月24日号)发售  
> 「Angel Heart」第175话/封面  
> 【恋人達の未来】  
> ▽C-Color专题「A・Hを構築する3つの世界」（译注：构建A·H的3个世界）  
> →Angel BOX的申请券  
>   
### ●2005年06月09日(木)  
> Bunch Comics**『Angel Heart』[第15巻](./book_bc.md#15)** 发售  
> →定价530日元（含税）  
> ▽封面是Comic Bunch第28号封面的图案  
>   
> ▽漫画累计突破1000万册纪念「Angel BOX」present  
> Comics第1～15卷的封面画复制品/18张一套  
> →B4尺寸（高364x 宽257mm）/装在带有序号的豪华礼盒中  
> ※腰封上的入场券和Comic Bunch第28期（6月10日发售）上的入场券都是必需的/1000名获奖者将被抽出 
> →截止日期为6月30日（周四），以当天邮戳为准，详见腰封  
>   
### ●2005年06月03日(金)  
> Comic Bunch第27号(6月17日号)  
> 「Angel Heart」暂停。  
> ▽下一期（第28期）AH封面&Center Color为特集  
>   
### ●2005年05月27日(金)  
> Comic Bunch第26号(6月10日号)发售  
> 「Angel Heart」第174話  
> 【麗泉(れいせん)の悩み】  
>   
### ●2005年05月20日(金)  
> Comic Bunch第25号(6月3日号)发售  
> 「Angel Heart」第173話  
> 【アカルイミライ！？】  
>   
### ●2005年05月14日(土)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[別巻VOLUME:Y（イラストレーションズ2）](./book_pf.md#Y)** 发售  
> →定价800日元（含税）  
> ▽有彩色纸张赠送/抽出200名获奖者  
>   
### ●2005年05月12日(木)  
> Comic Bunch第24号(5月27日号)发售  
> 「Angel Heart」第172話  
> 【母なる思い】  
> ▽包括北斗の拳、CH贴纸&走运千社札带  
>   
### ●2005年04月28日(木)  
> Comic Bunch第22・23合并特刊(5月13日・20日号)发售  
> 「Angel Heart」第171話  
> 【再会…！！】  
> ▽下期5月12日(周四)发售  
>   
### ●2005年04月22日(金)  
> Comic Bunch第21号(5月6日号)发售  
> 「Angel Heart」第170话/封面  
> 【楊(ヤン)、始動！】  
> ▽下期4月28日(木)发售  
>   
### ●2005年04月15日(金)  
> Comic Bunch第20号(4月29日号)发售  
> 「Angel Heart」第169話  
> 【黒幕現る！！】  
>   
### ●2005年04月15日(金)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:32](./book_pf.md#32)** 发售  
> →定价980日元（含税）  
>   
> ◯连续3卷的爱读者W感謝企画  
> ▽每期抽奖：特殊的 character quo card 
> 　32巻「リョウ＆香」  
> ▽30・31・32巻 全卷compaign：Complete BOX  
> 　CH完全版17～32+独立卷Y、Z可以收纳！！ / 申请者全员service  
> →详情参见腰封  
>   
### ●2005年04月08日(金)  
> Comic Bunch第19号(4月22日号)发售  
> 「Angel Heart」第168話  
> 【天国と地獄！？】  
> ▽动画香莹役川崎真央的直接Interview  
>   
### ●2005年04月03日(日)23時～NACK5  
### ●2005年04月02日(土)24時～FM愛知・FM大阪  
> 广播节目「HEART OF ANGEL」中的一角  
> 「XYZ Ryo's Bar」广播start  
> →神谷明和美女Guest的talk corner  
> ▽第一批的Guest是麻上洋子和伊倉一恵  
>   
### ●2005年04月01日(金)  
> Comic Bunch第18号(4月15日号)  
> ▽动画AH配音演员Audition公布  
> →香莹役声優：川崎真央(18岁)  
> 「Angel Heart」暂停。  
>   
### ●2005年03月25日(金)  
> Comic Bunch第17号(4月8日号)发售  
> 「Angel Heart」第167話  
> 【楊芳玉(ヤン fan ユィ)の仕事】  
> ▽下期（第18期）将有一个关于动漫AH的主要专题  
> →香莹役声优公布/本篇暂停 
>   
### ●2005年03月17日(木)  
> Comic Bunch第16号(4月1日号)发售  
> 「Angel Heart」第166話  
> 【愛は地球を救う！？】  
> ▽Anime AH News  
> 潜入香莹役声优Audition！  
> 香莹&獠 线画设定定稿!  
>   
> ▽Anime放送日時  
> 日本TV系 4月22日(金)27時8分～  
> 读卖TV系4月18日(月)24時58分～  
>   
### ●2005年03月15日(火)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:30](./book_pf.md#30)、[VOLUME:31](./book_pf.md#31)** 同时发售  
> →定价980日元（含税）  
>   
> ◯连续3卷的爱读者W感謝企画start  
> ▽每期抽奖：特制character quocard  
> 　30巻「リョウ」、31巻「香」、32巻 「？」的三种／合计600名  
> ▽30・31・32巻 全卷campaign：Complete BOX  
> 　CH完全版17～32+独立卷Y、Z可以收纳！！ / 申请者全员service第2弹  
> →详情参见腰封  
>   
### ●2005年03月11日(金)  
> Comic Bunch第15号(3月25日号)发售  
> 「Angel Heart」第165話  
> 【楊(ヤン)からの依頼？】  
> ▽Anime AH News  
> 　带有天使之翼的logo决定了!  
> 　主要的演员阵容与CH相同！  
>   
### ●2005年03月04日(金)  
> Comic Bunch第14号(3月18日号)发售  
> 「Angel Heart」第164話  
> 【それぞれの場所】  
>   
### ●2005年02月25日(金)  
> Comic Bunch第13号(3月11日号)发售  
> 「Angel Heart」第163話  
> 【女同士の酒】  
> ▽C-color/手机 AH Game「Same Bunch」攻略留言板  
> ▽北斗の拳Raoh外传，2006年春季改编为电影  
> →新角色悲伤女战士「Reina」角色由北条先生设计  
>   
### ●2005年02月18日(金)  
> Comic Bunch第12号(3月4日号)发售  
> 「Angel Heart」第162話  
> 【リョウの大失敗】  
>   
### ●2005年02月18日(金)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:28](./book_pf.md#28)、[VOLUME:29](./book_pf.md#29)** 同时发售  
> →定价980日元（含税）  
>   
### ●2005年02月10日(木)  
> Comic Bunch第11号(2月25日号)  
> 「Angel Heart」暂停。  
>   
### ●2005年02月09日(水)  
> Bunch Comics**『Angel Heart』[第14巻](./book_bc.md#14)** 发售  
> →定价530日元（含税）  
>   
> ▽封面是Comic Bunch'03年第52号封面的图案  
>   
### ●2005年02月04日(金)  
> Comic Bunch第10号(2月18日号)发售  
> 「Angel Heart」第161話  
> 【恋の覚悟】  
>   
### ●2005年01月28日(金)  
> Comic Bunch第9号(2月11日号)发售  
> 「Angel Heart」第160話  
> 【ホレた男は謎だらけ！？】  
> ▽今春决定TV转播! 动画AH速报第2弹  
> 　Center Color 香莹役声优Audition详情  
>   
### ●2005年01月21日(金)  
> Comic Bunch第8号(2月4日号)发售  
> 「Angel Heart」第159話  
> 【私、恋してます！】  
>   
### ●2005年01月15日(土)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:26](./book_pf.md#26)、[VOLUME:27](./book_pf.md#27)** 同时发售  
> →定价980日元（含税）  
>   
### ●2005年01月14日(金)  
> Comic Bunch第7号(1月28日号)发售  
> 「Angel Heart」第158話  
> 【愛される理由】  
> ▽第9期中香莹役声优Audition细节  
>   
### ●2005年01月07日(金)  
> Comic Bunch新年6号(1月21日号)发售  
> 「Angel Heart」第157話  
> 【リョウのケータイライフ】  
> ▽决定举办香莹役声优Audition  
>   
### ●2004年12月24日(金)  
> Comic Bunch新年4・5合并特刊(1月11日・14日号)发售  
> 「Angel Heart」第156话/封面  
> 【家族の写真】  
> ▽包括TV动画改变公告第一弾&AH特制Calender  
>   
### ●2004年12月16日(木)  
> Comic Bunch新年3号(1月7日号)发售  
> 「Angel Heart」第155話  
> 【告白…！】  
> ▽第4、5期合并号在卷首彩画上有一个关于TV动画改编的重要预告专题  
>   
### ●2004年12月15日(水)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:24](./book_pf.md#24)、[VOLUME:25](./book_pf.md#25)** 同时发售  
> →定价980日元（含税）  
>   
### ●2004年12月03日(金)  
> Comic Bunch新年1・2合并特刊(12月24日・31日号)发售  
> 「Angel Heart」第154話  
> 【ジョイの行き先】  
> ▽下一期（新年第3期）于12月16日（周四）开始发售  
>   
### ●2004年11月26日(金)  
> Comic Bunch第52号(12月10日号)发售  
> 「Angel Heart」第153話  
> 【親子のように】  
>   
### ●2004年11月19日(金)  
> Comic Bunch第51号(12月3日号)  
> 「Angel Heart」暂停。  
>   
### ●2004年11月15日(月)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:22](./book_pf.md#22)、[VOLUME:23](./book_pf.md#23)** 同时发售  
> →定价980日元（含税）  
>   
### ●2004年11月12日(金)  
> Comic Bunch第50号(11月26日号)发售  
> 「Angel Heart」第152話  
> 【ミキの幸せ】  
>   
### ●2004年11月09日(火)  
> Bunch Comics**『Angel Heart』[第13巻](./book_bc.md#13)** 发售  
> →定价530日元（含税）  
>   
> ▽封面是Comic Bunch第40期的封面的图案  
>   
### ●2004年11月05日(金)  
> Comic Bunch第49号(11月19日号)发售  
> 「Angel Heart」第151話  
> 【共同生活開始！】  
> ▽决定将AH改编为TV动画  
>   
### ●2004年10月29日(金)  
> Comic Bunch第48号(11月12日号)发售  
> 「Angel Heart」第150話  
> 【愛しき人の形見】  
>   
### ●2004年10月22日(金)  
> Comic Bunch第47号(11月5日号)发售  
> 「Angel Heart」第149話  
> 【思い人はこの街に？】  
>   
### ●2004年10月15日(金)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:20](./book_pf.md#20)、[VOLUME:21](./book_pf.md#21)** 同时发售  
> →定价980日元（含税）  
>   
### ●2004年10月15日(金)  
> Comic Bunch第46号(10月29日号)发售  
> 「Angel Heart」第148話  
> 【ジョイの事情】  
>   
### ●2004年10月07日(木)  
> Comic Bunch第45号(10月22日号)发售  
> 「Angel Heart」第147話  
> 【"危険"な大女優！】  
>   
### ●2004年10月01日(金)  
> Comic Bunch第44号(10月15日号)发售  
> 「Angel Heart」第146話  
> 【優しき店長(マスター)】  
>   
### ●2004年09月24日(金)  
> Comic Bunch第43号(10月8日号)  
> 「Angel Heart」暂停。  
>   
### ●2004年09月16日(木)  
> Comic Bunch第42号(10月1日号)发售  
> 「Angel Heart」第145話  
> 【暗闇に見えた夕陽】  
> ▽下一期，第43期暂停  
>   
### ●2004年09月15日(水)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:19](./book_pf.md#19)、[別巻VOLUME:X（イラストレーションズ1）](./book_pf.md#X)** 同时发售  
> →定价980日元、插图集800日元（含税）  
>   
### ●2004年09月10日(金)  
> Comic Bunch第41号(9月24日号)发售  
> 「Angel Heart」第144話  
> 【いつも一緒】  
>   
### ●2004年09月09日(木)  
> Bunch Comics**『Angel Heart』[第12巻](./book_bc.html#12)** 发售  
> →定价530日元（含税）  
>   
> ▽封面是Comic Bunch第27期扉页的图案  
>   
### ●2004年09月03日(金)  
> Comic Bunch第40号(9月17日号)发售  
> 「Angel Heart」第143话/封面  
> 【ママへの想い】  
>   
### ●2004年08月27日(金)  
> Comic Bunch第39号(9月10日号)发售  
> 「Angel Heart」第142話  
> 【汚れなき心】  
>   
### ●2004年08月19日(木)  
> Comic Bunch第38号(9月3日号)发售  
> 「Angel Heart」第141話  
> 【海坊主と座敷童】  
>   
### ●2004年08月14日(土)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:17](./book_pf.md#17)、[VOLUME:18](./book_pf.md#18)** 同时发售  
> →定价980日元（含税）  
>   
### ●2004年08月06日(金)  
> Comic Bunch第36・37合并特刊(8月20日・27日号)发售  
> 「Angel Heart」第140話  
> 【穏やかな夢】  
>   
### ●2004年07月30日(金)  
> Comic Bunch第35号(8月13日号)发售  
> 「Angel Heart」第139話  
> 【冴子のお返し】  
>   
### ●2004年07月23日(金)  
> Comic Bunch第34号(8月6日号)  
> 「Angel Heart」暂停。  
>   
### ●2004年07月15日(木)  
> Comic Bunch第33号(7月30日号)发售  
> 「Angel Heart」第138話  
> 【都会の座敷童】  
> ▽下周，第34期暂停  
>   
### ●2004年07月15日(木)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:15](./book_pf.md#15)、[VOLUME:16](./book_pf.md#16)** 同时发售  
> →定价980日元（含税）  
>   
> ◯豪华特制「Complete BOX」全員service 第1弾start（附报名券）  
> ▽可以收纳CH完全版17册  
> ▽豪华特制BOX上有新绘制的封面插图  
> 剪下第13卷的报名表，将第14、15、16卷的报名票粘贴在上面  
> 需要 - 1400日元的汇票（含运费）  
> →截止日期为8月31日（星期二），以当天的邮戳为准，详见腰封  
>   
### ●2004年07月09日(金)  
> Comic Bunch第32号(7月23日号)发售  
> 「Angel Heart」第137話  
> 【冴子と謎の女の子】  
>   
### ●2004年07月02日(金)  
> Comic Bunch第31号(7月16日号)发售  
> 「Angel Heart」第136話  
> 【高畑のお守(まも)り】  
>   
### ●2004年06月25日(金)  
> Comic Bunch第30号(7月9日号)发售  
> 「Angel Heart」第135話  
> 【生きた証】  
>   
### ●2004年06月18日(金)  
> Comic Bunch第29号(7月2日号)发售  
> 「Angel Heart」第134話  
> 【生きて…！】  
>   
### ●2004年06月15日(火)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:13](./book_pf.md#13)、[VOLUME:14](./book_pf.md#14)** 同时发售  
> →定价980日元（含税）  
>   
> ◯豪华特制「Complete BOX」全员service第1弹Start(附报名券)  
> ▽CH完全版可以整齐地收纳起来  
> ▽豪华特制BOX上有新绘制的封面插图  
> →申请方法在第15、16卷的腰封中给出  
>   
### ●2004年06月11日(金)  
> Comic Bunch第28号(6月25日号)发售  
> 「Angel Heart」第133話  
> 【心臓移植のリスク】  
>   
### ●2004年06月09日(水)  
> Bunch Comics**『Angel Heart』[第11巻](./book_bc.md#11)** 发售  
> →定价530日元（含税）  
>   
> ▽封面是Comic Bunch第27期封面的图案  
>   
### ●2004年06月04日(金)  
> Comic Bunch第27号(6月18日号)发售  
> 「Angel Heart」第132话/封面＆卷首彩页  
> 【明かされた真実】  
>   
### ●2004年05月28日(金)  
> Comic Bunch第26号(6月11日号)  
> 「Angel Heart」暂停。  
> ▽27号封面＆卷首彩页  
>   
### ●2004年05月21日(金)  
> Comic Bunch第25号(6月4日号)发售  
> 「Angel Heart」第131話  
> 【高畑の嘘】  
> ▽26号暂停。27号封面＆卷首彩页  
>   
### ●2004年05月15日(土)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:11](./book_pf.md#11)、[VOLUME:12](./book_pf.md#12)** 同时发售  
> →定价980日元（含税）  
>   
### ●2004年05月14日(金)  
> Comic Bunch第24号(5月28日号)发售  
> 「Angel Heart」第130話  
> 【綾音(あーや)の嘘】  
>   
### ●2004年05月06日(木)  
> Comic Bunch第23号(5月21日号)发售  
> 「Angel Heart」第129話  
> 【姉(さおり)の想い】  
>   
### ●2004年04月23日(金)  
> Comic Bunch第21・22合并特刊(5月7日・14日号)发售  
> 「Angel Heart」第128話  
> 【心臓(こころ)の共振】  
>   
### ●2004年04月16日(金)  
> Comic Bunch第20号(4月30日号)发售  
> 「Angel Heart」第127話  
> 【心臓(こころ)の声を信じて】  
>   
### ●2004年04月15日(木)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:09](./book_pf.md#9)、[VOLUME:10](./book_pf.md#10)** 同时发售  
> →定价980日元（含税）  
>   
> ◯100万部突破記念Campaign  
> ▽漫画第9、10卷的顺位的2张入场券可以用来填写问卷，以赢得一套4张豪华明信片，共1000人。  
> →提交申请截止至2004年5月31日(月)<font color=#ff0000>邮戳有効</font>  
>   
> 详情参见[徳間書店的Homepage](http://www.tokuma.jp)。  
>   
### ●2004年04月09日(金)  
> Comic Bunch第19号(4月23日号)发售  
> 「Angel Heart」第126話  
> 【白蘭からの手紙】  
>   
### ●2004年04月02日(金)  
> Comic Bunch第18号(4月16日号)发售  
> 「Angel Heart」第125話  
> 【受け継がれし命】  
>   
### ●2004年04月01日(木)～  
> 新宿「MY CITY」6楼的山下書店举行的CH完全版 发售記念企画  
> ▽CITY HUNTER的复制原画公开  
> ▽购买完全版的前300名，以北条先生亲笔为基础的original paper作为礼物  
>   
### ●2004年03月26日(金)  
> Comic Bunch第17号(4月9日号)  
> 「Angel Heart」暂停。  
>   
### ●2004年03月19日(金)  
> Comic Bunch第16号(4月2日号)发售  
> 「Angel Heart」第124話  
> 【生きる糧】  
>   
### ●2004年03月15日(月)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:07](./book_pf.md#7)、[VOLUME:08](./book_pf.md#8)** 同时发售  
> →定价933日元（不含税）  
>   
### ●2004年03月12日(金)  
> Comic Bunch第15号(3月26日号)发售  
> 「Angel Heart」第123話  
> 【悲しみの決行日】  
>   
### ●2004年03月09日(火)  
> Bunch Comics**『Angel Heart』[第10巻](./book_bc.md#10)** 发售  
> →定价505日元（不含税）  
>   
> ▽封面是Comic Bunch'03年第52期扉页  
>   
### ●2004年03月05日(金)  
> Comic Bunch第14号(3月19日号)发售  
> 「Angel Heart」第122話  
> 【白蘭の決意】  
>   
### ●2004年02月27日(金)  
> Comic Bunch第13号(3月12日号)发售  
> 「Angel Heart」第121話  
> 【初めての愛情】  
>   
### ●2004年02月20日(金)  
> Comic Bunch第12号(3月5日号)发售  
> 「Angel Heart」第120話  
> 【神から授かりし子】  
>   
### ●2004年02月14日(土)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:05](./book_pf.md#5)、[VOLUME:06](./book_pf.md#6)** 同时发售  
> →定价933日元（不含税）  
>   
### ●2004年02月13日(金)  
> Comic Bunch第11号(2月27日号)发售  
> 「Angel Heart」第119话/封面  
> 【スコープの中の真意】  
>   
### ●2004年02月06日(金)  
> Comic Bunch第10号(2月20日号)发售  
> 「Angel Heart」第118話  
> 【白蘭の任務】  
>   
### ●2004年01月30日(金)  
> Comic Bunch第9号(2月13日号)  
> 「Angel Heart」暂停。  
>   
### ●2004年01月23日(金)  
> Comic Bunch第8号(2月6日号)发售  
> 「Angel Heart」第117話  
> 【二人の変化】  
>   
### ●2004年01月16日(金)  
> Comic Bunch第7号(1月30日号)发售  
> 「Angel Heart」第116話  
> 【運命の再会】  
>   
### ●2004年01月15日(木)  
> 德间Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:03](./book_pf.md#3)、[VOLUME:04](./book_pf.md#4)** 同时发售  
> →定价933日元（不含税）  
>   
### ●2004年01月08日(木)  
> Comic Bunch新年6号(1月23日号)发售  
> 「Angel Heart」第115話  
> 【聖夜の奇跡】  
>   
### ●2004年01月06日(火)  
> Da Vinci 2月号 发售（定价450日元）  
> ▽Comics Da Vinci「北条 司 その尽きない魅力」（译注：「北条司 无尽的魅力」）  
> 　※上个月预告的title是「工匠北条司的作品」
>   
### ●2004年01月05日(月)  
> 日经Entertainment！2月刊发售  
> →定价500日元  
> ▽【対談】北条司×飯島愛  
> 「Cat's Eye」「City Hunter」  
> 「Angel Heart」诞生的秘密故事  
>   
### ●2003年12月25日(木)  
> Comic Bunch新年4・5合并特刊(1月13日・1月16日号)发售  
> 「Angel Heart」第114话/封面  
> 【恋人同士の誤解】  
> ▽Premium附录，AH特制Comics cover  
> ▽下期（新年第6期）于1月8日发售  
>   
### ●2003年12月19日(金)  
> Comic Bunch新年3号(1月9日号)发售  
> 「Angel Heart」第113話  
> 【ベイビートラブル！】  
> ▽下一期（12月25日发售）包括AH特制book cover（B6）附录  
>   
### ●2003年12月15日(月)  
> 徳間Comics**『CITY HUNTER 《COMPLETE EDITION》』[VOLUME:01](./book_pf.md#1)、[VOLUME:02](./book_pf.md#2)** 同时发售  
> →定价933日元（不含税）  
> ●所有卷的新封面插图  
> ●在杂志上发表的彩页完全再现  
> ●以前的漫画版本中未收集的页面被重新收录  
>   
> ▽「发刊纪念 campaign！！」**特制电话卡&海报**present！！  
> ▽漫画1、2巻的腰封的入场券2个，特制电话卡1000名、海报2000名。（译注：待校对）  
> →申请截至于2004年1月31日(星期六)<font color=#ff0000>邮戳有效</font>  
>   
> 详情请参阅[徳間書店的Homepage](http://www.tokuma.jp)。  
>   
### ●2003年12月09日(火)  
> Bunch Comics**『Angel Heart』[第9巻](./book_bc.md#9)** 发售  
> →定价505日元（不含税）  
>   
> ▽封面是Comic Bunch第31号扉页  
>   
### ●2003年12月05日(金)  
> Comic Bunch新年1・2合并特刊(12月26日・1月2日号)发售  
> 「Angel Heart」第112話  
> 【依頼人はひったくり！？】  
> ▽下一期（新年第3期）于12月19日发售  
>   
### ●2003年11月28日(金)  
> Comic Bunch第52号(12月12日号)发售  
> 「Angel Heart」第111话/封面＆卷首彩画（译注：待校对）  
> 【早百合の旅立ち】  
>   
### ●2003年11月20日(木)  
> Comic Bunch第51号(12月5日号)  
> 「Angel Heart」暂停。  
> ▽下一期（第52期）有AH封面&卷首彩画（译注：待校对）  
>   
### ●2003年11月14日(金)  
> Comic Bunch第50号(11月28日号)发售  
> 「Angel Heart」第110話  
> 【一途(バカ)な男】  
>   
### ●2003年11月07日(金)  
> Comic Bunch第49号(11月21日号)发售  
> 「Angel Heart」第109話  
> 【リョウからの依頼】  
>   
### ●2003年10月30日(木)  
> Comic Bunch第48号(11月14日号)发售  
> 「Angel Heart」第108话/封面  
> 【Ｃ・Ｈの正体】  
>   
### ●2003年10月24日(金)  
> Comic Bunch第47号(11月7日号)发售  
> 「Angel Heart」第107話  
> 【槇村の決意】  
>   
### ●2003年10月17日(金)  
> Comic Bunch第46号(10月31日号)发售  
> 「Angel Heart」第106話  
> 【小さな願い】  
>   
### ●2003年10月09日(木)  
> Comic Bunch第45号(10月24日号)发售  
> 「Angel Heart」第105話  
> 【思い出の街】  
>   
### ●2003年10月03日(金)  
> Comic Bunch第44号(10月17日号)发售  
> 「Angel Heart」第104話  
> 【奇跡の適合性】  
>   
### ●2003年09月26日(金)  
> Comic Bunch第43号(10月10日号)发售  
> 「Angel Heart」第103話  
> 【新宿の天使】  
>   
### ●2003年09月19日(金)  
> Comic Bunch第42号(10月3日号)发售  
> 「Angel Heart」第102話  
> 【心臓(かおり)の反応】  
>   
### ●2003年09月11日(木)  
> Comic Bunch第41号(9月26日号)发售  
> 「Angel Heart」第101話  
> 【妹は幸せだった？】  
>   
### ●2003年09月09日(火)  
> Bunch Comics**『Angel Heart』[第8巻](./book_bc.md#8)** 发售  
> →定价505元（不含税）  
>   
> ▽Comic Bunch第19号封面  
> →超豪华! 有装裱的彩色纸质present  
> 北条先生签名的彩色纸（有获奖者的名字）25名/失望奖明信片套装500名  
> 截止日期为10月8日（周三）；详情见単行本帯  
>   
### ●2003年09月05日(金)  
> Comic Bunch第40号(9月19日号)发售  
> 「Angel Heart」第100话/封面・封面画（译注：待校对）  
> 【妹を捜して！】  
> ▽AH QUO卡和T-shirt present etc...  
>   
### ●2003年08月29日(金)  
> Comic Bunch第39号(9月12日号)  
> 「Angel Heart」暂停。  
> ▽下一期是AH封面&封面画（译注：待校对）  
>   
### ●2003年08月22日(金)  
> Comic Bunch第38号(9月5日号)发售  
> 「Angel Heart」第99話  
> 【恋する人の気持ち】  
> ▽下一期暂停，第40期是新展开封面画（译注：待校对）  
>   
### ●2003年08月08日(金)  
> Comic Bunch第36・37合并特刊(8月22日・29日号)发售  
> 「Angel Heart」第98話  
> 【初恋、涙の別れ】  
> ▽豪華特別附录 全彩的AH第1話收录  
>   
### ●2003年08月01日(金)  
> Comic Bunch第35号(8月15日号)发售  
> 「Angel Heart」第97话/封面  
> 【幸せな笑顔】  
> ▽下一期的Bunch，AH第1話的前48页以全彩的形式完全收录  
>   
### ●2003年07月25日(金)  
> Comic Bunch第34号(8月8日号)发售  
> 「Angel Heart」第96話  
> 【遠い約束】  
>   
### ●2003年07月17日(木)  
> Comic Bunch第33号(8月1日号)  
> 「Angel Heart」暂停  
>   
### ●2003年07月11日(金)  
> Comic Bunch第32号(7月25日号)发售  
> 「Angel Heart」第95話  
> 【私、恋してます】  
> ▽下一期AH暂停。96话第34期  
>   
### ●2003年07月04日(金)  
> Comic Bunch第31号(7月18日号)发售  
> 「Angel Heart」第94話/封面画  
> 【父親が娘を想う気持ち】  
>   
### ●2003年06月27日(金)  
> Comic Bunch第30号(7月11日号)发售  
> 「Angel Heart」第93話  
> 【恋より深い感情】  
>   
### ●2003年06月20日(金)  
> Comic Bunch第29号(7月4日号)发售  
> 「Angel Heart」第92話  
> 【これが、恋？】  
>   
### ●2003年06月13日(金)  
> Comic Bunch第28号(6月27日号)发售  
> 「Angel Heart」第91話  
> 【本当の表情(かお)】  
>   
### ●2003年06月09日(月)  
> Bunch Comics**『Angel Heart』[第7巻](./book_bc.md#7)** 发售。505日元+税。  
> ▽Comic Bunch第10号封面  
> →累计发行了500多万册纪念present  
> A奖「作者亲笔签名的报刊」50人/B奖「Clear File Set」1000人（译注：待校对）  
> 截止日期7月8日(星期二)·详情请参阅单行本带（译注：待校对）  
>   
### ●2003年06月06日(金)  
> Comic Bunch第27号(6月20日号)发售  
> 「Angel Heart」第90话/封面  
> 【初めての感情】  
>   
### ●2003年05月30日(金)  
> Comic Bunch第26号(6月13日号)发售  
> 「Angel Heart」第89話  
> 【遅れて来た幸せ】  
>   
### ●2003年05月23日(金)  
> Comic Bunch第25号(6月6日号)  
> 「Angel Heart」暂停  
>   
### ●2003年05月16日(金)  
> Comic Bunch第24号(5月30日号)发售  
> 「Angel Heart」第88话/封面  
> 【あたたかい銃弾】  
> ▽下一期AH暂停；第89话将在第26期发表。  
>   
### ●2003年05月09日(金)  
> Comic Bunch第23号(5月23日号)发售  
> 「Angel Heart」第87話  
> 【唯一の愛情】  
> ▽AH2周年纪念明信片2种  
>   
### ●2003年04月25日(金)  
> Comic Bunch第21・22合并特刊(5月9日・16日号)发售  
> 「Angel Heart」第86話  
> 【思い出の場所】  
> ▽AH original tapestry、QUO卡、图书卡 present  
>   
### ●2003年04月18日(金)  
> Comic Bunch第20号(5月2日号)发售  
> 「Angel Heart」第85話  
> 【愛に飢えた悪魔】  
> ▽下周在Bunch签名的AH Original Goods Present  
>   
### ●2003年04月11日(金)  
> Comic Bunch第19号(4月25日号)发售  
> 「Angel Heart」第84话/封面  
> 【悪魔の Ending 】  
>   
### ●2003年04月07日(月)  
> Bunch官方i-mode网站「i-bunch 」OPEN  
> ▽AH的shooting game「Mokkori Hunter」和待机画面  
> 进入方法：iMenu→menu list→TV/广播/杂志→(5)杂志→iBunch  
> 每月：300日元  
>   
### ●2003年04月04日(金)  
> Comic Bunch第18号(4月18日号)发售  
> 「Angel Heart」第83話  
> 【哀しき連続殺人犯】  
>   
### ●2003年03月28日(金)  
> Comic Bunch第17号(4月11日号)发售  
> 「Angel Heart」第82話  
> 【危険な匂い】  
>   
### ●2003年03月20日(木)  
> Comic Bunch第16号(4月3日号)发售  
> 「Angel Heart」第81話  
> 【心臓(かおり)の涙】  
>   
### ●2003年03月14日(金)  
> Comic Bunch第15号(3月28日号)发售  
> 「Angel Heart」第80話/封面画  
> 【ターゲットは香莹】  
>   
### ●2003年03月08日(土)  
> Bunch Comics**『Angel Heart』[第6巻](./book_bc.md#6)** 发售。505日元+税。  
> ▽2002年第44期Comic Bunch的封面  
>   
### ●2003年03月07日(金)  
> Comic Bunch第14号(3月21日号)发售  
> 「Angel Heart」第79话/封面画  
> 【冴子からのＸＹＺ】  
> ▽下期AH的封面画  
>   
### ●2003年02月28日(金)  
> Comic Bunch第13号(3月14日号)  
> 「Angel Heart」 暂停  
>   
### ●2003年02月21日(金)  
> Comic Bunch第12号(3月7日号)发售  
> 「Angel Heart」第78話  
> 【遺された指輪】  
> ▽第13号 AH暂停  
>   
### ●2003年02月14日(金)  
> Comic Bunch第11号(2月28日号)发售  
> 「Angel Heart」第77話  
> 【本当の家族】  
>   
### ●2003年02月07日(金)  
> Comic Bunch第10号(2月21日号)发售  
> 「Angel Heart」第76话/封面画  
> 【もう一度あの頃に…】  
>   
### ●2003年01月31日(金)  
> Comic Bunch第9号(2月14日号)发售  
> 「Angel Heart」第75話  
> 【命より大切な絆】  
>   
### ●2003年01月24日(金)  
> Comic Bunch第8号(2月7日号)发售  
> 「Angel Heart」第74話  
> 【別れの五日元玉】  
>   
### ●2003年01月17日(金)  
> Comic Bunch第7号(1月31日号)发售  
> 「Angel Heart」第73話  
> 【不器用な男】  
>   
### ●2003年01月10日(金)  
> Comic Bunch新年6号(1月24日号)发售  
> 「Angel Heart」第72話  
> 【伝わらぬ思い】  
> ▽新春特典2003年版AH Original Calendar（1～2月）  
> →Calendar的图案是Comic Bunch2002年第36/37期合并特刊的封面  
>   
### ●2003年01月07日(火)  
> Raijin Collection  
> **『北条司短編集 Vol.1 [天使的礼物](./book_rc.md#短編1)』**发售  
> ▽City Hunter的2个短篇故事收录在内  
> →定价286日元＋税  
>   
### ●2003年01月06日(月)  
> 英語版漫画雑誌  
> **RAIJIN COMICS（ライジンComics）vol.4（2月号）**发售  
> 「CITY HUNTER（英語版）」  
> "EPISODE5: A SNIPER IN THE DARK"  
> ▽特別附录 Listening CD（CH和北斗の拳）  
>   
### ●2002年12月27日(金)  
> Comic Bunch新年4・5合并特刊(1月13日・17日号)发售  
> 「Angel Heart」第71話  
> 【依頼は殺し！？】  
> ▽下一期是在1月10日，并附有全彩色的超大Calendar  
>   
### ●2002年12月20日(金)  
> Comic Bunch新年3号(1月10日号)发售  
> 「Angel Heart」第70話  
> 【香莹の変化】  
>   
### ●2002年12月06日(金)  
> Comic Bunch新年1・2合并特刊(1月1日・3日号)发售  
> 「Angel Heart」第69話  
> 【笑顔の二人】  
> ▽下一期于12月20日发售  
>   
### ●2002年12月05日(木)  
> 英語版漫画雑誌  
> **RAIJIN COMICS（ライジンComics）2003年1月号** 发售  
> 「CITY HUNTER（英語版）」  
> "EPISODE4: THE DEVIL IN THE BMW part 3"  
> ▽番外篇「爆发了 "mokkori"大争论！！」  
> →对50名母语人士和英语语言专家进行深入采访  
> ▽特別附录 Listening CD（CH和北斗の拳）  
> ▽CH海报作为50名问卷调查礼物  
> →店面销售定价880日元（含税）  
>   
### ●2002年11月29日(金)  
> Comic Bunch第52号(12月13日号)发售  
> 「Angel Heart」第68話/封面人物/封面   
> 【優しい心音(おと)】  
>   
### ●2002年11月22日(金)  
> Comic Bunch第51号(12月6日号)  
> 「Angel Heart」暂停  
>   
### ●2002年11月15日(金)  
> Comic Bunch第50号(11月29日号)发售  
> 「Angel Heart」第67話  
> 【不公平な幸せ】  
> ▽下期AH暂停 → 在第52号中恢复封面人物
>   
### ●2001年11月09日(土)  
> Bunch Comics**『Angel Heart』[第5巻](./book_bc.md#5)** 发售。505日元+税。  
> ▽Comic Bunch第31期的封面  
>   
### ●2002年11月08日(金)  
> Comic Bunch第49号(11月22日号)发售  
> 「Angel Heart」第66話  
> 【天使の心】  
>   
### ●2002年11月05日(火)  
> 英語版漫画雑誌  
> **RAIJIN COMICS（ライジンComics）创刊2号（12月号）**发售  
> 「CITY HUNTER（英語版）」/封面  
> "EPISODE3: THE DEVIL IN THE BMW part 2"  
> ▽特別附录 Listening CD（CH和北斗の拳）  
> ▽CH海报作为50名问卷调查礼物  
> →店面销售价格880日元（含税）  
>   
### ●2002年11月01日(金)  
> Comic Bunch第48号(11月15日号)发售  
> 「Angel Heart」第65话/封面  
> 【香莹の決意】  
>   
### ●2002年10月25日(金)  
> Comic Bunch第47号(11月8日号)发售  
> 「Angel Heart」第64話  
> 【突きつけられた真実】  
>   
### ●2002年10月18日(金)  
> Comic Bunch第46号(11月1日号)发售  
> 「Angel Heart」第63話  
> 【夢を守る！】  
>   
### ●2002年10月11日(金)  
> Comic Bunch第45号(10月25日号)发售  
> 「Angel Heart」第62話  
> 【媽媽の心臓】  
>   
### ●2002年10月05日(土)  
> 英语版漫画杂志 创刊  
> **RAIJIN COMICS（ライジンComics）创刊号（11月号）**发售  
> 「CITY HUNTER（英語版）」  
> "EPISODE2: THE DEVIL IN THE BMW part 1"  
> ▽特别附录Listening CD（CH和北斗の拳）  
> ▽CH海报作为100名问卷调查礼物  
> →每月5日发售/店内销售880日元（含税）  
>   
### ●2002年10月04日(金)  
> Comic Bunch第44号(10月18日号)发售  
> 「Angel Heart」第61話封面  
> 【孤独な少女】  
>   
### ●2002年09月27日(金)  
> Comic Bunch第43号(10月11日号)  
> 「Angel Heart」暂停  
>   
### ●2002年09月20日(金)  
> Comic Bunch第42号(10月4日号)发售  
> 「Angel Heart」第60話  
> 【心の傷】  
>   
### ●2002年09月13日(金)  
> Comic Bunch第41号(9月27日号)发售  
> 「Angel Heart」第59话/封面  
> 【C・Hが犯人！？】  
>   
### ●2002年09月上旬～11月末  
> 新宿「MY CITY」 6楼的山下书店设立了[「XYZ 留言板BOX」](./xyzbox.md)  
> ▽征集想让CH解决的独特委托、有趣委托  
> ▽优秀的委托被作品采用，还有亲笔签名的单行本礼物  
> ▽用配备的特制Postcard投递  
> →Card图案是以Bunch的第22/23号合并特刊封面为蓝本  
>   
### ●2002年09月06日(金)  
> Comic Bunch第40号(9月20日号)发售  
> 「Angel Heart」第58話  
> 【親子の絆】  
> ▽特别附录CH等手机用贴纸 
>   
### ●2002年08月30日(金)  
> Comic Bunch第39号(9月13日号)发售  
> 「Angel Heart」第57話  
> 【押しかけC・H】  
>   
### ●2002年08月30日(金)<font color=#ff0000>之前</font>  
> 通过预购英文版漫画杂志「RAIJIN COMICS」的年度订阅  
>「CITY HUNTER/高級giclee印刷品和Primagraphy」赠品 （译注：待校对）
> ▽可选择3种不同的模式  
> ▽尺寸157mm×116mm  
> ▽海洋堂「北斗の拳」还可选择肯健次郎的人物 
>   
> <s>每年（A course）550日元×48本=26400日元 运费4800日元 总计31200日元</s>  
> →决定将其定为月刊，年费为6600日元（包括邮费）  
>   
### ●2002年08月23日(金)  
> Comic Bunch第38号(9月6日号)发售  
> 「Angel Heart」第56話  
> 【二匹の野良犬】  
> ▽特别附录AH 手机的贴纸  
>   
### ●2002年08月09日(金)  
> Comic Bunch第36・37合并特刊(8月23日・8月30日号)发售  
> 「Angel Heart」第55话/封面  
> 【槇村兄妹】  
> ▽下一期于8月23日开始销售（有AH封面人物和手机贴纸） （译注：待校对） 
>   
### ●2002年08月02日(金)  
> Comic Bunch第35号(8月16日号)发售  
> 「Angel Heart」第54話  
> 【夢の中の出会い】  
>   
### ●2002年07月26日(金)  
> Comic Bunch第34号(8月9日号)发售  
> 「Angel Heart」第53話  
> 【冴子の誕生日】  
> ▽彩色企画「北条司 ANIME EXPO 2002 Report in L.A.」  
>   
> 「RAIJIN COMICS」预创刊号**City Hunter Special**发售  
> →定价350日元（含税）  
> ▽特别2个主要附录：特别CD（CH有声剧、屏幕保护程序、RAIJIN Junk English摘要）&CH特别鼠标垫  
> ▽7-11、イトーヨーカドー、紀伊国屋書店、文教堂、旭屋書店、三省堂、ジュンク堂書店和其他书店发售  
>   
### ●2002年07月24日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.38 [最終巻FOREVER, CITY HUNTER!!編]」](./book_bw.md#39)发售。286日元+税。  
>   
### ●2002年07月19日(金)  
> Comic Bunch第33号(8月2日号)  
> 「Angel Heart」暂停  
>   
### ●2002年07月16日(火)～08月31日(土)  
> 吉祥寺[BOOKS Ruhe](http://www.books-ruhe.co.jp/)原哲夫／北条司原画展  
> ▽楼梯平台的空间  
> ▽为CH、AH和其他购买漫画书的人抽选签名和Original Goods的Present   
>   
### ●2002年07月12日(金)  
> Comic Bunch第32号(7月26日号)发售  
> 「Angel Heart」第52話  
> 【香との会話】  
>   
### ●2002年07月10日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.38 [にせC・H登場！！編]」](./book_bw.md#38)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第30回 北方謙三「風樹の剣」  
>   
### ●2002年07月09日(火)  
> Bunch Comics**『Angel Heart』[第4巻](./book_bc.md#4)** 发售。505日元+税。  
> ▽Comic Bunch第18期的封面  
>   
### ●2002年07月05日(金)  
> Comic Bunch第31号(7月19日号)发售  
> 「Angel Heart」第51话/封面  
> 【風船のクマさん】  
>   
### ●2002年06月28日(金)  
> Comic Bunch第30号(7月12日号)发售  
> 「Angel Heart」第50話  
> 【狙撃準備完了】  
>   
### ●2002年06月26日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.37 [ヒーローの怪我！？編]」](./book_bw.md#37)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第29回 初野晴「水の時計」  
>   
### ●2002年06月21日(金)  
> Comic Bunch第29号(7月5日号)发售  
> 「Angel Heart」第49話  
> 【ターニャの本当の笑顔】  
>   
### ●2002年06月14日(金)  
> Comic Bunch第28号(6月28日号)发售  
> 「Angel Heart」第48話  
> 【パパの似顔絵】  
>   
### ●2002年06月12日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.36 [涙のペンダント編]」](./book_bw.md#36)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第28回 森村誠一「砂漠の駅（Station）」  
>   
### ●2002年06月07日(金)  
> Comic Bunch第27号(6月21日号)发售  
> 「Angel Heart」第47話  
> 【パパを捜して！！】  
>   
### ●2002年05月31日(金)  
> Comic Bunch第26号(6月14日号)发售  
> 「Angel Heart」第46話  
> 【リョウとベンジャミンと女社長】  
>   
### ●2002年05月29日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.35 [写真を巡る思い出！！編]」](./book_bw.md#35)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第27回 園 子温「自殺circle」  
>   
### ●2002年05月24日(金)  
> Comic Bunch第25号(6月7日号)发售  
> 「Angel Heart」第45話  
> 【殺し屋の習性】  
>   
### ●2002年05月17日(金)  
> Comic Bunch第24号(5月31日号)发售  
> 「Angel Heart」第44話  
> 【依頼人第一号】  
> ▽从今天起，Comic Bunch<font color=#ff0000>每周五</font>发售
>   
> 英文漫画杂志**「RAIJIN COMICS」创刊0号**发售  
> 「CITY HUNTER（英語版）」  
> "EPISODE1: TEN COUNT WITH NO GLORY"  
> ▽配有10cm的健次郎模型  
> ▽在7-11独家销售/300日元（含税）  
> →后来免费发行的这本杂志没有模型  
>   
### ●2002年05月15日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.34 [もっこり十発の陰謀！？編]」](./book_bw.md#34)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第26回 霧舎 巧「四月は霧の00（ラブラブ）密室」  
>   
### ●2002年05月13日(月)～  
> (工作日)从24:55开始  
> 在TBS电台（和其他27个国家网络/27：00～※）  
> **「RAIJIN（雷神） Junk English」**放送中  
> ▽通过CITY HUNTER和其他漫画的对话来学习英语  
> ▽由Coamix提供的广播节目，与「RAIJIN COMICS」连动  
> ▽Personality是Thane Camus  
> 　※大阪电台在23:42播出  
>   
### ●2002年04月30日(火)  
> Comic Bunch第22・23合并特刊(5月14日・5月21日号)发售  
> 「Angel Heart」第43話  
> 【新宿の洗礼】  
>   
### ●2002年04月24日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.33 [ハートマークの逃がし屋！？編]」](./book_bw.md#33)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第25回 山口雅也「垂里冴子のお見合いと推理」  
>   
### ●2002年04月23日(火)  
> Comic Bunch第21号(5月7日号)发售  
> 「Angel Heart」第42話  
> 【おしゃれ】  
>   
### ●2002年04月16日(火)  
> Comic Bunch第20号(4月30日号)发售  
> 「Angel Heart」第41話  
> 【陳老人の店】  
> ▽Center Color 特別企画「在新宿捕获GH！！」  
>   
### ●2002年04月10日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.32 [命懸けのパートナー編]」](./book_bw.md#32)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第24回 吉村達也「杀手街谋杀案」  
>   
### ●2002年04月09日(火)  
> Comic Bunch第19号(4月23日号)发售  
> 「Angel Heart」第40話  
> 【親不孝】  
>   
### ●2002年04月02日(火)  
> Comic Bunch第18号(4月16日号)发售  
> 「Angel Heart」第39話  
> 【李大人からの贈り物】  
>   
### ●2002年04月01日(月)  
> **「CITY HUNTER 3D Art Crystal Paperweight」**（译注：水晶镇纸）  
> ▽限量220件・[网上销售](http://www.coamix.co.jp/)从中午开始  
> ▽锤子、蜻蜓、乌鸦，3种类型  
> →价格4500日元（不含税）+500日元（不含税）的邮费  
>   
### ●2002年03月27日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.31 [冴子のお見合い！！編]」](./book_bw.md#31)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第23回 高部正樹「雇佣兵的骄傲」  
>   
### ●2002年03月26日(火)  
> Comic Bunch第17号(4月9日号)发售  
> 「Angel Heart」第38話  
> 【おかえり】  
>   
### ●2002年03月19日(火)  
> Comic Bunch第16号(4月2日号)发售  
> 「Angel Heart」第37話  
> 【影のパーパ】  
>   
### ●2002年03月13日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.30 [祖父、現る！？編]」](./book_bw.md#30)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第22回 森 詠「横浜狼犬（hound dog）」  
>   
### ●2002年03月12日(火)  
> Comic Bunch第15号(3月26日号)发售  
> 「Angel Heart」第36話  
> 【船上の出会いと別れ】  
>   
### ●2002年03月08日(金)  
> Bunch Comics**『Angel Heart』[第3巻](./book_bc.md#3)** 发售。505日元+税。  
> ▽封面是Comic Bunch新年7/8号特刊AH扉页  
>   
### ●2002年02月27日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.29 [都会のシンデレラ！！編]」](./book_bw.md#29)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第21回 戸梶圭太「ご近所探偵TOMOE」  
>   
### ●2002年02月26日(火)  
> Comic Bunch第13号(3月12日号)发售  
> 「Angel Heart」第35話  
> 【パパと娘の初デート】  
> ▽…下一期（第14期）是AH暂停  
>   
### ●2002年02月19日(火)  
> Comic Bunch第12号(3月5日号)发售  
> 「Angel Heart」第34話/封面  
> 【退院】  
>   
### ●2002年02月13日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.28 [突然の出会い！！編]」](./book_bw.md#28)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第20回 谷 恒生　警視庁歌舞伎町分室「新宿暴力街」  
>   
### ●2002年02月12日(火)  
> Comic Bunch第11号(2月26日号)发售  
> 「Angel Heart」第33話  
> 【一年ぶりの衝撃】  
> ▽…下一期（第12期）是AH封面 
>   
### ●2002年02月06日(水)  
> **LAWSON BUNCH WORLD SELECTION「CITY HUNTER」**LAWSON发售  
> →定价495日元＋税  
> 「がんばれ！香ちゃん！！編」  
> 「海坊主にゾッコン！！編」  
>   
### ●2002年02月05日(火)  
> Comic Bunch第10号(2月19日号)发售  
> 「Angel Heart」第32話  
> 【パーパ】（译注：帕帕。待校对）   
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
> <  
### ●2002年01月30日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.27 [美女と野獣！？編]」](./book_bw.md#27)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第19回 赤川次郎「水手服和机枪」  
>   
### ●2002年01月29日(火)  
> Comic Bunch第9号(2月12日号)发售  
> 「Angel Heart」第31話  
> 【最後の決着(ケリ)】  
>   
### ●2002年01月16日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.26 [天使の落としもの！？編]」](./book_bw.md#26)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第18回 柘植久慶「雇佣兵」系列  
>   
### ●2002年01月15日(火)  
> Comic Bunch新年7・8合并特刊(1月29日・2月5日号)发售  
> 「Angel Heart」第30話/封面人物  
> 【失われた名前】  
>   
### ●2002年01月08日(火)  
> **『北条司Episode 1』**发售  
> →定价320日元  
> ▽包括CITY HUNTER在内的北条司作品的连载第1话，共五部  
> ▽包括一张CITY HUNTERxCat's Eye的大海报  
>   
### ●2002年01月04日(金)  
> Comic Bunch新年5・6合并特刊(1月16日・1月22日号)发售  
> 「Angel Heart」第29話  
> 【李大人の制裁】  
> ▽下期，AH将是巻頭Color  
>   
### ●2001年12月19日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.25 [飛ぶのが怖い！？編]」](./book_bw.md#25)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第17回 小鷹信光「探偵物語」系列  
>   
### ●2001年12月18日(火)  
> Comic Bunch新年3・4合并特刊(1月10日・1月15日号)发售  
> 「Angel Heart」第28話  
> 【命にかえてでも】  
>   
### ●2001年12月16(日)～2002年01月31(木)  
> 吉祥寺[BOOKS Ruhe](http://www.books-ruhe.co.jp/)Coamix原画展  
> ▽在楼梯平台的空间里（译注：待校对）  
> ▽「Angel Heart」等、「蒼天の拳」「251」等  
>   
### ●2001年12月11日(火)  
> Comic Bunch新年2号(1月8日号)发售  
> 「Angel Heart」第27話  
> 【訓練生時代の想い出】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年12月08日(土)  
> Bunch Comics**『Angel Heart』[第2巻](./book_bc.md#2)** 发售。505日元+税。  
>   
>   
### ●2001年12月05日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.24 [ふりかえったO・SHI・RI編]」](./book_bw.md#24)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」柴田よしき「For Dear Life」  
>   
### ●2001年12月04日(火)  
> Comic Bunch新年1号(1月1日号)发售  
> 「Angel Heart」第26話  
> 【惜しみない命】  
>   
### ●2001年11月27日(火)  
> Comic Bunch第29号(12月11日号)发售  
> 「Angel Heart」第25話  
> 【甦った過去】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年11月21日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.23 [セーラー服パニック！編]」](./book_bw.md#23)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第15回 Raymond Chandler「Playback」等  
>   
### ●2001年11月20日(火)  
> Comic Bunch第28号(12月4日号)  
> 「Angel Heart」は休載  
>   
### ●2001年11月13日(火)  
> Comic Bunch第27号(11月27日号)发售  
> 「Angel Heart」第24話  
> 【忘れていたもの】  
>   
### ●2001年11月07日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.22 [ビル街のコールサイン編]」](./book_bw.md#22)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第14回 Lillian J. Brown「猫は殺しをかぎつける」  
>   
### ●2001年11月06日(火)  
> Comic Bunch第26号(11月20日号)发售  
> 「Angel Heart」第23話  
> 【戦士たちの絆】  
>   
### ●2001年10月30日(火)  
> Comic Bunch第25号(11月13日号)发售  
> 「Angel Heart」第22話  
> 【生き続ける理由】  
>   
### ●2001年10月24日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.21 [再び空飛ぶオシリ！編]」](./book_bw.md#21)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第13回 ダシール・ハメット「マルタの鷹」  
>   
### ●2001年10月16日(火)  
> Comic Bunch第23・24合并特大号(10月30日・11月6日号)发售  
> 「Angel Heart」第21話/封面画（译注：待校对）  
> 【戦士の決意】  
> ▽包括AH特别宽幅海报  
> 　→AH漫画第1卷的封面图案  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年10月10日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.20 [哀しい天使編編]」](./book_bw.md#20)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第12回 鳴海章「撃つ」  
>   
### ●2001年10月09日(火)  
> Bunch Comics[「Angel Heart 第1巻」](./book_bc.md#1)发售  
>   
> Comic Bunch第22号(10月23日号)发售  
> 「Angel Heart」第20話  
> 【新宿で宣戦布告！！】  
> ▽下周的合并号有opening color和宽幅彩色海报（译注：待校对）  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年10月02日(火)  
> Comic Bunch第21号(10月16日号)发售  
> 「Angel Heart」第19話  
> 【ミステリアスなリョウ】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年10月01日(月)  
> CH的A3日历，现在可以在[Coamix HP](http://www.coamix.co.jp/)获得。  
> 10月中旬开始提供桌面日历  
>   
### ●2001年9月26日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.19 [海坊主(ファルコン)にゾッコン！！編]」](./book_bw.html#19)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第11回 Sue Grafton 「Alibi的A」（キンジー・ミルホーン系列）
>   
### ●2001年9月25(火)  
> Comic Bunch第20号(10月9日号)发售  
> 「Angel Heart」第18話/封面  
> 【グラスハートのときめき】  
> ▽「听漫」 Drama CD、Comics书带的应征券等Present    
> ▽[Coamix HP](http://www.coamix.co.jp/)＆[北条司公式Homepage](http://www.hojo-tsukasa.com/)、Comics发行信息更新  
>   
### ●2001年9月18日(火)  
> Comic Bunch第19号(10月2日号)  
> 「Angel Heart」暂停  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年9月12日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.18 [やってきた春編]」](./book_bw.md#18)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第10回 乃南Asa 「凍える牙」（女刑警 音道貴子系列）  
>   
### ●2001年9月11日(火)  
> Comic Bunch第18号(9月25日号)发售  
> 「Angel Heart」第17話  
> 【運命の対面】  
> ▽Comics第1巻、10月9日(火)发售決定  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年9月04日(火)  
> Comic Bunch第17号(9月18日号)发售  
> 「Angel Heart」第16話  
> 【出生の秘密】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年8月29日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.17 [恋人はCity Hunter編]」](./book_bw.md#17)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第9回 田中芳樹「薬師寺涼子の怪奇事件簿」系列  
>   
### ●2001年8月28日(火)  
> Comic Bunch第16号(9月11日号)发售  
> 「Angel Heart」第15話  
> 【李兄弟との宿運】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
> ▽CH「On Demand Personal Calendar」将于10月1日开始使用  
>   
### ●2001年8月21日(火)  
> Comic Bunch第15号(9月4日号)发售  
> 「Angel Heart」第14話  
> 【衝撃のフラッシュバック】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年8月08日(水)  
> BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.16 [TOKYO（トーキョー）デート・スクランブル編]」](./book_bw.md#16)发售。286日元+税。  
> <font color=#ff0000>JC未收录扉页</font>  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第8回 Sara Paretsky「Summertime Blues」  
>   
### ●2001年8月07日(火)  
> Comic Bunch第13・14合并特大号(8月21日・28日号)发售  
> 「Angel Heart」第13話/封面画（译注：待校对）  
> 【緊迫する新宿】  
> ▽AH手机的待机图像在测验中 免费DL（8月20日止）  
> ▽手机待机图片是Comic Bunch第3号的封面图片  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年8月02日(木)  
> 「Angel Heart」总集篇将发售 定价290日元  
> ▽第1話～第9話被收录  
> ▽原创T恤（仅限L码） 目前的项目  
>   
### ●2001年7月31日(火)  
> Comic Bunch第12号(8月14日号)发售  
> 「Angel Heart」第12話  
> 【衝撃を超えた真実】  
> 下期的13+14合并号是AH的封面  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年7月28(土) 22:15～23:09  
> NHK BS2『[週刊Book Review](http://www.nhk.or.jp/book/)』  
> ▽关于Coamix的漫画制作文件的mini专题片。  
> ▽有对北条司的Interview。  
>   
### ●2001年7月25日(水)  
> 新潮社BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.15 [越してきた女（ひと）編] 」](./book_bw.md#15)发售。286日元+税。  
> ▽[特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第7回 南里征典「新宿欲望探偵」　大多和伴彦  
>   
### ●2001年7月24(火)  
> Comic Bunch第11号(8月7日号)发售  
> 「Angel Heart」第11話  
> 【事実への昂揚】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年7月17(火)  
> Comic Bunch第10号(7月31日号)发售  
> 「Angel Heart」第10話  
> 【こころとの対話】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
> ▽「CITY HUNTER」ZIPPO（9450日元，含税和运费），中午开始在网上销售→<font color=#ff0000>限量200件，已售完</font>  
>   
### ●2001年7月14日(土) 15時～17時  
> 包括神谷明在内的嘉宾在东京拍卖行/日本广播系统组织的嘉宾访谈节目和慈善拍卖会上。  
> 拍卖品包括「City Hunter」冴羽獠赛璐璐（签名）和「北斗の拳」健次郎人物（签名）。  
> 地点：东京拍卖行　港区麻布十番1-10-10 Jules A座4F  
> 详情→http://www.auctionhouse.co.jp  
> （OMO提供的信息。 谢谢m(__)m）  
>   
### ●2001年7月11日(水)  
> 新潮社BUNCH WORLD B6尺寸漫画 [「CITY HUNTER Vol.14 [海坊主からの依頼編] 」](./book_bw.md#14)发售。286日元+税。  
> [特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第6回 Evelyn E. Smith「Miss Melville」 系列　大多和伴彦  
>   
### ●2001年7月10日(火)  
> Comic Bunch第9号(7月24日号)发售  
> 「Angel Heart」第9話  
> 【衝撃の邂逅】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
### ●2001年7月3日(火)  
> Comic Bunch第8号(7月17日号)发售  
> 「Angel Heart」第8話/封面  
> 【出会いと言う名の再会】  
> ▽[Coamix HP](http://www.coamix.co.jp/)＆[北条司公式Homepage](http://www.hojo-tsukasa.com/)、网站在中午时分重新启动!  
> ▽「CITY HUNTER」ZIPPO、7月17日在[网上销售](http://www.hojo-tsukasa.com/)将会开始！  
> 　价格8500日元（运费：全国500日元，不含消费税）  
>   
### ●2001年6月27日(水)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.13 [トラブル・ scoop ！編]」](./book_bw.md#13)发售。286日元+税。  
> [特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第5回 船戸与一「新宿・夏之死」　大多和伴彦  
>   
### ●2001年6月26日(火)  
> 週刊Comic Bunch第7号(7月10日号)发售  
> 「Angel Heart」第7話/封面画（译注：待校对）  
> 【向公告栏提供帮助】  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
>   
> ・文艺春秋发售的『TITLE』8月号（530日元）的漫画介绍页上有一篇关于意大利空前的漫画热潮的文章。  
> ▽「『City Hunter』在日本掀起了风暴」「『Family Compo』目前非常受欢迎」  
> （mizue提供的信息。谢谢m(__)m）  
>   
### ●2001年6月25(月)  
> 新潮社出版的『波』7月号（100日元）中的「Comic Bunch Interview」。  
> ▽根岸忠氏、原哲夫氏、北条司氏的interview包含其中。  
> ▽还可查看[新潮社website【Ｗｅｂ新潮】](http://www.webshincho.com/)。  
> （[Mary](mailto:rosemary@badgirl.co.jp)提供的信息。谢谢你m(__)m）  
>   
### ●2001年6月24日(日)  
> 在朝日电视台系「Scoop 21」复活的名作漫画的特辑中，推出Coamix如何创刊Comic Bunch的纪录片。由神谷明旁白，北条氏评论。  
>   
### ●2001年6月19日(火)  
> 週刊Comic Bunch第6号(7月3日号)发售  
> 「Angel Heart」第6話  
> 【思い出との再会】之巻  
>   
> ▽下周，AH将成为巻頭Color！  
> ▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
> ▽「CITY HUNTER」ZIPPO将于7月开始在网上销售。 最终价格为8,500日元（不包括运费和消费税）。  
>   
### ●2001年6月13日(水)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.12 [槇村の忘れもの編]」](./book_bw.md#12)发售。286日元+税。  
> [特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第4回 Mickey Spillane「我是法官」　大多和伴彦  
>   
### ●2001年6月12日(火)  
> 週刊Comic Bunch第5号(6月26日号)发售  
> 「Angel Heart」封面/第5話・巻頭Color（译注：待校对）  
> 【思い出に導かれて】之巻  
>   
### ●2001年6月5日(火)  
> 週刊Comic Bunch第4号(6月19日号)发售  
> 「Angel Heart」第4話  
> 【新宿で大暴れ！】之巻  
>   
> ▽下周，AH封面&卷首彩页！  
> ▽可以在6月14日之前在[Coamix HP](http://www.coamix.co.jp/)收听「听漫」！  
>   
### ●2001年5月31日(木)  
> 朝日新聞的文化・娱乐版关于Comic Bunch等里的Hero复活的話題。  
> 「…漫画Hero归来」为题、「**Angel Heart**」中獠说「今后会扮演重要的角色」。 
>   
### ●2001年5月30日(水)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.11 [看護婦には手を出すな！編]」](./book_bw.md#11)发售。286日元+税。  
> [特別栏目] Bunch为City fans提供的book guide！…另一个「City Hunter」第3回 菊地秀行「鬼兽传」　大多和伴彦  
>   
### ●2001年5月29日(火)  
> 週刊Comic Bunch創刊3号(6月12日号)发售  
> 「Angel Heart」封面/第3話  
> 【あの人に会いたくて】之巻  
>   
### ●2001年5月26日(土)  
> 读卖新闻晚间版有一篇介绍Comic Bunch等新漫画杂志的文章。 「『City Hunter』的真的复活」。  
>   
### ●2001年5月25日(金)  
> [「北条司公式Homepage」](http://www.hojo-tsukasa.com/)出售的原创特别T恤已经售罄  
>   
### ●2001年5月22日(火)  
> 周刊 Comic Bunch創刊2号（6月5日）发售→54万册售罄  
> 「Angel Heart」第2話  
> 【絶叫する夢の記憶】之巻  
>   
### ●2001年5月16日(水)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.10 [掠奪してきたひとりの花嫁編]」](./book_bw.md#10)发售。286日元+税。  
> <font color=#ff0000>JC未未收录扉页</font>  
> [特別栏目] Bunch为City fans提供的book guide! ...另一个「City Hunter」第2回 馳星周「不夜城」系列 大多和伴彦  
>   
### ●2001年5月15日(火)  
> 周刊Comic Bunch创刊（5月29日）→72万册售罄  
> 北条司新連載「Angel Heart」第1話  
> 【運命に揺れる暗殺者】之巻  
>   
> ▽新潮社Information 03-3269-4800，神谷明「听漫」的介绍。 「蒼天の拳」的有声剧（至18日）。  
> ▽「CITY HUNTER」ZIPPO商品化決定。  
> ▽「Comic Bunch」电视广告目前正在播出！  
>   
### ●2001年4月下旬  
> 新潮社的新创刊号「週刊Comic Bunch创刊0号」现在可以在书店和便利店买到（免费）。  
>   
> ▽在评论「Angel Heart」时，北条说「~这个美丽的主人公将如何行动，她将来会做什么？ 这是我真正期待的事情。 我可能会被意想不到的发展吓一跳（笑）。」  
>   
### ●2001年4月25日(水)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.9 [天使のほほえみ編]」](./book_bw.md#9)发售。286日元+税。  
> [新登場特別专栏]City Fans的Book Guide! ...另一个「City Hunter」第1回：大沢在昌「新宿鮫」系列，作者是大多和伴彦  
> ▽「Angel Heart」的解释性声明  
>   
### ●2001年4月11日(水)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.8 [気になるあいつ！編]」](./book_bw.md#8)发售。286日元+税。  
> <font color=#ff0000>JC未收录扉页</font>  
> [特別栏目] 必杀！武器目录 火箭筒（M20A1）和榴弹发射器（RPG-7）  
> ▽我们得到消息，新连载作品是「Angel Heart」!  
>   
### ●2001年4月4日(水)  
> 周刊Comic Bunch 5月15日(火)创刊的通知  
> ▽新潮社从今天开始发售的B6版漫画「CAT'S EYE」系列「 Vol.1 [セクシーダイナマイトギャルズ編]」。286日元+税。  
>   
### ●2001年3月27日(火)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.7 [ギャンブルクイーン！編]」](./book_bw.md#7)发售。286日元+税。  
> <font color=#ff0000>JC未收录章节</font>  
> [特別栏目] 必杀！武器目录M16 & M60轻机枪  
>   
### ●2001年3月12日(月)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.6 [待ちつづける少女編]」](./book_bw.md#6)发售。286日元+税。  
> [特別栏目] 必杀！武器目录 Colt Roman MK3 & 千里挑一的S&W41 Magnum  
>   
> ▽原创的特制T恤现在可以在网上买到。  
> ▽从4月起，BUNCH WORLD将在每周三发售。  
>   
### ●2001年2月27日(金)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.5 [空とぶオシリ！編]」](./book_bw.md#5)发售。286日元+税。  
> [新的特别栏目] 必杀！武器目录 Colt Python 357  
>   
### ●2001年2月10日(土)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.4 [その女に手を出すな！編]」](./book_bw.md#4)发售。286日元+税。  
> コラムなし。  
>   
### ●2001年1月27日(土)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.3 [裸足の女優編]」](./book_bw.md#3)发售。286日元+税。  
> Column [Special Photo Series] 渡边克己的新宿故事（完）  
> Strap赠送项目此次结束。  
>   
### ●2001年1月12日(金)  
> 新潮社BUNCH WORLD B6尺寸漫画[「CITY HUNTER Vol.2 [危険な家庭教師編]」](./book_bw.md#2)发售。286日元+税。  
> Column [Special Photo Series] 渡边克己的新宿故事  
>   
### ●2000年12月26日(火)  
> [「北条司公式Homepage」](http://www.hojo-tsukasa.com/)开启！  
>   
> 作为新潮社新推出的「周刊Comic Bunch」的前奏「BUNCH WORLD」B6尺寸漫画「CITY HUNTER」系列，每月12日和27日与「北斗の拳」同时在书店和便利店销售，286日元+税。  
>   
> [「CITY HUNTER Vol.1 [ステキな相棒！編]」](./book_bw.md#1)  
> Column [Special Photo Series] 渡边克己的新宿故事  
> BUNCH WORLD发刊纪念City Hunter，特别推出手机表带，限量10,000人（Vol.3之前的计划。报名截止到2月3日）。（译注：待校对）  
>   
### ●2000年12月20日(水)  
> 集英社[「HOJO TSUKASA 20th ANIVERSARY ILLUSTRATIONS」](./book_aizo.md#ill)（北条司 漫画家20周年記念 画册）发售。包括CH以前的画集中没有的插画。2400日元+税  
>   
### ●2000年11月15日(水)  
> 集英社ALLMAN的「F.COMPO」连载结束  
>   
### ●2000年9月28日(木)  
> 集英社SC Allman愛蔵版[「北条司短編集　天使的礼物」](./book_scaizo.md#tensi)发售。包含作者自己选择的6个故事。1429日元+税
>   
### ●2000年9月12日(火)  
> 新连载的内容是City Hunter这一点是肯定的，编辑部正在考虑各种适合新连载的题材。  
> <font color=#0000ff>感谢Coamix的编辑部的持田様先生给我们写信提供这些信息。m(__)m</font>（译注：待校对）  
>   
### ●2000年9月11日(月)  
> 新潮社的新Comics杂志的信息。  
> 根据新潮社Comics编辑部的答复，北条的连载已经决定，是「City Hunter」的现代的、大人版。第一期于2001年5月出版。  
> （[BBS(http://www.tcup5.com/533/arcadia.html)](../cgi/533/readme.md)泄漏的信息？）  
>   
### ●2000年8月18日(金)  
> 集英社SC Allman[「F.COMPO VOL.13 失楽園（Lost Paradise）」](./book_sc.md#f13)发售。「一日だけの女将」这话故事里出现了很像香话槇村的旅馆的客人。见↓6月21日信息。505日元+税  
>   
### ●2000年8月8日(火)  
> 据悉，新潮社正计划在明年春天之前推出一本漫画周刊，该杂志以「Coamix」为基础，由前「周刊少年Jump」主编堀江信彦、漫画家原哲夫、北条司、声优神谷明共同创立。 这本新的漫画杂志将在8月底前命名，预计创刊时发行量约为50万册。  
> （摘自《产经新闻》文章）  
>   
### ●2000年6月21日(水)  
> 在集英社出版的Allman No.13（2000年7月5日）连载的 「F.COMPO」第60分格中，旅馆的客人看起来像槇村和香・・・。  
>   
### ●2000年4月26日(水)  
> 集英社 Allman特别版的增刊「F.COMPO杰作精选」发售（480日元）。 「特別対话 北条司 VS. 佐藤藍子」中稍微提到了CH。  
>   
### ●2000年2月15日(火)  
> 「City Hunter Perfect Guidebook」的第2次印刷现已完成。 初版中发现的一些校对错误已经得到修正。  
> <font color=#0000ff>感谢集英社Comic文库编辑部在BBS上发布的信息m(__)m</font>  
>   
### ●2000年1月21日(金)  
> 集英社发售[「City Hunter Perfect Guidebook」](./book_aizo.md#perfect)。  
>   
### ●2000年1月18日(周五)  
> 集英社文库Comic版[「北条司短篇集1 City Hunter-XYZ-」](./book_bunko.md#xyz)、[「北条司短篇集2 少年们的夏天 ～Melody of Jenny～」](./book_bunko.md#melody)同时发行。  
>   
>   
> 
> 何かCH関連の最新情報がありましたら、ぜひお教えください。
> 
> お名前　　　　  
> メールアドレス  
> ★情報★  

---

---

**[** Back **]**
