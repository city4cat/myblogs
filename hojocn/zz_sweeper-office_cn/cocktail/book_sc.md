http://www.netlaputa.ne.jp/~arcadia/cocktail/book_sc.html


**CITY HUNTER 関連書籍**

---

> SC ALLMAN

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">獠在温かいご飯之卷中做了一个小小的客串。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">F.COMPO（ファミリー・コンポ）VOL.1</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>「家族」初体験</TD><TD ALIGN="CENTER">4-08-878076-0</TD></TR>
<TR><TD>・1st.day 「家族」初体験<BR>・2nd.day 裸天(ラテン)な夜に<BR>・3rd.day 温かいご飯<BR>・4th.day 入学式の条件<BR>・5th.day キャッチボールの思い出<BR>・6th.day 遠くで見ていた初恋<BR>・7th.day 一肌脱ぎます。</TD><TD ALIGN="CENTER">1997年4月23日<BR>530円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">二人のバースデー之卷中电报杆上的RN侦探社的标志。 辰巳の告白之卷中有心形标志的逃亡车。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600">F.COMPO（ファミリー・コンポ）VOL.9</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>二人のバースデー</TD><TD ALIGN="CENTER">4-08-878266-6</TD></TR>
<TR><TD>・57th.day 二人のバースデー<BR>・58th.day メールラバー<BR>・59th.day ライバルに投げ勝て<BR>・60th.day 辰巳の願い<BR>・61st.day 辰巳の告白<BR>・62nd.day 母のイメージ<BR>・63rd.day 早紀の誘惑</TD><TD ALIGN="CENTER">1999年8月19日<BR>530円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">一日だけの女将之卷，一个长得很像香和槇村（北尾か）的人在客栈出现。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600"><A NAME="f13">F.COMPO（ファミリー・コンポ）VOL.13</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>失楽園（ロスト・パラダイス）</TD><TD ALIGN="CENTER">4-08-878303-4</TD></TR>
<TR><TD>・85th.day 失楽園（ロストパラダイス）<BR>・86th.day 親心<BR>・87th.day 親離れ子離れ<BR>・88th.day 近くて遠い東京<BR>・89th.day 母と娘<BR>・90th.day 衝突<BR>・91th.day 一日だけの女将</TD><TD ALIGN="CENTER">2000年8月18日<BR>530円</TD></TR>
</TABLE>

---

---

**[** [戻る](./book.md) **]**