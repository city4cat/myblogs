http://www.netlaputa.ne.jp/~arcadia/cocktail/book_bunko.html



**CITY HUNTER 関連書籍**

---

> 集英社文庫Comic版

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 01</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617161-9</TD></TR>
<TR><TD>・栄光なきテンカウント！の巻<BR>・BMWの悪魔の巻<BR>・闇からの狙撃者！の巻<BR>・恐怖のエンジェルダストの巻<BR>・ステキな相棒！の巻<BR>・将軍の罠！の巻<BR>・悪党にはなにもやるな！の巻<BR>・危険な家庭教師の巻<BR><BR>・解説　こだま兼嗣</TD><TD ALIGN="CENTER">1996年6月23日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 02</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617162-7</TD></TR>
<TR><TD>・アンバランスギャングの巻<BR>・待ちつづける少女の巻<BR>・裸足の女優の巻<BR>・撮影所パニック！の巻<BR>・ゆれる心の巻<BR>・亡霊を撃て！の巻<BR>・とんだティーチャーの巻<BR>・思い出のキズ跡の巻<BR>・鐘とともに運命が！の巻</TD><TD ALIGN="CENTER">1996年6月23日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 03</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617163-5</TD></TR>
<TR><TD>・その女に手をだすな！の巻<BR>・ワン・オブ・サウザンドの巻<BR>・なんたって執念!!の巻<BR>・恐怖の男センサーの巻<BR>・ギャンブルクィーン！の巻<BR>・哀愁のギャンブラーの巻<BR>・気になるあいつ！の巻</TD><TD ALIGN="CENTER">1996年8月18日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 04</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617164-3</TD></TR>
<TR><TD>・恋は盲目！の巻<BR>・危険な国からきた女！の巻<BR>・空飛ぶオシリ！の巻<BR>・とんでる博士！の巻<BR>・心に咲いたチューリップの巻<BR>・天使のほほえみの巻<BR>・愛ってなんですかの巻<BR>・リョウちゃんの恋愛講座の巻</TD><TD ALIGN="CENTER">1996年8月18日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 05</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617165-1</TD></TR>
<TR><TD>・お医者様でも草津の湯でも!!の巻<BR>・初恋の巻<BR>・思い出の渚の巻<BR>・掠奪してきたひとりの花嫁の巻<BR>・アブナイ解毒薬の巻<BR>・看護婦には手を出すな！の巻</TD><TD ALIGN="CENTER">1996年10月23日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 06</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617166-X</TD></TR>
<TR><TD>・さよなら…そしてコンニチハ！の巻<BR>・ある夜のマチガイの巻<BR>・ブラジャー大作戦!!の巻<BR>・槇村の忘れもの の巻<BR>・リョウ…その受難の日の巻<BR>・リョウの純情物語の巻<BR>・美人キャスターの実力の巻<BR>・トラブル・スクープの巻<BR>・ニュース速報 礼子の場合の巻<BR>・出逢って恋して占っての巻<BR>・シンデレラの見る夢は…の巻</TD><TD ALIGN="CENTER">1996年10月23日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 07</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617167-8</TD></TR>
<TR><TD>・暗闇でボッキリ！の巻<BR>・はい、チーズ!!の巻<BR>・占いなんて大嫌い！？の巻<BR>・海坊主からの依頼の巻<BR>・スネーク現る！の巻<BR>・ラスト・コンサートの巻<BR>・香ちゃん狙撃さる！？の巻<BR>・SAYONARA…の巻<BR>・がんばれ！香ちゃん!!の巻<BR>・越してきた女(ひと)の巻<BR>・動き出した犯人の巻</TD><TD ALIGN="CENTER">1996年12月18日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 08</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617168-6</TD></TR>
<TR><TD>・ラブラブ大作戦！の巻<BR>・ジェラシー寸前の巻<BR>・ハーレム地獄の巻<BR>・TOKYOデート・スクランブルの巻<BR>・王女誘拐！？の巻<BR>・告白のエアポートの巻<BR>・越してきたおとなりさんの巻<BR>・舞子のマイ・ボディーガードの巻<BR>・本能のおもむくままに！？の巻<BR>・守ってあげたい！？の巻<BR>・サクセスは遠く！？の巻<BR>・恋人はシティーハンターの巻<BR>・ドア越しの告白の巻<BR>・秘密のバイト君！の巻</TD><TD ALIGN="CENTER">1996年12月18日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 09</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617169-4</TD></TR>
<TR><TD>・懲りない葉子サン！の巻<BR>・暁のMEMORYの巻<BR>・やってきた春の巻<BR>・香ちゃんの初デートの巻<BR>・危険なふたり！の巻<BR>・許してくれ…の巻<BR>・墓地のナンパニストの巻<BR>・あねさんは女子大生！の巻<BR>・悲しき未亡人の巻<BR>・花火をあげろ！盛大に!!の巻<BR>・白いキャンバスの巻<BR>・海坊主にゾッコン!!の巻<BR>・史上最大の作戦！の巻<BR>・海坊主の愛情の巻</TD><TD ALIGN="CENTER">1997年2月23日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 10</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617170-8</TD></TR>
<TR><TD>・海坊主が愛した女(ひと)の巻<BR>・哀しい天使の巻<BR>・すねた紗羅ちゃんの巻<BR>・勇気をください！の巻<BR>・再び空飛ぶオシリ！の巻<BR>・人質になったもっこりの巻<BR>・泥棒勝負開始!!の巻<BR>・鏡の秘密の巻<BR>・憂いのMy Sisterの巻<BR>・姉妹のきずなの巻<BR>・指輪に秘めた夢の巻<BR>・リョウさん、出番です!!の巻<BR>・さよならの向こう側…の巻</TD><TD ALIGN="CENTER">1997年2月23日<BR>600円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 11</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617171-6</TD></TR>
<TR><TD>・ビル街のコールサインの巻<BR>・夢の中の憧れの男(ひと)の巻<BR>・セーラー服パニック！の巻<BR>・プロポーズ狂想曲！？の巻<BR>・嫌い帰れも好きのうち！？の巻<BR>・怒りのマグナム！の巻<BR>・17年目のおばあちゃんの巻<BR>・お嬢さんにパイソンを！の巻<BR>・天下無敵の女探偵誕生！？の巻<BR>・ふりかえったO・SHI・RIの巻</TD><TD ALIGN="CENTER">1997年4月23日<BR>610円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 12</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617172-4</TD></TR>
<TR><TD>・涙のバースデーの巻<BR>・女心を知るものは…の巻<BR>・愛は銃よりも強く！の巻<BR>・明日へのリバイバルの巻<BR>・飛ぶのが怖い！？の巻<BR>・飛べないスイーパーの巻<BR>・Fright,Flight(恐怖の飛行)の巻<BR>・エアポート'89の巻<BR>・大空の告白の巻<BR>・天使の落としもの！？の巻<BR>・憧れのうしろ姿の巻<BR>・香、努力する!!の巻<BR>・決闘！蝙蝠V.S.リョウ!!の巻<BR>・背中にさよなら!!の巻</TD><TD ALIGN="CENTER">1997年4月23日<BR>610円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 13</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617173-2</TD></TR>
<TR><TD>・依頼人は幽霊！？の巻<BR>・ふたつの顔をもつ女！？の巻<BR>・暗闇でドッキリ・デートの巻<BR>・鏡の中のもうひとりの私！？の巻<BR>・炎の中のピアニスト!!の巻<BR>・Play it again, Mami! -あの曲をもう一度-の巻<BR>・美女と野獣！？の巻<BR>・港の決闘!!の巻<BR>・涙の街頭募金の巻<BR>・突然の出会い!!の巻<BR>・引き裂かれた心の巻<BR>・強く、堅い絆の巻<BR>・マイ・フェア・リョウちゃん！？の巻</TD><TD ALIGN="CENTER">1997年6月23日<BR>610円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 14</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617174-0</TD></TR>
<TR><TD>・香が水着に着がえたら！？の巻<BR>・美しき標的!!の巻<BR>・コートの秘密！？の巻<BR>・華麗なる脱出!!の巻<BR>・都会のシンデレラ!!の巻-前編-<BR>・都会のシンデレラ!!の巻-後編-<BR>・殺したい男!!の巻<BR>・暁の決断!!の巻<BR>・過去の傷跡の巻<BR>・賽は投げられた!!の巻<BR>・勝敗の行方!!の巻<BR>・賢者の贈り物!!の巻<BR>・祖父、現る！？の巻<BR>・狙われたリョウの許嫁！？の巻<BR>・誘拐された<FONT SIZE="-2">じいさん付き</FONT>下着!!の巻</TD><TD ALIGN="CENTER">1997年6月23日<BR>610円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 15</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617175-9</TD></TR>
<TR><TD>・卑劣な罠(トラップ)！？の巻<BR>・ふたりのシティーハンターの巻<BR>・伊集院隼人氏の平穏な一日の巻<BR>・冴子のお見合い!!の巻<BR>・アニキの残像!!の巻<BR>・変えられた台本(シナリオ）!!の巻<BR>・命懸けのパートナーの巻<BR>・失われた過去!!の巻<BR>・死を呼ぶ暗示!!の巻<BR>・思い出を消して…の巻<BR>・ハートマークの逃がし屋！？の巻<BR>・恐怖の運送!!の巻<BR>・暗号を歌う女！？の巻</TD><TD ALIGN="CENTER">1997年8月17日<BR>610円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 16</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617176-7</TD></TR>
<TR><TD>・あきれた大逃走!!の巻<BR>・あなたをパートナーに!!の巻<BR>・リョウと恐るべき似た者姉妹!!の巻<BR>・父、来襲す!!の巻<BR>・卑怯者!!の巻<BR>・ふたりでひとりの心!!の巻<BR>・もっこり十発の陰謀！？の巻<BR>・消えたもっこり!!の巻<BR>・おかしなふたり!!の巻</TD><TD ALIGN="CENTER">1997年8月17日<BR>610円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 17</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617177-5</TD></TR>
<TR><TD>・写真を巡る思い出!!の巻<BR>・悲しき旅立ち!!の巻<BR>・涙のペンダントの巻<BR>・嵐の前…の巻<BR>・地獄への出航!!の巻<BR>・愛と憎しみと…!!の巻<BR>・ペンダントの記憶!!の巻<BR>・息子よ!! の巻<BR>・地獄からの生還!!の巻</TD><TD ALIGN="CENTER">1997年10月22日<BR>610円</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600">CITY HUNTER（シティーハンター）VOLUME 18</FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>-</TD><TD ALIGN="CENTER">4-08-617178-3</TD></TR>
<TR><TD>・ヒーローの怪我！？の巻<BR>・ワンワンスイーパー!!の巻<BR>・にせＣ・Ｈ(シティーハンター)登場!!の巻<BR>・かみあうふたり!!の巻<BR>・素直な気持ちに!!の巻<BR>・ウェディング・ベル！の巻<BR>・FOREVER, CITY HUNTER!!の巻<BR><BR>・あとがき　北条司</TD><TD ALIGN="CENTER">1997年10月22日<BR>610円</TD></TR>
</TABLE><BR><BR>

<TABLE WIDTH="80%" BORDER="1">
<TR><TD><FONT COLOR="#FF6600"><A NAME="xyz">北条司 短編集1</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>シティーハンター -XYZ-</TD><TD ALIGN="CENTER">4-08-617304-2</TD></TR>
<TR><TD>・シティーハンター -XYZ-<BR>・シティーハンター -ダブル-エッジ-<BR>・ネコまんまおかわり<BR>・天使の贈りもの<BR>・少女の季節 -サマードリーム-<BR>・桜の花 咲くころ<BR>・おれは男だ！</TD><TD ALIGN="CENTER">2000年1月23日<BR>本体571円+税</TD></TR>
</TABLE>

<TABLE WIDTH="80%" BORDER="1"><CAPTION ALIGN="BOTTOM"><FONT COLOR="RED">↑TAXI DRIVER和Family Plot有Saeba公寓和獠。</FONT></CAPTION>
<TR><TD><FONT COLOR="#FF6600"><A NAME="melody">北条司 短編集2</A></FONT></TD><TD ALIGN="CENTER">北条司</TD></TR>
<TR><TD>少年たちのいた夏 ～Melody of Jenny～</TD><TD ALIGN="CENTER">4-08-617305-0</TD></TR>
<TR><TD>・蒼空の果て… -少年たちの戦場-<BR>・少年たちのいた夏 ～Melody of Jenny～<BR>・American Dream<BR>・TAXI DRIVER<BR>・ファミリー・プロット</TD><TD ALIGN="CENTER">2000年1月23日<BR>本体571円+税</TD></TR>
</TABLE>

---

---

**[** [戻る](./book.md) **]**