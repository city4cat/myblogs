http://www.netlaputa.ne.jp/~arcadia/iframe02.html

**→[2003年的信息](./iframe03.md)**  
  
**▼2002年的信息**  
  
■12/27(金)  
　Comic Bunch新年4・5合并特大号 发售  
　「**Angel Heart**」第71話/封面  
　【依頼は殺し！？】  
　▽下期系1月10日，并附有Calendar  
  
■12/20(金)  
　Comic Bunch新年3号(1月10日号)发售  
　「**Angel Heart**」第70話  
　【香瑩の変化】  
  
■12/06(金)  
　Comic Bunch新年1・2合并特大号 发售  
　「**Angel Heart**」第69話  
　【笑顔の二人】  
　▽下期是12月20日发售  
  
■12/05(木)  
　英語版漫画雑誌  
　RAIJIN COMICS（ライジンComicス）2003年1月号发售  
　「**CITY HUNTER**（英語版）」  
　"EPISODE4: THE DEVIL IN THE BMW part 3"  
　▽番外篇「爆发了 "mokkori"大争论！！」  
　→对50名母语人士和英语语言专家进行深入采访  
　▽特別附录 Listening CD（CH和北斗の拳）  
　▽CH海报作为50名问卷调查礼物  
　→店面销售定价880日元（含税）  
  
■11/29(金)  
　Comic Bunch第52号(12月13日号)发售  
　「**Angel Heart**」第68話/巻頭Color/封面  
　【優しい心音(おと)】  
  
■11/22(金)  
　Comic Bunch第51号(12月6日号)  
　「**Angel Heart**」暂停。  
  
■11/15(金)  
　Comic Bunch第50号(11月29日号)发售  
　「**Angel Heart**」第67話  
　【不公平な幸せ】  
　▽下期是AH休載→第52号巻頭Color再开  
  
■11/09(土)  
　★Bunch Comics  
　「**Angel Heart**」第5巻 发售  
　▽封面是Comic Bunch第31号封面  
  
■11/08(金)  
　Comic Bunch第49号(11月22日号)发售  
　「**Angel Heart**」第66話  
　【天使の心】  
  
■11/05(火)  
　英語版漫画雑誌  
　RAIJIN COMICS（ライジンComicス）創刊2号（12月号）发售  
　「CITY HUNTER（英語版）」/封面  
　"EPISODE3: THE DEVIL IN THE BMW part 2"  
　▽特別附录 Listening CD（CH和北斗の拳）  
　▽CH海报作为50名问卷调查礼物  
　→店面销售定价880日元（含税）  
  
■11/01(金)  
　Comic Bunch第48号(11月15日号)发售  
　「**Angel Heart**」第65話/封面  
　【香瑩の決意】  
  
■10/25(金)  
　Comic Bunch第47号(11月8日号)发售  
　「**Angel Heart**」第64話  
　【突きつけられた真実】  
  
■10/18(金)  
　Comic Bunch第46号(11月1日号)发售  
　「**Angel Heart**」第63話  
　【夢を守る！】  
  
■10/11(金)  
　Comic Bunch第45号(10月25日号)发售  
　「**Angel Heart**」第62話  
　【媽媽の心臓】  
  
■10/05(土)  
　英語版漫画雑誌 創刊  
　RAIJIN COMICS（ライジンComicス）創刊号（11月号）发售  
　「CITY HUNTER（英語版）」  
　"EPISODE2: THE DEVIL IN THE BMW part 1"  
　▽特別附录 Listening CD（CH和北斗の拳）  
　▽CH海报作为100名问卷调查礼物  
　→毎月5日发售／店内销售880日元（含税）  
  
■10/04(金)  
　Comic Bunch第44号(10月18日号)发售  
　「**Angel Heart**」第61話/封面  
　【孤独な少女】  
  
■09/27(金)  
　Comic Bunch第43号(10月11日号)  
　「**Angel Heart**」暂停。  
  
■09/20(金)  
　Comic Bunch第42号(10月4日号)发售  
　「**Angel Heart**」第60話  
　【心の傷】  
  
■09/13(金)  
　Comic Bunch第41号(9月27日号)发售  
　「**Angel Heart**」第59話/封面  
　【C・Hが犯人！？】  
  
■09月/初旬～11月/最后一天为止  
新宿「MY CITY」6楼的山下書店设立了[「**XYZ 伝言板BOX**」](./cocktail/xyzbox.md)  
▽征集想让CH解决的独特委托、有趣委托  
▽优秀的委托被作品采用，还有亲笔签名的单行本Present  
▽用配备的特制Postcard投递  
→ Card系Bunch第22・23合并特刊封面的图案  
  
■09/06(金)  
　Comic Bunch第40号(9月20日号)发售  
　「**Angel Heart**」第58話  
　【親子の絆】  
　▽特別付録CH等手机用贴纸  
  
■08/30(金)  
　Comic Bunch第39号(9月13日号)发售  
　「**Angel Heart**」第57話  
　【押しかけC・H】  
  
■08/30(金)までに  
通过预购英文版漫画杂志「RAIJIN COMICS」的年度订阅  
「CITY HUNTER/高級giclee印刷品和Primagraphy」赠品 （译注：待校对） 
▽可选择3种不同的模式  
▽尺寸157mm×116mm  
▽海洋堂「北斗の拳」还可选择肯健次郎的人物 
  
　每年（A course）550日元×48本=26400日元 运费4800日元 总计31200日元  
　→决定将其定为月刊，年费为6600日元（包括邮费）  
  
■08/23(金)  
　Comic Bunch第38号(9月6日号)发售  
　「**Angel Heart**」第56話/巻頭Color  
　【二匹の野良犬】  
　▽特別付録AH手机用贴纸  
  
■08/09(金)  
　Comic Bunch第36・37合并特大号 发售  
　「**Angel Heart**」第55話/封面  
　【槇村兄妹】  
　▽下期系8月23日发售（有AH封面人物和手机贴纸） （译注：待校对）   
  
■08/02(金)  
　Comic Bunch第35号(8月16日号)发售  
　「**Angel Heart**」第54話  
　【夢の中の出会い】  
  
■07/26(金)  
　Comic Bunch第34号(8月9日号)发售  
　「**Angel Heart**」第53話  
　【冴子の誕生日】  
　▽Color企划「北条司 ANIME EXPO 2002 Report in L.A.」   
  
「RAIJIN COMICS」预创刊号**City Hunter・ Special **发售  
→定价350日元（含税）  
▽特别2个主要附录：特别CD（CH有声剧、屏幕保护程序、RAIJIN Junk English摘要）&CH特别鼠标垫  
▽7-11、イトーヨーカドー、紀伊国屋書店、文教堂、旭屋書店、三省堂、ジュンク堂書店和其他书店发售  
  
■07/24(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.39  
　[最終巻FOREVER, CITY HUNTER!!編]」发售  
  
■07/19(金)  
　Comic Bunch第33号(8月2日号)  
　「**Angel Heart**」暂停。  
  
■07/16(火)～08/31(土)  
吉祥寺[BOOKS Ruhe](http://www.books-ruhe.co.jp/)原哲夫／北条司原画展  
▽在楼梯平台的空间里  
▽通过抽奖向CH和AH等漫画购买者赠送签名彩纸和Original Goods的Present  
  
■07/12(金)  
　Comic Bunch第32号(7月26日号)发售  
　「**Angel Heart**」第52話  
　【香との会話】  
  
■07/10(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.38  
　[にせC・H登場！！編]」发售  
　▽Book Guide on Bunch！第30回 北方謙三「風樹の剣」  
  
■07/09(火)  
　★Bunch Comics  
　「**Angel Heart**」第4巻 发售  
　▽封面是Comic Bunch第18号封面  
  
■07/05(金)  
　Comic Bunch第31号(7月19日号)发售  
　「**Angel Heart**」第51話/封面  
　【風船のクマさん】  
  
■06/28(金)  
　Comic Bunch第30号(7月12日号)发售  
　「**Angel Heart**」第50話  
　【狙撃準備完了】  
  
■06/26(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.37  
　[ヒーローの怪我！？編]」发售  
　▽Book Guide on Bunch！第29回 初野晴「水の時計」  
  
■06/21(金)  
　Comic Bunch第29号(7月5日号)发售  
　「**Angel Heart**」第49話  
　【ターニャの本当の笑顔】  
  
■06/14(金)  
　Comic Bunch第28号(6月28日号)发售  
　「**Angel Heart**」第48話  
　【パパの似顔絵】  
  
■06/12(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.36  
　[涙のペンダント編]」发售  
　▽Book Guide on Bunch！第28回 森村誠一「砂漠の駅（Station）」  
  
■06/07(金)  
　Comic Bunch第27号(6月21日号)发售  
　「**Angel Heart**」第47話/封面  
　【パパを捜して！！】  
  
■05/31(金)  
　Comic Bunch第26号(6月14日号)发售  
　「**Angel Heart**」第46話  
　【リョウとベンジャミンと女社長】  
  
■05/29(水)  
　Bunch WorldB6尺寸Comic  
　「**CITY HUNTER** Vol.35  
　[写真を巡る思い出！！編]」发售  
　▽Book Guide on Bunch！第27回 園 子温「自殺Circle」  
  
■05/24(金)  
　Comic Bunch第25号(6月7日号)发售  
　「**Angel Heart**」第45話  
　【殺し屋の習性】  
  
■05/17(金)  
　Comic Bunch第24号(5月31日号)发售  
　「**Angel Heart**」第44話  
　【依頼人第一号】  
　▽今天起，Comic Bunch每周五发售  
  
　英語版漫画雑誌「RAIJIN COMICS」創刊0号 发售  
　「**CITY HUNTER**（英語版）」  
"EPISODE1: TEN COUNT WITH NO GLORY"  
▽配有10cm的健次郎模型  
▽在7-11独家销售/300日元（含税）  
→后来免费发行的这本杂志没有模型  
  
■05/15(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.34  
　[もっこり十発の陰謀！？編]」发售  
　▽Book Guide on Bunch！第26回 霧舎 巧「四月は霧の00（ラブラブ）密室」  
  
■05/13(月)～  
(工作日)从24:55开始  
在TBS电台（和其他27个国家网络/27：00～※）   
**「RAIJIN（雷神） Junk English」**放送中  
▽通过CITY HUNTER和其他漫画的对话来学习英语  
▽由Coamix提供的广播节目，与「RAIJIN COMICS」连动  
▽Personality是Thane Camus  
　※大阪电台在23:42播出  
  
■04/30(火)  
　Comic Bunch第22・23合并特大号 发售  
　「**Angel Heart**」第43話/封面  
　【新宿の洗礼】  
  
■04/24(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.33  
　[ハートマークの逃がし屋！？編]」发售  
　▽Book Guide on Bunch！第25回 山口雅也「垂里冴子のお見合いと推理」  
  
■04/23(火)  
　Comic Bunch第21号(5月7日号)发售  
　「**Angel Heart**」第42話  
　【おしゃれ】  
  
■04/16(火)  
　Comic Bunch第20号(4月30日号)发售  
　「**Angel Heart**」第41話  
　【陳老人の店】  
　▽CenterColor特別企划「在新宿捕获GH！！」  
  
■04/10(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.32  
　[命懸けのパートナー編]」发售  
　▽Book Guide on Bunch！第24回 吉村達也「キラー通り殺人事件」  
  
■04/09(火)  
　Comic Bunch第19号(4月23日号)发售  
　「**Angel Heart**」第40話  
　【親不孝】  
  
■04/02(火)  
　Comic Bunch第18号(4月16日号)发售  
　「**Angel Heart**」第39話/封面  
　【李大人からの贈り物】  
  
■04/01(月)  
　「**CITY HUNTER** 3D Art Crystal Paperweight」  
▽限量220个，从中午开始[网上销售](http://www.coamix.co.jp/)  
▽ Hammer 、蜻蜓、乌鸦的3种类型可选  
→价格4500日元（不含税）+邮费500日元（不含税）  
  
■03/27(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.31  
　[冴子のお見合い！！編]」发售  
　▽Book Guide on Bunch！第23回 高部正樹「傭兵の誇り」  
  
■03/26(火)  
　Comic Bunch第17号(4月9日号)发售  
　「**Angel Heart**」第38話  
　【おかえり】  
  
■03/19(火)  
　Comic Bunch第16号(4月2日号)发售  
　「**Angel Heart**」第37話  
　【影のパーパ】  
  
■03/13(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.30  
　[祖父、現る！？編]」发售  
　▽Book Guide on Bunch！第22回 森 詠「横浜狼犬（Hound Dog）」  
  
■03/12(火)  
　Comic Bunch第15号(3月26日号)发售  
　「**Angel Heart**」第36話/巻頭Color  
　【船上の出会いと別れ】  
  
■03/08(金)  
　★Bunch Comics  
　「**Angel Heart**」第3巻 发售  
　▽封面是Comic Bunch新年7・8合并特大号AH扉絵  
  
■02/27(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.29  
　[都会のシンデレラ！！編]」发售  
　▽Book Guide on Bunch！第21回 戸梶圭太「ご近所探偵TOMOE」  
  
■02/26(火)  
　Comic Bunch第13号(3月12日号)发售  
　「**Angel Heart**」第35話  
　【パパと娘の初デート】  
　▽…下期（14号）系AH暂停   
  
■02/19(火)  
　Comic Bunch第12号(3月5日号)发售  
　「**Angel Heart**」第34話/封面  
　【退院】  
  
■02/13(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.28  
　[突然の出会い！！編]」发售  
　▽Book Guide on Bunch！第20回 谷 恒生　警視庁歌舞伎町分室「新宿暴力街」  
  
■02/12(火)  
　Comic Bunch第11号(2月26日号)发售  
　「**Angel Heart**」第33話  
　【一年ぶりの衝撃】  
　▽…下期（12号）系AH封面  
  
■02/06(水)  
　Lawson・Bunch World Selection「**CITY HUNTER**」  
　Lawson发售。定价495日元＋税  
　「がんばれ！香ちゃん！！編」  
　「海坊主にゾッコン！！編」  
  
■02/05(火)  
　Comic Bunch第10号(2月19日号)发售  
　「**Angel Heart**」第32話  
　【パーパ】  
　▽[Coamix HP](http://www.coamix.co.jp/)、AH的概要更新  
  
■01/30(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.27  
　[美女と野獣！？編]」发售  
　▽Book Guide on Bunch！第19回 赤川次郎「Sailor服和机枪」  
  
■01/29(火)  
　Comic Bunch第9号(2月12日号)发售  
　「**Angel Heart**」第31話  
　【最後の決着(ケリ)】  
  
■01/16(水)  
　Bunch World B6尺寸Comic  
　「**CITY HUNTER** Vol.26  
　[天使の落としもの！？編]」发售  
　▽Book Guide on Bunch！第18回 柘植久慶「傭兵」Series  
  
■01/15(火)  
　Comic Bunch新年7・8合并特大号 发售  
　「**Angel Heart**」第30話/巻頭Color  
　【失われた名前】  
  
■01/08(火)  
　Comic Bunch 1月22日増刊号  
　「北条司 Episode 1」发售。定价320日元  
　▽**CITY HUNTER**等，共5部作品的第1話出版  
　▽附带CITY HUNTER×Cat's Eye的Big Poster  
  
■01/04(金)  
　Comic Bunch新年5・6合并特大号 发售  
　「**Angel Heart**」第29話  
　【李大人の制裁】  
　▽下期，AH将是巻頭Color   
  
**→[2001年的信息](./iframe01.md)**