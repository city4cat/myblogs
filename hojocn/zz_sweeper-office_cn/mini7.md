www.netlaputa.ne.jp/~arcadia/mini7.html

[戻る](./mini6.md)

>   
> # Mini Cooper复活  
>   
> 　1989年，Rover集团被卖给了英国宇航公司，这是一家航空和其他公司，这两家公司曾被划分为负责乘用车的「Austin Rover Group」和负责四轮驱动车的「Land Rover Group」，并合并为一家名为「Rover Cars」的公司。 自1989年以来，Mini车一直由Rover Cars公司销售。 这导致日本公司「Austin Rover Japan」也改名为「Rover Japan」。  
>   
> 　然后在1990年，Mini Cooper被Rover公司恢复了。 最初作为2000辆的限量版推出（600辆分配给日本），它很快就在全球范围内销售一空，Rover在次年的1991年将Cooper作为目录车型推出。  
>   
> 　今天复活的Rover Mini Cooper1.3仅仅是一款配备了顶级Metro1.3发动机、白色车身顶棚和Mk2型镀铬前格栅的Mayfair。 底盘和悬挂与1000cc版本基本相同，使其成为区别于以前的Mini Cooper S的车型。 换句话说，它是一个只重现Cooper S氛围的车型。 自然，它没有Cooper S的个性，包括全轮驱动汽车所固有的驾驶困难。 这是一款以实用性能为主要考虑的车型。    
>   
> 　1992年，Mini系列改用电子喷射技术，采用计算机化的气体控制，取代了长期使用的SU化油器。  
>   
> 　Cooper 1.3也发展成了Cooper 1.3i，其中i是指喷射的i。 Mini和Mini Cooper的发动机是通过喷射进行电子控制的，比老式的Mini车更容易操作。 毕竟，只需轻点油门就可以启动发动机（这在现代汽车中是可以期待的）。 (以前，驾驶室操作的Mini车在启动发动机时，必须拉动「扼流圈」来调整油门开度）。  
>   
> 　然而，那些认为模拟质量是使Mini车卓越的原因的人，往往对喷射技术望而却步。 肯定有一些顽固的粉丝认为，当你称自己为Cooper时，你必须有两个化油器。    
>   
>   

[次へ](./mini8.md)

---

---

[あばうとミニへ](./mini0.md)