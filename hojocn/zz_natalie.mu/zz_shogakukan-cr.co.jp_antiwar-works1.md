https://www.shogakukan-cr.co.jp/book/b110795.html


# 漫画が語る戦争　焦土の鎮魂歌

焦土の哀しみを知り平和を訴える名作漫画集  
焦土之哀，呼吁和平的名作漫画集  

著者

[手塚　治虫](../author/a929.html) 著  
[北条　司](../author/a53863.html) 著  
[中沢　啓治](../author/a53864.html) 著  

ジャンル  
体裁    
[Comic](../search/g118.html)  

Series  
[復刻名作漫画Series](../search/s38.html)

出版年月日  
2013/07/29

ISBN  
9784778032579

判型・Page数  
4-6・480Page

定価  
1,650円（税込）

内容紹介

目次

平和の尊さを「戦争を知らない若い世代」に伝えるため、戦争をテーマにした短編マンガを集成。「焦土の鎮魂歌」は、銃後の日本を舞台にしたマンガを集成します。機銃掃射の犠牲になった学童たちを描いた手塚治虫の『カノン』、集団疎開していた子どもたちとアメリカ人捕虜の交流を描いた北条司の『少年たちのいた夏』、原爆の悲劇を描いた中沢啓治の『黒い鳩の群れに』、自ら体験した満州引揚者の苦闘を描いたちばてつやの『家路1945～2003』など、戦争の恐ろしさや、戦争の愚かさを描いた作品を多数収録します。  
为了向“不了解战争的年轻一代”传达和平的珍贵，汇集了以战争为主题的短篇漫画。《焦土的镇魂歌》是以日本后方为舞台的漫画合集。手冢治虫的《Kanon》描写因机关枪扫射而牺牲的学童、北条司的《少年们的夏天》描写集体疏散的孩子们和美国俘虏之间的交流、中泽启治的《黑鸽群》描写原子弹爆炸的悲剧、描绘了亲身经历战争的满洲遣返者的ちばてつや的《家路1945～2003》等多部描写战争的恐怖和战争的愚蠢的作品。

Kanon（手塚治虫）、石の戦場（巴里夫）、火の瞳（政岡としや・早乙女勝元原作）、少年たちのいた夏（北条司）、黒い鳩の群れに（中沢啓治）、ヒロシマのおばちゃん（曽根富美子）、家路1945～2003（ちばてつや）、回転（山上たつひこ）、御身大事に（村野守美『垣根の魔女』より）  
Kanon(手冢治虫)、石之战场(巴里夫)、火之瞳(政冈としや·早乙女胜元原作)、少年们的夏天(北条司)、黑鸽群(中泽启治)、广岛的阿姨(曾根富美子)、家路1945 ~ 2003（ちばてつや）、回转(山上龙彦)、保重身体(出自村野守美《篱笆的魔女》)（译注：待校对）  

[![漫画が語る戦争　焦土の鎮魂歌](https://hondana-image.s3.amazonaws.com/book/image/110795/f37a86b2-cab6-49b0-8f4b-3fdcdc49f406.jpg)](https://www.shogakukan-cr.co.jp/contact/?book_no=110795&category=%8F%91%90%D0%82%D6%82%CC%8A%B4%91z)
