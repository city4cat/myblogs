https://natalie.mu/comic/news/158041


# 北条司と実写版・冴羽リョウの上川隆也が行きつけのバーで対談、ゼノンに
北条司和真人版冴羽獠的上川隆也在他们最喜欢的Zenon酒吧对谈

2015年8月25日 [Comment](https://natalie.mu/comic/news/158041/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)と「Angel Heart」ドラマ版にて冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）を演じる俳優・[上川隆也](https://natalie.mu/comic/artist/14598)の対談記事が、本日8月25日に発売された月刊Comic Zenon10月号（徳間書店）に掲載されている。  
北条司和《Angel Heart》电视剧版中饰演冴羽獠的演员上川隆也的对谈报道刊登在8月25日发行的月刊Comic Zenon 10月号(德间书店)上。

[
![「Angel Heart 2ndシーズン」の冴羽リョウ。(c)北条司/NSP 2010](https://ogre.natalie.mu/media/news/comic/2015/0825/AH2ndryo.jpg?imwidth=468&imdensity=1)
「Angel Heart 2ndシーズン」の冴羽リョウ。(c)北条司/NSP 2010  
大きなサイズで見る（全4件）  
“Angel Heart 2ndSeason”的冴羽獠。(c)北条司/NSP 2010  
大尺寸查看(全4件)  
](https://natalie.mu/comic/gallery/news/158041/401338)

対談が行われたのは、北条の行きつけだというバー。2人は「シティーハンター」から「Angel Heart」にかけてリョウがどのように変化していったかを、読者と作者の視点からトークした。上川は昔のリョウを“オス”と表現しつつ、20代男子の憧れが詰まった存在だと評価。北条は「若気の至り」と執筆当時を振り返りながら、リョウに込めた思いを語った。  
对谈的地点是北条常去的酒吧。两人从读者和作者的角度讨论了从《城市猎人》到《Angel Heart》，獠是如何变化的。上川一边用“雄性”来形容过去的獠，一边评价他是充满20多岁男子憧憬的存在。北条一边回顾写作当时的情形，一边说“年轻气盛之至”，同时表达了自己对獠的想法。  

なお今号には、同誌のWebサイト「WEBコミックぜにょん」にて連載中の[モリコロス](https://natalie.mu/comic/artist/9941)「タレソカレ」が出張。オールカラーの特別編が掲載されている。  
另外，在该杂志的网站“Web Comic Zenon”上连载中的モリコロス的“タレソカレ”出張。刊登了全彩色的特别篇。（译注：待校对）  

この記事の画像（全4件）   
这篇报道的图片(共4篇)  

[![「Angel Heart 2ndシーズン」の冴羽リョウ。(c)北条司/NSP 2010](https://ogre.natalie.mu/media/news/comic/2015/0825/AH2ndryo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/158041/401338 "「Angel Heart 2ndシーズン」の冴羽リョウ。(c)北条司/NSP 2010")
[![上川隆也](https://ogre.natalie.mu/media/news/comic/2015/0812/ah-kawakami.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/158041/396862 "上川隆也")
[![「CR 義風堂々!!～兼続と慶次～」のホール導入を記念した、月刊Comic Zenon10月号付録のクリアしおり(c)武村勇治・原哲夫・堀江信彦/NSP2010, (c)義風堂々2013](https://ogre.natalie.mu/media/news/comic/2015/0825/zenon1510-furoku.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/158041/401339 "「CR 義風堂々!!～兼続と慶次～」のホール導入を記念した、月刊Comic Zenon10月号付録のクリアしおり(c)武村勇治・原哲夫・堀江信彦/NSP2010, (c)義風堂々2013（“CR 义风堂堂!!~兼续与庆次~”的大厅导入纪念，月刊Comic Zenon 10月号附录的Clearしおり(c)武村勇治·原哲夫·堀江信彦/NSP2010， (c)义风堂堂2013）")
[![月刊Comic Zenon10月号](https://ogre.natalie.mu/media/news/comic/2015/0825/zenon1510.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/158041/401340 "月刊Comic Zenon10月号")

LINK

[Comic Zenon](http://www.comic-zenon.jp/)
[Angel Heart | 日本TV](http://www.ntv.co.jp/angelheart/)