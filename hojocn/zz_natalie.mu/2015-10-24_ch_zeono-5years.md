https://natalie.mu/comic/news/163908

# ゼノン創刊5周年号は「シティーハンター」新作アニメの先行視聴パス収録  
Zenon创刊5周年收录《城市猎人》新作动画先行视听Path  

2015年10月24日 [Comment](https://natalie.mu/comic/news/163908/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター XYZ Edition」では、全12巻の購入者特典として新作アニメ「リョウのプロポーズ（リョウの漢字はけものへんに「僚」のつくり）」のDVDを用意。そのアニメを先行視聴できるパスワードが、本日10月24日発売の月刊コミックゼノン12月号（徳間書店）に掲載されている。  
北条司的“城市猎人 XYZ Edition”，为全12卷的购买者准备的特典是新作动画“獠的求婚(獠的汉字是反犬旁和“僚”)”的DVD。今天（10月24日）发售的月刊漫画Zenon的12月号（德间书店）上有一个提前观看动画的密码。  

[
![アニメ「リョウのプロポーズ」の1シーン。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1024/ch-anime001.jpg?impolicy=hq&imwidth=730&imdensity=1)
アニメ「リョウのプロポーズ」の1シーン。(c)北条司/NSP 1985  
大きなサイズで見る（全16件）  
动画“獠的求婚”的一个场景。(c)北条司/NSP 1985  
大尺寸查看(全16件)  
](https://natalie.mu/comic/gallery/news/163908/425226)

「リョウのプロポーズ」の制作は、TVアニメ「ジョジョの奇妙な冒険」のオープニングなどを手がけた映像制作集団・神風動画が担当。映像は北条によるマンガの線を活かしたモーショングラフィックアニメになり、声優陣には[神谷明](https://natalie.mu/comic/artist/60997)、伊倉一恵らオリジナルキャストが集合する。視聴動画では本編の一部と、メイキングを披露。メイキングは今後も継続して公開される。2回目は11月13日にアップ予定だ。  
《獠的求婚》的制作由亲自制作过TV动画《JoJo的奇妙冒险》片头等的映像制作集团·神风动画负责。影像是活用了北条的漫画线条的Motion Graph Anime，声优阵容集合了神谷明、伊仓一惠等原班人马。视听视频中披露了正篇的一部分和幕后花絮。拍摄花絮今后也将继续公开。第二段视频预定在11月13日上传。  

またゼノンは今号にて創刊5周年。これを記念して[新久千映](https://natalie.mu/comic/artist/10758)「ワカコ酒」の主人公・村崎ワカコのフィギュアが100名にプレゼントされる。フィギュアは座った状態のワカコを立体化したもので、机の端などに乗せられる仕様。なお「ワカコ酒」はTVドラマ第2期の製作が決定しており、2016年1月8日からBSジャパンで放送が開始される。  
另外，Zenon杂志于本期创刊5周年。作为纪念，新久千映的《若子酒》的主人公·村崎若子的人偶将作为礼物送给100人。手办是将坐着的若子立体化的样子，放在桌子的一端等的规格。另外，《ワカコ酒》已决定制作电视剧第2期，将于2016年1月8日开始在BS Japan播出。

そのほか「花鬼扉の境目屋さん」のオイカワマコによる新連載「グレンデル」が12月号では開始。罪人として処刑されるはずだった女剣士カメリアが、とある護衛任務を完遂したら自由になれると取り引きを持ちかけられたことから物語は動き出す。  
此外，《花鬼门之境屋》的作者小川真子的新连载《グレンデル》将在12月号上开始连载。本应作为罪人被处刑的女剑士卡梅里亚，被人提出只要完成某护卫任务就能获得自由的交易，故事由此展开。  

この記事の画像（全16件）  
这篇报道的图片(共16篇)  

[![アニメ「リョウのプロポーズ」の1シーン。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1024/ch-anime001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425226 "アニメ「リョウのプロポーズ」の1シーン。(c)北条司/NSP 1985（这是动画片《獠的求婚》中的一个场景。(c)北条司/NSP 1985）")
[![絵の下地となるCGを作るため、役者の動きを撮影する様子。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1024/ch-anime003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425227 "絵の下地となるCGを作るため、役者の動きを撮影する様子。(c)北条司/NSP 1985（为了制作成为画的基础的CG，拍摄演员的动作的样子。(c)北条司/NSP 1985）")
[![実際の役者の演技をCGに起こしたもの。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1024/ch-anime002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425228 "実際の役者の演技をCGに起こしたもの。(c)北条司/NSP 1985（在CG中追踪实际演员的表演。(c)北条司/NSP 1985）")
[![ゼノン5周年記念で100名にプレゼントされるワカコのフィギュア。(c)新久千映/NSP 2011](https://ogre.natalie.mu/media/news/comic/2015/1024/wakako-koshikake001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425229 "ゼノン5周年記念で100名にプレゼントされるワカコのフィギュア。(c)新久千映/NSP 2011（芝诺5周年纪念被送给100人的ワカコ的手办。(c)新久千映/NSP 2011）")
[![ゼノン5周年記念で100名にプレゼントされるワカコのフィギュア。(c)新久千映/NSP 2011](https://ogre.natalie.mu/media/news/comic/2015/1024/wakako-koshikake002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425230 "ゼノン5周年記念で100名にプレゼントされるワカコのフィギュア。(c)新久千映/NSP 2011（芝诺5周年纪念赠送给100人的和香子的手办。(c)新久千映/NSP 2011）")
[![オイカワマコによる新連載「グレンデル」カット。(c)オイカワマコ/NSP 2015](https://ogre.natalie.mu/media/news/comic/2015/1028/grendel-sashikae.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425231 "オイカワマコによる新連載「グレンデル」カット。(c)オイカワマコ/NSP 2015（小川真子的新连载《glendel》剪辑。(c)小川真子/NSP 2015）")
[![新連載「グレンデル」カット。(c)オイカワマコ/NSP 2015](https://ogre.natalie.mu/media/news/comic/2015/1024/grendel002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425232 "新連載「グレンデル」カット。(c)オイカワマコ/NSP 2015（新连载《glendel》剪报。(c)小川真子/NSP 2015）")
[![月刊Comic Zenon 12月号。](https://ogre.natalie.mu/media/news/comic/2015/1024/zenon1512.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425233 "月刊コミックゼノン12月号。")
[![「シティーハンター XYZ Edition」1巻](https://ogre.natalie.mu/media/news/comic/2015/0624/CHXYZ01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/375065 "「シティーハンター XYZ Edition」1巻")
[![「シティーハンター XYZ Edition」2巻](https://ogre.natalie.mu/media/news/comic/2015/0624/CHXYZ02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/375066 "「シティーハンター XYZ Edition」2巻")
[![「シティーハンター XYZ Edition」3巻](https://ogre.natalie.mu/media/news/comic/2015/1024/xyz3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425234 "「シティーハンター XYZ Edition」3巻")
[![「シティーハンター XYZ Edition」4巻](https://ogre.natalie.mu/media/news/comic/2015/1024/xyz4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425235 "「シティーハンター XYZ Edition」4巻")
[![「シティーハンター XYZ Edition」5巻](https://ogre.natalie.mu/media/news/comic/2015/1024/xyz5.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425236 "「シティーハンター XYZ Edition」5巻")
[![「シティーハンター XYZ Edition」6巻](https://ogre.natalie.mu/media/news/comic/2015/1024/xyz6.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/425237 "「シティーハンター XYZ Edition」6巻")
[![「シティーハンター XYZ Edition」7巻](https://ogre.natalie.mu/media/news/comic/2015/1017/XYZ7.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/422127 "「シティーハンター XYZ Edition」7巻")
[![「シティーハンター XYZ Edition」8巻](https://ogre.natalie.mu/media/news/comic/2015/1017/XYZ8.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163908/422128 "「シティーハンター XYZ Edition」8巻")

LLINK

[Comic Zenon](http://www.comic-zenon.jp/)  