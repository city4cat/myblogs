https://natalie.mu/comic/news/263969

# ジャンプ協力ドラマ、ナレーションは神谷明「への突っ張りで頑張って」とエール
Jump合作drama，旁白神谷明声援“坚持到底加油”  

2018年1月5日  [Comment](https://natalie.mu/comic/news/263969/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


週刊少年ジャンプ（集英社）全面協力によるテレビドラマ「Oh My Jump！～少年ジャンプが地球を救う～」のオープニングナレーションを[神谷明](https://natalie.mu/comic/artist/60997)が担当する。  
周刊少年Jump(集英社)全面合作的电视剧《Oh My Jump!~少年Jump拯救地球~》的Opening Narration将由神谷明担任。

[
![「Oh My Jump！～少年ジャンプが地球を救う～」第1話より。](https://ogre.natalie.mu/media/news/comic/2018/0104/ohmyjump01.jpg?impolicy=hq&imwidth=730&imdensity=1)
「Oh My Jump！～少年ジャンプが地球を救う～」第1話より。
大きなサイズで見る（全5件）  
《Oh My Jump! ~少年Jump拯救地球~》第1话。  
查看大图(共5件)  
](https://natalie.mu/comic/gallery/news/263969/844935)

[
![「Oh My Jump！～少年ジャンプが地球を救う～」第1話より。](https://ogre.natalie.mu/media/news/comic/2018/0104/ohmyjump02.jpg?imwidth=468&imdensity=1)
「Oh My Jump！～少年ジャンプが地球を救う～」第1話より。［拡大］  
《Oh My Jump!~少年Jump拯救地球~》第1话。[扩大]
](https://natalie.mu/comic/gallery/news/263969/844936)

[ゆでたまご](https://natalie.mu/comic/artist/1756)「キン肉マン」のキン肉マン、[武論尊](https://natalie.mu/comic/artist/2383)原作による[原哲夫](https://natalie.mu/comic/artist/1917)「北斗の拳」のケンシロウ、[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」の冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）などこれまでにさまざまなジャンプアニメで主人公を演じてきた神谷。ナレーションを担当するにあたり、「当時の少年が、自分たちの歴史をジャンプ魂で切り開いていく。考えただけでもワクワクしちゃいます。私も最後まで作品に寄り添って行こうと思います。スタッフ、キャストの皆さん『への突っ張り』で頑張ってください」とコメントを寄せた。  
ゆでたまご《筋肉人》中的筋肉人、根据武论尊原作改编的原哲夫《北斗神拳》中的健四郎、北条司《城市猎人》中的冴羽獠(獠的汉字是反犬旁加“僚”的写法)等至今为止各种各样的Jump动画中饰演主人公的神谷。在担任旁白时，他说:“当时的男孩们正在用Jump精神打开自己的历史。光是想想就觉得很兴奋。我也想一直陪伴作品到最后。希望工作人员、演员们‘努力应对’”。（译注：待校对）

「Oh My Jump！～少年ジャンプが地球を救う～」は、テレビ東京金曜深夜放送の「ドラマ24」枠の50作目のタイトルとして1月12日よりオンエア。[伊藤淳史](https://natalie.mu/comic/artist/14098)演じるかつてヒーローに憧れていたことなかれ主義の営業マン・月山が、「1日1回、ヒーローらしいことをする」というルールを課せられた秘密クラブ「Oh My Jump！」と出会い、成長していく姿を描いていく。なお第1話の場面写真も公開されている。  
《Oh My Jump!~少年Jump拯救地球~》作为东京电视台周五深夜播出的“电视剧24”时段的第50部作品的标题，将于1月12日开播。伊藤淳史饰演的曾经崇拜英雄、奉行“英雄主义”的销售员月山，他与秘密俱乐部“Oh My Jump！”相遇并长大，该俱乐部以“每天做一次英雄般的事情”为规则。描绘其成长的身影。另外，第一话的剧照也已公开。


## 神谷明Comment

[
![神谷明](https://ogre.natalie.mu/media/news/comic/2018/0104/kamiya.jpg?imwidth=468&imdensity=1)
神谷明［拡大］
](https://natalie.mu/comic/gallery/news/263969/844937)

ジャンプ作品に初めて携わったのは『荒野の少年イサム』。その後『キン肉マン』、『北斗の拳』、『シティーハンター』と続きました。思えば、素晴らしい作品、キャラクターたち 更に、沢山のジャンプファンに恵まれ、歩んで来られたのだと思います。  
第一次参与Jump作品是《荒野少年伊萨姆》。之后是《筋肉人》、《北斗神拳》、《城市猎人》。 回顾过去，我认为我得到了精彩的作品和角色，以及许多Jump粉丝的祝福。  

ジャンプの旗印は、「友情」、「努力」、「勝利」。そこに数多くの優れた作品が生まれ、ファンの方々に親しまれた秘密があると思います。そのジャンプ魂を、今回ドラマで見られるという知らせが届きました。当時の少年が、自分たちの歴史をジャンプ魂で切り開いていく。考えただけでもワクワクしちゃいます。私も最後まで作品に寄り添って行こうと思います。スタッフ、キャストの皆さん「への突っ張り」で頑張ってください。  
Jump的旗帜是“友情”、“努力”、“胜利”。在那里产生了很多优秀的作品，我想有一个深受粉丝们喜爱的秘密。有消息说，这次可以在电视剧中看到那个Jump精神。当时的少年，用Jump精神开创了自己的历史。光是想想就觉得很兴奋。我也想一直陪伴作品到最后。希望工作人员、演员们‘努力应对’。（译注：待校对）

この記事の画像（全5件）  
这篇报道的图片(共5篇)  

[![「Oh My Jump！～少年ジャンプが地球を救う～」第1話より。](https://ogre.natalie.mu/media/news/comic/2018/0104/ohmyjump01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/263969/844935 "「Oh My Jump！～少年ジャンプが地球を救う～」第1話より。")
[![「Oh My Jump！～少年ジャンプが地球を救う～」第1話より。](https://ogre.natalie.mu/media/news/comic/2018/0104/ohmyjump02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/263969/844936 "「Oh My Jump！～少年ジャンプが地球を救う～」第1話より。")
[![神谷明](https://ogre.natalie.mu/media/news/comic/2018/0104/kamiya.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/263969/844937 "神谷明")
[![「Oh My Jump！～少年ジャンプが地球を救う～」のポスタービジュアル。](https://ogre.natalie.mu/media/news/comic/2017/1228/ohmy.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/263969/841017 "「Oh My Jump！～少年ジャンプが地球を救う～」のポスタービジュアル。")
[![「Oh My Jump！ ～少年ジャンプが地球を救う～」ロゴ。](https://ogre.natalie.mu/media/news/comic/2017/1210/ohmyjump_rogo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/263969/828873 "「Oh My Jump！ ～少年ジャンプが地球を救う～」ロゴ。")

## ドラマ24第50弾特別企画「Oh My Jump！～少年ジャンプが地球を救う～」
电视剧24第50弹特别企划“Oh My Jump!~少年Jump拯救地球~”

放送日時：2018年1月12日（金）より毎週金曜24:12～（※テレビ大阪のみ、毎週月曜24:12～）  
播放日期:2018年1月12日(周五)起每周五24:12 ~(仅大阪电视台每周一24:12 ~)  
放送局：テレビ東京、テレビ北海道、テレビ愛知、テレビ大阪、テレビせとうち、TVQ九州放送  
放送局:东京电视台、北海道电视台、爱知电视台、大阪电视台、设内电视台、九州电视台  

  
監督：三木康一郎、今泉力哉、青山貴洋ほか  
脚本：根本ノンジ、西条みつとし、横幕智裕、粟島瑞丸、守口悠介  
チーフプロデューサー：浅野太（テレビ東京）  
プロデューサー：阿部真士（テレビ東京）、橘康仁・堀英樹（ドリマックステレビジョン）  
制作：テレビ東京／ドリマックステレビジョン  
导演:三木康一郎、今泉力哉、青山贵洋等  
编剧:根本浓治、西条光敏、横幕智裕、粟岛瑞丸、守口悠介  
总制作人:浅野太(东京电视台)  
制作人:阿部真士(东京电视台)、橘康仁·堀英树(dolimax电视台)  
制作:东京电视台/东京电视台  

  
出演：[伊藤淳史](https://natalie.mu/comic/artist/14098)、[生駒里奈](https://natalie.mu/comic/artist/68843)、[馬場徹](https://natalie.mu/comic/artist/54625)、[柳俊太郎](https://natalie.mu/comic/artist/65252)、[山崎萌香](https://natalie.mu/comic/artist/95793)、[斉木しげる](https://natalie.mu/comic/artist/6044)、[ケンドーコバヤシ](https://natalie.mu/comic/artist/6953)、[佐藤仁美](https://natalie.mu/comic/artist/16063)、[寺脇康文](https://natalie.mu/comic/artist/15584)ほか



(c)「Oh My Jump！」製作委員会

LINK

[「Oh My Jump！」公式サイト](http://www.tv-tokyo.co.jp/oh_my_jump/)  
[『週刊少年ジャンプ50周年』ニュース一覧｜集英社『週刊少年ジャンプ』公式サイト](http://www.shonenjump.com/j/50th/)  