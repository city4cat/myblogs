https://natalie.mu/comic/film/list/artist_id/2405

# 北条司の映画作品

[
![劇場版City Hunter 天使の涙（Angel Dust）](https://ogre.natalie.mu/media/ex/film/189680/flyer_1.jpg?impolicy=thumb_fit&height=260&width=260)  
劇場版City Hunter 天使の涙（Angel Dust）  
監督：こだま兼嗣  
2023年09月30日（土）公開  
](https://natalie.mu/eiga/film/189680)

[
![City Hunter THE MOVIE 史上最香のMission](https://ogre.natalie.mu/media/ex/film/182791/flyer_1.jpg?impolicy=thumb_fit&height=260&width=260)  
City Hunter THE MOVIE 史上最香のMission  
監督：Philippe Lachaux  
2019年11月29日（金）公開  
](https://natalie.mu/eiga/film/182791)

[
![Angel Sign](https://ogre.natalie.mu/media/ex/film/182396/flyer_1.jpg?impolicy=thumb_fit&height=260&width=260)  
Angel Sign  
監督：ハム・トランほか  
2019年11月15日（金）公開  
](https://natalie.mu/eiga/film/182396)

[
![劇場版City Hunter <新宿Private Eyes>](https://ogre.natalie.mu/media/ex/film/175572/flyer_1.jpg?impolicy=thumb_fit&height=260&width=260)  
劇場版City Hunter <新宿Private Eyes>  
監督：こだま兼嗣  
2019年02月08日（金）公開  
](https://natalie.mu/eiga/film/175572)

[
![CAT'S EYE](https://ogre.natalie.mu/media/ex/film/115006/flyer_1.jpg?impolicy=thumb_fit&height=260&width=260)  
CAT'S EYE  
監督：林海象  
1997年08月30日（土）公開  
](https://natalie.mu/eiga/film/115006)

[
![City Hunter](https://ogre.natalie.mu/media/ex/film/807208/flyer_1.jpg?impolicy=thumb_fit&height=260&width=260)  
City Hunter  
監督：Barry Wong  
](https://natalie.mu/eiga/film/807208)

[
![City Hunter 愛と宿命のMagnum](https://ogre.natalie.mu/media/ex/film/800337/flyer_1.jpg?impolicy=thumb_fit&height=260&width=260)  
City Hunter 愛と宿命のMagnum  
監督：こだま兼嗣  
](https://natalie.mu/eiga/film/800337)

[
![City Hunter Bay City Wars](https://ogre.natalie.mu/media/ex/film/800334/flyer_1.jpg?impolicy=thumb_fit&height=260&width=260)  
City Hunter Bay City Wars  
監督：こだま兼嗣  
](https://natalie.mu/eiga/film/800334)

[
![City Hunter 百万ドルの陰謀](https://ogre.natalie.mu/asset/natalie/common/polar/common/image/eiga/blank_artist.png?impolicy=thumb_fit&width=180&height=180)  
City Hunter 百万ドルの陰謀  
監督：こだま兼嗣  
](https://natalie.mu/eiga/film/800383)