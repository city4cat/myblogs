https://natalie.mu/comic/news/160486


# 【9月19日付】本日発売の単行本リスト
【9月19日付】本日発売の単行本List

2015年9月19日  [Comment](https://natalie.mu/comic/news/160486/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


コミックナタリーより、本日9月19日に発売される単行本をお知らせいたします。本日発売の雑誌リストは[こちら](https://natalie.mu/comic/news/160310)から。  
Comic Natalie将于9月19日发售单行本。今天发售的杂志列表在这里。

## 一迅社

[「放課後カンパニー」](http://www.amazon.co.jp/exec/obidos/ASIN/4758074836/nataliecomic-22) みさちゅう/Nash  
[「姉妹ちがい 米田和佐短編集」](http://www.amazon.co.jp/exec/obidos/ASIN/4758065489/nataliecomic-22) [米田和佐](https://natalie.mu/comic/artist/9160)  
[「だんちがい（4）」](http://www.amazon.co.jp/exec/obidos/ASIN/4758082464/nataliecomic-22) 米田和佐

## KADOKAWA

[「フォルティッシモ（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4048654144/nataliecomic-22) ウダジョ/藤谷燈子/ハラダサヤカ/叶瀬あつこ  
[「うたの☆プリンスさまっ♪All Star コミックアンソロジー」](http://www.amazon.co.jp/exec/obidos/ASIN/4048654128/nataliecomic-22) アンソロジー/ブロッコリー  
[「盾の勇者の成り上がり（4）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678095/nataliecomic-22) [藍屋球](https://natalie.mu/comic/artist/9449)/アネコユサギ/弥南せいら  
[「フロアに魔王がいます（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678079/nataliecomic-22) 川上真樹/はと  
[「ちおちゃんの通学路（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678117/nataliecomic-22) 川崎直孝  
[「八男って、それはないでしょう！（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/404067796X/nataliecomic-22) 楠本弘樹/Y.A/藤ちょこ  
[「ガールズ＆パンツァー 激闘！マジノ戦ですっ!!（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678192/nataliecomic-22) [才谷屋龍一](https://natalie.mu/comic/artist/10694)/ガールズ＆パンツァー製作委員会  
[「カーグラフィティJK（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678060/nataliecomic-22) さきしまえのき  
[「女子大生の日常（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678044/nataliecomic-22) [津々巳あや](https://natalie.mu/comic/artist/8793)  
[「学戦都市アスタリスク（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678052/nataliecomic-22) にんげん/三屋咲ゆう/okiura  
[「ディーふらぐ！（10）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678036/nataliecomic-22) [春野友矢](https://natalie.mu/comic/artist/5400)  
[「断裁分離のクライムエッジ（11）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678028/nataliecomic-22) [緋鍵龍彦](https://natalie.mu/comic/artist/7953)  
[「無職転生～異世界行ったら本気だす～（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678087/nataliecomic-22) フジカワユカ/理不尽な孫の手/シロタカ  
[「魔弾の王と戦姫（8）」](http://www.amazon.co.jp/exec/obidos/ASIN/4040678109/nataliecomic-22) 柳井伸彦/川口士/よし☆ヲ/片桐雛太

## スクウェア・エニックス

[「アラクニド（13）」](http://www.amazon.co.jp/exec/obidos/ASIN/4757547404/nataliecomic-22) いふじシンセン/村田真哉  
[「コープスパーティー（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4757547420/nataliecomic-22) 肝匠（FriendlyLand）/映画「コープスパーティー」製作委員会  
[「ブラッディ・クロス（12）」](http://www.amazon.co.jp/exec/obidos/ASIN/4757547358/nataliecomic-22) [米山シヲ](https://natalie.mu/comic/artist/8275)  
[「今日のケルベロス（5）」](http://www.amazon.co.jp/exec/obidos/ASIN/4757547366/nataliecomic-22) [桜井亜都](https://natalie.mu/comic/artist/3653)  
[「スクールガールストライカーズAnthology Channel」](http://www.amazon.co.jp/exec/obidos/ASIN/4757547390/nataliecomic-22) スクウェア・エニックス  
[「その吸血鬼は正しくない夢をみる（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4757546203/nataliecomic-22) [葉月めぐみ](https://natalie.mu/comic/artist/4917)  
[「Now playing（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/475754717X/nataliecomic-22) 一二三  
[「魔女に与える鉄鎚（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4757547412/nataliecomic-22) [檜山大輔](https://natalie.mu/comic/artist/5177)/村田真哉  
[「スクールガールストライカーズComic Channel（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4757547382/nataliecomic-22) [桃山ひなせ](https://natalie.mu/comic/artist/5351)  
[「Im～イム～（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4757547374/nataliecomic-22) 森下真

## 青泉社

[「鬼談4 死人蝶」](http://www.amazon.co.jp/exec/obidos/ASIN/4907203330/nataliecomic-22) 櫂広海  
[「幽子と俺の奇妙な日常 ムーンライト 山内規子ホラー傑作選」](http://www.amazon.co.jp/exec/obidos/ASIN/4907203322/nataliecomic-22) 山内規子

## 青林工藝舎

[「暖かい日陰に」](http://www.amazon.co.jp/exec/obidos/ASIN/4883794156/nataliecomic-22) 神村篤

## 徳間書店

[「スズキさんはただ静かに暮らしたい（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199802959/nataliecomic-22) 佐藤洋寿  
[「おとりよせ王子 飯田好実（6）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199802940/nataliecomic-22) [高瀬志帆](https://natalie.mu/comic/artist/10331)  
[「ちるらん新撰組鎮魂歌（13）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199802932/nataliecomic-22) [橋本エイジ](https://natalie.mu/comic/artist/7769)/梅村真也  
[「シティーハンター XYZ edition（5）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199802975/nataliecomic-22) [北条司](https://natalie.mu/comic/artist/2405)  
[「シティーハンター XYZ edition（6）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199802983#/nataliecomic-22) 北条司  
[「鳴沢くんはおいしい顔に恋してる（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199802967/nataliecomic-22) [山田怜](https://natalie.mu/comic/artist/8004)

## 日本文芸社

[「新宿セブン（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4537133384/nataliecomic-22) [奥道則](https://natalie.mu/comic/artist/3183)/観月昴  
[「隠密包丁～本日も憂いなし～（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4537133406/nataliecomic-22) [本庄敬](https://natalie.mu/comic/artist/4823)/花形怜  
[「今日からヒットマン（31）」](http://www.amazon.co.jp/exec/obidos/ASIN/4537133414/nataliecomic-22) [むとうひろし](https://natalie.mu/comic/artist/7640)  
[「白竜LEGEND（38）」](http://www.amazon.co.jp/exec/obidos/ASIN/4537133392/nataliecomic-22) [渡辺みちお](https://natalie.mu/comic/artist/8161)/天王寺大

## フロンティアワークス

[「生徒会長のジジョウ」](http://www.amazon.co.jp/exec/obidos/ASIN/4861348145/nataliecomic-22) 大槻ミゥ  
[「くちびるに蝶の骨2～バタフライ・ルージュ～」](http://www.amazon.co.jp/exec/obidos/ASIN/4861348110/nataliecomic-22) 冬乃郁也/崎谷はるひ  
[「翳りに木漏れ日、それから緑」](http://www.amazon.co.jp/exec/obidos/ASIN/4861348137/nataliecomic-22) みよしあやと

