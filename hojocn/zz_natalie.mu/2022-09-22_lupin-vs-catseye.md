https://natalie.mu/comic/news/494680

# 「ルパン三世」×「キャッツ・アイ」コラボアニメ決定！制作はトムスが担当
「Lupin三世」×「Cat's Eye」合作动画决定!Toms负责制作  

2022年9月22日  [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[モンキー・パンチ](https://natalie.mu/comic/artist/1749)原作によるアニメ「ルパン三世」と[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「キャッツ・アイ」のコラボアニメ映画「ルパン三世VSキャッツ・アイ」が、2023年にPrime Videoで世界独占配信。ティザービジュアル、PV第1弾が公開された。  
由Monkey Punch原作的动画《鲁邦三世》和北条司原作的动画《猫眼》合作的动画电影《鲁邦三世VS猫眼》将于2023年通过Prime Video全球独家放送。海报、PV第1弹公开。

[
![「ルパン三世 VS キャッツ・アイ」キービジュアル](https://ogre.natalie.mu/media/news/comic/2022/0922/teaser_visual.jpg?imwidth=468&imdensity=1)
「ルパン三世 VS キャッツ・アイ」キービジュアル  
大きなサイズで見る（全13件）  
《鲁邦三世VS猫眼》关键图像  
大尺寸观看(共13件)  
](https://natalie.mu/comic/gallery/news/494680/1905449)

これは「ルパン三世」アニメ化50周年と「キャッツ・アイ」の原作40周年を記念して展開されるもの。「キャッツ・アイ」連載当時の1980年代を舞台に、泥棒と怪盗のレトロでスタイリッシュなクライム・アクションが繰り広げられる。なお「ルパン三世VSキャッツ・アイ」は“Amazon Original”で日本のアニメーション映画として初めての作品となる。  
这是为了纪念《鲁邦三世》动画化50周年和《猫眼》原作40周年而展开的。影片以《猫眼》连载当时的1980年代为舞台，讲述了小偷和怪盗之间复古而时尚的犯罪动作故事。另外，《鲁邦三世VS猫眼》是第一部在Amazon Original上上映的日本动画电影。  

監督は[静野孔文](https://natalie.mu/comic/artist/62364)、[瀬下寛之](https://natalie.mu/comic/artist/92064)が担当。アニメーション制作はトムス・エンタテインメントが務める。さらに北条、モンキー・パンチの次男でありエム・ピー・ワークス代表の加藤州平氏、ルパン三世役の[栗田貫一](https://natalie.mu/comic/artist/6789)、来生瞳役の[戸田恵子](https://natalie.mu/comic/artist/16682)、プロデューサーの石山桂一からはコメントも到着した。
导演由静野孔文、濑下宽之担任。动画制作由Toms Entertainment担任。北条、Monkey Punch的次子、MPWorks的代表加藤州平、饰演鲁邦三世的栗田贯一、饰演来生瞳的户田惠子、制片人石山桂一也发表了意见。  

<a name="cd">comment</a>  
## 「キャッツ・アイ」原作者・北条司コメント
猫眼原作者北条司评语  

『ルパン三世』は、中学生の時から読んでいた大先輩の作品です。モンキー・パンチ先生がこのコラボの話を聞かれたらどんな反応をされていたかな・・・？と考えずにはいられません。生前、何度かバーでご一緒する機会がありましたが、お互いマンガのお話をすることはありませんでした。ある時、人づてに頼みたいことがあると言われ、ただそれをおうかがいする前に亡くなられてしまいました。それがさすがにこのコラボの話ではなかったとは思いますが、今回、不思議なご縁でご一緒することができて嬉しいです。モンキー・パンチ先生にも喜んでいただけていたら良いなと心より願っています。  
《鲁邦三世》是我从初中就开始读的老前辈的作品。如果Monkey Punch老师听到这个合作的消息会有怎样的反应呢?不得不这样想。生前，我们有过几次机会一起去酒吧，但都没有聊过漫画。有一次，他对我说有件事想问我，但在我回答之前他就去世了。我想那到底是不是这次合作的事，不过，这次，不可思议的缘分能在一起很高兴。衷心希望Monkey Punch老师也能高兴。  

## エム・ピー・ワークス代表・加藤州平氏（モンキー・パンチ次男）コメント
MPWorks代表加藤州平(Monkey Punch次子)评语    

モンキー・パンチは、『キャッツ・アイ』の世界観が大好きでした。『キャッツ・アイ』と『ルパン三世』が対決したらきっと面白い作品になるだろうと、楽しそうに話をしていたことがあります。親しく交流させていただいた北条先生の『キャッツ・アイ』と、『ルパン三世』のコラボレーションは、熱望していたモンキー・パンチにとって、夢のような企画だと思います。  
Monkey Punch非常喜欢《猫眼》的世界观。我曾经很开心地和他聊过，如果《猫眼》和《鲁邦三世》对决的话，一定会成为很有趣的作品。与我亲密交流过的北条老师的《猫眼》和《鲁邦三世》的合作，对于渴望已久的Monkey Punch来说，是一个梦幻般的企划。  

原作者でも、アニメの『ルパン三世』は、ファンの一人として楽しむ人でした。きっと、今回の企画も、ファンの一人として、作品完成を待ちわびていると思います。  
原著作者也是动画片《鲁邦三世》的粉丝之一。我想这次的企划，作为粉丝之一，一定也在焦急地等待着作品的完成吧。  

## 栗田貫一（ルパン三世役）コメント
栗田贯一(鲁邦三世演员)Comment

『ルパン三世』はアニメ化50周年、『キャッツ・アイ』は原作40周年。記念すべき年に、まさかの対決？ができるとは思っていませんでした。ルパン、次元、五ェ門と銭形のとっつぁん。そして、不二子ちゃんと3姉妹！今からとても楽しみです！  
《鲁邦三世》是动画化50周年，《猫眼》是原作40周年。值得纪念的一年，意外的对决?我没想到能做到。鲁邦、次元、五右卫门和钱形特查。然后，不二子和3姐妹!从现在开始非常期待!

## 戸田恵子（来生瞳役）コメント

お話聞いて、えーー？マジーー？って感じでひっくり返りました！（笑）何十年ぶりとかは最早考えたくない！（笑）お祭り気分で楽しもうと思います。  
听我说，啊？神奇啊？(笑)我不想去想已经过去了几十年! (笑)我要在节日的气氛中享受它。（译注：待校对）    

## プロデューサー・石山桂一氏コメント
制作人石山桂一评述  

『ルパン三世 VS 名探偵コナン』に継ぐ夢のコラボ作品『ルパン三世 VS キャッツ・アイ』。泥棒 VS 怪盗ということもあり華麗でスタイリッシュなアクション要素も満載なテンポのいい作品に仕上がりました。北条先生からモンキー先生へのリスペクトもあり「ルパンの世界観にキャッツたちが存在している様に見せたい、ルパンのキャラクターに寄せてほしい」というオーダーがあったため、漫画家の[中田春彌](https://natalie.mu/comic/artist/11043)先生にイラストを描いていただき、両方のキャラクターを一新いたしました。また、ルパンのジャケットがピンクなのは、『キャッツ・アイ』の時代設定が80年代であり、本作もその時代設定をフィーチャーしているため「80年代のルパンと言えばピンク」という思いがあり決定しました。  
继《鲁邦三世VS名侦探柯南》之后，梦幻合作作品《鲁邦三世VS猫眼》。小偷VS怪盗的华丽的动作要素满载的节奏的好作品完成了。北条老师出于对Monkey老师的尊重，要求“让人看到在鲁邦的世界观中有猫们存在的样子，并希望她们更像鲁邦的人物”。所以请中田春彌老师画插图,请双方的角色都被重新设计。  

この記事の画像・動画（全13件）

[![「ルパン三世 VS キャッツ・アイ」key visual](https://ogre.natalie.mu/media/news/comic/2022/0922/teaser_visual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905449 "「ルパン三世 VS キャッツ・アイ」キービジュアル")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/01c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905460 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/02a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905459 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/03a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905458 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/04c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905457 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/05b.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905456 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/06c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905455 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/07c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905454 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/08c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905453 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/09c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905452 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/10a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905451 "「ルパン三世 VS キャッツ・アイ」場面Cut")
[![「ルパン三世 VS キャッツ・アイ」Logo](https://ogre.natalie.mu/media/news/comic/2022/0922/lvc_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/494680/1905450 "「ルパン三世 VS キャッツ・アイ」ロゴ")
[![「ルパン三世 VS キャッツ・アイ」第1弾PV](https://i.ytimg.com/vi/6s5QLmAc7gM/default.jpg)](https://natalie.mu/comic/gallery/news/494680/media/81880 "「ルパン三世 VS キャッツ・アイ」第1弾PV")

## Amazon Original「ルパン三世 VS キャッツ・アイ」

2023年よりPrime Videoにて世界独占配信

### スタッフ

原作：[モンキー・パンチ](https://natalie.mu/comic/artist/1749)『ルパン三世』/[北条司](https://natalie.mu/comic/artist/2405)『キャッツ・アイ』  
監督：[静野孔文](https://natalie.mu/comic/artist/62364)、[瀬下寛之](https://natalie.mu/comic/artist/92064)  
脚本：葛原秀治  
副監督：井手惠介  
キャラクターデザイン：[中田春彌](https://natalie.mu/comic/artist/11043)、山中純子  
プロダクションデザイン：田中直哉、フェルディナンド・パトゥリ  
アートディレクター：片塰満則  
編集：肥田文  
音響監督：清水洋史  
音楽：[大野雄二](https://natalie.mu/comic/artist/105386)、大谷和夫  
アニメーション制作：トムス・エンタテインメント  
製作：ルパン三世 VS キャッツ・アイ製作委員会

### キャスト

ルパン三世：[栗田貫一](https://natalie.mu/comic/artist/6789)  
来生瞳：[戸田恵子](https://natalie.mu/comic/artist/16682)

全文を表示

(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会

[Comment](https://natalie.mu/comic/news/494680/comment)