https://natalie.mu/eiga/news/354990

# 松下奈緒×ディーン・フジオカ「Angel Sign」冒頭4分をGYAO!で公開

2019年11月11日  [Comment](https://natalie.mu/eiga/news/354990/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)

マンガ家・[北条司](https://natalie.mu/eiga/artist/2405)が総監督を務める「[Angel Sign](https://natalie.mu/eiga/film/182396)」より、冒頭約4分の本編映像がGYAO!で公開された。  
由漫画家北条司担任总监督的《天使印记》，开头约4分钟的正篇影像是GYAO!中公开了。  


[
![「Angel Sign」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?impolicy=hq&imwidth=730&imdensity=1)
「Angel Sign」  
大きなサイズで見る（全14件）  
“天使印记”查看大图(共14件)  
](https://natalie.mu/eiga/gallery/news/354990/1189290)

[
![「Angel Sign」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?imwidth=468&imdensity=1)
「Angel Sign」ポスタービジュアル［拡大］  
“天使印记”海报[放大]
](https://natalie.mu/eiga/gallery/news/354990/1222121)

[松下奈緒](https://natalie.mu/eiga/artist/11381)と[ディーン・フジオカ](https://natalie.mu/eiga/artist/65834)が共演した本作。世界中から寄せられたサイレントマンガオーディションの受賞作をアジア各国の監督が映像化したオムニバスとなっており、6つの物語が映像と音楽のみで展開する。「プロローグ」「エピローグ」で北条がメガホンを取り、松下がアイカ役、フジオカがタカヤ役で参加した。  
松下奈绪和藤冈靛共演的本作。亚洲各国的导演将从世界各地寄来的沉默漫画甄选的获奖作品影像化而成的集锦，6个故事仅以影像和音乐展开。《序幕》和《尾声》由北条掌镜，松下和藤冈分别饰演伊卡和高也。  

[
![「Angel Sign」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?imwidth=468&imdensity=1)
「Angel Sign」［拡大］
](https://natalie.mu/eiga/gallery/news/354990/1189295)

映像は、コンサート会場でチェロを演奏するアイカと、楽譜を抱えながら走るタカヤの姿からスタート。その後、タカヤが急な心臓発作で階段から転げ落ちる場面や、アイカのチェロの弦が演奏中に切れてしまう様子が切り取られていく。同映像はGYAO!の特集ページで確認できる。  
视频从音乐会现场演奏大提琴的伊卡和抱着乐谱奔跑的高也的身影开始。之后，高也突然心脏病发作从楼梯上摔下来的场面，以及伊卡的大提琴琴弦在演奏中断裂的场面被剪了下来。该影像由GYAO!的特集页面可以确认。

「Angel Sign」は11月15日より東京のユナイテッド・シネマ豊洲ほか全国で順次公開。  
《天使印记》将于11月15日起在东京的丰洲联合电影院等全国各地依次上映。

この記事の画像・動画（全14件）  
这篇报道的图片、视频(共14篇)  

[![「Angel Sign」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1189290 "「Angel Sign」")
[![「Angel Sign」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222121 "「Angel Sign」ポスタービジュアル")
[![「Angel Sign」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1189295 "「Angel Sign」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222122 "「別れと始まり」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222123 "「別れと始まり」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222124 "「父の贈り物」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222125 "「空へ」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222126 "「空へ」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222127 "「30分30秒」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222128 "「30分30秒」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222129 "「父の贈り物」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222130 "「故郷へ」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/354990/1222131 "「故郷へ」")
[![「Angel Sign」予告編](https://i.ytimg.com/vi/4gFDdfZBS7A/default.jpg)](https://natalie.mu/eiga/gallery/news/354990/media/42069 "「Angel Sign」予告編")

(c)「Angel Sign」製作委員会

LINK

[「Angel Sign」GYAO!特集ページ](https://gyao.onelink.me/AeWv/d69dd690)  
[「Angel Sign」公式サイト](https://angelsign.jp/)  
[AngelSignMovie (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[「Angel Sign」予告編](https://youtu.be/4gFDdfZBS7A)  