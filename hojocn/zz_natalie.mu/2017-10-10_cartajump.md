source:  
https://natalie.mu/comic/pp/cartajump

![](https://ogre.natalie.mu/media/pp/static/comic/cartajump/pc_header.jpg)  


# 「かるたジャン100」  
「纸牌ジャン100」  

2017年10月10日  

1968年に創刊され、来年50周年を迎える週刊少年ジャンプ（集英社）。連載作が一堂に会する「週刊少年ジャンプ展」などのイベントが実施され、バックナンバーの復刻版など記念アイテムがリリースされる中、同誌50年の歴史を彩ってきた連載作から100作超をピックアップし、名セリフを選りすぐってかるたにした「かるたジャン100」が発売された。  
周刊少年Jump（集英社）创刊于1968年，明年将迎来50周年。届时将举办连载作品齐聚一堂的“周刊少年Jump展”等活动，并推出复刻版等纪念道具，从中从该杂志50年历史上浓墨重彩的连载作品中精选出超过100部，并选出经典台词。精选并制作成纸牌的「纸牌ジャン100」上市了。  

コミックナタリーではかるたのポイントを解説するとともに、かるた化された作品をリスト形式で紹介。また「かるたジャン100」の制作担当者にインタビューを実施し、ジャンプの50年を振り返る上で“最小限の読書ガイド”になるという本アイテムの、制作エピソードや、作品、セリフの選定方法などの裏話を語ってもらった。  
漫画Natalie在解说纸牌的要点的同时，以列表的形式介绍纸牌化的作品。此外，我们还采访了「纸牌ジャン100」的制作负责人，在回顾Jump 50年的过程中，讲述了作为“最小限度的阅读指南”的书单的制作花絮、作品、台词的选定方法等幕后故事。  


取材・文 / 宮津友徳


## ![「かるたジャン100」はここがポイント！（“纸牌酱100”的关键就在这里!）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/title1.png)

### ![1.読み札、取り札それぞれ100枚以上の大ボリューム（1.读牌、取牌各100张以上的大Volume）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/sub_title1.png)

一般的に読み札、取り札それぞれ44～48枚程度とされるかるただが、「かるたジャン100」は100＋αのタイトルをかるた化。読み札、取り札を合わせると計208枚の大ボリュームだ。のちのジャンプ作家に大きな影響を与えた本宮ひろ志「男一匹ガキ大将」をはじめとした黎明期を支えた名作から、この10月にテレビアニメの放送がスタートした田畠裕基「ブラッククローバー」など最新作まで、幅広くかるた化されている。  
一般来说，读牌和取牌各有44~48张左右的纸牌，「纸牌ジャン100」是100+α的标题纸牌化。加上读牌和取牌，共计208张。从给后来的Jump作家带来巨大影响的本宮ひろ志的「男一匹ガキ大将」等支撑初期的名作，到今年10月开始播放电视动画的田畠裕基的「Black Clover」等最新作品，被范围广泛地纸牌化了。  

#### 読み札（读牌）  

*   [![「男一匹ガキ大将」の読み札。（「男一匹ガキ大将」的读牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card1s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card1.jpg)  
    表
*   [![読み札の裏面は、すべてジャンプパイレーツのMarkで統一されている。（读牌的背面全部统一为Jump Pirates标志。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card2.jpg)  
    裏

#### 取り札（取牌）  

*   [![「男一匹ガキ大将」の取り札。（「男一匹ガキ大将」的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card3s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card3.jpg)  
    表
*   [![「男一匹ガキ大将」の取り札。（「男一匹ガキ大将」的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card4s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card4.jpg)  
    裏

### ![2.50音全部あるの？「＋α」ってなに？（2.50所有声音都有吗？“+α”是什么？）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/sub_title2.png)

「かるたジャン」では「を」を除いた45音の札をラインナップ。通常のかるたで省かれてしまうことも多い「ん」の音にどのセリフが採用されているのかも注目だ。また「＋α」として「エクストラカード」も同梱。「エクストラカード」には久保帯人「BLEACH」から「卍」、さくまあきら「ジャンプ放送局」から「な」、同誌のトレードMark・ジャンプパイレーツを使用した「○」「ゆ」を頭文字に持つセリフが採用されている。漢字が頭文字になっている「卍」は作品のファンであればピンとくる人も多いだろうが、「な」や「○」「ゆ」は一体なんの頭文字なのか。その答えは実際に「かるたジャン」を手にとって確認してほしい。  
「纸牌ジャン」中，除了“を”以外，用45个音组成的牌。在通常的纸牌中经常被省略的“ん”音采用了哪句台词也备受关注。另外，作为“+ α”附赠“特别卡”。“特别卡片”中采用了由久保带人“BLEACH”的“卍”、由佐久间“Jump放送局”的“な”、使用该杂志的标志Jump Pirates的「○」「ゆ」的首字母组成的台词。。以汉字为首字母的“卍”，想必很多作品的粉丝都能认出来，那么「な「○」「ゆ」到底是什么首字母呢?答案就请实际拿起「纸牌ジャン」来确认吧。  


#### エクストラカード（Extra Card）

*   [![「BLEACH」の取り札。（「BLEACH」的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card5s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card5.jpg)
*   [![「ジャンプ放送局」の取り札。（「ジャンプ放送局」的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card6s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card6.jpg)
*   [![週刊少年ジャンプの取り札。（週刊少年Jump的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card7s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card7.jpg)
*   [![週刊少年ジャンプの取り札。（週刊少年Jump的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card8s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card8.jpg)

### ![3.どこで、誰が言った？すべての名言をばっちり解説した小冊子付き（在哪里，谁说的?附有详细解说所有名言的小册子）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/sub_title3.png)

長らく少年誌の発行部数トップを走り続けるジャンプといえど、50年の歴史の中で生まれた連載作の名セリフを、すべての年代を通して語れるファンは少ないはず。付属の小冊子では読者のためにかるた化された104つのセリフを、どのような場面で誰が発したのか多彩なコマを使って解説。読み札化されたセリフ以外の名言を紹介する「これもいいジャン！」のコーナーも、各タイトルごとに設けられている。  
即使是长期位居少年杂志发行量首位的Jump，应该也很少有粉丝能将50年历史中诞生的连载作品的名台词贯穿所有年代。在附赠的小册子中，为读者讲解了被纸牌化的104句台词，在什么样的场景下由谁说出，使用了丰富多彩的画面。介绍被读牌化的台词以外的名言“这个也不错啊!”的栏目，也是根据各个标题设置的。  

*   [![付属の小冊子に掲載されている、「ど根性ガエル」の紹介ページ。（附赠的小册子中刊登的「ど根性ガエル」的介绍页。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card10s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card10.jpg)
*   [![付属の小冊子に掲載されている、「ジョジョの奇妙な冒険」の紹介ページ。（附赠的小册子中刊登的「JoJoの奇妙な冒険」的介绍页。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card11s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card11.jpg)
*   [![付属の小冊子に掲載されている、「世紀末リーダー伝たけし！」の紹介ページ。（附赠的小册子中登载着「世紀末リーダー伝たけし！」的介绍页。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card12s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card12.jpg)
*   [![エクストラカード（Extra Card）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card13s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card13.jpg)

## ![アイデア次第で遊び方が広がる？「かるたジャン」遊び方解説（根据想法的不同，玩法也会扩大？「纸牌ジャン」玩法解说）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/title2.png)

### ![1.まずはこれから！基本プレイ方法（1.首先从现在开始！基本游戏玩法）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/sub_title4.png)

[![「かるたジャン100」のプレイイメージ。（「纸牌ジャン100」的游戏形象。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/photo01s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/photo01.jpg)

読み手1人と取り手2人以上でプレイする、スタンダードなプレイ方法。104枚の取り札を並べ、最終的により多くの札を手にした方が勝者となる。取り札の裏面には単行本の表紙のみが掲載されているので、札を裏返して並べることで難易度がアップ！   
一个读者和两个以上的领手一起玩，标准的游戏方法。将104张牌并排摆放，最终获得更多牌的一方为胜者。因为取牌的背面只刊登单行本的封面，所以把牌翻过来排列难度会提高!（译注：待校对）  

### ![2.競技百人一首風ルールで真剣勝負！（用竞技百人一首风规则真刀真枪决胜负!）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/sub_title5.png)

[![「かるたジャン100」のプレイイメージ。（「かるたジャン100」的游戏形象。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/photo02s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/photo02.jpg)

競技百人一首の要領で104枚の取り札から合計50枚を使用して、読み手1人と取り手2人で遊ぶルール。それぞれ25枚ずつを自陣に並べ、読み上げられた札を自陣、敵陣問わず獲得し札を減らしていく。敵陣から札を取った場合は自陣の札から1枚を選び、敵陣の好きな場所に並べることが可能。自陣の札が先にすべてなくなったほうが勝者となる。  
按照竞技百人一首的要领，从104张牌中使用合计50张，由1名读者和2名牌手游戏的规则。各自在自己的阵营中排列25张牌，朗读出来的牌，不管是自己阵营还是敌人阵营，获得的牌逐渐减少。从敌阵拿牌的情况下可以从自己阵的牌中选择一张，在敌阵喜欢的地方排列。己方的牌先用完的一方为胜者。  

### ![3.トランプや名言当てクイズも（扑克牌和猜名言）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/sub_title6.png)

*   [![「ROOKIES」の読み札。友情Markには四つ葉のクローバーのイラストが採用された。（“ROOKIES”的读牌。友情标志采用了四叶草的插图。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card14s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card14.jpg)  
    友情Mark
*   [![「ピューと吹く！ジャガー」の読み札。努力Markには汗のイラストが採用された。（「ピューと吹く！ジャガー」的读牌。努力标志上采用了汗水插图。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card15s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card15.jpg)  
    努力Mark
*   [![「D.Gray-man」の読み札。勝利Markには王冠のイラストが採用された。（「D.Gray-man」的读牌。胜利标志采用了王冠的插图。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card16s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card16.jpg)  
    勝利Mark

「かるたジャン100」の読み札の一部にはハート、ダイヤ、スペード、クローバーの4種のいずれかと、A～Kまで13種の数字1つが描かれており、トランプとして使用することが可能。またジャンプのキーワードとなっている「友情」「努力」「勝利」を意味するオリジナルMarkの札も13枚ずつ用意されており、うまく組み合わせればいつもより多い枚数のトランプで神経衰弱やババ抜きを楽しめる。このほか取り札だけを見て、対応する読み札に書かれた文を推測する「名言当てクイズ」など、アイデア次第で遊び方が広がる。  
「纸牌ジャン100」的读牌的一部分绘有红心、钻石、黑桃、三叶草4种中的一种和A～K 13种数字中的一个，可以作为扑克牌使用。另外，还准备了13张代表“友情”、“努力”、“胜利”的原创Mark牌，如果组合得好，就能玩到比平时多出的扑克牌。除此之外，只看取牌，推测对应的读牌上写的句子的“猜谜名言”等，根据创意的不同，玩法也不同。  


#### ![名言揃いのジャンプ作品！ セレクトされたのはどのセリフ？（名言云集的Jump作品!被选中的是哪句台词?）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/tit_quiz.png)

*   [![「プレイボール」の取り札。（「プレイボール」的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card17s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card17.jpg)
*   [![「ストップ!! ひばりくん！」の取り札。（「ストップ!! ひばりくん！」的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card18s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card18.jpg)
*   [![「明稜帝梧桐勢十郎」の取り札。（「明稜帝梧桐勢十郎」的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card19s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card19.jpg)
*   [![「バクマン。」の取り札。（「バクマン。」的取牌）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card20s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_card20.jpg)


-----  
Page2  
-----  

## ![セレクトされたタイトルを一挙に紹介（一举介绍精选标题）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/title3.png)

「100＋α」のタイトルが収録されている「かるたジャン100」。ここでは少年ジャンプが創刊された1968年からを約10年ずつに区切り、かるた化された全作品をリストで公開するとともに、一部タイトルの札をピックアップして紹介していく。  
收录了「100＋α」标题的「纸牌ジャン100」。这里从少年Jump创刊的1968年开始，每隔约10年为一段时间，以列表公开被纸牌化的全部作品，同时选出一部分标题的纸牌进行介绍。  

### ![1968年～1979年](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit1_1.png)

### ![1960～70年代のジャンプって？（1960～70年代的Jump是什么?）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit1_2.png)

1968年7月11日に毎月第2、第4木曜日刊行の隔週誌として創刊された少年ジャンプ。翌年10月には週刊化され永井豪、本宮ひろ志を筆頭に人気作家を続々輩出し、1970年には創刊からわずか2年足らずで発行部数100万部を突破する。  
少年Jump创刊于1968年7月11日，是每月第2、4星期四发行的隔周杂志。第二年10月周刊化，以永井豪、本宮ひろ志为首的人气作家不断涌现，1970年创刊不到2年发行量就突破了100万部。  

##### 纸牌化Title  

永井豪「ハレンチ学園」、本宮ひろ志「男一匹ガキ大将」、とりいかずよし「トイレット博士」、吉沢やすみ「ど根性ガエル」、梶原一騎・井上コオ「侍ジャイアンツ」、山川惣治・川崎のぼる「荒野の少年イサム」、遠崎史朗・中島徳博「アストロ球団」、中沢啓治「はだしのゲン」、ちばあきお「プレイボール」、牛次郎・ビッグ錠「包丁人味平」、諸星大二郎「妖怪ハンター」、池沢さとし「サーキットの狼」、武論尊・平松伸二「ドーベルマン刑事」、小林よしのり「東大一直線」、秋本治「こちら葛飾区亀有公園前派出所」、車田正美「リングにかけろ」、江口寿史「すすめ!!パイレーツ」、寺沢武一「コブラ」、ゆでたまご「キン肉マン」  

*   [![「侍ジャイアンツ」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb01s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb01.jpg)
*   [![「侍ジャイアンツ」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb01_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb01_2.jpg)
*   [![「リングにかけろ」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb02s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb02.jpg)
*   [![「リングにかけろ」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb02_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb02_2.jpg)
*   [![「コブラ」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb03s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb03.jpg)
*   [![「コブラ」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb03_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb03_2.jpg)
*   [![「キン肉マン」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb04s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb04.jpg)
*   [![「キン肉マン」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb04_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb04_2.jpg)

###### ![Pick Up](https://ogre.natalie.mu/media/pp/static/comic/cartajump/pickup.png)

「こちら葛飾区亀有公園前派出所」  
「这里是葛饰区龟有公园前派出所」  

「入試 就職 結婚 みんなギャンブルみたいなもんだろ！ 人生すべて博打だぞ」（発言者：両津勘吉）  
「入学考试、就业、结婚都像赌博一样吧!人生都是赌博。」(发言者:两津勘吉)  

*   [![「こちら葛飾区亀有公園前派出所」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_kochikames.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_kochikame.jpg)
*   [![「こちら葛飾区亀有公園前派出所」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_kochikame2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_kochikame2.jpg)

2016年、40年の連載に幕を下ろした「こち亀」。交番の近くにオープンした場外馬券売り場に行きたいものの、周囲の目を気にしていた両さんは、近所の子供に馬券を買わせることを思いつく。その様子を麗子に見とがめられた際に発したのがこちらの一言だ。両さんの人生哲学が詰まったセリフだが、麗子や中川にはあっさりその人生観を否定されてしまう。

### ![1980年～1989年](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit2_1.png)

### ![1980年代のJump？](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit2_2.png)

1980年代に突入する頃には発行部数が300万部近くに。その後「Dr.スランプ」「キン肉マン」「キャプテン翼」など連載作が次々とアニメ化。「キン肉マン」の「キン消し」をはじめとした大ヒットグッズも生まれ、ジャンプの名はさらに多くの人に知られていく。なお1984年にはジャンプ最大発行部数653万部への原動力のひとつとなった「DRAGON BALL」がスタートした。   
进入20世纪80年代，发行量接近300万册。之后「Dr.Slump」、「筋肉人」、「足球小将」等连载作品相继动画化。以“筋肉人”的「キン消し」为首的大热商品也诞生了，Jump的名字被更多人知道了。1984年，「DRAGON BALL」成为Jump最大发行量653万册的原动力之一。  

##### かるた化タイトル（纸牌化Title）  

鳥山明「Dr.スランプ」、高橋陽一「キャプテン翼」、北条司「キャッツ♥アイ」、江口寿史「ストップ!! ひばりくん！」、新沢基栄「ハイスクール！奇面組」、次原隆二「よろしくメカドック」、桂正和「ウイングマン」、武論尊・原哲夫「北斗の拳」、高橋よしひろ「銀牙ー流れ星 銀ー」、まつもと泉「きまぐれオレンジ☆ロード」、鳥山明「DRAGON BALL」、北条司「シティハンター」、えんどコイチ「ついでにとんちんかん」、宮下あきら「魁!!男塾」、車田正美「聖闘士星矢」、こせきこうじ「県立海空高校野球部員山下たろーくん」、荒木飛呂彦「ジョジョの奇妙な冒険」、佐藤正「燃える！お兄さん」、にわのまこと「THE MOMOTAROH」、萩原一至「BASTARD!!―暗黒の破壊神―」、徳弘正也「ジャングルの王者ターちゃん♡」、森田まさのり「ろくでなしBLUES」、江川達也「まじかる☆タルるートくん」、三条陸・稲田浩司・堀井雄二「DRAGON QUEST―ダイの大冒険―」、桂正和「電影少女」  
鸟山明的「Dr.Slump」、高桥阳一的「足球小将」、北条司的「Cat's♥Eye」、江口寿史的「停止!!ひばりくん!」、新泽基荣「高中!奇面组」、次原隆二「请多指教修车郎中」、桂正和「WingMan」、武论尊·原哲夫「北斗神拳」、高桥吉弘「銀牙ー流れ星 銀ー」、松本泉「橙路」、鸟山明「DRAGON BALL」、北条司「CityHunter」、えんどコイチ「ついでにとんちんかん」、宫下あきら<魁!!男塾」、车田正美「圣斗士星矢」、こせきこうじ「县立海空高中棒球部员山下太郎」、荒木飞吕彦「JoJo的奇妙冒险」、佐藤正「燃える！お兄さん」、にわのまこと“THE MOMOTAROH”、萩原一至「BASTARD!!—暗黑破坏神—」、德弘正也「ジャングルの王者ターちゃん♡」、森田まさ「ろくでなしBLUES」、江川达也「魔法少女」、三条陆·稻田浩司·堀井雄二「DRAGON QUEST -大的大冒险-」、桂正和「电影少女」  

*   [![「キャプテン翼」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb05s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb05.jpg)
*   [![「キャプテン翼」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb05_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb05_2.jpg)
*   [![「ジョジョの奇妙な冒険」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb07s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb07.jpg)
*   [![「ジョジョの奇妙な冒険」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb07_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb07_2.jpg)
*   [![「BASTARD!!―暗黒の破壊神―」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb06s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb06.jpg)
*   [![「BASTARD!!―暗黒の破壊神―」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb06_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb06_2.jpg)
*   [![「DRAGON QUEST―ダイの大冒険―」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb24s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb24.jpg)
*   [![「DRAGON QUEST―ダイの大冒険―」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb24_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb24_2.jpg)

###### ![Pick Up](https://ogre.natalie.mu/media/pp/static/comic/cartajump/pickup.png)

「DRAGON BALL」

「クリリンのことかーっ!!!!!」（発言者：孫悟空）  
「你是说克林!!!!!」（発言者：孫悟空）（译注：待校对）    

*   [![「DRAGON BALL」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_dbs.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_db.jpg)
*   [![「DRAGON BALL」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_db2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_db2.jpg)

新作アニメ「ドラゴンボール超」やゲーム、グッズなどで今もなお人気を博す「DRAGON BALL」。幼い頃からの親友・クリリンの死をフリーザに侮辱された悟空は、怒りとともに本セリフを放った。なおこの一言が登場したエピソードが掲載された週刊少年ジャンプ1991年21・22合併号では、無作為抽出で選ばれた1000通をもとに集計される同誌の読者アンケートで「DRAGON BALL」が815票を獲得し、同誌のアンケート史上最大得票を記録している。  
以新作动画「DragonBall超」、游戏、周边等至今仍博得人气的“DRAGON BALL”。悟空因为小时候好友克林的死而被弗里萨侮辱，他愤怒地说出了这句话。另外，在「周刊少年Jump」1991年21、22合并号上，以随机抽取的1000封为基础统计的该杂志读者问卷调查中，“DRAGONBALL’获得815票，创下该杂志调查问卷史上的最大得票记录。  

### ![1990年～1999年](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit3_1.png)

### ![1990年代のJump？](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit3_2.png)

「DRAGON BALL」「SLAM DUNK」「幽☆遊☆白書」を中心に人気を集め、1995年3・4合併号では最大発行部数となる653万部を記録。1997年には「ONE PIECE」、1999年には「NARUTO-ナルト-」とその後のジャンプを牽引するタイトルがスタートしている。なお現在も続く年末恒例のイベント・ジャンプフェスタが初めて開催されたのは1999年。  
以「DRAGON BALL」「灌篮高手」「幽游白书」为中心聚集了人气，在1995年3·4合并号上创下了653万部的最大发行量记录。1997年的「ONE PIECE」，1999年的「火影忍者」，都是引领其后Jump的标题。至今仍在持续的年末惯例活动Jump Festa首次举办于1999年。  

##### かるた化タイトル（纸牌化Title）  

井上雄彦「SLAM DUNK」、漫☆画太郎「珍遊記ー太郎とゆかいな仲間たちー」、冨樫義博「幽☆遊☆白書」、光原伸「アウターゾーン」、あんど慶周「究極!!変態仮面」、梅澤春人「BØY―ボーイ―」、ガモウひろし「とっても！ラッキーマン」、桐山光侍「NINKŪ-忍空-」、真倉翔・岡野剛「地獄先生ぬ～べ～」、和月伸宏「るろうに剣心-明治剣客浪漫譚-」、つの丸「みどりのマキバオー」、うすた京介「セクシーコマンドー外伝 すごいよ!!マサルさん」、藤崎竜「封神演義」、高橋和希「遊☆戯☆王」、小栗かずまた「花さか天使テンテンくん」、島袋光年「世紀末リーダー伝たけし！」、尾田栄一郎「ONE PIECE」、かずはじめ「明稜帝梧桐勢十郎」、森田まさのり「ROOKIES」、樋口大輔「ホイッスル！」、冨樫義博「HUNTER×HUNTER」、鈴木央「ライジング インパクト」、ほったゆみ・小畑健「ヒカルの碁」、許斐剛「テニスの王子様」、岸本斉史「NARUTO-ナルト-」  
井上雄彦的「灌篮高手」、漫☆画太郎的「珍游记太郎和愉快的伙伴们」、富坚义博的「幽☆游☆白书」、光原伸的「Outer Zone」、あんど庆周的「究极!!变态面具」,梅泽春人「BØY」,ガモウひろし「真的!lucky man」,桐山光侍「NINKŪ-忍空-」,真正仓翔·冈野刚「地獄先生ぬ～べ～」,和月伸宏「浪客剑心-明治剑客浪漫谭-」,つの丸「みどりのマキバオー」,うすた京介“Sexy Commando外传好厉害啊!!Masaru先生」、藤崎竜的「封神演义」、高桥和希的「游☆戏☆王」、小栗かずまた的「花さか天使テンテンくん」、岛袋光年的「世紀末リーダー伝たけし!」、尾田荣一郎的「ONE PIECE」、かずはじめ的「明棱帝梧桐势十郎」、森田まさのり「ROOKIES」、樋口大辅的「Whistle!」、富坚义博「HUNTER×HUNTER」、铃木央「Rising Impact」、ほったゆみ・小畑健「棋魂」、许斐刚「网球王子」、岸本齐史「火影忍者」

*   [![「SLAM DUNK」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb08s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb08.jpg)
*   [![「SLAM DUNK」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb08_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb08_2.jpg)
*   [![「幽☆遊☆白書」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb09s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb09.jpg)
*   [![「幽☆遊☆白書」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb09_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb09_2.jpg)
*   [![「るろうに剣心-明治剣客浪漫譚-」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb10s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb10.jpg)
*   [![「るろうに剣心-明治剣客浪漫譚-」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb10_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb10_2.jpg)
*   [![「セクシーコマンドー外伝 すごいよ!!マサルさん」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb11s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb11.jpg)
*   [![「セクシーコマンドー外伝 すごいよ!!マサルさん」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb11_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb11_2.jpg)
*   [![「封神演義」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb12s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb12.jpg)
*   [![「封神演義」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb12_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb12_2.jpg)
*   [![「遊☆戯☆王」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb13s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb13.jpg)
*   [![「遊☆戯☆王」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb13_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb13_2.jpg)
*   [![「テニスの王子様」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb14s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb14.jpg)
*   [![「テニスの王子様」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb14_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb14_2.jpg)
*   [![「NARUTO-ナルト-」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb15s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb15.jpg)
*   [![「NARUTO-ナルト-」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb15_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb15_2.jpg)

###### ![Pick Up](https://ogre.natalie.mu/media/pp/static/comic/cartajump/pickup.png)

「ONE PIECE」

「“海賊王”に!!! おれはなるっ!!!!」（発言者：モンキー・D・ルフィ）  
「我!!!!要成为“海贼王“!!!」(发言者:蒙奇·D·路飞)  

*   [![「ONE PIECE」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_ops.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_op.jpg)
*   [![「ONE PIECE」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_op2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_op2.jpg)

3億冊以上を発行し、2015年に「最も多く発行された単一作者によるコミックシリーズ」としてギネス世界記録に認定されるなど、週刊少年ジャンプを代表する作品である「ONE PIECE」。2017年に連載20周年を迎えハリウッドドラマ化も発表された本作からは、ルフィが自身の夢について声高らかに宣言したシーンがかるた化されている。  
「ONE PIECE」是周刊少年Jump的代表作品，发行超过3亿册，2015年被吉尼斯世界纪录认定为“发行最多的单一作者漫画系列”。2017年迎来连载20周年，并宣布好莱坞电视剧化的本作品中，路飞对自己的梦想高声宣言的场景被做成了纸牌。  

### ![2000年～2009年](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit4_1.png)

### ![2000年代のJump？](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit4_2.png)

2002年は同誌の北米版のSHONEN JUMPの刊行がスタート。2005年には「ONE PIECE」と「こちら葛飾区亀有公園前派出所」が累計発行部数1億冊を突破している。なお「ONE PIECE」はその後2010年に2億冊、2013年に3億冊に到達した。このほか2009年には少年ジャンプ作品を扱うテレビ番組「サキよみ ジャンBANG!」もスタートしている。  
2002年该杂志北美版的SHONEN JUMP开始发行。2005年「ONE PIECE」和「乌龙派出所」的累计发行量突破了1亿册。「ONE PIECE」之后在2010年和2013年分别达到2亿册和3亿册。除此之外，2009年还播出了有关少年Jump作品的电视节目「Saki Yomi Jean BANG!」也开始了。  

##### かるた化タイトル（纸牌化Title）

矢吹健太朗「BLACK CAT」、うすた京介「ピューと吹く！ジャガー」、澤井啓夫「ボボボーボ・ボーボボ」、鈴木信也「Mr.FULLSWING」、河下水希「いちご100％」、稲垣理一郎・村田雄介「アイシールド21」、大場つぐみ・小畑健「DEATH NOTE」、空知英秋「銀魂」、天野明「家庭教師ヒットマンREBORN!」、星野桂「D.Gray-man」、松井優征「魔人探偵脳噛ネウロ」、長谷見沙貴・矢吹健太朗「To Loveる-とらぶる-」、篠原健太「SKET DANCE」、椎橋寛「ぬらりひょんの孫」、島袋光年「トリコ」、大場つぐみ・小畑健「バクマン。」、藤巻忠俊「黒子のバスケ」、西尾維新・暁月あきら「めだかボックス」  
矢吹健太郎「BLACK CAT」、うすた京介「ピューと吹く!Jaguar」、泽井启夫「ボボボーボ・ボーボボ」、铃木信也「Mr.FULLSWING」、河下游希「草莓100%」、稻垣理一郎·村田雄介「iShield 21」、大场つぐみ·小畑健「DEATH NOTE」、空知英秋「银魂」、天野明「家庭教师Hitman REBORN!」、星野桂「D.Gray-man」、松井优征「魔人探偵脳噛ネウロ」、长谷见沙贵·矢吹健太郎「To Love -とらぶる-」、筱原健太「SKET DANCE」、椎桥宽「ぬらりひょんの孫」、岛袋光年「トリコ」、大场つぐみ·小畑健「Backman。」、藤卷忠俊「黒子のバスケ」、西尾维新·晓月あきら「Meta Box」

*   [![「アイシールド21」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb16s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb16.jpg)
*   [![「アイシールド21」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb16_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb16_2.jpg)
*   [![「銀魂」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb19s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb19.jpg)
*   [![「銀魂」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb19_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb19_2.jpg)
*   [![「To Loveる-とらぶる-」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb18s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb18.jpg)
*   [![「To Loveる-とらぶる-」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb18_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb18_2.jpg)
*   [![「黒子のバスケ」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb17s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb17.jpg)
*   [![「黒子のバスケ」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb17_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb17_2.jpg)

###### ![Pick Up](https://ogre.natalie.mu/media/pp/static/comic/cartajump/pickup.png)

「DEATH NOTE」

「計画通り」（発言者：夜神月）  
「按计划」(发言者:夜神月)  

*   [![「DEATH NOTE」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_deaths.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_death.jpg)
*   [![「DEATH NOTE」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_death2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_death2.jpg)

「DEATH NOTE」では天才たちの頭脳戦が緻密な描写で描かれ、連載終了後に小説、映画、アニメ、ミュージカル、ドラマ化と様々なメディアミックスが敢行された。2017年にはNetflixオリジナル映画も配信されている。月が「計画通り」とほくそ笑むシーンこのシーンは、その邪悪な表情も相まって読者に大きなインパクトを与えた。  
「DEATH NOTE」对天才们的头脑战进行了细致的描写，连载结束后被改编成小说、电影、动画、音乐剧、电视剧等多种媒体形式。Netflix的原创电影也将于2017年上映。月暗自笑说“按计划”的场景，再加上那邪恶的表情，给了读者很大的冲击。  

### ![2010年～2017年](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit5_1.png)

### ![2010年代のJump？](https://ogre.natalie.mu/media/pp/static/comic/cartajump/subtit5_2.png)

スマートフォンやタブレットの普及による電子書籍利用者の増加にともない、ジャンプも2014年9月より電子版の配信をスタート。またマンガ誌アプリ・少年ジャンプ＋も同年に創刊されている。新たな試みの一方で「NARUTO-ナルト-」「BLEACH」「こちら葛飾区亀有公園前派出所」など長年同誌を支えてきた長期連載が次々と完結した。また2018年の創刊50周年に合わせ、六本木ヒルズにて「創刊50周年記念 週刊少年ジャンプ展」の第1弾が2017年7月にスタートしている。  
随着智能手机和平板电脑的普及，电子书使用者的增加，Jump也从2014年9月开始发布电子版。另外，漫画杂志App少年Jump+也在同年创刊。在新的尝试的同时，「火影忍者」「BLEACH」「乌龙派出所」等支撑该杂志多年的长期连载也相继完结。另外，在2018年创刊50周年之际，于2017年7月在六本木Hills举办了“创刊50周年纪念周刊少年Jump展”第一弹。  

##### かるた化タイトル(纸牌化Title)  

古味直志「ニセコイ」、古舘春一「ハイキュー!!」、麻生周一「斉木楠雄のΨ難」、松井優征「暗殺教室」、附田祐斗・佐伯俊・森崎友紀「食戟のソーマ」、葦原大介「ワールドトリガー」、仲間りょう「磯部磯兵衛物語～浮世はつらいよ～」、川田「火ノ丸相撲」、堀越耕平「僕のヒーローアカデミア」、田畠裕基「ブラッククローバー」、ミウラタダヒロ「ゆらぎ荘の幽奈さん」、吾峠呼世晴「鬼滅の刃」、白井カイウ・出水ぽすか「約束のネバーランド」  
古味直志「ニセコイ」、古馆春一「排球少年!!」、麻生周一「斉木楠雄のΨ難」,松井优征「暗杀教室」,附田祐斗、佐伯俊、森崎友纪「食戟的Soma」,苇原大介「World Trigger」,仲間りょう「磯部磯兵衛物語～浮世はつらいよ～」,川田「火ノ丸相撲」,堀越耕平「我的Hero Academia」、田畠裕基「Black Clover」、Miura Takahiro「ゆらぎ荘の幽奈さん」、吾峠呼世晴「鬼灭之刃」、白井カイウ·出水波希卡「约定的Neverland」  

*   [![「暗殺教室」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb20s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb20.jpg)
*   [![「暗殺教室」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb20_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb20_2.jpg)
*   [![「僕のヒーローアカデミア」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb21s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb21.jpg)
*   [![「僕のヒーローアカデミア」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb21_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb21_2.jpg)
*   [![「ブラッククローバー」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb22s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb22.jpg)
*   [![「ブラッククローバー」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb22_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb22_2.jpg)
*   [![「約束のネバーランド」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb23s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb23.jpg)
*   [![「約束のネバーランド」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb23_2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_thumb23_2.jpg)

###### ![Pick Up](https://ogre.natalie.mu/media/pp/static/comic/cartajump/pickup.png)

「ハイキュー!!」  
「排球少年!!」  

「おれはどこにだってとぶ!! どんな球（ボール）だって打つ!! だからオレにトス、持って来い!!!」（発言者：日向翔陽）  
「我哪里都能飞!!什么球都能打!!所以把球传给我，拿过来!!!」(发言者:日向翔阳)

*   [![「ハイキュー!!」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_hqs.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_hq.jpg)
*   [![「ハイキュー!!」の読み札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_hq2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img_hq2.jpg)

2012年より連載中の「ハイキュー!!」はテレビアニメ化、劇場映画化、ゲーム化、舞台化などさまざまなメディアミックス展開が行われており、週刊少年ジャンプの人気を目下牽引中。連載初期に日向が影山に対して投げかけたこのセリフから、凸凹なコンビの成長が始まる。  
2012年开始连载的「排球少年!!」周刊少年Jump的电视动画化、剧场电影化、游戏化、舞台剧化等各种各样的媒体组合展开，目前正引领着周刊少年Jump的人气。从连载初期日向对影山说出的这句台词开始，这对不平凡的二人组开始成长。

---  
Page3  
---  

## ![制作担当者Interivew](https://ogre.natalie.mu/media/pp/static/comic/cartajump/title4.png)

### 50年分の連載作でかるたを作って成立するのはジャンプだけ!!
50年的连载作品中制作纸牌成立的只有Jump !!  

──週刊少年ジャンプは2018年に創刊50周年を迎えます。記念イヤーに向けさまざまなグッズや書籍の企画が進行しているかと思うのですが、その先陣を切る形で「かるたジャン100」が発売されることになったのはどういった理由からなのでしょうか。  
——周刊少年Jump将于2018年迎来创刊50周年。为了迎接纪念年的到来，日本正在进行各种各样的周边和书籍企划，那么「纸牌ジャン100」先行发售的理由是什么呢?  

[![「○」カードの取り札裏面。少年ジャンプ創刊号の表紙がデザインされている。（「○」卡片的背面。这是少年Jump创刊号的封面设计。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img01s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img01.jpg)

50周年に向けて、「読み物以外に読者が遊んで楽しめるものを作れないか」という話が社内で上がり、会議を進める中で「ジャンプ作品の名セリフをかるたにしたらいいんじゃないか」という案が出たんです。50年分の連載作から名言を選り抜いてかるたにしたときに商品として成立するのは、ジャンプという雑誌の特性だろうと。  
在50周年之际，公司内部有人提出“除了读物以外，能不能制作出读者能玩的东西”，在会议的过程中，有人提出了“把Jump作品的名台词制作成纸牌不是很好吗”的方案。从50年的连载作品中精选名言并做成纸牌，就能成为商品，这是Jump杂志的特性吧。

──読者がどの世代だったとしても、マンガ好きであれば1970年代から2010年代までの連載作で、年代ごとに語れるタイトルがひとつはありますよね。  
──不管读者是哪个时代的人，只要喜欢漫画，从1970年代到2010年代的连载作品中，每个年代都有一个可以谈论的作品吧。  

ええ。集英社は過去にも「ベルサイユのばら」をはじめ（参照：[「ベルばら」がカルタに！三十路すぎてもマドモアゼル～](https://natalie.mu/comic/news/40291)）いろいろな作品のかるたを発売しているんですが、「ベルばら」のかるたは1作品だけにフォーカスしていたので、「三十路過ぎてもマドモアゼル」「貴族のやつらをしばり首」とか、クスッと笑えるようなカードも用意できたんですよ。でも「かるたジャン」は101タイトルを収録していて、かるたにするのは1作品につき1枚だったので、誰もが知っているセリフをセレクトしないといけない。「ネタっぽいセリフでカードを作れない」っていうのは悩みどころでしたね。  
嗯。集英社过去也以「凡尔赛玫瑰」为首(参照:[“贝尔玫瑰”在纸牌上！过了三十路也是Mademoiselle~](https://natalie.mu/comic/news/40291))发售了各种各样的作品的纸牌，不过，因为“贝尔芭拉”的纸牌只聚焦了1个作品，“三十多路也还是纸牌”“绑住贵族们的脖子”等等，还准备了能让人扑哧一笑的卡片。但是「纸牌ジャン」收录了101个标题，每个作品只有一张纸牌，所以必须选择谁都知道的台词。“无法用段子般的台词制作卡片”是烦恼的地方。

──たしかに「かるたジャン」はいわゆるネタセリフや迷言というよりは、ストレートな名セリフで構成されていますね。  
——的确，「纸牌ジャン」与其说是段子或迷言，不如说是由直接的名台词构成的。  

[![400枚構想時に作成された収納ボックス。実際に商品化されたバージョンの2倍の厚さとなっている。(400张构思时制作的收纳盒。厚度是实际商品化版本的2倍。)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img02s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img02.jpg)

1作品2枚ずつ「真面目なセリフ」と「ネタセリフ」、もしくは「主人公の名セリフ」と「ライバルの名セリフ」という振り分けで読み札、取り札合わせて400枚作ろうっていう構想もあったんです。「北斗の拳」だったらケンシロウの「おまえは もう死んでいる！」と、ラオウの「わが生涯に一片の悔いなし!!」はどっちもかるたにしたいじゃないですか。でもどうしても値段が今の倍以上になってしまうので、泣く泣く断念しました。  
我有一个构想，就是每一部作品分别制作两张“严肃的台词”和“段子台词”，或者“主人公的名台词”和“对手的名台词”，把读牌和取牌加在一起制作400张。在「北斗神拳」中，健四郎的“你已经死了!”，拉奥的“我的一生一片无悔!!”不是都想打纸牌吗?但无论如何价格都要涨到现在的一倍以上，只好忍痛放弃。  

### 唯一、主人公以外でかるたになったのは……  
唯一成为主人公以外的纸牌的是…  

──そもそもかるたにするタイトルやセリフの選定はどのように行ったのでしょうか。 
──最初是如何选定要用纸牌的标题和台词的呢?

最初は「かるただから50音分、50タイトルから集めよう」というところから始めて、スタッフで集まって「あれを入れよう、これを入れよう」と収録タイトルを決めていきました。  
一开始，我们从“因为是纸牌，所以从50个音、50个标题开始收集”开始，工作人员聚在一起决定了收录标题，“把那个放进去吧，把这个放进去吧”。

[![「ゆ」カードの取り札裏面。週刊少年ジャンプの最大発行部数653万部を記録した、1994年12月20日発売の1995年3・4合併号の表紙がデザインされている。（「ゆ」卡的取牌背面。周刊少年Jump创下最大发行量653万部记录的1994年12月20日发行的1995年3·4合并号的封面就是这张卡的背面。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img03s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img03.jpg)

──でも50年分の歴史を考えると、50タイトルでは全然足りないですよね。  
──不过考虑到50年的历史，50个标题根本不够用。  

そこから100タイトル扱おうということになったんです。それでも何を入れるかっていうのはなかなか決まらなかったですね。「このラインから上の作品を入れる」みたいな客観的な基準を作ろうとしたんですが、50年も歴史があると数字も意味を成さない部分があって。単行本の部数は創刊当時より80年代、90年代以降の作品のほうが伸びてきますし、連載の長さも昔よりは今のほうが長期になる傾向があるから。結局100タイトルでも「俺の大好きなあの作品が入っていないじゃないか」って言いたくなる人がいるとは思うんですけど、そこは「本当に本当にすみません」としか言いようがないです。  
从那以后就决定使用100个标题。尽管如此，放什么进去还是很难决定。“这条线以上的作品都可以入选”，我想制定这样的客观标准，但有50年的历史，数字也有失去意义的部分。单行本的发行量比起创刊当时，80年代、90年代以后的作品会有所增长，连载的长度也有现在比过去更长期的倾向。结果即使是100个标题，我想也会有人想说“里面没有我最喜欢的那部作品”，但我只能说“真的真的对不起”。  

──1人で何本もヒット作を出されている先生もいますし。  
──也有一个人出了好几部热门作品的老师。  

[![「ウイングマン」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img04-1s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img04-1.jpg) [![「電影少女」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img04-2s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img04-2.jpg)

「1作家1タイトルにしよう」という話にもなったんですが、「DRAGON BALL」と「Dr.スランプ」、「リングにかけろ」と「聖闘士星矢」のどちらかだけにするっていうのはやっぱり難しくて。たとえば桂正和先生なんかは「ウイングマン」「電影少女」「D・N・A² ～何処かで失くしたあいつのアイツ～」「I’’s」と何作品もヒット作を輩出しているんですが、今回収録できたのは「ウイングマン」と「電影少女」だけなんです。  
虽然也说过“一个作家一个标题”，但在「DRAGON BALL」和「Dr.Slump」、「リングにかけろ」和「圣斗士星矢」中只选一个还是很难的。比如桂正和老师「WingMan」、「电影少女」、「D·N·A²~不知在哪里丢失的那家伙~」「I’’s」等几部热门作品辈出，不过，这次能收录的只有「WingMan」和「电影少女」。  

──そして100タイトルが決まったら、セリフを選定していく。  
──确定了100个作品后，就要选定台词。  

各作品の名言をさらって、「『あ』はこのセリフだろう」「でもこの作品と被るから『か』にしようか」など、パズルのように入れ替えて選定していましたね。少年マンガって「俺」とか「お前」から始まるセリフが多くて、「お」の文字なんかは普通に選ぶと多くなっちゃうので、被らないようには気をつけていました。あと「かるたジャン」は、普通にかるたを作ると難航するであろう「ん」を2つ用意できたのが売りのひとつだとは思っています。  
收集了各作品的名言，“『あ』是这句台词吧“、”和这部作品有关系，就选『か』”等，像猜谜一样替换后选定。少年漫画中以“我”或“你”开头的台词很多，「お」这个字一般会选择很多，所以我很注意不要盖过。另外，我认为「纸牌ジャン」的卖点之一是准备了两个一般制作歌牌就会变得困难的「ん」。

──基本的にはすべて主人公のセリフを使用しているんですよね。  
──基本上全部使用了主人公的台词吧。

[![「魁!!男塾」の取り札。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img05s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img05.jpg)

そうです。どのタイトルも大ヒット作ですし、主人公は誰もが知っているキャラクターなので、それ以外のキャラを採用すると、キャラの存在感に差がついてしまうんです。ただ「魁!!男塾」だけは、主人公の剣桃太郎ではなく、江田島平八のセリフにしました。ほかの作品がすべて主人公の中、宮下あきら先生に申し訳ないなという気持ちもあるんですが、やっぱり「男塾」と言えば塾長か民明書房なんじゃないかと。ほかには「BLEACH」も名セリフの山で、単行本の巻頭に書かれている先生の詩も読者から非常に人気があるので、主人公の黒崎一護の言葉じゃなくてもいいんじゃないかという話になって難航したのですが、最終的に王道の選択になりました。  
是的。无论哪个标题都是大热作品，主人公都是家喻户晓的角色，如果采用其他角色的话，角色的存在感就会有差距。只有「魁!!男塾」的台词不是主人公剑桃太郎，而是江田岛平八。其他的作品都是主人公，我也觉得很对不起宫下あきら老师，但说到“男塾”，还是觉得应该是校长或民明书房。另外「BLEACH」也是名台词堆积成山，单行本卷首写的老师的诗在读者中也非常受欢迎，所以我觉得不用主人公黑崎一护的话也可以，但最终决定成为了王道的选择。

──「BLEACH」は最終的に一護の「卍解」というセリフが、ジョーカー的な扱いのエクストラカードとして収録されました。  
——「BLEACH」最终一护的“卍解”这句台词，作为王牌般的处理的特别卡片被收录了。  

「卍」だったら、50音じゃないし、もう1作品プラスして収録できるんじゃないかという悪あがきです（笑）。エクストラカードはかるたを並べてみたときに、100枚のカード以外にも会話の種になる要素というか遊びがほしいよねということで、ジャンプに関連した「ゆ」「○」から始まるワードや、10年以上続いた読者投稿ページ「ジャンプ放送局」に登場したえのんのセリフをかるたにしています。作品はそれぞれ独立して世界を持っているわけですが、ジャンプという雑誌の文化というか、これまでの歴史の中で作り上げられてきた共通記憶みたいなものを入れたかったのです。  
如果是“卍”的话，不是50音了，再加上一部作品就可以收录了吧，这是一种挣扎(笑)。特别卡片是在排列纸牌的时候，除了100张卡片以外，也想要一些能成为对话种子的要素或者说是游戏，所以就有了和跳跃相关的以「ゆ」「○」开头的词，还有持续了10年以上的读者投稿页。将在「Jump放送局」中登场的几句的台词做成了纸牌。虽然每个作品都有自己独立的世界，但我想加入Jump杂志的文化，或者说是在迄今为止的历史中形成的共同记忆。 

### ジャンプの歴史を最小サイズで詰め込んだ読書ガイド  
用最小尺寸塞满跳跃历史的阅读指南  

──取り札は表面にカラー原画、裏面に単行本1巻の書影が使われていますね。黎明期の作品の原画もすべてキレイな状態で残っていたんでしょうか。  
──取牌正面是彩色原画，背面是单行本1卷的书影。初期作品的原画也都是完好无损的吗?  

古い作品の原画はやっぱり集英社にはなかったですね。著者の先生からお借りした作品もあるんですけど、先生が「ちょっとどこにあるかわからない」とおっしゃられることもあって。ただここに収録されている作品って、どれもヒットタイトルで、保存しているデータもある程度ありました。イラスト集になっているものもあるので、イラスト集から画像をスキャンして使用したりもしています。あと先ほども話題に上がりましたが、桂先生は単行本の版が変わるときに、絵を描き直すことがあったんですよ。  
旧作品的原画果然是集英社没有的。也有向作者老师借的作品，但老师有时说“不知道在哪里”。只是这里收录的作品，每一部都是热门标题，也有一定程度的保存数据。因为也有插图集，所以也从插图集扫描图像来使用。刚才也提到过，桂老师在单行本的版本改变时，曾经重新画画。  

──初版とそれ以降の版で表紙イラストが違うということですか？  
──初版和之后的版本封面插图不一样吗?

ええ。どんどん絵柄を変えていくんです。単行本を作った担当が註釈を残してくれてはいるんですが、いざ使おうとなったとき、どれが最新のバージョンかわからなかったりもして。  
嗯。不断地改变图案。虽然负责制作单行本的人会留下注释，但一旦要用的时候，可能不知道哪个是最新版本。  

[![「かるたジャン100」の制作スペースに積まれたジャンプ作品の単行本。これでも全体のほんの一部だという。（堆在「纸牌ジャン100」制作空间的Jump作品单行本。据说这也只是全部的一小部分。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img06s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img06.jpg)

──昔の作品は、単行本の書影自体なかなか残っていないのではないかと思うのですが。  
──我觉得以前的作品，单行本的书影本身就很难留存下来。  

実物からスキャンして使用しているものもあります。80年代以前の作品は保存資料として手元にある単行本が傷んでいたり、編集作業用に何セットか必要になることもあったので、「なんで自分の会社の本をこんなに買うんだ」っていうくらい古書店からたくさん取り寄せました（笑）。  
也有从实物扫描使用的东西。80年代以前的作品，作为保存资料，手边的单行本有时会损坏，有时编辑工作需要几套，所以我从旧书店买了很多书，让人觉得“为什么要买这么多自己公司的书”(笑)。

──取り札に1巻の書影が使われている中、「侍ジャイアンツ」だけ2巻の書影になっているのが気になりました。  
──我注意到在纸币上使用了1卷的书影，只有「武士Giants」是2卷的书影。

[![「侍ジャイアンツ」取り札の裏面。](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img07s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img07.jpg)

「侍ジャイアンツ」は1巻の表紙に長嶋茂雄さんの写真と番場蛮のイラストが使われているんですよ。1巻の書影を使うとすると長嶋茂雄さんにも、撮影したカメラマンにも許諾を取らないといけないんですが、誰が撮影したものなのか限られた時間では調べきれず。かるたの画面は小さいので、複数の人物を入れるとわかりづらいこともあり、主人公の番場蛮が大きく描かれている2巻の表紙を使おうということになりました。ほかにも「はだしのゲン」はジャンプコミックスレーベルから単行本が出ていないので、文庫版1巻の書影を使ったりしています。スタッフが生まれる前に連載されていた作品も多く、素材チェックで当時の事情を追体験することになって興味深かったです。70年代前半はすべての連載がコミックスになるわけではなかったこととか、話には聞いていても実感していなかったので。こういうかるた用の素材集めもそうなんですが、作品の情報について事実確認を取るのも大変でしたね。  
「武士Giants」第1卷的封面使用了长嶋茂雄先生的照片和番场蛮的插图。如果要使用第1卷的书影的话，必须要得到长岛茂雄先生和摄影师的许可，但是在有限的时间里无法调查到底是谁拍摄的。纸牌的画面很小，如果加入多个人物的话会很难看懂，所以决定使用主人公番场蛮被画得很大的第2卷的封面。另外「はだしのゲン」因为Jump Comics厂牌没有发行单行本，所以使用了文库版1卷的书影。工作人员出生前连载的作品也很多，通过素材检查重温了当时的情况，很感兴趣。70年代前半期并不是所有的连载都能变成漫画，虽然听说过，但并没有实际的感受。除了收集纸牌用的素材外，确认作品的事实信息也很困难。

──事実確認というと？  
──确认事实是指?  

たとえば「BASTARD!!-暗黒の破壊神-」は連載初期に「破壊神」の部分に「アンスラサクス」というルビが振られていたのですが、もしかしたら読み方は「はかいしん」ではなくそちらが正しいのではないかとか、「ジャングルの王者ターちゃん」か「ジャングルの王者ターちゃん♡」か、「キャッツ♥アイ」のハートは「♥」なのか、それとも「♡」なのかとかですね。連載当時は当たり前のことでも、意外にあとになるとわからない。ジャンプ作品の固有名詞は独特なものが多いですし、何かの事情で1回だけ違うルビがふってある回があったりして、表記がぶれていると、正しい表記の根拠を探さねばなりません。連載が長いと結構ぶれていることがあるんですよ。どうしてもとなれば先生に聞くわけですが、「なんで集英社が知らないんだ？」って当然なるので調べるわけです。「BASTARD!!」くらいまでなら、ギリギリ社内に初代担当がいるんですが、それ以前になるとすでに引退してしまっているので、確認は結構大変でした。  
例如「BASTARD!!-黑暗的破坏神-」在连载初期，“破坏神”的部分用了「アンスラサクス」这个路飞，说不定读法不是「はかいしん」，而哪个才是正确的呢?是「ジャングルの王者ターちゃん」还是「ジャングルの王者ターちゃん♡」？「Cat's♥Eye」的Heart是「♥」还是「♡」。连载当时理所当然的事，出乎意料地事后就不知道了。Jump作品的固有名词有很多都是独特的，而且因为某些原因，有时会只有一次不同的标记，标记出现偏差的话，就必须寻找正确标记的根据。连载时间一长，就会出现很多偏差。实在不行就问老师:“为什么集英社不知道?”这是理所当然的，所以要调查。象“BASTARD!!”这种程度的话，公司里还勉强有第一任负责人，但在那之前就已经退休了，所以确认起来相当麻烦。  

──付属の冊子には各タイトルの連載データも載っていますし、表記の1つひとつまで確認されているということは、ジャンプの50年をまとめた資料としての意味合いも出てきますよね。  
——附赠的册子里有各标题的连载数据，连每一个的标记都被确认了，也体现出了作为Jump 50年总结资料的意义吧。  

[![付属の小冊子に掲載されている、「DRAGON BALL」の紹介ページ。（附赠的小册子中刊登的“DRAGON BALL”的介绍页。）](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img08s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cartajump/img08.jpg)

だから発売されて「誤表記が見つかった」なんてことになったら大変まずいです。すごくドキドキしています（笑）。もちろん普段の書籍も厳しくチェックはしていますが、今回はそれ以上に念入りに校正をしたつもりです。ジャンプ50年の歴史をすべて語れる読者ってそうはいないと思うので、かるたや冊子を見ながら、これまで読んでこなかった作品にも触れてみてほしいですね。  
所以如果发售后“发现了错误的标记”的话就非常糟糕了。我很紧张(笑)。当然，平时的书籍我也会严格检查，但这次我打算更加仔细地校对。我想没有几个读者能把Jump 50年的历史全部讲出来，所以希望大家一边看纸牌和册子，一边接触之前没读过的作品。  

──読書ガイド的な使い方で。  
──阅读指南式的使用方法。

ええ。ジャンプの50年をまとめた「ジャンプ大辞典」みたいな書籍って、作ろうと思えばもちろん作れるんですけど、おそらくとんでもなく分厚くて値段も高くなってしまうでしょうし、このサイズではできない。そういう意味では「かるたジャン」には最小サイズでジャンプの歴史を詰め込んだ、ジャンプのこれまでについて知るなら、まずはこのタイトルからという「“最小限”の読書ガイド」にもなっています。資料的な意味以外でも、親、子、孫の3世代で楽しめる内容になっているので、家族や友達と一緒に「この作品好きだったな」「こんな作品あるんだ」と話しながら、遊んでもらえればと思っています。  
嗯。像「Jump大辞典」这样的书，总结了Jump 50年的历史，想做的话当然可以做，但恐怕会很厚，价格也会很高，这个尺寸也做不到。从这个意义上来说，「纸牌ジャン」用最小的篇幅讲述了Jump的历史，如果想要了解Jump至今为止的故事，就先从这个标题开始，可以说是一本“最小限度”的阅读指南。除了资料上的意义以外，因为是父母、孩子、孙子3代人都能享受的内容，所以希望能和家人、朋友一起边说“我很喜欢这个作品”、“原来还有这样的作品”一边玩。  


©2017 Go Nagai / Dynamic Production ©本宮ひろ志 / 集英社 ©とりいかずよし ©吉沢やすみ / オフィス安井 ©梶原一騎・井上コオ ©山川惣治・川崎のぼる ©遠崎史朗・中島徳博 / 太田出版 ©中沢啓治 ©ちばあきお ©牛次郎・リュウプロ / 集英社 ©諸星大二郎 ©池沢さとし ©武論尊・平松伸二 / 集英社 ©小林よしのり ©秋本治・アトリエびーだま / 集英社 ©車田正美 / 集英社 ©江口寿史 ©BUICHI TERASAWA / A-GIRL RIGHTS ©ゆでたまご ©鳥山明 / 集英社 ©高橋陽一 / 集英社 ©北条司 / NSP 1981 版権許諾証 GU-017 ©江口寿史 / 集英社 ©新沢基栄 / 集英社 ©次原隆二 / NSP 1982 版権許諾証 GU-017 ©桂正和 / 集英社 ©武論尊・原哲夫 / NSP 1983 版権許諾証 GU-017 ©高橋よしひろ / 集英社 ©まつもと泉 / 集英社 ©バードスタジオ / 集英社 ©北条司 / NSP 1985 版権許諾証 GU-017 ©えんどコイチ / 集英社 ©宮下あきら / 集英社 ©こせきこうじ ©荒木飛呂彦 / 集英社 ©佐藤正 / 集英社 ©にわのまこと ©萩原一至 / 集英社 ©徳弘正也 / 集英社 ©森田まさのり・スタジオヒットマン / 集英社 ©江川達也 / 集英社 ©三条陸・稲田浩司 / 集英社 ©SQUARE ENIX ©井上雄彦 I.T.Planning,Inc. ©漫☆画太郎 / 集英社 ©冨樫義博 / 集英社 ©光原伸 / 集英社 ©あんど慶周 / 集英社 ©梅澤春人 / 集英社 ©ガモウひろし / 集英社 ©桐山光侍 / 集英社 ©真倉翔・岡野剛 / 集英社 ©和月伸宏 / 集英社 ©つの丸 / 集英社 ©うすた京介 / 集英社 ©藤崎竜 / 集英社 ©高橋和希 スタジオ・ダイス / 集英社 ©小栗かずまた / 集英社 ©島袋光年 / 集英社 ©尾田栄一郎 / 集英社 ©かずはじめ / 集英社 ©樋口大輔 / 集英社 ©POT / 集英社 ©鈴木央 / 集英社 ©ほったゆみ・小畑健 / 集英社 ©許斐 剛 / 集英社 ©岸本斉史 / 集英社 ©矢吹健太朗 / 集英社 ©澤井啓夫 / 集英社 ©鈴木信也 / 集英社 ©河下水希 / 集英社 ©稲垣理一郎・村田雄介 / 集英社 ©大場つぐみ・小畑健 / 集英社 ©空知英秋 / 集英社 ©天野明 / 集英社 ©星野桂 / 集英社 ©松井優征 / 集英社 ©矢吹健太朗・長谷見沙貴 / 集英社 ©篠原健太 / 集英社 ©椎橋寛 / 集英社 ©藤巻忠俊 / 集英社 ©西尾維新・暁月あきら / 集英社 ©古味直志 / 集英社 ©古舘春一 / 集英社 ©麻生周一 / 集英社 ©附田祐斗・佐伯俊 / 集英社 ©葦原大介 / 集英社 ©仲間りょう / 集英社 ©川田 / 集英社 ©堀越耕平 / 集英社 ©田畠裕基 / 集英社 ©ミウラタダヒロ / 集英社 ©吾峠呼世晴 / 集英社 ©白井カイウ・出水ぽすか / 集英社 ©久保帯人 / 集英社 ©佐久間あきら / 集英社


---  

「かるたジャン100」

[![「かるたジャン100」](https://ogre.natalie.mu/media/pp/static/comic/cartajump/pc_item01.png)](https://www.amazon.co.jp/exec/obidos/ASIN/4089083087/nataliecomic-pp-22)

2017年10月10日発売  集英社  3218円

[Amazon.co.jp](https://www.amazon.co.jp/exec/obidos/ASIN/4089083087/nataliecomic-pp-22)

週刊少年ジャンプが2018年に創刊50周年を迎えることを記念して制作された「かるたジャン100」では、同誌50年の歴史からセレクトした、100を超える連載作の名セリフや名シーンを1作品につき1枚ずつかるた化。読み札104枚、取り札104枚の計208枚に加え、各作品1ページの解説を収録した116Pの小冊子も付属している。  
周刊少年Jump为了纪念2018年迎来创刊50周年而制作的「纸牌ジャン100」，从该杂志50年的历史中精选了超过100篇连载作品中的名台词和名场景，每部作品一张一张地纸牌化。除了104张读牌、104张取牌等共计208张外，还附带收录了各作品1页解说的116p小册子。  

---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处