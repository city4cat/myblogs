https://natalie.mu/comic/news/46562

# ひうらさとる、新條まゆ、北条司らがチャリティ壁紙提供

2011年3月22日  [Comment](https://natalie.mu/comic/news/46562/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


ポータルサイトgooの東日本大震災義援金募集ページにて、50名を超えるマンガ家たちがチャリティ壁紙を提供する。サイトで任意の金額を募金すると、マンガ家が描いたオリジナルのイラスト壁紙がダウンロードできる仕組みだ。募金額は1回につき100円から1万円。  
在门户网站goo的东日本大地震捐款募集页面上，超过50名的漫画家们提供了慈善壁纸。只要在网站上募捐任意金额，就可以下载漫画家所画的原创插图壁纸。募捐金额为1次100日元至1万日元。

[
![ひうらさとるによる壁紙。](https://ogre.natalie.mu/media/news/comic/2011/0322/hiurasatoru.jpg?imwidth=468&imdensity=1)
ひうらさとるによる壁紙。  
大きなサイズで見る  
由冰原萨特鲁制作的壁纸。  
用大尺寸查看  
](https://natalie.mu/comic/gallery/news/46562/73681)

3月22日現在、壁紙を提供しているマンガ家は、[ひうらさとる](https://natalie.mu/comic/artist/1732)、[北条司](https://natalie.mu/comic/artist/2405)、中村地里、[石岡ショウエイ](https://natalie.mu/comic/artist/4167)、[長江朋美](https://natalie.mu/comic/artist/4424)、進藤ウニ、雨月衣、すずはら篠、ムラカミアヤコ、ほりたみわ、葛西映子、阿部ゆたか、三上ヒロミ、[愛本みずほ](https://natalie.mu/comic/artist/3032)、[新條まゆ](https://natalie.mu/comic/artist/2109)、山田雨月、[池沢理美](https://natalie.mu/comic/artist/4359)、[尾崎南](https://natalie.mu/comic/artist/4719)、[なかはら★ももた](https://natalie.mu/comic/artist/5185)、[うめ](https://natalie.mu/comic/artist/1647)、[野間美由紀](https://natalie.mu/comic/artist/2428)の21人。すでに以下の作家も参加を表明しており、イラストが届き次第追加されていく。  
3月22日提供壁纸的漫画家有:日浦智、北条司、中村地下、石冈翔永、长江朋美、进藤宇尼、雨月衣、铃原篠、村上亚矢子、堀田美和、葛西映子、阿部丰、三上广海、爱本瑞穗、新条茧、山田雨月、池泽理美、尾崎南、中原★桃田、梅、野间美由纪共21人。以下的作家也已经表明了参加，插图一送到就会追加。  

## 今後参加予定の作家  
今后计划参加的作家  

[大久保ヒロミ](https://natalie.mu/comic/artist/5184)／緒形裕美／[稚野鳥子](https://natalie.mu/comic/artist/2241)／[樋口橘](https://natalie.mu/comic/artist/2364)／[星崎真紀](https://natalie.mu/comic/artist/4117)／永澄田えま／本田恵子／冬乃郁也／[折原みと](https://natalie.mu/comic/artist/2181)／[青木光恵](https://natalie.mu/comic/artist/2162)／[小池田マヤ](https://natalie.mu/comic/artist/3879)／[山田デイジー](https://natalie.mu/comic/artist/5737)／[山崎みちよ](https://natalie.mu/comic/artist/8687)／佐野まさき・わたなべ京／[海野つなみ](https://natalie.mu/comic/artist/1842)／[磯谷友紀](https://natalie.mu/comic/artist/1792)／[みづき水脈](https://natalie.mu/comic/artist/7866)／[原明日美](https://natalie.mu/comic/artist/7758)／大橋薫／さべあのま／福田さかえ／[赤石路代](https://natalie.mu/comic/artist/2178)／[いしかわじゅん](https://natalie.mu/comic/artist/2579)／[青木俊直](https://natalie.mu/comic/artist/5627)／[花津ハナヨ](https://natalie.mu/comic/artist/8320)／[とだ勝之](https://natalie.mu/comic/artist/2813)／[高尾滋](https://natalie.mu/comic/artist/1961)／[織田綺](https://natalie.mu/comic/artist/3974)／[あゆみゆい](https://natalie.mu/comic/artist/2557)／kanoto／[中垣慶](https://natalie.mu/comic/artist/4381)／[楠桂](https://natalie.mu/comic/artist/4633)ほか

## 発起人ひうらさとるよりコメント
发起人ひうらさとる发表评论

今回の震災で私自身は被災しなかったものの、被災地からの報道とネットの情報に混乱し、動揺し続ける毎日を送ってきました。  
在这次的地震中，虽然我自己没有受灾，但是来自灾区的报道和网络上的信息让我陷入混乱，每天都过着动荡不安的生活。

「曲がりなりにも発信者である私たちががんばらないと」とカラ元気を出してみたり、その反動なのか「だからって今、漫画なんか描いている場合なのか」と無力さに嗚咽したり、の繰り返しでした。  
“不管怎么说，作为发信人的我们必须努力”，我打起了精神，也许是反作用力，“所以现在是画漫画的时候吗”，我无力地呜咽着，周而复始。

そんな中、それでも被災者の方々の力になれることはないかと漫画家さんや友人と話し合ったり、ツイートを交わしたりして、今回のチャリティ企画が生まれました。  
在这样的情况下，我和漫画家以及朋友们讨论是否可以帮助受灾的人们，在推特上互相交流，于是就有了这次的慈善企划。

光の速さで賛同し、行動してくださった  
多くの漫画家さんたちとgooのみなさんに感謝です。  
続々届く、力と光あふれるイラストに私自身も励まされています。  
以光速响应并行动。
感谢众多的漫画家和goo的每个人。
陆续收到的充满力量与光芒的插画也激励着我。

私はやはり漫画家で、漫画を描くことでしか自分も人も救えません。  
頼りない発起人ですがスタッフともども力を合わせてがんばります。  
ぜひみなさんご協力下さい！  
我毕竟是漫画家，只能通过画漫画来拯救自己和别人。
虽然发起人不可靠，但我会和工作人员一起齐心协力努力。
请大家务必协助!

LINK  
[東北地方太平洋沖地震 緊急災害募金「がんばろう！日本」 - goo 募金](http://special.goo.ne.jp/donation_earthquake/)  