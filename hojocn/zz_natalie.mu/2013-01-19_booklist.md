https://natalie.mu/comic/news/83382

# 【1月19日付】本日発売の単行本リスト

2013年1月19日  [Comment](https://natalie.mu/comic/news/83382/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


コミックナタリーより、本日1月19日に発売される単行本をお知らせいたします。

## 潮出版社

[「クマーラジーヴァ（5）」](http://www.amazon.co.jp/exec/obidos/ASIN/4267905754/nataliecomic-22/ref=nosim) [くさか里樹](https://natalie.mu/comic/artist/1665)

## 少年画報社

[「愛気（14）」](http://www.amazon.co.jp/exec/obidos/ASIN/4785940050/nataliecomic-22/ref=nosim) [ISUTOSHI](https://natalie.mu/comic/artist/2486)  
[「吾輩は××である」](http://www.amazon.co.jp/exec/obidos/ASIN/4785940042/nataliecomic-22/ref=nosim) 堀博昭

## 徳間書店

[「カナヤゴ（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801316/nataliecomic-22/ref=nosim) [日笠優](https://natalie.mu/comic/artist/10053)  
[「エンジェル・ハート 2ndシーズン（5）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801308/nataliecomic-22/ref=nosim) [北条司](https://natalie.mu/comic/artist/2405)  
[「エンジェル・ハート 1stシーズン（21）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801340/nataliecomic-22/ref=nosim) 北条司  
[「エンジェル・ハート 1stシーズン（22）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801359/nataliecomic-22/ref=nosim) 北条司  
[「キミにともだちができるまで。（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801332/nataliecomic-22/ref=nosim) [保谷伸](https://natalie.mu/comic/artist/9940)  
[「のぼさんとカノジョ？（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801324/nataliecomic-22/ref=nosim) [モリコロス](https://natalie.mu/comic/artist/9941)

## 双葉社

[「紅蓮のアルマ（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4575841889/nataliecomic-22/ref=nosim) こも  
[「カロン サイフォン（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4575841897/nataliecomic-22/ref=nosim) 橘りた/榊ヘヌウ