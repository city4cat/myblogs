https://natalie.mu/comic/news/169894

# 「CityHunter XYZ Edition」全巻発売、特典DVDのED曲はGet Wild！
《城市猎人 XYZ Edition》全卷发售，特典DVD的ED曲是Get Wild!

2015年12月19日  [Comment](https://natalie.mu/comic/news/169894/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「CityHunter」の30周年を記念した「CityHunter XYZ Edition」11巻と最終12巻が、本日12月19日に同時発売された。  
纪念北条司《城市猎人》发行30周年的《城市猎人 XYZ Edition》11卷和最终12卷于今天12月19日同时发售。

[
![「CityHunter XYZ Edition」最終12巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz12.jpg?imwidth=468&imdensity=1)
「CityHunter XYZ Edition」最終12巻  
大きなサイズで見る（全19件）  
《城市猎人 XYZ Edition》最终12卷  
大尺寸查看(全19件)  
](https://natalie.mu/comic/gallery/news/169894/450669)

「CityHunter XYZ Edition」は、B6判で各巻約600ページの大ボリュームに仕上げられた新装版。北条が制作秘話を語るロングインタビューや、全57エピソードの解説、全巻揃えると1枚の絵が完成する描き下ろし背表紙などファン必携の豪華な内容になっている。  
《城市猎人 XYZ Edition》是B6尺寸的新装版，每卷约600页。北条讲述制作秘史的长篇采访、全57个小故事的解说、集齐全卷就能完成一幅画的新画背封面等粉丝必备的豪华内容。  

また全12巻の購入者には、2種類から選べる特典も用意された。1つは銃を撃ち放つ香を獠（獠の漢字はけものへんに「僚」のつくり）が支える名シーンと、「XYZ Edition」1巻カバーを飾ったイラストの複製原画2枚セット。もう一方は、新作Motion Graphic Anime「獠のProposal」のDVDだ。  
另外，对于全12卷的购买者，为他们还准备了2种可选的特典。一个是由香瞄准、由獠(獠的汉字是反犬边上组合“僚”)发射的名场景，和“XYZ Edition”1卷封皮上的插图的复制原画2张组套。另一边是新制作的动态动画《獠的求婚》的DVD。  

「獠のProposal」は、「エンジェル・ハート」特別編として執筆された読み切りを映像化したもの。アニメ「ジョジョの奇妙な冒険」のオープニングなどを手がけた神風動画が制作を務め、声優は神谷明ら当時のオリジナルキャストが担当した。エンディングには、TVアニメ1stシリーズでエンディングを飾った[TM NETWORK](https://natalie.mu/comic/artist/1270)の名曲「Get Wild」が使用されている。  
《獠的求婚》是将作为《天使心》特别版写的故事改编的电影。制作动画《JoJo的奇妙冒险》片头的神风动画担任制作，声优则由神谷明等当时的原班演员担当。片尾使用了TV动画1st系列中作为结尾的TM NETWORK名曲《Get Wild》。  

この記事の画像（全19件）  
这篇报道的图片(共19篇)  

[![「CityHunter XYZ Edition」最終12巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450669 "「CityHunter XYZ Edition」最終12巻")
[![「CityHunter XYZ Edition」11巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450670 "「CityHunter XYZ Edition」11巻")
[![「CityHunter XYZ Edition」10巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450671 "「CityHunter XYZ Edition」10巻")
[![「CityHunter XYZ Edition」9巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450672 "「CityHunter XYZ Edition」9巻")
[![「CityHunter XYZ Edition」8巻](https://ogre.natalie.mu/media/news/comic/2015/1219/XYZ08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450673 "「CityHunter XYZ Edition」8巻")
[![「CityHunter XYZ Edition」7巻](https://ogre.natalie.mu/media/news/comic/2015/1219/XYZ07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450674 "「CityHunter XYZ Edition」7巻")
[![「CityHunter XYZ Edition」6巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450675 "「CityHunter XYZ Edition」6巻")
[![「CityHunter XYZ Edition」5巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450676 "「CityHunter XYZ Edition」5巻")
[![「CityHunter XYZ Edition」4巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450677 "「CityHunter XYZ Edition」4巻")
[![「CityHunter XYZ Edition」3巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450678 "「CityHunter XYZ Edition」3巻")
[![「CityHunter XYZ Edition」2巻](https://ogre.natalie.mu/media/news/comic/2015/1219/XYZ02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450679 "「CityHunter XYZ Edition」2巻")
[![「CityHunter XYZ Edition」1巻](https://ogre.natalie.mu/media/news/comic/2015/1219/XYZ01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/450680 "「CityHunter XYZ Edition」1巻")
[![「CityHunter XYZ Edition」全巻特典の複製原画。](https://ogre.natalie.mu/media/news/comic/2015/0624/cityhunter-fukusei2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/375069 "「CityHunter XYZ Edition」全巻特典の複製原画。")
[![「CityHunter XYZ Edition」全巻特典の複製原画。](https://ogre.natalie.mu/media/news/comic/2015/0624/cityhunter-fukusei1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/375068 "「CityHunter XYZ Edition」全巻特典の複製原画。")
[![新作Motion Graphic Anime「獠のProposal」の場面写真。](https://ogre.natalie.mu/media/news/comic/2015/1113/city-bamensya1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/434537 "新作Motion Graphic Anime「獠のProposal」の場面写真。")
[![Motion Graphic Anime「獠のProposal」のDVD盤面※デザインは仮のものにつき、完成版とは異なる場合があります](https://ogre.natalie.mu/media/news/comic/2015/0624/CH_DVD_sample.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/375067 "Motion Graphic Anime「獠のProposal」のDVD盤面※デザインは仮のものにつき、完成版とは異なる場合があります")
[![新作Motion Graphic Anime「獠のProposal」の場面写真。](https://ogre.natalie.mu/media/news/comic/2015/1113/city-bamensya2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/434538 "新作Motion Graphic Anime「獠のProposal」の場面写真。")
[![新作Motion Graphic Anime「獠のProposal」の場面写真。](https://ogre.natalie.mu/media/news/comic/2015/1113/city-bamensya3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/434539 "新作Motion Graphic Anime「獠のProposal」の場面写真。")
[![新作Motion Graphic Anime「獠のProposal」の場面写真。](https://ogre.natalie.mu/media/news/comic/2015/1113/city-bamensya4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/169894/434540 "新作Motion Graphic Anime「獠のProposal」の場面写真。")

(c)北条司/NSP 1985


LINK

[Comic Zenon | Comics](http://www.comic-zenon.jp/comics/)
[北条司 OFFICIAL WEB SITE](http://www.hojo-tsukasa.com/)

