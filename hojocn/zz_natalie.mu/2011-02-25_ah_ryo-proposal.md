https://natalie.mu/comic/news/45565

# ゼノンで「エンジェル・ハート」特別編、獠のプロポーズ話
Zenon“天使心”特别篇、獠的求婚故事

2011年2月25日 [Comment](https://natalie.mu/comic/news/45565/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


吉祥寺のCAFE ZENONにて、婚活イベント「縁結びパーティーXYZ」が2月26日に開催される。これを記念して、本日2月25日に発売された月刊コミックゼノン4月号（徳間書店）は婚活をテーマにした特集を組んでいる。  
吉祥寺的CAFE ZENON将于2月26日举办婚活活动“姻缘派对XYZ”。为了纪念这个，今天2月25日发售的月刊Comic Zenon 4月号(德间书店)以婚活为主题制作了特辑。

[
![月刊コミックゼノン4月号に掲載された「エンジェル・ハート」の特別編より。](https://ogre.natalie.mu/media/news/comic/2011/0225/konkatsuheart.jpg?imwidth=468&imdensity=1)
月刊コミックゼノン4月号に掲載された「エンジェル・ハート」の特別編より。  
大きなサイズで見る（全2件）  
这是月刊Comic Zenon 4月号上刊登的《天使心》的特别篇。   
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/45565/72052)

特集の目玉となるのは、[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート」の特別編。この特別編は同作の第109話に加筆を加えたもので、獠が香にプロポーズした日のことを香瑩に語る内容になっている。「縁結びパーティーXYZ」に参加するゼノン編集部の副編集長と獠の電話シーンなど、特別編らしいユニークな演出も見所だ。  
特集的重头戏是北条司的《天使心》特别篇。这个特别篇是在该作的第109话基础上加笔的东西，内容是獠对香莹讲述向香求婚那天的事。特别版还有一些独特的表演，例如的Zenon编辑部副总编和参加“姻缘派对XYZ”獠的电话场景。（译注：待校对）  

またマンガ家の妻たちが結婚についてを描く「ダーリンは漫画家さん♥」や、高瀬志帆による読み切りのラブストーリー「そこそこ愛してる」など、婚活がテーマの特別企画が満載。3月25日に発売される5月号は、「縁結びパーティーXYZ」のレポートが掲載される。  
还有漫画家的妻子们描绘关于结婚的“Darling漫画家♥”、高濑志帆的Love story“还算爱你”等，满载着以相亲为主题的特别企划。3月25日发售的5月号将刊登“姻缘Party XYZ”的报道。  


この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![月刊コミックゼノン4月号に掲載された「エンジェル・ハート」の特別編より。](https://ogre.natalie.mu/media/news/comic/2011/0225/konkatsuheart.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/45565/72052 "月刊コミックゼノン4月号に掲載された「エンジェル・ハート」の特別編より。")
[![月刊コミックゼノン4月号](https://ogre.natalie.mu/media/news/comic/2011/0225/zenon11-4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/45565/72053 "月刊Comic Zenon 4月号")



LINK  
[Comic Zenon](http://www.comic-zenon.jp/)


