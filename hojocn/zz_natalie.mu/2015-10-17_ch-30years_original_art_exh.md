https://natalie.mu/comic/news/163196

# 「シティーハンター30周年記念 北条司原画展」新宿で開催、ミニクーパーも展示
「City Hunter 30周年纪念 北条司原画展」在新宿举行，还展示了Mini Cooper

2015年10月17日  [Comment](https://natalie.mu/comic/news/163196/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


「シティーハンター30周年記念 [北条司](https://natalie.mu/comic/artist/2405)原画展」が、本日10月17日から11月8日にかけて東京・新宿住友ビルにて開催されている。  
“城市猎人30周年纪念 北条司原画展”于10月17日至11月8日在东京新宿住友大厦举行。

[
![「シティーハンター30周年記念 北条司原画展」のキービジュアルに使用されているイラスト。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1017/CH30THgenga.jpg?imwidth=468&imdensity=1)
「シティーハンター30周年記念 北条司原画展」のキービジュアルに使用されているイラスト。(c)北条司/NSP 1985  
大きなサイズで見る（全10件）  
作为“城市猎人30周年纪念北条司原画展”的关键图像使用的插图。(c)北条司/NSP 1985  
大尺寸查看(全10件)  
](https://natalie.mu/comic/gallery/news/163196/422119)

会場に使われている新宿住友ビルは「シティーハンター」の作中でもたびたび描かれている建物。原画は多くの人が行き交う1階エントランス付近に「シティーハンター」から15枚、「エンジェル・ハート」から15枚がピックアップされ飾られている。そのほか冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）の愛車・ミニクーパーも展示中だ。  
会场的新宿住友大厦是《城市猎人》作品中多次描写的建筑物。原画装饰在人来人往的1楼入口附近，从《城市猎人》和《天使心》中分别挑选了15张。除此之外，冴羽獠(獠的汉字是在反犬旁的边上加上“僚”字)的爱车Mini Cooper也在展示中。

「シティーハンター」は、1985年から1991年にかけて執筆されていた北条の代表作。現在30周年を記念した新装版「シティーハンター XYZ Edition」が毎月2冊ずつリリースされており、10月20日には7・8巻と「エンジェル・ハート2ndシーズン」の12巻も発売される。  
《城市猎人》是北条创作于1985年至1991年的代表作。现在纪念30周年的新装版《城市猎人XYZ Edition》每月发售2册，10月20日还将发售7、8卷和《Angel Heart 2ndSeason》共12卷。

なお「エンジェル・ハート」は、10月から日本テレビ系でTVドラマが放送中。明日10月18日にオンエアされる第2話には、葛山信吾演じる槇村秀幸が登場する。香の兄である槇村が実写化されるのはこれが初めて。どのような演技が披露されるのか期待して待とう。  
另外，《天使心》将于10月起在日本电视台播出电视剧。明天10月18日播出的第2集中，葛山信吾饰演的槙村秀幸将登场。这是香的哥哥槙村首次被拍成真人版。究竟会有怎样的表演，让我们拭目以待吧。

## シティーハンター30周年記念 北条司原画展
城市猎人30周年纪念 北条司原画展  

期間：2015年10月17日（土）～11月8日（日）  
会場：新宿住友ビル1階（新宿住友大厦1楼）  
住所：東京都新宿区西新宿2-6-1

この記事の画像（全10件）  
这篇报道的图片(共10篇)  

[![「シティーハンター30周年記念 北条司原画展」のキービジュアルに使用されているイラスト。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1017/CH30THgenga.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422119 "「シティーハンター30周年記念 北条司原画展」のキービジュアルに使用されているイラスト。(c)北条司/NSP 1985（作为“城市猎人30周年纪念北条司原画展”的关键图像使用的插图。(c)北条司/NSP 1985）")
[![新宿住友ビルが登場する「シティーハンター」の1ページ。このシーンは原画展に飾られていないので注意してほしい。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1017/XYZ5_P529.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422120 "新宿住友ビルが登場する「シティーハンター」の1ページ。このシーンは原画展に飾られていないので注意してほしい。(c)北条司/NSP 1985（新宿住友大楼登场的“城市猎人”的1页。请注意，这个场景并没有装饰在原画展上。(c)北条司/NSP 1985）")
[![新宿住友ビルが登場する「シティーハンター」の1ページ。このシーンは原画展に飾られていない。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1017/XYZ8_P473.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422121 "新宿住友ビルが登場する「シティーハンター」の1ページ。このシーンは原画展に飾られていない。(c)北条司/NSP 1985（新宿住友大楼登场的“城市猎人”的1页。这个场景没有被装饰在原画展上。）")
[![ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。](https://ogre.natalie.mu/media/news/comic/2015/1017/AH2-001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422122 "ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。（电视剧《天使之心》第2集中，槙村秀幸的登场场景。）")
[![ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。](https://ogre.natalie.mu/media/news/comic/2015/1017/AH2-005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422123 "ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。（电视剧《天使之心》第2集中，槙村秀幸的登场场景。）")
[![ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。](https://ogre.natalie.mu/media/news/comic/2015/1017/AH2-004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422124 "ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。（电视剧《天使之心》第2集中，槙村秀幸的登场场景。）")
[![ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。](https://ogre.natalie.mu/media/news/comic/2015/1017/AH2-002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422125 "ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。（电视剧《天使之心》第2集中，槙村秀幸的登场场景。）")
[![ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。](https://ogre.natalie.mu/media/news/comic/2015/1017/AH2-003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422126 "ドラマ「エンジェル・ハート」第2話より、槇村秀幸の登場シーン。（电视剧《天使之心》第2集中，槙村秀幸的登场场景。）")
[![10月20日に発売される「シティーハンター XYZ Edition」7巻。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1017/XYZ7.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422127 "10月20日に発売される「シティーハンター XYZ Edition」7巻。(c)北条司/NSP 1985")
[![10月20日に発売される「シティーハンター XYZ Edition」8巻。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1017/XYZ8.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/163196/422128 "10月20日に発売される「シティーハンター XYZ Edition」8巻。(c)北条司/NSP 1985")

LINK

[Angel Heart | 日本TV](http://www.ntv.co.jp/angelheart/)  
[「City Hunter 30周年記念 北条司原画展」開催!! / NEWS | 30th Anniversary CITYHUNTER since 1985](http://www.hojo-tsukasa.com/ch30/news_5.html)  
[Comic Zenon](http://www.comic-zenon.jp/)  