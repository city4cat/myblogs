https://natalie.mu/eiga/news/204951

# 「シティーハンター」中国で実写映画化、主演はホアン・シャオミン
“城市猎人”将在中国拍真人电影，预计2018年底上映
  
2016年10月11日  [Comment](https://natalie.mu/eiga/news/204951/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


[北条司](https://natalie.mu/eiga/artist/2405)のマンガ「シティーハンター」が中国で実写映画化されることがわかった。  
北条司的漫画《城市猎人》将在中国拍摄真人电影。

[
![「シティーハンター」 Original Manga「CITY HUNTER」(c)1985 by Tsukasa Hojo/North Stars Pictures, Inc. All Rights Reserved.](https://ogre.natalie.mu/media/news/eiga/2016/1011/cityhunter_1.jpg?imwidth=468&imdensity=1)
「シティーハンター」 Original Manga「CITY HUNTER」(c)1985 by Tsukasa Hojo/North Stars Pictures, Inc. All Rights Reserved.  
大きなサイズで見る（全3件）  
查看大图(共3件)
](https://natalie.mu/eiga/gallery/news/204951/593200)

「シティーハンター」は1985年に週刊少年ジャンプで連載が開始されたアクションコメディ。腕は立つが無類の女好きである“スイーパー”冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）と、そのパートナー・槇村香の活躍を描いた。読売テレビ・日本テレビ系列にてテレビアニメ版が放送され、劇場アニメも3本製作。また、1993年にはジャッキー・チェン主演で初めて実写映画化され、2011年にはイ・ミンホ主演で韓国にてテレビドラマ化されている。  
《城市猎人》是1985年在《周刊少年Jump》上开始连载的动作喜剧。作品描写了技艺高超却无比好色的“Sweeper”冴羽獠(獠的汉字由“僚”组成)和他的搭档槙村香活跃的故事。在读卖TV·日本TV播出了电视动画版，剧场动画也制作了3部。另外，1993年由成龙主演首次被拍成真人电影，2011年由李敏镐主演被韩国拍成电视剧。

[
![ホアン・シャオミン（写真提供：CHINAFOTOPRESS / MAXPPP / ゼータ イメージ）](https://ogre.natalie.mu/media/news/eiga/2016/1011/huangxiaoming_201610.jpg?imwidth=468&imdensity=1)
Huang XiaoMing（写真提供：CHINAFOTOPRESS / MAXPPP / ゼータ イメージ）［拡大］  
黄晓明(图片来源:CHINAFOTOPRESS / MAXPPP / Zeta Image)[放大]  
](https://natalie.mu/eiga/gallery/news/204951/593201)

2度目の実写映画化となる今作で主演を務めるのは「イップ・マン 葉問」「ラスト・シャンハイ」の[ホアン・シャオミン](https://natalie.mu/eiga/artist/60722)。「ポリス・ストーリー3」「レッド・ブロンクス」で監督を務めた[スタンリー・トン](https://natalie.mu/eiga/artist/11934)が製作総指揮を執る。  
这是黄晓明第二次拍摄真人电影，他曾主演《叶问》、《Last Shanghai》。曾执导《警察故事3》、《红布朗克斯区》的Stanley Ton将担任制作总指挥。  

「シティーハンター」は2018年12月より中国全土で公開予定。  
《城市猎人》预定于2018年12月在中国全国上映。  



この記事の画像（全3件）  
这篇报道的图片(共3篇)  

[![「シティーハンター」 Original Manga「CITY HUNTER」(c)1985 by Tsukasa Hojo/North Stars Pictures, Inc. All Rights Reserved.](https://ogre.natalie.mu/media/news/eiga/2016/1011/cityhunter_1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/204951/593200 "「シティーハンター」 Original Manga「CITY HUNTER」(c)1985 by Tsukasa Hojo/North Stars Pictures, Inc. All Rights Reserved.")
[![ホアン・シャオミン（写真提供：CHINAFOTOPRESS / MAXPPP / ゼータ イメージ）](https://ogre.natalie.mu/media/news/eiga/2016/1011/huangxiaoming_201610.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/204951/593201 "ホアン・シャオミン（写真提供：CHINAFOTOPRESS / MAXPPP / ゼータ イメージ）")
[![「シティーハンター」 Original Manga「CITY HUNTER」(c)1985 by Tsukasa Hojo/North Stars Pictures, Inc. All Rights Reserved.](https://ogre.natalie.mu/media/news/eiga/2016/1011/cityhunter_2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/204951/593202 "「シティーハンター」 Original Manga「CITY HUNTER」(c)1985 by Tsukasa Hojo/North Stars Pictures, Inc. All Rights Reserved.")


関連商品

[
![北条司「シティーハンター XYZ edition（1）」](https://images-fe.ssl-images-amazon.com/images/I/51wmgxxDDoL._SS70_.jpg)
北条司「シティーハンター XYZ edition（1）」  
[漫画] 2015年7月18日発売 / 徳間書店 / 978-4199802812  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802819/natalieeiga-22)

[
![シティーハンター（SPECIAL VERSION / DVD）](https://images-fe.ssl-images-amazon.com/images/I/51NMHNPfFaL._SS70_.jpg)
シティーハンター（SPECIAL VERSION / DVD）  
[DVD] 2014年11月28日発売 / RAX-901  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B00PJRN2KO/natalieeiga-22)

[
![イップ・マン 葉問（DVD）](https://images-fe.ssl-images-amazon.com/images/I/51yPf07ZsrL._SS70_.jpg)
イップ・マン 葉問（DVD）  
[DVD] 2011年6月2日発売 / Happinet / BBBN-1046  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B004NRP1QO/natalieeiga-22)

[
![Last Shanghai（DVD）](https://images-fe.ssl-images-amazon.com/images/I/515MgUvtDLL._SS70_.jpg)
Last Shanghai（DVD）  
[DVD] 2013年10月25日発売 / TCエンタテインメント / TCED-1867  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B00E0H99PG/natalieeiga-22)