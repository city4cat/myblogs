# 「少年ジャンプ展」民明書房の房闘羅札（トランプ）など販売グッズ公開
“少年Jump展”民明书房的房斗罗牌(Trump)等商品公开

2017年6月27日  [Comment](https://natalie.mu/comic/news/238527/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


週刊少年ジャンプ（集英社）の創刊50周年を記念した「週刊少年ジャンプ展」の第1弾「創刊50周年記念 週刊少年ジャンプ展VOL.1 創刊～1980年代、伝説のはじまり」のグッズ情報が公開された。  
为纪念周刊少年Jump(集英社)创刊50周年，《周刊少年Jump展》公开了第1弹《创刊50周年纪念 周刊少年Jump展VOL.1 创刊~1980年代，传说的开始》的周边信息。

[
![「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/otoko_tp_mihon-6.jpg?imwidth=468&imdensity=1)
「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社  
大きなサイズで見る（全24件）  
《魁!!男塾》民明书房斗罗札》(c)宫下明/集英社  
查看大号(共24件)  
](https://natalie.mu/comic/gallery/news/238527/729347)

会場では[宮下あきら](https://natalie.mu/comic/artist/1877)「魁!!男塾」に登場する民明書房の書籍の記述を記した「『魁!!男塾』民明書房闘羅札（トランプ）」や、[鳥山明](https://natalie.mu/comic/artist/2266)「Dr.スランプ」のぬいぐるみ「『Dr.スランプ』アラアラおてだまBOX」、[北条司](https://natalie.mu/comic/artist/2405)「キャッツ▼アイ」の予告状「キャッツカード」をモチーフにしたパスケースなど200種類を超えるアイテムを販売。会場受注商品もラインナップされる。  
在会场上，宫下あきら“魁!!男塾”中登场的民明书房的书籍记述了《魁!!男塾》民明书房斗罗牌(Trump)、鸟山明《Dr.Slump》的布偶“《Dr.Slum》啦啦啦手鼓BOX”、北条司《Cat's▼Eye》的预告信《Cat'sCard》为主题的卡包等。销售超过200种的商品。会场订货商品也将列装。  

「創刊50周年記念 週刊少年ジャンプ展VOL.1」は7月18日から10月15日まで、東京・森アーツセンターギャラリーにて開催。2018年春には第2弾「創刊50周年記念 週刊少年ジャンプ展 VOL.2 1990年代、発行部数653万部の衝撃」、2018年夏には「創刊50周年記念 週刊少年ジャンプ展 VOL.3 2000年代～、進化する最強雑誌の現在（いま）」がそれぞれ実施される。  
“创刊50周年纪念 周刊少年Jump展VOL.1”将于7月18日至10月15日在东京·森艺术中心画廊举行。2018年春推出第2弹“创刊50周年纪念 周刊少年Jump展VOL.2 1990年代发行量653万部的冲击”，2018年夏推出“创刊50周年纪念 周刊少年Jump展VOL.3 2000年代~进化中的最强杂志的现在”。

この記事の画像（全24件）  
这篇报道的图片(共24篇)  

[![「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/otoko_tp_mihon-6.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729347 "「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社")
[![「『Dr.スランプ』アラアラおてだまBOX」(c)鳥山明/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/araaraotedama2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729348 "「『Dr.スランプ』アラアラおてだまBOX」(c)鳥山明/集英社")
[![「キャッツ▼アイ」パスケース (c)北条司/NSP 1981 版権許諾証 GM-807](https://ogre.natalie.mu/media/news/comic/2017/0627/bacon0011.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729349 "「キャッツ▼アイ」パスケース (c)北条司/NSP 1981 版権許諾証 GM-807")
[![「シティーハンター」アートボード (c)北条司/NSP 1985 版権許諾証 GM-807](https://ogre.natalie.mu/media/news/comic/2017/0627/cityhunter02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729350 "「シティーハンター」アートボード (c)北条司/NSP 1985 版権許諾証 GM-807")
[![「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/otoko_tp_mihon-1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729351 "「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社")
[![「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/otoko_tp_mihon-2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729352 "「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社")
[![「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/otoko_tp_mihon-3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729353 "「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社")
[![「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/otoko_tp_mihon-4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729354 "「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社")
[![「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/otoko_tp_mihon-5.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729355 "「『魁!!男塾』民明書房闘羅札」(c)宮下あきら/集英社")
[![「3年奇面組」マグカップ (c)新沢基栄](https://ogre.natalie.mu/media/news/comic/2017/0627/magcup_sannenkimengumi2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729356 "「3年奇面組」マグカップ (c)新沢基栄")
[![「3年奇面組」マグカップ (c)新沢基栄](https://ogre.natalie.mu/media/news/comic/2017/0627/magcup_sannenkimengumi1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729357 "「3年奇面組」マグカップ (c)新沢基栄")
[![「『Dr.スランプ』アラアラおてだまBOX」(c)鳥山明/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/araaraotedama1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729358 "「『Dr.スランプ』アラアラおてだまBOX」(c)鳥山明/集英社")
[![「キャッツ▼アイ」パスケース (c)北条司/NSP 1981 版権許諾証 GM-807](https://ogre.natalie.mu/media/news/comic/2017/0627/card.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729359 "「キャッツ▼アイ」パスケース (c)北条司/NSP 1981 版権許諾証 GM-807")
[![「キャプテン翼」フェイスタオル (c)高橋陽一/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/captaintsubasafacetowel.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729360 "「キャプテン翼」フェイスタオル (c)高橋陽一/集英社")
[![「キン肉マン」木樽ジョッキ (c)ゆでたまご/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/kinnnikuman01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729361 "「キン肉マン」木樽ジョッキ (c)ゆでたまご/集英社")
[![「キン肉マン」木樽ジョッキ (c)ゆでたまご/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/kinnnikuman02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729362 "「キン肉マン」木樽ジョッキ (c)ゆでたまご/集英社")
[![「キン肉マン」木樽ジョッキ (c)ゆでたまご/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/kinnnikuman03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729363 "「キン肉マン」木樽ジョッキ (c)ゆでたまご/集英社")
[![「シティーハンター」アートボード (c)北条司/NSP 1985 版権許諾証 GM-807](https://ogre.natalie.mu/media/news/comic/2017/0627/cityhunter01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729364 "「シティーハンター」アートボード (c)北条司/NSP 1985 版権許諾証 GM-807")
[![「聖闘士星矢」iPhoneケース (c)車田正美/集英社](https://ogre.natalie.mu/media/news/comic/2017/0627/seiyaiPhone.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729365 "「聖闘士星矢」iPhoneケース (c)車田正美/集英社")
[![「『北斗の拳』断末魔バウムクーヘン」(c)武論尊・原哲夫/NSP 1983 版権許諾証 GM-807](https://ogre.natalie.mu/media/news/comic/2017/0627/hokutonoken02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729366 "「『北斗の拳』断末魔バウムクーヘン」(c)武論尊・原哲夫/NSP 1983 版権許諾証 GM-807")
[![「『北斗の拳』断末魔バウムクーヘン」(c)武論尊・原哲夫/NSP 1983 版権許諾証 GM-807](https://ogre.natalie.mu/media/news/comic/2017/0627/hokutonoken01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729367 "「『北斗の拳』断末魔バウムクーヘン」(c)武論尊・原哲夫/NSP 1983 版権許諾証 GM-807")

[![「創刊50周年記念 週刊少年ジャンプ展 VOL.1 創刊～1980年代、伝説のはじまり」のビジュアル。](https://ogre.natalie.mu/media/news/comic/2017/0627/jumptenvisual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/729368 "「創刊50周年記念 週刊少年ジャンプ展 VOL.1 創刊～1980年代、伝説のはじまり」のビジュアル。")
[![「創刊50周年記念 週刊少年ジャンプ展 VOL.2 1990年代、発行部数653万部の衝撃」のロゴ。](https://ogre.natalie.mu/media/news/comic/2017/0507/jumpten11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/701099 "「創刊50周年記念 週刊少年ジャンプ展 VOL.2 1990年代、発行部数653万部の衝撃」のロゴ。")
[![「創刊50周年記念 週刊少年ジャンプ展 VOL.3 2000年代～、進化する最強雑誌の現在」のロゴ。](https://ogre.natalie.mu/media/news/comic/2017/0507/jumpten12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/238527/701100 "「創刊50周年記念 週刊少年ジャンプ展 VOL.3 2000年代～、進化する最強雑誌の現在」のロゴ。")

## 創刊50周年記念 週刊少年ジャンプ展 VOL.1 創刊～1980年代、伝説のはじまり
创刊50周年纪念 周刊少年Jump展 VOL.1 创刊~1980年代，传说的开始

会期：2017年7月18日（火）～10月15日（日）  
会場：森アーツセンターギャラリー  
会场:森艺术中心画廊  

### 出展予定作品

[秋本治](https://natalie.mu/comic/artist/2053)「こちら葛飾区亀有公園前派出所」、[荒木飛呂彦](https://natalie.mu/comic/artist/1940)「ジョジョの奇妙な冒険」「魔少年ビーティー」、池沢さとし「サーキットの狼」、[今泉伸二](https://natalie.mu/comic/artist/3579)「神様はサウスポー」「空のキャンバス」、[江川達也](https://natalie.mu/comic/artist/1936)「まじかる☆タルるートくん」、[江口寿史](https://natalie.mu/comic/artist/1935)「すすめ!!パイレーツ」「ストップ!!ひばりくん！」、[えんどコイチ](https://natalie.mu/comic/artist/2630)「ついでにとんちんかん」、[貝塚ひろし](https://natalie.mu/comic/artist/3266)「父の魂」、鏡丈二・[金井たつお](https://natalie.mu/comic/artist/3394)「ホールインワン」、[梶原一騎](https://natalie.mu/comic/artist/5588)・井上コオ「侍ジャイアンツ」、[桂正和](https://natalie.mu/comic/artist/1908)「ウイングマン」「電影少女」、牛次郎・[ビッグ錠](https://natalie.mu/comic/artist/2869)「包丁人味平」、[車田正美](https://natalie.mu/comic/artist/2044)「リングにかけろ」「風魔の小次郎」「男坂」「聖闘士星矢」、[小林よしのり](https://natalie.mu/comic/artist/2078)「東大一直線」、[コンタロウ](https://natalie.mu/comic/artist/2717)「1・2のアッホ!!」、[佐藤正](https://natalie.mu/comic/artist/3612)「燃える！お兄さん」、[三条陸](https://natalie.mu/comic/artist/3688)・[稲田浩司](https://natalie.mu/comic/artist/3122)・堀井雄二「DRAGON QUEST-ダイの大冒険-」、[新沢基栄](https://natalie.mu/comic/artist/3980)「3年奇面組」「ハイスクール！奇面組」、[高橋陽一](https://natalie.mu/comic/artist/1953)「キャプテン翼」、[高橋よしひろ](https://natalie.mu/comic/artist/1950)「悪たれ巨人」「銀牙-流れ星 銀-」、[ちばあきお](https://natalie.mu/comic/artist/2793)「プレイボール」、[次原隆二](https://natalie.mu/comic/artist/3787)「よろしくメカドック」、[寺沢武一](https://natalie.mu/comic/artist/2034)「コブラ」、寺島優・[小谷憲一](https://natalie.mu/comic/artist/3877)「テニスボーイ」、[遠崎史朗](https://natalie.mu/comic/artist/3172)・[中島徳博](https://natalie.mu/comic/artist/2258)「アストロ球団」、[徳弘正也](https://natalie.mu/comic/artist/2325)「シェイプアップ乱」「ジャングルの王者ターちゃん▽」、とりいかずよし「トイレット博士」、[鳥山明](https://natalie.mu/comic/artist/2266)「Dr.スランプ」「DRAGON BALL」、[永井豪](https://natalie.mu/comic/artist/1804)「ハレンチ学園」、[中沢啓治](https://natalie.mu/comic/artist/2257)「はだしのゲン」、[にわのまこと](https://natalie.mu/comic/artist/2838)「THE MOMOTAROH」、[萩原一至](https://natalie.mu/comic/artist/2351)「BASTARD!!-暗黒の破壊神-」、[土方茂](https://natalie.mu/comic/artist/4528)「CYBORGじいちゃんG」、[平松伸二](https://natalie.mu/comic/artist/4772)「ブラック・エンジェルズ」、[武論尊](https://natalie.mu/comic/artist/2383)・平松伸二「ドーベルマン刑事」、武論尊・[原哲夫](https://natalie.mu/comic/artist/1917)「北斗の拳」、[北条司](https://natalie.mu/comic/artist/2405)「キャッツ▼アイ」「シティーハンター」、[星野之宣](https://natalie.mu/comic/artist/2145)「ブルーシティー」、[巻来功士](https://natalie.mu/comic/artist/3279)「ゴッドサイダー」、[宮下あきら](https://natalie.mu/comic/artist/1877)「激!!極虎一家」「魁!!男塾」、[本宮ひろ志](https://natalie.mu/comic/artist/2409)「男一匹ガキ大将」「山崎銀次郎」「さわやか万太郎」「やぶれかぶれ」、[森田まさのり](https://natalie.mu/comic/artist/2117)「ろくでなしBLUES」、[諸星大二郎](https://natalie.mu/comic/artist/2057)「妖怪ハンター」「孔子暗黒伝」、[山川惣治](https://natalie.mu/comic/artist/95751)・[川崎のぼる](https://natalie.mu/comic/artist/2186)「荒野の少年イサム」、[ゆでたまご](https://natalie.mu/comic/artist/1756)「キン肉マン」、[吉沢やすみ](https://natalie.mu/comic/artist/3320)「ど根性ガエル」  
秋本治《乌龙派出所》、荒木飞吕彦《jojoo的奇妙冒险》、《魔少年比蒂》、池泽贤《赛道之狼》、今泉伸二《神是左》、《天空的画布》、江川达也《魔法小子》小斗”、江口寿史“前进!!海盗”“停止!!云雀君!”、end小一《顺道来个猪鸡》、贝冢宏《父亲的灵魂》、镜丈二·金井达夫《一杆进洞》、梶原一骑·井上子夫《武士巨人》、桂正和《翼人》《电影少女》、牛次郎·大锭《菜刀人味》平》、车田正美《上擂台》、《风魔小次郎》、《男坂》、《圣斗士星矢》、小林吉典《东大一直线》、康塔郎《1·2的阿浩!!》、佐藤正《燃烧吧!哥哥》、三条陆、稻田浩司、堀井雄二《DRAGON queste -大的大冒险》、新泽基荣《3年奇面组》《高中!奇面组》、高桥阳一《足球小将》、高桥吉广《恶巨人》、《银牙-流星》银-”、千叶昭夫“play ball”、次原隆二“请多关照机械doc”、寺泽武一“眼镜蛇”、寺岛优·小谷宪一“网球男孩”、远崎史朗·中岛德博“astroo球团”、德弘正也“shopup乱”“丛林的森林”《王者归来》、《toilet博士》、鸟山明《Dr.瓶颈期》、《DRAGON BALL》、永井豪《harench学园》、中泽启治《赤足小子》、庭之真《THEMOMOTAROH”、萩原一至“BASTARD!!-黑暗的破坏神-》、土方茂《CYBORG爷爷G》、平松伸二《黑色天使》、武论尊·平松伸二《doberman刑警》、武论尊·原哲夫《北斗神拳》、北条司《猫▼爱》《城市猎人》”、星野之宣“blue city”、卷来功勋士“上帝汽水”、宫下彰“激!!极虎一家”“魁!!男塾》、本宫浩志《男子汉大兵》、《山崎银次郎》、《清爽万太郎》、《破罐子破爬》、森田正宪《混账小子BLUES》、诸星大二郎《妖怪猎人》、《孔子黑暗传》、山川惣治·川崎升《荒野少年井》山姆”、煮鸡蛋“筋肉人”、吉泽休假“根性青蛙” (译注：待校对)  

## 創刊50周年記念 週刊少年ジャンプ展 VOL.2 1990年代、発行部数653万部の衝撃  
创刊50周年纪念 周刊少年Jump展 VOL.2 1990年代发行量653万部的冲击

開催時期：2018年春を予定  
举办时间:预计2018年春季  

会場：森アーツセンターギャラリー  
会场:森艺术中心画廊  

### 出展予定作品

鳥山明「DRAGON BALL」、[井上雄彦](https://natalie.mu/comic/artist/1791)「SLAM DUNK」、[冨樫義博](https://natalie.mu/comic/artist/2377)「幽☆遊☆白書」、[和月伸宏](https://natalie.mu/comic/artist/4971)「るろうに剣心 -明治剣客浪漫譚-」、[高橋和希](https://natalie.mu/comic/artist/1955)「遊☆戯☆王」ほか  
鸟山明《DRAGON BALL》、井上雄彦《灌篮高手》、富坚义博《幽游白书》、和月伸宏《浪客剑心-明治剑客浪漫谭-》、高桥和希《游戏王》等  

## 創刊50周年記念 週刊少年ジャンプ展 VOL.3 2000年代～、進化する最強雑誌の現在
创刊50周年纪念 周刊少年Jump展 VOL.3 2000年代~进化的最强杂志的现在  

開催時期：2018年夏を予定  
举办时间:预计2018年夏天  

会場：森アーツセンターギャラリー  
会场:森艺术中心画廊  

### 出展予定作品

[尾田栄一郎](https://natalie.mu/comic/artist/2368)「ONE PIECE」、[岸本斉史](https://natalie.mu/comic/artist/3293)「NARUTO‐ナルト‐」、[大場つぐみ](https://natalie.mu/comic/artist/2218)・[小畑健](https://natalie.mu/comic/artist/2075)「DEATH NOTE」、[古舘春一](https://natalie.mu/comic/artist/8561)「ハイキュー!!」、[松井優征](https://natalie.mu/comic/artist/2081)「暗殺教室」ほか  
尾田荣一郎《海贼王》、岸本齐史《火影忍者》、大场鸫·小畑健《死亡笔记》、古馆春一《排球少年!!》、松井优征《暗杀教室》等  

※▼はハートマーク。  
※▼是心形图案。  

LINK

[週刊少年Jump展](http://shonenjump-ten.com/)