source:  
https://natalie.mu/comic/news/513837

# 「劇場版CityHunter」新作は"AngelDust"、香が2023tHammer振り回す特報も
「劇場版CityHunter」新片"AngelDust"、香在特别预告片中挥舞着2023t锤子  

2023年2月22日  
[Comic Natalie編集部](https://natalie.mu/comic/author/72)

[記事へのComment（45件）](https://natalie.mu/comic/news/513837/comment)

[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「劇場版CityHunter」の正式タイトルが「劇場版CityHunter 天使の涙（AngelDust）」であることが明らかに。今年秋に公開される。  
北条司原作的动画「劇場版CityHunter」的正式标题「劇場版CityHunter 天使之泪（AngelDust）」。该片将于今年秋天上映。  

[
![「劇場版CityHunter 天使の涙」Poster Visual](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_poster.jpg?imwidth=468&imdensity=1)
「劇場版CityHunter 天使の涙」Poster Visual
大きなサイズで見る（全14件）（查看大图）
](https://natalie.mu/comic/gallery/news/513837/2003409)

[
「劇場版CityHunter 天使の涙」特報より。［拡大］
](https://natalie.mu/comic/gallery/news/513837/2003404)

「CityHunter」は新宿の凄腕スイーパーである“CityHunter”こと冴羽リョウの活躍を描く物語。2019年に公開された前作「[劇場版CityHunter <新宿Private Eyes>](https://natalie.mu/eiga/film/175572)」と同じく現代の東京を舞台に、物語は主人公・冴羽リョウの過去と、彼のパートナーであった槇村秀幸の死の核心に迫っていく。正式タイトルと公開時期の発表と合わせ、特報も解禁に。香が「祝 新作 2023t」と書かれたHammerを振り回すさまや、リョウが仮面の人物と撃ち合いになる様子が切り取られた。  
《CityHunter》是描写新宿的厉害Sweeper“CityHunter”冴羽獠活跃的故事。2019年公开的前作“剧场版CityHunter<新宿Private Eyes>”一样,现代的东京为舞台,故事将逼近主人公·冴羽獠的过去，和他的搭档牧村秀幸之死的核心。随着正式标题和上映时间的发表，特报也将解禁。香挥舞写有“祝 新作2023 t”的锤子的样子，獠与一个蒙面人物发生枪战。。

[
「劇場版CityHunter 天使の涙」特報より。［拡大］
](https://natalie.mu/comic/gallery/news/513837/2003401)

スタッフ情報も一部公開され、「劇場版CityHunter <新宿Private Eyes>」に引き続き[こだま兼嗣](https://natalie.mu/comic/artist/13476)が総監督として参加することが明らかに。脚本はむとうやすゆきが手がける。こだまからのCommentは以下に掲載した。  
剧场版CityHunter <新宿Private Eyes>”继续由こだま兼嗣作为总教练。剧本由武藤泰之执笔。来自こだま的Comment刊登如下。  

[
「劇場版CityHunter 天使の涙」Mvtkcard［拡大］
](https://natalie.mu/comic/gallery/news/513837/2003407)

なお明日2月23日に、第1弾特典付きMvtkcardが販売開始。B4サイズのクリアポスターが特典として用意された。  
另外明天2月23日，附有第1弹特典的Mvtkcard开始销售。B4尺寸的透明海报作为特典准备好了。  

## こだま兼嗣（総監督）Comment

アニメ放送開始から36年、もはや避けて通れない冴羽リョウの原点を探る作品となっています。愛ともっこり、そしてHard-boiled。  
在动画片开始播放的36年后，这部作品探讨了再也无法回避的冴羽獠的起源。爱情、莫逆之交和硬派。  

冴羽リョウの前に現れる史上最強の敵。  
历史上最强大的敌人出现在冴羽獠面前。  

傷を負いながらも依頼を遂行しようとする冴羽リョウ。  
尽管有伤在身，冴羽獠还是试图执行他的委托。  

究極の戦いが、ここに始まります。アクションたっぷりの映像をお楽しみください。  
最终的战斗在这里开始。请欣赏这段充满动作的镜头。  

関連する特集・Interview（相关专题/访谈）  

[
「劇場版CityHunter <新宿Private Eyes>」特集 神谷明Interview
2019年10月25日
](https://natalie.mu/comic/pp/cityhunter-movie03)

[
「劇場版CityHunter <新宿Private Eyes>」玄田哲章Interview
2019年2月7日
](https://natalie.mu/eiga/pp/cityhunter-movie02)

[
「劇場版CityHunter <新宿Private Eyes>」特集 北条司（原作者）×神谷明（冴羽獠役）Interview
2019年2月1日
](https://natalie.mu/comic/pp/cityhunter-movie01)

この記事の画像・動画（全14件）

*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_poster.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003409 "「劇場版CityHunter 天使の涙」Poster Visual")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_5.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003404 "「劇場版CityHunter 天使の涙」特報より。")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003401 "「劇場版CityHunter 天使の涙」特報より。")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_movitichet1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003407 "「劇場版CityHunter 天使の涙」Mvtkcard")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003402 "「劇場版CityHunter 天使の涙」特報より。")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003403 "「劇場版CityHunter 天使の涙」特報より。")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003400 "「劇場版CityHunter 天使の涙」特報より。")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_7.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003406 "「劇場版CityHunter 天使の涙」特報より。")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_6.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003405 "「劇場版CityHunter 天使の涙」特報より。")
*   [![](https://ogre.natalie.mu/media/news/comic/2023/0221/chm_movitichet2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/2003408 "「劇場版CityHunter 天使の涙」ムビチケ第1弾特典のクリアポスター。")
*   [![](https://ogre.natalie.mu/media/news/comic/2022/1201/cityhunter-movi_1202.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/1951712 "「劇場版CityHunter」の北条司描き下ろし新ビジュアル。")
*   [![](https://ogre.natalie.mu/media/news/comic/2022/0407/gekijouban_city_hunter_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/513837/1798730 "劇場版「CityHunter」のロゴ。")
*   [![](https://i.ytimg.com/vi/pvVjkP_94-k/default.jpg)](https://natalie.mu/comic/gallery/news/513837/media/75595 "新作「劇場版CityHunter」制作決定スペシャルムービー2022")
*   [![](https://i.ytimg.com/vi/7O1VR5cbQ_k/default.jpg)](https://natalie.mu/comic/gallery/news/513837/media/87528 "「劇場版CityHunter 天使の涙」特報")

## 「劇場版CityHunter 天使の涙」

2023年秋公開

### staff

原作：[北条司](https://natalie.mu/comic/artist/2405)  
総監督：[こだま兼嗣](https://natalie.mu/comic/artist/13476)  
脚本：むとうやすゆき  
制作：Sunrise, Answer Studio      
配給：Aniplex  

### cast

冴羽リョウ：[神谷明](https://natalie.mu/comic/artist/60997)  
槇村香：[伊倉一恵](https://natalie.mu/comic/artist/95562)  
野上冴子：[一龍斎春水](https://natalie.mu/comic/artist/107953)  
海坊主：[玄田哲章](https://natalie.mu/comic/artist/19455)  
美樹：[小山茉美](https://natalie.mu/comic/artist/19454)

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。
※冴羽獠的獠的官方写法是犭的「僚」  

(c)北条司/Coamix・「2023 劇場版CityHunter」製作委員会

## 読者の反応

*   [Comment](https://natalie.mu/comic/news/513837/comment)

## 北条司のほかの記事

*   [仏の実写版「CityHunter」冴羽リョウの誕生日に新宿で“もっこり応援上映”](https://natalie.mu/comic/news/513554)
*   [映画「CityHunter」香役は森田望智、伝言板の前で笑顔を見せる姿も公開](https://natalie.mu/comic/news/510937)
*   [「ルパン三世VSキャッツ・アイ」“幻”のシナリオ言い当てる栗田貫一に監督「鋭い！」](https://natalie.mu/comic/news/510174)
*   [「ルパン三世VSキャッツ・アイ」栗田貫一や戸田恵子ら11人からComment到着](https://natalie.mu/comic/news/508556)
*   [北条司のプロフィール](https://natalie.mu/comic/artist/2405)

Link:  

*   [アニメ「劇場版CityHunter <新宿プライベート･アイズ>」公式サイト](https://cityhunter-movie.com/)
*   [アニメ「劇場版CityHunter ＜新宿プライベート･アイズ＞」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)
*   [Get Wild EXPO 2022（ゲワイ博）TM NETWORK「Get Wild」35th Anniversary 「Get Wild」総選挙](https://www.110107.com/s/oto/page/GetWildEXPO?ima=3401)
*   [TM NETWORK - YouTube](https://www.youtube.com/channel/UC6cJPKVcHh31efgzk4W2_EA)

Tag:  

*   [動画あり](https://natalie.mu/comic/tag/90)
*   [Commentあり](https://natalie.mu/comic/tag/91)
*   [北条司](https://natalie.mu/comic/artist/2405)
*   [神谷明](https://natalie.mu/comic/artist/60997)
*   [伊倉一恵](https://natalie.mu/comic/artist/95562)
*   [一龍斎春水](https://natalie.mu/comic/artist/107953)
*   [玄田哲章](https://natalie.mu/comic/artist/19455)
*   [小山茉美](https://natalie.mu/comic/artist/19454)
*   [こだま兼嗣](https://natalie.mu/comic/artist/13476)
*   [劇場版CityHunter <新宿Private Eyes>](https://natalie.mu/eiga/film/175572)


---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
