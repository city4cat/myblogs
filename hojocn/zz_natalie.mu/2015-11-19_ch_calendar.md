https://natalie.mu/comic/news/166481

# 「シティーハンター」リョウと香が北九州市を巡る2016年カレンダー
《城市猎人》獠和香围绕北九州市的2016年历

2015年11月19日 [Comment](https://natalie.mu/comic/news/166481/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」30周年を記念した2016年のカレンダーを、北九州市漫画ミュージアムが12月2日から2016年2月末にかけて500部限定で販売する。  
北九州市漫画博物馆将于12月2日至2016年2月末发售纪念北条司《城市猎人》发行30周年的2016年日历，限量发售500本。

[
![「シティーハンター30th　ANNIVERSARY Calendar　2016 獠と香とめぐる 北条司のふるさと・北九州」。500部限定につき、なくなり次第販売は終了される。](https://ogre.natalie.mu/media/news/comic/2015/1118/cityhunter-calender002.jpg?imwidth=468&imdensity=1)
「シティーハンター30th　ANNIVERSARY Calendar　2016 獠と香とめぐる 北条司のふるさと・北九州」。500部限定につき、なくなり次第販売は終了される。  
大きなサイズで見る（全2件）  
《城市猎人30th ANNIVERSARY Calendar 2016 獠与香围绕北条司的故乡北九州》。限量500本，售完即止。  
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/166481/436790)

同ミュージアムは北九州ゆかりのマンガ家とコラボしたカレンダーを毎年制作しており、北条が北九州出身であることから2016年版には「シティーハンター」が起用された。北九州市の観光スポットをリョウ（リョウの漢字はけものへんに「僚」のつくり）」と香が巡る、実写の風景とイラストが融合したビジュアルがカレンダーでは楽しめる。  
该博物馆每年都会制作与北九州有关的漫画家合作制作日历，因为北条是北九州人，所以2016年版启用了《城市猎人》。这本日历的特点是视觉上融合了实景和插图，"獠"（"亮 "的汉字由反犬旁组合"僚"）和香游览了北九州市的景点。

カレンダーは壁掛けタイプで、B3サイズの14枚綴り。価格は1000円。ミュージアム6階の常設展エントランスで販売されるほか、通信販売も行われる。詳細な購入方法は公式サイトにて確認を。  
挂历是壁挂式的，14张B3大小的挂历。价格为1000日元。除了在博物馆6层的常设展入口出售外，也有邮购。详细的购买方法请在官方网站确认。  

この記事の画像（全2件）  
这篇报道的图片(共两篇)  

[![「シティーハンター30th　ANNIVERSARY Calendar　2016 獠と香とめぐる 北条司のふるさと・北九州」。500部限定につき、なくなり次第販売は終了される。](https://ogre.natalie.mu/media/news/comic/2015/1118/cityhunter-calender002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/166481/436790 "「シティーハンター30th　ANNIVERSARY Calendar　2016 獠と香とめぐる 北条司のふるさと・北九州」。500部限定につき、なくなり次第販売は終了される。(《城市猎人30th ANNIVERSARY Calendar 2016獠与香围绕北条司的故乡北九州》。限量500本，售完即止。)")
[![「シティーハンター30th　ANNIVERSARY Calendar　2016 獠と香とめぐる 北条司のふるさと・北九州」のチラシ。](https://ogre.natalie.mu/media/news/comic/2015/1118/cityhunter-calender001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/166481/436791 "「シティーハンター30th　ANNIVERSARY Calendar　2016 獠と香とめぐる 北条司のふるさと・北九州」のチラシ。(“城市猎人30th ANNIVERSARY Calendar 2016獠与香的北条司故乡北九州”的传单。)")

(c)北条 司／NSP 1985


LINK

[「シティーハンター」Original Calendar発売決定！！ | 北九州市漫画Museum](http://www.ktqmm.jp/blog/7171.html)