https://natalie.mu/comic/news/56973

# ゼノン付録に、シティーハンター＆AHClearFile
Zenon附录，城市猎人& AH ClearFile  

2011年9月24日  [Comment](https://natalie.mu/comic/news/56973/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート2ndシーズン」「シティーハンター」のイラストがあしらわれた特製ClearFileが、発売中の月刊Comic Zenon11月号に付いてくる。  
印有北条司“Angel Heart 2nd季”、“城市猎人”插图的特制透明文件，附在发售中的月刊Comic Zenon 11月号上。

[
![月刊Comic Zenon11月号付録のClearFile。](https://ogre.natalie.mu/media/news/comic/2011/0923/ahcl.jpg?imwidth=468&imdensity=1)
月刊Comic Zenon 11月号付録のClearFile。  
大きなサイズで見る（全2件）  
月刊Comic Zenon 11月号附录的ClearFile。  
查看大图(共2件)
](https://natalie.mu/comic/gallery/news/56973/92133)

ClearFileは片面が獠、香瑩、香の3ショットが描かれた「エンジェル・ハート2ndシーズン」サイドに、その裏面はアニメの名場面をデザインした「シティーハンター」サイドになっている。往年のファンにはたまらないアイテムだ。  
ClearFile一面是獠、香莹、香的3个镜头的“天使之心2nd季”侧面，其背面是设计动画名场面的“城市猎人”。这是昔日粉丝的必备物品。  

なお月刊Comic Zenonは、10月25日に発売される12月号にて創刊1周年。これを記念して12月号にはPEACH JOHNとコラボした勝負パンツが付いてくる。  
月刊Comic Zenon 将于10月25日发售的12月号是创刊1周年。为了纪念这个，12月号还附有与PEACH JOHN合作的决胜Pants。  

この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![月刊Comic Zenon11月号付録のClearFile。](https://ogre.natalie.mu/media/news/comic/2011/0923/ahcl.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/56973/92133 "月刊Comic Zenon11月号付録のClearFile。")
[![月刊Comic Zenon11月号](https://ogre.natalie.mu/media/news/comic/2011/0923/zenon11-11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/56973/92134 "月刊Comic Zenon11月号")



LINK

[Comic Zenon](http://www.comic-zenon.jp/)

