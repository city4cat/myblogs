https://natalie.mu/comic/news/37632

# 北条司、globeのCDジャケで小室他メンバー描き下ろし
北条司，在globe的CD杂志上画下小室其他成员

2010年9月14日 [Comment](https://natalie.mu/comic/news/37632/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)の描き下ろしイラストが、9月29日に発売されるglobeのデビュー15周年記念アルバム「15YEARS -BEST HIT SELECTION-」のジャケットに使用される。  
北条司所绘制的插画将用于9月29日发售的globe出道15周年纪念专辑《15YEARS -BEST HIT SELECTION-》的封面。  

[
![北条司がイラストを描き下ろしたジャケット。左の元気に飛び跳ねているのがMARC、真ん中の女性がKEIKO、右の色男が小室哲哉だ。](https://ogre.natalie.mu/media/news/music/2010/0914/globe_AVCG70102.jpg?imwidth=468&imdensity=1)  
北条司がイラストを描き下ろしたジャケット。左の元気に飛び跳ねているのがMARC、真ん中の女性がKEIKO、右の色男が小室哲哉だ。  
大きなサイズで見る（全5件）  
封面由北条司绘制插画。左边蹦蹦跳跳的是MARC，中间的女性是KEIKO，右边的美男子是小室哲哉。  
查看大图(共5件)  
](https://natalie.mu/comic/gallery/news/37632/57242)

このイラストは、globeが2008年にシングルリリースする予定だったTM NETWORKのカバー「Get Wild」のジャケット用に描き下ろされていたもの。「Get Wild」は「シティーハンター」アニメ化（読売テレビ系）の際にエンディングテーマに起用され大ヒットしたこともあり、作曲者である小室哲哉が自ら「北条司先生に描き下ろしていただきたい」と発案。スタッフ経由でオファーしたところ北条はこれを快諾し、小室、KEIKO、MARCの3人を独特のタッチで描いた。  
这张插图是globe原本预定2008年发行单曲的TM NETWORK的封面“Get Wild”的封面。《Get Wild》在《城市猎人》动画化(读卖TV)时作为片尾曲而大受欢迎，作曲者小室哲哉亲自提议“请北条司老师来画”。经由工作人员提议，北条欣然接受，用独特的笔触描绘了小室、KEIKO、MARC 3人。  

シングルリリースが見送りになったことから発表されることのなかったこのイラストが、ベストアルバムのジャケットとして登場。北条はこのイラストについて「懐かしさを感じつつ、『シティーハンター』と『globe』の融合を面白がって描かせていただきました」と語っている。  
因单曲发行被搁置而没有被发表的该插图作为精选集的封面登场。关于这幅插画，北条说:“一边感到怀念，一边觉得《城市猎人》和《globe》的融合很有趣，所以就画了下来。”

「15YEARS -BEST HIT SELECTION-」はCD3枚組で、「Get Wild」を含む合計30曲を収録。DISC 3にはさらにCD-EXTRAトラックとして、「Get Wild」のビデオクリップが収められる。「15YEARS」にはこのほか、CD20枚＋DVD13枚に小室哲哉直筆サイン色紙とTシャツを同梱したコンプリートボックス「15YEARS -ANNIVERSARY BOX-」と、小室自身のセレクトによるベスト選曲のCD5枚+DVD2枚組「15YEARS -TK SELECTION-」も用意。  
《15YEARS -BEST HIT SELECTION-》共3张cd，收录了包括《Get Wild》在内的总计30首歌曲。DISC 3中还收录了作为CD-EXTRA音轨的“Get Wild”的视频剪辑。此外，《15years》还附赠了20张cd+13张DVD、13张小室哲哉亲笔签名彩纸和t恤的组合套装《15years -ANNIVERSARY BOX-”和小室自己挑选的精选曲的CD5张+DVD2张组“15years -TK SELECTION-”。  

avex公式YouTubeチャンネルではアルバム発売に先駆け、これまでのビデオクリップ18本と、17分を超える特別映像「globe 15YEARS Voice of KEIKO」が公開中だ。  
avex官方YouTube频道在专辑发售之前，公开了至今为止的18个视频剪辑和超过17分钟的特别影像“globe 15YEARS Voice of KEIKO”。




## 北条司 Comment

懐かしさを感じつつ、「シティーハンター」と「globe」の融合を面白がって描かせていただきました。  
「 Get Wild 」という曲のイメージから躍動的な絵を描こうと……  
それでいて3人姿が小さくならないよう横並びの構図にしました。  
最初に頭に浮かんだのは飛び跳ねているマークです（笑）。  
一边感到怀念，一边有趣地画了“城市猎人”和“globe”的融合。  
从“Get Wild”这首歌的形象中描绘出生动的画面……  
所以为了不让3个人的身姿变小，就采用了并排的构图。  
第一个浮现在脑海的是跳跃的标志(笑)。  


この記事の画像（全5件）  
这篇报道的图片(共5篇)  

*   [![北条司がイラストを描き下ろしたジャケット。左の元気に飛び跳ねているのがMARC、真ん中の女性がKEIKO、右の色男が小室哲哉だ。](https://ogre.natalie.mu/media/news/music/2010/0914/globe_AVCG70102.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/37632/57242 "北条司がイラストを描き下ろしたジャケット。左の元気に飛び跳ねているのがMARC、真ん中の女性がKEIKO、右の色男が小室哲哉だ。（封面由北条司绘制插画。左边蹦蹦跳跳的是MARC，中间的女性是KEIKO，右边的美男子是小室哲哉。）")
*   [![なお北条司は、10月25日に創刊する新雑誌・月刊コミックゼノン（徳間書店）にて「エンジェル・ハート 2ndシーズン」を連載スタート。同作は「シティーハンター」の登場人物が活躍する人気作で、1stシーズン完結から待望の再開となる。(C)北条司/NSP 2010](https://ogre.natalie.mu/media/news/comic/2010/0826/angel-heart.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/37632/55763 "なお北条司は、10月25日に創刊する新雑誌・月刊コミックゼノン（徳間書店）にて「エンジェル・ハート 2ndシーズン」を連載スタート。同作は「シティーハンター」の登場人物が活躍する人気作で、1stシーズン完結から待望の再開となる。(C)北条司/NSP 2010  
（北条司将在10月25日创刊的新杂志月刊Comic Zenon(德间书店)上开始连载《天使心2nd季》。该作品是《城市猎人》的登场人物活跃的人气作品，从第1季完结开始期待的再开。(C)北条司/NSP 2010）")
*   [![また10月25日に創刊する新雑誌・月刊コミックゼノンでは、北条の名作「キャッツ・アイ」を原作としたスピンアウト「キャッツ❤愛」もスタートする。シナリオは中目黒さくらが、作画は阿佐維シンが手がける。画像は同作のカット。](https://ogre.natalie.mu/media/news/comic/2010/0913/zenon002cats3hitomi.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/37632/57154 "また10月25日に創刊する新雑誌・月刊コミックゼノンでは、北条の名作「キャッツ・アイ」を原作としたスピンアウト「キャッツ❤愛」もスタートする。シナリオは中目黒さくらが、作画は阿佐維シンが手がける。画像は同作のカット。（10月25日创刊的新杂志月刊Comic Zenon中，改编自北条名作「Cat's❤Eye」的衍生作品「Cat's❤爱」也即将开始。剧本由中目黑樱执笔，作画由阿佐维信执笔。图像是该作品的剪影。）")
*   [![新雑誌・月刊コミックゼノンでスタートする「キャッツ❤愛」カット。](https://ogre.natalie.mu/media/news/comic/2010/0913/zenon002-cats2rui.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/37632/57155 "新雑誌・月刊コミックゼノンでスタートする「キャッツ❤愛」カット。（在新杂志月刊Comic Zenon中开始的「Cat's❤爱」片段。）")
*   [![新雑誌・月刊コミックゼノンでスタートする「キャッツ❤愛」カット。](https://ogre.natalie.mu/media/news/comic/2010/0913/zonon002-cats1ai.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/37632/57156 "新雑誌・月刊コミックゼノンでスタートする「キャッツ❤愛」カット。（在新杂志月刊Comic Zenon中开始的“猫咪大全爱”片段。）")

関連商品

[
![globe「15YEARS -BEST HIT SELECTION-（CD3枚組）」](https://images-fe.ssl-images-amazon.com/images/I/51PRaN4SFlL._SS70_.jpg)  
globe「15YEARS -BEST HIT SELECTION-（CD3枚組）」  
[CD3枚組] 2010年9月29日発売 / avex globe / AVCG-70102～4  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B003WMI578/nataliecomic-22)