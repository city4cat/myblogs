https://natalie.mu/comic/news/319839

# ジャンプキャラ勢揃いの「JUMP FORCE」ルフィや悟空が現実世界で佇む広告

2019年2月13日 [Comment](https://natalie.mu/comic/news/319839/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


週刊少年ジャンプ（集英社）の創刊50周年を記念したPlayStation 4 / Xbox One / Steam対応のゲーム「JUMP FORCE」の交通広告「それぞれの開戦前夜」が、本日2月13日から17日まで掲出されている。  
纪念周刊少年JUMP(集英社)创刊50周年的PlayStation4/Xbox One/Steam对应的游戏“JUMP FORCE”的交通广告“各自的开战前夜”于今天2月13日至17日揭幕。

[
![「JUMP FORCE」の交通広告。](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoukoku1.jpg?impolicy=hq&imwidth=730&imdensity=1)
「JUMP FORCE」の交通広告。  
大きなサイズで見る（全20件）  
“JUMP FORCE”的交通广告。  
大尺寸观看(共20件)。  
](https://natalie.mu/comic/gallery/news/319839/1106876)

[尾田栄一郎](https://natalie.mu/comic/artist/2368)「ONE PIECE」のルフィや、[鳥山明](https://natalie.mu/comic/artist/2266)「DRAGON BALL」の悟空、[岸本斉史](https://natalie.mu/comic/artist/3293)「NARUTO-ナルト-」のナルトといったキャラクターたちが、遊園地や中華街、東京タワーなど現実世界のロケーションを背景に佇む姿をデザインした「それぞれの開戦前夜」。全16種類のデザインが用意されており、東急田園都市線渋谷駅道玄坂ハッピーボード、山手線全駅で掲出されているほか、「JUMP FORCE」の公式サイトでも公開している。  
尾田荣一郎“ONE PIECE”中的路飞、鸟山明“DRAGON BALL”中的悟空、岸本齐史“火影忍者”中的鸣人等角色伫立在游乐园、中华街、东京塔等现实世界的背景中，并设计了“各自的开战前夜”。共准备了16种设计，除了在东急田园都市线涩谷驿道玄坂Happy board、山手线全站揭牌外，还在“JUMP FORCE”的官方网站上公开。

「JUMP FORCE」は「ジャンプヒーローたちが現実の世界で戦ったら」というコンセプトのもと、ニューヨークやマッターホルンなど、実在の場所でキャラクターがバトルを繰り広げる対戦アクションゲーム。明日2月14日に発売される。  
“JUMP FORCE”是以“如果Jump英雄们在现实世界中战斗”为理念，在New York、Matterhorn等真实存在的地方展开角色战斗的对战动作游戏。将于明天2月14日发售。  

この記事の画像（全20件）  
这篇报道的图片(共20张)  

[![「JUMP FORCE」の交通広告。](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoukoku1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106876 "「JUMP FORCE」の交通広告。")
[![「JUMP FORCE」の交通広告（「ONE PIECE」モンキー・D・ルフィ / 遊園地）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106877 "「JUMP FORCE」の交通広告（「ONE PIECE」モンキー・D・ルフィ / 遊園地）")
[![「JUMP FORCE」の交通広告（「ドラゴンボール」孫悟空 / 中華街）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106878 "「JUMP FORCE」の交通広告（「ドラゴンボール」孫悟空 / 中華街）")
[![「JUMP FORCE」の交通広告（「NARUTO-ナルト-」うずまきナルト / 東京タワー）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106879 "「JUMP FORCE」の交通広告（「NARUTO-ナルト-」うずまきナルト / 東京タワー）")
[![「JUMP FORCE」の交通広告（「HUNTER×HUNTER」ゴン / ボクシングジム）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106880 "「JUMP FORCE」の交通広告（「HUNTER×HUNTER」ゴン / ボクシングジム）")
[![「JUMP FORCE」の交通広告（「ブラッククローバー」アスタ / 横浜）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu5.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106881 "「JUMP FORCE」の交通広告（「ブラッククローバー」アスタ / 横浜）")
[![「JUMP FORCE」の交通広告（「僕のヒーローアカデミア」緑谷出久 / 校庭）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu6.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106882 "「JUMP FORCE」の交通広告（「僕のヒーローアカデミア」緑谷出久 / 校庭）")
[![「JUMP FORCE」の交通広告（「BLEACH」黑崎一護 / 渋谷スクランブル交差点）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu7.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106883 "「JUMP FORCE」の交通広告（「BLEACH」黑崎一護 / 渋谷スクランブル交差点）")
[![「JUMP FORCE」の交通広告（「遊☆戯☆王」武藤遊戯 / みなとみらいに停泊する豪華客船）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu8.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106884 "「JUMP FORCE」の交通広告（「遊☆戯☆王」武藤遊戯 / みなとみらいに停泊する豪華客船）")
[![「JUMP FORCE」の交通広告（「幽☆遊☆白書」浦飯幽助 / コンビニの前）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu9.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106885 "「JUMP FORCE」の交通広告（「幽☆遊☆白書」浦飯幽助 / コンビニの前）")
[![「JUMP FORCE」の交通広告（「ジョジョの奇妙な冒険」空条承太郎 / 富士山の麓）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106886 "「JUMP FORCE」の交通広告（「ジョジョの奇妙な冒険」空条承太郎 / 富士山の麓）")
[![「JUMP FORCE」の交通広告（「DRAGON QUEST -ダイの大冒険-」ダイ / 日原鍾乳洞）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106887 "「JUMP FORCE」の交通広告（「DRAGON QUEST -ダイの大冒険-」ダイ / 日原鍾乳洞）")
[![「JUMP FORCE」の交通広告（「『るろうに剣心 -明治剣客浪漫譚-」緋村剣心 / 京都三条大橋）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106888 "「JUMP FORCE」の交通広告（「『るろうに剣心 -明治剣客浪漫譚-」緋村剣心 / 京都三条大橋）")
[![「JUMP FORCE」の交通広告（「DEATH NOTE」夜神月・リューク / 六本木）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu13.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106889 "「JUMP FORCE」の交通広告（「DEATH NOTE」夜神月・リューク / 六本木）")
[![「JUMP FORCE」の交通広告（「シティーハンター」冴羽リョウ / 新宿）※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu14.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106890 "「JUMP FORCE」の交通広告（「シティーハンター」冴羽リョウ / 新宿）※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。")
[![「JUMP FORCE」の交通広告（「聖闘士星矢」星矢 / プラネタリウム）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu15.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106891 "「JUMP FORCE」の交通広告（「聖闘士星矢」星矢 / プラネタリウム）")
[![「JUMP FORCE」の交通広告（「北斗の拳」ケンシロウ / 浅草雷門）](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoutsu16.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106892 "「JUMP FORCE」の交通広告（「北斗の拳」ケンシロウ / 浅草雷門）")
[![「JUMP FORCE」の交通広告。](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoukoku3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106893 "「JUMP FORCE」の交通広告。")
[![「JUMP FORCE」の交通広告。](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforcekoukoku6.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106894 "「JUMP FORCE」の交通広告。")
[![「JUMP FORCE」PlayStation 4版パッケージ。](https://ogre.natalie.mu/media/news/comic/2019/0213/jumpforce.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319839/1106895 "「JUMP FORCE」PlayStation 4版パッケージ。")

(c)JUMP 50th Anniversary (c)BANDAI NAMCO Entertainment Inc

LINK

[INFORMATION｜PS4/Xbox One「JUMP FORCE（ジャンプフォース）」 | Bandai Namco Entertainment公式Site](https://ce-f-jump.bn-ent.net/ZrRWemZW/?jf_information=promo)