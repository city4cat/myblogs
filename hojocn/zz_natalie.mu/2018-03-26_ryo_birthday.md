https://natalie.mu/comic/news/275285

# 今日は冴羽リョウの誕生日！記念ぴあに、全もっこり＆ハンマーコレクション掲載

2018年3月26日  [Comment](https://natalie.mu/comic/news/275285/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


本日3月26日は[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」に登場する冴羽リョウの誕生日。これを記念した「シティーハンター 冴羽リョウ×ぴあ」が刊行された。  
今天3月26日是北条司《城市猎人》中登场的冴羽獠的生日。为了纪念这个，发行了《城市猎人冴羽獠×Pia》。

[
![「シティーハンター 冴羽リョウ×ぴあ」](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia.jpg?imwidth=468&imdensity=1)
「シティーハンター 冴羽リョウ×ぴあ」  
大きなサイズで見る（全9件）  
《城市猎人冴羽獠×Pia》  
大尺寸看(全9件)
](https://natalie.mu/comic/gallery/news/275285/894626)

「シティーハンター 冴羽リョウ×ぴあ」では、北条や新作アニメーション映画「劇場版シティーハンター（仮題）」のこだま兼嗣総監督のコメント、北条からリョウへのメッセージ、イラストコレクションなどを掲載。また名シーンの数々をテーマ別に紹介しており「全もっこりコレクション」「全ハンマーコレクション」や名言集など読み応えのある記事が満載だ。北条による直筆サインイラストが当選者の名前入りでプレゼントされる抽選企画も開催。  
“城市猎人冴羽獠×Pia”收录了北条和新动画电影《城市猎人电影（暂定）》的总导演北条和儿玉兼嗣的评论、北条给獠的message以及Illust Collection。 此外，还按主题介绍了一些名场面，还有许多值得一读的文章，例如“全部Mokkori Collection”，“全部Hammer Collection”和语录集。 还将有一个抽奖项目，其中北条的亲笔签名插图将带有获奖者的名字。

なおTSUTAYAでは、北条が描き下ろした「シティーハンター 冴羽リョウ×ぴあ」表紙イラストのA4クリアファイルを購入者に配布している。特典は数に限りがあるため、なくなり次第終了。詳細は購入予定の店舗で確認を。
TSUTAYA还向购买者分发了北条所绘的《城市猎人冴羽獠×Pia》封面插图的A4 Clear File。特典数量有限，售完即止。详情请到预定购买的店铺确认。  

この記事の画像（全9件）  
这篇报道的图片(共9篇)  

[![「シティーハンター 冴羽リョウ×ぴあ」](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894626 "「シティーハンター 冴羽リョウ×ぴあ」")
[![「シティーハンター 冴羽リョウ×ぴあ」より。](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894627 "「シティーハンター 冴羽リョウ×ぴあ」より。")
[![「シティーハンター 冴羽リョウ×ぴあ」より。](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894628 "「シティーハンター 冴羽リョウ×ぴあ」より。")
[![「シティーハンター 冴羽リョウ×ぴあ」より。](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894629 "「シティーハンター 冴羽リョウ×ぴあ」より。")
[![「シティーハンター 冴羽リョウ×ぴあ」より。](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894630 "「シティーハンター 冴羽リョウ×ぴあ」より。")
[![「シティーハンター 冴羽リョウ×ぴあ」より。](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894631 "「シティーハンター 冴羽リョウ×ぴあ」より。")
[![「シティーハンター 冴羽リョウ×ぴあ」より。](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894632 "「シティーハンター 冴羽リョウ×ぴあ」より。")
[![「シティーハンター 冴羽リョウ×ぴあ」より。](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894633 "「シティーハンター 冴羽リョウ×ぴあ」より。")
[![「シティーハンター 冴羽リョウ×ぴあ」TSUTAYA特典のクリアファイル。](https://ogre.natalie.mu/media/news/comic/2018/0326/saebapia08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/275285/894634 "「シティーハンター 冴羽リョウ×ぴあ」TSUTAYA特典のクリアファイル。")

※リョウの漢字はけものへんに「僚」のつくり
(c)北条司／NSP 1985 版権許諾証AD-308


LINK

[アニメ「劇場版シティーハンター」公式サイト](http://cityhunter-movie.com/)

関連商品  

[
![CityHunter 冴羽獠pair: pair Mook](https://images-fe.ssl-images-amazon.com/images/I/61gL%2BEhonnL._SS70_.jpg)  
CityHunter 冴羽獠pair: pair Mook  
[Mook] 2018年3月26日発売 / pair / 978-4835634227
Amazon.co.jp
](https://www.amazon.co.jp/シティーハンター-冴羽リョウぴあ-ぴあムック/dp/4835634225/)  