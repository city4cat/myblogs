https://natalie.mu/comic/news/318341

# 「CityHunter」読み放題がブックパスで、映画鑑賞券のプレゼントも

2019年2月1日  [Comment](https://natalie.mu/comic/news/318341/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版CityHunter <新宿Private Eyes>](https://natalie.mu/eiga/film/175572)」が2月8日より全国公開。これに合わせ原作マンガ全32巻の読み放題キャンペーンが、本日2月1日から3月4日にかけて電子書籍サービス・ブックパスにて展開される。  
北条司原作的动画电影《剧场版城市猎人<新宿Private Eyes>》将于2月8日在日本全国上映。与此同时，32卷原作漫画的免费阅读活动也将于2月1日至3月4日在电子书服务bookpass上展开。  

[
![「劇場版CityHunter <新宿Private Eyes>」Visual (c)北条司/NSP・「2019 劇場版CityHunter」製作委員会](https://ogre.natalie.mu/media/news/comic/2019/0201/ch_bookpass01.jpg?imwidth=468&imdensity=1)
「劇場版CityHunter <新宿Private Eyes>」Visual (c)北条司/NSP・「2019 劇場版CityHunter」製作委員会  
大きなサイズで見る（全2件）  
“剧场版城市猎人”《新宿私人之眼》视觉(c)北条司/NSP·“2019剧场版城市猎人”制作委员会  
大尺寸观看(全2件)  
](https://natalie.mu/comic/gallery/news/318341/1100074)

対象となるのはブックパスの読み放題プラン総合コースの加入者。またキャンペーンの実施に合わせて、「劇場版CityHunter」の鑑賞チケット、冴羽リョウ・槇村香のイラストがプリントされた原作マンガの膝掛けブランケットのプレゼントや、「今日からCITY HUNTER」「CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常」のスピンオフ2作品の単行本を購入した人に購入金額の30%をブックパスで使用できるコインで還元する施策も開催されている。読み放題の詳細、プレゼントへの応募方法はブックパス特集ページにて確認を。  
该活动向BookPass的无限量阅读计划综合课程的用户开放。另外，配合宣传活动的实施，还将赠送《剧场版城市猎人》的观赏票、印有冴羽獠和槙村香插画的原作漫画膝盖毯的礼物，以及《从今日起CITYHUNTER》、《CITY HUNTER外传 伊集院隼人氏的平静日常》等2部衍生作品的单行本，对购买这两部衍生作品的人给予购买价30%的硬币回馈，这些硬币可以在Book Pass上使用。 关于无限制阅读和如何申请礼物的细节，请查看图书通特别页面。

なおコミックナタリーでは20年ぶりの新作となる「劇場版CityHunter <新宿Private Eyes>」の特集を展開。冴羽リョウ役の神谷明と原作者の北条司にインタビューし、約20年ぶりの冴羽リョウとの再会や、映画の見どころなどについて語ってもらった。  
另外，Comic Natalie也推出了时隔20年的新作《剧场版城市猎人<新宿Private Eyes>》的特集。采访了冴羽獠的扮演者神谷明和原作者北条司，请他们谈了约20年后与冴羽獠的再会，以及电影的看点等。  



この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![「劇場版CityHunter <新宿Private Eyes>」Visual (c)北条司/NSP・「2019 劇場版CityHunter」製作委員会](https://ogre.natalie.mu/media/news/comic/2019/0201/ch_bookpass01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/318341/1100074 "「劇場版CityHunter <新宿Private Eyes>」Visual (c)北条司/NSP・「2019 劇場版CityHunter」製作委員会")
[![「CityHunter」全巻のImage。](https://ogre.natalie.mu/media/news/comic/2018/0801/cityhunter1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/318341/978572 "「CityHunter」全巻のImage。")

※リョウの漢字はけものへんに「僚」のつくり


LINK

[CityHunter | ブックパス – auの電子書籍ストア –](https://bookpass.auone.jp/info/cityhuntermovie/?aid=lbs_AS0_twit_bookpass#link2)  
[ブックパス@au以外も使える！ (@aubookpass) | Twitter](https://twitter.com/aubookpass)  