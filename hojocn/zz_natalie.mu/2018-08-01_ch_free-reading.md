https://natalie.mu/comic/news/293592

# 「シティーハンター」読み放題キャンペーン、Tシャツなどグッズプレゼントも
《城市猎人》免费阅读活动，还赠送T恤等周边礼品


2018年8月1日  [Comment](https://natalie.mu/comic/news/293592/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「劇場版シティーハンター」が2019年初春に公開。これに合わせて原作マンガ全32巻の読み放題キャンペーンが、本日8月1日から31日にかけて電子書籍サービス「ブックパス」にて展開される。  
由北条司原作改编的动画电影“剧场版城市猎人”将于2019年初春上映。与此相配合，原作漫画全部32卷的无限阅读宣传活动，将于今天8月1日至31日在电子书服务“bookpass”上展开。

[
![「シティーハンター」全巻のイメージ。](https://ogre.natalie.mu/media/news/comic/2018/0801/cityhunter1.jpg?imwidth=468&imdensity=1)
「シティーハンター」全巻のイメージ。  
大きなサイズで見る（全4件）  
《城市猎人》全卷的形象。  
查看大图(共4件)  
](https://natalie.mu/comic/gallery/news/293592/978572)

また読み放題キャンペーンの実施に合わせて、auスマートパスプレミアム会員に「冴羽リョウTシャツ」「シティーハンタークリアファイル」が当たるプレゼントキャンペーンも開催。当選者数は各10名になっている。読み放題の詳細、プレゼントへの応募方法はブックパス特集ページにて確認を。  
另外，配合免费阅读活动的实施，au Smart Pass Premium会员还将获得“冴羽獠t恤”和“City Hunter clear file”的奖品。中奖人数各为十人。免费阅读的详细内容和礼物的应募方法请在bookpass特集页上确认。

この記事の画像（全4件）  
这篇报道的图片(共4篇)  

[![「シティーハンター」全巻のイメージ。](https://ogre.natalie.mu/media/news/comic/2018/0801/cityhunter1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/293592/978572 "「シティーハンター」全巻のイメージ。")
[![「劇場版シティーハンター」の第1弾ビジュアル。](https://ogre.natalie.mu/media/news/comic/2018/0801/cityhunter2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/293592/978573 "「劇場版シティーハンター」の第1弾ビジュアル。")
[![「冴羽リョウTシャツ」](https://ogre.natalie.mu/media/news/comic/2018/0801/cityhunter3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/293592/978574 "「冴羽リョウTシャツ」")
[![「シティーハンタークリアファイル」](https://ogre.natalie.mu/media/news/comic/2018/0801/cityhunter4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/293592/978575 "「シティーハンタークリアファイル」")


※リョウの漢字はけものへんに「僚」のつくり  
※獠字的汉字是在反犬旁的边上写“僚”  

LINK

[シティーハンター | bookpass – auの電子書籍Store –](https://bookpass.auone.jp/info/cityhunter/?aid=lbs_AW0_city_press)  