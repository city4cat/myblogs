https://natalie.mu/comic/news/335565

# モンキー・パンチを偲ぶ会、次元役・小林清志も心境述べる「俺より先に逝きやがって」

2019年6月14日  [Comment](https://natalie.mu/comic/news/335565/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)



去る4月11日に81歳で永眠した[モンキー・パンチ](https://natalie.mu/comic/artist/1749)をしのぶ「モンキー・パンチ先生を偲ぶ会」が、本日6月14日に東京・青山葬儀所にて行われた。  
为了缅怀4月11日以81岁高龄长眠的Monkey Punch, 6月14日在东京青山葬礼所举行了“Monkey Punch老师追思会”。

[
![モンキー・パンチの花祭壇。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SBB7160.jpg?impolicy=hq&imwidth=730&imdensity=1)  
モンキー・パンチの花祭壇。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会  
大きなサイズで見る（全60件）  
Monkey Punch祭坛。(c)“Monkey Punch老师追思会”执行委员会  
查看大图(共60件)  
](https://natalie.mu/comic/gallery/news/335565/1179960)

[
![「モンキー・パンチ先生を偲ぶ会」の入り口。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0009.jpg?imwidth=468&imdensity=1)  
「モンキー・パンチ先生を偲ぶ会」の入り口。［拡大］  
“Monkey Punch老师追思会”入口。[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179486)

[
![式場前に展示されたパネル。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0023.jpg?imwidth=468&imdensity=1)
式場前に展示されたパネル。［拡大］  
会场前展示的展板。[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179494)

晴天に恵まれた青山葬儀所。会場入り口にはR-33ナンバーのフィアットを模したバルーンアートが、記念写真の撮影スポットとして設置された。会場内にはモンキー・パンチが手がけたイラスト、また仕事風景やプライベートの写真が並ぶ。  
青山葬仪所天气晴朗。会场入口设置了R-33编号的仿菲亚特气球艺术，作为纪念照的拍摄点。会场内陈列着Monkey Punch亲手绘制的插画，以及工作场景和私人照片。  

花祭壇には、Lupin三世のイメージカラーが赤であることにちなみ、たくさんの赤いバラが。赤が引き立つようにと、バックには夜の街並みがセレクトされた。バラの中には「Lupin三世」のキャラクターパネルが配置され、祭壇の中で今まさにLupinたちの追走劇が展開されているかのよう。その様子を見守るように、モンキー・パンチの遺影が飾られた。   
祭坛上摆放着很多红色的玫瑰，因为鲁邦三世的象征色是红色的。为了突出红色，背景选择了夜晚的街道。玫瑰中摆放着《鲁邦三世》中的角色面板，就像现在正在祭坛中上演鲁邦等人的追逐剧一样。就像在观看这一幕一样，Monkey Punch的遗像被挂在了墙上。  

[
![栗田貫一 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8124.jpg?imwidth=468&imdensity=1)  
栗田貫一 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会［拡大］  
栗田贯一(c)“Monkey Punch老师追思会”执行委员会[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179954)

式典では黙祷と、モンキー・パンチの経歴紹介に続き、アニメでLupin三世役を演じる[栗田貫一](https://natalie.mu/comic/artist/6789)が弔辞を捧げる。弔辞という大役を一度は辞退したという栗田は、「24年前、先生から大切なLupin三世をお任せいただいたときも、『（自分に）Lupin三世なんてできるわけない」とお断りしたことを思い出します」と回顧。続けて「Lupin三世という大役を山田康雄さんから引き継いだときは、さまざまな方面からご批判もありました。そんなとき、先生は『なんも心配しなくていいよ』『そのままでいいんですよ』とおっしゃってくださったことを思い出します。先生に守られてきたから、今日まで続けられてきたんだと思います」と述べて、「先生の、すべてを受け入れる懐の大きさは、Lupin三世そのものでないかと思います」と話した。栗田はまた、モンキー・パンチの故郷である北海道・浜中町で行われた「Lupin三世」のイベントで、モンキー・パンチの幼なじみらと食事をした際のエピソードを紹介。「『先生の初恋の女性が、峰不二子のモデルだったんだよ。おっぱいが大きくてね、きれいな人なんだよ』って。（僕が）『先生、その人に会ってみたい』と言ったら、先生は『会わないほうがいいよ』とおっしゃっていましたね」と懐かしげに語る。最後には「これからもLupin三世を見守っていてください。モンキー・パンチ先生、本当にお世話になりました。ありがとうございました」と結んだ。  
仪式上，继默哀和介绍Monkey Punch的经历之后，在动画中饰演鲁邦三世的栗田贯一致悼词。曾一度谢绝致悼词这一重要任务的栗田回忆说:“我想起了24年前，老师把重要的鲁邦三世交给我的时候，我也拒绝说‘(我)不可能做到鲁邦三世’。”他接着说:“从山田康雄手中接过《鲁邦三世》这一重任时，受到了来自各个方面的批评。这时，我想起老师对我说过‘什么都不用担心’‘这样就好了’。因为有老师的保护，我才能够坚持到今天”，“老师包容一切的胸怀，不正是《鲁邦三世》本身吗?”栗田还介绍了在Monkey Punch的故乡北海道滨中町举行的《鲁邦三世》活动中，与Monkey Punch的发小们一起吃饭时的趣事。“‘老师的初恋女友就是峰不二子的原型，胸部很大，很漂亮’。(我)说‘老师，我想见见那个人’，老师说‘最好不要见’”。最后，他说:“今后请继续关注鲁邦三世。Monkey Punch老师，承蒙您照顾。”  

[
![里中満智子 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8143.jpg?imwidth=468&imdensity=1)  
里中満智子 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会［拡大］  
里中满智子(c)“Monkey Punch老师追思会”执行委员会[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179955)

続いて、日本漫画家協会理事長・[里中満智子](https://natalie.mu/comic/artist/4932)が弔辞を読み上げる。里中はモンキー・パンチと初めて出会ったときのことを「作品から受ける印象で、Lupin三世のような“ちょい悪ラテン系”だと思っていたのですが、実際はとても穏やかでにこやかで、一切のバリアもトゲもないやわらかい方で驚きました」とコメント。里中はさらに、モンキー・パンチが「もっと勉強したい」「基礎知識を学び直す」と66歳にして大学院に入学したこと、新しいデジタル機器が発売されるとすぐに入手し使いこなしていたことなど、モンキー・パンチが晩年まで勤勉だったことを紹介する。また印象に残っている出来事として、「若い人たちに創作のコツを伝えるときに、雲をヒントに語っておられたのを聞いて、感動しました。『雲は刻々と形を変える。雲をキャラクターだと思って見ていると、キャラクターの動きが見えてくる』とおっしゃって、皆の前で雲の絵を描き、それをなぞって『Lupin』の不二子ちゃんを描かれたのです。先生の描くキャラクターがずば抜けて躍動的なのは、これなのか！と納得がいきました」とエピソードを紹介。雲の上でもチャレンジを続けているであろうモンキー・パンチに、「先生、またいつかお会いしましょう。そのときに、今描いていらっしゃる作品を見せてくれるのを楽しみにしています」と呼びかけた。  
随后，日本漫画家协会理事长里中满智子宣读悼词。里中说第一次见到Monkey Punch时，“从作品中得到的印象来看，我认为他是鲁邦三世那样的‘稍微邪恶的拉丁裔人’，但实际上他非常温和、和蔼，没有任何屏障和刺。我很惊讶”。里中进一步介绍了Monkey Punch如何保持勤奋，直到晚年，例如他在66岁时报名参加了研究生课程，以 "更多的学习 "和 "重新学习基本知识"，以及他在新的数字设备上市后立即购置和使用。另外，关于印象深刻的事情，她说:“在向年轻人传授创作诀窍时，他以云为灵感，我听了很感动。云每时每刻都在改变形状。把云当成角色来看，就能看到角色的动作。”然后在大家面前画了一幅云的画，然后照着画了《鲁邦》中的不二子。我相信，这就是你所画的人物之所以有如此突出的活力的原因！对在云上也继续挑战的Monkey Punch，她说:“老师，什么时候再见面吧。到时候，我很期待给你看我现在画的作品。”（译注：待校对）  


![石橋保彦氏 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8164.jpg?imwidth=468&imdensity=1)
石橋保彦氏 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会［拡大］  
石桥保彦(c)“Monkey Punch老师追思会”执行委员会[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179956)

3人目には、株式会社平和の相談役および公益財団法人ジュニアゴルファー育成財団理事長の石橋保彦氏が、友人代表としてお別れの言葉を述べる。石橋氏は初めてモンキー・パンチと出会ったとき、「処女作からのファンです！」と伝えた石橋氏にモンキー・パンチが「君はLupinに似てるね」と言ってくれたこと、モンキー・パンチの家族旅行に同行したときの思い出などを語った。  
第三位是株式会社和平的顾问及公益财团法人青少年高尔夫选手培养财团理事长石桥保彦，他将作为友人代表发表告别词。石桥先生第一次遇到Monkey Punch的时候，"从他的第一部小说开始，我就是他的粉丝！" 他告诉石桥先生，猴拳对他说："你看起来像鲁邦"，还回忆了和他一起去Monkey Punch一家旅行时的事情。  

[
![加藤州平氏 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8253.jpg?imwidth=468&imdensity=1)
加藤州平氏 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会［拡大］  
加藤州平(c)“Monkey Punch老师追思会”执行委员会[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179958)

式典の最後にはモンキー・パンチの次男で、この日の喪主を務めた加藤州平氏が挨拶に立つ。加藤氏は「父はとても優しく家族思いの人でした。亡くなる前日が最後の会話になったのですが、支え続けた母に感謝の言葉を伝え、家族が前を向いて人生を歩んでいけるよう言葉を遺してくれました」と亡くなる直前のモンキー・パンチの様子を紹介。また「とても優しく真面目で、努力家の父ですが、生前自分の作品について『モンキー・パンチの作品には教えがない』と言っておりました」と明かし、「モンキー・パンチは自分の作品に道徳的なメッセージを入れず、誰もが気軽に楽しめる、純度100パーセントのエンターテインメントを追求していたのだと思います」と考えを示す。そして「『Lupin三世』は父が生み出し、ここにお集まりの皆様、多くのスタッフの皆様、ファンの皆様に育てられた作品です。父の追求した誰もが気軽に楽しめる作品作りに、携わっていただいた多くの皆様に、父に代わってお礼申し上げます」と感謝を述べた。  
在仪式的最后，由Monkey Punch的次子、当天的丧主加藤州平致词。加藤氏说“父亲是一个很温柔很顾家的人。去世的前一天是最后一次对话，他向一直支持自己的母亲表达了感谢之情，并留下了让家人在人生道路上勇往直前的话语。”此外，他还表示“父亲是一位非常温柔、认真、努力的人，但生前他对自己的作品说过‘Monkey Punch的作品中没有教义’”，“我认为是为了追求能让人轻松愉快地享受、纯度百分之百的娱乐”。他还说:“《鲁邦三世》是我父亲创作出来的作品，是在在座的各位、众多工作人员、粉丝们的共同培育下的作品。对于参与父亲所追求的谁都能轻松享受的作品制作的许多人，我代表父亲表示感谢。”

[
![浅野忠信](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0042.jpg?imwidth=468&imdensity=1)
浅野忠信［拡大］
](https://natalie.mu/comic/gallery/news/335565/1179796)

式典の後には囲み取材の場が設けられ、一部の弔問客が取材に応じた。2014年公開の実写映画「Lupin三世」で銭形幸一役を演じた[浅野忠信](https://natalie.mu/comic/artist/11576)は、役柄をイメージした衣装で会場入り。その理由を「こういう格好で来てはいけないんだろうと思っていたんですけど、やっぱり先生に会うときはできるだけ銭形警部を感じてもらえたら」と明かす。また映画の撮影当時を振り返り、「正直に言うと、最初にオファーをもらったときは次元だと思ったんです。とっつぁんだと思ってなかったので、自分の中で自信を持ちきれていない部分があったんですけど、先生に現場でお会いしたとき『とてもいいよ』と言ってくれて。本当に優しい笑顔とお言葉をいただいたので、そのあとは本当に自信を持って銭形警部を演じることができました。そのことを思い出して、改めて先生に感謝しています」と語った。  
仪式结束后还安排了采访环节，部分吊唁客接受了采访。在2014年上映的真人电影《鲁邦三世》中饰演钱形幸一的浅野忠信身着与剧中角色相呼应的服装进入会场。理由是“虽然觉得不能这样来，不过还是希望见到老师的时候能尽量感觉到钱形警部的感觉”。他还回忆起拍摄电影的时候说:“说实话，最初接到邀请的时候，我觉得是次元。因为我没觉得是突然出现的，所以有自己没有自信的部分，但是在现场见到老师的时候他说‘非常好’。因为得到了真的温柔的笑容和话语，之后能够有自信地出演钱形警部。回想起这一点，再次感谢老师”。

[
![献花を捧げる小栗旬。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8331.jpg?imwidth=468&imdensity=1)  
献花を捧げる小栗旬。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会［拡大］  
献花的小栗旬。(c)“Monkey Punch老师追思会”执行委员会[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179959)

[
![小栗旬](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0090.jpg?imwidth=468&imdensity=1)
小栗旬［拡大］
](https://natalie.mu/comic/gallery/news/335565/1179800)

同じく実写映画でLupin三世役を演じた[小栗旬](https://natalie.mu/comic/artist/27613)は「残念です。少し体調を崩されているとは聞いていたんですけど、こんな形で会えぬままになるとは思っていなかったので……」と話し、「映画の撮影現場に遊びに来ていただいたとき、『おお、Lupinがいるじゃないか！』なんて言いながら入ってきてくれたりして……。そういう一言一言が自分にとって大切な宝物になっていますし、勇気と力をもらえました」と頷く。「また何かしらの形で先生とお会いしたかった」と述べて、「モンキー先生が作った大切なキャラクターを、一度でもやらせていただけたので、本当にそれは誇りに思っています」と挨拶した。  
同样在真人电影中饰演鲁邦三世的小栗旬说:“很遗憾，虽然听说身体有点不好，但是没想到会以这样的形式见不到面……”，“当来电影的拍摄现场玩的时候，一边说着‘噢，不是有鲁邦吗!’一边进来……这样的每一句话对我来说都是很重要的宝物，让我得到了勇气和力量”。他说:“我想再以某种形式和老师见面”，“Monkey老师创作的重要角色，能让我演一次，我真的很自豪。”

[
![用意してきた言葉を読み上げる小林清志。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0167.jpg?imwidth=468&imdensity=1)
用意してきた言葉を読み上げる小林清志。［拡大］  
小林清志念着事先准备好的话语。[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179805)

アニメで次元大介役を演じる[小林清志](https://natalie.mu/comic/artist/95607)は、取材陣に「お別れできましたか？」と聞かれると、「お別れ……できたと思うけど、まだまだ信じられないね」と返答。次元大介がどんな存在という質問には「またその質問か」と笑いながら、「分身みたいなもんだな。小林清志か次元大介か、そんな感じになってきたねえ。ほかの人たちが許してくれるかわかんないけど、そう思ってるよ」と述べる。次元役を長年演じ続けていることについては、「結局俺だけ残ってるんだよな。俺もがんばんなきゃな。まだまだ大丈夫だから。皆さんさえよろしければもう少しやらせてもらうから、期待しといてくれよな」と力強くコメント。また「書いてきた言葉があるので読み上げたい」とスマートフォンを取り出し、「最初に会ったときは若かったな。先生も俺も若かった。今2019年だよ。ずいぶん長いことやってきたな。それなのに俺はまだ生きて動いてんだからしぶといもんだ。先生からはいろんなことを教わりました。男の色気、男のかわいらしさ、哀愁、子供っぽさ……。なんとかわかるんだが、表現するのは難しいぞ。それをやってきたんだから大変だ、俺も。ともかく、俺より若いくせに先に逝きやがって。しょうがない先生だ。ダメだよ。ダメだよ先生。残念だ、残念だよ。ご冥福をお祈りします」と、呼びかけるように読み上げた。最後には取材陣に「次元大介と出会えて、幸せに決まってる。こう言っちゃおこがましいけど、もう次元とは離れらんないよ。次元が小林清志であり、小林清志が次元。大きなことを言っちゃったけど、そんな気持ちでやっています」と語った。  
在动画中饰演次元大介的小林清志被记者们询问“你能够说再见了吗？”，他回答说:“我想我能够说再见了，但还是不敢相信。”对于“次元大介是怎样的存在”的提问，他笑着说“又是这个问题？”，“就像分身一样呢。小林清志还是次元大介呢?虽然不知道其他人会不会原谅我，但我是这么想的。”对于长年出演次元角色，他说:“结果就剩下我一个人了，我也要加油了，因为还没有问题。如果大家觉得可以的话，我还会再演一些，请大家期待吧”。他又拿出手机说:“我有写下来的话，想把它念出来。”“第一次见到你的时候你很年轻。老师和我都很年轻。现在是2019年了。这么多年了。但是我还活着，还在动，所以很顽强。老师教了我很多东西。男人的魅力，男人的可爱、哀愁、孩子气……我多少能理解，但是很难表现出来。我做了这么多事情，也很辛苦。总之，明明比我年轻却先去世了，真是个没办法的老师。不行啊，不行啊老师。真遗憾，真遗憾。祈祷冥福。”最后他对记者们说:“能和次元大介相遇一定很幸福。虽然这么说有点可笑，但是我已经不能离开次元了。次元就是小林清志，小林清志就是次元。虽然说了很大的话，但是以那样的心情做着”。  

[
![懇親会で振舞われた「名物・ミートボールスパゲッティ」。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0062.jpg?imwidth=468&imdensity=1)  
懇親会で振舞われた「名物・ミートボールスパゲッティ」。［拡大］  
在联谊会上展出的“名产肉丸意大利面”。[扩大]  
](https://natalie.mu/comic/gallery/news/335565/1179515)

モンキー・パンチをしのびながら、心が温かくなるような食事をという主催者のはからいで、関係者式典の懇親会では「名物・ミートボールスパゲッティ」「Lupin寿司＆拳銃いか握り」といった「Lupin三世」のキャラクターをモチーフにした特別料理が振る舞われた。また関係者には返礼品として、原作画クッキーの詰め合わせが、Lupinの黄色いネクタイでラッピングされ渡されている。  
组织者希望用一顿温暖人心的饭菜来纪念Monkey Punch，因此在招待会上为参与的人提供了基于鲁邦三世这个角色的特别菜肴，包括“名物·肉丸意大利面”、“鲁邦寿司&手枪墨鱼手握”等《鲁邦三世》中的角色。以塔为主题的特色菜上桌了。另外，作为回礼，相关人员还收到了原作曲奇饼干的礼盒，包装上是鲁邦的黄色领带。  

なお会場では本日18時まで、一般のファンからの献花を受付中。一般弔問客への返礼品には、モンキー・パンチのイラストがあしらわれたポストカードが用意された。  
会场到今天18点为止，接受普通粉丝的献花。作为给一般吊唁客的回礼，准备了印有Monkey Punch图案的明信片。  

この記事の画像（全60件）  
这篇报道的图片(共60张)  

[![モンキー・パンチの花祭壇。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SBB7160.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179960 "モンキー・パンチの花祭壇。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![青山葬儀所の入り口。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179485 "青山葬儀所の入り口。")
[![「モンキー・パンチ先生を偲ぶ会」の入り口。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0009.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179486 "「モンキー・パンチ先生を偲ぶ会」の入り口。")
[![栗田貫一 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8124.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179954 "栗田貫一 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![里中満智子 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8143.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179955 "里中満智子 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![石橋保彦氏 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8164.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179956 "石橋保彦氏 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![左から栗田貫一、里中満智子、石橋保彦氏。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8236.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179957 "左から栗田貫一、里中満智子、石橋保彦氏。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![加藤州平氏 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8253.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179958 "加藤州平氏 (c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![献花を捧げる小栗旬。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SAA8331.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179959 "献花を捧げる小栗旬。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![式典の様子。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SBB7375.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179961 "式典の様子。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![式典の様子。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SBB7381.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179962 "式典の様子。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![式典の様子。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会](https://ogre.natalie.mu/media/news/comic/2019/0614/mp_SBB7411.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179963 "式典の様子。(c)「モンキー・パンチ先生を偲ぶ会」実行委員会")
[![式場前の庭園。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0012.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179487 "式場前の庭園。")
[![式場前に飾られた「Lupin三世」のイラスト。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0020.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179488 "式場前に飾られた「Lupin三世」のイラスト。")
[![式場前に飾られた「Lupin三世」のイラスト。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0021.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179489 "式場前に飾られた「Lupin三世」のイラスト。")
[![式場前の様子。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0024.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179490 "式場前の様子。")
[![式場前の様子。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0025.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179491 "式場前の様子。")
[![式場前の様子。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0028.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179492 "式場前の様子。")
[![式場前に展示されたパネル。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0023.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179494 "式場前に展示されたパネル。")
[![式場前に展示されたパネル。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0044.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179495 "式場前に展示されたパネル。")
[![式場前に展示されたパネル。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0045.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179496 "式場前に展示されたパネル。")


[![モンキー・パンチの花祭壇。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0032_hosei.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179813 "モンキー・パンチの花祭壇。")
[![モンキー・パンチの花祭壇。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0041.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179493 "モンキー・パンチの花祭壇。")
[![懇親会会場の入り口。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0047.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179497 "懇親会会場の入り口。")
[![返礼品の原作画クッキー詰め合わせと、オリジナルポストカード。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0048.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179498 "返礼品の原作画クッキー詰め合わせと、オリジナルポストカード。")
[![懇親会で振舞われた「Lupin寿司＆拳銃いか握り」。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0050.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179499 "懇親会で振舞われた「Lupin寿司＆拳銃いか握り」。")
[![懇親会で振舞われた「銭形のとっつぁん稲荷＆金塊玉子焼き」。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0051.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179500 "懇親会で振舞われた「銭形のとっつぁん稲荷＆金塊玉子焼き」。")
[![懇親会で振舞われた「次元おにぎり＆タバコチーズ」。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0059.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179502 "懇親会で振舞われた「次元おにぎり＆タバコチーズ」。")
[![懇親会で振舞われた「五ェ門 斬鉄剣オードブル」。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0057.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179501 "懇親会で振舞われた「五ェ門 斬鉄剣オードブル」。")
[![懇親会で振舞われた「不二子ちゃんスイーツプレート」。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0065.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179506 "懇親会で振舞われた「不二子ちゃんスイーツプレート」。")
[![懇親会で振舞われた「不二子ちゃんスイーツプレート」。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0067.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179508 "懇親会で振舞われた「不二子ちゃんスイーツプレート」。")
[![懇親会で振舞われた「名物・ミートボールスパゲッティ」。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0062.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179515 "懇親会で振舞われた「名物・ミートボールスパゲッティ」。")
[![芳名板](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0014.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179509 "芳名板")
[![芳名板](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0015.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179510 "芳名板")
[![芳名板](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0016.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179511 "芳名板")
[![芳名板](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0017.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179512 "芳名板")
[![芳名板](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0018.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179513 "芳名板")
[![小林清志](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0093.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179782 "小林清志")
[![山寺宏一](https://ogre.natalie.mu/media/news/comic/2019/0614/GR001422a_hosei.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179914 "山寺宏一")
[![浅野忠信](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0117.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179784 "浅野忠信")
[![さいとう・たかを](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0129.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179785 "さいとう・たかを")
[![ちばてつや](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179786 "ちばてつや")
[![吉本浩二](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0059-7sfj.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179788 "吉本浩二")
[![手塚るみ子](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0014-1tn3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179790 "手塚るみ子")
[![手塚眞](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0071.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179791 "手塚眞")
[![実写映画「Lupin三世」の製作に携わったトライストーン・エンタテイメント代表取締役の山本又一朗氏。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0080.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179792 "実写映画「Lupin三世」の製作に携わったトライストーン・エンタテイメント代表取締役の山本又一朗氏。")
[![栗田貫一](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179793 "栗田貫一")
[![栗田貫一](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0009-5ja1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179794 "栗田貫一")
[![里中満智子](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0024-dtbq.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179795 "里中満智子")
[![浅野忠信](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0042.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179796 "浅野忠信")
[![浅野忠信](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0035.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179797 "浅野忠信")
[![浅野忠信](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0058.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179798 "浅野忠信")
[![小栗旬](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0070.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179799 "小栗旬")
[![小栗旬](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0090.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179800 "小栗旬")
[![小栗旬](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0108.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179801 "小栗旬")
[![小栗旬](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0115.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179802 "小栗旬")
[![小林清志](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0144.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179803 "小林清志")
[![小林清志](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0159.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179804 "小林清志")
[![用意してきた言葉を読み上げる小林清志。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0167.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179805 "用意してきた言葉を読み上げる小林清志。")
[![モンキー・パンチの次男で、本会の喪主を務めた加藤州平氏。](https://ogre.natalie.mu/media/news/comic/2019/0614/DSC_0172.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/335565/1179806 "モンキー・パンチの次男で、本会の喪主を務めた加藤州平氏。")

