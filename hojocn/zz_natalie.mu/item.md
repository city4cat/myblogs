https://natalie.mu/comic/work/list/artist_id/2405

# 北条司の関連商品

### 「City Hunter THE MOVIE 史上最香のMission」

[
![City Hunter THE MOVIE 史上最香のMission（Blu-ray）](https://m.media-amazon.com/images/I/51aEQ0e6yfL._SL160_._SS70_.jpg)  
City Hunter THE MOVIE 史上最香のMission（Blu-ray）  
[Blu-ray Disc] 2020年5月8日発売 / Happinet  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B084HVVY7W/nataliecomic-22)

[
![City Hunter THE MOVIE 史上最香のMission（DVD）](https://m.media-amazon.com/images/I/51pyB4nHQsL._SL160_._SS70_.jpg)  
City Hunter THE MOVIE 史上最香のMission（DVD）  
[DVD] 2020年5月8日発売 / Happinet  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B084HVYGT1/nataliecomic-22)

### 「劇場版City Hunter <新宿Private Eyes>」

[
![劇場版City Hunter <新宿Private Eyes>（完全生産限定版 / Blu-ray）](https://images-fe.ssl-images-amazon.com/images/I/51zuXYblGzL._SS70_.jpg)  
劇場版City Hunter <新宿Private Eyes>（完全生産限定版 / Blu-ray）  
[Blu-ray Disc] 2019年10月30日発売 / Aniplex / ANZX-15021  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B07W3MBV17/nataliecomic-22)

[
![劇場版City Hunter <新宿Private Eyes>（完全生産限定版 / DVD）](https://images-fe.ssl-images-amazon.com/images/I/51zuXYblGzL._SS70_.jpg)  
劇場版City Hunter <新宿Private Eyes>（完全生産限定版 / DVD）  
[DVD2枚組] 2019年10月30日発売 / ANZB-15021  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B07W1KM7KH/nataliecomic-22)

[
![劇場版City Hunter <新宿Private Eyes>（通常版 / DVD）](https://images-fe.ssl-images-amazon.com/images/I/51wnkizMniL._SS70_.jpg)  
劇場版City Hunter <新宿Private Eyes>（通常版 / DVD）  
[DVD] 2019年10月30日発売 / ANSB-15021  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B07W3M9PR2/nataliecomic-22)

### 「「CITY HUNTER」Jump Best Scene」

[
![「CITY HUNTER」Jump Best Scene](https://images-fe.ssl-images-amazon.com/images/I/61hU4GcHxRL._SS70_.jpg)  
「CITY HUNTER」Jump Best Scene  
[Mook] 2019年2月8日発売 / 集英社 / 978-4081022878  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4081022879/nataliecomic-22)

### 「CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常」

[
![CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常（1）](https://images-fe.ssl-images-amazon.com/images/I/51aC7UXZXfL._SS70_.jpg)  
CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常（1）  
[漫画] 2019年1月19日発売 / 竹書房 / 978-4801964921  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4801964923/nataliecomic-22)

### 「今日からCITY HUNTER」

[
![今日からCITY HUNTER（3）](https://images-fe.ssl-images-amazon.com/images/I/51PN8Yv9eFL._SS70_.jpg)  
今日からCITY HUNTER（3）  
[漫画] 2019年1月19日発売 / 徳間書店 / 978-4199805431  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199805435/nataliecomic-22)

[
![今日からCITY HUNTER（1）](https://images-fe.ssl-images-amazon.com/images/I/51WwWQpAzKL._SS70_.jpg)  
今日からCITY HUNTER（1）  
[漫画] 2018年4月20日発売 / 徳間書店 / 978-4199804885  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199804889/nataliecomic-22)

### 「RASH!! ―Rash―」

[
![RASH!! ―Rash―](https://images-fe.ssl-images-amazon.com/images/I/51j8pv3o4kL._SS70_.jpg)  
RASH!! ―Rash―  
[漫画] 2017年9月20日発売 / 徳間書店 / 978-4199804472  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199804471/nataliecomic-22)

### 「桜の花 咲くころ 北条司 Short Stories Vol.2」

[
![桜の花 咲くころ 北条司 Short Stories Vol.2](https://images-fe.ssl-images-amazon.com/images/I/51WecAgXUpL._SS70_.jpg)  
桜の花 咲くころ 北条司 Short Stories Vol.2  
[漫画] 2017年8月19日発売 / 徳間書店 / 978-4199804403  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199804404/nataliecomic-22)

### 「小説 Angel Heart ～消えた心臓～」

[
![小説 Angel Heart ～消えた心臓～](https://images-fe.ssl-images-amazon.com/images/I/51Wvy8AvieL._SS70_.jpg)  
小説 Angel Heart ～消えた心臓～  
[書籍] 2017年8月19日発売 / 徳間書店 / 978-4198644697  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4198644691/nataliecomic-22)

### 「こもれ陽の下で…」

[
![こもれ陽の下で…（2）](https://images-fe.ssl-images-amazon.com/images/I/61tFe3AxKxL._SS70_.jpg)  
こもれ陽の下で…（2）  
[漫画] 2017年9月20日発売 / 徳間書店 / 978-4199804465  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199804463/nataliecomic-22)

[
![こもれ陽の下で…（1）](https://images-fe.ssl-images-amazon.com/images/I/51sMOvOWVkL._SS70_.jpg)  
こもれ陽の下で…（1）  
[漫画] 2017年8月19日発売 / 徳間書店 / 978-4199804410  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199804412/nataliecomic-22)

### 「City Hunter －Double Edge－ 北条司 Short Stories」

[
![City Hunter －Double Edge－ 北条司 Short Stories Vol.1](https://images-fe.ssl-images-amazon.com/images/I/51SqbHPln8L._SS70_.jpg)  
City Hunter －Double Edge－ 北条司 Short Stories Vol.1  
[2Blu-ray Disc＋PhotoBook] 2017年7月20日発売 / 978-4199804342  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/419980434X/nataliecomic-22)

### 「北条司画集 ANGEL HEART Color&Monochrome Illustrations」

[
![北条司画集 ANGEL HEART Color&Monochrome Illustrations](https://images-fe.ssl-images-amazon.com/images/I/51NuMzNw5SL._SS70_.jpg)  
北条司画集 ANGEL HEART Color&Monochrome Illustrations  
[画集] 2017年7月20日発売 / 徳間書店 / 978-4198644482  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4198644489/nataliecomic-22)

### 「Angel Heart完全読本」

[
![Angel Heart完全読本](https://images-fe.ssl-images-amazon.com/images/I/61yZRxH7oOL._SS70_.jpg)  
Angel Heart完全読本  
[Mook] 2016年3月31日発売 / 徳間書店 / 978-4197204458  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4197204450/nataliecomic-22)

### 「City Hunter完全読本」

[
![City Hunter完全読本](https://images-fe.ssl-images-amazon.com/images/I/61EKjKGKf5L._SS70_.jpg)  
City Hunter完全読本  
[Mook] 2015年7月25日発売 / 徳間書店 / 978-4197204298  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4197204299/nataliecomic-22)

### 「City Hunter XYZ edition」

[
![City Hunter XYZ edition（12）](https://images-fe.ssl-images-amazon.com/images/I/51Wrw2DejZL._SS70_.jpg)  
City Hunter XYZ edition（12）  
[漫画] 2015年12月19日発売 / 徳間書店 / 978-4199803239  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803238/nataliecomic-22)

[
![City Hunter XYZ edition（11）](https://images-fe.ssl-images-amazon.com/images/I/511-MjR4IzL._SS70_.jpg)  
City Hunter XYZ edition（11）  
[漫画] 2015年12月19日発売 / 徳間書店 / 978-4199803222  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/419980322X/nataliecomic-22)

[
![City Hunter XYZ edition（9）](https://images-fe.ssl-images-amazon.com/images/I/61itv2mLCLL._SS70_.jpg)  
City Hunter XYZ edition（9）  
[漫画] 2015年11月1日発売 / 徳間書店 / 978-4199803154  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803157/nataliecomic-22)

[
![City Hunter XYZ edition（7）](http://ecx.images-amazon.com/images/I/61GaFnGM8SL._SS70_.jpg)  
City Hunter XYZ edition（7）  
[漫画] 2015年10月20日発売 / 徳間書店 / 978-4199803079  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803076/nataliecomic-22)

[
![City Hunter XYZ edition（8）](https://images-fe.ssl-images-amazon.com/images/I/51Yqg0sD4dL._SS70_.jpg)  
City Hunter XYZ edition（8）  
[漫画] 2015年10月20日発売 / 徳間書店 / 978-4199803086  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803084/nataliecomic-22)

[
![City Hunter XYZ edition（10）](https://images-fe.ssl-images-amazon.com/images/I/51P1VG2%2BBoL._SS70_.jpg)  
City Hunter XYZ edition（10）  
[漫画] 2015年10月1日発売 / 徳間書店 / 978-4199803161  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803165/nataliecomic-22)

[
![City Hunter XYZ edition（5）](https://images-fe.ssl-images-amazon.com/images/I/51sQLGuDeTL._SS70_.jpg)  
City Hunter XYZ edition（5）  
[漫画] 2015年9月19日発売 / 徳間書店 / 978-4199802973  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802975/nataliecomic-22)

[
![City Hunter XYZ edition（6）](https://images-fe.ssl-images-amazon.com/images/I/61hOD3I88aL._SS70_.jpg)  
City Hunter XYZ edition（6）  
[漫画] 2015年9月19日発売 / 徳間書店 / 978-4199802980  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802983/nataliecomic-22)

[
![City Hunter XYZ edition（3）](https://images-fe.ssl-images-amazon.com/images/I/51Lfk67h6PL._SS70_.jpg)  
City Hunter XYZ edition（3）  
[漫画] 2015年8月20日発売 / 徳間書店 / 978-4199802911  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802916/nataliecomic-22)

[
![City Hunter XYZ edition（4）](https://images-fe.ssl-images-amazon.com/images/I/51dZQFM%2BoOL._SS70_.jpg)  
City Hunter XYZ edition（4）  
[漫画] 2015年8月20日発売 / 徳間書店 / 978-4199802928  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802924/nataliecomic-22)

[
![City Hunter XYZ edition（1）](https://images-fe.ssl-images-amazon.com/images/I/51wmgxxDDoL._SS70_.jpg)  
City Hunter XYZ edition（1）  
[漫画] 2015年7月18日発売 / 徳間書店 / 978-4199802812  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802819/nataliecomic-22)

[
![City Hunter XYZ edition（2）](https://images-fe.ssl-images-amazon.com/images/I/51OzDNHsMmL._SS70_.jpg)  
City Hunter XYZ edition（2）  
[漫画] 2015年7月18日発売 / 徳間書店 / 978-4199802829  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802827/nataliecomic-22)

### 「City Hunter」

[
![City Hunter（SPECIAL VERSION / DVD）](https://images-fe.ssl-images-amazon.com/images/I/51NMHNPfFaL._SS70_.jpg)  
City Hunter（SPECIAL VERSION / DVD）  
[DVD] 2014年11月28日発売 / RAX-901  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B00PJRN2KO/nataliecomic-22)

### 「世界から届いたJapanese Manga」
来自世界各地的日本漫画  

[
![世界から届いたJapanese Manga](https://images-fe.ssl-images-amazon.com/images/I/51N27TjOwsL._SS70_.jpg)  
世界から届いたJapanese Manga  
[書籍] 2014年2月1日発売 / 978-4905246312  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4905246318/nataliecomic-22)

### 「漫画が語る戦争 焦土の鎮魂歌」

[
![漫画が語る戦争 焦土の鎮魂歌](http://ecx.images-amazon.com/images/I/51ZWgOcyCQL._SS70_.jpg)  
漫画が語る戦争 焦土の鎮魂歌  
[漫画] 2013年7月29日発売 / 小学館 / 978-4778032579  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4778032578/nataliecomic-22)

### 「Angel Heart 1stSeason」

[
![Angel Heart 1stSeason 1-24巻Set](https://images-fe.ssl-images-amazon.com/images/I/51ik-KCZ4aL._SS70_.jpg)  
Angel Heart 1stSeason 1-24巻Set  
[漫画] 2013年2月20日発売 / 徳間書店  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/B00BC9M7W2/nataliecomic-22)

[
![Angel Heart 1stSeason（12）](https://images-fe.ssl-images-amazon.com/images/I/5176u%2Bsa3dL._SS70_.jpg)  
Angel Heart 1stSeason（12）  
[漫画] 2012年8月20日発売 / 徳間書店 / 978-4199801037  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801030/nataliecomic-22)

[
![Angel Heart 1stSeason（11）](https://images-fe.ssl-images-amazon.com/images/I/51CXpCg3IzL._SS70_.jpg)  
Angel Heart 1stSeason（11）  
2012年8月20日発売 / 徳間書店 / 978-4199801020  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801022/nataliecomic-22)

[
![Angel Heart 1stSeason（1）](https://images-fe.ssl-images-amazon.com/images/I/61YGr9jynaL._SS70_.jpg)  
Angel Heart 1stSeason（1）  
[漫画] 2012年3月19日発売 / 徳間書店 / 978-4199800733  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800735/nataliecomic-22)

### 「F.COMPO」

[
![F.COMPO（3）](https://images-fe.ssl-images-amazon.com/images/I/51K2tynEL6L._SS70_.jpg)  
F.COMPO（3）  
[書籍] 2011年9月20日発売 / 徳間書店 / 978-4199800399  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800395/nataliecomic-22)

### 「Angel Heart 2ndSeason」

[
![Angel Heart 2ndSeason（16）](https://images-fe.ssl-images-amazon.com/images/I/61rTquJgGhL._SS70_.jpg)  
Angel Heart 2ndSeason（16）  
[漫画] 2017年7月20日発売 / 徳間書店 / 978-4199804304  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199804307/nataliecomic-22)

[
![Angel Heart 2ndSeason（12）](https://images-fe.ssl-images-amazon.com/images/I/51g5X-A7RYL._SS70_.jpg)  
Angel Heart 2ndSeason（12）  
[漫画] 2015年10月20日発売 / 徳間書店 / 978-4199803000  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803009/nataliecomic-22)

[
![Angel Heart 2ndSeason（11）](https://images-fe.ssl-images-amazon.com/images/I/61o2%2B4S91qL._SS70_.jpg)  
Angel Heart 2ndSeason（11）  
[漫画] 2015年7月18日発売 / 徳間書店 / 978-4199802805  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802800/nataliecomic-22)

[
![Angel Heart 2ndSeason（10）](https://images-fe.ssl-images-amazon.com/images/I/51hCyaVx05L._SS70_.jpg)  
Angel Heart 2ndSeason（10）  
[漫画] 2015年2月20日発売 / 徳間書店 / 978-4199802560  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802568/nataliecomic-22)

[
![Angel Heart 2ndSeason（9）](https://images-fe.ssl-images-amazon.com/images/I/61N9rva2qIL._SS70_.jpg)  
Angel Heart 2ndSeason（9）  
[漫画] 2014年9月20日発売 / 徳間書店 / 978-4199802324  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802320/nataliecomic-22)

[
![Angel Heart 2ndSeason（8）](https://images-fe.ssl-images-amazon.com/images/I/51564QZxCeL._SS70_.jpg)  
Angel Heart 2ndSeason（8）  
2014年3月20日発売 / 徳間書店 / 978-4199801976  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801979/nataliecomic-22)

[
![Angel Heart 2ndSeason（7）](https://images-fe.ssl-images-amazon.com/images/I/51XwXaXIBbL._SS70_.jpg)  
Angel Heart 2ndSeason（7）  
2013年11月20日発売 / 徳間書店 / 978-4199801709  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801707/nataliecomic-22)

[
![Angel Heart 2ndSeason（6）](https://images-fe.ssl-images-amazon.com/images/I/51iisaYDR2L._SS70_.jpg)  
Angel Heart 2ndSeason（6）  
2013年6月20日発売 / 徳間書店 / 978-4199801488  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801480/nataliecomic-22)

[
![Angel Heart 2ndSeason（4）](https://images-fe.ssl-images-amazon.com/images/I/51l8qt9vxnL._SS70_.jpg)  
Angel Heart 2ndSeason（4）  
2012年8月20日発売 / 徳間書店 / 978-4199801006  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801006/nataliecomic-22)

[
![Angel Heart 2ndSeason（3）](https://images-fe.ssl-images-amazon.com/images/I/514bMgMxP%2BL._SS70_.jpg)  
Angel Heart 2ndSeason（3）  
[漫画] 2012年3月19日発売 / 徳間書店 / 978-4199800696  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800697/nataliecomic-22)

[
![Angel Heart 2ndSeason（2）](https://images-fe.ssl-images-amazon.com/images/I/51e9TySarML._SS70_.jpg)  
Angel Heart 2ndSeason（2）  
[書籍] 2011年9月20日発売 / 徳間書店 / 978-4199800351  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800352/nataliecomic-22)

### 「Angel Heart 2ndSeason」

[
![Angel Heart 2ndSeason（5）](http://ecx.images-amazon.com/images/I/51lDtiz5AuL._SS70_.jpg)  
Angel Heart 2ndSeason（5）  
[漫画] 2015年6月8日発売 / 徳間書店  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B00YV3WB22/nataliecomic-22)

[
![Angel Heart 2ndSeason (1)](https://images-fe.ssl-images-amazon.com/images/I/51MyAQuvH%2BL._SS70_.jpg)  
Angel Heart 2ndSeason (1)  
[書籍] 2011年3月19日発売 / 徳間書店 / 978-4199800016  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800018/nataliecomic-22)

### 「Cat's❤愛」

[
![Cat's▼愛（7）](https://images-fe.ssl-images-amazon.com/images/I/51GgqrbPNjL._SS70_.jpg)  
Cat's▼愛（7）  
[書籍] 2013年11月20日発売 / 徳間書店 / 978-4199801730  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801731/nataliecomic-22)

[
![Cat's▼愛（6）](https://images-fe.ssl-images-amazon.com/images/I/516DQcs0SYL._SS70_.jpg)  
Cat's▼愛（6）  
[書籍] 2013年6月20日発売 / 徳間書店 / 978-4199801495  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801499/nataliecomic-22)

[
![Cat's♥愛（2）](https://images-fe.ssl-images-amazon.com/images/I/51-Ic0QlBAL._SS70_.jpg)  
Cat's♥愛（2）  
[書籍] 2011年9月20日発売 / 徳間書店 / 978-4199800368  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800360/nataliecomic-22)

[
![Cat's❤愛 (1)](https://images-fe.ssl-images-amazon.com/images/I/51vBpGokiUL._SS70_.jpg)  
Cat's❤愛 (1)  
[書籍] 2011年3月19日発売 / 徳間書店 / 978-4199800023  
Amazon.co.jp [書籍版 / Kindle版]
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800026/nataliecomic-22)

### 「Angel Heart」

[
![北条司デビュー30周年記念 Angel HeartPair MugCup](https://ogre.natalie.mu/media/goods/114502/EH_0.jpg?impolicy=thumb_fit&width=180&height=180)  
北条司デビュー30周年記念 Angel HeartPair MugCup  
[Goods] 2017年5月9日発売  
Natalie Store
](http://store.natalie.mu/shopdetail/000000001036)

[
![Angel Heart (28)](https://images-fe.ssl-images-amazon.com/images/I/619VjubGDyL._SS70_.jpg)  
Angel Heart (28)  
[書籍] 2008年12月9日発売 / 新潮社 / 978-4107714442  
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4107714446/nataliecomic-22)

