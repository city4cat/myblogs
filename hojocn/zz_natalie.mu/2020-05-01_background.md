https://natalie.mu/comic/news/377664


# 「鬼滅の刃」「SAO」「あの花」「Fate」アニプレアニメ18作の通話用背景一挙配布

2020年5月1日  [Comment](https://natalie.mu/comic/news/377664/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


「鬼滅の刃」「ソードアート・オンライン」「[あの日見た花の名前を僕達はまだ知らない。](https://natalie.mu/comic/anime/176)」「劇場版『Fate/stay night [Heaven's Feel]』」など、アニプレックスが製作するアニメ18作品のオンライン通話用背景画像が期間限定で配布されている。  
《鬼灭之刃》《刀剑神域》《我们仍未知道那天所看见的花的名字。》“剧场版《Fate/stay night [Heaven's Feel]》”等由Aniplex制作的18部动画的在线通话用背景图像在限定时间内发布。  

[
![「ANIPLEX NEXT」によるオンライン通話用背景画像配布作品の一覧。](https://ogre.natalie.mu/media/news/comic/2020/0501/aniplex_next_01.jpg?impolicy=hq&imwidth=730&imdensity=1)
「ANIPLEX NEXT」によるオンライン通話用背景画像配布作品の一覧。  
大きなサイズで見る（全4件）  
“ANIPLEX NEXT”的在线通话用背景图像发布作品一览。“ANIPLEX NEXT”的在线通话用背景图像发布作品一览。  
大尺寸查看(共4件)  
](https://natalie.mu/comic/gallery/news/377664/1371548)

これは、アニプレックスの最新情報を発信する「ANIPLEX NEXT（アニプレッククスト）」のWeb企画「おうちで充実！アニメもゲームも楽しもう！」の一環として実施されるもの。配布されている画像はアニメ本編の背景を切り取ったもので、「あの日見た花の名前を僕達はまだ知らない。」の秘密基地、「かぐや様は告らせたい～天才たちの恋愛頭脳戦～」から生徒会室、「鬼滅の刃」の無限城、「劇場版『Fate/stay night [Heaven's Feel]』」から衛宮家の居間など、オンライン通話の背景に設定すると、作品世界に入り込んだ気分を味わえるものが揃えられた。  
这是发布ANIPLEX最新情报的“ANIPLEX NEXT”的Web企划“在家里充实!享受动画和游戏!”的一个环节。被分发的图像是截取了动画正片的背景，“我们仍未知道那天所看见的花的名字。”的秘密基地、《辉夜大人想告之~天才们的恋爱头脑战~》中的学生会室、《鬼灭之刃》中的无限城、剧场版《Fate/stay night [Heaven'sFeel》》中卫宫家的客厅等，如果设定为在线通话的背景，就会有进入作品世界的感觉。

また今回の企画では、SONYから発売されているワイヤレスノイズキャンセリングヘッドセットなどが当たるキャンペーンもTwitterにて実施。「アニメ視聴時のおすすめアイテム」や「グッズを飾るときのおすすめ方法」といった家でのアニメ・ゲームの楽しみ方をハッシュタグ「#おうちでアニゲー」を付けてツイートすると、抽選で景品がプレゼントされる。さらに、「ANIPLEX NEXT」のWeb番組でパーソナリティを務める[前野智昭](https://natalie.mu/comic/artist/72030)と[茅野愛衣](https://natalie.mu/comic/artist/85722)による特別賞も設置。特別賞の景品には49V型の4K液晶テレビが2名分用意された。応募期間は5月11日23時59分まで。  
另外，在此次企划中，SONY发售的无线降噪耳机等也将在Twitter上进行抽奖活动。“观看动画时推荐的道具”、“装饰周边时推荐的方法”等在家里享受动画、游戏的方法，加上主题标签“#在家里玩动漫”并在推特上发表，就可以通过抽奖获得奖品。此外，“ANIPLEX NEXT”Web节目的主持人前野智昭和茅野爱衣也将获得特别奖。作为特别奖奖品，还准备了2台49v型4k液晶电视。报名时间截止到5月11日23时59分。  

そのほか「ANIPLEX NEXT」のサイトにて、アニプレックス作品60タイトル以上の放送・配信情報を掲載中。dアニメストア、Amazon Prime Video、Netflixなどのサブスクリプションサービスから、再放送まで各作品ごとに情報がまとめられている。  
除此之外，在“ANIPLEX NEXT”的网站上，还登载了动画作品60部以上的放送和配信信息。从d动漫商店、Amazon Prime Video、Netflix等订阅服务，到重播为止汇总了各作品的信息。  

この記事の画像（全4件）  
这篇报道的图片(共4篇)  

[![「ANIPLEX NEXT」によるオンライン通話用背景画像配布作品の一覧。](https://ogre.natalie.mu/media/news/comic/2020/0501/aniplex_next_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/377664/1371548 "「ANIPLEX NEXT」によるオンライン通話用背景画像配布作品の一覧。")
[![「ANIPLEX NEXT」によるオンライン通話用背景画像配布作品の一覧。](https://ogre.natalie.mu/media/news/comic/2020/0501/aniplex_next_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/377664/1371549 "「ANIPLEX NEXT」によるオンライン通話用背景画像配布作品の一覧。")
[![プレゼント企画の景品一覧。](https://ogre.natalie.mu/media/news/comic/2020/0501/aniplex_next_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/377664/1371550 "プレゼント企画の景品一覧。")
[![「ANIPLEX NEXT」ロゴ](https://ogre.natalie.mu/media/news/comic/2020/0323/next.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/377664/1353186 "「ANIPLEX NEXT」ロゴ")

## オンライン通話用背景画像配布作品の一覧
在线通话用背景图像发布作品列表  

「アイドルマスター」  
「[あの日見た花の名前を僕達はまだ知らない。](https://natalie.mu/comic/anime/176)」  
「エロマンガ先生」  
「かぐや様は告らせたい ～天才たちの恋愛頭脳戦～」  
「傷物語」  
「鬼滅の刃」  
「劇場版『冴えない彼女の育てかた Fine』」  
「3月のライオン」  
「四月は君の嘘」  
「劇場版CityHunter ＜新宿Private Eyes＞」  
「ソードアート・オンライン」  
「つり球」  
「映画 ねこねこ日本史 ～龍馬のはちゃめちゃタイムトラベルぜよ！～」  
「鋼の錬金術師 FULLMETAL ALCHEMIST」  
「はたらく細胞」  
「劇場版『Fate/stay night [Heaven's Feel]』」  
「魔法少女まどか☆マギカ」  
「約束のネバーランド」


LINK

[ANIPLEX NEXT](https://aniplex-next.jp/)  
[おうちで充実！アニメもゲームも楽しもう！ | ANIPLEX NEXT  
(充实在家里!动漫和游戏都要享受! | ANIPLEX NEXT)](https://aniplex-next.jp/stayhome/)  
[Aniplex NEXT【公式】 (@aniplex\_next) | Twitter](https://twitter.com/aniplex_next)  
[Aniplex - YouTube](https://www.youtube.com/aniplex)  