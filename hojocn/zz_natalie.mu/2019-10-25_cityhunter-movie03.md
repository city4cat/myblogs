source:  
https://natalie.mu/comic/pp/cityhunter-movie03

![](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/pc_header.jpg?impolicy=hq)  

「劇場版CityHunter <新宿Private Eyes>」  

2019年10月25日

# ![「劇場版CityHunter <新宿Private Eyes>」｜冴羽獠がDisk工場にやってきた！神谷明の“お宝Disk”制作Report＆Interview（剧场版CityHunter <新宿Private Eyes>”|冴羽獠Disk工厂来到了!神谷明的“宝贝Disk”制作Report＆Interview）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/pc_header.jpg?impolicy=hq)

「劇場版CityHunter <新宿Private Eyes>」のBlu-ray / DVDが、10月30日に発売される。北条司原作のアニメ「CityHunter」が約20年ぶりに復活し、冴羽獠役の神谷明、槇村香役の伊倉一恵らのオリジナルキャストが再集結したことが大きな話題を呼んだ映画の、待望のソフト化だ。  
《剧场版CityHunter <新宿Private Eyes>》的蓝光/ DVD将于10月30日发售。北条司原作的动画「CityHunter」时隔约20年复活，冴羽獠役的神谷明、槇村香役的伊仓一惠等原班人马再次集结，引发了巨大的话题，这部电影是期待已久的改编。

このBlu-ray / DVD完全限定生産版のうち、150枚は神谷がDiskの盤面に直筆サインを入れた“お宝Disk”であることが発表された。コミックナタリーでは発売を前に、神谷が静岡にあるDisk工場へ訪れ、サインを入れる様子をReport。さらに神谷にInterviewを実施し、映画公開後の反響や、Blu-ray / DVDの見どころなども聞いた。  
这个Blu-ray / DVD完全限定生产版中，150张是神谷在Disk的盘面上亲笔签名的“宝贝Disk”。在ComicNatalie发售前，神谷访问了位于静冈的Disk工厂，并在那里签名。并且采访了神谷，听取他在电影发行后的反应和蓝光/DVD的亮点。  

取材・文 / 松本真一　撮影 / 笹井タカマサ



## 神谷明のDisk工場訪問記 in 静岡
神谷明的Disk工厂访问记in静冈  

「劇場版CityHunter <新宿Private Eyes>」Blu-ray / DVDの発売を前にした10月某日、Sony・Music Solutionsの静岡工場を神谷明が訪れた。Blu-ray、DVD、CDなどのDiskをプレスし、パッケージングなども手がけているこの工場を神谷が訪れたのは、このたびのソフト発売を記念したキャンペーンの一環。できたばかりのソフトのうち150枚の盤面に神谷がサインを入れ、それが全国に出荷されるのだ。  
《剧场版CityHunter <新宿Private Eyes>》蓝光/ DVD发售前的10月某日，神谷明造访了Sony Music Solutions的静冈工厂，蓝光、DVD、CD和其他光盘都是在这里压制和包装的。神谷造访这个工厂，是为了纪念这次的软件发售活动的一部分。刚完成的软件中，神谷在150张盘面上签名，然后销往全国。  

[![ウェルカムボードにサインを入れる神谷明。（神谷明在欢迎板上签名。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report01s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report01.jpg)

[![神谷明と、彼を迎えるSony・Music Solutions社員。（神谷明和迎接他的Sony Music Solutions员工。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report02s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report02.jpg)

人気声優の神谷が工場を訪れるということで、入口では社員のうち約50名がお出迎え。思わぬ歓迎に神谷は笑顔で応え、手作りのウェルカムボードにサインを書き入れた。  
据说人气声优神谷将来到工厂，在入口约有50名职员前来迎接。神谷笑着回应了意想不到的欢迎，并在手工制作的欢迎板上签名。  

[![工場見学をする神谷明。（参观工厂的神谷明。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report03s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report03.jpg)

[![工場見学をする神谷明。（参观工厂的神谷明。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report04s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report04.jpg)

工場内に入った神谷はまずBlu-ray / DVDの製造過程を見学した。長年のキャリアを誇る神谷も初体験だったようで興味津々。従業員に「これは何をしているところですか？」と積極的に質問していた。  
进入工厂后，神谷首先参观了蓝光/ DVD的制作过程。拥有多年职业经验的神谷也是第一次体验，饶有兴趣。积极地提问员工“这是做什么的地方?”。  

[![特典を封入する神谷明。（封入特典的神谷明。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report05s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report05.jpg)

[![「劇場版CityHunter <新宿Private Eyes>」完全生産限定版を手にする神谷明。（“剧场版CityHunter <新宿Private Eyes>”完全生产限定版的神谷明。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report06s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report06.jpg)

続いては、なんと神谷がソフトのパッケージングに挑戦！ 従業員にやり方を教わりながら、特典などを手作業で封入していく。ソフトを購入した人は、「これは神谷さんが入れてくれた特典かも……？」と思いを馳せてみるのも一興だ。  
接下来，神谷竟然挑战了软件包装!一边向员工学习方法，一边手工将优惠等装入信封。那些购买了软件的人可以想，"也许这是神谷先生为我放的奖励...？" 想一想也很有趣。  

[![ソフトの盤面にサインを入れる神谷明。（在软件的盘面上签名的神谷明。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report07s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report07.jpg)

[![神谷明のサイン入りDVD。（神谷明签名的DVD。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report08s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report08.jpg)

最後はこの日の最大の目的である、盤面へのサイン入れ。「これ、僕が自分で買ったやつに入ってる可能性もあるのかな？（笑）」。そんな冗談を飛ばしながらも、すさまじい集中力でどんどんサインを入れていく。  
当天的最后一项活动是今天的主要目的：请神谷昭在DVD上签名。“这个，有可能在我自己买的东西吗?(笑)”。一边开着这样的玩笑，一边以惊人的集中力不断地签名。  

[![サインを入れ終えた神谷明。（签完名的神谷明。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report09s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report09.jpg)

[![サインを入れ終えた神谷明。（签完名的神谷明。）](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report10s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/report10.jpg)

約1時間で、用意されたDiskのすべてにサインを入れ終えた神谷。全国に流通する完全生産限定版のうち、合計150枚のみがこの激レア仕様の“お宝Disk”となっている。  
用约1小时，神谷签名了全部准备好的Disk。在全国流通的完全生产限定版中，合计只有150枚是这款超稀有规格的“宝贝Disk”。  



----  
Page2  
----  

## 冴羽獠役・神谷明Interview

[![神谷明](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/int01s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/int01.jpg)

──まずは工場見学、お疲れさまでした。  
——首先参观工厂，辛苦了。  

50名以上の皆様に出迎えていただきました。感激しました。お世話になりました。そして、ありがとうございました。  
受到了50多人的迎接。我很感激。承蒙关照。还有，非常感谢。  

──DVDやBlu-rayが制作される過程を見るのは貴重な経験だと思います。  
——我认为观看DVD和蓝光的制作过程是宝贵的经验。  

実際の基盤やコーティング、焼き付けなど興味深く見学しました。当たり前のことですが、大きな工場でたくさんの方の手によって生み出されているのだということに感動しましたね。  
饶有兴趣地参观了实际的基础和涂层、烫印等。虽然是理所当然的事情，但是通过很多人的手在大工厂生产出来的事让人感动。  

──特典の封入まで手伝われていたのには驚きました。  
——连特典的封套都帮忙，这让我很吃惊。  

教えていただき、ほんの少しパッケージングのお手伝いをしましたが、むちゃくちゃ緊張しました。大変いい思い出になりましたよ。あのパッケージ、どなたのお手元に届いているのでしょう。気になります。  
有人教我怎么做，并帮我做了一点包装，但我非常紧张。成为了非常美好的回忆哦。那个包装送到了谁的手上呢?我很在意。  

──「劇場版CityHunter<新宿Private Eyes>」は、約20年ぶりの「CityHunter」復活ということで、公開前から話題を呼びました。公開後、ファンやお仕事仲間からの反応はいかがでしたか？  
——《剧场版CityHunter<新宿Private Eyes>》因为是时隔20年的《CityHunter》复活，上映前就引起了话题。公映后，粉丝和同事们的反应如何?  

「待ち望んでいた」という声、「復活させてくれてうれしかった」という声、また、「絵、音楽、最高！」という声が多かったです。そして「レギュラー声優陣がオリジナルのままでよかった、期待以上だった」という方もたくさんいらっしゃいました。  
“期待已久”的声音，“能让我复活很高兴”的声音，还有“画，音乐，最棒!”这样的声音很多。而且也有很多人说“常规声优阵容保持原创真是太好了，超出了期待”。  

──本作は応援上映も実施されましたね（参照：[「CityHunter」100万人突破に北条司は目を白黒、神谷明は「この先を期待」](https://natalie.mu/comic/news/325528)）。  
——本作品也进行了应援上映吧(参照:[「CityHunter」突破100万人,北条司的吓了一跳，神谷明则是“期待未来”](https://natalie.mu/comic/news/325528))。  

[![「劇場版CityHunter<新宿Private Eyes>」より。](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/scene01s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/scene01.jpg)

「少し恥ずかしかった」という方もいらっしゃいましたが、多くの皆さんは、自分の思いを大声で叫べて楽しかったという反応でした。  
虽然也有人说“有点不好意思”，但大多数人的反应是，大声喊出自己的想法很开心。  

──20年前はなかった文化なので、「CityHunter」ファンは初めて体験する人も多かったかもしれませんね。  
——因为是20年前没有的文化，所以《城市猎人》的粉丝可能有很多人是第一次体验。

ですが、応援上映最初の、制作会社のテロップが映る時点から「アニプレックスありがとう！」などと盛り上がっていたのが驚きでした。また劇中でモデルを募集するという場面で「はーい」と返事するお客様もいたり、各キャラクターに声がかかったり、楽しんでくれているのを見てうれしかったです。  
但是，应援上映最初，制作公司的字幕映出来的时候，我听到人们喊着“Aniplex谢谢!”如此热烈的气氛让我很惊讶。还有在剧中招募模特的场面，也有客人回答“好”，和各个角色打招呼，看到他们很开心。  

──プライベートで劇場まで観に行かれたりはしましたか？  
——您有私下去电影院看电影吗?  

何回か観に行って、4DXは（野上冴子役の）一龍斎春水さんたちと観に行きました。臨場感たっぷりで、全員大いに楽しみました。お客様の層はけっこう幅広かったですね。  
去看了几次，4DX和(饰演野上冴子的)一龙斋春水一起去看了。现场感十足，全员都很享受。观众的层次相当广泛。  

──僕も劇場に行く前は正直、リアルタイムでアニメを観ていたアラフォーの方が多いのかなと思っていたんですが、若いファンもいらっしゃいましたよね。そして映画は動員人数100万人を超える大ヒットでした。公開前、ここまでのヒットは予測されていましたか？  
——说实话，我在去剧场之前，也觉得有很多人是实时观看动画的，但也有年轻的粉丝。而且电影动员人数超过了100万，大受欢迎。上映前，预测到这里的卖座吗?  

まったく想像していませんでした。むしろ想像以上です。また、いくつかの映画館が独自に応援してくれたことも忘れられません。100トンハンマーを作ってくださったり、上映期間外でも劇場独自に大幅延長をしてくださったり。ありがたかったです。  
完全没有想象过。甚至超乎想象。另外，几家电影院独自支持的事也忘不了。制作了100吨的锤子，在上映期间外剧场独自大幅度延长。太感谢了。  

──約20年ぶりに公開される「CityHunter」新作がここまでヒットした要因はどこにあると思いますか。  
——您认为时隔20年上映的《城市猎人》新作如此受欢迎的主要原因是什么?

[![「劇場版CityHunter<新宿Private Eyes>」より。](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/scene02s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/scene02.jpg)

企画、そして制作に携わったスタッフの思いと、お客様の思いが一致したことでしょうね。また、作品のクオリティ、公開のタイミングも良かったと思います。  
参与企划和制作的工作人员的想法和观众的想法是一致的吧。另外，我觉得作品的品质、公开的时机都很好。  

──今回の「新宿プライベートアイズ」で好きなシーンはどこでしょうか。もう公開後なので、ネタバレはあまり気にせず語ってもらえれば。  
——这次的“新宿Private Eyes”中喜欢的场景是哪个呢?已经上映了，不用太在意剧透，说出来就好了。  

新宿の夜景を見ながら、獠と亜衣が語るシーンですね。   
看着新宿的夜景，獠和亚衣讲述的场景。  

──「ここへ来ると、新宿もまんざら捨てたもんじゃないと思えるんだ」というシーンですよね。  
——“来到这里，就会觉得新宿也不是那么糟糕”这样的场景是吧?  

そしてエンディング近くの、香がヘリに飛びつき、ロープを巻かれた獠が海坊主に蹴り落とされるシーンから御国を倒すまでの一連の流れが好きです。もちろんエンディングもね。  
然后接近结尾的时候，从香扑向直升飞机，缠上绳索的獠被海坊主踢下去的场景到打倒御国的一系列流程我很喜欢。当然结尾也是。  

──エンディングの「Get Wild」はやはり盛り上がっていましたね。映画では、アニメの歴代主題歌や挿入歌が数多く流れました。個人的にお気に入りの曲はありますか？  
——结尾的“Get Wild”果然很热烈呢。电影中，播放了很多动画的历代主题曲和插曲。你个人有喜欢的曲子吗?  

オープニングのラップ（「Mr.Cool」）、戦いのシーンで使われていた「FOOT STEPS」、そして、御国との対決中に聴こえてくる「City Hunter～愛よ消えないで」……。もちろん「Get Wild」は外せません。  
开场的Rap(“Mr.Cool”)、战斗场景中使用的“FOOT STEPS”，还有和御国对决中听到的“City Hunter ~爱不要消失”……。当然，“Get Wild”是必不可少的。  

──このたび待望のBlu-ray / DVD発売となりますが、ソフトならではの見どころはありますか？  
——这次是期待已久的蓝光 / DVD发售，有仅软件才有的看点吗?

全編を通して何度でも観てみてほしいのですが、エンディングロールのキャスト・スタッフの紹介、そして画面右上の回想シーン。これも観ていただきたいですね。劇場ではゆっくり観られなかったと思うので。  
我希望人们可以随心所欲地观看整部影片，但我希望他们能在结尾卷中观看演员和工作人员的介绍，以及屏幕右上角的回忆场景。 我希望你也能看一下。 我想我没能在剧院里看完它。  

──スタッフロールの文字と回想の映像って同時に観るのが難しいですからね。さて、2月には「新宿プライベートアイズ」公開、11月末にはフランス版「CityHunter」公開ということで、2019年は「CityHunter」イヤーとなりました。作品が長く愛されている理由はどういった部分だと思われますか。  
——因为工作人员的文字和回忆的影像很难同时看。2月《新宿private eyes》上映，11月末法国版《CityHunter》上映，2019年成为“CityHunter”年。您认为作品长期受人喜爱的理由是什么?  

[![「劇場版CityHunter<新宿Private Eyes>」より。](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/scene03s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/scene03.jpg)

作品の面白さとキャラクターの魅力に尽きると思います。「CityHunter」という素敵な作品にめぐり合い、最高のスタッフと作り上げることができた幸せを感じています。  
我认为是作品的有趣和角色的魅力。能够邂逅《CityHunter》这样优秀的作品，和最棒的工作人员一起创作，我感到很幸福。  

──当時のスタッフやキャストが再集結、ということで話題になりましたが、「CityHunter」を長年応援し続けてくれたファンも再集結してくれたことで、ここまでのヒットになったんでしょうね。  
——当时的工作人员和演员们再次集结，这成为了话题，长年支持《CityHunter》的粉丝们也再次集结，才有了今天的人气吧。  

そうですね。「CityHunter」を愛し、支えて続けてくださったファンの皆様には心から感謝しています。  
是啊。对一直喜爱并支持《CityHunter》的粉丝们表示衷心的感谢。  

## 「劇場版CityHunter <新宿Private Eyes>」完全生産限定版の購入者には、ほかにもこんなEvents・キャンペーンが！
“剧场版CityHunter <新宿Private Eyes>”完全生产限定版的购买者，还有这样的活动·宣传!  

### 神谷明トークショー＆特典お渡し会に300名を招待  
神谷明脱口秀&特典交付会邀请300人  

日時：2019年12月8日（日）  
1部：開場13:30、開演14:00（予定）  
2部：開場15:30、開演16:00（予定）  
会場：都内某所（当選メールにて案内）  
定員：1部、2部ともに150名ずつ  
応募方法：完全生産限定版パッケージに封入されている抽選申込券に記載。  
応募締め切り：2019年11月17日（日）23:59  

时间:2019年12月8日(星期日)  
第一部:开场13:30，开演14:00(预定)  
第二部:开场15:30，开演16:00(预定)  
会场:东京都内某处(用中奖邮件介绍)  
名额:1、2份各150人  
应募方法:完全生产限定版包装被封入的抽选申请券记载。  
截止日期:2019年11月17日(周日)23:59  


[![Original Blouson](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/image01s.jpg)](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/image01.jpg)

### 「CityHunter」オリジナルブルゾンをプレゼント
赠送“CityHunter”原创夹克  

当選者：20名  
応募方法：完全生産限定版に封入されている専用ハガキを使用して応募。  
当选者:20人  
应征方法:使用完全生产限定版的专用明信片应征。  


------  

「劇場版CityHunter <新宿Private Eyes>」

2019年10月30日発売 / Aniplex

[ ![ 「劇場版CityHunter <新宿Private Eyes>」完全生産限定版Blu-ray ](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/pc_item01.jpg) ](https://www.amazon.co.jp/exec/obidos/ASIN/B07W3MBV17/nataliecomic-pp-22)

完全生産限定版  [Blu-ray]  
税込9680円 / ANZX-15021

[Amazon.co.jp](https://www.amazon.co.jp/exec/obidos/ASIN/B07W3MBV17/nataliecomic-pp-22)

[![「劇場版CityHunter <新宿Private Eyes>」完全生産限定版DVD](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/pc_item01.jpg)](https://www.amazon.co.jp/exec/obidos/ASIN/B07W1KM7KH/nataliecomic-pp-22)

完全生産限定版  [DVD 2枚組]  
税込8580円 / ANZB-15021

[Amazon.co.jp](https://www.amazon.co.jp/exec/obidos/ASIN/B07W1KM7KH/nataliecomic-pp-22)

[![「劇場版CityHunter <新宿Private Eyes>」通常版Blu-ray](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/pc_item02.jpg)](https://www.amazon.co.jp/exec/obidos/ASIN/B07W8LBDYS/nataliecomic-pp-22)

通常版  [Blu-ray]  
税込5280円 / ANSX-15021

[Amazon.co.jp](https://www.amazon.co.jp/exec/obidos/ASIN/B07W8LBDYS/nataliecomic-pp-22)

[![「劇場版CityHunter <新宿Private Eyes>」通常版DVD](https://ogre.natalie.mu/media/pp/static/comic/cityhunter-movie03/pc_item02.jpg)](https://www.amazon.co.jp/exec/obidos/ASIN/B07W3M9PR2/nataliecomic-pp-22)

通常版  [DVD]  
税込4180円 / ANSB-15021

[Amazon.co.jp](https://www.amazon.co.jp/exec/obidos/ASIN/B07W3M9PR2/nataliecomic-pp-22)

## 特典収録内容

完全生産限定版

*   特製Booklet（92P）
*   Staff audio commentary  
    出演：こだま兼嗣（総監督）、加藤陽一（脚本）、小形尚弘（プロデューサー）、若林豪（プロデューサー）  
    字幕：Barrier-free日本語字幕、“もっこり”かけ声応援上映会用歌詞字幕
*   特典映像
*   予告編＆CM集
*   Non-creditOP
*   Non-credit ED「Get Wild」 Full Screen Ver.
*   舞台挨拶映像
*   縮小版アフレコ台本
*   Events参加券・豪華商品が当たる応募ハガキ

通常版

*   Staff audio commentary  
    出演：こだま兼嗣（総監督）、加藤陽一（脚本）、小形尚弘（プロデューサー）、若林豪（プロデューサー）  
    字幕：Barrier-free日本語字幕、“もっこり”かけ声応援上映会用歌詞字幕

## Story / Cast / Staff

### Story

裏社会No.1の腕を持つCityHunter冴羽獠は、普段は新宿に事務所を構え、相棒の槇村香と様々な依頼を受けている。  
拥有黑社会No.1技术的CityHunter冴羽獠，平时在新宿出没，与搭档槙村香接受各种各样的委托。  

そこに、何者かに襲われたモデル・進藤亜衣がボディーガードを依頼にやってきた。  
这时，被人袭击的模特进藤亚衣前来请求保护。  

美女の依頼を快諾した獠だが、撮影スタジオで更衣室を覗いたり、もっこり全開のやりたい放題……。  
爽快地答应了美女的要求的獠，在摄影棚偷看更衣室，完全嚣张地为所欲为……。

亜衣がキャンペーンモデルを務めるIT企業の社長・御国真司は、なんと香の幼なじみ。  
亚衣担任运动模特的IT企业社长·御国真司，竟然是香的青梅竹马。

撮影現場で久々に香と再会した御国は彼女をデートに誘う。  
在拍摄现场与久违的香再会的御国邀请她去约会。  

しかし、獠は香に無関心で亜衣にスケベ心丸出し……。  
但是，獠对香毫不关心，对亚衣露出好色之心……。  

一方、海坊主と美樹は傭兵が新宿に集結するという情報を入手した。そして傭兵たちはなぜか亜衣を狙うのだった……。  
另一方面，海坊主和美树得到了佣兵将在新宿集结的情报。然后佣兵们不知为何瞄准了亚衣……。  

敵の正体を探る冴子が直面する巨大な陰謀！  
探寻敌人真面目的冴子面临的巨大阴谋!  

来日する大物武器商人・ヴィンス・イングラードと最新兵器──。  
来日本的重量级武器商人Vince Ingrado和最新武器——。  

御国の登場により、すれ違う獠と香。  
随着御国的登场，獠与香擦身而过。  

CityHunterは亜衣と新宿を護りぬくことができるのか!?  
CityHunter能保护亚衣和新宿吗!?  

### Cast

冴羽獠：神谷明

槇村香：伊倉一恵

進藤亜衣：飯豊まりえ

御国真司：山寺宏一

野上冴子：一龍斎春水

海坊主：玄田哲章

美樹：小山茉美

Vince Ingrado：大塚芳忠

Konita：徳井義実（チュートリアル）

来生瞳・来生泪：戸田恵子

来生愛：坂本千夏

### Staff

原作：北条司

総監督：こだま兼嗣

脚本：加藤陽一

Chief演出：佐藤照雄、京極尚彦

Character Design：高橋久美子、菱沼義仁

総作画監督：菱沼義仁

美術監督：加藤浩（ととにゃん）

色彩設計：久保木裕一

撮影監督：長田雄一郎

編集：今井大介（JAYFILM）

音楽：岩崎琢

音響監督：長崎行男

音響制作：AUDIO PLANNING U

Animation 制作：Sunrise

配給：Aniplex

*   [アニメ「劇場版CityHunter <新宿Private Eyes>」公式サイト](https://cityhunter-movie.com/)
*   [アニメ「劇場版CityHunter ＜新宿Private Eyes＞」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)
*   [「劇場版CityHunter <新宿Private Eyes>」作品情報](https://natalie.mu/eiga/film/175572)

©北条司/NSP・「2019 劇場版CityHunter」製作委員会


## 神谷明（カミヤアキラ）

1946年9月18日生まれ、神奈川県出身。1970年にTVアニメ「魔法のマコちゃん」の少年C役でアニメ声優デビュー。「バビル二世」で初の主役を担当。1980年代には「キン肉マン」のキン肉マン、「北斗の拳」のケンシロウ、「シティーハンター」の冴羽獠など、週刊少年ジャンプ（集英社）作品に数多く出演。そのほかの代表作に「ゲッターロボ」の流竜馬、「宇宙戦艦ヤマト」の加藤三郎、「うる星やつら」の面堂終太郎、「めぞん一刻」の三鷹瞬、「名探偵コナン」毛利小五郎（初代）などがある。バラエティー番組のナレーションや、テレビ、ラジオ、Web等のCMナレーション、イベントや講演会なども多数行っている。  
1946年9月18日出生，神奈川县人。1970年在TV动画《魔法真子》中饰演少年C而作为动画声优出道。在《巴维尔二世》中首次担任主角。1980年代出演《筋肉人》中的筋肉人、《北斗神拳》中的健四郎、《城市猎人》中的冴羽獠等，出演了许多《周刊少年jump》(集英社)的作品。其他代表作还有《盖特机器人》的流龙马、《宇宙战舰大和号》的加藤三郎、《福星小子》的面堂终太郎、《相聚一刻》的三鹰瞬、《名侦探柯南》的毛利小五郎(初代)等。担任娱乐节目的旁白，电视、广播、Web等广告的旁白，也多次参加活动和演讲会等。

*   [神谷明事務所 冴羽商事 ｜ Top page](http://www.tokyosaeba.com/)
*   [神谷明Official Blog「神谷明の屁の突っ張りはいらんですよ！！」Powered by Ameba](https://ameblo.jp/kamiya-akira/)
*   [神谷明 (@kamiyaakira29) | Twitter](https://twitter.com/kamiyaakira29)
*   [神谷明の記事まとめ（神谷明的报道总结）](https://natalie.mu/comic/artist/60997)


記事内に記載されている商品の価格は、公開日現在のものです。  
文章中提到的产品价格是发表之日的价格。  

---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
