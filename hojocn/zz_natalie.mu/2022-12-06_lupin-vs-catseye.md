https://natalie.mu/comic/news/504012

# 「ルパン三世VSキャッツ・アイ」1月配信開始！キャスト＆PV公開、来生泪役は深見梨加
「Lupin三世VS Cat'sEye」1月开始发布！出演者&PV公开，来生泪角色是深见梨加  

2022年12月6日   [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[モンキー・パンチ](https://natalie.mu/comic/artist/1749)原作によるアニメ「ルパン三世」と[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「キャッツ・アイ」のコラボアニメ映画「ルパン三世VSキャッツ・アイ」が、2023年1月27日にPrime Videoで世界独占配信スタート。キービジュアル、PV第2弾、追加キャスト情報が解禁された。
由Monkey Punch原作动画《鲁邦三世》和北条司原作动画《猫眼》合作的动画电影《鲁邦三世VS猫眼》将于2023年1月27日推出PrimeVideo开始全球独家配信。key visual、PV第2弹、追加Cast信息解禁。  

[
![「ルパン三世 VS キャッツ・アイ」キービジュアル (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/keyvisual.jpg?imwidth=468&imdensity=1)
「ルパン三世 VS キャッツ・アイ」キービジュアル (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会  
大きなサイズで見る（全44件）  
《鲁邦三世VS猫眼》key visual(c)北条司/鲁邦三世VS猫眼制作委员会  
大尺寸观看(共44件)  
](https://natalie.mu/comic/gallery/news/504012/1953846)

[
![「ルパン三世 VS キャッツ・アイ」メインカット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/main.jpg?imwidth=468&imdensity=1)
「ルパン三世 VS キャッツ・アイ」Main Cut (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会［拡大］  
《鲁邦三世VS猫眼》主镜头(c) Monkey Punch北条司/鲁邦三世VS猫眼制作委员会[放大]  
](https://natalie.mu/comic/gallery/news/504012/1953845)

ルパン三世役の[栗田貫一](https://natalie.mu/comic/artist/6789)、来生瞳役の[戸田恵子](https://natalie.mu/comic/artist/16682)に加え、次元大介役は[大塚明夫](https://natalie.mu/comic/artist/26700)、石川五ェ門役は[浪川大輔](https://natalie.mu/comic/artist/45738)、峰不二子役は[沢城みゆき](https://natalie.mu/comic/artist/65954)、銭形警部役は[山寺宏一](https://natalie.mu/comic/artist/30965)、来生泪役は[深見梨加](https://natalie.mu/comic/artist/96805)、来生愛役は[坂本千夏](https://natalie.mu/comic/artist/95232)、内海俊夫役は[安原義人](https://natalie.mu/comic/artist/91212)、永石役は[麦人](https://natalie.mu/comic/artist/95976)が担当。そのほか[銀河万丈](https://natalie.mu/comic/artist/95632)、[東地宏樹](https://natalie.mu/comic/artist/96008)、[菅生隆之](https://natalie.mu/comic/artist/96023)の出演も明らかになった。また主題歌は[杏里](https://natalie.mu/comic/artist/8220)／ANRI「CAT’S EYE2023」、オープニングテーマ曲は[fox capture plan](https://natalie.mu/comic/artist/10729)「THEME FROM LUPIN vs CAT’S EYE」に決定。両アーティストからはコメントも寄せられている。  
栗田贯一饰演鲁邦三世、户田惠子饰演来生瞳、大冢明夫饰演次元大介、浪川大辅饰演石川五右卫门、泽城美雪饰演峰不二子、山寺宏一饰演钱形警部、深见梨加饰演来生泪、坂本千夏饰演来生爱、安原义饰演内海俊夫、永石由麦人担当。此外，银河万丈、东地宏树、菅生隆之也将出演本剧。另外，主题曲定为杏里/ ANRI的《CAT’S EYE 2023》，片头主题曲决定为fox capture plan的《THEME FROM LUPIN vs CAT’S EYE》。两位艺术家也发表了评论。  

「ルパン三世VSキャッツ・アイ」は、「ルパン三世」アニメ化50周年と「キャッツ・アイ」の原作40周年を記念して展開されるもの。「キャッツ・アイ」連載当時の1980年代を舞台に、泥棒と怪盗のレトロでスタイリッシュなクライム・アクションが繰り広げられる。  
《鲁邦三世VS猫眼》是为纪念《鲁邦三世》动画化50周年和《猫眼》原作40周年而展开的。影片以《猫眼》连载当时的1980年代为舞台，讲述了小偷和怪盗之间复古而时尚的犯罪动作故事。  


## 杏里／ANRIコメント

[
![杏里／ANRI](https://ogre.natalie.mu/media/news/comic/2022/1206/Anri_1.jpg?imwidth=468&imdensity=1)
杏里／ANRI［拡大］
](https://natalie.mu/comic/gallery/news/504012/1953844)

ルパン三世とキャッツによる華麗なるコラボレーション作品のために新録した主題歌「CAT'S EYE 2023」は、オリジナルに寄り添いながらも、今の時代の「CAT'S EYE」に仕上がっています。どうぞお楽しみに！  
为鲁邦三世和Cat's的华丽的合作作品新录的主题曲“CAT'S EYE 2023”，在贴近原作的同时，也完成了当今时代的“CAT'S EYE”。敬请期待!  

## fox capture planコメント

[
![fox capture plan](https://ogre.natalie.mu/media/news/music/2021/0722/foxcaptureplan_art202107.jpg?imwidth=468&imdensity=1)
fox capture plan［拡大］
](https://natalie.mu/comic/gallery/news/504012/1633522)

大好きな作品に参加できると知った時は正直興奮しました。  
本作はルパンとキャッツアイ（fcpがリアレンジ）とで音楽もVSしています。  
両者のクールさと華麗さが楽しめると思います。  
そしてテーマ曲にも気合い入ってます！（カワイヒデヒロ）  
知道能参加最喜欢的作品的时候说实话很兴奋。  
本片的音乐是鲁邦和猫眼之间的VS（由fcp重新编排）。（译注：待校对）  
我想能享受两者的酷和华丽。  
而且主题曲也很有气势!(カワイヒデヒロ)（译注：待校对）   


この記事の画像・動画（全44件）  
这篇报道的图片和视频(共44件)  

[![「ルパン三世 VS キャッツ・アイ」キービジュアル (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/keyvisual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953846 "「ルパン三世 VS キャッツ・アイ」キービジュアル (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」メインカット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953845 "「ルパン三世 VS キャッツ・アイ」メインカット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953847 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953848 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953849 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953850 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953851 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953852 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_013.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953853 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_014.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953854 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_010.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953855 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_011.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953864 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_012.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953866 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953867 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_009.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953868 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![「ルパン三世 VS キャッツ・アイ」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_007.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953869 "「ルパン三世 VS キャッツ・アイ」第2弾PVより。")
[![ルパン三世（CV：栗田貫一）。天才的泥棒の家系に生を受け、世界中をまたにかける大泥棒。どんなターゲットも奇想天外なアイディアと大胆不敵なテクニックで手に入れる。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953840 "ルパン三世（CV：栗田貫一）。天才的泥棒の家系に生を受け、世界中をまたにかける大泥棒。どんなターゲットも奇想天外なアイディアと大胆不敵なテクニックで手に入れる。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![次元大介（CV：大塚明夫）。早撃ち0.3秒の天才ガンマン。その卓越したガンさばきは、ルパン三世も一目置く。窮地に陥っても常に冷静に状況を把握して、難局を打破する男。酒と煙草をこよなく愛する。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953837 "次元大介（CV：大塚明夫）。早撃ち0.3秒の天才ガンマン。その卓越したガンさばきは、ルパン三世も一目置く。窮地に陥っても常に冷静に状況を把握して、難局を打破する男。酒と煙草をこよなく愛する。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![石川五ェ門（CV：浪川大輔）。約400年前に大怪盗を輩出した石川家の末裔。斬鉄剣を自在に操る居合の達人であり、剣の道を究めるためにひたすら修行の道を歩む誇り高き剣豪。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953835 "石川五ェ門（CV：浪川大輔）。約400年前に大怪盗を輩出した石川家の末裔。斬鉄剣を自在に操る居合の達人であり、剣の道を究めるためにひたすら修行の道を歩む誇り高き剣豪。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![峰不二子（CV：沢城みゆき）。さまざまな顔を持つ謎の美女。類まれなる美貌と抜群のプロポーションを持ちながら、ルパンさえも手玉にとる天才的頭脳をあわせ持つ。行動は単純で、己の欲望に従っているだけ。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_007.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953836 "峰不二子（CV：沢城みゆき）。さまざまな顔を持つ謎の美女。類まれなる美貌と抜群のプロポーションを持ちながら、ルパンさえも手玉にとる天才的頭脳をあわせ持つ。行動は単純で、己の欲望に従っているだけ。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![銭形警部（CV：山寺宏一）。ICPO（インターポール）のルパン三世専任捜査官であり、江戸時代の有名な岡っ引きの子孫。ルパン逮捕に執念を燃やし、ルパンの事なら誰よりも熟知している。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953834 "銭形警部（CV：山寺宏一）。ICPO（インターポール）のルパン三世専任捜査官であり、江戸時代の有名な岡っ引きの子孫。ルパン逮捕に執念を燃やし、ルパンの事なら誰よりも熟知している。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")


[![来生瞳（CV：戸田恵子）。キャッツアイ三姉妹の次女。爽やかにロングヘアーをなびかせる美人。抜群の身体能力を活かし、華麗に盗みを遂行する。刑事の俊夫とは学生の時から恋人関係にある。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_009.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953832 "来生瞳（CV：戸田恵子）。キャッツアイ三姉妹の次女。爽やかにロングヘアーをなびかせる美人。抜群の身体能力を活かし、華麗に盗みを遂行する。刑事の俊夫とは学生の時から恋人関係にある。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![来生泪（CV：深見梨加）。キャッツアイ三姉妹の長女。大人の色気を漂わせる、グラマラスな美女。その明晰な頭脳を活かし、作戦の司令塔を担う。優れた変装技術も持っている。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_010.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953833 "来生泪（CV：深見梨加）。キャッツアイ三姉妹の長女。大人の色気を漂わせる、グラマラスな美女。その明晰な頭脳を活かし、作戦の司令塔を担う。優れた変装技術も持っている。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![来生愛（CV：坂本千夏）。キャッツアイ三姉妹の三女。元気いっぱい、ボーイッシュな女の子。メカやITに強く、自作ギミックで盗みを成功に導く。三姉妹の中で唯一、父親との思い出がない。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_011.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953843 "来生愛（CV：坂本千夏）。キャッツアイ三姉妹の三女。元気いっぱい、ボーイッシュな女の子。メカやITに強く、自作ギミックで盗みを成功に導く。三姉妹の中で唯一、父親との思い出がない。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![内海俊夫（CV：安原義人）。瞳の恋人で、キャッツアイ事件を担当する犬鳴署の刑事。お調子者だが正義感は強く、キャッツアイ逮捕に執念を燃やす熱血漢。銭形に憧れを抱いている。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_012.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953842 "内海俊夫（CV：安原義人）。瞳の恋人で、キャッツアイ事件を担当する犬鳴署の刑事。お調子者だが正義感は強く、キャッツアイ逮捕に執念を燃やす熱血漢。銭形に憧れを抱いている。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![永石（CV：麦人）。ハインツから恩義を受け、三姉妹を陰から支える紳士。キャッツアイの盗みのサポートや、ハインツコレクションの管理などを行っている。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953841 "永石（CV：麦人）。ハインツから恩義を受け、三姉妹を陰から支える紳士。キャッツアイの盗みのサポートや、ハインツコレクションの管理などを行っている。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![ハインリッヒ・ベルガー。喫茶キャッツアイを訪れる初老の画商。キャッツ三姉妹に絵画「花束と少女」にまつわる依頼を持ち込む。失踪前のハインツを知っている。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953838 "ハインリッヒ・ベルガー。喫茶キャッツアイを訪れる初老の画商。キャッツ三姉妹に絵画「花束と少女」にまつわる依頼を持ち込む。失踪前のハインツを知っている。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![デニス・キルヒマン。裏世界を暗躍する武器商「ファーデン」の幹部。元傭兵で、目的のためなら手段を択ばない冷酷な男。ルパンやキャッツアイの前に強敵として立ちはだかる。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953839 "デニス・キルヒマン。裏世界を暗躍する武器商「ファーデン」の幹部。元傭兵で、目的のためなら手段を択ばない冷酷な男。ルパンやキャッツアイの前に強敵として立ちはだかる。(c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![杏里／ANRI](https://ogre.natalie.mu/media/news/comic/2022/1206/Anri_1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1953844 "杏里／ANRI")
[![fox capture plan](https://ogre.natalie.mu/media/news/music/2021/0722/foxcaptureplan_art202107.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1633522 "fox capture plan")
[![「ルパン三世 VS キャッツ・アイ」キービジュアル (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/teaser_visual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905449 "「ルパン三世 VS キャッツ・アイ」キービジュアル (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/01c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905460 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット](https://ogre.natalie.mu/media/news/comic/2022/0922/02a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905459 "「ルパン三世 VS キャッツ・アイ」場面カット")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/03a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905458 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/04c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905457 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/05b.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905456 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/06c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905455 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/07c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905454 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/08c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905453 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/09c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905452 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/10a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905451 "「ルパン三世 VS キャッツ・アイ」場面カット (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」ロゴ (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/lvc_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/504012/1905450 "「ルパン三世 VS キャッツ・アイ」ロゴ (c)モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会")
[![「ルパン三世 VS キャッツ・アイ」第2弾PV](https://i.ytimg.com/vi/zZAsIu9oBIo/default.jpg)](https://natalie.mu/comic/gallery/news/504012/media/84700 "「ルパン三世 VS キャッツ・アイ」第2弾PV")
[![「ルパン三世 VS キャッツ・アイ」第1弾PV](https://i.ytimg.com/vi/6s5QLmAc7gM/default.jpg)](https://natalie.mu/comic/gallery/news/504012/media/81880 "「ルパン三世 VS キャッツ・アイ」第1弾PV")

## Amazon Original「ルパン三世 VS キャッツ・アイ」

2023年1月27日（金）よりPrime Videoにて世界独占配信

### スタッフ

原作：[モンキー・パンチ](https://natalie.mu/comic/artist/1749)『ルパン三世』/[北条司](https://natalie.mu/comic/artist/2405)『キャッツ・アイ』  
監督：[静野孔文](https://natalie.mu/comic/artist/62364)、[瀬下寛之](https://natalie.mu/comic/artist/92064)  
脚本：葛原秀治  
副監督：井手惠介  
キャラクターデザイン：[中田春彌](https://natalie.mu/comic/artist/11043)、山中純子  
プロダクションデザイン：田中直哉、フェルディナンド・パトゥリ  
アートディレクター：片塰満則  
編集：肥田文  
音響監督：清水洋史  
音楽：[大野雄二](https://natalie.mu/comic/artist/105386)、大谷和夫、[fox capture plan](https://natalie.mu/comic/artist/10729)  
アニメーション制作：トムス・エンタテインメント  
アニメーション制作協力：萌  
製作：ルパン三世 VS キャッツ・アイ製作委員会

### キャスト

ルパン三世：[栗田貫一](https://natalie.mu/comic/artist/6789)  
次元大介：[大塚明夫](https://natalie.mu/comic/artist/26700)  
石川五ェ門：[浪川大輔](https://natalie.mu/comic/artist/45738)  
峰不二子：[沢城みゆき](https://natalie.mu/comic/artist/65954)  
銭形警部：[山寺宏一](https://natalie.mu/comic/artist/30965)  
来生瞳：[戸田恵子](https://natalie.mu/comic/artist/16682)  
来生泪：[深見梨加](https://natalie.mu/comic/artist/96805)  
来生愛：[坂本千夏](https://natalie.mu/comic/artist/95232)  
内海俊夫：[安原義人](https://natalie.mu/comic/artist/91212)  
永石：[麦人](https://natalie.mu/comic/artist/95976)  
[銀河万丈](https://natalie.mu/comic/artist/95632)  
[東地宏樹](https://natalie.mu/comic/artist/96008)  
[菅生隆之](https://natalie.mu/comic/artist/96023)