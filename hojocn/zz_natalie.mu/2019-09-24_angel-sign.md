https://natalie.mu/eiga/news/348696

# 北条司「エンジェルサイン」予告公開、DEAN FUJIOKA担当の主題歌に松下奈緒も参加

2019年9月24日  [Comment](https://natalie.mu/eiga/news/348696/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


「シティーハンター」で知られるマンガ家・[北条司](https://natalie.mu/eiga/artist/2405)が総監督を務める実写映画「[エンジェルサイン](https://natalie.mu/eiga/film/182396)」の主題歌をDEAN FUJIOKAが担当することが明らかに。また予告編がYouTubeで公開された。
以《城市猎人》而闻名的漫画家北条司担任总导演的真人电影《天使印记》的主题曲将由DEAN FUJIOKA演唱。预告片也在YouTube上公开了。


[
![「エンジェルサイン」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」ポスタービジュアル  
大きなサイズで見る（全16件）  
“天使印记”海报视觉效果(共16件)  
](https://natalie.mu/eiga/gallery/news/348696/1222121)

[
「エンジェルサイン」［拡大］
](https://natalie.mu/eiga/gallery/news/348696/1189295)

本作は奇跡の訪れを告げるブルーバタフライと音楽が鍵を握る6つの物語からなるオムニバス作品。全編通してセリフはなく、映像と音楽でストーリーが展開するサイレント映画となる。「プロローグ」「エピローグ」では北条自身がメガホンを取り、「別れと始まり」「空へ」「30分30秒」「父の贈り物」「故郷へ」では、世界中から寄せられたサイレントマンガオーディションの受賞作品をアジア各国の監督が映像化した。「プロローグ」「エピローグ」では[松下奈緒](https://natalie.mu/eiga/artist/11381)と[ディーン・フジオカ](https://natalie.mu/eiga/artist/65834)が主演している。  
这部综合电影由六个故事组成，其中一只蓝色的蝴蝶宣布了一个奇迹的到来，音乐起到了关键作用。全篇没有台词，是用影像和音乐展开故事的无声电影。《序幕》、《结尾》由北条亲自掌镜，《离别与开始》、《天空》、《30分30秒》、《父亲的礼物》、《故乡》则是亚洲各国从世界各地寄来的无声漫画甄选的获奖作品。卢导演拍成了影像。《序幕》和《尾声》由松下奈绪和藤冈靛主演。

主題歌「Chasing A Butterfly feat. Nao Matsushita」を聴くことができる予告映像には、松下演じるチェリストのアイカとフジオカ演じるピアニストのタカヤの演奏シーンが収められた。さらに[緒形直人](https://natalie.mu/eiga/artist/11951)、[菊池桃子](https://natalie.mu/eiga/artist/34588)、[佐藤二朗](https://natalie.mu/eiga/artist/35114)ら共演陣の姿も。  
主题曲《Chasing A Butterfly feat. Nao Matsushita》的预告影像中，收录了松下饰演的大提琴家爱香和藤冈饰演的钢琴家高也的演奏场面。还有绪形直人、菊池桃子、佐藤二朗等共演阵容。

DEAN FUJIOKAはタカヤが遺した楽曲“Angel Sign”を挙げ、「“Angel Sign”の次に、もしもタカヤがもう一曲だけ作っていたとしたらどんな楽曲を紡ぎ出したのだろうか？ すべてはその想像から始まりました」と主題歌制作の経緯を明かした。ピアノ演奏と歌で楽曲に参加した松下は「演奏するにあたり、アイカのタカヤを想う気持ちをピアノにのせました。なかなか難しかったですが、ディーンさんが先にレコーディングして下さっていたので、その歌に寄り添って行けたらいいなと思いながら、レコーディングしました」と振り返っている。  
DEAN FUJIOKA提到了高屋留下来的歌曲《Angel Sign》，“在《Angel Sign》之后，如果高谷再创作一首歌的话会谱写出怎样的乐曲呢?一切都是从那个想象开始的”阐明了主题曲制作的经过。松下以钢琴演奏和歌曲的形式参与了乐曲的演奏，他说:“在演奏的时候，钢琴承载了我对爱卡的思念之情。虽然很难，但是Dean先录制了这首歌，所以我一边想着如果能贴近那首歌就好了，一边录制了这首歌”。  

[落合賢](https://natalie.mu/eiga/artist/67170)、[ノンスィー・ニミブット](https://natalie.mu/eiga/artist/30201)、ハム・トラン、[旭正嗣](https://natalie.mu/eiga/artist/41796)、[カミラ・アンディニ](https://natalie.mu/eiga/artist/95113)が監督に名を連ねた「エンジェルサイン」は東京のユナイテッド・シネマ豊洲ほかで11月15日に公開。  
落合贤、侬西·尼米布托、哈姆·托兰、旭正嗣、卡米拉·安迪尼担任导演的《天使印记》将于11月15日在东京的丰洲联合电影院等地上映。  

## DEAN FUJIOKA Comment

この映画のメインテーマ曲“Angel Sign”の次に、もしもタカヤがもう一曲だけ作っていたとしたらどんな楽曲を紡ぎ出したのだろうか？ すべてはその想像から始まりました。例え命が尽きても、相手を想う気持ちは変わらず、未来に向かって歩んで欲しい。そう切に願う登場人物たちの気持ちに寄り添いながら、この映像作品を象徴するような主題歌を生み出すべく制作に当たりました。映画本編でもご一緒させて頂いた松下奈緒さんをフィーチャーしたこの“Chasing A Butterfly”。6つの愛の物語を結ぶ“ブルーバタフライ”の行き先を示すような存在になってくれればと願っています。  
这部电影的主题曲《Angel Sign》之后，如果高屋再创作一首歌的话，会谱写出怎样的乐曲呢?一切都是从那个想象开始的。即使生命已尽，思念对方的心情也不会改变，希望你能走向未来。一边贴近这样恳切祈愿的登场人物们的心情，一边创作出象征这部影像作品的主题歌。这首《Chasing A Butterfly》是以在电影正编中也与我一起出演的松下奈绪为主题的作品。希望你能成为连结6个爱的故事的“蓝蝴蝶”指明目的地的存在。



## 松下奈緒 Comment

今回は、ディーンさんが作詞作曲され、2人で歌える事が嬉しかったです。初めて楽曲を聞いた時からメロディーがずっと頭から離れず、気づいたら口ずさんでいました。とてもステキな曲で、さすがディーンさんだなと思いました。演奏するにあたり、アイカのタカヤを想う気持ちをピアノにのせました。なかなか難しかったですが、ディーンさんが先にレコーディングして下さっていたので、その歌に寄り添って行けたらいいなと思いながら、レコーディングしました。  
这次，Dean先生作词作曲，能两个人唱歌，我很高兴。从第一次听乐曲的时候开始旋律就一直在脑海中挥之不去，注意到的时候已经哼唱了。非常棒的曲子，不愧是Dean。在演奏的时候，把高屋思念爱卡的心情放在了钢琴上。虽然很难，但是因为Dean先录制了，所以我一边想着如果能贴近那个歌就好了，一边录制了。  


この記事の画像・動画（全16件）  
这篇报道的图片、视频(共16篇)  

[![「エンジェルサイン」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222121 "「エンジェルサイン」ポスタービジュアル")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1189295 "「エンジェルサイン」")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1189290 "「エンジェルサイン」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222122 "「別れと始まり」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222123 "「別れと始まり」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222124 "「父の贈り物」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222125 "「空へ」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222126 "「空へ」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222127 "「30分30秒」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222128 "「30分30秒」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222129 "「父の贈り物」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222130 "「故郷へ」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222131 "「故郷へ」")
[![左から緒形直人、菊池桃子、佐藤二朗。](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1222132 "左から緒形直人、菊池桃子、佐藤二朗。")
[![左からディーン・フジオカ、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/0924/angelsign_201909_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/348696/1244280 "左からディーン・フジオカ、松下奈緒。")
[![「エンジェルサイン」予告編](https://i.ytimg.com/vi/4gFDdfZBS7A/default.jpg)](https://natalie.mu/eiga/gallery/news/348696/media/42069 "「エンジェルサイン」予告編")

(c)「エンジェルサイン」製作委員会

LINK  

[「エンジェルサイン」公式サイト](https://angelsign.jp/)  
[AngelSignMovie (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[「エンジェルサイン」予告編](https://youtu.be/4gFDdfZBS7A)  