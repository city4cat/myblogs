https://natalie.mu/comic/news/243684

# 今と昔の冴羽リョウがダ・ヴィンチで共演、北条司描き下ろし＆インタビュー
今昔的冴羽獠在daVinci共演，北条司新画&采访  

2017年8月5日  [Comment](https://natalie.mu/comic/news/243684/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


本日8月5日に発売されたダ・ヴィンチ9月号（KADOKAWA）では、[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」「エンジェル・ハート」についての特集が組まれている。  
今天8月5日发售的daVinci 9月号(KADOKAWA)上，制作了关于北条司“城市猎人”、“天使之心”的特辑。

[
![ダ・ヴィンチ9月号](https://ogre.natalie.mu/media/news/comic/2017/0805/davinci9.jpg?imwidth=468&imdensity=1)
ダ・ヴィンチ9月号  
大きなサイズで見る  
达·芬奇9月号  
大尺寸查看。  
](https://natalie.mu/comic/gallery/news/243684/752512)

1985年に連載開始した「シティーハンター」から、今年完結を迎えた「エンジェル・ハート」までの32年間を追った本特集。扉ページには北条により描き下ろしイラストが提供され、連載当初の扉絵として描かれた冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）の肩に手をかける32年後の冴羽リョウが登場した。また北条へのロングインタビューをはじめ、アニメで冴羽リョウ役を務めた[神谷明](https://natalie.mu/comic/artist/60997)やエンディングテーマを手がけた[小室哲哉](https://natalie.mu/comic/artist/860)らからの寄稿も。冴羽リョウのパートナーや家族、新宿の街にスポットを当てたコンテンツも展開され、全24ページとボリュームたっぷりに仕上がった。  
本特辑追踪了从1985年开始连载的“城市猎人”到今年完结的“天使心”的32年间。在扉页上提供了由北条绘制的插图，32年后，在作为连载之初的扉页图描绘的冴羽獠(獠的汉字是“僚”的制作)被32年后的冴羽獠搭肩。另外，还有以对北条的长篇采访为首、在动画中担任冴羽獠一角的神谷明和创作片尾曲的小室哲哉等人的供稿。这期杂志共24页，内容也很丰富，主要是关于冴羽獠的伙伴、家庭和新宿市的情况。  
（译注：待校对。如下图  
![](img/1709saeba.jpg) 
[该图片的链接](https://web.archive.org/web/20210125010912/https://ddnavi.com/news/391770/a/) )


そのほか今号のダ・ヴィンチでは、「いま、このヒロインに弄ばれたい。」と題された特集も。[中村勇志](https://natalie.mu/comic/artist/101319)「六道の悪女（おんな）たち」や、[西森博之](https://natalie.mu/comic/artist/4146)「柊様は自分を探している。」のキャラクターたちの名が挙げられた。また「からかい上手の高木さん」の[山本崇一朗](https://natalie.mu/comic/artist/10635)へのインタビューも実施され、同作のアニメ化や昨年完結した「ふだつきのキョーコちゃん」について語られている。  
除此之外，在这一期的daVinci，还有题为“现在，我想被这个女主角玩弄。“的专题。中村勇志《六道的恶女们》和西森博之《Se大人正在寻找自己。》的角色的名字。另外，还对《擅长戏弄的高木》的山本崇一朗进行了采访，讲述了该作品的动画化和去年完结的“Fudatsuki的kyoko”。

LINK

[daVinci　2017年9月号：雑誌 | KADOKAWA](http://www.kadokawa.co.jp/product/321702000321/)

関連商品

[
![「daVinci 2017年9月号」](https://images-fe.ssl-images-amazon.com/images/I/614HHhqsklL._SS70_.jpg)
「daVinci 2017年9月号」  
[雑誌] 2017年8月5日発売 / KADOKAWA / 491-0059870973  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B073LC54R1/nataliecomic-22)

[
![「daVinci 2017年9月号 Kindle版」](https://images-fe.ssl-images-amazon.com/images/I/51URU8vyDqL._SS70_.jpg)
「daVinci チ2017年9月号 Kindle版」  
[電子書籍] 2017年8月5日発売 / KADOKAWA  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B074K2X42X/nataliecomic-22)

译注：  该杂志中与北条司相关的目录如下：     
●特集2  
『シティーハンター』~『エンジェル・ハート』完結!冴羽獠が見守った32年間  
『City Hunter』~『Angel Heart』完结!冴羽獠陪伴的32年间  
（译注：原文/中译文参见[『City Hunter』~『Angel Heart』完结!冴羽獠陪伴的32年间 |ddnavi](../zz_ddnavi.com/2019-03-01.md)  ）  

◎[描き下ろしイラスト]冴羽獠 1985/2017  
[新插画]冴羽獠1985/2017  

◎『シティーハンター』&『エンジェル・ハート』読み解き  [インタビュー&寄稿]大沢在昌/神谷明/小室哲哉  
『City Hunter』&『Angel Heart』解读  [采访&投稿]大泽在昌/神谷明/小室哲哉    

◎北条 司ロングインタビュー  
北条 司 Long Interview  

◎冴羽獠が駆け抜けた「新宿」の32年間  [インタビュー]さくまあきら/横山昌義  
冴羽獠结束后的“新宿”的32年间  [采访]佐久间/横山昌义  


译注：  

- [【da Vinci 2017年9月号】「冴羽獠陪伴的32年」特集番外篇 |ddnavi](../zz_ddnavi.com/2017-08-05_ryo-32years.md)  