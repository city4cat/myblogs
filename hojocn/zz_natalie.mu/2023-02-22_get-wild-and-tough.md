source:  
https://natalie.mu/eiga/news/513832



# Get wild and tough!「劇場版CityHunter」特報で2023t Hammer炸裂
Get wild and tough!「劇場版CityHunter」特报 2023t Hammer炸裂  

2023年2月22日 
[Comment](https://natalie.mu/eiga/news/513832/comment) 
[映画Natalie編集部](https://natalie.mu/eiga/author/74)

[記事へのComment（8件）](https://natalie.mu/eiga/news/513832/comment)

アニメ「CityHunter」シリーズの新作映画が「[劇場版CityHunter 天使の涙（AngelDust）](https://natalie.mu/eiga/film/189680)」のタイトルで今秋に公開。特報がYouTubeで公開された。  
动画“CityHunter”系列的新电影“剧场版CityHunter 天使之泪（AngelDust）”的标题,今年秋天公开。特报在YouTube上公开了。  

[
![「劇場版CityHunter 天使の涙」場面Cut](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_05.jpg?impolicy=hq&imwidth=730&imdensity=1)
「劇場版CityHunter 天使の涙」場面Cut 大きなサイズで見る（全11件）（《剧场版CityHunter天使之泪》场面Cut大尺寸观看）
](https://natalie.mu/eiga/gallery/news/513832/2003394)

[
「劇場版CityHunter 天使の涙」Visual［拡大］
](https://natalie.mu/eiga/gallery/news/513832/2003397)

[北条司](https://natalie.mu/eiga/artist/2405)のマンガ「CityHunter」をもとにした本作。無類の女好きだが依頼を受ければ並み外れた銃の腕と身体能力、冷静沈着な頭脳で仕事を遂行する超一流スイーパー・冴羽リョウの活躍が描かれる。[神谷明](https://natalie.mu/eiga/artist/60997)、[伊倉一恵](https://natalie.mu/eiga/artist/95562)、[一龍斎春水](https://natalie.mu/eiga/artist/107953)、[玄田哲章](https://natalie.mu/eiga/artist/19455)、[小山茉美](https://natalie.mu/eiga/artist/19454)が声の出演をした。  
北条司的漫画作品“CityHunter”为本作的基础。本剧描写了超一流Sweeper冴羽獠的活跃表现，虽然他无比好色，但只要接受委托，就会以超凡的枪法和身体能力、冷静沉着的头脑来完成工作。神谷明、伊倉一恵、一龍斎春水、玄田哲章、小山茉美担任了配音。  

[
「劇場版CityHunter 天使の涙」場面Cut［拡大］
](https://natalie.mu/eiga/gallery/news/513832/2003392)

[
「劇場版CityHunter 天使の涙」場面Cut［拡大］
](https://natalie.mu/eiga/gallery/news/513832/2003395)

特報のバックには、シリーズのファンにはおなじみの[TM NETWORK](https://natalie.mu/eiga/artist/1270)による楽曲「Get Wild」が。そして香が“100tHammer”ならぬ「祝 新作2023t Hammer」を振り下ろす様子や、リョウの銃撃シーンなどが映し出される。  
特报的背景是TM NETWORK的歌曲「Get Wild」，这是该系列粉丝所熟知的。 影片还展示了香挥下"祝 新作2023t 锤子"，而不是 "100t锤子"，以及獠的开枪场景。  

なお総監督の[こだま兼嗣](https://natalie.mu/eiga/artist/13476)は「究極の戦いが、ここに始まります。アクションたっぷりの映像をお楽しみください」とCommentした。  
总导演こだま兼嗣说："终极战斗从这里开始。 请欣赏这段充满动作的镜头"。  

「劇場版CityHunter 天使の涙」は全国の劇場で上映。2月23日にはB4サイズのクリアポスター付きムビチケカードが発売される。  
「劇場版CityHunter 天使の涙」将在全国影院放映，带有B4尺寸透明海报的Mvtkcard将于2月23日开始销售。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  
※冴羽獠的獠的官方写法是犭的「僚」    

## こだま兼嗣 Comment

アニメ放送開始から36年、もはや避けて通れない冴羽リョウの原点を探る作品となっています。愛ともっこり、そしてHard-boiled。  
在动画片开始播放的36年后，这部作品探讨了再也无法回避的冴羽獠的起源。爱情、莫逆之交和硬派。  

冴羽リョウの前に現れる史上最強の敵。  
历史上最强大的敌人出现在冴羽獠面前。  

傷を負いながらも依頼を遂行しようとする冴羽リョウ。  
尽管有伤在身，冴羽獠还是试图执行他的委托。  

究極の戦いが、ここに始まります。アクションたっぷりの映像をお楽しみください。  
最终的战斗在这里开始。请欣赏这段充满动作的镜头。  




関連する特集・インタビュー（相关专题/访谈）

[
「劇場版CityHunter <新宿プライベート・アイズ>」特集 北条司（原作者）×神谷明（冴羽獠役）インタビュー 2019年2月1日
](./2019-02-01_ch_movie01.md)

[
「劇場版CityHunter <新宿プライベート・アイズ>」玄田哲章インタビュー 2019年2月7日
](./2019-02-07_cityhunter-movie02.md)

[
「劇場版CityHunter <新宿プライベート・アイズ>」特集 神谷明インタビュー 2019年10月25日
](./2019-10-25_cityhunter-movie03.md)

この記事の画像・動画（全11件）

[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003394 "「劇場版CityHunter 天使の涙」場面Cut")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003397 "「劇場版CityHunter 天使の涙」Visual")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003392 "「劇場版CityHunter 天使の涙」場面Cut")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003395 "「劇場版CityHunter 天使の涙」場面Cut")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003390 "「劇場版CityHunter 天使の涙」場面Cut")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003391 "「劇場版CityHunter 天使の涙」場面Cut")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003393 "「劇場版CityHunter 天使の涙」場面Cut")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003396 "「劇場版CityHunter 天使の涙」場面Cut")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003398 "「劇場版CityHunter 天使の涙」ムビチケカードのVisual。")
[![](https://ogre.natalie.mu/media/news/eiga/2023/0221/cityhunter_202302_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513832/2003399 "「劇場版CityHunter 天使の涙」ムビチケ購入特典のVisual。")
[![](https://i.ytimg.com/vi/7O1VR5cbQ_k/default.jpg)](https://natalie.mu/eiga/gallery/news/513832/media/87528 "「劇場版CityHunter 天使の涙」特報")

(c)北条司/Coamix・「2023 劇場版CityHunter」製作委員会


---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处