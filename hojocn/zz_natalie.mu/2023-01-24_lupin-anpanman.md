source:  
https://natalie.mu/eiga/news/510130


# Lupinの次なるコラボはアンパンマン？栗田貫一が戸田恵子の前で宣言
Lupin的下一个合作是面包超人?栗田贯一在户田惠子面前宣布  

2023年1月24日 
 [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


Amazon Original「Lupin三世 VS Cat'sEye」のジャパンプレミア試写会が本日1月24日に東京・TOHOシネマズ 六本木ヒルズで行われ、声優キャストの[栗田貫一](https://natalie.mu/eiga/artist/6789)、[戸田恵子](https://natalie.mu/eiga/artist/16682)、監督を務めた[瀬下寛之](https://natalie.mu/eiga/artist/92064)が登壇した。  
Amazon Original《鲁邦三世VS猫眼》的日本首映式将于今天1月24日在东京·TOHO Cinemas活动在六本木Hills举行，声优栗田贯一、户田惠子、导演濑下宽之出席了活动。

[
![Amazon Original「Lupin三世 VS Cat'sEye」ジャパンプレミア試写会の様子。 左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子、瀬下寛之。  
（Amazon Original《鲁邦三世VS猫眼》日本首映式的样子。左起黑泽和子、村上知子、大岛美幸、栗田贯一、户田惠子、濑下宽之）
](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_25.jpg?impolicy=hq&imwidth=730&imdensity=1)
Amazon Original「Lupin三世 VS Cat'sEye」ジャパンプレミア試写会の様子。 左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子、瀬下寛之。
大きなサイズで見る（全15件）
（Amazon Original《鲁邦三世VS猫眼》日本首映式的样子。左起黑泽和子、村上知子、大岛美幸、栗田贯一、户田惠子、濑下宽之。查看大图(共15件)）
](https://natalie.mu/eiga/gallery/news/510130/1983671)

[
![「Lupin三世 VS Cat'sEye」Main Visual](https://ogre.natalie.mu/media/news/eiga/2022/1206/lupin-vs-catseye_202212_29.jpg?imwidth=468&imdensity=1)
「Lupin三世 VS Cat'sEye」Main Visual［拡大］
](https://natalie.mu/eiga/gallery/news/510130/1953812)

本作は「Lupin三世」のアニメ化50周年、「Cat'sEye」原作開始40周年を記念した長編コラボアニメーション。キャッツアイの父が遺した「三枚の絵」を軸に、絵を狙う泥棒Lupin三世と怪盗三姉妹キャッツアイの夢の対決が繰り広げられる。栗田がLupin、戸田が三姉妹の次女・瞳に声を当て、アニメ映画「GODZILLA」3部作で知られる瀬下と静野孔文が共同で監督を務めた。  
本作品是为纪念《鲁邦三世》动画化50周年、《猫眼》原作开始40周年而推出的长篇合作动画。以猫眼的父亲留下的“三张画”为主轴，以画为目标的小偷鲁邦三世和怪盗三姐妹猫眼展开了梦幻对决。栗田为鲁邦配音，户田为三姐妹中的二女儿瞳配音，以动画电影《格斯拉》三部曲而闻名的濑下和静野孔文共同担任导演。  

[
![栗田貫一](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_02.jpg?imwidth=468&imdensity=1)
栗田貫一［拡大］
](https://natalie.mu/eiga/gallery/news/510130/1983642)

「名探偵コナン」に続いて「Cat'sEye」とコラボした「Lupin三世」。栗田は「コナンがあるなら、絶対Cat'sEyeが来るとは予想してました。うちの（峰）不二子ちゃんの絵面が三姉妹と並んでも遜色がない。ちょっとコナンの中にいると（世界観が）違う？と思ったことがないわけじゃないので……」と笑い、「いずれはアンパンマンの中に入ろうと思ってます。それは最後の砦」と、戸田が長年声優を担う「それいけ！アンパンマン」との次なるコラボを願った。  
继《名侦探柯南》之后，《鲁邦三世》又与猫眼电影合作。栗田笑着说:“如果有柯南的话，我就预料到猫眼电影一定会来，我家(峰)不二子的画面和三姐妹放在一起也毫不逊色。在柯南里面的话(世界观)不一样吗?不是没有这样想过……”，并表示“总有一天会加入到面包超人的队伍中，这是最后的堡垒”，希望能与户田长年担任声优的《就这样吧!面包超人》有下一次合作。

[
![左から栗田貫一、戸田恵子。](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_21.jpg?imwidth=468&imdensity=1)
左から栗田貫一、戸田恵子。［拡大］
](https://natalie.mu/eiga/gallery/news/510130/1983667)

[
![瀬下寛之](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_18.jpg?imwidth=468&imdensity=1)
瀬下寛之［拡大］
](https://natalie.mu/eiga/gallery/news/510130/1983665)

2019年2月に公開された、同じく[北条司](https://natalie.mu/eiga/artist/2405)原作の「劇場版シティーハンター <新宿プライベート・アイズ>」でCat'sEyeの声優として復活を果たしていた戸田は「そのときも、かなりひっくり返りました。今回は『Lupin三世』というビッグタイトルとのコラボで、再びのけぞった感じはある。40年近く経ってるのに、また私たちを起用していただいて、感謝の一言です」と再演を喜ぶ。不二子とCat'sEyeのバトルを予想していたという栗田が「4人の戦いをおじさんが遠目で見ているのをイメージしてたんですけど、実はあまり戦ってない。僕の中では予想を裏切られた」と話すと、瀬下は「まさに、そういうプロットを考えていた」と告白。「ある事件で不二子がぼろぼろになって、泪（るい）姉さんが『替えのレオタードならあるわよ』と。不二子と三姉妹がレオタードを着て並ぶシーンを作ったんですけど、収拾がつかなくなってプロットを変更したんです。この4人の“最強感”を見てみたかったんですが、うまくまとまらなかった」と裏話を披露した。  
2019年2月上映的同为北条司原作的《剧场版城市猎人<新宿Private Eyes>》中，户田以猫眼声优的身份复活，她说:“当时我也很惊讶。这次与《鲁邦三世》这样的大作合作，让人感觉再次穿越回去了。时隔近40年，还能再次起用我们，我想说一句感谢的话”。栗田说她期待着藤子和猫眼之间的战斗“想象叔叔在远处看4人战斗的样子，不过，其实不怎么战斗。与我的预想相反”，濑下坦白道“我确实在考虑这样的情节”。披露了幕后故事“在某个事件中，不二子被打得稀烂，泪姐姐说『有替换的紧身衣哦』。本来制作了不二子和三姐妹穿着紧身衣排列在一起的场景，但后来无法收拾，就改变了情节。虽然想看这4人的‘最强组合’，但是没能很好地整合”。  

[
![Cat'sEyeに扮した森三中の登場シーン。（扮演Cat'sEye的森三中的登场场景。）](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_22.jpg?imwidth=468&imdensity=1)
Cat'sEyeに扮した森三中の登場シーン。［拡大］
（扮演Cat'sEye的森三中的登场场景。[放大]）
](https://natalie.mu/eiga/gallery/news/510130/1983668)

[
![Cat'sEyeに扮した森三中の登場シーン。（扮演Cat'sEye的森三中的登场场景。）](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_03.jpg?imwidth=468&imdensity=1)
Cat'sEyeに扮した森三中の登場シーン。［拡大］
扮演Cat'sEye的森三中的登场场景。[扩大]
](https://natalie.mu/eiga/gallery/news/510130/1983643)

イベント中盤にはシークレットゲストとして[森三中](https://natalie.mu/eiga/artist/7317)が登壇。[村上知子](https://natalie.mu/eiga/artist/53966)、[大島美幸](https://natalie.mu/eiga/artist/53964)、[黒沢かずこ](https://natalie.mu/eiga/artist/53965)の3人は青、紫、オレンジのレオタードを着て登場し、「BIG」のCM以来、12年ぶりにキャッツアイ姿を披露した。  
活动中期，森三中作为神秘嘉宾登台。村上知子、大岛美幸、黑泽かずこ3人身着青、紫、橙三色紧身衣登场，自“BIG”广告以来，时隔12年再次以猫眼造型亮相。  

[
![左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子。](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_24.jpg?imwidth=468&imdensity=1)
左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子。［拡大］
](https://natalie.mu/eiga/gallery/news/510130/1983670)

最後に栗田は「Lupinとキャッツさんたちがうまく駆け引きをして、いい人生ドラマになった感じもあるし、ちょっとメランコリーな空気もある。こんなふうにまとまるとは」と感想を吐露。戸田は長女・泪のオリジナルキャストで、2018年12月に死去した藤田淑子の名前を挙げ「『劇場版シティーハンター』のときは私が瞳と泪姉さんの1人2役だったんですが、今回は深見梨加さんがお姉ちゃんの声をやってくださって。実に見事で心から感謝をしております。いろんな思いがあるCat'sEyeを再び演じることができて、そしてLupinとコラボすることができて、幸せいっぱいです」と話しながら「物語は多少、愛ちゃんにジェラシーを感じる意外な展開。最初はとてもびっくりしつつ、ハートウォーミングで、Lupinに心を奪われました」とアピールした。  
最后栗田吐露感想说:“Lupin和Cats很好地进行了讨价还价，感觉是一部很好的生活剧，也有稍微忧郁的气息。没想到会这样总结。”户田提到了长女·泪的原出演者、2018年12月去世的藤田淑子的名字，“《剧场版城市猎人》的时候我是瞳和泪姐姐一人分饰两角，这次是深见梨加为姐姐配音。非常出色，在此表示衷心的感谢。能够再次演绎有着各种各样想法的Cat'sEye，并且能够和Lupin合作，我感到非常幸福”，“故事的展开多少让我对小爱感到了一些意外。一开始非常吃惊，但是因为Lupin的暖心，我的心被Lupin吸引住了”。（译注：待校对）

「Lupin三世 VS Cat'sEye」は1月27日よりPrime Videoで世界独占配信。  
《Lupin三世VS Cat'sEye》将于1月27日起通过Prime Video全球独家发布。  

この記事の画像・動画（全15件）  
这篇报道的图片、视频(共15篇)  

*   [![Amazon Original「Lupin三世 VS Cat'sEye」Japan Premiere試写会の様子。 左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子、瀬下寛之。](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_25.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983671 "Amazon Original「Lupin三世 VS Cat'sEye」ジャパンプレミア試写会の様子。 左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子、瀬下寛之。")
*   [![「Lupin三世 VS Cat'sEye」キービジュアル](https://ogre.natalie.mu/media/news/eiga/2022/1206/lupin-vs-catseye_202212_29.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1953812 "「Lupin三世 VS Cat'sEye」キービジュアル")
*   [![栗田貫一](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983642 "栗田貫一")
*   [![左から栗田貫一、戸田恵子。](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_21.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983667 "左から栗田貫一、戸田恵子。")
*   [![瀬下寛之](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_18.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983665 "瀬下寛之")
*   [![Cat'sEyeに扮した森三中の登場シーン。](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_22.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983668 "Cat'sEyeに扮した森三中の登場シーン。")
*   [![Cat'sEyeに扮した森三中の登場シーン。](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983643 "Cat'sEyeに扮した森三中の登場シーン。")
*   [![左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子。](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_24.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983670 "左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子。")
*   [![戸田恵子](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_16.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983664 "戸田恵子")
*   [![左から黒沢かずこ、村上知子、大島美幸。](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_23.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983669 "左から黒沢かずこ、村上知子、大島美幸。")
*   [![村上知子](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983649 "村上知子")
*   [![大島美幸](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983647 "大島美幸")
*   [![黒沢かずこ](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983648 "黒沢かずこ")
*   [![黒沢かずこ](https://ogre.natalie.mu/media/news/eiga/2023/0124/lupincatseye_202301_12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/510130/1983652 "黒沢かずこ")
*   [![「Lupin三世 VS Cat'sEye」第2弾PV](https://i.ytimg.com/vi/zZAsIu9oBIo/default.jpg)](https://natalie.mu/eiga/gallery/news/510130/media/84700 "「Lupin三世 VS Cat'sEye」第2弾PV")

(c)Monkey Punch 北条司／Lupin三世 VS Cat'sEye製作委員会

## 読者の反応
读者的反应  

[Comment](https://natalie.mu/eiga/news/510130/comment)





---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处