https://natalie.mu/comic/news/355747

# 仏版「シティーハンター」inコミコンに神谷明が登場、マンガ風予告映像も到着

2019年11月16日  [Comment](https://natalie.mu/comic/news/355747/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)原作によるフランスの実写映画「[シティーハンター THE MOVIE 史上最香のミッション](https://natalie.mu/eiga/film/182791)」が、11月22日から24日にかけて千葉・幕張メッセの国際展示場9・10・11ホールで開催される「東京コミコン2019」に出展する。  
北条司原作的法国真人电影《城市猎人THE MOVIE史上最香的任务》将于11月22日至24日在千叶幕张messe的国际展示场9.10.11大厅举办的“2019年东京动漫展”上展出。  

[
![神谷明](https://ogre.natalie.mu/media/news/comic/2019/1116/kamiya.jpg?imwidth=468&imdensity=1)
神谷明  
大きなサイズで見る（全5件）  
神谷明大尺寸看(全5件)  
](https://natalie.mu/comic/gallery/news/355747/1278150)

[
![「東京コミコン2019」で販売される「CITY HUNTER × TOKYO COMICON 限定《C. H. Y！》Tシャツ」。](https://ogre.natalie.mu/media/news/comic/2019/1116/cityhunter_Tshirts.jpg?imwidth=468&imdensity=1)
「東京コミコン2019」で販売される「CITY HUNTER × TOKYO COMICON 限定《C. H. Y！》Tシャツ」。［拡大］  
“东京动漫展2019”上出售的“CITY HUNTER ×东京动漫展限定《C. H. Y！》T恤”。[扩大]  
](https://natalie.mu/comic/gallery/news/355747/1278147)

本作のブースでは、会場限定の「CITY HUNTER × TOKYO COMICON 限定《C. H. Y！》Tシャツ」を販売。また100tハンマー付きのミニクーパーや、記念撮影できる顔はめポスターパネルが展示される。さらにステッカーが配られるほか、“会場には海坊主らしき奴も登場するかも!?”との告知も打たれている。  
本作品的展位上，出售了限定在会场的“CITY HUNTER × TOKYO COMICON限定《C. H. Y！》T恤”。另外，还将展示附有100t锤子的MiniCooper和可拍照留念的面部海报。此外，除了发放贴纸外，还有“会场可能会有海坊主模样的家伙登场!?”的通告。  

11月24日には、本作の吹替版に参加した[神谷明](https://natalie.mu/comic/artist/60997)登壇のトークイベントを実施。[なだぎ武](https://natalie.mu/comic/artist/6022)、[バッファロー吾郎](https://natalie.mu/comic/artist/7195)の[竹若元博](https://natalie.mu/comic/artist/93157)ら率いるアメコミリーグとともに、神谷のこれまでの活動や、さまざまな展開があった2019年の“シティーハンターイヤー”を振り返る。  
11月24日，参加了本作品配音版的神谷明登坛进行谈话活动。与直木武、Buffalo吾郎的竹若元博率领的美式漫画联盟一起，回顾神谷迄今为止的活动，以及有各种发展的2019年的“City Hunter Year”。  

[
![「シティーハンター THE MOVIE 史上最香のミッション」の「コミック仕様SP予告」。](https://ogre.natalie.mu/media/news/comic/2019/1116/cityhunter_cm.jpg?imwidth=468&imdensity=1)
「シティーハンター THE MOVIE 史上最香のミッション」の「コミック仕様SP予告」。［拡大］  
《城市猎人THE MOVIE史上最香的任务》的“漫画版SP预告”。[扩大]  
](https://natalie.mu/comic/gallery/news/355747/1278148)

「シティーハンター THE MOVIE 史上最香のミッション」は、11月29日より東京・TOHOシネマズ 日比谷ほかにて全国公開。本作の「東京コミコン2019」出展に併せ、YouTubeでは映画のセリフがフキダシで登場する予告映像「コミック仕様SP予告」も公開された。  
《城市猎人THE MOVIE史上最香的任务》将于11月29日在东京·TOHO电影院日比谷等地全国上映。在本作品参展“2019东京动漫展”的同时，在YouTube上也公开了以电影台词为开头的预告影像“Comic规格SP预告”。  

なおコミックナタリーでは「劇場版シティーハンター <新宿プライベート・アイズ>」のBlu-ray / DVD発売を記念した特集を展開中。神谷が静岡にあるディスク工場を訪れた際のレポートやインタビューを掲載している。  
另外，Comic Natalie还推出了纪念《剧场版城市猎人<新宿Private Eyes>》蓝光/ DVD发售的特集。刊登了神谷访问静冈磁盘工厂时的报告和采访。  

この記事の画像・動画（全5件）  
这篇报道的图片、视频(共5篇)  

[![神谷明](https://ogre.natalie.mu/media/news/comic/2019/1116/kamiya.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/355747/1278150 "神谷明")
[![「東京コミコン2019」で販売される「CITY HUNTER × TOKYO COMICON 限定《C. H. Y！》Tシャツ」。](https://ogre.natalie.mu/media/news/comic/2019/1116/cityhunter_Tshirts.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/355747/1278147 "「東京コミコン2019」で販売される「CITY HUNTER × TOKYO COMICON 限定《C. H. Y！》Tシャツ」。")
[![「東京コミコン2019」で配布されるステッカー。](https://ogre.natalie.mu/media/news/comic/2019/1116/cityhunter_sticker.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/355747/1278149 "「東京コミコン2019」で配布されるステッカー。")
[![「シティーハンター THE MOVIE 史上最香のミッション」の「コミック仕様SP予告」。](https://ogre.natalie.mu/media/news/comic/2019/1116/cityhunter_cm.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/355747/1278148 "「シティーハンター THE MOVIE 史上最香のミッション」の「コミック仕様SP予告」。")
[![シティーハンター THE MOVIE 史上最香のミッション【コミック仕様SP予告】](https://i.ytimg.com/vi/5I0f9iWs85c/default.jpg)](https://natalie.mu/comic/gallery/news/355747/media/43408 "シティーハンター THE MOVIE 史上最香のミッション【コミック仕様SP予告】")

## 「シティーハンター THE MOVIE 史上最香のミッション」ブース
“城市猎人THE MOVIE史上最香的任务”展台  

日時：2019年11月22日（金）12:00～20:00、23日（土）10:00～20:00、24日（日）10:00～18:00  
場所：千葉県 幕張メッセ 国際展示場9・10・11ホール(千叶县 幕张messe国际展示场9.10.11厅)  

### 東京COMIC CON Presents 祝・芸歴50周年！ヒーロー声優界のレジェンド神谷明、降臨！  
东京COMIC CON Presents庆祝·演艺经历50周年!英雄声优界的传奇神谷明降临!   

日時：2019年11月24（日）14:45～15:15  
場所：東京コミコン メインステージ  
スペシャルゲスト：[神谷明](https://natalie.mu/comic/artist/60997)  
MC：アメコミリーグ（[なだぎ武](https://natalie.mu/comic/artist/6022)、[バッファロー吾郎](https://natalie.mu/comic/artist/7195)・[竹若元博](https://natalie.mu/comic/artist/93157)、[2丁拳銃](https://natalie.mu/comic/artist/7165)・[川谷修士](https://natalie.mu/comic/artist/52847)、佐藤ピリオド）  
地点:东京动漫展主舞台  
特别嘉宾:神谷明  
MC:美式漫画联盟(田木武、布法罗吾郎·竹若元博、双枪·川谷修士、佐藤毕奥多)



(c)AXEL FILMS PRODUCTION - BAF PROD - M6 FILMS


LINK

[映画『シティーハンター THE MOVIE 史上最香のミッション』公式サイト](http://cityhunter-themovie.com/)  
[シティーハンターTHE MOVIE 史上最香のミッション (@cityhunter\_2019) | Twitter](https://twitter.com/cityhunter_2019)  
[NEWS｜映画『シティーハンター THE MOVIE 史上最香のミッション』公式サイト](https://cityhunter-themovie.com/news/2019/11/15/12/)  
[「東京コミコン2019 | TokyoComicCon」世界最大級のポップ・カルチャーの祭典が今年も開催決定！！  
（“东京展2019 | TokyoComicCon”世界最大级的流行文化的盛典今年也决定举行! !）](https://tokyocomiccon.jp/)  
