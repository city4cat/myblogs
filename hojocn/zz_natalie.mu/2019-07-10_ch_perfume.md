https://natalie.mu/comic/news/339191

# 「シティーハンター」をモチーフにしたユニセックスで使えるオードパルファム発売

2019年7月10日  [Comment](https://natalie.mu/comic/news/339191/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」をモチーフにした香水「シティーハンター　オードパルファム」が、香水の輸入販売を手がけるフェアリーテイルより本日7月10日に発売された。  
北条司原作的动画电影《剧场版城市猎人》以“新宿Private Eyes”为主题的香水“城市猎人Eau de Parfum”，由从事香水进口销售的Fairy Tail于7月10日发售。

[
![「シティーハンター　オードパルファム」イメージビジュアル](https://ogre.natalie.mu/media/news/comic/2019/0710/main-4da8.jpg?imwidth=468&imdensity=1)
「シティーハンター　オードパルファム」イメージビジュアル  
大きなサイズで見る（全6件）  
“城市猎人”图像视觉  
大尺寸查看(共6件)  
](https://natalie.mu/comic/gallery/news/339191/1197293)

[

![「シティーハンター　Eau de Parfum」](https://ogre.natalie.mu/media/news/comic/2019/0710/sub1-euxq.jpg?imwidth=468&imdensity=1)
「シティーハンター　Eau de Parfum」［拡大］
](https://natalie.mu/comic/gallery/news/339191/1197294)

本商品は「シティーハンター」の世界観を香りで表現した、男女問わず使用できるマリンノートの香水。トップノートはシクラメン、フリージア、ローズ、ミドルノートはカーネーション、白ゆり、ラストノートは金木犀、チュベローズ、アンバーをブレンドし、安らぎを与える香りからロマンティックな香りへと変化していく。ボトルのキャップ部分には、作中に登場する新宿駅伝言板に書かれた「XYZ」の文字をあしらったチャームが付属。ボトル本体には冴羽リョウのシルエットが映され、パッケージには「100tハンマー」が箔押しされた。  
本商品用香味表现了“城市猎人”的世界观，男女都能使用的MarinNote香水。前调是Cyclamen(仙客来)、Freesia（小苍兰）、玫瑰，中调是康乃馨、白百合，后调是Osmanthus（丹桂/金桂）、Tubereuse(夜来香)、琥珀混合，从令人安心的香味转变为浪漫的香味。瓶身的瓶盖部分，附赠了作品中登场的新宿站留言板上写着的“XYZ”字样的小饰物。瓶身上映出冴羽獠的剪影，包装上写着“100t锤子”。

価格は税込3024円。フェアリーテイルの公式オンラインストア、Amazon.co.jp、および専門店にて購入可能だ。  
价格为含税3024日元。可在Fairy Tail的官方在线商店、Amazon.co.jp以及专卖店购买。

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。  
※冴羽獠的獠，在动物旁正式标记为“僚”。






この記事の画像（全6件）  
这篇报道的图片(共6篇)  

[![「シティーハンター　オードパルファム」イメージビジュアル](https://ogre.natalie.mu/media/news/comic/2019/0710/main-4da8.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/339191/1197293 "「シティーハンター　オードパルファム」イメージビジュアル")
[![「シティーハンター　オードパルファム」](https://ogre.natalie.mu/media/news/comic/2019/0710/sub1-euxq.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/339191/1197294 "「シティーハンター　オードパルファム」")
[![「シティーハンター　オードパルファム」](https://ogre.natalie.mu/media/news/comic/2019/0710/sub2-4t2e.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/339191/1197295 "「シティーハンター　オードパルファム」")
[![「シティーハンター　オードパルファム」パッケージ](https://ogre.natalie.mu/media/news/comic/2019/0710/sub3-9l37.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/339191/1197296 "「シティーハンター　オードパルファム」パッケージ")
[![「シティーハンター　オードパルファム」スリーブ](https://ogre.natalie.mu/media/news/comic/2019/0710/sub4-cjcu.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/339191/1197297 "「シティーハンター　オードパルファム」スリーブ")
[![「シティーハンター　オードパルファム」香りのイメージ図。](https://ogre.natalie.mu/media/news/comic/2019/0710/sub5-du44.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/339191/1197298 "「シティーハンター　オードパルファム」香りのイメージ図。")

(c)T.HOJO/N,C

LINK

[シティーハンター　オードパルファム|ドリーミング　プリンセス｜Dreming Princess](https://www.dreaming-princess.com/product/?pid=1561017623-229530)  
[アニメ「劇場版シティーハンター <新宿プライベート・アイズ>」公式サイト](https://cityhunter-movie.com/)  