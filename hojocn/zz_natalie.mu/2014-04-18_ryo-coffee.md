https://natalie.mu/comic/news/114762


# 冴羽リョウが微笑むドリップコーヒー登場、プレゼントも
冴羽獠微笑Drip Coffee登场，礼物也有

2014年4月18日 [Comment](https://natalie.mu/comic/news/114762/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」シリーズのイラストをPackageにDesignしたドリップバッグコーヒーが、オンラインショップのblueshopにて発売された。4月22日からは東京の漫画空間高円寺店をはじめとする実店舗で購入できる。  
以北条司的《城市猎人》系列插画为包装设计的Drip Coffee在网上商店blueshop发售。4月22日起可在东京漫画空间高圆寺店等实体店铺购买。

[
![PackageのDesign。](https://ogre.natalie.mu/media/news/comic/2014/0418/package.jpg?imwidth=468&imdensity=1)
PackageのDesign。  
大きなサイズで見る（全7件）  
包装设计。  
查看大图(共7件)  
](https://natalie.mu/comic/gallery/news/114762/249077)

この商品はマンガ家の協力のもと、東日本大震災の被災地復興を支援する「日本応援プロジェクト」が携わるコーヒー「MANGA DRIP」の第2弾。ブレンドにあたっては北条が実際に試飲し、監修を務めた。Packageには冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）のイラストと、北条からのメッセージがあしらわれている。価格は1個150円、10個セットは1450円。  
这个商品是在漫画家的协助下，支援东日本大地震受灾地复兴的“日本应援计划”所参与的咖啡“MANGA DRIP”的第2弹。调配时北条实际试饮，担任监修。包装上有冴羽獠的插图(獠的汉字是在反犬旁边加上“僚”的组成)和北条的留言。价格为1个150日元，10个一组1450日元。

また10個入りPackageの購入者から抽選で、北条のイラスト入りタンブラーが当たるキャンペーンも実施中。「MANGA DRIP」の売上の一部は「日本応援プロジェクト」が被災地で行う、お絵かき教室などの活動支援金に充てられる。  
另外，从10个包装的购买者中抽选出北条的插图的玻璃杯的活动也在实施中。“MANGA DRIP”销售额的一部分将作为“日本应援计划”在受灾地进行的绘画教室等活动的支援金。

この記事の画像（全7件）  
这篇报道的图片(共7篇)  

[![PackageのDesign。](https://ogre.natalie.mu/media/news/comic/2014/0418/package.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/114762/249077 "PackageのDesign。（包装设计。）")
[![Packageの写真。](https://ogre.natalie.mu/media/news/comic/2014/0418/IMG_0287.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/114762/249078 "Packageの写真。")
[![Packageの写真。](https://ogre.natalie.mu/media/news/comic/2014/0418/IMG_0297.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/114762/249079 "Packageの写真。")
[![Packageの写真。](https://ogre.natalie.mu/media/news/comic/2014/0418/IMG_0295.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/114762/249080 "Packageの写真。")
[![CoffeeをDripしているところ。](https://ogre.natalie.mu/media/news/comic/2014/0418/soziai4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/114762/249081 "コーヒーをドリップしているところ。")
[![徳島県のCafe・TOKUSHIMA COFFEE WORKSのImage。](https://ogre.natalie.mu/media/news/comic/2014/0418/soziai3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/114762/249082 "徳島県のCafe・TOKUSHIMA COFFEE WORKSのImage。")
[![「日本応援Project」のlogo。](https://ogre.natalie.mu/media/news/comic/2014/0418/logo_jp01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/114762/249083 "「日本応援Project」のlogo。")


関連記事

[
![キン肉マンが復興を支援、Drip CoffeeのIllust](https://ogre.natalie.mu/media/news/comic/2013/1009/manga_drip05.jpg?impolicy=thumb_fit&width=140&height=140)
キン肉マンが復興を支援、Drip CoffeeのIllust
](https://natalie.mu/comic/news/101074)

LINK

[【楽天市場】MANGA DRIP/Manga Drip　10コ入りPack：【CityHunter】【1パックあたり10g】【中挽き】【Mail便/Pos Packet対応】漫画家やプロのEntertainerたちが多数参加する日本応援ProjectのCollabo商品第二号。：blueshop／ブルーショップ](http://item.rakuten.co.jp/blue-shop/002-03-005-2/)  
[株式会社ニコ » Blog Archive » MANGADRIP第二弾は「CityHunter」の冴羽獠に！！](http://niconico.co.jp/p719/)  
[株式会社ニコ - 福島市 - Corporate Office ... - Facebook](https://www.facebook.com/nico2niconicoegao)  