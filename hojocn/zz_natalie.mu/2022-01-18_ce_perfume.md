https://natalie.mu/comic/news/462142

# 「キャッツ♥アイ」連載40周年記念した香水セット、キュートで妖艶な3人を香りで表現  
「Cat's♥Eye」连载40周年纪念的香水套装，用香味表现可爱妩媚的3人  

2022年1月18日 21:20 226 [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)「キャッツ♥アイ」の連載40周年を記念し、瞳、泪、愛の3姉妹をイメージした香水セットが予約販売される。  
为纪念北条司「Cat's♥Eye」连载40周年，以瞳、泪、爱三姐妹为形象的香水套装开始预售。  

[
![「キャッツ♥アイ」香水セットの告知画像。](https://ogre.natalie.mu/media/news/comic/2022/0118/catseye_eaudeparfum_08.jpg?impolicy=hq&imwidth=730&imdensity=1)
「キャッツ♥アイ」香水セットの告知画像。  
大きなサイズで見る（全6件）  
「Cat's♥Eye」香水套装的宣传图。  
查看大图(共6件)  
](https://natalie.mu/comic/gallery/news/462142/1745023)

[
![「『キャッツ♥アイ』 40th Anniversary オードパルファム」](https://ogre.natalie.mu/media/news/comic/2022/0118/catseye_eaudeparfum_07.jpg?imwidth=468&imdensity=1)
「『キャッツ♥アイ』 40th Anniversary オードパルファム」［拡大］
](https://natalie.mu/comic/gallery/news/462142/1745022)

普段は喫茶店「キャッツアイ」で働く美人3姉妹が、レオタード姿の怪盗・キャッツアイとして活躍する姿を描く「キャッツ♥アイ」。週刊少年ジャンプ（集英社）にて1981年に連載がスタートした。次女・瞳をイメージした香水は、Verbena citrus
flowerの爽やかな香りに。妖艶な美女である長女・泪をイメージしたものは、Bergamontやlemonなど爽やかな香りからcyclamen、muskの香りへと変化。明るくキュートでボーイッシュな三女・愛をイメージした香水には、Bitter orange、 Honey sabon、 rhubarbなどが調香され、爽やかな香りに仕上げられている。3種のボトルには、それぞれのキャラクターのシルエットがデザインされた。さらにノベルティとして、夜の都会をイメージしたアクリル製の香水スタンドも付属。ボトルを飾って楽しむこともできる。  
「Cat's♥Eye」描写的是平时在「Cat'sEye」咖啡屋工作的美女三姐妹，作为穿着Leotard(译注：紧身衣)的怪盗“猫眼”而活跃的样子。周刊少年Jump(集英社)于1981年开始连载。以二女儿·瞳为形象的香水，是马鞭草柑橘花的清爽香气。以妩媚美女的长女泪为形象的东西，从佛手柑和柠檬等清爽的香变成了仙客来、麝香的香。以开朗可爱、男孩子气十足的三女·爱为形象的这款香水，调香了苦橙、蜂蜜sabon、大黄等，使香气更加清爽。三种瓶子都设计了各自的人物轮廓。另外，还附带了以夜晚都市为印象的亚克力制香水台。还可以装饰瓶子。  

価格は税込7150円。フェアリーテイルの公式オンラインストアと、Amazon.co.jpにて予約を受け付けており、購入者には2月20日より順次発送される。なお東京・中野ブロードウェイ内にある直営店「FAIRYTAIL」でも販売予定だ。  
价格为含税7150日元。FairyTail的官方在线商店和Amazon.co.jp接受预约，购买者从2月20日开始依次发货。另外，预定在东京·中野百老汇内的直营店“FAIRYTAIL”销售。

この記事の画像（全6件）  
这篇文章的图片(共6条)  

[![「キャッツ♥アイ」香水セットの告知画像。](https://ogre.natalie.mu/media/news/comic/2022/0118/catseye_eaudeparfum_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/462142/1745023 "「キャッツ♥アイ」香水セットの告知画像。")
[![「『キャッツ♥アイ』 40th Anniversary オードパルファム」](https://ogre.natalie.mu/media/news/comic/2022/0118/catseye_eaudeparfum_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/462142/1745022 "「『キャッツ♥アイ』 40th Anniversary オードパルファム」")
[![「『キャッツ♥アイ』 40th Anniversary オードパルファム」より、来生瞳をイメージした香水。](https://ogre.natalie.mu/media/news/comic/2022/0118/catseye_eaudeparfum_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/462142/1745028 "「『キャッツ♥アイ』 40th Anniversary オードパルファム」より、来生瞳をイメージした香水。")
[![「『キャッツ♥アイ』 40th Anniversary オードパルファム」より、来生泪をイメージした香水。](https://ogre.natalie.mu/media/news/comic/2022/0118/catseye_eaudeparfum_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/462142/1745026 "「『キャッツ♥アイ』 40th Anniversary オードパルファム」より、来生泪をイメージした香水。")
[![「『キャッツ♥アイ』 40th Anniversary オードパルファム」より、来生愛をイメージした香水。](https://ogre.natalie.mu/media/news/comic/2022/0118/catseye_eaudeparfum_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/462142/1745025 "「『キャッツ♥アイ』 40th Anniversary オードパルファム」より、来生愛をイメージした香水。")
[![「キャッツ♥アイ」香水セットの告知画像。](https://ogre.natalie.mu/media/news/comic/2022/0118/catseye_eaudeparfum_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/462142/1745029 "「キャッツ♥アイ」香水セットの告知画像。")

(c)北条司／コアミックス1981, 版権許諾証AN-801




LINK

[Cat's Eye Eau de parfum | Fairy Tale](https://fairytail.jp/catseye/parfum/)  
[Cat's Eye 40th Anniversary Eau de parfum | Dreming Princess](https://www.dreaming-princess.com/product/?pid=1642402031-121063)

译注：  
Eau de parfum简称EDP，香精浓度为15-20%。香味仍然具有持久的特性，从3-5小时不等。(摘自[香水中的EDP, EDT, EDC到底是什么?](https://zhuanlan.zhihu.com/p/33886786) )  
