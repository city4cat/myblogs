https://natalie.mu/stage/news/549105

# 舞台「メイジ・ザ・キャッツアイ」アフターイベントに河原雅彦・七海ひろき・船越英一郎ら
舞台剧《Mage the Cat's Eye》河原雅彦、七海弘木、船越英一郎等人参加的后续活动  

2023年11月14日 18:00 374 [14](https://natalie.mu/stage/news/549105/comment) [ステージナタリー編集部](https://natalie.mu/stage/author/75)


舞台「メイジ・ザ・キャッツアイ」の一部公演でアフターイベントが開催される。
舞台剧《Mage the Cat's Eye》的一部分公演将会举办售后活动。  

[
![明治座創業150周年ファイナル公演 舞台「メイジ・ザ・キャッツアイ」アフターイベントのゲスト。](https://ogre.natalie.mu/media/news/stage/2023/1114/after.jpg?impolicy=hq&imwidth=730&imdensity=1)
明治座創業150周年ファイナル公演 舞台「メイジ・ザ・キャッツアイ」アフターイベントのゲスト。  
大きなサイズで見る（全10件）
](https://natalie.mu/stage/gallery/news/549105/2189750)

これは、東京・明治座の創業150周年記念公演のファイナルを飾る作品。アフターイベントは、そのファイナル公演を記念し、明治座とゆかりのあるゲストを招き実施されるものとなる。2月12日17:00開演回には本公演の演出を担う[河原雅彦](https://natalie.mu/stage/artist/34774)、17日17:00開演回には[七海ひろき](https://natalie.mu/stage/artist/93430)、20日17:00開演回には[船越英一郎](https://natalie.mu/stage/artist/33714)、23日17:00開演回には[コロッケ](https://natalie.mu/stage/artist/6783)、24日17:00開演回には[檀れい](https://natalie.mu/stage/artist/48828)、27日17:00開演回には[松平健](https://natalie.mu/stage/artist/45623)、3月1・2日の17:00開演回には[荒牧慶彦](https://natalie.mu/stage/artist/86676)が登場する。  
这是装饰东京·明治座创业150周年纪念公演最终场的作品。之后的活动，是为了纪念该最终公演，邀请和明治座有渊源的嘉宾实施的活动。2月12日17:00开演回由担任本公演导演的河原雅彦担任，17日17:00开演回由七海广木担任，20日17:00开演回由船越英一郎担任，23日17:00开演回由可乐饼担任，24日17:00开演回由檀礼担任。松平健将于27日17:00开演，荒牧庆彦将于3月1、2日17:00开演登场。  


舞台「メイジ・ザ・キャッツアイ」は、[北条司](https://natalie.mu/stage/artist/2405)のマンガ「CAT'S♥EYE」を[岩崎う大](https://natalie.mu/stage/artist/91973)の脚本、河原の演出で舞台化するもので、物語の時代設定は、明治座が創業された明治時代に置き換えられる。劇中では、失踪した父親の真相を探るべく、美術品を盗む三姉妹・怪盗キャッツアイの物語が展開。本公演では、次女・来生瞳役に[藤原紀香](https://natalie.mu/stage/artist/13895)、長女・来生泪役に[高島礼子](https://natalie.mu/stage/artist/12822)、三女・来生愛役に[剛力彩芽](https://natalie.mu/stage/artist/10358)がキャスティングされた。公演は、来年2月6日から3月3日まで明治座で上演される。チケットの一般販売は12月17日10:00にスタート。  
舞台剧《Mage the Cat's Eye》是将北条司的漫画《CAT'S♥EYE》改编成舞台剧，由岩崎宇大编剧，河原执导，故事的时代设定置换为明治座创业的明治时代。剧中，为了寻找失踪的父亲的真相，三姐妹怪盗猫眼偷美术品的故事展开。本次公演中，次女来生瞳由藤原纪香饰演，长女来生泪由高岛礼子饰演，三女来生爱由刚力彩芽饰演。公演将于明年2月6日至3月3日在明治座上演。12月17日10:00开始售票。  


この記事の画像（全10件）

*   [![明治座創業150周年ファイナル公演 舞台「メイジ・ザ・キャッツアイ」アフターイベントのゲスト。](https://ogre.natalie.mu/media/news/stage/2023/1114/after.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2189750 "明治座創業150周年ファイナル公演 舞台「メイジ・ザ・キャッツアイ」アフターイベントのゲスト。")
*   [![明治座創業150周年ファイナル公演 舞台「メイジ・ザ・キャッツアイ」速報チラシ](https://ogre.natalie.mu/media/news/stage/2023/0929/catseyesokuho.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2157667 "明治座創業150周年ファイナル公演 舞台「メイジ・ザ・キャッツアイ」速報チラシ")
*   [![藤原紀香](https://ogre.natalie.mu/media/news/music/2023/0624/fujiwaranorika_art202306.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2088056 "藤原紀香")
*   [![剛力彩芽](https://ogre.natalie.mu/media/news/stage/2023/0929/gorikiayame_art202309.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2157668 "剛力彩芽")
*   [![高島礼子](https://ogre.natalie.mu/media/news/stage/2023/0929/takashimareiko_art202309.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2157669 "高島礼子")
*   [![美弥るりか](https://ogre.natalie.mu/media/news/stage/2023/0511/miyarurika_art202305.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2057472 "美弥るりか")
*   [![染谷俊之](https://ogre.natalie.mu/media/news/stage/2022/0304/someyasub2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/1773805 "染谷俊之")
*   [![上山竜治](https://ogre.natalie.mu/media/news/stage/2023/1026/kamiyamaryuji_art202310.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2176920 "上山竜治")
*   [![長谷川初範](https://ogre.natalie.mu/media/news/stage/2023/1026/hasegawahatsunori_art202310.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2176921 "長谷川初範")
*   [![「CAT'S♥EYE」（コアミックス）原作ビジュアル](https://ogre.natalie.mu/media/news/stage/2023/0929/catseyegensaku.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/stage/gallery/news/549105/2157666 "「CAT'S♥EYE」（コアミックス）原作ビジュアル")

## 明治座創業150周年ファイナル公演 舞台「メイジ・ザ・キャッツアイ」


2024年2月6日（火）～3月3日（日）  
東京都 明治座  
  
原作：[北条司](https://natalie.mu/stage/artist/2405)「CAT'S♥EYE」  
脚本：[岩崎う大](https://natalie.mu/stage/artist/91973)  
演出：[河原雅彦](https://natalie.mu/stage/artist/34774)  
出演：[藤原紀香](https://natalie.mu/stage/artist/13895) / [剛力彩芽](https://natalie.mu/stage/artist/10358) / [染谷俊之](https://natalie.mu/stage/artist/62286)、[上山竜治](https://natalie.mu/stage/artist/104244) / [川久保拓司](https://natalie.mu/stage/artist/47878)、[佃井皆美](https://natalie.mu/stage/artist/56648)、[新谷姫加](https://natalie.mu/stage/artist/125796) / [長谷川初範](https://natalie.mu/stage/artist/19384) / [前田悟](https://natalie.mu/stage/artist/88620)、[松之木天辺](https://natalie.mu/stage/artist/55140)、若井龍也、石井亜早実、吉田繭、[那須沙綾](https://natalie.mu/stage/artist/112409) / [花柳のぞみ](https://natalie.mu/stage/artist/116568)、浅野琳、佐藤マリン、[今村ゆり子](https://natalie.mu/stage/artist/124751)、[廣瀬水美](https://natalie.mu/stage/artist/124749)、金川希美、小泉丞 / [美弥るりか](https://natalie.mu/stage/artist/93404) / [高島礼子](https://natalie.mu/stage/artist/12822)