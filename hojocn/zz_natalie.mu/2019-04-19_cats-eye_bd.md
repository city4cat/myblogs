https://natalie.mu/comic/news/328559


# 「キャッツ▼アイ」第1・2期のBD-BOX、各巻ディスク2枚のコンパクトサイズ
《Cat's▼Eye》第1、2期的BD-BOX，每卷2张光盘的紧凑尺寸

2019年4月19日  [Comment](https://natalie.mu/comic/news/328559/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「キャッツ▼アイ」（▼はハートマーク）第1期、第2期のBlu-ray BOXが発売される。
根据北条司原作改编的动画《Cat's▼Eye》(▼是心形标志)第1季、第2季的Blu-ray BOX即将发售。  

[
![TVアニメ「キャッツ▼アイ」ビジュアル](https://ogre.natalie.mu/media/news/comic/2019/0419/catseye_main.jpg?imwidth=468&imdensity=1)
TVアニメ「キャッツ▼アイ」ビジュアル  
大きなサイズで見る（全4件）  
动画《Cat's▼Eye》Visual   
大尺寸观看(共4件)  
](https://natalie.mu/comic/gallery/news/328559/1147049)

今回のBlu-ray BOXには、1983年から1985年にかけて放送されたTVアニメ「キャッツ▼アイ」1stシーズン全36話、2ndシーズン全37話を、オリジナルマスターからデジタルリマスタリングしたハイレートSD画質で収録。各巻Blu-rayディスク2枚組のコンパクトサイズに収められ、1stシーズンが8月23日、2ndシーズンが9月20日に発売される。また封入特典としてブックレット、劇中に登場する「キャッツカード」をイメージしたアクリルカードが用意された。  
这次的蓝光BOX收录了1983年至1985年播放的TV动画《Cat's▼Eye》第一季全36话，第二季全37话，由Original Master Digital Remastering的High rate SD画质。收录。每卷以2张蓝光光盘的紧凑尺寸收录，第一季于8月23日发售，第二季于9月20日发售。另外，作为封入特典，还准备了以剧中登场的“Cat's Card”为形象的Acrylic card。

普段は「喫茶キャッツアイ」で働く美人3姉妹が、裏の顔としてレオタード姿で怪盗キャッツアイとして活躍する姿を描く「キャッツ▼アイ」。今年公開された北条原作によるアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」にも登場し話題を呼んだ。  
《Cat's▼Eye》描写了平时在“Cat'sEye咖啡屋”工作的美女三姐妹，但穿着紧身衣作为她们的秘密面孔，以幽灵小偷猫眼的身份活动。在今年上映的根据北条原作改编的动画电影《剧场版城市猎人<新宿Private Eyes>》中也有登场，引起了话题。  

この記事の画像（全4件）  
这篇报道的图片(共4篇)  

[![TVアニメ「キャッツ▼アイ」ビジュアル](https://ogre.natalie.mu/media/news/comic/2019/0419/catseye_main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/328559/1147049 "TVアニメ「キャッツ▼アイ」ビジュアル")
[![TVアニメ「キャッツ▼アイ」2ndシーズンのビジュアル。](https://ogre.natalie.mu/media/news/comic/2019/0419/catseye_2nd_main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/328559/1147050 "TVアニメ「キャッツ▼アイ」2ndシーズンのビジュアル。")
[![TVアニメ「キャッツ▼アイ」1stシーズンのビジュアル。](https://ogre.natalie.mu/media/news/comic/2019/0419/catseye_1st_main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/328559/1147051 "TVアニメ「キャッツ▼アイ」1stシーズンのビジュアル。")
[![TVアニメ「キャッツ▼アイ」ロゴ](https://ogre.natalie.mu/media/news/comic/2019/0419/catseye_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/328559/1147052 "TVアニメ「キャッツ▼アイ」ロゴ")

## TVアニメ「キャッツ▼アイ」
TV动画《猫▼爱》  

### Cast

来生瞳：戸田恵子  
来生泪：藤田淑子  
来生愛：坂本千夏  
内海俊夫：安原義人  
課長：内海賢二  
浅谷光子：榊原良子



(c)北条司／NSP・TMS 1983


LINK

[「キャッツ♥アイ」Compact BD-BOX | Frontier Works](http://www.fwinc.co.jp/goods/51124/)  
[「キャッツ♥アイ」2nd Season Compact BD-BOX | Frontier Works](http://www.fwinc.co.jp/goods/51127/)  