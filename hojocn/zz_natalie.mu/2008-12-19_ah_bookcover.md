https://natalie.mu/comic/news/12025

# バンチ特製、エンジェルハートの豪華ブックカバー
Bunch特制天使之心豪华BookCover

2008年12月19日 4:18 [コミックナタリー編集部](https://natalie.mu/comic/author/72)

本日発売の週刊コミックバンチ(新潮社)3号には、付録として「エンジェルハート特製ブックカバー」が付いてくる。  
今天发售的周刊Comic Bunch(新潮社)第3号，附赠了「Angel Heart特制BookCover」。

このブックカバー、同誌にて[北条司](https://natalie.mu/comic/artist/2405)が連載する人気作品「エンジェル・ハート」のイラストがプリントされたもの。表面は同作のヒロインである香瑩の絵柄、裏面は作中の名シーンが抜粋されたデザインとなっている。  
该BookCover上印有北条司在该杂志上连载的人气作品「Angel Heart」的插图。正面是该作品的女主角香莹的图案，背面是作品中名场景的精选设计。

時間が経つほど入手困難となる週刊マンガ誌の付録だけに、気になる方は今すぐ書店へ。  
正因为是时间越久就越难买到的周刊漫画杂志的附录，在意的人现在就去书店。  

LINK

[週刊Comic Bunch☆Coamix](http://www.comicbunch.com/)

関連商品

[
![北条司「エンジェル・ハート (28)」](https://images-fe.ssl-images-amazon.com/images/I/619VjubGDyL._SS70_.jpg)
北条司「エンジェル・ハート (28)」  
[書籍] 2008年12月9日発売 / 新潮社 / 978-4107714442  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4107714446/nataliecomic-22)