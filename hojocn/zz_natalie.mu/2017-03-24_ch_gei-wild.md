https://natalie.mu/comic/news/225943


# 「シティーハンター」のジャケットが復刻封入！「Get Wild」記念盤に  
「City Hunter」的Jacket复刻封入!在「Get Wild」纪念盘中  

2017年3月24日  [Comment](https://natalie.mu/comic/news/225943/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるテレビアニメ「シティーハンター」のイラストを使用したジャケットが、[TM NETWORK](https://natalie.mu/comic/artist/1270)「Get Wild」の30周年記念盤レコードに封入される。  
使用北条司原作的电视动画《城市猎人》插图的封面，将被收录在TM NETWORK“Get Wild”的30周年纪念唱片中。

[
![封入されるジャケットのイラスト。](https://ogre.natalie.mu/media/news/comic/2017/0324/CH_jaket_1.jpg?impolicy=hq&imwidth=730&imdensity=1)
封入されるジャケットのイラスト。  
大きなサイズで見る（全2件）  
被封入的封面的插图。  
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/225943/677537)

ジャケットのイラストは1987年に発売された「Get Wild」のオリジナルシングルや、1989年にリリースされたサウンドトラック「CITY HUNTER Dramatic Master」のブックレットにて使用されたもの。ポジフィルムから改めて起こしたデータを、本ジャケットではタイトルなどの文字を入れずに掲載する。大きさはシングルレコードサイズの直径17cmだ。  
封面的插图是1987年发行的《Get Wild》的原创单曲和1989年发行的原声带《CITY HUNTER Dramatic Master》的小册子中使用的东西。这些数据是根据正片重新制作的，在这个外套上没有标题或其他文字。大小为17厘米，相当于一张唱片。  

「Get Wild」の記念盤は未発表バージョンを含む4曲入りのアナログ盤として4月12日にリリースされる。インナースリーブには藤井徹貫が執筆した楽曲の誕生秘話を掲載。初披露のエピソードがいくつも収められる。  
《Get Wild》的纪念盘包括未发表的版本在内，将作为4首歌的黑胶唱片于4月12日发行。 内封刊登了藤井彻贯执笔的乐曲的诞生秘话。收录了许多首次披露的小故事。

さらに記念盤の発売を記念したインターネットラジオ番組「Get Wild Hunter～名曲に隠された謎を解明せよ！～」の配信が決定。TM NETWORKの[木根尚登](https://natalie.mu/comic/artist/18633)が出演し、関係者の証言を交えつつ「Get Wild」に隠された謎を解き明かしていく。番組特設サイトにて「Get Wild」に関する疑問や木根へのメッセージを募集しているので、30年分の思いをぶつけてみては。   
此外，纪念盘发售的网络广播节目“Get Wild Hunter~解开名曲中隐藏的谜题! ~”的配信也已决定。TM NETWORK的木根尚登出演，结合相关人员的证言，解开“Get Wild”中隐藏的谜团。在节目特设的网站上征集关于“Get Wild”的疑问和给木根的留言，试着碰撞30年的想法吧。  

この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![封入されるジャケットのイラスト。](https://ogre.natalie.mu/media/news/comic/2017/0324/CH_jaket_1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/225943/677537 "封入されるジャケットのイラスト。（被封入的封面的插图。）")
[![封入されるジャケットのイラスト。](https://ogre.natalie.mu/media/news/comic/2017/0324/CH_jaket_2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/225943/677538 "封入されるジャケットのイラスト。（被封入的封面的插图。）")

## 「Get Wild Hunter～名曲に隠された謎を解明せよ！～」（仮）
“Get Wild Hunter ~解开名曲中隐藏的谜团! ~”(暂定)  

日時：2017年4月8日 23:00～ ※公開期間は2週間を予定  
时间:2017年4月8日23:00 ~※公开时间预定2周  
出演：[木根尚登](https://natalie.mu/comic/artist/18633)   


LINK

[TM NETWORK「Get Wild」オリジナル発売30周年記念企画!完全生産限定の12インチ・アナログレコード。  
（TM NETWORK“Get Wild”原创发售30周年纪念企划!完全生产限定的12英寸黑胶唱片。）](http://www.110107.com/s/OTONANO/page/tmn_getw?ima=0706)

関連商品

[
![TM NETWORK「Get Wild（完全生産限定盤）」](https://images-fe.ssl-images-amazon.com/images/I/51qpnkPU0TL._SS70_.jpg)
TM NETWORK「Get Wild（完全生産限定盤）」  
[Analog] 2017年4月12日発売 / Sony Music Direct  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B01N4S7FNM/nataliecomic-22)