https://takarush.jp/promo/cityhunter/


# CityHunter　狙われた天使のレクイエム｜リアル宝探し・謎解きならタカラッシュ！  
Cityhunter × Real寻宝 遭袭的天使的Requiem | Real寻宝、解谜Takarush !  
![](http://www.takarush.jp/promo/cityhunter/img/sns.jpg)



# [![Takarush！　Hunter's Village](https://takarush.jp/_common/global/img/logo_v.png)](https://huntersvillage.jp/)

[![](https://www.takarush.jp/m2/_common/img/logo.png)](http://www.takarush.jp/)

- [TOP](#top)  
- [STORY](#story)  
- [おすすめポイント](#howto)(推荐要点)  
- [開催概要](#gaiyo)(举办概要)  
- [グッズ情報](#goods)(商品信息)  
- [注意事項／Q&A](#attention)(注意事项/Q&A)  


![](https://takarush.jp/promo/cityhunter/img/main.png)![](https://takarush.jp/promo/cityhunter/img/sp-main.png)

## 吉祥寺の街を捜査しながら謎を解く  
一边搜查吉祥寺的街道一边解开谜题  

体験型プログラム！
体验型项目!  

##### ![お知らせ](https://takarush.jp/promo/cityhunter/img/title15.png)(通知)

2023.1.17  
[大好評につき、【1年間】イベント延長決定！](https://huntersvillage.jp/quest/cityhunter)  
关于大好评，【1年】活动延长决定!  

2022.3.22  
[3/28(月)からの一部の問題変更に関するお知らせ](https://huntersvillage.jp/news/important/17038.html)  
关于3/28(月)起部分问题变更的通知

2020.2.17  
[2/19(水)にご参加予定のお客様へ重要なお知らせ](https://www.takarush.jp/info/city_info20200217.html)  
2/19(周三)给预定参加的客人的重要通知

2020.1.20  
[ご参加される皆様へ重要なお知らせ](https://www.takarush.jp/info/20200120.html)  
给参加的各位的重要通知  

2020.1.10  
サイト公開！  
网站公开!  

## ![Story](https://takarush.jp/promo/cityhunter/img/title01.png)![Story](https://takarush.jp/promo/cityhunter/img/sp-title01.png)

謎の組織から狙われたのは、お宝『天使のレクイエム』と依頼人の命！  
裏社会No.1の銃の腕をもつ、〝CityHunter″冴羽獠と、そのパートナー槇村香。  
ある日二人の元を訪れた女性・天野咲奈は、何者かに命を狙われていた。  
その鍵を握るのは「天使のレクイエム」…。  
彼女に惹かれた獠は彼女の身辺警護の依頼を受け、  
「天使のレクイエム」の正体を明らかにすることを約束する。  
だが、その裏には〝吉祥寺″の街を揺るがす大きな陰謀が渦巻いていた…。  
獠は咲奈と吉祥寺を護りぬくことができるのか！？  

被神秘组织盯上的，是宝物“天使的安魂曲”和委托人的性命!  
拥有黑社会No.1枪手的“城市猎人”冴羽獠，和他的搭档槙村香。  
有一天，造访两人身边的女性天野咲奈，被什么人盯上了。  
掌握其关键的是“天使的安魂曲”…。  
被她吸引的獠接受了她的贴身护卫的委托，   
约定要揭开“天使的安魂曲”的真面目。  
但是，在这背后，“吉祥寺”这条街也陷入了巨大的阴谋漩涡中…。  
獠能拯救咲奈和吉祥寺吗!?  


![冴羽獠](https://takarush.jp/promo/cityhunter/img/Ryo-Saeba-img.png)

## ![このイベントのオススメポイント（这个活动的推荐点）](https://takarush.jp/promo/cityhunter/img/title10.png)![このイベントのオススメポイント（这个活动的推荐点）](https://takarush.jp/promo/cityhunter/img/sp-title10.png)

![香](https://takarush.jp/promo/cityhunter/img/Kaori-img.png)

![point1](https://takarush.jp/promo/cityhunter/img/point01.png)![point1](https://takarush.jp/promo/cityhunter/img/sp-point01.png)  
Point1 獠と香が街を駆け巡るオリジナルストーリー!&北条司先生描き下ろしのメインビジュアル!!CityHunterファン納得のスリリングでもっこりな本格ストーリーで街歩きを楽しめる!いつものあの仲間たちも登場!  
Point1 獠和香在街上巡游的原创故事!北条司老师描绘的主视觉!!城市猎人》的粉丝们可以在一个惊险、厚重、真实的故事中享受漫步城市的乐趣! 通常的朋友们也会出场!  

![point2](https://takarush.jp/promo/cityhunter/img/point02.png)![point2](https://takarush.jp/promo/cityhunter/img/sp-point02.png)  
Point2 謎解きの舞台は街歩きで大人気な吉祥寺!舞台は、謎解きスポットとして注目を浴びる吉祥寺!今回は「天使のレクイエム」を巡って街の様々な場所を探索するぞ!謎解きを進める中で手がかりだけでなく、吉祥寺の街の新たな魅力も発見できるかも!?  
Point2 解谜的舞台是走在街上很有人气的吉祥寺!舞台是作为解谜地点而备受瞩目的吉祥寺!这次围绕“天使的安魂曲”探索街道的各种各样的地方!在解开谜团的过程中，不仅能发现线索，说不定还能发现吉祥寺街新的魅力!?  

![point3](https://takarush.jp/promo/cityhunter/img/point03.png)![point3](https://takarush.jp/promo/cityhunter/img/sp-point03.png)  
Point3 キャラクターが素敵なグッズに!北条司先生描き下ろしのメインビジュアルを使用した、素敵なグッズカ贓々登場!「XYZクリップボード付きDX謎解きキット」や「お持ち帰リクリアファイル謎(全3種)」も要チェックだ!  
Point3 角色很棒的周边!使用了北条司老师画的主图像，存放了很好的物品!“附带XYZ剪贴板的DX解谜套装”和“带回家的透明文件谜(全3种)”也要看!


![香](https://takarush.jp/promo/cityhunter/img/Kaori-img.png)

## ![参加方法](https://takarush.jp/promo/cityhunter/img/title02.png)

![step1](https://takarush.jp/promo/cityhunter/img/step1.png)

![step2](https://takarush.jp/promo/cityhunter/img/step2.png)

![step3](https://takarush.jp/promo/cityhunter/img/step3.png)

![step4](https://takarush.jp/promo/cityhunter/img/step4.png)

![海坊主](https://takarush.jp/promo/cityhunter/img/Umiboze-img.png)


## ![開催概要](https://takarush.jp/promo/cityhunter/img/title03.png)![開催概要](https://takarush.jp/promo/cityhunter/img/sp-title03.png)

Title  
CityHunter　狙われた天使のレクイエム  
城市猎人 被瞄准的天使的安魂曲  

開催期間  
2020年1月18日～2024年1月18日

開催場所  
東京都武蔵野市　吉祥寺

参加費用  
謎解きキット　2,200円(税込）  
XYZクリップボード付　DX謎解きキット　4,400円(税込）  
解谜套装  200日元(含税)  
XYZ剪贴板附赠DX解谜套装4400日元(含税)  


販売場所  
BOOKSルーエ  
〒180-0004 東京都武蔵野市吉祥寺本町1-14-3

[GoogleMap ＞＞](https://goo.gl/maps/FoX43FS1U3TNnK8dA)  
_phone_0422-22-5677

営業時間　10:00～21:30  
定休日／元旦以外年中無休  
ジュンク堂書店 吉祥寺店   
〒180-0004 東京都武蔵野市吉祥寺本町1-11-5　コピス吉祥寺B館 6F  

[GoogleMap ＞＞](https://goo.gl/maps/3cakG3MZXLPsrAcu7)  
_phone_0422-28-5333  
営業時間　10:00～21:00  
定休日／事前にご確認ください。  

最終報告受付場所  
ZENON CAFE&ZENON SAKABA  
〒180-0003 東京都武蔵野市吉祥寺南町2-11-1

[GoogleMap ＞＞](https://g.page/ZENONSAKABA?share)  
_phone_0422-27-2275  
営業時間：  
2021年 1月8日から 2021年 1月31日まで  
平日　11:00〜17:00  
土日祝　11:00〜20:00  
  
2021年2月1日から通常営業予定となります  
定休日／年末年始  
预定2021年2月1日开始正常营业  
法定休息日/年末年初  


Game System  
![場所屋外](https://takarush.jp/promo/cityhunter/img/detail_gamesystem_1.png)  
![suishounennrei 12歳から](https://takarush.jp/promo/cityhunter/img/detail_gamesystem_2.png)  
![人数１人からOK](https://takarush.jp/promo/cityhunter/img/detail_gamesystem_3.png)  
![推定所要時間3時間から](https://takarush.jp/promo/cityhunter/img/detail_gamesystem_4.png)  

持ち物  
随身物品  

- [必須]  
    - ・参加キット  
    参与套件
    - ・Web接続が可能なスマートフォンまたはタブレット（必須）  
    可连接Web的智能手机或平板电脑(必须)  
        ※接続料・通信費は参加者負担となります。  
        连接费·通信费成为参加者负担。
- [あれば便利]  
有的话方便
    - ・クリップボード  
    剪贴板  
    - ・消しゴム  
    橡皮擦  
    - ・歩きやすい靴  
    容易走路的鞋  

Challenge Parameters    
【難易度】★★★☆☆☆  
【Volume】★★★★★☆  
【距離】★★★★☆☆  
[※Challenge Parametersとは？>>](https://www.takarush.jp/special/challepara/)  
Challenge Parameters是?  

##### ![初めての参加・謎解きが苦手な方へ](https://takarush.jp/promo/cityhunter/img/title04.png)
给第一次参加·不擅长解谜的人  

謎が難しい時は、ヒントもありますので、初心者の方も安心してご参加ください。  
谜题很难的时候，也有提示，初学者也请安心参加。  

## ![Goods情報](https://takarush.jp/promo/cityhunter/img/title05.png)![Goods情報](https://takarush.jp/promo/cityhunter/img/sp-title05.png)

※ジュンク堂書店 吉祥寺店、BOOKSルーエでの販売はしておりません。  
在顺益堂书店吉祥寺店、BOOKSRuhe不销售。  

※「お持ち帰りクリアファイル謎３種」を除く商品は[コミックゼノンオンラインストア](https://zenonofficialshop.jp/)にて販売しております。（品切れの場合はご了承ください）  
“带回家的清透文件谜3种”除外的商品在Comic Zenon网上商店出售。(如果断货，请谅解)  

※「お持ち帰りクリアファイル謎３種」及び一部商品をCAFE ZENON&ZENON SAKABAにて販売しております。（品切れの場合はご了承ください）  
“带回家的透明文件谜3种”及一部分商品在CAFE Zenon&Zenon SAKABA出售。(如果断货，请谅解)

![goods03](https://takarush.jp/promo/cityhunter/img/goods03.png)

お持ち帰りクリアファイル謎  
CityHunter　冴羽獠編  
『CityHunterと少年』  
带回家的透明文件夹谜  
城市猎人冴羽獠篇  
《城市猎人与少年》  

[ご購入はこちら（在这里购买）](https://store.shopping.yahoo.co.jp/takarushshop/cityhunter-ryou.html)

※yahoo shopへ移動します  
※移动到yahoo shop  

800円＋税

![goods01](https://takarush.jp/promo/cityhunter/img/goods02.png)

お持ち帰りクリアファイル謎  
CityHunter　槇村香編  
『CityHunter香の極秘捜査』  
带回家的透明文件夹谜  
槙村香编《城市猎人》  
《城市猎人香的绝密搜查》  


[ご購入はこちら（在这里购买）](https://store.shopping.yahoo.co.jp/takarushshop/cithhunter-kaori.html)

※yahoo shopへ移動します  
※移动到yahoo shop  

800円＋税

![goods02](https://takarush.jp/promo/cityhunter/img/goods01.png)

お持ち帰りクリアファイル謎  
CityHunter　海坊主編  
『海坊主、はじめてのラテアート！』  
带回家的透明文件夹谜  
城市猎人海坊主篇  
“海坊主，第一次的拉花艺术!”  


[ご購入はこちら（在这里购买）](https://store.shopping.yahoo.co.jp/takarushshop/cityhunter-umibouzu.html)

※yahoo shopへ移動します  
※移动到yahoo shop  

800円＋税

![goods04](https://takarush.jp/promo/cityhunter/img/goods04.png)

CityHunterマグカップ  
《狙われた天使のレクイエム》  
城市猎人马克杯  
《目标天使的安魂曲》  
  

1,500円＋税

![goods05](https://takarush.jp/promo/cityhunter/img/goods05.png)

CityHunter缶バッジ 4個セット  
《狙われた天使のレクイエム》  
4个城市猎人罐徽章套装  
《目标天使的安魂曲》  
  
1,500円＋税

![goods06](https://takarush.jp/promo/cityhunter/img/goods06.png)

CityHunterマスキングテープ  
城市猎人胶带  

500円＋税

![goods07](https://takarush.jp/promo/cityhunter/img/goods07.png)

CityHunter35周年記念  
アクリルキーホルダー  
城市猎人35周年纪念  
亚克力钥匙圈  

700円＋税

![goods08](https://takarush.jp/promo/cityhunter/img/goods08.png)

CityHunter  
Acrylic stand figure　冴羽獠  
城市猎人  
亚克力棒手办冴羽獠  

1,200円＋税

![goods09](https://takarush.jp/promo/cityhunter/img/goods09.png)

CityHunter  
Acrylic stand figure　槇村香

1,200円＋税

![goods10](https://takarush.jp/promo/cityhunter/img/goods10.png)

CityHunter  
Acrylic stand figure　海坊主

1,200円＋税

![goods11](https://takarush.jp/promo/cityhunter/img/goods11.png)

CityHunter  
Acrylic stand figure　天野咲奈

1,200円＋税

![goods12](https://takarush.jp/promo/cityhunter/img/goods12.png)

CityHunter  
full-color towel　冴羽獠

2,728円＋税

![goods13](https://takarush.jp/promo/cityhunter/img/goods13.png)

CityHunterトートバッグ  
《狙われた天使のレクイエム》  
城市猎人手提袋  
《目标天使的安魂曲》  


2,300円＋税

![goods14](https://takarush.jp/promo/cityhunter/img/goods14.png)

CityHunter  
Trading Clear Card Collection  
selection1（全18種類）  
※ブラインド仕様  
百叶窗式样  

182円＋税

![goods15](https://takarush.jp/promo/cityhunter/img/goods15.png)

CityHunter  
Collection Card収納Binder

1,637円＋税

## ![賞品紹介](https://takarush.jp/promo/cityhunter/img/title06.png)![賞品紹介](https://takarush.jp/promo/cityhunter/img/sp-title06.png)

##### ![クリア賞(通关奖)](https://takarush.jp/promo/cityhunter/img/title12.png)

オリジナルポストカード  
デジタル宝物アイテム  
原创明信片
数码宝物


## ![タカラッシュのデジタルアイテムとは？](https://takarush.jp/promo/cityhunter/img/title07.png)  
什么是Takarush的数码物品?

![Card](https://takarush.jp/promo/cityhunter/img/card_img.png)

全国のリアル宝探しに参加すると「宝物」のデジタル画像がもらえます。獲得した宝物アイテムは、マイページ上でコレクションしていくことができます。  
参加全国的现实寻宝活动的话就能得到“宝物”的数字图像。获得的宝物道具，可以在我的页面上收藏。  

[詳しい登録の仕方などはこちら ≫（详细的登录方法在此）](http://www.takarush.jp/mypage/collection.html)

## ![参加者クチコミ（参与者评论）](https://takarush.jp/promo/cityhunter/img/title08.png)![参加者クチコミ](https://takarush.jp/promo/cityhunter/img/sp-title08.png)

ちゃらゆきさん  
投稿日：2023年02月08日  
かなり楽しめました。アイテム作成も謎解き風だったらもっと良かったです。  
相当享受了。如果道具制作也是解谜风就更好了。  

CITY HUNTERさん  
投稿日：2023年02月05日  
吉祥寺の街も楽しみながら、2日掛けて楽しむことができました。  
吉祥寺的街道也很有趣，花了2天时间就逛完了。  

さとちちさん  
投稿日：2023年02月05日  
知らない場所にも足を伸ばせて良かった ギミックは秀逸でとても楽しかった  
我很高兴能去到陌生的地方  

[クチコミ一覧へ(进入评论列表)](https://takarush.jp/review/storylist.php?story=2704)

## ![CityHunterとは(什么是CityHunter ?)](https://takarush.jp/promo/cityhunter/img/title09.png)![CityHunterとは](https://takarush.jp/promo/cityhunter/img/sp-title09.png)

![CityHunter単行本](https://takarush.jp/promo/cityhunter/img/ZenonSelection CITY HUNTER_01.png)

北条司により1985年「週刊少年ジャンプ」にて連載開始。  
北条司于1985年在《周刊少年jump》上开始连载。

暗殺からボディーガード、人探しまであらゆる依頼を請け負う裏社会の始末屋《スイーパー》、人呼んで“CityHunter”こと冴羽獠のハードボイルド＆コメディ。パートナーの槇村香と共に、コルト・パイソンを操り、法で裁けない社会の闇を撃つ――。  
承包暗杀、保镖、寻人等所有委托的地下社会的始末屋“Sweeper”，人称“CityHunter”的冴羽獠的硬汉&喜剧。和搭档槙村香一起，用Colt Python，射杀法律无法制裁的社会黑暗——。


大人向けのシックな世界観と、冴羽獠の桁外れな女好きによる「もっこり」などのコミカルなキャラクター性、そして北条司の高い画力による美女キャラクター達が話題を呼び、一躍人気作品になった。またこの作品の影響により、冴羽獠への依頼方法である、駅の伝言板に合言葉である「XYZ」と書くことが流行した。  
成人向的潇洒世界观、冴羽獠超乎寻常的好色导致的“滑稽”等角色性格，以及北条司高超的画力带来的美女角色们引起话题，一跃成为人气作品。另外由于这部作品的影响，对冴羽獠的委托方法，即在车站留言板上写下口号“XYZ”风靡一时。  

1987年にはTVアニメが放送スタートし、大ヒットシリーズとなった。  
1987年TV动画开始放送，成为大受欢迎的系列。  

2019年には、劇場アニメ作品として「劇場版CityHunter＜新宿プライベート・アイズ＞」が公開され、観客動員数100万人を突破。  
2019年，剧场版动画作品《剧场版CityHunter <新宿Private Eyes>》上映，观众人数突破100万人。

また、同年フランスで製作された実写版「CityHunter THE MOVIE 史上最香のミッション」も大ヒットした。  
另外，同年在法国制作的真人版《CityHunter  THE MOVIE史上最香的任务》也大受欢迎。

[原作コミックの試し読みはコチラ！ ＞＞(原作漫画的试读在这里!)](https://mangahot.jp/site/works/j_R0006)  
  
2020年に連載開始30周年を迎え、現在もなお多くの人々に愛される作品です。  
2020年将迎来连载开始30周年，至今仍是深受人们喜爱的作品。

## ![キャラクター紹介](https://takarush.jp/promo/cityhunter/img/title16.png)

![冴羽獠](https://takarush.jp/promo/cityhunter/img/Ryo-Saeba.png)

##### 冴羽獠 Ryo Saeba

裏社会で伝説の始末屋（スイーパー）。高い戦闘能力を持ち、銃の腕は世界屈指。  
在地下社会传说中的始末屋(Sweeper)。拥有高超的战斗能力，枪法在世界首屈一指。  

普段は美女好きの三枚目だが、時にはクールな一面を見せる。依頼を受ける条件は「美女の依頼」か「心が震えた時」  
平时是喜欢美女的美男子，但有时也会展现酷酷的一面。接受委托的条件是“美女的委托”或“心动的时候”  

![槇村香](https://takarush.jp/promo/cityhunter/img/Kaori.png)

##### 槇村香 Kaori Makimura

獠の相棒。ボーイッシュな外見通り、男勝りで快活な性格。  
獠的搭档。像男孩一样的外表，比男人还活泼的性格。  

女性にスケベなことをしようとする 獠にお仕置きを与える一方で、パートナーとして深い信頼感を抱いている。  
一方面惩罚企图对女性毛手毛脚的獠，另一方面作为伴侣抱有很深的信赖感。  


![海坊主](https://takarush.jp/promo/cityhunter/img/Umiboze.png)

##### 海坊主 Umibozu

表向きは喫茶キャッツアイのマスターだが、元傭兵で獠と互角の腕をもつ始末屋（スイーパー）。  
表面上是猫眼咖啡店的老板，但原本是佣兵，拥有与獠势均力敌的手腕的始末屋（Sweeper）。

裏社会では「ファルコン」の名で呼ばれている。傭兵時代は獠と敵同士だったが、現在はライバル兼悪友。
在地下社会被称为“Falcon”。佣兵时代与獠是敌对关系，但现在是对手兼恶友。

## 注意事項

### 混雑時の注意
拥挤时的注意事项  

●土日祝日は、途中報告で利用するカフェが大変混雑する可能性があります。可能な方は、平日のご参加を、難しい場合は時間に余裕をもってご参加ください。  
周末和节假日，中途报告要去的咖啡店可能会非常拥挤。可能的话请在工作日参加，有困难的请留出充裕的时间参加。

●途中報告で利用するカフェの混雑により、想定より時間がかかる場合がございます。時間には余裕を持ってご参加ください。  
由于中途报告要使用的咖啡厅拥挤，有可能比预想的花费时间长。请留出充裕的时间参加。  

### 参加の注意

●本プログラムはヒントや解説の閲覧及び解答報告にWEB接続が可能なスマートフォンまたはタブレットが必要です。ご使用の際はバッテリー切れにお気をつけください。また、通信料はお客様負担となります。  
本程序需要能够WEB连接的智能手机或平板电脑来阅览提示和解说以及报告答案。使用的时候请注意电池没电。另外，通信费由客人负担。

●プログラム実施中の事故や怪我、器物破損などで発生した損害については、参加者個人の責任となりますので、ご理解の上ご参加ください。  
项目实施过程中发生的事故、受伤、器物损坏等损害，由参加者个人承担责任，请理解后参加。

●キット内容物は紛失した場合、再発行は致しかねますのでご注意ください。  
内容物如果一定丢失了，很难再发行，请注意。

●クリア賞のお渡しは、1キットにつき1つのみです。  
通关奖的交付，每个套件只有一个。

●本プログラムの設置物は次の場所にはありません。(立ち入り禁止の場所／水の中／土の中）  
本程序的设置物不在以下地方。(禁区/水里/土里)

●本プログラムで使用する設置物は、動かしたり、いたずらしたりしないでください。  
不要移动或篡改本方案中使用的装置。

●夜間、台風や警報が出ているなどの荒天時、その他自身や他の方に危険が及ぶと判断されるような状況での捜索は絶対に行なわないでください。  
不要在夜间、台风或警告等暴风雨天气下，或在任何其他可能对你或他人造成危险的情况下进行搜索。

●本イベントに関するネタバレはお控えください。（※話題にすることは問題ありません）  
请不要剧透有关本活动的内容。(※作为话题没有问题)

●その他ご不明点がある方は[コチラ](https://www.takarush.jp/faq/)でご確認ください。  
有其他不明白的地方的人请在这里确认。  

## Q&A

Q.一人でも参加できますか？  
一个人也可以参加吗?  
A.もちろんおひとりさまでも複数人でもお楽しみいただけます。ただし、ご購入された数のみキットやクリア賞をお渡しいたします。  
当然一人或多人都可以享用。但是，只给您购买的数量的工具包和通关奖。  

Q.初めて謎解きイベントに参加する人でも楽しめますか？  
第一次参加解谜活动的人也能享受吗?  
A.もちろんお楽しみいただけます。謎解きに詰まった際にはヒントもご用意しておりますので、安心してお楽しみください。
当然可以享受。在解谜过程中遇到困难的时候，我们还准备了提示，请大家安心享受。

Q.子どもでも楽しめますか？  
孩子也能玩吗?  
A.小学校高学年レベルの日本語を読み書きできる方、簡単な英単語がわかる方でしたら、ご参加いただけます。ただし、12歳以上推奨の内容となりますので、お子様が参加される場合は大人もご一緒に参加ください。  
能读写小学高年级水平的日语，能懂简单的英语单词的人都可以参加。但是，因为是12岁以上推荐的内容，孩子参加的话大人也请一起参加。

Q.プレイするにあたって、スマートフォンは必要ですか？  
玩的时候需要智能手机吗?  
A.途中、Webサイト上で行う仕組みがあるため、WEBに接続できるスマートフォンまたはタブレットが必要です。フィーチャーフォン（ガラケー）には対応しておりません。  
※通信費はお客様のご負担となります。  
因为中途需要在网站上进行，所以需要能够连接Web的智能手机或平板电脑。不支持功能手机(翻盖手机)。  
※通信费由客人负担。  

Q.参加できない日はありますか？  
有不能参加的日子吗?  
A.カフェの営業状況により参加出来ない可能性があります。参加される前に必ず本サイトをご確認の上、ご参加ください。  
根据咖啡馆的营业状况，有可能不能参加。参加之前请务必在确认本网站的基础上参加。  

Q.宝物アイテムはどこでもらえますか？  
在哪里能得到宝物道具?  
A.最終解答報告時、正解だった場合に渡されるエンディングシートに宝物アイテムが掲載されているページへのリンクがございます。そちらのページから手に入れてください。  
最终答案报告时，在正确答案的情况下被交给的结尾表中有宝物道具被登载的页的链接。请从那一页入手。

## この吉祥寺（まち）を巡る、謎を解き明かせ―
解开围绕吉祥寺的谜团——  

[![Twitter](https://takarush.jp/promo/cityhunter/img/twitter.png)](http://twitter.com/share?text=CityHunter　狙われた天使のレクイエム&url=http://www.takarush.jp/promo/cityhunter/&hashtags=CityHunter,XYZ謎,タカラッシュ,リアル宝探し)
[![Facebook](https://takarush.jp/promo/cityhunter/img/facebook.png)](http://www.facebook.com/share.php?u=http://www.takarush.jp/promo/cityhunter/)
[![LINE](https://takarush.jp/promo/cityhunter/img/line.png)](https://social-plugins.line.me/lineit/share?url=http://www.takarush.jp/promo/cityhunter/)

![吉祥寺Image](https://takarush.jp/promo/cityhunter/img/kichijoji,jpg.png)

## 株式会社じぞう屋とは  
什么是地藏菩萨株式会社  

漫画雑誌「コミックゼノン」を編集し、「CityHunter」や「北斗の拳」といった作品でお馴染みの漫画出版社「コアミックス」のグループとして2011年、吉祥寺にて設立。  
2011年在吉祥寺成立了漫画出版社Coamix的组合，该出版社曾编辑漫画杂志《Comic Zenon》，并以《CityHunter》、《北斗神拳》等作品为大家所熟知。  

マンガ作りで培ったノウハウを生かし、様々なコンテンツを「空間で表現する会社」です。「カフェゼノン」、「ゼノンサカバ」などの飲食店を運営し、特にグループ創業の地である吉祥寺では地元の一員として深く地域活動に参加しています。  
这是一家利用漫画创作积累的经验，将各种内容“用空间表现出来的公司”。经营着“Cafe Zenon”、“Zenon Sakaba”等餐饮店，特别是在集团创业之地吉祥寺，作为当地的一员，深入地参与了地区活动。  

## ![Takarush！とは](https://takarush.jp/promo/cityhunter/img/title-takarashu.png)

映画や小説の中のアドベンチャー「宝探し」の冒険を現実の世界で体験できる『リアル宝探し』を企画・制作。2002年より有名テーマパークや人気観光地、TV番組などに『リアル宝探し』を提供し、現在では年間200万人を熱狂させる人気エンターテインメントとなっています。これからも『リアル宝探し』を通じ、驚きと発見の感動を提供していきます。  
企划、制作了能在现实世界体验电影和小说中的探险“寻宝”的冒险的“Real寻宝”。从2002年开始在有名的主题公园、人气观光地、电视节目等提供“Real寻宝”，现在已经成为每年吸引200万人狂热的人气娱乐项目。今后也通过“Real寻宝”，提供惊喜和发现的感动。


[![Takarush！公式Siteはこちら](https://takarush.jp/promo/cityhunter/img/btn.png)](https://www.takarush.jp/)

[![](https://takarush.jp/promo/cityhunter/img/doga.png)](http://blacklabel.takarush.jp/)

##### 【　Takarush!公式Account　】

[![Twitter](https://takarush.jp/promo/cityhunter/img/twitter-s.png)](http://twitter.com/share?text=&url=http://www.takarush.jp/promo/cityhunter/&hashtags=takarush,リアル宝探し)
[![Facebook](https://takarush.jp/promo/cityhunter/img/facebook-s.png)](http://www.facebook.com/share.php?u=http://www.takarush.jp/promo/cityhunter/)
[![LINE](https://takarush.jp/promo/cityhunter/img/line-s.png)](https://social-plugins.line.me/lineit/share?url=http://www.takarush.jp/promo/cityhunter/)
[![Instagram](https://takarush.jp/promo/cityhunter/img/instagram-s.png)](https://www.instagram.com/takarush_official/)

主催：株式会社じぞう屋  
主办:地藏屋株式会社  

企画協力：株式会社ノース・スターズ・ピクチャーズ／株式会社コアミックス／武蔵野市観光機構  
企划协力:株式会社NorthStars Pictures /株式会社Coamix /武藏野市观光机构

©北条司／Coamix 1985 版権許諾証AB-100

企画制作：Takarush！

[![pagetop](https://takarush.jp/promo/cityhunter/img/lock_btn_pagetop.png)](#top)

[![](https://takarush.jp/promo/cityhunter/img/sm_footer_line.png)](https://social-plugins.line.me/lineit/share?url=http://www.takarush.jp/promo/cityhunter/)
![](https://takarush.jp/promo/cityhunter/img/sm_footer_menu.png)

[![pagetop](https://takarush.jp/promo/cityhunter/img/lock_btn_pagetop.png)](#top)

1.  [HOME](https://takarush.jp/)　＞　
2.  [イベントを探す](https://takarush.jp/event/search.php)　＞　
3.  [CityHunter　狙われた天使のレクイエム](https://takarush.jp/promo/cityhunter/)

﻿

## 企画ご担当者様へ
致企划负责人  

タカラッシュでは、「リアル宝探し」を使って、日本全国で、商品やサービスのプロモーション、地域活性化などを行っています。  
“Takarush”利用“Real寻宝”的方式，在日本全国范围内进行商品和服务的推广、地区活跃等活动。  


[![](https://www.takarush.jp/_common/global/img/c_pr.jpg)](http://www.takarush.co.jp/lp/mf/promo/)
[![](https://www.takarush.jp/_common/global/img/company_bnr_facility.png)](http://www.takarush.co.jp/lp/mf/facility/)
[![](https://www.takarush.jp/_common/global/img/c_pr2.jpg)](http://www.takarush.co.jp/lp/mf/group/)

[![](https://www.takarush.jp/_common/global/img/common_footer_castentry.png)](http://www.takarush.co.jp/recruit/201807/)

(C)Takarush Corp. ALL RIGHTS RESERVED.

## 企画ご担当者様へ
致企划负责人  

タカラッシュでは、「リアル宝探し」を使って、日本全国で、商品やサービスのプロモーション、地域活性化などを行っています。  
“Takarush”利用“Real寻宝”的方式，在日本全国范围内进行商品和服务的推广、地区活跃等活动。  

[![](https://www.takarush.jp/_common/global/img/c_pr.jpg)](http://www.takarush.co.jp/lp/mf/promo/)
[![](https://www.takarush.jp/_common/global/img/company_bnr_facility.png)](http://www.takarush.co.jp/lp/mf/facility/)
[![](https://www.takarush.jp/_common/global/img/c_pr2.jpg)](http://www.takarush.co.jp/lp/mf/group/)

[![](https://www.takarush.jp/_common/global/img/common_footer_logo.png)](http://www.takarush.co.jp/recruit/201807/)

株式会社 Takarush  
〒140-00140  
東京都品川区大井一丁目28番1号  
住友不動産大井町駅前ビル13階  
TEL:3-6417-1204

[![Takarush！　Hunter's Village](https://www.takarush.jp/_common/global/img/logo_v.png)](https://huntersvillage.jp/)

[![](https://www.takarush.jp/_common/global/img/common_footer_castentry.png)](http://www.takarush.co.jp/recruit/201807/)

「Real宝探し」は株式会社 タカラッシュの登録商標です。　(C)takarush Corp. ALL RIGHTS RESERVED.
「Real宝探し」是株式会社Takrash的注册商标。

[CMS](http://www.dugwood.com/index.html)


![](https://takarush.jp/promo/cityhunter/img/sm_menu_close.png)

*   [TOP](#top)
*   [STORY](#story)
*   [おすすめポイント](#howto)
*   [開催概要](#gaiyo)
*   [グッズ情報](#goods)
*   [注意事項／Q&A](#attention)

![](https://takarush.jp/promo/cityhunter/img/sm_menu_close2.png)