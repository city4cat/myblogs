https://natalie.mu/comic/news/39569

# コミックゼノン創刊号、御パンツ付録でかぶいて候

2010年10月25日  [Comment](https://natalie.mu/comic/news/39569/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

月刊コミックゼノン（徳間書店）の創刊号が、本日10月25日に発売された。休刊となった週刊コミックバンチ（新潮社）の編集を手がけていたコアミックスが、新たに仕掛ける新雑誌だ。  
月刊Comic Zenon(德间书店)的创刊号于今天10月25日发售。这是曾担任停刊周刊Comic Bunch(新潮社)编辑的Coamix新推出的新杂志。

[
![意中の女性のハートを召し捕る、デートという名の戦に出陣する際も「出陣！傾奇パンツ」が1枚あればバッチリだ。](https://ogre.natalie.mu/media/news/comic/2010/1025/zenon10-12furoku2.jpg?imwidth=468&imdensity=1)
意中の女性のハートを召し捕る、デートという名の戦に出陣する際も「出陣！傾奇パンツ」が1枚あればバッチリだ。  
大きなサイズで見る（全3件）  
捕获中意的女性的心，以约会为名出征的时候，只要有一条“出征!倾奇Pants”就足够了。  
查看大图(共3件)  
](https://natalie.mu/comic/gallery/news/39569/60821)

創刊号の目玉となるのは、[原哲夫](https://natalie.mu/comic/artist/1917)が10年ぶりに立ち上げる新連載「いくさの子－織田三郎信長伝－」。さらに[北条司](https://natalie.mu/comic/artist/2405)の代表作「キャッツ❤アイ」を原作にした、阿佐維シン作画のスピンアウト「キャッツ❤愛」も創刊前より話題を集めていた。「ANGEL HEART 2ndシーズン」や、「義風堂々!! 直江兼続～前田慶次酒語り～」など週刊コミックバンチで人気だった連載作品の移籍も多数。  
创刊号的重头戏是原哲夫时隔10年推出的新连载《战争之子-织田三郎信长传-》。另外，改编自北条司代表作「Cat's❤Eye」、由阿佐维新作画的衍生剧「Cat's❤爱」在创刊前就备受关注。《ANGEL HEART 2ndSeason》、《义风堂堂!!直江兼续~前田庆次酒谈~》等在漫画周刊上颇受欢迎的连载作品也多次转会。

月刊コミックゼノンと連動した携帯サイト・ゼノンランドや、吉祥寺の喫茶店CAFE ZENONでのイベントなど、一般的な紙媒体での雑誌という概念にとらわれないユニークな戦略も見所だ。読者を驚かせ楽しませたい思いから「傾（かぶ）く」という言葉をコンセプトに掲げ、創刊号では「出陣！傾奇パンツ」と名づけられた下着を付けるなど、付録にも趣向を凝らしている。  
与月刊Comic Zenon联动的手机网站Zenon Land、吉祥寺咖啡厅CAFE ZENON的活动等，不拘泥于一般纸质杂志的概念的独特战略也值得一看。为了给读者带来惊喜和快乐，杂志提出了“倾”这个词的概念，在创刊号上还设计了名为“出阵!倾奇Pants”的内衣，附录也别出心裁。

#### 月刊コミックゼノン創刊号Lineup
月刊Comic Zenon创刊号阵容  

原作／北原星望　原哲夫「いくさの子‐織田三郎信長伝‐」  
北条司「エンジェル・ハート2ndシーズン」  
原案／[武論尊](https://natalie.mu/comic/artist/2383)・原哲夫　[カジオ](https://natalie.mu/comic/artist/8947)「DD北斗の拳」  
原作／原哲夫・堀江信彦　[武村勇治](https://natalie.mu/comic/artist/5279)「義風堂々!! 直江兼続～前田慶次酒語り～」  
[関根尚](https://natalie.mu/comic/artist/8944)「パパは鈍感さま　-旦那イクメン化計画-」  
原作／北条司　Scenario／中目黒さくら　阿佐維シン「Cat's❤愛」  
原作／遠藤彩見　佐藤いづみ「冷蔵庫探偵」  
原作／梅村真也　[橋本エイジ](https://natalie.mu/comic/artist/7769)　「ちるらん新撰組鎮魂歌（レクイエム）」  
原案／いしぜきひでゆき　[藤栄道彦](https://natalie.mu/comic/artist/2309)「コンシェルジュプラチナム」  
Character Design / Devil Robots　原作／松家幸治　[岡春樹](https://natalie.mu/comic/artist/8946)「マンダラだらだら」  
[真野真](https://natalie.mu/comic/artist/8945)「笑えぬ童子108の業（カルマ）」  
[田中克樹](https://natalie.mu/comic/artist/8123)「Valientes 伊達の鬼 片倉小十郎」  
原作／北尾トロ　[松橋犬輔](https://natalie.mu/comic/artist/2084)「裁判長！ここは懲役4年でどうすか ぼくに死刑と言えるのか」  

この記事の画像（全3件）  
这篇报道的图片(共3篇)  

[![意中の女性のハートを召し捕る、デートという名の戦に出陣する際も「出陣！傾奇パンツ」が1枚あればバッチリだ。](https://ogre.natalie.mu/media/news/comic/2010/1025/zenon10-12furoku2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39569/60821 "意中の女性のハートを召し捕る、デートという名の戦に出陣する際も「出陣！傾奇パンツ」が1枚あればバッチリだ。")
[![「出陣！傾奇パンツ」表はこんな感じ。](https://ogre.natalie.mu/media/news/comic/2010/1025/zenon10-12furoku1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39569/60822 "「出陣！傾奇パンツ」表はこんな感じ。(“出阵!倾奇Pants”正面是这样的感觉。)")
[![月刊コミックゼノン創刊号となる12月号。](https://ogre.natalie.mu/media/news/comic/2010/1025/zenon10-12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39569/60823 "月刊コミックゼノン創刊号となる12月号。")

LINK  
[COMIC ZENON｜コミックゼノン](http://www.comic-zenon.jp/)