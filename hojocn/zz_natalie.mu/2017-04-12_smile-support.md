https://natalie.mu/comic/news/228447

# ルフィ、カイジ、冴羽リョウらが笑顔で応援「熊本国際漫画祭」今週末開催
路飞、海二、冴羽獠等笑容应援“熊本国际漫画祭”本周末举行

2017年4月12日 [Comment](https://natalie.mu/comic/news/228447/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


復興支援を目的にマンガやイラストを展示するイベント「熊本国際漫画祭～世界中から“笑顔”が一番集まる日～」が、4月15日、16日に熊本の鶴屋百貨店にて開催される。  
4月15日、16日，以支援复兴为目的的漫画和插图展示活动“熊本国际漫画节~全世界笑脸最集中的日子~”将在熊本的鹤屋百货店举行。

[
![尾田栄一郎のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/oda.jpg?imwidth=468&imdensity=1)
尾田栄一郎のSign色紙。  
大きなサイズで見る（全21件）  
尾田荣一郎的签名彩纸。  
查看大图(共21件)  
](https://natalie.mu/comic/gallery/news/228447/687865)

このイベントでは「笑顔」をテーマに、世界中からセリフを用いないサイレントマンガを募集。32カ国から届いた178作品の展示と、優秀作品の表彰を行う。またマンガとともに寄せられた551のイラスト作品で制作された、熊本城の大型モザイクアートがお目見え。[尾田栄一郎](https://natalie.mu/comic/artist/2368)、[井上雄彦](https://natalie.mu/comic/artist/1791)、[福本伸行](https://natalie.mu/comic/artist/2387)らがイベントを応援するゲストマンガ家として「笑顔のキャラクター」を描いたSign色紙を寄せており、これらの色紙もモザイクアートの一部としてお披露目される。  
本次活动以“笑脸”为主题，向全世界征集不使用台词的默白漫画。届时，将展示来自32个国家的178件作品，并表彰优秀作品。另外，与漫画一起被寄来的551个插画作品制作的熊本城的大型马赛克艺术也将亮相。尾田荣一郎、井上雄彦、福本伸行等人作为应援活动的特邀漫画家，将以“笑脸人物”为主题的签名彩纸寄来，这些彩纸也将作为马赛克艺术的一部分亮相。

[
![北条司による「熊本国際漫画祭」Key Visual。(c)北条司/NSP 2010](https://ogre.natalie.mu/media/news/eiga/2017/0412/key_hojo.jpg?imwidth=468&imdensity=1)
北条司による「熊本国際漫画祭」Key Visual。(c)北条司/NSP 2010［拡大］  
北条司的“熊本国际漫画祭”关键视觉。北条司/NSP 2010[扩大]
](https://natalie.mu/comic/gallery/news/228447/687883)

15日に開催されるオープニングイベントでは[北条司](https://natalie.mu/comic/artist/2405)と[次原隆二](https://natalie.mu/comic/artist/3787)が登壇し、編集者の[堀江信彦](https://natalie.mu/comic/artist/5280)とトークショーを行う。北条と堀江は16日のトークイベントにも登場。「シティーハンター」の最終回をモーションコミックにした動画の上映や、「熊本国際漫画祭」Key Visualの制作風景を撮影した映像を公開する。開催時間などの詳細は公式サイトにて確認してほしい。   
在15日举办的开幕活动中，北条司和次原隆二将登台，与编辑堀江信彦进行TalkShow。北条和堀江也在16日的谈话活动中登场。将上映《城市猎人》最终回的动画动画，并公开“熊本国际漫画节”Key Visual的制作效果的片段。举办时间等详情请在官方网站确认。

この記事の画像（全21件）  
这篇报道的图片(共21张)   

[![尾田栄一郎のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/oda.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687865 "尾田栄一郎のSign色紙。")
[![井上雄彦のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/inoue.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687866 "井上雄彦のSign色紙。")
[![福本伸行のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/hukumoto.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687867 "福本伸行のSign色紙。")
[![北条司のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/hojo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687868 "北条司のSign色紙。")
[![ちばてつやのSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/chiba.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687869 "ちばてつやのSign色紙。")
[![うすた京介のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/usuta.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687870 "うすた京介のSign色紙。")
[![次原隆二のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/tsugihara.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687871 "次原隆二のSign色紙。")
[![かざま鋭二のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/kazama_eiji.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687872 "かざま鋭二のSign色紙。")
[![橋本エイジのSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/hashimoto_eiji.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687873 "橋本エイジのSign色紙。")
[![出口真人のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/deguchi_masato.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687874 "出口真人のSign色紙。")
[![あみだむくのSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/amida_muku.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687875 "あみだむくのSign色紙。")
[![目黒川うなのSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/megurogawa_una.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687876 "目黒川うなのSign色紙。")
[![あさのゆきこのSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/asano_yukiko.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687877 "あさのゆきこのSign色紙。")
[![大久保圭のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/ohkubo_kei.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687878 "大久保圭のSign色紙。")
[![モリコロスのSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/mori_korosu.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687879 "モリコロスのSign色紙。")
[![黒川依のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/kurokawa_yori.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687880 "黒川依のSign色紙。")
[![田中克樹、水城水城のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/tanaka_katsuki_gensaku_no_sign_mo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687881 "田中克樹、水城水城のSign色紙。")
[![古賀慶のSign色紙。](https://ogre.natalie.mu/media/news/eiga/2017/0412/koga_kei.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687882 "古賀慶のSign色紙。")
[![北条司による「熊本国際漫画祭」Key Visual。(c)北条司/NSP 2010](https://ogre.natalie.mu/media/news/eiga/2017/0412/key_hojo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687883 "北条司による「熊本国際漫画祭」Key Visual。(c)北条司/NSP 2010")
[![原哲夫による「熊本国際漫画祭」Key Visual。(c)原哲夫/NSP 2017](https://ogre.natalie.mu/media/news/eiga/2017/0412/key_hara_full.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687884 "原哲夫による「熊本国際漫画祭」Key Visual。(c)原哲夫/NSP 2017")
[![次原隆二による「熊本国際漫画祭」Key Visual。(c)次原隆二/NSP 1982](https://ogre.natalie.mu/media/news/eiga/2017/0412/key_tsugihara.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/228447/687885 "次原隆二による「熊本国際漫画祭」Key Visual。(c)次原隆二/NSP 1982")

## 熊本復興支援2017「熊本国際漫画祭～世界中から“笑顔”が一番集まる日～」
熊本复兴支援2017“熊本国际漫画祭~全世界笑脸最集中的日子~”  

日程：2017年4月15日（土）、16日（日）  
会場：鶴屋百貨店 東館7F鶴屋Hall  
料金：無料  
费用:免费  

LINK

[熊本国際漫画祭｜世界でいちばん笑顔が集まる日](http://www.kk-mangasai.jp/)  
[「熊本国際漫画祭」開催！｜鶴屋百貨店](https://www.tsuruya-dept.co.jp/event/pickup/manga/index.html)  