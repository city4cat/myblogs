https://natalie.mu/comic/news/262648

# ゼノンで瀬川藤子が新連載、パンダの香香を「香」繋がりで北条司が描く付録も
濑川藤子在Zenon的新连载，北条司将熊猫香香与「香」联系起来画  

2017年12月25日  [Comment](https://natalie.mu/comic/news/262648/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

「VIVO!」の[瀬川藤子](https://natalie.mu/comic/artist/9178)による新連載「コノマチキネマ」が、本日12月25日発売の月刊コミックゼノン2018年2月号（徳間書店）でスタートした。
“VIVO!”野濑川藤子的新连载「コノマチキネマ」在12月25日发售的月刊comic zenon 2018年2月号(德间书店)上开始连载。

[
![「コノマチキネマ」カット](https://ogre.natalie.mu/media/news/comic/2017/1225/konomatikinema.jpg?imwidth=468&imdensity=1)
「コノマチキネマ」カット  
大きなサイズで見る（全4件）  
「コノマチキネマ」Cut  
大尺寸看(共4件)  
](https://natalie.mu/comic/gallery/news/262648/838847)

「コノマチキネマ」は、祖母の経営するアパートで1人暮らしを始めた女子・小野町楓を描くコメディ。幼い頃に住人たちに遊んでもらった楽しい記憶から「大人になったら絶対ここで暮らしたい」と思っていた楓だが、実際のアパート暮らしは楽しいことばかりでなく、現実に打ちのめされる。  
「コノマチキネマ」是一部描写在祖母经营的公寓里开始一个人生活的女子·小野町枫的喜剧。小时候和房客们一起玩耍的快乐记忆让枫产生了“长大后一定要住在这里”的想法，但实际的公寓生活却不只是快乐的事，还被现实所击倒。

また毎号異なるゲスト作家が登場し、肉をテーマにしたマンガを披露するアンソロジー企画「マンガ肉」も今号から開始。第1回目には[高尾じんぐ](https://natalie.mu/comic/artist/7680)、ばったん、[谷口菜津子](https://natalie.mu/comic/artist/10554)の3名が寄稿している。付録には、「シティーハンター」の香（かおり）、「エンジェル・ハート」の香瑩（シャンイン）、上野動物園のジャイアントパンダ・香香（シャンシャン）という名前に「香」の字を冠する2人と1匹を[北条司](https://natalie.mu/comic/artist/2405)が描き下ろした限定クリアファイルが封入された。  
另外，每期都有不同的嘉宾作家登场，以肉为主题的漫画的选集企划“漫画肉”也从本期开始。第1回有高尾津、ばったん、谷口菜津子3人投稿。附录包括一个限量版的ClearFile，其中北条司画了两个人和其中一只动物，名字上有“香”字的《城市猎人》的香(Kaori)、《天使之心》的香莹(XiangYin)、上野动物园的大熊猫香香(Shan-Shan)。


この記事の画像（全4件）

[![「コノマチキネマ」カット](https://ogre.natalie.mu/media/news/comic/2017/1225/konomatikinema.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/262648/838847 "「コノマチキネマ」カット")
[![付録「“香香香”クリアファイル」に使用された描き下ろしイラスト。(附录““香香香”ClearFile”所使用的新插画。)](https://ogre.natalie.mu/media/news/comic/2017/1225/panda.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/262648/838848 "付録「“香香香”クリアファイル」に使用された描き下ろしイラスト。  
(附录“香香香”ClearFile所使用的新插画。)")
[![「マンガ肉」のため高尾じんぐが描いたカラーカット。](https://ogre.natalie.mu/media/news/comic/2017/1225/manganiku.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/262648/838849 "「マンガ肉」のため高尾じんぐが描いたカラーカット。")
[![月刊コミックゼノン2018年2月号](https://ogre.natalie.mu/media/news/comic/2017/1225/zenon1802.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/262648/838850 "月刊コミックゼノン2018年2月号")

(c)瀬川藤子/NSP 2017 (c)北条司/NSP 1985 北条司/NSP 2010 (c)高尾じんぐ/NSP 2017



LINK

[Comic Zenon](http://www.comic-zenon.jp/)

