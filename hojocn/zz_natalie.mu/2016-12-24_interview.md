https://natalie.mu/comic/news/214533


# ゼノンで北条司×井上雄彦対談、桜木花道と海坊主のコラボ色紙プレゼント

2016年12月24日  [Comment](https://natalie.mu/comic/news/214533/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)と[井上雄彦](https://natalie.mu/comic/artist/1791)の対談が、本日12月24日発売の月刊コミックゼノン2017年2月号（徳間書店）に掲載された。  
北条司和井上雄彦的对谈刊登在12月24日发售的月刊Comic Zenon 2017年2月号(德间书店)上。

[
![北条司と井上雄彦のコラボ色紙。](https://ogre.natalie.mu/media/news/comic/2016/1224/sakuragibozu.jpg?imwidth=468&imdensity=1)
北条司と井上雄彦のコラボ色紙。  
大きなサイズで見る（全5件）  
北条司和井上雄彦的合作彩纸。  
查看大图(共5件)  
](https://natalie.mu/comic/gallery/news/214533/633334)

「シティーハンター」が連載されていた1988年に、北条のアシスタントをしていた井上。北条が深夜に仕事場を出て朝に戻るとブラインドにBB弾の弾痕が残っていたという、当時の思い出話から対談は始まる。2人が想定する読者像についてや、「SLAM DUNK」は描きながら泣いていたという執筆の裏話、感動できる作品にはユーモアが必要という創作論まで幅広い話題が繰り広げられた。なお発売中のグランドジャンプ2017年2号（集英社）には、この対談の別バージョンが収録されている。  
《城市猎人》连载的1988年，井上担任北条的助手。北条深夜从工作的地方出来，早上回来发现百叶窗上留有BB弹的弹痕，对谈从当时的回忆开始。包括两人设想的读者形象、《灌篮高手》边画边哭的执笔故事、让人感动的作品需要幽默的创作论等，话题非常广泛。另外，正在发售的《Grand Jump》2017年2号(集英社)中，收录了该对谈的其他版本。  

師弟対談の実現を記念してゼノンでは「SLAM DUNK」の桜木花道、「シティーハンター」「エンジェル・ハート」の海坊主が描かれた2人のコラボ色紙を1名にプレゼント。応募は2017年1月24日に締め切られる。申し込み方法は誌面にて確認を。  
为纪念师徒对谈的实现，Zenon将赠送一份印有《灌篮高手》中的樱木花道、《城市猎人》和《天使心》中的海坊主的两人合作彩纸。报名截止日期为2017年1月24日。申请方法请在杂志上确认。  

なお同号では田丸鴇彦の新連載「矢野七菜子、白球を追う。」がスタート。またゲーム「パズル＆ドラゴンズ」と「北斗の拳」のコラボ企画第4弾で「リン（究極進化）」が入手できるシリアルコードが封入されている。  
另外在同号上田丸鸨彦的新连载“矢野七菜子追逐白球。”开始。另外，游戏《谜题与龙》和《北斗神拳》的合作企划第4弹中，“林(究极进化)”可以得到的串行代码被封入。


※記事初出時、作家名に誤りがありました。お詫びして訂正いたします。  
※报道初出时，作家名有错误。向您道歉并更正。  

この記事の画像（全5件）  
这篇报道的图片(共5篇)  

[![北条司と井上雄彦のコラボ色紙。](https://ogre.natalie.mu/media/news/comic/2016/1224/sakuragibozu.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214533/633334 "北条司と井上雄彦のコラボ色紙。")
[![井上雄彦（左）と北条司（右）。](https://ogre.natalie.mu/media/news/comic/2016/1224/hojoinoue.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214533/633335 "井上雄彦（左）と北条司（右）。")
[![田丸鴇彦の新連載「矢野七菜子、白球を追う。」カラーカット。](https://ogre.natalie.mu/media/news/comic/2016/1224/yanonanako.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214533/633336 "田丸鴇彦の新連載「矢野七菜子、白球を追う。」カラーカット。")
[![「パズル＆ドラゴンズ」で入手できる「リン（究極進化）」イメージ。](https://ogre.natalie.mu/media/news/comic/2016/1224/rinkansenshinka.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214533/633337 "「パズル＆ドラゴンズ」で入手できる「リン（究極進化）」イメージ。")
[![月刊コミックゼノン2017年2月号](https://ogre.natalie.mu/media/news/comic/2016/1224/zenon1702.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214533/633338 "月刊コミックゼノン2017年2月号")

(c)武論尊・原哲夫/NSP 1983 版権許諾証 GW-114 (c)GungHo Online Entertainment, Inc. All Rights Reserved. (c)田丸鴇彦/NSP 2016


LINK

[Comic Zenon](http://www.comic-zenon.jp/)  
[INOUE TAKEHIKO ON THE WEB | HOME](http://itplanning.co.jp/)  
[北条司 OFFICIAL WEB SITE](http://www.hojo-tsukasa.com/)  

関連商品

[
![「月刊Comic Zenon 2017年2月号」](https://images-fe.ssl-images-amazon.com/images/I/61AJOz7Y0QL._SS70_.jpg)
「月刊Comic Zenon 2017年2月号」  
[雑誌] 2016年12月24日発売 / 徳間書店 / 491-0137730274  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B01MDU36FX/nataliecomic-22)

[
![「Grand Jump  2017年2号」](https://images-fe.ssl-images-amazon.com/images/I/61V7YcQ3rAL._SS70_.jpg)
「Grand Jump 2017年2号」  
[雑誌] 2016年12月21日発売 / 集英社 / 491-0276720174  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B01N6EATS6/nataliecomic-22)