https://natalie.mu/comic/news/66640

# 「エンジェル・ハート」香が携帯に残した声、ゼノンで配信

2012年3月25日  [Comment](https://natalie.mu/comic/news/66640/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


発売中の月刊コミックゼノン5月号（徳間書店）には、[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート2ndシーズン」の特別企画として「香の伝言メモ」を入手できるQRコードが掲載されている。  
发售中的月刊Comic Zenon 5月号(德间书店)上，作为北条司“Angel Heart 2nd Season”的特别企划刊登了可以得到“香的留言笔记”的QR码。

[
![月刊コミックゼノン5月号では、このイラストの複製原画に伊倉一恵のサインが入ったものが3名に当たるプレゼントを実施中。](https://ogre.natalie.mu/media/news/comic/2012/0324/ah-genga.jpg?imwidth=468&imdensity=1)
月刊コミックゼノン5月号では、このイラストの複製原画に伊倉一恵のサインが入ったものが3名に当たるプレゼントを実施中。  
大きなサイズで見る（全2件）  
月刊Comic Zenon 5月号中，该插图的复制原画中有伊仓一惠的签名，将抽取3人作为礼物。  
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/66640/114689)

「香の伝言メモ」は、「エンジェル・ハート2ndシーズン」3巻収録話「香の声」のエピソードを再現したもの。誌面に掲載されたQRコードにアクセスすると、冴羽獠の携帯電話に香が吹き込んだ伝言メモを実際に聞くことができる。伝言は2種類あり、もう1種類は「エンジェル・ハート2ndシーズン」3巻付属のコードから入手可能だ。  
“香的留言笔记”再现了“Angel Heart 2nd Season”第3卷收录的故事《香之声》中的故事。登录刊登在杂志上的QR码，就能实际听到香在冴羽獠的手机上吹下的留言备忘录。留言有两种，另一种可以从“Angel Heart 2nd Season”3卷附带的代码入手。  

なお月刊コミックゼノン5月号では「香の伝言メモ」の配信を記念して、声優・伊倉一恵のサイン入り「エンジェル・ハート2ndシーズン」複製原画を3名にプレゼントしている。申し込みの締め切りは4月24日消印有効。  
月刊Comic Zenon 5月号为了纪念《香的留言笔记》的配信，三位获奖者将获得由声优伊仓一惠签名的《Angel Heart 2nd Season》复制原画作为礼物。申请截止日期为4月24日，邮戳有效。  

この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![月刊コミックゼノン5月号では、このイラストの複製原画に伊倉一恵のサインが入ったものが3名に当たるプレゼントを実施中。](https://ogre.natalie.mu/media/news/comic/2012/0324/ah-genga.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/66640/114689 "月刊コミックゼノン5月号では、このイラストの複製原画に伊倉一恵のサインが入ったものが3名に当たるプレゼントを実施中。（月刊Comic Zenon 5月号中，该插图的复制原画中有伊仓一惠的签名，将抽取3人作为礼物。）")  
[![月刊コミックゼノン5月号](https://ogre.natalie.mu/media/news/comic/2012/0324/zenon12-5.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/66640/114690 "月刊コミックゼノン5月号（月刊Comic Zenon 5月号）")  





LINK  
[Comic Zenon](http://www.comic-zenon.jp/)


関連商品  
[
![北条司「エンジェル・ハート2ndシーズン（3）」](https://images-fe.ssl-images-amazon.com/images/I/514bMgMxP%2BL._SS70_.jpg)  
北条司「Angel Heart 2nd Season（3）」  
[漫画] 2012年3月19日発売 / 徳間書店 / 978-4199800696  
Amazon.co.jp [書籍版 / Kindle版]  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800697/nataliecomic-22)