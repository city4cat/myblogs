https://natalie.mu/comic/news/242188


喜欢《城市猎人》的女性在漫画世界转世《一八先生》的锦ソクラ新连载

# 「シティーハンター」好き女性がマンガ世界に転生「一八先生」の錦ソクラ新連載

2017年7月25日  [Comment](https://natalie.mu/comic/news/242188/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

「3年B組一八先生」の錦ソクラによる新連載「今日からCITY HUNTER」が、本日7月25日発売の月刊コミックゼノン9月号（徳間書店）でスタートした。

[
![「今日からCITY HUNTER」カット](https://ogre.natalie.mu/media/news/comic/2017/0725/kyokaracityhunter.jpg?imwidth=468&imdensity=1)
「今日からCITY HUNTER」カット
大きなサイズで見る（全3件）
](https://natalie.mu/comic/gallery/news/242188/745699)

「今日からCITY HUNTER」は、マンガ「シティーハンター」の世界に迷い込んでしまった女性を描く転生もの。高校生の頃に「シティーハンター」を読み、その主人公・冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）に恋をした女性・青山香は40歳になっても独身生活を送っていた。そんな彼女が不幸な事故に遭い、目覚めると10代の姿で新宿駅のホームに倒れていたことから物語は動き出す。

なお同号では、[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート」完結記念で展開されている3号連続付録のクリアファイル第3弾を封入。また1995年に週刊少年ジャンプ（集英社）で発表された短編「少年たちのいた夏 ～Melody of Jenny～」前編も掲載されている。同作は8月19日発売の単行本「桜の花 咲くころ 北条司 Short Stories Vol.2」にも収録予定。

この記事の画像（全3件）

[![「今日からCITY HUNTER」カット](https://ogre.natalie.mu/media/news/comic/2017/0725/kyokaracityhunter.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/242188/745699 "「今日からCITY HUNTER」カット")
[![「エンジェル・ハート」完結記念クリアファイル第3弾](https://ogre.natalie.mu/media/news/comic/2017/0725/cityhunter-file3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/242188/745700 "「エンジェル・ハート」完結記念クリアファイル第3弾")
[![月刊コミックゼノン9月号](https://ogre.natalie.mu/media/news/comic/2017/0725/zenon1709.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/242188/745701 "月刊コミックゼノン9月号")

(c)錦ソクラ/NSP 2017 (c)北条司/NSP 2010


LINK

[コミックゼノン](http://www.comic-zenon.jp/)

関連商品

[
![月刊コミックゼノン2017年9月号](https://images-fe.ssl-images-amazon.com/images/I/61zay1q4q0L._SS70_.jpg)
月刊コミックゼノン2017年9月号  
[雑誌] 2017年7月25日発売 / 徳間書店 / 491-0137730977  
](https://www.amazon.co.jp/exec/obidos/ASIN/B071W42R4S/nataliecomic-22)  