https://natalie.mu/comic/news/363406

# 「シティーハンター」もっこりメニューなどがカフェ＆酒場に登場、謎解き企画も

2020年1月16日  [Comment](https://natalie.mu/comic/news/363406/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」の謎解きイベント「シティーハンター ～狙われた天使のレクイエム～」が、東京・武蔵野市吉祥寺で1月18日より開始。これに合わせて、2月29日までCAFE ZENON、ZENON SAKABAで「シティーハンター」のコラボメニューが展開されることが決定した  
北条司《城市猎人》的解谜活动“城市猎人~被盯上的天使的安魂曲~”将于1月18日在东京武藏野市吉祥寺开始。与此同时，到2月29日为止，CAFE ZENON、ZENON SAKABA将推出“城市猎人”的合作菜单。

[
![CAFE ZENONメニュー](https://ogre.natalie.mu/media/news/comic/2020/0116/CITYHUNTER_CAFE.jpg?imwidth=468&imdensity=1)
CAFE ZENONメニュー  
大きなサイズで見る（全3件）  
CAFE ZENON菜单CAFE ZENON菜单  
查看大图(共3件)    
](https://natalie.mu/comic/gallery/news/363406/1315032)

CAFE ZENONでは「もっこりオムライス2020」「もっこりチョコバナナパンケーキ」「リョウのもこもこブルーソーダ」「恥ずかしがり屋な海坊主のミリタリーティーラテ」、ZENON SAKABAでは「100tハンマーグ」「100tハンマーで壊して食べるラザニア」「カクテル 天使のレクイエム」「カクテル 謎解きXYZ」と、両店で4品ずつコラボメニューを用意。それぞれ特典として限定マグネットが付属し、8種類をコンプリートした人にはさらに限定マグネット2種が進呈される。  
CAFE ZENON有“Mokkori蛋包饭2020”、“Mokkori巧克力香蕉薄饼”、“獠的Mokkori蓝苏打”、“害羞的海坊主的巧克力拿铁”、ZENON SAKABA准备了“100t锤子”、“用100t锤子打碎吃的千面”、“鸡尾酒 天使的安魂曲”、“鸡尾酒 解谜XYZ”，两店各4种合作菜单。分别附带限定磁铁作为优惠，集齐8种磁铁的人还将赠送2种限定磁铁。  

両店ではコラボアイテムの販売も実施。マグカップ、缶バッジ、トートバッグ、キーホルダー、トレーディングカード、タオルなどの新作グッズが用意され、ZENON SAKABAでは北条の描き下ろしイラストをあしらった限定ペアジョッキも販売される。なお「リアル宝探し×シティーハンター ～狙われた天使のレクイエム～」は約3年間にわたって実施される企画で、CAFE ZENON、ZENON SAKABAでのコラボイベント終了後も継続する予定。詳細はイベント公式サイトにて確認を。  
两家店还将推出合作款。准备了马克杯、易拉罐徽章、Tote包、钥匙圈、交换卡、毛巾等新商品，ZENON SAKABA还出售印有北条亲手绘制插图的限定酒杯。另外，“现实寻宝×城市猎人~被盯上的天使的安魂曲~”是持续约3年的企划，预定在CAFE ZENON、ZENON SAKABA的合作活动结束后也将继续进行。详情请在活动官方网站确认。  

この記事の画像（全3件）  
这篇报道的图片(共3篇)  

[![CAFE ZENONメニュー](https://ogre.natalie.mu/media/news/comic/2020/0116/CITYHUNTER_CAFE.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/363406/1315032 "CAFE ZENONメニュー")
[![ZENON SAKABAメニュー](https://ogre.natalie.mu/media/news/comic/2020/0116/CITYHUNTER_SAKABA.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/363406/1315033 "ZENON SAKABAメニュー")
[![ZENON SAKABA限定のペアジョッキ。](https://ogre.natalie.mu/media/news/comic/2020/0116/CITYHUNTER_beermug.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/363406/1315034 "ZENON SAKABA限定のペアジョッキ。（ZENON SAKABA限定的双人酒杯。）")

(c)北条司/NSP 1985


リンク

[CAFE ZENON | カフェゼノン](http://www.cafe-zenon.jp/)  
[吉祥寺 ZENON SAKABA | ゼノンサカバ](https://zenon-sakaba.jp/)  
[シティーハンター　狙われた天使のレクイエム｜リアル宝探し・謎解きならタカラッシュ！](https://takarush.jp/promo/cityhunter/)  