https://natalie.mu/comic/news/336333


# 北条司が総監督を務める映画「エンジェルサイン」に松下奈緒＆ディーン・フジオカ

2019年6月20日  [Comment](https://natalie.mu/comic/news/336333/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)が総監督を務める実写映画「エンジェルサイン」のメインキャストが発表された。[松下奈緒](https://natalie.mu/comic/artist/11381)と[ディーン・フジオカ](https://natalie.mu/comic/artist/65834)が主演を務める。  
北条司担任总导演的真人电影《天使印记》发表了主要演员阵容。由松下奈绪和藤冈靛主演。

[
![「エンジェルサイン」場面写真](https://ogre.natalie.mu/media/news/comic/2019/0619/angelsign02.jpg?impolicy=hq&imwidth=730&imdensity=1)
「エンジェルサイン」場面写真
大きなサイズで見る（全5件）
](https://natalie.mu/comic/gallery/news/336333/1183127)

[
![「エンジェルサイン」場面写真](https://ogre.natalie.mu/media/news/comic/2019/0619/angelsign01.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」場面写真［拡大］
](https://natalie.mu/comic/gallery/news/336333/1183128)

「エンジェルサイン」は、ノース・スターズ・ピクチャーズが開催している「サイレントマンガオーディション」へ寄せられたストーリー数編に、北条が描き下ろしたオリジナルのプロローグとエピローグを加えて構成された長編オムニバス作品。セリフを用いず、映像と音楽のみでストーリーが展開していく。  
“天使印记”是North Star Pictures举办的“SILENT MANGA AUDITION”收到的故事数篇，加上北条描绘的原创的序幕和尾声构成。多长篇集锦作品不用台词，只用影像和音乐展开故事。

本作で松下はチェリストのアイカ、フジオカはアイカの恋人でピアニストのタカヤを演じる。またキャストの発表と併せて、2人の演奏シーンや恋人らしく自撮りをする様子を捉えた場面写真もお目見え。さらにティザー映像も公開された。  
在本作品中，松下饰演大提琴家伊卡，藤冈饰演伊卡的恋人钢琴家高也。另外，在发表出演者名单的同时，2人的演奏场面和像恋人一样自拍的场面照片也一并亮相。更公开了Teaser映像。  

译注：以下译文参见[松下奈绪和藤冈靛出演北条司担任总导演的电影《天使印记》](./2019-06-20_angel-sign_2.md#comment)  

## 松下奈緒（アイカ役）コメント

初めて台本を頂きまず驚いたのは台詞が一言もなく、絵コンテだったことです。  
しかも、その絵コンテは北条先生が描き下ろされた絵コンテでしたのでとても感激しました。  
サイレントムービーもチェロ演奏も初めての経験でしたので、撮影はとても濃厚に感じました。台詞がないのでディーンさんとその場に合った台詞でお芝居をしていたので、とても良い緊張感がありました。  
言葉や文化が違っていても、主人公と同じ気持ちになれる。  
そう改めて感じられた撮影現場でした。  
美しい音楽も合わせて楽しんでいただけたら嬉しいです。

## ディーン・フジオカ（タカヤ役）コメント

北条先生はサングラスがすごく似合うと思いました。あんなにディレクターズチェアーに座って、サングラスが似合う監督はウォン・カーウァイか北条先生じゃないでしょうか（笑）。そういうハードボイルドなビジュアルから想像できない柔らかさがあって、本当に現場は自由でした。僕の世代の男の子だったら、冴羽リョウみたいな男になりたいというボーイズドリームは、皆持っていたと思うので、北条先生の作品の一部に参加させていただけるとのことで、すごく光栄です。  
※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。

この記事の画像・動画（全5件）

[![「エンジェルサイン」場面写真](https://ogre.natalie.mu/media/news/comic/2019/0619/angelsign02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/336333/1183127 "「エンジェルサイン」場面写真")
[![「エンジェルサイン」場面写真](https://ogre.natalie.mu/media/news/comic/2019/0619/angelsign01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/336333/1183128 "「エンジェルサイン」場面写真")
[![北条司](https://ogre.natalie.mu/media/news/comic/2019/0319/hojotsukasa.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/336333/1127745 "北条司")
[![「エンジェルサイン」ロゴ](https://ogre.natalie.mu/media/news/comic/2019/0319/angelsign-logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/336333/1127746 "「エンジェルサイン」ロゴ")
[![『エンジェルサイン』ティザー映像 #1](https://i.ytimg.com/vi/Sg5E-CGB33o/default.jpg)](https://natalie.mu/comic/gallery/news/336333/media/39577 "『エンジェルサイン』ティザー映像 #1")

## 実写映画「エンジェルサイン」  
真人电影《天使印记》  

企画：[堀江信彦](https://natalie.mu/comic/artist/5280)  
総監督：[北条司](https://natalie.mu/comic/artist/2405)  
キャスト：[松下奈緒](https://natalie.mu/comic/artist/11381)、[ディーン・フジオカ](https://natalie.mu/comic/artist/65834) ほか
企划:堀江信彦  
总监督:北条司  
演员:松下奈绪、藤冈靛等  



(c)「エンジェルサイン」製作委員会


LINK

[映画『エンジェルサイン』公式サイト](https://angelsign.jp)  
[「エンジェルサイン」公式ページ | Facebook](https://www.facebook.com/AngelSignMovie/)  
[AngelSignMovie (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[北条司 OFFICIAL WEB SITE](http://www.hojo-tsukasa.com/)  
[SILENT MANGA AUDITION(R) - The home of world's biggest Manga Award, SILENT MANGA AUDITION Community(SMAC!) Official Website.](http://www.manga-audition.com/)  