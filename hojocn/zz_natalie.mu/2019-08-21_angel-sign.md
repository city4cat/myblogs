https://natalie.mu/eiga/news/344328

# 北条司「エンジェルサイン」公開日決定、新キャストに緒形直人、菊池桃子、佐藤二朗

2019年8月21日  [Comment](https://natalie.mu/eiga/news/344328/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


「シティーハンター」で知られるマンガ家・[北条司](https://natalie.mu/eiga/artist/2405)が総監督を務める実写映画「[エンジェルサイン](https://natalie.mu/eiga/film/182396)」の公開日が11月15日に決定。[緒形直人](https://natalie.mu/eiga/artist/11951)、[菊池桃子](https://natalie.mu/eiga/artist/34588)、[佐藤二朗](https://natalie.mu/eiga/artist/35114)が出演していることがわかった。  
以《城市猎人》而闻名的漫画家北条司担任总导演的真人电影《天使印记》将于11月15日上映。知道了绪形直人、菊池桃子、佐藤二朗出演。

[
![「エンジェルサイン」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」ポスタービジュアル  
大きなサイズで見る（全15件）  
“天使印记”海报视觉  
大尺寸观看(共15件)  
](https://natalie.mu/eiga/gallery/news/344328/1222121)

[
![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」［拡大］
](https://natalie.mu/eiga/gallery/news/344328/1189290)

本作は奇跡の訪れを告げるブルーバタフライと音楽が鍵を握る6つの物語からなるオムニバス作品。全編通してセリフはなく、映像と音楽でストーリーが展開するサイレント映画となる。北条自身がメガホンを取った「プロローグ」「エピローグ」で[松下奈緒](https://natalie.mu/eiga/artist/11381)と[ディーン・フジオカ](https://natalie.mu/eiga/artist/65834)が主演を務めるのは既報の通り。このたび世界中から寄せられたサイレントマンガオーディションの受賞作品をもとに、アジア各国の監督が手がけた5編「別れと始まり」「空へ」「30分30秒」「父の贈り物」「故郷へ」の詳細も明らかに。  
这部综合电影由六个故事组成，其中宣布奇迹到来的蓝蝴蝶和音乐起到了关键作用。全篇没有台词，是用影像和音乐展开故事的无声电影。正如之前报道的那样，松下奈绪和藤冈迪安主演了《序幕》和《尾声》，这两部影片是北条亲自导演的。以此次从世界各地收到的默白漫画甄选的获奖作品为基础，由亚洲各国导演亲自制作的5部作品《离别与开始》、《飞向天空》、《30分30秒》、《父亲的礼物》、《故乡》的详细内容也已公布。

[
![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_03.jpg?imwidth=468&imdensity=1)
「別れと始まり」［拡大］
](https://natalie.mu/eiga/gallery/news/344328/1222122)

[
![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_04.jpg?imwidth=468&imdensity=1)
「別れと始まり」［拡大］
](https://natalie.mu/eiga/gallery/news/344328/1222123)

[
![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_07.jpg?imwidth=468&imdensity=1)
「父の贈り物」［拡大］
](https://natalie.mu/eiga/gallery/news/344328/1222124)

愛する妻を亡くし生きる希望を失った鉄道運転士と愛犬を描く「別れと始まり」では、緒形と菊池が夫婦役で共演し、「タイガーマスク」の[落合賢](https://natalie.mu/eiga/artist/67170)が監督を務めた。「ジャンダラ」で知られるタイの[ノンスィー・ニミブット](https://natalie.mu/eiga/artist/30201)による「空へ」では、病気で飼い犬を亡くしたことが理解できない幼い少女が主人公に。ベトナム系アメリカ人のハム・トランがメガホンを取った「30分30秒」は、出産を目前にして命を落としかける妊婦とその前に現れた死神の物語だ。亡き父親からもらったロボットが起こす奇跡を描いた「父の贈り物」では、「チェンジ！」の[旭正嗣](https://natalie.mu/eiga/artist/41796)が監督を務め、佐藤が父親役で出演。「鏡は嘘をつかない」で知られるインドネシアの[カミラ・アンディニ](https://natalie.mu/eiga/artist/95113)が手がけた「故郷へ」では、戦地に赴いた父と残された娘の物語がつむがれる。  
在《离别与开始》中，绪形和菊池共演夫妇，《虎面具》的导演落合贤担任导演，讲述的是因失去心爱的妻子而失去生存希望的铁路司机和爱犬的故事。泰国的《飞向天空》的主人公是因病而失去家犬的年幼少女。《30分30秒》由越南裔美国人哈姆·托兰掌镜，讲述了临产前差点丧命的孕妇和出现在她面前的死神的故事。《父亲的礼物》描写了从亡父那里得到的机器人所引发的奇迹，在《Change !》的旭正嗣担任导演，佐藤出演父亲。《还乡》由以《镜子不会说谎》而闻名于世的印尼作家卡米拉·安迪尼亲自操刀，讲述了奔赴战场的父亲和女儿的故事。  

「エンジェルサイン」は東京のユナイテッド・シネマ豊洲ほかで公開される。緒形、菊池、佐藤によるコメントは下記の通り。
《天使印记》将在东京的丰洲联合电影院等地公映。绪形、菊池、佐藤的评论如下。  

<a name="comment"></a>  
## 緒形直人 Comment

セリフがない作品ということで演技に悩みましたが、セリフがない分、心の動きを表現できる喜びがあって、逆に想像力を働かせながら観ていただけるんじゃないかと思います。またサイレントなので、楽しみでもありチャレンジでもありますが、監督にしがみつきながら演じています。  
因为是没有台词的作品，所以在演技上很苦恼，但是因为没有台词，能表现出内心的悸动，所以反而能发挥想象力来观看。另外因为无对白，既是期待也是挑战，但我在表演时严格按照导演的要求。（译注：待校对）  


## 菊池桃子 Comment

サイレントムービーのため、手探りでわからないことも多かったですが、監督が頭の中に描いているイメージを信じて、楽しみながら演じました。言葉を使わないことで、より心に焦点があたるという感覚は、不思議で素敵なものでした。この新しい挑戦に参加できたことは幸せなことです。  
因为是默白电影，有很多摸不着头脑的地方，但是相信导演在脑海中描绘的形象，一边享受一边演了。不使用对白，更能让心灵成为焦点，这种感觉既不可思议又美妙。能参加这个新的挑战是很幸福的事情。  

## 佐藤二朗 Comment

世界中から寄せられたサイレントマンガを実写化し、色々な国の俳優がそれぞれの役を演じています。セリフがないからこそ世界中の人に理解してもらえるという試みがとても面白くて、参加させていただきました。実際に演じてみて、普段、言葉に依存しているということに改めて気付かされました。共演した娘役のニーンちゃんは偶然にも息子と同じ7歳ということもあり、愛おしくて仕方ないです（笑）。  
由来自世界各地的默白漫画改编而成，由各国的演员出演各自的角色。正因为没有台词才能让全世界的人理解，这种尝试非常有趣，所以让我参加了。实际表演之后，我再次意识到自己平时是依赖语言的。共同出演的饰演女儿的尼恩碰巧和儿子一样都是7岁，真是让人疼爱(笑)。  

この記事の画像・動画（全15件）  
这篇报道的图片、视频(共15篇)  

[![「エンジェルサイン」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222121 "「エンジェルサイン」ポスタービジュアル")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1189290 "「エンジェルサイン」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222122 "「別れと始まり」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222123 "「別れと始まり」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222124 "「父の贈り物」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222125 "「空へ」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222126 "「空へ」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222127 "「30分30秒」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222128 "「30分30秒」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222129 "「父の贈り物」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222130 "「故郷へ」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222131 "「故郷へ」")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1189295 "「エンジェルサイン」")
[![左から緒形直人、菊池桃子、佐藤二朗。](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/344328/1222132 "左から緒形直人、菊池桃子、佐藤二朗。")
[![「エンジェルサイン」ティザー映像](https://i.ytimg.com/vi/Sg5E-CGB33o/default.jpg)](https://natalie.mu/eiga/gallery/news/344328/media/39570 "「エンジェルサイン」ティザー映像")

(c)「エンジェルサイン」製作委員会

LINK  

[「エンジェルサイン」公式サイト](https://angelsign.jp/)  
[AngelSignMovie (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[「エンジェルサイン」ティザー映像](https://youtu.be/Sg5E-CGB33o)    
