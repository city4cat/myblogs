https://natalie.mu/eiga/news/353597


# ディーン・フジオカ、松下奈緒にコラボ曲褒められにんまり「やったー！」

2019年10月31日 [Comment](https://natalie.mu/eiga/news/353597/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)

「[Angel Sign](https://natalie.mu/eiga/film/182396)」の完成披露試写会が本日10月31日に東京・品川プリンスホテル クラブeXで開催され、キャストの[松下奈緒](https://natalie.mu/eiga/artist/11381)、[ディーン・フジオカ](https://natalie.mu/eiga/artist/65834)が出席した。  
《Angel Sign》的完成披露试映会于今天10月31日在东京·品川王子酒店club eX举行，演员松下奈绪、藤冈靛出席。  

[
![左からディーン・フジオカ、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_26441.jpg?impolicy=hq&imwidth=730&imdensity=1)
左からディーン・フジオカ、松下奈緒。  
大きなサイズで見る（全16件）  
左起为藤冈靛、松下奈绪。  
查看大图(共16件)    
](https://natalie.mu/eiga/gallery/news/353597/1268802)

マンガ家・[北条司](https://natalie.mu/eiga/artist/2405)が総監督を務める本作は、世界中から寄せられたサイレントマンガオーディションの受賞作をアジア各国の監督が映像化したオムニバス。人々に訪れる奇跡を映像と音楽のみで描いた6つの物語が展開する。北条が「プロローグ」「エピローグ」でメガホンを取り、両作に松下とフジオカが出演した。  
漫画家·北条司担任总导演的本作品，是亚洲各国的导演将从世界各地被寄来的默白漫画大赛的获奖作品影像化的集锦。影片用影像和音乐讲述了降临人间的奇迹，讲述了6个故事。北条为《序幕》和《尾声》掌镜，两部作品分别由松下和藤冈出演。

[
![ネタバレを交えたトークを展開しようと意気込むディーン・フジオカ。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2636.jpg?imwidth=468&imdensity=1)
ネタバレを交えたトークを展開しようと意気込むディーン・フジオカ。［拡大］  
藤冈靛兴致勃勃地打算展开夹杂着剧透的谈话。[放大]  
](https://natalie.mu/eiga/gallery/news/353597/1268785)

オーケストラによるメインテーマ「Angel Sign」の生演奏のあと、登壇した松下とフジオカ。フジオカは「裏で松下さんと作戦会議をしていたのですが、ガンガンネタバレしていこうと思います」と意気込むが、映画の上映前だったことからMCに制止されて「ダメなんですか？」と照れ笑いを浮かべる。  
松下和藤冈在管弦乐团现场演奏了主题曲《Angel Sign》之后登台。藤冈干劲十足地说:“我在幕后和松下开了作战会议，不过，我想要一直剧透下去”。不过，因为是在电影上映前，所以被主持人制止。“不行吗?”他害羞地笑了。

[
![松下奈緒](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2694.jpg?imwidth=468&imdensity=1)
松下奈緒［拡大］
](https://natalie.mu/eiga/gallery/news/353597/1268795)

松下は「子供の頃から先生の作品を見ていたので、衣装合わせから緊張していました」と北条との初対面時に思いを馳せ、「モニタの前にいる北条先生はかっこいいなと思いながら背中を見ていました」と撮影時を回想する。フジオカはその言葉にうなずきながら「サングラス似合ってましたねえ。北条先生はいい意味ですごくゆるいんですよ。僕たちを信頼してくださって、こちらの意見も採用してくださる。素敵な現場でした」と続け、「今めっちゃセリフが多い役をやっていて、毎日死にそうになりながら覚えているんです……」と現状を伝えてセリフがない本作の撮影を懐かしんだ。  
松下回忆起与北条初次见面时的回忆说，“因为从小就看老师的作品，所以从试装开始就很紧张”，“一边想着监视器前的北条老师好帅，一边看着他的背影”。藤冈对这句话一边点头一边说:“这副墨镜很适合啊。北条老师非常宽容，他很信任我们，也采纳了我们的意见。真是很棒的现场”，接着说“现在演的角色台词特别多，我每天都在努力记台词，我快死了……”，表达了对拍摄这部没有台词的电影的怀念。

[
![松下奈緒（右）を前にピアノを弾くことのプレッシャーを語るディーン・フジオカ（左）。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2687.jpg?imwidth=468&imdensity=1)  
松下奈緒（右）を前にピアノを弾くことのプレッシャーを語るディーン・フジオカ（左）。［拡大］  
冈靛(左)在松下奈绪(右)面前讲述弹钢琴的压力。[放大]  
](https://natalie.mu/eiga/gallery/news/353597/1268793)

[
![松下奈緒に褒められたディーン・フジオカ。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2753.jpg?imwidth=468&imdensity=1)
松下奈緒に褒められたディーン・フジオカ。［拡大］  
被松下奈绪夸奖的藤冈靛。[放大]  
](https://natalie.mu/eiga/gallery/news/353597/1268798)

劇中には松下がチェロ、フジオカがピアノを演奏するシーンも。チェロに憧れてはいたものの触ったことがなかった松下は「苦労しかなかったです」と笑い、フジオカは松下を見やりながら「ピアニストの前でピアノを弾くプレッシャーといったら！」と嘆いて会場の笑いを誘う。そしてフジオカがDEAN FUJIOKA名義で松下とコラボした楽曲「Chasing A Butterfly feat. Nao Matsushita」に話が及ぶと、松下は「ディーンさんのアーティストの部分を感じられて、すごく好きな曲」と感想を述べてフジオカを「うわーうれしい！ やったー！ 褒められちゃった」と喜ばせていた。  
剧中还有松下演奏大提琴、藤冈演奏钢琴的场景。虽然憧憬大提琴，但没有接触过大提琴的松下笑着说“只有辛苦”，藤冈一边看着松下一边说“在钢琴家面前弹钢琴的压力山大!”的感叹引起了会场的笑声。藤冈以DEAN FUJIOKA的名义与松下合作的歌曲“Chasing A Butterfly feat. Nao”Matsushita”，松下发表感想说“能感受到dean的艺术家的部分，非常喜欢的曲子”让藤冈高兴地说“哇!太棒了!被夸奖了”。  

最後に松下は「言葉はなくとも伝わることがある、ということを皆さんに伝えられたらいいなと思ってこの映画ができあがりました」と挨拶。フジオカは「サイレントの作品のよさを、ぜひ皆さん言葉を使って表現してください」とSNSへの投稿を呼びかけてイベントを締めくくった。  
最后松下致辞说:“我想把即使没有语言也能传达的东西传达给大家，这样就好了，所以完成了这部电影。”藤冈呼吁大家在SNS上投稿“请大家一定要用语言来表现寂静作品的优点”，活动结束。

「Angel Sign」は11月15日より公開。  
《Angel Sign》将于11月15日公开。  

この記事の画像・動画（全16件）  
这篇报道的图片、视频(共16篇)  

[![左からディーン・フジオカ、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_26441.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268802 "左からディーン・フジオカ、松下奈緒。")
[![ネタバレを交えたトークを展開しようと意気込むディーン・フジオカ。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2636.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268785 "ネタバレを交えたトークを展開しようと意気込むディーン・フジオカ。  
（藤冈靛兴致勃勃地打算展开夹杂着剧透的谈话。）")
[![松下奈緒](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2694.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268795 "松下奈緒")
[![松下奈緒（右）を前にピアノを弾くことのプレッシャーを語るディーン・フジオカ（左）。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2687.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268793 "松下奈緒（右）を前にピアノを弾くことのプレッシャーを語るディーン・フジオカ（左）。  
（藤冈靛(左)在松下奈绪(右)面前讲述弹钢琴的压力。）")
[![松下奈緒に褒められたディーン・フジオカ。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2753.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268798 "松下奈緒に褒められたディーン・フジオカ。")
[![松下奈緒](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2621.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268787 "松下奈緒")
[![ディーン・フジオカ](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2632.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268786 "ディーン・フジオカ")
[![「Angel Sign」完成披露試写会の様子。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2647.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268790 "「Angel Sign」完成披露試写会の様子。")
[![松下奈緒](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2617.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268788 "松下奈緒")
[![ディーン・フジオカ](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2597.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268789 "ディーン・フジオカ")
[![松下奈緒](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2677.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268792 "松下奈緒")
[![MCにネタバレ禁止を言い渡されたディーン・フジオカ。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2654.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268791 "MCにネタバレ禁止を言い渡されたディーン・フジオカ。")
[![ディーン・フジオカ](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2721.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268796 "ディーン・フジオカ")
[![「Angel Sign」完成披露試写会の様子。](https://ogre.natalie.mu/media/news/eiga/2019/1031/DSC_2830.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1268797 "「Angel Sign」完成披露試写会の様子。")
[![「Angel Sign」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/353597/1222121 "「Angel Sign」ポスタービジュアル")
[![「Angel Sign」予告編](https://i.ytimg.com/vi/4gFDdfZBS7A/default.jpg)](https://natalie.mu/eiga/gallery/news/353597/media/42069 "「Angel Sign」予告編")

(c)「Angel Sign」製作委員会

LINK

[「Angel Sign」公式Site](https://angelsign.jp/)  
[AngelSignMovie (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[「Angel Sign」予告編](https://youtu.be/4gFDdfZBS7A)  