https://natalie.mu/comic/news/214191

# 北条司と井上雄彦がGJで師弟対談、ジャンプにおける連載とアンケートの関係語る
北条司和井上雄彦师徒在GJ对谈，谈Jump连载和问卷调查的关系  

2016年12月21日 [Comment](https://natalie.mu/comic/news/214191/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


本日12月21日に発売されたグランドジャンプ2017年2号（集英社）の、「GRAND TALKs」のコーナーに[北条司](https://natalie.mu/comic/artist/2405)と[井上雄彦](https://natalie.mu/comic/artist/1791)の対談が掲載されている。  
今天12月21日发售的《GRAND Jump》2017年第2号(集英社)中，“GRAND TALKs”栏目刊登了北条司和井上雄彦的对谈。


[
![北条司（左）と井上雄彦。](https://ogre.natalie.mu/media/news/comic/2016/1221/hojyoinoue_photo.jpg?impolicy=hq&imwidth=730&imdensity=1)
北条司（左）と井上雄彦。  
大きなサイズで見る（全5件）  
北条司(左)和井上雄彦。  
查看大图(共5件)  
](https://natalie.mu/comic/gallery/news/214191/631818)

[
![北条司と井上雄彦の合同サイン色紙。](https://ogre.natalie.mu/media/news/comic/2016/1221/hojyoinoue.jpg?imwidth=468&imdensity=1)
北条司と井上雄彦の合同サイン色紙。［拡大］  
北条司和井上雄彦的联合签名彩纸。[扩大]  
](https://natalie.mu/comic/gallery/news/214191/631819)

井上が過去に北条のアシスタントを務め、師弟関係にある2人。対談では当時の北条の仕事場についてや、週刊少年ジャンプ（集英社）における連載とアンケートの関係性、「SLAM DUNK」連載終盤のエピソードなどが語られた。なお12月24日に発売される月刊コミックゼノン2017年2月号（徳間書店）には、同対談の別バージョンが収録される。  
井上过去担任北条的助手，2人有师徒关系。对谈中谈到了当时北条工作的地方、周刊少年Jump(集英社)连载和问卷调查的关系、《灌篮高手》连载最后阶段的轶事等。另外，12月24日发售的月刊Comic Zenon 2017年2月号(德间书店)将收录该对谈的其他版本。

また集英社は北条と井上のイラスト入りサイン色紙を3名にプレゼント。色紙は北条と井上の合同版と、それぞれの単独版の計3種類が用意されている。希望者は今号のグランドジャンプに付属する券をハガキに貼り、2017年1月4日までに応募しよう。  
集英社还赠送了3位北条和井上的签名彩纸。彩纸有北条和井上的联合版和各自的单独版共3种。有意者将本期《Grand Jump》附赠的门票贴在明信片上，于2017年1月4日之前报名。

この記事の画像（全5件）  
这篇报道的图片(共5篇)  
[![北条司（左）と井上雄彦。](https://ogre.natalie.mu/media/news/comic/2016/1221/hojyoinoue_photo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214191/631818 "北条司（左）と井上雄彦。")
[![北条司と井上雄彦の合同サイン色紙。](https://ogre.natalie.mu/media/news/comic/2016/1221/hojyoinoue.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214191/631819 "北条司と井上雄彦の合同サイン色紙。")
[![北条司のサイン色紙。](https://ogre.natalie.mu/media/news/comic/2016/1221/hojyo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214191/631820 "北条司のサイン色紙。")
[![井上雄彦のサイン色紙。](https://ogre.natalie.mu/media/news/comic/2016/1221/inoue.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214191/631821 "井上雄彦のサイン色紙。")
[![グランドジャンプ2017年2号](https://ogre.natalie.mu/media/news/comic/2016/1221/gj_1702.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/214191/631613 "グランドジャンプ2017年2号")

LINK

[集英社 グランドジャンプ[GRANDJUMP] 公式Site](http://grandjump.shueisha.co.jp/)
[Comic Zenon](http://www.comic-zenon.jp/)

関連商品

[
![「Grand Jump 2017年2号」](https://images-fe.ssl-images-amazon.com/images/I/61V7YcQ3rAL._SS70_.jpg)
「Grand Jump 2017年2号」
[雑誌] 2016年12月21日発売 / 集英社 / 491-0276720174
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B01N6EATS6/nataliecomic-22)

[
![「月刊Comic Zenon 2017年2月号」](https://images-fe.ssl-images-amazon.com/images/I/61AJOz7Y0QL._SS70_.jpg)
「月刊コComic Zenon 2017年2月号」
[雑誌] 2016年12月24日発売 / 徳間書店 / 491-0137730274
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B01MDU36FX/nataliecomic-22)