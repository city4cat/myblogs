source:https://www.goodsmile.info/zh/product/8146/黏土人+槙村香.html  
日文版：https://www.goodsmile.info/ja/product/8146/ねんどろいど+槇村香.html  
英文版：https://www.goodsmile.info/en/product/8146/Nendoroid+Kaori+Makimura.html  


發售日期: 2019年10月

1101

# 黏土人 槙村香

### 給我收斂一點，獠！！

出自《劇場版 城市獵人〈新宿 PRIVATE EYES〉》，冴羽獠的夥伴「槙村香」，奇蹟地化身為黏土人！可替換的表情零件有「普通臉」、對獠大發雷霆的「生氣臉」、對獠感到無奈的「傻眼臉」。配件附有從哥哥那裡獲得的手槍與不能忘記的100t鐵鎚。著重於平衡感，股關節採用單軸可動零件。敬請搭配「[黏土人 冴羽獠](./zz_goodsmile.info_nendoroid_ryo.md)」（另售）一同賞玩。

### カホタンブログでご紹介 (2019年03月11日)  
在カホタンBlog上的介绍  
(译注：本节文字只在日文版才有)  

[
![](https://images.goodsmile.info/cgm/images/article/20131104/7/9076/original/1df0e6a7a1f74b6ae424ade98f44ed17.jpg)
](https://ameblo.jp/gsc-mikatan/entry-12443190304.html?utm_source=BLOG&utm_medium=8146&utm_campaign=pcsite)

[【CityHunter】「Nendoroid 槇村香」3月12日（火）案内開始☆](https://ameblo.jp/gsc-mikatan/entry-12443190304.html?utm_source=BLOG&utm_medium=8146&utm_campaign=pcsite)みなさまごきげんよう。新宿がダンジョンすぎて攻略できないカホタン（グッドスマイルカンパニー企画部）です。出口がたくさんあると迷ってしまう・・・！ さてさて、本日ご紹介するのは「ねんどろいど 槇村香」です◎『劇場版シティーハンター＜新宿プライベート・ア...  [>>続きを読む](https://ameblo.jp/gsc-mikatan/entry-12443190304.html?utm_source=BLOG&utm_medium=8146&utm_campaign=pcsite)  
【CityHunter】“黏土人 槙村香”3月12日(周二)开始介绍☆大家下午好，我是カホタン(GoodSmile Company企划部)。新宿太多dungeon，不能攻略。出口太多的话会迷路的…!好了好了，今天要介绍的是“Nendroid 槙村香”◎《剧场版城市猎人<新宿Private Eyes>》…>>接着读（译注：待校对。另，该ameblo.jp链接有更多截图。）  

### 商品詳情

商品名稱  
黏土人 槙村香

作品名稱  
劇場版 城市獵人〈新宿 PRIVATE EYES〉

製造商  
Good Smile Company

分類  
黏土人

價格  
5,297日圓 （含稅）

發售日期  
2019/10

商品規格  
ABS&PVC 塗裝完成可動模型・無比例・附專用台座・全高：約100mm

原型製作  
七兵衛

製作協力  
ねんどろん

- 本產品無法自行站立，請使用產品所附的台座  
- 本頁所刊登的照片與實際商品會有些許差異。  
- 本商品的色彩工程為手工作業，因此在塗裝上會有些許的個體差異。敬請見諒。  

©北条司/NSP・「2019 劇場版CityHunter」製作委員会  

## 購買方式

■ GOODSMILE線上商店  
![GOODSMILE ONLINE SHOP](https://images.goodsmile.info/media/gsc_online-d2be2a4607a4a05e364c9518f2bc3bda.png)

「GOODSMILE線上商店」的預購期間為  
日本時間2019年03月12日（二）12:00～2019年04月11日（四）12:00止。  
  
關於運費與發送相關說明請至「GOODSMILE線上商店」商品頁觀看。  
→ [GOODSMILE線上商店商品頁面](https://goodsmileshop.com/tw/p/G_NEN_WD_01101/?utm_source=internal&utm_medium=product&utm_campaign=8146)  
  
  
※開放期間中可隨時預購。  
※上述期間將決定販售預定數量。  
※期間後開放至達到預購數量上限為止。  

  

■ 實體通路  
商品預購期間等詳細資訊，請洽詢各店鋪。[合作店鋪一覽](http://partner.goodsmile.info/support/zh/partnershops/)

[![黏土人 槙村香](https://images.goodsmile.info/cgm/images/product/20190304/8146/58743/thumb/77826516a95a9d1901ab2286c30406dc.jpg)](#itemZoom1)
[![黏土人 槙村香](https://images.goodsmile.info/cgm/images/product/20190304/8146/58744/thumb/352f1417cf948616ecb77c6dcbf9db7a.jpg)](#itemZoom2)
[![黏土人 槙村香](https://images.goodsmile.info/cgm/images/product/20190304/8146/58745/thumb/70a60d110c6efaa63a74ff44ae29a986.jpg)](#itemZoom3)
[![※可搭配「黏土人 冴羽獠」（另售）一同賞玩](https://images.goodsmile.info/cgm/images/product/20190304/8146/58746/thumb/f4cffb5488ca314e7a16bab0b1d2476f.jpg)](#itemZoom4)
[![黏土人 槙村香](https://images.goodsmile.info/cgm/images/product/20190304/8146/58747/thumb/668844b19aed3cddbdf5619e4c1b3f31.jpg)](#itemZoom5)

[![](https://images.goodsmile.info/cgm/images/product/20190304/8146/58743/large/77826516a95a9d1901ab2286c30406dc.jpg)](https://images.goodsmile.info/cgm/images/product/20190304/8146/58743/large/77826516a95a9d1901ab2286c30406dc.jpg "黏土人 槙村香")
    
[![](https://images.goodsmile.info/cgm/images/product/20190304/8146/58744/large/352f1417cf948616ecb77c6dcbf9db7a.jpg)](https://images.goodsmile.info/cgm/images/product/20190304/8146/58744/large/352f1417cf948616ecb77c6dcbf9db7a.jpg "黏土人 槙村香")
    
[![](https://images.goodsmile.info/cgm/images/product/20190304/8146/58745/large/70a60d110c6efaa63a74ff44ae29a986.jpg)](https://images.goodsmile.info/cgm/images/product/20190304/8146/58745/large/70a60d110c6efaa63a74ff44ae29a986.jpg "黏土人 槙村香")
    
[![](https://images.goodsmile.info/cgm/images/product/20190304/8146/58746/large/f4cffb5488ca314e7a16bab0b1d2476f.jpg)](https://images.goodsmile.info/cgm/images/product/20190304/8146/58746/large/f4cffb5488ca314e7a16bab0b1d2476f.jpg "※可搭配「黏土人 冴羽獠」（另售）一同賞玩")  
    
※可搭配「黏土人 冴羽獠」（另售）一同賞玩
    
[![](https://images.goodsmile.info/cgm/images/product/20190304/8146/58747/large/668844b19aed3cddbdf5619e4c1b3f31.jpg)](https://images.goodsmile.info/cgm/images/product/20190304/8146/58747/large/668844b19aed3cddbdf5619e4c1b3f31.jpg "黏土人 槙村香")


相關商品

[![黏土人 冴羽獠](//images.goodsmile.info/cgm/images/product/20190204/8055/57974/thumb/9ce5694f5838e577b7d5ef01636c0ccd.jpg) 黏土人 冴羽獠](./zz_goodsmile.info_nendoroid_ryo.md)