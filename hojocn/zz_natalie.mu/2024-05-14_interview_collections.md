source: https://natalie.mu/eiga/pp/cityhunter  

# 日本初の実写化！Netflix映画「シティーハンター」冴羽獠が駆け抜ける“はじまりの物語” 

Netflix映画「シティーハンター」 PR   2024年4月22日  

![](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/pc_header_bg.jpg?imwidth=100&mpolicy=pp_image)  
![](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/pc_header_txt.png?impolicy=pp_image)  



鈴木亮平が主演を務めるNetflix映画「シティーハンター」の世界独占配信が、4月25日にスタートする。  
由铃木亮平主演的 Netflix 电影《城市猎人》将于 4 月 25 日开始全球独家发行。  

1985年に連載が始まり、現在、累計発行部数5000万部を超える北条司の大ヒットマンガを日本で初めて実写化した本作。舞台は令和の新宿だ。裏社会でのトラブル処理を請け負う冴羽獠が、相棒・槇村秀幸の死を発端に、槇村の妹・香と出会うことから物語は展開していく。超一流の腕を持ちながら美女の前ではだらしないスイーパー（始末屋）・獠に鈴木が扮し、森田望智、安藤政信、木村文乃もキャストに名を連ねる。監督は「ストロベリーナイト」シリーズの佐藤祐市が担当した。  
这部电影是日本首部改编自北条司热门漫画的真人电影，该漫画于 1985 年开始连载，目前销量已超过 5000 万册。 故事发生在 2025 年的新宿。 故事发生在受雇处理黑社会问题的冴羽獠在搭档槇村秀幸死后，遇到了槇村的妹妹香。 铃木在片中饰演Sweeper（杀手）獠，他拥有高超的技艺，但在美女面前却很搞笑，其他演员还包括森田望智、安藤政信、木村文乃。 影片由《Strawberry Night》系列的导演佐藤祐市执导。  

本作の配信を記念して、映画ナタリー、音楽ナタリー、コミックナタリーで全3回の特集を掲載する。映画ナタリーでは実写版キャストの鈴木・森田、アニメ版で声優を務めた神谷明・伊倉一恵を一挙に集めた座談会を実施。音楽ナタリーではエンディングテーマ「Get Wild Continual」を手がけたTM NETWORKにインタビューを行い作品を深掘りする。コミックナタリーにはヒコロヒーが登場。「シティーハンター」の“ライト層代表”として同作の魅力を語ってもらった。  
为庆祝影片的发行，映画Natalie、音楽Natalie和Comic Natalie共将刊登3期特辑。 映画 Natalie收录了真人演员铃木和森田，以及为动画版角色配音的神谷明和伊倉一恵的圆桌讨论。 音乐Natalie采访了创作片尾曲 “Get Wild Continual ”的 TM NETWORK，深入了解作品。 漫画Natalie采访了ヒコロヒー。 作为 “轻型”《城市猎人》的代表人物，她讲述了这部作品的魅力所在。  

文 / 田尻和花  


## TRAILER  
[Video@Youtube](https://www.youtube.com/embed/P_NoGRiyXrU?rel=0&playsinline=1)  


## INTERVIEW  

[Netflix电影“城市猎人”特集|完全不知道原作的ヒコロヒー看了之后，选角、演出、肌肉，全都很喜欢](./2024-05-14_interview_netflix-ch_review-from-nonfans.md)  

[就 Netflix 电影《城市猎人》 TM NETWORK 接受采访 | 最新 “Continual”《Get Wild》](./2024-05-02_interview_netflix-ch_tmnetwork.md)  

[铃木良平×森田望智×神谷明×伊倉一恵齐聚一堂！讨论 Netflix 电影「CityHunter」](./2023-04-26_interview_netflix-cityhunter01.md)   



## CHARACTER  

![CHARACTER](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/ttl_3.png?impolicy=pp_image)

[![冴羽獠（演：鈴木亮平）](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/chara_1.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/chara_1.jpg?impolicy=pp_image)

### 冴羽獠  
演：鈴木亮平

裏社会のトラブル処理を担うスイーパー（始末屋）で、類いまれなる身体能力・戦闘力・銃の腕前の持ち主。“シティーハンター”とも呼ばれる。「美女の依頼しか受けない」と断言するほどの女好きで超ドスケベ。相棒・槇村秀幸が不慮の事故で死亡し、その真相を追いかけることになる。

[![槇村香（演：森田望智）](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/chara_2.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/chara_2.jpg?impolicy=pp_image)

### 槇村香  
演：森田望智

槇村秀幸の義妹で、自身の誕生日の夜に兄を亡くす。死の真相を求め、彼のバディだった冴羽獠に付きまとうように。純粋かつ猪突猛進な性格で、捜査のためなら危険な場所にも飛び込んでいく。

[![槇村秀幸（演：安藤政信）](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/chara_3.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/chara_3.jpg?impolicy=pp_image)

### 槇村秀幸  
演：安藤政信

冴羽獠の親友で、スイーパー稼業の相棒。以前は腕利きの刑事だった。血のつながらない香を妹として育ててきたが、彼女の誕生日にその事実を伝えようと決める。

[![野上冴子（演：木村文乃）](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/chara_4.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/chara_4.jpg?impolicy=pp_image)

### 野上冴子  
演：木村文乃

槇村秀幸とかつて同僚だった警視庁刑事。不思議な関係性を保っている冴羽獠には何度も口説かれているがうまくかわしている。新宿で多発する連続暴力事件を捜査中。


---  

[![Netflix映画「シティーハンター」](https://ogre.natalie.mu/media/pp/static/eiga/cityhunter/poster.jpg?impolicy=pp_image)](https://www.netflix.com/CityHunter)  
Netflix映画「シティーハンター」  
Netflixにて独占配信中  

[視聴ページ](https://www.netflix.com/CityHunter)
詳細を見る

###### ストーリー

事の発端は、新宿駅東口の伝言板に書かれた「XYZ 妹をさがしてください」という依頼。超一流の始末屋（スイーパー）で“シティーハンター”とも呼ばれる冴羽獠と相棒・槇村秀幸は、コスプレイヤー・くるみの捜索を請け負うことになる。同じ頃、新宿では謎の暴力事件が多発していた。獠と槇村が捜査を続ける中、槇村は突然事故に遭い死亡してしまう。現場に駆け付けた獠は、そこで槇村の妹・香と遭遇。“兄の死の真相を知りたい”と香から依頼を受けるも、獠は事件に巻き込みたくないと彼女を避け続ける。しかし、香がくるみを発見・保護したことで事態は一変。くるみがコスプレイヤーとして出演するイベントに、獠と香は護衛役として潜入することになるのだが……。

###### スタッフ / キャスト

原作：北条司「シティーハンター」  
監督：佐藤祐市  
脚本：三嶋龍朗  
エンディングテーマ：TM NETWORK「Get Wild Continual」（Sony Music Labels Inc.）  
出演：鈴木亮平、森田望智、安藤政信、華村あすか、水崎綾女、片山萌美、阿見201、杉本哲太、迫田孝也 / 木村文乃、橋爪功

©北条司／コアミックス 1985