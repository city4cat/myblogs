https://natalie.mu/music/news/160950

# 西内まりや、実写版「Angel Heart」主題歌を書き下ろし

2015年9月25日  [Comment](https://natalie.mu/music/news/160950/comment) [音楽ナタリー編集部](https://natalie.mu/music/author/71)


[西内まりや](https://natalie.mu/music/artist/11330)が10月28日にニューシングル「Save me」をリリース。今作の表題曲が10月11日（日）に放送開始となる日本テレビ系ドラマ「Angel Heart」の主題歌のために西内自身が書き下ろした楽曲であることが発表された。  
西内玛利亚将于10月28日发行新单曲《Save me》。此次作品的标题曲是西内为了10月11日(星期日)开始放送的日本电视台系电视剧“Angel Heart”的主题曲而创作的歌曲。

[
![西内まりや](https://ogre.natalie.mu/media/news/music/2015/0924/nishiuchimariya_art201509.jpg?imwidth=468&imdensity=1)
西内まりや  
大きなサイズで見る（全14件）  
西内玛利亚  
查看大图(共14件)  
](https://natalie.mu/music/gallery/news/160950/413703)

[
![Save me（初回限定盤）](https://images-fe.ssl-images-amazon.com/images/I/51TY-cR8snL._SS70_.jpg)
西内まりや「Save me（初回限定盤）」  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B014FEA8L2/nataliemusic-22)

[
![Save me（CD＋DVD盤）](https://images-fe.ssl-images-amazon.com/images/I/51xLroxsrPL._SS70_.jpg)
西内まりや「Save me（CD＋DVD盤）」  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B014FEFXDA/nataliemusic-22)

[
![Save me（CD盤）](https://images-fe.ssl-images-amazon.com/images/I/51IaMM0TEXL._SS70_.jpg)
西内まりや「Save me（CD盤）」  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B014FEFHTU/nataliemusic-22)

「Angel Heart」は[北条司](https://natalie.mu/music/artist/2405)原作のマンガを実写化したもので、北条が代表作「シティーハンター」のキャラクターや設定をもとに、パラレルワールドで生きる冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）たちの活躍を描いた作品。実写化にあたり、冴羽リョウ役を上川達也、香瑩（シャンイン）役を[三吉彩花](https://natalie.mu/music/artist/58300)、槇村香役を[相武紗季](https://natalie.mu/music/artist/45754)、劉信宏（リュウシンホン）役を[三浦翔平](https://natalie.mu/music/artist/58983)、ファルコン役を[ブラザートム](https://natalie.mu/music/artist/24793)（バブルガムブラザーズ）、ドク役を[ミッキー・カーチス](https://natalie.mu/music/artist/13236)が演じるなど個性的な出演陣が名を連ねている。  
《Angel Heart》是由北条司原作的漫画改编而成的真人版作品，北条根据其代表作《城市猎人》中的角色和设定，创作出了生活在平行世界的冴羽獠(獠的汉字是反犬旁组合“僚”)。)等人活跃的作品。这部改编的真人电影拥有独特的演员阵容，冴羽獠由上川达也饰演、香莹由三吉彩花饰演、槙村香由相武纱季饰演、刘信宏由三浦翔平饰演，Falcon由ブラザートム饰演，医生由ミッキー・カーチス饰演。

「Save me」は“人間愛”をテーマに制作された楽曲で、美しくも切ないピアノの旋律や、力強い歌声が印象的なミディアムバラード。西内は同曲について「私にとっても新たな一歩と言ってもいい様な、今までにない曲調にも挑戦して、こだわって作った曲」とコメントしている。また今回の発表にあわせて、新たなアーティスト写真およびJacket写真が公開された。  
“Save me”是以“人间爱”为主题制作的乐曲，美丽又悲伤的钢琴旋律，强有力的歌声给人留下深刻印象的中板叙事曲。西内对这首歌评述道:“对我来说这也是新的一步，挑战了至今为止没有的曲调，是非常执着创作的歌曲。”另外，配合此次的发表，新的艺人照片及封面照片也一并公开。  

[
![西内まりや「Save me」初回限定盤Jacket](https://ogre.natalie.mu/media/news/music/2015/0924/nishiuchimariyaJK_shokai.jpg?imwidth=468&imdensity=1)
西内まりや「Save me」初回限定盤Jacket［拡大］  
](https://natalie.mu/music/gallery/news/160950/413704)

## 西内まりや Comment

この度、ドラマ「Angel Heart」の主題歌を歌わせて頂くことになりました。  
这次，我演唱了电视剧《Angel Heart》的主题曲。

ドラマのストーリーをイメージしながら愛をテーマに曲と歌詞を書かせて頂きました。  
一边想象电视剧的故事一边以爱为主题写了曲子和歌词。

「Angel Heart」にちなんで、「Angel heart」というワードを歌詞の中に入れているので、そこにも注目して頂きたいです  
因为“Angel Heart”的关系，歌词中加入了“Angel Heart”这个词，希望大家注意这一点。

私にとっても新たな一歩と言ってもいい様な、今までにない曲調にも挑戦して、こだわって作った曲なので、ドラマやCDなどで是非聴いて下さい。  
对我来说也可以说是新的一步，挑战了前所未有的曲调，是很讲究地制作的曲子，请一定要在电视剧和CD上听。



## ドラマ「Angel Heart」次屋尚プロデューサー Comment
电视剧《Angel Heart》次屋尚制作人评语

西内さんにはデビュー時連続ドラマに出演いただいたこともあり、最近の活躍と成長ぶりを注目させて頂いていました。  
西内出道时出演过连续剧，最近的活跃和成长令人瞩目。

モデル、女優、アーティストと幅広く才能を発揮され、まさに才色兼備ですね。  
在模特、女演员、艺术家等领域都发挥了才能，真是才貌双全啊。

今回のドラマはハードボイルドながらも愛を描く物語です。  
这次的电视剧是硬核的同时描写爱情的故事。

そんなドラマのテーマとヒロインの心情をみごとに歌い上げた素晴らしい楽曲を提供してくれました。  
她为我们提供了一首美妙的歌曲，优美地唱出了该剧的主题和女主角的情感。


この記事の画像（全14件）  
这篇报道的图片(共14篇)  

[![西内まりや](https://ogre.natalie.mu/media/news/music/2015/0924/nishiuchimariya_art201509.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/413703 "西内まりや")
[![西内まりや「Save me」初回限定盤Jacket](https://ogre.natalie.mu/media/news/music/2015/0924/nishiuchimariyaJK_shokai.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/413704 "西内まりや「Save me」初回限定盤Jacket")
[![西内まりや「Save me」CD＋DVD盤Jacket](https://ogre.natalie.mu/media/news/music/2015/0924/nishiuchimariyaJK_CDDVD.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/413705 "西内まりや「Save me」CD＋DVD盤Jacket")
[![西内まりや「Save me」CD盤Jacket](https://ogre.natalie.mu/media/news/music/2015/0924/nishiuchimariyaJK_CDonly.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/413706 "西内まりや「Save me」CD盤Jacket")
[![ドラマ「Angel Heart」にて冴羽リョウ役を演じる上川隆也。](https://ogre.natalie.mu/media/news/comic/2015/0812/ah-kawakami.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/396862 "ドラマ「Angel Heart」にて冴羽リョウ役を演じる上川隆也。")
[![冴羽リョウ (c)北条司/NSP 2001](https://ogre.natalie.mu/media/news/comic/2015/0812/ah-ryo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/396863 "冴羽リョウ (c)北条司/NSP 2001")
[![ドラマ「Angel Heart」にて香瑩役を演じる三吉彩花。](https://ogre.natalie.mu/media/news/comic/2015/0903/AH-miyoshi002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/405589 "ドラマ「Angel Heart」にて香瑩役を演じる三吉彩花。")
[![香瑩の原作カット。(c)北条司/NSP 2001](https://ogre.natalie.mu/media/news/comic/2015/0903/ah-shanin.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/405592 "香瑩の原作カット。(c)北条司/NSP 2001")
[![ドラマ「Angel Heart」にて槇村香役を演じる相武紗季。](https://ogre.natalie.mu/media/news/comic/2015/0907/aibusaki.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/407165 "ドラマ「Angel Heart」にて槇村香役を演じる相武紗季。")
[![槇村香の原作カット。(c)北条司/NSP 2001](https://ogre.natalie.mu/media/news/comic/2015/0907/kaori.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/407166 "槇村香の原作カット。(c)北条司/NSP 2001")
[![ドラマ「Angel Heart」にて劉信宏役を演じる三浦翔平。](https://ogre.natalie.mu/media/news/comic/2015/0911/miura-angelheart.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/409164 "ドラマ「Angel Heart」にて劉信宏役を演じる三浦翔平。")
[![劉信宏 (c)北条司/NSP 2001](https://ogre.natalie.mu/media/news/comic/2015/0911/shinhon-angelheart.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/409165 "劉信宏 (c)北条司/NSP 2001")
[![ドラマ「Angel Heart」ロゴ](https://ogre.natalie.mu/media/news/comic/2015/0903/ah_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/405593 "ドラマ「Angel Heart」ロゴ")
[![北条司「Angel Heart」原作ビジュアル](https://ogre.natalie.mu/media/news/comic/2015/0715/AH-visual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/music/gallery/news/160950/384516 "北条司「Angel Heart」原作ビジュアル")

## 西内まりや「Save me」収録内容

### CD

01\. Save me  
02\. Let's start over again  
03\. Save me（Instrumental）  
04\. Let's start over again（Instrumental）

### DVD

・「Save me」Music Video  
・「Save me」making video  

## 日本TV系「Angel Heart」

2015年10月11日（日）22:00～  
※初回30分拡大  
※2話以降、毎週日曜22:30より放送


LINK

[西内まりや Official Website | RISINGPRODUCTION](http://www.rising-pro.jp/artist/mariya/)
[Angel Heart | 日本TV](http://www.ntv.co.jp/angelheart/)

関連商品

[
![西内まりや「Save me（初回限定盤）」](https://images-fe.ssl-images-amazon.com/images/I/51TY-cR8snL._SS70_.jpg)
西内まりや「Save me（初回限定盤）」  
[CD＋DVD＋PhotoBook] 2015年10月28日発売 / SONIC GROOVE / AVCD-16558/B  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B014FEA8L2/nataliemusic-22)

[
![西内まりや「Save me（CD＋DVD盤）」](https://images-fe.ssl-images-amazon.com/images/I/51xLroxsrPL._SS70_.jpg)
西内まりや「Save me（CD＋DVD盤）」  
[CD＋DVD] 2015年10月28日発売 / SONIC GROOVE / AVCD-16559/B  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B014FEFXDA/nataliemusic-22)

[
![西内まりや「Save me（CD盤）」](https://images-fe.ssl-images-amazon.com/images/I/51IaMM0TEXL._SS70_.jpg)
西内まりや「Save me（CD盤）」  
[CD] 2015年10月28日発売 / SONIC GROOVE / AVCD-16560  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B014FEFHTU/nataliemusic-22)