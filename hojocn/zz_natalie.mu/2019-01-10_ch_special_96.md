https://natalie.mu/comic/news/315423

# 96年作「CityHunter Special The Secret Service」がTV放送
96年作品「City Hunter Special The Secret Service」TV放送

2019年1月10日  [Comment](https://natalie.mu/comic/news/315423/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「CityHunter Special The Secret Service」が、1月28日25時59分より日本テレビ「映画天国」にて放送される。  
北条司原作的动画《CityHunter Special The Secret Service》将于1月28日25点59分在日本电视台《电影天国》播出。

[
![「CityHunter Special　The Secret Service」](https://ogre.natalie.mu/media/news/comic/2019/0109/20190109NW00014.jpg?imwidth=468&imdensity=1)
「CityHunter Special　The Secret Service」  
大きなサイズで見る（全3件）  
大尺寸观看(共3件)  
](https://natalie.mu/comic/gallery/news/315423/1086201)

[
![「CityHunter Special　The Secret Service」より。](https://ogre.natalie.mu/media/news/comic/2019/0109/20190109NW00015.jpg?imwidth=468&imdensity=1)
「CityHunter Special　The Secret Service」より。［拡大］  
来自《CityHunter Special The Secret Service》。[扩大]  
](https://natalie.mu/comic/gallery/news/315423/1086202)

「CityHunter Special The Secret Service」は、1996年に日本テレビ系「金曜Roadshow」にて放送された、「シティーハンター」シリーズ初のテレビスペシャル作品。冴羽リョウと槇村香のもとに、来日中のガイナム共和国の大統領候補・マクガイアをガードしているシークレットサービス、新庄安奈を護衛してほしいという依頼が舞い込むところから物語が動き出す。  
《CityHunter Special The Secret Service》于1996年在日本电视台的《星期五Roadshow》中播出，是《城市猎人》系列的首部电视特别作品。冴羽獠和槙村香接到了一个请求，要求他们去保护来日本的盖南共和国总统候选人·麦克盖亚的secret service新庄安奈，故事由此展开。

なお今回のテレビ放送は「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」が2月8日に公開されることを記念したもの。番組内ではアニメ「シティーハンター」シリーズの魅力や、映画の情報も紹介される。  
另外这次的电视播放是为了纪念《剧场版城市猎人<新宿Private Eyes>》2月8日上映。节目中还会介绍动画《城市猎人》系列的魅力和电影的相关信息。

## 諏訪道彦（読売テレビエグゼクティブプロデューサー）コメント  
诹访道彦(读卖电视台执行制片人)  

「CityHunter Special The Secret Service」は、1992年の大ヒット映画「ボディガード」を少し意識した記憶があります。この作品直後から「名探偵コナン」を手がけることになる[こだま兼嗣](https://natalie.mu/comic/artist/13476)監督が、シナリオにも手を入れるほど気合が入っていた作品です。1996年1月5日に「金曜ロードショー」で放送し、その3日後にTVシリーズ「名探偵コナン」がスタートするというタイミング！ いろいろ手いっぱいでがんばった年末年始でした。そんな名作をじっくりとお楽しみくださいませ！  
记得《CityHunter Special The Secret Service》是在1992年的大热电影《Bodyguard》的基础上拍摄的。在这部作品之后不久就着手制作《名侦探柯南》的儿玉兼嗣导演，对剧本也进行了修改，是一部干劲十足的作品。1996年1月5日在“星期五Roadshow”播放，3天后TV系列《名侦探柯南》开播的时机!年末年初真是忙得不可开交。请好好享受这样的名作吧!




※リョウの漢字はけものへんに「僚」のつくりが正式表記。  
※两字的汉字在兽的旁边“僚”的构造是正式表记。  

この記事の画像（全3件）  
这篇报道的图片(共3篇)  

[![「CityHunter Special　The Secret Service」](https://ogre.natalie.mu/media/news/comic/2019/0109/20190109NW00014.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315423/1086201 "「CityHunter Special　The Secret Service」")
[![「CityHunter Special　The Secret Service」より。](https://ogre.natalie.mu/media/news/comic/2019/0109/20190109NW00015.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315423/1086202 "「CityHunter Special　The Secret Service」より。")
[![「CityHunter Special　The Secret Service」より。](https://ogre.natalie.mu/media/news/comic/2019/0109/20190109NW00016.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315423/1086203 "「CityHunter Special　The Secret Service」より。")

## 「CityHunter Special The Secret Service」

### 放送日時

日本テレビ（関東ローカル）：2019年1月28日（月）25:59～27:59  
日本电视台(关东地区):2019年1月28日(星期一)25:59 ~ 27:59  

### Staff・Cast

原作：[北条司](https://natalie.mu/comic/artist/2405)  
総監督：[こだま兼嗣](https://natalie.mu/comic/artist/13476)  
Cast：[神谷明](https://natalie.mu/comic/artist/60997)、[伊倉一恵](https://natalie.mu/comic/artist/95562)、[麻上洋子](https://natalie.mu/comic/artist/95546)、[玄田哲章](https://natalie.mu/comic/artist/19455)



(c)北条司／NSP・読売テレビ・Sunrise  

LINK

[映画天国](http://www.ntv.co.jp/eigatengoku/)  
[Stanley@映画天国公式 (@eigatengoku) | Twitter](https://twitter.com/eigatengoku)  
[CityHunter Special　The Secret Service｜作品紹介｜Sunrise](http://www.sunrise-inc.co.jp/work/detail.php?cid=235)   