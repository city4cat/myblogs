https://natalie.mu/comic/news/23473


# 吉祥寺に新名所、マンガを感じる店「CAFE ZENON」誕生
吉祥寺新名胜，充满漫画感的店「CAFE ZENON」诞生  

2009年11月8日   [Comment](https://natalie.mu/comic/news/23473/comment) Comic Natalie編集部  


来る11月11日、マンガ編集を行う株式会社コアミックスがプロデュースするカフェ「CAFE ZENON」が吉祥寺に開店する。正式オープンに先駆け11月6日、同店は報道陣を招いたレセプションパーティを開催した。  
即将到来的11月11日，由漫画编辑株式会社Coamix制作的咖啡店“CAFE ZENON”将在吉祥寺开业。在正式开业之前的11月6日，该店举办了招待记者的招待会。  

「空間の漫画雑誌」をコンセプトに掲げる同店は、店舗オープンを「新創刊」と表現。マンガとは「想念のパッケージ」であるとして、カフェで過ごす時間そのものがマンガ雑誌を読む行為に相当する濃密な空間を演出する。「北斗の拳」のコマ割を模様にした床、マンガ雑誌を積み重ねデコレーションされたシャンデリアなど、店内には各界クリエイターによって仕掛けられた数え切れない工夫が施されている。    
以“空间的漫画杂志”为理念的这家店，用“新创刊”来形容店铺的开业。漫画是“想念的包装”，在咖啡馆度过的时间就相当于阅读漫画杂志，营造出浓厚的空间。以《北斗神拳》的分格为图案的地板、用漫画杂志堆砌而成的枝形吊灯等，店内被各界创作者精心设计。  

レセプションはコアミックス代表取締役の堀江信彦の挨拶からスタート。「マンガ関係のカフェと聞いて、みんなマンガ喫茶を想像したと思いますが違います」と笑いながら同店の見所を紹介した。続いて、同店を経営するノース・スターズ・ピクチャーズの取締役である北条司、原哲夫、次原隆二が壇上に登場。代表陣による乾杯が行われ、盛大な拍手の中「CAFE ZENON」誕生が祝福された。  
招待会从Coamix代表董事堀江信彦的致辞开始。他笑着说:“一听说是漫画有关的咖啡店，大家就会联想到漫画咖啡店，但其实不是。”接着，经营该店的North Stars Pictures的董事北条司、原哲夫、次原隆二登上了讲台。代表们举行干杯，在盛大的掌声中祝贺了“CAFE ZENON”的诞生。  

同店に協力したクリエイターの紹介では、ロゴや小物デザイン担当のデビルロボッツ、グラフィックアート担当のライトニングボルト、シャンデリアを制作したキムソンヘ、絵画を手がけた井上文太、設計デザイン他を担当するカフェ・カンパニーが登場。デビルロボッツは同店を世界の客人たちに誇れる名所として「東京にきたら『CAFE ZENON』へ行け」とオススメすることを約束した。  
与该店合作的创作者介绍，logo和小东西设计担当的Devil Robots，图形艺术担当的Lightning Bolt，吊灯的制作金圣海，绘画手井上文太，设计担当等的Café Company登场。Devil Robots推荐该店作为世界客人们引以为豪的名胜，并称「如果来到东京，就去『CAFE ZENON』」  

絵画担当の井上は、ギターとバイオリンの演奏に合わせたライブペインティングを披露。人生初のライブペインティングということで最初こそ緊張した様子だったが、リズムを掴むにつれ動きにダイナミックさが加わり、最後は勢い余ってステージから転がり落ちるハプニングも。完成した作品は期間限定で展示されるとのこと。  
绘画担当的井上，配合吉他和小提琴的演奏现场彩绘。因为是人生第一次的现场彩绘，一开始是紧张的样子，不过，随着节奏的抓住动作加入了动感，最后也有气势过度地从舞台上滚落下来的意外。完成的作品将在限定时间内展出。

イベントラストにはクリスタルキングのムッシュ吉崎が登場し、アニメ「北斗の拳」の主題歌として知られる名曲「愛をとりもどせ!!」を熱唱。クラウドを魅了する「ユワ、シャーッ！」の叫びで場内を熱狂の渦に巻き込み、盛況の中パーティは幕を閉じた。  
活动最后，Crystal King Monsieur吉崎登场，热情演唱了作为动画《北斗神拳》主题曲而广为人知的名曲「愛をとりもどせ!!」。群众在「ユワ、シャーッ！」的呼声中陶醉了。 宴会在高潮中结束，观众们兴奋得热泪盈眶。



誰もがご存知「愛をとりもどせ!!」のサビを熱唱する、クリスタルキングのムッシュ吉崎。  
谁都知道「愛をとりもどせ!!」的副歌，Crystal King Monsieur吉崎热情演唱。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-live003.jpg)  

人生初のライブペインティングに挑戦する井上文太。3分の短い持ち時間で仕上げたとは思えぬ、鮮やかで力強いタッチの絵を描く。  
井上文太人生第一次挑战现场彩绘。画出了一幅笔触鲜明有力的画，不像是只用了短短3分钟的时间就完成的。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-live002.jpg)  

ギター、バイオリンの演奏に合わせてダイナミックに筆を動かす井上文太。絵を仕上げた直後、気が抜けたのかステージから転がり落ちるハプニングも。  
配合着吉他和小提琴的演奏，井上文太动笔。画完之后，可能是松懈了吧，意外从舞台上滚落下来。     
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-live001.jpg)  

コアミックス代表取締役の堀江信彦。   
Coamix董事长堀江信彦。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk000.jpg)  

「シティーハンター」「エンジェル・ハート」でおなじみ北条司。   
因《城市猎人》、《天使心》而为人熟知的北条司。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk001.jpg)  

「北斗の拳」「花の慶次～雲のかなたに～」の原哲夫。  
《北斗神拳》、《花之庆次~云之彼端》的作者原哲夫。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk002.jpg)  

「よろしくメカドック」「少年リーダム～友情・努力・勝利の詩～」で知られる次原隆二。  
以《请多指教跑车郎中》、《少年Ledum~友情·努力·胜利之诗~》而闻名的次原隆二。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk003.jpg)  

左から次原隆二、原哲夫、北条司。ノース・スターズ・ピクチャーズ取締役の3人。  
左起:次原隆二、原哲夫、北条司。North Stars Pictures的3名董事。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk004.jpg)  

「CAFE ZENON」誕生を記念した、代表陣による乾杯の風景。  
为纪念“CAFE ZENON”的诞生，代表们干杯的场景。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk005.jpg)  

ロゴや小物デザイン担当したデビルロボッツ。  
负责logo和小物件设计的devil robots。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk006.jpg)  

グラフィックアートを担当したライトニングボルト。  
负责绘图艺术的lightning bolt。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk007.jpg)  

マンガ雑誌をデコーレーションした、オリジナリティ溢れるシャンデリアを制作したキムソンヘ。  
金成海以漫画杂志为装饰，制作了充满独创性的吊灯。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk008.jpg)  

「CAFE ZENON」に飾られている絵画を手がけ、人生初ライブペインティングも行った井上文太。  
井上文太亲自创作了“CAFE ZENON”装饰的绘画，人生第一次现场彩绘。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk009.jpg)  

設計デザイン他を担当するカフェ・カンパニー。  
负责Design等的Café Company。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-talk010.jpg)  

「CAFE ZENON」制服には、同店のロゴマークがあしらわれている。  
“CAFE ZENON”制服上印有该店的标志。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-uniform001.jpg)  

「CAFE ZENON」制服背面。  
“CAFE ZENON”制服背面。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-uniform002.jpg)  

レセプションパーティは立食形式でフードも振舞われた。  
招待会上只有站着的人，而且有食物供应。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-food001.jpg)  

レセプションパーティにて振舞われたスイーツ。「CAFE ZENON」では60品以上のフード、120品以上のドリンクメニューを用意している。  
宴会上摆的甜点。“CAFE ZENON”提供60多种食物和120多种饮料。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon-food002.jpg)  

「CAFE ZENON」は吉祥寺を走る中央線の高架下にある。  
“CAFE ZENON”位于中央线吉祥寺的高架桥下。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon000.jpg)  

開店を記念して、店頭には各界著名人による多数のお祝いが届いていた。  
为了纪念开店，店里收到了各界知名人士的祝贺。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon001.jpg)  

「CAFE ZENON」中央の壁は「Center Wall Project」として、12名のクリエイターによるマンガをテーマにした絵画の展示販売を行う。第1回のテーマは「FUKIDASHIの世界『Congratulations!』」だ。  
“CAFE ZENON”中央的墙壁作为“Center Wall Project”，展示贩卖12名创作者以漫画为主题的绘画。第一回的主题是"FUKIDASHI的世界『Congratulations!』"  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon002.jpg)  

マンガ雑誌をデコレーションしたシャンデリア。週刊コミックバンチ（新潮社）などが積み重ねられている。  
装饰漫画杂志的吊灯。周刊comic bunch(新潮社)等堆积如山。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon005.jpg)  

「CAFE ZENON」店内の様子。  
“CAFE ZENON”店内情况。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon003.jpg)  

「CAFE ZENON」店内の様子。  
“CAFE ZENON”店内情况。  
![](https://ogre.natalie.mu/media/news/comic/2009/1107/zenon006.jpg)  






















