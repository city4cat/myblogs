https://natalie.mu/comic/news/176054

# 「CityHunter」リョウ生誕30周年で「BAR CAT'S EYE」が限定オープン
“城市猎人”獠诞辰30周年“BAR CAT'S EYE”限定开业

2016年2月13日 [Comment](https://natalie.mu/comic/news/176054/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「CityHunter」冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）の生誕30周年を記念したイベントが、本日2月13日から3月27日にかけて京都・元本能寺跡信長茶寮にて開催されている。  
北条司的“城市猎人”冴羽獠(獠的汉字是“僚”字的变体)诞辰30周年纪念活动于2月13日至3月27日在京都·元本能寺遗迹信长茶寮举行。

[
![「BAR CAT'S EYE」のOriginal Drink。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar008.jpg?impolicy=hq&imwidth=730&imdensity=1)
「BAR CAT'S EYE」のOriginal Drink。  
大きなサイズで見る（全20件）  
“BAR CAT'S EYE”的原创饮料。  
查看大图(共20件)  
](https://natalie.mu/comic/gallery/news/176054/475872)

期間中は地下1階のBAR ZIPANGUが「BAR CAT'S EYE」に変身。「バージンX.Y.Z」など「CityHunter」や「エンジェル・ハート」にちなんだオリジナルカクテルが登場するほか、土日は限定Menuの「100t coffee」「香Special Coffee」がオーダーできる。またバレンタインデーやホワイトデーにもスペシャルMenuを展開。リョウと香の思い出の日である3月26日にはシークレット企画も行われる予定だ。  
活动期间，地下一层的BAR ZIPANGU将变成“BAR CAT'S EYE”。除了推出“virgin x.y.z”等与“城市猎人”和“天使之心”有关的原创鸡尾酒外，周末还推出了限定菜单“100t”coffee”“香特别咖啡”可以点单。另外在情人节和白色情人节也推出特别菜单。在亮和香的回忆之日3月26日，还将举行秘密企划。

なおコミックナタリーでは新装版「CityHunter XYZ Edition」の刊行を記念した特集記事を展開中。モーショングラフィックアニメ「リョウのプロポーズ」を制作する神風動画のインタビューや、北条と小室哲哉が主題歌「Get Wild」の魅力を語り合う対談記事を掲載している。  
另外，漫画娜塔莉为了纪念新装版《城市猎人XYZ Edition》的发行而进行的特集报道也正在展开中。动画《凉的求婚》的制作公司神风动画的采访，以及北条和小室哲哉对谈主题歌《Get Wild》的魅力的对谈报道。  

この記事の画像（全20件）

[![「BAR CAT'S EYE」のOriginal Drink。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/475872 "「BAR CAT'S EYE」のOriginal Drink。")
[![土日限定Menuの「100t coffee」。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/475873 "土日限定Menuの「100t coffee」。")
[![土日限定Menuの「「香Special Coffee」はHoney and Mint入り。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/475874 "土日限定Menuの「「香Special Coffee」はHoney and Mint入り。")
[![「BAR CAT'S EYE」のImage。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/475875 "「BAR CAT'S EYE」のImage。")
[![「BAR CAT'S EYE」のImage。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/475876 "「BAR CAT'S EYE」のImage。")
[![「BAR CAT'S EYE」のImage。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/475877 "「BAR CAT'S EYE」のImage。")
[![「BAR CAT'S EYE」のImage。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/475878 "「BAR CAT'S EYE」のImage。")
[![「BAR CAT'S EYE」のImage。](https://ogre.natalie.mu/media/news/comic/2016/0213/cityhunter-bar007.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/475879 "「BAR CAT'S EYE」のImage。")
[![「CityHunter XYZ Edition」1巻](https://ogre.natalie.mu/media/news/comic/2015/1219/XYZ01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450680 "「CityHunter XYZ Edition」1巻")
[![「CityHunter XYZ Edition」2巻](https://ogre.natalie.mu/media/news/comic/2015/1219/XYZ02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450679 "「CityHunter XYZ Edition」2巻")
[![「CityHunter XYZ Edition」3巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450678 "「CityHunter XYZ Edition」3巻")
[![「CityHunter XYZ Edition」4巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450677 "「CityHunter XYZ Edition」4巻")
[![「CityHunter XYZ Edition」5巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450676 "「CityHunter XYZ Edition」5巻")
[![「CityHunter XYZ Edition」6巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450675 "「CityHunter XYZ Edition」6巻")
[![「CityHunter XYZ Edition」7巻](https://ogre.natalie.mu/media/news/comic/2015/1219/XYZ07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450674 "「CityHunter XYZ Edition」7巻")
[![「CityHunter XYZ Edition」8巻](https://ogre.natalie.mu/media/news/comic/2015/1219/XYZ08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450673 "「CityHunter XYZ Edition」8巻")
[![「CityHunter XYZ Edition」9巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450672 "「CityHunter XYZ Edition」9巻")
[![「CityHunter XYZ Edition」10巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450671 "「CityHunter XYZ Edition」10巻")
[![「CityHunter XYZ Edition」11巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450670 "「CityHunter XYZ Edition」11巻")
[![「CityHunter XYZ Edition」12巻](https://ogre.natalie.mu/media/news/comic/2015/1219/xyz12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/176054/450669 "「CityHunter XYZ Edition」12巻")

## 冴羽リョウ生誕30周年特別企画 BAR CAT'S EYE
冴羽凉诞生30周年特别企划BAR CAT'S EYE  

期間：2016年2月13日（土）～3月27日（日）  
BarTime：20:00～26:00（日曜日は24:00まで）  
CafeTime：11:00～17:00（土日限定）  
会場：元本能寺跡 信長茶寮 B1F  
住所：京都市中京区西洞院通蛸薬師上る池須町419-2

LINK

[冴羽獠 生誕30周年特別企画 BAR CAT'S EYE](http://www.shinchou-saryou.jp/barcatseye2016.html)

関連商品

[
![北条司「CityHunter XYZ edition（1）」](https://images-fe.ssl-images-amazon.com/images/I/51wmgxxDDoL._SS70_.jpg)
北条司「CityHunter XYZ edition（1）」  
[漫画] 2015年7月18日発売 / 徳間書店 / 978-4199802812  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802819/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（2）」](https://images-fe.ssl-images-amazon.com/images/I/51OzDNHsMmL._SS70_.jpg)
北条司「CityHunter XYZ edition（2）」  
[漫画] 2015年7月18日発売 / 徳間書店 / 978-4199802829  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802827/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（3）」](https://images-fe.ssl-images-amazon.com/images/I/51Lfk67h6PL._SS70_.jpg)
北条司「CityHunter XYZ edition（3）」  
[漫画] 2015年8月20日発売 / 徳間書店 / 978-4199802911  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802916/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（4）」](https://images-fe.ssl-images-amazon.com/images/I/51dZQFM%2BoOL._SS70_.jpg)
北条司「CityHunter XYZ edition（4）」  
[漫画] 2015年8月20日発売 / 徳間書店 / 978-4199802928  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802924/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（5）」](https://images-fe.ssl-images-amazon.com/images/I/51sQLGuDeTL._SS70_.jpg)
北条司「CityHunter XYZ edition（5）」  
[漫画] 2015年9月19日発売 / 徳間書店 / 978-4199802973  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802975/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（7）」](http://ecx.images-amazon.com/images/I/61GaFnGM8SL._SS70_.jpg)
北条司「CityHunter XYZ edition（7）」  
[漫画] 2015年10月20日発売 / 徳間書店 / 978-4199803079  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803076/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（8）」](https://images-fe.ssl-images-amazon.com/images/I/51Yqg0sD4dL._SS70_.jpg)
北条司「CityHunter XYZ edition（8）」 
[漫画] 2015年10月20日発売 / 徳間書店 / 978-4199803086  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803084/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（9）」](https://images-fe.ssl-images-amazon.com/images/I/61itv2mLCLL._SS70_.jpg)
北条司「CityHunter XYZ edition（9）」  
[漫画] 2015年11月1日発売 / 徳間書店 / 978-4199803154  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803157/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（10）」](https://images-fe.ssl-images-amazon.com/images/I/51P1VG2%2BBoL._SS70_.jpg)
北条司「CityHunter XYZ edition（10）」  
[漫画] 2015年10月1日発売 / 徳間書店 / 978-4199803161  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803165/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（11）」](https://images-fe.ssl-images-amazon.com/images/I/511-MjR4IzL._SS70_.jpg)
北条司「CityHunter XYZ edition（11）」  
[漫画] 2015年12月19日発売 / 徳間書店 / 978-4199803222  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/419980322X/nataliecomic-22)

[
![北条司「CityHunter XYZ edition（12）」](https://images-fe.ssl-images-amazon.com/images/I/51Wrw2DejZL._SS70_.jpg)
北条司「CityHunter XYZ edition（12）」  
[漫画] 2015年12月19日発売 / 徳間書店 / 978-4199803239  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199803238/nataliecomic-22)