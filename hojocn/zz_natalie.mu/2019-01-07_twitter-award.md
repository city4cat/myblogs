https://natalie.mu/comic/news/315072

# 「劇場版シティーハンター」神谷明サイン入りポスターなどがTwitterで当たる
「剧场版City Hunter」神谷明签名海报等推特奖品  

2019年1月7日  [Comment](https://natalie.mu/comic/news/315072/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」の公開を記念し、飲食店を展開するニラックスとコラボしたTwitterキャンペーンを開催。第1弾が1月8日から13日まで、第2弾が1月8日から2月7日まで実施される。  
为了纪念根据北条司原作改编的动画电影《剧场版城市猎人<新宿Private Eyes>》的上映，与经营餐饮店的nilax合作举办了推特活动。第1弹于1月8日至13日举行，第2弹将于1月8日至2月7日举行。

[
![「劇場版シティーハンター <新宿プライベート・アイズ>」ポスター](https://ogre.natalie.mu/media/news/comic/2019/0107/city_poster.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター <新宿プライベート・アイズ>」ポスター
大きなサイズで見る  
《剧场版城市猎人<新宿Private Eyes>》海报  
大尺寸观看  
](https://natalie.mu/comic/gallery/news/315072/1084755)

第1弾ではキャンペーンに参加した人の中から抽選で2組4名を、1月29日に行われる完成披露試写会に招待。希望者はニラックスの公式アカウントをフォローし、該当のツイートをRTしよう。また第2弾では[神谷明](https://natalie.mu/comic/artist/60997)のサイン入りポスター、ブランケット、缶バッジマグネットといったグッズを抽選でプレゼント。同キャンペーンにはニラックスの公式アカウントをフォローし、ニラックス系列の店舗で撮影したテーブルPOPの写真をハッシュタグ「#劇場版シティーハンターニラックス」を付けてツイートすると参加できる。  
第1弹从参加了宣传活动的人中抽选2组4名，邀请在1月29日举行的完成披露试映会。有意者请关注nilax的官方账号，并转发相应的推文。另外，第2弹还将以抽签形式赠送神谷明的签名海报、毛毯、易拉罐徽章磁铁等周边商品。关注nilax的官方账号，将在nilax系列店铺拍摄的POP照片加上主题标签“#剧场版城市猎人nilax”发表推特，即可参加该活动。


「劇場版シティーハンター <新宿プライベート・アイズ>」は2月8日ロードショー。神谷、伊倉一恵らTVアニメのキャストが再集結し、映画のために書き下ろされたオリジナルストーリーが展開される。  
《剧场版城市猎人<新宿Private Eyes>》将于2月8日上映。神谷、伊仓一惠等TV动画的Cast再次集结，为电影谱写的原创故事即将展开。

[
![](https://pbs.twimg.com/profile_images/895173138958438401/mbCUTEuh_normal.jpg)
](https://twitter.com/nilax_buffet?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1082167899421257728%7Ctwgr%5E77fc182170d8ebc5572c7b91b9635ca699ecb060%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fnatalie.mu%2Fcomic%2Fnews%2F315072)

[
nilax\_buffet《スペインフェア開催中》  
nilax_buffet《Spain Fair举行中》  
](https://twitter.com/nilax_buffet?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1082167899421257728%7Ctwgr%5E77fc182170d8ebc5572c7b91b9635ca699ecb060%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fnatalie.mu%2Fcomic%2Fnews%2F315072)

[
@nilax\_buffet
](https://twitter.com/nilax_buffet?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1082167899421257728%7Ctwgr%5E77fc182170d8ebc5572c7b91b9635ca699ecb060%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fnatalie.mu%2Fcomic%2Fnews%2F315072)

·

[フォローする（Follow us）](https://twitter.com/intent/follow?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1082167899421257728%7Ctwgr%5E77fc182170d8ebc5572c7b91b9635ca699ecb060%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fnatalie.mu%2Fcomic%2Fnews%2F315072&screen_name=nilax_buffet)

[](https://twitter.com/nilax_buffet/status/1082167899421257728?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1082167899421257728%7Ctwgr%5E77fc182170d8ebc5572c7b91b9635ca699ecb060%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fnatalie.mu%2Fcomic%2Fnews%2F315072)

(c)北条司/NSP・「2019 劇場版シティーハンター」製作委員会

LINK

[ニラックス【公式】食べ放題・ブッフェ (@nilax\_buffet) | Twitter](https://twitter.com/nilax_buffet)  
[アニメ「劇場版シティーハンター <新宿プライベート・アイズ>」公式サイト | 2019年2月8日全国ロードショー](https://cityhunter-movie.com/)  