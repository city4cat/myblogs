https://natalie.mu/comic/news/35795

# 「エンジェル・ハート」最終回、2ndシーズン予定あり

2010年8月6日  [Comment](https://natalie.mu/comic/news/35795/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート」が本日8月6日発売の週刊コミックバンチ36・37合併号（新潮社）にて最終回を迎えた。今号掲載話までを「1stシーズン」と区切り「2ndシーズン」が制作予定であることも明かしている。  
北条司“天使心”在8月6日发行的周刊Comic Bunch 36·37合并号(新潮社)上迎来了最终回。同时宣布，这一期的故事将分为 "第1季"，并将制作 "第2季"。（译注：待校对）

[
![「エンジェル・ハート」1stシーズン最終話の1ページ。2ndシーズンのスタートが待たれる。(C)北条司 2001　](https://ogre.natalie.mu/media/news/comic/2010/0806/angelheart-last.jpg?imwidth=468&imdensity=1)
「エンジェル・ハート」1stシーズン最終話の1ページ。2ndシーズンのスタートが待たれる。(C)北条司 2001  
大きなサイズで見る（全2件）  
《天使之心》第一季最终话的一页。第二季的开始令人期待。(C)北条司2001  
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/35795/53845)

「エンジェル・ハート」は週刊コミックバンチ創刊号からのラインナップとして、2001年より連載されてきた人気作品。北条の代表作「シティーハンター」のキャラクターが活躍することでも話題を集め、2005年にはTVアニメ化された。  
“Angel Heart”作为周刊Comic Bunch创刊号的人气作品。北条的代表作“城市猎人”的角色活跃在其中，引起了话题，2005年被TV动画化。

1stシーズン最終話までを収録した単行本33巻は9月9日発売。2ndシーズンの詳細は北条の公式サイトおよび、北条の公式Twitterにて発表される予定だ。  
收录第一季最终话的单行本33卷将于9月9日发售。第二季的详细内容将在北条的官方网站以及北条的官方Twitter上发布。

この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![「エンジェル・ハート」1stシーズン最終話の1ページ。2ndシーズンのスタートが待たれる。(C)北条司 2001　](https://ogre.natalie.mu/media/news/comic/2010/0806/angelheart-last.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/35795/53845 "「エンジェル・ハート」1stシーズン最終話の1ページ。2ndシーズンのスタートが待たれる。(C)北条司 2001　")
[![週刊コミックバンチ36・37合併号](https://ogre.natalie.mu/media/news/comic/2010/0806/bunch10-3637.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/35795/53846 "週刊コミックバンチ36・37合併号")


LINK  
[北条司 OFFICIAL WEB SITE](http://www.hojo-tsukasa.com)  
[北条司Official (hojo_official) on Twitter](http://twitter.com/hojo_official)  


