https://natalie.mu/comic/news/353417

# 「劇場版CityHunter」リョウ＆海坊主モデルのサングラス、Zoffから登場

2019年10月30日  [Comment](https://natalie.mu/comic/news/353417/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版CityHunter <新宿Private Eyes>](https://natalie.mu/eiga/film/175572)」と、メガネブランド・Zoffのコラボサングラスが登場。11月1日より東京と大阪で順次開催される企画展「劇場版CityHunter PRIVATE EXHIBITION」にて限定販売される。
根据北条司原作改编的动画电影《剧场版CityHunter <新宿Private Eyes >》和眼镜品牌Zoff的合作太阳镜登场。将于11月1日在东京和大阪依次举办的企划展“剧场版CityHunter PRIVATE EXHIBITION”中限定发售。

[
![冴羽リョウモデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub1.jpg?impolicy=hq&imwidth=730&imdensity=1)
冴羽リョウモデルのサングラス。  
大きなサイズで見る（全9件）  
冴羽獠模型的太阳眼镜。  
查看大图(共9件)  
](https://natalie.mu/comic/gallery/news/353417/1267262)

[
![海坊主モデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub5.jpg?imwidth=468&imdensity=1)
海坊主モデルのサングラス。［拡大］  
海坊主模特的太阳镜。[放大]  
](https://natalie.mu/comic/gallery/news/353417/1267267)

「劇場版CityHunter ＜新宿Private Eyes＞」のBlu-ray / DVD発売を記念して制作された本アイテム。冴羽リョウモデルと海坊主モデルの2型が用意され、それぞれにオリジナルデザインのメガネ拭きとケースが付属する。価格はどちらも税込各1万円。Zoff店舗では販売されないためご注意を。  
为纪念《剧场版CityHunter <新宿Private Eyes>》的蓝光/ DVD发售而制作的本道具。有冴羽獠款和海坊主2款，分别附带原创设计的擦眼镜布和盒子。价格都是含税各1万日元。请注意Zoff店铺不销售。  

なおコミックナタリーでは「劇場版CityHunter <新宿Private Eyes>」のBlu-ray / DVD発売を記念した特集を展開中。神谷が静岡にあるディスク工場を訪れた際のレポートやインタビューを掲載している。  
另外，comic natalie还推出了纪念《剧场版CityHunter <新宿Private Eyes>》蓝光/ DVD发售的特集。刊登了神谷访问静冈光盘工厂时的报告和采访。

この記事の画像（全9件）  
这篇报道的图片(共9篇)  

[![冴羽リョウモデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267262 "冴羽リョウモデルのサングラス。")
[![海坊主モデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub5.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267267 "海坊主モデルのサングラス。")
[![「劇場版CityHunter <新宿Private Eyes>」とZoffのコラボロゴ。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267263 "「劇場版CityHunter <新宿Private Eyes>」とZoffのコラボロゴ。")
[![冴羽リョウモデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267264 "冴羽リョウモデルのサングラス。")
[![冴羽リョウモデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267265 "冴羽リョウモデルのサングラス。")
[![冴羽リョウモデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267266 "冴羽リョウモデルのサングラス。")
[![海坊主モデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub6.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267268 "海坊主モデルのサングラス。")
[![海坊主モデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub7.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267269 "海坊主モデルのサングラス。")
[![海坊主モデルのサングラス。](https://ogre.natalie.mu/media/news/comic/2019/1030/cityhunter_movie_sub8.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353417/1267270 "海坊主モデルのサングラス。")

## 劇場版CityHunter PRIVATE EXHIBITION

期間：2019年11月1日（金）～6日（水）  
会場：東京都 LUMINE0（ルミネゼロ）

期間：2020年1月9日（木）～20日（月）  
会場：大阪府 あべのハルカス近鉄本店 ウイング館4階 第2催会場  
大阪府安倍harukas近铁总店翼馆4楼第2举办会场  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  
※冴羽獠的獠，在动物旁正式标明“僚”的写法  


(c)北条司/NSP・「2019 劇場版CityHunter」製作委員会

LINK

[アニメ「劇場版CityHunter <新宿Private Eyes>」公式サイト](https://cityhunter-movie.com/)  
[アニメ「劇場版CityHunter ＜新宿Private Eyes＞」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)  
