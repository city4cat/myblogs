https://natalie.mu/comic/news/500000

# 「シティーハンター」ミック・香・獠にスポットを当てたポップアップショップが岩手で  
岩手县的POP UP STORE专注于「CityHunter」Mick、香、獠  

2022年11月3日 20:26 199 [Comment](https://natalie.mu/comic/news/500000/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」のポップアップショップ「シティーハンターin岩手～MKRな決闘編～POP UP STORE」が、11月29日から12月4日まで岩手・さくら野百貨店北上店にて開催される。  
北条司《城市猎人》的POP UP STORE“城市猎人in岩手~ MKR的决斗篇~ POP UP STORE”将于11月29日至12月4日在岩手 樱野百货店北上店举行。  

[
![「シティーハンター in岩手～MKRな決闘編～ POP UP STORE」ビジュアル](https://ogre.natalie.mu/media/news/comic/2022/1103/cityhunter_pus01.jpg?imwidth=200&imdensity=1)
「シティーハンター in岩手～MKRな決闘編～ POP UP STORE」ビジュアル  
大きなサイズで見る（全7件）  
“城市猎人in岩手~ MKR的决斗篇~ POP UP STORE”视觉图  
查看大图(共7件)  
](https://natalie.mu/comic/gallery/news/500000/1933716)

ミック、香、獠の3人にスポットを当て開催される同イベント。会場には扉絵がデザインされたTシャツやミック、香、獠がプリントされたアクリルブロックのセット、コーヒーハウス「キャッツアイ」のマスターこだわりの国産はちみつなど、数種類のグッズが用意されている。さらに税込5000円以上購入した人にはしおりを1枚プレゼント。しおりは3種類のなかから希望のものを選ぶことができる。グッズの詳細は墓場の画廊の特設ページを確認しよう。  
针对米克、香、獠3人举行的该活动。会场上还准备了数种周边，包括印有扉页图案的T恤、印着米克、香、獠的亚克力积木套装、猫眼咖啡店Master特色的国产蜂蜜等。购买含税5000日元以上者赠送书签一枚。书签可以从3种中选择自己想要的。周边的详细内容请在墓地的画廊特设页面上确认。  

この記事の画像（全7件）  
这篇文章的图片(共7条)
  
[![「シティーハンター in岩手～MKRな決闘編～ POP UP STORE」ビジュアル](https://ogre.natalie.mu/media/news/comic/2022/1103/cityhunter_pus01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/500000/1933716 "「シティーハンター in岩手～MKRな決闘編～ POP UP STORE」ビジュアル")
[![「HERO Tシャツ」](https://ogre.natalie.mu/media/news/comic/2022/1103/cityhunter_pus02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/500000/1933721 "「HERO Tシャツ」")
[![「MKRミニアクリルブロック3種セット（リョウ 香 ミック）」](https://ogre.natalie.mu/media/news/comic/2022/1103/cityhunter_pus03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/500000/1933722 "「MKRミニアクリルブロック3種セット（リョウ 香 ミック）」")
[![「アニメ版 ブロマイド5枚セットD」](https://ogre.natalie.mu/media/news/comic/2022/1103/cityhunter_pus04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/500000/1933720 "「アニメ版 ブロマイド5枚セットD」")
[![「コーヒーハウス『キャッツアイ』Tシャツ」](https://ogre.natalie.mu/media/news/comic/2022/1103/cityhunter_pus05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/500000/1933718 "「コーヒーハウス『キャッツアイ』Tシャツ」")
[![「コーヒーハウス『キャッツアイ』～マスターのこだわりハニー～」](https://ogre.natalie.mu/media/news/comic/2022/1103/cityhunter_pus06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/500000/1933719 "「コーヒーハウス『キャッツアイ』～マスターのこだわりハニー～」")
[![購入特典のしおり全3種。](https://ogre.natalie.mu/media/news/comic/2022/1103/cityhunter_pus07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/500000/1933717 "購入特典のしおり全3種。")

## 「シティーハンター in岩手～MKRな決闘編～ POP UP STORE」

日程：2022年11月29日（火） ～ 12月4日（日）  
時間：10:00～20:00  
会場：岩手県 さくら野百貨店北上店（岩手县樱野百货店北上店）




LINK

[★出店情報★【シティーハンター in岩手〜MKRな決闘編〜 POP UP STORE】の開催が決定。11月29日(火)〜12月4日(日)の期間、さくら野百貨店北上店で開催‼︎■ CITY HUNTER | 墓場の画廊  
（★开店信息★【城市猎人in岩手~ MKR的决斗篇~ POP UP STORE】的举办决定。11月29日(周二)~12月4日(周日)期间，在樱野百货商店北上店举办!︎■City Hunter |坟墓的画廊）](https://hakabanogarou.jp/archives/33726)

