https://natalie.mu/comic/news/102208

# 冴羽リョウの“抱かれ枕”など、ゼノンでプレゼント多数

2013年10月25日  [Comment](https://natalie.mu/comic/news/102208/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


月刊コミックゼノン（徳間書店）が、本日10月25日に発売された12月号にて創刊3周年を迎えた。  
月刊Comic Zenon(德间书店)在今天10月25日发售的12月号上迎来了创刊3周年。

[
![「抱かれたり、抱いたり枕」のうち1種。「エンジェル・ハート 2ndシーズン」のもの。](https://ogre.natalie.mu/media/news/comic/2013/1025/daki_ryo2.jpg?imwidth=468&imdensity=1)  
「抱かれたり、抱いたり枕」のうち1種。「エンジェル・ハート 2ndシーズン」のもの。  
大きなサイズで見る（全11件）  
“被抱、抱枕”中的一种。“天使之心第二季”的东西。  
查看大图(共11件)  
](https://natalie.mu/comic/gallery/news/102208/210671)

これを記念し今号では、連載作品にちなんだプレゼントを多数ラインナップ。「エンジェル・ハート 2ndシーズン」や「いくさの子 織田三郎信長伝」などのキャラクターをデザインした、特大サイズの「抱かれたり、抱いたり枕」が抽選で合計75名に贈呈されるほか、WEBコミックぜにょんの連載作家による直筆色紙なども用意されている。応募の締め切りは11月24日。詳細は誌面にて確認しよう。  
为了纪念这个，在本期中，连载作品相关的礼物很多。《天使之心第二季》和《战争之子 织田三郎信长传”等角色的特大号“抱枕”将通过抽签赠送给共计75人，此外还准备了WEB Comic Zenon连载作家的亲笔彩纸等。应征截止日期为11月24日。详细内容请在杂志上确认。

また綴じ込み付録として、[武論尊](https://natalie.mu/comic/artist/2383)原作による[原哲夫](https://natalie.mu/comic/artist/1917)「北斗の拳」の設定資料集も付属。原へのインタビューや、刊行中の「究極版」に収録される新エピソードのキャラクターが掲載されているので、ファンは要チェックだ。  
另外，作为装订附录，根据武论尊原作的原哲夫《北斗神拳》的设定资料集也一并附赠。对原的采访，以及发行中的“究极版”中收录的新一集的角色，粉丝们一定要看看。

そのほか今号では、ルネサンス期のフィレンツェを舞台にした[大久保圭](https://natalie.mu/comic/artist/10760)の「アルテ」と、[竹添裕史](https://natalie.mu/comic/artist/10761)がNBAを目指す高校生を描く「籠球者－バスケモノ－」の新連載2本がスタート。また鬼灯の「バック・トゥー・ザ・ヒーロー」は最終回を迎えた。  
此外，本期还将推出以文艺复兴时期的佛罗伦萨为舞台的大久保圭的《アルテ》和竹添裕史描写以NBA为目标的高中生的《篮球者－バスケモノ－》两篇新连载。鬼灯的《回到英雄》迎来了最终回。

この記事の画像（全11件）  
这篇报道的图片(共11篇)  

[![「抱かれたり、抱いたり枕」のうち1種。「エンジェル・ハート 2ndシーズン」のもの。](https://ogre.natalie.mu/media/news/comic/2013/1025/daki_ryo2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210671 "「抱かれたり、抱いたり枕」のうち1種。「エンジェル・ハート 2ndシーズン」のもの。")
[![「抱かれたり、抱いたり枕」のうち1種。「いくさの子 織田三郎信長伝」のもの。](https://ogre.natalie.mu/media/news/comic/2013/1025/daki_ikusa.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210672 "「抱かれたり、抱いたり枕」のうち1種。「いくさの子 織田三郎信長伝」のもの。")
[![「抱かれたり、抱いたり枕」のうち1種。「おとりよせ王子 飯田好実」のもの。](https://ogre.natalie.mu/media/news/comic/2013/1025/daki_otori.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210673 "「抱かれたり、抱いたり枕」のうち1種。「おとりよせ王子 飯田好実」のもの。")
[![「抱かれたり、抱いたり枕」のうち1種。「義風堂々!! 直江兼続 －前田慶次 酒語り－」のもの。](https://ogre.natalie.mu/media/news/comic/2013/1025/daki_gifu.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210674 "「抱かれたり、抱いたり枕」のうち1種。「義風堂々!! 直江兼続 －前田慶次 酒語り－」のもの。")
[![「抱かれたり、抱いたり枕」のうち1種。「コンシェルジュプラチナム」のもの。](https://ogre.natalie.mu/media/news/comic/2013/1025/daki_con.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210675 "「抱かれたり、抱いたり枕」のうち1種。「コンシェルジュプラチナム」のもの。")
[![「抱かれたり、抱いたり枕」のうち1種。「ワカコ酒」のもの。](https://ogre.natalie.mu/media/news/comic/2013/1025/daki_wakako2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210676 "「抱かれたり、抱いたり枕」のうち1種。「ワカコ酒」のもの。")
[![「抱かれたり、抱いたり枕」の使用例。](https://ogre.natalie.mu/media/news/comic/2013/1025/daki_image.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210677 "「抱かれたり、抱いたり枕」の使用例。")
[![月刊コミックゼノン12月号](https://ogre.natalie.mu/media/news/comic/2013/1025/zenon13-11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210678 "月刊コミックゼノン12月号")
[![「北斗の拳［究極極秘資料集］」](https://ogre.natalie.mu/media/news/comic/2013/1025/hokuto.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210679 "「北斗の拳［究極極秘資料集］」")
[![竹添裕史「籠球者－バスケモノ－」のカット。 (c)竹添裕史/NSP 2013](https://ogre.natalie.mu/media/news/comic/2013/1025/baskemono.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210680 "竹添裕史「籠球者－バスケモノ－」のカット。 (c)竹添裕史/NSP 2013")
[![大久保圭「アルテ」のカット。 (c)大久保圭/NSP 2013](https://ogre.natalie.mu/media/news/comic/2013/1025/arte.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/102208/210681 "大久保圭「アルテ」のカット。 (c)大久保圭/NSP 2013")


LINK

[Comic Zenon](http://www.comic-zenon.jp/index.html)

関連商品

[
![原哲夫,武論尊「北斗の拳［究極版］（1）」](https://images-fe.ssl-images-amazon.com/images/I/513FUtNBUxL._SS70_.jpg)
原哲夫,武論尊「北斗の拳［究極版］（1）」  
2013年9月20日発売 / 徳間書店 / 978-4199801518  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801510/nataliecomic-22)