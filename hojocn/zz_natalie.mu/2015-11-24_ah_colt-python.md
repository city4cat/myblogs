https://natalie.mu/comic/news/166813

# 「エンジェル・ハート」リョウモデルのコルトパイソンなどau SHINJUKUに展示  
「Angel Heart」獠Model Colt Python等在au SHINJUKU展示

2015年11月24日  [Comment](https://natalie.mu/comic/news/166813/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート」の展示が、東京・新宿のau SHINJUKUにて本日11月24日から29日にかけて行われている。  
北条司的“天使之心”展示于11月24日至29日在东京新宿的au SHINJUKU举行。  

[
![au SHINJUKUで開催されている「エンジェル・ハート」イベントのチラシ。](https://ogre.natalie.mu/media/news/comic/2015/1124/ah-au001.jpg?imwidth=468&imdensity=1)
au SHINJUKUで開催されている「エンジェル・ハート」イベントのチラシ。  
大きなサイズで見る  
au SHINJUKU举办的“天使之心”活动的宣传单。  
查看大图  
](https://natalie.mu/comic/gallery/news/166813/438824)

LINK

[Angel Heart 2ndSeason x au Campaign](http://www.kisekae-touch.com/angelheart/)  
[11/24～29 City Hunterが来店 | お知らせ | au SHINJUKU](http://au-shinjuku.kddi.com/2015/11/112429.html)  
[Angel Heart  | 日本TV](http://www.ntv.co.jp/angelheart/)  