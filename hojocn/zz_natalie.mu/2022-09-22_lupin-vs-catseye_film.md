https://natalie.mu/eiga/news/494638

# ルパン三世 VS キャッツ・アイ、アニメ映画が2023年に世界配信

2022年9月22日   [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


「ルパン三世」のアニメ化50周年、「キャッツ・アイ」原作開始40周年を記念したコラボアニメーション「ルパン三世 VS キャッツ・アイ」の製作が明らかに。日本のアニメ映画として初めて2023年にPrime Videoで世界独占配信される。  
为了纪念“鲁邦三世”动画化50周年、“猫眼”原作开始40周年，合作动画“鲁邦三世VS猫眼”的制作已经明确。这是日本动画电影首次在2023年在Prime Video上全球独家发布。  

[
![「ルパン三世 VS キャッツ・アイ」ティザービジュアル](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_12.jpg?imwidth=468&imdensity=1)
「ルパン三世 VS キャッツ・アイ」ティザービジュアル  
大きなサイズで見る（全13件）  
“鲁邦三世VS猫眼”宣传海报  
以大尺寸观看(共13件)  
](https://natalie.mu/eiga/gallery/news/494638/1905274)

[
![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_01.jpg?imwidth=468&imdensity=1)
「ルパン三世 VS キャッツ・アイ」［拡大］
](https://natalie.mu/eiga/gallery/news/494638/1905263)

本作はキャッツアイの父が遺した「三枚の絵」を軸に、絵を狙う泥棒ルパン三世と怪盗三姉妹キャッツアイの夢の対決が繰り広げられるクライムアクション。1980年代を舞台にキャラクターたちが入り乱れ、物語はやがて絵に隠された驚くべき秘密へとつながっていく。「ルパン三世」にとっては「名探偵コナン」に次ぐコラボ。トムス・エンタテインメントがAmazon Original作品として制作し、アニメ映画「GODZILLA」3部作で知られる[静野孔文](https://natalie.mu/eiga/artist/62364)と[瀬下寛之](https://natalie.mu/eiga/artist/92064)が監督を務める。  
本作品以猫眼父亲留下的“三幅画”为轴心，展开了以画为目标的小偷鲁邦三世和怪盗三姐妹猫眼的梦幻对决。以20世纪80年代为舞台，角色们错综复杂，故事最终与画中隐藏的令人惊讶的秘密联系在一起。对于“鲁邦三世”来说，这是继“名侦探柯南”之后的合作。由Toms娱乐制作为Amazon Original作品，以动画电影“GODZILLA”三部曲而闻名的静野孔文和濑下宽之担任导演。

[
![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_10.jpg?imwidth=468&imdensity=1)
「ルパン三世 VS キャッツ・アイ」［拡大］
](https://natalie.mu/eiga/gallery/news/494638/1905272)

「キャッツ・アイ」原作者の[北条司](https://natalie.mu/eiga/artist/2405)は亡き[モンキー・パンチ](https://natalie.mu/eiga/artist/1749)へのリスペクトを込めて「ルパンの世界観にキャッツたちが存在している様に見せたい、ルパンのキャラクターに寄せてほしい」とオーダー。「Levius -レビウス-」で知られるマンガ家の[中田春彌](https://natalie.mu/eiga/artist/11043)が描いたイラストをもとにキャラクターデザインも一新された。脚本は「映画 それいけ！アンパンマン ドロリンとバケ～るカーニバル」の葛原秀治。音楽には「ルパン三世」シリーズの[大野雄二](https://natalie.mu/eiga/artist/105386)と、「キャッツ・アイ」を手がけた大谷和夫が名を連ねている。  
“猫眼”原作者北条司对已故的Monkey Punch充满了尊重，“我想让他们看起来像鲁邦的世界观中存在着猫眼的样子，希望能寄给鲁邦的角色”。以“Levius-Levius-Lavius-”而闻名的漫画家中田春弥描绘的插图为基础，角色设计也焕然一新。剧本是曾执导《电影就去吧!面包超人:泥林与怪物嘉年华》的葛原秀治。音乐方面，《鲁邦三世》系列的编剧大野雄二和《猫眼电影》的编剧大谷和夫都名列其中。  

[
![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_05.jpg?imwidth=468&imdensity=1)
「ルパン三世 VS キャッツ・アイ」［拡大］
](https://natalie.mu/eiga/gallery/news/494638/1905267)

ルパンの声優である[栗田貫一](https://natalie.mu/eiga/artist/6789)は「記念すべき年に、まさかの対決？ができるとは思っていませんでした。ルパン、次元、五ェ門と銭形のとっつぁん。そして、不二子ちゃんと3姉妹！ 今からとても楽しみです！」とコメント。来生三姉妹の次女・瞳役の[戸田恵子](https://natalie.mu/eiga/artist/16682)は「お話聞いて、えーー？ マジーー？って感じでひっくり返りました！（笑） 何十年ぶりとかは最早考えたくない！（笑） お祭り気分で楽しもうと思います」と語っている。  
鲁邦的声优栗田贯一评论道:“在值得纪念的一年里，没想到会有这样的对决。鲁邦、次元、五卫门和钱形的超级粉丝，还有不二子和三姐妹!从现在开始就非常期待!”。在来生三姐妹中饰演二女儿瞳的户田惠子说:“听了您的故事后，我有种‘啊？神奇啊？’的感觉(笑)我已经不想再考虑时隔几十年了!(笑)我想用节日的气氛来享受这部作品。”  

「ルパン三世 VS キャッツ・アイ」は第1弾PVがYouTubeで公開中。北条のほか、モンキー・パンチの息子である加藤州平、プロデューサーの石山桂一によるコメントも下記に掲載した。  
《鲁邦三世VS猫眼》的第1弹PV在YouTube公开中。除了北条之外，下面还刊登了Monkey Punch的儿子加藤州平、制作人石山桂一的评论。  

译注：以下内容的翻译参见[「Lupin三世」×「Cat'sEye」合作动画决定!Toms负责制作](./2022-09-22_lupin-vs-catseye.md#comment) 。  

## 北条司 コメント

「ルパン三世」は、中学生の時から読んでいた大先輩の作品です。モンキー・パンチ先生がこのコラボの話を聞かれたらどんな反応をされていたかな……？と考えずにはいられません。生前、何度かバーでご一緒する機会がありましたが、お互いマンガのお話をすることはありませんでした。ある時、人づてに頼みたいことがあると言われ、ただそれをおうかがいする前に亡くなられてしまいました。それがさすがにこのコラボの話ではなかったとは思いますが、今回、不思議なご縁でご一緒することができて嬉しいです。モンキー・パンチ先生にも喜んでいただけていたら良いなと心より願っています。

## 加藤州平（エム・ピー・ワークス代表） コメント

モンキー・パンチは、「キャッツ・アイ」の世界観が大好きでした。  
「キャッツ・アイ」と「ルパン三世」が対決したらきっと面白い作品になるだろうと、楽しそうに話をしていたことがあります。  
親しく交流させていただいた北条先生の「キャッツ・アイ」と、「ルパン三世」のコラボレーションは、熱望していたモンキー・パンチにとって、夢のような企画だと思います。  
原作者でも、アニメの「ルパン三世」は、ファンの一人として楽しむ人でした。  
きっと、今回の企画も、ファンの一人として、作品完成を待ちわびていると思います。

## 石山桂一 コメント

「ルパン三世 VS 名探偵コナン」に継ぐ夢のコラボ作品「ルパン三世 VS キャッツ・アイ」。泥棒VS怪盗ということもあり華麗でスタイリッシュなアクション要素も満載なテンポのいい作品に仕上がりました。北条先生からモンキー先生へのリスペクトもあり「ルパンの世界観にキャッツたちが存在している様に見せたい、ルパンのキャラクターに寄せてほしい」というオーダーがあったため、漫画家の中田春彌先生にイラストを描いていただき、両方のキャラクターを一新いたしました。また、ルパンのジャケットがピンクなのは、「キャッツ・アイ」の時代設定が80年代であり、本作もその時代設定をフィーチャーしているため「80年代のルパンと言えばピンク」という思いがあり決定しました。

この記事の画像・動画（全13件）

[![「ルパン三世 VS キャッツ・アイ」ティザービジュアル](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905274 "「ルパン三世 VS キャッツ・アイ」ティザービジュアル")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905263 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905272 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905267 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905268 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905264 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905265 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905266 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905269 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905270 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905271 "「ルパン三世 VS キャッツ・アイ」")
[![「ルパン三世 VS キャッツ・アイ」ロゴ](https://ogre.natalie.mu/media/news/eiga/2022/0921/lupin-vs-catseye_202209_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/494638/1905273 "「ルパン三世 VS キャッツ・アイ」ロゴ")
[![「ルパン三世 VS キャッツ・アイ」第1弾PV](https://i.ytimg.com/vi/6s5QLmAc7gM/default.jpg)](https://natalie.mu/eiga/gallery/news/494638/media/81880 "「ルパン三世 VS キャッツ・アイ」第1弾PV")

## 読者の反応

[Comment](https://natalie.mu/eiga/news/494638/comment)
