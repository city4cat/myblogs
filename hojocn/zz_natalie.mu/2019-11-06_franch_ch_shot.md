https://natalie.mu/comic/news/354299

# 仏版「C・H」スナイパーライフル構えるリョウの姿など切り取った場面写真

2019年11月6日 [Comment](https://natalie.mu/comic/news/354299/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるフランスの実写映画「[シティーハンター THE MOVIE 史上最香のミッション](https://natalie.mu/eiga/film/182791)」の場面写真が公開された。  
根据北条司原作改编的法国真人电影《城市猎人THE MOVIE史上最香的任务》公开了剧照。

[
![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter01.jpg?impolicy=hq&imwidth=730&imdensity=1)
「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。  
大きなサイズで見る（全9件）  
《城市猎人THE MOVIE史上最香的任务》剧照。  
查看大图(共9件)  
](https://natalie.mu/comic/gallery/news/354299/1271586)

[
![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter07.jpg?imwidth=468&imdensity=1)
「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。［拡大］  
《城市猎人THE MOVIE史上最香的任务》剧照。[放大]    
](https://natalie.mu/comic/gallery/news/354299/1271587)

[
![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter06.jpg?imwidth=468&imdensity=1)
「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。［拡大］  
《城市猎人THE MOVIE史上最香的任务》剧照。[放大]  
](https://natalie.mu/comic/gallery/news/354299/1271588)

場面写真には[フィリップ・ラショー](https://natalie.mu/comic/artist/99116)扮するリョウがスナイパーライフルを構える姿や、リョウと[エロディー・フォンタン](https://natalie.mu/comic/artist/94115)扮する香のツーショット、銃を手にした[カメル・ゴンフー](https://natalie.mu/comic/artist/113818)扮する海坊主などおなじみのメンバーが登場。特注で用意されたというミニクーパーや、ポケットのないリョウのジャケットなど小道具にも注目を。  
剧照中，菲利普·拉肖扮演的獠手持狙击步枪的样子、獠和艾洛迪·方坦扮演的香的合照、手持枪的Kamel Guenfoud扮演的海坊主等熟悉的成员悉数登场。特别定制的MiniCooper、没有口袋的夹克等小道具也备受瞩目。  

「シティーハンター THE MOVIE 史上最香のミッション」は、11月29日に東京・TOHOシネマズ 日比谷ほかにて全国公開。吹替版ではリョウ役を山寺宏一、香役を沢城みゆきが務め、アニメでリョウ役を務めた神谷明と香役を務めた伊倉一恵は特別ゲストとして参加する。  
《城市猎人 THE MOVIE 史上最香的任务》将于11月29日在东京·TOHO电影院日比谷等地全国上映。配音版的獠由山寺宏一担任，香由泽城美雪担任，动画中配音獠的神谷明和香的伊仓一惠作为特别嘉宾参加。  

なおコミックナタリーでは「劇場版シティーハンター <新宿プライベート・アイズ>」のBlu-ray / DVD発売を記念した特集を展開中。神谷が静岡にあるディスク工場を訪れた際のレポートやインタビューを掲載している。  
另外，Comic Natalie还推出了纪念《剧场版城市猎人<新宿Private Eyes>》蓝光/ DVD发售的特集。刊登了神谷访问静冈光盘工厂时的报告和采访。


この記事の画像・動画（全9件）  
这篇报道的图片、视频(共9篇)  

[![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354299/1271586 "「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。")
[![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354299/1271587 "「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。")
[![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354299/1271588 "「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。")
[![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354299/1271589 "「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。")
[![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354299/1271590 "「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。")
[![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354299/1271591 "「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。")
[![「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。](https://ogre.natalie.mu/media/news/comic/2019/1106/cityhunter04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354299/1271592 "「シティーハンター THE MOVIE 史上最香のミッション」の場面写真。")
[![「シティーハンター THE MOVIE 史上最香のミッション」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0919/NICKYLARSON_201909_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354299/1241731 "「シティーハンター THE MOVIE 史上最香のミッション」ポスタービジュアル")
[![「シティーハンター THE MOVIE 史上最香のミッション」日本語吹替版予告](https://i.ytimg.com/vi/G-hOPyicPzU/default.jpg)](https://natalie.mu/comic/gallery/news/354299/media/42447 "「シティーハンター THE MOVIE 史上最香のミッション」日本語吹替版予告")

※リョウのリョウは、けものへんに「僚」のつくりが正式表記
※獠的“獠”字，在动物的旁边正式标明“僚”的构成。

(c)AXEL FILMS PRODUCTION - BAF PROD - M6 FILMS



LINK

[「CityHunter THE MOVIE 史上最香のMission」公式Site](http://cityhunter-themovie.com/)

