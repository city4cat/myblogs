https://natalie.mu/comic/news/317381

# もっこりオムライスなど「シティーハンター」限定メニューがカフェゼノンに登場
Mokkori蛋包饭等“城市猎人”限定菜单在Café Zenon登场

2019年1月25日  [Comment](https://natalie.mu/comic/news/317381/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」が2月8日に公開となることを記念して、東京・吉祥寺のCAFE ZENONには1月28日からコラボメニューが用意される。   
根据北条司原作改编的动画电影《剧场版城市猎人<新宿Private Eyes>》将于2月8日上映，东京吉祥寺的CAFE ZENON将从1月28日开始推出合作菜单。  

[
![「100tハンマープレート」](https://ogre.natalie.mu/media/news/comic/2019/0125/cafezenon-100t.jpg?impolicy=hq&imwidth=730&imdensity=1)
「100t hammer plate」  
大きなサイズで見る（全9件）  
“100t锤子牌”  
大尺寸查看(共9件)  
](https://natalie.mu/comic/gallery/news/317381/1095701)

[
![「恥ずかしがり屋な海坊主のカフェラテ」](https://ogre.natalie.mu/media/news/comic/2019/0125/cafezenon-umibouzu.jpg?imwidth=468&imdensity=1)
「恥ずかしがり屋な海坊主のカフェラテ」［拡大］  
“害羞的海坊主拿铁咖啡”[扩大]   
](https://natalie.mu/comic/gallery/news/317381/1095703)

[
![「もっこりオムライス」の2019年バージョン。](https://ogre.natalie.mu/media/news/comic/2019/0125/cafezenon-mokkori.jpg?imwidth=468&imdensity=1)
「もっこりオムライス」の2019年バージョン。［拡大］  
“Mokkori蛋包饭”的2019年版本。[扩大]  
](https://natalie.mu/comic/gallery/news/317381/1095702)

コラボメニューには、過去に人気を博した「もっこりオムライス」の2019年バージョンをはじめ、100tハンマーをショコラ風味のミルクレープで再現した「100tハンマープレート」やドリンク「恥ずかしがり屋な海坊主のカフェラテ」「香のお仕置き辛口ジンジャーエール」を用意。各メニューには缶バッジが付属する。また店内では劇場版アニメの場面写真や、原作マンガの複製原画も展示。  
合作菜单中，包括以过去广受欢迎的“Mokkori蛋包饭”的2019年版本，还有用巧克力风味的mille crepe重现100t锤子的“100t锤子板”，并准备有饮料“害羞海坊主拿铁咖啡”、“香的惩罚辣口姜汁汽水”。每个菜单都附赠罐头徽章。店内还展示了剧场版动画的场面照片和原作漫画的复制原画。

4つのコラボメニューを制覇し、カフェゼノンの姉妹店・ZENON SAKABAを利用した人には先着100名で「限定缶バッジ2種」と劇場版アニメのポスターがプレゼントされる。イベント期間は2月28日まで。  
征服4种合作菜单，并在Cafe Zenon的姐妹店ZENON SAKABA消费的前100名顾客将获得“2种限定罐头徽章”和剧场版动画海报。活动截止日期为2月28日。  

なおコミックナタリーでは「シティーハンター」の海坊主を主役にしたスピンオフ作品「CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常」作者・えすとえむにインタビューをしたコミックタタン特集を公開中。佐原ミズ、あみだむく、モリコロス、三堂マツリ、三月えみ、仁茂田あい、田村茜、山崎智史が海坊主を描くトリビュートイラストギャラリーも展開している。  
另外，Comic Natalie将推出以《城市猎人》的海坊主为主角的衍生作品《CITY HUNTER外传 伊集院隼人氏不平稳的日常》对作者·えすとえむに进行采访的漫画特集公开中。佐原水、amedamk、morkross、三堂祭、三月惠美、仁茂田爱、田村茜、山崎智史等人也在展出海坊主的作品。



この記事の画像（全9件）  
这篇报道的图片(共9篇)  

[![「100tハンマープレート」](https://ogre.natalie.mu/media/news/comic/2019/0125/cafezenon-100t.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1095701 "「100tハンマープレート」")
[![「恥ずかしがり屋な海坊主のカフェラテ」](https://ogre.natalie.mu/media/news/comic/2019/0125/cafezenon-umibouzu.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1095703 "「恥ずかしがり屋な海坊主のカフェラテ」")
[![「もっこりオムライス」の2019年バージョン。](https://ogre.natalie.mu/media/news/comic/2019/0125/cafezenon-mokkori.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1095702 "「もっこりオムライス」の2019年バージョン。")
[![「香のお仕置き辛口ジンジャーエール」](https://ogre.natalie.mu/media/news/comic/2019/0125/cafezenon-oshioki.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1095704 "「香のお仕置き辛口ジンジャーエール」")
[![カフェゼノンの「劇場版シティーハンター <新宿プライベート・アイズ>」公開記念限定メニュー。](https://ogre.natalie.mu/media/news/comic/2019/0125/cafezenon-menu.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1095705 "カフェゼノンの「劇場版シティーハンター <新宿プライベート・アイズ>」公開記念限定メニュー。")
[![プレゼントの缶バッジ。](https://ogre.natalie.mu/media/news/comic/2019/0125/badge.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1095706 "プレゼントの缶バッジ。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」ポスター](https://ogre.natalie.mu/media/news/comic/2019/0125/cityhunter-poster.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1095707 "「劇場版シティーハンター <新宿プライベート・アイズ>」ポスター")
[![ZENON SAKABAでは「シティーハンター」のワイングラスでドリンクを楽しめる。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_20.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1054747 "ZENON SAKABAでは「シティーハンター」のワイングラスでドリンクを楽しめる。")
[![ZENON SAKABAの店内には、北条司をは始め多数のマンガ家や声優などの直筆サインが展示されている。](https://ogre.natalie.mu/media/news/comic/2019/0125/zenonsakaba-wall.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/317381/1095708 "ZENON SAKABAの店内には、北条司をは始め多数のマンガ家や声優などの直筆サインが展示されている。")

(c)北条司/NSP・「2019 劇場版シティーハンター」製作委員会

LINK

[CAFE ZENON | カフェゼノン](http://www.cafe-zenon.jp/)  
[吉祥寺 ZENON SAKABA | ゼノンサカバ](https://zenon-sakaba.jp/)  
[アニメ「劇場版シティーハンター <新宿プライベート・アイズ>」公式サイト | 2019年2月8日全国Roadshow](https://cityhunter-movie.com/)  

