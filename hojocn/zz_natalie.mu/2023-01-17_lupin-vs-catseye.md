source:  
https://natalie.mu/comic/pp/lupin-vs-catseye

# 「Lupin三世VSCat's Eye」日本一有名な泥棒と怪盗三姉妹が強力コラボ！鑑賞して残る、“いつも通りだけど、ちょっと特別”な余韻  
「Lupin三世VSCat‘s Eye」日本最有名的小偷和怪盗三姐妹强力合作！欣赏后留下“和往常一样，但有点特别”的余韵  

Prime Video「Lupin三世 VS Cat's Eye」  

2023年1月17日

# ![日本を代表する泥棒一味と美人怪盗三姉妹が強力コラボ！「Lupin三世VSCat's Eye」｜“必然だけど不思議”、“いつも通りだけどちょっと特別” レトロ＆スタイリッシュな爽快アクションをレビュー！（代表日本的小偷一伙和美女怪盗三姐妹强力合作!“lupin三世 vs cat's eye”|“必然不可思议”,“往常有点特别”复古&有风格的爽快动作滑稽剧!）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/pc_header.jpg?impolicy=pp_image)

日本一有名な泥棒・Lupin三世一味と、華麗に夜を駆ける怪盗三姉妹Cat's Eyeの強力タッグによる新作アニメ映画が誕生。モンキー・パンチ「Lupin三世」のアニメ化50周年と北条司「Cat's Eye」の原作40周年を記念したコラボアニメ映画「Lupin三世 VS Cat's Eye」が、1月27日よりPrime Videoで世界独占配信される。同作の舞台は、1980年代の日本。来生姉妹に関係が深い、ある画家の絵画を巡り、LupinとCat'sEyeが激しく、美しく火花を散らす。  
日本最有名的小偷·Lupin三世一伙，和在夜晚华丽奔跑的怪盗三姐妹Cat's Eye强力搭档的新动画电影诞生了。为纪念Monkey Punch《Lupin三世》动画化50周年和北条司《Cat's Eye》原作40周年而推出的合作动画电影《Lupin三世VS Cat's Eye》将于1月27日推出PrimeVideo将在全球独家播放。该作品的舞台是1980年代的日本。围绕着与来生姐妹关系密切的画家的画，Lupin和Cat'sEye擦出了激烈而美丽的火花。   

コミックナタリーでは配信を記念し、「Lupin三世VSCat's Eye」の見どころを解説する特集を展開。「Lupin三世」「Cat's Eye」両作品に精通するライター・小林聖が、「Lupin三世」のエキセントリックな世界観と「Cat's Eye」のエモーショナルなストーリーラインが見事に融合した同作の魅力を、たっぷりと綴っている。「Lupin三世 VS Cat's Eye」が感じさせる、“ちょっと特別”な味わいとは？  
ComicNatally为了纪念发布，展开了“Lupin三世VSCat's Eye”的看点解说特集。精通《Lupin三世》、《猫之眼》两部作品的作家小林圣，将《Lupin三世》奇特的世界观与《Cat'sEye》的情感故事线完美融合，充分展现了该作的魅力。“Lupin三世VS Cat's Eye”给人的“有点特别”的感觉是什么?  


## ![Introduction](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/ttl_intro.png?impolicy=pp_image)

物語の舞台は1981年の東京。昼は喫茶店を営み、夜は怪盗Cat'sEyeとして世間を騒がす美人三姉妹、瞳・泪・愛は、ある晩美術展から1枚の絵画を盗み出す。時を同じくして、神出鬼没の大泥棒・Lupin三世も東京に出没。彼もまた、とある武装組織を出し抜き、絵画を盗むことに成功した。  
故事的舞台是1981年的东京。白天经营咖啡店，晚上作为怪盗Cat'sEye而轰动社会的美女三姐妹——瞳·泪·爱，有一天晚上从美术展偷出了一幅画。与此同时，神出鬼没的大盗Lupin三世也在东京出没。他也成功地摆脱了某个武装组织，成功地偷走了画作。  

両者が盗んだ絵はどちらも、画家Michael Heinzの描いた作品、「花束と少女」3連作の1枚。Cats三姉妹にとっては、父であるHeinzの消息を掴むための重要な手がかり。  
两者偷的都是画家迈克尔•海因茨的作品《花束与少女》3连作中的一幅。对猫三姐妹来说，这是掌握父亲海因茨消息的重要线索。   

伝説的な泥棒・Lupin三世の“獲物”が自分たちと同じであると知った彼女たちは、その眼差しに美しい闘志を宿し……。  
得知传说中的小偷Lupin三世的“猎物”与自己相同的她们，眼神中充满了美丽的斗志……。  

「Lupin三世」らしいエキセントリックな世界観に、「Cat's Eye」らしい心を揺さぶるエモーショナルな物語が融合し、レトロ＆スタイリッシュな、爽快クライム・アクションが繰り広げられる。  
《Lupin三世》特有的奇特世界观，融合了《Cat's Eye》特有的震撼人心的情感故事，复古&时尚、爽快的犯罪动作。

## ![Character紹介](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/ttl_chara.png?impolicy=pp_image)

[![Lupin三世（CV：栗田貫一）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/1_lupins.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/1_lupin.jpg?impolicy=pp_image)

Lupin三世（CV：栗田貫一）

天才的泥棒の家系に生を受け、世界中をまたにかける大泥棒。  
出生在天才小偷的家族，是纵横世界的大盗。  
どんなターゲットも奇想天外なアイデアと大胆不敵なテクニックで手に入れる。  
用异想天开的想法和大胆无畏的技巧来获得任何目标。

[![次元大介（CV：大塚明夫）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/2_jigens.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/2_jigen.jpg?impolicy=pp_image)

次元大介（CV：大塚明夫）

早撃ち0.3秒の天才ガンマン。その卓越したガンさばきは、Lupin三世も一目置く。窮地に陥っても常に冷静に状況を把握して、難局を打破する男。酒と煙草をこよなく愛する。  
快枪手0.3秒的天才枪手。其卓越的枪战能力，连Lupin三世也对其刮目相看。即使陷入困境也能冷静地把握状况，打破僵局的男人。嗜酒嗜烟。  

[![石川五ェ門（CV：浪川大輔）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/3_goemons.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/3_goemon.jpg?impolicy=pp_image)

石川五ェ門（CV：浪川大輔）

約400年前に大怪盗を輩出した石川家の末裔。斬鉄剣を自在に操る居合の達人であり、剣の道を究めるためにひたすら修行の道を歩む誇り高き剣豪。  
是约400年前大怪盗辈出的石川家的后裔。是自由操纵斩铁剑的居合达人，也是为了钻研剑道一味走修行之路的骄傲的剑豪。  

[![峰不二子（CV：沢城みゆき）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/4_fujikos.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/4_fujiko.jpg?impolicy=pp_image)

峰不二子（CV：沢城みゆき）

さまざまな顔を持つ謎の美女。類まれなる美貌と抜群のプロポーションを持ちながら、Lupinさえも手玉に取る天才的頭脳を併せ持つ。行動は単純で、己の欲望に従っているだけ。  
拥有各种各样面孔的谜之美女。拥有罕见的美貌和出众的身材，同时还拥有连Lupin都能玩弄于股掌之间的天才头脑。行动单纯，只是遵从自己的欲望。  

[![来生瞳（CV：戸田恵子）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/5_hitomis.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/5_hitomi.jpg?impolicy=pp_image)

来生瞳（CV：戸田恵子）

Cat'sEye三姉妹の次女。  
爽やかにロングヘアーをなびかせる美人。抜群の身体能力を活かし、華麗に盗みを遂行する。刑事の俊夫とは学生のときから恋人関係にある。
Cat'sEye三姐妹中的次女。  
清爽飘逸的长发美女。灵活出众的身体能力，华丽地完成偷盗。和刑警俊夫从学生时代开始就是恋人关系。  

[![来生泪（CV：深見梨加）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/6_ruis.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/6_rui.jpg?impolicy=pp_image)

来生泪（CV：深見梨加）

Cat'sEye三姉妹の長女。  
大人の色気を漂わせる、グラマラスな美女。その明晰な頭脳を活かし、作戦の司令塔を担う。優れた変装技術も持っている。
Cat'sEye三姐妹中的长女。  
散发着成熟魅力的迷人美女。活用那个明晰的头脑，担当作战的指挥官。还拥有高超的乔装技术。  

[![来生愛（CV：坂本千夏）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/7_ais.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/7_ai.jpg?impolicy=pp_image)

来生愛（CV：坂本千夏）

Cat'sEye三姉妹の三女。  
元気いっぱい、ボーイッシュな女の子。メカやITに強く、自作ギミックで盗みを成功に導く。三姉妹の中で唯一、父親との思い出がない。
Cat'sEye三姐妹中的三女。  
充满活力，像男孩子一样的女孩子。擅长机械和IT技术，用自创的Gimmick将盗窃引向成功。她是三姐妹中唯一没有父亲记忆的人。  

## ![「Lupin三世」であり「Cat's Eye」でもある──。日本を代表する大怪盗2組のコラボが生んだ、“いつも通りだけどちょっと特別な”余韻（既有“Lupin三世”也有“Cat's Eye”——。代表日本的大怪盗2组的合作,产生“和往常一样，但有点特别的”余韵）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/ttl_column.png?impolicy=pp_image)

文 / 小林聖

## 国民的泥棒×怪盗の必然で不思議な出会い
国民小偷×怪盗的必然而不可思议的相遇  

Lupin三世とCat'sEyeが競演する。必然のようで、どこか不思議な感触がするコラボレーションだ。  
Lupin三世和Cat'sEye同台竞技。这是一种看似必然，却又让人觉得不可思议的合作。  

「Lupin三世」は特別な作品だ。40年以上絶え間なく作品が作られており、シリーズ・作品ごとに少しずつLupin像に変化があり、捉える側面が異なっているが、それでいてどれもLupinだと思わせることができる。アニメ評論家・藤津亮太の言葉を借りるなら「Lupinはジェームズ・ボンドなどと同類の、記号がゆえに永遠に変わることのないCharacter」（「増補改訂版『アニメ評論家』宣言」収録「パラシュートの白い寂しさ『Lupin三世 カリオストロの城』」より）というわけだ。  
《Lupin三世》是一部特别的作品。40多年来不断创作作品，每个系列、作品的Lupin像都有少许变化，捕捉的侧面也有所不同，但无论哪一个都能让人觉得是Lupin。借用动画评论家藤津亮太的话来说就是“Lupin是和詹姆斯·邦德等人一样的，因为是符号所以永远不会变的Character”(「增补修订版《动画评论家》宣言」收录「降落伞的白色寂寞」出自《Lupin三世Cagliostro之城》)。
   

[![「Lupin三世VSCat's Eye」より。（出自《Lupin三世VSCat's Eye》）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/01c.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/01c.jpg?impolicy=pp_image)

その強固であり同時に柔軟なCharacterと世界観があるからこそ、「Lupin三世VS名探偵コナン」や本作のような他作品とのクロスオーバーも容易になる。  
正因为有那个坚固同时柔软的Character和世界观，像“Lupin三世 VS 名侦探柯南”和本作品一样的其他作品的交叉也变得容易。  

特に今回のコラボは作品同士の符合も強い。ともに日本を代表する大泥棒の物語というのはもちろんだが、それだけではない。
特别是这次的合作作品之间的契合也很强。当然都是代表日本的大盗的故事，但还不止于此。  

「Cat's Eye」は泪・瞳・愛の来生三姉妹、特に次女の瞳を中心とした物語だ。彼女たちは普段は喫茶店「Cat'sEye」を営む普通の姉妹だが、怪盗「Cat'sEye」の顔も持っている。そして、そのライバルとも言えるのが、瞳の恋人であり、正体を知らないままCatsを追う若き警察官・内海俊夫だ。  
「Cat's Eye」是泪、瞳、爱的来生三姐妹，特别是以二女儿瞳为中心的故事。她们平时是经营“Cat'sEye”咖啡屋的普通姐妹，但也有怪盗“Cat'sEye”的一面。而他的对手、瞳的恋人、在不明真相的情况下追捕猫的年轻警官·内海俊夫。  

[![「Lupin三世VSCat's Eye」より。](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/10a.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/10a.jpg?impolicy=pp_image)

「Cat's Eye」は、この瞳と俊夫の奇妙な関係が大きな軸になっている。瞳は俊夫を利用しているという顔をしながら、本当は本心から愛している。俊夫はCats逮捕に執念を燃やすが、追いかけるうちにCatsに心惹かれるようにもなっていく。宿敵であり、どこかで惹かれているという関係は、Lupinと銭形の追いかけっこの構図を想起させる。  
“Cat's Eye”以瞳和俊夫的奇妙关系为主轴。瞳表面上是在利用俊夫，其实是真心爱他。俊夫对逮捕Cats燃起了执念，但在追逐的过程中也被Cats所吸引。既是宿敌，又在某处被吸引的关系，让人想起Lupin和钱形的追逐。   

必然の交錯と感じさせるのはそんな部分だ。  
让人感到必然交错的就是这些部分。  

## 無頼の物語と家族の物語の出会い  
无赖故事和家族故事的相遇  

一方で、物語の本質や手触りは意外と遠いところにある。  
另一方面，故事的本质和触感却出乎意料地遥远。  

「Lupin三世」は作品にもよるが、基本的には軽妙洒脱なシリーズだ。人情味はあるが、どこかドライで、仲間はいても家族を感じさせることはない。  
「Lupin三世」根据作品的不同，基本上是轻松洒脱的系列。虽然有人情味，但总觉得有些冷漠，即使有同伴，也不会让人有家人的感觉。  

「Cat's Eye」はと言うと、都会的な洗練や洒脱さはあるが、物語の根本は家族や絆といったウェットな部分にある。瞳と俊夫の「仕事とパートナー」の間で揺れ動く関係はもちろん、Cats自体も、父である画家のMichael Heinzの作品を取り戻しながら、父を探すことを目的としている。この家族の因縁というのが、「Cat's Eye」のもう1つの軸になっている。  
「Cat's Eye」虽然有都市的洗练和洒脱，但故事的根本在于家庭和羁绊等温情的部分。不仅是在瞳和俊夫的“工作和伴侣”之间摇摆不定的关系，就连猫本身也以找回父亲——画家迈克尔•海因茨的作品、寻找父亲为目的。这个家庭的原因，是“Cat's Eye”的另一个轴。  

[![「Lupin三世VSCat's Eye」より。(出自《Lupin三世VSCat's Eye》。)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/4_03c.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/4_03c.jpg?impolicy=pp_image)

これは「Cat's Eye」だけに限った話ではない。北条司作品はいつも家族というテーマの存在が大きい。基本的にシリーズごとの読み切り的なテイストが強い「CityHunter」でも、血縁の家族を持たない冴羽獠が、過去や育ての父との因縁に決着をつけ、香というパートナーを迎える覚悟を決めるというのが、物語の背骨になっている。「CityHunter」のパラレルワールド作品として描かれた「AngelHeart」や、性別逆転夫婦とその家に入った青年を描いた「F.COMPO」などは、より鮮明に家族というテーマが描かれている。  
这并不仅限于「Cat's Eye」。北条司的作品总是以家庭为主题。基本上每个连载都带有强烈的短篇故事意味的《城市猎人》中，没有血缘家族的冴羽獠，结束了过去和养父之间的因缘，决心迎接香这个搭档，这就是故事的主干。变成「CityHunter」的平行世界作品的「AngelHeart」和描写进入性别逆转的夫妇家的青年的「F.COMPO」等作品，更鲜明地描绘了家庭这一主题。  

いわば帰るべき場所を巡る物語である「Cat's Eye」や北条作品と、帰るべき場所と無縁な無頼さこそが叙情を生む「Lupin三世」は、根っこの部分では正反対と言える。これが「不思議な感触」の正体だ。  
「Cat's Eye」和北条的作品都是围绕着归处而展开的故事，而《Lupin三世》则以与归处无缘的无赖感为抒情，在根源上可以说是完全相反的。这就是“不可思议的触感”。  

## 若いCatsと老成の域に達したLupin
年轻的Cats和老成的Lupin  

似ているようで、コアの部分ではまったく別の性質を持っている両作品をどう融合させるのか。楽しみ半分、不安半分だったが、「Lupin三世VSCat's Eye」は想像以上に見事に2つの世界をつなげて、その魅力を倍増させた。  
如何将看似相似，但在核心部分具有完全不同性质的两部作品融合在一起?一半是期待，一半是不安，“Lupin三世 VS Cat's Eye”超乎想象地完美地连接了两个世界，使其魅力倍增。  

いつも通りのLupinとして。そして、大人と若者の物語として。  
作为一如既往的Lupin。以及一个关于成年人和年轻人的故事。    

本作では、Heinzの絵画を盗み出す中で、CatsとLupinが邂逅し、絵を巡る謎と冒険が始まる。東京から始まり、フランスへと飛び回る世界観や物語世界は非常にLupinらしい。「Cat's Eye」をよく知らない人でも、いつものLupinの感覚で観ることができる。  
在本作品中，Cats和Lupin在偷出海因茨的画的过程中邂逅，围绕画的谜题和冒险开始了。世界观和故事世界从东京开始，跳转到法国，非常像Lupin的风格。即使是不太了解「Cat's Eye」的人，也能以往常Lupin的感觉来观看。

と同時に、「Cat's Eye」とのクロスオーバーでいつもとは違う魅力も生まれている。  
与此同时，与“Cat's Eye”的跨界合作也产生了与以往不同的魅力。  

本作でCatsが出会うLupin一味はミステリアスで真意が見えない。そして、その姿はCatsよりはるかに老練だ。
在本作品中，Cats遇到的Lupin一伙充满神秘感，让人看不清他们的真实意图。而且，他的样子比Cats老练得多。  

Lupin三世はシリーズのなかでさまざまな顔を見せる。冒険心溢れる若き青年のようなシリーズもあれば、飄々として落ち着いた大人の男の側面が強いシリーズもある。
Lupin三世在系列中展现了各种各样的面孔。在一些系列中，他是一个富有冒险精神的年轻人，而在另一些系列中，他更像是一个冷漠、冷静和成熟的人。  

本作でのLupinはそうした中でも際だって円熟している。スタイリッシュに盗みを完遂するCatsも、このLupinたちの前ではまだまだひよっこ。若いLupinならCatsの美貌に飛び付きそうなものだが、本作ではそんな若さがなりを潜め、子供を見守り、導くような視線になっている。その姿は、老成という域に達しているようにすら見える。  
在本作品中，Lupin尤其圆熟。潇洒地完成偷盗的Cats，在Lupin面前还是雏鸟。如果是年轻的Lupin，肯定会被Cats的美貌所吸引，但在本作品中，这样的青春容颜已经褪去，变成了守护孩子、引导孩子的视线。他的样子看起来甚至达到了老成的境界。 

[![「Lupin三世VSCat's Eye」より。(出自《Lupin三世VSCat's Eye》。)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/05b.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/05b.jpg?impolicy=pp_image)

LupinとCatsと同じように、銭形と俊夫のコンビも見どころだ。銭形らしい不器用ながむしゃらさはそのままに、若い俊夫にはない経験の厚さを見せつける。  
与Lupin和Cats一样，钱形和俊夫的组合也是一大看点。钱形保持着那种笨拙的鲁莽，展现出了年轻俊夫所没有的丰富经验。  

Catsは「スタイリッシュな美人怪盗」のイメージも強いが、前述のとおり、物語の根本には家族や恋人との関係や過去の因縁といったモチーフがある。全体を通して見ると、揺れ動く青年期の青春の物語だ。  
Cats有着强烈的“时尚美人怪盗”的印象，但如前所述，故事的根本是与家人、恋人的关系以及过去的因缘。从整体上看，这是一个摇摆不定的青年时期的青春故事。  

本作はそんな若者たちを、大人のLupinたちが迎え、導いていく形になっている。内海俊夫が銭形に出会って「本物だ……！」と感激する場面があるが、見ている側も俊夫と同じように憧れのヒーローの背中を垣間見るような気持ちになる。  
本作品是成熟的Lupin等人迎接、引导这样的年轻人的形式。内海俊夫遇到钱形“是真的……!”的场面，看的人也和俊夫一样，仿佛看到了自己憧憬的英雄的背影。

## 「Lupin」であり「Cats」である、特別な出会い  
既是“Lupin”又是“Cats”的特别邂逅  

そして、だからこそ主役はLupinであるだけでなく、Catsたちでもある。物語を動かしていくのは、すでに完成されたLupinたちではなく、若いCatsの面々になる。  
因此，主角不仅是Lupin，也是Cats等人。推动故事发展的不是已经完成的Lupin们，而是年轻的Cats。  

この構図の中で「Cat's Eye」と北条司のエッセンスも光っている。Heinzの絵を巡る物語になっていたり、「Cats」ファンには（あるいは「CityHunter」ファンにも）おなじみの喫茶店「Cat'sEye」が出てきたりといった設定部分もだが、親子や過去の因縁といったテーマも顔を見せる。  
“Cat's Eye”和北条司的精髓也在这部作品中闪现出来。故事围绕着海因茨的画展开，也出现了Cats粉丝(或者「CityHunter」粉丝)很熟悉的「Cat'sEye」咖啡屋，但亲子关系和过去关系的主题也凸显出来。  

鍵となる3枚のHeinzの絵画や、それを巡る人々には、家族や過去の秘密が織り込まれている。Lupin自身には馴染まない家族というテーマが、Catsという主人公によって活かされているのだ。  
作为关键的三幅海因茨的画和围绕这三幅画的人们之间，交织着家人和过去的秘密。Lupin自己不熟悉的家庭主题，被主人公Cats活用了。  

観終わってみれば、「Lupin三世VSCat's Eye」は、「Lupin三世」であり、「Cat's Eye」でもあった。「Lupin三世」しか知らない人には、ゲストキャラのいるいつものLupinに見えるし、同時にちょっと違う魅力も見える。「Cat's Eye」ファンには、北条作品のエッセンスや、若者としてのCatsたちを見つけることができる。  
看完之后才发现，「Lupin三世VS Cat's Eye」既是「Lupin三世」，也是「Cat's Eye」。对只知道「Lupin三世」的人来说，是有客串角色的Lupin，同时也能看到不一样的魅力。对于「Cat's Eye」的粉丝来说，可以发现北条作品的精髓，以及作为年轻人的Cats等人。  

必然のようで不思議な出会いは、いつも通りだけどちょっと特別な作品を生んだのだ。  
看似必然却不可思议的邂逅，诞生了一如既往却有点特别的作品。  

[![「Lupin三世VSCat's Eye」より。（出自《Lupin三世VSCat's Eye》。）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/1_main_13b.jpg?impolicy=pp_image)](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/1_main_13b.jpg?impolicy=pp_image)

## ![知っているともっと楽しめる！「Lupin三世VSCat's Eye」ひと口メモ（知道的话会更快乐!鲁平三世VS Cat'seye”小笔记）](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/ttl_memo.png?impolicy=pp_image)

### Michael Heinz

来生三姉妹の父親であるドイツ人画家。あることをきっかけに美術シンジケートから狙われることになり、三女・愛が生まれる前に行方不明となった。Cat'sEyeはこのHeinzの絵を集めながら彼の行方を追っている。  
来生三姐妹的父亲，德国画家。因为某件事而被美术集团盯上，在三女儿·爱出生之前就失踪了。Cat'sEye一边收集Heinz的画一边追踪他的行踪。

### 喫茶「Cat'sEye」（“Cat'sEye”咖啡屋）

来生三姉妹が営む喫茶店。のちに「CityHunter」やその派生作品「エンジェルハート」にも同じ姿・名前の喫茶店が登場し、そちらは海坊主（ファルコン）とミキが経営している。2019年に公開された「劇場版CityHunter〈新宿プライベート・アイズ〉」では、海坊主たちとCatsの関係が新たに描かれた。  
来生三姐妹经营的咖啡店。后来在《CityHunter》及其衍生作品《AngelHeart》中也出现了同样模样和名字的咖啡店，由海坊主(Falcon)和美纪经营。在2019年上映的《剧场版CityHunter <新宿Private Eyes>》中，对海坊主们与Cats的关系进行了全新的描写。

### 永石

Heinz夫婦に仕え、その後はCatsの怪盗仕事もサポートする執事のような役割を務めている。  
侍奉Heinz夫妇，之后担任管家般的角色，也支持Cats的怪盗工作。  

小林聖（コバヤシアキラ）

フリーライター。主な執筆分野はマンガ。その1年で読んだマンガから面白かった作品を自身の独断と偏見で選ぶTwitter上の企画「俺マン」こと「俺マンガ大賞」を毎年開催している。  
自由撰稿人。主要执笔领域是漫画。每年都会举办Twitter上的企划「俺マン」，即“我漫画大奖”，根据自己的独断和偏见，从这一年里读过的漫画中选出有趣的作品。  

*   [小林聖（Frog/蛙）@ネルヤ (@frog88) | Twitter](https://twitter.com/frog88)
*   [#俺マン2021 特設サイト](https://oreman.jp/)

「Lupin三世 VS Cat's Eye」

Prime Videoにて2023年1月27日（金）より配信

[![「Lupin三世 VS Cat's Eye」](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/pp_item.jpg?impolicy=pp_image)](https://www.amazon.co.jp/dp/B0B8THMHBS/ref=dvm_pdd_tie_jp_ha_s_00jan_LVC_-_TULVC32)

[視聴ページへ](https://www.amazon.co.jp/dp/B0B8THMHBS/ref=dvm_pdd_tie_jp_ha_s_00jan_LVC_-_TULVC32)

## Staff

原作：Monkey Punch「Lupin三世」、北条司「Cat's Eye」

監督：静野孔文、瀬下寛之

脚本：葛原秀治

副監督：井手惠介

Character Design：中田春彌、山中純子

Production Design：田中直哉、フェルディナンド・パトゥリ

Art Director：片塰満則

編集：肥田文

音響監督：清水洋史

音楽：大野雄二、大谷和夫、fox capture plan

Animation制作：TMS Entertainment

Animation制作協力：萌

製作：Lupin三世 VS Cat's Eye製作委員会

## Cast

Lupin三世：栗田貫一

次元大介：大塚明夫

石川五ェ門：浪川大輔

峰不二子：沢城みゆき

銭形警部：山寺宏一

来生瞳：戸田恵子

来生泪：深見梨加

来生愛：坂本千夏

内海俊夫：安原義人

永石：麦人

銀河万丈

東地宏樹

菅生隆之

*   [「Lupin三世 VS Cat's Eye」| Prime Video](https://www.amazon.co.jp/dp/B0B8THMHBS/ref=dvm_pdd_tie_jp_ha_s_00jan_LVC_-_TULVC32)
*   [「Lupin三世 VS Cat's Eye」公式サイト](https://www.lupin-vs-catseye.com/)
*   [「Lupin三世 VS Cat's Eye」 (@lupin\_vs\_cats) | Twitter](https://twitter.com/lupin_vs_cats)

©Monkey Punch 北条司／Lupin三世 VS Cat's Eye製作委員会

Prime Video

[![Prime Video](https://ogre.natalie.mu/media/pp/static/comic/lupin-vs-catseye/primevideo.png?impolicy=pp_image)](https://www.amazon.co.jp/amazonprime)

[公式サイトへ](https://www.amazon.co.jp/amazonprime)

利用者が、観たい作品を観たい方法で視聴できるよう多様な選択肢が用意されているオンデマンド・エンターテイメント・サービス。  
按需娱乐服务，提供多种选择，让用户可以以想要观看的方式观看他们想看的作品。  

※作品の視聴には会員の登録が必要です。（AmazonプライムについてはAmazon.co.jpサイトをご覧ください。）  
※收看作品时需要会员的登录。(关于Amazon prime，请访问Amazon.co.jp网站。)  

※Amazon、Prime Video及びこれらに関連するすべての商標は、Amazon.com, Inc.又はその関連会社の商標です。  
※Amazon、Prime Video及与之相关的所有商标，Amazon.com，Inc.。或者是其关联公司的商标。  

[Amazon Prime](https://www.amazon.co.jp/amazonprime)


---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
