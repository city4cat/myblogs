https://natalie.mu/comic/news/338027

# 80年代ジャンプ作品が集結する、“おとなのジャンプ酒場”が1年限定で歌舞伎町に

2019年7月2日  [Comment](https://natalie.mu/comic/news/338027/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


週刊少年ジャンプ（集英社）作品にスポットを当てた「おとなのジャンプ酒場」が、7月11日に東京・WaMall歌舞伎町5階にオープンする。  
聚焦《周刊少年Jump》(集英社)作品的《大人的Jump酒馆》将于7月11日在东京WaMall歌舞伎町5楼开业。


[
![「おとなのジャンプ酒場」の告知画像。(c)ゆでたまご/集英社 (c)宮下あきら/集英社 (c)北条司/NSP 1985 (c)車田正美/集英社 (c)バード・スタジオ/集英社 (c)武論尊・原哲夫/NSP 1983 (c)森田まさのり/集英社](https://ogre.natalie.mu/media/news/comic/2019/0702/jumpsakaba01.jpg?impolicy=hq&imwidth=730&imdensity=1)
「おとなのジャンプ酒場」の告知画像。(c)ゆでたまご/集英社 (c)宮下あきら/集英社 (c)北条司/NSP 1985 (c)車田正美/集英社 (c)バード・スタジオ/集英社 (c)武論尊・原哲夫/NSP 1983 (c)森田まさのり/集英社
大きなサイズで見る（全3件）
](https://natalie.mu/comic/gallery/news/338027/1191611)

[
![「おとなのジャンプ酒場」のメニューイメージ。(c)ゆでたまご/集英社 (c)えんどコイチ/集英社 (c)次原隆二/NSP 1983 (c)徳弘正也/集英社 (c)バード・スタジオ/集英社 (c)森田まさのり/集英社](https://ogre.natalie.mu/media/news/comic/2019/0702/jumpsakaba02.jpg?imwidth=468&imdensity=1)
「おとなのジャンプ酒場」のメニューイメージ。(c)ゆでたまご/集英社 (c)えんどコイチ/集英社 (c)次原隆二/NSP 1983 (c)徳弘正也/集英社 (c)バード・スタジオ/集英社 (c)森田まさのり/集英社［拡大］
](https://natalie.mu/comic/gallery/news/338027/1191612)

「おとなのジャンプ酒場」では[ゆでたまご](https://natalie.mu/comic/artist/1756)「キン肉マン」、[宮下あきら](https://natalie.mu/comic/artist/1877)「魁!!男塾」、[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」、[車田正美](https://natalie.mu/comic/artist/2044)「聖闘士星矢」、[鳥山明](https://natalie.mu/comic/artist/2266)「DRAGON BALL」、[武論尊](https://natalie.mu/comic/artist/2383)原作による[原哲夫](https://natalie.mu/comic/artist/1917)「北斗の拳」、[森田まさのり](https://natalie.mu/comic/artist/2117)「ろくでなしBLUES」といった、1980年代のジャンプ読者に馴染み深いタイトルで店内を装飾。巨大壁画や、表紙ウォール、巨大フィギュア、フィギュアコレクション、マンガ家のお宝グッズ、80年代のジャンプ本誌などが店内に並ぶ。
《大人的Jump酒馆》中有ゆでたまご的“筋肉人”、宫下亮的“魁!!男塾》、北条司的《城市猎人》、车田正美的《圣斗士星矢》、鸟山明的《DRAGON BALL》、根据武论尊原作改编的原哲夫的《北斗神拳》、森田雅典的《无赖BLUES》等，以1980年代Jump读者熟悉的标题装饰店内。店内陈列着巨大的壁画、封面墙、巨大的手办、手办收藏品、漫画家的宝贝商品、80年代的Jump本刊等。

このほか各作品にフォーカスしたコラボメニューも販売。営業期間は1年限定を予定しているので、気になる人は早めに足を運ぼう。  
除此之外，还销售聚焦于各作品的合作菜单。营业时间限定为1年，感兴趣的人请尽早前往。  

この記事の画像（全3件）  
这篇报道的图片(共3篇)  

[![「おとなのジャンプ酒場」の告知画像。(c)ゆでたまご/集英社 (c)宮下あきら/集英社 (c)北条司/NSP 1985 (c)車田正美/集英社 (c)バード・スタジオ/集英社 (c)武論尊・原哲夫/NSP 1983 (c)森田まさのり/集英社](https://ogre.natalie.mu/media/news/comic/2019/0702/jumpsakaba01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/338027/1191611 "「おとなのジャンプ酒場」の告知画像。(c)ゆでたまご/集英社 (c)宮下あきら/集英社 (c)北条司/NSP 1985 (c)車田正美/集英社 (c)バード・スタジオ/集英社 (c)武論尊・原哲夫/NSP 1983 (c)森田まさのり/集英社")
[![「おとなのジャンプ酒場」のメニューイメージ。(c)ゆでたまご/集英社 (c)えんどコイチ/集英社 (c)次原隆二/NSP 1983 (c)徳弘正也/集英社 (c)バード・スタジオ/集英社 (c)森田まさのり/集英社](https://ogre.natalie.mu/media/news/comic/2019/0702/jumpsakaba02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/338027/1191612 "「おとなのジャンプ酒場」のメニューイメージ。(c)ゆでたまご/集英社 (c)えんどコイチ/集英社 (c)次原隆二/NSP 1983 (c)徳弘正也/集英社 (c)バード・スタジオ/集英社 (c)森田まさのり/集英社")
[![「おとなのジャンプ酒場」のロゴ。](https://ogre.natalie.mu/media/news/comic/2019/0702/sakaba.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/338027/1191664 "「おとなのジャンプ酒場」のロゴ。")

## 「おとなのジャンプ酒場」

期間：2019年7月11日（木）～1年間限定営業予定  
時間：平日17:00～23:00、土日祝14:00～23:00   
※ラストオーダーはそれぞれ22:30。（ 最后点餐时间分别为22:30）   
場所 : WaMall歌舞伎町5F  



LINK

[集英社『週刊少年ジャンプ』公式サイト](https://www.shonenjump.com/j/)  