https://natalie.mu/comic/news/167089

# 「シティーハンター」モノさしゼノン付録に、大きな男も安心のもっこりサイズ
在“城市猎人”Monosa Zenon附录中，大男人也能安心的Mokkori尺寸

2015年11月25日 [Comment](https://natalie.mu/comic/news/167089/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」の付録「CITY HUNTER モノさし もっこり Edition」が、本日11月25日に発売された月刊コミックゼノン2016年1月号（徳間書店）に綴じ込まれている。  
北条司的《城市猎人》附录《CITY HUNTER Monosa Mokkori Edition》被编入11月25日发售的月刊Comic Zenon 2016年1月号(德间书店)。

[
![付録の「CITY HUNTER モノさし もっこり Edition」。24cmもあれば大抵の“モノ”は問題なく測れるはずだ。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1125/cityhunter-monosashi.jpg?imwidth=468&imdensity=1)
付録の「CITY HUNTER モノさし もっこり Edition」。24cmもあれば大抵の“モノ”は問題なく測れるはずだ。(c)北条司/NSP 1985  
大きなサイズで見る（全4件）  
附录的“CITY HUNTER Monosa Mokkori Edition”。24cm应该可以测量大多数“东西”。(c)北条司/NSP   
1985大尺寸看(全4件)  
](https://natalie.mu/comic/gallery/news/167089/439285)

「CITY HUNTER モノさし もっこり Edition」は、綴じ込み付録としては限界ギリギリの24cmまで測れる特大サイズ。誌面では「リョウ（リョウの漢字はけものへんに「僚」のつくり）のように“大きな”男でも安心」と解説されており、使い勝手は折り紙つきだ。定規の表面には、新装版「シティーハンター XYZ Edition」の背表紙用に描き下ろされたイラストが使用された。  
“CITY HUNTER Monosa Mokkori Edition”作为装订附录，是可以测量到极限的24cm的特大号。杂志的版面上有这样的解说:“即使是像獠(獠的汉字是反犬旁加‘僚’字)一样的‘大’男人也可以放心”，使用方便还附带折纸。尺的表面使用了新装版“城市猎人 XYZ Edition”的书脊用的插图。

また月刊コミックゼノン2016年1月号では、森拓也による新連載「疫病神とバカ女神」が開始。並外れて運がよく、その力を使い世界から戦争をなくした女子高生が目標のため奮闘する様子を描く。  
月刊Comic Zenon 2016年1月号上，森拓也的新连载《瘟神与笨蛋女神》开始连载。描写了运气特别好，使用了那个力量使战争从世界消失的女高中生为了目标奋斗的样子。  

この記事の画像（全4件）  
这篇报道的图片(共4篇)  

[![付録の「CITY HUNTER モノさし もっこり Edition」。24cmもあれば大抵の“モノ”は問題なく測れるはずだ。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/1125/cityhunter-monosashi.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/167089/439285 "付録の「CITY HUNTER モノさし もっこり Edition」。24cmもあれば大抵の“モノ”は問題なく測れるはずだ。(c)北条司/NSP 1985（附录的“CITY HUNTER Monosa Mokkori Edition”。24cm应该可以测量大多数“东西”。北条司/NSP 1985）")
[![森拓也「疫病神とバカ女神」カット(c)森拓也/NSP 2015](https://ogre.natalie.mu/media/news/comic/2015/1125/yakubyogami.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/167089/439286 "森拓也「疫病神とバカ女神」カット(c)森拓也/NSP 2015")
[![月刊コミックゼノン2016年1月号は、アニメ「北斗の拳 イチゴ味」でリン役を演じる蒼井翔太、バット役を務める山下大輝のインタビュー記事も掲載している。(c)武論尊・原哲夫/NSP 1983, (c)イチゴ味 2015](https://ogre.natalie.mu/media/news/comic/2015/1125/ichigo_bamensya.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/167089/439287 "月刊コミックゼノン2016年1月号は、アニメ「北斗の拳 イチゴ味」でリン役を演じる蒼井翔太、バット役を務める山下大輝のインタビュー記事も掲載している。(c)武論尊・原哲夫/NSP 1983, (c)イチゴ味 2015 （月刊Comic Zenon 2016年1月号是动画《北斗神拳》草莓味”中饰演林的苍井翔太、饰演球棒的山下大辉的采访报道也刊载了。武论尊·原哲夫/NSP 1983， (c)草莓味2015）")
[![月刊コミックゼノン2016年1月号](https://ogre.natalie.mu/media/news/comic/2015/1125/Zenon1601.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/167089/439288 "月刊コミックゼノン2016年1月号（月刊Comic Zenon 2016年1月号）")

LINK

[Comic Zenon](http://www.comic-zenon.jp/)