https://natalie.mu/eiga/news/357381

# 仏版「シティーハンター」リョウと香が息ぴったりのアクション見せる特別映像
法国版《城市猎人》獠和香配合默契的动作特别影像  

2019年11月29日  [Comment](https://natalie.mu/eiga/news/357381/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


本日公開された「[シティーハンター THE MOVIE 史上最香のミッション](https://natalie.mu/eiga/film/182791)」の特別映像がYouTubeで公開された。  
今天公开的“城市猎人THE MOVIE史上最香的任务”的特别映像在YouTube上公开了。  

[
![「シティーハンター THE MOVIE 史上最香のミッション」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0919/NICKYLARSON_201909_01.jpg?imwidth=468&imdensity=1)
「シティーハンター THE MOVIE 史上最香のミッション」ポスタービジュアル
大きなサイズで見る（全12件）  
《城市猎人THE MOVIE史上最香的任务》海报  
大尺寸观看(共12件)  
](https://natalie.mu/eiga/gallery/news/357381/1241731)

[北条司](https://natalie.mu/eiga/artist/2405)のマンガ「シティーハンター」をフランスで実写化した本作。原作ファンである「世界の果てまでヒャッハー！」の[フィリップ・ラショー](https://natalie.mu/eiga/artist/99116)が監督を務め、自らニッキー・ラーソン＝冴羽リョウを演じた。ローラ＝槇村香役は「最高の花婿」の[エロディー・フォンタン](https://natalie.mu/eiga/artist/94115)が担当。日本語吹替版では[山寺宏一](https://natalie.mu/eiga/artist/30965)がリョウ、[沢城みゆき](https://natalie.mu/eiga/artist/65954)が香に声を当てる。公開された映像は、リョウと香が敵に囲まれた状態で銃を乱射するアクションシーン。体を寄せ合いながら撃つ、香の銃にリョウが素早く装填するなど息の合った動きが見られる。  
北条司的漫画《城市猎人》在法国真人电影化的本作品。作为原作粉丝、《到世界尽头!》的菲利普·拉肖担任导演，自己饰演Nicky Larson=冴羽獠。Laura=槙村香由《最佳新郎》的艾洛迪·方当担当。日语配音版中山寺宏一和泽城美雪分别为獠和香配音。公开的影像是獠和香在被敌人包围的状态下用枪乱射的动作场面。两人一边射击一边挤在一起，獠迅速给香的枪上膛，显示出他们默契的动作。

[
![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1013/nickylarson_201910_01.jpg?imwidth=468&imdensity=1)
「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production［拡大］  
《城市猎人:史上最香的任务》AxelFilms Production[扩大]  
](https://natalie.mu/eiga/gallery/news/357381/1257604)

本作には巨額の予算が用意されていたことから、ラショーは「このジャンルの映画に初めて取り組むのは、恐ろしいことだった」と振り返り、アクションシーンへのこだわりについて「『シティーハンター』を実写化するなら、どうしてもレベルは高くなる。なので、リュック・ベッソン監督の作品に参加した技術者をはじめ、このジャンルの精鋭を集めることにした」とコメント。「彼らはとてもいい仕事をしてくれて、爆発シーンやカーチェイス、アクションシーンにすごく現実味が出せた」と自信をうかがわせた。  
由于本片准备了巨额的预算，拉肖回顾道:“第一次尝试这种类型的电影，是一件很可怕的事情。”对于对动作场面的执着，他说:“如果你要做《城市猎人》的真人改编，标准不可避免地很高。因此，我们决定集中该领域的精英，包括曾参与吕克·贝松导演作品的技术人员在内。”他自信地说:“他们做得很好，在爆炸场面、汽车追逐场面、动作场面中表现出了很强的真实感。”  

「シティーハンター THE MOVIE 史上最香のミッション」は、東京・TOHOシネマズ 日比谷ほか全国で公開中。  
《城市猎人THE MOVIE史上最香的任务》在东京·TOHO电影院日比谷等全国公映中。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  
※冴羽獠的獠，在动物旁正式标明“僚”的写法  

この記事の画像・動画（全12件） 
这篇报道的图片、视频(共12篇)  

[![「シティーハンター THE MOVIE 史上最香のミッション」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0919/NICKYLARSON_201909_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1241731 "「シティーハンター THE MOVIE 史上最香のミッション」ポスタービジュアル")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1013/nickylarson_201910_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1257604 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1115/nickylarson_201911_3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1277327 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1115/nickylarson_201911_1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1277325 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1115/nickylarson_201911_2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1277326 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1013/nickylarson_201910_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1257605 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1013/nickylarson_201910_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1257606 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1013/nickylarson_201910_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1257607 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1013/nickylarson_201910_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1257608 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1013/nickylarson_201910_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1257609 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production](https://ogre.natalie.mu/media/news/eiga/2019/1007/NICKYLARSON_2019010.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/357381/1253740 "「シティーハンター THE MOVIE 史上最香のミッション」 (c)Axel Films Production")
[![「シティーハンター THE MOVIE 史上最香のミッション」特別映像2](https://i.ytimg.com/vi/_Cig5LjbaXo/default.jpg)](https://natalie.mu/eiga/gallery/news/357381/media/43683 "「シティーハンター THE MOVIE 史上最香のミッション」特別映像2")

(c)AXEL FILMS PRODUCTION - BAF PROD - M6 FILMS


LINK

[「シティーハンター THE MOVIE 史上最香のミッション」公式サイト](http://cityhunter-themovie.com)  
[「シティーハンター THE MOVIE 史上最香のミッション」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhunter_2019)  
[「シティーハンター THE MOVIE 史上最香のミッション」特別映像2](https://www.youtube.com/watch?v=_Cig5LjbaXo&feature=youtu.be)  