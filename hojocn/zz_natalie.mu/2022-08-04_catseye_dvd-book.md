https://natalie.mu/comic/news/488160

# 「Cat's♥Eye」DVD BOOK全4巻を9月より刊行、貴重な資料を収めたブックレット付属
「Cat's♥Eye」DVD BOOK全4卷9月开始发行，收录珍贵资料的小册子附送  

2022年8月4日 12:00 155 [10](https://natalie.mu/comic/news/488160/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「Cat's♥Eye」全36話を4巻にわけて収録した「Cat's♥Eye COMPLETE DVD BOOK」が、9月22日より月1回ペースで順次刊行される。  
北条司原作的动画「Cat's♥Eye」全36话分为4卷收录的《Cat's♥Eye COMPLETE DVD BOOK》将于9月22日起以每月一次的频率依次发行。  

[
![「Cat's♥Eye COMPLETE DVD BOOKvol.1」の告知ビジュアル。(c)北条司／Coamix・TMS 1983](https://ogre.natalie.mu/media/news/comic/2022/0804/catseye_dvdbook.jpg?imwidth=468&imdensity=1)
「Cat's♥Eye COMPLETE DVD BOOKvol.1」の告知ビジュアル。(c)北条司／Coamix・TMS 1983  
大きなサイズで見る（全2件）  
“Cat's♥Eye COMPLETE DVD BOOKvol.1”的公告图像。(c)北条司/ TMS 1983  
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/488160/1872148)

1981年に週刊少年ジャンプ（集英社）で連載がスタートし、1983年にTVアニメ化された「Cat's♥Eye」。「Cat's♥Eye COMPLETE DVD BOOK」に毎号付属する16ページのブックレットには、各話のストーリー解説とチェックポイントの紹介をはじめ、設定画、イラストギャラリーなどの貴重な資料が収録される。また一部書店では、購入者特典としてポストカードを配布予定。詳細は決定次第、ぴあ関西版Webで発表となる。  
《猫♥爱》于1981年在《周刊少年Jump》(集英社)上开始连载，1983年TV动画化。猫♥爱 COMPLETE DVD BOOK”每期附赠的16页小册子中，收录了各话的故事解说和检查点介绍，以及设定画、插图图库等珍贵资料。另外，部分书店还将发放明信片作为购买者优惠。详细情况一决定，就会在关西版Web上发表。

創刊号は税込1870円。Amazon.co.jpほか、全国の書店、ネット書店で販売される。なおYouTubeのTMSアニメ公式チャンネルでは、「Cat's♥Eye」第1話「君はセクシーな泥棒」を公開中だ。  
创刊号含税1870日元。Amazon.co.jp以及全国书店、网上书店均有销售。另外，YouTube的TMS动画官方频道正在公开《猫♥爱》第一话《你是性感的小偷》。  

この記事の画像・動画（全2件）

[![「Cat's♥Eye COMPLETE DVD BOOKvol.1」の告知Visual。(c)北条司／Coamix・TMS 1983](https://ogre.natalie.mu/media/news/comic/2022/0804/catseye_dvdbook.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/488160/1872148 "「Cat's♥Eye COMPLETE DVD BOOKvol.1」の告知ビジュアル。(c)北条司／Coamix・TMS 1983")
[![【公式】Cat's Eye 第1話「君はセクシーな泥棒」 “CAT'S EYE” EP01(1983)](https://i.ytimg.com/vi/iv3klTuYC3w/default.jpg)](https://natalie.mu/comic/gallery/news/488160/media/80066 "【公式】Cat's Eye 第1話「君はセクシーな泥棒」 “CAT'S EYE” EP01(1983)")

LINK

[ぴあ関西版WEB](http://kansai.pia.co.jp/)

関連商品

[
![「Cat'sEye COMPLETE DVD BOOK VOL.1」](https://m.media-amazon.com/images/I/51VRP1yg2gL._SL160_._SS70_.jpg)  
「Cat'sEye COMPLETE DVD BOOK VOL.1」
[DVD＋Booklet] 2022年9月22日発売 / ぴあ / 978-4835646619
](https://www.amazon.co.jp/exec/obidos/ASIN/4835646614/nataliecomic-22)
