source:  
https://natalie.mu/eiga/news/510863


# 鈴木亮平主演「シティーハンター」相棒・槇村香役は森田望智  
铃木亮平主演「CityHunter」搭档槙村香由森田望智饰演  

2023年1月31日  

鈴木亮平が主演するNetflix映画「シティーハンター」に、槇村香役で森田望智が出演する。  
在铃木亮平主演的Netflix电影《城市猎人》中，森田望智将出演槙村香一角。  

[![https://ogre.natalie.mu/media/news/eiga/2023/0130/cityhunter_202301.jpg?imwidth=468&imdensity=1] 
森田望智演じる槇村香。（森田望智饰演的槙村香。）](https://natalie.mu/eiga/gallery/news/510863/1987260)

[大きなサイズで見る（全2件）（看大尺寸）](https://natalie.mu/eiga/gallery/news/510863/1987260)  

無類の女好きだが依頼を受ければ並み外れた銃の腕と身体能力、冷静沈着な頭脳で仕事を遂行する超一流スイーパー・冴羽リョウの活躍を描く北条司のマンガ「シティーハンター」。日本での実写映画化はこれが初で、物語の舞台は現代の新宿となる。鈴木がリョウを演じ、「Strawberry Night」seriesの佐藤祐市が監督、三嶋龍朗が脚本、大友良英が音楽を担当する。  
北条司的漫画《城市猎人》描写了虽然无比好色，但只要接受委托就会以超凡的枪法和身体能力、冷静沉着的头脑来完成工作的超一流Sweeper冴羽獠的活跃。这是日本首次真人电影化，故事的舞台是现代的新宿。铃木饰演獠，《草莓之夜》系列导演佐藤祐市担任导演，三嶋龙朗担任编剧，大友良英担任音乐。  

森田が演じる香は、リョウの元相棒である槇村秀幸の妹。リョウの新たなパートナーとなる香が、駅の伝言板の前に笑顔で立つ写真も公開された。森田は「一生懸命でパワフルで愛情深くてチャーミングな香さん。彼女の心を覗けば覗くほど、その魅力の虜になっている自分がいます。皆さんの記憶の中にある彼女の断片に、少しでも触れることができるよう、愛を持って全力で突き進んでいきたいと思います」と意気込んでいる。鈴木、佐藤、エクゼクティブプロデューサーの高橋信一からのコメントも到着した。   
森田饰演的香是獠原搭档槙村秀幸的妹妹。将成为獠的新伙伴的香，在车站留言板前微笑站着的照片也被公开了。森田表示，“香小姐是一个拼命努力、充满力量、充满爱心、充满魅力的人，越是窥探她的内心，就越会发现自己被她的魅力俘虏了。为了能够接触到大家记忆中的她的片断，我会带着爱全力向前迈进”。铃木、佐藤、制片人高桥信一也发表了意见。  

Netflix映画「シティーハンター」は2024年に全世界で配信。  
Netflix电影《城市猎人》将于2024年在全球上映。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  
※冴羽獠的獠，在反犬旁正式标明“僚”的写法  

（原文以下内容同报道《电影「CityHunter」香由森田望智饰演，留言板前的笑容也将公开》，原文和译文参见[这里](./2023-01-31_ch_actress_board.md#comment) ）




---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处