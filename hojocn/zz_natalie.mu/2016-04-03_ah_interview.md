https://natalie.mu/comic/news/182072

# 「エンジェル・ハート」完全読本、北条司のインタビューや仕事場訪問など

2016年4月3日  [Comment](https://natalie.mu/comic/news/182072/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート」のムック「エンジェル・ハート完全読本」が徳間書店より刊行された。  
北条司《Angel Heart》的漫画《Angel Heart完全读本》由德间书店发行。

[
![「Angel Heart完全読本」](https://ogre.natalie.mu/media/news/comic/2016/0402/ahdokuhon.jpg?imwidth=468&imdensity=1)
「Angel Heart完全読本」
大きなサイズで見る  
“天使之心完整读本”  
查看大图  
](https://natalie.mu/comic/gallery/news/182072/500109)

「エンジェル・ハート完全読本」では、北条のロングインタビューを掲載。登場人物について語る「キャラクター編」と、物語の起点と終着点に迫る「エピソード編」の前後編が展開された。また作業机やネームを写真付きで公開する仕事場レポートも。そのほか1stシーズン全363話と2ndシーズン60話の完全ガイド、キャラクター解説など読み物も充実しており、綴じ込み付録でカラーイラストギャラリーも収められている。  
《Angel Heart完全读本》刊登了北条的长篇采访。讲述登场人物的“角色篇”，展开了临近故事起点和终点的“情节部分”的前后篇。还有一份工作场所报告，附有工作台面和故事的照片。除此之外，第一季共363话和第二季60话的完整指南、角色解说等读物也很丰富，作为装册附录还收录了彩色插图图库。（译注：待校对）

なおコミックナタリーでは「エンジェル・ハート」の関連作である「シティーハンター」の特集記事を昨年12月に掲載。キャラクター紹介、用語解説、モーショングラフィックアニメ「リョウのプロポーズ」を制作する神風動画のインタビュー、北条と小室哲哉が主題歌「Get Wild」の魅力を語り合う対談記事などを展開しているので未読の人は要チェック。  
另外Comic Natalie在去年12月刊登了《Angel Heart》的相关作品《城市猎人》的特别报道。角色介绍、用语解说、动画动画《獠的求婚》的制作公司神风动画的采访、北条和小室哲哉演唱主题曲《Get Wild》的魅力的对谈报道等，未读的人一定要看看。  

LINK

[Roman Album「Angel Heart完全読本」発売！ | 北条司 OFFICIAL WEB SITE](http://www.hojo-tsukasa.com/cat-c/1667.html)

関連商品

[
![北条司「Angel Heart完全読本」](https://images-fe.ssl-images-amazon.com/images/I/61yZRxH7oOL._SS70_.jpg)
北条司「エンジェル・ハート完全読本」  
[Mook] 2016年3月31日発売 / 徳間書店 / 978-4197204458  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4197204450/nataliecomic-22)

[
![北条司「シティーハンター XYZ edition（1）」](https://images-fe.ssl-images-amazon.com/images/I/51wmgxxDDoL._SS70_.jpg)
北条司「シティーハンター XYZ edition（1）」  
[漫画] 2015年7月18日発売 / 徳間書店 / 978-4199802812  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199802819/nataliecomic-22)