https://natalie.mu/comic/news/156744

# 「シティーハンター」と北九州のサッカークラブがコラボ、イラストチケット発売
《城市猎人》与北九州足球俱乐部合作，发售插画门票  

2015年8月11日  [Comment](https://natalie.mu/comic/news/156744/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」とサッカークラブのギラヴァンツ北九州がコラボレート。イラストを使用した試合のチケットが発売された。  
北条司的“城市猎人”和足球俱乐部的ギラヴァンツ北九州合作。使用插图的比赛门票开始发售了。

[
![「シティーハンター」とコラボした限定チケットをケースに収めた様子。](https://ogre.natalie.mu/media/news/comic/2015/0811/ticket05.jpg?imwidth=468&imdensity=1)
「シティーハンター」とコラボした限定チケットをケースに収めた様子。  
大きなサイズで見る（全6件）  
和“城市猎人”合作的限定门票收在盒子里的样子。  
查看大图(共6件)  
](https://natalie.mu/comic/gallery/news/156744/396010)

チケットは8月23日に北九州市立本城陸上競技場で行われる対水戸ホーリーホック戦のもの。A席ゾーンの指定券で、シリアルナンバー入りのものが300枚限定で販売されている。ケース付きで価格は2500円。詳細な購入方法は、ギラヴァンツ北九州の公式サイトにて確認を。  
门票是8月23日在北九州市立本城陆上竞技场举行的对水户ホーリーホック的比赛。A席区域的指定票，包含序列号的限量发售300张。附带外壳的价格为2500日元。详细的购买方法，请在ギラヴァンツ北九州的官方网站确认。

なお当日は冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）役で知られる神谷明がスタジアムDJを担当。そのほか会場では写真撮影ブースや、「シティーハンター」を紹介するコーナーなども設けられる。  
另外，当天由饰演冴羽獠(獠的汉字是在反犬旁组合“僚”)的神谷明担任体育场DJ。此外，会场还设有摄影展位和介绍“城市猎人”的环节。


※記事初出時より、タイトルを一部変更しました。  
※报道初次出现时，标题变更了一部分。

この記事の画像（全6件）  
这篇报道的图片(共6篇)  

[![「シティーハンター」とコラボした限定チケットをケースに収めた様子。](https://ogre.natalie.mu/media/news/comic/2015/0811/ticket05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/156744/396010 "「シティーハンター」とコラボした限定チケットをケースに収めた様子。（和“城市猎人”合作的限定门票收在盒子里的样子。）")
[![「シティーハンター」のイラストがあしらわれた限定チケット。](https://ogre.natalie.mu/media/news/comic/2015/0811/ticket01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/156744/396011 "「シティーハンター」のイラストがあしらわれた限定チケット。（这是一张印有《城市猎人》插图的限定门票。）")
[![Ticket Caseの表紙。](https://ogre.natalie.mu/media/news/comic/2015/0811/ticket02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/156744/396012 "Ticket Caseの表紙。（票箱的封面。）")
[![Ticket Caseの裏面。](https://ogre.natalie.mu/media/news/comic/2015/0811/ticket03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/156744/396013 "Ticket Caseの裏面。（票箱的背面。）")
[![Ticket Case。](https://ogre.natalie.mu/media/news/comic/2015/0811/ticket06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/156744/396014 "Ticket Case。（票箱）")
[![Ticket Caseの中面。](https://ogre.natalie.mu/media/news/comic/2015/0811/ticket04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/156744/396015 "Ticket Caseの中面。（票箱的中间部分。）")




LINK   
[City Hunter Collabo Ticket A席入場記念Ticket 限定販売のお知らせ / ギラヴァンツ北九州 公式Site](http://www.giravanz.jp/news/2015/08/cityhunter.html)

