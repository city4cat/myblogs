https://natalie.mu/comic/news/368736

# マンガ・アニメ・ゲーム・特撮と“東京”の関係を辿る展覧会、国立新美術館で開催
国立新美术馆举办追溯漫画、动画、游戏、特摄与“东京”关系的展览会  

2020年2月27日  [Comment](https://natalie.mu/comic/news/368736/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


日本のマンガやアニメを扱った展覧会「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」が、東京・国立新美術館にて7月8日から9月22日まで開催される。開催にあたり、去る2月26日に記者発表会が行われた。  
以日本漫画和动画为主题的展览会“MANGA都市TOKYO日本漫画·动画·游戏·特摄2020”将于7月8日至9月22日在东京国立新美术馆举行。召开之际，在2月26日举行了记者发布会。  

[
![「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」キービジュアル（イラストレーション：吉成曜） (c)Crypton Future Media, INC. www.piapro.net　(c)カラー　(c)武内直子・PNP・東映アニメーション　(c)秋本治・アトリエびーだま／集英社　(c)創通・サンライズ　TM & (c) TOHO CO., LTD.　(c)TOKYO TOWER](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_01.jpg?imwidth=468&imdensity=1)
「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」キービジュアル（イラストレーション：吉成曜） (c)Crypton Future Media, INC. www.piapro.net　(c)カラー　(c)武内直子・PNP・東映アニメーション　(c)秋本治・アトリエびーだま／集英社　(c)創通・サンライズ　TM & (c) TOHO CO., LTD.　(c)TOKYO TOWER
大きなサイズで見る（全51件）
](https://natalie.mu/comic/gallery/news/368736/1339206)

[
![ヨリコ＆ヴィッピー (c)2018 OPMA All Rights Reserved.](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_03.jpg?imwidth=468&imdensity=1)
ヨリコ＆ヴィッピー (c)2018 OPMA All Rights Reserved.［拡大］
](https://natalie.mu/comic/gallery/news/368736/1339207)

文化庁、独立行政法人日本芸術文化振興会、国立新美術館が主催する「MANGA都市TOKYO」は、2018年冬にフランス・パリのラ・ヴィレットにて開催された「MANGA⇔TOKYO」展の凱旋展示として行われるもの。全93タイトルから500点以上におよぶ資料を横断的に展示し、マンガやアニメといったポップカルチャー作品と都市“東京”との関係を、大きく3つのセクションに分けて辿っていく。展覧会のマスコットキャラクターであるヴィッピーとヨリコは、「MANGA⇔TOKYO」展の開催時に制作されたもの。キャラクターデザインとイラストは[吉成曜](https://natalie.mu/comic/artist/67403)が手がけ、キャラクター設定とデザインでコヤマシゲト、草野剛、ゲストキュレーターを務める森川嘉一郎氏が協力した。またヨリコの声は船戸ゆり絵が担当。展覧会のキービジュアルにはセーラームーンや綾波レイ、アムロ・レイといったキャラクターたちに扮したヨリコの姿が描かれている。  
由文化厅、独立行政法人日本艺术文化振兴会、国立新美术馆主办的“MANGA都市TOKYO”，作为2018年冬季在法国巴黎La Villette举办的“MANGA现TOKYO”展览的胜利回归。从93个标题到500件以上的资料横向展示，将漫画和动画等流行文化作品与都市“东京”的关系大致分为3个部分。展览会的吉祥物vippy和Yoriko是“MANGA←TOKYO”展览举办时制作的。角色设计和插画由吉成曜亲自操刀，角色设定和设计由小山重和草野刚、担任嘉宾馆长的森川嘉一郎协助。此外，由船户百合绘为赖子配音。在展览会的Key Visual中，描绘了扮成水兵月、绫波丽、阿姆罗·丽等角色的赖子的身姿。

[
![記者発表会に登壇した国立新美術館の逢坂恵理子館長（左）、ゲストキュレーターを務める森川嘉一郎氏（右）。](https://ogre.natalie.mu/media/news/comic/2020/0226/DSC_9716.jpg?imwidth=468&imdensity=1)
記者発表会に登壇した国立新美術館の逢坂恵理子館長（左）、ゲストキュレーターを務める森川嘉一郎氏（右）。［拡大］
](https://natalie.mu/comic/gallery/news/368736/1339208)

「MANGA都市TOKYO」というテーマの出発点について、発表会に登壇した森川氏は「パリで大規模なポップカルチャーの展示を行うことになり、巨大な会場をどう使うかを考えたときに、巨大な模型を中央に置いて、その模型の範囲を舞台にした作品群を取り巻くように展示するというアイデアが浮かんだ」と語る。そして「日本のマンガ・アニメ・ゲーム・特撮は、世界的に見たときに、具体的な場所を舞台やモデルにした作品が非常に多いのが特徴。それも単に特定の場所を模して背景が描かれているということにとどまらない、相互の影響関係が見てとれる。それを展示にしたいと考えました」と続けた。  
关于“MANGA都市TOKYO”这一主题的出发点，在发表会上登台的森川先生说:“要在巴黎进行大规模的流行文化展示，在考虑如何使用巨大的会场时，把巨大的模型放在中央，我想到了围绕以模型范围为舞台的作品群进行展示的想法”。并且“日本的漫画·动画·游戏·特摄，在世界范围内看的时候，以具体的场所为舞台和模特的作品非常多。不仅是模仿特定的地方来描绘背景，还可以看到相互的影响关系。我想把它展示出来。“他继续说。  

[
![パリでの「MANGA⇔TOKYO」展の様子。(c)MANGA ⇔ TOKYO Japonismes 2018](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_05.jpg?imwidth=468&imdensity=1)
パリでの「MANGA⇔TOKYO」展の様子。(c)MANGA ⇔ TOKYO Japonismes 2018［拡大］
](https://natalie.mu/comic/gallery/news/368736/1339209)

[
![パリでの「MANGA⇔TOKYO」展の様子。(c)MANGA ⇔ TOKYO Japonismes 2018](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_06.jpg?imwidth=468&imdensity=1)
パリでの「MANGA⇔TOKYO」展の様子。(c)MANGA ⇔ TOKYO Japonismes 2018［拡大］
](https://natalie.mu/comic/gallery/news/368736/1339210)

その巨大な模型というのが、イントロダクションとして設置される1/1000スケールの巨大な東京都市部の模型。幅約17m、長さ約22mにおよぶ模型では、山手線が収まる程度の都市部が再現されるという。また模型の付近にはスクリーンを併設し、「秒速5センチメートル」「残響のテロル」「ラブライブ！」「STEINS;GATE」などの作品から、実在の東京の各所が登場するシーンを動画で紹介。模型が表す現実の都市と、フィクションの中の都市を重ね合わせるように鑑賞できる。森川氏は「現在の東京の都市風景は戦後に作り上げられたもので、とりわけ繁華街は10年前とは姿がまったく異なっていることも多い。そうなると、マンガやアニメに描かれたその時々の東京の風景が、物理的な都市風景より長生きする可能性がある」と話し、現実とフィクションのハイブリッドによって形成される都市風景のイメージを表現したい、と意図を語った。  
那个巨大的模型，是作为开头设置的1/1000比例的巨大的东京都市部的模型。宽约17m，长约22m的模型，再现了山手线所能容纳的程度的城市。另外，模型附近还设置了屏幕，播放“秒速5厘米”、“残响的恐怖”、“lovelive !”从“STEINS;GATE”等作品中，以实际存在的东京各处登场的场景的动画介绍。观众可以欣赏到，模型所表现的现实城市和虚构的城市重叠在一起。森川先生说:“现在东京的都市风景是战后形成的，特别是繁华的街道，很多情况与10年前的面貌完全不同。这样的话，漫画和动画中描绘的当时的东京风景，就有可能比物理上的都市风景更长寿。”他说，想表现现实和虚构混合形成的都市风景的形象。

[
![大友克洋「AKIRA」 (c)MASH・ROOM/KODANSHA](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_10.jpg?imwidth=468&imdensity=1)
大友克洋「AKIRA」 (c)MASH・ROOM/KODANSHA［拡大］
](https://natalie.mu/comic/gallery/news/368736/1339211)

[
![「サクラ大戦」 (c)SEGA](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_22.jpg?imwidth=468&imdensity=1)
「サクラ大戦」 (c)SEGA［拡大］
](https://natalie.mu/comic/gallery/news/368736/1339212)

[
![上條淳士「To-y」 (c)上條淳士／小学館／小学館クリエイティブ](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_29.jpg?imwidth=468&imdensity=1)
上條淳士「To-y」 (c)上條淳士／小学館／小学館クリエイティブ［拡大］
](https://natalie.mu/comic/gallery/news/368736/1339213)

[
![羽海野チカ「3月のライオン」 (c)羽海野チカ／白泉社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_34.jpg?imwidth=468&imdensity=1)
羽海野チカ「3月のライオン」 (c)羽海野チカ／白泉社［拡大］
](https://natalie.mu/comic/gallery/news/368736/1339214)

展覧会を構成する3つのセクションのうち、セクション1では「破壊と復興の反復」と題し、東京の大規模な破壊や、破壊からの復興を描いた作品として、[大友克洋](https://natalie.mu/comic/artist/2225)「AKIRA」、「エヴァンゲリオン」シリーズ、「ゴジラ」シリーズなどを取り上げる。セクション2の「東京の日常」ではさらに3つの時代に区切り、「プレ東京としての江戸」では[杉浦日向子](https://natalie.mu/comic/artist/2138)「百日紅」や[安野モヨコ](https://natalie.mu/comic/artist/1783)「さくらん」など江戸を描いた作品を、「近代化の幕開けからポストモダン都市まで」では[和月伸宏](https://natalie.mu/comic/artist/4971)「るろうに剣心－明治剣客浪漫譚－」や[谷口ジロー](https://natalie.mu/comic/artist/2232)・関川夏央「『坊っちゃん』の時代」などの明治期から、高森朝雄・[ちばてつや](https://natalie.mu/comic/artist/1706)「あしたのジョー」や[上條淳士](https://natalie.mu/comic/artist/2102)「To-y」といった高度経済成長期までの、豊かになっていく東京を舞台にした作品を展示。「世紀末から現在まで」では[浅野いにお](https://natalie.mu/comic/artist/2189)「ソラニン」や[羽海野チカ](https://natalie.mu/comic/artist/1800)「3月のライオン」など、未来に希望を持ちにくくなった時代の作品を例に挙げ、東京の描かれ方の変遷を追っていく。なお森川氏は「あしたのジョー」について、「さまざまなマンガに関する展覧会で展示されている作品ですが、本展ではボクシングのシーンを一切展示していません」と明かし、都市“東京”にフォーカスした本展の特徴が出ている点だと語った。  
在构成展览会的三个部分中，第一部分以“破坏与复兴的反复”为题，描绘东京的大规模破坏，以及从破坏中复兴的作品有:大友克洋的《AKIRA》、《福音战士》系列、《五字》等”系列报道。第二部分“东京的日常”将作品进一步划分为三个时代，“作为东京的江户”包含杉浦日向子的《百日红》、安野茂横的《樱花》等描写江户的作品，“从近代化的开始到后现代都市”包含杉浦日向子的作品从和月伸宏的《浪客剑心-明治剑客浪漫谭-》、谷口次郎·关川夏央的《哥儿的时代》等明治时期，到高森朝雄·千叶铁也的《明日之丈》、上条淳士的《To-y》等经济高度成长期的丰富。展示了以东京为舞台的作品。《从世纪末到现在》以浅野伊尼夫的《空人》、羽海野千花的《3月的狮子》等难以对未来抱有希望的时代作品为例，追踪描写东京的变迁。关于《明日之丈》，森川先生表示:“虽然在各种各样的漫画展览会上都展出过作品，但本次展览完全没有展示拳击的场景。”这是聚焦于都市“东京”的本次展览的特征。他说，这是突出的一点。  

そしてセクション3「キャラクターvs.都市」では、ここまでとは逆に、初音ミクとNewDaysのコラボ、お台場の実物大ユニコーンガンダム立像といった、現実の都市に現れたキャラクターたちに目を向け、インスタレーションでその様子を再現する。なおセクション3はセクション1の向かいに配置され、虚構の東京の中をゴジラが侵食している様子に対して、現実の東京が虚構の世界に侵食されているさまが対比できるよう構成される。  
在Part3“角色vs.都市”中，与之前相反，初音未来和NewDays的合作，台场的实物大独角兽高达立像等，将目光转向现实都市中出现的角色们。用挂图再现那个样子。另外，Part3被配置在Part2的对面，虚构的东京中哥斯拉侵蚀的样子与现实的东京被虚构的世界侵蚀的样子形成对比。  

パリでの「MANGA⇔TOKYO」展の開催時から、東京での巡回を想定していたという本展。国立新美術館主任研究員の真住貴子氏は、会期が東京オリンピック・パラリンピックの開催時期と重なることについて、「海外からいらした方にもお見せできる機会を作れたことはうれしく、身の引き締まる思いもある」と語る。「MANGA⇔TOKYO」展の現地での反響については「いわゆるオタク的な方だけではなく、『なんで若者がこんなに日本のマンガに熱中しているのか、理由が知りたい』という方もかなりいらしていました」と振り返り、「通常マンガやアニメの展覧会では、作品のハイライトになるようなシーンを展示することが多いですが、本展では“都市・東京”をテーマにすることで、こうした分野にさほど興味がない人にも楽しんでいただける内容にしております」と解説した。また国立新美術館の逢坂恵理子館長からは、「虚構と現実のハイブリッドな体験を、今回の展覧会を通して実感してもらうことができれば幸いです」と挨拶が述べられた。  
在巴黎举办“MANGA⇔TOKYO”展览时，就设想了在东京举办巡回展览。国立新美术馆主任研究员真住贵子，对于会期与东京奥运会·残奥会的举办时间重叠，表示“能有机会让海外来的观众也能看到，感到很高兴，也很紧张”。他说。关于“MANGA⇔TOKYO”展览在当地的反响，他回顾道:“不只是所谓的宅男，也有很多人想知道为什么年轻人如此热衷于日本漫画。”通常，在漫画和动画的展览会上，展示的多是作品的高潮部分，但这次以“都市·东京”为主题，让对这些领域没有兴趣的人也能乐在其中。我已经原谅了。”此外，国立新美术馆的逢坂惠理子馆长也发表了致辞，他说:“如果能通过此次展览会，让大家切实感受到虚构与现实的混合体验，将是我的荣幸。”

「MANGA都市TOKYO」の前売券は7月7日まで展覧会Webサイト、チケットぴあにて販売される。展覧会Twitterアカウントも開設され、招待券が当たるTwitterキャンペーンが3月12日まで実施中だ。会期中にはコスプレイベントなども開催予定。なお11月21日から2021年1月17日にかけて、大分・大分県立美術館への巡回が決定している。  
“MANGA都市TOKYO”的预售票截止到7月7日在展览会网站和Ticket Pia上销售。展览会推特账号也已开设，截止到3月12日正在进行中奖邀请券的推特活动。会议期间预定还将举办cosplay活动等。另外，11月21日至2021年1月17日，将在大分县立美术馆巡回展出。

※5/26追記：新型コロナウイルス感染症拡大の影響で、「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」の開催は8月12日～11月3日に変更となりました。最新の情報は公式サイトにてご確認ください。  
※5/26补充:受新型冠状病毒感染扩大的影响，“MANGA都市TOKYO日本漫画·动画·游戏·特摄2020”的举办日期变更为8月12日~11月3日。最新的信息请在官方网站确认。  

この記事の画像・動画（全51件）  
这篇报道的图片、视频(共51篇)  

[![「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」キービジュアル（イラストレーション：吉成曜） (c)Crypton Future Media, INC. www.piapro.net　(c)カラー　(c)武内直子・PNP・東映アニメーション　(c)秋本治・アトリエびーだま／集英社　(c)創通・サンライズ　TM & (c) TOHO CO., LTD.　(c)TOKYO TOWER](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339206 "「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」キービジュアル（イラストレーション：吉成曜） (c)Crypton Future Media, INC. www.piapro.net　(c)カラー　(c)武内直子・PNP・東映アニメーション　(c)秋本治・アトリエびーだま／集英社　(c)創通・サンライズ　TM & (c) TOHO CO., LTD.　(c)TOKYO TOWER")
[![ヨリコ＆ヴィッピー (c)2018 OPMA All Rights Reserved.](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339207 "ヨリコ＆ヴィッピー (c)2018 OPMA All Rights Reserved.")
[![記者発表会に登壇した国立新美術館の逢坂恵理子館長（左）、ゲストキュレーターを務める森川嘉一郎氏（右）。](https://ogre.natalie.mu/media/news/comic/2020/0226/DSC_9716.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339208 "記者発表会に登壇した国立新美術館の逢坂恵理子館長（左）、ゲストキュレーターを務める森川嘉一郎氏（右）。")
[![パリでの「MANGA⇔TOKYO」展の様子。(c)MANGA ⇔ TOKYO Japonismes 2018](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339209 "パリでの「MANGA⇔TOKYO」展の様子。(c)MANGA ⇔ TOKYO Japonismes 2018")
[![パリでの「MANGA⇔TOKYO」展の様子。(c)MANGA ⇔ TOKYO Japonismes 2018](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339210 "パリでの「MANGA⇔TOKYO」展の様子。(c)MANGA ⇔ TOKYO Japonismes 2018")
[![大友克洋「AKIRA」 (c)MASH・ROOM/KODANSHA](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339211 "大友克洋「AKIRA」 (c)MASH・ROOM/KODANSHA")
[![「サクラ大戦」 (c)SEGA](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_22.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339212 "「サクラ大戦」 (c)SEGA")
[![上條淳士「To-y」 (c)上條淳士／小学館／小学館クリエイティブ](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_29.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339213 "上條淳士「To-y」 (c)上條淳士／小学館／小学館クリエイティブ")
[![羽海野チカ「3月のライオン」 (c)羽海野チカ／白泉社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_34.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339214 "羽海野チカ「3月のライオン」 (c)羽海野チカ／白泉社")
[![「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」チラシ](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339216 "「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」チラシ")
[![「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」ロゴ](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339217 "「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」ロゴ")
[![「グランツーリスモＳＰＯＲＴ」 (c) 2017 Sony Interactive Entertainment Inc. Developed by Polyphony Digital Inc.Manufacturers, cars, names, brands and associated imagery featured in this game in some cases includetrademarks and/or copyrighted materials of their respective owners. All rights reserved. Any depiction orrecreation of real world locations, entities, businesses, or organizations is not intended to be or imply anysponsorship or endorsement of this game by such party or parties. &quot;Gran Turismo&quot; logos are registeredtrademarks or trademarks of Sony Interactive Entertainment Inc.](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339218 "「グランツーリスモＳＰＯＲＴ」 (c) 2017 Sony Interactive Entertainment Inc. Developed by Polyphony Digital Inc.Manufacturers, cars, names, brands and associated imagery featured in this game in some cases includetrademarks and/or copyrighted materials of their respective owners. All rights reserved. Any depiction orrecreation of real world locations, entities, businesses, or organizations is not intended to be or imply anysponsorship or endorsement of this game by such party or parties. &quot;Gran Turismo&quot; logos are registeredtrademarks or trademarks of Sony Interactive Entertainment Inc.")
[![「ゴジラ」（1954） TM & (c) TOHO CO., LTD.](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339219 "「ゴジラ」（1954） TM & (c) TOHO CO., LTD.")
[![大友克洋「AKIRA」 (c)MASH・ROOM/KODANSHA](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339220 "大友克洋「AKIRA」 (c)MASH・ROOM/KODANSHA")
[![「ヱヴァンゲリヲン新劇場版:破」 (c)カラー](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339221 "「ヱヴァンゲリヲン新劇場版:破」 (c)カラー")
[![松本大洋・永福一成「竹光侍」 (c)松本大洋・永福一成／小学館](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_14.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339224 "松本大洋・永福一成「竹光侍」 (c)松本大洋・永福一成／小学館")
[![杉浦日向子「百日紅」 (c)杉浦日向子](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_15.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339225 "杉浦日向子「百日紅」 (c)杉浦日向子")
[![一ノ関圭「鼻紙写楽」 (c)一ノ関圭／小学館](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_16.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339226 "一ノ関圭「鼻紙写楽」 (c)一ノ関圭／小学館")
[![安野モヨコ「さくらん」 (c)Moyoco Anno / Cork](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_17.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339227 "安野モヨコ「さくらん」 (c)Moyoco Anno / Cork")
[![安野モヨコ「さくらん」 (c)Moyoco Anno / Cork](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_18.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339228 "安野モヨコ「さくらん」 (c)Moyoco Anno / Cork")
[![和月伸宏「るろうに剣心－明治剣客浪漫譚－」 (c)和月伸宏／集英社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_19.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339229 "和月伸宏「るろうに剣心－明治剣客浪漫譚－」 (c)和月伸宏／集英社")

[![関川夏央・谷口ジロー「『坊っちゃん』の時代」 (c)パピエ](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_20.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339230 "関川夏央・谷口ジロー「『坊っちゃん』の時代」 (c)パピエ")
[![関川夏央・谷口ジロー「『坊っちゃん』の時代」 (c)パピエ](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_21.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339231 "関川夏央・谷口ジロー「『坊っちゃん』の時代」 (c)パピエ")
[![大和和紀「はいからさんが通る」 (c)大和和紀／講談社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_23.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339232 "大和和紀「はいからさんが通る」 (c)大和和紀／講談社")
[![村上もとか「フイチン再見！」 (c)村上もとか／小学館](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_24.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339233 "村上もとか「フイチン再見！」 (c)村上もとか／小学館")
[![滝田ゆう「寺島町奇譚」 (c)滝田ゆう](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_25.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339234 "滝田ゆう「寺島町奇譚」 (c)滝田ゆう")
[![永島慎二「フーテン」 (c)永島慎二](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_26.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339235 "永島慎二「フーテン」 (c)永島慎二")
[![高森朝雄・ちばてつや「あしたのジョー」 (c)高森朝雄・ちばてつや／講談社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_27.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339236 "高森朝雄・ちばてつや「あしたのジョー」 (c)高森朝雄・ちばてつや／講談社")
[![「ルパン三世（PART2）」原作：モンキー・パンチ (c)TMS](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_28.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339237 "「ルパン三世（PART2）」原作：モンキー・パンチ (c)TMS")
[![北条司「シティーハンター」 (c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_30.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339238 "北条司「シティーハンター」 (c)北条司/NSP 1985")
[![桜沢エリカ「ラブリー！」 (c)桜沢エリカ／ナデシコプロ](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_31.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339239 "桜沢エリカ「ラブリー！」 (c)桜沢エリカ／ナデシコプロ")
[![わたせせいぞう「東京エデン」 (c)わたせせいぞう](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_32.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339240 "わたせせいぞう「東京エデン」 (c)わたせせいぞう")
[![「時をかける少女」 (c)「時をかける少女」製作委員会2006](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_33.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339241 "「時をかける少女」 (c)「時をかける少女」製作委員会2006")
[![浅野いにお「ソラニン」 (c)浅野いにお／小学館](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_35.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339242 "浅野いにお「ソラニン」 (c)浅野いにお／小学館")
[![「秒速5センチメートル」 (c)Makoto Shinkai / CoMix Wave Films](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_36.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339243 "「秒速5センチメートル」 (c)Makoto Shinkai / CoMix Wave Films")
[![「秒速5センチメートル」 (c)Makoto Shinkai / CoMix Wave Films](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_37.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339244 "「秒速5センチメートル」 (c)Makoto Shinkai / CoMix Wave Films")
[![岡崎京子「リバーズ・エッジ」 (c)岡崎京子／宝島社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_38.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339245 "岡崎京子「リバーズ・エッジ」 (c)岡崎京子／宝島社")
[![高尾じんぐ「くーねるまるた」 (c)高尾じんぐ／小学館](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_39.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339246 "高尾じんぐ「くーねるまるた」 (c)高尾じんぐ／小学館")
[![久住昌之・水沢悦子「花のズボラ飯」 (c)久住昌之・水沢悦子（秋田書店）2010](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_40.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339247 "久住昌之・水沢悦子「花のズボラ飯」 (c)久住昌之・水沢悦子（秋田書店）2010")
[![「おおかみこどもの雨と雪」 (c)2012「おおかみこどもの雨と雪」製作委員会](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_41.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339248 "「おおかみこどもの雨と雪」 (c)2012「おおかみこどもの雨と雪」製作委員会")
[![「すばらしきこのせかい -Final Remix-」 (c) 2007,2018 SQUARE ENIX CO., LTD. All Rights Reserved. CHARACTER DESIGN: TETSUYA NOMURA & GEN KOBAYASHI](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_42.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339249 "「すばらしきこのせかい -Final Remix-」 (c) 2007,2018 SQUARE ENIX CO., LTD. All Rights Reserved. CHARACTER DESIGN: TETSUYA NOMURA & GEN KOBAYASHI")
[![「JSRF　ジェットセットラジオフューチャー」 (c)SEGA](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_43.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339250 "「JSRF　ジェットセットラジオフューチャー」 (c)SEGA")
[![「龍が如く 極2」 (c)SEGA](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_44.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339251 "「龍が如く 極2」 (c)SEGA")
[![和久井健「新宿スワン」 (c)和久井健／講談社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_45.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339252 "和久井健「新宿スワン」 (c)和久井健／講談社")
[![「STEINS; GATE」 (c) MAGES./Nitroplus](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_46.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339253 "「STEINS; GATE」 (c) MAGES./Nitroplus")
[![「STEINS; GATE」 (c) MAGES./Nitroplus](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_47.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339254 "「STEINS; GATE」 (c) MAGES./Nitroplus")
[![木尾士目「げんしけん」 (c)木尾士目／講談社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_48.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339255 "木尾士目「げんしけん」 (c)木尾士目／講談社")
[![秋本治「こちら葛飾区亀有公園前派出所」 (c)秋本治・アトリエびーだま／集英社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_50.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339256 "秋本治「こちら葛飾区亀有公園前派出所」 (c)秋本治・アトリエびーだま／集英社")
[![秋本治「こちら葛飾区亀有公園前派出所」 (c)秋本治・アトリエびーだま／集英社](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_51.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339257 "秋本治「こちら葛飾区亀有公園前派出所」 (c)秋本治・アトリエびーだま／集英社")
[![黒川依「ひとり暮らしのOLを描きました」 (c)黒川依・pickles the frog/NSP 2015](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_53.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/368736/1339258 "黒川依「ひとり暮らしのOLを描きました」 (c)黒川依・pickles the frog/NSP 2015")
[![7/8開幕『MANGA都市TOKYO』展（六本木・国立新美術館）PR映像](https://i.ytimg.com/vi/bBTYINoxrxI/default.jpg)](https://natalie.mu/comic/gallery/news/368736/media/46220 "7/8開幕『MANGA都市TOKYO』展（六本木・国立新美術館）PR映像")

## 「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」

会場：国立新美術館 企画展示室2E  
会期：2020年7月8日（水）～9月22日（火・祝）10:00～18:00（金・土 10:00～21:00）※入場は閉館の30分前まで  
休館日：毎週火曜日（ただし7月28日、8月4日、25日、9月1日、22日は開館）  
料金：当日 一般1600円、大学生1200円、高校生800円 / 前売・団体 一般1400円、大学生1000円、高校生600円  
※中学生以下、障害者手帳をご持参の方（付添の方1名含む）は入場無料  
※8月14日（金）～16日（日）は高校生無料観覧日（要学生証）

