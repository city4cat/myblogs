https://natalie.mu/comic/news/373243

# お気に入りの扉絵を着よう！シティーハンター35周年のプリントTシャツ・トート企画  
穿上喜欢的扉页画吧!城市猎人35周年的印花T恤tote企划  

2020年3月29日 [Comment](https://natalie.mu/comic/news/373243/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」の連載開始35周年を記念した企画「CITY HUNTER 336T PROJECT」が、3月31日より東京・吉祥寺のCAFE ZENONおよびZENON SAKABAにて実施される。  
北条司纪念《城市猎人》连载35周年的企划“CITY HUNTER 336t PROJECT”将于3月31日在东京吉祥寺的CAFE ZENON及ZENON在SAKABA实施。

[
![「CITY HUNTER 336T PROJECT」で作れるTシャツの一例。](https://ogre.natalie.mu/media/news/comic/2020/0329/cityhunter_tshirts_project01.jpg?impolicy=hq&imwidth=730&imdensity=1)
「CITY HUNTER 336T PROJECT」で作れるTシャツの一例。  
大きなサイズで見る（全5件）  
这是“CITY HUNTER 336t PROJECT”制作T恤的一个例子。  
查看大图(共5件)
](https://natalie.mu/comic/gallery/news/373243/1356964)

[
![「CITY HUNTER 336T PROJECT」で作れるトートバッグの一例。](https://ogre.natalie.mu/media/news/comic/2020/0329/cityhunter_tshirts_project03.jpg?imwidth=468&imdensity=1)
「CITY HUNTER 336T PROJECT」で作れるトートバッグの一例。［拡大］  
这是“CITY HUNTER 336t PROJECT”制作的托特包的一个例子。[放大]  
](https://natalie.mu/comic/gallery/news/373243/1356965)

2019年より刊行されているゼノンセレクション版「シティーハンター」全336話の扉絵から、好きなページをプリントしたTシャツやトートバッグを作れる「CITY HUNTER 336T PROJECT」。実施店舗にて扉絵とアイテムを選んでオーダーシートに記入し、支払いをすると、約1カ月後に希望のアイテムが届けられる仕組みだ。  
从2019年开始发行的Zenon精选版《城市猎人》全336话的扉页中，可以制作出印有自己喜欢的页面的T恤和提包的“CITY HUNTER 336T PROJECT”。在实施店铺选择扉页和物品填写在订单上，支付的话，大约一个月后就能收到想要的物品。  

Tシャツは税込4378円、トートバッグは税込3960円で、どちらも送料は500円。なおプロジェクト期間中、「シティーハンター」35周年公式Twitterアカウント（@cityhunter100t）では全336話を1日1話投稿する。  
T恤含税4378日元，Tote包含税3960日元，两者运费均为500日元。另外在项目期间，《城市猎人》35周年官方推特账号(@cityhunter100t)将每天发布一集共336话。


この記事の画像（全5件）  
这篇报道的图片(共5篇)  

[![「CITY HUNTER 336T PROJECT」で作れるTシャツの一例。](https://ogre.natalie.mu/media/news/comic/2020/0329/cityhunter_tshirts_project01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/373243/1356964 "「CITY HUNTER 336T PROJECT」で作れるTシャツの一例。")
[![「CITY HUNTER 336T PROJECT」で作れるトートバッグの一例。](https://ogre.natalie.mu/media/news/comic/2020/0329/cityhunter_tshirts_project03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/373243/1356965 "「CITY HUNTER 336T PROJECT」で作れるトートバッグの一例。")
[![「CITY HUNTER 336T PROJECT」で作れるTシャツのイメージ。](https://ogre.natalie.mu/media/news/comic/2020/0329/cityhunter_tshirts_project04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/373243/1356966 "「CITY HUNTER 336T PROJECT」で作れるTシャツのイメージ。")
[![ゼノンセレクション版「シティーハンター」1巻](https://ogre.natalie.mu/media/news/comic/2020/0329/cityhunter_tshirts_project05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/373243/1356967 "ゼノンセレクション版「シティーハンター」1巻")
[![「CITY HUNTER 336T PROJECT」バナー](https://ogre.natalie.mu/media/news/comic/2020/0329/cityhunter_tshirts_project02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/373243/1356963 "「CITY HUNTER 336T PROJECT」バナー")

(c)TSUKASA HOJO / COAMIX 1985


LINK

[CITYHUNTER 336T PROJECT | 『シティーハンター』の全336話の扉絵から選べるあなただけの特別な一枚。](https://cityhunter-336tshirts.com/)  
[シティーハンター公式@35th Anniversary (@cityhunter100t) | Twitter](https://twitter.com/cityhunter100t)  
[北条司 公式サイト ｜ TSUKASA HOJO OFFICIAL WEB SITE](http://www.hojo-tsukasa.com/)  
[北条司オフィシャル (@hojo\_official) | Twitter](https://twitter.com/hojo_official)  

