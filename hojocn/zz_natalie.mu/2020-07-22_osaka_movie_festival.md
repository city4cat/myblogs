https://natalie.mu/comic/news/388740


# 大阪で8月に爆音映画祭「逆襲のシャア」「メイドインアビス 深き魂の黎明」など

2020年7月22日  [Comment](https://natalie.mu/comic/news/388740/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

「なんばパークスシネマ アニメーション爆音映画祭」が8月14日から23日まで大阪・なんばパークスシネマで開催される。
“Namba Parks Cinema Animation爆音电影节”将于8月14日至23日在大阪·Namba Parks Cinema举行。

[
![「なんばパークスシネマ アニメーション爆音映画祭」](https://ogre.natalie.mu/media/news/comic/2020/0722/bakuoneigasai_nambacinemaparks.jpg?imwidth=468&imdensity=1)
「なんばパークスシネマ アニメーション爆音映画祭」  
大きなサイズで見る（全3件）  
“Namba Parks Cinema Animation爆音电影节”  
大尺寸观看(共3件)  
](https://natalie.mu/comic/gallery/news/388740/1412241)

「アニメを“音”で、もっと楽しもう！」をコンセプトに、ライブ・コンサート用の音響機器を使い、高品質かつ大音量のサウンドで作品を上映する同イベント。上映作品には「[機動戦士ガンダム 逆襲のシャア](https://natalie.mu/eiga/film/114620)」「メイドインアビス 深き魂の黎明」「劇場版 TIGER & BUNNY -The Beginning-」「劇場版 TIGER & BUNNY -The Rising-」「[COWBOY BEBOP 天国の扉](https://natalie.mu/eiga/film/137598)」「[クラッシャージョウ](https://natalie.mu/eiga/film/133008)」「[パプリカ](https://natalie.mu/eiga/film/145913)」「[千年女優](https://natalie.mu/eiga/film/139006)」「[プロメア](https://natalie.mu/eiga/film/178482)」「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」「[スパイダーマン：スパイダーバース](https://natalie.mu/eiga/film/174851)」がラインナップされた。チケットは7月30日22時になんばパークスシネマ公式サイトで発売され、8月11日からは劇場の窓口でも販売される。  
“用‘声音’来享受动画吧!”为概念，使用现场音乐会用的音响设备，以高质量和大音量放映作品的该活动。上映作品有《机动战士高达逆袭的夏亚》、《深渊制造》、《tig&bunny -The beginning》、《tig&bunny -The剧场版》《Rising-》、《COWBOY BEBOP天国之门》、《clisshore》、《红辣椒》、《千年女优》、《promea》、《剧场版城市猎人<新宿Private Eyes>》、《蜘蛛侠:蜘蛛巴斯》的阵容。门票将于7月30日22时在南波帕克斯电影官方网站发售，8月11日开始在剧场窗口销售。

この記事の画像（全3件）  
这篇报道的图片(共3篇)  
[![「なんばパークスシネマ アニメーション爆音映画祭」](https://ogre.natalie.mu/media/news/comic/2020/0722/bakuoneigasai_nambacinemaparks.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/388740/1412241 "「なんばパークスシネマ アニメーション爆音映画祭」")
[![「なんばパークスシネマ アニメーション爆音映画祭」チラシ](https://ogre.natalie.mu/media/news/comic/2020/0722/bakuoneigasai_nambacinemaparks_poster01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/388740/1412242 "「なんばパークスシネマ アニメーション爆音映画祭」チラシ")
[![「なんばパークスシネマ アニメーション爆音映画祭」チラシの裏。](https://ogre.natalie.mu/media/news/comic/2020/0722/bakuoneigasai_nambacinemaparks_poster02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/388740/1412243 "「なんばパークスシネマ アニメーション爆音映画祭」チラシの裏。")

## 「なんばパークスシネマ アニメーション爆音映画祭」

期間：2020年8月14日（金）～23日（日）  
会場：大阪府 なんばパークスシネマ（Namba Parks Cinema）  
料金：2000円

### 上映作品

「[機動戦士ガンダム 逆襲のシャア](https://natalie.mu/eiga/film/114620)」  
「メイドインアビス 深き魂の黎明」  
「劇場版 TIGER & BUNNY -The Beginning-」  
「劇場版 TIGER & BUNNY -The Rising-」  
「[COWBOY BEBOP 天国の扉](https://natalie.mu/eiga/film/137598)」  
「[クラッシャージョウ](https://natalie.mu/eiga/film/133008)」  
「[パプリカ](https://natalie.mu/eiga/film/145913)」  
「[千年女優](https://natalie.mu/eiga/film/139006)」  
「[プロメア](https://natalie.mu/eiga/film/178482)」  
「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」  
「[スパイダーマン：スパイダーバース](https://natalie.mu/eiga/film/174851)」

LINK

[Namba Parks Cinema](https://www.parkscinema.com/site/namba/)  
[「Namba Parks Cinema Animation爆音映画祭」公式Site](https://www.parkscinema.com/campaign/bakuon_namba_202008_anime)  