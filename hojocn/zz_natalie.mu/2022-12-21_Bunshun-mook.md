https://natalie.mu/comic/news/505882

# 「Lupin三世VSCat's Eye」Japan Premiere開催、文春MookのCoverをジャック  
「Lupin三世VS Cat's Eye」举办日本首映礼，文春Mook封面Jack  

2022年12月21日  [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[Monkey Punch](https://natalie.mu/comic/artist/1749)原作によるアニメ「Lupin三世」と[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「Cat's Eye」のコラボアニメ映画「Lupin三世VSCat's Eye」より、Japan Premiereの開催が決定。2023年1月24日に東京・TOHOシネマズ六本木で行われる。  
由Monkey Punch原作的动画《鲁邦三世》和北条司原作的动画《猫眼》合作制作的动画电影「Lupin三世VS Cat's Eye」将在日本首映。将于2023年1月24日在东京·TOHO Cinemas六本木举行。  

[
![Amazon Original「Lupin三世 VS Cat's Eye」Japan Premiereの告知画像。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1220/japan_p.jpg?impolicy=hq&imwidth=730&imdensity=1)
Amazon Original「Lupin三世 VS Cat's Eye」Japan Premiereの告知画像。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会  
大きなサイズで見る（全47件）  
（Amazon Original《鲁邦三世VS猫眼》日本首映的通告图像。北条司/鲁邦三世VS猫眼制作委员会  
大尺寸观看(共47件)）  
](https://natalie.mu/comic/gallery/news/505882/1963798)

2023年1月27日にPrime Videoで世界独占配信される「Lupin三世VSCat's Eye」。Japan Premiere当日は、5.1chの大スクリーンでの本編上映会に加え、Lupin三世役の[栗田貫一](https://natalie.mu/comic/artist/6789)、来生瞳役の[戸田恵子](https://natalie.mu/comic/artist/16682)、[静野孔文](https://natalie.mu/comic/artist/62364)監督、[瀬下寛之](https://natalie.mu/comic/artist/92064)監督の舞台挨拶も実施される予定だ。チケットは12月23日12時から2023年1月9日23時59分まで、チケットぴあでオフィシャル先行予約を受け付ける。  
《鲁邦三世VS猫眼》将于2023年1月27日通过Prime Video全球独家发布。日本首映当天，除了5.1声道的大银幕正片放映会之外，鲁邦三世的扮演者栗田贯一、来生瞳的扮演者户田惠子、静野孔文导演、濑下宽之导演也将登台致辞。门票将从12月23日12点开始到2023年1月9日23点59分为止，通过Ticket Pia接受官方先行预约。  

[
![「Cat's Eye週刊文春エンタ+ 80年代！少年マンガの熱狂」のCoverイラスト。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1220/bunshun_plus_cover.jpg?imwidth=468&imdensity=1)
「週刊文春エンタ+ 80年代！少年マンガの熱狂」のCoverイラスト。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会［拡大］
](https://natalie.mu/comic/gallery/news/505882/1963799)

[
![「中綴じ付録ポスター」のイラスト。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1220/bunshun_plus_poster.jpg?imwidth=468&imdensity=1)
「中綴じ付録ポスター」のイラスト。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会［拡大］
](https://natalie.mu/comic/gallery/news/505882/1963800)

さらに明日12月22日発売のMook「週刊文春エンタ+ 80年代！少年マンガの熱狂」では、「Lupin三世VSCat's Eye」の配信を記念し、北条の描き下ろしイラストがCoverの表裏を飾ることも明らかに。「中綴じ付録ポスター」には、不二子と瞳の肩を抱いたLupin三世のほか、元気いっぱいの愛にタジタジの五ェ門、そして背中合わせに大人の雰囲気を漂わせる次元と泪の2ショットが収められる。  
明天12月22日发售的《周刊文春娱乐+ 80年代!少年漫画的狂热》中，为了纪念《鲁邦三世VS猫眼》的配信，封面的正反面装饰了北条的插画。这张"中綴じ的附录海报 "包含两个镜头：鲁邦三世抱着藤子和瞳的肩膀，五ェ門看着精力充沛的小爱，以及背靠背洋溢着大人气息的次元和眼泪。（译注：待校对）  

北条はイラストについて、「ストーリーを考えながら描くのが楽しかったですね。意外と描きやすかったのは次元でした。いちばん僕の絵に近いと思います。Lupinは難しかったですね。僕が描くと小悪党になってしまい、ちょっと悩みました。このイラストを公開するときは『今回のアニメの内容とは関係ありません』って解説がないと誤解を招くよね。作中にこういうシーンはないですから（笑）」とコメント。また同誌には「Lupin三世VSCat's Eye」特集として、巻頭4ページにわたる北条のロングインタビュー、作品紹介が掲載される。  
关于插画，北条说:“一边思考故事一边画很开心。意外容易画的是次元。我觉得最接近我的画。Lupin很难。我一画就变成了小坏蛋，有点烦恼。公开这张插图的时候如果没有‘与这次的动画内容无关’的解说会引起误解吧。因为作品中没有这样的场景(笑)”。另外，该杂志作为“Lupin三世VS Cat's Eye”特集，在卷头刊登了长达4页的北条的长篇采访和作品介绍。  

「Lupin三世VS Cat's Eye」は、「Lupin三世」アニメ化50周年と「Cat's Eye」の原作40周年を記念して展開されるもの。「Cat's Eye」連載当時の1980年代を舞台に、泥棒と怪盗のレトロでスタイリッシュなクライム・アクションが繰り広げられる。  
《Lupin三世VS Cat's Eye》是为纪念《Lupin三世》动画化50周年和《Cat's Eye》原作40周年而展开的。故事以《Cat's Eye》连载当时的1980年代为舞台，讲述了小偷和怪盗之间复古又时尚的犯罪动作故事。  

この記事の画像・動画（全47件）  
该报道的图片、视频(共47件)  

[![Amazon Original「Lupin三世 VS Cat's Eye」Japan Premiereの告知画像。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1220/japan_p.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1963798 "Amazon Original「Lupin三世 VS Cat's Eye」Japan Premiereの告知画像。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「週刊文春エンタ+ 80年代！少年マンガの熱狂」のCoverイラスト。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1220/bunshun_plus_cover.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1963799 "「週刊文春エンタ+ 80年代！少年マンガの熱狂」のCoverイラスト。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「中綴じ付録ポスター」のイラスト。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1220/bunshun_plus_poster.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1963800 "「中綴じ付録ポスター」のイラスト。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」キービジュアル (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/keyvisual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953846 "「Lupin三世 VS Cat's Eye」キービジュアル (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」メインカット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953845 "「Lupin三世 VS Cat's Eye」メインカット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953847 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953848 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953849 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953850 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953851 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953852 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_013.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953853 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_014.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953854 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_010.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953855 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_011.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953864 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_012.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953866 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953867 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_009.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953868 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![「Lupin三世 VS Cat's Eye」第2弾PVより。](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_pv_007.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953869 "「Lupin三世 VS Cat's Eye」第2弾PVより。")
[![Lupin三世（CV：栗田貫一）。天才的泥棒の家系に生を受け、世界中をまたにかける大泥棒。どんなターゲットも奇想天外なアイディアと大胆不敵なテクニックで手に入れる。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953840 "Lupin三世（CV：栗田貫一）。天才的泥棒の家系に生を受け、世界中をまたにかける大泥棒。どんなターゲットも奇想天外なアイディアと大胆不敵なテクニックで手に入れる。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![次元大介（CV：大塚明夫）。早撃ち0.3秒の天才ガンマン。その卓越したガンさばきは、Lupin三世も一目置く。窮地に陥っても常に冷静に状況を把握して、難局を打破する男。酒と煙草をこよなく愛する。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953837 "次元大介（CV：大塚明夫）。早撃ち0.3秒の天才ガンマン。その卓越したガンさばきは、Lupin三世も一目置く。窮地に陥っても常に冷静に状況を把握して、難局を打破する男。酒と煙草をこよなく愛する。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")


[![石川五ェ門（CV：浪川大輔）。約400年前に大怪盗を輩出した石川家の末裔。斬鉄剣を自在に操る居合の達人であり、剣の道を究めるためにひたすら修行の道を歩む誇り高き剣豪。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953835 "石川五ェ門（CV：浪川大輔）。約400年前に大怪盗を輩出した石川家の末裔。斬鉄剣を自在に操る居合の達人であり、剣の道を究めるためにひたすら修行の道を歩む誇り高き剣豪。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![峰不二子（CV：沢城みゆき）。さまざまな顔を持つ謎の美女。類まれなる美貌と抜群のプロポーションを持ちながら、Lupinさえも手玉にとる天才的頭脳をあわせ持つ。行動は単純で、己の欲望に従っているだけ。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_007.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953836 "峰不二子（CV：沢城みゆき）。さまざまな顔を持つ謎の美女。類まれなる美貌と抜群のプロポーションを持ちながら、Lupinさえも手玉にとる天才的頭脳をあわせ持つ。行動は単純で、己の欲望に従っているだけ。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![銭形警部（CV：山寺宏一）。ICPO（インターポール）のLupin三世専任捜査官であり、江戸時代の有名な岡っ引きの子孫。Lupin逮捕に執念を燃やし、Lupinの事なら誰よりも熟知している。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953834 "銭形警部（CV：山寺宏一）。ICPO（インターポール）のLupin三世専任捜査官であり、江戸時代の有名な岡っ引きの子孫。Lupin逮捕に執念を燃やし、Lupinの事なら誰よりも熟知している。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![来生瞳（CV：戸田恵子）。キャッツアイ三姉妹の次女。爽やかにロングヘアーをなびかせる美人。抜群の身体能力を活かし、華麗に盗みを遂行する。刑事の俊夫とは学生の時から恋人関係にある。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_009.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953832 "来生瞳（CV：戸田恵子）。キャッツアイ三姉妹の次女。爽やかにロングヘアーをなびかせる美人。抜群の身体能力を活かし、華麗に盗みを遂行する。刑事の俊夫とは学生の時から恋人関係にある。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![来生泪（CV：深見梨加）。キャッツアイ三姉妹の長女。大人の色気を漂わせる、グラマラスな美女。その明晰な頭脳を活かし、作戦の司令塔を担う。優れた変装技術も持っている。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_010.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953833 "来生泪（CV：深見梨加）。キャッツアイ三姉妹の長女。大人の色気を漂わせる、グラマラスな美女。その明晰な頭脳を活かし、作戦の司令塔を担う。優れた変装技術も持っている。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![来生愛（CV：坂本千夏）。キャッツアイ三姉妹の三女。元気いっぱい、ボーイッシュな女の子。メカやITに強く、自作ギミックで盗みを成功に導く。三姉妹の中で唯一、父親との思い出がない。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_011.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953843 "来生愛（CV：坂本千夏）。キャッツアイ三姉妹の三女。元気いっぱい、ボーイッシュな女の子。メカやITに強く、自作ギミックで盗みを成功に導く。三姉妹の中で唯一、父親との思い出がない。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![内海俊夫（CV：安原義人）。瞳の恋人で、キャッツアイ事件を担当する犬鳴署の刑事。お調子者だが正義感は強く、キャッツアイ逮捕に執念を燃やす熱血漢。銭形に憧れを抱いている。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_012.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953842 "内海俊夫（CV：安原義人）。瞳の恋人で、キャッツアイ事件を担当する犬鳴署の刑事。お調子者だが正義感は強く、キャッツアイ逮捕に執念を燃やす熱血漢。銭形に憧れを抱いている。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![永石（CV：麦人）。ハインツから恩義を受け、三姉妹を陰から支える紳士。キャッツアイの盗みのサポートや、ハインツコレクションの管理などを行っている。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953841 "永石（CV：麦人）。ハインツから恩義を受け、三姉妹を陰から支える紳士。キャッツアイの盗みのサポートや、ハインツコレクションの管理などを行っている。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![ハインリッヒ・ベルガー。喫茶キャッツアイを訪れる初老の画商。キャッツ三姉妹に絵画「花束と少女」にまつわる依頼を持ち込む。失踪前のハインツを知っている。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953838 "ハインリッヒ・ベルガー。喫茶キャッツアイを訪れる初老の画商。キャッツ三姉妹に絵画「花束と少女」にまつわる依頼を持ち込む。失踪前のハインツを知っている。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![デニス・キルヒマン。裏世界を暗躍する武器商「ファーデン」の幹部。元傭兵で、目的のためなら手段を択ばない冷酷な男。Lupinやキャッツアイの前に強敵として立ちはだかる。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953839 "デニス・キルヒマン。裏世界を暗躍する武器商「ファーデン」の幹部。元傭兵で、目的のためなら手段を択ばない冷酷な男。Lupinやキャッツアイの前に強敵として立ちはだかる。(c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![杏里／ANRI](https://ogre.natalie.mu/media/news/comic/2022/1206/Anri_1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1953844 "杏里／ANRI")
[![fox capture plan](https://ogre.natalie.mu/media/news/music/2021/0722/foxcaptureplan_art202107.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1633522 "fox capture plan")
[![「Lupin三世 VS Cat's Eye」キービジュアル (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/teaser_visual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905449 "「Lupin三世 VS Cat's Eye」キービジュアル (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/01c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905460 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット](https://ogre.natalie.mu/media/news/comic/2022/0922/02a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905459 "「Lupin三世 VS Cat's Eye」場面カット")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/03a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905458 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/04c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905457 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/05b.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905456 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/06c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905455 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/07c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905454 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/08c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905453 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/09c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905452 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/10a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905451 "「Lupin三世 VS Cat's Eye」場面カット (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」ロゴ (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/lvc_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/505882/1905450 "「Lupin三世 VS Cat's Eye」ロゴ (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」第2弾PV](https://i.ytimg.com/vi/zZAsIu9oBIo/default.jpg)](https://natalie.mu/comic/gallery/news/505882/media/84700 "「Lupin三世 VS Cat's Eye」第2弾PV")
[![「Lupin三世 VS Cat's Eye」第1弾PV](https://i.ytimg.com/vi/6s5QLmAc7gM/default.jpg)](https://natalie.mu/comic/gallery/news/505882/media/81880 "「Lupin三世 VS Cat's Eye」第1弾PV")

## Amazon Original「Lupin三世 VS Cat's Eye」Japan Premiere

日時：2023年1月24日（火）17:45 開場 / 18:15 開演  
場所：東京都 TOHO Cinemas六本木　  
登壇者：[栗田貫一](https://natalie.mu/comic/artist/6789)、[戸田恵子](https://natalie.mu/comic/artist/16682)、[静野孔文](https://natalie.mu/comic/artist/62364)監督、[瀬下寛之](https://natalie.mu/comic/artist/92064)監督

## Amazon Original「Lupin三世 VS Cat's Eye」

2023年1月27日（金）よりPrime Videoにて世界独占配信  
2023年1月27日(周五)在Prime Video上全球独家发布  

### Staff

原作：[Monkey Punch](https://natalie.mu/comic/artist/1749)『Lupin三世』/[北条司](https://natalie.mu/comic/artist/2405)『Cat's Eye』  
監督：静野孔文、瀬下寛之  
脚本：葛原秀治  
副監督：井手惠介  
Character design：[中田春彌](https://natalie.mu/comic/artist/11043)、山中純子  
Production Design：田中直哉、フェルディナンド・パトゥリ  
Art Director：片塰満則  
編集：肥田文  
音響監督：清水洋史  
音楽：[大野雄二](https://natalie.mu/comic/artist/105386)、大谷和夫、[fox capture plan](https://natalie.mu/comic/artist/10729)  
Animation制作：TMS Entertainment  
Animation制作協力：萌  
製作：Lupin三世 VS Cat's Eye製作委員会

### キャスト

Lupin三世：栗田貫一  
次元大介：[大塚明夫](https://natalie.mu/comic/artist/26700)  
石川五ェ門：[浪川大輔](https://natalie.mu/comic/artist/45738)  
峰不二子：[沢城みゆき](https://natalie.mu/comic/artist/65954)  
銭形警部：[山寺宏一](https://natalie.mu/comic/artist/30965)  
来生瞳：戸田恵子  
来生泪：[深見梨加](https://natalie.mu/comic/artist/96805)  
来生愛：[坂本千夏](https://natalie.mu/comic/artist/95232)  
内海俊夫：[安原義人](https://natalie.mu/comic/artist/91212)  
永石：[麦人](https://natalie.mu/comic/artist/95976)  
[銀河万丈](https://natalie.mu/comic/artist/95632)  
[東地宏樹](https://natalie.mu/comic/artist/96008)  
[菅生隆之](https://natalie.mu/comic/artist/96023)