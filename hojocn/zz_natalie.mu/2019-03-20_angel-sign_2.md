https://natalie.mu/eiga/news/324499

# 北条司が実写監督デビュー！“愛のものがたり”描く「エンジェルサイン」製作決定

2019年3月20日 [Comment](https://natalie.mu/eiga/news/324499/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


マンガ家・[北条司](https://natalie.mu/eiga/artist/2405)が総監督を務める実写映画「[エンジェルサイン](https://natalie.mu/eiga/film/182396)」の製作が発表された。  
由漫画家北条司担任总导演的真人电影《天使印记》宣布制作。

[
![「エンジェルサイン」ロゴ](https://ogre.natalie.mu/media/news/eiga/2019/0319/angelsign_201903_01.jpg?impolicy=hq&imwidth=730&imdensity=1)
「エンジェルサイン」ロゴ  
大きなサイズで見る（全3件）  
“天使印记”Logo  
大尺寸看(共3件)
](https://natalie.mu/eiga/gallery/news/324499/1128033)

[
![「エンジェルサイン」撮影現場の北条司。](https://ogre.natalie.mu/media/news/eiga/2019/0319/angelsign_201903_02.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」撮影現場の北条司。［拡大］  
“天使印记”拍摄现场的北条司。[扩大]  
](https://natalie.mu/eiga/gallery/news/324499/1128034)

北条が審査員を務める「サイレントマンガオーディション」に寄せられたストーリー数編に、自身のオリジナルストーリーを加え、大きな1つの“愛のものがたり”を描いた本作。編集者として、北条とともに「キャッツ・アイ」「シティーハンター」といった作品を生み出した堀江信彦が企画を担当している。今回、堀江の思いに共感し実写映画の総監督としてデビューすることになった北条は「マンガを描くのとは違い、映画制作はとても新鮮です。 気を引き締め、粛粛と臨んでいきたいと思っています」とコメントしている。  
北条担任评委的“默白漫画大赛”收到的数篇故事，加上自己的原创故事，描绘了一个大的“爱的故事”。作为编辑、与北条一起创作了《猫眼》《城市猎人》等作品的堀江信彦担任企划。此次，对堀江的想法产生共鸣的北条表示“与画漫画不同，电影制作非常新鲜。我想振作、严肃地面对这部电影”。  

「エンジェルサイン」のキャストや公開時期は続報を待とう。なお北条原作の長編アニメ「劇場版シティーハンター <新宿プライベート・アイズ>」は全国で公開中。  
《天使印记》的演员阵容和公开时间等后续报道吧。另外，北条原作的长篇动画《剧场版城市猎人<新宿Private Eyes>》正在全国上映中。


この記事の画像（全3件）  
这篇报道的图片(共3篇)  

[![「エンジェルサイン」ロゴ](https://ogre.natalie.mu/media/news/eiga/2019/0319/angelsign_201903_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/324499/1128033 "「エンジェルサイン」ロゴ")
[![「エンジェルサイン」撮影現場の北条司。](https://ogre.natalie.mu/media/news/eiga/2019/0319/angelsign_201903_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/324499/1128034 "「エンジェルサイン」撮影現場の北条司。")
[![「シティーハンター」イラスト (c)北条司／NSP 1985](https://ogre.natalie.mu/media/news/eiga/2019/0319/angelsign_201903_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/324499/1128035 "「シティーハンター」イラスト (c)北条司／NSP 1985")


LINK

[「エンジェルサイン」公式サイト](https://angelsign.jp/)  
[「劇場版シティーハンター <新宿プライベート・アイズ>」公式サイト](https://cityhunter-movie.com/)  