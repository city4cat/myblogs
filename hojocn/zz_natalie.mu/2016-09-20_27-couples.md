https://natalie.mu/comic/news/202517

# エンタミクスで「名作漫画の伝説カップル」27組の出会い振り返る特集
Entermix“名作漫画中的传说情侣”27对相遇回顾特集  

2016年9月20日  [Comment](https://natalie.mu/comic/news/202517/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


「名作漫画の伝説カップル はじまりの瞬間」と題したマンガ特集が、本日9月20日発売のエンタミクス11月号（KADOKAWA）にて組まれた。  
以“名作漫画的传说情侣开始的瞬间”为题的漫画特集，在今天9月20日发售的Entermix 11月号(KADOKAWA)上刊登。

[
![エンタミクス11月号](https://ogre.natalie.mu/media/news/comic/2016/0920/em1611.jpg?imwidth=468&imdensity=1)
エンタミクス11月号  
大きなサイズで見る  
Entermix 11月号  
查看大图  
](https://natalie.mu/comic/gallery/news/202517/583743)

マンガ好きなら誰もが知る有名作品のカップルにスポットを当て、その出会いを紹介する本企画。[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」の冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）と槇村香、[日渡早紀](https://natalie.mu/comic/artist/2341)「ぼくの地球を守って」の紫苑と木蓮、[まつもと泉](https://natalie.mu/comic/artist/2921)「きまぐれオレンジ☆ロード」の春日恭介と鮎川まどかなど、記事には27組の男女が登場している。  
本企划聚焦漫画爱好者都知道的著名作品中的情侣，介绍他们的相遇。北条司《城市猎人》中的冴羽獠(獠的汉字是“僚”字的写法)和槙村香、日渡早纪《守护我的地球》中的紫苑和木莲、松本泉《きまぐれOrange☆Road》中的春日恭介和鲇川圆等。报道中出现了27对男女。

そのほかエンタミクス11月号ではメイン特集として、尖った企画を紹介したり、その仕掛け人にプロジェクトの成り立ちを聞いたりする「無茶しやがって！飛ばしすぎプロジェクト一斉検挙」を展開。同コーナーでは、週刊少年ジャンプ（集英社）の副編集長にアプリ・少年ジャンプ＋の展望を聞いている。  
除此之外，Entermix 11月号还将以主要特辑的形式，介绍优秀的企划，并向策划人员询问项目的来龙去脉，展开“荒唐无理!Jump的项目一齐检举”。在该栏目中，周刊少年Jump(集英社)的副主编询问了App·少年Jump +的展望。


LINK

[Entermix](http://www.entermix.jp/)

関連商品

[
![Entermix 2016年11月号](https://images-fe.ssl-images-amazon.com/images/I/61ANa1BtRCL._SS70_.jpg)
Entermix 2016年11月号  
[雑誌] 2016年9月20日発売 / KADOKAWA / 491-0022631167  
Amazon.co.jp [書籍版 / Kindle版]  
](https://www.amazon.co.jp/exec/obidos/ASIN/B01LEPKJ28/nataliecomic-22)