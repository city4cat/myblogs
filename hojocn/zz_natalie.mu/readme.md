source:  
https://natalie.mu/comic/artist/2405  

# 北条司

ホウジョウツカサ  
Hojo Tsukasa  

- [Profile](#profile)  
- [画像](#picture)  
- [特集記事](#pp)  
- [News](#news)  
- [関連商品](#item)  
- [映画作品](#film)  
- [こちらもおすすめ](#recommend_artist)（相关艺术家推荐）  

<a name="profile"></a>  
## 1. Profile  
1959年、福岡県生まれ。1980年、週刊少年ジャンプ（集英社）にて読み切り「おれは男だ！」でデビュー。代表作に3人組の女怪盗を描いた「キャッツ♥アイ」、新宿で生きるスイーパー・冴羽獠を主役にした「シティーハンター」など。2001年よりコミックバンチ（新潮社）にて「シティーハンター」の世界をベースにしたパラレルワールド作品「エンジェル・ハート」をスタートさせ、同作は2010年に月刊コミックゼノン（徳間書店）に移籍したのを機に「エンジェル・ハート 2ndシーズン」へと改題、2017年に同誌にて完結した。   
1959年出生于福冈县。1980年，在周刊少年Jump(集英社)上短篇「我是男人啊!」中出道。代表作有描写3人组女怪盗的「Cat's♥Eyes」、以在新宿生活的Sweeper冴羽獠为主角的「City Hunter」等。2001年开始在Comic Bunch(新潮社)上发表以「City Hunter」的世界为基础的平行世界作品「Angel Heart」，该作品于2010年移至月刊Comic Zennon(德间书店)以此为契机更名为「Angel Heart 2ndSeason」，并于2017年在该杂志上完结。  

<a name="picture"></a>  
## 2. [北条司の画像](./gallary.md)  

<a name="pp"></a>  
## 3. 北条司の特集・Interview

- 2024-05-14 [日本首次真人版！Netflix电影《城市猎人》冴羽獠獠穿越“开始的故事”](./2024-05-14_interview_collections.md)  
- 2024-05-14 [Netflix电影“城市猎人”特集|完全不知道原作的ヒコロヒー看了之后，选角、演出、肌肉，全都很喜欢](./2024-05-14_interview_netflix-ch_review-from-nonfans.md)  
- 2024-05-02 [就 Netflix 电影《城市猎人》 TM NETWORK 接受采访 | 最新 “Continual”《Get Wild》](./2024-05-02_interview_netflix-ch_tmnetwork.md)  
- 2024-04-26 [铃木良平×森田望智×神谷明×伊倉一恵齐聚一堂！讨论 Netflix 电影「CityHunter」](./2023-04-26_interview_netflix-cityhunter01.md)   
- 2023-01-17 [「Lupin三世VSCat‘s Eye」日本最有名的小偷和怪盗三姐妹强力合作！欣赏后留下“和往常一样，但有点特别”的余韵](./2023-01-17_lupin-vs-catseye.md)  
- 2019-10-25 [「劇場版City Hunter <新宿Private Eyes>」特集 神谷明Interview](./2019-10-25_City Hunter-movie03.md)  
- 2019-02-07 [「剧场版City Hunter <新宿Private Eyes>」玄田哲章Interview](./2019-02-07_City Hunter-movie02.md)  
- 2019-02-01 [「劇場版City Hunter <新宿Private Eyes>」特集 北条司（原作者）×神谷明（冴羽獠役）Interview](./2019-02-01_ch_movie01.md)  
- 2019-01-18 [ComicTatan特集 精选采访&海坊主(City Hunter)致敬插画展览](./2019-01-18_comictatan.md)     
- 2017-10-10 [「纸牌ジャン100」](./2017-10-10_cartajump.md)    
- 2015-12-21 [「City Hunter XYZ Edition」神风动画Interview & 北条司×小室哲哉对谈](./2015-12-21_xyz-edition.md)  

<a name="news"></a>  
## 4. News

### 2024  
- 2024-11-21 [北条司《Cat's♥Eye》新作动画化！2025年Disney+，主题曲是Ado](./2024-11-21.md)  
    - 主题曲为Ado演唱的《CAT'S EYE》、北条司的《Cat's♥Eye》完全动画化  
    - 柳楽優弥对速度加快的新片《食人魔》充满信心。笠松将“作为一个团队克服了困难”  
- 2024-10-30 [北条司《Cat's♥Eye》今日限定全话免费阅读](./2024-10-30.md)  
- 2024-09-07 [Netflix电影“城市猎人”将在剧院上映两周，所有观众将收到明信片](./2024-09-07.md)  
    - 铃木亮平主演Netflix电影《城市猎人》2周限定剧场上映  
- 2024-08-17 [《城市猎人》今天在LINE漫画中全免费，包括"我不喜欢那个笑容"](./2024-08-17.md)  
- 2024-07-24 [“剧场版城市猎人”最新作品在DMM TV上开始独家配信，前作也成为无限观看对象](./2024-07-24.md)
- 2024-05-21 [“80年代动画歌曲总选举最佳100”发售，刊登小室哲哉等人的采访](./2024-05-21.md)  
- 2024-05-08 [现在在“城市猎人”的世界里…铃木亮平和森田望智共演电影回顾视频公开](./2024-05-08.md)  
- 2024-05-02 [灵魂的力量是迷人的，森田望智讲述“城市猎人”香的新场景照片也到了](./2024-05-02.md)  
- 2024-05-01 [Netflix电影《城市猎人》铃木亮平×森田望智的“搭档镜头”公开](./2024-05-01.md)  
- 2024-04-29 [Netflix 电影《城市猎人》布景和物品的详细照片来袭，小细节曝光](./2024-04-29.md)  
- 2024-04-23 [「城市猎人」铃木亮平：”一生的回忆 “。小室哲哉也赶赴新宿 ”圣地"](./2024-04-23.md)  
- 2024-04-17 [「北条司展」今天开始，展出《猫眼》、《城市猎人》的原画](./2024-04-17.md)  
- 2024-04-15 [电影“城市猎人”场景截图槙村庆祝香的生日，随后发生悲剧](./2024-04-15.md)  
    - Netflix 电影《城市猎人》发布了新照片，其中包括獠和槙村的 “搭档Cut”    
- 2024-04-08 [《城市猎人》公布预告片和其他演员；北条司："这是一种让人无法入睡的乐趣"](./2024-04-08.md)  
    - 铃木亮平主演《城市猎人》新预告，原作者北条司“有趣得睡不着”  
    - 今天是「Get Wild日」！电影《城市猎人》预告片发布，TM NETWORK 电台直播特别节目也已确定  
- 2024-04-07 [《城市猎人》共336话可在Zenon编集部免费获取，其中包括北条司和铃木良平的讨论](./2024-04-07_ch-free.md)  
- 2024-03-31 [电影“城市猎人”新场景照片，冴羽獠在胯下长出马头](./2024-03-31_netflix-ch.md)     
    - 牛仔帽的獠大暴走！铃木亮平主演《城市猎人》新剧照  
- 2024-03-18 [Coamix Manga Art Gallery Open，第一次是北条司展](./2024-03-18_hojo-exhibition.md)  
- 2024-03-15 [电影《城市猎人》片尾曲《Get Wild Continual》](./2024-03-15_netflix-ch-ed_get-wild-continual.md)  
    - 铃木亮平主演《城市猎人》预告公开，《Get Wild》重获新生  
- 2024-02-28 [《城市猎人》冴羽獠的城市流行T恤、唱片风杯垫](./2024-02-28_ch.md)  
- 2024-02-24 [铃木亮平主演Netflix《城市猎人》安藤政信饰演槙村秀幸木村文乃登场](./2024-02-24_ch-2.md)  
- 2024-02-24 [电影《城市猎人》由安藤政信饰演香的哥哥槙村秀幸，冴子由木村文乃饰演](./2024-02-24_ch.md)  
- 2024-02-20 [【2月20日付】本日発売の単行本リスト](./2024-02-20.md)  
- 2024-02-09 [Netflix电影《城市猎人》4月25日发布，部分正片影像和新视觉解禁](./2024-02-09_ch.md)  

### 2023  

- 2023-12-16 [舞台剧《Mage the Cat's Eye》7名主要演员集合的视觉发布会](./2023-12-16_ce-2.md)  
- 2023-12-16 [明治时代怪盗三姐妹活跃，舞台剧《Mage the Cat's Eye》出演者7人悉数登场](./2023-12-16_ce.md)  
- 2023-11-24 [没有什么不可能的。舞台剧《Mage the Cat's Eye》三姐妹的视觉解禁](./2023-11-24_ce.md)  
- 2023-11-14 [舞台剧《Mage the Cat's Eye》河原雅彦、七海弘木、船越英一郎等人参加的后续活动](./2023-11-14_ce.md)  
- 2023-10-26 [舞台剧《Mage the Cat's Eye》追加卡司:美弥留香、染谷俊之、上山龙治、长谷川初范](./2023-10-26_ce.md)  
- 2023-10-07 [《城市猎人》小室哲哉担任音乐解说，从事音乐行业50年的总监则对某首歌曲有了第一次体验](./2023-10-07_ch.md)  
- 2023-09-29 [藤原纪香、高岛礼子、刚力彩芽成为三姐妹!舞台剧《mage the猫眼》时代是明治时期](./2023-09-29_ce-2.md)  
- 2023-09-29 [《Cat's▼Eye》舞台剧化!藤原纪香、刚力彩芽、高岛礼子主演，时代设定为明治时代](./2023-09-29_ce.md)  
- 2023-09-09 [在《城市猎人》圣地新宿举行舞台致辞，泽城美雪“创造了新的历史”](./2023-09-09_ch-2.md)  
- 2023-09-09 [采用《城市猎人》神谷明方案的台词，泽城美雪拼命避免剧透](./2023-09-09_ch.md)  
- 2023-09-04 [《剧场版城市猎人》场景剪辑："邋遢鬼"与美女在一起，但在工作时，他却很犀利。](./2023-09-04_ch.md)  
- 2023-08-30 [《剧场版城市猎人》中鲁邦、次元、小玉登场!前作的地面波首播](./2023-08-30.md)  
- 2023-08-29 [“剧场版城市猎人”公开纪念!JR新宿站“XYZ”熟悉的告示板登场](./2023-08-29.md)  
- 2023-08-26 [【8月19日付】本日発売の単行本リスト](./2023-08-26.md)  
- 2023-08-23 [《剧场版城市猎人》北条司亲手绘制的彩纸将于9月9日登台致辞](./2023-08-23.md)  
- 2023-08-19 [【8月19日付】本日発売の単行本リスト](./2023-08-19.md)  
- 2023-08-08 [《城市猎人》獠在新宿的大使剧场版试映会神谷明等集结](./2023-08-08_ch-3.md)  
- 2023-08-08 [在《城市猎人》神谷明之前，木村昴发表了“敷衍”的发言，关智一立刻提醒](./2023-08-08_ch-2.md)  
- 2023-08-08 [网罗动画《城市猎人》!全史钢琴发售，刊载神谷明与伊仓一惠的对谈](./2023-08-08_ch.md)  
- 2023-08-08 [“城市猎人”冴羽獠成为新宿观光大使，神谷明“非常高兴”](./2023-08-08_ch_ryo.md)  
- 2023-08-07 [《剧场版城市猎人》×天下一品，“NO MOKKORI,NO LIFE”的盖浇饭](./2023-08-07_ch.md)  
- 2023-08-05 [山岸凉子的恐怖解开特辑 DaVinci，北川翔 & 松田奈绪子投稿](./2023-08-05_davinci.md)  
- 2023-08-04 [FANTASTICS·世界将客串《剧场版城市猎人》直接谈判](./2023-08-04_ch-2.md)  
- 2023-08-04 [剧场版城市猎人FANTASTICS世界直接谈判出演“没想到真的能实现”](./2023-08-04_ch.md)  
- 2023-08-03 [山里出演《剧场版城市猎人》，听了几百遍《Get Wild》来调整心情](./2023-08-03_ch-3.md)  
- 2023-08-03 [山里亮太将在《剧场版城市猎人 天使泪（Angel Dust）》中配音“喜欢搭讪的男人”](./2023-08-03_ch-2.md)  
- 2023-08-03 [喜欢《城市猎人》的山里亮太客串出演剧场版《Get Wild》制造心情](./2023-08-03_ch.md)  
- 2023-08-01 [《剧场版城市猎人》獠与香，TM NETWORK共同踏上黎明的新宿](./2023-08-01_ch.md)  
- 2023-07-04 [《城市猎人》的名场景以拉花的形式，出现在Zenon的Cafe & Sakaba](./2023-07-04_ch_cafe.md)  
- 2023-06-30 [《城市猎人》TV动画系列严选话放送剧场版公开纪念](./2023-06-30_ch_ex.md)  
- 2023-06-29 [动画《城市猎人》系列特别展召开，展示Cel画和分镜等](./2023-06-29_ch_ex.md)  
- 2023-06-22 [动漫《城市猎人》系列Cel画等展示活动将于7月在东京举行](./2023-06-22_ch_ex.md)  
“动画版《城市猎人》系列的世界”展  
- 2023-06-13 [神谷明评价《剧场版城市猎人》的展开“就像坐过山车一样”](./2023-06-13_ch-4.md)  
- 2023-06-13 [小室哲哉《城市猎人》OP主题自信不输《Get Wild》](./2023-06-13_ch-3.md)  
- 2023-06-13 [剧场版城市猎人》公开日，新角色、预告片、OP主题等一举解禁](./2023-06-13_ch-2.md)  
- 2023-06-13 [《剧场版城市猎人》将于9月8日上映，新出演者泽城美雪和关智一，OP也发表](./2023-06-13_ch.md)  
- 2023-05-26 [纪念剧场版城市猎人“Get Wild”ED采用的TM新写真，由Sunrise制作](./2023-05-26_get-wild.md)  
- 2023-04-08 [TM NETWORK的代表曲是日本音乐史上第一个纪念日，今天4月8日是「Get Wild日」](./2023-04-08_get-wild.md)  
- 2023-02-22 [Get wild and tough!「劇場版City Hunter」特报 2023t Hammer炸裂 ](./2023-02-22_get-wild-and-tough.md)  
- 2023-02-22 [「劇場版City Hunter」新片"AngelDust"、香在特别预告片中挥舞着2023t锤子](./2023-02-22_angeldust.md)  
- 2023-02-20 [法国版「City Hunter」正式上映!带手鼓、团扇OK](./2023-02-20_france_ch_ok.md)  
- 2023-02-20 [法国真人版「City Hunter」在冴羽獠的生日于新宿"Mokkori应援上映"](./2023-02-20_france_ch.md)  
- 2023-02-20 [今天发售的单行本列表](./2023-02-20_volume.md)  
- 2023-01-31 [铃木亮平主演「City Hunter」森田望智饰演搭档槙村香](./2023-01-31_ch_actress.md)  
- 2023-01-31 [电影「City Hunter」香由森田望智饰演，留言板前的笑容也将公开](./2023-01-31_ch_actress_board.md)  
- 2023-01-24 [栗田貫一说「鲁邦三世VS Cat's Eye」的"奇妙"情景是"精明!"的](./2023-01-24_lupin-vs-catseye.md)  
- 2023-01-24 [Lupin的下一个合作是面包超人?栗田贯一在户田惠子面前宣布](./2023-01-24_lupin-anpanman.md)  
- 2023-01-12 [「Lupin三世VS Cat's Eye」栗田贯一、户田惠子等11人发表评论](./2023-01-12_lupin-vs-catseye.md)  

### 2022
- 2022-12-22 [在文春娱乐中，有一个追溯80年代少年Manga的狂热特集，主角是北条司、ゆでたまご、稲垣理一郎](./2022-12-22_Bunshun.md)  
- 2022-12-21 [「Lupin三世VS Cat's Eye」举办日本首映礼，文春Mook封面Jack](./2022-12-21_Bunshun-mook.md)  
- 2022-12-15 [「City Hunter」日本首次真人电影化，由铃木亮平主演](./2022-12-21_ch_live-action_film_01.md)  
- 2022-12-15 [「City Hunter」决定在日本拍真人电影！冴羽獠由铃木亮平饰演，导演为佐藤祐市。](./2022-12-21_ch_live-action_film_00.md)  
- 2022-12-06 [「Lupin三世VS Cat's Eye」1月开始发布！出演者&PV公开，来生泪角色是深见梨加](./2022-12-06_lupin-vs-catseye.md)  
- 2022-12-02 [「剧场版City Hunter」新作将于2023年上映，北条司亲自绘制视觉图](./2022-12-02_ch_movie_film.md)  
- 2022-12-02 [「剧场版City Hunter」明年上映！连任演员阵容，北条司新视觉描绘](./2022-12-02_ch_movie_comic.md)  
- 2022-11-03 [岩手县的POP UP STORE专注于「City Hunter」Mick、香、獠](./2022-11-03_mick_kaori_ryo.md)  
- 2022-09-22 [「Lupin三世」×「Cat's Eye」合作动画决定!Toms负责制作](./2022-09-22_lupin-vs-catseye.md)  
- 2022-09-22 [Lupin三世VS Cat's Eye，动画电影将于2023年在世界发布](./2022-09-22_lupin-vs-catseye_film.md)  
- 2022-08-04 [「Cat's♥Eye」DVD BOOK全4卷9月开始发行，收录珍贵资料的小册子附送](./2022-08-04_catseye_dvd-book.md)  
- 2022-07-08 [从「City Hunter」名场景到「下班」，集齐40种LINE贴图](./2022-07-08_ch_line.md)  
- 2022-05-12 [「Cat's♥Eye」原画展明天开始!北条司久违的三姐妹，服装差异值得关注](./2022-05-12_ce-40th.md)  
- 2022-05-03 [法国版「City Hunter」日语配音版将于5月7日在BS12播出](./2022-05-03_ch.md)  
- 2022-04-27 [「Cat's♥Eye」原画展，会场限定展示重绘的「Space Angel」](./2022-04-27_ce-40th.md)  
- 2022-04-08 [「Get Wild」发布35年后，「剧场版City Hunter」新作制作决定](./2022-04-08_ch.md)  
- 2022-04-08 [「City Hunter」新剧场版决定制作！神谷明继续担任，ED是「Get Wild」](./2022-04-08_ch_comic.md)  
- 2022-04-01 [「Cat's▼Eye」40周年纪念首次举办原画展，北条司绘制的三姐妹也将登场](./2022-04-01_ce-40th.md)  
- 2022-02-10 [昭和50年男性向杂志少年漫画特集，藤田和日郎绘制“牛虎”](./2022-02-10_50years.md)  
    - [昭和50年男Vol.015](./zz_s50otoko_vol15.md)  
- 2022-01-18 [「Cat's♥Eye」连载40周年纪念的香水套装，用香味表现可爱妩媚的3人](./2022-01-18_ce_perfume.md)  

### 2021  
- 2021-12-11 [「剧场版 City Hunter<新宿Private Eyes >」BS12播出](./2021-12-11_ce_perfume.md)  
- 2021-10-22 [青山刚昌、北条司、浅野忠信等知名人士共同庆祝动画片「鲁邦三世」50周年](./2021-10-22_lupin_50years.md)  
- 2021-10-01 [彩风咲奈的"冴羽獠"前往东京，“充满魅力”的新生雪组公演明天开始](./2021-10-01_ryo_tokyo.md)  
- 2021-09-30 [宝冢歌剧雪组「CITY HUNTER」东京公演千秋乐将在全国电影院实况转播](./2021-09-30_ryo_tokyo_live.md)  
- 2021-08-02 [宝冢歌剧雪组「City Hunter」宝冢大剧场千秋乐演唱会决定](./2021-08-02_ch_live.md)  
- 2021-07-15 [SHISHAMO・宫崎朝子为宝冢歌剧雪组「CITY HUNTER」作曲](./2021-07-15_shishamo.md)  
- 2021-07-15 [SHISHAMO宫崎朝子首次为宝冢歌剧团雪组提供乐曲!担任「CITY HUNTER」剧中歌曲](./2021-07-15_shishamo2.md)  
- 2021-05-26 [宝冢歌剧雪组明星助阵，宝冢纪念×「City Hunter」特设网站](./2021-05-26_gi_ch.md)  
- 2021-05-08 [宝冢雪组舞台剧「City Hunter」海报公开!凉、香、海坊主](./2021-05-08_poster.md)  
- 2021-05-08 [彩风咲奈和朝月希和主演的「CITY HUNTER」海报公开，海坊主也登场](./2021-05-08_poster2.md)  
- 2021-04-23 [佐藤拓也&堀江瞬再现“哆啦A梦”macaro烧，Toms官方YouTube](./2021-04-23_jarinko.md)  
- 2021-03-22 [「City Hunter」 獠和香的Birthday Event在Cafe Zenon & Sakaba举行](./2021-03-22_birthday_event_cafe_zenon.md)  

### 2020
- 2020-12-22 [宝冢雪组将北条司「City Hunter」舞台化!主演:彩风咲奈和朝月希和](./2020-12-22_ch.md)  
- 2020-12-22 [「City Hunter」宝冢歌剧团雪组将舞台化!主演是彩风咲奈和朝月希和](./2020-12-22_ch_comic.md)  
- 2020-12-14 [以「City Hunter」獠和香的名场景为图像的香水，用Acrylic Stand再现场景](./2020-12-14_ryo_kaori.md)  
- 2020-10-09 [芝诺创刊10周年举办1卷免费&半价大型展场，共计231部作品参加](./2020-10-09_zenon_10years.md)  
- 2020-08-12 [“MANGA都市TOKYO”开幕，展示eva等93个主题的500多件资料](./2020-08-12_manga-tokyo.md)  
- 2020-07-22 [8月在大阪举行的爆音电影节「逆袭的夏亚」「深渊制造」等](./2020-07-22_osaka_movie_festival.md)  
- 2020-05-01 [「鬼灭之刃」「SAO」「那朵花」「Fate」18部动画通话用背景一举发布](./2020-05-01_background.md)  
- 2020-03-29 [穿上喜欢的扉页画吧!City Hunter35周年的印花T恤tote企划](./2020-03-29_ch_35years_336t_project_tshirt.md)  
- 2020-03-24 [为庆祝獠的生日，「City Hunter」式样的威士忌3月26日发售](./2020-03-24_whisky.md)  
- 2020-02-27 [国立新美术馆举办追溯漫画、动画、游戏、特摄与“东京”关系的展览会](./2020-02-27_national_art_center_exhibition.md)  
- 2020-01-26 [爆音电影节in尼崎上映「City Hunter」法国真人版&日本动画版等](./2020-01-26_niqi.md)  
- 2020-01-16 [「City Hunter」自带菜单等将在咖啡店和酒馆登场，还有解谜企划](./2020-01-16_ch_menu_puzzle.md)  
- 2020-01-04 [早见优等三姐妹出演的电视剧「Cats Eye」播出，西城秀树饰演刑警](./2020-01-04_cats-eye_live-tv.md)  

### 2019  
- 2019-11-29 [City Hunter×Sony，配有双影的随身听和耳机](./2019-11-29_ch_sony.md)  
- 2019-11-29 [法国版「City Hunter」獠和香配合默契的动作特别影像](./2019-11-29_france_ch_ryo_kaori.md)  
- 2019-11-26 [山寺宏一与泽城美雪真人版再现TV动画「City Hunter」下回预告](./2019-11-26_ch_live.md)  
- 2019-11-26 [山寺宏一&泽城美雪再现动画「City Hunter」下回预告，法国版预告片](./2019-11-26_ch_live_comic.md)  
- 2019-11-16 [法国版「City Hunter」in漫展神谷明登场，漫画风预告影像也来了](./2019-11-16_tokyo-comic-con_ch-year.md)  
（其中称2019年为“City Hunter Year”）  
- 2019-11-16 [藤冈靛惊讶于北条司的分镜“冴羽獠来了吗!”](./2019-11-16_ch_seq.md)  
- 2019-11-15 [增肥、特别定制夹克…法国版「City Hunter」中的冴羽獠就是这样诞生的](./2019-11-15_franch_ch.md)  
- 2019-11-15 [肉×“Angel Sign”，松下奈绪和靛等出演的作品将享受家乡税优惠](./2019-11-15_angel-sign.md)  
- 2019-11-11 [松下奈绪×藤冈“Angel Sign”开头4分钟为GYAO！中公开](./2019-11-11_angel-sign.md)  
- 2019-11-07 [电影“Angel Sign”入场者特典附有北条司亲手绘制的分镜ClearFile](./2019-11-07_angel-sign_clearfile.md)  
- 2019-11-06 [法国版「C.H.」剧照，包括獠手持狙击枪](./2019-11-06_franch_ch_shot.md)  
- 2019-11-01 [「剧场版City Hunter」与SEIKO合作的手表，刻度上有XYZ字样](./2019-11-01_ch_seiko_watch.md)  
- 2019-10-31 [藤冈靛、松下奈绪笑着夸奖合作曲“太棒了!”](./2019-10-31_angel-sign.md)  
- 2019-10-31 [「City Hunter」在吉祥寺的街道上解谜&香和谜，也是北条司新画的](./2019-10-31_ch.md)  
    - [City Hunter × Real寻宝 遭袭的天使的Requiem | Real寻宝、解谜Takarush !](./zz_takarush_promo_City Hunter.md)
- 2019-10-30 [「剧场版City Hunter」獠&海坊主款式的太阳镜，从Zoff登场](./2019-10-30_ch_sunglass.md)  
- 2019-10-29 [「剧场版City Hunter」导演们透露北条司的要求，神谷明希望Part2](./2019-10-29_ch.md)  
- 2019-10-21 [北条司描绘的“Angel Sign”影像到达，决定举办活动](./2019-10-21_angel-sign.md)  
- 2019-10-20 [集结3人的獠和2人的香!神谷明谈法国版“C·H”：“深切地传达了热爱”](./2019-10-20_france_ch.md)  
- 2019-10-13 [法国版「City Hunter」捕捉了獠、香、海坊主的6个新镜头，还有100t锤子](./2019-10-13_france_ch.md)  
- 2019-10-11 [「剧场版City Hunter」影像大师版大银幕上映，神谷明也将登台](./2019-10-11_ch.md)  
- 2019-10-08 [法国版「City Hunter」正式预告!山寺宏一的獠与泽城美雪的香首次披露](./2019-10-08_france_ch.md)  
- 2019-10-08 [法国真人版「City Hunter」配音版预告解禁, 饰演冴羽獠的演员将访问日本](./2019-10-08_france_ch_2.md)  
- 2019-10-01 [「剧场版City Hunter」原画展开幕, 新插画公开](./2019-10-01_ch-movie_original-art-exh.md)  
- 2019-09-24 [北条司“Angel Sign”预告公开, 松下奈绪也参与DEAN FUJIOKA演唱主题曲](./2019-09-24_angel-sign.md)  
- 2019-09-19 [法版「City Hunter」山寺宏一演獠，泽城美雪演香!神谷和伊仓打包票](./2019-09-19_france_ch.md)  
- 2019-09-19 [山寺宏一和泽城美雪将为法版「City Hunter」配音，神谷明也打包票](./2019-09-19_france_ch_2.md)  
- 2019-09-06 [法国真人版「City Hunter」MVTK特典与原作的亮等人合作](./2019-09-06_france_ch.md)  
- 2019-09-06 [法国真人版「City Hunter」上映日期确定，附赠限定特典的「City Hunter」发售](./2019-09-06_france_ch_2.md)  
- 2019-08-21 [北条司「Angel Sign」上映日期确定，绪形直人、菊池桃子、佐藤二朗将出演新角色](./2019-08-21_angel-sign.md)  
- 2019-08-21 [北条司的真人电影「Angel Sign」新演员包括佐藤次郎和Mechatrowego](./2019-08-21_angel-sign_2.md)  
- 2019-08-19 [在圣地新宿举办“剧场版City Hunter”的预约活动等](./2019-08-19_ch.md)  
- 2019-08-17 [法国真人版「City Hunter」11月上映!獠翻裙子的特别报道](./2019-08-17_france_ch.md)  
（文中提到北条司本人称2019年为“City Hunter Year”的原因。）  
- 2019-08-17 [法国真人版「City Hunter」日本上映决定!獠挑战的是史上最“香”的任务](./2019-08-17_france_ch_2.md)  
（文中提到北条司本人称2019年为“City Hunter Year”的原因。）  
- 2019-08-07 [「剧场版City Hunter」BD / DVD将于10月上映，剧本和小册子等特典也将推出](./2019-08-07_ch-movie.md)  
- 2019-07-10 [以「City Hunter」为主题，男女皆可使用的Eau de Parfum发售](./2019-07-10_ch_perfume.md)  
- 2019-07-09 [“那个时候的Jump编辑是最强的”传奇人物回顾了那些日子](./2019-07-09_jump_legends.md)  
（文中提到CH的香的名字源自堀江的女儿）
- 2019-07-02 [80年代Jump作品集结，“大人的Jump酒馆”1年限定歌舞伎町](./2019-07-02_jump.md)  
- 2019-07-01 [松下奈绪和靛在现场说，北条司的分镜“有冴羽獠!”](./2019-07-01_angel-sign.md)  
- 2019-06-20 [松下奈绪和藤冈靛成为音乐家情侣，共同出演北条司导演的作品](./2019-06-20_angel-sign.md)  
- 2019-06-20 [松下奈绪和藤冈靛出演北条司担任总导演的电影「Angel Sign」](./2019-06-20_angel-sign_2.md)  
（「Angel Sign」的策划人为：堀江信彦）  
- 2019-06-14 [Monkey Punch追思会，次元角色小林清志也阐述心境“比我先去世”](./2019-06-14_monkey-punch.md)  
- 2019-04-19 [「Cat's▼Eye」第1、2期的BD-BOX，每卷2张光盘的紧凑尺寸](./2019-04-19_cats-eye_bd.md)  
- 2019-04-13 [「剧场版City Hunter」中威风凛凛地拿着枪的冴羽獠&槙村香立体化](./2019-04-13_ch_movie.md)  
- 2019-03-26 [「City Hunter」突破100万观众令北条司瞠目，神谷明「期待未来」](./2019-03-26_ch_100w.md)   
 （译注：指「剧场版City Hunter <新宿Private Eyes>」。)  
 （文中提到为何将3月26日定为生日。）  
- 2019-03-26 [神谷明「City Hunter」应援上映感谢粉丝「真是做梦都没想到」](./2019-03-26_ch_support.md)   
- 2019-03-20 [「City Hunter」的北条司首次执导真人电影「Angel Sign」制作开始](./2019-03-20_angel-sign.md)   
- 2019-03-20 [北条司真人导演出道!描绘“爱的故事”的「Angel Sign」制作决定](./2019-03-20_angel-sign_2.md)   
- 2019-03-12 [「剧场版City Hunter」受到惩罚的良画的到场特典登场了](./2019-03-12.md)   
- 2019-03-07 [「City Hunter」“爱与纠缠”精选节目在Abema播出](./2019-03-07_ch_abema.md)   
- 2019-03-05 [剧场版「City Hunter」新到场者特典Cat's Eye三姐妹登场](./2019-03-05_ch_ce.md)   
- 2019-03-03 [手持枪的「City Hunter」獠的形象，并有一个Mokkori套装作为奖励](./2019-03-03_ch_figure.md)   
- 2019-02-26 [「剧场版City Hunter」"Mokkori"应援上映举行，每周轮换的特典中也有海坊主](./2019-02-26_ch_support.md)   
- 2019-02-25 [原哲夫·堀江信彦原作，出口真人作画「前田庆次歌舞伎之旅」Zenon新连载](./2019-02-25_.md)   
- 2019-02-19 [「剧场版City Hunter」震撼动作4d版上映决定](./2019-02-19_ch_4d.md)   
- 2019-02-19 [「剧场版City Hunter」4d上映决定，特典第二弹有伊仓一惠签名的卡](./2019-02-19_ch_4d_2.md)   
- 2019-02-16 [神谷明感谢「City Hunter」大热“会努力延续下去”](./2019-02-16_ch.md)   
- 2019-02-13 [JUMP角色集合的广告“JUMP FORCE”，路飞和悟空伫立在现实世界中](./2019-02-13_jump-force.md)   
- 2019-02-13 [使用北条司的插图，「剧场版City Hunter」每周更换特典卡5张套装](./2019-02-13_ch.md)   
- 2019-02-09 [对「剧场版City Hunter」“时常回来”的存在表示期待，神谷明等舞台致辞](./2019-02-09_ch.md)   
- 2019-02-09 [「City Hunter」圣地·新宿，大家一起“Mokkori”！北条司说「我很感动」](./2019-02-09_ch_2.md)   
- 2019-02-09 [「Fate HF」须藤友德导演和Task owner，用Newtype谈士郎和樱](./2019-02-09_fate.md)   
- 2019-02-08 [「City Hunter」冴羽獠的黏土人，也有第三张表情的零件。](./2019-02-08_ryo_nendoroid.md)   
    - [黏土人 冴羽獠 |GoodSmile](./zz_goodsmile.info_nendoroid_ryo.md)  
    - [黏土人 槙村香 |GoodSmile](./zz_goodsmile.info_nendoroid_kaori.md)  
    - [“黏土人 冴羽獠”2月07日(周四)开始介绍 |Ameblo](./zz_ameblo.jp_nendoroid_ryo.md)  
    - [“黏土人 牧村香”3月12日(周二)开始介绍  |Ameblo](./zz_ameblo.jp_nendoroid_kaori.md)  
- 2019-02-08 [整本「City Hunter」的Jump，包括对北条司的Interview](./2019-02-08_ch_jump.md)   
- 2019-02-06 [「剧场版City Hunter」留言板在新宿站，还有巨大广告和Golden街合作](./2019-02-06_ch_message-board.md)   
- 2019-02-04 [「剧场版City Hunter」北条司和神谷明看点对谈每周举行](./2019-02-04_hojo_talk.md)   
- 2019-02-01 [「City Hunter」免费阅读，还有电影鉴赏券赠送](./2019-02-01_ch_free-reading.md)   
- 2019-01-30 [Tutorial德井在City Hunter舞台致辞时内裤提得太多“好像得膀胱炎”](./2019-01-30_ch.md)   
- 2019-01-29 [「剧场版City Hunter」北条司打包票「要懂得审时度势啊!」](./2019-01-29_ch.md)   
- 2019-01-29 [「City Hunter」神谷明保持年轻的秘诀是什么?饭丰真理惠用锤子吐槽](./2019-01-29_ch_2.md)   
- 2019-01-25 [Zenon 100号达成，北条司在SNS上回答提问的Q&A企划等登载](./2019-01-25_zenon-100.md)   
- 2019-01-25 [Mokkori蛋包饭等「City Hunter」限定菜单在Café Zenon登场](./2019-01-25_cafe-zenon_menu.md)   
- 2019-01-22 [赢取えすとえむ、モリコロス等tatan作家的签名和単行本的活动](./2019-01-22_tatan.md)   
- 2019-01-19 [在她的好奇心面前，常识和人类的烦恼都无关紧要，「异世界的Oswald」，第一卷](./2019-01-19_oswald.md)   
- 2019-01-19 [森克罗斯“绽放吧，绽放吧!”BL喜欢的男子和百合喜欢的女子在美大邂逅](./2019-01-19_bl.md)  
- 2019-01-19 [与杀死的同伴在死后的世界重逢，能重归于好吗?](./2019-01-19_die.md)  
- 2019-01-19 [有困难就找海坊主商量!「City Hunter」衍生剧第1卷](./2019-01-19_falcon.md)  
- 2019-01-11 [「劇場版City Hunter」中的Cat's Eye! 户田惠子分饰两角，分别扮演瞳和泪](./2019-01-11_ce_ch.md)  
- 2019-01-11 [「劇場版City Hunter」怪盗Cat's Eye三姐妹登场!户田惠子等也出演](./2019-01-11_ce_ch_2.md)  
- 2019-01-10 [City Hunter×Monst，在任务中出现了冴羽獠！Get Wild也会流动](./2019-01-10_ch_monst.md)  
- 2019-01-10 [96年作品「City Hunter Special The Secret Service」TV放送](./2019-01-10_ch_special_96.md)  
- 2019-01-09 [从「剧场版City Hunter」到XYZ!舞台问候活动的服务人员招募](./2019-01-09_job.md)  
- 2019-01-07 [「剧场版City Hunter」神谷明签名海报等推特奖品](./2019-01-07_twitter-award.md)  

### 2018  
- 2018-12-28 [在City Hunter音乐特别节目中，TMN·木根、八木一郎、神谷明演唱了「Get Wild」](./2018-12-28_ch_special_music.md)  
- 2018-12-23 [神谷明&伊仓一惠选出的「City Hunter」TV系列共计12集从明天开始重播](./2018-12-23_ch_12ep.md)  
- 2018-12-13 [チュート徳井作为配音演员加入了「劇場版City Hunter」，同时还有飯豊まりえ](./2018-12-13_ch.md)  
- 2018-12-13 [「剧场版City Hunter」饭丰真理惠、チュート德井客串出演，本预告也解禁](./2018-12-13_ch_2.md)  
- 2018-12-13 [「剧场版City Hunter」ED主题为耳熟能详的「Get Wild」，预告影像公开](./2018-12-13_ch_3.md)  
- 2018-12-13 [饭丰真理惠、德井义实客串出演「City Hunter」!山寺宏一与大冢芳忠](./2018-12-13_ch_4.md)  
- 2018-12-06 [剧场版「City Hunter」特典北条司插画明信片套装](./2018-12-06_ch_postcard.md)  
- 2018-11-21 [用「北斗神拳」「City Hunter」的杯子干杯，ZENON SAKABA在吉祥寺开店](./2018-11-21_zenon-sakaba.md)  
（提到了ZENON SAKABA、CAFE ZENON、じぞう屋（Zizoya）、Coamix之间的关系）  
- 2018-08-21 [冴羽獠是理想的男性! BedIn讲述「City Hunter」](./2018-08-21_ch.md)  
    - 访谈原文：[「City Hunter」无限制阅读纪念 BedIn讲述冴羽獠的魅力  ](./zz_passgetit.auone_2018-08-21.md)  
    - [ <BookPass>「City Hunter」剧场动画化纪念采访『BedIn讲述”冴羽 獠“的魅力』 2018年8月21日（火）特设网页公开](./zz_prtimes.jp_2018-08-21.md)  
- 2018-08-18 [「City Hunter」巨人×广岛战与东京巨蛋合作，神谷明献声参战](./2018-08-18_ch.md)  
- 2018-08-18 [「City Hunter Night」巨人队vs广岛队的比赛中，神谷明在场内播报](./2018-08-18_ch_2.md)  
- 2018-08-07 [一本可以阅读「NARUTO(译注：火影忍者)」全书的电子书，封面插图是新画的](./2018-08-07_naruto.md)  
- 2018-08-02 [劇場版City Hunter「Get Wild」流传的特辑被释放出来了! 冴子和其他成员的演员与TV版相同](./2018-08-02_get-wild.md)  
- 2018-08-02 [「City Hunter」剧场版，时隔约20年的新动画影像--特别报道解禁](./2018-08-02_ch.md)  
- 2018-08-01 [「City Hunter」免费阅读活动，还赠送T恤等周边礼品](./2018-08-01_ch_free-reading.md)  
- 2018-07-20 [【7月20日付】本日発売の単行本リスト](./2018-07-20.md)  
- 2018-07-08 [与Jump编辑部密切相关的节目今晚将在BS1播出，漫画家和历代主编也将登场](./2018-07-08_jump.md)  
- 2018-04-29 [「City Hunter」海坊主的日常生活将在新网站tatan开始](./2018-04-29.md)  
- 2018-04-20 [【4月20日付】本日発売の単行本List](./2018-04-20.md)  
- 2018-03-26 [今天是冴羽獠的生日!纪念Pia，全Mokkori&Hammer Collection刊载](./2018-03-26_ryo_birthday.md)  
- 2018-03-19 [「City Hunter」剧场动画新作决定!演员和导演说“还是那个时候的样子”](./2018-03-19_ch.md)  
- 2018-03-19 [「City Hunter」剧场版制作决定，以现代为舞台的全新故事](./2018-03-19_ch_2.md)  
- 2018-03-12 [Jump 50周年CD第2弹发售!封神演义、星矢、Hiroaka等的曲子也新收录](./2018-03-12_jump-50years-cd.md)  
- 2018-02-14 [Jump展第1弹用VR 360度慢慢享受，“Jump 360”送信](./2018-02-14_jump_vr-360.md)  
- 2018-02-06 [Jump 50周年纪念的“体感プチプチRPG”历代角色大集合](./2018-02-06_jump-50years.md)  
- 2018-01-05 [Jump合作drama，旁白神谷明声援“坚持到底加油”](./2018-01-05_jump-drama.md)  

### 2017  
- 2017-12-25 [濑川藤子在Zenon的新连载，北条司将熊猫香香与「香」联系起来画](./2017-12-25_hojo_panda.md)  
- 2017-10-10 [追寻Jump的历史的纸牌今天发售！从DB、OP到Astro球队、变态面具](./2017-10-10_jump.md)  
- 2017-09-25 [吉崎观音、北条司等41名作家，绘制熊本风景和熊本熊图案的插画集](./2017-09-25_hojo.md)  
- 2017-09-20 [鲁莽的女医生在监狱大闹，北条司「RASH!! ―Rash―」新装版发售](./2017-09-20_rash.md)  
- 2017-09-20 [【9月20日付】本日発売の単行本List](./2017-09-20_book-list.md)  
- 2017-09-16 [Jump重印版的最后两本书，其中出现了悟空和ラオウ的著名台词](./2017-09-16_jump-book.md)  
- 2017-09-09 [原哲夫×荒木飞吕彦的Jump展对谈，“JoJo诞生的契机是经费旅行”](./2017-09-09_jump-interview_jojo.md)  
- 2017-08-21 [“少年Jump展”第1弹的后期展示将于9月1日开始，原画先行公开](./2017-08-21_jump-exh.md)  
- 2017-08-19 [「Angel Heart」首部小说，描写与消失的心脏有关的事件](./2017-08-19_ah.md)  
- 2017-08-19 [【8月19日付】本日発売の単行本List](./2017-08-19_book-list.md)  
- 2017-08-13 [剧场版「City Hunter」导演&Producer Talk，以及Night Show](./2017-08-13_ch.md)  
- 2017-08-12 [Jump 50周年纪念的重印版，第2弹是JOJO & ONE PIECE的连载开始号](./2017-08-12_jump_50years.md)  
- 2017-08-05 [今昔的冴羽獠在da Vinci共演，北条司新画&采访](./2017-08-05_ryo_davinci.md)  
    - [『City Hunter』~『Angel Heart』完结!冴羽獠陪伴的32年间 |ddnavi](../zz_ddnavi.com/2019-03-01.md)  
    - [【da Vinci 2017年9月号】「冴羽獠陪伴的32年」特集番外篇 |ddnavi](../zz_ddnavi.com/2017-08-05_ryo-32years.md)  
- 2017-07-25 [喜欢「City Hunter」的女性在漫画世界转世「一八先生」的锦ソクラ新连载](./2017-07-25_ch_derivative.md)  
- 2017-07-20 [「Angel Heart」最终卷发售，画集&「City Hunter」原点短篇集也将推出](./2017-07-20_ah_end.md)  
- 2017-07-20 [【7月20日付】本日発売の単行本List](./2017-07-20_booklist.md)  
- 2017-07-18 [Jump的大奖奖品接连不断，杂志logo的模型和浴巾等](./2017-07-18_jump.md)  
- 2017-07-18 [「Angel Heart」LINE贴图化，滑稽的发言和留言板上XYZ字样](./2017-07-18_ah_line.md)  
- 2017-07-15 [Jump 50周年纪念重印版，第1弹是创刊号和653万部记录号两册](./2017-07-15_jump-50years.md)  
- 2017-07-14 [「Angel Heart 1stSeason」免费连载开始，「2nd」完结卷发售纪念](./2017-07-14_ah.md)  
- 2017-06-27 [“少年Jump展”民明书房的房斗罗牌(Trump)等商品公开](./2017-06-27_ah.md)  
- 2017-06-27 [Jump 50周年纪念，连续3个月复刻纪念、记录、记忆的刊号](./2017-06-27_jump-50years.md)  
- 2017-06-24 [梅泽春人的Zenon新连载，中年挑战冲浪的迟来的青春涂鸦](./2017-06-24_zenon.md)  
- 2017-06-18 [「City Hunter的一切」展举办决定，征集粉丝选出的最佳小故事](./2017-06-18_ch.md)  
- 2017-06-01 [“MangaHot”今天发布，「北斗神拳」的免费阅读和语音漫画](./2017-06-01_mangahot.md)  
- 2017-05-29 [「Angel Heart」Café，海坊主Float和蛋包饭等](./2017-05-29_ah_cafe.md)  
- 2017-05-25 [「Angel Heart」完结，7月转生物语「今日からCITY HUNTER」开始](./2017-05-25_ch_derive.md)  
- 2017-05-08 [“少年Jump展”第1弹，共63作参展作品公开!附带品的预售票](./2017-05-08_jump.md)  
- 2017-05-01 [“MangaHot”事前登录数特典发生变化，浪川大辅等人参加的语音漫画等](./2017-05-01_mangahot.md)  
- 2017-04-25 [北条司发表「Angel Heart」完结，5月发售的Zenon 7月号上发表最终回](./2017-04-25_ah-end.md)  
- 2017-04-13 [「Angel Heart」「Wakako酒」等Zenon作品通过App发布，初夏开始](./2017-04-13_ah_app.md)  
- 2017-04-12 [路飞、海二、冴羽獠等笑容应援“熊本国际漫画祭”本周末举行](./2017-04-12_smile-support.md)  
- 2017-03-24 [「City Hunter」的Jacket复刻封入!在「Get Wild」纪念盘中](./2017-03-24_ch_gei-wild.md)  
- 2017-02-28 [【2月20日~ 2月26日】每周单行本销量排行榜](./2017-02-28_ranking.md)  
- 2017-02-20 [【2月20日付】本日発売の単行本List](./2017-02-20_booklist.md)  
- 2017-01-06 [东村明子daVinci大特集，迫近她说话和作为干练P的脸](./2017-01-06_davinci.md)  

### 2016
- 2016-12-24 [北条司×井上雄彦对谈芝诺，樱木花道和海坊主的合作彩纸礼物](./2016-12-24_interview.md)  
- 2016-12-21 [北条司和井上雄彦师徒在GJ对谈，谈Jump连载和问卷调查的关系](./2016-12-21_interview.md)  
- 2016-11-25 [围绕神秘美女的悬疑恐怖片「尸牙姬」佐藤洋寿的Zenon新连载](./2016-11-25.md)  
- 2016-10-11 [「City Hunter」将在中国拍真人电影，预计2018年底上映](./2016-10-11_ch_china-live.md)  
- 2016-09-27 [【9月19日~ 9月25日】每周单行本销量排行榜](./2016-09-27_ranking.md)  
- 2016-09-20 [Entermix“名作漫画中的传说情侣”27对相遇回顾特集](./2016-09-20_27-couples.md)  
- 2016-09-20 [【9月20日付】本日発売の単行本List](./2016-09-20_booklist.md)  
- 2016-09-14 [北条司亲自绘制Purin的Package，在吉祥寺的CAFE ZENON限量销售](./2016-09-14_hojo-purin.md)  
- 2016-04-26 [【4月18日~ 4月24日】单行本周销量排行榜](./2016-04-26_ranking.md)  
- 2016-04-20 [【4月20日付】本日発売の単行本List](./2016-04-20_booklist.md)  
- 2016-04-03 [「Angel Heart」完全读本，北条司的采访和工作室访问等](./2016-04-03_ah_interview.md)  
- 2016-02-13 [「City Hunter」獠诞辰30周年“BAR CAT'S EYE”限定开业](./2016-02-13_ryo-30years_bar-ce.md)  
- 2016-01-21 [「龙珠」超过30年的历史汇集于一册!尾田、岸本、富坚等人的投稿](./2016-01-21_dragon-ball.md)  

### 2015  
- 2015-12-24 [「City Hunter」獠款眼镜和Falcon SunGlass上市](./2015-12-24_glasses.md)  
- 2015-12-23 [「City Hunter」香之Hammer特辑，还有Signed Hammer的Present](./2015-12-23_hammer.md)  
- 2015-12-19 [「City Hunter XYZ Edition」全卷发售，特典DVD的ED曲是Get Wild!](./2015-12-19_xyz-edition.md)  
- 2015-12-19 [【12月19日付】本日発売の単行本List](./2015-12-19_booklist.md)  
- 2015-11-29 [北条司的玩笑让上川隆也心跳加速“我的Angel Heart快要碎了”](./2015-11-29_hojo-joke.md)  
- 2015-11-25 [在「City Hunter」Monosa Zenon附录中，大男人也能安心的Mokkori尺寸](./2015-11-25_zenon-mokkori.md)  
- 2015-11-24 [「Angel Heart」獠Model Colt Python等在au SHINJUKU展示](./2015-11-24_ah_colt-python.md)  
- 2015-11-21 [City Hunter&TV「AH」活动，北条等的谈话和动画上映](./2015-11-21_ah_tv.md)  
- 2015-11-20 [【11月20日付】本日発売の単行本List](./2015-11-20_booklist.md)  
- 2015-11-19 [「City Hunter」獠和香围绕北九州市的2016年历](./2015-11-19_ch_calendar.md)  
- 2015-11-13 [「City Hunter」动画新到影像公开，獠和香的争吵场面等](./2015-11-13_ch.md)  
- 2015-11-02 [高野莓「orange」电子版，1~4卷可以随意阅读！完结5卷发售前](./2015-11-02.md)  
- 2015-10-27 [【10月19日~ 10月25日】单行本周销量Ranking](./2015-10-27_weekly-ranking.md)  
- 2015-10-24 [Zenon创刊5周年收录「City Hunter」新作动画先行视听Path](./2015-10-24_ch_zeono-5years.md)  
- 2015-10-20 [【10月20日付】本日発売の単行本List](./2015-10-20_booklist.md)  
- 2015-10-18 [「City Hunter」发行30周年纪念邮票，附赠持卡和明信片](./2015-10-18_ch-30years.md)  
- 2015-10-17 [「City Hunter 30周年纪念 北条司原画展」在新宿举行，还展示了Mini Cooper](./2015-10-17_ch-30years_original_art_exh.md)  
- 2015-10-09 [「Angel Heart」电视剧化纪念特别版，一口气读懂獠与香莹的相遇](./2015-10-09_ah_tv_special.md)  
- 2015-09-25 [西内玛利亚为真人版「Angel Heart」谱写主题曲](./2015-09-25_ah-live_music.md)  
- 2015-09-19 [【9月19日付】本日発売の単行本List](./2015-09-19_booklist.md)  
- 2015-09-13 [TV「Angel Heart」刘信宏由三浦翔平饰演，猎鹰由Brother Tom饰演](./2015-09-13_tv-ah_cast.md)  
- 2015-09-08 [TV「Angel Heart」冴羽獠的搭档槙村香由相武纱季饰演](./2015-09-08_tv-ah_cast.md)  
- 2015-09-04 [描绘City HunterIF世界的「Angel Heart」真人版·香莹是三吉彩花](./2015-09-04_tv-ah_cast.md)  
- 2015-08-25 [北条司和真人版冴羽獠的上川隆也在他们最喜欢的Zenon酒吧对谈](./2015-08-25_hojo_talk_zenon.md)  
- 2015-08-23 [从Lupin到弱佩达！回顾TMS 50周年的纪念书，带Golgo剧场版DVD](./2015-08-23_tms-50years.md)  
- 2015-08-20 [【8月20日付】本日発売の単行本List](./2015-08-20_booklist.md)  
- 2015-08-14 [「Angel Heart」电视剧化纪念，美丽插画和SD角色周边发售](./2015-08-14_ah_tv.md)  
- 2015-08-13 [电视剧「Angel Heart」冴羽獠确定由上川隆也饰演](./2015-08-13_ah_live_cast.md)  
- 2015-08-11 [「City Hunter」与北九州足球俱乐部合作，发售插画门票](./2015-08-11_club.md)  
- 2015-07-28 [「City Hunter」30周年完整读本，新绘插图精选等](./2015-07-28_ch-30years.md)  
- 2015-07-25 [在Zenon中，北条司谈到了冴羽獠诞生前夕的故事，动画“イチゴ味”的设定画面](./2015-07-25_ch.md)  
- 2015-07-20 [「City Hunter」30周年周边，画有獠和香的canvas等](./2015-07-20_ch-30years.md)  
- 2015-07-19 [「City Hunter」30周年特辑，北条司采访](./2015-07-19_ch-30years_interview.md)  
- 2015-07-18 [「City Hunter」新装版1、2卷发售&可以吃蛋包饭的活动](./2015-07-18_ch_new-edition.md)  
- 2015-07-18 [【7月18日付】本日発売の単行本List](./2015-07-18_booklist.md)  
- 2015-07-17 [City Hunter×吉祥寺Parco展示MiniCooper并销售限定商品](./2015-07-17_mini.md)  
- 2015-07-16 [描绘City HunterIF世界的「Angel Heart」TV电视剧化决定](./2015-07-16_ah_tv.md)  
- 2015-06-25 [北条司以Angel Heart风格创作「战国大战」卡片，附录Zenon](./2015-06-25_card.md)  
- 2015-06-25 [北条司签名会「City Hunter XYZ Edition」「AH2nd」新刊纪念](./2015-06-25_ch_ah.md)  
- 2015-06-25 [「City Hunter」新作动画详细发表，神谷明等5名元祖Cast](./2015-06-25_ch_anime.md)  
- 2015-05-25 [「City Hunter」新动画的制作决定，和以前一样的原创声优阵容](./2015-05-25_ch_anime.md)  
- 2015-02-25 [「City Hunter」30周年Project启动&獠的Mokkori次数统计](./2015-02-25_ch-30years_mokkori.md)  
- 2015-02-20 [「ANGEL HEART 2nd」第10卷人气花絮发表&采访收录](./2015-02-20_ah_vol10.md)  
- 2015-02-20 [【2月20日付】本日発売の単行本List](./2015-02-20_booklist.md)  
- 2015-01-14 [默白漫画的颁奖典礼上，原哲夫苦笑着说:“我不敢说，我反对默白”](./2015-01-14_silent-manga.md)  

### 2014  
- 2014-09-25 [在Zenon谈到北条司「A・H」人气投票的结果](./2014-09-25 _ah_zenon.md)  
- 2014-09-20 [【9月20日付】本日発売の単行本List](./2014-09-20_booklist.md)  
- 2014-08-25 [北条司在北海道签名会，AH2nd的新刊纪念](./2014-08-25_signed.md)  
- 2014-06-25 [北条司「Angel Heart」全新收纳盒等赠送](./2014-06-25_ah-box.md)  
- 2014-04-18 [冴羽獠微笑Drip Coffee登场，礼物也有](./2014-04-18_ryo-coffee.md)  
- 2014-03-20 [【3月20日付】本日発売の単行本list](./2014-03-20_booklist.md)  
- 2014-02-11 [永井豪等人对世界默白漫画作品集的采访](./2014-02-11_silent-manga.md)  
- 2014-01-27 [“义风堂堂!!”酒谈篇完结，新章3月开始](./2014-01-27_.md)  

### 2013  
- 2013-11-26 [【11月18日～11月24日】週間単行本売り上げRanking](./2013-11-26_ranking.md)  
- 2013-11-20 [北条司「Angel Heart」第7卷纪念日历](./2013-11-20_ah_calendar.md)  
- 2013-11-20 [【11月20日付】本日発売の単行本List](./2013-11-20_booklist.md)  
- 2013-10-25 [冴羽獠的“抱枕”等，在Zenon有很多礼物](./2013-10-25_ryo_pillow.md)  
- 2013-10-10 [熊本举行Comic Zenon展，在Cat's Eye咖啡屋合影留念](./2013-10-10_ce.md)  
- 2013-07-29 [手冢治虫、水木茂等人的反争作品选集](./2013-07-29_antiwar-works.md)  
    - [漫画が語る戦争　戦場の挽歌 - 株式会社小学館Creative](./zz_shogakukan-cr.co.jp_antiwar-works0.md)  
    - [漫画が語る戦争　焦土の鎮魂歌 - 株式会社小学館Creative](./zz_shogakukan-cr.co.jp_antiwar-works1.md)  
- 2013-07-24 [「City Hunter」以冴羽獠的枪为主题的眼镜](./2013-07-24_glasses.md)  
- 2013-07-22 [北条司&Comic Zenon展在北九州展出原画200幅](./2013-07-22_exh.md)  
- 2013-06-25 [「Angel Heart」黑白画集将抽选100人](./2013-06-25_ah-gift.md)  
- 2013-06-25 [【6月17日~ 6月23日】每周单行本销量排行榜](./2013-06-25_ranking.md)  
- 2013-06-20 [【6月20日付】本日発売の単行本List](./2013-06-20_booklist.md)  
- 2013-02-20 [【2月20日付】本日発売の単行本List](./2013-02-20_booklist.md)  
- 2013-01-29 [【1月21日～1月27日】週間単行本売り上げRanking](./2013-01-29_ranking.md)  
- 2013-01-25 [北条司、日笠优、保谷伸、Molicoross在芝诺举行座谈会](./2013-01-25_talk.md)  
- 2013-01-19 [【1月19日付】本日発売の単行本List](./2013-01-19_booklist.md)  

### 2012  
- 2012-12-20 [【12月20日付】本日発売の単行本List](./2012-12-20_booklist.md)  
- 2012-11-20 [【11月20日付】本日発売の単行本List](./2012-11-20_booklist.md)  
- 2012-10-25 [Zenon 2周年，原稿Present和“花之庆次”全部Goods](./2012-10-25_zenon-2years.md)  
- 2012-10-20 [【10月20日付】本日発売の単行本List](./2012-10-20_booklist.md)  
- 2012-09-20 [【9月20日付】本日発売の単行本List](./2012-09-20_booklist.md)  
- 2012-08-26 [北条司Produce，田中克树撰写Ramen人情剧](./2012-08-26_derivative.md)   
- 2012-08-22 [抽取50人「Angel Heart」香的Coffee Cup](./2012-08-22_ah_cup此.md)   
- 2012-08-20 [【8月20日付】本日発売の単行本List](./2012-08-20_booklist.md)  
- 2012-07-26 [舞台剧「Cat's♥Eye」3姐妹和女刑警浅谷的角色选拔决定](./2012-07-26_ce.md)   
- 2012-07-25 [「Cat's♥Eye」舞台化，Berryz工房&℃-ute的小偷猫](./2012-07-25_ce.md)   
- 2012-07-20 [【7月20日付】本日発売の単行本List](./2012-07-20_booklist.md)  
- 2012-06-20 [【6月20日付】本日発売の単行本List](./2012-06-20_booklist.md)  
- 2012-05-25 [Jump历代动画第一话DVD系列诞生](./2012-05-25_jump.md)  
- 2012-05-25 [北条司×小室哲哉，虚构女歌手演唱的「GET WILD」](./2012-05-25_get-wild.md)  
- 2012-05-19 [【5月19日付】本日発売の単行本List](./2012-05-19_booklist.md)  
- 2012-04-20 [【4月20日付】本日発売の単行本List](./2012-04-30_booklist.md)  
- 2012-03-25 [「Angel Heart」香留在手机里的声音，用Zenon传送](./2012-03-25_ah_kaori-voice.md)  
- 2012-03-19 [发送「Angel Heart」香用手机录音的留言](./2012-03-19_ah_kaori-voice.md)  
- 2012-03-19 [【3月19日付】本日発売の単行本List](./2012-03-19_booklist.md)  
- 2012-03-04 [北条司「Angel Heart第二季」第3卷签名会](./2012-03-04_ah_singed.md)  
- 2012-02-20 [【2月20日付】本日発売の単行本List](./2012-02-20_booklist.md)  
- 2012-01-20 [【1月20日付】本日発売の単行本List](./2012-01-20_booklist.md)  

### 2011
- 2011-12-20 [【12月20日付】本日発売の単行本List](./2011-12-20_booklist.md)  
- 2011-11-19 [【11月19日付】本日発売の単行本List](./2011-11-19_booklist.md)  
- 2011-10-25 [Mokkori Date的附带Zenon附录「冴羽獠“胜负Pants”」](./2011-10-25_ryo-pants.md)  
- 2011-10-20 [【10月20日付】本日発売の単行本List](./2011-10-20_booklist.md)  
- 2011-09-25 [北条司原创图书卡100人的宣传活动](./2011-09-25_book-card.md)  
- 2011-09-24 [Zenon附录，City Hunter& AH ClearFile](./2011-09-24_ch_ah_clearfile.md)  
- 2011-09-20 [【09月20日付】本日発売の単行本List](./2011-09-20_booklist.md)  
- 2011-08-20 [【08月20日付】本日発売の単行本List](./2011-08-20_booklist.md)  
- 2011-07-20 [【07月20日付】本日発売の単行本List](./2011-07-20_booklist.md)  
- 2011-06-26 [「City Hunter」獠&冴子手办，还有Mokkori的表情](./2011-06-26_ch_figure.md)  
- 2011-03-26 [在Zenon刊登了冴羽良主办的Mokkori相亲PartyRepo](./2011-03-26_ch_dating.md)  
- 2011-03-22 [ひうらさとる、新條まゆ、北条司等提供慈善壁纸](./2011-03-22_wallpaper.md)  
- 2011-03-22 [【03月22日付】本日発売の単行本List](./2011-03-22_booklist.md)  
- 2011-03-09 [Zenon的新灵感，用报纸制作的单行本「eco Zenon」](./2011-03-09_eco-zenon.md)  
（有「北条司クロニクル～抒情詩の軌跡～」（「北条司Chronicle~抒情诗的轨迹~」）相关内容）  
- 2011-02-25 [Zenon「Angel Heart」特别篇、獠的求婚故事](./2011-02-25_ah_ryo-proposal.md)  
- 2011-02-06 [Mokkori XYZ「City Hunter」獠主办的结婚派对](./2011-02-06_date-party.md)  
    - [City Hunter举办!!「姻缘Party XYZ」召开决定!! |Comic Zenon](./zz_comic-zenon__mokkori-xyz-party.md)
- 2011-01-09 [SHIBUYA TSUTAYA的300多位漫画家的彩纸展览让人目不暇接!](./2011-01-09_shibuya-tsutaya.md)  

### 2010  
- 2010-12-26 [ホリエモン的小说“拜金”在Zenon开始漫画化](./2010-12-26_yumi.md)  
- 2010-11-25 [久保ミツロウ画的冴羽獠等，在Zenon向北条司致敬](./2010-11-25_ryo_tribute.md)  
- 2010-10-25 [Comic Zenon创刊号，带有Pants附录](./2010-10-25_comic-zenon.md)  
- 2010-10-23 [CAFE ZENON中以「Cat's❤Eye」为灵感的Cocktail](./2010-10-23_ce-cocktail.md)  
- 2010-09-14 [北条司，在globe的CD杂志上画下小室其他成员](./2010-09-14_globe-15years-cd.md)  
- 2010-09-13 [名作复活「Cat's❤愛」等，Comic Zenon发表了详细内容](./2010-09-13_cats-ai.md)  
- 2010-09-09 [【9月9日付】本日発売の単行本List](./2010-09-09_booklist.md)  
- 2010-08-27 [Bunch最终号月刊「@Bunch」发表，还有神秘的Zenon细节](./2010-08-27_bunch.md)  
- 2010-08-20 [在北条司的官网上发表了「Angel Heart」转会地点](./2010-08-20_ah.md)  
- 2010-08-11 [吉祥寺拍卖会上展出多田かおる、サイバラ等人的稀有作品](./2010-08-11_.md)  
- 2010-08-06 [「Angel Heart」最終回，2ndSeason预定](./2010-08-06_ah.md)  
- 2010-06-12 [壮观！SHIBUYA TSUTAYA的庆祝彩纸近300张](./2010-06-12_.md)  
- 2010-05-14 [富坚义博、るーみっくら参加北条司30th致敬展](./2010-05-14_hojo-30th.md)  
- 2010-05-07 [小畑健、和月伸宏、绫小路翔等人为次原隆二的展览献上寄语](./2010-05-07.md)  
- 2010-04-28 [SHIBUYA TSUTAYA有180位漫画家投稿](./2010-04-28.md)  
- 2010-03-09 [北条司30周年周边展开，首先是复制原画和Zippo](./2010-03-09_hojo-30th.md)  
- 2010-03-09 [【3月9日付】本日発売の単行本List](./2010-03-09_booklist.md)  

### 2009  
- 2009-12-24 [北条司30周年，新條まゆ、井上雄彦等人赠送纪念画](./2009-12-24_hojo-30th.md)  
- 2009-11-09 [「My Girl」等畅销连载2010年挂历发售](./2009-11-09_.md)  
- 2009-11-08 [吉祥寺新名胜，充满漫画感的店「CAFE ZENON」诞生](./2009-11-08_cafe-zenon.md)  
（提到名字"Zenon"的来源。）  

### 2008
- 2008-12-19 [Bunch特制Angel Heart豪华BookCover](./2008-12-19_ah_bookcover.md)  

<a name="item"></a>  
## 5. [北条司の関連商品](./item.md)  

<a name="film"></a>  
## 6. [北条司の映画作品](./movie.md)  

<a name="recommend_artist"></a>  
## 7. こちらもおすすめ  
相关艺术家推荐

[玄田哲章](https://natalie.mu/eiga/artist/19455)  
[麻上洋子](https://natalie.mu/eiga/artist/95546)  
[堀江信彦](https://natalie.mu/comic/artist/5280)  
[小山茉美](https://natalie.mu/eiga/artist/19454)  
[梅澤春人](https://natalie.mu/comic/artist/2349)  
[井上雄彦](https://natalie.mu/comic/artist/1791)  

---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown), [CodeBeautify](https://codebeautify.org/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  
6. [OCRSpace](https://ocr.space/)  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



<font color=#ff0000></font>
<a name="comment"></a>  
<s></s>