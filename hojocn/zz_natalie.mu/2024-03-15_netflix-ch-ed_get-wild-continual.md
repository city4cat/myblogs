https://natalie.mu/comic/news/565111  
https://natalie.mu/eiga/news/565108  

# 映画「シティーハンター」あのイントロ流れる予告、ED曲は「Get Wild Continual」
电影《城市猎人》片尾曲《Get Wild Continual》  

2024年3月15日 12:00  

[北条司](https://natalie.mu/comic/artist/2405)原作によるNetflix映画「シティーハンター」のティーザー予告映像が公開された。  
北条司原作的Netflix电影《城市猎人》的预告片公开了。  

[![Netflix映画「シティーハンター」場面写真](https://ogre.natalie.mu/media/news/comic/2024/0315/cityhunter_teaseryokoku02.jpg?impolicy=hq&imwidth=730&imdensity=1)
Netflix映画「シティーハンター」場面写真  
大きなサイズで見る（全16件）  
Netflix电影《城市猎人》剧照Netflix电影《城市猎人》剧照  
大尺寸查看（共16张）  
](https://natalie.mu/comic/gallery/news/565111/2272445)  

[Netflix映画「シティーハンター」場面写真［拡大］  
Netflix电影《城市猎人》剧照[放大]
](https://natalie.mu/comic/gallery/news/565111/2272446)

[Netflix映画「シティーハンター」場面写真［拡大］  
Netflix电影《城市猎人》剧照[放大]
](https://natalie.mu/comic/gallery/news/565111/2272448)

予告映像に映し出されるのは令和の新宿。「もっこり」を連呼して歌ったり、怒涛のガンアクションを披露したりと、ギャップのある冴羽リョウの姿が収められている。  
预告影像中出现的是令和的新宿。连喊着“Mokkori”唱歌，表演怒涛般的枪战戏，收录了冴羽獠充满反差的身影。  

[Netflix映画「シティーハンター」場面写真［拡大］  
Netflix电影《城市猎人》剧照[放大]  
](https://natalie.mu/comic/gallery/news/565111/2272452)

[Netflix映画「シティーハンター」場面写真［拡大］  
Netflix电影《城市猎人》剧照[放大]  
](https://natalie.mu/comic/gallery/news/565111/2272449)

また映像のラストでは“あのイントロ”が赤いミニクーパーのエンジン音とともに鳴り響く。作品のエンディング曲は[TM NETWORK](https://natalie.mu/comic/artist/1270)が「Get Wild」を新たに制作した「Get Wild Continual」に決定した。[小室哲哉](https://natalie.mu/comic/artist/860)は「この日までTM NETWORKの Get Wildがエンディング曲として寄り添えるなんて。必ずまた一歩を踏み出せる、そんな曲に育て続けてくださる皆さんに感謝しています」とコメントした。Netflix映画「シティーハンター」は4月25日に配信開始。  
另外，在视频的最后，“那个前奏”伴随着红色MiniCooper的引擎声响起。作品的片尾曲是TM NETWORK将《Get Wild》重新制作的《Get Wild Continual》。小室哲哉评论道，“到今天为止，TM NETWORK的Get Wild还能作为片尾曲来贴近大家。一定能迈出第一步，感谢一直培育这首歌曲的大家”。Netflix电影《城市猎人》将于4月25日上映。  


## 小室哲哉コメント  
小室哲哉Review    

ハードボイルドであってコミカルでもあり、終わりのないまた続きそうなストーリーのエンディング曲。   
とにかく冴羽獠がより近く、より遠くにみえてしまうような魅惑のサウンドを目指しました。  
这是一首既硬汉又滑稽的故事结尾曲，该故事没有尽头似乎会继续下去。  
总之，我们的目标是创造出一种迷人的声音，让冴羽獠看起来乎近乎远。    

そして満を持しての実写化での世界配信！  
この日までTM NETWORKの Get Wildがエンディング曲として寄り添えるなんて。  
随着时间的推移，真人版将在全球发行！  
时至今日，我仍无法相信 TM NETWORK 的《Get Wild》能成为片尾曲。  

必ずまた一歩を踏み出せる、そんな曲に育て続けてくださる皆さんに感謝しています。  
我感谢所有继续培育这首歌的人，它一定会让我再向前迈进一步。
  
小室哲哉（TM NETWORK）  

## Netflix映画『シティーハンター』最新トレーラー映像

この記事の画像・動画（全16件）

-   [](https://natalie.mu/comic/gallery/news/565111/2272445 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272446 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272448 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272452 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272449 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272444 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272447 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272450 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272451 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/565111/2272443 "Netflix映画「シティーハンター」ティザーアート")
-   [](https://natalie.mu/comic/gallery/news/565111/2247357 "Netflix映画「シティーハンター」ビジュアル")
-   [](https://natalie.mu/comic/gallery/news/565111/2257099 "Netflix映画「シティーハンター」より。左から安藤政信扮する槇村秀幸、鈴木亮平扮する冴羽リョウ、森田望智扮する槇村香、木村文乃演じる野上冴子。")
-   [](https://natalie.mu/comic/gallery/news/565111/2247358 "Netflix映画「シティーハンター」より、鈴木亮平扮する冴羽リョウ。")
-   [](https://natalie.mu/comic/gallery/news/565111/2247359 "Netflix映画「シティーハンター」より、森田望智扮する槇村香。")
-   [](https://natalie.mu/comic/gallery/news/565111/media/102891 "Netflix映画『シティーハンター』最新トレーラー映像")
-   [](https://natalie.mu/comic/gallery/news/565111/media/101413 "2024年 Netflixが贈る実写の注目作品 - Netflix")

## Netflix映画「シティーハンター」

2024年4月25日（木）より、Netflix にて全世界独占配信  


原作：[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」  
出演：[鈴木亮平](https://natalie.mu/comic/artist/50666)、[森田望智](https://natalie.mu/comic/artist/68469)、[安藤政信](https://natalie.mu/comic/artist/19383)、[木村文乃](https://natalie.mu/comic/artist/48878)  
監督：[佐藤祐市](https://natalie.mu/comic/artist/47028)  
脚本：三嶋龍朗  
エグゼクティブ・プロデューサー：高橋信一（Netflix）  
プロデューサー：三瓶慶介、押田興将  
制作：ホリプロ、オフィス・シロウズ  
製作：Netflix  
原作協力：コアミックス

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。




----  

# 鈴木亮平主演「シティーハンター」の予告公開、「Get Wild」が新たに生まれ変わる  
铃木亮平主演《城市猎人》预告公开，《Get Wild》重获新生  

2024年3月15日 12:00

[鈴木亮平](https://natalie.mu/eiga/artist/50666)が主演するNetflix映画「シティーハンター」のティザー予告映像、ティザーアート、場面写真が到着した。  
铃木亮平主演的Netflix电影《城市猎人》的预告片、预告片和场景照片已经到货。  

[![Netflix映画「シティーハンター」場面写真](https://ogre.natalie.mu/media/news/eiga/2024/0315/cityhunter_202403_06.jpg?impolicy=hq&imwidth=730&imdensity=1)
Netflix映画「シティーハンター」場面写真   
大きなサイズで見る（全14件）  
Netflix电影《城市猎人》剧照Netflix电影《城市猎人》剧照  
大尺寸查看（共14个）  
](https://natalie.mu/eiga/gallery/news/565108/2272465)

[Netflix映画「シティーハンター」ティザーアート［拡大］  
Netflix电影《城市猎人》预告片[扩展]  
](https://natalie.mu/eiga/gallery/news/565108/2272469)  

[北条司](https://natalie.mu/eiga/artist/2405)のマンガ「シティーハンター」を日本で初めて実写化した本作。無類の女好きだが超一流なスイーパー・冴羽リョウを鈴木が演じ、リョウの新たなパートナー・香に[森田望智](https://natalie.mu/eiga/artist/68469)、リョウを支える相棒・槇村秀幸に[安藤政信](https://natalie.mu/eiga/artist/19383)、警視庁の敏腕刑事・野上冴子に[木村文乃](https://natalie.mu/eiga/artist/48878)が扮する。  
本片是北条司的漫画《城市猎人》首次在日本拍摄真人版。无比喜欢女色但却是超一流清道夫·冴羽獠由铃木饰演，獠的新搭档·香由森田望智饰演，支持獠的搭档·槙村秀幸由安藤政信饰演，警视厅的干练刑警·野上冴子由木村文乃饰演。  

[Netflix映画「シティーハンター」場面写真［拡大］  
](https://natalie.mu/eiga/gallery/news/565108/2272460)

ティザー予告はYouTubeで公開。リョウが「もっこり」を連呼して歌うおバカな姿と、ダイナミックなアクションを披露するさまが収められている。  
预告片在YouTube公开。獠连呼“Mokori”唱歌的搞笑模样，以及披露动态动作的样子被收录其中。  

[Netflix映画「シティーハンター」場面写真［拡大］  
](https://natalie.mu/eiga/gallery/news/565108/2272464)

[Netflix映画「シティーハンター」場面写真［拡大］  
](https://natalie.mu/eiga/gallery/news/565108/2272466)

本作ではアニメ「シティーハンター」のエンディングテーマとしてよく知られる[TM NETWORK](https://natalie.mu/eiga/artist/1270)の楽曲「Get Wild」が新たに制作され、「Get Wild Continual」として生まれ変わった。TM NETWORKの[小室哲哉](https://natalie.mu/eiga/artist/860)からは「満を持しての実写化での世界配信！ TM NETWORKのGet Wildがエンディング曲として寄り添えるなんて。必ずまた一歩を踏み出せる、そんな曲に育て続けてくださる皆さんに感謝しています」と喜びのコメントも。「Get Wild Continual」は4月21日発売のTM NETWORKのアルバムにも収録予定だ。  
在本片中，TM NETWORK 的歌曲《Get Wild》作为动画片《城市猎人》的片尾曲而广为人知，经过全新制作后以《Get Wild Continual》的形式重生。TMNETWORK 的小室哲哉评论说："这部全球发行的真人电影让我们忙得不可开交！我不敢相信 TM NETWORK 的《Get Wild》可以作为片尾曲。 我非常感谢大家一直以来对这首歌曲的支持，它一定会帮助我再向前迈进一步"。 Get Wild Continual" 也将收录在 TM NETWORK 于 4 月 21 日发行的专辑中。  

Netflix映画「シティーハンター」は4月25日に全世界で配信スタート。「ストロベリーナイト」シリーズの[佐藤祐市](https://natalie.mu/eiga/artist/47028)が監督、三嶋龍朗が脚本を担当した。  
Netflix 电影《城市猎人》于 4 月 25 日开始全球发行。 该片由佐藤雄一（《草莓之夜》系列）执导，三岛龙郎编剧。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記


## Netflix映画『シティーハンター』最新トレーラー映像
Netflix 电影《城市猎人》最新预告片视频。  

この記事の画像・動画（全14件）  
本文图片和视频（共 14 个）  

-   [](https://natalie.mu/eiga/gallery/news/565108/2272465 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272469 "Netflix映画「シティーハンター」ティザーアート")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272460 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272464 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272466 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272461 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272462 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272463 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272467 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/2272468 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/565108/1959485 "鈴木亮平演じる冴羽リョウ。")
-   [](https://natalie.mu/eiga/gallery/news/565108/1987260 "森田望智演じる槇村香。")
-   [](https://natalie.mu/eiga/gallery/news/565108/2257060 "「シティーハンター」キャラクタービジュアル")
-   [](https://natalie.mu/eiga/gallery/news/565108/media/102891 "Netflix映画『シティーハンター』最新トレーラー映像")


