https://natalie.mu/comic/news/353723

# 劇場版「CityHunter」SEIKOとのコラボ腕時計、目盛にXYZの文字

2019年11月1日  [Comment](https://natalie.mu/comic/news/353723/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版CityHunter <新宿Private Eyes>](https://natalie.mu/eiga/film/175572)」と時計メーカー・セイコーのコラボ腕時計が、ANIPLEX+に登場した。  
根据北条司原作改编的动画电影《剧场版城市猎人<新宿Private Eyes>》与钟表制造商Seiko的合作腕表在ANIPLEX+登场。  

[
![「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計。](https://ogre.natalie.mu/media/news/comic/2019/1101/seiko_cityhunter_watch02.jpg?impolicy=hq&imwidth=730&imdensity=1)
「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計。  
大きなサイズで見る（全6件）  
“剧场版城市猎人<新宿Private Eyes>”和精工合作的手表。  
查看大图(共6件)  
](https://natalie.mu/comic/gallery/news/353723/1269065)

時計の文字盤は照準器のレティクルをイメージ。目盛は冴羽リョウの愛銃・コルトパイソン357マグナムにちなみ3時、5時、7時の位置に数字が配され、9時の位置には“ピンホールショット”で打ち抜かれたようなデザインが施された。10時、11時、12時の位置にはそれぞれX、Y、Zの文字が、そのほかの位置には弾丸のモチーフがあしらわれている。盤面裏にはリョウのイラストとシリアルナンバーを刻印。時計にはソーラー充電機能が搭載されている。  
手表的表盘是瞄准器的样式。刻度与冴羽獠的爱枪·Colt Python 357Magnum有关，在3点、5点、7点的位置以数字排列，9点的位置则设计成好像是用“pinhole shot”打出来的。10点、11点、12点的位置分别印有字母X、Y、Z，其他位置则印有子弹图案。盘面背面刻印了獠的插图和序列号。手表配备了太阳能充电功能。  

商品は受注生産品で、価格は税込4万9250円。本日11月1日から12月22日24時まで注文を受け付け、2020年4月よりボックスに収められ順次届けられる。  
商品为订单生产产品，价格为含税49250日元。即日11月1日起至12月22日24时接受订购，2020年4月起装入箱内依次送达。  

なおコミックナタリーでは「劇場版CityHunter <新宿Private Eyes>」のBlu-ray / DVD発売を記念した特集を展開中。神谷が静岡にあるディスク工場を訪れた際のレポートやインタビューを掲載している。
另外，漫画版《剧场版城市猎人<新宿Private Eyes>》的蓝光/ DVD发售纪念特集也在展开中。刊登了神谷访问静冈光盘工厂时的报告和采访。  

この記事の画像（全6件）  
这篇报道的图片(共6篇)  

[![「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計。](https://ogre.natalie.mu/media/news/comic/2019/1101/seiko_cityhunter_watch02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353723/1269065 "「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計。")
[![「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計。](https://ogre.natalie.mu/media/news/comic/2019/1101/seiko_cityhunter_watch05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353723/1269066 "「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計。")
[![「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計。](https://ogre.natalie.mu/media/news/comic/2019/1101/seiko_cityhunter_watch01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353723/1269069 "「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計。")
[![「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計裏面。](https://ogre.natalie.mu/media/news/comic/2019/1101/seiko_cityhunter_watch03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353723/1269067 "「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計裏面。")
[![「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計を収納するボックス。](https://ogre.natalie.mu/media/news/comic/2019/1101/seiko_cityhunter_watch06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353723/1269068 "「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計を収納するボックス。")
[![「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計を収納するボックス。](https://ogre.natalie.mu/media/news/comic/2019/1101/seiko_cityhunter_watch07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/353723/1269070 "「劇場版CityHunter <新宿Private Eyes>」とセイコーのコラボ腕時計を収納するボックス。")

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。  
※冴羽獠的獠，在动物旁正式标记为“僚”。  


(c)北条司/NSP・「2019 劇場版CityHunter」製作委員会

LINK

[劇場版CityHunter <新宿Private Eyes> ×Seiko Collaboration Watch | ANIPLEX+](https://www.aniplexplus.com/HfCCkaaW)  
[Anime「劇場版CityHunter <新宿Private Eyes>」公式Site](https://cityhunter-movie.com/)  
[Anime「劇場版CityHunter ＜新宿Private Eyes＞」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)  