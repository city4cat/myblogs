https://natalie.mu/comic/news/357048

# 山寺宏一＆沢城みゆきがアニメ「シティーハンター」次回予告を再現、仏版予告編
山寺宏一&泽城美雪再现动画《城市猎人》下回预告，法国版预告片

2019年11月26日  [Comment](https://natalie.mu/comic/news/357048/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)原作によるフランスの実写映画「[シティーハンター THE MOVIE 史上最香のミッション](https://natalie.mu/eiga/film/182791)」の特別予告編がYouTubeで公開された。  
根据北条司原作改编的法国真人电影《城市猎人 THE MOVIE 史上最香的任务》在YouTube上公开了特别预告片。

[
![「シティーハンター THE MOVIE 史上最香のミッション」“次回予告”風特別予告より。](https://ogre.natalie.mu/media/news/comic/2019/1126/cityhunter20191126.jpg?impolicy=hq&imwidth=730&imdensity=1)
「シティーハンター THE MOVIE 史上最香のミッション」“次回予告”風特別予告より。  
大きなサイズで見る（全5件）  
“城市猎人THE MOVIE史上最香的任务”“下次预告”风特别预告。  
查看大图(共5件)
](https://natalie.mu/comic/gallery/news/357048/1284761)

本映像はTVアニメ「シティーハンター」の“次回予告”をオマージュして制作されたもの。映画の日本語吹替版でリョウ役を務める[山寺宏一](https://natalie.mu/comic/artist/30965)、香役を務める[沢城みゆき](https://natalie.mu/comic/artist/65954)が、当時の楽曲を再現したBGMとともに掛け合いを繰り広げている。  
本影像是向TV动画《城市猎人》的“下次预告”致敬而制作的。在电影的日语配音版中，山寺宏一饰演獠，泽城美雪饰演香，在重现当时乐曲的背景音乐的伴奏下，两人展开了对手戏。

[
![神谷明が声をあてる“モッコリー氏”の登場シーン。](https://ogre.natalie.mu/media/news/comic/2019/1126/kamiya_mkkory.jpg?imwidth=468&imdensity=1)
神谷明が声をあてる“モッコリー氏”の登場シーン。［拡大］  
神谷明配音的“Mokkori先生”的登场场景。[扩大]  
](https://natalie.mu/comic/gallery/news/357048/1284762)

「シティーハンター THE MOVIE 史上最香のミッション」は、11月29日より東京・TOHOシネマズ 日比谷ほかにて全国ロードショー。なおアニメでリョウ役を務めた[神谷明](https://natalie.mu/comic/artist/60997)と香役を務めた伊倉一恵は特別ゲストとして参加し、神谷が声をあてる2役のうちの1つ“モッコリー氏”が登場するカットも公開されている。  
《城市猎人THE MOVIE史上最香的任务》将于11月29日在东京·TOHO电影院日比谷等地全国上映。另外，在动画中为獠配音的神谷明和为香配音的伊仓一惠将作为特别嘉宾出席，神谷配音的两个角色之一“Mokkori先生”登场的镜头也已公开。

この記事の画像・動画（全5件）  
这篇报道的图片、视频(共5篇)  

[![「シティーハンター THE MOVIE 史上最香のミッション」“次回予告”風特別予告より。](https://ogre.natalie.mu/media/news/comic/2019/1126/cityhunter20191126.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/357048/1284761 "「シティーハンター THE MOVIE 史上最香のミッション」“次回予告”風特別予告より。")
[![神谷明が声をあてる“モッコリー氏”の登場シーン。](https://ogre.natalie.mu/media/news/comic/2019/1126/kamiya_mkkory.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/357048/1284762 "神谷明が声をあてる“モッコリー氏”の登場シーン。")
[![映画「シティーハンター THE MOVIE 史上最香のミッション」本ポスタービジュアル](https://ogre.natalie.mu/media/news/comic/2019/0919/france_cityhunter_postervisual01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/357048/1241525 "映画「シティーハンター THE MOVIE 史上最香のミッション」本ポスタービジュアル")
[![「シティーハンター THE MOVIE 史上最香のミッション」“次回予告”風の特別予告](https://i.ytimg.com/vi/aelCkhfs5u4/default.jpg)](https://natalie.mu/comic/gallery/news/357048/media/43620 "「シティーハンター THE MOVIE 史上最香のミッション」“次回予告”風の特別予告")
[![シティーハンター THE MOVIE 史上最香のミッション ／ 特別映像①](https://i.ytimg.com/vi/IwYYUsCnFCs/default.jpg)](https://natalie.mu/comic/gallery/news/357048/media/43633 "シティーハンター THE MOVIE 史上最香のミッション ／ 特別映像①")

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。  
※冴羽獠的獠，在动物旁正式标记为“僚”。  

(c)AXEL FILMS PRODUCTION - BAF PROD - M6 FILMS


LINK

[映画『シティーハンター THE MOVIE 史上最香のミッション』公式サイト](http://cityhunter-themovie.com/)  
[シティーハンターTHE MOVIE 史上最香のミッション (@cityhunter\_2019) | Twitter](https://twitter.com/cityhunter_2019)  