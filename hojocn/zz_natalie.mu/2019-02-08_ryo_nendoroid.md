https://natalie.mu/comic/news/319237

# 「シティーハンター」冴羽リョウがねんどろいどに、三枚目な表情のパーツも

2019年2月8日  [Comment](https://natalie.mu/comic/news/319237/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


本日2月8日公開の[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」より、冴羽リョウのねんどろいどが発売される。  
今天2月8日上映的北条司原作动画电影《剧场版城市猎人<新宿Private Eyes>》，将发售冴羽獠的Nendoroid。  

(译注：关于[ねんどろいど](https://www.goodsmile.info/ja/aboutnendoroids) / [Nendoroid](https://www.goodsmile.info/en/aboutnendoroids) / [黏土人](https://www.goodsmile.info/zh/aboutnendoroids):  
在喧囂的日常生活中，約手掌大小的尺寸，可愛的模樣治癒著玩家的心靈。總是陪伴在身邊並帶給大家滿滿的元氣，那就是「黏土人」。手掌大小尺寸的模型系列。將動漫、遊戲角色或是實際存在的人物製成2.5頭身約10公分的可愛模樣。替換表情零件與手腳零件，可隨心所欲地賞玩動作姿勢的模型系列。
)

[
![「ねんどろいど 冴羽獠」](https://ogre.natalie.mu/media/news/comic/2019/0208/ryou01.jpg?imwidth=468&imdensity=1)
「ねんどろいど 冴羽獠」  
大きなサイズで見る（全6件）  
“Nendoroid冴羽獠”  
大尺寸查看(全6件)  
](https://natalie.mu/comic/gallery/news/319237/1104335)

おなじみのジャケットを着たリョウを全高約100mmで立体化したこのねんどろいど。クールな面持ちの「通常顔」のほか、三枚目な表情を見せる「笑顔」「焦り顔」といった交換パーツや、愛銃、硝煙エフェクトが付属する。価格は4800円。GOODSMILE ONLINE SHOPでは3月13日21時まで予約を受け付け、購入者には9月に発送される。  
穿着熟悉的夹克的獠以全高约100mm立体化的这个Nendoroid。除了拥有冷酷表情的“通常表情”以外，还附带能展现“笑脸”、“焦虑脸”三张表情的可交换零件、以及爱枪、硝烟效果。价格为4800日元。GOODSMILE ONLINE SHOP在3月13日21点之前接受预约，将在9月发货给购买者。  

なおコミックナタリーでは「劇場版シティーハンター <新宿プライベート・アイズ>」の特集を展開。冴羽リョウ役の神谷明と北条にインタビューし、約20年ぶりの冴羽リョウとの再会や、映画の見どころなどについて語ってもらった。  
另外，Comic Natalie还将推出“剧场版城市猎人<新宿Private Eyes>”的特集。采访了饰演冴羽獠的神谷明和北条，让他们谈了约20年后与冴羽獠的再会，以及电影的看点等。  


この記事の画像（全6件）  
这篇报道的图片(共6篇)  

[![「ねんどろいど 冴羽獠」](https://ogre.natalie.mu/media/news/comic/2019/0208/ryou01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319237/1104335 "「ねんどろいど 冴羽獠」")
[![「ねんどろいど 冴羽獠」](https://ogre.natalie.mu/media/news/comic/2019/0208/ryou03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319237/1104336 "「ねんどろいど 冴羽獠」")
[![「ねんどろいど 冴羽獠」](https://ogre.natalie.mu/media/news/comic/2019/0208/ryou05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319237/1104337 "「ねんどろいど 冴羽獠」")
[![「ねんどろいど 冴羽獠」](https://ogre.natalie.mu/media/news/comic/2019/0208/ryou06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319237/1104338 "「ねんどろいど 冴羽獠」")
[![「ねんどろいど 冴羽獠」](https://ogre.natalie.mu/media/news/comic/2019/0208/ryou02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319237/1104339 "「ねんどろいど 冴羽獠」")
[![「ねんどろいど 冴羽獠」](https://ogre.natalie.mu/media/news/comic/2019/0208/ryou04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/319237/1104340 "「ねんどろいど 冴羽獠」")

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。  
※冴羽獠的獠，在动物旁正式标记为“僚”。  


(c)北条司/NSP・「2019 劇場版シティーハンター」製作委員会

LINK

[Nendoroid 冴羽獠](https://www.goodsmile.info/ja/product/8055/ねんどろいど+冴羽獠.html)  

関連商品

[
![「Nendoroid 冴羽獠」](https://images-fe.ssl-images-amazon.com/images/I/41C4-WS3qCL._SS70_.jpg)  
「Nendoroid 冴羽獠」  
[Figure] 2019年9月30日発売 / GoodSmile company  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/B07NDHVZP1/nataliecomic-22)

译注：  
[黏土人 冴羽獠 |GoodSmile](./zz_goodsmile.info_nendoroid_ryo.md)  
[黏土人 槙村香 |GoodSmile](./zz_goodsmile.info_nendoroid_kaori.md)  
[“黏土人 冴羽獠”2月7日(周四)开始介绍!](./zz_ameblo.jp_nendoroid_ryo.md)  
[“黏土人 牧村香”3月12日(周二)介绍开始☆ ](./zz_ameblo.jp_nendoroid_kaori.md)  