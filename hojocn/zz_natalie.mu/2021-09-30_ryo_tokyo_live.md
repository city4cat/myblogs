https://natalie.mu/stage/news/447332

# 宝塚歌劇雪組「CITY HUNTER」東京公演千秋楽、全国の映画館でライブ中継  
宝冢歌剧雪组《CITY HUNTER》东京公演千秋乐将在全国电影院实况转播  


2021年9月30日 [Comment](https://natalie.mu/stage/news/447332/comment) [ステージナタリー編集部](https://natalie.mu/stage/author/75)


宝塚歌劇雪組「ミュージカル『CITY HUNTER』―盗まれたXYZ―」「ショー オルケスタ『Fire Fever!』」東京・東京宝塚劇場公演千秋楽の様子が、全国各地の映画館でライブ中継される。  
宝冢歌剧雪组「音乐剧『CITY HUNTER』—被盗的XYZ—」「Show Orquesta『Fire Fever!』」东京·宝冢剧场公演千秋乐的情况，将在全国各地的电影院实况转播。  

[
![宝塚歌劇雪組「ミュージカル『CITY HUNTER』―盗まれたXYZ―」「ショー オルケスタ『Fire Fever!』」東京・東京宝塚劇場公演千秋楽、ライブ中継開催の告知ビジュアル。](https://ogre.natalie.mu/media/news/stage/2021/0930/1114TOKYO_cityhunter.jpg?impolicy=hq&imwidth=730&imdensity=1)
宝塚歌劇雪組「ミュージカル『CITY HUNTER』―盗まれたXYZ―」「ショー オルケスタ『Fire Fever!』」東京・東京宝塚劇場公演千秋楽、ライブ中継開催の告知ビジュアル。  
大きなサイズで見る  
宝冢歌剧雪组「音乐剧『CITY HUNTER』—被盗的XYZ—」「Show Orquesta『Fire Fever!』」东京·宝冢剧场公演千秋乐，举办实况转播的通告。  
用大尺寸看  
](https://natalie.mu/stage/gallery/news/447332/1673162)

ライブビューイングの対象となるのは、11月14日13:30開演回。チケットの先行抽選販売は10月2日11:00から18日12:00まで実施され、一般販売は11月6日11:00に開始される。  
现场观看是11月14日13:30开演场次。门票的先行抽签销售从10月2日11:00开始到18日12:00为止，一般销售从11月6日11:00开始。

[齋藤吉正](https://natalie.mu/stage/artist/105109)が脚本・演出を手がける「ミュージカル『CITY HUNTER』―盗まれたXYZ―」は、[北条司](https://natalie.mu/stage/artist/2405)のマンガ「シティーハンター」を原作としたミュージカル。[稲葉太地](https://natalie.mu/stage/artist/114917)が作・演出を担う「ショー オルケスタ『Fire Fever!』」は、エネルギッシュでバラエティ豊かなショー作品だ。8月7日から9月13日にかけては、[彩風咲奈](https://natalie.mu/stage/artist/93421)と[朝月希和](https://natalie.mu/stage/artist/100741)による雪組新トップコンビ大劇場お披露目公演が、兵庫・宝塚大劇場で行われた。東京宝塚劇場公演は10月2日から11月14日まで実施される。  
斋藤吉正编剧兼导演的「音乐剧『CITY HUNTER』—被盗的XYZ—」是以北条司的漫画《城市猎人》为原作的音乐剧。稻叶太地担任编剧兼导演的「Show Orquesta『Fire Fever!』」是一部充满活力、内容丰富的表演作品。8月7日至9月13日，彩风咲奈和朝月希和的雪组新top组合大剧场披露公演在兵库·宝冢大剧场举行。东京宝冢剧场公演将于10月2日至11月14日举行。

LINK

[宝塚歌劇雪組「ミュージカル『CITY HUNTER』―盗まれたXYZ―」「ショー オルケスタ『Fire Fever!』」ライブビューイング](https://liveviewing.jp/cityhunter-tokyo/)  
[雪組公演 『CITY HUNTER』『Fire Fever!』 | 宝塚歌劇公式ホームページ](https://kageki.hankyu.co.jp/revue/2021/cityhunter/index.html)  
