https://natalie.mu/comic/news/66378


# 「エンジェル・ハート」香が携帯に録音した伝言を配信
发送“天使心”香用手机录音的留言  

2012年3月19日  [Comment](https://natalie.mu/comic/news/66378/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート2ndシーズン」の3巻が、本日3月19日に発売された。これを記念して月刊コミックゼノン（徳間書店）は、冴羽獠の携帯電話に香が吹き込んだ伝言メモを期間限定で配信する。  
北条司「Angel Heart 2ndSeason」的第3卷于3月19日发售。为了纪念此事，月刊Comic Zenon(德间书店)在限期内公布香向冴羽獠的手机发送的留言备忘录。  

[
![「エンジェル・ハート2ndシーズン」3巻収録のエピソード「香の声」より。](https://ogre.natalie.mu/media/news/comic/2012/0319/kaoriVoice_ryo2.jpg?imwidth=468&imdensity=1)
「エンジェル・ハート2ndシーズン」3巻収録のエピソード「香の声」より。  
大きなサイズで見る（全2件）  
这是「Angel Heart 2ndSeason」第3卷收录的插曲《香之声》。  
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/66378/113976)

この伝言メモは「エンジェル・ハート2ndシーズン」3巻収録のエピソード「香の声」に登場したものの再現。伝言メモの声は「シティーハンター」で香役を務めた伊倉一恵が演じる。家出した香が仲直りのチャンスとして獠に残したメッセージは、往年のファンならば感涙もの。  
这个留言笔记再现了「Angel Heart 2ndSeason」3卷收录的小故事“香之声”中登场的东西。留言笔记由曾在《城市猎人》中饰演香一角的伊仓一惠配音。离家出走的香给獠的留言作为和好的机会，对该剧的粉丝来说是一个催人泪下的过去。  

伝言メモは2種類用意されており「エンジェル・ハート2ndシーズン」3巻、3月24日発売の月刊コミックゼノン5月号にて配信サイトにアクセスできる。また「エンジェル・ハート1stシーズン」1・2巻では伝言メモとは別に、1stシーズンの名台詞を配信。詳細は各対象書籍にて確認を。  
留言备有2种，可以在「Angel Heart 2ndSeason」第3卷、3月24日发售的月刊Comic Zenon 5月号上登陆。另外，在「Angel Heart 1stSeason」第一、二卷中，除了留言笔记之外，还会配信1stSeason的名台词。详细内容请在各自书籍中确认。  

この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![「Angel Heart 2ndSeason」3巻収録のエピソード「香の声」より。](https://ogre.natalie.mu/media/news/comic/2012/0319/kaoriVoice_ryo2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/66378/113976 "「Angel Heart 2ndSeason」3巻収録のエピソード「香の声」より。(这是《Angel Heart 2ndSeason》第3卷收录的插曲《香之声》。)")  
[![「Angel Heart 2ndSeason」3巻](https://ogre.natalie.mu/media/news/comic/2012/0302/ah2nd3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/66378/111862 "「Angel Heart 2ndSeason」3巻")  



LINK  
[Comic Zenon](http://www.comic-zenon.jp/)

関連商品  
[
![北条司「Angel Heart 2ndSeason（3）」](https://images-fe.ssl-images-amazon.com/images/I/514bMgMxP%2BL._SS70_.jpg)  
北条司「Angel Heart 2ndSeason（3）」  
[漫画] 2012年3月19日発売 / 徳間書店 / 978-4199800696  
Amazon.co.jp [書籍版 / Kindle版]  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199800697/nataliecomic-22)