https://natalie.mu/comic/news/510174


# 「Lupin三世VS Cat'sEye」“幻”のシナリオ言い当てる栗田貫一に監督「鋭い！」
栗田貫一说「鲁邦三世VS Cat'sEye」的"奇妙"情景是"精明!"的  

2023年1月24日  
[ComicNatalie編集部](https://natalie.mu/comic/author/72)  

アニメ映画「Lupin三世VSCat'sEye」のジャパンプレミア試写会が、本日1月24日に東京のTOHOシネマズ 六本木ヒルズで開催。Lupin三世役の[栗田貫一](https://natalie.mu/comic/artist/6789)、来生瞳役の[戸田恵子](https://natalie.mu/comic/artist/16682)、[瀬下寛之](https://natalie.mu/comic/artist/92064)監督が登壇した。  
动画电影“鲁邦三世VS Cat'sEye”的日本首映式试映会于今天1月24日在东京TOHO Cinemas六本木Hills举行。饰演鲁邦三世的栗田贯一、来生瞳的户田惠子、濑下宽之导演登台演出。  

[
![「Lupin三世VSCat'sEye」ジャパンプレミア試写会の様子。左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子、瀬下寛之監督。(“Lupin三世VS Cat'sEye”日本首映试映会的样子。从左边开始黑泽佳子、村上知子、大岛美幸、栗田贯一、户田惠子、濑下宽之导演。)](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6137.jpg?impolicy=hq&imwidth=730&imdensity=1)
「Lupin三世VSCat'sEye」ジャパンプレミア試写会の様子。左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子、瀬下寛之監督。
大きなサイズで見る（全44件）(大尺寸观看)
](https://natalie.mu/comic/gallery/news/510174/1983728)

[
左から栗田貫一、戸田恵子、瀬下寛之監督。［拡大］
](https://natalie.mu/comic/gallery/news/510174/1983715)

「Lupin三世」のアニメ化50周年と「Cat'sEye」の原作40周年を記念し、1月27日よりPrime Videoで世界独占配信される「Lupin三世VSCat'sEye」。Lupin三世一味と瞳らキャッツアイが、ある絵画を巡って火花を散らすさまが描かれる。当初静野孔文監督も登壇を予定していたものの、体調不良のため欠席となった。  
为纪念“鲁邦三世”动画化50周年和「Cat'sEye」原作40周年，“鲁邦三世VS Cat'sEye”将于1月27日在Prime Video上全球独家发布。描绘了鲁邦三世一伙和猫眼（包括Hitomi）之间因一幅画而擦出的火花。虽然最初静野孔文导演也计划上台，但由于身体不适而缺席。  

[栗田貫一［拡大］](https://natalie.mu/comic/gallery/news/510174/1983716)

[戸田恵子［拡大］](https://natalie.mu/comic/gallery/news/510174/1983718)

オファーが来たときの心境について、栗田は「以前（江戸川）コナンさんとLupinが対決したことがあるんですが、コナンさんがありならキャッツアイも来るだろうと思いました。不二子ちゃんがキャッツアイたちと並んでいても遜色ないですし。やっぱり来たか！と思いましたね」とコメント。続けて栗田は「いずれ『アンパンマン』とのコラボもあるかもしれない」とジョークを飛ばし、アニメで長年アンパンマン役を演じている戸田を笑わせる。戸田は「何年か前に『劇場版シティーハンター <新宿プライベート・アイズ>』で『Cat'sEye』が30何年かぶりに復活、というお仕事をいただいたんですが、そのときかなり驚いてひっくり返ったんですよね。それから数年経って、今回は『Lupin三世』というビッグタイトルと同じ画角に収まることができるということで、再びひっくり返りました（笑）。でもまた私たちを起用していただき、本当に光栄なことです」とほほ笑んだ。  
关于接到邀请时的心情，栗田说:“以前(江户川)柯南和Lupin有过对决，我想如果有柯南的话猫眼也会来的。不二子和猫眼们站在一起也毫不逊色。果然来了啊!”接着栗田开玩笑说:“说不定哪天还会和《面包超人》合作呢。”这让常年在动画中饰演面包超人的户田笑了。户田说:“几年前接到了《剧场版城市猎人<新宿Private Eyes>》中《Cat'sEye》时隔30多年复活的工作，当时我非常吃惊。从那之后过了几年，这次因为可以和《Lupin三世》这个大作同台，所以再次翻盘了(笑)。但是再次起用我们，真的是非常光荣的事情”。  

[瀬下寛之監督［拡大］](https://natalie.mu/comic/gallery/news/510174/1983719)

オリジナルのシナリオで展開していく「Lupin三世VSCat'sEye」について、瀬下監督は「僕も静野監督も、オリジナルシナリオで作る以外想像が付きませんでした。僕ら自身が想像付かないことはできないですから。それに『Lupin三世』も『Cat'sEye』も、絵として個性の強いキャラクターが登場しますし、演じるキャストの皆さんも強烈に上手い皆さんから、僕たちの頭の中にあるイメージですと、オリジナル以外あり得ませんでした」と語る。栗田が当初、不二子とキャッツアイの女性怪盗同士の対決をイメージしていたと述べると、瀬下監督は「栗田さんは本当に鋭い！ 当初そういうプロットを考えていたんです。不二子がボロボロになって、泪姉さんが『替えのレオタードならあるわよ』と差し出すようなシーンも考えたんですが、微妙に収集が付かなくて変えたんです。でもあの3人に不二子が加わったら最強感があるし、見てみたかったですね」と制作の裏側を明かした。  
关于以原创剧本展开的《Lupin三世VS Cat'sEye》，濑下导演说:“除了用原创剧情制作，我和静野导演都无法想象其他东西。 我们不能做我们自己无法想象的事情。 而且由于《鲁邦三世》和《猫眼》都有个性鲜明的人物形象，扮演他们的演员也都非常优秀，从我们脑海中的形象来看，除了拍一部原创电影，没有其他办法”。当栗田说当初的印象是不二子和猫眼的女怪盗之间的对决时，濑下监督说“栗田先生真的很敏锐!当初我想的是这样的情节，不二子摔得稀烂，泪姐姐递过来说:“有替换的紧身衣。”但我不能完全做到，所以改了。但是如果不二子加入她们3个人的话，那将是最强的一幕，想看一下呢”。  

[キャッツアイに扮して乱入する森三中。［拡大］（森山中扮成猫眼闯入现场）](https://natalie.mu/comic/gallery/news/510174/1983721)

ここでシークレットゲストとして、[森三中](https://natalie.mu/comic/artist/7317)の[黒沢かずこ](https://natalie.mu/comic/artist/53965)、[村上知子](https://natalie.mu/comic/artist/53966)、[大島美幸](https://natalie.mu/comic/artist/53964)がキャッツアイ風のレオタード姿で乱入。ステージ上で前転を披露する、キャッツアイの予告状・キャッツカードを投げるなどして観客を湧かせた。12年前にCMでもキャッツアイに扮していた森三中。大島は久しぶりのレオタード姿に「いつもの普段着より着やすいので、これを着て帰りたいですね」と笑顔を見せる。キャッツアイ同様、女性3人組で活動する森三中に、МCが「女性3人組というのは、収まりがいいんでしょうか？」と質問する。村上は「そうですね、2人ならケンカしたらコンビ別れしちゃうけど、3人なら絶対に誰か仲裁してくれますから。ギャラは割りづらいけど。だから、キャッツアイ＝森三中ということで」とアピールした。  
在这里，秘密嘉宾森三中、黒沢かずこ、村上智子、大岛美幸穿着猫眼风格的紧身衣冲了进来。 12年前也曾在一个广告中装扮成猫眼的森山中，在舞台上表演了一个前滚翻，并抛出了一张猫眼的通告信，令观众激动不已。大岛笑着说，她很久以来第一次穿上紧身衣，说她愿意穿着它回家，因为它比她平常的日常服装更容易穿。与"猫眼"一样，"森山中"也是一个三个人的团体，МC问道："你们适合做三个人的团体吗？"。村上回答说："嗯，如果只有我们两个人，发生争吵的话，我们就会分开，但如果只有我们三个人，总会有人来调停。不过，要分担费用是很难的。这就是为什么我们称之为 "猫眼=森山中"。（译注：待校对）    

[キャッツアイに扮して乱入する森三中。［拡大］（扮成猫眼闯入的森三中）](https://natalie.mu/comic/gallery/news/510174/1983721)

映画の感想を問われ、村上が「小さいときから観てたLupin三世とキャッツアイがコラボすることだけでも素敵なんですけど、銭形警部と（内海）俊夫の組み合わせもどうなっちゃうの!?っていう」とコメントすると、黒沢が「そう、こんな組み合わせ!?って思えるような、まるでМ‐1王者が集まっているようなエンターテインメントが繰り広げられています！ そして本当にハートフルで、すごく楽しい作品でした！」と熱烈に見どころを語った。  
被问及对电影的感想时，村上说：“从小就看的Lupin三世和猫眼合作就很棒了，这样的话钱形警部和(内海)俊夫的组合也会怎么样呢！？“，黑泽热情洋溢地谈到了该剧的亮点：“对，这样的组合！？就像是M-1王者聚集在一起的娱乐！而且真的是充满爱心，非常有趣的作品！“。  

最後に栗田は「キャッツさんたちとLupinがうまく駆け引きをして、いい人生ドラマになった感じもあるし、メランコリーな場面もある。こんなふうにまとまるんだ、と思いました」と感想を述べ、先ほど話題に出た不二子とキャッツアイのコラボを例に挙げつつ「パート2作ってもいいんじゃない？」と瀬下監督に語りかける。戸田は、TVシリーズなどで泪役を演じていた藤田淑子が2018年に亡くなったことから、「『劇場版シティーハンター <新宿プライベート・アイズ>』では私が瞳と泪姉さんの2役をやらせていただいたんですが、今回は深見梨加さんが実に見事に姉さんを演じてくださいました。たくさんの思い出がある『Cat'sEye』に参加でき、しかも『Lupin三世』とコラボということで本当に幸せな気持ちです」と挨拶する。最後に栗田と戸田、瀬下監督と森三中でフォトセッションを行い、イベントの幕が閉じた。  
最后栗田说“Cats和Lupin巧妙地进行了周旋，感觉就像一部好的生活剧，也有浪漫的场面。我想就是这样总结的”，并以刚才提到的不二子和猫眼的合作为例，“制作第二部分也不错吧?”对濑下导演说。在TV系列中饰演泪一角的藤田淑子于2018年去世后，户田表示“《剧场版城市猎人 新宿Private Eyes》中，我扮演了瞳和泪姐两个角色，这次深见梨加非常出色地扮演了姐姐。能参加有很多回忆的‘Cat'sEye’，而且和‘Lupin三世’合作真的感到很幸福”。最后栗田和户田、濑下导演和森三中合影，活动落下了帷幕。  

なおComicNatalieでは「Lupin三世VSCat'sEye」の特集を展開中。「Lupin三世」シリーズと「Cat'sEye」に精通するライターが配信開始より一足先に同作を鑑賞し、同作の魅力をたっぷりと綴っている。  
另外ComicNatalie也推出了“Lupin三世VSCat'sEye”的特集。精通“Lupin三世”系列和“Cat'sEye”的作家在配信开始之前先一步鉴赏了同作，充分描写了同作的魅力。  

関連する特集・インタビュー（相关的专题采访）  

[「Lupin三世VSCat'sEye」日本一有名な泥棒と怪盗三姉妹が強力コラボ！鑑賞して残る、“いつも通りだけど、ちょっと特別”な余韻 1月17日  
（《Lupin三世VSCat'sEye》日本最著名的小偷和怪盗三姐妹强力合作!观赏留下的“一如既往，但有点特别”的余韵1月17日）
](./2023-01-17_lupin-vs-catseye.md)

この記事の画像・動画（全44件）（这篇报道的图片和视频）

[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6137.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983728 "「Lupin三世VSCat'sEye」ジャパンプレミア試写会の様子。左から黒沢かずこ、村上知子、大島美幸、栗田貫一、戸田恵子、瀬下寛之監督。  
（Lupin三世vs Cat'sEye Japan首映式试映会的样子。左起黑泽和子、村上知子、大岛美幸、栗田贯一、户田惠子、濑下宽之导演。）")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_5965.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983715 "左から栗田貫一、戸田恵子、瀬下寛之監督。")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_5968.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983716 "栗田貫一")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_5985.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983718 "戸田恵子")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_5991.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983719 "瀬下寛之監督")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6034.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983721 "キャッツアイに扮して乱入する森三中。  
(扮演猫眼闯入的森林三中。)")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6076.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983724 "「Lupin三世VSCat'sEye」ジャパンプレミア試写会の様子。  
(Lupin三世VSCat‘sEye“日本首映试映会的样子。)")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6113.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983727 "キャッツカードを使って、МCにティッシュ配りの真似を披露する黒沢かずこ。  
(黒沢かずこ用猫眼卡对МC的模仿表演。)")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_5976.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983717 "栗田貫一")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6020.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983720 "戸田恵子")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6072.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983729 "黒沢かずこ")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6070.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983723 "村上知子")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6067.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983722 "大島美幸")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6087.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983725 "「今年盗んでみたいものは？」という質問に対する、栗田貫一の回答。")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6094.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983730 "「今年盗んでみたいものは？」という質問に対する、戸田恵子の回答。")
[![](https://ogre.natalie.mu/media/news/comic/2023/0124/DSC_6104.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1983726 "「今年盗んでみたいものは？」という質問に対する、森三中の回答。")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/keyvisual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953846 "「Lupin三世 VS Cat'sEye」キービジュアル (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世VS Cat‘sEye“主视觉(C)Monkey Punch北条司/Lupin三世VS Cat’sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953840 "Lupin三世（CV：栗田貫一）。天才的泥棒の家系に生を受け、世界中をまたにかける大泥棒。どんなターゲットも奇想天外なアイディアと大胆不敵なテクニックで手に入れる。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世(CV：栗田贯一)。出生于天才小偷的家族，遍布世界各地的大盗。任何目标都可以通过异想天开的想法和大胆无畏的技巧得到。(C)Monkey Punch北条司/Lupin三世VS Cat‘sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953837 "次元大介（CV：大塚明夫）。早撃ち0.3秒の天才ガンマン。その卓越したガンさばきは、Lupin三世も一目置く。窮地に陥っても常に冷静に状況を把握して、難局を打破する男。酒と煙草をこよなく愛する。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（次元大介(CV：大冢明夫)。快枪手0.3秒的天才枪手。其卓越的枪战技巧让Lupin三世也刮目相看。即使陷入困境也要经常冷静地把握状况，打破困局的男人。他喜欢喝酒和抽烟。(C)Monkey Punch北条司/Lupin三世VS Cat‘sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953835 "石川五ェ門（CV：浪川大輔）。約400年前に大怪盗を輩出した石川家の末裔。斬鉄剣を自在に操る居合の達人であり、剣の道を究めるためにひたすら修行の道を歩む誇り高き剣豪。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（石川五ェ門（简历：浪川大辅）。 是约400年前大怪盗辈出的石川家的后裔。是自由操纵斩铁剑的居合达人，也是为了钻研剑道一味走修行之路的骄傲的剑豪。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_007.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953836 "峰不二子（CV：沢城みゆき）。さまざまな顔を持つ謎の美女。類まれなる美貌と抜群のプロポーションを持ちながら、Lupinさえも手玉にとる天才的頭脳をあわせ持つ。行動は単純で、己の欲望に従っているだけ。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（峰不二子(cv:沢城みゆき)。拥有各种各样面孔的谜之美女。拥有罕见的美貌和出众的身材，同时还拥有连Lupin都能玩弄的天才头脑。行动简单，只是遵从自己的欲望。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")

さらに読み込む

[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953834 "銭形警部（CV：山寺宏一）。ICPO（インターポール）のLupin三世専任捜査官であり、江戸時代の有名な岡っ引きの子孫。Lupin逮捕に執念を燃やし、Lupinの事なら誰よりも熟知している。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（钱形警官(cv:山寺宏一)。ICPO(国际刑警组织)的Lupin三世专职搜查官，江户时代有名的冈引的子孙。他对逮捕Lupin充满执念，比谁都熟悉Lupin。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_009.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953832 "来生瞳（CV：戸田恵子）。キャッツアイ三姉妹の次女。爽やかにロングヘアーをなびかせる美人。抜群の身体能力を活かし、華麗に盗みを遂行する。刑事の俊夫とは学生の時から恋人関係にある。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
(来生瞳(cv:户田惠子)。猫眼三姐妹中的次女。清爽飘逸的长发美女。活用出众的身体能力，华丽地完成偷盗。和刑警俊夫从学生时代开始就是恋人关系。(c)北条司/Lupin三世VS Cat'sEye制作委员会)")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_010.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953833 "来生泪（CV：深見梨加）。キャッツアイ三姉妹の長女。大人の色気を漂わせる、グラマラスな美女。その明晰な頭脳を活かし、作戦の司令塔を担う。優れた変装技術も持っている。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（来生泪(cv:深见梨加)。猫眼三姐妹中的长女。散发着成熟魅力的迷人美女。利用自己清醒的头脑来指挥行动。还拥有高超的乔装技术。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_011.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953843 "来生愛（CV：坂本千夏）。キャッツアイ三姉妹の三女。元気いっぱい、ボーイッシュな女の子。メカやITに強く、自作ギミックで盗みを成功に導く。三姉妹の中で唯一、父親との思い出がない。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（来生爱(cv:坂本千夏)。猫眼三姐妹中的三女。充满活力，像男孩子一样的女孩子。擅长机械和IT技术，用自创的噱头成功盗窃。三姐妹中，她是唯一没有与父亲回忆的人。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_012.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953842 "内海俊夫（CV：安原義人）。瞳の恋人で、キャッツアイ事件を担当する犬鳴署の刑事。お調子者だが正義感は強く、キャッツアイ逮捕に執念を燃やす熱血漢。銭形に憧れを抱いている。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（内海俊夫(cv:安原义人)。瞳的恋人，是负责猫眼事件的犬鸣署刑警。虽然行事高调，但正义感很强，是对逮捕猫眼充满执念的热血汉子。对钱形抱有憧憬。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953841 "永石（CV：麦人）。ハインツから恩義を受け、三姉妹を陰から支える紳士。キャッツアイの盗みのサポートや、ハインツコレクションの管理などを行っている。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（永石(cv:麦人)。受海因茨恩惠，在背后支持三姐妹的绅士。提供猫眼盗窃的支持，以及管理海因茨收藏品等。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953838 "ハインリッヒ・ベルガー。喫茶キャッツアイを訪れる初老の画商。キャッツ三姉妹に絵画「花束と少女」にまつわる依頼を持ち込む。失踪前のハインツを知っている。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Heinrich Berger。来到猫眼咖啡店的老年画商。向猫三姐妹提出了关于画作《花束与少女》的委托。在海因茨失踪前就认识他。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1953839 "デニス・キルヒマン。裏世界を暗躍する武器商「ファーデン」の幹部。元傭兵で、目的のためなら手段を択ばない冷酷な男。Lupinやキャッツアイの前に強敵として立ちはだかる。 (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Denis Kirchmann。在黑社会暗中活动的武器商“Faden”的干部。原是雇佣兵，为达目的不择手段的冷酷男人。作为强敌阻挡在Lupin和猫眼的前面。(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/teaser_visual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905449 "「Lupin三世 VS Cat'sEye」キービジュアル (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/01c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905460 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/02a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905459 "「Lupin三世 VS Cat'sEye」場面カット")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/02a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905458 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/04c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905457 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/05b.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905456 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/06c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905455 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/07c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905454 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/08c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905453 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/09c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905452 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/10a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905451 "「Lupin三世 VS Cat'sEye」場面カット (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://ogre.natalie.mu/media/news/comic/2022/0922/lvc_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/510174/1905450 "「Lupin三世 VS Cat'sEye」ロゴ (c)モンキー・パンチ 北条司/Lupin三世 VS Cat'sEye製作委員会  
（Lupin三世vs Cat'sEye”另外(c)北条司/Lupin三世VS Cat'sEye制作委员会）")
[![](https://i.ytimg.com/vi/zZAsIu9oBIo/default.jpg)](https://natalie.mu/comic/gallery/news/510174/media/84700 "「Lupin三世 VS Cat'sEye」第2弾PV")
[![](https://i.ytimg.com/vi/6s5QLmAc7gM/default.jpg)](https://natalie.mu/comic/gallery/news/510174/media/81880 "「Lupin三世 VS Cat'sEye」第1弾PV")
[![](https://i.ytimg.com/vi/kdeH7Ro0MZw/default.jpg)](https://natalie.mu/comic/gallery/news/510174/media/86737 "森三中キャッツアイも駆け付けた！「Lupin三世VSCat'sEye」ジャパンプレミア  
（森三中猫眼也赶来了!《Lupin三世VSCat'sEye》日本首映）")

## Amazon Original「Lupin三世 VS Cat'sEye」

2023年1月27日（金）よりPrime Videoにて世界独占配信  
2023年1月27日(星期五)起将通过Prime Video全球独家放送  

### Staff

原作：[Monkey Punch](https://natalie.mu/comic/artist/1749)『Lupin三世』/[北条司](https://natalie.mu/comic/artist/2405)『Cat'sEye』  
監督：静野孔文、[瀬下寛之](https://natalie.mu/comic/artist/92064)  
脚本：葛原秀治  
副監督：井手惠介  
Character Design：中田春彌、山中純子  
Production Design：田中直哉、フェルディナンド・パトゥリ  
Art Director：片塰満則  
編集：肥田文  
音響監督：清水洋史  
音楽：大野雄二、大谷和夫、fox capture plan  
Animation制作：トムス・エンタテインメント  
Animation制作協力：萌  
製作：Lupin三世 VS Cat'sEye製作委員会

### Cast

Lupin三世：[栗田貫一](https://natalie.mu/comic/artist/6789)  
次元大介：大塚明夫  
石川五ェ門：浪川大輔  
峰不二子：沢城みゆき  
銭形警部：山寺宏一  
来生瞳：[戸田恵子](https://natalie.mu/comic/artist/16682)  
来生泪：深見梨加  
来生愛：坂本千夏  
内海俊夫：安原義人  
永石：麦人  
銀河万丈  
東地宏樹  
菅生隆之



## 読者の反応


[Comment](https://natalie.mu/comic/news/510174/comment)



---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处