https://natalie.mu/comic/news/321507

# 劇場版「シティーハンター」“もっこり”応援上映開催、週替わり特典に海坊主も

2019年2月26日  [Comment](https://natalie.mu/comic/news/321507/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」の「“もっこり”かけ声応援上映会」が、3月7日に東京・TOHOシネマズ新宿にて開催される。  
根据北条司原作改编的动画电影《剧场版城市猎人<新宿Private Eyes>》，将于3月7日在东京·TOHO Cinemas新宿举行“Mokkori”应援上映会”。

[
![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut_main.jpg?impolicy=hq&imwidth=730&imdensity=1)
「劇場版シティーハンター <新宿プライベート・アイズ>」より。  
大きなサイズで見る（全18件）  
出自《剧场版城市猎人<新宿Private Eyes>》。  
查看大图(共18件)  
](https://natalie.mu/comic/gallery/news/321507/1114535)

[
![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut02.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター <新宿プライベート・アイズ>」より。［拡大］
出自《剧场版城市猎人<新宿Private Eyes>》。[扩大]
](https://natalie.mu/comic/gallery/news/321507/1114536)

「“もっこり”かけ声応援上映会」は、劇中で冴羽リョウが叫ぶ「もっこり」というフレーズをともに叫ぶなど、上映中の発声やサイリウムの使用が可能な上映会。冴羽リョウ役の[神谷明](https://natalie.mu/comic/artist/60997)も登壇する。チケット販売方法や上映時間は後日発表される。  
观众可以在「“Mokkori”应援上映会」放映过程中喊出"Mokkori"这句话，这句话是由冴羽獠在电影中喊出的，同时观众还可使用荧光棒。饰演冴羽獠的神谷明也将登台。售票方法和上映时间将在日后发表。

[
![「劇場版シティーハンター <新宿プライベート・アイズ>」の「週替わり来場者特典 3弾」トレーディングカード5枚セット。](https://ogre.natalie.mu/media/news/eiga/2019/0226/CH-movie_tokuten_3_sugatami.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター <新宿プライベート・アイズ>」の「週替わり来場者特典 3弾」トレーディングカード5枚セット。［拡大］
“剧场版城市猎人<新宿Private Eyes>”的“每周更换参观者特典3弹”的5张交易卡套装。[扩大]  
](https://natalie.mu/comic/gallery/news/321507/1114528)

また3月1日より上映がスタートする4DX版、MX4D版の来場者特典として、それぞれ絵柄が異なるポストカードが配布されることも明らかに。併せて3月2日から配布される「週替わり来場者特典 第3弾」のトレーディングカード5枚セットの絵柄も公開された。第3弾では野上冴子役・[一龍斎春水](https://natalie.mu/comic/artist/107953)、海坊主役・[玄田哲章](https://natalie.mu/comic/artist/19455)、美樹役・[小山茉美](https://natalie.mu/comic/artist/19454)のサインとコメントがプリントされたカードも用意されている。特典はいずれもなくなり次第終了となるので注意しよう。  
另外，从3月1日开始上映的4DX版、MX4D版的参观者特典，将分别发放图案不同的明信片。同时还公开了将于3月2日开始发放的“每周更换到场者特典第3弹”的5张交换卡套装图案。第3弹还准备了印有野上冴子演员一龙斋春水、海坊主演员玄田哲章、美树演员小山茉美的签名和评语的卡片。要注意的是优惠都是售完即止。  

(译注：4D指观影时的体感效果，它在3D效果的基础上增加体感效果，例如：（看电影时）有时椅子会随着画面一起动、有时天花板会喷水。4DX和MX4D是制作4D效果的两个公司。4DX是韩国公司CJ旗下的品牌，是世界上第一个把电影做成4D效果的公司。MX4D动感特效影厅 (MX4DMOTION EFX THEATRE) 是由美国公司MediaMation开发的4D影院技术。摘自[4D、4DX、MX4D、IMAX](https://www.douban.com/note/741626485/)，[4DX和MX4D有什么区别？](https://www.zhihu.com/question/43235739) )

なおコミックナタリーでは「劇場版シティーハンター <新宿プライベート・アイズ>」の特集を展開。冴羽リョウ役の神谷明と原作者の北条司にインタビューし、約20年ぶりの冴羽リョウとの再会や、映画の見どころなどについて語ってもらった。  
另外，Comic Natalie还将推出“剧场版城市猎人<新宿Private Eyes>”的特集。采访了冴羽獠的扮演者神谷明和原作者北条司，请他们谈了约20年后与冴羽獠的再会，以及电影的看点等。  



この記事の画像（全18件）  
这篇报道的图片(共18张)  

[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut_main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114535 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114536 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」の「週替わり来場者特典 3弾」トレーディングカード5枚セット。](https://ogre.natalie.mu/media/news/eiga/2019/0226/CH-movie_tokuten_3_sugatami.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114528 "「劇場版シティーハンター <新宿プライベート・アイズ>」の「週替わり来場者特典 3弾」トレーディングカード5枚セット。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」の4DX版、MX4D版特典のポストカード。](https://ogre.natalie.mu/media/news/eiga/2019/0226/CH-movie_4d_tokuten.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114529 "「劇場版シティーハンター <新宿プライベート・アイズ>」の4DX版、MX4D版特典のポストカード。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114537 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114538 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114539 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114540 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114541 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114542 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114543 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114544 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114545 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114546 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114547 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut13.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114548 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/eiga/2019/0226/cityhunter_cut14.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1114549 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」ビジュアル](https://ogre.natalie.mu/media/news/comic/2019/0201/ch_bookpass01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/321507/1100074 "「劇場版シティーハンター <新宿プライベート・アイズ>」ビジュアル")

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。  
※冴羽獠的獠，在动物旁正式标记为“僚”。  


(c)北条司/NSP・「2019 劇場版シティーハンター」製作委員会

LINK

[アニメ「劇場版シティーハンター <新宿プライベート・アイズ>」公式Site](https://cityhunter-movie.com/)  
[アニメ「劇場版シティーハンター ＜新宿プライベート・アイズ＞」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)  