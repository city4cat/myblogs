https://natalie.mu/comic/news/103984

# 北条司「エンジェル・ハート」7巻記念でカレンダー当たる

2013年11月20日  [Comment](https://natalie.mu/comic/news/103984/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「エンジェル・ハート 2ndシーズン」7巻が、本日11月20日に発売された。これを記念し、「エンジェル・ハート特製カレンダー 2014年版」が抽選で100名に当たるプレゼントキャンペーンが実施されている。  
北条司《Angel Heart第二季》第七卷于11月20日发售。为了纪念这一活动，Angel Heart特制日历2014年版正在进行抽奖活动，抽取100人。

[
![「エンジェル・ハート特製カレンダー 2014年版」](https://ogre.natalie.mu/media/news/comic/2013/1120/ahcalender.jpg?imwidth=468&imdensity=1)
「エンジェル・ハート特製カレンダー 2014年版」  
大きなサイズで見る（全3件）  
“Angel Heart特制日历2014年版”  
查看大图(共3件)  
](https://natalie.mu/comic/gallery/news/103984/216110)

このカレンダーは北条が今年、実際に仕事場で使用していたというカレンダーをベースに制作されたもの。広げるとB4サイズになる壁掛けタイプで、予定も多く書き込める実用的な一品に仕上がっている。もちろん北条のカラーイラストも多数使用された。  
这个日历是以北条今年在工作场所实际使用的日历为基础制作的。展开的话是B4尺寸的壁挂式、能写很多时间表的实用款。当然北条的彩色插图也被大量使用。

プレゼントの申し込みに必要な応募券は、「エンジェル・ハート 2ndシーズン」7巻、同じく本日発売の北条原作による[阿左維シン](https://natalie.mu/comic/artist/8943)・中目黒さくら「キャッツ▼愛」（▼はハートマーク）7巻、11月25日発売の月刊コミックゼノン2014年1月号（徳間書店）にそれぞれ付属している。応募にはそのうち2枚が必要だ。締め切りは2014年1月6日。  
申请礼物所需的应征券是《Angel Heart”第二季》第7卷，同样是今天发售的北条原作阿左维新·中目黑樱《Cat's▼爱》（▼是Heart mark）第7卷，11月25日发售的月刊Comic Zenon 2014年1月号(德间书店)分别附赠有。应征需要其中两张。截止日期为2014年1月6日。  

この記事の画像（全3件）  
这篇报道的图片(共3篇)  

[![「エンジェル・ハート特製カレンダー 2014年版」](https://ogre.natalie.mu/media/news/comic/2013/1120/ahcalender.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/103984/216110 "「エンジェル・ハート特製カレンダー 2014年版」")
[![「エンジェル・ハート 2ndシーズン」7巻](https://ogre.natalie.mu/media/news/comic/2013/1120/ah2nd7.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/103984/216111 "「エンジェル・ハート 2ndシーズン」7巻")
[![「キャッツ▼愛」7巻](https://ogre.natalie.mu/media/news/comic/2013/1120/cats7.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/103984/216112 "「キャッツ▼愛」7巻")


LINK

[【エンジェル・ハート最新7巻!!】11月20日発売!! | コミックゼノン編集部ブログ](http://www.comic-zenon.jp/blog/?p=4611)  
[コミックゼノン｜「ANGEL HEART 2ndシーズン」](http://www.comic-zenon.jp/magazine/angelheart.html)  
[コミックゼノン｜「キャッツ♥愛」](http://www.comic-zenon.jp/magazine/catseye.html)  

関連商品

[
![北条司「エンジェル・ハート 2ndシーズン（7）」](https://images-fe.ssl-images-amazon.com/images/I/51XwXaXIBbL._SS70_.jpg)
北条司「エンジェル・ハート 2ndシーズン（7）」  
2013年11月20日発売 / 徳間書店 / 978-4199801709  
Amazon.co.jp [書籍版 / Kindle版]  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801707/nataliecomic-22)

[
![阿左維シン,北条司,中目黒さくら「キャッツ▼愛（7）」](https://images-fe.ssl-images-amazon.com/images/I/51GgqrbPNjL._SS70_.jpg)
阿左維シン,北条司,中目黒さくら「キャッツ▼愛（7）」  
[書籍] 2013年11月20日発売 / 徳間書店 / 978-4199801730  
Amazon.co.jp [書籍版 / Kindle版]  
](https://www.amazon.co.jp/exec/obidos/ASIN/4199801731/nataliecomic-22)