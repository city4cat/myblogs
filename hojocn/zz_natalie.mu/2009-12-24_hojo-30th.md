https://natalie.mu/comic/news/25605


# 北条司30周年、新條まゆ・井上雄彦らがトリビュート絵贈呈
北条司30周年，新條まゆ、井上雄彦等人赠送纪念画

2009年12月24日 [Comment](https://natalie.mu/comic/news/25605/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)のデビュー30周年を記念して、ゲストマンガ家が北条作品のキャラクターを描く「北条司トリビュート・ピンナップ」が本日12月24日発売の週刊コミックバンチ2010年4・5合併号（新潮社）に収録されている。  
为纪念北条司出道30周年，特邀漫画家绘制北条作品角色的「北条司Tribute Pin-Up」收录于12月24日发售的周刊Comic Bunch 2010年4·5合并号(新潮社)。

[
![「北条司トリビュート・ピンナップ」が収録された週刊コミックバンチ2010年4・5合併号。](https://ogre.natalie.mu/media/news/comic/2009/1224/bunch10-4to5.jpg?imwidth=468&imdensity=1)
「北条司トリビュート・ピンナップ」が収録された週刊コミックバンチ2010年4・5合併号。  
大きなサイズで見る  
收录了「北条司Tribute Pin-Up」的漫画周刊2010年4·5合并号。  
查看大图  
](https://natalie.mu/comic/gallery/news/25605/37343)

同企画に参加したマンガ家は[井上雄彦](https://natalie.mu/comic/artist/1791)、[梅澤春人](https://natalie.mu/comic/artist/2349)、[朝基まさし](https://natalie.mu/comic/artist/2260)、[のりつけ雅春](https://natalie.mu/comic/artist/1725)、[新條まゆ](https://natalie.mu/comic/artist/2109)、[原哲夫](https://natalie.mu/comic/artist/1917)の6名。新條の描く耽美で儚げな「冴羽りょう」など、各作家の個性と融合を果たした北条ワールドは必見だ。  
参与该企划的漫画家有井上雄彦、梅泽春人、朝基まさし、のりつけ雅春、新條まゆ、原哲夫 6人。新条描绘的唯美梦幻的《冴羽獠》等，融合了各作家个性的北条世界是必看的。

さらに吉祥寺のカフェ「CAFE ZENON」では、本日12月24日から12月27日までの4日間「北条司と仲間たち サプライズ原画展」を開催する。同原画展では「北条司トリビュート・ピンナップ」の原画も展示。さらに週刊コミックバンチ4・5合併号を持って来店した人には、先着200名で「北条司特製ポストカード」がプレゼントされる。  
此外，吉祥寺的咖啡店“CAFE ZENON”从12月24日到12月27日将举办为期4天的“北条司和伙伴们 Surprise原画展”。该原画展还展示了「北条司Tribute Pin-Up」的原画。另外，持周刊Comic Bunch 4·5合并号来店的顾客，前200名将获赠“北条司特制明信片”。



LINK  
[週刊Comic Bunch☆Coamix：News Release](http://www.comicbunch.com/news_release/hojo-exhibition.php)  
[Comic Natalie - 吉祥寺に新名所、マンガを感じる店「CAFE ZENON」誕生](http://natalie.mu/comic/news/show/id/23473)  

