https://natalie.mu/comic/news/314198

# シティーハンター音楽特番でTMN・木根、やついいちろう、神谷明が「Get Wild」

2018年12月28日 [Comment](https://natalie.mu/comic/news/314198/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」を題材にしたアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」の公開を記念した特番が、2019年1月4日に放送されることが決定した。  
以北条司的《城市猎人》为题材的动画电影《剧场版城市猎人<新宿Private  Eyes>》的公映纪念特别节目将于2019年1月4日播出。

[
![「ミュージック・シティーハンター」より、左から神谷明、木根尚登（TM NETWORK）、やついいちろう。](https://ogre.natalie.mu/media/news/comic/2018/1228/CH_music.jpg?impolicy=hq&imwidth=730&imdensity=1)
「ミュージック・シティーハンター」より、左から神谷明、木根尚登（TM NETWORK）、やついいちろう。  
大きなサイズで見る（全3件）  
《Music CityHunter》从左至右依次为神谷明、木根尚登(TM NETWORK)、八木一郎。  
用大尺寸查看(共3件)
](https://natalie.mu/comic/gallery/news/314198/1080377)

[
![「劇場版シティーハンター <新宿プライベート・アイズ>」の本ポスター。](https://ogre.natalie.mu/media/news/comic/2018/1212/cityhunter01.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター <新宿プライベート・アイズ>」の本ポスター。［拡大］  
《剧场版城市猎人<新宿Private Eyes>》的海报。[扩大]  
](https://natalie.mu/comic/gallery/news/314198/1069398)

「ミュージック・シティーハンター」と題された今回の特番では、TVアニメ「シティーハンター」の世界を彩った数多くの楽曲を音楽番組風に紹介。[エレキコミック](https://natalie.mu/comic/artist/6532)の[やついいちろう](https://natalie.mu/comic/artist/55652)がMCを務め、[TM NETWORK](https://natalie.mu/comic/artist/1270)の[木根尚登](https://natalie.mu/comic/artist/18633)がアニメのエンディングテーマ「Get Wild」誕生秘話を語るほか、ゲストとして冴羽リョウ役の神谷明も登場する。番組内では、TM NETWORKの宇都宮隆からのコメント映像や、アニメの名シーン紹介などのコーナーが用意され、エンディングではやつい・木根・神谷による「Get Wild」も披露される。  
以“Music CityHunter”为题的这次的特别节目，以音乐节目的风格介绍了在TV动画“城市猎人”的世界中增色的众多乐曲。Electric Comic的八木一郎将担任MC，TM NETWORK的木根尚登将讲述动画片尾曲《Get Wild》的诞生秘闻，冴羽獠的声优神谷明也将作为嘉宾登场。在节目中，还准备了来自TM NETWORK的宇都宫隆的评论影像、动画的名场景介绍等环节，在结尾还披露了早井·木根·神谷的《Get Wild》。  

映画「劇場版シティーハンター <新宿プライベート・アイズ>」は2019年2月8日ロードショー。神谷、伊倉一恵らTVアニメのキャストが再集結し、映画のために書き下ろされたオリジナルストーリーが展開される。  
电影《剧场版城市猎人<新宿Private Eyes>》将于2019年2月8日上映。神谷、伊仓一惠等TV动画的Cast再次集结，为电影谱写的原创故事即将展开。  

この記事の画像・動画（全3件）  
这篇报道的图片、视频(共3篇)  

[![「ミュージック・シティーハンター」より、左から神谷明、木根尚登（TM NETWORK）、やついいちろう。](https://ogre.natalie.mu/media/news/comic/2018/1228/CH_music.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/314198/1080377 "「ミュージック・シティーハンター」より、左から神谷明、木根尚登（TM NETWORK）、やついいちろう。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」の本ポスター。](https://ogre.natalie.mu/media/news/comic/2018/1212/cityhunter01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/314198/1069398 "「劇場版シティーハンター <新宿プライベート・アイズ>」の本ポスター。")
[![「劇場版シティーハンター ＜新宿プライベート・アイズ＞」本予告 | 2019年2月8日(金)全国ロードショー](https://i.ytimg.com/vi/CRFrmaZa1OY/default.jpg)](https://natalie.mu/comic/gallery/news/314198/media/34932 "「劇場版シティーハンター ＜新宿プライベート・アイズ＞」本予告 | 2019年2月8日(金)全国ロードショー（“城市猎人剧场版<新宿プライベート・アイズ>”书预告| 2019年2月8日(金)在日本全国上映）")  

## 「ミュージック・シティーハンター」
《Music CityHunter》  

放送日時：2019年1月4日（金）24:00～24:30  
放送局：TOKYO MX、群馬テレビ、とちぎテレビ、BS11
放送局:TOKYO MX、群马电视台、土岐电视台、BS11  

## 映画「劇場版シティーハンター <新宿プライベート・アイズ>」
电影《剧场版城市猎人<新宿Private Eyes>》  

2019年2月8日（金）公開

### Staff

原作：[北条司](https://natalie.mu/comic/artist/2405)  
総監督：こだま兼嗣  
脚本：加藤陽一  
チーフ演出：佐藤照雄、京極尚彦  
キャラクターデザイン：高橋久美子  
総作画監督：菱沼義仁  
美術監督：加藤浩  
色彩設計：久保木裕一  
音響監督：長崎行男  
音響制作：AUDIO PLANNING U  
音楽：岩崎琢  
編集：今井大介（JAYFILM）  
Animation制作：sunrise  
配給：aniplex  

### Cast

冴羽リョウ：神谷明  
槇村香：伊倉一恵  
進藤亜衣：飯豊まりえ  
御国真司：山寺宏一  
野上冴子：一龍斎春水  
海坊主：玄田哲章  
美樹：小山茉美  
ヴィンス・イングラード：大塚芳忠  
徳井義実

※リョウの漢字はけものへんに「僚」のつくりが正式表記。  
※岩崎琢の琢は旧字体が正式表記。  
※两字的汉字在兽的旁边“僚”的构造是正式表记。  
※岩崎琢的琢是旧字体正式标记。  



(c)北条司/NSP・「2019 劇場版シティーハンター」製作委員会

LINK

[アニメ「劇場版シティーハンター」公式サイト](http://cityhunter-movie.com/)  
[「劇場版シティーハンター」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)  