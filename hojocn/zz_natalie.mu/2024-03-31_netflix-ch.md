https://natalie.mu/comic/news/567389  
https://natalie.mu/eiga/news/567299  

# 映画「シティーハンター」新たな場面写真、冴羽リョウが馬の頭を股間に生やし大暴れ
电影“城市猎人”新场景照片，冴羽獠在胯下长出马头  

2024年3月31日 18:00 930

[北条司](https://natalie.mu/comic/artist/2405)原作によるNetflix映画「シティーハンター」の新たな場面写真が公開された。  
北条司原作的Netflix电影《城市猎人》的新场景照片公开了。  

[![Netflix映画「シティーハンター」場面写真](https://ogre.natalie.mu/media/news/comic/2024/0331/CityHunter_0331_02.jpg?impolicy=hq&imwidth=730&imdensity=1)
Netflix映画「シティーハンター」場面写真  
大きなサイズで見る（全20件）  
Netflix电影《城市猎人》剧照  
大尺寸查看（共20篇）  
](https://natalie.mu/comic/gallery/news/567389/2285119)

[Netflix映画「シティーハンター」場面写真［拡大］  
Netflix电影《城市猎人》剧照[放大]  
](https://natalie.mu/comic/gallery/news/567389/2285117)

[Netflix映画「シティーハンター」場面写真［拡大］  
Netflix电影《城市猎人》剧照[放大]  
](https://natalie.mu/comic/gallery/news/567389/2285120)

新たに公開されたのは、冴羽リョウの力強い姿やコメディチックなシーンを収めた4枚。XYZと書かれた新宿東口の古い伝言板の前に佇むクールな姿から、ブーメランパンツ1枚での肉体美、股間に巨大な馬の首を生やしながら暴れているシーンなど、冴羽リョウ役の[鈴木亮平](https://natalie.mu/comic/artist/50666)の演技の幅を楽しめる。Netflix映画「シティーハンター」は4月25日に配信開始。キャストには、鈴木のほか、槇村香役に[森田望智](https://natalie.mu/comic/artist/68469)、槇村秀幸役に[安藤政信](https://natalie.mu/comic/artist/19383)、野上冴子役に[木村文乃](https://natalie.mu/comic/artist/48878)が名を連ねている。  
新公开的是收录了冴羽獠强有力的身姿和充满喜剧色彩的场景的4张。从站在写着XYZ的新宿东口的旧留言板前的酷酷模样，到穿着一条游隼裤的健美身材，以及胯部长出巨大马头发狂的场景等，可以欣赏到饰演冴羽獠的铃木亮平演技的幅度。Netflix电影《城市猎人》将于4月25日上映。演员阵容方面，除铃木外，槙村香由森田望智饰演，槙村秀幸由安藤政信饰演，野上冴子由木村文乃饰演。  

この記事の画像・動画（全20件）  
本文图片视频（共20篇）  

-   [](https://natalie.mu/comic/gallery/news/567389/2285119 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2285117 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2285120 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2285122 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272443 "Netflix映画「シティーハンター」ティザーアート")
-   [](https://natalie.mu/comic/gallery/news/567389/2272445 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272446 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272448 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272452 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272449 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272444 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272447 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272450 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2272451 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/comic/gallery/news/567389/2247357 "Netflix映画「シティーハンター」ビジュアル")
-   [](https://natalie.mu/comic/gallery/news/567389/2257099 "Netflix映画「シティーハンター」より。左から安藤政信扮する槇村秀幸、鈴木亮平扮する冴羽リョウ、森田望智扮する槇村香、木村文乃演じる野上冴子。")
-   [](https://natalie.mu/comic/gallery/news/567389/2247358 "Netflix映画「シティーハンター」より、鈴木亮平扮する冴羽リョウ。")
-   [](https://natalie.mu/comic/gallery/news/567389/2247359 "Netflix映画「シティーハンター」より、森田望智扮する槇村香。")
-   [](https://natalie.mu/comic/gallery/news/567389/media/102891 "Netflix映画『シティーハンター』最新トレーラー映像")
-   [](https://natalie.mu/comic/gallery/news/567389/media/101413 "2024年 Netflixが贈る実写の注目作品 - Netflix")


## Netflix映画「シティーハンター」  

2024年4月25日（木）より、Netflix にて全世界独占配信  

原作：[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」  
出演：[鈴木亮平](https://natalie.mu/comic/artist/50666)、[森田望智](https://natalie.mu/comic/artist/68469)、[安藤政信](https://natalie.mu/comic/artist/19383)、[木村文乃](https://natalie.mu/comic/artist/48878)  
監督：[佐藤祐市](https://natalie.mu/comic/artist/47028)  
脚本：三嶋龍朗  
エグゼクティブ・プロデューサー：高橋信一（Netflix）  
プロデューサー：三瓶慶介、押田興将  
制作：ホリプロ、オフィス・シロウズ  
製作：Netflix  
原作協力：コアミックス

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。






# カウボーイハットのリョウが大暴れ！鈴木亮平主演「シティーハンター」新場面写真  
牛仔帽的獠大暴走！铃木亮平主演《城市猎人》新剧照  

2024年3月31日 18:00 

[鈴木亮平](https://natalie.mu/eiga/artist/50666)が主演するNetflix映画「シティーハンター」の新たな場面写真が到着した。  
铃木亮平主演的Netflix电影《城市猎人》的最新剧照已经发布。  

[![Netflix映画「シティーハンター」新場面写真](https://ogre.natalie.mu/media/news/eiga/2024/0330/cityhunter_202403_01.jpg?impolicy=hq&imwidth=730&imdensity=1)
Netflix映画「シティーハンター」新場面写真  
大きなサイズで見る（全18件）  
Netflix电影《城市猎人》新剧照  
大尺寸查看（共18个）  
](https://natalie.mu/eiga/gallery/news/567299/2284740)  

[Netflix映画「シティーハンター」新場面写真［拡大］  
Netflix电影《城市猎人》新场景图片［放大］   
](https://natalie.mu/eiga/gallery/news/567299/2284741)

[北条司](https://natalie.mu/eiga/artist/2405)のマンガ「シティーハンター」を日本で初めて実写化した本作。無類の女好きだが超一流なスイーパー・冴羽リョウを鈴木が演じ、リョウの新たなパートナー・香に[森田望智](https://natalie.mu/eiga/artist/68469)、リョウを支える相棒・槇村秀幸に[安藤政信](https://natalie.mu/eiga/artist/19383)、警視庁の敏腕刑事・野上冴子に[木村文乃](https://natalie.mu/eiga/artist/48878)が扮する。  
本片是北条司的漫画《城市猎人》首次在日本拍摄真人版。无比喜欢女色但却是超一流清道夫·冴羽獠由铃木饰演，獠的新搭档·香由森田望智饰演，支持獠的搭档·槙村秀幸由安藤政信饰演，警视厅的干练刑警·野上冴子由木村文乃饰演。  

[Netflix映画「シティーハンター」新場面写真［拡大］  
Netflix电影《城市猎人》新场景图片［放大］ 
](https://natalie.mu/eiga/gallery/news/567299/2284743)  

このたび新たに解禁された場面写真は4点。リョウがXYZと書かれた新宿東口の伝言板の前に立つさまや、“もっこりダンス”を披露する様子、またコスプレ会場でカウボーイハットを被り暴れる姿などが切り取られている。  
这次新解禁的场景照片有4张。獠站在写着XYZ的新宿东口留言板前的样子，表演“Mokkori舞”的样子，还有在cosplay会场戴着牛仔帽发狂的样子等都被拍了下来。  

[Netflix映画「シティーハンター」新場面写真［拡大］  
Netflix电影《城市猎人》新场景图片［放大］ 
](https://natalie.mu/eiga/gallery/news/567299/2284742)  

鈴木は「原作やアニメでリョウがあれだけふざけられるのは、香が制裁をしてくれる前提があるからなんです。けれど本作で描かれる香はまだリョウのパートナーではない。槇村を亡くした悲しみや緊張感もまだ大きい段階でただふざけているだけのキャラになってしまうことは避けたかったし、どうやっておふざけシーンを挿入していくのかはたくさん話し合いました。だからこそ、劇中の『もっこりダンス』と『馬もっこり』はこの作品で最も重要なシーンともいえます。いわば『シティーハンター』の真骨頂です」とコメントしている。  
铃木说：“原作和动画中獠被那样戏弄，是以香制裁他为前提的。但是本作中描绘的香还不是獠的搭档。在槙村去世的悲伤和紧张感还很强烈的阶段，我想避免角色变成只是开玩笑的角色，如何插入开玩笑的场景，我们讨论了很多。正因为如此，剧中的“Mokkori舞”和“马Mokkori”可以说是这部作品中最重要的场景，也可以说是《城市猎人》的精髓所在”。  

Netflix映画「シティーハンター」は4月25日に全世界で配信スタート。「ストロベリーナイト」シリーズの[佐藤祐市](https://natalie.mu/eiga/artist/47028)が監督、三嶋龍朗が脚本を担当した。  
Netflix电影《城市猎人》将于4月25日在全球上映。《草莓之夜》系列的导演佐藤祐市担任导演，三岛龙朗担任编剧。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  


この記事の画像・動画（全18件）

-   [](https://natalie.mu/eiga/gallery/news/567299/2284740 "Netflix映画「シティーハンター」新場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2284741 "Netflix映画「シティーハンター」新場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2284743 "Netflix映画「シティーハンター」新場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2284742 "Netflix映画「シティーハンター」新場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272460 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272465 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272469 "Netflix映画「シティーハンター」ティザーアート")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272464 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272466 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272461 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272462 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272463 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272467 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/2272468 "Netflix映画「シティーハンター」場面写真")
-   [](https://natalie.mu/eiga/gallery/news/567299/1959485 "鈴木亮平演じる冴羽リョウ。")
-   [](https://natalie.mu/eiga/gallery/news/567299/1987260 "森田望智演じる槇村香。")
-   [](https://natalie.mu/eiga/gallery/news/567299/2257060 "「シティーハンター」キャラクタービジュアル")
-   [](https://natalie.mu/eiga/gallery/news/567299/media/102891 "Netflix映画『シティーハンター』最新トレーラー映像")

