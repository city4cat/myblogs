https://natalie.mu/eiga/news/335127

# 松下奈緒＆ディーンが現場で語る、北条司の絵コンテに「冴羽リョウがいる！」

2019年7月1日  [Comment](https://natalie.mu/eiga/news/335127/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


「シティーハンター」で知られるマンガ家・[北条司](https://natalie.mu/eiga/artist/2405)が総監督を務める実写映画「[エンジェルサイン](https://natalie.mu/eiga/film/182396)」。[松下奈緒](https://natalie.mu/eiga/artist/11381)と[ディーン・フジオカ](https://natalie.mu/eiga/artist/65834)が主演を務める本作の撮影現場に映画ナタリーが潜入した。  
以《城市猎人》而闻名的漫画家北条司担任总导演的真人电影《天使印记》。Natalie潜入了由松下奈绪和藤冈靛主演的本作的拍摄现场。


[
![「エンジェルサイン」撮影現場より。左からディーン・フジオカ、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_08.jpg?impolicy=hq&imwidth=730&imdensity=1)
「エンジェルサイン」撮影現場より。左からディーン・フジオカ、松下奈緒。  
大きなサイズで見る（全11件）  
“天使印记”拍摄现场。左起为藤冈靛、松下奈绪。  
查看大图(共11件)
](https://natalie.mu/eiga/gallery/news/335127/1189289)

[
![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」［拡大］
](https://natalie.mu/eiga/gallery/news/335127/1189290)

本作は、セリフを使わない“サイレントマンガ”をもとにした5つの物語に、北条自身がメガホンを取ったオリジナルストーリー「プロローグ」「エピローグ」を加えたオムニバス。映画はサイレントマンガ同様に全編通してセリフはなく、映像と音楽だけでストーリーが展開していく。松下とディーンは、5つの物語をつなぐ役割を持つ「プロローグ」「エピローグ」に出演。それぞれチェリストのアイカ、ピアニストのタカヤという役柄で、若き音楽家として“音楽で世界を感動させる夢”を追う恋人同士を演じた。  
本作品是以不使用台词的“默白漫画”为基础的5个故事，加上北条自己掌镜的原创故事“序幕”“尾声”的集锦。电影和默白漫画一样，全篇没有台词，仅靠影像和音乐展开故事。松下和靛将出演负责连接5个故事的《序幕》和《尾声》。两人分别饰演大提琴家伊卡、钢琴家高也，演绎了年轻音乐家追求“用音乐感动世界的梦想”的一对恋人。

[
![「エンジェルサイン」の絵コンテ台本。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_13.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」の絵コンテ台本。［拡大］
](https://natalie.mu/eiga/gallery/news/335127/1189292)

通常の文字台本は用意されておらず、現場は北条自ら描いた絵コンテを指針にワンカットワンカット撮り進めていく。北条は「あくまでガイド」とその役割を語るものの、マンガの1コマを抜き出したかのように細部まで描き込まれた絵コンテ。絵柄はもちろん北条マンガに登場するキャラクターを想起させ、マンガならではのオノマトペや、人物が驚いたときに見せる目が点といった表情も再現されていた。また「エンジェルサイン」では、実写映画の現場では珍しいビデオコンテも制作されたという。  
没有准备通常的文字脚本，现场以北条自己画的分镜为指导一个镜头一个镜头地拍摄。北条虽然谈到了“只是向导”的作用，不过，像抽出漫画的1帧一样描绘了细节的分镜。画风当然会让人想起北条漫画中登场的角色，漫画中特有的人物形象，以及人物吃惊时露出的眼睛点等表情也被再现出来。另外，“天使印记”还制作了真人电影现场少见的录像镜头。  

[
![](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_05.jpg?imwidth=468&imdensity=1)
北条司［拡大］
](https://natalie.mu/eiga/gallery/news/335127/1189291)

[
![](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_11.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」の撮影現場に貼られていた絵コンテの抜粋。［拡大］  
贴在“天使印记”拍摄现场的分镜摘录。[扩大]  
](https://natalie.mu/eiga/gallery/news/335127/1189293)

カメラの動きやカット割り、構図もすべてが組み立てられた絵コンテの執筆について、北条は「マンガのネームを描く作業に近い。だから絵コンテを描き上げた段階でもうやることはない。（現場を）見てるだけです」と笑いつつ、実際に“画が動く”映画と決して動き出すことはないマンガの制作過程を比較する。そして「よく言われることですが、マンガを描いてるときも頭の中ではキャラクターが演技をしてるんです。マンガ家はその動きの最適な瞬間を1コマで切り取っています」と言及した。  
在编写故事板时，所有的摄影机动作、剪辑分工和构图都被集合在一起，北条说“这类似于为漫画绘制故事板的过程，所以画完分镜的阶段已经没有要做的事了。(现场)只是看而已”，并将“画面会动”的电影和绝对不会动的漫画制作过程进行比较。他还说:“经常有人说，画漫画的时候，角色在脑海中进行着表演。漫画家会把那个动作最合适的瞬间用一个画面剪下来。”  

[
![左からディーン・フジオカ、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_06.jpg?imwidth=468&imdensity=1)
左からディーン・フジオカ、松下奈緒。［拡大］  
左起为藤冈靛、松下奈绪。[扩大]  
](https://natalie.mu/eiga/gallery/news/335127/1189294)

最初に台本を読んだ印象を、松下は「北条先生の絵だ！と思って（笑）。台本もすべてが『シティーハンター』に見えてくるし、すごく不思議な気持ちでワクワクしました。少し冷静になってから読むと作品の世界観が手に取るようにわかったんです」とコメント。ディーンも「スラスラ一気読みしてしまいました。セリフがなくても文脈がつかめて、行間に何が表現されてるかわかりやすい」と振り返りつつ、「最初にタカヤを見たとき、冴羽リョウがいる！と思いました。僕の世代の男性だったら、冴羽リョウみたいな男になりたいというボーイズドリームをみんな持っていた。こうして北条先生の作品の一部として参加できるのはとても不思議で光栄なこと」と出演の喜びを語った。  
对于最初读剧本的印象，松下说:“是北条老师的画!这样想的(笑)。剧本也全部看起来像《城市猎人》，以非常不可思议的心情欢欣雀跃。我可以把握住作品的世界”。迪恩也回顾道“一口气读完了，即使没有台词也能抓住文脉，字里行间表现了什么也很容易理解”，“第一次看到高也的时候，就看到了冴羽獠!如果是我这一代的男性，大家都有想成为冴羽獠那样的男人的男孩梦。能像这样作为北条老师的作品的一部分参加是非常不可思议和光荣的事”说出了出演的喜悦。

この日撮影されていたのは、タカヤがアイカに指輪を渡しプロポーズする「エピローグ」の場面だ。清楚な雰囲気を漂わせる白いセーターを着たアイカと、青色のシャツで爽やかな印象のタカヤ。グランドピアノが中央に置かれたマンションの一室は、間接照明の温かい光に包まれていた。松下は衣装合わせの際、北条から言われた「着たいものを着たらいい。自分のイメージに合うものを選んだらいいから」という言葉が印象に残っているそうで「そこから撮影がもっともっと楽しみになりました」と振り返る。  
当天拍摄的是高也把戒指交给伊卡求婚的“尾声”场面。伊卡穿着散发清新气息的白色毛衣，高也穿着蓝色衬衫给人清爽的印象。公寓的一个房间中央放着一架大钢琴，被间接照明的温暖光线包围着。松下在试装的时候，北条说了“穿想穿的就好。只要选择符合自己形象的东西就可以了”这句话给她留下了深刻的印象，她回忆道“从那以后就更加期待拍摄了”。

[
![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」［拡大］
](https://natalie.mu/eiga/gallery/news/335127/1189295)

タカヤが生み出した楽曲「エンジェルサイン」が物語の鍵を握る本作。それぞれミュージシャンの一面もあわせ持つ松下とディーン。本作でも猛特訓の末、吹替なしでチェロとピアノの演奏を披露している。松下は「お互い音楽を愛していて、アイカはタカヤの作った音楽を体に入れて紡ぎ出す。すごく美しい愛の形」と2人の関係性を紹介しながら、「だから意地でも自分で弾きたかった。音楽をやっている身としてそこだけは譲りたくなかった」と熱いまなざし。  
高也创作的乐曲“天使印记”在本片的故事中起到了关键作用。松下和靛分别有音乐家的一面。在本作品中，他也经过了艰苦的特训，展示了演奏大提琴和钢琴。松下在介绍两人的关系时表示:“彼此都爱着音乐，伊卡把高也创作的音乐融入身体中编织出来。这是一种非常美丽的爱的形式。”作为做音乐的人只有这一点不想让步”。  

[
![左からディーン・フジオカ、北条司、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_09.jpg?imwidth=468&imdensity=1)
左からディーン・フジオカ、北条司、松下奈緒。［拡大］  
左起为藤冈靛、北条司、松下奈绪。[扩大]  
](https://natalie.mu/eiga/gallery/news/335127/1189296)

一方のディーンは松下の横でピアノを弾くことに多大なプレッシャーを感じていたそうで「どれだけお上手か知ってるから、なんで自分がピアノを弾いてるんだろう？と（笑）。とても練習しがいがありました」と続ける。松下が「最初からバッチリ弾かれてましたよ」と称賛すると、ディーンは「がんばりました」と笑顔で胸を張った。撮影の合間には2人でセッションすることもあり、松下は「やっぱり私たち音楽が好きなんです。楽器が近くにあるとついつい弾いてしまいます」と現場の様子を明かしていた。  
另一方面，靛在松下旁边弹钢琴感到了很大的压力，他继续说:“因为知道松下弹钢琴有多好，所以为什么自己弹钢琴呢?(笑)。非常有练习的意义。”松下称赞说:“从一开始就弹得很好。”靛笑着说:“努力了。”在拍摄的间隙，两人也会进行对话，松下表示:“我们还是喜欢音乐，有乐器在附近的时候就会情不自禁地弹奏。”

「エンジェルサイン」の公開時期やそのほかの物語に関する詳細は続報を待とう。  
关于《天使印记》的公开时间和其他故事的详细内容请等待后续报道。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  
※冴羽獠的獠，在动物旁正式标明“僚”的写法  


この記事の画像・動画（全11件）  
这篇报道的图片、视频(共11篇)  
[![「エンジェルサイン」撮影現場より。左からディーン・フジオカ、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189289 "「エンジェルサイン」撮影現場より。左からディーン・フジオカ、松下奈緒。（‘天使印记’拍摄现场。左起为藤冈靛、松下奈绪。）")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189290 "「エンジェルサイン」")
[![「エンジェルサイン」の絵コンテ台本。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_13.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189292 "「エンジェルサイン」の絵コンテ台本。（“天使印记”的分镜脚本。）")
[![北条司](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189291 "北条司")
[![「エンジェルサイン」の撮影現場に貼られていた絵コンテの抜粋。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189293 "「エンジェルサイン」の撮影現場に貼られていた絵コンテの抜粋。（贴在北条司“天使印记”拍摄现场的分镜摘录。）")
[![左からディーン・フジオカ、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189294 "左からディーン・フジオカ、松下奈緒。")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189295 "「エンジェルサイン」")
[![左からディーン・フジオカ、北条司、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189296 "左からディーン・フジオカ、北条司、松下奈緒。")
[![北条司](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189297 "北条司")
[![「エンジェルサイン」の撮影現場に貼られていた絵コンテの抜粋。](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/335127/1189298 "「エンジェルサイン」の撮影現場に貼られていた絵コンテの抜粋。（贴在北条司“天使印记”拍摄现场的分镜摘录。）")
[![「エンジェルサイン」ティザー映像](https://i.ytimg.com/vi/Sg5E-CGB33o/default.jpg)](https://natalie.mu/eiga/gallery/news/335127/media/39570 "「エンジェルサイン」ティザー映像（“天使印记”teaser映像）")


LINK

[「エンジェルサイン」公式サイト](https://angelsign.jp/)  
[AngelSignMovie (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[「エンジェルサイン」ティザー映像](https://youtu.be/Sg5E-CGB33o)  
