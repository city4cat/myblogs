https://natalie.mu/eiga/news/325470

# 神谷明「CityHunter」応援上映でファンに感謝「夢のまた夢でした」

2019年3月26日  [Comment](https://natalie.mu/eiga/news/325470/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)



「[劇場版CityHunter <新宿Private Eyes>](https://natalie.mu/eiga/film/175572)」の“もっこり”かけ声応援上映会が、本日3月26日に東京・TOHOシネマズ 新宿にて開催。キャストの[神谷明](https://natalie.mu/eiga/artist/60997)と[伊倉一恵](https://natalie.mu/eiga/artist/95562)、原作者の[北条司](https://natalie.mu/eiga/artist/2405)、総監督の[こだま兼嗣](https://natalie.mu/eiga/artist/13476)、プロデューサーの小形尚弘が登壇した。  
“剧场版CityHunter <新宿Private Eyes>”的“Mokkori”宣传应援上映会于3月26日在东京·TOHO Cinemas新宿举行。演员神谷明和伊仓一惠、原作者北条司、总导演回声兼嗣、制片人小形尚弘登台。

[
![「劇場版CityHunter <新宿Private Eyes>」“もっこり”かけ声応援上映会の様子。左から北条司、神谷明、伊倉一恵。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_357101.jpg?impolicy=hq&imwidth=730&imdensity=1)
「劇場版CityHunter <新宿Private Eyes>」“もっこり”かけ声応援上映会の様子。左から北条司、神谷明、伊倉一恵。  
大きなサイズで見る（全16件）  
「剧场版CityHunter <新宿Private Eyes>」“Mokorri”应援上映会的样子。左起北条司、神谷明、伊仓一惠。  
查看大图(共16件)  
](https://natalie.mu/eiga/gallery/news/325470/1133052)

[
![神谷明](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_345801.jpg?imwidth=468&imdensity=1)
神谷明［拡大］
](https://natalie.mu/eiga/gallery/news/325470/1133053)

サイリウムや応援グッズを持った観客から大歓声で迎えられたキャストたち。冴羽リョウ役の神谷は、ライブビューイングのカメラに「もっこりこんばんは！」とアピールすると「生まれてからこんなにたくさん舞台挨拶をした映画はありません。これも、応援してくれた皆さまの熱い思いの結果だと感謝しています。今日も一緒に楽しみましょう！」と挨拶する。槇村香役の伊倉は「神谷さんが『生まれてからこんなに舞台挨拶をしたことない』ということなら、私もそう。びっくりしています」と続け、2人は「それが『CityHunter』なのがうれしいね」とほほえみ合った。  
演员们受到了手持荧光棒和应援物品的观众们的热烈欢呼。饰演冴羽獠的神谷，对着实况转播的镜头说「Mokkori晚上好!」，并表示「出生以来从未有过这么多的电影登台致辞。这也是支持我的大家的热情的结果，对此我表示感谢。今天也一起期待吧!」。饰演槙村香的伊仓说「如果神谷先生说『从出生以来就没有这样在舞台上致辞过』的话，我也一样。我很吃惊」，2人微笑着说「我很高兴那是『CityHunter』」。

[
![左からこだま兼嗣、北条司。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_342608.jpg?imwidth=468&imdensity=1)
左からこだま兼嗣、北条司。［拡大］
](https://natalie.mu/eiga/gallery/news/325470/1133054)

100万人を超える動員を記録したことについて、こだまは「『CityHunter』がこの時代に認知されたことがうれしいです」と笑顔を見せると、「今回は昔のイメージをできるだけ画面に出そうとして、今風の複雑な加工をほとんどしていないんです。コアなファンの方たちのためにそうしようと決めていました」と明かす。リョウが香に出会った3月26日が彼の誕生日とされていることから“リョウちゃんお誕生日記念”と題された本イベント。北条が「3月26日なんて誰も覚えてないと思ったけど、すごいよね」と思わずこぼすと、客席から「覚えてますー！」と次々に声が上がり、北条は「本当にありがたいです。大方の予想を裏切ってヒットしていますが……感無量です」としみじみ述べた。  
关于观影超过100万人的记录，儿玉笑着说「很高兴『CityHunter』在这个时代被认可」，「这次我想尽量把以前的印象呈现在画面上。几乎没有进行现代风格的加工，是为了核心的粉丝们才决定这么做的」。因为獠与香相遇的3月26日是獠的生日，所以本次活动的主题是「獠酱生日纪念」。北条情不自禁地说「我以为谁都不记得3月26日了，好厉害啊」，从观众席有人说「记得--!」，北条感慨地说「真的很感谢。虽然出乎意料地大受欢迎……真是感慨万千。」

[
![伊倉一恵](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_349206.jpg?imwidth=468&imdensity=1)
伊倉一恵［拡大］
](https://natalie.mu/eiga/gallery/news/325470/1133055)

この日の応援上映にも参加していたという神谷と伊倉。応援上映は初めてだったという伊倉が「気の利いたツッコミを入れてくれていて笑っちゃいました。交通事故が起きてしまうシーンで『前向いて！』とか（笑）」と話すと、神谷はうなずきながら「そのあと携帯電話を持ってかれたら『ドロボー！』ってね。これは僕も笑いました。あと、皆さん『ボン・キュッ・ボン』とかシーンを覚えていらっしゃって、それも楽しかったです」と振り返った。  
神谷和伊仓也参加了当天的应援上映。第一次应援上映的伊仓说:“他们机灵的调侃让我笑了。在发生交通事故的场景中，他说‘向前走!’(笑)”。神谷一边点头一边说：“然后他拿着手机说‘dorobo!(译注：小偷）‘，我也笑了。还有，大家都记得『ボン・キュッ・ボン』和那些镜头，所以很开心。”（译注：ボン・キュッ・ボン表示女性丰胸、细腰、肥臀。）

[
![リョウと香の誕生日ケーキ。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_362503.jpg?imwidth=468&imdensity=1)
リョウと香の誕生日ケーキ。［拡大］  
亮和香的生日蛋糕。[扩大]  
](https://natalie.mu/eiga/gallery/news/325470/1133056)

[
![誕生日ケーキと神谷明を撮影をする伊倉一恵（左）。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_354904.jpg?imwidth=468&imdensity=1)
誕生日ケーキと神谷明を撮影をする伊倉一恵（左）。［拡大］  
伊仓一惠(左)拍摄生日蛋糕和神谷明。[扩大]  
](https://natalie.mu/eiga/gallery/news/325470/1133057)

イベントでは、リョウと3月31日が誕生日である香を祝福するケーキが運ばれる場面も。伊倉は率先して記念撮影をすると「あとでTwitterに上げます！」と興奮気味にコメントした。最後に神谷は「どのくらい続いてくれるかな、1週でも長く続いてくれるといいなと願い続けてきました。それが『ドラえもん』と一緒に上映され、おそらくこのあと『コナン』とも上映される。それははっきり言って、夢のまた夢でした。これもひとえに応援してくれた皆さんの熱い思いの結晶だと思います」とファンへ感謝の言葉を語り、イベントの幕を引いた。  
活动中，还出现了为獠和3月31日生日的香送上祝福蛋糕的场面。伊仓率先拍摄了纪念照，并兴奋地表示“稍后会在Twitter上发布!”。最后，神谷说:“不知道能持续多久，哪怕是一周也好，我一直在祈祷能持续很久。这部电影和《哆啦a梦》一起上映，恐怕之后还会和《柯南》一起上映。坦白地说，这是梦想中的梦想，我想这也是给予我全力支持的大家的热情的结晶。”

「劇場版CityHunter <新宿Private Eyes>」は全国で公開中。  
《剧场版CityHunter <新宿Private Eyes>》正在全国上映中。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  
※冴羽獠的獠，在动物旁正式标明“僚”的写法  

この記事の画像・動画（全16件）  
这篇报道的图片、视频(共16篇)  

[![「劇場版CityHunter <新宿Private Eyes>」“もっこり”かけ声応援上映会の様子。左から北条司、神谷明、伊倉一恵。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_357101.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133052 "「劇場版CityHunter <新宿Private Eyes>」“もっこり”かけ声応援上映会の様子。左から北条司、神谷明、伊倉一恵。")
[![神谷明](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_345801.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133053 "神谷明")
[![左からこだま兼嗣、北条司。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_342608.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133054 "左からこだま兼嗣、北条司。")
[![伊倉一恵](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_349206.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133055 "伊倉一恵")
[![リョウと香の誕生日ケーキ。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_362503.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133056 "リョウと香の誕生日ケーキ。")
[![誕生日ケーキと神谷明を撮影をする伊倉一恵（左）。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_354904.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133057 "誕生日ケーキと神谷明を撮影をする伊倉一恵（左）。")
[![伊倉一恵](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_339402.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133058 "伊倉一恵")
[![ライブビューイングのカメラへ手を振る登壇者たち。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_339809.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133059 "ライブビューイングのカメラへ手を振る登壇者たち。")
[![誕生日ケーキの写真を撮影する伊倉一恵（中央）。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_354303.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133060 "誕生日ケーキの写真を撮影する伊倉一恵（中央）。")
[![左から神谷明、伊倉一恵。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_351505.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133061 "左から神谷明、伊倉一恵。")
[![左から神谷明、伊倉一恵。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_348107.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133062 "左から神谷明、伊倉一恵。")
[![左からアニプレックス制作プロデューサーの若林豪、こだま兼嗣、北条司、神谷明、伊倉一恵、小形尚弘。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_358202.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133063 "左からアニプレックス制作プロデューサーの若林豪、こだま兼嗣、北条司、神谷明、伊倉一恵、小形尚弘。")
[![「劇場版CityHunter <新宿Private Eyes>」“もっこり”かけ声応援上映会の様子。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_360410.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133064 "「劇場版CityHunter <新宿Private Eyes>」“もっこり”かけ声応援上映会の様子。")
[![「劇場版CityHunter <新宿Private Eyes>」“もっこり”かけ声応援上映会の様子。](https://ogre.natalie.mu/media/news/eiga/2019/0326/DSC_361111.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1133065 "「劇場版CityHunter <新宿Private Eyes>」“もっこり”かけ声応援上映会の様子。")
[![「劇場版CityHunter <新宿Private Eyes>」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2018/1212/CityHunterMovie_poster.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/325470/1069071 "「劇場版CityHunter <新宿Private Eyes>」ポスタービジュアル")
[![「劇場版CityHunter <新宿Private Eyes>」本予告2](https://i.ytimg.com/vi/mji4WOAQT0w/default.jpg)](https://natalie.mu/eiga/gallery/news/325470/media/35561 "「劇場版CityHunter <新宿Private Eyes>」本予告2")

(c)北条司/NSP・「2019 劇場版CityHunter」製作委員会

LINK

[「劇場版CityHunter <新宿Private Eyes>」公式サイト](http://cityhunter-movie.com/)  
[アニメ「劇場版CityHunter」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)  
[「劇場版CityHunter <新宿Private Eyes>」本予告2](https://www.youtube.com/watch?v=mji4WOAQT0w)  
