https://natalie.mu/comic/news/316630


# モリコロス「咲けよ花咲け！」1巻、BL好き男子と百合好き女子が美大で邂逅

2019年1月19日  [Comment](https://natalie.mu/comic/news/316630/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[モリコロス](https://natalie.mu/comic/artist/9941)「咲けよ花咲け！」1巻が、本日1月19日に竹書房より発売された。
森克罗斯“绽放吧，绽放吧!”第1卷于今天1月19日由竹书房发售。  

[
![「咲けよ花咲け！」1巻](https://ogre.natalie.mu/media/news/comic/2019/0119/sakeyo_01.jpg?imwidth=468&imdensity=1)
「咲けよ花咲け！」1巻
大きなサイズで見る（全9件）
](https://natalie.mu/comic/gallery/news/316630/1092297)

「咲けよ花咲け！」は美大のマンガコースを舞台に、BL作品が好きな美少年・飛島一太と百合が好きな女子・町屋むつみの出会いを描く恋愛コメディ。一太は同じ趣味を持つ腐女子と出会い楽しいキャンパスライフを送ることを夢見ていたが、自分は女子を相手にすると緊張してまともに会話ができないことに入学してから気が付く。女子に対する苦手意識と同好の士を求める気持ちの間で板挟みになる一太は、同性愛ジャンルに理解のある町屋と仲良くなろうと奮闘する。  
“绽放吧，绽放吧!”是一部描写喜欢BL作品的美少年·飞岛一太和喜欢百合的女子·町屋睦美相遇的恋爱喜剧。一太梦想着与拥有相同兴趣的腐女相遇并过上快乐的校园生活，但入学后才发现自己一遇到女生就会紧张到无法正常对话。夹在对女生的不擅长意识和追求同好的心情之间的一太，为了和对同性恋类型有理解的町屋成为好朋友而奋斗。

購入特典はいまじん白揚用、喜久屋書店用、COMIC ZIN用、アニメイト名古屋用、そのほかの書店用の5種が存在。詳細は購入予定の店舗にて確認を。  
购买特典有今津白扬用、喜久屋书店用、COMIC ZIN用、animate名古屋用、其他书店用5种。详情请到预定购买的店铺确认。  

同作はコアミックスのWebマンガサイト・コミックタタンで連載中。単行本レーベル「バンブーコミックス タタン」の第1弾タイトルとなっており、同レーベルから[北条司](https://natalie.mu/comic/artist/2405)キャラクター原案による[えすとえむ](https://natalie.mu/comic/artist/2622)「CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常」1巻、[小手川ゆあ](https://natalie.mu/comic/artist/2064)「殺人プルガトリウム」1巻、おかざきさと乃「異邦のオズワルド」1巻も本日発売された。  
该作品在Coamix的Web漫画网站comic tatan连载中。这是单行本“bamboo comic tatan”的第1弹标题，也是北条司角色原案的《CITY HUNTER外传 伊集院隼人氏的平静度过的日常》1卷、小手川悠亚的《杀人鬼宫》1卷、冈崎里乃的《异邦的奥斯瓦尔德》1卷也在今天发售。

なおコミックナタリーでは、コミックタタンのレーベル第1弾タイトル発売を記念した特集を実施。「CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常」を執筆するえすとえむのインタビューや、佐原ミズ、あみだむく、モリコロス、三堂マツリ、三月えみ、仁茂田あい、田村茜、山崎智史が海坊主を描くトリビュートイラストギャラリーを展開している。
另外，comic natalie还实施了纪念comic tatan标签第1弹标题发售的特集。《CITY HUNTER外传 伊集院隼人氏的平静度过的日常》的作者えすとえむ的采访，以及佐原水、amedame、森克罗、三堂祭、三月惠美、仁茂田爱、田村茜、山崎智史的画的海坊主。

この記事の画像（全9件）

[![「咲けよ花咲け！」1巻](https://ogre.natalie.mu/media/news/comic/2019/0119/sakeyo_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092297 "「咲けよ花咲け！」1巻")
[![「咲けよ花咲け！」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092298 "「咲けよ花咲け！」1巻より。")
[![「咲けよ花咲け！」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092299 "「咲けよ花咲け！」1巻より。")
[![「咲けよ花咲け！」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092300 "「咲けよ花咲け！」1巻より。")
[![「咲けよ花咲け！」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092301 "「咲けよ花咲け！」1巻より。")
[![「咲けよ花咲け！」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/007.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092302 "「咲けよ花咲け！」1巻より。")
[![「咲けよ花咲け！」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092303 "「咲けよ花咲け！」1巻より。")
[![「咲けよ花咲け！」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/009.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092304 "「咲けよ花咲け！」1巻より。")
[![「咲けよ花咲け！」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/010.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316630/1092305 "「咲けよ花咲け！」1巻より。")

LINK

[咲けよ花咲け！｜コミックタタン](https://www.tatan.jp/lib/top.php?id=137)