https://natalie.mu/comic/news/318990

# 「劇場版シティーハンター」伝言板が新宿駅に、巨大広告やゴールデン街コラボも
「剧场版City Hunter」留言板在新宿站，还有巨大广告和Golden街合作

2019年2月6日  [Comment](https://natalie.mu/comic/news/318990/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」の公開を記念し、劇中に登場する伝言板を模したオブジェが設置された。  
为纪念根据北条司原作改编的动画电影《剧场版城市猎人<新宿Private Eyes>》的上映，设置了以剧中登场的留言板为原型的物件。  

[
![JR新宿駅中央東口に登場した伝言板。](https://ogre.natalie.mu/media/news/comic/2019/0206/cityhunter04.jpg?imwidth=468&imdensity=1)
JR新宿駅中央東口に登場した伝言板。  
大きなサイズで見る（全6件）  
JR新宿站中央东口出现了留言板。  
查看大图(共6件)  
](https://natalie.mu/comic/gallery/news/318990/1103467)

[
![東京メトロ丸ノ内線新宿駅メトロプロムナードの広告。](https://ogre.natalie.mu/media/news/comic/2019/0206/cityhunter02.jpg?imwidth=468&imdensity=1)
東京メトロ丸ノ内線新宿駅メトロプロムナードの広告。［拡大］  
东京Metro丸之内线新宿站Metro Promenade的广告。[扩大]  
](https://natalie.mu/comic/gallery/news/318990/1103468)  
(译注：プロムナード/Promenade 指“散歩道，人行道”)  

伝言板には北条をはじめ、映画で総監督を務めた[こだま兼嗣](https://natalie.mu/comic/artist/13476)、キャスト陣の直筆メッセージやサインをデザイン。また東京メトロ丸ノ内線新宿駅メトロプロムナードにも「劇場版シティーハンター <新宿プライベート・アイズ>」の巨大広告が掲出されている。このほか新宿のゴールデン街の一部店舗ではメニューを注文した際に「XYZ」と伝えると特製シールをプレゼントするキャンペーンを行う。  
留言板上设计有北条、儿玉兼嗣（该片的总导演）和演员们的手写留言和签名。另外，在东京地铁丸之内线新宿站Metro Promenade也张贴了《剧场版城市猎人<新宿Private Eyes>》的巨大广告。此外，新宿Golden街的部分店铺还推出了点菜单时只要告知“XYZ”就赠送特制贴纸的活动。  

映画「劇場版シティーハンター <新宿プライベート・アイズ>」は2月8日ロードショー。  
电影《剧场版城市猎人<新宿Private Eyes>》将于2月8日上映。  

なおコミックナタリーでは20年ぶりの新作となる「劇場版シティーハンター <新宿プライベート・アイズ>」の特集を展開。冴羽リョウ役の神谷明と原作者の北条司にインタビューし、約20年ぶりの冴羽リョウとの再会や、映画の見どころなどについて語ってもらった。  
另外，Comic Natalie也推出了时隔20年的新作《剧场版城市猎人<新宿Private Eyes>》的特集。采访了冴羽獠的扮演者神谷明和原作者北条司，请他们谈了约20年后与冴羽獠的再会，以及电影的看点等。  



この記事の画像（全6件）  
这篇报道的图片(共6篇)  

[![JR新宿駅中央東口に登場した伝言板。](https://ogre.natalie.mu/media/news/comic/2019/0206/cityhunter04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/318990/1103467 "JR新宿駅中央東口に登場した伝言板。")
[![東京メトロ丸ノ内線新宿駅メトロプロムナードの広告。](https://ogre.natalie.mu/media/news/comic/2019/0206/cityhunter02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/318990/1103468 "東京メトロ丸ノ内線新宿駅メトロプロムナードの広告。")
[![ゴールデン街でプレゼントされるステッカー。](https://ogre.natalie.mu/media/news/comic/2019/0206/sticker.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/318990/1103469 "ゴールデン街でプレゼントされるステッカー。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/comic/2019/0206/cityhunter01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/318990/1103470 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」より。](https://ogre.natalie.mu/media/news/comic/2019/0206/cityhunter03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/318990/1103471 "「劇場版シティーハンター <新宿プライベート・アイズ>」より。")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2018/1212/CityHunterMovie_poster.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/318990/1069071 "「劇場版シティーハンター <新宿プライベート・アイズ>」ポスタービジュアル")

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。  
※冴羽獠的獠，在动物旁正式标记为“僚”。  


(c)北条司/NSP・「2019 劇場版シティーハンター」製作委員会

LINK

[「劇場版シティーハンター <新宿プライベート・アイズ>」公式サイト](http://cityhunter-movie.com/)  
[アニメ「劇場版シティーハンター」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)  
[「劇場版シティーハンター <新宿プライベート・アイズ>」本予告2](https://www.youtube.com/watch?v=mji4WOAQT0w)  