https://natalie.mu/comic/news/503544

# 「劇場版シティーハンター」来年公開！キャスト続投、北条司が新ビジュアル描き下ろし

2022年12月2日   [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「劇場版シティーハンター」の続報が到着。北条描き下ろしの新ビジュアルや公開時期、キャストが発表された。  
北条司原作动画《剧场版城市猎人》的后续报道。北条所绘的新海报、上映时间、演员阵容均已发表。


[
![「劇場版シティーハンター」の北条司描き下ろし新ビジュアル。](https://ogre.natalie.mu/media/news/comic/2022/1201/cityhunter-movi_1202.jpg?impolicy=thumb_fit&width=180&height=180)
「劇場版シティーハンター」の北条司描き下ろし新ビジュアル。  
大きなサイズで見る（全4件）  
《剧场版城市猎人》的北条司新形象。  
大尺寸观看(共4件)  
](https://natalie.mu/comic/gallery/news/503544/1951712)

新ビジュアルに描かれたのは、凄腕スイーパーの“シティーハンター”こと冴羽リョウが、新宿の夜景をバックにおなじみの衣装で凛々しく仁王立ちする姿。また「The Final Chapter Begins.」「過去に終止符を打つ戦いが始まる──」と、シティーハンターが最終決戦へ向けて動き出すことを予感させるキャッチコピーが添えられている。  
新视觉形象描绘的是精明能干的Sweeper“城市猎人”冴羽獠，以新宿的夜景为背景，穿着熟悉的服装威风凛凛地站立的身姿。另外，“The Final Chapter Begins.”“为过去画上句号的战斗开始了——”这句宣传语让人预感到城市猎人即将走向最终决战。  

公開時期は2023年。すでに発表されていた冴羽リョウ役の[神谷明](https://natalie.mu/comic/artist/60997)に加え、槇村香役に[伊倉一恵](https://natalie.mu/comic/artist/95562)、野上冴子役に[一龍斎春水](https://natalie.mu/comic/artist/107953)、海坊主役に[玄田哲章](https://natalie.mu/comic/artist/19455)、美樹役に[小山茉美](https://natalie.mu/comic/artist/19454)が過去シリーズから続投する。  
上映时间为2023年。除了已经公布的冴羽獠由神谷明出演外，槙村香由伊仓一惠出演，野上冴子由一龙斋春水出演，海坊主由玄田哲章出演，美树由小山茉美出演。  

なお12月17日と18日に千葉・幕張メッセで開催される「ジャンプフェスタ2023」では、同作のムビチケカードを販売。ムビチケカードには、今回の新ビジュアルを用いたジオラマアクリルスタンドが付属する。  
另外，12月17日和18日在千叶 幕张messe举办的“Jump Festa 2023”上，也将销售该作的Mviticket卡。附赠采用此次新视觉效果的Diorama acrylic stand。  

この記事の画像・動画（全4件）  
这篇报道的图片、视频(共4篇)  

[![「劇場版シティーハンター」の北条司描き下ろし新ビジュアル。](https://ogre.natalie.mu/media/news/comic/2022/1201/cityhunter-movi_1202.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/503544/1951712 "「劇場版シティーハンター」の北条司描き下ろし新ビジュアル。")
[![ムビチケカードに付属するジオラマアクリルスタンド。](https://ogre.natalie.mu/media/news/comic/2022/1201/cityhunter-movi_acrylicstand.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/503544/1951711 "ムビチケカードに付属するジオラマアクリルスタンド。")
[![劇場版「シティーハンター」のLogo。](https://ogre.natalie.mu/media/news/comic/2022/0407/gekijouban_city_hunter_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/503544/1798730 "劇場版「シティーハンター」のロゴ。")
[![新作「劇場版シティーハンター」制作決定スペシャルムービー2022](https://i.ytimg.com/vi/pvVjkP_94-k/default.jpg)](https://natalie.mu/comic/gallery/news/503544/media/75595 "新作「劇場版シティーハンター」制作決定スペシャルムービー2022")

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。


(c)北条司/コアミックス・「2023 劇場版シティーハンター」製作委員会