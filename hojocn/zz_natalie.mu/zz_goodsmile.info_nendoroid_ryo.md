source:https://www.goodsmile.info/zh/product/8055/黏土人+冴羽獠.html  
日文版：https://www.goodsmile.info/ja/product/8055/ねんどろいど+冴羽獠.html  
英文版：https://www.goodsmile.info/en/product/8055/Nendoroid+Ryo+Saeba.html  


發售日期: 2019年09月

1084

# 黏土人 冴羽獠

### 是你叫我來的對吧？

出自《劇場版 城市獵人〈新宿 PRIVATE EYES〉》，傳說中的清道夫‧城市獵人「冴羽獠」，奇蹟地化身為黏土人！可替換的表情零件有帥氣的「普通臉」、滑稽的「笑臉」、「焦急臉」。配件有附獠的愛槍與硝煙特效零件。著重於平衡感，股關節採用單軸可動零件，能讓Q版的黏土人擺出帥氣姿勢。當然也能賞玩城市獵人不可或缺的滑稽一面。

### 商品詳情

商品名稱  
黏土人 冴羽獠

作品名稱  
劇場版 城市獵人〈新宿 PRIVATE EYES〉

製造商  
Good Smile Company

分類  
黏土人

價格  
4,888日圓 （含稅）

發售日期  
2019/09

商品規格  
ABS&PVC 塗裝完成可動模型・無比例・附專用台座・全高：約100mm

原型製作  
あやりん｡(Cool Drive Factory)

製作協力  
ねんどろん

- 本產品無法自行站立，請使用產品所附的台座  
- 本頁所刊登的照片與實際商品會有些許差異。  
- 本商品的色彩工程為手工作業，因此在塗裝上會有些許的個體差異。敬請見諒。  

©北条司/NSP・「2019 劇場版CityHunter」製作委員会  

## 購買方式

■ GOODSMILE線上商店  
![GOODSMILE ONLINE SHOP](https://images.goodsmile.info/media/gsc_online-d2be2a4607a4a05e364c9518f2bc3bda.png)

「GOODSMILE線上商店」的預購期間為  
日本時間2019年02月07日（四）12:00～2019年03月14日（四）12:00止。  
  
關於運費與發送相關說明請至「GOODSMILE線上商店」商品頁觀看。  
→ [GOODSMILE線上商店商品頁面](https://goodsmileshop.com/tw/p/G_NEN_WD_01084/?utm_source=internal&utm_medium=product&utm_campaign=8055)  
  
  
※開放期間中可隨時預購。  
※上述期間將決定販售預定數量。  
※期間後開放至達到預購數量上限為止。  

  

■ 實體通路  
商品預購期間等詳細資訊，請洽詢各店鋪。[合作店鋪一覽](http://partner.goodsmile.info/support/zh/partnershops/)

[![黏土人 冴羽獠](https://images.goodsmile.info/cgm/images/product/20190204/8055/57968/thumb/ce864b03e97fd5ded5c59701bda2eb33.jpg)](#itemZoom1)
[![黏土人 冴羽獠](https://images.goodsmile.info/cgm/images/product/20190204/8055/57969/thumb/b172006e380678055dd1c82bd55ca799.jpg)](#itemZoom2)
[![黏土人 冴羽獠](https://images.goodsmile.info/cgm/images/product/20190204/8055/57970/thumb/96fa09494356c24c52556a1601f636da.jpg)](#itemZoom3)
[![黏土人 冴羽獠](https://images.goodsmile.info/cgm/images/product/20190204/8055/57971/thumb/849c234cd944c45adf7847e0d0e0dac5.jpg)](#itemZoom4)
[![黏土人 冴羽獠](https://images.goodsmile.info/cgm/images/product/20190204/8055/57972/thumb/53679f841a2e188260868ce623b3e491.jpg)](#itemZoom5)
[![黏土人 冴羽獠](https://images.goodsmile.info/cgm/images/product/20190204/8055/57973/thumb/7f597d490bc79a8aad8bba5e2780c677.jpg)](#itemZoom6)
[![※可以與「黏土人 槙村香」（另售）一起擺飾。](https://images.goodsmile.info/cgm/images/product/20190204/8055/58761/thumb/f4cffb5488ca314e7a16bab0b1d2476f.jpg)](#itemZoom7)

    
[![](https://images.goodsmile.info/cgm/images/product/20190204/8055/57968/large/ce864b03e97fd5ded5c59701bda2eb33.jpg)](https://images.goodsmile.info/cgm/images/product/20190204/8055/57968/large/ce864b03e97fd5ded5c59701bda2eb33.jpg "黏土人 冴羽獠")
    
[![](https://images.goodsmile.info/cgm/images/product/20190204/8055/57969/large/b172006e380678055dd1c82bd55ca799.jpg)](https://images.goodsmile.info/cgm/images/product/20190204/8055/57969/large/b172006e380678055dd1c82bd55ca799.jpg "黏土人 冴羽獠")

[![](https://images.goodsmile.info/cgm/images/product/20190204/8055/57970/large/96fa09494356c24c52556a1601f636da.jpg)](https://images.goodsmile.info/cgm/images/product/20190204/8055/57970/large/96fa09494356c24c52556a1601f636da.jpg "黏土人 冴羽獠")
    
[![](https://images.goodsmile.info/cgm/images/product/20190204/8055/57971/large/849c234cd944c45adf7847e0d0e0dac5.jpg)](https://images.goodsmile.info/cgm/images/product/20190204/8055/57971/large/849c234cd944c45adf7847e0d0e0dac5.jpg "黏土人 冴羽獠")
   [![](https://images.goodsmile.info/cgm/images/product/20190204/8055/57972/large/53679f841a2e188260868ce623b3e491.jpg)](https://images.goodsmile.info/cgm/images/product/20190204/8055/57972/large/53679f841a2e188260868ce623b3e491.jpg "黏土人 冴羽獠")
    
[![](https://images.goodsmile.info/cgm/images/product/20190204/8055/57973/large/7f597d490bc79a8aad8bba5e2780c677.jpg)](https://images.goodsmile.info/cgm/images/product/20190204/8055/57973/large/7f597d490bc79a8aad8bba5e2780c677.jpg "黏土人 冴羽獠")
    
[![](https://images.goodsmile.info/cgm/images/product/20190204/8055/58761/large/f4cffb5488ca314e7a16bab0b1d2476f.jpg)](https://images.goodsmile.info/cgm/images/product/20190204/8055/58761/large/f4cffb5488ca314e7a16bab0b1d2476f.jpg "※可以與「黏土人 槙村香」（另售）一起擺飾。")  
    
※可以與「黏土人 槙村香」（另售）一起擺飾。

相關商品

[![黏土人 槙村香](//images.goodsmile.info/cgm/images/product/20190304/8146/58748/thumb/6918102f7018e6fa99f263a4f1617d5f.jpg) 黏土人 槙村香](./zz_goodsmile.info_nendoroid_kaori.md)