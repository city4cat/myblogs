https://natalie.mu/eiga/news/503534

# 「劇場版シティーハンター」新作は2023年公開、北条司がビジュアル描き下ろし
《剧场版城市猎人》新作将于2023年上映，北条司亲自绘制视觉图  

2022年12月2日 [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


「劇場版シティーハンター」が2023年に公開されることが決定。原作者・[北条司](https://natalie.mu/eiga/artist/2405)による描き下ろしビジュアルが到着した。  
《剧场版城市猎人》将于2023年上映。由原作者·北条司亲自绘制的视觉海报也已抵达。  

[
![「劇場版シティーハンター」原作者・北条司の描き下ろしビジュアル。](https://ogre.natalie.mu/media/news/eiga/2022/1201/cityhunter_202211_visual.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター」原作者・北条司の描き下ろしビジュアル。  
大きなサイズで見る（全6件）  
《剧场版城市猎人》原作者北条司所绘视觉图。  
大尺寸观看(共6件)  
](https://natalie.mu/eiga/gallery/news/503534/1951618)

テレビアニメ放送開始から2022年で35年を迎えた「シティーハンター」。描き下ろしビジュアルには、初めて公式ビジュアルで「CH」という略字が使われており、東京・新宿の夜景をバックに冴羽リョウが仁王立ちする姿がデザインされている。また制作発表時に発表されていたリョウ役の[神谷明](https://natalie.mu/eiga/artist/60997)に加え、槇村香役の[伊倉一恵](https://natalie.mu/eiga/artist/95562)、野上冴子役の[一龍斎春水](https://natalie.mu/eiga/artist/107953)、海坊主役の[玄田哲章](https://natalie.mu/eiga/artist/19455)、美樹役の[小山茉美](https://natalie.mu/eiga/artist/19454)が続投することも明らかになった。  
《城市猎人》在2022年迎来了电视动画播放开始后的35周年。新绘制的视觉图首次在官方视觉图中使用了缩写"CH"，并设计了以东京新宿的夜景为背景，以牛郎织女的姿势站立的冴羽獠。除了在制作公告时宣布的神谷明饰演的獠之外，还宣布伊倉一恵饰演槇村香、一龙斋春水饰演野上冴子、玄田哲章饰演海坊主、小山茉美饰演美樹。  

[
![「劇場版シティーハンター」ジオラマアクリルスタンド付きムビチケカード](https://ogre.natalie.mu/media/news/eiga/2022/1201/cityhunter_202211_stand.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター」ジオラマアクリルスタンド付きムビチケカード［拡大］

](https://natalie.mu/eiga/gallery/news/503534/1951617)

さらに12月17日から18日にかけて千葉・幕張メッセで開催される「ジャンプフェスタ2023」では、このたび解禁された「原作・北条司描き下ろし第1弾キービジュアル -The Final Chapter Begins.-」仕様のジオラマアクリルスタンド付きムビチケカードが販売される。  
此外，12月17日至18日在千叶 幕张会展中心举办的“Jump Festa 2023”上，将出售此次解禁的「原作·北条司绘制的第1弹Key Visual -The Final Chapter Begins.-」式样的带有Diorama acrylic stand的Mviticket卡。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記   
※冴羽獠的獠，在反犬旁正式标明“僚”的写法 

この記事の画像・動画（全6件） 
这篇报道的图片、视频(共6篇)  

[![「劇場版シティーハンター」原作者・北条司の描き下ろしビジュアル。（《剧场版城市猎人》原作者北条司所绘制的视觉图。）](https://ogre.natalie.mu/media/news/eiga/2022/1201/cityhunter_202211_visual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/503534/1951618 "「劇場版シティーハンター」原作者・北条司の描き下ろしビジュアル。")
[![「劇場版シティーハンター」ジオラマアクリルスタンド付きムビチケカード（带有Diorama acrylic stand的Mviticket卡）](https://ogre.natalie.mu/media/news/eiga/2022/1201/cityhunter_202211_stand.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/503534/1951617 "「劇場版シティーハンター」ジオラマアクリルスタンド付きムビチケカード")
[![冴羽リョウ](https://ogre.natalie.mu/media/news/eiga/2022/0407/cityhunter_202204_1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/503534/1798582 "冴羽リョウ")
[![「劇場版シティーハンター」Logo](https://ogre.natalie.mu/media/news/eiga/2022/0407/cityhunter_202204_2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/503534/1798581 "「劇場版シティーハンター」ロゴ")
[![神谷明](https://ogre.natalie.mu/media/news/eiga/2022/0407/kamiyaakira_art202204.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/503534/1798580 "神谷明")
[![新作「劇場版シティーハンター」制作決定スペシャルムービー2022（新作《剧场版城市猎人》制作决定特别电影2022）](https://i.ytimg.com/vi/pvVjkP_94-k/default.jpg)](https://natalie.mu/eiga/gallery/news/503534/media/75595 "新作「劇場版シティーハンター」制作決定スペシャルムービー2022")

新作「劇場版シティーハンター」制作決定スペシャルムービー | 2023年公開  

<iframe width="640" height="360" src="https://www.youtube.com/embed/pvVjkP_94-k" title="新作「劇場版シティーハンター」制作決定スペシャルムービー | 2023年公開" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


## 上映スケジュール
上映日程  

[近くの映画館](#)
附近的电影院  

上映スケジュールがありません  
没有上映档期。  

![読み込み中（读入中）](https://ogre.natalie.mu/asset/natalie/common/polar/desktop/image/common/loading.gif)

[さらに読み込む（进一步读入）](#)

情報提供：ぴあ
