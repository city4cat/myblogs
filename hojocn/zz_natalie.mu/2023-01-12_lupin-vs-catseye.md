source:  
https://natalie.mu/comic/news/508556

# 「Lupin三世VSCat's Eye」栗田貫一や戸田恵子ら11人からComment到着  
「Lupin三世 Cat'sEye」栗田贯一、户田惠子等11人发表评论    

2023年1月12日  
[Comic Natalie編集部](https://natalie.mu/comic/author/72)



[Monkey Punch](https://natalie.mu/comic/artist/1749)原作によるアニメ「Lupin三世」と、[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ「Cat's Eye」のコラボアニメ映画「Lupin三世VSCat's Eye」。同作の配信開始に先がけ、キャストとスタッフからCommentが到着した。  
Monkey Punch原作的动画《Lupin三世》和北条司原作的动画《Cat's Eye》的合作动画电影《Lupin三世VS Cat's Eye》。在该作品的配信开始之前，演员和工作人员的评论来了。

[
![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会  
（《Lupin三世VS Cat's Eye》片段剪辑(c)Monkey Punch北条司/Lupin三世VS Cat's Eye制作委员会）
](https://ogre.natalie.mu/media/news/comic/2023/0112/lupin-vs-catseye_main.jpg?impolicy=hq&imwidth=730&imdensity=1)
「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会
大きなサイズで見る（全39件）  
（《Lupin三世VS Cat's》Eye’场面剪辑(c)Monkey Punch北条司/Lupin三世VS Cat’s Eye制作委员会大尺寸观看(共39件)）
](https://natalie.mu/comic/gallery/news/508556/1976273)

「Lupin三世」のアニメ化50周年と「Cat's Eye」の原作40周年を記念し、1月27日よりPrime Videoで世界独占配信される「Lupin三世VSCat's Eye」。CommentはLupin三世役の[栗田貫一](https://natalie.mu/comic/artist/6789)、次元大介役の[大塚明夫](https://natalie.mu/comic/artist/26700)、石川五ェ門役の[浪川大輔](https://natalie.mu/comic/artist/45738)、峰不二子役の[沢城みゆき](https://natalie.mu/comic/artist/65954)、銭形警部役の[山寺宏一](https://natalie.mu/comic/artist/30965)、来生瞳役の[戸田恵子](https://natalie.mu/comic/artist/16682)、来生泪役の[深見梨加](https://natalie.mu/comic/artist/96805)、来生愛役の[坂本千夏](https://natalie.mu/comic/artist/95232)、内海俊夫役の[安原義人](https://natalie.mu/comic/artist/91212)、[静野孔文](https://natalie.mu/comic/artist/62364)監督、[瀬下寛之](https://natalie.mu/comic/artist/92064)監督の計11人から寄せられた。栗田は「物語として違和感なく2つの作品が成立する作品になっています」と、戸田は「2つの作品の代表曲がこの作品の中で同時に聴けることの贅沢さも、是非お楽しみいただけたらと思います」と述べている。  
为纪念《Lupin三世》动画化50周年和《Cat's Eye》原作40周年，《Lupin三世VS Cat's Eye》将于1月27日以Prime Video的形式全球独家放送。评论如下:饰演Lupin三世的栗田贯一、饰演次元大介的大冢明夫、饰演石川五卫门的浪川大辅、饰演峰不二子的泽城美雪、饰演钱形警部的山寺宏一、饰演来生瞳的户田惠子、饰演来生泪的深见梨加、饰演来生爱的坂本千夏、饰演内海俊夫的安原义人、静野孔文导演、濑下宽之导演共11人投稿。栗田说:“作为故事，两部作品毫无违和感，成为了一部能够成立的作品。”户田说:“两部作品的代表角色能在这部作品中同时出现，这是多么奢侈的享受，希望大家一定能享受到。”

## 栗田貫一（Lupin三世役）Comment

[
栗田貫一［拡大］
](https://natalie.mu/comic/gallery/news/508556/1976257)

最初は「Lupin三世」と「Cat's Eye」が同じ時代に現れるなんてどんなふうになるんだろうと思っていましたが、物語として違和感なく2つの作品が成立する作品になっています。  
一开始我还在想，“Lupin三世”和“Cat's Eye”会出现在同一个时代，这是怎样的一种情况呢?从故事的角度来看，这两个作品是毫无违和感的。  

今回、Lupin三世の過去がCat's Eye3姉妹と絡んでくる物語も入っており、是非そのあたりもご注目いただき、ご覧いただきたいです。  
这次，Lupin三世的过去和Cat's Eye3姐妹有关的故事也加入了，请大家观看时一定要关注这一点。

## 大塚明夫（次元大介役）Comment

[
大塚明夫［拡大］
](https://natalie.mu/comic/gallery/news/508556/1952119)

なんて楽しい企画！ Lupin一味とCat's Eye！ それぞれの良さが生きる脚本と演出になるんだろうと、今から期待に胸が膨らみます。  
多么开心的企划!Lupin一伙和Cat's Eye!我很期待看到剧本和导演会如何发挥出他们每个人的优点。  

Lupinファミリーでは新入りの私ですが、皆さんの脚を引っ張らずになんとか演じおおせたい！ そして次元大介の名に恥じぬ存在感を示したい！ でもこんなに入れ込んでると落とし穴に嵌りそうで怖い！ とにかく皆様お楽しみに！  
虽然我是Lupin家族的新人，但我一定要演好，不拖大家的后腿!并且展现出无愧于次元大介之名的存在感!但我又怕投入这么多，会掉进坑里! 总之，我希望大家喜欢这个节目!  

## 浪川大輔（石川五ェ門役）Comment

[
浪川大輔［拡大］
](https://natalie.mu/comic/gallery/news/508556/1975035)

どちらの作品もとても歴史があります。さらに、記念すべき周年イヤー！その２つがまさかのコラボ。それも、VS？ どういうことに？ どんな展開に?! 化学反応が楽しみでしかありません。宜しくお願い致します！  
两部作品都很有历史。今年是一个值得纪念的周年纪念年! 这两个意想不到的合作。而VS呢，会发生什么?怎样展开?!只能期待化学反应。请多多关照!  

## 沢城みゆき（峰不二子役）Comment

[
沢城みゆき［拡大］
](https://natalie.mu/comic/gallery/news/508556/1895331)

愛、可愛い～!!!と思う歳になったのだなと、感慨深い気持ちです。当時小学生だった頃は、泪の底知れない色っぽさと、瞳のカッコ可愛さに夢中でした。まさか憧れのお姉さんだった彼女達を「子猫たち」と呼称する日が来ようとは。ちぇきら！  
爱，可爱~ !!!我已经到了这么想的年纪了，真是感慨万千。当时我还是小学生的时候，就着迷于泪那无尽的妩媚和那双可爱的眼睛。没想到有一天会有把向往的姐姐们称为“小猫们”。真是太好了!

## 山寺宏一（銭形警部役）Comment

[
山寺宏一［拡大］
](https://natalie.mu/comic/gallery/news/508556/1681769)

モンキーパンチ先生と北条司先生。原作が大変お世話になったお2人のWネームである事に鳥肌が立ちました！  
Monkey Punch老师和北条司老师。当我发现原作是在两个对我很观照的人的W名下时，我起了一身鸡皮疙瘩!!（译注：待校对）  

まさに夢のコラボ！内海と銭形の絡みが嬉しすぎます。尊敬する大先輩の安原さんに「先輩」と呼ばれるなんて！  
简直是梦幻的合作!内海和钱形的纠缠太让人高兴了。被尊敬的大前辈安原先生称呼为“前辈”!  

愛とロマンとアクション満載のスーパーエンタテインメントをお楽しみに!!!  
请欣赏这部充满爱情、浪漫和行动的超级娱乐片吧!!!  

## 戸田恵子（来生瞳役）Comment

[
戸田恵子［拡大］
](https://natalie.mu/comic/gallery/news/508556/1922449)

30数年の年月を経て、また瞳を演じさせていただける喜びを噛み締めながら演じさせていただきました。  
30多年后我再次扮演Hitomi这个角色。能够再次扮演她，我非常高兴。。  

「Lupin三世」と「Cat's Eye」2つの作品の世界観が重なっても、なんの違和感も感じず、当たり前のように没入できる作品です。  
即使「Lupin三世」和「Cat's Eye」这两部作品的世界观重叠，也不会有任何违和感，是一部可以理所当然地沉浸其中的作品。  

そして、2つの作品の代表曲がこの作品の中で同時に聴けることの贅沢さも、是非お楽しみいただけたらと思います。  
而且，能在这首作品中同时听到这两首作品的代表曲目，是多么奢侈的享受啊。

## 深見梨加（来生泪役）Comment

[
深見梨加［拡大］
](https://natalie.mu/comic/gallery/news/508556/1345113)

両作品の一ファンだった私が、記念すべき年から参加させて頂ける事は不思議であり、責任の重大さも感じていましたが、作品の勢いとパワーでそんな事は吹き飛びました。ちゃんと進化している名作二つを一緒に楽しみましょう。  
作为这两部作品的粉丝，能够参加纪念年的活动对我来说很不可思议，我感到责任重大，但作品的气势和力量把这一切都吹散了。 让我们一起欣赏这两部适当演变的杰作。  

## 坂本千夏（来生愛役）Comment

[
坂本千夏［拡大］
](https://natalie.mu/comic/gallery/news/508556/1976251)

もう～～～！ 大事件ですよ！ なんて素敵な企みなんでしょう！  
哦不~~! 这是个大事件! 多么美妙的计划啊!  

ずいぶん会ってない懐かしい仲間に会うような、そして新しい出会いも！ ここんとこ味わったことのないドキドキワクワクです。  
就像见到了多年未见的老朋友，也见到了新朋友! 这是我最近没有经历过的刺激和兴奋。  

## 安原義人（内海俊夫役）Comment

[
安原義人［拡大］
](https://natalie.mu/comic/gallery/news/508556/1976254)

「久し振りだにゃー。まだまだ馬鹿（わか）くて、カッコいいところを見せるぜ！ まってろよー ヒトミー！＜俊夫デカ＞」
「好久没见了。 我要让你知道，我还是个有点傻、有点酷的人! 等着我吧，Hitomi! ＜俊夫デカ＞」

## 静野孔文（監督）Comment

[
静野孔文監督［拡大］
](https://natalie.mu/comic/gallery/news/508556/1976255)

「Lupin三世」は現在もTVシリーズなどが放送しているので皆さんもご覧になっている作品ですが「Cat's Eye」は80年代に放送されていた作品なのでまだご覧になっていらっしゃらない方もおられると思います。そんな方々にも、「Lupin三世」と「Cat's Eye」の魅力を感じていただけるよう、ドラマをつくっていきました。是非お楽しみいただけたら嬉しいです。  
「Lupin三世」现在也在电视上播出，大家也看到了，「Cat's Eye」是80年代播出的作品，应该还有没看过的人吧。为了让这些人也能感受到「Lupin三世」和「Cat's Eye」的魅力，我们制作了这部电视剧。如果您能享受的话我一定会很高兴的。

## 瀬下寛之（監督）Comment

[
瀬下寛之監督［拡大］
](https://natalie.mu/comic/gallery/news/508556/1976256)

「Lupin三世」そして「Cat's Eye」  
80年代を代表する魅力的なキャラクターたちが鮮やかに対決します。  
2つの作品が絡むことでしか味わえない面白さを是非映像で体験いただきたいです。  
《Lupin三世》和《Cat's Eye》  
代表80年代的有魅力的角色们鲜明的对决。  
我们希望您能体验到这两部作品在电影中交织在一起时的乐趣。   


この記事の画像・動画（全39件）  
本文的图片和视频（共39张）  

[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2023/0112/lupin-vs-catseye_main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1976273 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![栗田貫一](https://ogre.natalie.mu/media/news/comic/2023/0112/kuritakanichi_art202301.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1976257 "栗田貫一")
[![大塚明夫](https://ogre.natalie.mu/media/news/comic/2022/1202/ootsukaakio_20221202.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1952119 "大塚明夫")
[![浪川大輔](https://ogre.natalie.mu/media/news/comic/2023/0109/namikawadaisuke.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1975035 "浪川大輔")
[![沢城みゆき](https://ogre.natalie.mu/media/news/comic/2022/0907/sawashiromiyuki_art20220907.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1895331 "沢城みゆき")
[![山寺宏一](https://ogre.natalie.mu/media/news/comic/2021/1013/yamaderakouichi_art202110.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1681769 "山寺宏一")
[![戸田恵子](https://ogre.natalie.mu/media/news/comic/2022/1017/todakeiko_art202210.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1922449 "戸田恵子")
[![深見梨加](https://ogre.natalie.mu/media/news/comic/2020/0307/fukami.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1345113 "深見梨加")
[![坂本千夏](https://ogre.natalie.mu/media/news/comic/2023/0112/sakamotochika_art202301.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1976251 "坂本千夏")
[![安原義人](https://ogre.natalie.mu/media/news/comic/2023/0112/yasuharayoshito_art2020301.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1976254 "安原義人")
[![静野孔文監督](https://ogre.natalie.mu/media/news/comic/2023/0112/shizunokoubun_art202301.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1976255 "静野孔文監督")
[![瀬下寛之監督](https://ogre.natalie.mu/media/news/comic/2023/0112/seshitahiroyuki_art202301.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1976256 "瀬下寛之監督")
[![「Lupin三世 VS Cat's Eye」key visual (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/keyvisual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953846 "「Lupin三世 VS Cat's Eye」key visual (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![Lupin三世（CV：栗田貫一）。天才的泥棒の家系に生を受け、世界中をまたにかける大泥棒。どんなターゲットも奇想天外なアイディアと大胆不敵なテクニックで手に入れる。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953840 "Lupin三世（CV：栗田貫一）。天才的泥棒の家系に生を受け、世界中をまたにかける大泥棒。どんなターゲットも奇想天外なアイディアと大胆不敵なテクニックで手に入れる。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![次元大介（CV：大塚明夫）。早撃ち0.3秒の天才ガンマン。その卓越したガンさばきは、Lupin三世も一目置く。窮地に陥っても常に冷静に状況を把握して、難局を打破する男。酒と煙草をこよなく愛する。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_005.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953837 "次元大介（CV：大塚明夫）。早撃ち0.3秒の天才ガンマン。その卓越したガンさばきは、Lupin三世も一目置く。窮地に陥っても常に冷静に状況を把握して、難局を打破する男。酒と煙草をこよなく愛する。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![石川五ェ門（CV：浪川大輔）。約400年前に大怪盗を輩出した石川家の末裔。斬鉄剣を自在に操る居合の達人であり、剣の道を究めるためにひたすら修行の道を歩む誇り高き剣豪。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_006.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953835 "石川五ェ門（CV：浪川大輔）。約400年前に大怪盗を輩出した石川家の末裔。斬鉄剣を自在に操る居合の達人であり、剣の道を究めるためにひたすら修行の道を歩む誇り高き剣豪。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![峰不二子（CV：沢城みゆき）。さまざまな顔を持つ謎の美女。類まれなる美貌と抜群のプロポーションを持ちながら、Lupinさえも手玉にとる天才的頭脳をあわせ持つ。行動は単純で、己の欲望に従っているだけ。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_007.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953836 "峰不二子（CV：沢城みゆき）。さまざまな顔を持つ謎の美女。類まれなる美貌と抜群のプロポーションを持ちながら、Lupinさえも手玉にとる天才的頭脳をあわせ持つ。行動は単純で、己の欲望に従っているだけ。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![銭形警部（CV：山寺宏一）。ICPO（インターポール）のLupin三世専任捜査官であり、江戸時代の有名な岡っ引きの子孫。Lupin逮捕に執念を燃やし、Lupinの事なら誰よりも熟知している。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_008.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953834 "銭形警部（CV：山寺宏一）。ICPO（インターポール）のLupin三世専任捜査官であり、江戸時代の有名な岡っ引きの子孫。Lupin逮捕に執念を燃やし、Lupinの事なら誰よりも熟知している。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![来生瞳（CV：戸田恵子）。キャッツアイ三姉妹の次女。爽やかにロングヘアーをなびかせる美人。抜群の身体能力を活かし、華麗に盗みを遂行する。刑事の俊夫とは学生の時から恋人関係にある。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_009.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953832 "来生瞳（CV：戸田恵子）。キャッツアイ三姉妹の次女。爽やかにロングヘアーをなびかせる美人。抜群の身体能力を活かし、華麗に盗みを遂行する。刑事の俊夫とは学生の時から恋人関係にある。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![来生泪（CV：深見梨加）。キャッツアイ三姉妹の長女。大人の色気を漂わせる、グラマラスな美女。その明晰な頭脳を活かし、作戦の司令塔を担う。優れた変装技術も持っている。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_010.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953833 "来生泪（CV：深見梨加）。キャッツアイ三姉妹の長女。大人の色気を漂わせる、グラマラスな美女。その明晰な頭脳を活かし、作戦の司令塔を担う。優れた変装技術も持っている。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![来生愛（CV：坂本千夏）。キャッツアイ三姉妹の三女。元気いっぱい、ボーイッシュな女の子。メカやITに強く、自作ギミックで盗みを成功に導く。三姉妹の中で唯一、父親との思い出がない。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_011.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953843 "来生愛（CV：坂本千夏）。キャッツアイ三姉妹の三女。元気いっぱい、ボーイッシュな女の子。メカやITに強く、自作ギミックで盗みを成功に導く。三姉妹の中で唯一、父親との思い出がない。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")

さらに読み込む

[![内海俊夫（CV：安原義人）。瞳の恋人で、キャッツアイ事件を担当する犬鳴署の刑事。お調子者だが正義感は強く、キャッツアイ逮捕に執念を燃やす熱血漢。銭形に憧れを抱いている。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_012.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953842 "内海俊夫（CV：安原義人）。瞳の恋人で、キャッツアイ事件を担当する犬鳴署の刑事。お調子者だが正義感は強く、キャッツアイ逮捕に執念を燃やす熱血漢。銭形に憧れを抱いている。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![永石（CV：麦人）。ハインツから恩義を受け、三姉妹を陰から支える紳士。キャッツアイの盗みのサポートや、ハインツコレクションの管理などを行っている。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953841 "永石（CV：麦人）。ハインツから恩義を受け、三姉妹を陰から支える紳士。キャッツアイの盗みのサポートや、ハインツコレクションの管理などを行っている。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![ハインリッヒ・ベルガー。喫茶キャッツアイを訪れる初老の画商。キャッツ三姉妹に絵画「花束と少女」にまつわる依頼を持ち込む。失踪前のハインツを知っている。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953838 "ハインリッヒ・ベルガー。喫茶キャッツアイを訪れる初老の画商。キャッツ三姉妹に絵画「花束と少女」にまつわる依頼を持ち込む。失踪前のハインツを知っている。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![デニス・キルヒマン。裏世界を暗躍する武器商「ファーデン」の幹部。元傭兵で、目的のためなら手段を択ばない冷酷な男。Lupinやキャッツアイの前に強敵として立ちはだかる。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/1206/lupin_catseye_character_004.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1953839 "デニス・キルヒマン。裏世界を暗躍する武器商「ファーデン」の幹部。元傭兵で、目的のためなら手段を択ばない冷酷な男。Lupinやキャッツアイの前に強敵として立ちはだかる。 (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」key visual (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/teaser_visual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905449 "「Lupin三世 VS Cat's Eye」key visual (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/01c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905460 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut](https://ogre.natalie.mu/media/news/comic/2022/0922/02a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905459 "「Lupin三世 VS Cat's Eye」場面Cut")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/03a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905458 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/04c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905457 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/05b.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905456 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/06c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905455 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/07c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905454 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/08c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905453 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/09c.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905452 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/10a.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905451 "「Lupin三世 VS Cat's Eye」場面Cut (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」Logo (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会](https://ogre.natalie.mu/media/news/comic/2022/0922/lvc_logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/508556/1905450 "「Lupin三世 VS Cat's Eye」Logo (c)Monkey Punch 北条司/Lupin三世 VS Cat's Eye製作委員会")
[![「Lupin三世 VS Cat's Eye」第2弾PV](https://i.ytimg.com/vi/zZAsIu9oBIo/default.jpg)](https://natalie.mu/comic/gallery/news/508556/media/84700 "「Lupin三世 VS Cat's Eye」第2弾PV")
[![「Lupin三世 VS Cat's Eye」第1弾PV](https://i.ytimg.com/vi/6s5QLmAc7gM/default.jpg)](https://natalie.mu/comic/gallery/news/508556/media/81880 "「Lupin三世 VS Cat's Eye」第1弾PV")



## Amazon Original「Lupin三世 VS Cat's Eye」

2023年1月27日（金）よりPrime Videoにて世界独占配信  
从023年1月27日（周五）起在Prime Video全球独家播出
  
### Staff

原作：[Monkey Punch](https://natalie.mu/comic/artist/1749)『Lupin三世』/[北条司](https://natalie.mu/comic/artist/2405)『Cat's Eye』  
監督：[静野孔文](https://natalie.mu/comic/artist/62364)、[瀬下寛之](https://natalie.mu/comic/artist/92064)  
脚本：葛原秀治  
副監督：井手惠介  
Character Design：中田春彌、山中純子  
Production Design：田中直哉、フェルディナンド・パトゥリ  
Art Director：片塰満則  
編集：肥田文  
音響監督：清水洋史  
音楽：大野雄二、大谷和夫、fox capture plan  
Animation制作：トムス・エンタテインメント  
Animation制作協力：萌  
製作：Lupin三世 VS Cat's Eye製作委員会

### Cast

Lupin三世：[栗田貫一](https://natalie.mu/comic/artist/6789)  
次元大介：[大塚明夫](https://natalie.mu/comic/artist/26700)  
石川五ェ門：[浪川大輔](https://natalie.mu/comic/artist/45738)  
峰不二子：[沢城みゆき](https://natalie.mu/comic/artist/65954)  
銭形警部：[山寺宏一](https://natalie.mu/comic/artist/30965)  
来生瞳：[戸田恵子](https://natalie.mu/comic/artist/16682)  
来生泪：[深見梨加](https://natalie.mu/comic/artist/96805)  
来生愛：[坂本千夏](https://natalie.mu/comic/artist/95232)  
内海俊夫：[安原義人](https://natalie.mu/comic/artist/91212)  
永石：麦人  
銀河万丈  
東地宏樹  
菅生隆之


