https://natalie.mu/comic/news/391449

# 「MANGA都市TOKYO」開幕、エヴァなど全93タイトルから500点以上の資料展示

2020年8月12日  [Comment](https://natalie.mu/comic/news/391449/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


日本のマンガやアニメを扱った展覧会「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」が、本日8月12日に東京・国立新美術館で開幕。去る8月11日にプレス向け内覧会が行われた。  
以日本漫画和动画为主题的展览会“MANGA都市TOKYO日本漫画·动画·游戏·特摄2020”于8月12日在东京国立新美术馆开幕。8月11日举行了面向记者的内部展览会。

[
![「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report1.jpg?impolicy=hq&imwidth=730&imdensity=1)
「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」より。  
大きなサイズで見る（全39件）  
“MANGA都市TOKYO日本漫画·动画·游戏·特摄2020”。  
查看大图(共39件)  
](https://natalie.mu/comic/gallery/news/391449/1424607)

[
![「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」キービジュアル（イラストレーション：吉成曜） (c)Crypton Future Media, INC. www.piapro.net　(c)カラー　(c)Naoko Takeuchi　(c)武内直子・PNP・東映アニメーション　(c)秋本治・アトリエびーだま／集英社　(c)創通・サンライズ　TM & (c) TOHO CO., LTD.　(c)TOKYO TOWER](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_01.jpg?imwidth=468&imdensity=1)
「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」キービジュアル（イラストレーション：吉成曜） (c)Crypton Future Media, INC. www.piapro.net　(c)カラー　(c)Naoko Takeuchi　(c)武内直子・PNP・東映アニメーション　(c)秋本治・アトリエびーだま／集英社　(c)創通・サンライズ　TM & (c) TOHO CO., LTD.　(c)TOKYO TOWER［拡大］  
“MANGA都市TOKYO日本漫画·动画·游戏·特摄2020”KeyVisual(插图:吉成曜)(c)Crypton Future Media，INC. www.piapro.net (c)カラー(c)Naoko Takeuchi (c)武内直子·PNP·东映动画 (c)秋本治·工作室弹珠/集英社(c)创通·Sunrise TM & (c) TOHO CO.，LTD. (c)TOKYO TOWER
](https://natalie.mu/comic/gallery/news/391449/1339206)

[
![左からヴィッピー、ヨリコ。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report3.jpg?imwidth=468&imdensity=1)
左からヴィッピー、ヨリコ。［拡大］  
左起vippy、赖子。[放大]
](https://natalie.mu/comic/gallery/news/391449/1424610)

文化庁、独立行政法人日本芸術文化振興会、国立新美術館が主催する「MANGA都市TOKYO」は、2018年冬にフランス・パリのラ・ヴィレットにて開催された「MANGA⇔TOKYO」展の凱旋展示。マンガやアニメといったポップカルチャー作品と都市“東京”との関係を辿る内容だ。会場である企画展示室1Eの入り口では、展覧会のマスコットキャラクターであるヨリコとヴィッピーのスタンドが来場者を出迎える。ヨリコたちのキャラクターデザインとイラストは[吉成曜](https://natalie.mu/comic/artist/67403)が手がけており、キャラクター設定とデザインにコヤマシゲト、草野剛、ゲストキュレーターを務める森川嘉一郎氏が協力した。また場内に流れるヨリコの声は、船戸ゆり絵が担当している。  
由文化厅、独立行政法人日本艺术文化振兴会、国立新美术馆主办的“MANGA都市TOKYO”，是2018年冬季在Paris France的La Villette的“MANGA⇔TOKYO”展览的凯旋展示。内容是追溯漫画、动画等流行文化作品与都市“东京”的关系。在作为会场的企划展览室1E的入口，展览会的吉祥物依子和vippy的台灯迎接参观者。依子等的角色设计和插画由吉成曜亲自操刀，角色设定和设计由小山重和草野刚、担任嘉宾馆长的森川嘉一郎先生协助。另外，在场内播放的赖子的声音是由船户百合绘负责的。

[
![東京都市部の模型。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report5.jpg?imwidth=468&imdensity=1)
東京都市部の模型。［拡大］
](https://natalie.mu/comic/gallery/news/391449/1424612)

入り口を抜けた先に広がるのは、東京都市部を幅約17m、長さ約22mという大きさで再現した1/1000スケールの模型。その奥に設置されたスクリーンには、「[秒速5センチメートル](https://natalie.mu/eiga/film/146686)」「残響のテロル」「ラブライブ！」「STEINS;GATE」などの作品から、東京の風景を描いたシーンが次々と映されていく。模型が表す現実の都市と、スクリーンに映るフィクションの中の都市を重ね合わせるように鑑賞できる構成だ。  
入口前方展开的是以宽约17m，长约22m的大小再现东京都市的1/1000比例的模型。在那里面设置的屏幕上，“秒速5厘米”“残响的恐怖”“lovelive !”从“STEINS;GATE”等作品中，描绘东京风景的场景不断被放映。通过模型展示的现实城市和银幕呈现的虚构城市相互重叠。


[
![セクション1「破壊と復興の反復」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report4.jpg?imwidth=468&imdensity=1)
セクション1「破壊と復興の反復」より。［拡大］  
节选自第一小节“破坏与复兴的反复”。[扩大]  
](https://natalie.mu/comic/gallery/news/391449/1424611)

この巨大な模型とスクリーンの周りには、全93タイトルから500点以上におよぶ資料が「破壊と復興の反復」「東京の日常」「キャラクターvs.都市」という3つのセクションに分かれて展示されている。セクション1「破壊と復興の反復」では、東京の「破壊と復興」をテーマにした作品をピックアップ。ゴジラが映画で襲撃した場所を示した地図のほか、[大友克洋](https://natalie.mu/comic/artist/2225)「AKIRA」で“ネオ東京”が破壊される場面、「エヴァンゲリオン」シリーズの場面カットや設定資料などが展示された。さらにこのセクションでは「[人狼](https://natalie.mu/eiga/film/134853) JIN-ROH」「[千年女優](https://natalie.mu/eiga/film/139006)」「火要鎮」などのキャラクター設定画や絵コンテを見ることができる。  
在这个巨大的模型和屏幕周围，分为“破坏和复兴的反复”、“东京的日常”、“角色vs.城市”3个部分展示了共93个主题的500多件资料。第一部分“破坏与复兴的反复”精选了东京以“破坏与复兴”为主题的作品。除了哥斯拉在电影中袭击地点的地图外，还展示了大友克洋“AKIRA”中“新东京”被破坏的场面、《EVA》系列的场面剪辑和设定资料等。在这个部分还能看到“人狼JIN-ROH”、“千年女优”、“火要镇”等的角色设定画和分镜。

[
![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report11.jpg?imwidth=468&imdensity=1)
セクション2「東京の日常」より。［拡大］  
Part2“东京的日常”。[扩大]  
](https://natalie.mu/comic/gallery/news/391449/1424617)

続く「東京の日常」は東京における日常生活を描写した作品群を通して、生活の場としての東京の変遷を追うセクション。「プレ東京としての江戸」「近代化の幕開けからポストモダン都市まで」「世紀末から現在まで」の3コーナーに分かれており、それぞれのコーナーでは各時代の東京を舞台にした作品が特集された。まず「プレ東京としての江戸」では江戸を舞台にした[杉浦日向子](https://natalie.mu/comic/artist/2138)「百日紅」、[安野モヨコ](https://natalie.mu/comic/artist/1783)「さくらん」、[松本大洋](https://natalie.mu/comic/artist/2094)・永福一成「竹光侍」などにフィーチャーしている。次の「近代化の幕開けからポストモダン都市まで」では[和月伸宏](https://natalie.mu/comic/artist/4971)「るろうに剣心－明治剣客浪漫譚－」、[谷口ジロー](https://natalie.mu/comic/artist/2232)・関川夏央「『坊っちゃん』の時代」、[大和和紀](https://natalie.mu/comic/artist/2227)「はいからさんが通る」、高森朝雄・[ちばてつや](https://natalie.mu/comic/artist/1706)「あしたのジョー」、[上條淳士](https://natalie.mu/comic/artist/2102)「To-y」、[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」など、明治期から高度経済成長期までの東京を舞台にした作品が幅広く取り上げられた。  
接下来的“东京的日常”是通过描写东京日常生活的作品群，追踪作为生活场所的东京的变迁的部分。本次展览分为“作为东京的江户”、“从近代化的开始到后现代都市”、“从世纪末到现在”三个板块，每个板块都以各个时代的东京为舞台制作了作品特辑。首先是“作为东京的江户”，以江户为舞台的杉浦日向子的《百日红》、安野茂横的《樱》、松本大洋·永福一成的《竹光武士》等为特色。接下来的“从近代化的开始到后现代都市”，分别是和月伸宏的《浪客剑心-明治剑客浪漫谭-》、谷口次郎·关川夏央的《哥儿的时代》、大和和纪的《从高开始》、高森朝雄·千马铁也的《明天》《的乔》、上条淳士的《To-y》、北条司的《城市猎人》等，以明治时期到经济高度成长期的东京为舞台的作品被广泛选取。  

激動の時代を追った同コーナーを抜けた先、「世紀末から現在まで」には、現代の東京を舞台にした[浅野いにお](https://natalie.mu/comic/artist/2189)「ソラニン」、[羽海野チカ](https://natalie.mu/comic/artist/1800)「3月のライオン」などの資料が並ぶ。それぞれの作品に描かれているのは、経済的な低迷期に入った東京の駅のホームや、スーパーからの家路といった何気ない景色。「近代化の幕開けからポストモダン都市まで」後半の展示物に見られたきらびやかな大都市の風景と対照を成していた。  
穿过追逐激荡时代的该栏目，前面的“从世纪末到现在”陈列着以现代东京为舞台的浅野伊尼雄的《空人》、羽海野千花的《3月的狮子》等资料。每幅作品描绘的都是进入经济萧条期的东京车站站台、以及从超市回家的路上等看似平淡的景色。这与“从近代化的开始到后现代都市”后半段展示物中所看到的华丽的大都市风景形成了鲜明的对比。


[
![セクション3「キャラクターvs.都市」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report34.jpg?imwidth=468&imdensity=1)
セクション3「キャラクターvs.都市」より。［拡大］  
Part3“角色vs.城市”。[扩大]
](https://natalie.mu/comic/gallery/news/391449/1424640)

「東京の日常」で移り変わる東京の姿を楽しんだ後は、最後のセクションへ。「キャラクターvs.都市」と題されたこのセクション3では、ここまでとは逆に、お台場に設置中のユニコーンガンダム立像など、現実の都市に現れたキャラクターたちにフォーカスしている。足を踏み入れると視界に飛び込んでくるのは、初音ミクとコラボしたNewDaysの店舗と、「ラブライブ！」の広告が掲出された山手線の電車内を再現したインスタレーション。そのほか葛飾区の各所にある「こちら葛飾区亀有公園前派出所」登場キャラクターの像の設置場所や、「ラブライブ！」と神田祭がコラボしたときのグッズ、当日の様子を映した写真などが紹介されている。  
在《东京的日常》中欣赏完东京的变化后，进入最后的部分。以“角色vs.都市”为题的这个第3部分，与到这里相反，在台场设置中的独角兽高达立像等，聚焦在现实都市中出现的角色们。一踏进店内，映入眼帘的是与初音未来合作的NewDays店铺，以及“lovelive !”的广告，再现了山手线电车内的装置。此外，在葛饰区随处可见的“这里是葛饰区龟有公园前派出所”登场角色的雕像设置场所、“lovelive !”和神田祭合作时的商品、当天的照片等。  

「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」は11月3日まで開催。当初7月8日から9月22日までの開催が予定されていたが、新型コロナウイルスの影響を受けて変更された。また混雑緩和のため、入場には日時指定観覧券、もしくは日時指定券の予約が必要。詳細は展示会の特設サイトで確認しよう。  
“MANGA都市TOKYO日本漫画·动画·游戏·特摄2020”将持续到11月3日。原定于7月8日至9月22日举行，但由于受到新型冠状病毒的影响而变更。另外，为了缓解拥挤，入场时需要预约日期指定参观券或日期指定券。详情请登录展示会特设网站确认。  

この記事の画像・動画（全39件）  
该报道的图片、视频(共39件)  

[![「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424607 "「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」より。")
[![「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」キービジュアル（イラストレーション：吉成曜） (c)Crypton Future Media, INC. www.piapro.net　(c)カラー　(c)Naoko Takeuchi　(c)武内直子・PNP・東映アニメーション　(c)秋本治・アトリエびーだま／集英社　(c)創通・サンライズ　TM & (c) TOHO CO., LTD.　(c)TOKYO TOWER](https://ogre.natalie.mu/media/news/comic/2020/0226/mangatoshitokyo_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1339206 "「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」キービジュアル（イラストレーション：吉成曜） (c)Crypton Future Media, INC. www.piapro.net　(c)カラー　(c)Naoko Takeuchi　(c)武内直子・PNP・東映アニメーション　(c)秋本治・アトリエびーだま／集英社　(c)創通・サンライズ　TM & (c) TOHO CO., LTD.　(c)TOKYO TOWER")
[![左からヴィッピー、ヨリコ。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424610 "左からヴィッピー、ヨリコ。")
[![東京都市部の模型。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report5.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424612 "東京都市部の模型。")
[![セクション1「破壊と復興の反復」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424611 "セクション1「破壊と復興の反復」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424617 "セクション2「東京の日常」より。")
[![セクション3「キャラクターvs.都市」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report34.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424640 "セクション3「キャラクターvs.都市」より。")
[![セクション1「破壊と復興の反復」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report6.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424613 "セクション1「破壊と復興の反復」より。")
[![セクション1「破壊と復興の反復」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report7.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424614 "セクション1「破壊と復興の反復」より。")
[![セクション1「破壊と復興の反復」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report8.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424615 "セクション1「破壊と復興の反復」より。")
[![セクション1「破壊と復興の反復」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report9.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424606 "セクション1「破壊と復興の反復」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424616 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424618 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report13.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424619 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report14.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424620 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report15.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424621 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report19.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424625 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report20.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424626 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report18.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424624 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report21.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424627 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report22.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424628 "セクション2「東京の日常」より。")


[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report23.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424629 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report25.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424631 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report16.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424622 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report17.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424623 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report26.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424632 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report27.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424633 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report24.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424630 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report28.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424634 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report29.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424635 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report30.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424636 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report31.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424637 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report32.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424638 "セクション2「東京の日常」より。")
[![セクション2「東京の日常」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report33.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424639 "セクション2「東京の日常」より。")
[![セクション3「キャラクターvs.都市」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report35.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424641 "セクション3「キャラクターvs.都市」より。")
[![セクション3「キャラクターvs.都市」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report36.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424642 "セクション3「キャラクターvs.都市」より。")
[![セクション3「キャラクターvs.都市」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report37.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424643 "セクション3「キャラクターvs.都市」より。")
[![セクション3「キャラクターvs.都市」より。](https://ogre.natalie.mu/media/news/comic/2020/0811/MANGA_toshi_TOKYO_report38.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/391449/1424644 "セクション3「キャラクターvs.都市」より。")
[![7/8開幕『MANGA都市TOKYO』展（六本木・国立新美術館）PR映像](https://i.ytimg.com/vi/bBTYINoxrxI/default.jpg)](https://natalie.mu/comic/gallery/news/391449/media/46220 "7/8開幕『MANGA都市TOKYO』展（六本木・国立新美術館）PR映像")

## 「MANGA都市TOKYO ニッポンのマンガ・アニメ・ゲーム・特撮2020」

会場：東京都 国立新美術館 企画展示室1E  
期間：2020年8月12日（水）～11月3日（火・祝）  
時間：10:00～18:00  
休館日：毎週火曜日 ※9月22日（火）・11月3日（火）は開館、9月23日（水）は休館。  
料金：一般1600円、大学生1200円、高校生800円




LINK

[MANGA都市TOKYO](https://manga-toshi-tokyo.jp/)  
[【公式】MANGA都市TOKYO (@manga\_toshi\_tyo) | Twitter](https://twitter.com/manga_toshi_tyo)   
