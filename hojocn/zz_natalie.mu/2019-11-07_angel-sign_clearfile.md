https://natalie.mu/comic/news/354515

# 映画「Angel Sign」入場者特典に北条司が手がけた絵コンテ付クリアファイル

2019年11月7日  [Comment](https://natalie.mu/comic/news/354515/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)が総監督を務める実写映画「Angel Sign」の入場者特典が発表された。  
北条司担任总导演的真人电影《Angel Sign》发表了入场者特典。  

[
![「北条司絵コンテ付きクリアファイル」](https://ogre.natalie.mu/media/news/comic/2019/1107/angelsign_tokuten.jpg?impolicy=hq&imwidth=730&imdensity=1)
「北条司絵コンテ付きクリアファイル」  
大きなサイズで見る（全14件）  
“附北条司分镜的透明文件夹”  
查看大尺寸(共14件)  
](https://natalie.mu/comic/gallery/news/354515/1272453)

映画「Angel Sign」はセリフを用いず、映像と音楽のみでストーリーが展開していくオムニバス映画。11月15日の公開初日より、入場者特典としてA5サイズのクリアファイルと、本作の台本として使用された北条による絵コンテがセットになった「北条司絵コンテ付きクリアファイル」が配布される。絵コンテには松下奈緒演じるチェリストのアイカ、ディーン・フジオカ演じるアイカの恋人でピアニストのタカヤによるシーンが描かれた。数量限定のため、手に入れたい人は早めに劇場へ足を運ぼう。  
电影《Angel Sign》是不使用台词，仅靠影像和音乐展开故事的集锦电影。从11月15日的首映日开始，作为入场者特典的A5尺寸的透明文件夹和作为本作品脚本使用的北条的分镜的配套“附北条司分镜的ClearFile”被分发。分镜中描绘了松下奈绪饰演的大提琴家爱香，藤冈靛饰演的爱香的恋人钢琴家高也的场景。由于数量有限，想要入手的人请尽早前往剧场。  

この記事の画像・動画（全14件）  
这篇报道的图片、视频(共14篇)  

[![「北条司絵コンテ付きクリアファイル」](https://ogre.natalie.mu/media/news/comic/2019/1107/angelsign_tokuten.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1272453 "「北条司絵コンテ付きクリアファイル」")
[![映画「Angel Sign」メインカット](https://ogre.natalie.mu/media/news/comic/2019/1107/angelsign_main.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1272454 "映画「Angel Sign」メインカット")
[![映画「Angel Sign」ポスタービジュアル](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222327 "映画「Angel Sign」ポスタービジュアル")
[![「別れと始まり」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222336 "「別れと始まり」より。")
[![「別れと始まり」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222335 "「別れと始まり」より。")
[![「空へ」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222330 "「空へ」より。")
[![「空へ」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222329 "「空へ」より。")
[![「30分30秒」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222338 "「30分30秒」より。")
[![「30分30秒」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222337 "「30分30秒」より。")
[![「父の贈り物」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222332 "「父の贈り物」より。")
[![「父の贈り物」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222331 "「父の贈り物」より。")
[![「故郷へ」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222334 "「故郷へ」より。")
[![「故郷へ」より。](https://ogre.natalie.mu/media/news/comic/2019/0820/angelsign07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/354515/1222333 "「故郷へ」より。")
[![「Angel Sign」ティザー映像](https://i.ytimg.com/vi/Sg5E-CGB33o/default.jpg)](https://natalie.mu/comic/gallery/news/354515/media/39570 "「Angel Sign」ティザー映像")

## 映画「Angel Sign」

2019年11月15日（金）よりユナイテッド・シネマ豊洲ほかにて公開  
2019年11月15日(周五)在联合电影丰洲等上映  

企画：堀江信彦  
総監督：[北条司](https://natalie.mu/comic/artist/2405)  
監督：北条司、落合賢、ノンスィー・ニミブット、ハム・トラン、旭正嗣、カミラ・アンディニ  
出演：松下奈緒、ディーン・フジオカ、緒形直人、菊池桃子、佐藤二朗、プレオパン・パンイム、ピポッブ・カモンケットソーポン、ブンイン・インルアン、ゴー・クアン・トゥアン、スアン・ヴァン、坂井彩香、ニーンナラ・ブンビティパイシット、メカトロウィーゴ、トゥク・リフヌ・ウィカナ、アビゲイル、吉田美佳子  
製作：「Angel Sign」製作委員会  
配給：ノース・スターズ・ピクチャーズ

全文を表示

(c)「Angel Sign」製作委員会