https://natalie.mu/comic/news/41116


# 久保ミツロウ描く冴羽獠など、ゼノンで北条司トリビュート
久保ミツロウ画的冴羽獠等，在Zenon向北条司致敬  

2010年11月25日 [Comment](https://natalie.mu/comic/news/41116/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)のデビュー30周年を祝して、ゲスト作家によるトリビュートイラストなどを収めた画集が本日11月25日発売の月刊コミックゼノン2011年1月号（徳間書店）に付いてくる。  
为庆祝北条司出道30周年，收录了特邀作家的致敬和插图的画集将在11月25日发售的月刊Comic Zenon 2011年1月号(德间书店)上附送。

[
![久保ミツロウがトリビュートイラストで描いたのは「シティーハンター」。なお久保が「もっこり」を描くのは、今回が初とのこと。](https://ogre.natalie.mu/media/news/comic/2010/1125/kubo-cityhunter.jpg?imwidth=468&imdensity=1)
久保ミツロウがトリビュートイラストで描いたのは「シティーハンター」。なお久保が「もっこり」を描くのは、今回が初とのこと。  
大きなサイズで見る（全4件）  
久保光郎的致敬之作是《城市猎人》。另外，久保这是第一次画“Mokkori”。  
查看大图(共4件)  
](https://natalie.mu/comic/gallery/news/41116/63825)

画集では「モテキ」の[久保ミツロウ](https://natalie.mu/comic/artist/1875)、「アイシールド21」の[村田雄介](https://natalie.mu/comic/artist/2209)、「テルマエ・ロマエ」の[ヤマザキマリ](https://natalie.mu/comic/artist/2973)、「マイガール」の[佐原ミズ](https://natalie.mu/comic/artist/1973)、「アタック!!」の[大島司](https://natalie.mu/comic/artist/2223)、「ブッシメン！」の[小野洋一郎](https://natalie.mu/comic/artist/3898)らが北条作品を題材にイラストを描き下ろしている。北条の秘蔵イラストギャラリーも収録。  
画集中有《モテキ》的久保光郎、《アイシールド21》的村田雄介、《罗马浴场》的山崎麻里、《我的女孩》的佐原水、《Attack !!》的大岛司，《ブッシメン!》的小野洋一郎等人以北条的作品为题材绘制插画。北条的秘藏插图画廊也有收录。  

また北条トリビュートイラスト企画は今回が第3弾にあたり、過去2回に登場した12作家のイラストも画集には収められている。新規収録6名分の描き下ろしイラストは、本日から12月5日まで吉祥寺のCAFE ZENONに展示される予定だ。  
另外，北条致敬企划此次是第3弹，过去2回登场的12名作家的插图也收录在画集中。新收录的6幅插画将于即日起至12月5日在吉祥寺的CAFE ZENON展出。  

#### 北条司30周年記念 特製画集参加作家

第1弾：[井上雄彦](https://natalie.mu/comic/artist/1791)、[梅澤春人](https://natalie.mu/comic/artist/2349)、[朝基まさし](https://natalie.mu/comic/artist/2260)、[のりつけ雅春](https://natalie.mu/comic/artist/1725)、[新條まゆ](https://natalie.mu/comic/artist/2109)、[原哲夫](https://natalie.mu/comic/artist/1917)  
第2弾：[小畑友紀](https://natalie.mu/comic/artist/2076)、[芹沢直樹](https://natalie.mu/comic/artist/1897)、[高橋留美子](https://natalie.mu/comic/artist/1954)、[次原隆二](https://natalie.mu/comic/artist/3787)、[冨樫義博](https://natalie.mu/comic/artist/2377)、[渡辺航](https://natalie.mu/comic/artist/4511)  
第3弾：久保ミツロウ、村田雄介、ヤマザキマリ、佐原ミズ、大島司、小野洋一郎

この記事の画像（全4件）

[![久保ミツロウがトリビュートイラストで描いたのは「シティーハンター」。なお久保が「もっこり」を描くのは、今回が初とのこと。](https://ogre.natalie.mu/media/news/comic/2010/1125/kubo-cityhunter.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/41116/63825 "久保ミツロウがトリビュートイラストで描いたのは「シティーハンター」。なお久保が「もっこり」を描くのは、今回が初とのこと。（久保光郎的致敬之作是《城市猎人》。另外，久保这是第一次画“Mokkori”。）")
[![「北条司30周年記念 特製画集」表紙。](https://ogre.natalie.mu/media/news/comic/2010/1125/hojotsukasa30th.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/41116/63826 "「北条司30周年記念 特製画集」表紙。")
[![「北条司30周年記念 特製画集」参加者が確認できる裏表紙。](https://ogre.natalie.mu/media/news/comic/2010/1125/hojotsukasa30th2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/41116/63827 "「北条司30周年記念 特製画集」参加者が確認できる裏表紙。")
[![月刊コミックゼノン2011年1月号では、原哲夫原作による吉原基貴「サイバーブルー 失われた子供たち」もスタートした。](https://ogre.natalie.mu/media/news/comic/2010/1125/zenon11-1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/41116/63828 "月刊コミックゼノン2011年1月号では、原哲夫原作による吉原基貴「サイバーブルー 失われた子供たち」もスタートした。（月刊Comic Zenon 2011年1月号上，也开始了原哲夫原作的吉原基贵《Cyber Blue 失落的孩子们》。）")


LINK  
[CAFE ZENON | カフェゼノン](http://cafe-zenon.jp/index.html)  
[COMIC ZENON｜コミックゼノン](http://www.comic-zenon.jp/)  