https://natalie.mu/comic/news/162437


# 「Angel Heart 」ドラマ化記念特別版、リョウと香瑩の出会いを一気読み

2015年10月9日  [Comment](https://natalie.mu/comic/news/162437/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「Angel Heart 」のTVドラマが、10月11日に日本テレビ系にて放送スタート。これを記念した単行本「Angel Heart  ドラマ化記念 Special Edition」が、本日10月9日に発売された。  
北条司《Angel Heart》的电视剧将于10月11日在日本电视台开播。为纪念这一事件，单行本《Angel Heart Darma化纪念 Special Edition》于今天10月9日发售。

[
![「Angel Heart  ドラマ化記念 Special Edition」](https://ogre.natalie.mu/media/news/comic/2015/1009/ah-special.jpg?imwidth=468&imdensity=1)
「Angel Heart  ドラマ化記念 Special Edition」  
大きなサイズで見る（全3件）  
“Angel Heart Darma化纪念 Special Edition”  
大尺寸观看(共3件)  
](https://natalie.mu/comic/gallery/news/162437/419107)

「Angel Heart  ドラマ化記念 Special Edition」は、コンビニなどで販売される廉価版。1話から33話までが収められており、リョウ（リョウの漢字はけものへんに「僚」のつくり）と香瑩が出会い“親子”として歩み始めるまでを一気読みできる大ボリュームになっている。  
“Angel Heart Darma化纪念Special Edition”是在便利店等地销售的廉价版。收录了从第1话到第33话的内容，读者可以一口气读到獠(獠的汉字是“僚”的缩写)和香莹相遇成为“父女”的过程。

また巻頭のカラーページでは、ドラマで各キャラクターを演じるメインキャストの紹介とコメント、キャラクター相関図といったコーナーを展開。月刊コミックゼノン10月号（徳間書店）に掲載された、北条とリョウ役・上川隆也による対談も再編集して収められた。  
另外，卷首的彩页上，还会有在电视剧中饰演各角色的主要演员的介绍和评论，以及角色相关图等栏目。月刊Comic Zenon 10月号(德间书店)刊登的北条和獠役·上川隆也的对谈也重新编辑后收入其中。

「Angel Heart」は、「シティーハンター」のキャラクターや設定をもとに、パラレルワールドで生きるリョウたちの活躍を描く“IFストーリー”。10月20日には、ゼノンで連載中の「Angel Heart 2ndSeason」12巻が発売される。  
《Angel Heart》是以《城市猎人》的角色和设定为基础，描写在平行世界中生活的獠等人的活跃的“IFStory”。10月20日，在Zenon连载中的《Angel Heart 2ndSeason》第12卷将发售。

※記事初出時、見出しおよび本文における登場人物名に誤りがありました。お詫びして訂正いたします。  
※报道初次出现时，标题及正文中的登场人物名称有错误。向您道歉并更正。  

この記事の画像（全3件）  
这篇报道的图片(共3篇)  
[![「Angel Heart  ドラマ化記念 Special Edition」](https://ogre.natalie.mu/media/news/comic/2015/1009/ah-special.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/162437/419107 "「Angel Heart  ドラマ化記念 Special Edition」（《Angel Heart Darma化纪念Special Edition》）")
[![リョウ役・上川隆也（左）と北条司（右）。](https://ogre.natalie.mu/media/news/comic/2015/1009/angel-taidan.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/162437/419108 "リョウ役・上川隆也（左）と北条司（右）。（饰演獠的上川隆也(左)和北条司(右)。）")
[![「Angel Heart 2ndSeason」12巻](https://ogre.natalie.mu/media/news/comic/2015/1009/ah2nd12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/162437/419109 "「Angel Heart 2ndSeason」12巻")

LINK

[Comic Zenon｜「ANGEL HEART 2ndSeason」](http://www.comic-zenon.jp/magazine/angelheart.html)  
[Angel Heart  | 日本TV](http://www.ntv.co.jp/angelheart/)  