https://natalie.mu/comic/news/421170

# 「シティーハンター」リョウと香の誕生日イベントがカフェゼノン＆サカバで開催

2021年3月22日  [Comment](https://natalie.mu/comic/news/421170/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」とのコラボイベント「シティーハンター バースデーパーティー2021」が、東京・吉祥寺のカフェゼノン＆ゼノンサカバで本日3月22日から4月30日まで開催されている。  
与北条司“城市猎人”的合作活动“城市猎人生日派对2021”于3月22日至4月30日在东京吉祥寺的芝诺咖啡馆举行。  

[
![「シティーハンター バースデーパーティー2021」の告知画像。](https://ogre.natalie.mu/media/news/comic/2021/0322/zenon_CH_menu1.jpg?imwidth=468&imdensity=1)
「シティーハンター バースデーパーティー2021」の告知画像。  
大きなサイズで見る（全7件）  
“城市猎人生日派对2021”的宣传图。  
查看大图(共7件)  
](https://natalie.mu/comic/gallery/news/421170/1558166)

冴羽リョウの誕生日が3月26日、香の誕生日が3月31日と、3月は「シティーハンター」メインキャラクター2人の誕生日が集中するバースデーマンスであることからコラボイベントは決定した。期間中は「もっこりオムライス バースデーver.」「バースデーケーキプレート2021」 「名シーンラテ バースデーver.」「もっこりブルークリームソーダ」「香のミモザ」という、作品や誕生日にちなんだコラボメニューが提供される。各メニューには記念マグネットが1枚付属し、全5種をコンプリートした人にはシークレットバージョンの「誕生日記念マグネット」2枚をプレゼント。特典はなくなり次第終了となるため注意しよう。  
冴羽獠的生日是3月26日、香的生日是3月31日，3月是《城市猎人》中两位主要角色的生日集中的生日纪念日，因此决定举办合作活动。期间将推出“蛋包饭 生日ver.”“生日蛋糕板块2021”“名场景 拿铁生日ver.”“摩可利蓝奶油苏打”“香之米摩沙”，根据作品和生日提供合作菜单。每道菜都附赠一枚纪念磁铁，完成全套5种磁铁的人可获得秘密版“生日纪念磁铁”2枚。要注意的是，优惠一取消就结束了。

また店内では「シティーハンター」グッズを販売。3月26日には「シティーハンター XYZ生ガトーショコラ」、3月31日には「シティーハンター XYZチーズケーキ」、4月9日・10日には各日100セット限定のお皿「シティーハンター スペシャルプレート3枚セット 第1弾」が発売される。  
另外，店内还出售“城市猎人”周边商品。3月26日“城市猎人XYZ生蛋糕”，3月31日“城市猎人XYZ芝士蛋糕”，4月9日、10日每天100套限定盘子“城市猎人”特别牌3枚套装第1弹”被发售。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。
※冴羽獠的獠，在动物旁正式标记为“僚”。  

この記事の画像（全7件）  
这篇报道的图片(共7篇)  
[![「シティーハンター バースデーパーティー2021」の告知画像。  
“城市猎人生日派对2021”的宣传图。  
](https://ogre.natalie.mu/media/news/comic/2021/0322/zenon_CH_menu1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/421170/1558166 "「シティーハンター バースデーパーティー2021」の告知画像。")
[![「シティーハンター バースデーパーティー2021」コラボメニューの告知画像。  
“城市猎人生日派对2021”合作菜单的宣传图。  
](https://ogre.natalie.mu/media/news/comic/2021/0322/zenon_CH_menu2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/421170/1558167 "「シティーハンター バースデーパーティー2021」コラボメニューの告知画像。")
[![「シティーハンター スペシャルプレート3枚セット 第1弾」  
“城市猎人特别铭牌3枚套装第1弹“  
](https://ogre.natalie.mu/media/news/comic/2021/0322/zenon_CH_osaraset.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/421170/1558171 "「シティーハンター スペシャルプレート3枚セット 第1弾」")
[![「シティーハンター スペシャルプレート3枚セット 第1弾」  
“城市猎人特别铭牌3枚套装第1弹”
](https://ogre.natalie.mu/media/news/comic/2021/0322/zenon_CH_osaraset2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/421170/1558172 "「シティーハンター スペシャルプレート3枚セット 第1弾」")
[![「シティーハンター XYZ生ガトーショコラ」「シティーハンター XYZチーズケーキ」は、海坊主が営むコーヒーハウス「キャッツアイ」で作られたケーキをイメージしたスペシャルパッケージになっている。](https://ogre.natalie.mu/media/news/comic/2021/0322/zenon_CH_cake1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/421170/1558168 "「シティーハンター XYZ生ガトーショコラ」「シティーハンター XYZチーズケーキ」は、海坊主が営むコーヒーハウス「キャッツアイ」で作られたケーキをイメージしたスペシャルパッケージになっている。（“城市猎人XYZ生的玩具箱”“城市猎人”XYZ芝士蛋糕”是以海坊主经营的咖啡屋“猫眼”制作的蛋糕为形象的特别包装。）")
[![「シティーハンター XYZ生ガトーショコラ」と「シティーハンター XYZチーズケーキ」。](https://ogre.natalie.mu/media/news/comic/2021/0322/zenon_CH_cake2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/421170/1558169 "「シティーハンター XYZ生ガトーショコラ」と「シティーハンター XYZチーズケーキ」。（“城市猎人XYZ生蛋糕”和“城市猎人XYZ芝士蛋糕”。）")
[![ケーキのパッケージ。  
蛋糕的包装。  
](https://ogre.natalie.mu/media/news/comic/2021/0322/zenon_CH_cake3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/421170/1558170 "ケーキのパッケージ。")

(c)北条司/Coamix 1985


LINK

[吉祥寺 ZENON SAKABA | Zeno Sakaba](https://zenon-sakaba.jp/news/2021/03/12/617/)

