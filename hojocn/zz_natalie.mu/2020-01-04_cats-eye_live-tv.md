https://natalie.mu/eiga/news/361880

# 早見優らが三姉妹演じたドラマ「キャッツ・アイ」放送、刑事役は西城秀樹

2020年1月4日 [Comment](https://natalie.mu/eiga/news/361880/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)

[西城秀樹](https://natalie.mu/eiga/artist/14949)が出演した1988年製作のドラマ「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」が、明日1月5日に衛星劇場でCS初放送される。  
西城秀树出演的1988年制作的电视剧《CAT'S EYE 猫眼 Midnight是恋爱的Aventure》将于明天1月5日在卫星剧场CS首播。  

[
![「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」](https://ogre.natalie.mu/media/news/eiga/2020/0102/catseye_202001_01.jpg?impolicy=hq&imwidth=730&imdensity=1)
「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」
大きなサイズで見る（全2件）
](https://natalie.mu/eiga/gallery/news/361880/1307974)

[
![「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」](https://ogre.natalie.mu/media/news/eiga/2020/0102/catseye_202001_02.jpg?imwidth=468&imdensity=1)
「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」［拡大］
](https://natalie.mu/eiga/gallery/news/361880/1307975)

[北条司](https://natalie.mu/eiga/artist/2405)の「キャッツ・アイ」をドラマ化した本作。喫茶店を切り盛りしながら、裏では怪盗CAT'S EYEとして絵画を盗む美人三姉妹の姿が描かれる。[早見優](https://natalie.mu/eiga/artist/16328)、[MIE](https://natalie.mu/eiga/artist/33584)、[立花理佐](https://natalie.mu/eiga/artist/13534)が三姉妹に扮し、西城は刑事の[内海俊夫](https://natalie.mu/eiga/artist/32292)を演じた。そのほか[小倉久寛](https://natalie.mu/eiga/artist/12782)、高橋良明、[京本政樹](https://natalie.mu/eiga/artist/11992)、[原田大二郎](https://natalie.mu/eiga/artist/14209)がキャストに名を連ねている。  
北条司的《CAT'S EYE》电视剧化的本作品。描写了一边经营咖啡店、一边在暗地里作为怪盗CAT'S EYE偷画的美女三姐妹的形象。早见优、MIE、立花理佐扮演三姐妹，西城扮演刑警内海俊夫。此外，小仓久宽、高桥良明、京本政树、原田大二郎也榜上有名。  

なお本作は、同局の「フォーエバー・ヒデキ～西城秀樹出演作より～」枠でオンエアされる。  
另外，本作品将在同局的“Forever 秀樹~西城秀树出演作品中~”时段播出。  


## CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール
CAT'S EYE 猫眼 Midnight是恋爱的Aventure  

衛星劇場 2020年1月5日（日）13:30～ほか
卫星剧场 2020年1月5日(周日)13:30 ~其他

この記事の画像（全2件）  
这篇报道的图片(共两篇)  

[![「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」](https://ogre.natalie.mu/media/news/eiga/2020/0102/catseye_202001_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/361880/1307974 "「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」")
[![「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」](https://ogre.natalie.mu/media/news/eiga/2020/0102/catseye_202001_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/361880/1307975 "「CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール」")

(c)北条司/NSP 1981 (c)NTV




LINK

[CAT'S EYE キャッツ・アイ ミッドナイトは恋のアバンチュール | 衛星劇場](https://www.eigeki.com/lineup?action=detail&id=108903)


関連商品

[
![北条司「キャッツ・アイ 文庫版全10巻完結セット」](https://images-fe.ssl-images-amazon.com/images/I/51Qo1YRToJL._SS70_.jpg)  
北条司「キャッツ・アイ 文庫版全10巻完結セット」(北条司“猫眼文库版全10卷完结套装”)  
[2Blu-ray Disc＋PhotoBook] 2017年7月19日発売 / 978-4086189293  
Amazon.co.jp [書籍版 / Kindle版]  
](https://www.amazon.co.jp/exec/obidos/ASIN/4086189291/natalieeiga-22)