https://natalie.mu/eiga/news/352367

# 北条司が描いた「エンジェルサイン」コンテ映像到着、イベント開催も決定

2019年10月21日  [Comment](https://natalie.mu/eiga/news/352367/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


マンガ家・[北条司](https://natalie.mu/eiga/artist/2405)が総監督を務める実写映画「[エンジェルサイン](https://natalie.mu/eiga/film/182396)」の特別映像がYouTubeで公開。あわせて11月16日、17日に東京のユナイテッド・シネマ豊洲で舞台挨拶が行われることも決定した。  
由漫画家北条司担任总导演的真人电影《天使印记》的特别影像在YouTube上公开。11月16日、17日将在东京的联合电影丰洲举行舞台致辞。

[
![「エンジェルサイン」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」ポスタービジュアル  
大きなサイズで見る（全16件）  
“天使印记”海报视觉效果(共16件)  
](https://natalie.mu/eiga/gallery/news/352367/1222121)

本作は、世界中から寄せられたサイレントマンガオーディションの受賞作をアジア各国の監督が映像化したオムニバス。人々に訪れる奇跡を映像と音楽のみで描いた6つの物語が展開する。北条が「プロローグ」「エピローグ」でメガホンを取り、両作には[松下奈緒](https://natalie.mu/eiga/artist/11381)と[ディーン・フジオカ](https://natalie.mu/eiga/artist/65834)が出演した。  
本作品是亚洲各国的导演将从世界各地被寄来的默白漫画大赛的获奖作品影像化的集锦。影片用影像和音乐讲述了降临人间的奇迹，讲述了6个故事。北条为《序幕》和《尾声》掌镜，两部作品分别由松下奈绪和藤冈靛出演。  

[
![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」［拡大］
](https://natalie.mu/eiga/gallery/news/352367/1189295)

映像には北条をはじめ、[富沢順](https://natalie.mu/eiga/artist/4740)、[次原隆二](https://natalie.mu/eiga/artist/3787)といったマンガ家が描き下ろした絵コンテとVコンテを収録。本編とVコンテの比較映像も観ることができる。
影像收录了以北条为首，富泽顺、次原隆二等漫画家所画的分镜和V镜。还可以观看正片和VConté的比较影像。  

[
![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」［拡大］
](https://natalie.mu/eiga/gallery/news/352367/1189290)

また舞台挨拶には、16日に松下とディーン、17日に北条と本作の企画を立ち上げた堀江信彦が登壇。チケットは明日10月22日正午にローソンチケットで先行抽選受付を開始し、11月4日18時より一般販売される。  
另外，16日松下和靛、17日北条和本作品企划的堀江信彦将登台致辞。门票将于明天10月22日正午在Lawson门票开始先行抽签，11月4日18时开始一般销售。

「エンジェルサイン」は11月15日より公開。  
《天使印记》将于11月15日上映。  

この記事の画像・動画（全16件）  
这篇报道的图片、视频(共16篇)  

[![「エンジェルサイン」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222121 "「エンジェルサイン」ポスタービジュアル")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1189295 "「エンジェルサイン」")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1189290 "「エンジェルサイン」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222122 "「別れと始まり」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222123 "「別れと始まり」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222124 "「父の贈り物」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222125 "「空へ」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222126 "「空へ」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222127 "「30分30秒」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222128 "「30分30秒」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222129 "「父の贈り物」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222130 "「故郷へ」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222131 "「故郷へ」")
[![左から緒形直人、菊池桃子、佐藤二朗。](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1222132 "左から緒形直人、菊池桃子、佐藤二朗。")
[![左からディーン・フジオカ、松下奈緒。](https://ogre.natalie.mu/media/news/eiga/2019/0924/angelsign_201909_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/352367/1244280 "左からディーン・フジオカ、松下奈緒。")
[![「エンジェルサイン」特別映像](https://i.ytimg.com/vi/4cFhG0GU3oM/default.jpg)](https://natalie.mu/eiga/gallery/news/352367/media/42748 "「エンジェルサイン」特別映像")

(c)「エンジェルサイン」製作委員会


LINK

[「エンジェルサイン」公式サイト](https://angelsign.jp/)  
[AngelSignMovie (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[「エンジェルサイン」特別映像](https://youtu.be/4cFhG0GU3oM)  