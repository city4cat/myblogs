
https://natalie.mu/comic/news/273117


# Jump50周年CD第2弾発売！封神演義や星矢、ヒロアカなどの曲も新たに収録

2018年3月12日 [Comment](https://natalie.mu/comic/news/273117/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


週刊少年Jump（集英社）の50周年を記念したCD「週刊少年Jump 50th Anniversary BEST ANIME MIX vol.2」が、4月4日にリリースされる。  
为纪念周刊少年Jump(集英社)发行50周年而发行的CD《周刊少年Jump 50th Anniversary BEST ANIME MIX vol.2》将于4月4日发行。  

[
![「週刊少年Jump50th Anniversity BEST ANIME MIX vol.2」](https://ogre.natalie.mu/media/news/comic/2018/0311/jump50th_anime.jpg?imwidth=468&imdensity=1)
「週刊少年Jump 50th Anniversity BEST ANIME MIX vol.2」  
大きなサイズで見る  
《周刊少年Jump 50th Anniversity BEST ANIME MIX vol.2》  
用大尺寸看  
](https://natalie.mu/comic/gallery/news/273117/884984)

1月に「vol.1」がリリースされた「週刊少年Jump50th Anniversary BEST ANIME MIX」では、週刊少年Jump掲載作を原作とするアニメの主題歌など計50曲を、DJシーザーが選曲しノンストップでMix。「vol.2」には「vol.1」に引き続き[尾田栄一郎](https://natalie.mu/comic/artist/2368)「ONE PIECE」、[北条司](https://natalie.mu/comic/artist/2405)「City Hunter」、[高橋陽一](https://natalie.mu/comic/artist/1953)「キャプテン翼」、[岸本斉史](https://natalie.mu/comic/artist/3293)「NARUTO-ナルト-」、[久保帯人](https://natalie.mu/comic/artist/3347)「BLEACH」、[古舘春一](https://natalie.mu/comic/artist/8561)「ハイキュー!!」、[冨樫義博](https://natalie.mu/comic/artist/2377)「幽☆遊☆白書」などの楽曲が収められているほか、新たに[藤崎竜](https://natalie.mu/comic/artist/2315)「封神演義」、[秋本治](https://natalie.mu/comic/artist/2053)「こちら葛飾区亀有公園前派出所」、[河下水希](https://natalie.mu/comic/artist/1831)「いちご100％」、[車田正美](https://natalie.mu/comic/artist/2044)「聖闘士星矢」、[堀越耕平](https://natalie.mu/comic/artist/8764)「僕のヒーローアカデミア」などの楽曲も収録される。  
1月发行“vol.1”的《周刊少年Jump 50th Anniversary BEST ANIME》MIX”，以《周刊少年Jump》刊载作品为原作的动画主题曲等共计50首歌曲，由DJ Caesar选曲不间断混音。《vol.2》继《vol.1》之后，尾田荣一郎《ONEPIECE》、北条司《城市猎人》、高桥阳一《足球小将》、岸本齐史《火影忍者》、久保带人《死神》、古馆春一《排球少年!!》，富坚义博的《幽游白书》等的乐曲之外，还收录了藤崎龙的《封神演义》、秋本治的《葛饰区龟有公园前派出所》、河下水希的《一护100%》、车田正美的《圣斗士星矢》、堀越耕平的《我的Hero Academia》等歌曲也被收录。

## 「週刊少年Jump 50th Anniversary BEST ANIME MIX vol.2」収録曲

01.「銀魂」／[SPYAIR](https://natalie.mu/comic/artist/9187)「サムライハート（Some Like It Hot!!）」  
02.「幽☆遊☆白書」／高橋ひろ「アンバランスなKissをして」  
03.「封神演義」／米倉千尋「WILL」  
04.「ボボボーボ・ボーボボ」／[mihimaru GT](https://natalie.mu/comic/artist/276)「H.P.S.J.」  
05.「花さか天使テンテンくん」／ブラブラブラボーズ「クラスで一番スゴイやつ」  
06.「ヒカルの碁」／[片瀬那奈](https://natalie.mu/comic/artist/48919)「FANTASY」  
07.「NARUTO-ナルト-」／little by little「悲しみをやさしさに」  
08.「NINKU -忍空-」／鈴木結女「輝きは君の中に」  
09.「City Hunter」／FENCE OF DEFENSE「SARA」  
10.「City Hunter」／小比類巻かほる「City Hunter ～愛よ消えないで～」  
11.「ついでにとんちんかん」／うしろ髪ひかれ隊「ごめんねカウボーイ」  
12.「Dragon Ball」／高橋洋樹「魔訶不思議アドベンチャー！」  
13.「ジャングルの王者ターちゃん」／[access](https://natalie.mu/comic/artist/1068)「MISTY HEARTBREAK」  
14.「JoJoの奇妙な冒険」／Coda「BLOODY STREAM」  
15.「こちら葛飾区亀有公園前派出所」／[堂島孝平](https://natalie.mu/comic/artist/876)「葛飾ラプソディー（TVサイズ）」  
16.「ニセコイ」／桐崎千棘（[東山奈央](https://natalie.mu/comic/artist/70914)）「Heart Pattern」  
17.「斉木楠雄のΨ難」／[でんぱ組.inc](https://natalie.mu/comic/artist/9608)「最Ψ最好調！」  
18.「ハイキュー!!」／[スキマスイッチ](https://natalie.mu/comic/artist/136)「Ah Yeah!!」  
19.「ニセコイ」／[ClariS](https://natalie.mu/comic/artist/10550)「STEP」  
20.「BORUTO-ボルト-NARUTO NEXT GENERATIONS」／[the peggies](https://natalie.mu/comic/artist/11290)「ドリーミージャーニー」  
21.「NARUTO-ナルト-疾風伝」／[いきものがかり](https://natalie.mu/comic/artist/763)「ブルーバード」  
22.「キャプテン翼」／小粥よう子「明日に向ってシュート」  
23.「風魔の小次郎」／三浦秀美「風の戦士」  
24.「BLEACH」／[ユンナ](https://natalie.mu/comic/artist/45739)「ほうき星」  
25.「D.Gray-man」／access「Doubt & Trust ～ダウト＆トラスト～」  
26.「いちご100％」／[HINOIチーム](https://natalie.mu/comic/artist/230)「IKE IKE（JAPANESE VERSION）」  
27.「べるぜバブ」／[佐々木希](https://natalie.mu/comic/artist/56565)「パペピプ♪パピペプ♪パペピプポ♪」  
28.「みどりのマキバオー」／F.MAP「走れマキバオー（原曲「走れコウタロー」）  
29.「こちら葛飾区亀有公園前派出所」／[テツandトモ](https://natalie.mu/comic/artist/6586)「テツandトモのなんでだろう～こち亀バージョン～」  
30.「きまぐれオレンジ☆ロード」／中原めいこ「鏡の中のアクトレス」  
31.「ハイスクール！奇面組」／うしろゆびさされ組「うしろゆびさされ組」  
32.「聖闘士星矢」／MAKE-UP「ペガサス幻想（ファンタジー）（T.V.Size）」  
33.「銀魂」／[RIZE](https://natalie.mu/comic/artist/1188)「SILVER」  
34.「BLEACH」／[UVERworld](https://natalie.mu/comic/artist/30)「D-tecnoLife」  
35.「NARUTO-ナルト-」／[FLOW](https://natalie.mu/comic/artist/728)「Re:member」  
36.「魔人探偵脳噛ネウロ」／[NIGHTMARE](https://natalie.mu/comic/artist/1294)「DIRTY」  
37.「黒子のバスケ」／[OLDCODEX](https://natalie.mu/comic/artist/10577)「カタルリズム」  
38.「ONE PIECE」／きただにひろし「ウィーゴー！」  
39.「めだかボックス」／[栗林みな実](https://natalie.mu/comic/artist/92869)「HAPPY CRAZY BOX」  
40.「バクマン。」／[YA-KYIM](https://natalie.mu/comic/artist/1552)「BAKUROCK ～未来の輪郭線～」  
41.「僕のヒーローアカデミア」／[Little Glee Monster](https://natalie.mu/comic/artist/10900)「だから、ひとりじゃない」  
42.「BLEACH」／[BEAT CRUSADERS](https://natalie.mu/comic/artist/226)「TONIGHT,TONIGHT,TONIGHT」  
43.「To LOVEる-とらぶる-」／[THYME](https://natalie.mu/comic/artist/1495)「forever we can make it!」  
44.「ハイキュー!! セカンドシーズン」／[BURNOUT SYNDROMES](https://natalie.mu/comic/artist/93516)「FLY HIGH!!」  
45.「食戟のソーマ」／[SCREEN mode](https://natalie.mu/comic/artist/10799)「ROUGH DIAMONDS」  
46.「銀魂」／[BLUE ENCOUNT](https://natalie.mu/comic/artist/10834)「DAY×DAY」  
47.「D.Gray-man」／[玉置成実](https://natalie.mu/comic/artist/165)「Brightdown」  
48.「べるぜバブ」／[中川翔子](https://natalie.mu/comic/artist/192)「つよがり」  
49.「僕のヒーローアカデミア」／[Brian the Sun](https://natalie.mu/comic/artist/10942)「HEROES」  
50.「ONE PIECE」／大槻真希「RUN! RUN! RUN!」



(c)秋本治・アトリエびーだま／集英社・ＡＤＫ (c)新沢基栄／NAS (c)えんどコイチ／集英社・NAS (c)徳弘正也／集英社・テレビ東京・アミューズ (c)バードスタジオ／集英社・東映アニメーション (c)北条司／NSP・読売テレビ・サンライズ (c)まつもと泉／集英社・日本テレビ・東宝 (c)荒木飛呂彦/集英社・ジョジョの奇妙な冒険製作委員会 原作：高橋陽一／集英社 (c)車田正美／集英社・東映アニメーション (c)桐山光侍/集英社・ぴえろ (c)安能務・藤崎竜／集英社・テレビ東京・封神プロジェクト１９９９ (c)つの丸・スタジオ将軍／集英社・ぴえろ (c)尾田栄一郎／集英社・フジテレビ・東映アニメーション 原作／冨樫義博「幽☆遊☆白書」（集英社「JumpコMix」刊）(c)Yoshihiro Togashi 1990年－1994年 (c)ぴえろ／集英社 (c)ほったゆみ・HMC・小畑健・ノエル／集英社・テレビ東京・電通・ぴえろ (c)岸本斉史 スコット／集英社・テレビ東京・ぴえろ (c)小栗かずまた/集英社・日本アニメーション (c)河下水希・集英社／いちご100％製作委員会 (c)久保帯人／集英社・テレビ東京・dentsu・ぴえろ (c)澤井啓夫／集英社・東映アニメーション (c)矢吹健太朗・長谷見沙貴／集英社・とらぶる製作委員会 (c)松井優征／集英社・VAP・マッドハウス・NTV・D.N.ドリームパートナーズ (c)空知英秋／集英社・テレビ東京・電通・BNP・アニプレックス (c)星野桂／集英社・テレビ東京・電通・TMS (c)大場つぐみ・小畑健・集英社／NHK・NEP・ShoPro (c)藤巻忠俊／集英社・黒子のバスケ製作委員会 (c)田村隆平／集英社・べるぜバブ製作委員会 2011 (c)附田祐斗・佐伯俊／集英社・遠月学園動画研究会弐 (c)古舘春一／集英社・「ハイキュー!!」製作委員会・MBS (c)麻生周一／集英社・PK学園2 (c)2012 西尾維新・暁月あきら／集英社・箱庭学園生徒会 (c)堀越耕平／集英社・僕のヒーローアカデミア製作委員会 (c)岸本斉史 スコット／集英社・テレビ東京・ぴえろ (c)JUMP 50th Anniversary



関連記事

[
![Jump50周年のMixCDが来春発売、男一匹ガキ大将からハイキュー!!まで](https://ogre.natalie.mu/media/news/comic/2017/1217/jump50th_CD.jpg?impolicy=thumb_fill&cropPlace=Center&width=140&height=140)
Jump50周年のMixCDが来春発売、男一匹ガキ大将からハイキュー!!まで
2017年12月17日
](https://natalie.mu/comic/news/261425)

[
![Jump50周年MixCD第3弾、1/3の純情な感情、romance、真赤な誓いも](https://ogre.natalie.mu/media/news/comic/2018/0603/jump50th_CD.jpg?impolicy=thumb_fit&width=140&height=140)
Jump50周年MixCD第3弾、1/3の純情な感情、romance、真赤な誓いも
2018年6月4日
](https://natalie.mu/comic/news/285067)



LINK

[「週刊少年Jump 50th Anniversity BEST ANIME MIX vol.2」 | Sony Music Official Site](http://www.sonymusic.co.jp/artist/Compilation/discography/ESCL-5044)  
[「週刊少年Jump 50th Anniversary BEST ANIME MIX vol.1」| Sony Music Official Site](http://www.sonymusic.co.jp/artist/Compilation/discography/ESCL-4955)  

関連商品

[
![V.A.「週刊少年Jump 50th Anniversity BEST ANIME MIX vol.2」](https://images-fe.ssl-images-amazon.com/images/I/71omn7J0xKL._SS70_.jpg)
V.A.「週刊少年Jump 50th Anniversity BEST ANIME MIX vol.2」
[CD] 2018年4月4日発売 / EPIC Record Japan / ESCL-5044
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B07BC5ZPZ7/nataliecomic-22)

[
![週刊少年Jump 50th Anniversary BEST ANIME MIX vol.1](https://images-fe.ssl-images-amazon.com/images/I/61Z7e6NmHqL._SS70_.jpg)
週刊少年Jump 50th Anniversary BEST ANIME MIX vol.1
[CD] 2018年1月10日発売 / EPIC RECORDS JAPAN
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B07894H9GW/nataliecomic-22)

