https://natalie.mu/comic/news/75354

# 北条司プロデュースで、田中克樹がラーメン人情劇を執筆
北条司Produce，田中克树执笔Ramen（译注：拉面）人情剧  

2012年8月26日 [Comment](https://natalie.mu/comic/news/75354/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)プロデュース・香月均史原作による前後編読み切り「ラーメン侍-初代熱風録-」を「バリエンテス 伊達の鬼 片倉小十郎」の[田中克樹](https://natalie.mu/comic/artist/8123)が執筆。発売中の月刊コミックゼノン10月号（徳間書店）に前編が掲載されている。  
北条司Produce、香月均史原作的前后篇《Ramen武士-初代热风录-》由《Valientes 伊达的鬼片仓小十郎》的田中克树执笔。发售中的月刊Comic Zenon 10月号(德间书店)刊登了前篇。

[
![月刊コミックゼノン10月号](https://ogre.natalie.mu/media/news/comic/2012/0825/zenon12-10.jpg?imwidth=468&imdensity=1)
月刊コミックゼノン10月号  
大きなサイズで見る  
月刊Comic Zenon 10月号  
查看大图
](https://natalie.mu/comic/gallery/news/75354/135779)

「ラーメン侍-初代熱風録-」は、映画「ラーメン侍」を原作とした人情劇。ラーメン店を営む主人公は、屋台仲間からも信頼の厚い熱血漢だ。物語は久留米の街で再開発が起こり、店が立ち退きを迫られるという筋書き。  
《Ramen武士-初代热风录-》是以电影《Ramen武士》为原作的人情剧。经营Ramen店的主人公是深受摊主们信赖的热血汉子。故事发生在久留米的街道重新开发，店家被迫拆迁的故事。  

作品の題材になっているラーメン店「大砲ラーメン」の店主と北条は、実は大学時代の同級生。月刊コミックゼノン10月号では、北条が同作をプロデュースすることになった経緯やラーメンにまつわる思い出話も語られた。  
作为作品题材的Ramen店“大炮拉面”的店主和北条实际上是大学时代的同学。月刊Comic Zenon 10月号上，北条担任该作品制作人的经过并讲述了关于Ramen的回忆。




LINK  
[Comic Zenon](http://www.comic-zenon.jp/)

