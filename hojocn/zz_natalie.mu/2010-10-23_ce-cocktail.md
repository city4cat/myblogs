https://natalie.mu/comic/news/39509

# CAFE ZENONに「キャッツ❤アイ」イメージしたカクテル登場
CAFE ZENON中以「Cat's❤Eye」为灵感的Cocktail

2010年10月23日  [Comment](https://natalie.mu/comic/news/39509/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)の代表作「キャッツ❤アイ」を題材にしたカクテルが、吉祥寺CAFE ZENONのメニューに10月25日から登場する。  
以北条司的代表作“Cat's❤Eye”为题材的鸡尾酒，从10月25日开始出现在吉祥寺CAFE ZENON的菜单上。  

[
![カクテルを頼むと数量限定で付いてくる「キャッツカードコースター」。](https://ogre.natalie.mu/media/news/comic/2010/1023/i_COASTER_cats_RED.jpg?imwidth=468&imdensity=1)
カクテルを頼むと数量限定で付いてくる「キャッツカードコースター」。  
大きなサイズで見る（全6件）  
点鸡尾酒的话会附带限量版的“Cat's Card杯垫”。  
查看大图(共6件)  
](https://natalie.mu/comic/gallery/news/39509/60662)

カクテルは「キャッツ❤アイ」の主要キャラクターである3姉妹をイメージした3種類を用意。長女をイメージした「ルイ　～Bouquet de Larmes　泪の花束～」はヴァイオレットリキュールベース、次女をイメージした「ヒトミ　～Eyes of Blue　青の瞳～」はブルーキュラソーベース、三女をイメージした「アイ　～Love in the Girlhood　少女時代の愛～」はオレンジジュースベースのノンアルコールカクテルだ。  
鸡尾酒是以“Cat's❤Eye”主要角色3姐妹为印象准备的3种。以长女为形象的“泪~ Bouquet de Larmes 泪之花束~”以紫罗兰利口酒为主，以次女为形象的“瞳~ Eyes of Blue 蓝之瞳~”以蓝咖喱酒为主，而以三女儿为形象的“爱~ Love in the Girlhood 少女时代的爱~”是以橙汁为基调的无酒精鸡尾酒。

また「キャッツ❤アイ」カクテルを注文した人には数量限定で、同作に登場する「キャッツカード」を模したコースターをプレゼントする。原作同様に裏面にはメッセージが書き込める仕様なので、伝言などに活用しよう。  
另外，点“Cat's❤Eye”鸡尾酒的人将获赠限定数量的该款“Cat's Card”造型杯垫。与原作一样，可以在杯垫的背面写上信息，因此可以用于留言等。  

なお10月25日創刊の新雑誌・月刊コミックゼノン（徳間書店）では、「キャッツ❤アイ」を原作にした阿佐維シン作画のスピンアウト「キャッツ❤愛」がスタートする。  
另外，在10月25日创刊的新杂志月刊Comic Zenon(德间书店)中，以“Cat's❤Eye”为原作，由阿佐维新作画的衍生作品“Cat's❤爱”即将开始。

#### CAFE ZENON特典

・10月25日から11月30日まで、カウンター席（キャッツシート）に座って「キャッツ❤アイ」カクテルをオーダーすると飲食代が20％OFF（15:00以降の来店に限る※その他サービスとの併用は不可）。  
10月25日至11月30日，坐在吧台座位(Cat's Seat)点“Cat's❤Eye”鸡尾酒的话餐费打8折(仅限于15:00以后来店※不可与其他服务一起使用)。

・10月25日限定で飲食代全品半額。  
10月25日限定餐饮费全部半价。  

・店頭で販売している月刊コミックゼノン創刊号（30冊限定）を購入すると飲食代が70％OFF。  
购买店内销售的月刊Comic Zenon创刊号(限定30册)，餐饮费打七折。


この記事の画像（全6件）  
这篇报道的图片(共6篇)  

[![カクテルを頼むと数量限定で付いてくる「キャッツカードコースター」。](https://ogre.natalie.mu/media/news/comic/2010/1023/i_COASTER_cats_RED.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39509/60662 "カクテルを頼むと数量限定で付いてくる「キャッツカードコースター」。")
[![「アイ　～Love in the Girlhood　少女時代の愛～」](https://ogre.natalie.mu/media/news/comic/2010/1023/ai.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39509/60663 "「アイ　～Love in the Girlhood　少女時代の愛～」（“爱~ Love in the Girlhood 少女时代的爱~”）")
[![「ルイ　～Bouquet de Larmes　泪の花束～」](https://ogre.natalie.mu/media/news/comic/2010/1023/rui.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39509/60664 "「ルイ　～Bouquet de Larmes　泪の花束～」（“泪~ Bouquet de Larmes 泪之花束~”）")
[![「ヒトミ　～Eyes of Blue　青の瞳～」](https://ogre.natalie.mu/media/news/comic/2010/1023/hitomi.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39509/60665 "「ヒトミ　～Eyes of Blue　青の瞳～」（“瞳 ~Eyes of Blue 蓝之瞳~”）")
[![カクテル3種類](https://ogre.natalie.mu/media/news/comic/2010/1023/cats-all.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39509/60666 "カクテル3種類（3种鸡尾酒）")
[![「キャッツ❤アイ」カクテルのチラシ。](https://ogre.natalie.mu/media/news/comic/2010/1023/cats-flyer.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/39509/60667 "「キャッツ❤アイ」カクテルのチラシ。（“Cat's❤Eye”鸡尾酒的传单。）")


LINK  
[CAFE ZENON | カフェゼノン](http://www.cafe-zenon.jp/)  
[COMIC ZENON｜コミックゼノン](http://www.comic-zenon.jp/)  

