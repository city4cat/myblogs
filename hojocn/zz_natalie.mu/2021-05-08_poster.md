https://natalie.mu/comic/news/427424

# 宝塚雪組による舞台「シティーハンター」ポスター公開！リョウや香、海坊主も
宝冢雪组舞台剧《城市猎人》海报公开!凉、香、海坊主  


2021年5月8日 [Comment](https://natalie.mu/comic/news/427424/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」を原作とした舞台「『CITY HUNTER』－盗まれたXYZ－」および、「ショー オルケスタ『Fire Fever!』」のポスタービジュアルが公開された。  
以北条司的《城市猎人》为原作的舞台剧「『CITY HUNTER』—被盗的XYZ—」和「Show Orquesta『Fire Fever!』」的海报。

[
![宝塚歌劇雪組「ミュージカル『CITY HUNTER』－盗まれたXYZ－」、「ショー オルケスタ『Fire Fever!』」宝塚大劇場公演ポスター。](https://ogre.natalie.mu/media/news/comic/2021/0508/cityhunter_takarazuka.jpg?imwidth=468&imdensity=1)
宝塚歌劇雪組「ミュージカル『CITY HUNTER』－盗まれたXYZ－」、「ショー オルケスタ『Fire Fever!』」宝塚大劇場公演ポスター。
大きなサイズで見る（全5件）
宝冢歌剧雪组「音乐剧『CITY HUNTER』—被盗的XYZ—」「Show Orquesta『Fire Fever!』」宝冢大剧场公演海报。查看大图(共5件)
](https://natalie.mu/comic/gallery/news/427424/1588409)

8月7日から9月13日まで兵庫・宝塚大劇場、10月2日から11月14日まで東京・東京宝塚劇場にて上演される同公演。「ミュージカル『CITY HUNTER』 ―盗まれたXYZ―」では、冴羽リョウ役を[彩風咲奈](https://natalie.mu/comic/artist/93421)、槇村香役を[朝月希和](https://natalie.mu/comic/artist/100741)、ミック・エンジェル役を[朝美絢](https://natalie.mu/comic/artist/93410)、槇村秀幸役を綾凰華、海坊主役を縣千が演じる。チケットの前売り券は兵庫公演が7月17日、東京公演が9月5日より一般販売開始。  
8月7日至9月13日在兵库·宝冢大剧场上演，10月2日至11月14日在东京·宝冢剧场上演。「音乐剧『CITY HUNTER』—被盗的XYZ—」中，冴羽獠由彩风咲奈饰演，槙村香由朝月希和饰演，Mick Angel由朝美绚饰演，槙村秀幸由绫凰华饰演，海坊主由县千饰演。兵库公演将于7月17日开始售票，东京公演将于9月5日开始售票。  

「シティーハンター」は裏社会の始末屋（スイーパー）として生きる“シティーハンター”こと冴羽リョウが主人公のハードボイルドコメディ。1985年より週刊少年ジャンプ（集英社）で連載され、1987年にはTVアニメの放送がスタートした。また2019年2月には「劇場版シティーハンター <新宿プライベート・アイズ>」が公開。同年、フランスでは実写映画「シティーハンター THE MOVIE 史上最香のミッション」も上映された。  
《城市猎人》是一部硬汉喜剧，主人公是作为黑社会的始末屋(Sweeper)而生活的“城市猎人”冴羽獠。1985年在《周刊少年jump》(集英社)上连载，1987年开始播放TV动画。2019年2月，《剧场版城市猎人<新宿Private Eyes>》将上映。同年，真人电影《城市猎人THE MOVIE史上最香的任务》也在法国上映。

この記事の画像（全5件）

[![宝塚歌劇雪組「ミュージカル『CITY HUNTER』－盗まれたXYZ－」、「ショー オルケスタ『Fire Fever!』」宝塚大劇場公演ポスター。](https://ogre.natalie.mu/media/news/comic/2021/0508/cityhunter_takarazuka.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/427424/1588409 "宝塚歌劇雪組「ミュージカル『CITY HUNTER』－盗まれたXYZ－」、「ショー オルケスタ『Fire Fever!』」宝塚大劇場公演ポスター。")
[![「シティーハンター」イラスト](https://ogre.natalie.mu/media/news/comic/2020/1222/cityhunter_illust.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/427424/1506836 "「シティーハンター」イラスト")
[![「シティーハンター」1巻](https://ogre.natalie.mu/media/news/comic/2020/1222/ZS_cityhunter01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/427424/1506846 "「シティーハンター」1巻")
[![「シティーハンター」2巻](https://ogre.natalie.mu/media/news/comic/2020/1222/ZS_cityhunter02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/427424/1506848 "「シティーハンター」2巻")
[![「シティーハンター」3巻](https://ogre.natalie.mu/media/news/comic/2020/1222/ZS_cityhunter03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/427424/1506843 "「シティーハンター」3巻")

## 宝塚歌劇雪組「ミュージカル『CITY HUNTER』 －盗まれたXYZ－」「ショー オルケスタ『Fire Fever!』」

日程：2021年8月7日（土）～ 9月13日（月）  
場所：兵庫県 宝塚大劇場  
  
日程：2021年10月2日（土）～11月14日（日）  
場所：東京都 東京宝塚劇場

## 「ミュージカル『CITY HUNTER』 －盗まれたXYZ－」

### スタッフ

原作：[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」  
脚本・演出：[齋藤吉正](https://natalie.mu/comic/artist/105109)

### キャスト

冴羽リョウ：[彩風咲奈](https://natalie.mu/comic/artist/93421)  
槇村香：[朝月希和](https://natalie.mu/comic/artist/100741)  
ミック・エンジェル：[朝美絢](https://natalie.mu/comic/artist/93410)  
槇村秀幸：綾凰華  
海坊主：縣千

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記。


