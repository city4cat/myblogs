https://natalie.mu/comic/news/74882


# 【8月20日付】本日発売の単行本リスト

2012年8月20日  [Comment](https://natalie.mu/comic/news/74882/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


コミックナタリーより、本日8月20日に発売される単行本をお知らせいたします。  
ComicNatalie于8月20日发售单行本。  

## 秋田書店

[「開花のススメ（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4253235166/nataliecomic-22/ref=nosim) 苺野しずく/[山口貴由](https://natalie.mu/comic/artist/3705)  
[「この○○がエロい!!」](http://www.amazon.co.jp/exec/obidos/ASIN/4253151116/nataliecomic-22/ref=nosim) [金平守人](https://natalie.mu/comic/artist/3399)  
[「凍牌（3）～人柱篇～」](http://www.amazon.co.jp/exec/obidos/ASIN/4253149456/nataliecomic-22/ref=nosim) [志名坂高次](https://natalie.mu/comic/artist/6017)  
[「CROSSandCRIME（8）」](http://www.amazon.co.jp/exec/obidos/ASIN/4253150349/nataliecomic-22/ref=nosim) [葉月京](https://natalie.mu/comic/artist/2444)  
[「死人の声を聞くがよい」](http://www.amazon.co.jp/exec/obidos/ASIN/4253232485/nataliecomic-22/ref=nosim) ひよどり祥子  
[「ゴッドサイダーサーガ（3）神魔三国志」](http://www.amazon.co.jp/exec/obidos/ASIN/4253255981/nataliecomic-22/ref=nosim) [巻来功士](https://natalie.mu/comic/artist/3279)  
[「エクゾスカル零（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4253234933/nataliecomic-22/ref=nosim) 山口貴由  
[「デメキン（6）」](http://www.amazon.co.jp/exec/obidos/ASIN/4253149677/nataliecomic-22/ref=nosim) [ゆうはじめ](https://natalie.mu/comic/artist/5899)/佐田正樹

## 少年画報社

[「アイドルを守るだけの簡単なおしごと。（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4785939036/nataliecomic-22/ref=nosim) 甘夏  
[「ツマヌダ格闘街（12）」](http://www.amazon.co.jp/exec/obidos/ASIN/4785939028/nataliecomic-22/ref=nosim) [上山道郎](https://natalie.mu/comic/artist/3958)  

## 竹書房

[「牌王伝説 ライオン（3）」](http://www.amazon.co.jp/exec/obidos/ASIN/4812479657/nataliecomic-22/ref=nosim) 志名坂高次  

## 徳間書店

[「義風堂々!!直江兼続 ～前田慶次月語り（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801049/nataliecomic-22/ref=nosim) [原哲夫](https://natalie.mu/comic/artist/1917)/[堀江信彦](https://natalie.mu/comic/artist/5280)/[武村勇治](https://natalie.mu/comic/artist/5279)  
[「義風堂々!!直江兼続 ～前田慶次月語り（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801057/nataliecomic-22/ref=nosim) 原哲夫/堀江信彦/武村勇治  
[「義風堂々!!直江兼続 ～前田慶次酒語り（5）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801014/nataliecomic-22/ref=nosim) 原哲夫/堀江信彦/武村勇治  
[「エンジェル・ハート 1stシーズン（11）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801022/nataliecomic-22/ref=nosim) [北条司](https://natalie.mu/comic/artist/2405)  
[「エンジェル・ハート 1stシーズン（12）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801030/nataliecomic-22/ref=nosim) 北条司  
[「エンジェル・ハート 2ndシーズン（4）」](http://www.amazon.co.jp/exec/obidos/ASIN/4199801006/nataliecomic-22/ref=nosim) 北条司  

## 日本文芸社

[「カタログ☆おねいさん」](http://www.amazon.co.jp/exec/obidos/ASIN/4537129298/nataliecomic-22/ref=nosim) 葉月京  
[「SとM（26）」](http://www.amazon.co.jp/exec/obidos/ASIN/4537129239/nataliecomic-22/ref=nosim) [村生ミオ](https://natalie.mu/comic/artist/4279)  

## 白泉社

[「執事様のお気に入り（13）」](http://www.amazon.co.jp/exec/obidos/ASIN/4592192753/nataliecomic-22/ref=nosim) [伊沢玲](https://natalie.mu/comic/artist/6016)/津山冬  
[「俺様ティーチャー（14）」](http://www.amazon.co.jp/exec/obidos/ASIN/4592192699/nataliecomic-22/ref=nosim) [椿いづみ](https://natalie.mu/comic/artist/4452)  
[「JIUJIU -獣従-（5）」](http://www.amazon.co.jp/exec/obidos/ASIN/4592190254/nataliecomic-22/ref=nosim) トビナトウヤ  
[「ニブンノワン！王子（5）」](http://www.amazon.co.jp/exec/obidos/ASIN/4592190459/nataliecomic-22/ref=nosim) 中村世子  
[「【急募】 村長さん（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4592190564/nataliecomic-22/ref=nosim) [藤原規代](https://natalie.mu/comic/artist/2312)  
[「咎人とメロディ」](http://www.amazon.co.jp/exec/obidos/ASIN/4592192885/nataliecomic-22/ref=nosim) [モリエサトシ](https://natalie.mu/comic/artist/5918)  
[「月の輝く夜に」](http://www.amazon.co.jp/exec/obidos/ASIN/4592192907/nataliecomic-22/ref=nosim) [山内直実](https://natalie.mu/comic/artist/2018)/氷室冴子  

## 双葉社

[「紅蓮のアルマ（1）」](http://www.amazon.co.jp/exec/obidos/ASIN/4575841234/nataliecomic-22/ref=nosim) こも  
[「カロン サイフォン（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4575841226/nataliecomic-22/ref=nosim) 橘りた/榊ヘヌウ  
[「嘘つき惑星（2）」](http://www.amazon.co.jp/exec/obidos/ASIN/4575841218/nataliecomic-22/ref=nosim) ヨシザワ

