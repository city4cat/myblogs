https://natalie.mu/comic/news/316633

# 殺した仲間と死後の世界で再会、仲直りはできるのか「殺人プルガトリウム」1巻

2019年1月19日  [Comment](https://natalie.mu/comic/news/316633/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[小手川ゆあ](https://natalie.mu/comic/artist/2064)「殺人プルガトリウム」1巻が、本日1月19日に竹書房より発売された。
小手川由雅的《杀人煉獄》第1卷于1月19日由竹书房发售。

[
![「殺人プルガトリウム」1巻](https://ogre.natalie.mu/media/news/comic/2019/0119/satsujin_P_01.jpg?imwidth=468&imdensity=1)
「殺人プルガトリウム」1巻
大きなサイズで見る（全9件）
](https://natalie.mu/comic/gallery/news/316633/1092323)

「殺人プルガトリウム」は、養父と仲間を殺したことを後悔しながら息を引き取った元殺し屋の男性・高杉玄（ハル）が、死後の世界で目覚めたことから始まるサスペンス作品。玄は死後の世界に先に来ていた養父と再会し、組織を裏切り彼を殺害したことを許してもらう。同じように死んだ仲間たちもこの世界に来ているかもしれないと考えた玄は、かつて自分が壊してしまった家族の絆を取り戻すことを決意する。  
《杀人煉獄》讲述了原杀手高杉玄(小春饰)因杀死养父和同伴而后悔不已，并在死后的世界中醒来，由此展开的悬疑作品。玄与先到死后世界的养父重逢，得到了背叛组织杀害他的原谅。考虑到同样死去的伙伴们也可能会来到这个世界的玄，决定要找回曾经被自己破坏的家人之间的羁绊。

購入特典は喜久屋書店用、メロンブックス用、そのほかの書店用が存在。なくなり次第配布は終了となるため、詳細は購入予定の各店にて確認を。  
购买特典有喜久屋书店用、哈密瓜书店用、其他书店用。因为一旦用完就结束分发，详细请在预定购买的各店确认。


「殺人プルガトリウム」は、コアミックスのWebマンガサイト・コミックタタンで連載中。単行本レーベル「バンブーコミックス タタン」の第1弾タイトルとなっており、同レーベルから[北条司](https://natalie.mu/comic/artist/2405)キャラクター原案による[えすとえむ](https://natalie.mu/comic/artist/2622)「CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常」1巻、[モリコロス](https://natalie.mu/comic/artist/9941)「咲けよ花咲け！」1巻、おかざきさと乃「異邦のオズワルド」1巻も本日発売された。  
《杀人煉獄》在Coamix的Web漫画网站comic tatan连载中。这是单行本“Bamboo Comic tatan”的第1弹标题，也是北条司角色原案的《CITY HUNTER外传 伊集院隼人氏的平静度过的日常》1卷，森克萝丝《绽放吧绽放吧!第1卷，冈崎里乃的《异邦的奥斯瓦尔德》第1卷也在今天发售。

コミックナタリーでは、コミックタタンのレーベル第1弾タイトル発売を記念した特集を実施。「CITY HUNTER外伝 伊集院隼人氏の平穏ならぬ日常」を執筆するえすとえむのインタビューや、佐原ミズ、あみだむく、モリコロス、三堂マツリ、三月えみ、仁茂田あい、田村茜、山崎智史が海坊主を描くトリビュートイラストギャラリーを展開している。  
另外，comic natalie还实施了纪念comic tatan标签第1弹标题发售的特集。《CITY HUNTER外传 伊集院隼人氏的平静度过的日常》的作者えすとえむ的采访，以及佐原水、amedame、森克罗、三堂祭、三月惠美、仁茂田爱、田村茜、山崎智史的画的海坊主。

この記事の画像（全9件）

[![「殺人プルガトリウム」1巻](https://ogre.natalie.mu/media/news/comic/2019/0119/satsujin_P_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092323 "「殺人プルガトリウム」1巻")
[![「殺人プルガトリウム」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/003-1m9l.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092324 "「殺人プルガトリウム」1巻より。")
[![「殺人プルガトリウム」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/004-9xhq.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092325 "「殺人プルガトリウム」1巻より。")
[![「殺人プルガトリウム」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/005-c8il.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092326 "「殺人プルガトリウム」1巻より。")
[![「殺人プルガトリウム」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/006-9tlr.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092327 "「殺人プルガトリウム」1巻より。")
[![「殺人プルガトリウム」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/007-eu1h.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092328 "「殺人プルガトリウム」1巻より。")
[![「殺人プルガトリウム」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/008-ezx1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092329 "「殺人プルガトリウム」1巻より。")
[![「殺人プルガトリウム」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/009-7fh8.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092330 "「殺人プルガトリウム」1巻より。")
[![「殺人プルガトリウム」1巻より。](https://ogre.natalie.mu/media/news/comic/2019/0119/010-89u9.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/316633/1092331 "「殺人プルガトリウム」1巻より。")

LINK

[殺人プルガトリウム｜コミックタタン](https://www.tatan.jp/lib/top.php?id=130)