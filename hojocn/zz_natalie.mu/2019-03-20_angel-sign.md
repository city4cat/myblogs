https://natalie.mu/comic/news/324454

# 「CityHunter」の北条司が実写映画を初総監督「AngelSign」製作開始

2019年3月20日  [Comment](https://natalie.mu/comic/news/324454/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

「CityHunter」の[北条司](https://natalie.mu/comic/artist/2405)が総監督を務める実写映画「AngelSign」の製作が発表された。
《城市猎人》的北条司担任总导演的真人电影《AngelSign》的制作被发表了。

[
![「AngelSign」](https://ogre.natalie.mu/media/news/comic/2019/0319/angelsign-logo.jpg?impolicy=hq&imwidth=730&imdensity=1)
「AngelSign」  
大きなサイズで見る（全2件）  
大尺寸观看(共2件)  
](https://natalie.mu/comic/gallery/news/324454/1127746)

[
![北条司](https://ogre.natalie.mu/media/news/comic/2019/0319/hojotsukasa.jpg?imwidth=468&imdensity=1)
北条司［拡大］
](https://natalie.mu/comic/gallery/news/324454/1127745)

「AngelSign」は、ノース・スターズ・ピクチャーズが開催している「SILENT MANGA AUDITION」へ寄せられたストーリー数編に、北条自身によるオリジナルストーリーを加えた“愛のものがたり”。月刊コミックゼノン（徳間書店）を編集するコアミックスの代表・[堀江信彦](https://natalie.mu/comic/artist/5280)が企画を立ち上げ、その思いに共感した北条が総監督がを務めることになった。  
「AngelSign」是North Star Pictures举办的“默白漫画大赛”收到的故事数篇，加上北条自己的原创故事的“爱的故事”。月刊Comic Zenon(德间书店)的编辑Coamix的代表·堀江信彦发起了企划，对其想法产生共鸣的北条担任了总监督。


「SILENT MANGA AUDITION」はこれまでに14回開催され、108の国と地域から3580名、6249編の作品が寄せられている大規模なマンガオーディション。北条は第1回から審査員を務めている。  
“默白漫画大赛”迄今为止已经举办了14届，来自108个国家和地区的3580名选手，收到了6249篇作品，是一场大规模的漫画选拔赛。北条从第一届开始担任审查员。  

この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![「AngelSign」](https://ogre.natalie.mu/media/news/comic/2019/0319/angelsign-logo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/324454/1127746 "「AngelSign」")
[![北条司](https://ogre.natalie.mu/media/news/comic/2019/0319/hojotsukasa.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/324454/1127745 "北条司")

## 北条司Comment

マンガを描くのとは違い、映画制作はとても新鮮です。  
与画漫画不同，电影制作非常新鲜。  

気を引き締め、粛粛と臨んでいきたいと思っています。  
我想振作，严肃地面对。  



LINK

[映画『AngelSign』公式Site](https://angelsign.jp)  
[「AngelSign」公式Site | Facebook](https://www.facebook.com/AngelSignMovie/)  
[AngelSign (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[北条司 OFFICIAL WEB SITE](http://www.hojo-tsukasa.com/)  
[SILENT MANGA AUDITION(R) - The home of world's biggest Manga Award, SILENT MANGA AUDITION Community(SMAC!) Official Website.](http://www.manga-audition.com/)  
[株式会社North Stars Pictures | North Stars Pictures](http://www.nsp.tv/)  