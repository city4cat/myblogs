https://natalie.mu/comic/news/173249

# 「ドラゴンボール」30年超える歴史が1冊に！尾田、岸本、冨樫らの寄稿も  
《龙珠》超过30年的历史汇集于一册!尾田、岸本、富坚等人的投稿  

2016年1月21日  [Comment](https://natalie.mu/comic/news/173249/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[鳥山明](https://natalie.mu/comic/artist/2266)「ドラゴンボール」の30年を超える歴史を、貴重な資料とともに追う「30th ANNIVERSARY ドラゴンボール 超史集-SUPER HISTORY BOOK-」が、本日1月21日に発売された。  
鸟山明“龙珠”超过30年的历史，与珍贵的资料一起追寻的《30th ANNIVERSARY 龙珠 超史集-SUPER HISTORY BOOK-》于1月21日正式发售。

[
![「30th ANNIVERSARY ドラゴンボール 超史集-SUPER HISTORY BOOK-」](https://ogre.natalie.mu/media/news/comic/2016/0121/dragonball_30th.jpg?imwidth=468&imdensity=1)
「30th ANNIVERSARY ドラゴンボール 超史集-SUPER HISTORY BOOK-」
大きなサイズで見る  
《30th ANNIVERSARY 龙珠 超史集 -SUPER HISTORY BOOK-》  
大尺寸看
](https://natalie.mu/comic/gallery/news/173249/464523)

同書にはキャラクターの設定画や、鳥山による描き下ろしマンガ、アニメ「ドラゴンボール超」や映画に登場するキャラクターの設定画、鳥山や声優の[野沢雅子](https://natalie.mu/comic/artist/10256)らへのインタビューを掲載しているほか、「ドラゴンボール」がメインを飾った週刊少年ジャンプ（集英社）の表紙を鳥山のコメントともに紹介。これまでに発売されてきた同作のゲームや、カードダス、グッズの歴史も振り返っている。  
这本书中除了有角色设定画、鸟山所画的漫画、动画《龙珠超》和电影中登场的角色设定画、对鸟山和声优野泽雅子等人的采访之外，还有《龙珠》为主要内容的《周刊少年Jump》(集英社)的封面，并介绍了鸟山的评论。回顾了至今为止发售的同作的游戏、卡片、周边的历史。  

このほか「ドラゴンボール」最終話のネームもすべて収録。また[尾田栄一郎](https://natalie.mu/comic/artist/2368)、[岸本斉史](https://natalie.mu/comic/artist/3293)、[冨樫義博](https://natalie.mu/comic/artist/2377)、[荒木飛呂彦](https://natalie.mu/comic/artist/1940)、[井上雄彦](https://natalie.mu/comic/artist/1791)ら総勢30名を超える作家による「ドラゴンボール」の寄稿イラストも収められた豪華な1冊だ。  
此外《龙珠》最终话的name也全部收录。另外还收录了尾田荣一郎、岸本齐史、富坚义博、荒木飞吕彦、井上雄彦等总计超过30名作家的《龙珠》投稿插图，堪称豪华的一本。  

## 寄稿作家一覧

[秋本治](https://natalie.mu/comic/artist/2053)、[浅田弘幸](https://natalie.mu/comic/artist/2188)、[麻生周一](https://natalie.mu/comic/artist/4835)、荒木飛呂彦、[池沢春人](https://natalie.mu/comic/artist/9600)、井上雄彦、[うすた京介](https://natalie.mu/comic/artist/1646)、オオイシナホ、尾田栄一郎、[小畑健](https://natalie.mu/comic/artist/2075)、[KAITO](https://natalie.mu/comic/artist/2489)、[川田](https://natalie.mu/comic/artist/11030)、[桂正和](https://natalie.mu/comic/artist/1908)、岸本斉史、[久保帯人](https://natalie.mu/comic/artist/3347)、[古味直志](https://natalie.mu/comic/artist/3469)、[佐伯俊](https://natalie.mu/comic/artist/10235)、[島袋光年](https://natalie.mu/comic/artist/2299)、[空知英秋](https://natalie.mu/comic/artist/1903)、[高橋和希](https://natalie.mu/comic/artist/1955)、[田畠裕基](https://natalie.mu/comic/artist/10218)、[附田祐斗](https://natalie.mu/comic/artist/8676)、冨樫義博、とよたろう、[仲間りょう](https://natalie.mu/comic/artist/10747)、[沼駿](https://natalie.mu/comic/artist/92423)、[原哲夫](https://natalie.mu/comic/artist/1917)、[古舘春一](https://natalie.mu/comic/artist/8561)、[北条司](https://natalie.mu/comic/artist/2405)、[堀越耕平](https://natalie.mu/comic/artist/8764)、[松井優征](https://natalie.mu/comic/artist/2081)、[村田雄介](https://natalie.mu/comic/artist/2209)、[森田まさのり](https://natalie.mu/comic/artist/2117)、[矢吹健太朗](https://natalie.mu/comic/artist/2434)、[ゆでたまご](https://natalie.mu/comic/artist/1756)（嶋田隆司、中井義則）、[横田卓馬](https://natalie.mu/comic/artist/9674)、[和月伸宏](https://natalie.mu/comic/artist/4971)

(c)Bird Studio／集英社 (c)Bird Studio／集英社・富士TV・東映Animation (c)Bird Studio／集英社 (c)「２０１３ DRAGONBALLＺ」製作委員会 (c)Bird Studio／集英社 (c)「２０１５ DRAGONBALLＺ」製作委員会 (c)BANDAI (c)BANDAI NAMCO Entertainment Inc.

LINK

[30th Anniversary DragonBall超史集─SUPER HISTORY BOOK─｜VJump WEB](http://vjump.shueisha.co.jp/vjbooks/30thdb/)

関連商品

[
![「30th ANNIVERSARY DragonBall 超史集-SUPER HISTORY BOOK-」](https://images-fe.ssl-images-amazon.com/images/I/51z9nYMNm6L._SS70_.jpg)
「30th ANNIVERSARY DragonBall 超史集-SUPER HISTORY BOOK-」
[書籍] 2016年1月21日発売 / 集英社 / 978-4087925050
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/4087925056/nataliecomic-22)