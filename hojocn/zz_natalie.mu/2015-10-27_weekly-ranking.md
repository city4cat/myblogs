https://natalie.mu/comic/news/164130

# 【10月19日～10月25日】週間単行本売り上げランキング

2015年10月27日 [Comment](https://natalie.mu/comic/news/164130/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


コミックナタリーより、10月19日から10月25日までの週間単行本売り上げランキングをお知らせいたします。  
下面是Comic Natalie从10月19日到10月25日的单行本周销量排行榜。  

## [TSUTAYA](http://www.tsutaya.co.jp/rank/book_sell.html?r=W082&other=rank_natalie)調べ

**1位**[「キングダム（40）」](http://store-tsutaya.tsite.jp/item/sell_book/9784088902777.html) [原泰久](https://natalie.mu/comic/artist/1916)（集英社）  
**2位**[「七つの大罪（17）」](http://store-tsutaya.tsite.jp/item/sell_book/9784063955194.html) [鈴木央](https://natalie.mu/comic/artist/4956)（講談社）  
**3位**[「マギ（27）」](http://store-tsutaya.tsite.jp/item/sell_book/9784091264695.html) [大高忍](https://natalie.mu/comic/artist/2217)（小学館）  
**4位**[「干物妹！うまるちゃん（7）」](http://store-tsutaya.tsite.jp/item/sell_book/9784088902685.html) [サンカクヘッド](https://natalie.mu/comic/artist/5035)（集英社）  
**5位**[「ONE PIECE（79）」](http://store-tsutaya.tsite.jp/item/sell_book/9784088804965.html) [尾田栄一郎](https://natalie.mu/comic/artist/2368)（集英社）  
**6位**[「俺物語!!（10）」](http://store-tsutaya.tsite.jp/item/sell_book/9784088454658.html) [アルコ](https://natalie.mu/comic/artist/1635)/[河原和音](https://natalie.mu/comic/artist/1832)（集英社）  
**7位**[「Angel Heart 2ndSeason（12）」](http://store-tsutaya.tsite.jp/item/sell_book/9784199803000.html) [北条司](https://natalie.mu/comic/artist/2405)（徳間書店）  
**8位**[「デストロ246（6）」](http://store-tsutaya.tsite.jp/item/sell_book/9784091574282.html) [高橋慶太郎](https://natalie.mu/comic/artist/3524)（小学館）  
**9位**[「思い、思われ、ふり、ふられ（1）」](http://store-tsutaya.tsite.jp/item/sell_book/9784088454672.html) [咲坂伊緒](https://natalie.mu/comic/artist/3650)（集英社）  
**10位**[「マギ シンドバッドの冒険（8）」](http://store-tsutaya.tsite.jp/item/sell_book/9784091265906.html) [大寺義史](https://natalie.mu/comic/artist/10459)/大高忍（小学館）  

## [COMIC ZIN](http://www.comiczin.jp/)調べ

**1位**[「デストロ246（6）」](http://shop.comiczin.jp/products/detail.php?product_id=10008003) 高橋慶太郎（小学館）  
**2位**[「干物妹！うまるちゃん（7）」](http://shop.comiczin.jp/products/detail.php?product_id=10008006) サンカクヘッド（集英社）  
**3位**[「ひまわりさん（6）」](http://shop.comiczin.jp/products/detail.php?product_id=10008047) 菅野マナミ（KADOKAWA）  
**4位**[「だがしかし（3）」](http://shop.comiczin.jp/products/detail.php?product_id=10007993) [コトヤマ](https://natalie.mu/comic/artist/11075)（小学館）  
**5位**[「孤独のグルメ（2）」](http://shop.comiczin.jp/products/detail.php?product_id=10007879) [谷口ジロー](https://natalie.mu/comic/artist/2232)/[久住昌之](https://natalie.mu/comic/artist/3343)（扶桑社）  
**6位**[「はぐれアイドル地獄変（2）」](http://shop.comiczin.jp/products/detail.php?product_id=10008005) [高遠るい](https://natalie.mu/comic/artist/1942)（日本文芸社）  
**7位**[「くまみこ（5）」](http://shop.comiczin.jp/products/detail.php?product_id=10008045) [吉元ますめ](https://natalie.mu/comic/artist/7688)（KADOKAWA）  
**8位**[「マナビヤゴラク（1）」](http://shop.comiczin.jp/products/detail.php?product_id=10008050) ぼにげん（KADOKAWA）  
**9位**[「はやて×ブレード2（3）」](http://shop.comiczin.jp/products/detail.php?product_id=10008009) [林家志弦](https://natalie.mu/comic/artist/2454)（集英社）  
**10位**[「まおゆう魔王勇者 『この我のものとなれ、勇者よ』『断る！』（16）」](http://shop.comiczin.jp/products/detail.php?product_id=10008034) [石田あきら](https://natalie.mu/comic/artist/4182)/橙乃ままれ/水玉螢之丞/[toi8](https://natalie.mu/comic/artist/9125)（KADOKAWA）

