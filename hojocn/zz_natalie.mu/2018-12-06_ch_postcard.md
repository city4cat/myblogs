https://natalie.mu/comic/news/311075

# 劇場版「シティーハンター」ムビチケ特典に北条司イラストのポストカードセット

2018年12月6日 [Comment](https://natalie.mu/comic/news/311075/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)



[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「劇場版シティーハンター（仮題）」の第2弾ムビチケカードが、12月14日より全国劇場にて販売される。    
根据北条司原作改编的动画电影《剧场版城市猎人(暂定名)》的第2弹Mvticket card将于12月14日起在日本全国剧场开始销售。

[
![「劇場版シティーハンター（仮題）」ビジュアル](https://ogre.natalie.mu/media/news/comic/2018/0801/CH2019_visual.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター（仮題）」ビジュアル  
大きなサイズで見る（全4件）  
“剧场版城市猎人(暂定名)”视觉  
大尺寸看(全4件)  
](https://natalie.mu/comic/gallery/news/311075/979068)

[
![「劇場版シティーハンター（仮題）」第2弾ムビチケカード特典のポストカード。](https://ogre.natalie.mu/media/news/comic/2018/1206/cityhunter_postcard.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター（仮題）」第2弾ムビチケカード特典のポストカード。［拡大］  
“剧场版城市猎人(暂定名)”第2弹Mvticket card特典的明信片。[扩大]  
](https://natalie.mu/comic/gallery/news/311075/1065258)

劇場版のビジュアルがデザインされた第2弾ムビチケカードには、特典として原作者・北条司のイラストを使用したポストカード4種セットが付属。ポストカードのイラストは連載当時に描かれたものの中から、映画の見どころと合ったイラストがセレクトされている。  
以剧场版的图像设计而成的第2弹Mvticket card，作为特典附赠了使用原作者北条司插画的4种明信片套装。明信片上的插图是从连载时所画的插图中挑选出的符合电影看点的插图。

[
![「劇場版シティーハンター（仮題）」アルミカードケース](https://ogre.natalie.mu/media/news/comic/2018/1206/cityhunter_cardcase.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター（仮題）」アルミカードケース［拡大］  
《剧场版城市猎人(暂定名)》铝制卡包[扩大]  
](https://natalie.mu/comic/gallery/news/311075/1065259)

また12月22日・23日に幕張メッセで開催される「ジャンプフェスタ2019」では、会場限定でB2ポスター付きの第2弾ムビチケカードを販売。ANIPLEX＋では第2弾ムビチケカードとアルミカードケースのセットが数量限定で用意され、本日より予約を受け付けている。  
另外，12月22日、23日在幕张展览馆举办的“Jump Festa 2019”上，也将发售附赠B2海报的第2弹Mvticket card。ANIPLEX +第2弹限量发售了Mvticket card和铝卡盒，从今天开始接受预约。

なおローソン・ミニストップ店頭Loppi端末のみで購入できる「グッズ引換券付コンビニムビチケ券」の発売も予定。詳細は後日発表される。「劇場版シティーハンター（仮題）」は2019年2月8日公開。  
另外，仅在Lawson·ministop店头Loppi终端能购买的“附带商品兑换券的便利店兑换券”也将发售。详细内容将在日后发表。《剧场版城市猎人(暂定名)》将于2019年2月8日上映。

※記事初出時、「ジャンプフェスタ2019」の開催日に誤りがありました。お詫びして訂正いたします。  
※报道初出时，“Jump Festa 2019”的举办日期有错误。向您道歉并更正。

この記事の画像・動画（全4件）  
这篇报道的图片、视频(共4篇)  

[![「劇場版シティーハンター（仮題）」ビジュアル](https://ogre.natalie.mu/media/news/comic/2018/0801/CH2019_visual.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/311075/979068 "「劇場版シティーハンター（仮題）」ビジュアル")
[![「劇場版シティーハンター（仮題）」第2弾ムビチケカード特典のポストカード。](https://ogre.natalie.mu/media/news/comic/2018/1206/cityhunter_postcard.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/311075/1065258 "「劇場版シティーハンター（仮題）」第2弾ムビチケカード特典のポストカード。")
[![「劇場版シティーハンター（仮題）」アルミカードケース](https://ogre.natalie.mu/media/news/comic/2018/1206/cityhunter_cardcase.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/311075/1065259 "「劇場版シティーハンター（仮題）」アルミカードケース")
[![「劇場版シティーハンター（仮題）」特報](https://i.ytimg.com/vi/4TnImjHwb7U/default.jpg)](https://natalie.mu/comic/gallery/news/311075/media/31388 "「劇場版シティーハンター（仮題）」特報")

## 映画「劇場版シティーハンター（仮題）」
电影《剧场版城市猎人(暂定名)》  

2019年2月8日（金）公開

### キャスト

冴羽リョウ：[神谷明](https://natalie.mu/comic/artist/60997)  
槇村香：[伊倉一恵](https://natalie.mu/comic/artist/95562)  
野上冴子：一龍斎春水  
海坊主：玄田哲章  
美樹：小山茉美

### スタッフ

原作：[北条司](https://natalie.mu/comic/artist/2405)  
総監督：こだま兼嗣  
脚本：加藤陽一  
チーフ演出：佐藤照雄、京極尚彦  
キャラクターデザイン：高橋久美子  
総作画監督：菱沼義仁  
美術監督：加藤浩  
色彩設計：久保木裕一  
音響監督：長崎行男  
音響制作：AUDIO PLANNING U  
音楽：岩崎琢  
編集：今井大介（JAYFILM）  
アニメーション制作：サンライズ  
配給：アニプレックス

※リョウの漢字はけものへんに「僚」のつくりが正式表記。  
※岩崎琢の琢は旧字体が正式表記。



(c)北条司/NSP・「2019 劇場版シティーハンター」製作委員会

LINK

[Anime「劇場版シティーハンター」公式サイト](http://cityhunter-movie.com/)  
[「劇場版シティーハンター」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)  