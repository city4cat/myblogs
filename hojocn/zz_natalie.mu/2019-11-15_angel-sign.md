https://natalie.mu/eiga/news/355594


# お肉×「エンジェルサイン」、松下奈緒やディーンら出演作がふるさと納税の特典に

2019年11月15日 18:00 106 [3](https://natalie.mu/eiga/news/355594/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)


マンガ家・[北条司](https://natalie.mu/eiga/artist/2405)が総監督を務めるオムニバス作品「[エンジェルサイン](https://natalie.mu/eiga/film/182396)」が、熊本・高森町のふるさと納税の返礼品にあわせて配信されることがわかった。  
由漫画家北条司担任总导演的集锦作品《Angel Sign》，将配合熊本·高森町的故乡纳税的回礼产品发布。。

[
![「エンジェルサイン」ポスタービジュアルと（左）と肉（右）。](https://ogre.natalie.mu/media/news/eiga/2019/1115/angelsign_201911_01.jpg?impolicy=hq&imwidth=730&imdensity=1)
「エンジェルサイン」ポスタービジュアルと（左）と肉（右）。  
大きなサイズで見る（全16件）  
“天使签名”海报视觉图(左)和肉(右)。  
查看大图(共16件)  
](https://natalie.mu/eiga/gallery/news/355594/1277550)

[
![「エンジェルサイン」より、「別れと始まり」のワンシーン。](https://ogre.natalie.mu/media/news/eiga/2019/1115/angelsign_201911_02.jpg?imwidth=468&imdensity=1)
「エンジェルサイン」より、「別れと始まり」のワンシーン。［拡大］  
《天使签名》中的“离别与开始”的一个场景。[扩大]  
](https://natalie.mu/eiga/gallery/news/355594/1277551)

この取り組みは、同地の高森駅が「エンジェルサイン」の一編「別れと始まり」が撮影された熊本・阿蘇を走る南阿蘇鉄道の終着駅であることにちなみ実施されるもの。楽天ふるさと納税、ANAふるさと納税、ふるさとチョイスなどのサイトで高森町の名産・馬刺しなどを返礼品として選択し寄付を行った人に、Rakuten TVで同作を視聴できるシリアルコードが寄付受領証明書とともに送られる。  
这是因为高森站是南阿苏铁路的终点站，该铁路途经熊本县阿苏市，《天使签名》的一篇《离别与开始》就是在这里拍摄的。在乐天故乡税、ANA故乡税、故乡Choice等网站上选择高森町的特产马肉刺身等作为回礼的捐赠人，Rakuten可以在电视上观看该作品的序列码将与捐赠领取证明书一起发送。  

全国の劇場で上映中の「エンジェルサイン」は、世界中から寄せられたサイレントマンガオーディションの受賞作をアジア各国の監督が映像化したもの。[松下奈緒](https://natalie.mu/eiga/artist/11381)、[ディーン・フジオカ](https://natalie.mu/eiga/artist/65834)、[緒形直人](https://natalie.mu/eiga/artist/11951)、[菊池桃子](https://natalie.mu/eiga/artist/34588)らがキャストに名を連ねた。  
在全国的剧场上映中的“天使签名”，是亚洲各国的导演将来自世界各地的默白漫画大赛的获奖作品影像化的作品。松下奈绪、藤冈靛、绪形直人、菊池桃子等演员也榜上有名。  

この記事の画像・動画（全16件）  
这篇报道的图片、视频(共16篇)  

[![「エンジェルサイン」ポスタービジュアルと（左）と肉（右）。](https://ogre.natalie.mu/media/news/eiga/2019/1115/angelsign_201911_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1277550 "「エンジェルサイン」ポスタービジュアルと（左）と肉（右）。  
（“天使签名”海报视觉图(左)和肉(右)。）")
[![「エンジェルサイン」より、「別れと始まり」のワンシーン。](https://ogre.natalie.mu/media/news/eiga/2019/1115/angelsign_201911_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1277551 "「エンジェルサイン」より、「別れと始まり」のワンシーン。")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1189290 "「エンジェルサイン」")
[![「エンジェルサイン」](https://ogre.natalie.mu/media/news/eiga/2019/0628/angelsign_201906_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1189295 "「エンジェルサイン」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222122 "「別れと始まり」")
[![「別れと始まり」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_04.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222123 "「別れと始まり」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_07.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222124 "「父の贈り物」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222125 "「空へ」")
[![「空へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_09.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222126 "「空へ」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222127 "「30分30秒」")
[![「30分30秒」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222128 "「30分30秒」")
[![「父の贈り物」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_08.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222129 "「父の贈り物」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_05.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222130 "「故郷へ」")
[![「故郷へ」](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_06.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222131 "「故郷へ」")
[![「エンジェルサイン」ポスタービジュアル](https://ogre.natalie.mu/media/news/eiga/2019/0820/angelsign_201908_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/355594/1222121 "「エンジェルサイン」ポスタービジュアル")
[![「エンジェルサイン」予告編](https://i.ytimg.com/vi/4gFDdfZBS7A/default.jpg)](https://natalie.mu/eiga/gallery/news/355594/media/42069 "「エンジェルサイン」予告編")

(c)「エンジェルサイン」製作委員会

リンク

[「エンジェルサイン」公式サイト](https://angelsign.jp/)
[AngelSignMovie (@AngelSignMovie) | Twitter](https://twitter.com/AngelSignMovie)  
[「エンジェルサイン」予告編](https://youtu.be/4gFDdfZBS7A)  
[熊本県高森町 | 楽天ふるさと納税](https://www.rakuten.co.jp/f434281-takamori/?l-id=furusato_pc_list_kyushuokinawa_94)  
[熊本県高森町 | ANAのふるさと納税](https://furusato.ana.co.jp/43428/)  
[熊本県高森町 | ふるさとチョイス](https://www.furusato-tax.jp/city/product/43428)  
