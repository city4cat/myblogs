https://natalie.mu/comic/news/148298


# 「シティーハンター」新作アニメの制作決定、昔と同じオリジナル声優陣で

2015年5月25日  [Comment](https://natalie.mu/comic/news/148298/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」の新作アニメが制作されることが決定した。  
北条司的《城市猎人》决定制作新动画。

[
![「シティーハンター」の主人公・冴羽リョウ。(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/0525/ryo-cityhunter.jpg?imwidth=468&imdensity=1)
「シティーハンター」の主人公・冴羽リョウ。(c)北条司/NSP 1985  
大きなサイズで見る  
《城市猎人》的主人公冴羽獠。(c)北条司/NSP 1985  
以大尺寸看  
](https://natalie.mu/comic/gallery/news/148298/362287)

この新作アニメは、新装版「シティーハンター XYZ Edition」全12巻の購入特典として贈呈されるDVDに収録。「リョウのプロポーズ（リョウの漢字はけものへんに「僚」のつくり）」のタイトルで、TVアニメ版と同じオリジナル声優陣が各キャラクターを演じる。  
这部新作动画收录在作为新装版《城市猎人XYZ Edition》全12卷的购买特典赠送的DVD中。以“獠的求婚(獠的汉字是反犬旁的变体“僚”)”为标题，由与TV动画版相同的原创声优阵容出演各角色。

「シティーハンター」30周年を記念した「シティーハンター XYZ Edition」は、7月18日に刊行開始。各巻600ページ超えの大ボリュームで、毎月2巻ずつ発売されていく。描き下ろしイラストや北条のロングインタビューなどオマケも満載。全巻購入特典は2種類が用意され、新作アニメDVDと複製原画から希望の品を選べる仕組みだ。  
为纪念《城市猎人》发行30周年，《城市猎人XYZ Edition》将于7月18日开始发行。每卷超过600页，每月发售2卷。新画的插图和北条的长篇采访等赠品也被满载。全卷购买特典有2种，可以从新作动画DVD和复制原画中选择自己想要的作品。


LINK

[Comic Zenon](http://www.comic-zenon.jp/)  
[北条司 OFFICIAL WEB SITE](http://www.hojo-tsukasa.com/)  
