https://natalie.mu/comic/news/78857

# ゼノン2周年、生原稿プレゼントや「花の慶次」グッズ全サ
Zenon 2周年，原稿Present和“花之庆次”全部Goods

2012年10月25日  [Comment](https://natalie.mu/comic/news/78857/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)



月刊Comic Zenon（徳間書店）が、本日10月25日に発売された12月号にて創刊2周年を迎えた。  
月刊Comic Zenon(德间书店)在今天10月25日发售的12月号上迎来了创刊2周年。

[
![全員Serviceの「花の慶次」ハラマキ。](https://ogre.natalie.mu/media/news/comic/2012/1025/zenon-haramaki.jpg?imwidth=468&imdensity=1)
全員Serviceの「花の慶次」ハラマキ。  
大きなサイズで見る（全4件）  
全员Service的“花之庆次”Haramaki。  
用大号看(共4件)
](https://natalie.mu/comic/gallery/news/78857/144366)

創刊2周年を記念して12月号では、隆慶一郎原作による[原哲夫](https://natalie.mu/comic/artist/1917)「花の慶次 -雲のかなたに-」のグッズを応募者全員Service。慶次愛用の朱槍を模した「朱槍イヤホンジャック」を1000円、名シーンをプリントした「傾奇ハラマキ」を1500円で頒布する。締め切りは11月30日。  
为纪念创刊2周年，在12月号上，应征者将获得隆庆一郎原作的原哲夫《花之庆次-云之远》的周边商品。以庆次爱用的朱枪为原型的“朱枪EarPhone Jack”售价1000日元，印有著名场景的“倾奇Haramaki”售价1500日元。截止日期为11月30日。

また連載作家16名が「2周年に寄せて」をテーマに、台詞なしマンガを執筆。マンガは月刊Comic Zenonの公式サイトにて公開されている。さらに生原稿に直筆サインを入れたものを、各1名にプレゼントする。応募受け付けは11月23日まで。詳細は誌面にて確認してほしい。  
另外，16名连载作家以“2周年寄语”为主题，执笔了没有默白漫画。漫画在月刊Comic Zenon的官方网站上公开。另外，还将赠送在原稿上亲笔签名的作品各1人。报名截止日期为11月23日。详细内容请在杂志上确认。

そのほか12月号では原が原作を務める[吉原基貴](https://natalie.mu/comic/artist/5077)「クロスバトラーズ～CYBERBLUE THE LAST STAND～」、横山了一原作による山田こたろ「漫画専門学校生の青春」の新連載2本がスタートした。  
除此之外，12月号上也开始了由原担任原作的吉原基贵的《CrossButlers  ~ CYBERBLUE THE LAST STAND ~》，以及根据横山了一原作的山田小太郎的《漫画専門学校生の青春》两篇新连载。

この記事の画像（全4件）  
这篇报道的图片(共4篇)  

[![全員Serviceの「花の慶次」Haramaki。](https://ogre.natalie.mu/media/news/comic/2012/1025/zenon-haramaki.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/78857/144366 "全員Serviceの「花の慶次」ハラマキ。（全员服务的“花之庆次”原卷）")
[![全員Serviceの「花の慶次」EarPhone Jack。](https://ogre.natalie.mu/media/news/comic/2012/1025/zenon-jack.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/78857/144367 "全員Serviceの「花の慶次」EarPhone Jack。")
[![新連載「CrossButlers CYBERBLUE THE LAST STAND」Cut](https://ogre.natalie.mu/media/news/comic/2012/1025/zenon-cross.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/78857/144368 "新連載「Cross Butlers CYBERBLUE THE LAST STAND」Cut")
[![月刊Comic Zenon12月号](https://ogre.natalie.mu/media/news/comic/2012/1025/1351144867zenon12-12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/78857/144369 "月刊Comic Zenon12月号")

LINK

[Comic Zenon](http://www.comic-zenon.jp/)  
[Comic Zenon｜創刊2周年記念特設Page](http://www.comic-zenon.jp/2nd/manuscript.html)  

