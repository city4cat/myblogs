https://natalie.mu/comic/news/565459

# コアミックスによるマンガのアートギャラリーがオープン、1回目は北条司展
Coamix Manga Art Gallery Open，第一次是北条司展  

2024年3月18日 12:00

コアミックスによるマンガをテーマにしたアートギャラリー・GALLERY ZENONが、4月17日に東京・吉祥寺にオープン。1回目の展示企画では、[北条司](https://natalie.mu/comic/artist/2405)の軌跡をたどる「北条司展」が4月17日から6月23日にかけて開催される。  
以漫画为主题的画廊GALLERY ZENON将于4月17日在东京吉祥寺开幕。第1次的展示企划中，追溯北条司的轨迹的“北条司展”将于4月17日至6月23日举办。  

[![「GALLERY ZENON オープン記念企画『北条司展』」告知画像](https://ogre.natalie.mu/media/news/comic/2024/0318/hojoten_2024_omote_jp.jpg?imwidth=468&imdensity=1)
「GALLERY ZENON オープン記念企画『北条司展』」告知画像  
大きなサイズで見る（全7件）  
“GALLERY ZENON开幕纪念企划“北条司展”公告图片  
大尺寸查看（共7个）  
](https://natalie.mu/comic/gallery/news/565459/2274265)  

「北条司展」では前期と後期を合わせて150点以上の原画を展示。代表作の「キャッツ♥アイ」「シティーハンター」をはじめ、デビュー作に読み切り、短編作品まで初公開の原画も数々登場する。またフォトスポットやコラボカフェ、グッズショップも展開。展示内容の詳細やチケット情報は追って発表される。  
“北条司展”前期和后期合计展出150件以上的原画。以代表作《猫♥眼》、《城市猎人》为首，从出道作品到短篇作品都有许多首次公开的原画登场。另外还有拍照景点、联名咖啡厅、商品商店等。展览的详细内容和门票信息将在稍后公布。  

この記事の画像（全7件）  
本文图片（共7张）  

-   [](https://natalie.mu/comic/gallery/news/565459/2274265 "「GALLERY ZENON オープン記念企画『北条司展』」告知画像")
-   [](https://natalie.mu/comic/gallery/news/565459/2274260 "「キャッツ♥アイ」の原画。")
-   [](https://natalie.mu/comic/gallery/news/565459/2274261 "「キャッツ♥アイ」の原画。")
-   [](https://natalie.mu/comic/gallery/news/565459/2274259 "「シティーハンター」の原画。")
-   [](https://natalie.mu/comic/gallery/news/565459/2274262 "「シティーハンター」の原画。")
-   [](https://natalie.mu/comic/gallery/news/565459/2274263 "「シティーハンター」の原画。")
-   [](https://natalie.mu/comic/gallery/news/565459/2274264 "GALLERY ZENONロゴ")

## 「GALLERY ZENON オープン記念企画『北条司展』」

期間：前期2024年4月17日（水）～5月19日（日）、後期2024年5月22日（水）～6月23日（日） ※前期・後期とも火曜定休  
場所：GALLERY ZENON  
住所：東京都武蔵野市吉祥寺南町 2-11-1