https://natalie.mu/comic/news/154360

# シティーハンター新装版1・2巻発売＆もっこりオムライス食べれるイベントも
《城市猎人》新装版1、2卷发售&可以吃蛋包饭的活动

2015年7月18日  [Comment](https://natalie.mu/comic/news/154360/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」30周年を記念したイベントが、吉祥寺のカフェ＆ダイニングバー・ソラZENONにて8月31日まで開催されている。  
北条司《城市猎人》30周年纪念活动在吉祥寺的咖啡厅&餐厅·Sora ZENON举行，活动将持续到8月31日。

[
![ソラZENONで食べられる「もっこりオムライス」。とても立派にもっこりしたチキンライスの重さは、リョウの愛銃コルトパイソン357にちなんで357gになっている。](https://ogre.natalie.mu/media/news/comic/2015/0718/mokkoriom.jpg?imwidth=468&imdensity=1)
ソラZENONで食べられる「もっこりオムライス」。とても立派にもっこりしたチキンライスの重さは、リョウの愛銃コルトパイソン357にちなんで357gになっている。  
大きなサイズで見る（全12件）  
在Sora ZENON能吃到的“Mokkori 蛋包饭”。非常漂亮的鸡肉饭的重量是357克，这与獠的爱Colt Python 357有关。  
查看大图(共12件)  
](https://natalie.mu/comic/gallery/news/154360/385729)

期間中は店内に、北条監修による特製メニュー5つが登場。「再現！タマゴとヨーグルトとミルクの赤まむし割り納豆フロート」は冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）が作中で作っていたスペシャルドリンクを再現したもので、元ネタのカラー原稿も店内に掲出されている。そのほか「もっこりオムライス」「XYZチョコバナナワッフル」「もっこりWフロートソーダ」「薬膳モッコリ!!」といったフード・ドリンクが用意されており、それぞれ注文した人には特典のマグネットが付属。リョウと香の縁結び絵馬など限定グッズも販売中だ。  
期间在店内，北条监修的5个特制菜单登场。“再现!鸡蛋、酸奶和牛奶的红豆混纳豆Float”是再现了冴羽獠(獠的汉字是“僚”的写法)在作品中制作的特别饮料，原型的彩色原稿也在店内张贴。此外还有“Mokkori蛋包饭”、“XYZ巧克力香蕉华夫饼”、“Mokkori WFloat Soda”、“药膳Mokkori!!”等食物和饮料，每个点餐的人都附赠特典磁铁。獠与香的姻缘匾额等限定商品也在销售中。

また本日7月18日には「シティーハンター XYZ Edition」1・2巻、「エンジェル・ハート2ndシーズン」11巻が発売。「シティーハンター XYZ Edition」は計57エピソードからなる「シティーハンター」を全12巻に再編集したもの。各巻600ページ超えの大ボリュームで、北条のロングインタビュー、全話リストなどファン必携の記事も多数収められている。  
今天7月18日《城市猎人XYZ Edition》1、2卷、《Angel Heart 2ndSeason》11卷发售。《城市猎人XYZ Edition》是将共计57集的《城市猎人》重新编辑成全12卷的作品。每卷超过600页的大篇幅，收录了北条的长篇采访、全集列表等众多粉丝必带的报道。  

全12巻の帯に付属する応募券を集めて申し込んだ人にはプレゼントを用意。北条の線画をもとに製作されるモーション・グラフィック・アニメ「リョウのプロポーズ」を収めたDVDか、複製原画2種セットから希望の品を選べる仕組みだ。  
为收集了全12卷的腰封附带的应募券的人准备了礼物。是以北条的线条画为基础被制作的动画“獠的求婚”的DVD，复制原画2种套装中选择希望的东西的结构。  

## 「シティーハンター」30周年イベント＠ソラZENON
《城市猎人》30周年活动@Sora ZENON  

期間：2015年7月17日（金）～8月31日（月）  
会場：Sora ZENON  
住所：東京都武蔵野市吉祥寺南町1-9-9 吉祥寺じぞうビル6F

この記事の画像（全12件）  
这篇报道的图片(共12张)  

[![ソラZENONで食べられる「もっこりオムライス」。とても立派にもっこりしたチキンライスの重さは、リョウの愛銃コルトパイソン357にちなんで357gになっている。](https://ogre.natalie.mu/media/news/comic/2015/0718/mokkoriom.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385729 "ソラZENONで食べられる「もっこりオムライス」。とても立派にもっこりしたチキンライスの重さは、リョウの愛銃コルトパイソン357にちなんで357gになっている。（在天空ZENON能吃到的“Mokkori蛋包饭”。非常漂亮的鸡肉饭的重量是357克，这与他的爱枪Colt Python 357有关。）")
[![「再現！タマゴとヨーグルトとミルクの赤まむし割り納豆フロート」。メニューを監修した北条司は「注文してもいいけど責任とらないよ！」とコメントしている。](https://ogre.natalie.mu/media/news/comic/2015/0718/nattof.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385730 "「再現！タマゴとヨーグルトとミルクの赤まむし割り納豆フロート」。メニューを監修した北条司は「注文してもいいけど責任とらないよ！」とコメントしている。（“再现!鸡蛋、酸奶、牛奶配红豆混纳豆Float”。监制菜单的北条司评论说:“你可以点餐，但我不负责!”。）")
[![ソラZENONには「再現！タマゴとヨーグルトとミルクの赤まむし割り納豆フロート」の元ネタとなった原画も展示されている。](https://ogre.natalie.mu/media/news/comic/2015/0718/CH30-natto-genga.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385731 "ソラZENONには「再現！タマゴとヨーグルトとミルクの赤まむし割り納豆フロート」の元ネタとなった原画も展示されている。（在“Sora ZENON”上还展示了作为“再现!鸡蛋、酸奶和牛奶的红豆混合纳豆Float”原型的原画。）")
[![「XYZチョコバナナワッフル」](https://ogre.natalie.mu/media/news/comic/2015/0718/xyzchocobanana.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385732 "「XYZチョコバナナワッフル」（XYZ巧克力香蕉华夫饼）")
[![「もっこりWフロートソーダ」](https://ogre.natalie.mu/media/news/comic/2015/0718/mokoriyakuzenberry.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385733 "「もっこりWフロートソーダ」（Mokkori WFloat Soda）")
[![「薬膳モッコリ!!」](https://ogre.natalie.mu/media/news/comic/2015/0718/yakuzenmokkori.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385734 "「薬膳モッコリ!!」（药膳Mokkori!!）")
[![ソラZENON限定販売のリョウと香の縁結び絵馬。](https://ogre.natalie.mu/media/news/comic/2015/0718/ch30-ema.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385735 "ソラZENON限定販売のリョウと香の縁結び絵馬。（Sora ZENON限定销售的獠与香的姻缘匾额。）")
[![ソラZENONの店内風景。](https://ogre.natalie.mu/media/news/comic/2015/0718/CH30-zenon-photo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385736 "ソラZENONの店内風景。（Sora ZENON的店内风景。）")
[![「シティーハンター」とソラゼノンによるコラボイベントのポスター。](https://ogre.natalie.mu/media/news/comic/2015/0718/ch30-zenonposter.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385737 "「シティーハンター」とソラゼノンによるコラボイベントのポスター。（《城市猎人》和Sora Zenon合作活动的海报。）")
[![「シティーハンター XYZ Edition」1巻](https://ogre.natalie.mu/media/news/comic/2015/0624/CHXYZ01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/375065 "「シティーハンター XYZ Edition」1巻")
[![「シティーハンター XYZ Edition」2巻](https://ogre.natalie.mu/media/news/comic/2015/0624/CHXYZ02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/375066 "「シティーハンター XYZ Edition」2巻")
[![「エンジェル・ハート2ndシーズン」11巻](https://ogre.natalie.mu/media/news/comic/2015/0718/AH2nd-11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/154360/385738 "「エンジェル・ハート2ndシーズン」11巻")


LINK  
[Sora ZENON | ソラゼノン](http://www.zenon-bar.jp/)

