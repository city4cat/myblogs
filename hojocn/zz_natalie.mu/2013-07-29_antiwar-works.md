https://natalie.mu/comic/news/96019

# 手塚治虫や水木しげるらの戦争作品収録したアンソロジー

2013年7月29日  [Comment](https://natalie.mu/comic/news/96019/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


戦争をテーマにした短編マンガのアンソロジー「漫画が語る戦争　戦場の挽歌」「漫画が語る戦争　焦土の鎮魂歌」の2冊が、本日7月29日に小学館クリエイティブより発売された。  
以战争为主题的短篇漫画选集《漫画讲述的战争:战场的挽歌》和《漫画讲述的战争:焦土的镇魂歌》两册，于7月29日由小学馆Creative发售。

[
![「漫画が語る戦争　戦場の挽歌」](https://ogre.natalie.mu/media/news/comic/2013/0729/war_elegy.jpg?imwidth=468&imdensity=1)
「漫画が語る戦争　戦場の挽歌」  
大きなサイズで見る（全2件）  
《漫画讲述的战争:战场的挽歌》  
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/96019/190973)

このアンソロジーは戦争の悲惨さや平和の尊さを、若い世代に伝えるために編纂されたもの。戦争で生き残った男の不思議な体験を描く[手塚治虫](https://natalie.mu/comic/artist/2048)の「カノン」や、[水木しげる](https://natalie.mu/comic/artist/2135)の自伝的作品「敗走記」など、単行本2冊に全20作が収録されている。  
这本文集是为了向年轻一代传达战争的悲惨以及和平的珍贵而编写的。手冢治虫的《Kanon》描写了在战争中幸存的男人不可思议的体验，水木茂的自传作品《败走记》等，单行本2册共收录了20部作品。   

## 収録作品

**漫画が語る戦争　戦場の挽歌**  
水木しげる「敗走記」  
[楳図かずお](https://natalie.mu/comic/artist/2350)「死者の行進」  
野間宏原作、[滝田ゆう](https://natalie.mu/comic/artist/4337)「真空地帯」  
[古谷三敏](https://natalie.mu/comic/artist/1922)「噺家戦記 柳亭円治」  
[新谷かおる](https://natalie.mu/comic/artist/2108)「イカロスの飛ぶ日」  
[比嘉慂](https://natalie.mu/comic/artist/2363)「砂の剣」  
[立原あゆみ](https://natalie.mu/comic/artist/2450)「手紙・敬礼」（「銀翼―つばさ―」より）  
湊谷夢吉「『マルクウ』兵器始末」  
[かわぐちかいじ](https://natalie.mu/comic/artist/1660)「一人だけの聖戦」（「テロルの系譜」より）  
[白土三平](https://natalie.mu/comic/artist/2354)「泣き原」  
[倉田よしみ](https://natalie.mu/comic/artist/2193)「若竹煮」（「味いちもんめ」より）  

**漫画が語る戦争　焦土の鎮魂歌**  
手塚治虫「カノン」  
[巴里夫](https://natalie.mu/comic/artist/4658)「石の戦場」  
早乙女勝元原作、政岡としや「火の瞳」  
[北条司](https://natalie.mu/comic/artist/2405)「少年たちのいた夏」  
[中沢啓治](https://natalie.mu/comic/artist/2257)「黒い鳩の群れに」  
[曽根富美子](https://natalie.mu/comic/artist/5072)「ヒロシマのおばちゃん」  
[ちばてつや](https://natalie.mu/comic/artist/1706)「家路1945～2003」  
[山上たつひこ](https://natalie.mu/comic/artist/2009)「回転」  
[村野守美](https://natalie.mu/comic/artist/4282)「御身大事に」（「垣根の魔女」より）  

この記事の画像（全2件）  
这篇文章的图片(共2篇)  
[![「漫画が語る戦争　戦場の挽歌」](https://ogre.natalie.mu/media/news/comic/2013/0729/war_elegy.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/96019/190973 "「漫画が語る戦争　戦場の挽歌」")
[![「漫画が語る戦争　焦土の鎮魂歌」](https://ogre.natalie.mu/media/news/comic/2013/0729/war_requiem.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/96019/190974 "「漫画が語る戦争　焦土の鎮魂歌」")


LINK

[漫画が語る戦争　戦場の挽歌 - 株式会社小学館Creative](./zz_shogakukan-cr.co.jp_antiwar-works0.md)  
[漫画が語る戦争　焦土の鎮魂歌 - 株式会社小学館Creative](./zz_shogakukan-cr.co.jp_antiwar-works1.md)  

関連商品

[
![Anthology「漫画が語る戦争 戦場の挽歌」](http://ecx.images-amazon.com/images/I/51izYR2ciuL._SS70_.jpg)  
anthology「漫画が語る戦争 戦場の挽歌」  
[漫画] 2013年7月29日発売 / 小学館 / 978-4778032562  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/477803256X/nataliecomic-22)

[
![Anthology「漫画が語る戦争 焦土の鎮魂歌」](http://ecx.images-amazon.com/images/I/51ZWgOcyCQL._SS70_.jpg)  
Anthology「漫画が語る戦争 焦土の鎮魂歌」  
[漫画] 2013年7月29日発売 / 小学館 / 978-4778032579  
Amazon.co.jp  
](https://www.amazon.co.jp/exec/obidos/ASIN/4778032578/nataliecomic-22)
