https://natalie.mu/comic/news/201830

# 北条司がプリンのパッケージを描き下ろし、吉祥寺のCAFE ZENONで限定販売
北条司亲自绘制Purin的Package，在吉祥寺的CAFE ZENON限量销售  

2016年9月14日  [Comment](https://natalie.mu/comic/news/201830/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)が、東京・吉祥寺のCAFE ZENONで販売されている「YAMANAKAプリン」のパッケージイラストを描き下ろした。9月20日から11月19日まで、一日50個限定で販売される。  
北条司为东京吉祥寺CAFE ZENON销售的“YAMANAKA布丁”绘制了包装插图。从9月20日到11月19日，每天限量销售50个。

[
![「YAMANAKAプリン」](https://ogre.natalie.mu/media/news/comic/2016/0914/purin003.jpg?impolicy=hq&imwidth=730&imdensity=1)
「YAMANAKAプリン」  
大きなサイズで見る（全3件）  
“YAMANAKA布丁”  
查看大图(共3件)  
](https://natalie.mu/comic/gallery/news/201830/580457)

[
![「美味しい笑顔の女の子」をテーマに、北条司は「エンジェル・ハート」のミキを描き下ろした。Illustration by TSUKASA HOJO](https://ogre.natalie.mu/media/news/comic/2016/0914/purin002.jpg?imwidth=468&imdensity=1)
「美味しい笑顔の女の子」をテーマに、北条司は「エンジェル・ハート」のミキを描き下ろした。Illustration by TSUKASA HOJO［拡大］  
以“美味笑容的女孩”为主题，北条司描绘了“天使心”的美纪。Illustration by TSUKASA HOJO[放大]  
](https://natalie.mu/comic/gallery/news/201830/580456)

「YAMANAKAプリン」はCAFE ZENONのオープン時から販売されている人気デザート。長らく店内でのみ提供されていたが、3月からテイクアウトが始まり手土産としても好評を博していた。今後も「美味しい笑顔の女の子」をテーマにさまざまなマンガ家がパッケージイラストを手がけていく予定。コラボ第1弾を担当した北条は「エンジェル・ハート」のミキを描き下ろした。  
“YAMANAKA布丁”是CAFE ZENON开业时就开始销售的人气甜点。长期以来只在店内提供，从3月开始外带作为伴手礼博得了好评。今后也将以“拥有美味笑容的女孩”为主题，邀请各种各样的漫画家来绘制包装插图。担当合作第1弹的北条描绘了“天使心”的美纪。  

この記事の画像（全3件）  
这篇报道的图片(共3篇)  

[![「美味しい笑顔の女の子」をテーマに、北条司は「エンジェル・ハート」のミキを描き下ろした。Illustration by TSUKASA HOJO](https://ogre.natalie.mu/media/news/comic/2016/0914/purin002.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/201830/580456 "「美味しい笑顔の女の子」をテーマに、北条司は「エンジェル・ハート」のミキを描き下ろした。Illustration by TSUKASA HOJO")
[![「YAMANAKAプリン」](https://ogre.natalie.mu/media/news/comic/2016/0914/purin003.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/201830/580457 "「YAMANAKAプリン」")
[![通常パッケージ時の「YAMANAKAプリン」。](https://ogre.natalie.mu/media/news/comic/2016/0914/purin001.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/201830/580458 "通常パッケージ時の「YAMANAKAプリン」。")

## 北条司Comment

このプリンの美味しさには僕も思わず笑顔になっちゃいました（笑）  
皆さんも是非一度食べてみてください。  
这布丁真好吃，我也忍不住露出了笑容。(笑)  
请大家一定要尝一尝。  


LINK

[CAFE ZENON | カフェゼノン](http://www.cafe-zenon.jp/)  
[株式会社ノース・スターズ・ピクチャーズ | North Stars Pictures](http://www.nsp.tv/)  
[株式会社じぞう屋](http://www.zizoya.co.jp/)  
