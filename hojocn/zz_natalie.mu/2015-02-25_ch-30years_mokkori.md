https://natalie.mu/comic/news/139357


# 「シティーハンター」30周年プロジェクト始動＆リョウのもっこり回数を集計

2015年2月25日  [Comment](https://natalie.mu/comic/news/139357/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)「シティーハンター」が、1985年の連載開始から今年で30周年を迎える。これを記念したプロジェクトの始動が、本日2月25日に発売された月刊コミックゼノン4月号（徳間書店）にて発表された。  
北条司的《城市猎人》自1985年开始连载以来，今年迎来了30周年。为了纪念这个Project，在今天2月25日发售的月刊Comic Zenon 4月号(德间书店)上发表了启动的消息。

[
![「シティーハンター」30周年ロゴ(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/0225/cityhunter30th.jpg?imwidth=468&imdensity=1)
「シティーハンター」30周年ロゴ(c)北条司/NSP 1985  
大きなサイズで見る（全2件）  
《城市猎人》30周年徽标(c)北条司/NSP 1985  
大尺寸查看(全2件)  
](https://natalie.mu/comic/gallery/news/139357/331438)

30周年を記念して、全12巻の「CITY HUNTER DX」が今夏より刊行開始。各巻600ページ超の大ボリュームで、描き下ろしイラストやロングインタビューなども収められる。有名ブランドとのコラボグッズや、メディアミックスなども予定。  
为纪念30周年，全12卷的《CITY HUNTER DX》将于今年夏天开始发行。每卷600页以上的大篇幅，还收录了亲自绘制的插图和长篇采访等。与著名品牌的合作商品和媒体组合也在计划之中。  

またゼノン4月号では、30年の歩みを振り返る特集記事も展開された。冴羽リョウ（リョウの漢字はけものへんに「僚」のつくり）がもっこりした回数、読み切りから連載化への変遷などが紹介されている。また全336話の中からベストエピソードを選ぶ人気投票も実施。トップ10に入ったエピソードはスマホサイトのゼノンランドで無料配信され、抽選で1名に北条の直筆サイン入り色紙がプレゼントされる。詳細は誌面にて確認してほしい。  
另外，Zenon 4月号还推出了回顾30年历程的特别报道。书中介绍了冴羽獠(獠的汉字是在反犬旁边组合“僚”)Mokkori的次数、从短篇到连载化的变迁等。另外，也将投票，从336话中选出最佳人气插曲。进入前10名的Episode将在智能手机网站Zenoland上免费播放，抽选1名将获得北条亲笔签名的彩纸作为礼物。详细内容请在杂志上确认。

この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![「シティーハンター」30周年ロゴ(c)北条司/NSP 1985](https://ogre.natalie.mu/media/news/comic/2015/0225/cityhunter30th.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/139357/331438 "「シティーハンター」30周年ロゴ(c)北条司/NSP 1985")
[![月刊コミックゼノン4月号](https://ogre.natalie.mu/media/news/comic/2015/0225/zenon1504.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/139357/331439 "月刊コミックゼノン4月号")


LINK

[ZENONLAND](http://zenonland.mopita.com/)  
[Comic Zenon](http://www.comic-zenon.jp/)  