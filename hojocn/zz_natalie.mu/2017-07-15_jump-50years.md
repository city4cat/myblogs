https://natalie.mu/comic/news/241003

# Jump50周年記念の復刻版、第1弾は創刊号と653万部記録号の2冊  
Jump 50周年纪念重印版，第1弹是创刊号和653万部记录号两册  

2017年7月15日  [Comment](https://natalie.mu/comic/news/241003/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


週刊少年Jump（集英社）の創刊50周年を記念した「復刻版 週刊少年Jump」の「パック1」が本日7月15日に刊行された。  
为纪念周刊少年Jump(集英社)创刊50周年，《复刻版 周刊少年Jump》的《Pack1》于7月15日发行。

[
![少年Jump1968年1号の復刻版。](https://ogre.natalie.mu/media/news/comic/2017/0715/jump_fukkokusoukan.jpg?imwidth=468&imdensity=1)
少年Jump1968年1号の復刻版。  
大きなサイズで見る（全2件）  
少年Jump 1968年1号的复刻版。  
查看大图(共2件)  
](https://natalie.mu/comic/gallery/news/241003/740342)

「復刻版 週刊少年Jump」は週刊少年Jumpの歴史の中で記念、記録、記憶に残った号を復刻するもの。「パック1」には創刊号にあたる1968年7月11日発売の1968年1号と、同誌の最大発行部数653万部を記録した1994年12月20日発売の1995年3・4合併号がセット販売されている。  
《复刻版 周刊少年Jump》是对周刊少年Jump历史中有纪念、记录、记忆的一期进行复刻。“Pack1”包括1968年7月11日发行的创刊号1968年1号和创下该杂志最大发行量653万部记录的1994年12月20日发行的1995年3、4合并号。

8月12日発売の「パック2」には2017年に30周年を迎える[荒木飛呂彦](https://natalie.mu/comic/artist/1940)「ジョジョの奇妙な冒険」、20周年を迎える[尾田栄一郎](https://natalie.mu/comic/artist/2368)「ONE PIECE」の連載がスタートした、1987年1・2合併号、1997年34号を同梱。9月16日発売の「パック3」では、連載作品の名セリフにフォーカスし、[武論尊](https://natalie.mu/comic/artist/2383)原作による[原哲夫](https://natalie.mu/comic/artist/1917)「北斗の拳」で「わが生涯に一片の悔いなし!!」、[鳥山明](https://natalie.mu/comic/artist/2266)「DRAGON BALL」で「クリリンのことかーっ!!!!!」が登場した1986年26号、1991年21・22合併号がパッケージされる。  
8月12日发售的《Pack2》中包括2017年迎来30周年的荒木飞吕彦的《JoJo的奇妙冒险》，以及迎来20周年的尾田荣一郎的《ONE PIECE》开始连载的1987年1、2合并号、1997年34号。9月16日发售的《Pack3》将聚焦连载作品中的名台词，根据武论尊原作改编的原哲夫的《北斗神拳》将以“我这一生无怨无悔!!”、鸟山明的《DRAGON BALL》中"克林的事!!!!!"的1986年26号，1991年21、22合并号被打包。

この記事の画像（全2件）  
这篇报道的图片(共2篇)  

[![少年Jump1968年1号の復刻版。](https://ogre.natalie.mu/media/news/comic/2017/0715/jump_fukkokusoukan.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/241003/740342 "少年Jump1968年1号の復刻版。")
[![週刊少年Jump1995年3・4合併号の復刻版。](https://ogre.natalie.mu/media/news/comic/2017/0715/jump_fukkoku953_4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/241003/740343 "週刊少年Jump1995年3・4合併号の復刻版。")

## 復刻版 週刊少年Jump パック1  
复刻版 周刊少年Jump Pack1  

### 少年Jump1968年1号掲載作品  
少年Jump 1968年1号刊载作品  

[梅本さちお](https://natalie.mu/comic/artist/4667)「くじら大吾」  
[赤塚不二夫](https://natalie.mu/comic/artist/2179)「大あばれアパッチ君」  
[高野よしてる](https://natalie.mu/comic/artist/3557)「地獄剣」  
[貝塚ひろし](https://natalie.mu/comic/artist/3266)「父の魂」  
[永井豪](https://natalie.mu/comic/artist/1804)「ハレンチ学園」  
[望月三起也](https://natalie.mu/comic/artist/2399)「ドル野郎」  
ダン・バリー「フラッシュ・ゴードン」  
[楳図かずお](https://natalie.mu/comic/artist/2350)「手」

### 週刊少年Jump1995年3・4合併号掲載作品  
周刊少年Jump 1995年3·4合并号刊载作品  

[井上雄彦](https://natalie.mu/comic/artist/1791)「SLAM DUNK」（オールカラー）  
[桂正和](https://natalie.mu/comic/artist/1908)「SHADOW LADY」  
[かずはじめ](https://natalie.mu/comic/artist/1654)「MIND ASSASSIN」  
[鳥山明](https://natalie.mu/comic/artist/2266)「DRAGON BALL」  
[つの丸](https://natalie.mu/comic/artist/1710)「みどりのマキバオー」  
[ガモウひろし](https://natalie.mu/comic/artist/2671)「とっても！ラッキーマン」  
[和月伸宏](https://natalie.mu/comic/artist/4971)「るろうに剣心-明治剣客浪漫譚-」  
[高橋ゆたか](https://natalie.mu/comic/artist/3522)「ボンボン坂高校演劇部」  
[梅澤春人](https://natalie.mu/comic/artist/2349)「NANPO U DEN」  
真倉翔・[岡野剛](https://natalie.mu/comic/artist/3213)「地獄先生ぬ～べ～」  
[荒木飛呂彦](https://natalie.mu/comic/artist/1940)「ジョジョの奇妙な冒険」  
[高橋陽一](https://natalie.mu/comic/artist/1953)「キャプテン翼 ワールドユース編」  
[秋本治](https://natalie.mu/comic/artist/2053)「こちら葛飾区亀有公園前派出所」  
[徳弘正也](https://natalie.mu/comic/artist/2325)「新ジャングルの王者ターちゃん▽」※▽はハートマーク。  
[三条陸](https://natalie.mu/comic/artist/3688)・[稲田浩司](https://natalie.mu/comic/artist/3122)・堀井雄二「DRAGON QUEST-ダイの大冒険-」  
隆慶一郎・[原哲夫](https://natalie.mu/comic/artist/1917)「影武者徳川家康」  
[桐山光侍](https://natalie.mu/comic/artist/1895)「NINKU-忍空- SECOND STAGE」※Uはマクロン付きが正式表記。  
[北条司](https://natalie.mu/comic/artist/2405)「RASH!!」  
[森田まさのり](https://natalie.mu/comic/artist/2117)「ろくでなしBLUES」  
梅澤春人「BOY」※Oはストローク符号付きが正式表記。  
[宮下あきら](https://natalie.mu/comic/artist/1877)「BAKUDAN」  
[なにわ小吉](https://natalie.mu/comic/artist/2827)「王様はロバ～はったり帝国の逆襲～」

## 復刻版 週刊少年Jump パック2
复刻版 周刊少年Jump Pack2  

週刊少年Jump1987年1・2合併号  
週刊少年Jump1997年34号

## 復刻版 週刊少年Jump パック3  
复刻版周刊少年Jump Pack3  

週刊少年Jump1986年26号  
週刊少年Jump1991年21・22合併号


LINK

[集英社『週刊少年Jump』公式Site](https://www.shonenjump.com/j/)