https://natalie.mu/eiga/news/456723

# 「劇場版シティーハンター <新宿プライベート・アイズ>」BS12でオンエア

2021年12月11日 [Comment](https://natalie.mu/eiga/news/456723/comment) [映画ナタリー編集部](https://natalie.mu/eiga/author/74)

長編アニメーション「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」が、明日12月12日19時からBS12 トゥエルビで放送される。
长篇动画《剧场版城市猎人<新宿Private Eyes>》将于明天12月12日19点在BS12 twellv播出。  

[
![「劇場版シティーハンター <新宿プライベート・アイズ>」ビジュアル](https://ogre.natalie.mu/media/news/eiga/2021/1208/ch_202112.jpg?imwidth=468&imdensity=1)
「劇場版シティーハンター <新宿プライベート・アイズ>」ビジュアル  
大きなサイズで見る  
《剧场版城市猎人<新宿Private Eyes>》视觉图  
大尺寸观看  
](https://natalie.mu/eiga/gallery/news/456723/1718439)

[北条司](https://natalie.mu/eiga/artist/2405)のマンガをもとにした本作では、裏社会の始末屋（スイーパー）であり無類の女好きである冴羽リョウと、彼の相棒・槇村香の活躍が描かれる。2019年にアニメ放送30周年記念プロジェクトとして制作された。  
根据北条司的漫画改编而成的本作品，描写了黑社会的Sweeper冴羽獠和他的搭档槙村香之间活跃的故事。2019年作为动画放送30周年纪念项目而制作。  

総監督は映画「名探偵コナン」シリーズを手がけた[こだま兼嗣](https://natalie.mu/eiga/artist/13476)。リョウに[神谷明](https://natalie.mu/eiga/artist/60997)、香に[伊倉一恵](https://natalie.mu/eiga/artist/95562)が声を当て、ゲスト声優として[飯豊まりえ](https://natalie.mu/eiga/artist/70103)、[徳井義実](https://natalie.mu/eiga/artist/41274)（[チュートリアル](https://natalie.mu/eiga/artist/7083)）が出演した。  
总导演是曾执导电影《名侦探柯南》系列的こだま兼嗣。神谷明为獠配音、伊仓一惠为香配音，饭丰まりえ、德井义实作为客串声优出演。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  
※冴羽獠的獠，在反犬旁正式标明“僚”的写法  

## 日曜アニメ劇場「劇場版シティーハンター <新宿プライベート・アイズ>」  
周日动画剧场《剧场版城市猎人<新宿Private Eyes>》  

BS12 トゥエルビ 2021年12月12日（日）19:00～  
BS12 twellv 2021年12月12日(星期日)19:00～  


(c)北条司/Coamix・「2019 劇場版シティーハンター」製作委員会  
(c)北条司/《2019剧场版城市猎人》制作委员会  


LINK  
[日曜Anime劇場｜BS12（twellv）](https://www.twellv.co.jp/program/anime/sunday-animation/)

