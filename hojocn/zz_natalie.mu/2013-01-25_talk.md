https://natalie.mu/comic/news/83720

# 北条司、日笠優、保谷伸、モリコロスがゼノンで座談会
北条司、日笠优、保谷伸、Molicoross在芝诺举行座谈会  

2013年1月25日  [Comment](https://natalie.mu/comic/news/83720/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


[北条司](https://natalie.mu/comic/artist/2405)、[日笠優](https://natalie.mu/comic/artist/10053)、[保谷伸](https://natalie.mu/comic/artist/9940)、[モリコロス](https://natalie.mu/comic/artist/9941)による座談会が、本日1月25日に発売された月刊コミックゼノン3月号（徳間書店）に掲載されている。  
北条司、日笠优、保谷伸、森克萝丝的座谈会刊登在1月25日发售的月刊Comic Zenon 3月号(德间书店)上。

[
![月刊コミックゼノン3月号](https://ogre.natalie.mu/media/news/comic/2013/0125/zenon13-3.jpg?imwidth=468&imdensity=1)
月刊コミックゼノン3月号
大きなサイズで見る
](https://natalie.mu/comic/gallery/news/83720/155917)

この座談会は月刊コミックゼノンの新人3名が、大先輩である北条にマンガの描き方を質問するという趣旨。ネームに費やす時間、アシスタントに指示を出す方法など、すでにプロとして活躍する3名は実践的なアドバイスの数々に耳を傾ける。「キャッツ▼アイ（▼はハートマーク）」執筆時の思い出など、北条が新人時代を思い出す貴重な話も満載だ。  
这个座谈会的宗旨是月刊Comic Zenon的3名新人，向大前辈北条提问漫画的画法。在name上花费的时间、向助手下达指示的方法等，已经作为专业人士活跃的3人倾听了很多实践性的建议。《Cat's▼Eye(▼是Heart mark)》执笔时的回忆等，满载着北条回忆新人时代的珍贵故事。  

(译注：  
关于ネーム/name的解释（源自：[精選版 日本国語大辞典](https://kotobank.jp/word/ネーム-594576)）：  
① 名字。〔舶来語便覧（1912）〕  
② 特别是在西装的翻领处、衬衫的手臂、胸前等处刺绣的名字。  ...  
③ 书籍、杂志等的卷首图、图版、插图等附加的简短说明性文字。另外，在漫画中，插在扉页上的对话行。〔造本と印刷（1948）〕  
)




LINK

[Comic Zenon](http://www.comic-zenon.jp/)

