https://natalie.mu/comic/news/315627


# 「劇場版シティーハンター」に怪盗キャッツアイの3姉妹登場！戸田恵子らも出演
「劇場版CityHunter」怪盗Cat'sEye三姐妹登场!户田惠子等也出演  

2019年1月11日  [Comment](https://natalie.mu/comic/news/315627/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

[北条司](https://natalie.mu/comic/artist/2405)原作によるアニメ映画「[劇場版シティーハンター <新宿プライベート・アイズ>](https://natalie.mu/eiga/film/175572)」の本予告動画第2弾が公開。北条の「キャッツ▼アイ」（▼はハートマーク）より、怪盗キャッツアイこと瞳、泪、愛の3姉妹が映画に登場することが明らかになった。  
根据北条司原作改编的动画电影《剧场版城市猎人<新宿私人之眼>》的第二弹预告视频公开。根据北条的《猫▼爱》，怪盗猫眼即瞳、泪、爱三姐妹将在电影中登场。

[
![怪盗キャッツアイ。左から次女・瞳、三女・愛、長女・泪。](https://ogre.natalie.mu/media/news/comic/2019/0111/catseye.jpg?impolicy=hq&imwidth=730&imdensity=1)
怪盗キャッツアイ。左から次女・瞳、三女・愛、長女・泪。
大きなサイズで見る（全10件）  
怪盗猫眼。左起次女·瞳、三女·爱、长女·泪。  
查看大图(共10件)  
](https://natalie.mu/comic/gallery/news/315627/1087346)

[
![「キャッツ▼アイ COMPLETE EDITION」1巻](https://ogre.natalie.mu/media/news/comic/2019/0111/catsaye01.jpg?imwidth=468&imdensity=1)
「キャッツ▼アイ COMPLETE EDITION」1巻［拡大］
](https://natalie.mu/comic/gallery/news/315627/1087347)

1981年に発表された「キャッツ▼アイ」は北条の連載デビュー作。普段は「喫茶キャッツアイ」で働く美人3姉妹が、裏の顔としてレオタード姿で怪盗キャッツアイとして活躍する姿を描き、1983年にはアニメ化もされた人気作だ。「シティーハンター」でも海坊主と美樹が働く店の名前が「喫茶キャッツアイ」ではあるものの、両者の関係は特に名言されていなかったが、このたびの映画では北条のアイデアにより「海坊主たちが働く店のオーナーが3姉妹」という設定になるという。このたびの予告動画ではアニメ「キャッツ▼アイ」の主題歌「CAT’S EYE」をバックに、レオタードを身にまとった3姉妹が新宿の街に登場している。  
1981年发表的《猫▼爱》是北条的连载处女作。描写了平时在“吃茶猫眼”工作的美人三姐妹，在幕后以猫王的姿态作为怪盗猫眼活跃的身姿，是1983年被动画化的人气作品。在《城市猎人》中，海坊主和美树工作的店的名字虽然是“猫眼咖啡店”，但两者的关系并没有特别的名言。在这部电影中，根据北条的创意，“海坊主们工作的店的店主是3姐妹”的设定。在这次的预告视频中，以动画片《猫》的主题曲《CAT’S EYE》为背景，身着休闲礼服的三姐妹在新宿街头登场。  

またキャッツアイの長女・泪と次女・瞳の2役を[戸田恵子](https://natalie.mu/comic/artist/16682)、三女・愛を[坂本千夏](https://natalie.mu/comic/artist/95232)が演じることも明らかに。瞳と愛はオリジナル版からの続投で、泪は昨年12月に亡くなった藤田淑子から戸田が引き継ぐ形だ。戸田は「泪役の藤田淑子さんとは公私ともに本当に姉妹のように過ごしてきました。実際、トコ姉さんも演じたい気持ちでいっぱいでした。その思いを汲んで妹の私が代役を申し出ました」とのコメントを発表している。  
另外，猫眼的长女·泪和次女·瞳这两个角色将由户田惠子饰演，三女·爱将由坂本千夏饰演。瞳和爱是原版的续篇，泪则由户田继承去年12月去世的藤田淑子。户田评述道:“我和饰演泪的藤田淑子无论于公于私都像姐妹一样相处。实际上，我也充满了想要出演床姐的心情。作为妹妹的我了解到她的这种想法，所以主动要求代替她出演。”  

さらにゲスト声優として発表されていた、[徳井義実](https://natalie.mu/comic/artist/41274)（[チュートリアル](https://natalie.mu/comic/artist/7083)）の演じるキャラクターが、個性的なファッションデザイナー・コニータであることも明らかになった。「劇場版シティーハンター <新宿プライベート・アイズ>」は2月8日ロードショー。神谷明、伊倉一恵らTVアニメのキャストが再集結し、映画のために書き下ろされたオリジナルストーリーが展開される。  
此外，作为特邀声优发表的德井义实(tutorial)所饰演的角色，也明确是个性十足的时装设计师康妮塔。《剧场版城市猎人<新宿私人之眼>》将于2月8日上映。神谷明、伊仓一惠等TV动画的演员再次集结，为电影谱写的原创故事即将展开。




## 戸田恵子Comment

[
![戸田恵子](https://ogre.natalie.mu/media/news/comic/2019/0111/todakeiko.jpg?imwidth=468&imdensity=1)
戸田恵子［拡大］
](https://natalie.mu/comic/gallery/news/315627/1087348)

『キャッツ▼アイ』の収録当時が思い出されて懐かしかったです。泪役の藤田淑子さんとは公私ともに本当に姉妹のように過ごしてきました。実際、トコ姉さんも演じたい気持ちでいっぱいでした。その思いを汲んで妹の私が代役を申し出ました。スタッフの皆さんも受け入れてくださって感謝します。  
想起了《猫▼爱》的录制当时，很怀念。和饰演泪的藤田淑子于公于私都真的像姐妹一样相处。实际上，我也充满了想演地板姐姐的心情。作为妹妹的我理解了她的想法，主动要求代替她。感谢各位工作人员接受我。

この記事の画像・動画（全10件）  
这篇报道的图片、视频(共10篇)  

[![怪盗キャッツアイ。左から次女・瞳、三女・愛、長女・泪。](https://ogre.natalie.mu/media/news/comic/2019/0111/catseye.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315627/1087346 "怪盗キャッツアイ。左から次女・瞳、三女・愛、長女・泪。")
[![「キャッツ▼アイ COMPLETE EDITION」1巻](https://ogre.natalie.mu/media/news/comic/2019/0111/catsaye01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315627/1087347 "「キャッツ▼アイ COMPLETE EDITION」1巻")
[![戸田恵子](https://ogre.natalie.mu/media/news/comic/2019/0111/todakeiko.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315627/1087348 "戸田恵子")
[![徳井義実が演じるコニータ。](https://ogre.natalie.mu/media/news/comic/2019/0111/tokui.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315627/1087349 "徳井義実が演じるコニータ。")
[![「劇場版シティーハンター 〈新宿プライベート・アイズ〉」の本ポスター。](https://ogre.natalie.mu/media/news/comic/2018/1212/cityhunter01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315627/1069398 "「劇場版シティーハンター 〈新宿プライベート・アイズ〉」の本ポスター。")
[![徳井義実](https://ogre.natalie.mu/media/news/comic/2018/1212/tokui.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315627/1069401 "徳井義実")
[![飯豊まりえ](https://ogre.natalie.mu/media/news/comic/2018/1212/iitoyo.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315627/1069400 "飯豊まりえ")
[![飯豊まりえ演じる進藤亜衣。](https://ogre.natalie.mu/media/news/comic/2018/1212/cityhunter02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/315627/1069399 "飯豊まりえ演じる進藤亜衣。")
[![「劇場版シティーハンター ＜新宿プライベート・アイズ＞」本予告 | 2019年2月8日(金)全国ロードショー](https://i.ytimg.com/vi/CRFrmaZa1OY/default.jpg)](https://natalie.mu/comic/gallery/news/315627/media/34932 "「劇場版シティーハンター ＜新宿プライベート・アイズ＞」本予告 | 2019年2月8日(金)全国ロードショー")
[![「劇場版シティーハンター <新宿プライベート・アイズ>」本予告2](https://i.ytimg.com/vi/mji4WOAQT0w/default.jpg)](https://natalie.mu/comic/gallery/news/315627/media/35561 "「劇場版シティーハンター <新宿プライベート・アイズ>」本予告2")

## 映画「劇場版シティーハンター 〈新宿プライベート・アイズ〉」  
电影《剧场版城市猎人<新宿私人眼>》  

2019年2月8日（金）公開

### Cast

冴羽獠：神谷明  
槇村香：伊倉一恵  
進藤亜衣：飯豊まりえ  
御国真司：山寺宏一  
野上冴子：一龍斎春水  
海坊主：玄田哲章  
美樹：小山茉美  
Vince・Ingrado：大塚芳忠  
Konita：[徳井義実](https://natalie.mu/comic/artist/41274)  
来生瞳・来生泪：[戸田恵子](https://natalie.mu/comic/artist/16682)  
来生愛：[坂本千夏](https://natalie.mu/comic/artist/95232)

### Staff

原作：[北条司](https://natalie.mu/comic/artist/2405)  
総監督：儿玉兼嗣  
脚本：加藤陽一  
Chief演出：佐藤照雄、京極尚彦  
Character Design：高橋久美子  
総作画監督：菱沼義仁  
美術監督：加藤浩  
色彩設計：久保木裕一  
音響監督：長崎行男  
音響制作：AUDIO PLANNING U  
音楽：岩崎琢  
編集：今井大介（JAYFILM）  
Animation制作：Sunrise
配給：Aniplex

※リョウの漢字はけものへんに「僚」のつくりが正式表記。  
※两字的汉字在兽的旁边“僚”的构造是正式表记。  

※岩崎琢の琢は旧字体が正式表記。  
※岩崎琢的琢是旧字体正式标记。



(c)北条司/NSP・「2019 劇場版シティーハンター」製作委員会

LINK

[アニメ「劇場版シティーハンター」公式サイト](http://cityhunter-movie.com/)
[「劇場版シティーハンター」公式 (@cityhuntermovie) | Twitter](https://twitter.com/cityhuntermovie)