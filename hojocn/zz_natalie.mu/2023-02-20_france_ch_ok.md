source:  
https://natalie.mu/eiga/news/513482

# フランス版「CityHunter」もっこり応援上映！タンバリン、うちわ持ち込みOK
法国版「CityHunter」正式上映!带手鼓、团扇OK（译注：待校对）  

2023年2月20日

「[CityHunter THE MOVIE 史上最香のMission](https://natalie.mu/eiga/film/182791)」日本語吹替版の“もっこり応援上映”が、冴羽リョウの誕生日にあたる3月26日に東京・新宿ピカデリーで開催される。  
「城市猎人THE MOVIE 史上最香的任务」日语配音版的“Mokkori应援放映”,相当于冴羽獠的生日3月26日在东京新宿Piccadilly召开被。  

[
![「CityHunter THE MOVIE 史上最香のMission」Poster Visual](https://ogre.natalie.mu/media/news/eiga/2023/0218/CH_2023_01.jpg?imwidth=468&imdensity=1)
「CityHunter THE MOVIE 史上最香のMission」Poster Visual
大きなサイズで見る（全3件）(大尺寸观看)
](https://natalie.mu/eiga/gallery/news/513482/2001590)

[
「CityHunter THE MOVIE 史上最香のMission」場面写真［拡大］
](https://natalie.mu/eiga/gallery/news/513482/2001591)

[北条司](https://natalie.mu/eiga/artist/2405)のマンガ「CityHunter」をフランスで実写化した本作。原作ファンの[フィリップ・ラショー](https://natalie.mu/eiga/artist/99116)が監督と主演を務め、日本語吹替版には[山寺宏一](https://natalie.mu/eiga/artist/30965)、[沢城みゆき](https://natalie.mu/eiga/artist/65954)が出演した。  
北条司的漫画“城市猎人”在法国真人版。原著粉丝菲利普·拉肖担任导演和主演，日语配音版由山寺宏一、沢城みゆき出演。

[
「CityHunter THE MOVIE 史上最香のMission」場面写真［拡大］
](https://natalie.mu/eiga/gallery/news/513482/2001592)

この上映企画では、発声・拍手しながら映画を楽しむことができる。さらにタンバリン、鈴、ペンライト、サイリウム、うちわなど手元に収まる範囲の応援グッズが持ち込み可能だ。鑑賞料金は税込2000円で、チケットは劇場のWebサイトおよび窓口で販売される。発売日は追ってアナウンスされる。  
在这个放映计划中，可以一边发声、鼓掌一边欣赏电影。手鼓、铃铛、小电筒、荧光棒、团扇等手头范围内的应援物品都可以带入。鉴赏费用为含税2000日元，门票在剧场的网站及售票窗口销售。发售日将在稍后广播。  

※冴羽リョウのリョウは、けものへんに「僚」のつくりが正式表記  
※冴羽獠的獠的官方写法是犭的「僚」   

## 「CityHunter THE MOVIE 史上最香のMission」“もっこり応援上映”  
「城市猎人THE MOVIE 史上最香的任务」日语配音版的“Mokkori应援放映”  

2023年3月26日（日）東京都 新宿Piccadilly  
18:30～  
料金：2000円  

この記事の画像（全3件）

*   [![](https://ogre.natalie.mu/media/news/eiga/2023/0218/CH_2023_01.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513482/2001590 "「CityHunter THE MOVIE 史上最香のMission」Poster Visual")
*   [![](https://ogre.natalie.mu/media/news/eiga/2023/0218/CH_2023_02.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513482/2001591 "「CityHunter THE MOVIE 史上最香のMission」場面写真")
*   [![](https://ogre.natalie.mu/media/news/eiga/2023/0218/CH_2023_03.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/eiga/gallery/news/513482/2001592 "「CityHunter THE MOVIE 史上最香のMission」場面写真")

(c)AXEL FILMS PRODUCTION - BAF PROD - M6 FILMS

## 読者の反応

[Comment](https://natalie.mu/eiga/news/513482/comment)

[コメントを読む（15件）](https://natalie.mu/eiga/news/513482/comment)

関連記事


[
山寺宏一と沢城みゆきがフランス版「CityHunter」で吹替、神谷明も太鼓判
](https://natalie.mu/eiga/news/348174)

[
神谷明「CityHunter愛がビンビン伝わる！」フランス実写版監督に対面し絶賛
](https://natalie.mu/eiga/news/352217)

[
増量、ジャケットを特注…仏版「CityHunter」の冴羽リョウはこうして生まれた
](https://natalie.mu/eiga/news/355555)

[
Get wild and tough!「劇場版CityHunter」特報で2023tハンマー炸裂
](https://natalie.mu/eiga/news/513832)

## フィリップ・ラショーのほかの記事

*   [記憶喪失の役者が勘違いヒーローに、「バッドマン」海外版Poster Visualが到着](https://natalie.mu/eiga/news/484977)
*   [記憶喪失から勘違いでヒーローに「バッドマン」予告編、“逆さ吊りキス”シーンも](https://natalie.mu/eiga/news/479858)
*   [「バッドマン」アクション収めた特報公開、アメコミヒーローを彷彿とさせる場面写真も](https://natalie.mu/eiga/news/477897)
*   [フランス版「CityHunter」チームが再集結、記憶喪失ヒーロー描くコメディ公開](https://natalie.mu/eiga/news/476638)
*   [フィリップ・ラショーのプロフィール](https://natalie.mu/eiga/artist/99116)

リンク

*   [新宿ピカデリー 公式サイト](https://www.smt-cinema.com/site/shinjuku/index.html)
*   [「CityHunter THE MOVIE 史上最香のMission」公式サイト](http://cityhunter-themovie.com)

関連商品

[
劇場版CityHunter <新宿プライベート・アイズ>（完全生産限定版 / Blu-ray）
[Blu-ray Disc] 2019年10月30日発売 / アニプレックス / ANZX-15021
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B07W3MBV17/natalieeiga-22)

[
CITY HUNTER Blu-ray Disc BOX（完全生産限定版）
[Blu-ray Disc 7枚組] 2019年1月30日発売 / アニプレックス / ANZX-14901
Amazon.co.jp
](https://www.amazon.co.jp/exec/obidos/ASIN/B07GH143NZ/natalieeiga-22)


---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

