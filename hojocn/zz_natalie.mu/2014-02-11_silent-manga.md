https://natalie.mu/comic/news/109637


# 世界のサイレントマンガ作品集に永井豪らのインタビュー
永井豪等人对世界默白漫画作品集的采访

2014年2月11日  [Comment](https://natalie.mu/comic/news/109637/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)


「世界から届いたジャパニーズマンガ」がコアミックスより発売された。セリフのない“サイレントマンガ”のコンテスト「第1回サイレントマンガオーディション」の受賞作品を収めた書籍だ。  
《来自世界的Japanese Manga》由Coamix发行。这是收录了没有对白的“Silent Manga”Audition“第一届Silent Manga Audition”的获奖作品的书籍。

[
![「世界から届いたジャパニーズマンガ」](https://ogre.natalie.mu/media/news/comic/2014/0211/silentmanga.jpg?imwidth=468&imdensity=1)
「世界から届いたジャパニーズマンガ」  
大きなサイズで見る  
“来自世界的Japanese Manga”  
查看大图  
](https://natalie.mu/comic/gallery/news/109637/232762)

本書には53の国と地域から集まった応募作の中から選ばれた、受賞作26編を収録。各受賞者に加え、[永井豪](https://natalie.mu/comic/artist/1804)、[古屋兎丸](https://natalie.mu/comic/artist/1919)へのインタビューや、[北条司](https://natalie.mu/comic/artist/2405)による総評も掲載された。また[堀江信彦](https://natalie.mu/comic/artist/5280)と、手塚プロダクション社長の松谷孝征が、日本のマンガの海外展開などについて語り合った対談も収められている。  
本书收录了从53个国家和地区的应征作品中选出的26篇获奖作品。除了各获奖者之外，还刊登了对永井豪、古屋兔丸的采访，以及北条司的总评。另外，堀江信彦与手冢制作公司社长松谷孝征就日本漫画的海外发展等话题进行的对谈也收录其中。

そのほか世界各国のマンガ事情についての解説ページも付属しており、世界で日本のマンガがどのように読まれているかがわかる1冊となっている。価格は3360円。  
除此之外，书中还附赠了世界各国漫画情况的解说页，是一本可以了解世界上是如何阅读日本漫画的书。价格为3360日元。

LINK

[Silent Manga Audition the Book 2014!!!『世界から届いたJapanese Manga　　第１回Silent Manga Audition受賞作品』 | SILENT MANGA AUDITION](http://www.manga-audition.com/silentmanga2014/)  
[書籍『世界から届いたJapanese Manga』2月上旬より発売です - COAMIX｜株式会社Coamix](http://www.coamix.co.jp/news/?p=208)  

関連商品

[
![Silent Manga Audition事務局「世界から届いたJapanese Manga」](https://images-fe.ssl-images-amazon.com/images/I/51N27TjOwsL._SS70_.jpg)
Silent Manga Audition事務局「世界から届いたJapanese Manga」  
[書籍] 2014年2月1日発売 / 978-4905246312  
Amazon.co.jp [書籍版 / Kindle版]  
](https://www.amazon.co.jp/exec/obidos/ASIN/4905246318/nataliecomic-22)