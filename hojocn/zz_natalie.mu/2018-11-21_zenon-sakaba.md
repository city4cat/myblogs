https://natalie.mu/comic/news/308902

# 「北斗の拳」「CityHunter」のGlassで乾杯、吉祥寺にZENON SAKABA開店  
用「北斗神拳」「City Hunter」的杯子干杯，ZENON SAKABA在吉祥寺开店

2018年11月21日 [Comment](https://natalie.mu/comic/news/308902/comment) [コミックナタリー編集部](https://natalie.mu/comic/author/72)

じぞう屋による飲食店ZENON SAKABAが、11月28日に東京・吉祥寺にオープンする。じぞう屋は月刊コミックゼノン（徳間書店）を発行するコアミックスのグループ会社だ。  
由Zizoya经营的饮食店ZENON SAKABA将于11月28日在东京吉祥寺开业。Zizoya是发行月刊Comic Zenon(德间书店)的Coamix的一个集团公司。

[
![ZENON SAKABAの外観。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_10.jpg?impolicy=hq&imwidth=730&imdensity=1)
ZENON SAKABAの外観。  
大きなサイズで見る（全21件）  
ZENON SAKABA的外观。  
查看大图(共21件)  
](https://natalie.mu/comic/gallery/news/308902/1054745)

[
![「北斗の拳」のGlass。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_19.jpg?imwidth=468&imdensity=1)
「北斗の拳」のGlass。［拡大］  
“北斗神拳”的酒杯。[扩大]  
](https://natalie.mu/comic/gallery/news/308902/1054746)

じぞう屋は“空間の漫画雑誌”というコンセプトを掲げるCAFE ZENONも運営しており、同店は吉祥寺の名物カフェとして2009年の開店から好評を博している。新たに立ち上げるZENON SAKABAは“体験と出逢いがテーマのネオサカバ”を謳い、熊本直送の希少肉などこだわりの食材を使った料理が楽しめるほか、ショーができるステージなども設置。大人の好奇心を刺激する仕掛けが多数用意された。  
Zizoya还经营着以“空间漫画杂志”为理念的CAFE ZENON，这家店作为吉祥寺的特色咖啡馆，自2009年开业以来大受好评。新成立的ZENON SAKABA号称“以体验和邂逅为主题的新咖啡馆”，除了可以品尝到使用熊本直送的稀有肉等特色食材制作的料理之外，还设置了可以表演的舞台。准备了很多刺激大人好奇心的装置。

店内のドリンクはすべてワインGlassで提供。「北斗の拳」のケンシロウとラオウ、「CityHunter」の冴羽リョウと槇村香が乾杯をする、描き下ろしイラストをあしらったオリジナルGlassが楽しめる。またゼノンで連載するマンガ家や吉祥寺にちなんだ作家のサインを飾る「Sign Wall」も見どころだ。  
店内的饮料都是用红酒杯提供的。《北斗神拳》的健四郎和罗王、《城市猎人》的冴羽獠和槙村香一起干杯，配上新绘制的插图的原创酒杯非常有趣。另外，装饰有在Zenon连载的漫画家和与吉祥寺相关的作家签名的“签名墙”也是一大看点。

この記事の画像（全21件）  
这篇报道的图片(共21张)  

[![ZENON SAKABAの外観。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_10.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054745 "ZENON SAKABAの外観。")
[![「北斗の拳」のGlas。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_19.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054746 "「北斗の拳」のGlass。")
[![「CityHunter」のGlass。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_20.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054747 "「CityHunter」のGlass。")
[![ZENON SAKABAのSign Wall。](https://ogre.natalie.mu/media/news/eiga/2018/1121/CCDN0045.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054748 "ZENON SAKABAのSign Wall。")
[![ZENON SAKABAのSign Wall。](https://ogre.natalie.mu/media/news/eiga/2018/1121/CCDN0055.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054749 "ZENON SAKABAのSign Wall。")
[![ZENON SAKABAで提供される料理とお酒のImage。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_17.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054750 "ZENON SAKABAで提供される料理とお酒のImage。")
[![ZENON SAKABAで提供されるお酒のImage。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_18.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054751 "ZENON SAKABAで提供されるお酒のImage。")
[![店内に吊るされているシャンデリア。(挂在店内的吊灯。)](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_6.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054752 "店内に吊るされているシャンデリア。")
[![ZENON SAKABAの外観。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_1.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054753 "ZENON SAKABAの外観。")
[![ZENON SAKABAの内装。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_2.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054754 "ZENON SAKABAの内装。")
[![ZENON SAKABAの炭焼場。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_3.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054755 "ZENON SAKABAの炭焼場。")
[![ZENON SAKABAのStage。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_4.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054756 "ZENON SAKABAのステージ。")
[![ZENON SAKABAのMini屋台。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_5.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054757 "ZENON SAKABAのMini屋台。")
[![ZENON SAKABAのStaff。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_8.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054758 "ZENON SAKABAのStaff。")
[![ZENON SAKABAの2階席。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_9.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054759 "ZENON SAKABAの2階席。")
[![「阿蘇あか牛溶岩焼き」（阿苏青牛熔岩烧）](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_11.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054760 "「阿蘇あか牛溶岩焼き」（阿苏青牛熔岩烧）")
[![「天草大王いいとこ焼き」（天草大王好地方烧）](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_12.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054761 "「天草大王いいとこ焼き」（天草大王好地方烧）")
[![「あか牛のローストビーフ」とそのほかの料理。（“红牛烤牛肉”和其他料理）](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_13.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054762 "「あか牛のローストビーフ」とそのほかの料理。（“红牛烤牛肉”和其他料理）")
[![「あまくさ宝牧豚のだご汁」](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_14.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054763 "「あまくさ宝牧豚のだご汁」")
[![料理メニューの集合写真。（料理菜单的集合照片。）](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_15.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054764 "料理メニューの集合写真。（料理菜单的集合照片。）")
[![炭焼焼きのImage。](https://ogre.natalie.mu/media/news/eiga/2018/1121/img_171559_16.jpg?impolicy=thumb_fit&width=180&height=180)](https://natalie.mu/comic/gallery/news/308902/1054765 "炭焼焼きのImage。")

※リョウの漢字はけものへんに「僚」のつくり。

LINK

[吉祥寺 ZENON SAKABA | ゼノンサカバ](https://zenon-sakaba.jp/)  
[zenon sakaba (@zenonsakaba) | Twitter](https://twitter.com/zenonsakaba)  
[CAFE ZENON | カフェゼノン](http://cafe-zenon.jp/)  
[株式会社じぞう屋](http://zizoya.co.jp/)  