source:  
http://hojofancity.free.fr/WorkDisplay.php?v=1&st=1&series=3&choix=0&fm=&status=0&s=783&t=


Fanfiction :: Une journée de folie!

Chapter 1 :: Ce dimanche matin...

Published: 01-06-08 - Last update: 01-06-08

Comments: Bonjour! Voici ma première fic sur F compo! J'espère que cela vous plaira. Je vais essayer de rester dans le ton de ce magnifique manga et peut-être le faire découvrir à ce qui n'aurait pas eut l'occasion de le lire.^^

Le nettoyage de printemps était arrivé pour la famille Wakanaé. Le soleil radieux filtrait à travers les stores de la chambre du jeune Masahiko. Il se passa une main dans ses cheveux sombre en soupirant. Sa chambre était un vrai foutoire! La nuit précédente, les « assistantes » de son « oncle » Sora étaient venues lui faire une visite...Nocturne en quelque sorte...Il s'était réveillé encore une fois entouré de travesties en jupe et barbe qui piquent!Masahiko avait parfois pensé à quitter ce foyer chaleureux pour retrouver une vie un peu plus normale...Mais, sa tante Yukari et son oncle Sora étaient si gentil et il y avait Shion...Ahhhh, Shion....La fille des Wakanaé. Elle était si belle...Masahiko se mit à sourire bêtement en faisant son lit. Shion était un mystère...Il savait que tante Yukari était en réalité un homme et oncle Sora une femme mais, en ce qui concernait Shion...Le doute subsistait! Le jeune homme serra sa couverture entre ses doigts d'un air dépité.  

 

-Arg!Arrête de rêvasser sur une personne dont on ne sait pas si c'est une fille ou un mec!grogna-t-il en balançant la couverture sur le lit.  

 

Il remettait son coussin en place quand il entendit du bruit à sa fenêtre. Il tourna la tête et il vit la tête pendu dans le vide de sa cousine.  

 

-Salut Masahiko! Bien dormis?  

 

Il pinça du nez. Shion était encore venue l'embêter, il en était certain.  

 

-Comme un loir!  

 

Shion haussa un sourcil, ses longs cheveux lui tombant de chaque coté de son jolie visage.  

 

-Ah oui? Il paraît que tu as eut de la visite...  

 

Masahiko grimaça.  

 

-Il ne s'est rien passé si tu veux savoir...  

 

-Oh mais c'est ta vie vie...Tu sais plus rien ne me surprend dans cette famille!!!Si tu aimes les hommes t'en mieux!  

 

Le jeune homme serra les poings.  

 

-Je n'aime pas les hommes!  

 

Shion poussa un petit rire et disparut de la fenêtre. Masahiko soupira une nouvelle fois et se laissa tomber sur son lit d'un air morne. Qu'est-ce qu'elle/il, pouvait l'agacer par moment! Mais...Elle était si sexy!Il se passa une main sur le visage. Il ne devait pas penser à sa cousine de cette manière! C'était peut-être un homme en plus et rien qu'a imaginer ça il en était malade. Il avait déjà du mal à digérer que sa magnifique tante Yukari était un homme...Il la revoyait sur la plage...Avec Shion...Alalalala! Masahiko se redressa vivement. Ses hormones étaient en ébullition se matin! Il devait se calmer!  

 

-Masahiko, Shion à table!!!!lança la voix de Yukari depuis le rez de chaussé.  

 

Le jeune homme enfila une chemise à carreaux par dessus son t-shirt noir et sortit de sa chambre. Il mourrait de faim! Il fut bousculé par Shion qui courrait dans l'escalier pour arriver à table. Il put voir sa jupe virevolter et laisser apercevoir de longue jambes...Il se retint de baver alors que la jeune fille tournait la tête.  

 

-Je te plaît on dirait?lâcha-t-elle en stoppant sa course. Tu veux en voir plus peut-être?  

 

Il leva le menton d'un air désabusé.  

 

-Pourquoi ça m'intéresserait? Je ne vois pas pourquoi je voudrais mater un truc dont on ne sait pas de quel genre il est!  

 

Shion haussa un sourcil en esquissant un sourire.  

 

-Mouai...  

 

Sur ce, elle se remit à courir. Masahiko serra les dents. Évidement que ça l'intéressait! Ça l'obsédait oui! Shion était-elle un homme? Une femme?Cette question tournait en boucle dans son crane...Comme il espérait qu'elle soit une fille....Il arriva finalement à la cuisine. Shion était déjà à table, et sa mère déposait les plats. Masahiko se figea devant la beauté de sa tante. Yukari était très belle ce matin, elle portait une robe à fleur, et ses cheveux tombaient nonchalamment sur ses épaules...Ahhhhhhh!!Mais c'était un homme!!!!Il devait s'en souvenir!  

 

-Où est oncle Sora?demanda-t-il en allant s'asseoir prés de Shion.  

 

Yukari lui déposa un bout d'omelette dans son assiette.  

 

-Il a encore beaucoup de travail à l'atelier. Il doit absolument finir 10 planches pour se soir...  

 

-Dix planches?!s'étouffa presque Mashiko. C'est énorme!  

 

-Oui...Mais Mori ne le lâchera pas tant qu'il n'aura pas fini....Ahhh....Moi qui pensait qu'on aurait put aller se promener à la campagne aujourd'hui' hui....Encore rapé!dit Yukari d'un air triste.  

 

Elle s'assit à son tour alors qu'à l'étage, dans l'atelier, oncle Sora s'évertuait à finir une planche de sa série à succès « Notre emblème ». Ses assistantes étaient toutes là, se tuant à la tache. Une jeune femme était debout, des lunettes sur le nez, l'ai sévère.  

 

-Maître, il faut que vous ayez terminé se soir à 20 heures...dit Mori.  

 

Sora serra les dents en traçant une courbe du bout de sa plume.  

 

-Ça sera prêt! Hein les filles!  

 

Les assistantes travesties levèrent les poings en l'air avec enthousiasme.  

 

-On est les meilleurs!lancèrent-telles en coeur.  

 

Mori opina du chef et se tourna vers la porte.  

 

-Très bien, je reviendrais se soir.  

 

Sur ce elle ferma la porte. Il y eut un silence dans l'atelier et soudain, ils s'écroulèrent tous sur leurs bureau, exténué. Hiromi repoussa une longue mèche de cheveux en soupirant.  

 

-J'ai des crampes dans les doigts....Aie...  

 

-Moi j'ai mal au dos...lança Makoto en grimaçant.  

 

Hiromi esquissa un sourire.  

 

-On aura dut s'abstenir d'aller rendre visite à notre petit Masahiko...J'ai mal aux rotules maintenant...  

 

De son coté, Sora soupira. Il avait promit à Yukari qu'ils iraient tous faire un tour à la campagne aujourd'hui car on était dimanche mais évidement...Il avait trop de boulot, encore...  

 

-Bon, les filles!lança-t-il en reprenant du poil de la bête. Au boulot!Si on pouvait finir avant la nuit ça m'arrangerait...  

 

Il soupira puis se remit au travail.  







Chapter 2 :: Bizare

Published: 02-06-08 - Last update: 02-06-08

Comments: voici mon deuxième chapitre, je sais c'est vpas long mais j'essairai de les aire plus long la prochaine fois. J'ai tenté d'expliqué un peu l'histoire...^^


Le téléphone sonna. Yukari se leva de table et décrocha.  

 

-Oui? Oh...Bonjour...Oui...  

 

Masahiko haussa un sourcil en enfournant une bouchée d'omelette. Il vit sa tante décoller le combiné de son oreille et dire:  

 

-Masahiko c'est pour toi.  

 

Le jeune homme se leva et prit le téléphone.  

 

-Oui? Ah...Sempai!  

 

Le directeur du club cinéma de l'université leva un doigt en l'air.  

 

-Masami, on a besoin de toi!  

 

Le jeune homme serra les dents. Ça n'allait pas recommencé! Il n'avait aucunement l'intention de se déguiser en femme aujourd'hui!  

 

-Il n'y aura pas de Masami aujourd'hui, Sempai! Hors de question!  

 

Masahiko grimaça. Depuis qu'il avait joué le rôle d'une jeune fille dans le film du club ciné, les autres n'en pouvaient plus! C'était « Masami » par-ci, « Masami » par là...Il était un homme à la fin!  

 

-Allez, yanagiba!Quoi, soit sympa!On veut tourner un cous métrage sur Masami..Histoire de rameuter du monde pour la projection de la semaine prochaine!  

 

Le jeune homme secoua la tête.  

 

-Non! Non et Non! J'ai autre chose de prévus en plus aujourd'hui...  

 

-Ah oui?lâcha le directeur, sa casquette vissée sur sa grosse tête.  

 

Masahiko grimaça. Qu'allait-il trouver comme excuse? Il allait dire quelque chose quand Shion se posta prés de lui et lança:  

 

-Masahiko doit me donner des cours particuliers!  

 

Le garçon haussa un sourcil. Des cours particuliers? Intéressant...Il serra les dents. C'était pas le moment de rêvasser!  

 

-Oui..C'est ça!  

 

A l'entente de la voix de Shion, le garçon posté prés du directeur du club bondit sur ses pieds.  

 

-On peut venir!lança alors Ejima.  

 

Masahiko posa une main sur son visage. Manqué plus que lui! Ejima, son pote de fac qui voulait absolument se faire Shion...  

 

-Plus on est de fou plus on ...Étudies...continua Ejima qui s'imaginait déjà entrain de peloter Shion Wakanaé.  

 

-T'as pas interré à venir Ejima! pesta Masahiko qui savait déjà à quoi pensait son ami.  

 

Mais il avait déjà raccroché. Masahiko sentit une goutte rouler sur sa tempe. Il sentait le plan foireux. Ils allaient tous se ramener histoire de pouvoir approcher Shion...Il soupira et retourna à table. Yukari but un verre d'eau et dit:  

 

-Des amis vont venir?  

 

-Non, tante Yukari...  

 

Il l'espérait...Il n'avait pas envi que les autres débarquent et découvre des choses qu'ils ne devraient pas savoir...Comme par exemple que Sora et Yukari étaient des travesties, qu'ils avaient échangé leurs rôle de mari et femme. D'aspect on ne pouvait absolument pas le deviner...Sora faisait réellement homme et Yukari avait la grâce et la douceur d'une vrai femme...Mais une fois nu..C'était autre chose... Rien que de revoir ces images, il avait envi de crever sur place!Il avait surpris tante Yukari sous la douche...Ahhh! Quel horreur!  

Quelqu'un arriva soudain dans la cuisine. Kaoru entra en se passant une main dans ses cheveux roux.  

 

-Salut...marmonna-t-il en se jetant sur une chaise.  

 

Masahiko leva les yeux vers le jeune homme qui portait une veste militaire et un baggy. Il se demandait souvent comment Kaoru serait si il décidait de reprendre sa vrai place...A savoir redevenir une jeune fille...Car Kaoru était une jeune fille, fille d'un Yakuza appelé Tatsumi. Cela-dit, elle avait tout du mec, elle parlait mal, jurait...Elle était insupportable. Shion esquissa un sourire.  

 

-Eh, Kaoru ça te dirait de venir faire les boutiques avec moi et Masahiko après mangé?  

 

Masahiko haussa un sourcil. Depuis quand avait-il dit qu'il allait faire les boutiques?  

 

-Moi aller faire les boutiques? Tu m'a bien regardé? lâcha Kaoru.  

 

-Ça serait amusant! En plus c'est maman qui paît!  

 

Yukari haussa les sourcils.  

 

-Contente de l'apprendre!  

 

Shion tourna la tête vers sa mère.  

 

-Allez maman...J'ai pus rien à me mettre!  

 

Yukari soupira.  

 

-D'accord...Mais ne dévalise pas les boutiques! Sinon ton père sera furieux!  

 

La jeune fille sourit.  

 

-Ne t'inquiètes pas maman!  

 

Mashiko soupira en levant les yeux vers Kaoru qui semblait pensive.  

 

-OK, je viens mais uniquement si on va là où je veux aller!  

 

-Pas de problème! lança Shion,l'air doux.  

 

Masahiko regarda Shion et Kaoru tour à tour. Il sentait qu'il allait arriver quelque chose de désagréable...Pour lui, comme toujours...  







Chapter 3 :: Travesties!

Published: 08-06-08 - Last update: 08-06-08

Comments: Coucou à mes deux lectrices!^^...Je me suis rendus compte que les lecteurs ne sont pas au-rendez vous pour F compo mais au moins j'ai mes deux plus fidèles lectrices! RKever et Koari4ver et ça ça me fais plaisir! Gros bisou à vous deux!!!

Shion pointa le doigt vers une boutique à la mode. Elle alpagua Masahiko et Kaoru puis il pénétrèrent dans le magasin. Masahiko soupira. Il s'ennuyait à mourir. C'était pas son truc d'aller faire les boutiques. Il regarda les rayons de vêtements d'un air morne puis il vit Kaoru et Shion s'enfermer dans des cabines. Il décida d'aller les rejoindre et de les attendre. Il se posa sur un fauteuil et leva les yeux vers le rideau qui cachait Shion. Il se demandait bien ce qu'elle avait put choisir comme habits...Un maillot de bain peut-être? Il soupira. Pourquoi fallait-il qu'il fantasme ainsi sur une personne au sexe inconnue?!A vrai dire pour lui, Shion ne pouvait être qu'une fille...Elle avait tout d'une fille mais..Avec des parents comme les siens..Comment être sur? Yukari ressemblait même à une femme en maillot de bain alors...Shion sortit enfin de la cabine habillée en homme! Elle avait relevé ses long cheveux en une queue de cheval mal faite, elle portait un baggy et un large switt à capuche. Masahiko tomba à la renverse.  

 

-Ça te plaît?lança Shion en se plantant devant lui.  

 

Le jeune homme se redressa. Non! Il ne l'aimait pas habillé en homme! Il n'aimait pas non plus le fait que Shion change de sexe comme de chemise! Depuis sa plus tendre enfance, Shion passait de statut de garçon et de fille d'une année sur l'autre. Elle avait même aimé une fille lorsqu'elle avait passé une année de primaire en garçon!Et elle avait aimé un garçon qui était devenu l'une des assistante travestie de Sora! Shion avait maintenant décidé d'aller à la fac en garçon...Idée qui ne plaisait pas du tout à Masahiko. Il ne comprenait pas comment on pouvait être aussi indécis sur sa propre nature...Lui, il était un homme est était fier de l'être!  

 

-Non.  

 

Shion soupira.  

 

-Allez quoi...T'es vraiment pas drôle...  

 

-Pas drôle? Attends...T'es sensée acheter des vêtements de fille! Je te signal que tes parents ne sont pas au courant que tu vas à la fac en garçon!  

 

-Et alors? Où est le problème! On dira que ces vêtements sont pour toi!  

 

Un corbeau passa derrière la tête de Masahiko. Cela voulait-il dire qu'il n'aurait pas de vêtements neufs?  

 

-C'est pour ça que tu voulais que je vienne hein? Pour avoir une excuse d'avoir acheter des fringues de mec...  

 

Shion lui fit un clin d'oeil.  

 

-Mais non! Je voulais juste passer un peu de temps avec mon « petit frère »!se moqua-t-elle gentiment.  

 

-Ne m'appelle pas comme ça!pesta Masahiko.  

 

Shion lui sourit puis elle entendit le rideau de la cabine de Kaoru s'ouvrir. Elle portait un jean large avec une chemise blanche.  

 

-Ça te vas bien, dit Shion.  

 

Kaoru haussa les épaules.  

 

-Ouais...  

 

Shion claqua soudain des doigts. Elle venait d'avoir une idée!  

 

-Eh! Et si tu t'habillais en fille Kaoru?  

 

-Hein?! Tu délires toi!pesta l 'autre en grimaçant.  

 

Masahiko esquissa un sourire.  

 

-Eh! C'est une bonne idée! J'aimerais bien voir comment t'es en fille...  

 

Kaoru tourna la tête vers lui.  

 

-Me regarde pas avec ton air pervers!  

 

Mashiko sembla vexé par cette remarque et grimaça. Il releva le menton d'un air mécontent. Il n'était pas un pervers! Ejima par contre...  

Shion leva les mains en l'air en se plaçant devant Kaoru.  

 

-On s'est tous au moins travestie sauf toi! Aurais-tu peur de te voir en fille?  

 

Kaoru lâcha un coup d'oeil agacé à Shion.  

 

-Peur?  

 

-Oui...Moi je n'ai aucun problème à passer de garçon à fille...Que se soit physiquement que mentalement...Et toi?  

 

Cette phrase sonna aux oreilles de Kaoru comme un défis. Elle tourna la tête vers Masahiko qui était toujours en train de bouder et dit:  

 

-Je m'habille en fille si lui se déguise aussi!  

 

Masahiko reporta son attention sur elles et secoua la tête.  

 

-Hors de question! C'est votre problème le travestissement pas le mien!  

 

Shion haussa un sourcil.  

 

-T'es pourtant mieux en fille qu'en gars...Y a qu'a voir les émeutes que provoque Masami...  

 

-Oui...C'est vrai mais...Moi je n'ai pas de problème d'identité!  

 

Kaoru et Shion lancèrent des coups d'oeil meurtrier à Masahiko.  

 

-Des problèmes d'identités?!pestèrent-elles en coeur.  

 

Le jeune homme opina du chef.  

 

-Moi je n'ai pas le besoin de me déguiser pour me sentir normal...  

 

Shion serra un poing, d'un air menaçant.  

 

-Je n'ai pas de problème d'identité! Tu veux qu'on règle ça entre homme!?  

 

Masahiko lança un regard dépité à sa cousine. Entre hommes? Elle était sérieuse?  

 

-Je vais te faire ta faite! cracha alors Kaoru en retroussant ses manches de chemise.  

 

Le jeune home leva les mains en signe de soumission.  

 

-Non mais je voulais pas dire que vous étiez pas normal mais juste que...  

 

-Je te ferais dire que nous au moins on passe pour des hommes...Alors que toi avec ton air efféminé...rétorqua Shion en arborant au air mauvais.  

 

Masahiko grimaça en se regardant dans un miroir. Il avait le trait fin certes et il était pas épais mais enfin! Ça se voyait qu'il était un garçon!  

 

-C'est vrai ça! On fait certainement plus homme que toi!renchérit Kaoru fièrement. Moi j'emballe grave et toi, Masahiko?  

.  

La tête du jeune homme s'enfonça dans ses épaules. Il devait avouer que coté conquêtes féminines s'était pas la fête...Il était sortis avec Yoko plusieurs mois mais...Elle avait fini par rompre il y avait de ça quelques semaines car elle avait sentit qu'il aimait Shion et pas elle...Masahiko avait pleurer se soir là...Et il s'était déclarer à Shion. Elle n'avait pas vraiment répondu en sa faveur mais Shion savait maintenant que Masahiko l'aimait et il avait enfin agis en homme. Il sentit la main de Shion se poser sur son épaule et il releva la tête.  

 

-Allez Masahiko...  

 

Il vit dans les yeux de sa cousine de la tendresse et il soupira.  

 

-Ok mais juste ici...après on redevient nous hein?!  

 

Kaoru et Shion s'échangèrent un regard complice.  

 

-Oui, oui!dirent-elles en coeur.  

 

Shion alla donc choisir des vêtements qu'elle tendit à Kaoru et Masahiko et ensuite ils allèrent s'enfermer dans les cabines...  
 
 




Chapter 4 :: Kaoru ressemble à une fille!?

Published: 09-06-08 - Last update: 09-06-08

Comments: Coucou mes fidèles lectrices! et voila un chapitre tout chaud!enjoy!!!

Le rideau s'ouvrit sur Masahiko et Shion sourit de toutes ses dents. Il portait une jupe en jean, une chemise rose et des chaussure à petite talon. Le jeune homme affichait un air morne, se sentant encore une fois ridicule. Il avait l'impression d'être une poupée dans les mains de tous ces travesties. Shion s'avança vers lui et l'inspecta des pieds à la tête.  

 

-Pas mal...Il faudrait qu'on te maquille un peu...dit-elle en lui ébouriffant les cheveux sur la tête.  

 

Masahiko soupira, las, alors que des gens passaient devant les cabines. Il entendit une jeune fille dire: « Oh...Comme ils sont beau tous les deux... » et sa copine de renchérir: « Le garçon est pas mal... ». Le jeune homme sembla ravis de voir qu'il plaisait mais réalisa vite qu'elles parlaient de Shion. Une goutte s'éleva au-dessus de sa tête et il grimaça.  

 

-C'est bon? Je peux me changer?  

 

-Non! On attends Kaoru! Alors qu'est-ce que tu fabriques!?s'exclama Shion en se plantant devant la cabine où se changeait la fille.  

 

-Je sortirais pas!lança alors Kaoru derrière le rideau.  

 

 

Shion posa ses mains sur ses hanches.  

 

-Et pourquoi donc?!  

 

-Je veux pas avoir la honte!  

 

-Masahiko s'est déguisé lui! Tu va pas te rabaisser devant lui?!  

 

-Il a l'habitude d'être ridicule!rétorqua Kaoru.  

 

Masahiko haussa un sourcil en croisant les bras sur sa chemise rose.  

 

-Ehh!!!pesta-t-il.  

 

Shion grimaça en levant les yeux au ciel d'un air contrit. Elle soupira et ouvrit d'un grand coup le rideau. Elle découvrit alors Kaoru qui avait mit une robe noire et des sandales...Elle était superbe! La jeune fille fronça les sourcils en voyant que Shion s'était aventurée dans la cabine.  

 

-Refermes ça!!pesta-telle en l'intimidant de son poing fermé.  

 

De l'autre coté, Masahiko se mit sur la pointe de pieds pour voir la dégaine de Kaoru. Il avait du mal à l'imaginer en fille...Il repensa soudain à la fois où il était déguisé en Masami et que Kaoru l'avait embrassé...A ce moment là, il ne savait pas que Kaoru était un réalité une fille. Il rougit presque puis il vit Shion refermer le rideau alors qu'elle était entrée dans la cabine.  

 

-Qu'est-ce qu'elles fabriquent? souffla-t-il, se sentant soudain bien seul, entouré de garçons qui le scrutait d'un œil avide.  

 

Masahiko grimaça et alla s'assoir sur le fauteuil, non sans serrer les cuisses...  

 

Dans la cabine, Shion écarquilla les yeux.  

 

-T'es super belle Kaoru!  

 

La fille grimaça, mal à l'aise.  

 

-Arrêtes tes conneries...  

 

-Je suis sérieuse!  

 

Kaoru tourna la tête vers le miroir et grimaça. Cela faisait tellement longtemps qu'elle ne s'était pas habillée en fille qu'elle avait presque oublié à quoi elle ressemblait.  

 

-J'ai l'air empotée habillé comme ça...Laisse tomber la te-hon! Si les membres de mon groupe me voit attifée comme ça...Ils vont me larguer c'est sur!  

 

-Ils sont tous aveugle,répliqua Shion en haussant un sourcil.  

 

Kaoru grimaça.  

 

-Mais si mon père...  

 

-Ton père n'attends que ça de te voir redevenir une fille!  

 

Kaoru foudroya Shion du regard.  

 

-T'as réponse à tout!?  

 

-Oui.  

 

Kaoru soupira.  

 

-Ok..Je sors mais si Masahiko se fout de moi, je te tiendrais pour responsable!  

 

-Parce que l'avis de Masahiko t'importe maintenant?  

 

Kaoru ne broncha pas. Shion avait quelque doute sur Kaoru. Elle la suspectait d'avoir certains sentiments pour Masahiko. Mais la jeune fille s'en défendait vivement. Kaoru ouvrit alors le rideau et quitta la cabine. Les cheveux dans les yeux, la tête baissée, elle se planta là, sous les spots, l'air bête. Masahiko qui attendait assis sur le fauteuil, poussa des yeux ronds. Kaoru était vraiment mignonne en fille! Et c'était bien une fille! Il se redressa, presque hypnotisé. La jeune fille leva la tête vers lui et fronça les sourcils en voyant l'air abrutit de Masahiko.  

 

-Qu'est-ce que t'as à me mater comme ça!?pesta-t-elle en serrant les poings. T'en veux une?  

 

Il leva les mains comme pour se défendre.  

 

-Non...C'est juste que...Wouah! T'es très jolie comme ça!  

 

Kaoru se figea. Elle afficha un air dépité.  

 

-Tu me trouves jolie?  

 

-Oui, vraiment.  

 

Kaoru ne savait pas si elle devait accepter ce compliment ou au contraire s'énerver. Elle ne voulait pas être une fille! Son but, était d'être aussi homme que Sora!  

 

-Oui bah rêve pas! Je me rhabille normalement!  

 

Elle se détournait vers la cabine, quand Shion lui barra le chemin.  

 

-Tu fais quoi là?  

 

-J'ai eu une idée!dit Shion.  

 

Masahiko sentit un corbeau passer derrière lui.  

 

-Encore un super idée je paris...  

 

Shion leva un index en l'air.  

 

-On pourrait sortir comme ça et se balader pour voir si on plait!  

 

Kaoru afficha un air stupéfait puis serra les dents.  

 

-Il est hors de question que je sortes déguisée comme ça!  

 

Masahiko sembla réfléchir.  

 

-Mais en y pensant bien..Tu n'es pas déguisée Kaoru..Être fille c'est ta vraie nature non?  

 

Kaoru détourna la tête vers lui et lui lança un coup d'oeil terrible. Le jeune homme grimaça, en baissant la tête dans ses épaules.  

 

-J'ai rien dit...  

 

Shion soupira.  

 

-Tu ne deviendra jamais vraiment un homme si tu n'accepte pas ta part féminine.  

 

Kaoru lui lâcha un coup d'oeil morne.  

 

-Qu'est-ce que tu racontes comme conneries?  

 

Shion haussa les épaules.  

 

-Mon père à un corps de femme...Il l'assume malgré tout. Et toi? As-tu assez de force pour accepter ton corps?  

 

Kaoru fronça les sourcils.  

 

-Je vois pas pourquoi je devrais me pavaner en fille! Je suis un mec!  

 

-C'est juste que t'as peur de te rendre comptes que tu es vraiment une fille...T'aurais pas honte sinon...  

 

Kaoru serra les dents et jeta un regard à Masahiko qui attendait en silence. Shion se pencha à l'oreille de la fille, un air amusé sur le visage.  

 

-Regarde bien Masahiko. Quand il est déguisé en fille, il en devient une...Personne ne peut deviner qu'en réalité il est un homme. Moi non plus...Et toi? Souviens toi que mes parents ont tout de suite vu que tu étais en réalité un fille...Tu voudrais pas pour une fois, prouver que tu peux vraiment devenir quelqu'un d'autre? Tu es un homme...Deviens une femme alors.  

 

Kaoru grogna, une goutte de sueur roulant sur sa tempe. Elle sembla réfléchir puis soupira.  

 

-Ok...Mais c'est la première et dernière fois que je fais ça!  

 

Shion sourit en fourrant ses mains dans ses poches.  

 

-Je vais sortir avec deux jolies filles..Je suis le plus heureux des mec!  

 

Kaoru et Masahiko tombèrent à la renverse. Tout ce cirque s'était pas pour se pavaner en ville et passer pour un tombeur plutôt?  


