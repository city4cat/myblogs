'''
cd /home/dev/family-compo-asset/family-compo-asset-utility
python3 ./fc_scale_image.py 


'''
import errno
import glob
import os
import shutil
import sys

import util_logging
import util_common


def glob_filter(source_dir, include_filters, exclude_filters=None):
    '''
    glob_filter('/a/b/c', '/d/e/f', ['*.h'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*.h', '*.py'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*'], '*.pyc')
    glob_filter('/a/b/c', '/d/e/f', '*', ['*.pyc', '*.log'])
    '''
    if include_filters is None:
        include_filters = []

    if exclude_filters is None:
        exclude_filters = []

    include_files = []
    for filter_ in include_filters:
         include_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('include_files=%s', ' '.join(include_files))

    exclude_files = []
    for filter_ in exclude_filters:
        exclude_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('exclude_files=%s', ' '.join(exclude_files))

    files = list(set(include_files) - set(exclude_files))
    #log('files=%s', ' '.join(files))
    return files

def get_immediate_subdirs(a_dir):
    sub_archives = []
    try:
        sub_archives = os.listdir(a_dir)
    except OSError as e:
        context = ' os.listdir(%s). '%a_dir
        if e.errno == errno.ENOENT:
            raise RuntimeError('No such file or directory. ' + context)
        else:
            raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)

    return [name for name in sub_archives if os.path.isdir(a_dir+'/'+name)]

#
def test_gather_files(root_dir, include_filters, exclude_filters):
    util_logging.info('>> test_gather_files(%s, )', root_dir)

    root_dir = root_dir.replace('\\','/')
    
    if root_dir[-1] != '/':
        root_dir += '/'
    
    filepath_list = glob_filter(root_dir, include_filters, exclude_filters)
    filepath_list.sort()

    util_logging.info('>> %d found:', len(filepath_list))
    #for filepath in filepath_list:
    #    util_logging.info('%s', filepath)
    util_logging.info('<< %d found.', len(filepath_list))
    #util_logging.info('\n')
    #for filepath in filepath_list:
    #    do_statistics(filepath)

    # process subdirectories
    #subdirs = get_immediate_subdirs(root_dir)
    filepath_list_in_subdirectory = []
    sub_archive_list = glob.glob(os.path.join(root_dir, '*'))
    for sub_archive in sub_archive_list:
        if sub_archive in [os.path.join(root_dir, exclude_filter) for exclude_filter in exclude_filters ]:
            util_logging.info('exclude: %s', sub_archive)
            continue
        if os.path.isdir(sub_archive):
            filepath_list_in_subdirectory += test_gather_files(sub_archive, include_filters, exclude_filters)
            
    return list(set(filepath_list + filepath_list_in_subdirectory))

#
def print_list(list_data, msg='', show_details=True):
    util_logging.info('print list:(%s)'%(msg))
    util_logging.info('>> length: %d', len(list_data))
    
    if show_details is True:
        for filepath in list_data:
            util_logging.info('%s', filepath)
        
    util_logging.info('<< length: %d', len(list_data))
    util_logging.info('\n')

       
#  
def exclude_files_by_prefix(filepath_list, prefix_list):
    util_logging.info('>> exclude_files_by_prefix(, %s)', prefix_list)
    
    included_list = []
    excluded_list = []
    for filepath in filepath_list:
        basename = os.path.basename(filepath)
        
        exclude = False
        for prefix in prefix_list:
            if basename.startswith(prefix) == True: # exclude this item
                excluded_list.append(filepath)
                exclude = True
                break
       
        if exclude == False:
            included_list.append(filepath)
               

    return included_list, excluded_list


def exclude_dir(filepath_list, exclude_pattern_list):
    util_logging.info('>> exclude_dir(, %s)', exclude_pattern_list)
    
    included_list = []
    excluded_list = []
    for filepath in filepath_list:
        dirname = os.path.dirname(filepath)
        
        exclude = False
        for wd in exclude_pattern_list:
            if wd in dirname: # exclude this item
                excluded_list.append(filepath)
                exclude = True
                break
       
        if exclude == False:
            included_list.append(filepath)
               

    return included_list, excluded_list
    
    
    
    
