import os
import logging

LOG_LEVEL_TEST          = 15
LOG_LEVEL_TEST_NAME     = 'TEST'
LOG_LEVEL_TRACE         = 5
LOG_LEVEL_TRACE_NAME    = 'TRACE'


gLogger = None

def test_memfun(self, msg, *args, **kwargs):
    if self.isEnabledFor(LOG_LEVEL_TEST):
        self._log(LOG_LEVEL_TEST, msg, args, **kwargs)


def trace_memfun(self, msg, *args, **kwargs):
    if self.isEnabledFor(LOG_LEVEL_TRACE):
        self._log(LOG_LEVEL_TRACE, msg, args, **kwargs)


def trace_fun(msg, *args, **kwargs):
    """
    Log a message with severity 'TRACE' on the root logger.
    """
    if len(logging.root.handlers) == 0:
        logging.basicConfig()
    logging.root.trace(msg, *args, **kwargs)


def add_log_level(gLogger):
    logging.addLevelName(LOG_LEVEL_TRACE, LOG_LEVEL_TRACE_NAME)
    logging.addLevelName(LOG_LEVEL_TEST,  LOG_LEVEL_TEST_NAME)

    logging.Logger.trace = trace_memfun
    logging.trace = trace_fun

    gLogger.trace = trace_memfun
    gLogger.test  = test_memfun

def setup():
    global gLogger
    #assert gLogger is None, 'gLogger should be None'
    if not 'AUTOMATION_LOG_LEVEL' in os.environ:
        os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'

    log_level_name = os.environ['AUTOMATION_LOG_LEVEL']

    # We should initialize log system as early as possible, but I have to feed 'args.loglevel' to the log system.


    formatString = '%(levelname)+8s| %(message)s'

    #logging.basicConfig(format='%(module)+'+str(at_common.gLogPadNumber)+'s| %(levelname)+8s| %(message)s',
    #                    level=logging.getLevelName(log_level_name))
    #logging.basicConfig(format=formatString, level=logging.getLevelName(log_level_name))



    formatter = logging.Formatter(formatString)
    gLogger = logging.getLogger('automation')
    gLogger.propagate = False
    add_log_level(gLogger)
    gLogger.setLevel(logging.getLevelName(log_level_name))

    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    ch.setLevel(logging.getLevelName(log_level_name))
    gLogger.handlers = []
    gLogger.addHandler(ch)


# ==========================================================================
# The following functions are similar with logging module.
# Usage:
# import at_logging
# at_logging.fatal('Test fatal message.')
# at_logging.error('Test error message.')
# at_logging.warn('Test warn message.')
# at_logging.info('Test info message.')
# at_logging.debug('Test debug message.')
# ==========================================================================
def isLogLevel(log_level_name):
    global gLogger
    if gLogger.getEffectiveLevel() == logging.getLevelName(log_level_name):
        return True
    elif gLogger.getEffectiveLevel() == logging.getLevelName(logging.getLevelName(log_level_name)):
        return True
    else:
        return False


def critical(msg, *args, **kwargs):
    """
    Log a message with severity 'CRITICAL' on the root logger.
    """
    global gLogger
    if gLogger is None:
        setup()
    gLogger.critical(msg, *args, **kwargs)

fatal = critical

def error(msg, *args, **kwargs):
    """
    Log a message with severity 'ERROR' on the root logger.
    """
    global gLogger
    if gLogger is None:
        setup()
    gLogger.error(msg, *args, **kwargs)

def exception(msg, *args, **kwargs):
    """
    Log a message with severity 'ERROR' on the root logger,
    with exception information.
    """
    error(msg, *args, **kwargs)

def warning(msg, *args, **kwargs):
    """
    Log a message with severity 'WARNING' on the root logger.
    """
    global gLogger
    if gLogger is None:
        setup()
    gLogger.warning(msg, *args, **kwargs)

warn = warning

def info(msg, *args, **kwargs):
    """
    Log a message with severity 'INFO' on the root logger.
    """
    global gLogger
    if gLogger is None:
        setup()
    gLogger.info(msg, *args, **kwargs)

def debug(msg, *args, **kwargs):
    """
    Log a message with severity 'DEBUG' on the root logger.
    """
    global gLogger
    if gLogger is None:
        setup()
    gLogger.debug(msg, *args, **kwargs)

trace = debug
