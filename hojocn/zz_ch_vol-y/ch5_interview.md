[hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96966)  

（译注：以下被"「」"包裹的段落应该为北条司的话，其余段落为采访者的话。）  


## City Hunter Complete Edition, Volume Y
### Chapter:5 FAN SQUARE(1color) Interview & Favorite  （译注：2004年02月）
### 北條司 ーその尽きない魅力ー  （北条司 无尽的魅力）  

「シティーハンター完全版」刊行記念インタビュー!!  
为纪念「City Hunter 完全版」的出版而进行的采访!  

大ヒット作を連発しながらも、いまだ衰えぬモチベーシヨン。完全版の発売に際し「ダ・ヴィンチ」誌上で自ら語った作品に込めた想いを再収録！！  
尽管有一连串的大动作，但积极性仍未减弱。 在完全版发行之际，他向「Da Vinci」杂志讲述了自己对这部作品的想法，现在重新收录了这些想法!    


#### 帰ってきた シティーハンター  (City Hunter 归来)

北条司といえば、『キャッツ・アイ』『シティーハンター』と立て続けにヒットを飛ばし、1980年のデヒュー以来、20年を超える歳月をマンガ界の第一線で走り続けてきた作家だ。現在連載中の『エンジエレ・ ハート』も、最新刊の発行部数が70万部を超えるなど、その勢いはいまだ衰えることを知らない。  
北条司是一名漫画家，自1980年在『Cat's Eye』『City Hunter』系列中首次亮相以来，20多年来一直处于漫画界的一线。他目前的连载『Angel Heart』的最新版本已售出70多万册，而且势头没有减弱的迹象。   

その『エンジェル・ハート』の基となった『シティーハンター』が帰ってきた。2003年の12月から徳間書店より、完全版の単行本が刊行開始されたのだ。『シティーハンター』の連載開始は1985年。その前に読切版が掲載されて好評を博し、その後連載化という経緯を持っている。  
『Angel Heart』所依据的『City Hunter』于2003年12月回归，德间书店开始以单行本的形式出版完全版。 『City Hunter』的连载开始于1985年。 在此之前，该系列作品以短篇读物的形式出版，受到好评，后来成为一个连载系列。  

「この作品を描き始めたきっかけは、『キャッツ・アイ』に出てくる"ねずみ"というキャラクター。自分でも気に入っていたキャラで、彼を主人公にして何か描きたいなと思ってたんです。そこで読切作品で、こいつに銃でも持たせてみるかとなった。そしてスィーパー(始末屋)でいこう、美女をどんどん出そうと(笑)」  
 「我开始画这个作品是因为『Cat's Eye』中的人物"老鼠"。 我喜欢这个人物，想画一些以他为主角的作品。 所以我决定在一个短篇小说中让他持枪。 然后我决定让他当Sweeper(清道夫)，把美女越来越多地带出来（笑）」  


そして後にアニメ化され、今もなお多くのファンから愛され続けている名作に成長していくのだ。そして2001年のコミックバンチ創刊と共に始まったのが最新作『エンジェル・ハート』である。これは『シティーハノンター』のキャラクターが織りなす北条司の新たな挑戦だ。  
它后来被改编为动画片，并成长为一部杰作，至今仍受到许多粉丝的喜爱。 最新作品『Angel Heart』始于2001年推出的『Comic Bunch』。 这是北条司的一个新挑战，人物来自『City Hunter』。 

「『シティーハノンター』はまた描きたいと思っていたんです。『シティーハンター』連載終了を知らされてから終了まであまり間がなかったので、どこか終わりきっていないという想いを引きずっていたんです」  
「我一直想再画『City Hunter』。 从『City Hunter』连载结束到这个连载结束之间没有多少时间，所以我一直坚持认为它还没有完全完成。」（译注：待校对）  


そんな想いから始まつた『エンジェル・ハート』だが、単純な続編モノとはならなかった。主人公についても、前作の冴羽獠ではなく、彼のパートナーにして最愛の人であった香の心臓を移植された少女・香瑩となっている。  
『Angel Heart』一开始就考虑到这一点，但它不是一个单纯的续集。 主角不是上一部电影中的冴羽獠，而是香瑩，一个被移植了他的搭档和最爱的香的心脏的女孩。   

「『シティーハンター』では、登場人物たちが律儀に年をとつていたし、時間も経ちすぎていたのでそのままだと続きを描けないと考えたんです。だから単純な続編ではなく、80年代の『シティーハンター』は無視して、2000年のモノを描こうとしたんです」  
「在『City Hunter』中，人物已经变老，时间也过得太长，所以我认为不可能按原样继续讲故事。 因此，我决定不做单纯的续集，而是忽略80年代的『City Hunter』，从2000年开始画一些东西。」


前作のヒロインだった香が物語から早々に退場してしまうことについては、読者としても衝撃が大きかった。  
前一个故事的女主角香过早地退场，作为读者的我们都震惊了。  

「香が出ないことについては、開始直後はいろいろいわれましたね。でも僕としては、むしろこの作品では香というキャラを目立たせたかったんです。それで香の子供を出そうと思ったけど、子供と香のキャラクターがダブってしまうのはまずい。それならば子供と香を一緒にしちゃおうと。そうすることによって、『香は死んでしまっているけれど、かえって香の生き方とか存在が浮かび上がる』というふうにしたんです」  
「很多人在作品开始后就说了很多关于香没有出现在作品中的事情。 但对我来说，我想让香这个角色在这部作品中脱颖而出。 这就是为什么我想讲她的孩子，但如果孩子和香的角色双双出现，那就不好了。然后我决定把孩子和香放在一起。 通过这样做，我使『香已经死了，但她的生活方式和存感却凸显出来』。 」  

『エンジェル・ハート』はスイーパーものとしての色合いが強かった『シティーハンタ一』に対し、人と人とのつながりに焦点を絞った家族の物語としての色が濃くなっているが、北条作品では「家族」というものを描くことが多い。初連載作品の「キャッツ・アイ』も美人怪盗3人娘を中心とした家族の物語となっていた。  
与『City Hunter』相比，『Angel Heart』更像是一个清道夫的故事，它更注重人与人之间的联系，但北条的作品经常描写 「家庭」。 他的第一部连载作品『Cat's Eye』也是一个以美女怪盗三姐妹为中心的家庭故事。  

「『キャッツ・アイ』は学生時代の友達と話し合っているうちに、泥棒家族というアイデアが出てきたんです。父と長男が刑事で、母と娘が泥棒。それでお母さんがべッドでお父さんから捜査の情報を引き出すっていう。でもそれだと少年誌では描けないので、3人娘にしたんです。でも結局は家族なんですよね。最後は3人娘の父親を探す話になっちゃいましたし」  
「我在与学校的一个朋友讨论『Cat's Eye』时想到了一个小偷家族的想法。 父亲和长子是侦探，而母亲和女儿是小偷。母亲从父亲那里得到了调查的信息。 但这不可能在一本少年杂志中描绘出来，所以我们决定用3个女子。 但归根结底，她们是一个家庭。 最后，它变成了一个关于三个女儿寻找父亲的故事。」（译注：待校对）  

『シティーハンター』の連載が終了した後、北条司は青年誌で『ファミリー・コンポ』という家族モノの作品を描いている。こちらは、父が女の格好、母が男の格好をした不思議な一家に主人公が居候をするという内容で、前作『シティーハンター』と比べると趣が大いに異なる作品だ。    
在『City Hunter』的连载结束后，北条司在一本青年杂志上画了一个名为『Family Compo』的家庭故事。 在这部作品中，主人公与一个神秘的家庭住在一起，父亲打扮成女人，母亲打扮成男人，这与他之前的作品『City Hunter』有很大不同。  

「正直なところドンパチを描くのに飽きちゃってたんですね。『キャッツ・アイ』『シティーハンター』と続いて、イメージが固まっていた。でも僕としては、10年間ドンパチ描いてヒットしたんだから、ここらでちょっと自由にやってみてもいいかなと思うようになったんです」  
「说实话，我已经厌倦了打打杀杀的角色。 在『Cat's Eye』和『City Hunter』之后，我有一个固定的形象。 但对我来说，在画打打杀杀十年之后，我觉得是时候尝试一下更自由的东西了。」（译注：待校对）      

その『ファミリー・コンポ』の後に描かれた『エンジェル・ハート』は、スイーパ一ものと家族ものという北条作品の2つの要素がバランスよく融合した、総決算的作品といえるかもしれない。  
『Angel Heart』紧随『F.Compo』之后，可以被看作是北条作品中2个元素的均衡融合的巅峰，即清道夫和家庭元素的融合。    

「家族を描くことが多いというのは、他人から指摘されて初めて気づいたんですよ。意識して描こうとしているわけじゃないんだけど、描いているうちに自然と出てきちゃうんでしようね。僕は高校生のときに父親がガンを発症し亡くしているのですが、それが影響しているのかもしれません」  
「我没有意识到我经常画家庭相关的作品，直到其他人向我指出了这一点。 我没有有意识地去画这些主题，而是在画的时候自然而然地出来。 我在大学时父亲因癌症去世，这可能对我产生了影响。」     

たとえ殺し屋を主人公としていても、アクションだけでなく家族の温もりを常に描き続けているので、北条作品はけして殺伐としたものとならない。優しさと人情味を内に秘めたカッコ良さに、読者はシビれるのだ。  
即使主人公是一名清道夫，北条的作品也从来不是阴暗的，因为他不仅不断地描绘杀手行动，而且还描绘家庭的温暖。 读者对隐藏在人物的善良和人性中的冷静印象深刻。   


#### もっともっと"楽しいマンガ"を! （更多"有趣的漫画"!）

デビュー以来、順風満帆でヒット街道を進んで来た北条司だが、最初はマンガ家になるつもりはなかったという。  
虽然北条司自出道以来在打拼的道路上一帆风顺，但他最初并不打算成为一名漫画家。   

「それまでマンガは趣味として描いていたけど、なまじ描けると分かっちゃうんですよね。こんなの毎週描けないって。だから当時自分の中でマンガ家は『なりたくない職業べスト10』に入っていました(笑)。だけど持ち込んじゃったんですよ。漫画賞で入選すればお金がもらえるからバイトをしなくて済むと思って。でもお金だけもらえると思っていたら、担当編集者もついてきちゃった(笑)」  
「我曾经把画漫画作为一种爱好，但是当我画完的时候，我意识到，我不可能每周都这样画。 这就是为什么当时做漫画家是我『10大不想做的职业』之一（笑）。但我获奖了。 我想，如果我在漫画奖中获奖，我就会得到报酬，这样我就不用做兼职了。 我以为来的只是奖金，但后来我的编辑也来了（笑）」（译注：待校对）  


そして加20年以上も第一線で活躍し続ける作家生活が始まったわけだが、マンガを描くことは「苦しい」という。北条作品といえば美麗な作画で有名だが、それでも「頭に思い浮かべているイメージの半分も絵にできていない」とさえ語る。そのように苦しみながらも、これだけ長く作家活動を続けてこられた秘訣について聞いてみた。  
就这样，他开始了作为漫画家的生活，20多年来一直活跃在第一线，但他说画漫画是「痛苦的」。 北条的作品以其美丽的图画而闻名，但即便如此，他说「我甚至不能画出我脑海中画面的一半」。 我们问他在遭受如此痛苦的情况下，如何能够持续这么长时间的漫画家生涯。   

「長くやろうという気持ちでいるわけではないです。描くのは年々ッラくなってきている。でもそんなこといってられないですから。行き詰まったとしてもそのまま描き続けるしかない。でも、もう20年以上やってるんだから描けるよ、大丈夫だよという気持ちもあるんです。『なるようになるんじゃない?』がモットーですから(笑)」  
「并不是说我想长期做下去。 每年都画得越来越难。 但我无法承担这样的后果。 即使我被卡住了，我也没有选择，只能继续画下去。 但我已经做了20多年了，所以我觉得我可以做到，而且会做得很好。 『有志者事竟成』是我的座右铭（笑）」  （译注：待校对）  

現在、北条氏は、『週刊コミックバンチ』を手がけるマンガコンテンツ配給会社であるコアミックスの取締役という顔も持っている。次代を担う若い才能に対して、バックアップをしていきたいのだという。  
目前，北条先生也是Coamix公司的主管，该公司是一家制作『週刊Comic Bunch』的漫画内容发行公司。 他希望支持下一代的年轻人才。  

「もっと“楽しいマンガ”を描いてほしいんです。読者は、楽しいもの、おもしろいものを求めてマンガを読むんだから、いたずらに残酷なものとかではなく、楽しい作品をね。『エンジェル・ハート』を描いているのは、ある意味宣伝みたいなつもりがありますね。自分はトシだけどまだ週刊で頑張れている。だから若いみんなも、楽しいマンガ描こうよ、いい作品描こうよっていう、僕なりのメッセージなんです」
「我希望多画一些"有趣的漫画"。 读者阅读漫画是为了寻找有趣的事情，所以我希望漫画是有趣的，而不是不必要的残酷。 在某种程度上，画『Angel Heart』就像做广告。(告诉人们)虽然我已经老了，但我仍然能够画周刊。 因此，这是我向年轻一代传递的信息，他们也应该画有趣的漫画，画出好的作品。」  


北条作品の美麗な作画はもちろん魅力的だ。しかし、それ以上に「楽しいマンガを描く」という意識を常に忘れず、読者を心地よく楽しませてくれる健全さが、北条作品にはあふれている。そのバランスのとれた物語作りこそが、いつまでも尽きない北条作品の魅力の源泉となっているのではないだろうか。
北条作品的精美图画当然是很有吸引力的。 然而，北条的作品充满了一种健康性，在舒适地娱乐读者的同时，始终保持着 "画出令人愉快的漫画 "的意识。 这种均衡的讲故事方式是北条作品的无尽吸引力的来源。

(取材・文 芝田隆広)  

(メディアファク下リー /ダ・ヴィンチ2004年2月号より転載)  
(转载自Mediafakushita Lee/Da Vinci，2004年2月)  （译注：待校对）    

-------------------  

**Links:**  

- https://ocr.space/  
- https://o-oo.net.cn/japaneseocr/  
- https://www.qiuziti.com/tool_dariyu.html  
- https://o-oo.net.cn/keyboard/  
- https://www.deepl.com/  
- https://translate.google.cn

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
