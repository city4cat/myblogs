

# FC的一些细节  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96736))  

说明：  

- "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  
- 类似03_143这样的字符串表示"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似03_143_7这样的字符串表示"卷-页-分格(panel)/分镜"编号。xx_yyy_z表示第xx卷、第yyy页、第z分格(panel)/分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  

----------------------------------------------------------

## 目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  


----------------------------------------------------------

## 背景资料  

### 日本小学、中学、大学的学期  

日本小学/初中?学期:  
第一学期：4月～7月  
暑假  
第二学期：8月～12月  
寒假  
第三学期：次年1月～3月中旬  
春假（3月下旬～4月初）  


日本高中三学年：  
上半学期：4月 ~ 7月21日前后  
暑假  
下半学期：8月24日前后 ~ 次年3月  

日本高中二学年：  
第一学期：4月～9月  
第二学期：10月～次年3月  


日本的大学的学期：  
上半学期： 4月 - 7月末/8月初  
暑假  
下半学期1：  10月 - 圣诞节  
寒假  
下半学期2：次年1月6日(约) - 次年1月底  
春假：次年2月 - 次年3月底  

参考资料： [日本高中学期制度](https://zhuanlan.zhihu.com/p/37253508)  


### FC的出版、重印日期  
FC单行本的版权页有如下信息：  

- Vol01. 1997年 4月23日 第1刷发行；1998年 8月12日 第8刷发行；  
- Vol02. 1997年 9月24日 第1刷发行；1997年12月10日 第2刷发行；  
- Vol03. 1998年 1月24日 第1刷发行；1998年06月10日 第3刷发行；  
- Vol04. 1998年 4月22日 第1刷发行；  
- Vol05. 1998年 7月20日 第1刷发行；1998年11月14日 第2刷发行；  
- Vol06. 1998年10月24日 第1刷发行；1999年 2月25日 第2刷发行；  
- Vol07. 1999年 1月24日 第1刷发行；  
- Vol08. 1999年 4月24日 第1刷发行；  
- Vol09. 1999年 8月24日 第1刷发行；  
- Vol10. 1999年11月24日 第1刷发行；  
- Vol11. 2000年 2月23日 第1刷发行；  
- Vol12. 2000年 5月24日 第1刷发行；  
- Vol13. 2000年 8月23日 第1刷发行；  
- Vol14. 2001年 1月24日 第1刷发行；  

从出版和重新印刷日期来看，FC约三个月重印一次。  

----------------------------------------------------------
## 一些话题的汇总  

### 紫苑的性别
[紫苑进浴室后的一个镜头](./vol01.md#shion-is-a-girl)， 
[紫苑："我像男人吗？是女人！！"](./vol01.md#shion-is-a-girl2)， 
[紫苑的喉结不明显](./vol02.md#shion-is-a-girl)， 
[(大结局)紫苑：“...我其实...是...”，"是女孩子！"](./vol14.md#shion-is-a-girl)， 

### 工作室   
[若苗空的工作室](./vol03.md#studio)， 
[空工作台前的不是窗户，而是落地窗](./vol08.md#studio)， 

<a name="wakanae-address"></a>  

### 若苗家住址  
[若苗家住址](./vol01.md#wakanae-address)， 
[若苗紫的驾照上显示的住址](./vol05.md#yukari-info)， 

<a name="wakanae-gate"></a>  

### 若苗家的大门  
[若苗家出门的左侧、右侧](./vol01.md#wakanae-gate)， 
[若苗家的朝向](./vol03.md#wakanae-gate)， 
[若苗家大门对面的建筑（民居）](./vol09.md#09_064_2)， 
[推测若苗家的朝向](./vol13.md#wakanae-gate)， 

<a name="marks-around-wakanae"></a>  

### 若苗家附近
[若苗家附近的电线杆上有标号"7424"](./vol06.md#marks-around-wakanae)， 
[若苗家附近的电线杆上有标号"3240"](./vol08.md#marks-around-wakanae-3240)， 
[若苗家附近的一个超市](./vol08.md#marks-around-wakanae-supermarket)， 
[若苗家附近的那个车站及站台](./vol08.md#marks-around-wakanae-station)， 
[若苗家附近的电线杆上有广告](./vol09.md#marks-around-wakanae)， 
[雅彦放学回家路上经历的地点](./vol05.md#around-wakanae)， 
[放学回家路上的街景](./vol14.md#14_006_5)， 
[若苗家附近的一个路口](./vol08.md#around-wakanae)， 
[应该是若苗家附近的建筑](./vol09.md#09_028_4)， 
[若苗家附近的一个交通灯](./vol09.md#09_182_0)， 

### 真朱家附近的地标
[真朱家附近的标记](./vol07.md#marks-around-kaoru)， 

### 年龄
[推算若苗紫](./vol02.md#age-yukari)， 
[雅彦18岁](./vol03.md#age-masahiko)， 
[推测雅彦的生日](./vol04.md#推测雅彦的生日)， 
[紫的驾照](./vol05.md#yukari-info)， 
[熏17岁](./vol07.md#age-kaoru)， 
[推测和子的年龄](./vol08.md#推测和子的年龄)， 

### 客串  
[老北客串](./vol03.md#老北客串)， 
[老北客串](./vol06.md#老北客串)， 
[在景点](./vol07.md#老北客串)， 


### 推测若苗空的截稿日  
[可能是周一](./vol01.md#deadline)， 
[推测剧情时间和截稿日](./vol14.md#deadline)， 

### 叶子的住处  
[叶子的住所](./vol03.md#house-yoko)， 
[叶子的住所是同一层中最靠边的那个](./vol09.md#house-yoko)， 
[叶子住处一层有8户](./vol11.md#house-yoko)， 
[叶子住处似乎不是最靠边的屋子](./vol14.md#house-yoko)， 

### 手机  
[紫苑的手机](./vol10.md#紫苑的手机)， 
[紫苑的手机](./vol11.md#紫苑的手机)， 
[紫苑的手机2](./vol14.md#紫苑的手机)， 
[雅彦的手机](./vol11.md#雅彦的手机)， 
[雅彦的手机2](./vol11.md#雅彦的手机2)， 
[雅彦的手机3](./vol13.md#雅彦的手机3)， 
[叶子的手机](./vol13.md#叶子的手机)， 
[浅葱的手机](./vol14.md#浅葱的手机)， 


### 身高
[叶子比雅彦高](./vol03.md#yoko-taller)， 
[雅美的信息](./vol03.md#masami-info)， 
[叶子比雅彦高](./vol11.md#yoko-taller)， 
[叶子比雅彦高2](./vol11.md#yoko-taller2)， 
[熏比雅彦高](./vol13.md#kaoru-masahiko)， 


### 吻  
[吻](./vol03.md#kiss)，  


### Mini
[江岛住处的mini](./vol06.md#mini)， 
[江岛住处的mini](./vol07.md#mini)， 
[江岛住处的mini](./vol07.md#mini2)， 


----------------------------------------------------------
<a name="ch006"></a>  
## 一些链接  

- [推测雅彦妈妈的忌日](./vol05.md#推测雅彦妈妈的忌日)  
- [关于叶子和雅彦分手的一个猜测](./vol14.md#关于叶子和雅彦分手的一个猜测)  
- [一些可能的错误](./errors.md)  
- [一些传神的美图](./beautiful/beautiful_images.md)  
    - 紫苑  
    - 若苗紫  
    - 浅冈叶子  
    - 雅美  
    - 齐藤茜  
    - 菊地顺子  
    - 中村浩美  
    - 叶子母亲  
- [圣诞夜的浪漫](./romance/romance_images.md)  
- [FC时间线](../fc_information/fc_timeline.md)  
- [FC实物集](../fc_things_in_real_world/readme.md)  


----------------------------------------------------------



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



