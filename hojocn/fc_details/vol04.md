
目录：  
[readme.md](./readme.md),  
[vol01](./vol01.md): [ch001](./vol01.md#ch001), [ch002](./vol01.md#ch002), [ch003](./vol01.md#ch003), [ch004](./vol01.md#ch004), [ch005](./vol01.md#ch005), [ch006](./vol01.md#ch006), [ch007](./vol01.md#ch007),   
[vol02](./vol02.md): [ch008](./vol02.md#ch008), [ch009](./vol02.md#ch009), [ch010](./vol02.md#ch010), [ch011](./vol02.md#ch011), [ch012](./vol02.md#ch012), [ch013](./vol02.md#ch013), [ch014](./vol02.md#ch014),  
[vol03](./vol03.md): [ch015](./vol03.md#ch015), [ch016](./vol03.md#ch016), [ch017](./vol03.md#ch017), [ch018](./vol03.md#ch018), [ch019](./vol03.md#ch019), [ch020](./vol03.md#ch020), [ch021](./vol03.md#ch021),  
[vol04](./vol04.md): [ch022](./vol04.md#ch022), [ch023](./vol04.md#ch023), [ch024](./vol04.md#ch024), [ch025](./vol04.md#ch025), [ch026](./vol04.md#ch026), [ch027](./vol04.md#ch027), [ch028](./vol04.md#ch028),   
[vol05](./vol05.md): [ch029](./vol05.md#ch029), [ch030](./vol05.md#ch030), [ch031](./vol05.md#ch031), [ch032](./vol05.md#ch032), [ch033](./vol05.md#ch033), [ch034](./vol05.md#ch034), [ch035](./vol05.md#ch035),  
[vol06](./vol06.md): [ch036](./vol06.md#ch036), [ch037](./vol06.md#ch037), [ch038](./vol06.md#ch038), [ch039](./vol06.md#ch039), [ch040](./vol06.md#ch040), [ch041](./vol06.md#ch041), [ch042](./vol06.md#ch042),  
[vol07](./vol07.md): [ch043](./vol07.md#ch043), [ch044](./vol07.md#ch044), [ch045](./vol07.md#ch045), [ch046](./vol07.md#ch046), [ch047](./vol07.md#ch047), [ch048](./vol07.md#ch048), [ch049](./vol07.md#ch049),  
[vol08](./vol08.md): [ch050](./vol08.md#ch050), [ch051](./vol08.md#ch051), [ch052](./vol08.md#ch052), [ch053](./vol08.md#ch053), [ch054](./vol08.md#ch054), [ch055](./vol08.md#ch055), [ch056](./vol08.md#ch056),  
[vol09](./vol09.md): [ch057](./vol09.md#ch057), [ch058](./vol09.md#ch058), [ch059](./vol09.md#ch059), [ch060](./vol09.md#ch060), [ch061](./vol09.md#ch061), [ch062](./vol09.md#ch062), [ch063](./vol09.md#ch063),  
[vol10](./vol10.md): [ch064](./vol10.md#ch064), [ch065](./vol10.md#ch065), [ch066](./vol10.md#ch066), [ch067](./vol10.md#ch067), [ch068](./vol10.md#ch068), [ch069](./vol10.md#ch069), [ch070](./vol10.md#ch070),  
[vol11](./vol11.md): [ch071](./vol11.md#ch071), [ch072](./vol11.md#ch072), [ch073](./vol11.md#ch073), [ch074](./vol11.md#ch074), [ch075](./vol11.md#ch075), [ch076](./vol11.md#ch076), [ch077](./vol11.md#ch077),  
[vol12](./vol12.md): [ch078](./vol12.md#ch078), [ch079](./vol12.md#ch079), [ch080](./vol12.md#ch080), [ch081](./vol12.md#ch081), [ch082](./vol12.md#ch082), [ch083](./vol12.md#ch083), [ch084](./vol12.md#ch084),  
[vol13](./vol13.md): [ch085](./vol13.md#ch085), [ch086](./vol13.md#ch086), [ch087](./vol13.md#ch087), [ch088](./vol13.md#ch088), [ch088](./vol13.md#ch088), [ch089](./vol13.md#ch089), [ch090](./vol13.md#ch090),  
[vol14](./vol14.md): [ch091](./vol14.md#ch091), [ch092](./vol14.md#ch092), [ch093](./vol14.md#ch093), [ch094](./vol14.md#ch094), [ch095](./vol14.md#ch095), [ch096](./vol14.md#ch096), [ch097](./vol14.md#ch097), [ch098](./vol14.md#ch098), [ch099](./vol14.md#ch099), [ch100](./vol14.md#ch100), [ch101](./vol14.md#ch101), [ch102](./vol14.md#ch102),  


# FC的一些细节 - Vol04


----------------------------------------------------------
<a name="ch022"></a>  
## Vol04 - CH022 - page003 - 顺子的过文定日子 / An Engagement Gift For Yoriko  {#ch022}  		  

### 04_016_1，  
- 顺子：(笑嘻嘻)“姐姐呀！你们究竟想畅饮到什么时候？即使是久别重逢，明天是的打日子嘛！！”。  
![](./img/04_016_1__.jpg) 
![](./img/04_016_2__.jpg) 
![](./img/04_016_3__.jpg)  

顺子的笑和空的伤心形成对比。而作为当事人，顺子的笑显得更加悲伤。  

### 04_024_4
宪司：“我既然要继承这个店，又不能当人家的入赘女婿...”。这么说来，顺子的结婚对象是要男方入赘到菊地家？    

### 04_025_2, 04_031_0,  
空和宪司打架，起初空腰部的衣服整齐，后来衣角零乱。  
![](./img/04_025_2__crop0.jpg) 
![](./img/04_031_0__crop0.jpg)  


----------------------------------------------------------
<a name="ch023"></a>  
## Vol04 - CH023 - page029 - 冤枉路 / Turnabout  {#ch023}  		  

### 04_045，04_055,  
关于顺子过文定的那个家庭的情况。  

顺子过文定日，若苗空闯入，介绍自己为顺子的姐姐春香。男方家庭惊讶。事后...  

- 爷爷：“我还恐怕他知道后，会一时不能接受...真不愧为县会议员，下届想必亦能连任的呀！！ 即使知道了真相，竟然说：‘怎么不早点跟我们说，反正我们都不会介意的...’就是这样呢！！唔！顺子一定会很幸福的呢！！这么一个思想开通的家庭，真的很了不起啊！！”    


### 04_050_5，04_051_0,   
顺子在高知市寄宿读大学时，宪司订婚，顺子新年回家时听到这个消息。  


### 04_052,  
顺子和宪司都因不自信，而没有说出口（喜欢对方）。  


----------------------------------------------------------
<a name="ch024"></a>  
## Vol04 - CH024 - page057 - 爸爸倒下了？ / Father Has Fainted?  {#ch024}  		  

### 04_058.  
下图里空两侧的烟雾应该是因叫喊声大而震落的尘土。  
![](.//img/04_058_1__.jpg)


### 04_062，  
- 空：“可是，雅彦的事有没有告诉爸爸？”  
- 紫：“说起来...因为发生了太多事，没机会说...”  
- 奶奶：“我亦忘了告诉他。”  
- 空：“那么，即是说，爸爸也毫不过问...自然地跟雅彦相处下来...?”   
- 紫：“可能是雅彦自己跟他说了吧...”     
- 空：“唔----”  
- 顺子：”令人意外地还有爸爸居然向阿紫小姐卑躬屈节，我真的不敢相信呢！“  
- 空："对呀！我到现在仍不敢相信呀！！ 我还以为阿紫一定是在爸爸威逼之下，才做我地替身...并不是她本人所愿..."  
- 紫："怎会以为他威逼我呢..."   
- 空："因为他初次见到自己女儿地妻子...他应该会感到不自然...以爸爸地性格，即使对你不揪不睬也是平常事。只能解释...因为爸爸知道阿紫地性格是不擅拒绝别人要求地。"  
- 顺子："这个...爸爸不可能知道的吧！？"  
- 奶奶："......"  

空的父亲的各种异常行为，制造悬念。事实上，是因为他看过信件（04_071，04_072，04_073）。后续，奶奶让紫帮忙收拾衣服供爷爷住院用。紫发现了抽屉里的信件。  

- 紫："这是我写给婆婆地信呀...在老爷的抽屉底里发现地。"  
- 奶奶："爸爸果然有看过。"  
- 紫："吓...?"  
- 奶奶："...初时，他说一发现你寄来的信...便要马上撕烂扔掉...说不要看这不孝女家里寄来地信...所以我才会快他一步到信箱取信，然后悄悄地看一遍...但是...我看过后，便再封好信封，放回信箱里去。"  
- 紫："哦？为何要这样做？"
- 奶奶："因为我觉得...他应该会看地...之后，我偷看信箱，发现那些信已不见了..."  
- 紫："难道獠页看过那些信？"  
- 奶奶：“嗯...最初我以为他不想让我看，所以撕烂扔掉了...哼哼...说也奇怪！有信来的时候，他提也没提...也不知道我曾把信放回信箱...但是，后来他始终也看了...虽然他口口声声说你不孝，毕竟都是开心你们的事...所以才这么慎重地...悄悄把那些信藏起来。那些信，他一定看过不少遍，对于你们的生活、各样事情也很清楚...在信纸上更沾了泪痕...你这次回来...其实爸爸是很高兴地。你也是很高兴吧...但你们...却偏偏都是死硬派.....爸爸已经一把年纪了...如果他出了什么事...想跟你们说出真心话夜蛾来不及了...”  


### 奶奶的名字  
04_073_2若苗紫寄给奶奶地信件上，收件人“菊地节子”。这应该是奶奶的名字。  
![](./img/04_073_2__.jpg)   
![](./img/04_073_2__crop0.jpg)  


### 04_076, 
爷爷钓鱼时摔倒。入住"南畠岡中央综合病院"（04_087_0）209室(04_076_4)。  
![](./img/04_076_0__crop0.jpg) 
![](./img/04_076_4__.jpg)  
中文版显示了更宽的页面，包括该医院的一些信息。     
![](./img/04_076_0__cn_crop0.jpg)  



----------------------------------------------------------
<a name="ch025"></a>  
## Vol04 - CH025 - page085 - 谁是鬼魂？ / Where's The Ghost?  {#ch025}  		  


### 04_087. 
右下角路人的衣服上的字是什么？“犬”？“火”？  
![](./img/04_087_1__.jpg) 
![](./img/04_087_1__crop0.jpg) 

### 04_091.  
空很生气自己不能去海滩玩，把笔都捏碎了。  
![](./img/04_091_6__.jpg) 
![](./img/04_091_6__crop0.jpg)  


----------------------------------------------------------
<a name="ch026"></a>  
## Vol04 - CH026 - page113 - 两位观众 / Two Spectators  {#ch026}  		  

### 04_114. 
雅彦的电影在学校上映。电影票上的日期是9月30日（土/星期六）。实际上，1997年9月30日是周二。  
![](./img/04_114_0__.jpg)  

由上看出FC世界的时间和现实世界时间不同。但，从04_170_0分析出雅彦的生日是10月21日，所以04_170_0里日历显示的是1997年10月21日（周二），这与现实世界的日期相符。  
![](./img/04_170_0__.jpg)  


### 04_132_4,
叶子看过雅彦的戏后找到雅彦。雅彦觉得叶子想要分手，但心里又说：“哦，不！我们从来没有拍拖过呀！！”。所以雅彦不认为两人是男女朋友关系。接着，04_134_3，雅彦内心："够...够了吧！！我不想！！再听下去了！！"。这说明雅彦以为叶子要提出分手。  
![](./img/04_132_4__.jpg) 
![](./img/04_134_3__.jpg)  

之前（03_085_0, 03_085_1,）（前后两周，叶子两次看到雅彦让她心痛的行为（异装、拍吻戏）。两人的关系降至冰点）叶子在思索和雅彦的关系：“我究竟做什么了...为什么会这么生气？是普通朋友吧，但却又...” 。所以叶子不认为两人是男女朋友关系。  


### 04_133_1, 04_133_2,  
- 叶子："是柳叶在戏中的接吻镜头...那个镜头...使我极之不快！！"。

从04_133_3、04_133_4到04_134_0、04_134_1，叶子头低得很深，结合前后台词能看出她此时内心紧张、羞涩、难为情。接着，叶子终于开口:  

- 叶子(04_134_2)：“柳叶！！我...”  
![](./img/04_133_1__.jpg) 
![](./img/04_133_2__.jpg)  

我猜，叶子这里想说的是后续04_158_4的台词"柳叶，我...不想再看到你这幅模样！！"。结果却被辰巳打断。  


### 04_134_0, 
雅彦手里的饮料洒了。  
![](./img/04_134_0__.jpg) 
![](./img/04_134_0__crop0.jpg)  



----------------------------------------------------------
<a name="ch027"></a>  
## Vol04 - CH027 - page141 - 把雅彦还给我！ / Rescue Masahiko!!  {#ch027}  		  


### 04_142_2  
辰巳打算将雅美介绍给制作公司的社长。04_164_5提到他姓樫村。    
![](./img/04_164_5__.jpg)  

社长看过雅美的演出后很喜欢。雅彦对艺能届无兴趣、不想当女演员（04_143_1）。   



### 04_143_3  
辰巳和那个制作公司的社长是多年知交，希望雅彦能帮这个忙。我猜，辰巳是在利用雅彦的善良。雅彦告诉叶子后，叶子识破了这点（04_158_1）  

- 叶子：“看来你被他骗了呢...柳叶！”  
![](./img/04_158_1__.jpg)  

### 04_146_0，
辰巳的内心独白:"现在...我一定要忍耐！！无论如何一定要让雅美进入艺能届，要使她成为女红星！！到她当上女星之后，一定很想身心都变成女人！！而且，更会对我这个一手培育她的男人...感恩不尽！！"  


### 04_150, 
这个酒店的电梯好宽阔！  
![](./img/04_146_3__.jpg) 
![](./img/04_146_3__crop0.jpg)  
![](./img/04_150_3__.jpg) 
![](./img/04_150_3__crop0.jpg)  


### 04_148. 
辰巳想助力雅美的演艺之路，于是向一个演艺公司的Boss推荐雅美。  
![](./img/04_148_1__.jpg)  
看到这个Boss时，我有点出戏，不知道为什么。仔细看了一会，发现这个人物的眼睛有点特别----似乎和FC的画风不一致，更像是素描。再看了一会儿...这会不会是老北照着自己画的？  
![](./img/hojo.jpg)  


### 04_158，04_159，04_160, 04_161，
(紫苑、叶子、影研社一行赶去解救被辰巳虏走的雅彦)在卫生间独处时:  

- 叶子："柳叶...我...不想再看到你这幅模样！！"(04_158_4)  
- 雅彦："我已经明白的了...今天，你来大学...是想说和我绝交的吧..."  
- 叶子："不是...我是不想柳叶你再打扮成这个样子！！一看到你这幅模样，我真的很生气！！即使只是演戏的接吻镜头。因为..."  
- 叶子：“我觉察到，你对于我来说...是个比朋友更重要的人！！你是我生命中最特别的男人！！所以，我不想你再作那种不伦不类的打扮！！”（04_160_0）  
- 叶子："我求你...不要再扮女人"（04_160_0）  

叶子表白雅彦(04_160_0)：“我觉察到，你对于我来说...是个比朋友更重要的人！！你是我生命中最特别的男人！！“，  
以及，她对雅彦的要求：“所以，我不想你再作那种不伦不类的打扮！！”(04_160_0)  


### 叶子、紫苑很勇敢 
04_163, 04_164，叶子再辰巳面前为雅彦辩护：  


- 叶子：“骗人！！你其实仍然很喜欢柳叶吧！？“(04_163_6)  
- 叶子：”辰巳先生...你捧柳叶成为女明星，真的不怕后悔吗？成了女明星后，在演出上一定会被人做不同要求的...例如要拍...接吻镜头！！就好像你今天在大学看电影一样...又要再一次忍受痛苦的打击呀！你有考虑过吗？”。(04_164_1, 04_164_2, 04_164_3, 04_164_4)  
![](./img/04_163_6__.jpg) 
![](./img/04_164_1__.jpg)  

 (辰巳很尴尬)  


随后（04_165_5,04_165_6），紫苑和叶子被辰巳手下绑架，但两人还在为雅彦说话：  

- 紫苑："辰巳先生，你真的不介意？"  
- 叶子："痛苦的是你自己呀！"  
![](./img/04_165_5__.jpg) 
![](./img/04_165_6__.jpg)  



----------------------------------------------------------
<a name="ch028"></a>  
## Vol04	CH028	page169		19岁的生辰  	His Nineteenth Birthday  {#ch028}  		  

<a name="推测雅彦的生日"></a>  
### 推测雅彦的生日:  {#推测雅彦的生日}

04_170_0, 第28话紫圈出的雅彦的生日：1997年某月21号。图里显示21号是周二、该月共31天。现实中1997年10月21日是周二。符合。同时，04_172_0紫苑提到雅彦是天枰座（9月23日-10月23日），也佐证了这一点。  
![](./img/04_170_0__.jpg)  


### 04_176.  
助手们吐糟雅彦的多愁善感。  
![](./img/04_176_5__.jpg)


### 04_181_4，
空和紫的生日很接近。04_181_5，空和紫一起生活后的第一个生日(19岁)。04_182_1, 紫在餐厅做招待，空在工地打工。  


### 04_191. 
庆祝雅彦19岁生日。叶子和雅彦先回到卧室。  

- 叶子：“很恩爱的夫妻呢”（注：指若苗空和若苗紫）  
- 雅彦：“嗯，如果不是男女身份互换...该更好呢！”  
- 叶子：“我们如果...能象他们一样就好了...”  

说明叶子对雅彦的好感继续增加。  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

