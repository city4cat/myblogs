写在前面的话：  
- 即便“心脏记忆”不被主流学术届承认，用其创作艺术（小说、影视、漫画等）也是无可厚非的。  



## [移植了别人的心脏后，她感觉自己“变成了别人”](https://mp.weixin.qq.com/s/oRkJ9_4awDmRDjLwDy-Klw  ) ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96961))    
Original 环球科学  

撰文 | 迈凯拉·文·库顿（Mykella Van Cooten）  
编译 | 黄雨佳  
编辑 | 二七  

加拿大安大略省的安妮·玛丽·斯威策（Anne Marie Switzer）患有先天性心脏病，并一直饱受心脏手术术后并发症的困扰。50岁那年，她终于得到了梦寐以求的礼物——一次心脏移植的机会。然而，就在心脏移植后不久，一些无法解释的现象发生了。  

比如，曾经的安妮并不喜欢酸黄瓜的味道，但现在的她却酷爱在汉堡里多加酸黄瓜。但更重要的是，斯威策认为她的情感发生了变化。  

“不知从何时开始，我理智上知道我爱我的家人，却再也无法拥有那些鲜活的感受了。那些曾让我感到温暖和刺痛的念头和记忆，如今变得有逻辑、理性又冷冰冰的。”她说，“对我来说，这当然是一种损失。我是一个充满爱的人，和他人的关系对我而言很重要。我怀念他人总是对我说，‘你心地真好’。可为什么我现在感受不到那份爱了呢？”  

我们或许会以为，器官移植后性情大变是只会出现在电影中的桥段，但这并不是什么闻所未闻的现象。  


### 陌生的心灵  
部分研究人员认为，移植的器官的确可能保有器官供体的某些特征和经历，甚至会通过一种叫**“细胞记忆”（cellular memory）**的方式将它们传给器官的受体。  

加拿大多伦多全科医院阿杰梅拉心脏移植中心（Toronto General Hospital's Ajmera Heart Transplant Centre）的医学主任迈克尔·麦克唐纳（Michael McDonald）说，“细胞记忆”一词通常指机体产生对疾病的免疫力的方式：**“细胞记忆作为我们适应性免疫反应的一部分，使我们不受疾病、感染、癌症和各种异物的损害。”**  

美国夏威夷大学的神经心理学家保罗·皮尔索尔（Paul Pearsall）就曾记录了十位有类似经历的心脏或心肺移植患者，他们的饮食偏好、艺术喜好、性取向、职业选择等方面在器官移植后发生了明显变化，变得更像移植器官的提供者。  

大多数移植后经历“细胞记忆”的患者案例都是由相关研究人员在医院的帮助下记录下来的，但由于医院会尽可能避免器官接受者与器官提供者的家属接触，这些报道中通常也不会提及相关患者姓名。  

在极少数有姓名的病例中，美国的职业舞者克莱尔·西尔维娅（Claire Sylvia）是一个经典案例。47岁那年，她接受了一名18岁男孩的心脏。术后不久，她发现自己喜欢上了啤酒、鸡块和青椒，并反复在梦中梦见一个名为蒂姆·L（Tim L.）的神秘男子。通过在当地报纸上搜索讣告，西尔维娅确认了自己的器官捐献者名为蒂姆并拜访了他的家人，而他家人对蒂姆的描述也与这些特征完全相符。  

1997年，西尔维娅将这段经历写成了回忆录——《变心》（A Change of Heart）。后来，这个故事又被改编成了电影——《陌生的心灵》（Heart of a Stranger）（转载注：该电影上映于2002年）。  


### 奇迹还是巧合？  
**传统的神经科学认为，记忆存储于大脑而非心脏中**。2019年，美国科罗拉多大学医学院（University of Colorado School of Medicine）的米切尔·李斯特（Mitchell Liester）提出了一个猜想，即器官提供者的记忆可能以表观遗传学、DNA、RNA、蛋白质、心内神经和能量等方式存储于移植的器官中，在移植手术后被器官接受者“记住”，从而改变其人格特征。也有学者认为，移植心脏中的记忆物质可能通过细胞外泌体转运到器官接受者的大脑中。不过，这些猜想并无确凿证据，且饱受争议。  
英国国民保健署（National Health Service，NHS）移植服务部门前主任约翰·沃尔沃克（John Wallwork）就认为，**器官移植不可能改变人的性格、记忆或感觉。许多实验也证实了这样的观点**。  

1992年，一项研究访问了奥地利维也纳47名经历心脏移植2年以上的患者，其中79%的患者表示他们术后性格并未发生任何变化；15%的患者称，自己的性格确实发生了变化，但不是因为器官移植，而是因为自己经历了危及生命的事件；仅6%（即3名）的患者报告称，由于移植了新心脏，他们的性格发生了明显变化。  

美国密歇根大学的移植外科医生杰夫·潘奇（Jeff Punch）对这种所谓的“细胞记忆”给出了几种可能解释。**也许是器官移植后患者使用的药物使他们感觉与从前不同**。比如某些器官接受者发现自己手术后开始爱吃甜食，恰好与捐献者一样。但这其实是因为治疗过程中使用的免疫抑制剂泼尼松龙会使人感到饥饿，因此器官接受者在用药后可能会渴望甜食——这样“神奇的性格转变”其实仅仅是药物的作用。  

**另一种可能是纯属巧合**，比如器官接受者在术后发现自己和捐献者一样，变得爱滑旱冰了。这可能是接受者在恢复期通过电视节目引起了对滑旱冰的兴趣，而此前因身体原因无法滑旱冰。在器官移植的几个月后，他们在购物时偶然看见了旱冰鞋并决定试试，而后发现器官捐献者恰好也是一个喜欢滑旱冰的年轻人。  

也有研究表明，**心脏移植本身就是一件令人感到压力的事情**，在这个过程中，会放大生存的不稳定感，对患者造成一定影响。  

以色列沙瓦塔精神病学中心（Shalvatah Psychiatric Center）一项对35名经历心脏移植的男性的调查表明，**即使是心脏移植后数年，这些受访者也依然感受到高水平的压力**。同时，73%的受访者认为心脏移植对他们的生活产生了巨大影响，他们对生命的珍贵有了新的认知，改变了生活中各项事务的优先级，变得更加利他主义和追求精神价值。40%的受访者对器官捐赠者的去世感到遗憾，也有一些重返工作岗位的人不得不重新适应周围人对他们态度的改变。  


### 他们依然相信  
尽管主流学界并不认为器官移植会改变人的性格，尽管这35名受访者对心脏的解剖学和生理意义都具有科学的认知，但其中仍有不少人对自己获得的新心脏抱有幻想：46%的受访者对捐赠者的身体机能抱有期待，34%的受访者认为自己有可能通过这颗新心脏获得某些捐赠者的特点。  

美国密歇根大学心理学研究员梅雷迪斯·迈耶（Meredith Meyer）和普林斯顿大学哲学教授莎拉-简·莱斯利（Sarah-Jane Leslie）的一项研究认为，**相信器官移植可以使接受者变得更像器官捐赠者是心理本质主义的一种体现**。莱斯利介绍：“心理本质主义是相信我们内心深处有某种本质，其决定了我们的外在特征和行为，这是一种不依赖于所学科学知识的强大思考倾向。”   

共有696名来自美国和印度的受访者参与了这项研究。尽管两国存在着明显的宗教和文化差异，两国在器官移植方面也有着截然不同的历史，但两国的参与者都给出了大致相似的反应模式。令研究人员感到惊讶的是，两国受访者对输血和器官移植会造成人格变化的信念同样强烈。“由于输血如此普遍且相对简单，我们曾以为受访者会认为输血没什么影响”， 迈耶说。  

这项研究和此前沙瓦塔精神病学中心的研究均表明，人们会根据对民族、种族或性别等相关的偏见、欲望或恐惧来选择一个想象中最喜欢或最不喜欢的捐赠者。  

“相同性别、背景和性取向的人会比其他捐赠者更受受访者青睐。要接受一个在道德上令人反感的（如杀人犯）或生存处境不佳的（如无家可归者）器官捐赠者，这样的想法会使受访者感到很不舒服。”莱斯利说，“社会评价对人们评估器官移植有很大影响，他们对和自己更相似的器官捐赠者表现出了强烈偏好。”  


### 参考链接：  

https://www.cbc.ca/radio/tapestry/where-the-heart-lives-transplant-memories-1.6531555

Pearsall, Paul, Gary ER Schwartz, and Linda GS Russek. "Changes in heart transplant recipients that parallel the personalities of their donors." Integrative Medicine 2.2-3 (2000): 65-72.

Liester, Mitchell B. "Personality changes following heart transplantation: The role of cellular memory." Medical hypotheses 135 (2020): 109468.

Lakota, Ján, Fedor Jagla, and Oľga Pecháňová. "Heart memory or can transplanted heart manipulate recipients brain control over mind body interactions?"

Bunzel, Benjamin, et al. "Does changing the heart mean changing personality? A retrospective inquiry on 47 heart transplant patients." Quality of life research 1.4 (1992): 251-256.

http://web.archive.org/web/20050205045342/http:/www.transweb.org/qa/asktw/answers/answers9701/96030807.html

Inspector, Yoram, Ilan Kutz, and David Daniel. "Another person's heart: magical and rational thinking in the psychological adaptation to heart transplantation." Israel Journal of Psychiatry 41.3 (2004): 161.

https://www.princeton.edu/news/2013/07/31/it-matters-where-it-comes-some-people-wary-organ-blood-donations-depending-source

Meyer, Meredith, et al. "Essentialist beliefs about bodily transplants in the United States and India." Cognitive science 37.4 (2013): 668-710.



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



