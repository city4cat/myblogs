
source:  
http://tvtropes.org/pmwiki/pmwiki.php/Manga/FamilyCompo

## Family Compo 里的转义词语（tvtropes版） ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96825))  

![](https://static.tvtropes.org/pmwiki/pub/images/4184dd91_43ab_4f41_8290_9b65c153dc6f.png)  

By the mangaka Tsukasa Hojo, Family Compo is a Slice of Life manga that, perhaps, is a more realistic than average depiction of transsexuality.  
由漫画家北条司的《非常家庭》是一部生活片段漫画，或许，该作品比变性人的一般描写更真实。

Ordinary college student Masahiko goes to live with his aunt and uncle, Yukari and Sora Wakanae. While initially excited about meeting his relatives for the first time, he is surprised to discover that his lovely aunt Yukari is psychically male and his handsome uncle Sora is physically female.  
普通大学的学生雅彦搬去住与他的舅母和舅舅--紫和空--一起住。当他第一次见到这两位亲人时，起初很兴奋，但却惊讶地发现可爱的舅母紫是生理上的男性，而帅气的舅舅空则是生理上的女性。

He doesn't take it well. However, given Masahiko's current fate - living alone in a fetid apartment that he can't afford and with no family to return to - he decides to stay anyway. Eventually, Masahiko's desire for a real home coincide with with the Wakanaes' surprising generosity to anyone who is willing to accept their family at face value, and he quickly becomes Happily Adopted into the household.  
他不适应。然而，考虑到雅彦目前的命运--独自一人住在臭气熏天的公寓里，付不起房租又无家可归--他还是决定留下来。最终，雅彦渴望一个真正的家，这与若苗家出人意料的对任何人的慷慨相吻合--只要这人愿意接受他们家庭，然后雅彦很快就成了幸福的被领养者。 

# Family Compo contains examples of:  
非常家庭包含的例子有:  

- Ambiguously Gay: A bunch of young hooligans question the sexuality of one of their members when he says he wants to have sex with Kaoru.  
含糊不清的同性恋：一群年轻的流氓质疑他们的一个成员的性取向，当他说他想和薰发生性关系。

- Artistic License – Biology: It's implied that neither Yukari or Sora are on hormones, yet it's also implied Sora has a masculine voice. For most trans men, this is highly unlikely without testosterone.  
艺术许可/授权-生物学：这意味着紫或空都没有使用激素，但它也暗示空有男性的声音。 对于大多数跨性别男性来说，如果没有睾酮，这是极不可能的。

- Author Avatar: Not the author, but the character Sora Wakanae has Sora Harukaze, who's idolized by people like Yoko. And Harukaze isn't secretly a woman.  
作者替身/笔名：不是作者，而是若苗空这个角色有笔名春风空，这是叶子等人的偶像。而且春风并不是私下里的女人。

- Berserk Button:  
狂暴按钮:

  - Don't screw up Sora's work. And imply that he's a woman. And, in turn, talk about the beach.  
别把空的工作搞砸了。不要暗示他是个女人。还有，不要谈论海滩。

  - Kaoru harbors deep grudge against her mother for conning Tatsumi for child support though he's not her real father, to the point of becoming a Female Misogynist and wanting to be a boy just to be less like her mother. However, when her mother tells, or really lies to her, that Tatsumi really is her father, Kaoru is infuriated and nearly attacks her.  
薰对她的母亲怀恨在心，因为她骗取了辰巳的抚养费，虽然辰巳不是她的亲生父亲；以至于熏成为一个女性厌恶者。想成为一个男孩，只是为了不那么像她的母亲。然而，当母亲告诉熏，或真的骗熏，辰巳真的是她的父亲，薰被激怒，差点攻击她。

- Boobs of Steel: Sora, a muscular guy who gets into fist-fights and knows kendo hasn't had his substantial breasts surgically removed. As a result, he cannot go to the beach, much to his chagrin.  
钢铁之胸。空，一个强壮的家伙，他习惯了用拳头打架，并懂剑道。没有实质性的乳房切除手术。因此，他不能去海边，为此他很懊恼。

- The Cameo: In the first volume, Shion tells Masahiko that she is going to live with the first stranger she meets. Said first stranger happens to be Ryo Saeba, who, being Ryo is happy with that idea. However, Masahiko takes her away before they can get through with it.  
客串：在第一卷中，紫苑告诉雅彦，她要和她遇到的第一个陌生人住在一起。说的第一个陌生人恰好是獠，獠对此可是很开心的。然而，还没等他们继续，雅彦就把她带走了。

- Can't Hold His Liquor: Sora passes out after a single shot. Would make sense in the context of his sex, but it's probably just because he's Sora.  
酒量不行。空喝一杯酒就倒，这在他的性别背景下说得通，但也可能只是因为他是空。

- Cast Full of Gay: Specifically Cast Full of Crossdressers. Everyone wears outfits of both male and female at one point or another.  
到处都是同性恋：特别是异装。每个人都在此时或彼时穿着男性和女性的服装。

- Crying Wolf: Both Tatsumi and Saki feigned illness to get Kaoru spend New Year with them. Shortly after their hoax was exposed, they fall ill for real, and Kaoru refused to come over to take care of them because s/he assumed it was just another trick.  
狼来了：辰巳和早纪都假装生病，好让薰和他们一起过年。在他们的骗局被揭穿后不久，他们就真的病倒了，薰拒绝过来照顾他们，因为他/她以为这只是另一个骗局。

- Don't You Dare Pity Me!: In volume 5, when Sora has appendicits, he insists that his period isn't an excuse to finish his work late. Even when he collapses and is dragged to the hospital, he resists seeing a gynaecologist because he's a man.  
你敢可怜我吗!：在第五卷中，当空得阑尾炎时，他坚持认为他的月经并不是拖延工作的借口。即使他倒下了、被拖到医院，他也因为自己是个男人而拒绝看妇科医生。

- Easy Amnesia: After passing out and hitting his head, Masahiko forgets finding out that Sora and Yukari are transgender. He remembers it all after simply seeing Sora and his coworkers strip naked while drunk.  
容易失忆。雅彦在撞到头晕倒后，忘了发现空和紫是跨性别者的事。他喝醉后，看到空和他的助手们脱光，才记起来这一切。

- First-Episode Twist: The reveal that Sora and Yukari are both transgender is built up to within the first chapter, with Masahiko actively speculating on what they're hiding after Shion tells him they have a secret. After the first chapter however, It becomes completely open and a prominent theme of the manga.  
第一集转折：空和紫都是性别者是在第一章内揭晓的，紫苑告诉雅彦夫妇二人有秘密后，雅彦积极猜测他们在隐瞒什么。然而，在第一章之后，这个秘密就完全公开了，并成为漫画的突出主题。

- Guy-on-Guy Is Hot: Inverted with Asoaka, who is disgusted when she thinks she sees Masahiko and Tatsuya kissing.  
火热的男男戏：反转的浅冈，她很反感，当她认为她看到雅彦和卓也(江岛)接吻时.

- Good Parents: Despite how freaked out Masahiko is initially by his new parents, they turn out to be positive role models who do their best to treat himself and Shion with the love and respect they deserve.  
好父母。尽管雅彦最初被他的新父母吓坏了，但他们是积极的榜样。对于雅彦和紫苑，空和紫尽力用两个孩子所需要的爱和尊重去对待他们。

- Have I Mentioned That I Am Heterosexual Today: Masahiko primarily, but most of the characters invoke this trope at one point or another, which is odd considering the manga's focus on transsexuality and alternative gender expression. It's justified at least with Sora and Yukari, who are clearly monogamous and straight, but are far more comfortable with switched gender roles.  
我有没有提到我今天是异性恋。主要是雅彦，而不是本文这里和那里所提到的大多数角色。考虑到漫画的重点是在变性和其他性别表达方式，所以这就显得很奇怪。至少在空和紫身上是合理的，他们显然是一夫一妻制、直男直女，只不过调换性别后这么认为才合适。

- If It's You, It's Okay: Played with. Tatsumi falls in love with a forcibly crossdressing Masahiko and doesn't mind when he finds out that "Masami" is biologically male, but gives up on his love after Masahiko explains that he isn't gay or Transgender. However, it's implied he might still have feelings for Masahiko.  
如果是你，就好了:被玩弄. 辰巳爱上了一个强行变装的雅彦，当他发现 "雅美 "是生物学上的男性后并不介意，但在雅彦解释说他是不是同性恋或跨性别者后，辰巳放弃了他的爱情。不过，有暗示他可能仍对雅彦有感情。

- Insistent Terminology:  
顽固的术语  

    - Sora is a man. Sora is Shion's father.  
    空是男人。空是紫苑的父亲。

    - However, Yukari recognizes that no matter what she does, she is still biologically male. Which is why she gets so emotional over having a child.  
    然而，紫认识到无论她做什么，她仍然是生物学上的男性。这也是为什么她会因为要孩子而变得如此情绪化。

- Insufferable Genius: Shion is very smart (and hot, as a girl), but at times will create messes out of fun, especially for Masahiko, and will say she doesn't feel responsible.  
令人难以忍受的天才：紫苑非常聪明（作为一个女孩，也很性感），但有时会因为好玩而恶作剧，尤其是对雅彦，而且她说她概不负责。

- Karaoke Box: There is one at the pub where Masahiko learns the family secret, since he forgot it due to being drunk.  
卡拉OK包厢。在酒吧里有一个，雅彦在那里知道了若苗家的秘密，因为他曾因喝醉而忘记了那个秘密。

- Kissing Cousins: Masahiko and Shion. However multiple times Masahiko has mentioned he wanted them to be Like Brother and Sister, he's constantly referred to as Shion's brother, and Shion's parents do act as his parents... So it seems like adopted Brother–Sister Incest.  
表兄妹之吻。雅彦和紫苑。然而雅彦多次提到他希望他们像兄妹一样，他一直被称为紫苑的哥哥，而紫苑的父母也确实在言行上像是他的父母一样......。所以好像是养兄妹乱伦。

- Manipulative Bitch: Saki often manipulate others from her own interests. However, as revealed on the 65th day, she has Hidden Depths and genuinely cares about Tatsumi and the happiness of her child; she copes with this by "being the villain."  
操纵型的贱人。早纪经常从自己的利益出发，操纵他人。不过，正如第65章所揭示的那样，她有隐藏的深度，真心关心辰巳和孩子的幸福，她以 "小人"的方式来演绎。

- Mr. Seahorse: Sora sees himself as a man, but is technically Shion's biologic mother. He initially refused, because men don’t give birth (and also a thing about his job), but agreed when he saw how saddened Yukari was by her inability to carry a child due to being biologically male. While he is ultimately glad that went through with having a child, his pride is greatly damaged when he's reminded of the process he went through for it to happen.  
海马先生：空视自己为男人，但从技术上讲是紫苑的生物学母亲。起初他拒绝，因为男人不生孩子（还有关于他的工作的事情），但是当他看到紫由于生物学上的原因无法生育而痛苦时，他同意了。虽然他最终很高兴能生育一个孩子，但当他想起要经历的过程时，他的自尊心就大大受损了。

- Muscles Are Meaningless: Shion seems to be abnormally strong for herself. Or maybe she's just that technically skilled and athletic.  
肌肉毫无意义：紫苑本身似乎身体素质异常的好。也许她只是技术娴熟和运动能力强。

- No Periods, Period: Averted. Shion confronts Sora during chapter 4 on stealing her tampons because refuses to buy them for himself during his period. In another chapter he suffers through severe 'pre-menstrual' abdominal pain that actually turns out to be appendicitis.  
无例假，例假：已避免。 紫苑在第四章中与空对峙，原因是空偷了卫生棉条，因为在他例假时拒绝自己购买卫生棉条。在另一章中，他遭受了严重的“经前”腹部疼痛，而实际上却是阑尾炎。

- Not Blood Siblings: Masahiko has feelings for Shion, his adoptive sister. Complicating matters further is that she's also his cousin.  
不是兄弟姐妹：雅彦对他的养妹妹紫苑有感情。使事情更加复杂的是，她也是他的表妹。

- Opportunistic Bastard: Saki doesn't know who Kaoru's father really is, and, despite telling her this while she was young, in the end she tells Kaoru that Tatsumi really is her father, after Tatsumi pays Saki to do so.  
机会主义的混蛋：早纪不知道熏的父亲到底是谁，尽管她在年轻时就告诉了她，但最后还是告诉熏辰巳确实是她的父亲，因为辰巳付了早纪钱。

- Rated M for Manly: Sora, in a more toned down version. There are several moments where either the author or Masahiko asks if Sora really is a woman.  
评为M级男子汉。空，色调较弱。 有几次，作者和雅彦会问空是否真的是女人。

- Schoolgirl Lesbians: They're college age, but Masami picks up a fangirl in addition to all the drooling guys. Naturally, she thinks Masami crossdresses as a man.  
女学生女同性恋者：他们已经上大学了，但是雅美除了被人意淫外还有一个女粉丝。 自然，她认为雅美异装为男人。

- Shout-Out: While trying to pass off Sora's breasts as a drunken hallucination, Masahiko mentions hearing that being drunk makes you see pink elephants.  
大声疾呼：当雅彦试图把空的胸部当作醉酒的幻觉时 正彦提到听说醉酒会让你看到粉红色的大象。

- Shower of Awkward: Completely drunk, Masahiko leans against a door to regain his senses, only to accidentally open it and fall back into the bathroom while Yukari is showering. He's obviously taken aback, but goes into complete shock and passes out once he notices what's between her legs.  
尴尬的淋浴。完全喝醉了，雅彦靠在门上恢复理智，只不过不小心打开了门，倒在浴室里，而紫正在洗澡。他显然被吓了一跳，但当他注意到她的双腿之间有什么东西时，便彻底震惊并昏迷了。

- Transgender: Sora and Yukari Wakanae, Kaoru, and Sora's assistants: Susumu, Hiromi (long blond hair), Kazuko (Judo champion), and Mako (wears glasses). Among the characters listed, some characters have undergone surgeries to alter their bodies but others are merely living their lives as their preferred gender. For example, Susumu went to America to have a complete male-to-female surgery, meanwhile Yukari has breast implants but hasn't had her male genitals removed. Sora hasn't actually gone through any sort of physical surgery at all, though he brings up the topic occasionally when he feels inconvenienced by his body, like when he gets his period.  
跨性别者：若苗空和若苗紫、熏、空的助手：横田、浩美（金色长发）、和子（柔道冠军）、真琴（戴眼镜）。在列举的角色中，有的角色接受了变性手术，但有的角色只是按照自己喜欢的性别生活。比如横田去美国做了一个彻底的男变女手术，同时紫也做了隆胸手术，但没有切除男性生殖器。空其实根本没有经历过任何形式的生理手术，虽然他偶尔会在感到身体不方便--比如来月经--的时候，提起这个话题。

- Strong Family Resemblance: When Yukari arrives to ask Masahiko to move in with her family, Misahako initially confuses her for his deceased mother due to their near-identical appearance.  
强大的家庭相似性：当紫来访要求雅彦搬去与她家时，雅彦最初混淆了她和他已故的母亲，因为她们几乎相同的外貌。

- Right for the Wrong Reasons: Everyone at Masahiko's entrance ceremony assumes Sora and Yukari are crossdressing transvestites; while this is true normally, the attendees believe they are a man dressing as a woman and female dressing as a man, when they are actually dressing as their assigned genders.  
-错误的理由是正确的：在雅彦的入学仪式上，每个人都认为空和紫都是异装癖者;虽然这是正常的--按照他们天生的性别来着装，但现场的人认为他们一个是男扮女装、一个是女扮男装。

- Trans Tribulations: The overarching plot involves Masahiko coming to terms with Sora and Yukari being transgender and Shion being genderfluid. he's initially very uncomfortable and dismissive over their preferred identities, to the point of hoping that requesting they dress as their biological sex during his university's entrance ceremony will make them suddenly stop being trans.  
Trans Tribulations:总体情节涉及雅彦学会接受空和紫是变性人、紫苑时男时女。 他最初对他们所倾向的身份感到非常不舒服和不屑一顾，甚至希望在大学的入学典礼上要求他们穿上生理性别的衣服，就能让他们突然停止跨性别。

- Trans Relationship Troubles: Masahiko is attracted to his cousin Shion but one issue he has is her genderfluidity. He also is troubled over not knowing her assigned gender.  
变性关系的烦恼。雅彦被他的表妹紫苑所吸引，但他的一个问题是她的时男时女。他也为不知道她的性别而烦恼。

- Unwanted Harem: Masahiko just keeps collecting suitors. For his female persona, naturally.  
不受欢迎的后宫。雅彦一直追求者不断，自然是因为他的女性形象。

- Unsettling Gender Reveal:  
性别暴露后令人不安：  

    - Masahiko learns Sora and Yukari and transgender by getting an unsightly look at their privates, Sora by trying to change his shirt after Shion douses him and Yukari by stumbling into the bathroom while she's taking a shower.  
    雅彦发现空和紫是跨性别者，是通过看到他们的私处而知道的：空是通过在紫苑给他浇水后由雅彦试图给他换衣服；而紫则是在洗澡时被雅彦跌跌撞撞地走进浴室

    - Masahiko finds out Sora's assistants are transvestites when they all drunkenly strip dance for him.  
    当空的助手们都喝醉了，为他跳脱衣舞时，雅彦发现他们是异装癖

- Women Drivers: Yukari has a driver's license but hasn't actually been behind the wheel in fifteen years. On the one occasion when she does take the car out, she winds up driving against traffic and jumping a barrier to get onto the right side of the road.  
女司机。紫有驾照，但她已经有15年没开过车了。有一次，当她把车开出去的时候，她却逆向行驶，并跳过障碍物进入道路的右侧。

- Younger Than They Look: Kaoru is strangely tall for a girl, taller than both Shion and Masahiko, but is the youngest one of the group, having just turned 17 at the time of introduction.  
比外表年轻。薰作为一个女孩个子高的出奇，比紫苑和雅彦都要高， 但熏是这群人中最年轻的一个，出场时刚满17岁。        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
