https://www.ktqmm.jp/event_info/60982

2022年7月30日〜2022年9月10日  
★開館10周年企画★必見！記念色紙展  
开馆10周年企划★必看!纪念彩纸展  

_**北九州市漫画ミュージアムは2022年8月3日で開館10周年‼**_  
北九州市漫画博物馆2022年8月3日开馆10周年!  

2012年8月3日、漫画文化の魅力を国内外へ向けて広く発信するとともに、街の個性づくり・にぎわい創出を図る拠点として、当館は誕生しました。常設展のほか、数多くの企画展やイベントの開催を重ね、実に多くの皆様に愛されるミュージアムとして成長して参りました。  
2012年8月3日，作为向国内外广泛传播漫画文化魅力的同时，打造城市个性、创造热闹的据点，本馆诞生了。除了常设展之外，还举办了许多企划展和活动，实际上已经成长为深受大家喜爱的博物馆。

**10年の節目を迎えるこの夏。日頃の感謝を込めて、記念事業を実施します！**  
迎来10年的这个夏天。为了表示平日的感谢，实施纪念事业!  

**開館10周年記念企画その②★★★**  
开馆10周年纪念企划之二★★★  

**10周年記念にお寄せいただいた色紙を一同に展示！**  
一起展示10周年纪念寄来的彩纸!  

![](https://www.ktqmm.jp/wp/wp-content/uploads/2022/07/5d000d013e75170a86148a010aa9b86a-960x720.jpg)

**ゆかりのある漫画家や関係団体の皆さまの素敵な記念色紙（20点）を展示！**  
展出有渊源的漫画家和相关团体的大家的精美纪念彩纸(20件)!  

**「開館10周年企画　記念色紙展」**  
“开馆10周年企划纪念彩纸展”  
  
開館10周年にあわせて、当館にゆかりのある漫画家の方や、お世話になった方々、関係団体の皆さまに、とても素敵な色紙をお寄せいただきました。開館記念日である**8月3日にお披露目**！**ここでしか見れない****渾身の作品に注目です**！  
在开馆10周年之际，与本馆有渊源的漫画家、曾经关照过我们的人以及相关团体都寄来了非常精美的彩纸。开馆纪念日8月3日披露!关注只有在这里才能看到的浑身的作品!

なお展示する色紙は、すべて[**「開館10周年記念誌」**](./2022-07_10-years_anni_flyer.md)に掲載されています。  
另外展示的彩纸全部刊登在“开馆10周年纪念杂志”上。  

* * *

展示開始日：2022年8月3日（水）

会　　 場：6F常設展内Event Cornerほか  
会　　 场：6F常设展内活动环节等  

展示作家：（以下、周年記念誌掲載順に記載）  
展览作家：(以下按周年纪念杂志刊登顺序记载)  

**ちばてつや氏、里中満智子氏、岩本しんじ氏、とだかづき氏、高橋よしひろ氏、鈴木伸一氏、荒俣宏氏、橋本博氏、ナム・ジョンフン氏、うえやまとち氏、ヴァンサン・ルフランソワ氏、せい☆けいすけ氏、関よしみ氏、萩岩睦美氏、北条司氏、陸奥Ａ子氏、伊藤明生氏、井上のぶひろ氏、わたせせいぞう名誉館長、田中時彦館長**

