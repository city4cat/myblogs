https://www.ktqmm.jp/event_info/6488

2015年8月23日〜2015年8月23日  
漫画フェスティバル／冴羽獠に会いに行こう  
漫画Festival/去见冴羽獠吧  

今年、北九州市出身、北条司先生の代表作「シティーハンター」が誕生して３０周年を迎えました。それを記念したイベントを２日間にわたり開催します！！  
今年是北九州市出身的北条司老师的代表作《城市猎人》诞生30周年。2天举办纪念活动! !  

２日目、８月２３日のイベントをご紹介！  
第2天，介绍8月23日的活动!  

**８月２３日（日）１５:００～  
漫画フェスティバル**  
漫画节  

**[![漫画フェスティバル](https://www.ktqmm.jp/wp/wp-content/uploads/2015/06/540ff6ebaeacb5fd4f3ac67d4e72a11c.jpg "漫画フェスティバル")](https://www.ktqmm.jp/wp/wp-content/uploads/2015/06/540ff6ebaeacb5fd4f3ac67d4e72a11c.jpg)**

今年もＪ２チーム・**ギラヴァンツ北九州**とコラボして、「**漫画フェスティバル**」を開催します。今回は、「シティーハンター誕生３０周年」を記念して、本城陸上競技場をシティーハンターでうめつくします！  
今年也将与J2 TEAM Giravanz北九州合作，举办“漫画Festivel”。此次，为纪念“城市猎人诞生30周年”，本城田径场将以城市猎人的身份尽善尽美！

この日のギラヴァンツは水戸ホーリーホック戦。１８時キックオフですが、その前に、サポーターの皆さんにはぜひ「シティーハンター」で楽しんでいただきたいと思います。  
当天Giravanz对阵水户Hollyhock。18点开始，不过，在那之前，请各位支持者务必享受《城市猎人》。  

今回は、西日本新聞社とギラヴァンツ北九州さんのご協力を得て、**シティーハンターをあしらった**、**限定オリジナルチケット**を販売します。  
这次，得到了西日本新闻社和Giravanz北九州的协助，有城市猎人的配合，销售限定原创门票。  

ギラヴァンツファンにとっても、シティーハンターファンにとっても、とてもレアなチケットになるにちがいありません！販売・引換方法は、決まり次第ギラヴァンツ北九州ＨＰにてお知らせします。  
无论对于Giravanz的粉丝，还是城市猎人的粉丝来说，一定都是非常稀有的门票!销售·兑换方法，决定后在Giravanz北九州HP通知。  

そして、なんといっても聞き逃してほしくないのが、**冴羽獠のスタジアムＤＪ！**獠がスタジアムを盛り上げます！！  
而且，不管怎么说都不想错过的，是冴羽良的体育场DJ！好让体育场气氛高涨！！

他にも、なりきり写真撮影ブースやシティーハンターを紹介するブースなどを出展します！  
除此之外，还会展出摄影展位和介绍城市猎人的展位等!

また、今回も**漫画ミュージアム特設ブースを出展**！！  
另外，这次也展出漫画博物馆特设展位! !

人気絵師による似顔絵に、ぬりえ体験、漫画家アシスタント体験ができますよ！  
人气画师的肖像画，有着色体验、漫画家助手体验哦！  

「漫画フェスティバル」は詳細が決まり次第、「[ギラヴァンツ北九州オフィシャルホームページ](http://www.giravanz.jp/ "ギラヴァンツ北九州")」でご案内します。  
“漫画节”的详细内容决定后，将在“Giravanz北九州官方主页”上介绍。

サッカーを観戦するまでの時間、漫画で楽しんでいただきたいと思います！  
在观看足球比赛之前的这段时间，希望大家都能享受漫画带来的乐趣!  

８月２３日、本城陸上競技場でお待ちしております！！  
8月23日，本城田径场恭候您的光临! !  

◆漫画フェスティバル  
日時／２０１５年８月２３日（日）　１５：００～  
　　　　ギラヴァンツ北九州ＶＳ水戸ホーリーホック  
場所／北九州市立 本城陸上競技場
◆漫画节  
时间/ 2015年8月23日(周日)15:00 ~  
Giravanz北九州VS水户霍利霍克  
场地/北九州市立 本城陆上竞技场  
