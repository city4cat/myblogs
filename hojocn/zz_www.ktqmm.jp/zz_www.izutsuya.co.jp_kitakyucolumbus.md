https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/

## ![ 北九州地域には、知らない魅力がたくさん。皆様の知らない地元の 「人」「もの」の魅力を〈きたきゅうコロンブス〉で “ 新発見” !](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/txt-mainv-pc.png)  
北九州地区有很多不为人知的魅力。大家所不知道的当地的“人”“物”的魅力在“北国哥伦布”中“新发现”!

## ![concept](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/ttl-concept.png)

### ![ 地域の魅力を“新発見” ! 作り手の熱い思いをご紹介](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/sttl-concept.png)  
“新发现”地域魅力!介绍制作者的热情  

　北九州地域は、関門海峡をはさんで本州と接し、大陸からも近い地理的特性により、古くから交通の要所として発展し、たくさんの人、物が行き交いました。また、ものづくりのまちとして発展し、近年は環境未来都市として世界をリードしています。 映画、文学、漫画などの文化も盛んなまちです。  
北九州地区隔着关门海峡与本州接壤，由于距离大陆较近的地理特点，自古以来就作为交通枢纽发展起来，有大量的人、物在此往来。此外，作为制造业城市发展，近年来作为环境未来城市引领世界。是电影、文学、漫画等文化也很盛行的城市。

　このような歴史、産業、文化の盛んなまちで育まれた、ものづくりに携わる方の熱い思いが詰まった商品を取り揃え、地域の方々が“地元の魅力を発見する”“地元を愛する思いを実現する”場が「Ｋｉｔａｋｙｕ Ｃｏｌｕｍｂｕｓ」です。  
“Kitakyu Columbus”是在这样的历史、产业、文化繁盛的城市中孕育出来的，汇集了从事制造业的人热情洋溢的商品，当地的人们“发现当地的魅力”、“实现对当地的热爱”。

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-concept-02-pc.png)

## ![Discovery](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/ttl-discovery.png)

作り手の思いや、商品へのこだわりをご紹介!  
介绍制作者的想法和对商品的讲究!  

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-discovery-vol01.png)

安心・安全にこだわった  
料理人が、  
魂と愛情を込めて炊き上げる  
小倉を代表する郷土料理  
注重安心、安全  
厨师说:  
用灵魂和爱情来烹煮  
代表小仓的乡土料理  


[![もっと見る](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-more1.png)](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/fujita.html  "查看更多")

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-discovery-vol02.png)

丁寧に作られた、  
世界で1つの作品に  
こだわりと想いを編み込んだ  
豆靴とアクセサリー  
精心制作的，  
世界上唯一的作品  
编织了执着和想法  
小豆豆鞋和装饰品  

[![もっと見る](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-more1.png)](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/tsumugi.html "查看更多")

* * *

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-discovery-vol03.png)

独自のデザインと  
鮮やかな色彩が  
大空や室内で個性を放つ、  
郷土で愛される手作り凧  
和独特的设计  
鲜艳的色彩  
在天空和室内释放个性，  
深受乡土喜爱的手工风筝  


[![もっと見る](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-more1.png)](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/magojidako.html "查看更多")

## ![Kitakyu Columbusの「人」「もの」「食」](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-kitakyucolumbus-sp.png "Kitakyu Columbus的“人”、“物”、“食”")

![『欲しい！』ボタンからオンラインページへ。お気に入りの商品をお買い求めいただけます♪](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-click.png "“想要!”从按钮进入在线页面。您可以购买您喜欢的商品♪")

NO.1

### STARFLYER

[ 小倉南区 ]

北九州市、北九州空港を拠点とする航空会社〈スターフライヤー〉。黒い機体のお洒落なデザインの飛行機が特徴です。オリジナルアイテムに込められたスターフライヤーの世界観をお楽しみください。  
以北九州市、北九州机场为据点的航空公司〈StarFlyer〉。黑色机身设计时尚的飞机是其特征。请享受融入Original Item的StarFlyer的世界观。  


![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/202112-1.png)

NO.2

### 北条司

[ 小倉北区 ]

北条司先生は、アニメ・漫画『シティ-ハンター』や『キャッツ💛アイ』などで有名な小倉生まれの漫画の街北九州を代表する漫画家です。北九州の夜景と北条司先生のキャラクターがコラボしたポストカードやパスケース・キーホルダーなどのグッズを販売しています。  
北条司老师是代表漫画城市北九州的漫画家，出生于小仓，以其动画和漫画『City Hunter』和『Cat's💛Eye』而闻名。以北九州的夜景和北条司老师的卡通形象相结合，出售明信片、卡包、钥匙圈等商品。

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/202109-1.png)

No.3

### プティパ （译注：Petipa）  
北九州の風景Series

[ 八幡西区 ]

双子の姉妹(絵描き\*あい と 言葉綴り\*ゆう)から生まれたオリジナルキャラクター プティパが、北九州の風景を巡る絵を描きます。色鉛筆で描く絵は、やさしく、故郷、北九州への愛を感じます。  
从双胞胎姐妹(绘画\*爱 和 语言拼写\*悠)中诞生的原创角色Petipa，描绘了围绕北九州的风景。用彩色铅笔描绘的画，让人感受到她对故乡、北九州的爱。

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/202109-2.png)

NO.4

### 波多野菜央

1996年生まれ。  
2019年1月アルバム「太陽と月」にて北九州のレーベルmusic-worksよりデビュー。北九州発シンガーソングライターとして北九州から全国へ、地域の魅力と波多野菜央の歌を発信するべく、イベントやCMソングラジオパーソナリティーなどで全力活動中。  
1996年出生。  
2019年1月凭借专辑《太阳和月亮》通过北九州的唱片公司music-works出道。作为来自北九州的创作歌手，为了将地域魅力和波多野菜央的歌曲从北九州传递到全国，在活动和CM节目主持人等方面全力活动中。


[![欲しい！](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-buy.png)](https://www.izutsuya-online.co.jp/front/commodity/0000/KC21036/ "想要!")

![AA](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/202112-2.png)

NO.5

### ORIORINO

[ 戸畑区 ]

伝統織物アクセサリーのITOHENの新ブランドORIORINOです。箸置きに小倉織や博多織など伝統織物を織り込んだ『ふたつがひとつ ひとつがふたつ』をコンセプトに作成された話題商品です。結婚の祝いやプレゼントにどうぞ。  
是传统织物饰品ITOHEN的新品牌ORIORINO。筷架上加入了小仓织和博多织等传统织物，以“二即是一 一即是二”为概念制作的话题商品。它可以作为结婚礼物或礼品。  

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/202109-4.png)

NO.6

### 追い出し猫  
驱赶猫  

本舗

[ 宮若市 ]

追い出し猫とは、宮若に伝わる物語から生まれた特産品です。表は、ほうきを持ってギョロリ、裏は手で招いてニッコリ。  
災いを退散させ幸福を招く、 表裏一体型の招き猫です。一つ一つ手作りで作成されています。  
驱赶猫是从宫若的传说中诞生的特产。正面是拿着扫帚笑着，背面是用手招呼微笑着。  
是驱除灾祸、招来幸福、表里一体型的招财猫。每一个都是手工制作的。  


![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-nov-new5.png)

No.7

### 手紙や
信件  

[ 築上町 ]

手紙を書く時間、本を読む時間を大切に。をコンセプトにより良い時間を送っていただけるように、紙などにもこだわり、オリジナルでつくっております。  
珍惜写信的时间和读书的时间。为概念，为了能让您度过美好的时光，纸张等也很讲究，是原创的。

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-noa7.png)

No.8

### グランダジュール　城チョコ
Grandazur 城Choco

[ 小倉南区 ]

北九州のお土産の定番となった”ネジチョコ”を作る工場です。きたきゅうコロンブスでは、小倉城の形をした城チョコや新日鐵の缶に入ったネジチョコ。トイレの形のトイレットショコラなどの変わり種を販売しております。ひと味ちがうお土産にどうぞ・・  
是制作北九州的特产“ネジ巧克力”的工厂。在北哥伦布，小仓城形状的城堡巧克力和装在新日铁罐子里的ネジ巧克力。还销售不寻常的产品，比如厕所形状的巧克力。请作为别有风味的礼物…

[![欲しい！](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-buy.png)](https://www.izutsuya-online.co.jp/front/app/catalog/list/?searchWord=%E5%9F%8E%E3%83%81%E3%83%A7%E3%82%B3&searchMethod=0&depth=2)

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-nov-new2102_8.png)

No.9

### 根根菜菜

[ 行橋市 ]

京築地区の食材を中心に使った炊き込みご飯。かわいらしいパッケージでプレゼントにも最適です。  
以京筑地区的食材为中心制作的米饭。可爱的包装作为礼物也最合适。  

[![欲しい！](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-buy.png)](https://www.izutsuya-online.co.jp/front/commodity/0000/KC0001/)

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-no4.png)

No.10

### ドルチェ・ディ・ロッカ カリーノ

[ 若松区 ]

動物たちがひょっこり顔を出した焼きドーナツ。かわいい動物たちは食べてしまうのがもったいなくなるほど愛嬌たっぷりです。  
烤好的甜甜圈上有动物的身影。 这些可爱的动物是如此迷人，以至于吃了它们都觉得很可惜。   

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-noa9.png)

No.11

### 浦野醤油醸造元
浦野酱油酿造公司  

[ 豊前市 ]

砂糖を使わずに麹由来の優しい甘さの「にじいろ甘酒」シリーズが人気です。  
不使用砂糖，从曲子中提取的温柔的甜味的“美味甜酒”系列很受欢迎。  

[![欲しい！](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-buy.png)](https://www.izutsuya-online.co.jp/front/app/catalog/list/?searchWord=%E6%B5%A6%E9%87%8E%E9%86%A4%E6%B2%B9&searchMethod=0&depth=2)

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-no6.png)

No.12

### SALON DE AMBR

[ 築上町 ]

奈良漬専門店が作った「奈良漬×クリームチーズ」です。ワインやウイスキーとのマリアージュは格別です。  
这是奈良渍专卖店制作的“奈良渍×Cream Cheese”。与Wine和Whiskey的搭配别具一格。

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-no8.png)

No.13

### マルホー醤油
MARUHO酱油  

[ 築上町 ]

昔ながらの味を頑固に守り続けてきた創業100年の醤油屋さんが作る、体にやさしいドレッシングとお醤油をお試しください。  
请尝试一下坚守过去味道的创业100年的酱油店制作的有益健康的调味汁和酱油。

[![欲しい！](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-buy.png)](https://www.izutsuya-online.co.jp/front/brand/0000/000701/)

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-noa13.png)

No.14

### いちじく姫本舗
无花果公主本铺  

[ 豊前市 ]

豊かな自然に恵まれた地、九州・豊の国。その風土に育てられた「旬」のいちじくを樹上のまま、完熟させました。  
九州·丰之国，拥有丰富的自然资源。在这种风土下培育的“应时”无花果在树上完全成熟。  

[![欲しい！](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-buy.png)](https://www.izutsuya-online.co.jp/front/app/catalog/list/?searchWord=%E3%81%84%E3%81%A1%E3%81%98%E3%81%8F%E5%A7%AB&searchMethod=0&depth=2)

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-no10.png)

No.15

### ネモコロ堂
Nemokoro堂

[ 小倉南区 ]

食から育むしあわせづくりをモットｰに、地元、北九州生まれの食品メーカーです。関門海峡とらふくだしやめで鯛だしなど地元の食材にこだわり、また、おしゃれななパッケージで、ギフトとしても人気があります。  
以从食物中培育幸福为宗旨，是土生土长的北九州的食品制造商。讲究当地的食材，如关门海峡的鱼汤和鲷鱼汤，其时尚的包装使他们成为受欢迎的礼物。


[![欲しい！](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-buy.png)](https://www.izutsuya-online.co.jp/front/app/catalog/list/?searchWord=ネモコロ&searchMethod=0&depth=2)

![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/img-no202105.png)

## ![Workshop & Event](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/ttl-workshop.png)

小倉井筒屋本館6階の〈きたきゅう コロンブス〉ショップ内では、掲載商品以外にも地元の食品と、地元の作家の作品を取り揃えております。試食会やワークショップなどのイベントも実施しておりますので、ショップにもぜひお越しください。  
在小仓井筒屋本馆6楼的〈きたきゅう Columbus〉商店内，除了刊载的商品以外，还备有当地的食品和当地作家的作品。我们还会举办试吃会和工作坊等活动，欢迎光临本店。


*   [![ONLINE SHOP こちらからその他の商品もご紹介しています！](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-onlineshop.png)](https://www.izutsuya-online.co.jp/front/category/kkc000100/ "ONLINE SHOP这里也介绍着其他的商品!")
*   [![Instagram](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/top/btn-instagram-pc.png)](https://www.instagram.com/kitakyu_columbus/?hl=ja)


![](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/common/img-footer.jpg)

### ![Kitakyu Columbus](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/common/logo-footer-kitakyucolumbus.png)

きたきゅうコロンブス  
Kitakyu Columbus  
小倉井筒屋 本館６階  
TEL093-522-2627  

### ![井筒屋](https://www.izutsuya.co.jp/storelist/kokura/kitakyucolumbus/img/common/logo-footer-izutsuya.png)

北九州市小倉北区船場町1-1  
TEL093-522-3111  

Copyright(C) Kitakyu Columbus All rights reserved.