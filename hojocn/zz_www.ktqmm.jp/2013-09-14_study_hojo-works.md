https://www.ktqmm.jp/blog/3807


2013年09月14日  
明日（９月１５日）北条司作品の研究イベント 開催！！  
明日（９月１５日）北条司作品の研究Event 举办！！  

[![](https://www.ktqmm.jp/wp/wp-content/uploads/2013/09/517784534948c2167860e478de8eef9b1-213x300.jpg "Talk Event看板")](https://www.ktqmm.jp/wp/wp-content/uploads/2013/09/517784534948c2167860e478de8eef9b1.jpg)  
「北条司＆コミックゼノン展」担当学芸員の郷田です。今回の企画展、会期もあと少し（９月23日まで）となりました。  
我是负责「北条司&Comic Zenon展」的乡田。这次的策展，会期还剩一点点了(9月23日结束)。

遠方からお越しいただくことも多く、北条司先生の作品、そして月刊コミックゼノンの作家の皆様の魅力を改めて感じさせていただいております。  
也有很多远道而来的读者，让我再次感受到了北条司老师的作品，以及月刊Comic Zenon的各位作家的魅力。  

明日、９月１５日には、北条司先生の作品をより楽しんでもらえるイベントを開催します。  
明天，9月15日，举办能让大家更欣赏北条司老师的作品的活动。  

「北条司＆コミックゼノン展」開催記念特別企画  
～九州で触れたいマンガ 読むひと・描くひとシリーズ 第７回～  
　[進 化 し 続 け る 漫 画 家 ・ 北 条 司 の 作 品 世 界](./2013-09_study_hojo-works.md)  
「北条司&Comic Zenon展」举办纪念特别企划  
～在九州想接触的Manga 读之人·画之人Series 第7回～（译注：待校对）  
不断进化的漫画家·北条司的作品世界  


北条司先生の同級生であり、漫画家でうえやまとち先生のチーフアシスタントでもある、松原香津美先生、また「City Hunter～愛よ消えないで～」など作曲され、愛が生まれた日でもおなじみの音楽プロデューサー大内義昭さんをゲストにお招きしてのイベントです。  
北条司老师的同级生，同时也是漫画家上山地老师的首席助手的松原香津美老师，还有「CityHunter ~爱不要消失~」等作曲，并以爱情诞生之日闻名的音乐制作人大内義昭作为嘉宾。(译注：待校对)  

当日参加可能。常設展示料金でご参加いただけます。  
当天可参加。可以用常设展览的费用参加。  

漫画作品の変遷や、作曲についてなど、北条司先生の進化を続ける作品のこれまでとこれからを感じられるイベントです。  
这个活动将让你感受到北条司老师不断发展的作品的过去和未来，包括他的漫画作品和他的作曲的过渡。

お気軽にご参加下さい。  
请随意参加。
