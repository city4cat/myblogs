source： 
https://comicvine.gamespot.com/weekly-comic-bunch/4050-97319/

#《Weekly Comic Bunch》部分封面([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96841))

Weekly Comic Bunch
Proper Japanese Title: 週刊コミックバンチ

A manga magazine launched by the quartet of Nobuhiko Horie, Tetsuo Hara, Tsukasa Hojo and Ryuji Tsugihara (all best-known at the time for their work at Weekly Shonen Jump where Horie was editor-in-chief and Hara and Hojo had both published some of the most popular titles of the 80's and 90's).

It lasted from 2001 to 2010 and featured a variety of titles with Hara, Hojo and Tsugihara each contributing a series for the magazine's full run (Sōten no Ken, Angel Heart, and Restore Garage 251 respectively). When Coamix (the company owned by the quartet) and Shinchosha parted ways in 2010 and the magazine ended, the creators and series in the magazines were split into two with the ones bound to Coamix moving to Comic Zenon and the ones bound to Shinchosha moving to Comic@Bunch.


201027, June 18, 2010


201024, May 28, 2010



201006, January 22, 2010


201004-05, January 15, 2010


200939,September 11, 2009


200928,June 26, 2009


200924, May 29, 2009


200921-22, May 8, 2009(怎么有猫眼？)


200916，April 3, 2009


200910，February 20, 2009


200828，June 27, 2008


1，May 29, 2001（AH连载第一话）
