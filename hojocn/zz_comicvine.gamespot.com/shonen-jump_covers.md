source:  
https://comicvine.gamespot.com/weekly-shonen-jump/4050-43519/  
增刊：  
https://comicvine.gamespot.com/weekly-shonen-jump-seasonal-special/4050-88290/

# 与北条司相关的Weekly Shonen Jump的封面


## 1971  
1971-21：  
![](./img/1971-21.jpg)  


## 1979  
1979-5-6：  
![](./img/1979-5-6.jpg)  


## 1980  
1980-5-6：  
![](./img/1980-5-6.jpg)  

《我是男子汉！》（おれは男だ！）发表于《週刊少年ジャンプ》 1980年8月20日増刊号[^1]；  


## 1981  
《三级刑事》（サード‧デカ）     发表于《週刊少年ジャンプ 》1981年1月15日増刊号[^1]；  

1981-6：  
![](./img/1981-6.jpg)  

《猫眼》 （キャッツ アイ）   发表于《週刊少年ジャンプ》1981年29号，这个就是后来《猫眼》连载的第一回[^1]。猫眼开始于这一期：
[Issue #659, No. 29, 1981, June 29, 1981](https://comicvine.gamespot.com/weekly-shonen-jump-659-no-29-1981/4000-515261/)  
Cat's Eye (though this bore the same author and title as the later serial that started a few months later, it is considered a precursor to that series)  
《猫眼》（尽管与几个月后开始的后续系列具有相同的作者和标题，但被认为是该系列的前身）  


Issue #671，No. 40, 1981，September 14, 1981

Issue #674，No. 43, 1981，October 5, 1981

Issue #682，No. 50, 1981，November 23, 1981


## 1982  
[1982-5](https://comicvine.gamespot.com/weekly-shonen-jump-689-no-5-1982/4000-515235/)：  
![](./img/1982-5.jpg)  

Issue #699，No. 15, 1982，March 29, 1982

Issue #700，No. 16, 1982，April 5, 1982

Issue #708，No. 23, 1982，May 24, 1982

Issue #709，No. 24, 1982，May 31, 1982  

Issue #715，No. 30, 1982，July 12, 1982

Issue #722，No. 37, 1982，August 30, 1982

Issue #732，No. 47, 1982，November 8, 1982


## 1983  

Issue #739，No. 1-2, 1983，January 1, 1983

1983-5-6：  
![](./img/1983-5-6.jpg)  

Issue #750，No. 14, 1983，March 21, 1983

Issue #764，No. 28, 1983，June 27, 1983

Issue #772，No. 36, 1983，August 22, 1983

Issue #782，No. 46, 1983，October 31, 1983

Issue #788，December 10, 1983（City Hunter !）


## 1984  

Issue #791，No. 3, 1984，January 9, 1984

Issue #792，No. 4, 1984，January 16, 1984

1984-5-6：  
![](./img/1984-5-6.jpg)  

Issue #794, No. 7, 1984，January 30, 1984

Issue #805，No. 18, 1984，April 16, 1984

Issue #818，No. 31, 1984，July 16, 1984

Issue #825，No. 38, 1984，September 3, 1984

Issue #831,，No. 44, 1984,，October 15, 1984  (1984年，《週刊少年ジャンプ》第44期，『CAT'S EYE』结束了连载。[^1]  )  


## 1985  
Issue #843，No. 6, 1985，January 22, 1985  (1985年，《週刊少年ジャンプ》第6期发表了单独的《猫眼三姐妹》大结局。[^1]  )  
1985-6：  
![](./img/1985-6.jpg)  

Issue #850，No. 13, 1985，March 11, 1985（City Hunter !）  (1985年，《週刊少年ジャンプ》第13期，《城市猎人》开始连载。[^1]  )  

Issue #855，No. 18, 1985，April 15, 1985

Issue #873，No. 36, 1985，August 19, 1985

Issue #885，No. 48, 1985，November 11, 1985


## 1986

Issue #892，No. 5, 1986，January 15, 1986

Issue #893，No. 6, 1986，January 22, 1986  
1986-6：  
![](./img/1986-6.jpg)  

Issue #898，No. 11, 1986，February 24, 1986

Issue #918，No. 31, 1986，July 14, 1986

Issue #922，No. 35, 1986，August 11, 1986


## 1987 

Issue #942，No. 5, 1987，January 15, 1987

[1987-6](https://comicvine.gamespot.com/weekly-shonen-jump-943-no-6-1987/4000-509133/)：  
![](./img/1987-6.jpg)  

Issue #948，No. 11, 1987，February 23, 1987

Issue #955，No. 18, 1987，April 13, 1987

Issue #973，No. 36, 1987，August 17, 1987

Issue #987，No. 50, 1987，November 23, 1987


## 1988  

1988-6：  
![](./img/1988-6.jpg)  

Issue #998，No. 11, 1988，February 22, 1988


Issue #1000，No. 13, 1988，March 7, 1988


Issue #1017，No. 30, 1988，July 4, 1988


Issue #1034，No. 47, 1988，October 31, 1988


## 1989  

Issue #1042，No. 3-4, 1989，January 15, 1989

[1989-5-6](https://comicvine.gamespot.com/weekly-shonen-jump-1043-no-5-6-1989/4000-530942/)：  
![](./img/1989-5-6.jpg)  

Issue #1046，No. 9, 1989，February 13, 1989

Issue #1050，No. 13, 1989，March 13, 1989

Issue #1067，No. 30, 1989，July 10, 1989

Issue #1068，No. 31, 1989，July 17, 1989


## 1990

Issue #1092，No. 5, 1990，January 15, 1990

[1990-6](https://comicvine.gamespot.com/weekly-shonen-jump-1093-no-6-1990/4000-534718/)：  
![](./img/1990-6.jpg)  

Issue #1103，No. 16, 1990，April 2, 1990

Issue #1117，No. 31, 1990，July 16, 1990

Issue #1136，No. 50, 1990，November 26, 1990


## 1991  

Issue #1141，No. 3-4, 1991，January 8, 1991

[1991-5](https://comicvine.gamespot.com/weekly-shonen-jump-1142-no-5-1991/4000-541543/)：  
![](./img/1991-5.jpg)  

Issue #1145，No. 8, 1991，February 11, 1991


Issue #1152，No. 15, 1991，April 1, 1991


## 1992  
1992-5：  
![](./img/1992-5.jpg)  

Issue #1222，No. 39, 1992，September 14, 1992


# 1993
1993-5-6：  
![](./img/1993-5-6.jpg)  

Issue #1263，No. 31, 1993，July 19, 1993  (1993年，在《週刊少年ジャンプ》第31期上北条的《阳光少女》（こもれ陽の下で…）开始连载[^1]。)  

Issue #1271，No. 40, 1993，September 20, 1993


## 1994
Issue #1286，No. 3-4, 1994，January 10, 1994

Issue #1287 ，No. 5-6, 1994，January 24, 1994  (1994年，在《週刊少年ジャンプ》第5・6合刊上，にて《阳光少女》（こもれ陽の下で…）连载结束。[^1]    )  
[1994-5-6]()https://comicvine.gamespot.com/weekly-shonen-jump-1287-no-5-6-1994/4000-540626/：  
![](./img/1994-5-6.jpg)  

Issue #1322，No. 43, 1994，October 10, 1994  (1994年，在《週刊少年ジャンプ》第43期，《RASH!!》开始连载。[^1]    )


## 1995

Issue #1335，No. 5-6, 1995，January 17, 1995  
1995-5-6：  
![](./img/1995-5-6.jpg)  

Issue #1338，No. 9, 1995，February 13, 1995  (1995年，在《週刊少年ジャンプ》第9期，《RASH!!》连载结束。[^1]    )  

Issue #1345，No. 16, 1995，April 3, 1995  (《青空之果-少年们的战场》（苍空の果て－少年たちの戦埸，原作：二橋進吾） 发表在《週刊少年ジャンプ 》1995年16、17两期；[^1]    )

Issue #1356，No. 28, 1995，June 26, 1995  (《少年们的夏天~珍妮的乐章~》（少年たちのいた夏~Melody of Jenny~） 发表在 《週刊少年ジャンプ 》1995年28、29两期； [^1]   )

Issue #1357，No. 29, 1995，July 3, 1995  

Issue #1364，No. 36-37, 1995，August 21, 1995  (《美国梦》（American Dream，原作：二橋進吾） 发表在《週刊少年ジャンプ 》1995年36、37合刊上；[^1]  )


## 1996   
1996-5-6：  
![](./img/1996-5-6.jpg)  


## 1997  
1997-5-6：  
![](./img/1997-5-6.jpg)  

1997-5-6 Kita：  
![](./img/1997-5-6 Kita.jpg)  











## 参考资料
[^1]: [北条司中文简介 By CatNj v4.0 updated](http://www.hojocn.com/bbs/viewthread.php?tid=3094)