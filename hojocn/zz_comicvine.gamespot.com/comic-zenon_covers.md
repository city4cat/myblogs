source： 
https://comicvine.gamespot.com/comic-zenon/4050-92824/

# 《Comic Zenon》部分封面

Proper Japanese Title: コミックゼノン

Monthly manga magazine that was the spiritual successor to Weekly Comic Bunch after the Weekly Shonen Jump alumni that launched Bunch split from the Shinchosha publisher. Like Comic Bunch, this magazine continued to serialize new material in the City Hunter and Hokuto no Ken franchises.


202010，October 2020


202001，January 2020


201903，March 2019


201812，December 2018


201809，September 2018


201806，June 2018


201803，March 2018


201802，February 2018


201711，November 2017


201710，October 2017


201709，September 2017


201707，July 2017（AH第二季完结）


201704，April 2017


201701，January 2017


201606，June 2016


201601，January 2016


201511，November 2015


201509，September 2015


201504，April 2015


201502，February 2015


201411，November 2014


201408，August 2014


201401，January 2014


201303，March 2013


201210，October 2012


201205，May 2012


201112，December 2011


201111，November 2011


201108，August 2011（猫眼-爱）


201105，May 2011


201104，April 2011


201103，March 2011


201102，February 2011


201101，January 2011


201012，December 2010
