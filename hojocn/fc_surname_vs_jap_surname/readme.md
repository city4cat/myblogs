（如果图片或格式有问题，可以访问[这个链接](https://gitlab.com/city4cat/myblogs/blob/master/hojocn/fc_surname_vs_jap_surname/readme.md)）


# FC里的人物姓名与日本姓氏  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96745))  

偶然的机会发现FC里的个别人名和日本姓氏有些相似，但又不同。于是整理了一些。个別人名暂时參考了老公寓里的[一张图](./img/fc_roles_names.jpg) 



## 人物姓名与其近似的日本姓氏

以下列出FC里的人物姓名，以及近似的真实的日本姓氏（用**黑体**表示）。  

- 若苗Wakanae 
    - 若菜Wakana 
- 紫苑Shion[^1] (或Sion[^2])  
    - 志藤/紫藤Shito 
    - 鹽野Shiono
    - 鹽谷/鹽野谷Shionoya  （男装时，在叶子面前紫苑自称塩谷（11卷011页））
- 空Sora（空结婚前的名字：菊地Kikuchi 春香Haruka ）
    - 空谷Kukoku
- 紫Yukari
    - 若菜Wakana 
    - 村崎/紫Murasaki
    - 湯川/湯河Yukawa
- 龙彦Tatsuhiko (紫结婚前的姓名：若苗Wakanae 龙彦Tatsuhiko)
    - Tatsukawa 達川
    - Tatsuke 田附/田付
    - Tatsukuchi 辰口
    - Tatsumatsu 辰松
    - Tatsumi 辰已/巽/立見/田積
    - Tatsumura 龍村
    - Tatsuno 立野/辰野/龍野
    - Tatsuoka 辰岡
    - Tatsusaki 龍崎
    - Tatsuta 龍田/立田

- 柳叶Yanagiba
    - Yanagi 柳
    - Yanagibashi 柳橋
    - Yanagida 柳田
- 雅彦Masahiko，雅美Masami
    - Masada 正田
    - Masago 真砂
    - Masai 正井。「木正」(木正是一個字，左邊為「木」字旁，右邊為「正」)井
    - Masaki 正木/「木正」木/政木
    - Masamori 正森
    - Masamune 正宗
    - Masamura 正村
    - Masaoka 正岡/政岡
    - Masata 政田
    - Masatomi 正富
    - Masayama 當山

- ？？ ？？ （雅彦的妈妈）
- ？？ ？？ （雅彦的爸爸）

- **浅冈Asaoka** 叶子Yoko(Youko)  
    - Asaoka 淺岡/朝岡
    - Yoko 洋子 （例如： 南野陽子(Yoko Minamino), 南田洋子(Yoko Minamida)）


- **菊地**
    - Kikuchi 菊池/菊地


- ？？ （空的爸爸：菊地 ？？ ）
- 节子 （空的妈妈：菊地 节子）

- 奥村

- 顺子Yoriko （空的妹妹：奥村 顺子Yoriko）
    - Yorifuji 依藤
    - Yorishima 庵島
    - Yorita 依田
- 宪司Kenji （空的同学：奥村 宪司Kenji）
- 章子Shoko （宪司的女儿：奥村 章子Shoko）

- **中村Nakamura** 浩美Hiromi （空的助手，本名: Nakamura中村 Mitsuhiro光浩）
    - Nakamura 中村/仲村
    - Hiromasa 廣政
    - Hiromoto 廣本
- 新潟Niigata 和子Kazuko （空的助手）(本名: Niigata新潟 Kazuto和人)
    - Nishikata 西方/西形/西片/西潟
    - Nii 新居/新/丹生/丹/二井/仁井
- **山崎Yamazaki** 真琴Makoto （空的助手）

- **横田Yokota** 进Susuma（空的助手）
    - Shin 秦/新/進/心
    - Suzuki 鈴木/鈴樹/鐸木/鈴記/鈴紀/鈴杵/鈴置/鈴城/錫木/須須木/壽壽木/周周木/壽洲貴/壽松木/進來
    - Susukida 薄田
- ？？ **森Mori**（督促空工作的女编辑）
    - Komori 小森/古森/小守
    - Mori 森/盛/守/毛利

- 真朱Masoba 薰Kaoru
    - Masaoka 正岡/政岡
    - Masada 正田
- **辰四Tatsumi**（薰的爸爸：真朱Masoba 辰四Tatsumi）
    - Tatsumi 辰已/巽/立見/田積
- **早纪Saki**（薰的妈妈：真朱Masoba 早纪Saki）
    - Saki 前。（saki和其他字母組合後，多翻譯為“崎”）

- 卓也Takuya **江岛Ejima** （雅彦的同学）
    - Takubo 田窪/田久保
    - Takuma 宅間/詫間/宅磨
    - Takura 田倉
    - Takusagawa 田草川
    - Takusari 田鎖

- **藤崎Fujisaki** 茜Akane（雅彦同学）
    - Fujisaki 藤崎
- **仁科Nishina** 耕平Kohei （迷恋雅彦的人）
    - Nishina 仁科
- ？？ 美葵Aoi （喜欢辰巳的人）
    - Aoi

- 浅葱Asagi 蓝（紫苑的初恋）
    - Sasagi 佐佐木

- **齐藤Saito** 玲子Reiko (紫苑(小学男装)是她的初恋)    - 

- Hanzu八不 Kyoko京子 (紫的中学插花社的成员)
    - Hanyu 羽生
    - Hanzawa 半澤/榛澤
    - Kyoko 今日子 (例如： 小泉今日子(Kyoko Koizumi) )
    - Kyogoku 京極
    - Kyono 京野
- **Isaka井阪** Kyoko京子（八不京子结婚前的姓名）
香港玉皇朝高清版里的字迹像是“岸田”（第2卷171页），即：
  **Kishida岸田** Kyoko京子



-----------------

## 角色的姓名与植物名  
作品里一些角色的名字与植物有关，例如：柳叶、若苗、紫苑、叶子、浅葱、菊地、齐藤、葵。  

1990年代，有以下事实:  
> 1992年的里约热内卢举行的联合国环境和发展会议[^rio1992]；  
> 1998年“環境ホルモン（译注：环境hormone）”成为当年的日本流行语之一[^1998] [^ch]；  

这似乎说明，当时环保在日本成为受关注的话题。我猜，这可能促使作者以植物为角色命名。  

[^rio1992]: 
1992年6月，在巴西里约热内卢举行的联合国环境和发展会议。主要成果之一是达成了《21世纪议程》,这是一项大胆的行动计划，呼吁投资于未来，实现21世纪的全面可持续发展。《21世纪议程》针对教育、自然资源保护、经济可持续发展等方面提出了建议。此外会议也取得了其他诸多重大成就：达成了《里约宣言》及其27项普遍原则，通过了 《联合国气候变化框架公约》、 《生物多样性公约》，达成了 《森林原则声明》 。此次会议召开后，可持续发展委员会正式成立，第一届小岛屿发展中国家可持续发展全球会议于1994年召开， 针对跨界种群和高度洄游鱼类种群协定的谈判也开始进行。[联合国环境和发展会议，1992年6月3至14日，巴西里约热内卢](https://www.un.org/zh/conferences/environment/rio1992)  

[^1998]: 
1998年“環境ホルモン（译注：环境hormone）”成为当年的日本流行语之一。“进入21世纪，环境问题被提出来，二恶英、杀虫剂、DDT和多氯联苯等化学物质显然会对人体和动物生态系统产生严重影响。这些物质被称为内分泌干扰物或内分泌干扰化学品，为了使这个极其难懂的名字更容易被大众理解，日本广播公司（NHK）和Yasusen Iguchi先生发明了一个普通的名字 "环境hormone"。此后，这一名称经常出现在大众媒体上。”转自[1998 トップテン入賞](https://kw-note.com/marketing/1998-shingo-ryukogo-taisho/)  

[^ch]: 
[CH年表@SweeperOffice](../zz_sweeper-office_cn/cocktail/history.md)  

<a name="Yanagiba"></a>  
## 关于 **柳叶Yanagiba 雅彦Masahiko**  
'彦'字的日文解释：男子的美称（与'姬'(女子的美称)相对）。应该是一个常见名，比如，井上雄彦、堀江信彦。  
作者北条司为雅彦取的姓氏"柳叶(Yanagiba)"可能与下面的历史[11]有关：  
> "1945 – Gay bar Yanagi opened in Japan"  
> (日本（首家？）同性恋酒吧"柳木(やなぎ(ゲイバー)，英文：Yanagi)" 于1945年开业)  


## 关于 **若苗Wakanae 紫苑Shion**  
FC第2卷卷首语：  
> “查看字典，"紫"这个字不念做"缘"这个读音，但是，我始终很想把她(?)的名字些作"紫"而念做"缘"。只因，在《日本语大辞典》中，有记载紫色亦称“缘之色”，词典中解释的意思是“比喻因某一种关系的缘故，使爱情有其他的演变”。我想这正好跟紫的性格吻合，所以把她的名字改成"紫"而读作"缘"...”。  
我猜，紫苑的名字会不会和此有关：紫缘-->紫苑（“苑”和“缘”的日文发音近似） 

其他参考资料：  
- [关于"紫苑"这个名字](./zz_shion_jianshu/zz_shion_jianshu.md)  
- [紫菀？紫苑？——灵魂燃烧！](https://www.jianshu.com/p/c04d26ca68c)    


## 关于 **若苗Wakanae 空Sora**   
北条司的短篇《蔚蓝长空》创作于1995年，其故事原型(参考文献[9])里提到高知县空军(简称高知空)(详见[10])。FC创作于1996年，"若苗 空"的名字里也有“空”，其家乡也在高知。不知这两者是否有联系。  


## 关于 **浅冈Asaoka 叶子Yoko**  
叶子姓氏“浅冈”。叶子家在奥多摩，据[12]猜测具体地点可能是"御岳渓谷"，门前的河流是"多摩川"。此外，有[多摩川浅間神社(Sengen Shrine)](https://pixta.jp/photo/64779711)，在多摩川的下游，距离御岳渓谷直线距离约50km。 不知道叶子的姓氏"浅冈"和浅間神社是否有关系。


## 关于 **森Mori**  
森是若苗空的编辑。[13]提到：  
"Among his former assistants there were Inoue Takehiko, Umezawa Haruto & Yanagawa Yoshihiro. Morita Masanori applied as his assistant, but he introduced Morita to HARA Tetsuo instead as he already had enough assistants at that time."  
(他(北条司)之前的助手有 井上雄彦(Inoue Takehiko)[14]，梅泽春人(Umezawa Haruto)，柳川喜弘(Yanagawa Yoshihiro)。森田真法(Morita Masanori) 曾应聘当他(北条司)的助手，但他(北条司)把 森田 介绍给了 原哲夫，因为他(北条司)当时的助手已经满了。)   
所以，我推测FC里的编辑 森的名字可能源于 森田真法[15]。    
    

## 关于 “龙之介”、“龙马”
14_215提到爷爷和空分别要给新生儿取名为“龙之介”、“龙马”，其他人觉得这两个名字都很糟糕。  
之所以有“龙”字，可能与2000年是龙年有关（但此时FC的剧情应该是1999年8月份）。日语里龍是旧字体，竜是日本的简化字[4]。  

- 爷爷给新生儿取名"龙之介"。“之介”是日本常用名。20世纪初，"之助"、“之介”在正式日本人名中大量出现。20世纪70年代后"之助"已经成为复古风名字的代表。[5]  
- 空和给新生儿取名"龙马"，并说“生男孩子的话，始终是叫龙马最好! 这才像土佐男人的名字嘛！”。相关人物：   
    - 坂本龙马（明治维新的重要功臣，其家乡正是“土佐“(高知三旧称[6])。此外，高知还有其铜像和紀念馆[7]，所以此人是高知的历史名人，是高知人的骄傲。所以空提议的名字“龙马”，应该与此人有关）；  
    - 剑豪龙马（《海贼王》里的人物。）；  
    - 越前龙马（《网球王子》）。    


## 一些有趣的巧合
以下这些联系可能仅仅是巧合而已。  

- 2024年2月，OpenAI发布了text-to-video模型Sora[^sora]。
    - 一方面，OpenAI Sora的“Sora”与本作品中的角色"若苗(Wakanae)空(Sora)"的英文名相同。   
    - 另一方面，据报道[^sora-nytimes]：OpenAI Sora的名字“Sora”取自日语“空”，意为天空；选用该词是因其让人联想到无限的创作潜力。这削弱了OpenAI Sora的“Sora”与FC角色若苗空的联系。  
    （注：由该报道，我猜，OpenAI Sora的“Sora”可中译为“天马行空”或“行空”）    

- 2024年12月，Google Quantum AI发布了量子计算领域里的程碑式产品：量子计算芯片Willow[^google-willow]。
    - 一方面：  
        - 英文"willow"意为：柳树、柳木[^willow-wr]。本作品中的角色柳叶雅彦的姓氏与“柳木”的一个（可能的）联系见[上文](#Yanagiba)。  
        - 英文"willow"也可指柳叶[^willow-ios]。这本作品中的角色柳叶雅彦的姓氏相同。  
    - 另一方面，曾于2019年报告了Sycamore[^rm]（悬铃木[^sycamore-camdict]）的Google Quantum AI团队似乎倾向于使用树木名命名其产品。这削弱了Willow与FC角色柳叶雅彦的联系。  

[^sora]: [OpenAI Sora](https://en.wikipedia.org/wiki/Sora_(text-to-video_model))  
[^sora-nytimes]: [OpenAI Unveils A.I. That Instantly Generates Eye-Popping Videos](https://www.nytimes.com/2024/02/15/technology/openai-sora-videos.html)  
[^google-willow]: [Google Quantum AI - Willow](https://blog.google/technology/research/google-willow-quantum-chip/)  
[^rm]: [Beyond-classical](https://quantumai.google/roadmap)
[^sycamore-camdict]: [sycamore - CambridgeDictionary](https://dictionary.cambridge.org/dictionary/english-chinese-simplified/sycamore)  
[^willow-wr]: [willow - WordReference](https://www.wordreference.com/enzh/willow)  
[^willow-ios]: 参见iOS中的牛津英汉汉英词典


## 參考链接

1. [Table of Common Japanese Surnames](http://htmfiles.englishhome.org/Japsurnames/Japsurnames.htm)  
2. [日本常用姓氏表收集](https://www.douban.com/note/353116593/)  
3. [日本为何多达十三万种姓氏?](http://book.sina.com.cn/z/ribenxingshi/)  
4. [为什么日语里芥川龙之介的 龍 字不是 竜字， 日语里有龍这个字吗](https://zhidao.baidu.com/question/455131769590688325.html)  
5. [日本人名中的"之助"、“之介”是什么意思](https://zhidao.baidu.com/question/751964630780136084.html)  
6. [百度百科-土佐](https://baike.baidu.com/item/土佐)  
7. [【四國-高知】桂浜公園．日本名海岸百選．坂本龍馬銅像](https://www.travalearth.com/post-31405926/)  
8. [...]()  
9. [永末千里『かえらざる翼』](http://www.warbirds.jp/senri/08tubasa/tubasa.html)  
10. [解读《蔚蓝长空》](../ss_blue-sky/readme.md)  
11. [Timeline of LGBT history, 20th century](https://en.wikipedia.org/wiki/Timeline_of_LGBT_history,_20th_century)  
12. [FC实物集](../fc_things_in_real_world/readme.md)  
13. [北条司 - MangaUpdates](https://www.mangaupdates.com/authors.html?id=350)  
14. [柳川喜弘](https://www.mangaupdates.com/authors.html?id=12391)  
15. [森田真法](https://www.mangaupdates.com/authors.html?id=994)  

[^1]: F.Compo英文版中紫苑的名字为"Shion";  
[^2]: 《北条司 漫画家20周年纪念Illustrations》(Hojo Tsukasa 20th Anniversary Illustrations)中显示紫苑的英文名为Sion：  
   ![](./img/shion_name__sion.jpg)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



