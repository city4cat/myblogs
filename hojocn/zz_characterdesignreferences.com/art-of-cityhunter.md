source:  
https://characterdesignreferences.com/art-of-animation-6/art-of-city-hunter

点击查看原大图。  


# Art of City Hunter
March 17, 2018

City Hunter is an animation franchise produced by Sunrise Studios. The first series was directed by Kenji Kodama in 1987, which was followed in later years by 2 other series and 7 movies. The anime was based on the homonym manga series written and illustrated by Tsukasa Hojo. The pictures on this page are a collection of artworks created for this anime franchise.

* * *

### THE STORY

Ryo Saeba is a private eye known as the "City Hunter" and people hire him to solve their dangerous problems. He is a skilled shooter, an expert in hand-to-hand combat and an excellent driver with a mysterious past. Not even Ryo himself does knows his origins nor his true age, in fact he can only remember that as a child he survived a plane crash in Central America, where his parents died. There he grew up as a guerrilla fighter, then he moved to the United States under his current profession of ''sweeper'' and finally in the Shinjuku district in Tokyo. One day, his associate, Hideyuki Makimura, is murdered. Ryo has to take care of Hideyuki's sister, Kaori, who becomes his new partner...

[![](./img/settei-city_hunter-001.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963600424-UNMXWXHC3SVOIOTXWP0T/settei-city_hunter-001.jpg)

[![](./img/settei-city_hunter-008.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963603879-HFMAMBSB5CGHAKOV6XUV/settei-city_hunter-008.jpg)

[![](./img/settei-city_hunter-011.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963608185-DH3UMYZI44LFAG5GFMQP/settei-city_hunter-011.jpg)

[![](./img/settei-city_hunter-058.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963631602-3TV4ZS9KWCLPJ24D9517/settei-city_hunter-058.jpg)

[![](./img/settei-city_hunter-002.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963600423-GATD2ZUCJY7T65IND1XO/settei-city_hunter-002.jpg)

[![](./img/settei-city_hunter-021.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963614553-21025OHH19AH649GQI2U/settei-city_hunter-021.jpg)

[![](./img/settei-city_hunter-018.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963613512-ZCT2EC1TH0RHIVEP47CG/settei-city_hunter-018.jpg)

[![](./img/settei-city_hunter-003.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963601162-2HF8NN62BX1M5CTOA1RY/settei-city_hunter-003.jpg)

[![](./img/settei-city_hunter-004.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963601660-K0JKCZISCKNWR25GG60T/settei-city_hunter-004.jpg)

[![](./img/settei-city_hunter-005.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963602503-V0WQ2CJYJ1K6PQQ60CDK/settei-city_hunter-005.jpg)

[![](./img/settei-city_hunter-006.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963602700-WRFT8N64R4SBX87JDOX2/settei-city_hunter-006.jpg)

[![](./img/settei-city_hunter-007.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963603875-S47XY2T24FEKWX78W82A/settei-city_hunter-007.jpg)

[![](./img/settei-city_hunter-009.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963607359-WQLV0GLRRTALEA5MEV8Y/settei-city_hunter-009.jpg)

[![](./img/settei-city_hunter-010.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963607362-59LBAGJSISJAJ2X9OJAY/settei-city_hunter-010.jpg)

[![](./img/settei-city_hunter-012.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963609528-5KIJXXH81B7XL3XQVFU4/settei-city_hunter-012.jpg)

[![](./img/settei-city_hunter-013.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963610035-L6LE0ZVH8YLU5W4NURIV/settei-city_hunter-013.jpg)

[![](./img/settei-city_hunter-014.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963610900-D0UO3CBZ0D63FRC78958/settei-city_hunter-014.jpg)

[![](./img/settei-city_hunter-015.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963611726-OEF6IIJMYD4P6DZW3W7X/settei-city_hunter-015.jpg)

[![](./img/settei-city_hunter-016.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963611896-PYYR6WIHKLFZC4S34528/settei-city_hunter-016.jpg)

[![](./img/settei-city_hunter-017.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963612594-FIZEHYY2G3FI7T1XMMLB/settei-city_hunter-017.jpg)

[![](./img/settei-city_hunter-019.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963613840-JDBH21D7RAH00MQBS23D/settei-city_hunter-019.jpg)

[![](./img/settei-city_hunter-022.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963615408-U11TNCONUNOIVD1SU4NM/settei-city_hunter-022.jpg)

[![](./img/settei-city_hunter-023.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963615967-8TDKV31OTRIOKAH35BQX/settei-city_hunter-023.jpg)

[![](./img/settei-city_hunter-024.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963616811-2BWIN4FGPUVX32LQXYS8/settei-city_hunter-024.jpg)

[![](./img/settei-city_hunter-025.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963617139-CVUDNRCB2WIAULWHZUKA/settei-city_hunter-025.jpg)

[![](./img/settei-city_hunter-026.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963617569-KTVFLC0G4GQC4OCS3SHY/settei-city_hunter-026.jpg)

[![](./img/settei-city_hunter-027.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963757631-P06OSUHR7E8EMAP2TFQJ/settei-city_hunter-027.jpg)

[![](./img/settei-city_hunter-028.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963618415-00O9D0JTCM22KJMTNRWW/settei-city_hunter-028.jpg)

[![](./img/settei-city_hunter-029.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963618409-VIB3EYOJ2A9E69ZPRYVN/settei-city_hunter-029.jpg)

[![](./img/settei-city_hunter-030.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963619016-XGW242FLA0BNL5ND1F7R/settei-city_hunter-030.jpg)

[![](./img/settei-city_hunter-031.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963619324-5RHTOPE4OZFDHWEQQZIX/settei-city_hunter-031.jpg)

[![](./img/settei-city_hunter-032.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963619681-3ZCXJOWXX98DO9BM7NGH/settei-city_hunter-032.jpg)

[![](./img/settei-city_hunter-033.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963620342-AUOKGTWWMPVBA2S734VE/settei-city_hunter-033.jpg)

[![](./img/settei-city_hunter-034.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963620984-T2OHLMCZLC4E5QJ4BC2Z/settei-city_hunter-034.jpg)

[![](./img/settei-city_hunter-035.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963621675-2HUTX5T5FTL919839SG2/settei-city_hunter-035.jpg)

[![](./img/settei-city_hunter-036.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963621663-R3NGEIL7VRHGRHNGNJGC/settei-city_hunter-036.jpg)

[![](./img/settei-city_hunter-037.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963622174-HWM1SR57JMLJJS48EL1S/settei-city_hunter-037.jpg)

[![](./img/settei-city_hunter-038.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963622475-G8MDL7KONE6CHEA7P77X/settei-city_hunter-038.jpg)

[![](./img/settei-city_hunter-039.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963622740-BB7J5TD6Y951BUDGY1P0/settei-city_hunter-039.jpg)

[![](./img/settei-city_hunter-040.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963623446-DAW0BBW8YZP0TJIA7O3X/settei-city_hunter-040.jpg)

[![](./img/settei-city_hunter-041.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963623458-DHW4S47T6LXZH7E9BBB3/settei-city_hunter-041.jpg)

[![](./img/settei-city_hunter-042.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963624224-SEKGRMJATRZIDJ5O9I6C/settei-city_hunter-042.jpg)

[![](./img/settei-city_hunter-043.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963624386-9D2DXJNQLTTJG3819Q93/settei-city_hunter-043.jpg)

[![](./img/settei-city_hunter-044.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963624658-NX45UA4A7JXTI48C3E35/settei-city_hunter-044.jpg)

[![](./img/settei-city_hunter-045.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963625284-DPB28CT283C7AJ4IV69A/settei-city_hunter-045.jpg)

[![](./img/settei-city_hunter-046.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963625277-XR730X807JK2NT1ZB82P/settei-city_hunter-046.jpg)

[![](./img/settei-city_hunter-047.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963625627-Y54CNAG3D0K00E2EGAJW/settei-city_hunter-047.jpg)

[![](./img/settei-city_hunter-048.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963626122-ZU96GHVZ89Q4RZRVXJXY/settei-city_hunter-048.jpg)

[![](./img/settei-city_hunter-049.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963626562-YJON6HF8K3TNIFQO46NA/settei-city_hunter-049.jpg)

[![](./img/settei-city_hunter-050.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963626851-OVCV36XPQP66AU92KTA0/settei-city_hunter-050.jpg)

[![](./img/settei-city_hunter-051.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963627521-MKV338LUQWIAE8AYE339/settei-city_hunter-051.jpg)

[![](./img/settei-city_hunter-052.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963627659-0X5YWRZ3RD49B4SL95ST/settei-city_hunter-052.jpg)

[![](./img/settei-city_hunter-053.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963627929-2P2ZHC41SMHET6S4U4N4/settei-city_hunter-053.jpg)

[![](./img/settei-city_hunter-054.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963628475-NP89U789HACMYWVB3SQ1/settei-city_hunter-054.jpg)

[![](./img/settei-city_hunter-055.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963628915-2CTYEBJW9V32TFDMP944/settei-city_hunter-055.jpg)

[![](./img/settei-city_hunter-056.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963629231-LH54NFDWRYKXGWK37QBQ/settei-city_hunter-056.jpg)

[![](./img/settei-city_hunter-057.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963630707-H42ECBQ5CUCN33PRWAE4/settei-city_hunter-057.jpg)

[![](./img/settei-city_hunter-059.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963631915-DEBVQPHFRS16QDOR772C/settei-city_hunter-059.jpg)

[![](./img/City_Hunter_settei_019.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963882155-142F9WUAEOGQV20SPEYH/City_Hunter_settei_019.jpg)

[![](./img/City_Hunter_settei_020.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963880772-M3WFK7A64W1F7963UDJ7/City_Hunter_settei_020.jpg)

[![](./img/City_Hunter_settei_021.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963882528-HHBWL9FFTJ9UMIXOBN5J/City_Hunter_settei_021.jpg)

[![](./img/City_Hunter_settei_022.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963882936-U5OOEEYGLXC6KNVLUF9J/City_Hunter_settei_022.jpg)

[![](./img/City_Hunter_settei_023.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963884593-4CLSJZYZOV7SD66CM9KL/City_Hunter_settei_023.jpg)

[![](./img/City_Hunter_settei_024.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963884270-8TVRO7ZESFCEK0MVRLDG/City_Hunter_settei_024.jpg)

[![](./img/City_Hunter_settei_025.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963885124-ARP3OWMFDKJ78JZDRNTK/City_Hunter_settei_025.jpg)

[![](./img/City_Hunter_settei_026.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963885368-GWO2PERX9QTT5QGARY9L/City_Hunter_settei_026.jpg)

[![](./img/City_Hunter_settei_027.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963886104-M5HWVWZSSFVTDY73OPN1/City_Hunter_settei_027.jpg)

[![](./img/City_Hunter_settei_028.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963886354-2QO22MUSXZ34RXSHCJ7Q/City_Hunter_settei_028.jpg)

[![](./img/City_Hunter_settei_030.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963886878-OEMZKIONETN1E82I7GUX/City_Hunter_settei_030.jpg)

[![](./img/City_Hunter_settei_031.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963888658-NQXT92ZQ0EO83DNXDAC3/City_Hunter_settei_031.jpg)

[![](./img/City_Hunter_settei_032.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963888907-4SPZJCBHON4R07QQLZBZ/City_Hunter_settei_032.jpg)

[![](./img/City_Hunter_settei_033.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963890538-Y9EJ3VXZ9X1VVZQGW3H3/City_Hunter_settei_033.jpg)

[![](./img/City_Hunter_settei_034.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963890542-ULJ5AUYPIP0K8XZDOIBI/City_Hunter_settei_034.jpg)

[![](./img/City_Hunter_settei_035.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963891508-T6UZVROKI1KXTVSPMF27/City_Hunter_settei_035.jpg)

[![](./img/City_Hunter_settei_036.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520963891766-YQVR3VTGYDRUXM6YF65T/City_Hunter_settei_036.jpg)

[![](./img/cityhunter001.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964214242-F69AXA0H9OOV45JUUCO8/cityhunter001.jpg)

[![](./img/cityhunter002.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964214239-KN5I5I5WJ87LTJ4TTUNI/cityhunter002.jpg)

[![](./img/cityhunter003.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964214969-HXQ99PSGH999SR6Y0N91/cityhunter003.jpg)

[![](./img/cityhunter004.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964214967-KKKHNV48UG7FNCV6123U/cityhunter004.jpg)

[![](./img/cityhunter005.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964215545-NCV6RTF5BF0H0Q76D410/cityhunter005.jpg)

[![](./img/cityhunter006.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964216047-VV3V6LWRN19OLBVHX0NC/cityhunter006.jpg)

[![](./img/cityhunter007.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964216387-DJ3C183EOEN9PZH9Y1U1/cityhunter007.jpg)

[![](./img/cityhunter008.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964216541-2FWOOEF23M5GAA1DDMNZ/cityhunter008.jpg)

[![](./img/cityhunter009.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964216726-A8WX60J6XXGVQG880FC3/cityhunter009.jpg)

[![](./img/cityhunter010.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964216894-0FDD8RLNG0ZA4SG4U7UD/cityhunter010.jpg)

[![](./img/cityhunter011.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964217061-AD1HNSZHKD6Y21RWN1SH/cityhunter011.jpg)

[![](./img/cityhunter012.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964217241-7W4R57BM0J8XI28S7I75/cityhunter012.jpg)

[![](./img/cityhunter013.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964219055-Q81GXKEZU28OWCELGB8Y/cityhunter013.jpg)

[![](./img/cityhunter014.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964218894-CZ46CY9LUMY6XQWZ1TFY/cityhunter014.jpg)

[![](./img/cityhunter015.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964219407-FH4ONAWDSMZBHLZJOQ0P/cityhunter015.jpg)

[![](./img/cityhunter016.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964219905-Z26YSR53ZKJF0GJKBP0Y/cityhunter016.jpg)

[![](./img/cityhunter017.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964221452-NUMF389HE7Q3CYJMB3UI/cityhunter017.jpg)

[![](./img/cityhunter018.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964221451-54D7YFEWH6DFAK47EF2S/cityhunter018.jpg)

[![](./img/cityhunter019.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964225803-C0ZDVT9F26ECH0EGW4A3/cityhunter019.jpg)

[![](./img/cityhunter020.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964226808-J437XD9M5O16HRS3U9KO/cityhunter020.jpg)

[![](./img/cityhunter022.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964230164-CXKUKQ9QXYB19C5PP9E8/cityhunter022.jpg)

[![](./img/cityhunter023.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964231236-DCULQUCTAGMEIR0MGS1R/cityhunter023.jpg)

[![](./img/cityhunter024.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964232278-Y2MFUVLS6NL0VHPVX44A/cityhunter024.jpg)

[![](./img/cityhunter025.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964234274-ULKLHA3QGUQL5UD6DD23/cityhunter025.jpg)

[![](./img/cityhunter026.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964236116-VJQ2WQ3ZFCMHU19QL9JA/cityhunter026.jpg)

[![](./img/cityhunter027.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964237008-09X3YPJN6HRP00DNAUOB/cityhunter027.jpg)

[![](./img/cityhunter028.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964242524-BAPKWH40B2IVI3VTJKMF/cityhunter028.jpg)

[![](./img/cityhunter029.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964242694-CHO4MK867END26ZN64LA/cityhunter029.jpg)

[![](./img/cityhunter034.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964247656-ZR6NVHPE9KP17IMOOL9Q/cityhunter034.jpg)

[![](./img/cityhunter035.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964248162-FCWL1DQFOUXRCCXVW5KW/cityhunter035.jpg)

[![](./img/cityhunter036.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964256590-O7D041B0Z6CK2655PWJY/cityhunter036.jpg)

[![](./img/cityhunter037.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964257206-HCSEC187DLZ2UTMVO5NL/cityhunter037.jpg)

[![](./img/cityhunter038.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964259355-QQTP3RYW3YM4ADBIMBYH/cityhunter038.jpg)

[![](./img/cityhunter039.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964261780-9IM3DUNCXWXSL7O4GSX0/cityhunter039.jpg)

[![](./img/cityhunter040.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964263496-RYH1U5N1ZG4KEM0452AE/cityhunter040.jpg)

[![](./img/cityhunter041.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964263831-JKQ2A2ZSY831GE8HN3HS/cityhunter041.jpg)

[![](./img/City_Hunter_cels_001.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964974148-29073HZQ3XW45F6IM7AW/City_Hunter_cels_001.jpg)

[![](./img/City_Hunter_cels_004.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964974038-5RBBRQOZPFSFROPPKPNM/City_Hunter_cels_004.jpg)

[![](./img/City_Hunter_cels_008.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964974655-6H88CU50G494XKDFIULK/City_Hunter_cels_008.jpg)

[![](./img/City_Hunter_cels_009.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964975111-WFDI8478Z8RDW8QV2W25/City_Hunter_cels_009.jpg)

[![](./img/City_Hunter_cels_024.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964976179-6A8CR8AVNXYEBIDCJFSL/City_Hunter_cels_024.jpg)

[![](./img/City_Hunter_cels_026.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964976133-9QT4VZ15HXD33YWNZ338/City_Hunter_cels_026.jpg)

[![](./img/City_Hunter_cels_034.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964976804-A2JX6VJGPUKKQFTJQ1UK/City_Hunter_cels_034.jpg)

[![](./img/City_Hunter_cels_035.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964976802-JS9FH9YMZSPPSLO0XAHM/City_Hunter_cels_035.jpg)

[![](./img/City_Hunter_cels_055.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964977768-LU148VMA3FL4BIPWVHGW/City_Hunter_cels_055.jpg)

[![](./img/City_Hunter_cels_057.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964977613-NELDFJK2UJOVLPO1ARO9/City_Hunter_cels_057.jpg)

[![](./img/City_Hunter_cels_063.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964977909-TVL8WOT04I65BLTRZDMU/City_Hunter_cels_063.jpg)

[![](./img/City_Hunter_cels_064.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964980258-FPBAPAHEMV4IVRI3YS8G/City_Hunter_cels_064.jpg)

[![](./img/City_Hunter_cels_065.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965041574-K14Z3SZ1SX1V1BHSXHBD/City_Hunter_cels_065.jpg)

[![](./img/City_Hunter_cels_069.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964983246-MZ0UPMJLWUOUXLK29C9U/City_Hunter_cels_069.jpg)

[![](./img/City_Hunter_cels_073.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964983943-2O3J4PPA0K5CKZCMOWZV/City_Hunter_cels_073.jpg)

[![](./img/City_Hunter_cels_074.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964987537-ZS8KCXV5ZVEEYNBUY1D4/City_Hunter_cels_074.jpg)

[![](./img/City_Hunter_cels_082.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964984509-DTLWKOY82ZSE6913AAKE/City_Hunter_cels_082.jpg)

[![](./img/City_Hunter_cels_086.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964985855-1GR07BWNCCYUKY1Y1H84/City_Hunter_cels_086.jpg)

[![](./img/City_Hunter_cels_0105.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965074947-RWA8N3J7EQJEF6R0J06O/City_Hunter_cels_0105.jpg)

[![](./img/City_Hunter_cels_0110.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964988555-3GNLUVY692CYNKR1Y9P7/City_Hunter_cels_0110.jpg)

[![](./img/City_Hunter_cels_0112.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965115038-PIU76M8K7RT9U50FR9YQ/City_Hunter_cels_0112.jpg)

[![](./img/City_Hunter_cels_0137.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964992053-SSJI46M9W4JQQV15QK54/City_Hunter_cels_0137.jpg)

[![](./img/City_Hunter_cels_0138.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964993966-JSDM3IXP0TWCLDF41KB8/City_Hunter_cels_0138.jpg)

[![](./img/City_Hunter_cels_0139.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964993205-7IGKMIWEMI7EHEYMAA2F/City_Hunter_cels_0139.jpg)

[![](./img/City_Hunter_cels_0156.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964994891-W1HJ49XRZT3L3U26DXIL/City_Hunter_cels_0156.jpg)

[![](./img/City_Hunter_cels_0157.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964995041-6545ZFY7XER5O5MI2ZT0/City_Hunter_cels_0157.jpg)

[![](./img/City_Hunter_cels_0162.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964996233-H4QGE43XY0PSDAYMC4EP/City_Hunter_cels_0162.jpg)

[![](./img/City_Hunter_cels_0179.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964996973-FHLMBZLDN0JYCR5SG9C4/City_Hunter_cels_0179.jpg)

[![](./img/City_Hunter_cels_0185.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964996829-ZIB28JY80Z4IQZRB1GVO/City_Hunter_cels_0185.jpg)

[![](./img/City_Hunter_cels_0188.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520964997901-TQHVD7WA8JNBLX3VCTRI/City_Hunter_cels_0188.jpg)

[![](./img/City_Hunter_cels_0193.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965154691-LK3DIX9N762RPGCHMOHR/City_Hunter_cels_0193.jpg)

[![](./img/city-hunter-357-magnum-mokkori-21885.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965316370-S6M8MWPCHRNC1PPLL28J/city-hunter-357-magnum-mokkori-21885.png)

[![](./img/city-hunter-357-magnum-mokkori-31083.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965313672-K8059TJACHWGPZ69NM17/city-hunter-357-magnum-mokkori-31083.png)

[![](./img/city-hunter-357-magnum-mokkori-34961.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965317847-DLSCK6RF3MN0TL46K5Z5/city-hunter-357-magnum-mokkori-34961.png)

[![](./img/city-hunter-357-magnum-mokkori-39048.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965332123-RMBKWX77NB6WRI7E0IBX/city-hunter-357-magnum-mokkori-39048.png)

[![](./img/city-hunter-357-magnum-mokkori-39146.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965332295-QMJWLMVLF9RWSNT3J933/city-hunter-357-magnum-mokkori-39146.png)

[![](./img/city-hunter-357-magnum-mokkori-41748.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965338779-IPQ6AEF4S2IOIP86LNI9/city-hunter-357-magnum-mokkori-41748.png)

[![](./img/city-hunter-357-magnum-mokkori-48245.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965338630-QL0M3PLOH8HOCGENG4BM/city-hunter-357-magnum-mokkori-48245.png)

[![](./img/city-hunter-357-magnum-mokkori-65979.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965344274-TABJPC3YN56UN9PQW8EQ/city-hunter-357-magnum-mokkori-65979.png)

[![](./img/city-hunter-357-magnum-mokkori-73437.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965344127-XWTFR6WZQSNHDTNEC1WD/city-hunter-357-magnum-mokkori-73437.png)

[![](./img/city-hunter-357-magnum-mokkori-74279.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965368645-5DD4K1NBCJT4R1LCW2FG/city-hunter-357-magnum-mokkori-74279.png)

[![](./img/city-hunter-357-magnum-mokkori-74348.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965368939-KR47PRPUHDDJMPD1Q0HW/city-hunter-357-magnum-mokkori-74348.png)

[![](./img/city-hunter-357-magnum-mokkori-75321.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965377683-MR9PZGV14D6E7HHOXJKC/city-hunter-357-magnum-mokkori-75321.png)

[![](./img/city-hunter-357-magnum-mokkori-82166.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965374048-YS62BPP5WZ27IKUI1WLK/city-hunter-357-magnum-mokkori-82166.png)

[![](./img/city-hunter-bay-city-wars-mokkori-1825.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965380520-967K74TKOAFWUMJR3FBA/city-hunter-bay-city-wars-mokkori-1825.jpg)

[![](./img/city-hunter-bay-city-wars-mokkori-15537.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965381637-X2T5K2RXJBCG0KDM21RL/city-hunter-bay-city-wars-mokkori-15537.jpg)

[![](./img/city-hunter-bay-city-wars-mokkori-15612.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965385060-X9R0GFABDMG0NIU7DHHA/city-hunter-bay-city-wars-mokkori-15612.jpg)

[![](./img/city-hunter-bay-city-wars-mokkori-45450.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965391844-38RRR5X3EITEQ1GTS860/city-hunter-bay-city-wars-mokkori-45450.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-5227.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965396171-LGJ8DW74IF0LSNLLVK1U/city-hunter-death-of-evil-ryo-saeba-anime101-5227.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-5352.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965397005-JMLB2JV0FFHC24K24J42/city-hunter-death-of-evil-ryo-saeba-anime101-5352.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-5944.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965400340-XM0M8C4YI7TF588W0UQ4/city-hunter-death-of-evil-ryo-saeba-anime101-5944.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-11826.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965400044-Y4HQSLLR4UV7YLI1XVBO/city-hunter-death-of-evil-ryo-saeba-anime101-11826.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-25257.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965402437-AED1W1YDLBJGM3YDV9T4/city-hunter-death-of-evil-ryo-saeba-anime101-25257.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-32003.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965404249-L01GUMMFGV5FCTVY9IUW/city-hunter-death-of-evil-ryo-saeba-anime101-32003.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-35438.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965407122-KRF9BRBEOXFNCURKSQG6/city-hunter-death-of-evil-ryo-saeba-anime101-35438.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-36747.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965407680-2JS50UUXKDQL1MRRWMC5/city-hunter-death-of-evil-ryo-saeba-anime101-36747.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-46218.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965409712-PA54R92CNGML83L2BYH8/city-hunter-death-of-evil-ryo-saeba-anime101-46218.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-49903.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965409852-H3RG5XA3OQ0AIX51Q9OC/city-hunter-death-of-evil-ryo-saeba-anime101-49903.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-49972.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965416758-I05HG7KJ6TVY92K6U2IN/city-hunter-death-of-evil-ryo-saeba-anime101-49972.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-50010.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965416760-GDUH4MGWVNBVT1AIZIKG/city-hunter-death-of-evil-ryo-saeba-anime101-50010.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-50041.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965423357-KWD2QGX4S112O18QS5GY/city-hunter-death-of-evil-ryo-saeba-anime101-50041.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-52093.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965423055-MPC4FOX5F59X8F7BDD7R/city-hunter-death-of-evil-ryo-saeba-anime101-52093.jpg)

[![](./img/city-hunter-death-of-evil-ryo-saeba-anime101-60975.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965425193-RGF0QW235CD8ZLI4OQGH/city-hunter-death-of-evil-ryo-saeba-anime101-60975.jpg)

[![](./img/city-hunter-goodbye-my-sweetheart-mokkori-16483.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965429263-40JYHL0Y5EDUQ0OF51LZ/city-hunter-goodbye-my-sweetheart-mokkori-16483.png)

[![](./img/city-hunter-goodbye-my-sweetheart-mokkori-17365.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965431666-9J36DGVCTYSYHF0LPZP6/city-hunter-goodbye-my-sweetheart-mokkori-17365.png)

[![](./img/city-hunter-goodbye-my-sweetheart-mokkori-112597.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965432843-93PFM9L3HT65KFP92M0E/city-hunter-goodbye-my-sweetheart-mokkori-112597.png)

[![](./img/city-hunter-goodbye-my-sweetheart-mokkori-112668.png)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965435136-XCSIN0Z9Z7KLRCRQW078/city-hunter-goodbye-my-sweetheart-mokkori-112668.png)

[![](./img/City_Hunter_cels_002.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965561658-SZJ6WKCVBRUEB4N4L5KV/City_Hunter_cels_002.jpg)

[![](./img/City_Hunter_cels_003.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965567327-WOPASQUROB1DP1XKLQHT/City_Hunter_cels_003.jpg)

[![](./img/City_Hunter_cels_019.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965760283-STSQNLHJQJINNGIBQ8NO/City_Hunter_cels_019.jpg)

[![](./img/City_Hunter_cels_011.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965563256-T188Q9KERIJ4GOGLT2UD/City_Hunter_cels_011.jpg)

[![](./img/City_Hunter_cels_032.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965762694-KSZE4BY29HQAW246TCT6/City_Hunter_cels_032.jpg)

[![](./img/City_Hunter_cels_012.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965565693-MYORRGSETU555WFT7UIU/City_Hunter_cels_012.jpg)

[![](./img/City_Hunter_cels_033.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965763620-E9J2LMLCRBJP6LT4Q1CK/City_Hunter_cels_033.jpg)

[![](./img/City_Hunter_cels_014.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965566657-QMYIWPMGAWF4ZD0CEU9R/City_Hunter_cels_014.jpg)

[![](./img/City_Hunter_cels_043.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965769196-2VXXK77J067NWERE44F5/City_Hunter_cels_043.jpg)

[![](./img/City_Hunter_cels_015.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965575619-V1LA98OEMOO8UK85HHAR/City_Hunter_cels_015.jpg)

[![](./img/City_Hunter_cels_049.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965782233-5SXYE6BU77L0MZMQ8M8Q/City_Hunter_cels_049.jpg)

[![](./img/City_Hunter_cels_019.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965578563-6Y77ZOU9ADKXZ9UV67TN/City_Hunter_cels_019.jpg)

[![](./img/City_Hunter_cels_051.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965782530-B2FEGRDWNW6GEF6MX9X6/City_Hunter_cels_051.jpg)

[![](./img/City_Hunter_cels_048.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965576297-2MPR42I0663DLS12KOF4/City_Hunter_cels_048.jpg)

[![](./img/City_Hunter_cels_060.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965579606-OOBRXW76UTI1QW9WOHHU/City_Hunter_cels_060.jpg)

[![](./img/City_Hunter_cels_094.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965784314-7J9954OSTU0WGWI27KAM/City_Hunter_cels_094.jpg)

[![](./img/City_Hunter_cels_079.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965580273-8MGI9K8FLZBDQVNME9WU/City_Hunter_cels_079.jpg)

[![](./img/City_Hunter_cels_0126.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965783866-DHURN0LE15H3OKT8402J/City_Hunter_cels_0126.jpg)

[![](./img/City_Hunter_cels_097.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965591681-L33F0NUZKEBTLJDCIVVR/City_Hunter_cels_097.jpg)

[![](./img/City_Hunter_cels_0127.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965788082-NM3EWYZQVJASG5RHOD6F/City_Hunter_cels_0127.jpg)

[![](./img/City_Hunter_cels_0129.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965588484-ZMD1MD9BSUOA1EAHC2PU/City_Hunter_cels_0129.jpg)

[![](./img/City_Hunter_cels_0135.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965788078-QQI9CEM6K9GSDHEPPW33/City_Hunter_cels_0135.jpg)

[![](./img/City_Hunter_cels_0134.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965591679-IRBC0R7ZJW8HPEJ2N5GI/City_Hunter_cels_0134.jpg)

[![](./img/City_Hunter_cels_0144.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965789750-IZXUFAX576J16KYP7PQA/City_Hunter_cels_0144.jpg)

[![](./img/City_Hunter_cels_0165.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965592934-J03VOWDDX2AVMR9OZEHJ/City_Hunter_cels_0165.jpg)

[![](./img/City_Hunter_cels_0148.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965794661-ZIHUE36WGS09BGIPKKB5/City_Hunter_cels_0148.jpg)

[![](./img/City_Hunter_cels_0166.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965593811-7UZ65J4N2Y6K3L74LV71/City_Hunter_cels_0166.jpg)

[![](./img/City_Hunter_cels_0155.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965794292-2V1BAF2S9AAI8FGF649C/City_Hunter_cels_0155.jpg)

[![](./img/City_Hunter_cels_0199.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965595722-VJ0EWVWDBQ1DMPYWL4M7/City_Hunter_cels_0199.jpg)

[![](./img/City_Hunter_cels_0200.jpg)](https://images.squarespace-cdn.com/content/v1/54fc8146e4b02a22841f4df7/1520965601411-T5V347M1U93324R195H1/City_Hunter_cels_0200.jpg)



* * *

Ready for more?

Discover thousands of model sheets, concept designs, background paintings from the best animation movies and TV series!

[RETURN TO THE MAIN MENU](https://characterdesignreferences.com/art-of-animation/)
