
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


说明：
  
- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
- 人物右脸对应图片左侧，人物左脸对应图片右侧。  
- 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
- 如有错误，请指出，非常感谢！  


# FC的画风-面部-眼睛2 ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96886))  


## 术语   

### 眼睛的结构及其名称[^4] [^5z]：  
![](img/v2-727dfad34165264e812a9c15e216932c_b.jpg) 
![](img/Form-of-the-Head-and-Neck_eye-structure.jpg)  
Cornea: 角膜;  
Pupil: 瞳孔;  
Iris: 虹膜;  
Sclera: 巩膜;  
Eyelid: 眼皮或眼睑；  
Limbus: 角膜边缘；  
Plica semilunaris(of conjunctiva): (结膜的)半月皱襞[^8]；  
Caruncula lacrimalis: 泪阜[^8]； 

注：日常用语中的“眼仁”包括虹膜和瞳孔。  

[^4]: [真实角色渲染----眼睛 - zhihu](https://zhuanlan.zhihu.com/p/73251979)  
[^5z]: Form of the Head and Neck, 2021, Uldis Zarins. page 44.   
[^8]: [IMAIOS中文](https://www.imaios.com/cn/imaios-search/(search_text)/)


<a name="mark"></a>  
### 眼部的mark点[^1]    
资料[1][^1x]中给出的Mark点:  
![](img/BiometricStudyOfEyelidShape...Fig2.jpg)  
(Landmarks or fiducials for our photogrammetric analysis [^1x])   

Rp   = right pupil;(右-瞳孔)  
Lp   = left pupil;(左-瞳孔)  
ML   = most medial point of limbus;(内侧角膜边缘?)  
LL   = most lateral point of limbus;(外侧角膜边缘?)   
Rzy  = right zygion;(右-颧点)  
Lzy  = left zygion;(左-颧点)  
en   = endocanthion;（眼内角点）  
ex   = exocanthion;（眼外角点？）  
Ren  = right endocanthion;（右-眼内角点）  
Len  = left endocanthion;（左-眼内角点）  
ps   = palpebrae superius;(=palpebra superior(上(眼)睑)[^8])  
pi   = palpebrae inferius;(=palpebra inferior(下(眼)睑)[^8])  
Uepi = midpoint between en and ps;(注:图文不符. 图中Uepi更接近en-es的中点)  
Lepi = midpoint between en and pi;  
Ucan = midpoint between ps and ex;(注:图文不符. 图中Ucan不在ps-ex的线段上, 更像是在es-ex的线段上)  
Lcan = midpoint between pi and ex;(注:图文不符. 图中Lcan不在pi-ex的线段上)  
es   = eyelid sulcus;("sulcus"意为"沟"[^8])  
br1  = lowest meeting point of eyebrow to vertical line from en;  
br2  = lowest meeting point of eyebrow to vertical line from Uepi;  
br3  = lowest meeting point of eyebrow to vertical line from ps;  
br4  = lowest meeting point of eyebrow to vertical line from Ucan;  
br5  = lowest meeting point of eyebrow to vertical line from ex;  
al   = most lateral point of alar curvature;(鼻翼外侧点)  
Mb   = most medial point of eyebrow;(眉内侧点)  
Lb   = most lateral point of eyebrow;(眉外侧点)  
Hb   = highest point of eyebrow;(眉高点)  
        
[^1]: Biometric Study of Eyelid Shape and Dimensions of Different Races with References to Beauty. Seung Chul Rhee, Kyoung-Sik Woo, Bongsik Kwon. Aesth Plast Surg (2012), DOI 10.1007/s00266-012-9937-7   
[^1x]: [^1]中的Fig2  

资料[5][^5p40]中给出的Mark点:  
![](img/Form-of-the-Head-and-Neck_eye-mark0.jpg) 
![](img/Form-of-the-Head-and-Neck_eye-mark1.jpg) 

Som = superior-most point on the supraorbital margin(眶上缘[^8]),(猜测:眶上缘高点)  
Iom = inferior-most point on the infraorbital margin,(猜测:眶下缘低点)  
Mom = Flower’s point (medial orbital margin),(猜测:眶缘内侧点)  
Lom = lateral-most point on the lateral orbital margin,(猜测:眶缘外侧点)  
Sg  = superior-most point of the equator of eyeball,(猜测:眼球(赤道)高点)  
Ig  = inferior-most point of the equator of eyeball,(猜测:眼球(赤道)低点)   
Mg  = medial-most point of the meridian of eyeball,(猜测:眼球(经线)内侧点)   
Lg  = lateral-most point of the meridian of eyeball,(猜测:眼球(经线)外侧点)  

P   = pupil center,  
Mc  = medial canthus (the inner angle of the eye, the place where the upper and lower eyelid meet).(内眼角(眼睛的内角，上下眼睑的交汇处)[^5b])  
Lc  = lateral canthus (outer corner or angle of the eye, the place where the upper and lower eyelid meet),(外眼角(眼睛的外角，上下眼睑的交汇处)[^5b])  
Stp = superior tarsal plates.  
Itp = inferior tarsal plates.  
Mct = medial canthal tendon,(猜测:内眼角肌腱)  
Lct = lateral canthal tendon,(猜测:外眼角肌腱)  

注:  
- Mct and Lct are canthal tendons that attach the upper and lower tarsal plates to the orbital margins.  
- Stp and Itp are two comparatively thick, elongated plates of dense connective tissue, about 10 mm (0.39 in) Stp and 5 mm for Itp; one is found in each eyelid, and contributes to its form and support.  

[^5p40]: Form of the Head and Neck, 2021, Uldis Zarins. page 40.   


对于某些似乎相同的眼部位置, 以上两个资料有不同名字的mark点。这里给出可能的对应关系:  

|                     <--- |                                  <--- | <---Mark1[^1x]         | Mark2[^5]---> | --->            | --->   |
|--------------------------|---------------------------------------|------------------------|---------------|-----------------|--------|
| CN                       | NAME                                  | ABBR.                  | ABBR.         | NAME            | CN     |
| 瞳孔,    <br>右~, <br>左~ | pupil,        <br>right ~, <br>left ~ | p,  <br>Rp,  <br>Lp    | P             | pupil           | 瞳孔    |
| 眼内角点, <br>右~, <br>左~ | endocanthion, <br>right ~, <br>left ~ | en, <br>Ren, <br>Len   | Mc            | medial canthus  | 内眼角  |
| 眼外角点, <br>右~, <br>左~ | exocanthion,  <br>right ~, <br>left ~ | ex, <br>Rex, <br>Lex   | Lc            | lateral canthus | 外眼角  |


<a name="reflections"></a>  
### 反光  
考虑到FC中角色眼睛处的线条经常留白，可能是表现高光效果。所以，这里摘录一些相关信息[^5y]。

![](img/Form-of-the-Head-and-Neck_reflections.jpg)  

- Scleral reflection (Sr) is glossy but uneven, due to the bumpy surface of the sclera (i.e. white of the eye).  
巩膜反射(Sr)有光泽但不均匀，因为巩膜表面凹凸不平(即眼白)。  
- Corneal reflection (Cr) is a very glossy reflection. The cornea behaves like a convex spherical mirror.  
角膜反射(Cr)是一种非常光滑的反射。角膜就像一面凸面镜。  
- Limbal reflection (Lr) appears on the boundary between the white sclera and the dark iris.   
角膜边缘反射(Lr)出现在白色巩膜和深色虹膜的交界处。
- Tear fluid reflections (Tfr) is a thin strip of tear fluid called the upper and lower tear menisci, and also along the plica semilunaris.  
泪液反射(Tfr)是一层薄薄的泪液，被称为上半、下半（表面张力形成的）弯液面，也会沿着半月皱襞[^8y]。

[^5y]: Form of the Head and Neck, 2021, Uldis Zarins. page 45.   

### 用一些标准来衡量眼睛的特征   
基于一些参考资料[^2] [^5]，本文采用以下特征及其取值来描述正面的眼睛：  

- Shape[^2]:  
    - Round (虹膜正下方露出眼白)[^2]   
    - Almond (虹膜正下方未露出眼白)[^2]  
- Size[^2]:  
    - Large (眼睛占面部的比例大)[^2]  
    - Small (眼睛占面部的比例小)[^2]  
    - AverageSize[^2]  
- Situation （该参数可同时取多个值）[^2]:  
    - Regular[^5d] （或 Absent epicanthic fold[^5a] 或 Crease(双眼皮)[^2]）   
    - Monolid(单眼皮)[^5b] [^2]  
    - Hooded[^5c]  
    - Sunken[^5c]  
- Intercanthal Distance(内眼角间距)[^5e] (或 Setting(眼间距)[^2]):  
    - Wide[^5e] [^2]  
    - Close[^5e] [^2]  
    - AverageDistance[^5e]（或 Proportional(眼间距等于眼睛宽度(Len点与ex点之间的水平距离))[^2])    
- Canthal tilt (Palpebral Fissure Inclination)(眼角的倾斜(睑裂倾斜))[^5f]  (或 Position[^2] ):  
    - Upnslanted[^5f] （或 Upturned[^2]）  
    - Idealized[^5f]  
    - Straight[^5f] [^2]  
    - Downslanted[^5f] （或 Downturned[^2]）  

例如：若将某角色的眼睛标记为Almond/AverageSize/Monolid/Wide/Straight，则表示其眼睛Shape值为Almond，Size值为AverageSize, Situation值为Monolid，Setting值为Wide，Position值为Straight。  


#### 关于Shape  
很多资料提及眼睛的形状，例如，Round[^2]、Almond[^2]等等，但参考资料5[^5]中没有提及人眼形状的上述分类。考虑到其作者有人体雕塑的专业背景，该书似乎应该比较权威。所以，笔者猜测，"将真实的人眼的形状分为Round、Almond等"的做法可能没有太大意义。  

但漫画作品中，不同角色的眼睛的形状有明显差异，似乎可以分为Round、Almond。所以，本文采用Shape作为眼睛的一个特征。  

[^5]: Form of the Head and Neck, 2021, Uldis Zarins.  


#### 关于Size  
多数漫画角色为了显得可爱，而将眼睛画得很大。


#### 关于Situation
<a name="monolid"></a>  
##### 关于其值Monolid[^5b]（也被称为Single Eyelid[^5e]）(内眦眼皮/单眼皮)  
"Epicanthic fold (Monolid eye)  
An epicanthic fold or epicanthus is the eye cover fold extended downward and partially or fully covering the upper eyelid and inner corner of the eye. The highest frequency of occurrence of epicanthic folds is found in specific ethnicities: East Asians, Southeast Asians, Central Asians, North Asians. In European populations, epicanthic folds occur at a considerably lower frequency."[^5a]  
（ 内眦赘皮(单眼皮)  
内眦赘皮或epicanthus是向下延伸并部分或完全覆盖上眼睑和内眼角的眼罩。 内眦赘皮的发生频率最高的是特定的种族:东亚人、东南亚人、中亚人、北亚人。在欧洲人群中，内眦赘皮发生的频率要低得多。[^5b]）  

Higher Degree Monolid eye(高内眦眼皮)[^5a]:  
![](img/epicanthic-fold-higher-degree.jpg)  
Lower Degree Monolid eye(低内眦眼皮)[^5a]:  
![](img/epicanthic-fold-lower-degree.jpg)  
Absent Monolid eye(无内眦眼皮)[^5a]。  
![](img/epicanthic-fold-absent.jpg)  

内眦眼皮与无内眦眼皮在侧面的差异[^5e]：   
"When the eye is closed, the upper eyelid skin crease usually matches with the edge of the palpebral plate. However, approximately 50% of the Asian population do not have upper lid skin creases, often referred to as the “single eyelid.” (see Epicanthic fold)"  
（闭上眼睛时，上眼皮皱褶通常与眼睑板边缘相匹配。然而，大约50%的亚洲人没有上眼皮皱褶，通常被称为“单眼皮”。(参见内眦赘皮)）  
如下图，upper eyelid skin crease, Asian (single eyelid)(左一)对比European(左二)：  
![](img/Form-of-the-Head-and-Neck_upper-eyelid-skin-crease_Asian.jpg) 
![](img/Form-of-the-Head-and-Neck_upper-eyelid-skin-crease_European.jpg) 

[^5a]: Form of the Head and Neck, 2021, Uldis Zarins. page 34.   
[^5b]: [有道翻译](https://fanyi.youdao.com/index.html)  
[^5e]: Form of the Head and Neck, 2021, Uldis Zarins. Page 47.   


##### 关于其值Hooded、Sunken  
因为网上的若干资料对Hooded、Sunken的区分方法不完全一致（例如，资料2[^2]和3[^3]对Hooded的解释不一致，资料5[^5]与6[^6]一致），故笔者不确定该如何选择。考虑到参考资料5[^5]可能更权威，故本文采用其方法。  

关于Hooded(耷拉眼皮)[^5c]:  
Lateral hooding and eyebrow fullness.Lateral hooding is related to medical disorders
or muscle overwork and an excess lateral upper eyelid skin and fat (1) causing sagging
eyebrows, droopy upper lids and usually goes together with bags under eyes.

关于Sunken(眼窝凹陷/上眼眶凹陷) [^5c] [^7]:  
参考资料5[^5]的解释：Sunken eyes can cause decreased orbital adipose tissue, "soft tissue deflation" in the periorbital area, deflation of brow fat, revealing upper orbital margin ... When the cause of sunken eyes is aging and lack of collagen in the skin or dehydration, features like prominent lower eyelid (3) and tear trough become prominent. The tear trough depression (4) is an important feature of eyelid and midface aging. Sunken eyes also go by other names, including “tear trough hollows” or “under-eye hollows.”  
(上眼眶凹陷的眼窝脂肪少、眼周“软组织紧缩”、眉毛处脂肪减少、能看到上眶缘 ... 当上眼眶凹陷的原因是衰老、皮肤胶原蛋白缺乏或脱水时，下眼睑突出(3)和泪槽等特征就会突出。泪沟凹陷(4)是眼睑和中脸衰老的重要特征。上眼眶凹陷也被称为“泪沟”或“眼下部凹陷”)。  

对比Hooded, Regular, Sunken[^5d]：  
![](img/hooded0.jpg) ![](img/regular0.jpg) ![](img/sunken0.jpg)
![](img/hooded1.jpg) ![](img/regular1.jpg) ![](img/sunken1.jpg)

[^5c]: Form of the Head and Neck, 2021, Uldis Zarins. page 32.   
[^5d]: Form of the Head and Neck, 2021, Uldis Zarins. page 33.   
[^7]: [How To Get Rid Of Sunken Eyes - RecaBlog](https://www.recablog.com/how-to-get-rid-of-sunken-eyes/)  


#### 关于Intercanthal Distance（内眼角间距）

"For most people, the inner intercanthal distance is roughly equal to the distance between the inner corner and the outer corner of each eye, that is, the width of the eye. The situation where inner intercanthal distance is intensely bigger than the width of the eye is called wide-set eyes. If intercanthal distance is less than one eye length in size, it is close-set eyes."[^5g]  
(对于大多数人来说，内眼角间距大致等于每只眼睛的内眼角与外眼角的间距，即眼睛的宽度。内眼角间距比眼宽大得多的情况称为wide-set。如果内眼角间距小于一只眼睛的宽度，则为close-set。“)

[^5g]: Form of the Head and Neck, 2021, Uldis Zarins. page 32.   

#### 关于Canthal tilt (Palpebral Fissure Inclination)(眼角的倾斜(睑裂倾斜))
对其值的说明[^5f]：  

- Upnslanted: Lateral canthus lie higher than the medial canthus. Common in East Asian populations. Tilt up to 10 degrees from Fp, it is also aversient of positive canthal tilt.  
（上倾型：外眼角高于内眼角。在东亚人群中很常见。与FP成10度的倾斜，这有别于正眼角倾斜。）  
- Idealized: Lateral canthus (Lc) lie slightly higher than the medial canthus (Mc), it is called the positive canthal tilt.  
（理想型：外眼角(Lc)略高于内眼角(Mc)，称为正眼角倾斜。）  
- Straight: Both medial and lateral canthi are at the same level, it is called the neutral canthal tilt.  
（平直型：内、外眼角水平，称为中性眼角倾斜。）  
- Downslanted: Lateral canthus lie lower than the medial canthus, it is called the negative canthal tilt.  
（下倾型：外眼角低于内眼角，称为负眼角倾斜。）  

如下图所示[^5f]：  
![](./img/canthal-tilt_upnslanted.jpg)  
![](./img/canthal-tilt_idealized.jpg)  
![](./img/canthal-tilt_straight.jpg)  
![](./img/canthal-tilt_downslanted.jpg)  

[^2]: [The 5 Golden Metrics for Your True Eye Shape](https://www.kendrapowell.com/blog/the-5-golden-metrics-for-your-true-eye-shape)  
[^5e]: Form of the Head and Neck, 2021, Uldis Zarins. page 42.   
[^5f]: Form of the Head and Neck, 2021, Uldis Zarins. page 43.   


## FC角色眼睛的一些特点，及其对比方法  
目前观察到FC里角色的眼睛有如下特点：  

### 特点1  
若把眼睛横向分为"内侧(Inner)"、"中间(Middle)"、"外侧(Outer)"三部分的话（如下图所示），  
![](img/BiometricStudyOfEyelidShape...Fig2_mod.jpg)    
那么，FC里有些角色的眼睛内侧竖向宽度大于外侧竖向宽度(熏)、有些内侧竖向宽度小于外侧竖向宽度(早纪)、有些内侧竖向宽度等于外侧竖向宽度(雅彦)：  
![](img/eye_front_200p/eye_front_kaoru.jpg) 
![](img/eye_front_200p/eye_front_saki.jpg) 
![](img/eye_front_200p/eye_front_masahiko.jpg)  
针对这种情况：  

- 似乎可以用线段"Uepi-Lepi"(上图中蓝色竖线)衡量内侧(竖向)宽度；  
- 似乎可以用线段"Ucan-Lcan"(上图中蓝色竖线)衡量外侧(竖向)宽度。  

### 特点2
上文提到眼睛的形状包含Round和Almond。笔者的理解是：Round眼睛的竖向宽度大、Almond眼睛的竖向宽度小(眼睛显得细长)。  

因为观察到：FC里，有些角色的眼睛虽同为Round形，但仍有差异。例如，真琴(Makoto)的眼睛比叶子(Yoko)的眼睛更圆(竖向宽度更大)；虽同为Almond形，但角色紫(Yukari)的眼睛比雅彦(Masahiko)的眼睛更细长(竖向宽度更小)。  
![](img/eye_front_200p/eye_front_makoto.jpg) 
![](img/eye_front_200p/eye_front_yoko.jpg)   
![](img/eye_front_200p/eye_front_yukari.jpg) 
![](img/eye_front_200p/eye_front_masahiko.jpg)    
因此，并进一步细化其值：   

- RoundN, (N=1, 2, 3, ...)  
表示Round的程度为N。例如，Round3 > Round2 > Round1(=Round)表示眼睛“圆(眼睛竖向宽度)的程度”依次递减。    
- AlmondN，(N=1, 2, 3, ...)  
表示Almond的程度为N。例如，Almond3 > Almond2 > Almond1(=Almond)表示眼睛“细长的程度”依次递减。  

### 特点3
FC里很多男性角色的眼内侧、外侧的双眼皮明显，但眼中部的双眼皮不明显。    
![](img/eye_front_200p/eye_front_masahiko.jpg) 
![](img/eye_front_200p/eye_front_sora.jpg) 
![](img/eye_front_200p/eye_front_ejima.jpg) 
![](img/eye_front_200p/eye_front_tatsumi.jpg) 
![](img/eye_front_200p/eye_front_susumu.jpg) 
![](img/eye_front_200p/eye_front_nishina.jpg) 
![](img/eye_front_200p/eye_front_fumiya.jpg) 
![](img/eye_front_200p/eye_front_matsu.jpg) 
![](img/eye_front_200p/eye_front_kazuki.jpg) 
![](img/eye_front_200p/eye_front_kimihiro.jpg) 

这可能是表示下图的眼睛[^5]：  
![](./img/=-=1.jpg) ![](./img/=-=0.jpg) ![](./img/eyebrow-male0.jpg) ![](./img/=-=2.jpg)   
这应该不是Hooded，因为Hooded是眼外侧的双眼皮被遮挡。这可能是上述的Regular[^5d] （或 Absent epicanthic fold[^5a] 或 Crease(双眼皮)[^2]）。但终因笔者不十分确定如何对其分类, 所以暂时用符号"=-="代替之。例如：如果某角色的眼睛标记为Almond/AverageSize/=-=/Wide/Straight，则表示其眼睛Situation值为=-=。  

### 特点4
在FC里很常见的一种情况是某角色的左、右眼不对称，例如：  

- [内、外眼角不对称](../fc_drawing_style__face_eye/readme.md#asymmetry0)  
- [左右眼高度不同](../fc_drawing_style__face_eye/readme.md#asymmetry1)  

对此，本文假设：角色的左眼和右眼对称。在此假设下，当角色的左、右眼都便于本文使用时，则选择其左眼（因为多数情况下角色发型向右脸偏，所以左眼完全被显露）；当其左眉不便使用时（例如，被部分遮挡），则将其右眼水平反转为左眼后再使用。（注：角色自身的左侧、右侧，即分别对应画面的右侧和左侧，以下同。）  
因为后续工作基于上述该假设，所以本文只能算是一个粗糙的比较。  

### 特点5
对于眼仁(虹膜)，有些眼睛的眼仁大(真琴)、有些眼睛的眼仁小(江岛)：  
![](img/eye_front_200p/eye_front_makoto.jpg) 
![](img/eye_front_200p/eye_front_ejima.jpg)  
同一角色的眼仁有时大、有时小(辰巳)：  
![](img/eye_front_tatsumi_03_103_4.jpg) 
![](img/eye_front_tatsumi_06_027_3.jpg)  

### 特点6
FC的画风遵循人体素描的比例（包括面部），所以Size值应该都为AverageSize。

### 特点7 
关于Canthal tilt (Palpebral Fissure Inclination)(眼角的倾斜(睑裂倾斜))，FC的画风似乎没有细致到能区分Idealized和Straight的程度。  


------

## 应用：分析FC中部分角色的眼睛  

### 雅彦(Masahiko)-正面    
- basis:  
    - 图片: 11_129_3，13_116_3，13_116_4，13_118_6，13_139_1，14_283_4，      
    - 特点： 眼睛类型应该是Almond/AverageSize/Regular/Wide/Straight。    
    - 叠加：  
        ![](./img/eye_front_masahiko_basis.png)  


### 雅美(Masami)(雅彦女装)-正面    
- o+1:  
    - 图片: (07_178_2), 03_060_1, 03_027_3, 03_027_1,  
    - 特点：。    
    - 叠加：  
        ![](./img/eye_front_masami_o+1.png)  

- o0:  
    - 图片: 07_048_0,  
    - 特点：和雅彦basis几乎一致，睫毛长(或眼影重)。Regular。    
    - 叠加效果；该组mark(红色)对比雅彦basis的mark(蓝色)。 比雅彦basis稍大：  
        ![](./img/eye_front_masami_o0.png) 
        ![](./img/eye_mark_masami_o0_vs_masahiko_basis.jpg)  
        
- o-1:  
    - 图片: 14_163_0, 03_031_2,  
    - 特点：。    
    - 叠加：  
        ![](./img/eye_front_masami_o-1.png)  

- o-2:  
    - 图片: 04_141_0, 03_114_0,  
    - 特点：。    
    - 叠加：  
        ![](./img/eye_front_masami_o-2.png)  

- o-3:  
    - 图片: 12_054_4, 06_155_2,  
    - 特点：。    
    - 叠加：  
        ![](./img/eye_front_masami_o-3.png)  

- o-5(close):  
    - 图片: 14_246_3,  
    - 特点：。    
    - 叠加：  
        ![](./img/eye_front_masami_o-5(close).png)  


### 紫苑(Shion)-正面    
首先，按照眼睛睁开大小分组(睁开程度由大到小)：  

- o+2:  
    - 图片: 01_161_2,02_009_5,10_172_1,(03_145_4), 06_065_0,07_188_0,08_165_4,(07_097_0),03_094_2,    
    - 特点：o+2. 。    
    - 叠加：  
        ![](./img/eye_front_shion_o+2.png)  
    
- o+1.5:  
    - 图片: 02_079_1,01_042_0,      
    - 特点：o+1.5. 。    
    - 叠加：  
        ![](./img/eye_front_shion_o+1.5.png)  

- o+1:  
    - 图片: 14_281_5,03_003_0,04_139_5,04_015_5,12_104_1,14_281_6,08_096_2,       
    - 特点：o+1.5. 。    
    - 叠加：  
        ![](./img/eye_front_shion_o+1.png)  

- o0:  
    - 图片: 14_280_6; 14_122_0,12_104_3,09_083_2,10_008_6,07_029_4,05_089_2,04_189_5; 02_119_6,01_150_7;   
    - 特点：Regular。      
    - 叠加：  
        ![](./img/eye_front_shion_o0.png)  

- o-0.5:  
    - 图片: 02_001, 11_133_2,14_283_5,        
    - 特点：o-0.5. 。    
    - 叠加：  
        ![](./img/eye_front_shion_o-0.5.png)  

- o-1:  
    - 图片: 04_064_6, 03_062_5, 10_073_1,01_047_0, 11_043_7        
    - 特点：o-1. 。    
    - 叠加：  
        ![](./img/eye_front_shion_o-1.png)  

- o-2:  
    - 图片: 08_176_6, 03_195_0, 14_282_1        
    - 特点：o-2. 。    
    - 叠加：  
        ![](./img/eye_front_shion_o-2.png)  
    

其次，按眼睛睁大程度排列上述组，形成动画：  
![](./img/eye_front_shion_o-3-o+2_peek.gif)  


最后，进一步细分o0组：  

- o0-almond:  
    - 图片: 14_122_0,12_104_3,09_083_2,10_008_6,07_029_4,05_089_2,04_189_5        
    - 特点：Almond/Regular。    
    - 为该组设置mark(红色)，并对比雅彦basis的mark(蓝色)。两者几乎一致；虹膜比雅彦basis稍大，眼影重(睫毛浓?)：  
        ![](./img/eye_front_shion_o0_almond.png) 
        ![](./img/eye_mark_shion_o0-almond_vs_masahiko_basis.jpg)  
    - mark点没能体现出一个特点："紫苑o0上眼皮线"比"雅彦basis上眼皮线"更圆滑。  

- o0-round:  
    - 图片: 02_119_6,01_150_7        
    - 特点：Round/Regular/Wide。集中出现在FC前两卷。    
    - 为该组设置mark(红色)，并对比雅彦basis的mark(蓝色)：  
        ![](./img/eye_front_shion_o0_round.png) 
        ![](./img/eye_mark_shion_o0-round_vs_masahiko_basis.jpg)  
        ![](./img/01_150_7__mod.jpg)  
            

### 塩谷/盐谷(Shionoya)(紫苑男装)-正面    

- o0:  
    - 图片: 14_067_4(内外侧双眼皮(外侧不明显)), 12_150_4(内外侧双眼皮), 12_143_0(内侧双眼皮), 10_125_0(内外侧双眼皮),  
    - 特点：和雅彦basis几乎一致。上眼皮线比雅彦basis更圆滑。Almond/Regular。  
    - 叠加效果；该组mark(红色)对比雅彦basis的mark(蓝色)、对比紫苑o0_almond的mark(蓝色)：   
        ![](./img/eye_front_shya_o0.png) 
        ![](./img/eye_mark_shya_o0_vs_masahiko_basis.jpg) 
        ![](./img/eye_mark_shya_o0_vs_shion_o0_almond.jpg)  
                    
- o-2:  
    - 图片: 12_151_4, 12_158_6,  
    - 特点：。    
    - 叠加：  
        ![](./img/eye_front_shya_o-2.png)  


### 若苗空(Sora)-正面    

- o0:  
    - 图片: 02_000a, 02_011_1,  
    - 特点：o-0. 眼宽度(内眼角(Len)-外眼角(ex)距离)稍小。眼睛类型应该是Regular。    
    - 叠加：  
        ![](./img/eye_front_sora_o0.png)  

- o-1(basis):  
    - 图片: 08_063_3, 06_135_2, 06_111_0, 04_039_0, 02_191_3, 01_152_0, 01_00a_0,  
    - 特点： Almond2/Regular, 比雅彦basis的眼睛细长, 内侧略宽于外侧。  
    - 叠加效果；该组mark(红色)对比雅彦basis的mark(蓝色)：    
        ![](./img/eye_front_sora_o-1.png) 
        ![](./img/eye_mark_sora_o-1_vs_masahiko_basis.jpg)  

- o-2:  
    - 图片: 05_059_0, 04_015_0,  
    - 特点：o-2  
    - 叠加：  
        ![](./img/eye_front_sora_o-2.png)  


### 若苗紫(Yukari)-正面    

- o+2:  
    - 图片: 05_092_2, 05_050_6,  
    - 特点：o+2。    
    - 叠加：  
        ![](./img/eye_front_yukari_o+2.png)  

- o+1:  
    - 图片: 06_092_6,  
    - 特点：o+1。    
    - 叠加：  
        ![](./img/eye_front_yukari_o+1.png)  

- o0:  
    - 图片: 10_057_0, 09_099_2, 08_011_5, (07_059_3), 06_111_0, 05_174_0, 05_064_2, 02_189_0, 02_004_4,  
    - 特点：o0。Almond3/Regular。    
    - 叠加：  
        ![](./img/eye_front_yukari_o0.png)  

- o-1:  
    - 图片: 10_055_4, 09_155_5, 09_097_6, 08_066_3, 06_135_2, 05_042_1, 01_124_1,   
    - 特点：o-1。    
    - 叠加：  
        ![](./img/eye_front_yukari_o-1.png)  

- o-2(basis):  
    - 图片: 10_048_7, 10_035_3, 10_020_4, 09_155_6, 08_017_5, 05_183_6, 04_062_4, 04_022_2, 04_000a_0,03_087_0, 03_025_4, 02_000a, 01_200_4, 01_025_4, 01_005_0,    
    - 特点：成组的图片数量最多, 选作basis。Almond2/Regular，内侧双眼皮；     
    - 叠加效果；该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/eye_front_yukari_o-2.png) 
        ![](./img/eye_mark_yukari_o-2_vs_masahiko_basis.jpg)  

- o-2.5:  
    - 图片: 01_030_0,  
    - 特点：o-2.5。    
    - 叠加：  
        ![](./img/eye_front_yukari_o-2.5.png)  

- o-3:  
    - 图片: 01_043_0, 01_012_3, 01_006_1, mama(01_025_4, 02_027_2),  
    - 特点：o-3, 均出现在FC前期。    
    - 叠加：  
        ![](./img/eye_front_yukari_o-3.png)  


### 浅冈叶子(Yoko)-正面    
- o+2.5:  
    - 图片: 13_125_3, 13_101_3, 04_139_5, 03_056_0(?)  
    - 特点：虽然成组图片数量最多，但眼睛明显是睁大，所以该组不适合作为basis。    
    - 叠加：  
        ![](./img/eye_front_yoko_o+2.5.png)  

- o+2:  
    - 图片: (14_191_1), 11_019_4, 08_160_3, 08_153_3, 04_136_3, 02_092_5, 02_088_2, 02_072_6, 02_065_0, 
    - 特点：虽然成组图片数量最多，但眼睛明显是睁大，所以该组不适合作为basis。    
    - 叠加：  
        ![](./img/eye_front_yoko_o+2.png)  

- o+1.5:  
    - 图片: 05_142_1，06_007_3，03_008_0  
    - 特点：o+1.5。    
    - 叠加：  
        ![](./img/eye_front_yoko_o+1.5.png)  

- o+1(basis):  
    - 图片: 13_172_0, 13_154_2, 13_142_1, 05_097_5,  
    - 特点：成组图片数量多，选作basis。Round2/Monolid。    
    - 叠加效果；该组mark(红色)对比雅彦basis的mark(蓝色)：  
        ![](./img/eye_front_yoko_o+1(basis).png) 
        ![](./img/eye_mark_yoko_o+1(basis)_vs_masahiko_basis.jpg)  

- o0:  
    - 图片: 06_060_3, 03_041_2  
    - 特点：o0。类型是Hooded。和雅彦basis眼睛一致。    
    - 叠加：  
        ![](./img/eye_front_yoko_o0.png)  

- o-1:  
    - 图片: 04_178_0, 08_152_6,  
    - 特点：o-1。    
    - 叠加：  
        ![](./img/eye_front_yoko_o-1.png)  

- o-2:  
    - 图片: 05_160_2, 04_161_0  
    - 特点：o-2。    
    - 叠加：  
        ![](./img/eye_front_yoko_o-2.png)  

- o-5:  
    - 图片: 13_122_2, 13_121_2, 13_116_3,  
    - 特点：o-5。    
    - 叠加：  
        ![](./img/eye_front_yoko_o-5.png)  


### 浩美(Hiromi)-正面    
- o+2:  
    - 图片: (09_042_4), 01_191_5, 09_043_4,  
    - 特点：o-5。    
    - 叠加：  
        ![](./img/eye_front_hiromi_o+2.png)  

- o+1:  
    - 图片: 09_047_1, 09_037_5, 09_046_6,  
    - 特点：o+1。    
    - 叠加：  
        ![](./img/eye_front_hiromi_o+1.png)  

- o-1(basis):  
    - 图片: 07_106_1, 06_103_2, 05_163_0, 05_130_2, 05_125_5,  
    - 特点：成组的数量最多。Almond3/Sunken。  
    - 叠加效果；该组mark(红色)对比紫o-2的mark(蓝色):  
        ![](./img/eye_front_hiromi_o-1(basis).png) 
        ![](./img/eye_mark_hiromi_o-1(basis)_vs_yukari_o-2.jpg)  

- o-3:  
    - 图片: 01_188_3, 09_058_0, 09_036_0,  
    - 特点：。  
    - 叠加：  
        ![](./img/eye_front_hiromi_o-3.png)  

- o-5(close):  
    - 图片: 03_008_3, 04_091_2,  
    - 特点：。  
    - 叠加：  
        ![](./img/eye_front_hiromi_o-5(close).png)  


### 熏(Kaoru)-正面    

- o+2:  
    - 图片: 07_197_0, 13_018_1, 10_024_3, 08_127_0, 07_152_3,  
    - 特点：。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o+2.png)  

- o+1:  
    - 图片: 07_192_6, 08_072_5, 13_011_5, 11_105_3;08_013_4,07_193_5; 10_045_4,13_075_2;    
    - 特点：分三组，眼睛有少许差异。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o+1.png) 
        ![](./img/eye_front_kaoru_o+1(2).png) 
        ![](./img/eye_front_kaoru_o+1(3).png)  

- o0:  
    - 图片: 13_076_1, 13_075_6, 09_104_0,   
    - 特点：选作basis。Almond2/=-=/Downslanted。  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_kaoru_o0.png) 
        ![](./img/eye_mark_kaoru_o0_vs_masahiko_basis.jpg)  

- o-0.5:  
    - 图片: 08_074_5, 08_036_5, 13_031_0, 12_190_7,   
    - 特点：图片表情强烈(导致眼部有变形?)，不适合作basis。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o-0.5.png)  

- o-1:  
    - 图片: 14_263_4, 11_114_6, 08_076_5, 08_062_2  
    - 特点：眯眼睛，不适合作basis。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o-1.png)  

- o-1.5:  
    - 图片: 13_022_2, 13_018_3, 13_082_1, 09_158_6, 13_053_6,   
    - 特点：眯眼睛，不适合作basis。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o-1.5.png)  

- o-1.6:  
    - 图片: 13_042_0, 13_029_3, 13_010_1, 13_010_2, 12_171_0, (08_071_1), 07_161_6,   
    - 特点：眯眼睛，不适合作basis。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o-1.6.png)  

- o-2:  
    - 图片: 11_045_1, 11_044_5, 09_101_3, 08_158_4,   
    - 特点：。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o-2.png)  

- o-5(close):  
    - 图片: 13_011_1, 08_173_2，  
    - 特点：闭眼，眼皮线向下弯，睫毛向下。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o-5(close).png)  

- o-5(close-amuse):  
    - 图片: 08_035_5, 10_124_6, 08_004_0，  
    - 特点：闭眼（笑），眼皮线向上弯，睫毛向上。  
    - 叠加：  
        ![](./img/eye_front_kaoru_o-5(close-amuse).png)  


### 江岛(Ejima)-正面    
- o0:  
    - 图片: 12_151_5, 05_012_0,03_004_4,   
    - 特点：选作basis。Almond/Regular, 眼睛内侧宽度略大于外侧宽度。  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_ejima_o0.png) 
        ![](./img/eye_mark_ejima_o0_vs_masahiko_basis.jpg)  

- o-1:  
    - 图片: 10_075_5, 12_140_5,  
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_ejima_o-1.png)  

- close:  
    - 图片: 12_061_1, 06_144_4,  
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_ejima_close.png)  

- close(amuse):  
    - 图片: 10_128_6, 05_025_0, 05_015_0, 10_123_1, 04_096_6, 04_103_2, 01_108_4, 12_069_0,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_ejima_close(amuse).png)  


### 辰巳(Tatsumi)-正面    
- o+1:  
    - 图片: 09_096_6, 13_074_4,07_184_0,09_110_1  
    - 特点： Hooded?  
    - 叠加：  
        ![](./img/eye_front_tatsumi_o+1.png)  

- o0:  
    - 图片: 09_093_7, 09_134_6, 09_097_2, 06_027_3,   
    - 特点：  Almond/=-=/ (Regular?) .  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色)。眼皮比雅彦basis低:  
        ![](./img/eye_front_tatsumi_o0.png) 
        ![](./img/eye_mark_tatsumi_o0_vs_masahiko_basis.jpg)  

- o0(2):  
    - 图片: 04_163_4, 08_048_2,  
    - 特点：眼皮和o0大小一致，但眼仁小。  
    - 叠加：  
        ![](./img/eye_front_tatsumi_o0(2).png)  

- o-2:  
    - 图片: 10_049_7_, 03_136_3  
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_tatsumi_o-2.png)  

- anger:  
    - 图片: 09_142_6，06_028_4，12_187_1，06_041_6，06_072_6，03_127_0，03_122_3，03_122_0，06_073_5，03_103_4   
    - 特点：怒。不同图片之间有明显差异。  
    - 叠加：  
        ![](./img/eye_front_tatsumi_anger.png)  

- close:  
    - 图片: 11_113_1, 04_139_4，03_125_5  
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_tatsumi_close.png)  

- close(amuse):  
    - 图片: 06_026_5, 06_041_0，04_135_6   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_tatsumi_close(amuse).png)  
<a name="TatsumiHoodedGroup"></a>  
- hooded:  
    - 图片: 09_086_0, 09_097_2, 09_098_3, 09_098_6, 09_106_4, 09_112_4, 09_135_2, 09_141_0,  
    - 特点：  Hooded  
        ![](./img/09_086_0__hooded.jpg) 
        ![](./img/09_097_2__hooded.jpg) 
        ![](./img/09_098_3__hooded.jpg) 
        ![](./img/09_098_6__hooded.jpg) 
        ![](./img/09_106_4__hooded.jpg) 
        ![](./img/09_112_4__hooded.jpg) 
        ![](./img/09_135_2__hooded.jpg) 


### 导演-正面    
角色眼睛被墨镜遮挡，无法处理。  


### 早纪(Saki)-正面    
- o+2:  
    - 图片: 10_049_6__r, 10_017_5__r, 09_193_1__r, 09_185_1, 09_175_7, 09_175_1, 09_152_4__r, 09_152_3,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_saki_o+2.png)  

- o0:  
    - 图片: 09_189_0, 10_017_4,   
    - 特点： Almond/=-=，内侧宽度小于外侧宽度。   
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_saki_o0.png) 
        ![](./img/eye_mark_saki_o0_vs_masahiko_basis.jpg)  

- o-1:  
    - 图片: 09_153_3, 11_113_1, (10_042_5),10_040_4, 09_176_2,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_saki_o-1.png)  

- o-2(2):  
    - 图片: 09_176_3, 10_019_2,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_saki_o-2(2).png)  

- o-2:  
    - 图片: 11_114_0__r, 10_011_2__r, 10_010_1__r, 10_009_1, 09_188_4__r, 09_182_4,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_saki_o-2.png)  

- close:  
    - 图片: 10_010_5, 11_113_2, 10_043_1,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_saki_close.png)  

- close(amuse):  
    - 图片: 09_181_1,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_saki_close(amuse).png)  

- 10_054_0, 镜头仰视，眼角偏下。符合实际。  


### 真琴(Makoto)-正面    

- o+5:  
    - 图片: 02_081_1, 09_033_6, 05_147_0,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_makoto_o+5.png)  

- o+3:  
    - 图片: 08_046_5, 07_063_4, 08_110_0,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_makoto_o+3.png)  

- o0:  
    - 图片: 07_097_0, 07_075_5, 03_013_0,   
    - 特点：  Round3/Regular或Monolid或=-=.  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_makoto_o0.png) 
        ![](./img/eye_mark_makoto_o0_vs_masahiko_basis.jpg)  

- o-3:  
    - 图片: 07_108_1, 07_072_0,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_makoto_o-3.png)  

- close(amuse):  
    - 图片: 11_035_0, 09_056_6, 08_125_5, 07_112_5, 07_051_1, 06_103_2, 06_005_6, (05_163_0), 04_176_1, 03_021_2, 01_063_0,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_makoto_close(amuse).png)  


### 摄像师-正面    
没有合适的镜头。  

- anger:  
    - 图片: 03_128_4, 03_124_0,   
    - 特点：怒。    
    - 叠加：  
        ![](./img/eye_front_cameraman_anger.png)  

- o+3:  
    - 图片: 04_126_4,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_cameraman_o+3.png)  

- o+2:  
    - 图片: 14_171_4,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_cameraman_o+2.png)  

- o+2(down):  
    - 图片: 14_171_1, 14_194_6, 03_096_2, (07_104_1),   
    - 特点：相机位于下方。    
    - 叠加：  
        ![](./img/eye_front_cameraman_o+2(down).png)  

- o-1:  
    - 图片: 12_163_6,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_cameraman_o-1.png)  

- o-2:  
    - 图片: 14_193_1,   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_cameraman_o-2.png)  

- o-3:  
    - 图片: 03_070_4, 10_063_1，14_014_0，(10_127_1), 12_148_0,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_cameraman_o-3.png)  

- close:  
    - 图片: 14_036_4, 10_117_1   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_cameraman_close.png)  

- close(amuse):  
    - 图片: 12_157_5, 03_098_4, (03_098_1), 10_067_3,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_cameraman_close(amuse).png)  


### 浅葱(Asagi)-正面    
- o+2:  
    - 图片: 14_250_1, 14_123_0,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_asagi_o+2.png)  

- o+1:  
    - 图片: 14_234_0, 14_067_6,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_asagi_o+1.png)  

- o0:  
    - 图片: 14_057_0, 14_039_0,     
    - 特点：  Round2/Monolid。  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色)。和雅彦的眼睛几乎一致(浅葱上眼皮高，所以眼睛稍大)。:  
        ![](./img/eye_front_asagi_o0.png) 
        ![](./img/eye_mark_asagi_o0_vs_masahiko_basis.jpg)  

- o-1:  
    - 图片: 14_197_0_r, 14_196_1, 14_193_1,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_asagi_o-1.png)  

- o-2:  
    - 图片: 14_074_2, 14_073_7,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_asagi_o-2.png)  

- close:  
    - 图片: 14_135_6, 14_185_2, 14_169_5   
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_asagi_close.png)  

- close(amuse):  
    - 图片: 14_089_6,    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_asagi_close(amuse).png)  


### 和子(Kazuko)-正面    
- o+2:  
    - 图片: (01_121_3), 08_113_0, 09_057_2, 08_132_0,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kazuko_o+2.png)  

- o+1:  
    - 图片: 08_123_3, 08_120_0,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kazuko_o+1.png)  

- o0:  
    - 图片: 08_105_0,     
    - 特点：  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_kazuko_o0.png) 
        ![](./img/eye_mark_kazuko_o0_vs_masahiko_basis.jpg)  

- o-1:  
    - 图片: (08_092_3), 08_092_0, 07_075_0,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kazuko_o-1.png)  

- o-1(man):  
    - 图片: 08_120_3, 08_091_5,     
    - 特点： 男装时。   
    - 叠加：  
        ![](./img/eye_front_kazuko_o-1(man).png)  

- o-1(top):  
    - 图片: 02_080_4,     
    - 特点： 摄像机位于角色上方(镜头俯视)，外眼角稍微高于内眼角。   
    - 叠加：  
        ![](./img/eye_front_kazuko_o-1(top).png)  

- o-2:  
    - 图片: 07_061_0, 07_074_8，05_061_3，    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kazuko_o-2.png)  

- o-3:  
    - 图片: 08_109_3，    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kazuko_o-3.png)  

- o-4:  
    - 图片: 06_103_2，05_147_0，04_189_1，01_069_0，01_066_0，03_021_2，    01_052_2，05_163_0，
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kazuko_o-4.png)  


### 爷爷-正面    
待完成  


### 横田进(Susumu)-正面    
- o+2:  
    - 图片: 05_065_5, 08_046_5, 08_050_0, 08_117_6, 08_119_2,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_susumu_o+2.png)  

- o0:  
    - 图片: 03_094_1, 03_012_5,  
    - 特点： Almond2/Hooded。上下眼皮线圆滑（无棱角）   
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_susumu_o0.png) 
        ![](./img/eye_mark_susumu_o0_vs_masahiko_basis.jpg)  

- close:  
    - 图片: 03_019_0,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_susumu_close.png)  

- close(amuse):  
    - 图片: 03_013_3, 08_135_2, 05_163_0, 06_103_2, 03_030_0, 03_021_2, 03_016_5,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_susumu_close(amuse).png)  


### 宪司(Kenji)-正面    
- o+2:  
    - 图片: 07_006_6, 04_082_4, 04_011_1, 03_179_4, 03_179_0, 03_175_4    
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kenji_o+2.png)  

- o0:  
    - 图片: 04_038_2__r,  
    - 特点：  Almond/Monolid  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_kenji_o0.png) 
        ![](./img/eye_mark_kenji_o0_vs_masahiko_basis.jpg)  

- o-1:  
    - 图片: 04_014_1,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kenji_o-1.png)  

- close:  
    - 图片: 04_060_0, 04_052_5, 04_014_3, 03_187_2, 03_189_5,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kenji_close.png)  

- close(amuse):  
    - 图片: 10_173_2, 04_010_0,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kenji_close(amuse).png)  


### 顺子(Yoriko)-正面    
- o+2:  
    - 图片: 04_005_5, 04_039_0,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_yoriko_o+2.png)  

- o0:  
    - 图片: 03_155_5, (10_173_3),     
    - 特点：  Almond2/Hooded。  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_yoriko_o0.png) 
        ![](./img/eye_mark_yoriko_o0_vs_masahiko_basis.jpg)  

- o-1:  
    - 图片: 04_016_3, (06_187_0), 03_172_0,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_yoriko_o-1.png)  

- o-2:  
    - 图片: 04_015_5,     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_yoriko_o-2.png)  


### 森(Mori)-正面    
- o+3:  
    - 图片: 10_093_4,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_mori_o+3.png)  

- o+2:  
    - 图片: (10_107_3, 10_102_3), 10_102_1, 10_095_0, 02_074_2,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_mori_o+2.png)  

- o-2:  
    - 图片: 06_114_0,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_mori_o-2.png)  



### 叶子母-正面    
- o+2:  
    - 图片: 13_136_4,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_ykma_o+2.png)  

- o+1:  
    - 图片: 13_161_3,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_ykma_o+1.png)  

- o0:  
    - 图片: 13_103_3,      
    - 特点：  Almond2/Monolid/Small。上眼皮线圆滑（无稜角）。  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色)。  
        ![](./img/eye_front_ykma_o0.png) 
        ![](./img/eye_mark_ykma_o0_vs_masahiko_basis.jpg)  
        
- close:  
    - 图片: 13_180_4,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_ykma_close.png)  


### 理沙(Risa)-正面    
- o0:  
    - 图片: 12_024_6, 12_025_0,      
    - 特点：  Round2/Monolid。   
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_risa_o0.png) 
        ![](./img/eye_mark_risa_o0_vs_masahiko_basis.jpg)  

- close:  
    - 图片: 11_147_1,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_risa_close.png)  


### 美菜(Mika)-正面    
- o+2.5:  
    - 图片: 11_166_6，11_166_5, 11_147_1,        
    - 特点： 比o+2稍大一点。    
    - 叠加：  
        ![](./img/eye_front_mika_o+2.5.png)  

- o+2:  
    - 图片: 12_054_2, 12_033_5，12_012_1__r，11_195_0，11_167_4，        
    - 特点：   
    - 叠加：  
        ![](./img/eye_front_mika_o+2.png)  

- o+1:  
    - 图片: 12_024_6, 12_025_0,      
    - 特点：Round3/Monolid。不确定是眼睛大(可作为o0)，还是睁大眼.   
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_mika_o+1.png) 
        ![](./img/eye_mark_mika_o+1_vs_masahiko_basis.jpg)  

- o-2:  
    - 图片: 12_053_3,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_mika_o-2.png)  

- close(amuse):  
    - 图片: 12_055_3, 12_008_6     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_mika_close(amuse).png)  


### 齐藤茜(Akane)-正面    
- o+1.5:  
    - 图片: (07_100_2, 06_159_2), 06_163_4,      
    - 特点：Round2/Monolid/  
    - 叠加：  
        ![](./img/eye_front_akane_o+1.5.png)  

- o+1:  
    - 图片: 06_145_7,      
    - 特点：Round2/Monolid/  
    - 叠加：  
        ![](./img/eye_front_akane_o+1.png)  

- o0:  
    - 图片: 06_144_5_o0,      
    - 特点：Round2/Regular/Downslanted。  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_akane_o0.png) 
        ![](./img/eye_mark_akane_o0_vs_masahiko_basis.jpg)  

- o-2:  
    - 图片: 06_157_1, (07_113_1),      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_akane_o-2.png)  

- close(amuse):  
    - 图片: 07_099_3     
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_akane_close(amuse).png)  


### 仁科(Nishina)-正面    
- o0:  
    - 图片: (14_027_4(down)), 14_009_5, (12_077_0), 10_147_6,      
    - 特点：Almond/=-=。   
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_nishina_o0.png) 
        ![](./img/eye_mark_nishina_o0_vs_masahiko_basis.jpg)  

- o-3:  
    - 图片: 14_008_7, 14_014_5,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_nishina_o-3.png)  

- close:  
    - 图片: 14_028_6,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_nishina_close.png)  
  


### 葵(Aoi)-正面    
- o+2:  
    - 图片: 06_073_3, 06_073_4, 09_101_6,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_aoi_o+2.png)  

- o0:  
    - 图片: 06_042_1,      
    - 特点：  Almond2/?。
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_aoi_o0.png) 
        ![](./img/eye_mark_aoi_o0_vs_masahiko_basis.jpg)  

- o-2:  
    - 图片: 06_056_0, 06_039_0, 06_080_6,      
    - 特点：  Almond/=-=/DeepSet。    
    - 叠加：  
        ![](./img/eye_front_aoi_o-2.png)  



### 奶奶-正面    
待完成。  


### 文哉(Fumiya)-正面    
- o0:  
    - 图片: 12_012_3,      
    - 特点：Almond/=-=/Downslanted.    
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_fumiya_o0.png) 
        ![](./img/eye_mark_fumiya_o0_vs_masahiko_basis.jpg)  

- o-1:  
    - 图片: 12_010_3,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_fumiya_o-1.png)  


### 齐藤玲子(Reiko)-正面    
- o+2:  
    - 图片: 01_163_3,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_reiko_o+2.png)  

- o+1:  
    - 图片: 01_166_0,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_reiko_o+1.png)  

- o0:  
    - 图片: 01_169_6,      
    - 特点：年幼时的样子。Round3/Monolid。  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_reiko_o0.png) 
        ![](./img/eye_mark_reiko_o0_vs_masahiko_basis.jpg)  


### 松下敏史(Toshifumi)-正面    
- o+1:  
    - 图片: 09_072_5,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_matsu_o+1.png)  

- o0:  
    - 图片: 09_084_4, 09_076_5,       
    - 特点：  Round/=-=.  
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_matsu_o0.png) 
        ![](./img/eye_mark_matsu_o0_vs_masahiko_basis.jpg)  

- o-1:  
    - 图片: 09_078_0, 09_077_5,       
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_matsu_o-1.png)  


### 章子(Shoko)-正面    
待完成  


### 阿透(Tooru)-正面    
- o0:  
    - 图片: 12_012_3,       
    - 特点：  Almond2/=-=/Downslanted。 
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):   
        ![](./img/eye_front_tooru_o0.png) 
        ![](./img/eye_mark_tooru_o0_vs_masahiko_basis.jpg)  


### 京子(Kyoko)-正面    
- o-1:  
    - 图片: 02_177_2,      
    - 特点：Almond2/Sunken。    
    - 叠加：  
        ![](./img/eye_front_kyoko_o-1.png)  

- o-1(2):  
    - 图片: 02_186_2,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kyoko_o-1(2).png)  

- close:  
    - 图片: 02_186_0,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kyoko_close.png)  


### 一马(Kazuma)-正面    
待完成  


### 一树(Kazuki)-正面    
- o-2:  
    - 图片: 12_028_4,      
    - 特点：不完全是正面。眯眼睛。      
    - 叠加：  
        ![](./img/eye_front_kazuki_o-2.png)  

- close(amuse):  
    - 图片: 12_012_3,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_kazuki_close(amuse).png)  


### 千夏(Chinatsu)-正面    
- o+1:  
    - 图片: 12_159_3,      
    - 特点：。    
    - 叠加：  
        ![](./img/eye_front_chinatsu_o+1.png)  

- o0:  
    - 图片: 12_141_0,      
    - 特点：  Round2/Monolid。   
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_chinatsu_o0.png) 
        ![](./img/eye_mark_chinastu_o0_vs_masahiko_basis.jpg)  

- close:  
    - 图片: 14_016_2,      
    - 特点：  
    - 叠加：  
        ![](./img/eye_front_chinatsu_close.png)  


### 麻衣(Mai)-正面    
- o+2:  
    - 图片: 12_162_4, 12_159_3     
    - 特点：Round3/AverageSize/Monolid。    
    - 叠加：  
        ![](./img/eye_front_mai_o+2.png)  

- o0:  
    - 图片: 12_141_0,      
    - 特点：Round. 不确定是眼睛大(可作为o0)，还是睁大眼。    
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_mai_o0.png) 
        ![](./img/eye_mark_mai_o0_vs_masahiko_basis.jpg)  


### 绫子(Aya)-正面    
- o0:  
    - 图片: 02_180_5,      
    - 特点：Small/Round/Regular/Wide。    
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_aya_o0.png) 
        ![](./img/eye_mark_aya_o0_vs_masahiko_basis.jpg)  


### 典子(Tenko)-正面    
没有合适的图。  


### 古屋公弘(Kimihiro)-正面    
- o+2:  
    - 图片: 09_051_6     
    - 特点：睁大眼。      
    - 叠加：  
        ![](./img/eye_front_kimihiro_o+2.png)  


### 古屋朋美(Tomomi)-正面    
- o0:  
    - 图片: 09_047_1,      
    - 特点：不确定是眼睛大(可作为o0)，还是睁大眼。    
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_tomomi_o0.png) 
        ![](./img/eye_mark_tomomi_o0_vs_masahiko_basis.jpg)  


### 八不(Hanzu)-正面    
- o0:  
    - 图片: 02_183_7__r,      
    - 特点：怒。外侧双眼皮，看不清内侧。    
    - 叠加效果；该组mark(红色)对比雅彦basis mark(蓝色):  
        ![](./img/eye_front_hanzu_o0.png) 
        ![](./img/eye_mark_hanzu_o0_vs_masahiko_basis.jpg)  


### 哲(Te)-正面    
没有合适的图。



------

## 排列如下  (角色左眼, 放大4倍)  
![](img/eye_front_200p/eye_front_masahiko.jpg) 
![](img/eye_front_200p/eye_front_masami.jpg) 
![](img/eye_front_200p/eye_front_shion.jpg) 
![](img/eye_front_200p/eye_front_shya.jpg) 
![](img/eye_front_200p/eye_front_sora.jpg) 
![](img/eye_front_200p/eye_front_yukari.jpg) 
![](img/eye_front_200p/eye_front_yoko.jpg) 
![](img/eye_front_200p/eye_front_hiromi.jpg) 
![](img/eye_front_200p/eye_front_kaoru.jpg) 
![](img/eye_front_200p/eye_front_ejima.jpg) 
![](img/eye_front_200p/eye_front_tatsumi.jpg) 
![](img/eye_front_200p/eye_front_makoto.jpg) 
![](img/eye_front_200p/eye_front_director.jpg) 
![](img/eye_front_200p/eye_front_saki.jpg) 
![](img/eye_front_200p/eye_front_asagi.jpg) 
![](img/eye_front_200p/eye_front_cameraman.jpg) 
![](img/eye_front_200p/eye_front_yoriko.jpg) 
![](img/eye_front_200p/eye_front_mori.jpg) 
![](img/eye_front_200p/eye_front_kazuko.jpg) 
![](img/eye_front_200p/eye_front_grandpa.jpg) 
![](img/eye_front_200p/eye_front_susumu.jpg) 
![](img/eye_front_200p/eye_front_kenji.jpg) 
![](img/eye_front_200p/eye_front_ykma.jpg) 
![](img/eye_front_200p/eye_front_risa.jpg) 
![](img/eye_front_200p/eye_front_mika.jpg) 
![](img/eye_front_200p/eye_front_akane.jpg) 
![](img/eye_front_200p/eye_front_nishina.jpg) 
![](img/eye_front_200p/eye_front_aoi.jpg) 
![](img/eye_front_200p/eye_front_grandma.jpg) 
![](img/eye_front_200p/eye_front_fumiya.jpg) 
![](img/eye_front_200p/eye_front_reiko.jpg) 
![](img/eye_front_200p/eye_front_matsu.jpg) 
![](img/eye_front_200p/eye_front_shoko.jpg) 
![](img/eye_front_200p/eye_front_tooru.jpg) 
![](img/eye_front_200p/eye_front_kyoko.jpg) 
![](img/eye_front_200p/eye_front_kazuma.jpg) 
![](img/eye_front_200p/eye_front_kazuki.jpg) 
![](img/eye_front_200p/eye_front_chinatsu.jpg) 
![](img/eye_front_200p/eye_front_mai.jpg) 
![](img/eye_front_200p/eye_front_aya.jpg) 
![](img/eye_front_200p/eye_front_tenko.jpg) 
![](img/eye_front_200p/eye_front_kimihiro.jpg) 
![](img/eye_front_200p/eye_front_tomomi.jpg) 
![](img/eye_front_200p/eye_front_hanzu.jpg) 
![](img/eye_front_200p/eye_front_te.jpg) 


------

## 合并分类(合并非常类似的眼睛)    
同一{}内的角色的眼睛被认为是同样的。例如，在该合并分类中，可以认为masahiko basis和shion o0的眼睛是一样的。   

- {masahiko basis, shion o0, nishina o0}:  
    ![](img/eye_front_200p/eye_front_masahiko.jpg) 
    ![](img/eye_front_200p/eye_front_shion.jpg) 
    ![](img/eye_front_200p/eye_front_nishina.jpg)  
- {masami o0}:  
    ![](img/eye_front_200p/eye_front_masami.jpg)  
- {shya o0, yoriko}:    
    ![](img/eye_front_200p/eye_front_shya.jpg) 
    ![](img/eye_front_200p/eye_front_yoriko.jpg)  
- {sora o-1(basis)}:  
    ![](img/eye_front_200p/eye_front_sora.jpg)  
- {yukari}:  
    ![](img/eye_front_200p/eye_front_yukari.jpg)  
- {yoko, risa, akane, chinatsu, asagi}:   
    ![](img/eye_front_200p/eye_front_yoko.jpg) 
    ![](img/eye_front_200p/eye_front_risa.jpg) 
    ![](img/eye_front_200p/eye_front_akane.jpg) 
    ![](img/eye_front_200p/eye_front_chinatsu.jpg) 
    ![](img/eye_front_200p/eye_front_asagi.jpg)  
- {makoto, mika, reiko, mai, tomomi o0}:  
    ![](img/eye_front_200p/eye_front_makoto.jpg) 
    ![](img/eye_front_200p/eye_front_mika.jpg) 
    ![](img/eye_front_200p/eye_front_reiko.jpg) 
    ![](img/eye_front_200p/eye_front_mai.jpg) 
    ![](img/eye_front_200p/eye_front_tomomi.jpg)  
- {hiromi}:  
    ![](img/eye_front_200p/eye_front_hiromi.jpg)  
- {ejima, kaoru, tatsumi, tooru}:  
    ![](img/eye_front_200p/eye_front_ejima.jpg) 
    ![](img/eye_front_200p/eye_front_kaoru.jpg) 
    ![](img/eye_front_200p/eye_front_tatsumi.jpg) 
    ![](img/eye_front_200p/eye_front_tooru.jpg)  
- {susumu, aoi}:  
    ![](img/eye_front_200p/eye_front_susumu.jpg) 
    ![](img/eye_front_200p/eye_front_aoi.jpg)      
- 其它未能合并的项：{saki}, {kyoko}, {fumiya}, {ykma}, {aya}, {cameraman}, {kenji}, {kazuko}, {matsu}(怒), {kazuki}(眯眼睛), {mori}(惊讶), {kimihiro}(惊讶), {hanzu}(怒):  
    ![](img/eye_front_200p/eye_front_saki.jpg), 
    ![](img/eye_front_200p/eye_front_kyoko.jpg), 
    ![](img/eye_front_200p/eye_front_ykma.jpg), 
    ![](img/eye_front_200p/eye_front_fumiya.jpg), 
    ![](img/eye_front_200p/eye_front_aya.jpg), 
    ![](img/eye_front_200p/eye_front_cameraman.jpg), 
    ![](img/eye_front_200p/eye_front_kenji.jpg), 
    ![](img/eye_front_200p/eye_front_kazuko.jpg), 
    ![](img/eye_front_200p/eye_front_matsu.jpg), 
    ![](img/eye_front_200p/eye_front_kazuki.jpg), 
    ![](img/eye_front_200p/eye_front_mori.jpg), 
    ![](img/eye_front_200p/eye_front_kimihiro.jpg), 
    ![](img/eye_front_200p/eye_front_hanzu.jpg),  


------

## 进一步合并分类2(合并类似的眼睛)    
同一{}的角色的眼睛被认为是同样的。例如，在该合并分类中，可以认为masahiko basis和fumiya o0的眼睛是一样的。   

- {masahiko basis, shion o0, nishina o0, fumiya}:  
    ![](img/eye_front_200p/eye_front_masahiko.jpg) 
    ![](img/eye_front_200p/eye_front_shion.jpg) 
    ![](img/eye_front_200p/eye_front_nishina.jpg) 
    ![](img/eye_front_200p/eye_front_fumiya.jpg)  
- {shya o0, yoriko, susumu, aoi, kenji o0}:  
    ![](img/eye_front_200p/eye_front_shya.jpg) 
    ![](img/eye_front_200p/eye_front_yoriko.jpg) 
    ![](img/eye_front_200p/eye_front_susumu.jpg) 
    ![](img/eye_front_200p/eye_front_aoi.jpg) 
    ![](img/eye_front_200p/eye_front_kenji.jpg)  
- {sora o-1(basis)}:  
    ![](img/eye_front_200p/eye_front_sora.jpg)  
- {yukari, hiromi, kyoko}:  
    ![](img/eye_front_200p/eye_front_yukari.jpg) 
    ![](img/eye_front_200p/eye_front_hiromi.jpg) 
    ![](img/eye_front_200p/eye_front_kyoko.jpg)  
- {yoko, risa, akane, chinatsu, asagi,   makoto, mika, reiko, mai, tomomi o0,   masami o0}:   
    ![](img/eye_front_200p/eye_front_yoko.jpg) 
    ![](img/eye_front_200p/eye_front_risa.jpg) 
    ![](img/eye_front_200p/eye_front_akane.jpg) 
    ![](img/eye_front_200p/eye_front_chinatsu.jpg) 
    ![](img/eye_front_200p/eye_front_asagi.jpg)  
    ![](img/eye_front_200p/eye_front_makoto.jpg) 
    ![](img/eye_front_200p/eye_front_mika.jpg) 
    ![](img/eye_front_200p/eye_front_reiko.jpg) 
    ![](img/eye_front_200p/eye_front_mai.jpg) 
    ![](img/eye_front_200p/eye_front_tomomi.jpg)  
    ![](img/eye_front_200p/eye_front_masami.jpg)  
- {ejima, tatsumi, kaoru, saki, tooru}:  
    ![](img/eye_front_200p/eye_front_kaoru.jpg) 
    ![](img/eye_front_200p/eye_front_tooru.jpg) 
    ![](img/eye_front_200p/eye_front_ejima.jpg) 
    ![](img/eye_front_200p/eye_front_tatsumi.jpg) 
    ![](img/eye_front_200p/eye_front_saki.jpg)  
- 其它未能合并的项：{ykma}, {aya}, {cameraman}, {kazuko}, {matsu}(怒), {kazuki}(眯眼睛), {mori}(惊讶), {kimihiro}(惊讶), {hanzu}(怒):  
    ![](img/eye_front_200p/eye_front_ykma.jpg), 
    ![](img/eye_front_200p/eye_front_aya.jpg), 
    ![](img/eye_front_200p/eye_front_cameraman.jpg), 
    ![](img/eye_front_200p/eye_front_kazuko.jpg), 
    ![](img/eye_front_200p/eye_front_matsu.jpg), 
    ![](img/eye_front_200p/eye_front_kazuki.jpg), 
    ![](img/eye_front_200p/eye_front_mori.jpg), 
    ![](img/eye_front_200p/eye_front_kimihiro.jpg), 
    ![](img/eye_front_200p/eye_front_hanzu.jpg),  


------

## 进一步合并分类3(合并稍微类似的眼睛)    
待完成  

ykma o0 ~~~ kyoko  
shion o0 ~~~ saki  
saki ~~ shya o0  
saki ~~ yoriko(almond-) ~~ hanzu  
matsu ~~ hanzu(round-)  

masahiko basis ~~ ejima o0  ~~ shya o0   
sora o-1(basis) ~~tatsumi(almond+)  
kaoru ~~ fumiya(almond+)  ~~ nishina ~~ saki(almond+)    
ejima ~~ nishina ~~ tatsumi?(眼皮低) ~~ yoriko（右眼）  
tatsumi ~~ nishina ~~ tooru  
yoriko ~~ susumu ~~ kenji o0(monolid)  
nishina ~~ tooru  
fumiya  ~~ tooru(almond-)  


------

## 其他  
- 若苗紫/若苗空的眼睛比雅彦/紫苑的眼睛细长。  
- 下图比较了雅彦basis、紫苑basis、若苗空basis、若苗紫basis的(左)眼睛，可以看出：在眼睛外侧上方(图中蓝色椭圆区域) 这个部位，女性比男性的阴影重。可能因为女性睫毛长、或者化妆(眼影)的原因。  
  ![](./img/male_vs_female_00.jpg)  
- 辰巳的眼睛和熏的类似，是否可以佐证“辰巳是熏的生父”？  
- 大多数具有"=-="眼睛的角色都不是正面角色，例如：辰巳、早纪、仁科、葵、文哉、松下敏史、阿透。  


## 参考资料    
[1]: Biometric Study of Eyelid Shape and Dimensions of Different Races with References to Beauty. Seung Chul Rhee, Kyoung-Sik Woo, Bongsik Kwon. Aesth Plast Surg (2012), DOI 10.1007/s00266-012-9937-7   
[2]: [The 5 Golden Metrics for Your True Eye Shape](https://www.kendrapowell.com/blog/the-5-golden-metrics-for-your-true-eye-shape)  
[3]: [What is the exact shape of the human eye? - Quora](https://www.quora.com/What-is-the-exact-shape-of-the-human-eye?share=1)  
[4]: [真实角色渲染----眼睛 - zhihu](https://zhuanlan.zhihu.com/p/73251979)  
[5]: Form of the Head and Neck, 2021, Uldis Zarins.  
[6]: [How to Determine Eye Shape - wikiHow ](https://www.wikihow.com/Determine-Eye-Shape)  
[7]: [How To Get Rid Of Sunken Eyes - RecaBlog](https://www.recablog.com/how-to-get-rid-of-sunken-eyes/)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
