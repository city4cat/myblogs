
- 网上关于嘴形/唇形的介绍不一致：  
  ![](img/1c702b61b8761f1e6156bbd1c2742125.jpg) 
  ![](img/7e64b378dc33f71965b9853b03ddde4d.jpg)  
  ![](img/2883062554_a116fae2fe_b.jpg) 
  ![](img/cdf012bc290ee02bda9eb7c239809ab8.jpg)  
  ![](img/75cae257838ca660314609e2dd.jpg) 
  ![](img/95c4055edb2ec00e0f04122e8066aff5-.jpg) 
  ![](img/lip-shapes.jpg)  
  虽然不一致，但有以下共同的嘴形/唇形。(thick/thin指嘴唇(竖向)的厚薄；large/small指嘴(横向)的宽窄)：  
    - 上嘴唇薄  
    - 下嘴唇薄  
    - 上下嘴唇都薄  
    -   

- 可以借助化妆来改变嘴形/唇形。  