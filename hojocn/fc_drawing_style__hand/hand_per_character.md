
## 每个角色的手  
素描中男性化的手的特点：  

- 手掌和手指粗大、棱角分明。  
- 手背面棱角分明（有明显的线条），手指关节背面有线条(皱纹(creases))。  
- 指甲粗大、呈方形。  

素描中女性化的手的特点：  

- 手掌和手指纤细、圆滑。  
- 手背面圆滑（无明显的线条）。  
- 指甲细圆、呈尖形。  
- 姿态。例如：小拇指翘起、兰花指、类似芭蕾舞手势（中指和无名指并拢）。  

需要说明的是，当物体距离相机近时，物体的细节多（对应到漫画里是线条多、阴影层次多）。所以，当漫画镜头里手背面的线条多时，不能一味地认为其呈男性化特征，还要考虑该镜头距离手的远近这个因素。  

以下图片为了排版美观，将图片缩小为统一高度。所以，若某图片整体线条偏细，则表示该图缩小倍数大；这意味着原图片大，这通常意味着物体距离镜头近。这种情况下，手背面的线条多表示其细节度高，而不能充分说明手背棱角分明、呈男性化。    

### Wakanae若苗 Shion紫苑  
典型的女性化的手。特写镜头里，指关节背面画有皱纹。    
![](./img/01_022_4__crop1.jpg) 
![](./img/01_047_6__crop0.jpg) 
![](./img/01_047_6__crop0.jpg) 
![](./img/01_090_5__crop0.jpg) 
![](./img/01_125_0__crop0.jpg) 
![](./img/01_143_3__crop0.jpg) 
![](./img/01_161_6__crop0.jpg) 
![](./img/02_018_7__crop0.jpg) 
![](./img/02_032_1__crop0.jpg) 
![](./img/02_036_0__crop0.jpg) 
![](./img/02_116_0__crop2.jpg) 
![](./img/02_118_2__crop0.jpg) 
![](./img/02_137_3__crop0.jpg) 
![](./img/02_141_0__crop0.jpg) 
![](./img/03_049_2__crop0.jpg) 
![](./img/03_069_6__crop0.jpg) 
![](./img/03_075_3__crop0.jpg) 
![](./img/05_003_0__crop0.jpg) 
![](./img/05_068_5__crop0.jpg) 
![](./img/05_131_0__crop0.jpg) 
![](./img/06_000a_0__crop0.jpg) 
![](./img/06_000a_0__crop1.jpg) 
![](./img/06_003_0__crop0.jpg) 
![](./img/06_058_1__crop0.jpg) 
![](./img/06_127_6__crop0.jpg) 
![](./img/07_011_5__crop0.jpg) 
![](./img/07_052_3__crop0.jpg) 
![](./img/08_001_0__crop0.jpg) 
![](./img/08_003_0__crop0.jpg) 
![](./img/09_079_4__crop0.jpg) 
![](./img/10_031_0__crop0.jpg) 
![](./img/10_059_0__crop0.jpg) 
![](./img/10_076_4__crop0.jpg) 
![](./img/11_000a_0__crop0.jpg) 
![](./img/11_135_3__crop0.jpg) 
![](./img/11_135_4__crop0.jpg) 
![](./img/12_038_7__crop0.jpg) 
![](./img/12_054_1__crop0.jpg) 
![](./img/12_110_0__crop0.jpg) 
![](./img/13_069_6__crop0.jpg) 
![](./img/13_031_0__crop0.jpg) 
![](./img/14_143_4__crop0.jpg) 
![](./img/14_146_4__crop0.jpg) 
![](./img/14_288_6__crop0.jpg) 

![11](./img/02_001__crop0.jpg) ![12](./img/02_116_0__crop0.jpg)  
![21](./img/02_001__crop1.jpg) ![22](./img/02_116_0__crop1.jpg)  

生气地抓：    
![](./img/11_156_5__crop0.jpg) 



### 塩谷Shionoya（紫苑男装）  
![](./img/14_045_6__crop0.jpg) 

最初，手偏女性化。下图左4的拇指指甲呈女性化。    
![](./img/10_138_6__crop0.jpg) 
![](./img/11_014_6__crop0.jpg) 
![](./img/11_024_1__crop0.jpg) 
![](./img/11_024_1__crop1.jpg) 

后来，多数手背有明显的线条，呈男性化；但指甲呈女性化：  
![](./img/12_193_3__crop0.jpg) 
![](./img/13_153_0__crop0.jpg) 
![](./img/14_054_6__crop0.jpg) 
![](./img/14_066_5__crop0.jpg) 
![](./img/14_086_4__crop0.jpg) 
![](./img/14_092_3__crop0.jpg) 
![](./img/14_114_4__crop0.jpg) 
![](./img/14_172_2__crop0.jpg) 
![](./img/14_173_2__crop0.jpg) 
![](./img/14_195_2__crop0.jpg) 
![](./img/14_195_3__crop0.jpg) 
![](./img/14_251_5__crop0.jpg) 
![](./img/14_251_6__crop0.jpg)  
其中，下图左1食指指甲、左2拇指指甲偏男性化，可能是因为视角导致的。  
![](./img/14_195_3__crop0.jpg) 
![](./img/14_251_5__crop0.jpg)  

该角色最初的手偏女性化，后来的手多数偏男性化。这或许可以理解为：该角色的男装打扮越来越逼真。  



### Masami雅美  

![](./img/03_045_0__crop0.jpg) 
![](./img/03_047_0__crop0.jpg) 
![](./img/03_047_0__crop1.jpg) 
![](./img/03_056_1__crop0.jpg) 
![](./img/03_104_6__crop1.jpg) 
![](./img/03_114_0__crop0.jpg) 
![](./img/03_114_0__crop1.jpg) 
![](./img/07_134_6__crop0.jpg) 
![](./img/07_138_5__crop0.jpg) 
![](./img/10_127_2__crop0.jpg) 
![](./img/12_015_3__crop0.jpg) 

以上图片的手呈女性化。该角色在以下场景中的手呈男性化。  

- 雅彦第一次被打扮成女装。下图左一为镜中画面；下图左二为实际画面，呈男性化。镜中的手比实际的手少了一些褶皱。可理解为镜中图像比实际图像少一些细节，这体现了作者的细心！      
![](./img/03_027_3__crop0.jpg) 
![](./img/03_028_0__crop0.jpg) 

- 女装初期（未接受紫苑的训练）：  
![](./img/03_037_3__crop0.jpg) 

- 在熏面前摘掉假发，揭示自己的真实身份为雅彦后，手部呈男性化：  
![](./img/07_140_2__crop0.jpg) 
![](./img/07_145_2__crop0.jpg) 

- 手部用力：  
![](./img/10_135_3__crop0.jpg) 
![](./img/12_021_2__crop0.jpg) 
![](./img/14_236_1__crop0.jpg) 

- 紫苑高中毕业旅行，雅彦（雅美）和紫苑独处时，手部呈男性化：  
![](./img/11_175_0__crop0.jpg) 
![](./img/11_183_6__crop0.jpg) 
![](./img/11_186_4__crop0.jpg) 

- 成人礼，手部呈男性化。  
![](./img/12_073_5__crop0.jpg) 
![](./img/12_076_5__crop0.jpg) 

- FC尾声处，雅彦(雅美)参与影研社拍戏时、找叶子分手时，手部呈男性化（此时身体的肌肉也更明显）：  
![](./img/14_262_4__crop0.jpg) 

- 紫苑高中毕业旅行时，雅美的指甲呈女性化，手背似乎呈男性化：    
![](./img/11_154_3__crop0.jpg) 
![](./img/11_156_5__crop1.jpg) 
![](./img/12_014_2__crop0.jpg) 


### Yanagiba柳叶 Masahiko雅彦  
典型的男性化的手。  
![](./img/01_047_6__crop0.jpg) 
![](./img/01_154_0__crop0.jpg) 
![](./img/02_006_6__crop0.jpg) 
![](./img/02_012_3__crop0.jpg) 
![](./img/02_012_6__crop0.jpg) 
![](./img/02_141_0__crop1.jpg) 
![](./img/05_161_3__crop1.jpg) 
![](./img/05_191_2__crop0.jpg) 
![](./img/06_005_1__crop0.jpg) 
![](./img/08_181_0__crop0.jpg) 
![](./img/11_188_3__crop0.jpg) 
![](./img/12_123_2__crop0.jpg) 
![](./img/13_069_6__crop1.jpg) 
![](./img/14_151_6__crop0.jpg) 


### Wakanae若苗 Sora空  
典型的男性化的手。  
![](./img/01_190_6__crop0.jpg) 
![](./img/02_031_0__crop0.jpg) 
![](./img/02_031_0__crop1.jpg) 
![](./img/04_183_6__crop1.jpg) 
![](./img/05_064_4__crop0.jpg) 
![](./img/06_111_0__crop0.jpg) 

![11](./img/01_021_6__crop0.jpg)  
![21](./img/01_021_6__crop1.jpg)  


### Wakanae若苗 Yukari紫  
典型的女性化的手。手背面指关节处几乎无皱纹，这表示其手背的皮肤光滑。我猜，这或许暗示她精心打扮。    
![](./img/01_007_4__crop0.jpg) 
![](./img/01_022_4__crop0.jpg) 
![](./img/02_031_0__crop2.jpg) 
![](./img/02_193_2__crop0.jpg) 
![](./img/04_183_6__crop0.jpg) 
![](./img/05_048_4__crop0.jpg) 
![](./img/05_175_2__crop0.jpg) 
![](./img/05_177_5__crop0.jpg) 
![](./img/05_189_0__crop0.jpg) 
![](./img/06_111_0__crop1.jpg) 
![](./img/09_094_4__crop0.jpg) 
![](./img/09_150_0__crop0.jpg) 
![](./img/09_150_0__crop1.jpg) 
![](./img/12_180_3__crop0.jpg) 
![](./img/01_043_0__crop0.jpg) 
![](./img/11_131_4__crop0.jpg) 
![](./img/14_106_6__crop0.jpg) 

- 用力挣扎：  
![](./img/09_134_1__crop0.jpg) 
![](./img/09_134_1__crop1.jpg) 

- 相比早纪的手（涂了指甲油），若苗紫的手的皱纹似乎更少：  
![](./img/09_150_0__crop0.jpg) 
![](./img/10_046_3__crop0.jpg) 
![](./img/10_054_1__crop0.jpg) 




### Asagi浅葱 Ai蓝（紫苑的同学）
指甲、手背、手指、姿态呈女性化，是典型的女性化的手。手背面指关节处几乎无皱纹。  

![](./img/14_059_4__crop0.jpg) 
![](./img/14_067_2__crop0.jpg) 
![](./img/14_071_0__crop0.jpg) 
![](./img/14_076_0__crop0.jpg) 
![](./img/14_092_3__crop1.jpg) 
![](./img/14_096_4__crop0.jpg) 
![](./img/14_113_0__crop0.jpg) 
![](./img/14_121_5__crop0.jpg) 
![](./img/14_135_3__crop0.jpg) 
![](./img/14_135_3__crop1.jpg) 
![](./img/14_175_1__crop0.jpg) 
![](./img/14_175_2__crop0.jpg) 
![](./img/14_193_1__crop0.jpg) 
![](./img/14_196_1__crop0.jpg) 
![](./img/14_232_1__crop0.jpg) 
![](./img/14_250_1__crop0.jpg) 

高中时：  
![](./img/14_126_5__crop0.jpg) 
![](./img/14_126_5__crop1.jpg) 
![](./img/14_127_0__crop0.jpg) 

### Asaoka浅冈 Yoko(Youko)叶子  
典型的女性化的手。手背面指关节处几乎无皱纹。       
![](./img/02_092_2__crop0.jpg) 
![](./img/02_092_2__crop1.jpg) 
![](./img/02_092_2__crop2.jpg) 
![](./img/05_157_1__crop0.jpg) 
![](./img/05_161_3__crop0.jpg) 
![](./img/08_155_7__crop0.jpg) 
![](./img/11_014_6__crop1.jpg) 
![](./img/13_090_2__crop0.jpg) 
![](./img/13_095_2__crop0.jpg) 
![](./img/13_115_0__crop0.jpg) 
![](./img/13_115_0__crop1.jpg) 
![](./img/13_125_5__crop0.jpg) 
![](./img/13_129_1__crop0.jpg) 
![](./img/13_148_1__crop0.jpg) 
![](./img/13_167_4__crop0.jpg) 
![](./img/13_167_5__crop0.jpg) 
![](./img/13_186_2__crop0.jpg) 
![](./img/13_186_6__crop0.jpg) 
![](./img/13_187_4__crop0.jpg) 
![](./img/13_193_4__crop0.jpg) 
![](./img/14_207_0__crop0.jpg) 

- 手用力时，手背有明显的线条（肌腱）。  
![](./img/14_190_3__crop0.jpg) 


### Mori森（督促若苗空工作的女编辑）  
典型的女性化的手。   
![](./img/02_061_3__crop0.jpg) 
![](./img/06_114_0__crop0.jpg) 
![](./img/10_088_2__crop0.jpg) 
![](./img/10_088_3__crop0.jpg) 
![](./img/10_090_2__crop0.jpg) 
![](./img/10_091_1__crop0.jpg) 
![](./img/10_092_6__crop0.jpg) 
![](./img/10_093_7__crop0.jpg) 
![](./img/10_096_2__crop0.jpg) 
![](./img/10_107_0__crop0.jpg) 
![](./img/10_109_0__crop0.jpg) 

### Nakamura中村 Hiromi浩美（若苗空的助手）
典型的女性化的手。   

![](./img/03_011_5__crop0.jpg) 
![](./img/05_129_2__crop0.jpg) 
![](./img/05_163_0__crop0.jpg) 
![](./img/07_061_0__crop0.jpg) 
![](./img/07_079_0__crop0.jpg) 
![](./img/07_107_5__crop0.jpg) 
![](./img/07_112_5__crop0.jpg) 
![](./img/09_032_0__crop0.jpg) 
![](./img/09_032_3__crop0.jpg) 
![](./img/09_034_2__crop0.jpg) 
![](./img/09_034_7__crop0.jpg) 
![](./img/09_039_5__crop0.jpg) 
![](./img/09_056_6__crop0.jpg) 
![](./img/09_056_6__crop1.jpg) 
![](./img/09_057_0__crop0.jpg) 

#### 浩美男装（中村 光浩）  
![](./img/05_125_2__crop0.jpg) 


### Yamazaki山崎 Makoto真琴（若苗空的助手）  
  
![](./img/01_054_0__crop0.jpg) 
![](./img/05_163_0__crop1.jpg) 
![](./img/07_061_0__crop1.jpg) 
![](./img/07_112_5__crop1.jpg) 
![](./img/08_135_5__crop0.jpg) 
![](./img/09_056_6__crop2.jpg) 
![](./img/11_035_0__crop0.jpg) 
![](./img/11_085_2__crop0.jpg) 

### Yokota横田 Susumu进（若苗空的助手）  
手背、手指呈男性化；指甲、姿态呈女性化。指甲圆又尖，我猜，这是该角色的化妆。    
![](./img/03_010_4__crop0.jpg) 
![](./img/03_013_5__crop0.jpg) 
![](./img/03_014_2__crop0.jpg) 
![](./img/03_014_5__crop0.jpg) 
![](./img/03_015_2__crop0.jpg) 
![](./img/03_017_0__crop0.jpg) 
![](./img/03_017_1__crop0.jpg) 
![](./img/03_017_4__crop0.jpg) 
![](./img/03_018_0__crop0.jpg) 
![](./img/03_020_2__crop0.jpg) 
![](./img/03_021_2__crop0.jpg) 
![](./img/05_103_5__crop0.jpg) 
![](./img/05_103_5__crop1.jpg) 
![](./img/07_070_0__crop0.jpg) 
![](./img/07_070_1__crop0.jpg) 
![](./img/07_086_2__crop0.jpg) 
![](./img/08_135_2__crop0.jpg) 
![](./img/11_035_0__crop1.jpg) 

#### 横田进的男装  
![](./img/03_009_7__crop0.jpg) 
![](./img/03_018_1__crop0.jpg) 
![](./img/03_018_1__crop1.jpg) 


### Niigata新潟 Kazuko和子（若苗空的助手）  
典型的男性化的手。   
![](./img/07_073_1__crop0.jpg) 
![](./img/08_104_1__crop0.jpg) 
![](./img/08_104_5__crop0.jpg) 
![](./img/08_131_4__crop0.jpg) 
![](./img/08_140_6__crop0.jpg) 
![](./img/10_163_1__crop0.jpg) 

![](./img/08_141_3__crop0.jpg)  
![](./img/08_141_3__crop1.jpg)  

#### 和子的男装  
![](./img/08_090_3__crop0.jpg) 
![](./img/08_096_0__crop0.jpg) 
![](./img/08_098_2__crop0.jpg) 


### 奥村 宪司Kenji（若苗空的高中同学）  
典型的男性化的手。   
![](./img/04_019_0__crop0.jpg) 
![](./img/04_023_4__crop1.jpg) 
![](./img/04_023_4__crop0.jpg) 
![](./img/07_007_5__crop0.jpg) 
![](./img/07_026_1__crop0.jpg) 
![](./img/14_293_3__crop0.jpg) 
 

### Yoriko顺子 Kikuchi菊地（若苗空的妹妹）  
典型的女性化的手。特写镜头里，手背指关节处有一些皱纹。     
![](./img/03_148_1__crop0.jpg) 
![](./img/07_053_2__crop0.jpg) 
![](./img/14_293_3__crop1.jpg) 
![](./img/14_294_1__crop0.jpg) 

![11](./img/04_041_0__crop0.jpg) ![12](./img/07_038_0__crop0.jpg)  
![21](./img/04_041_0__crop1.jpg) ![22](./img/07_038_0__crop1.jpg) 


### Masoba真朱 Kaoru薰  
手指关节处有皱纹，呈男性化。  

该角色出场早期，其指甲呈女性化。  
![](./img/08_009_0__crop0.jpg) 
![](./img/08_104_5__crop0.jpg) 
![](./img/09_154_5__crop0.jpg) 

该角色出场后期，其指甲女性化不明显。  
![](./img/11_092_4__crop0.jpg) 
![](./img/12_172_4__crop0.jpg) 
![](./img/12_189_1__crop0.jpg) 
![](./img/12_190_5__crop0.jpg) 
![](./img/12_190_7__crop0.jpg) 
![](./img/12_191_2__crop0.jpg) 
![](./img/13_011_4__crop0.jpg) 
![](./img/13_030_0__crop0.jpg) 
![](./img/13_031_0__crop1.jpg) 
![](./img/13_062_0__crop0.jpg) 


### Saki早纪（真朱薰的妈妈）  
![](./img/09_144_0__crop0.jpg) 
![](./img/09_150_0__crop0.jpg) 
![](./img/09_150_0__crop1.jpg) 
![](./img/09_159_4__crop0.jpg) 
![](./img/09_162_3__crop0.jpg) 
![](./img/09_163_4__crop0.jpg) 
![](./img/09_173_6__crop0.jpg) 
![](./img/09_180_0__crop0.jpg) 
![](./img/10_019_2__crop0.jpg) 
![](./img/10_022_3__crop0.jpg) 
![](./img/10_027_5__crop0.jpg) 
![](./img/10_028_2__crop0.jpg) 
![](./img/10_050_1__crop0.jpg) 
![](./img/10_053_3__crop0.jpg) 

![11](./img/09_148_0__crop0.jpg) ![12](./img/09_180_2__crop0.jpg) ![13](./img/10_044_1__crop0.jpg) 
![21](./img/09_148_0__crop1.jpg) ![22](./img/09_180_2__crop1.jpg) ![23](./img/10_044_1__crop1.jpg) 


### Masoba真朱 Tatsumi辰四 （真朱薰的爸爸？） 

![](./img/03_104_6__crop0.jpg) 
![](./img/10_005_4__crop0.jpg) 
![](./img/03_106_6__crop0.jpg) 
![](./img/03_121_0__crop0.jpg) 
![](./img/04_126_0__crop0.jpg) 
![](./img/06_041_1__crop0.jpg) 
![](./img/07_179_5__crop0.jpg) 
![](./img/07_198_0__crop0.jpg) 
![](./img/07_198_3__crop0.jpg) 
![](./img/09_113_5__crop0.jpg) 
![](./img/09_126_1__crop0.jpg) 
![](./img/09_173_0__crop0.jpg) 
![](./img/12_190_2__crop0.jpg) 
![](./img/13_033_1__crop0.jpg) 

- 手部用力：  
![](./img/04_124_6__crop0.jpg)  


### Aoi(美)葵（喜欢辰巳的人）  
指甲女性化；手背线条明显、指关节有一些褶皱。    
![](./img/06_039_0__crop0.jpg) 
![](./img/06_066_4__crop0.jpg) 
![](./img/06_066_5__crop0.jpg) 
![](./img/06_067_0__crop0.jpg) 


### Takuya卓也 Ejima江岛（雅彦的同学）  
手背线条明显、指关节有一些褶皱。  
![](./img/02_121_0__crop0.jpg) 
![](./img/02_136_5__crop0.jpg) 
![](./img/03_070_0__crop0.jpg) 
![](./img/04_107_0__crop0.jpg) 
![](./img/06_023_4__crop0.jpg) 
![](./img/06_061_0__crop0.jpg) 
![](./img/07_151_2__crop0.jpg) 
![](./img/09_012_6__crop0.jpg) 
![](./img/10_068_5__crop0.jpg) 
![](./img/10_074_4__crop0.jpg) 
![](./img/10_130_1__crop0.jpg) 
![](./img/10_147_2__crop0.jpg) 
![](./img/10_151_2__crop0.jpg) 
![](./img/10_151_3__crop0.jpg) 
![](./img/10_153_0__crop0.jpg) 
![](./img/12_124_1__crop0.jpg) 
![](./img/12_124_1__crop1.jpg) 

- 07_092_4，该镜头里，手距离镜头较近，手背线条明显。[疑问]但为何没有画出指甲？    
![](./img/07_092_4__thumb.jpg) 
![](./img/07_092_4__crop0.jpg) 

#### 女装的江岛卓也  
![](./img/05_015_1__crop0.jpg) 
![](./img/05_016_4__crop0.jpg) 
![](./img/05_017_2__crop0.jpg) 
![](./img/05_017_2__crop1.jpg) 

### 影研社的电影导演  
手指短粗。这符合他矮胖的身材。      
![](./img/03_035_5__crop0.jpg) 
![](./img/03_036_3__crop0.jpg) 
![](./img/03_071_5__crop0.jpg) 
![](./img/03_123_2__crop0.jpg) 
![](./img/10_077_1__crop0.jpg) 
![](./img/12_117_6__crop0.jpg) 
![](./img/12_145_2__crop0.jpg) 
![](./img/12_160_4__crop0.jpg) 
![](./img/12_161_4__crop0.jpg) 
![](./img/12_161_6__crop0.jpg) 
![](./img/14_036_2__crop0.jpg) 
![](./img/14_195_6__crop0.jpg) 


### 影研社的摄像师    
指关节有一些褶皱。男性化的手。    
![](./img/07_112_3__crop0.jpg) 
![](./img/07_126_3__crop0.jpg) 
![](./img/12_148_6__crop0.jpg) 
![](./img/12_149_2__crop0.jpg) 
![](./img/14_036_2__crop1.jpg) 


### 小孩-小紫苑  
![](./img/05_128_0__crop0.jpg) 
![](./img/05_128_1__crop0.jpg) 
![](./img/09_070_4__crop0.jpg) 
![](./img/09_072_3__crop0.jpg) 
![](./img/09_072_3__crop1.jpg) 

### 小孩-小盐谷
![](./img/14_052_0__crop0.jpg) 

### 小孩-小雅彦  

### 小孩-小浅葱  
![](./img/14_112_2__crop0.jpg) 

### 小孩-小叶子  
![](./img/13_126_7__crop0.jpg) 

### 小孩-小熏  
![](./img/09_154_4__crop0.jpg) 

### 小孩-一马(和子的孩子)

![](./img/08_092_6__crop0.jpg) 

- 手背有画肌腱(tendon)的线条，但没有画指甲。    
![](./img/08_126_1__crop0.jpg) 

- 手背上的两点可能是表示肉窝。  
![](./img/08_138_3__crop0.jpg) 

### 小孩-奥村 章子Shoko（宪司的女儿）


### 老人-紫苑的奶奶
![](./img/03_178_0__crop0.jpg) 
![](./img/04_075_1__crop0.jpg) 
![](./img/07_014_5__crop0.jpg) 
![](./img/07_014_6__crop0.jpg) 
![](./img/07_047_0__crop1.jpg) 
![](./img/07_053_3__crop0.jpg) 

### 老人-紫苑的爷爷
![](./img/03_165_4__crop0.jpg) 
![](./img/04_043_6__crop0.jpg) 
![](./img/07_013_1__crop0.jpg) 
![](./img/07_047_0__crop0.jpg) 

手背的肌腱的线条上有横向的线条：    
![](./img/03_196_3__crop0.jpg)  
![](./img/03_196_3__crop1.jpg)  


### 老人-宪司的妈妈
  
![](./img/04_011_1__crop0.jpg) 
![](./img/04_033_0__crop0.jpg) 
![](./img/04_033_1__crop0.jpg) 


### 雅彦的妈妈

![](./img/02_015_5__crop0.jpg) 
![](./img/02_025_1__crop0.jpg) 
![](./img/02_026_0__crop0.jpg) 
![](./img/05_186_1__crop0.jpg) 


### 雅彦的爸爸
![](./img/01_082_5__crop0.jpg) 
![](./img/05_170_2__crop0.jpg) 


### 叶子的妈妈  
 
![](./img/13_135_6__crop0.jpg) 
![](./img/13_136_4__crop0.jpg) 
![](./img/13_146_0__crop0.jpg) 
![](./img/13_151_5__crop0.jpg) 
![](./img/13_185_4__crop0.jpg) 
![](./img/13_186_1__crop0.jpg) 
![](./img/13_186_4__crop0.jpg) 
![](./img/13_186_7__crop0.jpg) 
![](./img/13_187_2__crop0.jpg) 


### 藤崎Fujisaki 茜Akane
![](./img/06_137_0__crop0.jpg) 
![](./img/06_151_4__crop0.jpg) 
![](./img/06_161_3__crop0.jpg) 
![](./img/06_161_3__crop1.jpg) 
![](./img/06_161_5__crop0.jpg) 

### Kohei耕平 Nishina仁科


### 理沙Risa(个子高，发色浅)（和紫苑一起高中毕业旅行）


### 美菜Mika(个子矮，发色深)（和紫苑一起高中毕业旅行）


### 千夏Chinatsu(个子高，发色浅)（和紫苑一起加入映研社）


### 麻衣Mai(个子矮，发色深)（和紫苑一起加入映研社）


### 齐藤Saito 玲子Reiko


### Matsushita松下 Toshifumi敏史, 棒球运动员, 他喜欢紫苑


### Saitou齐藤 Fumiya文哉，美菜的男友, 穿黑上衣、灰裤子

### Kazuki一树，Fumiya文哉 的同学, 戴一幅眼镜、短裤

### Tooru阿透，Fumiya文哉的同学，穿浅色上衣、黑裤子

### 哲，美菜的另一个男友


### Furuya古屋 Kimihiro公弘, 和浩美网恋的人


### Furuya古屋 Tomomi朋美, Furuya古屋 Kimihiro公弘的妹妹
              

### Hanzu八不 Kyoko京子（若苗紫的中学插花社的成员）


### Aya绫子（若苗紫的中学插花社的成员）


### Tenko典子（若苗紫的中学插花社的成员）


### 八不先生，Kyoko京子的丈夫


## 小结  

- 我猜，在手部方面，作者对这些人物有详细的设定。主要角色的手均各具特点。  
    - 至少能区分出男性的手和女性的手；  
    - 根据人物的特点，其手往往也有相应的特征。  

- 有些时候，手背画出了肌腱(下图位置1处)、关节的线条，但未画指甲。这可能意味着前者是比指甲更重要的特征。此外，通过观察FC中手背的皱纹发现：皱纹出现在下图2处的频率比出现在3处的频率高。综合这些，我猜，在手背上画细节（皱纹、指甲）的优先位置如下图1、2、3所示（从手掌背部到指尖，优先级递减）。      
![](./img/hand_crease_priority.jpg)  



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
