
## 绘画中的一些细节
### 手指跟部的皱纹  
FC里手指根处（下图红框所示）常常有一些细节：    
![](./img/11_156_5__crop0_mod2.jpg) 
![](./img/14_207_0__crop0_mod2.jpg) 
![](./img/08_141_3__crop1_mod2.jpg) 

为了说明这些细节的效果，以下对比“删去这些细节后的效果”。为了排版美观，将以下图片分为几组，每一组的第一行是原图，第二行是“删去这些细节后”的图。    

group1:  
![](./img/05_068_5__crop0.jpg) 
![](./img/11_135_3__crop0.jpg) 
![](./img/11_156_5__crop0.jpg) 
![](./img/12_054_1__crop0.jpg) 
![](./img/13_069_6__crop0.jpg) 

![](./img/05_068_5__crop0_mod1.jpg) 
![](./img/11_135_3__crop0_mod1.jpg) 
![](./img/11_156_5__crop0_mod1.jpg) 
![](./img/12_054_1__crop0_mod1.jpg) 
![](./img/13_069_6__crop0_mod1.jpg) 

group2:  
![](./img/13_031_0__crop0.jpg) 
![](./img/14_251_6__crop0.jpg) 
![](./img/11_154_3__crop0.jpg) 
![](./img/09_134_1__crop0.jpg) 
![](./img/14_076_0__crop0.jpg) 

![](./img/13_031_0__crop0_mod1.jpg) 
![](./img/14_251_6__crop0_mod1.jpg) 
![](./img/11_154_3__crop0_mod1.jpg) 
![](./img/09_134_1__crop0_mod1.jpg) 
![](./img/14_076_0__crop0_mod1.jpg) 

group3:  
![](./img/14_175_2__crop0.jpg) 
![](./img/13_148_1__crop0.jpg) 
![](./img/13_095_2__crop0.jpg) 
![](./img/04_041_0__crop1.jpg) 
![](./img/10_130_1__crop0.jpg) 

![](./img/14_175_2__crop0_mod1.jpg) 
![](./img/13_148_1__crop0_mod1.jpg) 
![](./img/13_095_2__crop0_mod1.jpg) 
![](./img/04_041_0__crop1_mod1.jpg) 
![](./img/10_130_1__crop0_mod1.jpg) 

group4:  
![](./img/13_135_6__crop0.jpg) 
![](./img/13_151_5__crop0.jpg) 
![](./img/13_167_4__crop0.jpg) 

![](./img/13_135_6__crop0_mod1.jpg) 
![](./img/13_151_5__crop0_mod1.jpg) 
![](./img/13_167_4__crop0_mod1.jpg) 

group5:  
![](./img/13_090_2__crop0.jpg) 
![](./img/13_129_1__crop0.jpg) 
![](./img/14_207_0__crop0.jpg) 
![](./img/09_032_0__crop0.jpg) 

![](./img/13_090_2__crop0_mod1.jpg) 
![](./img/13_129_1__crop0_mod1.jpg) 
![](./img/14_207_0__crop0_mod1.jpg) 
![](./img/09_032_0__crop0_mod1.jpg) 

group6:  
![](./img/10_163_1__crop0.jpg) 
![](./img/13_146_0__crop0.jpg) 
![](./img/14_262_4__crop0.jpg) 

![](./img/10_163_1__crop0_mod1.jpg) 
![](./img/13_146_0__crop0_mod1.jpg) 
![](./img/14_262_4__crop0_mod1.jpg) 

group7:  
![](./img/12_015_3__crop0.jpg) 
![](./img/14_175_1__crop0.jpg) 
![](./img/08_141_3__crop1.jpg) 
![](./img/14_293_3__crop1.jpg) 

![](./img/12_015_3__crop0_mod1.jpg) 
![](./img/14_175_1__crop0_mod1.jpg) 
![](./img/08_141_3__crop1_mod1.jpg) 
![](./img/14_293_3__crop1_mod1.jpg) 


### 关节处的皱纹  
对于角色横田进的手（下图左一）。删去或弱化关节处的皱纹后（下图左二），仍不像女性的手。可能是由于该手画得粗壮。    
![](./img/03_017_1__crop0.jpg) 
![](./img/03_017_1__crop0_mod1.jpg) 





--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

