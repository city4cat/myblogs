source: 
http://users.skynet.be/mangaguide/au460.html  
([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96845))  

Parrot - Koufuku no Hito [Parrot - The Blessed Man]  
ＰＡＲＲＯＴ（パロット）  
short story collection  
published by Shuueisha,  
1 volume @ Y952  
[ 01:0002]  
The short stories collection done in color, with CG backgrounds. A break through title (At least I have not see one like it before) which cover many new grounds, like in credits they have for computer company, models, photographers, stylist, customs, and makeup artists, etc.  

The main story is the 9 scenes Parrot, A guy who can imitate any voice he heard. Plus 4 short stories of The Eyes of Assassin, Air Man, Cat's Eye, and Portrait of Father. The stories tend to be melodrama and nice. Houjou seems to be writing scripts for a TV productions. The artwork quality is typically Houjou, consistently good. Houjou's story and taste has obviously changed from his City Hunter day. People who can enjoy a small, nice and quiet story should have no problem with this one. At this price I can only recommend it to people with that particular taste.  

On a side note. As a Mac user I like to point out this marks another mangaka who prefers the use of Mac like Masamune Shirow, Yui Toshiki, Kozou Youhei, and Terasawa Buichi, etc. {CPK}  

Note the appearance of Kamiya Akira (the seiyuu) of City Hunter's Ryo as the Airman. {PVH}  
   
   
最后提到：   
Note the appearance of Kamiya Akira (the seiyuu) of City Hunter's Ryo as the Airman.
(CityHunter里獠的声优神谷明现身为Airman)  

《Airman》里女主说：“你是配音员神谷郎对吧？我很迷配音员呢！早就觉得声音很熟...我是你的影迷哟”。后来，Airman接到电话说：“我是神谷！什么？六点进录音室？好！我记得了！不会忘记的！！是的，我马上到！”  


这让我想到《Parrot-幸福的人》里男主会口技、黄头发。神谷明是声优、经常见他染黄色头发。不知道这两者是否有联系呢？  





