登場人物紹介

[ 上へ ](./chara.md)   
[ 若苗家 ](./charawaka.md)   
[ アシスタント ](./charassi.md)  
[ 名脇役 ](./charawaki.md)  
[ 超脇役 ](./charare.md)  

**若苗家**

柳葉雅彦
　やなぎばまさひこ 	
主人公、20歳、大学3年生、
1978/10/21生 	
初出：[1st.day](./vol01-14.md)

    - 小学2年のときに母をガンで亡くし（命日は1月11日か？）父と二人で暮らしてきたが、その父も、雅彦が高校を卒業する年の2月に他界。母が亡くなる直前に雅彦に伝えた「ある事情で縁を切った弟」の家に居候をすることになる。それが若苗家だった。
    - 武蔵野産業大学経済学部3年生。江島とは入学式のときに前後に座っていた関係で知り合いになり、彼の陰謀もあり映研に所属。
    - アシさんたちからは「まーちゃん」と呼ばれる。
    - たびたび女装に縁がある。実はこれが作者の楽しみ？
    - 仁科耕平に惚れられて、紫苑も男装して、心配事ばっかりでため息の出る毎日。

[フリートーク「雅彦、苦難（口難？）の日々」](../post/03.md)も参考に。

　
若苗紫苑
　わかなえしおん 	
雅彦の従姉妹、18歳、大学1年生、
1981/3/19生 	
初出：[1st.day](./vol01-14.md)

    - 若苗家の一人娘。誕生日は、Vol.2の表紙で明らかになる。家族全員で撮ったポラロイド写真に「紫苑の誕生会、’97年3月19日」とあるため。
    - 9th. dayの家族旅行までは、雅彦は「紫苑さん」と呼んでいたが、その後は「紫苑」と呼び捨てに。実は雅彦の初恋の相手が紫苑だったことがVol.5(35th. day)で判明。
    - Vol.12以降は雅彦と同じ武蔵野産業大学の文学部英文科1年生。

　
若苗空
　わかなえそら 	
紫苑の父？、38歳、
旧姓菊地、本名若苗春香、1961/02生 	
初出：[1st.day](./vol01-14.md)

    - 春風空というペンネームの超一流漫画家。仕事が上がったときは、打ち上げの宴会（カラオケ）があるがその模様は何と・・・・・・裸踊り！！進ちゃんによると、毎週行われていたらしい。
    - 高校は私服で通えるところに行きたかったが、父親の反対で地元の高校に入学。そこで「学生服事件」が発生。
    - 見た目男性で、読んでいるこっちも彼が本当は女であることを忘れてしまう。

　
若苗紫
　わかなえゆかり 	
紫苑の母？、38歳、
本名若苗龍彦、1961/02生 	
初出：[1st.day](./vol01-14.md)

    - 本名若苗龍彦。「ある事情で縁を切った弟」とは彼女？のことで、雅彦の実の叔父にあたる。姉の結婚の際に両親の強要で存在を消された苦い思い出（＝ある事情）を持つ。雅彦の母である姉とは、仲がよかった。
    - 中学時代は華道部。女だけの部に男一人、ということで、アイドル的存在だったらしい。
    - 紫苑の通う高校と同じ女子高に通っていた。

　
菊地？　きくち？　 	
空の父 	
初出：[20th.day](./vol01-14.md)

    - 順子結納のときに登場するも、フルネームは一度も出てこなかった。
    - 頑固親父で空が男になったことを未だ許せないようだが、一度振り上げたこぶしを下ろすタイミングが見当たらないのかな。
    - 紫から菊地家に送られてきた手紙は、最初は読まなかったようだが、後に何度も何度も読み返すようになり、春香夫婦のことをとても心配していた。
    - 42nd. day で「友達：菊造」を名乗るも、やはり本名は不明。
    - 順子さんが結婚式を挙げても、本名は出てこない。北条先生、設定してますか？

　
菊地節子　きくちせつこ 	
空の母 	
初出：[21st.day](./vol01-14.md)

    - 順子結納のときに登場。空の父が隠し持っていた、紫からの手紙の宛名より名前が判明。
    - 空の同級生で親友の憲司と空の妹の順子の仲に気づかないなど、結構鈍感な人。

　
奥村憲司　おくむらけんじ 	
空の同級生、38歳 	
初出：[21st.day](./vol01-14.md)

    もとの奥さんと死別（癌、4年前＝93年）。順子に対する自分の気持ちには気付いていたが言い出せなかった。一女の父。
    空の妹の順子と結婚したので、空とは姉弟（兄弟？）に！？

　
奥村順子　おくむらよりこ（旧姓：菊地） 	
空の妹、31歳 	
初出：[20th.day](./vol01-14.md)

    - Vol.3(20th. day)で初登場。父親に、見合いによる望まぬ結婚をさせられようとするが、何とか回避。
    - 空の同級生で親友の奥村憲司と結婚した。
    - Vol.12終了時点（推定99年5月）で妊娠8ヶ月。そろそろ生まれるか？
    - 連載終了時（99年7月27日）に女児誕生。2861グラムで母子ともに健康。

　
奥村章子　おくむらしょうこ 	
憲司の一人娘 	
初出：[22nd.day](./vol01-14.md)

    - 憲司の前妻の忘れ形見。
    - 非常に勘が鋭く、憲司が順子を好きであることに早いうちから気づいた。
    - 憲司が順子と結婚した後の新婚旅行では、一人家に残るというおませな女の子。

　
　
	トップページへ戻る
Copyright 1996-2003 Rix All rights reserved.
Privacy Policy & Site Info.