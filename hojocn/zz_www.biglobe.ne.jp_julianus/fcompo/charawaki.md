登場人物紹介

[ 上へ ](./chara.md)   
[ 若苗家 ](./charawaka.md)   
[ アシスタント ](./charassi.md)  
[ 名脇役 ](./charawaki.md)  
[ 超脇役 ](./charare.md)  

名（迷？）脇役
江島卓也　えじまたくや 	
雅彦の友人 	
初出：[4th.day](./vol01-14.md)  

    - 映画研究会の部員で、俳優を目指す。雅彦を映研に引きずり込んだ（監督も参照）。どんな時もポジティブな性格で、しかしそれがかえって自分を失敗に導いている部分もある。
    - この人も意外に女装が似合う。

　
森さん　もりさん 	
担当・編集 	
初出：[7th.day](./vol01-14.md)  

    - その人物像はいろんな人のセリフからわかる。紫さん「怖い担当さんね」、マコちゃん「超苦手っすぅ」、空さん「この鬼編集ッ！！」。
    - しか～し！彼女が67th.day「二人の取材旅行」ではじめて見せた「女性」の顔は！！普段はあんなんでも、やっぱり、いいひとみたいですね。

　
浅岡葉子　あさおかようこ 	
雅彦のガールフレンド？、ニックネーム：ハコ 	
初出：[10th.day](./vol01-14.md)  

    - 美術大学に通う、雅彦の高校時代の同級生。
    - 雅彦の女装事件をきっかけに、自分が雅彦に惚れていることを自覚するが、肝心の雅彦のほうは・・・？
    - ついに実力行使に出るか？雅彦に迫る！

　
監督　かんとく 	
映画研究会の監督 	
初出：[12th.day](./vol01-14.md)  

    - 映研の監督。雅彦とは、紫苑を主人公にした映画（「ナイト・レパード」）を撮ったときから知り合いになる。その撮影中に何とかして紫苑をビデオに撮ろうと企みシャワー室にカメラを隠すが・・・・・・結果としてテープには雅彦のシャワーシーンが写ることに。そのテープを巧みに利用し、雅彦を映研に引きずり込んだ。
    - 監督としてのうでは？、悪巧みは一流。「柳葉雅美」主演の自主映画で部員を獲得という荒業をやり、新歓コンパでもその悪知恵を余すとこなく発揮した。が、半年後新入部員は三人にまで減った！
    - 優秀な編集者がいるのか、それとも彼の腕が凄いのか、雅彦のシャワーシーンを利用して合成でとんでもないものを作り上げた。ホントに、悪巧みだけは超一流。

　
辰巳さん　たつみさん 	
青丹興業社長 	
初出：[18th.day](./vol01-14.md)  

    - 華舞伎町周辺を縄張りとする暴力団「青丹興業」の社長。
    - 映画撮影のために女装した雅彦（雅美）に一目惚れし、ホテルにまで連れ込もうとした人。芸能プロダクションの社長と顔見知りなど、意外と顔は広いかも？
    - しかし、彼はあきらめていなかった！！春休みに、自らの経営するゲイバーで雅彦をバイトさせ、女性化を計るが、見事に失敗。
    - ついに娘が登場。彼女の破天荒ぶりには唖然。でもいい方向へ進んでいるよう。
    - 暴力団の頭だと思っていたけれど、上がいるらしいねえ。
    - 親ばかとはまさに彼のこと。娘の顔見たさに仮病使うとは。でも同様の人物がもう一人・・・似たもの夫婦。

　
葵さん　あおいさん 	
ゲイバーひみつの花園店員 	
初出：[37th.day](./vol01-14.md)  

    - 雅彦が一時期バイトしたひみつの花園で働く、辰巳さんにゾッコンの女性（？）。
    - 雅彦が店員一同に紹介されたときに、真っ先に怪しいとにらんだ、感の鋭い人。
    - 滅多に表に出さない紫苑の感情を一目で見抜いた。オカマの勘よ、とおちゃらけてみせたが、自信はあるに違いない。

　
藤崎茜　ふじさきあかね 	
映研の後輩、コレズ？ 	
初出：[41st.day](./vol01-14.md)  

    - 女の人が好きという女の子。学園祭で見た「柳葉雅美」に惚れて、武蔵野産大進学を決心したとか。雅彦の真の姿が「雅美」と勘違いしていたらしく、男だと知ったときには大泣きした。しかし、酔っていたため忘れたのか、数日後には再び雅彦に慕ってきた。
    - 映研の夏合宿で、雅彦が男とはっきり分かる事件が発生。しかし、そのときにゲスト参加した紫苑を見て、紫苑に惚れたか？
    - 最近はぜんぜん姿を見せないが、もしかして退部した？

　
真朱薫　まそほかおる 	
辰巳さんの一人娘 	
初出：[47th.day](./vol01-14.md)  

    - 本当の男だったら、最悪のパターン。「武蔵野産大の2年、マサヒコ」を名乗り、女性を騙し金を巻き上げていた犯人。しかし、本人の登場で収まったか？
    - 父親の辰巳さんから逃げて、若苗家に居候（お邪魔？）することに。しかし、紫苑の手にかかり、女に戻りつつあるような・・・？
    - Vol.11では嫉妬丸出し。

　
真朱早紀　まそほさき 	
薫の母 	
初出：[61st.day](./vol01-14.md)  

    - 悪女とはこの人のためにある言葉かもしれない。辰巳さんから送られてくる養育費で遊びまくっていた。
    - しかし辰巳さんから5000万をせしめた後の対応には、結構・・・。
    - すでにどこか別の町へ行っていたはず・・・ところが、99年の正月を迎えようとしているときに、薫の住む町へと舞い戻ってきていた。やっぱり親なのね。親ばかとはまさに彼女のこと。娘の顔見たさに仮病使うとは。でも同様の人物がもう一人・・・似たもの夫婦。

　
仁科耕平　にしなこうへい 	
雅彦の友人 	
初出：[69th.day](./vol01-14.md)  

    - 「青春ラブロード」の主演「柳葉雅美」に一目惚れした、雅彦と同じクラスのモテモテ男子学生。「雅美」を男と知ったときにますます胸が高鳴ったという人物。
    - アシさんたちに同情され、ゲイバーでバイトをし始め、本格的にその道に入ろうとしている。
    - 80th.day「成人式はミステリアス」にて雅彦とホテル街へ突入・・・したのかしてないのか？私、隊長はしていないと信じたい・・・！！ 

　
千夏＆麻衣　ちなつ・まい 	
紫苑の大学の同級生 	
初出：[82nd.day](./vol01-14.md)  

    - 実際に名前が明らかになるのは83rd.day「足は口ほどにモノを言う・・・」の回。栗毛のショートが千夏で、黒髪のセミロングが麻衣。
    - 入部テストの5分間ムービーで、千夏は自演の落語を演じたが、麻衣は不明（風景のカットはうまいらしい）。今後どのように活躍するか楽しみ。ところで、先輩の藤崎茜はどこ行った？

　
浅葱藍 	紫苑の同級生 	
出演：[93～96、98～102nd.day](./vol01-14.md)  

    - 紫苑の同級生。小学3年生まで同じ学校だったが、偶然にも同じ大学に通うことに。
    - 演劇部に所属する法学部の1年生で、「幻の君」と噂されていた「とされる」美女。もちろん「幻の君」は入学式に一度だけ女の姿で来た紫苑のことだが、浅葱にとってはいい迷惑かも・・・。
    - 紫苑にとっては男の子を演じていたときの初恋の相手。もちろん浅葱にとっても紫苑「君」は初恋の相手。思わぬ再開に二人はどうする・・・？
    - 最後の脇役となってしまったが、もしコアミックスの予定が繰り上がらずに連載が続いていたら、もっと活躍していたかもしれない。

　
　
	トップページへ戻る
Copyright 1996-2003 Rix All rights reserved.
Privacy Policy & Site Info.