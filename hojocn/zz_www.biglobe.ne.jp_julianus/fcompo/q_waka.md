F.COMPO Q&A 若苗家編

[雅彦編](./q_masa.md) 	
[紫苑編](./q_shion.md) 	
[若苗家編](./q_waka.md)  
[脇役編](./q_waki.md)  
[その他](./q_etc.md) 	

Q1、 	若苗家の所在地は？  
        🇨🇳若苗家的位置在哪里？  
A1、 	東京都豊島区雑司ヶ谷4丁目24‐15　TEL XXXX-5XX1（1st.day）  
        🇨🇳东京都丰岛区左卫门4-24-15 电话：XXXX-5XX1（1.day）。  
雑司ヶ谷までは存在するんだけど、4丁目以降はどうなのかなあ。隊長は熊本在住のためわかりません。   
🇨🇳它一直存在到左卫门，但我不知道4丁目以后的情况。 站长不知道，因为站长住在熊本县。  
    雑司ヶ谷は3丁目までです。家の地図で確認しました。お隣の南池袋は4丁目まであり、そこに都営雑司ヶ谷霊園があります。雑司ヶ谷4丁目があるとするとこのへんでしょうかねぇ。若苗家からの最寄りの駅は、「鬼子母神前」駅です（Vol.3、17th.day「追い討ち」78ページ）。この都電荒川線というのは6th.dayのラストに出てくる路面電車のことです。ちゃんと現実の終点も早稲田で一致します。ちなみにもう片方の終点は三輪橋です。（提供：ハル）  
    🇨🇳雑司ヶ谷是到3丁目。 这与房子的地图相印证。 隔壁的南池袋是4丁目，有一个市立左卫门公墓。 如果有左卫门4丁目，可能就是这个区域。 离若苗家最近的车站是 "鬼子母神前"车站（第3卷，第17天，"追い討ち"第78页）。 这条都電荒川線是在第六话结束时出现的街车。 现实中的终点是早稻田。 顺便说一下，另一个终点是三輪橋。 (小春提供)  
　

Q2、 	空さんの連載誌は？  
        🇨🇳空的连载杂志是什么？   
A2、 	空さんが連載している作品名は「俺たちの紋章」、ペンネームは「春風空」。ここまではいいとして、問題は連載誌名。10th.dayで浅岡葉子初登場のときに担当の森さんが「少年ホップ」の名を口にする。  
        🇨🇳空的连载作品被称为"俺たちの紋章"，而他的笔名是 "春風空"。 到目前为止，情况还不错，但问题是连载杂志的名称。 当浅岡葉子第一次出现在第十话时，负责人森小姐提到了"少年ホップ"的名字。  
　

Q3、 	紫苑の両親の本名は？  
        🇨🇳紫苑的父母的真名是什么？   
A3、 	
    若苗空：若苗春香（15th.day）、97年に36歳  
    若苗紫：若苗龍彦（14th.day）、空と同い年であることが 28th.day に判明。誕生月は、30th.day の免許証より2月。更新は2月11日となっているが・・・？誕生日は近いらしいので、2人とも2月中旬でしょう。  
    🇨🇳若苗空：若苗春香（第15话），1997年36岁。   
    若苗紫：若苗龍彦（第14话），与空同年（第28话）。从他（第30话）的驾照来看，他的出生月份是二月。 （驾照）更换日期是在2月11日......？ 看来他们的生日很接近，所以两个人的生日一定是在2月中旬。   
　

Q4、 	今は亡きあの車  
        🇨🇳那辆现在已经怀了的车  
Q4、 	もともとは、外車だった空さんの車。メーカーは「PEUGEOT（プジョー）」、型式は「605（のように見えるんだけど・・・）」。ナンバーは「？？14-25」（Vol.2 9th.day）。  
        🇨🇳最初，空的车是一辆外国车。 品牌是 "PEUGEOT"，型号是 "605"。 车牌号是"? 14-25"（第2卷第9话）。   
　

Q5、 	若苗家の電話機は？（提供：とも）  
        🇨🇳若苗家的电话在哪里？ (托莫提供)  
A5、 	若苗家の電話機がうちのに似てるなぁと常々思っていたのですが、実際比べてみると・・・なんと瓜二つでした（笑）。SANYOのCUTEで型番はTEL-L321です。外見が同じで別の型番もあるとは思いますが。電話機本体はVol.8、56th.day、180ページ、受話器はVol.10、69th.day 145および159ページで確認しました。（提供：とも）  
        🇨🇳我一直认为若苗家的电话与我们的相似，但当我对比时，我发现他们完全一样。 我认为还有一个外观相同的型号。 我在第8卷，第56话，第180页检查了手机本身，在第10卷，第69话，第145和159页检查了手机。 (托莫提供)    
　

Q6、 	今の車  
        🇨🇳该车现在
A6、 	Vol.11、73rd.day「すれ違いのイブ」で車が出てきます。後ろにあるメーカーのロゴマークは、ホンダのものに近いですね。そこでホンダのWebサイトを見てみると・・・まさしくぴったりのものがありました。[Odｙssey](https://web.archive.org/web/20050410143930/http://www.honda.co.jp/auto-lineup/odyssey/)([direct](http://www.honda.co.jp/auto-lineup/odyssey/))です。    
        🇨🇳Vol.11, 73话 "Passing Eve" 展示了一辆汽车。 背面的制造商标志与本田的标志接近。 所以我在本田的网站上看了一下......并找到了完全正确的那一款。

