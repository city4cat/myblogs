
# Cat's Eye 40周年纪念原画展的展品数据  


## 说明  
1. 本文的动机是为了备份Cat's Eye 40周年纪念原画展的商品数据。之所以要备份，是因为我发现edition-88官网于2022年11月份展示该原画展(福冈)的信息时，其2022年5月份该展(千代田)的部分展品被删除了，随之那些展品的局部超高清图片也看不到了。  
2. 以下资料或许有助于更好地理解该展览的展品信息：  
    - [关于纸张和印刷技术的资料](./extra_info.md)  
    - [关于官网展览和展品的一些说明](./about_edition88.md)  


## 福冈展的展品数据（2022年11月27日）  

### 高級Art Print 1 (A4)/Cat's♥Eye (1990.12)  
![](img/artprint1_A4_01_thumb.jpg)  

[日文版商品链接 高級アートプリント1 (A4)/Cat's♥Eye](https://edition-88.com/products/catseye-artprint1-a4)  
¥1,540  
「北条司ILLUSTRATIONS1991年ポスター（裏）」のイラストを使用した高級アートプリントです。  
由「北条司ILLUSTRATIONS 1991海报（背面）」绘制的高质量艺术印刷品。  

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。  
高清晰度全彩印刷品以生动的色彩再现了北条司先生的作品。  

SizeはコレクションしやすいA4Size。  
尺寸为A4，便于收集。  

大好きな作品を身近に感じられるアイテムです。  
这个项目可以让你感受到与你喜欢的作品的亲近。  

【仕様】  
【规格】 
●Size：A4(H297×W210mm)  
尺寸: A4 (H297xW210mm)  
●技法：オンデマンドプリント  
技术: 按需印刷  
●素材　紙：マーメイド紙  
材料 紙: Mermaid纸  

©北条司／Coamix 1981

[英文版商品链接 Cat's Eye, Art Prints #1 (A4 size / 10 sheets per set)](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
高清晰度的全彩印刷，生动地复制了10幅北条司的精选原创插画。  

The size is A4 (297 x 210mm) for easy collection.  
尺寸为A4（297 x 210mm），便于收藏。  

Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.
请在你的房间里展示你最喜欢的插画，品位Cat's Eye的世界。  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  


---    

---  

---  

### 豪华艺术印刷品2（A4）  


---    

---  

---  

### 豪华艺术印刷品3（A4）  

![image](img/A4artprint03_thumb.jpg)  

[日文版商品]无

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
高清晰度的全彩印刷，生动地复制了10幅由Tsukasa Hojo精选的原创插图。  

The size is A4 (297 x 210mm) for easy collection.  
尺寸为A4（297 x 210mm），便于收藏。  

Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.  
请在您的房间里展示您喜欢的插图，享受猫眼的世界。  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

---    

---  

---  

### 豪华艺术印刷品4（A4）  

![image](img/A4artprint04_thumb.jpg)  

[日文版商品]无

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.
The size is A4 (297 x 210mm) for easy collection.
Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.

Size: A4 (H297 x W210mm)
Technique: On-demand printing
Material: Fine art paper 'Marmaid' (Made in Japan)
Unframed / No signed /No edition number

---    

---  

---  

### 豪华艺术印刷品 5（A4）  

![image](img/A4artprint05_thumb.jpg)  

[日文版商品]无

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
The size is A4 (297 x 210mm) for easy collection.  
Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

---    

---  

---  

### 豪华艺术印刷品6（A4）  

![image](img/A4artprint06_thumb.jpg)  

[日文版商品]无

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
The size is A4 (297 x 210mm) for easy collection.  
Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

---    

---  

---  

### 豪华艺术印刷品7（A4）  

![image](img/A4artprint07_thumb.jpg)  

[日文版商品]无

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
The size is A4 (297 x 210mm) for easy collection.  
Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

---    

---  

---  

### 豪华艺术印刷品8（A4）  

![image](img/A4artprint08_thumb.jpg)  

[日文版商品]无

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
The size is A4 (297 x 210mm) for easy collection.  
Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

---    

---  

---  

### 豪华艺术印刷品9（A4）  

![image](img/A4artprint09_thumb.jpg)  

[日文版商品]无  

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
The size is A4 (297 x 210mm) for easy collection.  
Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

---    

---  

---  

### 豪华艺术印刷品10（A4）  

![image](img/A4artprint10_thumb.jpg)  

[日文版商品]无  

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
The size is A4 (297 x 210mm) for easy collection.  
Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

---    

---  

---  

### 豪华艺术印刷品11（A4）  

![image](img/A4artprint11_thumb.jpg)  

[日文版商品]无  

[英文版商品 Cat's Eye, Art Prints A4](https://edition88.com/products/catseye-artprint1-set-a4)  
¥811.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

High-definition, full-color printing vividly replicates 10 selected original illustrations by Tsukasa Hojo.  
The size is A4 (297 x 210mm) for easy collection.  
Please display your favorite illustrations in your room and enjoy the world of Cat's Eye.  

Size: A4 (H297 x W210mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

---    

---  

---  

### 高級アートプリント1 (B2) /Cat's♥Eye (1996.10)  
![](img/B2_Artprint_01_thumb.jpg)  
本画展的这张图（上图）以前多见于画册《北条司漫画家20周年記念》(20th anniversary illustrations)和《北条司美女写真馆》。

[日文版商品链接 高級アートプリント1 (B2) /Cat's♥Eye](https://edition-88.com/products/catseye-artprint1-b2)  
¥13,200  
ジェイブックス 1996年 「CAT’S♥EYE」の表紙Coverを、高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。  
Jay Books 1996 「CAT'S ♥ EYE」的封面是高分辨率的全彩印刷品，以生动的色彩表现了北条司先生的作品。  

SizeはB2で大きいため迫力抜群！  
尺寸是B2，这么大，这么有力量!  

お好きな額に入れてお部屋に飾ってみませんか？  
为什么不把它放在你选择的相框里，然后展示在你的房间里呢？  

●Size：B2（縦728mm×幅515mm）  
尺寸：B2（长728mmx宽515mm）  

●技法：オンデマンドプリント  
技术：按需印刷  

●材料  
  紙：マーメイド紙  
材料  
  纸：Mermaid纸  

[英文版商品链接 Cat's Eye, Art print #1 (B2)](https://edition-88.com/products/catseye-artprint1-b2)  
¥695.00 CNY
The illustration by Tsukasa Hojo for the cover of "CAT'S♥EYE" published by J-Books in 1996 was vividly replicated in high-definition full color printing.  
1996年J-Books出版的"CAT'S♥EYE"封面上由北条司创作的插图被生动地复制到了高清全彩印刷品中。  

The size is B2 (728 x 515mm), which is very large, so it will surely brighten up your room.  
尺寸为B2（728 x 515mm），非常大，所以它一定会照亮你的房间。  

Size: B2 (728 x 515mm)  
Technique: On-demand printing  
Material: Fine art paper 'Marmaid' (Made in Japan)  
Unframed / No signed /No edition number  

Print is wrapped and hand-rolled in acid free paper, inserted in a protective plastic sleeve, and shipped in a tube.  
印刷品用无酸纸包裹并手工卷起，插入保护性塑料套中，用管子装运。  


---    

---  

---  

### 复制原稿/1 (B2) /Cat's♥Eye (CE(18卷版)01卷01话(据该话末页的时间戳推测为1981.4))  

![](img/B2_fukuseigenko_01_thumb.jpg)  


[日文版商品链接 複製原稿/1 (B2) /Cat's♥Eye](https://edition-88.com/products/catseye-duplicateｍanuscript1-b2)  
¥4,400  
CAT’S♥EYE 「セクシーダイナマイトギャルズの巻」の扉絵を、高精細なオフセット印刷で再現しています。  
CAT'S♥EYE「性感的火爆女郎之卷」的扉页以高分辨率胶印再现。  

北条司先生の漫画原稿をスキャンし、本物そっくりに再現してみました。  
北条司先生的漫画原稿经过扫描和复制，看起来与真实的东西一模一样。  

高解像度で出力された複製原稿は、B2Sizeと大きいため迫力抜群！  
高分辨率输出的复制原稿是B2尺寸，所以很震撼!  

CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  
这个收藏品可以让你享受CAT'S ♥EYE的世界观。   

●Size：B2（縦728mm×幅515mm）  
尺寸：B2（长728毫米x宽515毫米）  
●技法：オフセットプリント  
技术：offset print  
●素材  
　紙：上質紙  
材料  
　纸：高档纸  

©北条司／Coamix1981

[英文版商品链接]无  

---    

---  

---  

### 复制原稿/2 (B2) /Cat's♥Eye  ()
![](img/B2_fukuseigenko_02_thumb.jpg)  

[商品链接 複製原稿/2 (B2) /Cat's♥Eye](https://edition-88.com/products/catseye-duplicateｍanuscript2-b2)  
¥4,400
CAT’S♥EYE 「最後のビッグゲームの巻」の三姉妹の一番の見せ場シーンを、高精細なオフセットで再現しています。  
CAT'S♥EYE 「最后的大游戏之卷」中三姐妹最精彩的一幕，以高清晰的胶印再现。  

北条司先生の漫画原稿をスキャンし、本物そっくりに再現してみました。  
北条司先生的漫画原稿经过扫描和复制，看起来与真实的东西一模一样。 

高解像度で出力された複製原稿は、B2Sizeと大きいため迫力抜群！  
高分辨率输出的复制原稿是B2尺寸，所以很震撼!  

CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  
这个收藏品可以让你享受CAT'S ♥EYE的世界。

●Size：B2（縦728mm×幅515mm）  
尺寸：B2（长728mmx宽515mm）  

●技法：オフセットプリント  
技术：胶印  

●素材  
　紙：上質紙  
材料  
　纸：高档纸  
 
©北条司／Coamix1981  

[英文版商品链接]无  

---    

---  

---  

### 复制原稿/3 (B2) /Cat's♥Eye  (1984.09.19)

![](img/B2_fukuseigenko_03_thumb.jpg)  

[商品链接 複製原稿/3 (B2) /Cat's♥Eye](https://edition-88.com/products/catseye-duplicateｍanuscript3-b2)  
¥4,400
CAT’S♥EYEコミックスに収録されている 「コレクション No.87」のイラストを、高精細なオフセット印刷で再現しています。
CAT'S♥EYE漫画中「Collection No.87」的插图以高清晰度的胶印方式再现。  

北条司先生の原稿をスキャンし、本物そっくりに再現してみました。

高解像度で出力された複製原稿は、B2Sizeと大きいため迫力抜群！

CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。

●Size：B2（縦728mm×幅515mm）

●技法：オフセットプリント

●素材
　紙：上質紙

©北条司／Coamix1981


[英文版商品链接]无  

---    

---  

---  

### 复制原稿/4 (B2) /Cat's♥Eye  (..)
![](img/B2_fukuseigenko_04_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript4-b2)  
¥4,400
CAT’S♥EYE 最終話、最終見開きページを、高精細なオフセット印刷で再現しています。  
CAT'S♥EYE 最終話、最后的对页是用高清的胶印重制的。   

北条司先生の漫画原稿をスキャンし、本物そっくりに再現してみました。  

高解像度で出力された複製原稿は、B2Sizeと大きいため迫力抜群！  

CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

●Size：B2（縦728mm×幅515mm）  
●技法：オフセットプリント  
●素材  
　紙：上質紙  

©北条司／Coamix1981


[英文版商品链接]无  


---    

---  

---  

### 「CityHunter」 版画1 (1987.06)
![](img/hanga_cty01_thumb.jpg)  

[日文版商品链接](https://edition-88.com/products/cityhunter-hanga1)  
¥59,400

税込み 送料計算済み Check Out時

週刊少年ジャンプ 1997年 特別編集　SUMMER SPECIAL/巻頭ポスター用イラストの版画です。  
週刊少年Jump 1997特别版SUMMER SPECIAL/ 卷首海报插画的版画。  

シティーハンターの版画では1枚毎、パール絵の具の粒子を吹き付けています。  
CityHunter的每张版画上都喷涂了珍珠漆颗粒。  

見る角度によって光沢が強調され、作品の奥行きが表現できました。  
从不同的观察角度强调光泽，使艺术品的深度得到表达。  

SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
这张纸的尺寸是原画的尺寸，所以你可以更接近于欣赏原画。  

北条司先生からのComment  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。  
北条司先生的评论   
在连载时，我被告知不要在彩色手稿中使用包括紫色或偏向紫色的蓝色加粉色的颜色，因为这些颜色很难在印刷中呈现。 我还是想使用它们。这一次，印刷品的颜色与原作非常接近，无论是实际绘画的颜色还是原纸的颜色。其结果是如此之好，以至于很难一眼就看出它是原作还是印刷品。40年后的今天，我已经能够重现原作的色彩，这在过去是无法用印刷品再现的。我很高兴这些版画现在在你手中。  

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、有编号、手工上色、木材装裱。  
●技法：ジクレ  
技法：giclée  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：Black  
　MatColor：Black  
材料  
　纸张：版画用中性纸  
　框架：木头，丙烯酸（表面覆盖）。  
　框架颜色: 黑色  
　亚光色：黑色  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）  
(无法选择版本编号)  
●Size  
　イラストSize：縦295×横426mm  
　額Size：縦471×横622×厚さ20mm  
尺寸  
　插图尺寸：长295x宽426mm  
　框架尺寸：长471x宽622x厚20mm  

©北条司／Coamix1985, 版権許諾証AF-502  
©北条司／Coremix 1985，版权许可证AF-502   

「Cat's♥Eye」または「シティーハンター」の商品をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！  
购买任何 "Cat's ♥ Eye"或 "City Hunter"产品，即可获得一套（2枚）明信片!  

[英文版商品链接 City Hunter, Art Print #1](https://edition88.com/products/cityhunter-hanga1)
¥2,836.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for a poster attached in ‘Weekly Shonen Jump (Special Edition)’ published in 1997.  
这幅艺术印刷品来自1997年出版的'周刊少年Jump（特别版）'所附的海报的插图。   

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  
这个艺术印刷品系列是为2022年5月在东京举行的 "Cat's♥Eye 40周年原画展 - 然后向着City Hunter "展览而创作的。  

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  
每幅印刷品有380个版本（普通版：200个，国际版：180个），都有北条司亲笔签名。  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）精致地表现色彩和纹理。  

The art prints have the same image size as the original pieces.  
艺术印刷品的图像尺寸与原作相同。   

**Comments from Tsukasa Hojo**  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.  
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.  
I am delighted that these quality art prints can find their way to you.  
**来自北条司的评论**  
当我在创作这些漫画连载时，对于我的彩页，我被告知要避免使用带有紫色的或接近紫色的蓝色加粉色，因为这些色调在杂志上很难印刷。但我还是会使用它们。  
当时的印刷技术无法表达我的一些独特色彩。40年后的今天，印刷技术的进步意味着原作中的颜色和色调可以被复现。这些艺术印刷品精确地传达了我所使用的颜色，甚至纸张的颜色也几乎与原作相同；一眼望去，很难区分艺术印刷品和原作。  
我很高兴这些高质量的艺术印刷品能够到你的手中。  

**On the Art Print, from EDITION88**  
**关于艺术版画，来自EDITION88**

Similarly, each City Hunter print is hand sprayed with pearlescent paint.   
同样，每幅CityHunter的印刷品都是手工喷上珠光颜料。    

The art prints have a luminosity that shifts with the viewer’s perspective, displaying depth.  
艺术版画有一种随观众的视角变化而变化的亮度，显示出深度。  

●Hand-signed by Tsukasa Hojo  
北条司亲笔签名  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
限量380册（国际版180册，普通版200册）  
●Mixed Media (Giclée and UV) on fine art paper for international edition  
国际版在美术纸上采用混合媒体（Giclée和UV）   
●Unframed  
无框  
●Size  
　▸Image size 295 x 426mm / 11.61 x 16.77inch  
　▸Sheet size 346 x 462mm / 13.62 x 18.18inch  
尺寸  
　▸图片尺寸 295 x 426mm / 11.61 x 16.77inch  
　▸纸张尺寸 346 x 462mm / 13.62 x 18.18inch  
●Officially licensed by Coamix Inc.  
由Coamix Inc.正式授权  
●Manufactured by EDITION88  
由EDITION88制作  

©Tsukasa Hojo/Coamix 1985  


---    

---  

---  

### 「CityHunter」 版画2 (1989.10)
![](img/hanga_cty02_thumb.jpg)  

[商品链接](https://edition-88.com/products/cityhunter-hanga2)  
¥59,400

税込み 送料計算済み Check Out時

週刊少年ジャンプ 1989年 第50号 扉絵の版画です。  
1989年周刊少年Jump第50期扉页的版画。  

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。  
Cat'sEye的版画使用白色颜料，每一张都有手工上色。  

この手作業により原画が持つWhite部分のマチエールを表現しています。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  
SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
这张纸的尺寸是原画的原始尺寸，所以你可以尽可能地欣赏到接近原作的作品。  

北条司先生からのComment
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、编号、手工彩绘、用木头装裱。  
●技法：ジクレ  
技法：giclée
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：Black  
　MatColor：Black  
材料  
　纸张：版画用中性纸  
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 黑色  
　亚光色：黑色  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）。
（不可选择版本编号）  
●Size  
　イラストSize：縦485×横322mm  
　額Size：縦622×横471×厚さ20mm  
尺寸  
　插图尺寸：长485x宽322mm  
　框架尺寸：长622×宽471×厚20mm  

©北条司／Coamix1985

「Cat's♥Eye」または「シティーハンター」の商品をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！  
购买任何「Cat's ♥ Eye」或「City Hunter」产品，即可获得一套（2个）明信片!    

[商品链接](https://edition88.com/products/cityhunter-hanga2)
¥2,836.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the title page of Weekly Shonen Jump No.50 published in 1989.  
这幅艺术印刷品来自1989年出版的周刊少年Jump第50期扉页的插图。  

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  
这个艺术印刷品系列是为2022年5月在东京举行的 "Cat's♥Eye 40周年原画展 - 然后向着City Hunter"展览而创作的。 

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  
每幅印刷品有380个版本（普通版：200个，国际版：180个），都有北条司本人的签名。   
The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），以精致地表现色彩和纹理。   
The art prints have the same image size as the original pieces.  
艺术印刷品的图像尺寸与原作相同。 

**Comments from Tsukasa Hojo**  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.  
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.  
I am delighted that these quality art prints can find their way to you.

**On the Art Print, from EDITION88**  
Similarly, each City Hunter print is hand sprayed with pearlescent paint.  
同样，每幅CityHunter都是手工喷上珠光颜料。   
The art prints have a luminosity that shifts with the viewer’s perspective, displaying depth.  
艺术版画有一种亮度，随着观众的视角变化而变化，显示出深度。   

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Mixed Media (Giclée and UV) on fine art paper for international edition  
国际版用的是美术纸上的混合媒体（Giclée和UV）   
●Unframed  
●Size  
　▸Image size 464 x 322mm / 18.27 x 12.67inch  
　▸Sheet size 512 x 360mm / 20.15 x 14.17inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  


---    

---  

---  

### 「CityHunter」 版画3 (1991)
![](img/hanga_cty03_thumb.jpg)  

[商品链接](https://edition-88.com/products/cityhunter-hanga3)  
¥59,400

税込み 送料計算済み Check Out時

北条司 ILLUSTRATIONS 1991年 付録ポスター用イラストの版画です。  
北条司 ILLUSTRATIONS 1991 附录的海报版画。  
シティーハンターの版画では1枚毎、パール絵の具の粒子を吹き付けています。  

見る角度によって光沢が強調され、作品の奥行きが表現できました。  
CityHunter的每张版画都喷上了珍珠漆颗粒。  
SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
纸张尺寸是原画的原始尺寸，让观众可以更近距离地欣赏原画。   

北条司先生からのComment  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、编号，手工上色，木质画框。
●技法：ジクレ  
技法：giclée
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：Black  
　MatColor：Black  
材料  
　纸张：版画用中性紙  
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 黑色  
　亚光色：黑色  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）  
（不可选择版本编号）  
●Size  
　イラストSize：縦380×横266mm  
　額Size：縦622×横471×厚さ20mm  
尺寸  
　插图尺寸：长380x 宽266mm  
　框架尺寸：长622×宽471×厚20mm  

©北条司／Coamix1985

「Cat's♥Eye」または「シティーハンター」の版画をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！

[商品链接 Art Print #3](https://edition88.com/products/cityhunter-hanga3)
¥2,836.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the poster attached in ‘Tsukasa Hojo’s illustrations’ published in 1991.  
这幅艺术印刷品来自1991年出版的'北条司illustrations'的附带的海报的插画。   

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），以精致地表现色彩和纹理。   

The art prints have the same image size as the original pieces.  

**Comments from Tsukasa Hojo**
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.
I am delighted that these quality art prints can find their way to you.

**On the Art Print, from EDITION88**
Similarly, each City Hunter print is hand sprayed with pearlescent paint.  
The art prints have a luminosity that shifts with the viewer’s perspective, displaying depth.  

●Hand-signed by Tsukasa Hojo
●Limited edition of 380(International edition 180 , Regular edition 200 )
●Mixed Media (Giclée and UV) on fine art paper for international edition
国际版在美术纸上采用混合媒体（Giclée和UV）  
●Unframed
●Size
　▸Image size 380 x 266mm / 14.96 x 10.47inch
　▸Sheet size 465 x 346mm / 18.30 x 13.62inch
●Officially licensed by Coamix Inc.
●Manufactured by EDITION88

©Tsukasa Hojo/Coamix 1985


---    

---  

---  

### 「CityHunter」 版画4 (1987-01)  
![](img/hanga_city04_thumb.jpg)  

[日文版商品链接](https://edition-88.com/products/cityhunter-hanga4)，  
¥59,400  

週刊少年ジャンプ 1987年 第11号 扉絵の版画です。  
週刊少年Jump 1987年第11期，扉页的版画。   

シティーハンターの版画では1枚毎、パール絵の具の粒子を吹き付けています。  
对于CityHunter的每张印刷品，都喷上了珍珠漆的颗粒。  

見る角度によって光沢が強調され、作品の奥行きが表現できました。  
光泽度的强度取决于观察的角度，以表达艺术品的深度。   

SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
纸张尺寸是原版印刷品的尺寸，所以你可以更接近于原版的欣赏。  

**北条司先生からのComment**  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。  
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。  
版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。  

**版画工房からのComment**  
来自版画工坊的评论  

夕焼けの赤から濃いブルーへのグラデーションが美しく、その夜空に映えるグリーンの建物が印象的な作品です。グリーンは主人公の輪郭でも使用されている重要な色なので、慎重に色を合わせていきました。  
夕阳从红色到深蓝色的渐变很美，而夜空中的绿色建筑也很醒目。 绿色是主角轮廓中使用的重要颜色，所以颜色的搭配很谨慎。 

星空の美しさを引き出すために、全体に薄くパール絵具を吹き付けることで、作品に立体感が生まれました。  
为了衬托出星空的美丽，在整个作品上喷了一层薄薄的珍珠漆，使其具有立体的感觉。  

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、编号、手工上色，用木头装裱  
●技法：ジクレ  
技法：giclée  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：Black  
　MatColor：Black  
材料  
　纸张：版画用中性纸  
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 黑色  
　亚光色：黑色  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）  
（不可选择版本编号）  
●Size  
　イラストSize：縦324×横430mm  
　額Size：縦471×横622×厚さ20mm  
尺寸  
　插图尺寸：长324x宽430 mm  
　框架尺寸：长471x宽622x厚20 mm  

©北条司／Coamix1985

「Cat's♥Eye」または「シティーハンター」の商品をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！

[英文版商品链接](https://edition88.com/products/cityhunter-hanga4)，  
¥2,836.00 CNY  
This art print is from the original illustration used for the front cover of ‘Weekly Shonen Jump No.1 and No.2’ published in 1983.  
这幅艺术印刷品来自1983年出版的周刊少年Jump第1期和第2期封面的原始插图。  

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  
每幅印刷品有380个版本（普通版：200个，国际版：180个），都有北条司亲笔签名。  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），以精致地表现颜色和质地。  

The art prints have the same image size as the original pieces.
艺术印刷品的图像尺寸与原作相同。  

**On the Original, from Tsukasa Hojo**
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway. At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance. I am delighted that these quality art prints can find their way to you.

**On the Art Print, from EDITION88**  
关于艺术印刷品，来自EDITION88  

The gradation from the red of the sunset to the deep blue of the night sky is beautiful, and the green buildings against the night sky are striking. The green in the outline of the main character is an important color that we carefully matched the colors with the original. To bring out the beauty of the starry sky, we sprayed a thin coat of pearlescent paint over the entire image to give it a three-dimensional effect.  
从夕阳的红色到夜空的深蓝色的渐变非常漂亮，绿色的建筑在夜空的映衬下也非常醒目。主角轮廓中的绿色是一个重要的颜色，我们仔细地将其与原作的颜色相匹配。为了衬托出星空的美丽，我们在整个画面上喷了一层薄薄的珠光漆，使其具有三维效果。 
 
●Hand-signed by Tsukasa Hojo  
北条司亲笔签名    
●Limited edition of 380(International edition 180 , Regular edition 200 )  
限量380册（国际版180册，普通版200册）  
●Mixed Media (Giclée and UV) on fine art paper for international edition  
国际版在美术纸采用混合媒体（Giclée和UV）  
●Black Frame / Unframed  
黑框/无框    
●Size  
　▸Image size 430 x 324mm / 16.93 x 12.76inch  
　▸Sheet size 460 x 368mm / 18.11 x 14.49inch  
　▸Frame size 622 x 471 x 20mm / 24.49 x 18.54 x 0.79inch  
尺寸  
　▸图片尺寸 430 x 324mm / 16.93 x 12.76inch  
　▸纸张尺寸 460 x 368mm / 18.11 x 14.49inch  
　▸框架尺寸 622 x 471 x 20mm / 24.49 x 18.54 x 0.79inch  
●Officially licensed by Coamix Inc.  
由Coamix Inc.官方授权  
●Manufactured by EDITION88  
由EDITION88制作  

©Tsukasa Hojo/Coamix 1985  

---    

---  

---  

### 「Cat's♥Eye」 版画1 (1983.07.11)
![](img/hanga_cat01_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga1)  
¥59,400

税込み 送料計算済み Check Out時

週刊少年ジャンプ 1983年 第36号 表紙の版画です。  
1983年周刊少年Jump第36期封面的版画。  

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。  
在Cat'sEye版画中，每幅版画都是用白颜料手工上色。   

この手作業により原画が持つWhite部分のマチエールを表現しています。      
这种手工上色的过程使表现了原版画中白色区域的不均匀性（matiere）。

SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
这张纸的尺寸是原画的原始尺寸，所以你可以尽可能地欣赏到接近原作的作品。  

北条司先生からのComment  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。    
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。  
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。  

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、编号、手工上色、用木头装裱   
●技法：ジクレ  
技法： giclée  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：White  
　MatColor：Cream  
材料  
　纸张：版画用中性紙  
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 白色  
　亚光色：乳白色  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）。 
（不可选择版本编号）  
●Size  
　イラストSize：縦485×横322mm  
　額Size：縦622×横471×厚さ20mm  
尺寸  
　插图尺寸：长485x宽322 mm  
　框架尺寸：长622×宽471×厚20 mm  

©北条司／Coamix1981

「Cat's♥Eye」または「シティーハンター」の商品をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！

[商品链接 Cat's Eye, Art Print #1](https://edition88.com/products/catseye-hanga1)  
¥2,836.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the cover of Weekly Shonen Jump No.36 published in 1983.
这幅艺术印刷品来自1983年出版的周刊少年Jump第36期的封面插图。  

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  
每幅印刷品有380个版本（普通版：200个，国际版：180个），都有北条司亲笔签名。   

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水基颜料）和UV打印机（UV固化墨水），精致地表现颜色和质地。   

The art prints have the same image size as the original pieces.

Comments from Tsukasa Hojo  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.
I am delighted that these quality art prints can find their way to you.

On the Art Print, from EDITION88
Each Cat’s Eye print is hand treated with white ink to replicate the matière on the original that resulted from the use of correction fluid.  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380 (International edition 180 , Regular edition 200 )  
●Mixed Media (Giclée and UV) on fine art paper for international edition  
国际版作品在美术纸上采用混合媒体（Giclée和UV）  
●Unframed  
●Size  
　▸Image size 485 x 322mm / 19.09 x 12.67inch  
　▸Sheet size 546 x 375mm / 21.49 x 14.76inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  

©Tsukasa Hojo/Coamix 1981



---    

---  

---  

### 「Cat's♥Eye」 版画2 (1984.03)
![](img/hanga_cat02_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga2)  
¥59,400

税込み 送料計算済み Check Out時

週刊少年ジャンプ 1984年 第18号 表紙の版画です。  
1984年周刊少年Jump第18期封面的版画。   

北条先生の直筆サインが入る豪華な仕様となっております。  
它有北条先生的亲笔华丽签名。   

版画は原画原寸Sizeで作られており、原画に近い状態でお楽しみいただけます。  
印刷品按原画尺寸制作，以尽可能地接近原画。   

商品の特徴としては、一枚一枚に職人による手作業が施されている点です。 
该商品的独特之处在于，每件都是由工匠手工制作的。  

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。この手作業により、原画が持つWhite部分の凹凸（マチエール）を表現することができました。    
在Cat'sEye版画中，每件作品都是用白颜料手工上色。这种手工上色的过程表现了原画白色区域的凹凸（matiere）质感。  

北条司先生からのComment  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは
印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。  
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。  
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。  

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、编号、手工上色、木质画框   
●技法：ジクレ    
技法：Giclée    
●素材
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：White  
　MatColor：Cream  
材料  
　纸张：用于版画的中性纸  
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 白色  
　亚光色：乳白色  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）  
（不可选择版本编号）  
●Size  
　イラストSize：縦485×横322mm  
　額Size：縦622×横471×厚さ20mm  
尺寸    
　插图尺寸：长485 x 宽322 mm  
　框架尺寸：长622×宽471×厚20 mm  

©北条司／Coamix1981

「Cat's♥Eye」または「シティーハンター」の商品をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！


[商品链接 Cat's Eye, Art Print #2](https://edition88.com/products/catseye-hanga2)  
¥2,836.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the cover of Weekly Shonen Jump No.18 published in 1984.  
这幅艺术印刷品来自1984年出版的周刊少年Jump第18期的封面插画。  

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  
这个艺术印刷品系列是为2022年5月在东京举行的 "Cat's♥Eye 40周年原画展--然后向着City Hunter "展览创作的。   

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版的作品是用混合媒体开发的，同时使用giclée（水基颜料）和UV打印机（UV固化墨水），精致地表达色彩和质地。   

The art prints have the same image size as the original pieces.  

Comments from Tsukasa Hojo
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.
I am delighted that these quality art prints can find their way to you.

On the Art Print, from EDITION88
Each Cat’s Eye print is hand treated with white ink to replicate the matière on the original that resulted from the use of correction fluid.  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Mixed Media (Giclée and UV) on fine art paper for international edition  
国际版作品在美术纸上的采用混合媒体（Giclée和UV）  
●Unframed  
●Size  
　▸Image size 485 x 322mm / 19.09 x 12.67inch  
　▸Sheet size 535 x 357mm / 21.06 x 14.05inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88

©Tsukasa Hojo/Coamix 1981

---    

---  

---  

### 「Cat's♥Eye」 版画3 (1985)
![](img/hanga_cat03_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga3)  
¥44,000

税込み 送料計算済み Check Out時  

ジャンプ・コミックス 1985年 第14巻 表紙の版画です。  
Cat'sEye 1985年第14巻封面的版画。   

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。  
在Cat'sEye版画中，每幅版画都是用白颜料工上色的。  
この手作業により原画が持つWhite部分のマチエールを表現しています。
SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
这种手工上色的过程使版画具有原画中白色区域的质感(matiere)。
这张纸的尺寸是原画的原始尺寸，所以你可以尽可能地欣赏到接近原作的作品。 

北条司先生からのComment
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、编号、手工上色、用木头装裱  
●技法：ジクレ  
技法： giclée  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：White  
　MatColor：Cream  
材料  
　纸张：版画用中性紙  
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 白色  
　亚光色：乳白色   
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）  
（不可选择版本编号）    
●Size  
　イラストSize：縦380×横266mm  
　額Size：縦525×横410×厚さ20mm  
尺寸   
　插图尺寸：长380x宽266mm  
　框架尺寸：长525×宽410×厚20mm  

©北条司／Coamix1981

「Cat's♥Eye」または「シティーハンター」の版画をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！

[商品链接 Cat's Eye, Art Print #3](https://edition88.com/products/catseye-hanga3)  
¥2,026.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the jacket of Jump Comics Vol.14 published in 1985.  
这幅艺术印刷品来自1985年出版的Jump Comics第14卷的书套插图。  

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版的作品是用混合媒体开发的，同时使用giclée（水基颜料）和UV打印机（UV固化墨水），以精致的表达色彩和质地。 

The art prints have the same image size as the original pieces.

Comments from Tsukasa Hojo
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.
I am delighted that these quality art prints can find their way to you.

On the Art Print, from EDITION88
Each Cat’s Eye print is hand treated with white ink to replicate the matière on the original that resulted from the use of correction fluid.  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Mixed Media (Giclée and UV) on fine art paper for international edition  
国际版作品在美术纸上采用混合媒体（Giclée和UV）  
●Unframed  
●Size  
　▸Image size 380 x 266mm / 14.96 x 10.47inch  
　▸Sheet size 426 x 300mm / 16.77 x 11.81inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  

©Tsukasa Hojo/Coamix 1981

---    

---  

---  

### 「Cat's♥Eye」 版画4 (1985 vol18)
![](img/hanga_cat04_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga4)  
¥44,000

税込み 送料計算済み Check Out時

ジャンプ・コミックス 1985年 第18巻 表紙の版画です。  
Jump Comics 1985年 第18卷封面的版画。   

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。  
この手作業により原画が持つWhite部分のマチエールを表現しています。
SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。 这张纸的尺寸是原画的原始尺寸，所以你可以尽可能地欣赏到接近原作的作品。  

北条司先生からのComment  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。  
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。  
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。  

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、有编号、手工上色、木材装裱。  
●技法：ジクレ  
技法：giclée  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：White  
　MatColor：Cream  
材料  
　纸张：版画用中性紙  
　框架：木头，丙烯酸（表面覆盖）。  
　框架颜色: 白色  
　亚光色：乳白色  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）  
(无法选择版本编号)  
●Size  
　イラストSize：縦359×横225mm  
　額Size：縦525×横410×厚さ20mm  

©北条司／Coamix1981

「Cat's♥Eye」または「シティーハンター」の版画をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！  

[商品链接 Cat's Eye, Art Print #4](https://edition88.com/products/catseye-hanga4)  
¥2,026.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the jacket of Jump Comics Vol.18 published in 1985.
这幅艺术印刷品来自1985年出版的Jump Comics第18卷的封面插图。  

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.
Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.
The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），以精致地表现色彩和纹理。  
The art prints have the same image size as the original pieces.  

Comments from Tsukasa Hojo  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.  
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.  
I am delighted that these quality art prints can find their way to you.  

On the Art Print, from EDITION88  
Each Cat’s Eye print is hand treated with white ink to replicate the matière on the original that resulted from the use of correction fluid.  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Mixed Media (Giclée and UV) on fine art paper for international edition  
国际版在美术纸上采用混合媒体（Giclée和UV）  
●Unframed  
●Size  
　▸Image size 359 x 225mm / 14.13 x 8.85inch  
　▸Sheet size 410 x 260mm / 16.14 x 10.23inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  

©Tsukasa Hojo/Coamix 1981  


---    

---  

---  

### 「Cat's♥Eye」 版画5 (1983第46号)
![](img/hanga_cat05_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-hanga5)  
¥44,000

税込み 送料計算済み Check Out時

週刊少年ジャンプ 1983年 第46号 表紙の版画です。  
周刊少年Jump1983年第46期封面的版画。  

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。 
Cat'sEye的版画使用白色颜料，每一张都有手工上色。  

この手作業により原画が持つWhite部分のマチエールを表現しています。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。   
SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。

北条司先生からのComment  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、有编号、手工上色、木材装裱。    
●技法：ジクレ
技法：giclée  
●素材
　紙：版画用中性紙
　額：木製、Acryl（表面Cover）
　額Color：White
　MatColor：Cream
材料  
　纸张：版画用中性紙  
　框架：木头，丙烯酸（表面覆盖）
　框架颜色: 白色  
　亚光色：乳白色  
●総Edition数：380　（国内版200、International版180）
（EditionNumberはお選びいただけません。）
●Size
　イラストSize：縦366×横262mm
　額Size：縦525×横410×厚さ20mm

©北条司／Coamix1981

「Cat's♥Eye」または「シティーハンター」の版画をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！

[Cat's Eye, Art Print #5, Hand Signed by Tsukasa Hojo](https://edition88.com/products/catseye-hanga5)
¥2,026.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the cover of Weekly Shonen Jump No.46 published in 1983.  
这幅艺术印刷品来自1983年出版的周刊少年Jump第46期的封面插图。  

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）精致地表现色彩和纹理。  

The art prints have the same image size as the original pieces.

Comments from Tsukasa Hojo  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.
I am delighted that these quality art prints can find their way to you.

On the Art Print, from EDITION88
Each Cat’s Eye print is hand treated with white ink to replicate the matière on the original that resulted from the use of correction fluid.  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  

●Hand-signed by Tsukasa Hojo
●Limited edition of 380(International edition 180 , Regular edition 200 )
●Mixed Media (Giclée and UV) on fine art paper for international edition
国际版在美术纸上采用混合媒体（Giclée和UV）  
●Unframed
●Size
　▸Image size 366 x 262mm / 14.40 x 10.31inch
　▸Sheet size 415 x 296mm /16.14 x 11.65inch
●Officially licensed by Coamix Inc.
●Manufactured by EDITION88

©Tsukasa Hojo/Coamix 1981


---    

---  

---  

### 「Cat's♥Eye」 版画6 (1982.07 vol2)
![](img/hanga_cat06_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga6)  
¥38,500

税込み 送料計算済み Check Out時

ジャンプ・コミックス 1982年 第2巻 表紙の版画です。  
Jump Comics 1982年第2卷封面的版画。  

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。  
Cat'sEye的版画使用白色颜料，每一张都有手工上色。  

この手作業により原画が持つWhite部分のマチエールを表現しています。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  

SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
这张纸的尺寸是原画的原始尺寸，所以你可以尽可能地欣赏到接近原作的作品。  

北条司先生からのComment  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは
印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。 
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。  
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。   

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、编号、手工彩绘、用木头装裱。  
●技法：ジクレ  
技法：giclée  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：White   
　MatColor：Cream  
材料  
　纸张：版画用中性紙  
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 白色  
　亚光色：乳白色  
●総Edition数：380　（国内版200、International版180）
（EditionNumberはお選びいただけません。）
●Size
　イラストSize：縦346×横255mm
　額Size：縦440×横364×厚さ20mm

©北条司／Coamix1981

「Cat's♥Eye」または「シティーハンター」の商品をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！

[商品链接 Cat's Eye, Art Print #6](https://edition88.com/products/catseye-hanga6)  
¥1,794.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the jacket of Jump Comics Vol.2 published in 1982.  
这幅艺术印刷品来自1982年出版的Jump Comics第2卷的封面插图。  

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）精致地表现色彩和纹理。   

The art prints have the same image size as the original pieces.

Comments from Tsukasa Hojo  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.
I am delighted that these quality art prints can find their way to you.

On the Art Print, from EDITION88
Each Cat’s Eye print is hand treated with white ink to replicate the matière on the original that resulted from the use of correction fluid.  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Mixed Media (Giclée and UV) on fine art paper for international edition   
国际版在美术纸上采用混合媒体（Giclée和UV）   
●Unframed  
●Size  
　▸Image size 346 x 255mm / 13.62 x 10.03inch  
　▸Sheet size 395 x 290mm / 15.55 x 11.41inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  

©Tsukasa Hojo/Coamix 1981  


---    

---  

---  

### 「Cat's♥Eye」 版画7 (1983年 第5巻)
![](img/hanga_cat07_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga7)  
¥33,000

税込み 送料計算済み Check Out時

ジャンプ・コミックス 1983年 第5巻 表紙の版画です。  
Jump Comics 1983年第5卷封面的版画。  

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。  
Cat'sEye的版画使用白色颜料，每一张都有手工上色。  

この手作業により原画が持つWhite部分のマチエールを表現しています。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  

SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  

北条司先生からのComment
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
规格：北条司亲笔签名、编号、手工彩绘、用木头装裱。  
●技法：ジクレ  
技法：giclée  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：White  
　MatColor：Cream  
材料  
　纸张：版画用中性紙    
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 白色  
　亚光色：乳白色  
●総Edition数：380　（国内版200、International版180）
（EditionNumberはお選びいただけません。）
●Size
　イラストSize：縦202×横204mm
　額Size：縦395×横304×厚さ20mm

©北条司／Coamix1981

「Cat's♥Eye」または「シティーハンター」の商品をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！

[商品链接 Cat's Eye, Art Print #7](https://edition88.com/products/catseye-hanga7)  
¥1,534.00 CNY

Tax included. Shipping calculated at checkout.

This art print is from the illustration for the jacket of Jump Comics Vol.5 published in 1983.  
这幅艺术印刷品来自1983年出版的Jump Comics第5卷的封面插图。 

This art print series was created for the “Cat’s♥Eye 40th Anniversary Original Art Exhibition - And to City Hunter” exhibit held in Tokyo in May 2022.  
Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），以精致地表现色彩和纹理。   

The art prints have the same image size as the original pieces.

Comments from Tsukasa Hojo  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.  
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.  
I am delighted that these quality art prints can find their way to you.  

On the Art Print, from EDITION88  
Each Cat’s Eye print is hand treated with white ink to replicate the matière on the original that resulted from the use of correction fluid.  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Mixed Media (Giclée and UV) on fine art paper for international edition  
国际版在美术纸上采用混合媒体（Giclée和UV）   
●Unframed  
●Size  
　▸Image size 202 x 204mm / 7.95 x 8.03inch  
　▸Sheet size 278 x 258mm / 10.94 x 10.15inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  

©Tsukasa Hojo/Coamix 1981  

---    

---  

---  

### 「Cat's♥Eye」 版画8 (1982.11)   
![](img/hanga_cat08_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-hanga8)  
¥44,000  
週刊少年ジャンプ 1983年 第1・2合併号／表紙Coverイラストの版画です。  
周刊少年Jump 1983年第1、2期合并号/封面插图的版画。   

Cat'sEyeの版画では、Whiteインクを使って、1枚毎に手彩色を施しています。  
Cat'sEye的版画使用白色颜料，每一张都有手工上色。  

この手作業により原画が持つWhite部分のマチエールを表現しています。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。

SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます  
这张纸的尺寸是原画的原始尺寸，所以你可以尽可能地欣赏到接近原作的作品。   

**北条司先生からのComment**  
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。
版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

**版画工房からのComment**  
**来自版画工房的评论**    
背景の濃いブルーと鮮やかなスキーウェアの色の対比がとても美しい作品でしたので、その部分の色再現に最も時間を費やしました。白い雪の部分は一部手彩色で白のインクを乗せることで、青空に映える雪を表現しました。  
背景的深蓝色和滑雪服的鲜艳色彩之间的对比是如此美丽，以至于花了大部分时间来再现该区域的色彩。白雪部分是用白颜料手绘的，以表现雪在蓝天下闪闪发光的样子。 

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り
规格：北条司亲笔签名、有编号、手工上色、木材装裱。  
●技法：ジクレ
技法：giclée  
●素材
　紙：版画用中性紙
　額：木製、Acryl（表面Cover）
　額Color：White
　MatColor：Cream
材料  
　纸张：版画用中性紙  
　框架：木头，丙烯酸（表面覆盖）  
　框架颜色: 白色  
　亚光色：乳白色  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
版本总数：380个（200个国内版本，180个国际版本）。 
（不可选择版本编号）   
●Size
　イラストSize：縦343×横296mm
　額Size：縦525×横410×厚さ20mm
尺寸  
　插图尺寸：长485x宽322 mm  
　框架尺寸：长622×宽471×厚20 mm  

©北条司／Coamix1981

「Cat's♥Eye」または「シティーハンター」の商品をご購入いただいた方には、ポストカードセット（2枚）をプレゼント！

[商品链接 Art Print #8](https://edition88.com/products/catseye-hanga8)  
¥2,026.00 CNY  

This art print is from the original illustration used for the front cover of ‘Weekly Shonen Jump No.1 and No.2’ published in 1983.  
这幅艺术印刷品来自1983年出版的周刊少年Jump第1期和第2期封面的原始插图。  

Each print has 380 editions (regular edition: 200, international edition: 180), all signed by Tsukasa Hojo himself.  
每幅印刷品有380个版本（普通版：200个，国际版：180个），都有北条司亲笔签名。  

The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）精致地表现色彩和纹理。  

The art prints have the same image size as the original pieces.  
艺术印刷品的图像尺寸与原作相同。   

**On the Original, from Tsukasa Hojo**  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway. At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance. I am delighted that these quality art prints can find their way to you.  

**On the Art Print, from EDITION88 ** 
关于艺术印刷品，来自EDITION88的评论  
The contrast between the deep blue of the background and the vivid colors of the skiwear was so beautiful that we spent most of the time replicating the colors in that area. The white snow was partially hand-colored with white ink to express the snow shining against the blue sky.  
背景的深蓝色和滑雪服的鲜艳色彩之间的对比是如此美丽，以至于我们花了大部分时间来复现该区域的色彩。白色的雪部分是用白色颜料手工着色的，以表达雪在蓝天下的闪耀。   

●Hand-signed by Tsukasa Hojo  
北条司亲笔签名  
●Limited edition of 380(International edition 180 , Regular edition 200 )   
限量380册（国际版180册，普通版200册）  
●Mixed Media (Giclée and UV) on fine art paper for international edition   
国际版在美术纸上采用混合媒体（Giclée和UV）   
●White Frame / Unframed   
●Size  
　▸Image size 343 x 296mm / 13.50 x 11.65inch   
　▸Sheet size 388 x 326mm / 15.26 x 12.83inch  
　▸Frame size 525 x 410 x 20mm / 20.67 x 16.14 x 0.79inch    
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  

©Tsukasa Hojo/Coamix 1981


---    

---  

---  

### 複製原稿1 (B4) /CityHunter  
![](img/hukuseigenkou_CH_1_B4-01-eg_thumb.jpg)  

[日文版商品链接]无

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4) 
¥290.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  
City Hunter XYZ漫画手稿以高清印刷方式复制，看起来与原作完全一样。 

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines,which which aren’t visible in the comic, can be seen clearly.  
不均匀的黑色油漆，使用白色修正笔的部分，以及蓝色的指示和起草线，这些在漫画中看不到的，都可以清楚地看到。 

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  
这本漫画手稿是一个收藏品，可以让你享受City Hunter的世界。 

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  
尺寸: B4 (364 x 257mm)  
技术: 按需印刷  
材料: Fine paper  
无框/无签名/无版号  

---    

---  

---  

### 複製原稿2 (B4) /CityHunter  
![](img/hukuseigenkou_CH_2_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript2-b4)  
¥1,100

税込み 送料計算済み Check Out時

「シティーハンター −XYZ−」の複製原稿です。  
「City Hunter −XYZ−」的复制原稿。  

高精細なプリントで再現しています。  
以高清晰度印刷品再现。   

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
北条司先生的漫画原稿经过扫描和复制，看起来与原作完全一样。  

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!    

複製原稿は原寸に近いB4Size。  
复制原稿为B4尺寸，接近原尺寸。  

シティーハンターの世界観が楽しめるコレクションアイテムです。  
这是一个收藏品，可以让你享受City Hunter的世界。   

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1985

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4) 
¥290.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  
City Hunter XYZ漫画原稿以高清印刷方式复制，看起来与原作完全一样。 

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines,which which aren’t visible in the comic, can be seen clearly.  
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  
尺寸：B4 (364 x 257mm)  
技术：按需印刷  
材料：Fine paper 
无框/无签名/无版号   

---    

---  

---  

### 複製原稿3 (B4) /CityHunter  
![](img/hukuseigenkou_CH_3_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript3-b4)  
¥1,100  

税込み 送料計算済み Check Out時  

「シティーハンター −XYZ−」の複製原稿です。  
「City Hunter −XYZ−」的复制原稿。  

高精細なプリントで再現しています。  
以高清晰度印刷品再现。   

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
北条司先生的漫画原稿经过扫描和复制，看起来与原作完全一样。  

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!   

複製原稿は原寸に近いB4Size。  
复制原稿为B4尺寸，接近原尺寸。  

シティーハンターの世界観が楽しめるコレクションアイテムです。  
这是一个收藏品，可以让你享受City Hunter的世界。   

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1985

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4) 
¥290.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  
City Hunter XYZ漫画原稿以高清印刷方式复制，看起来与原作完全一样。 

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。  

Size: B4 (364 x 257mm)
Technique: On-demand printing
Material: Fine paper
Unframed / No signed /No edition number

---    

---  

---  

### 複製原稿4 (B4) /CityHunter  
![](img/hukuseigenkou_CH_4_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript4-b4)  
¥1,100

税込み 送料計算済み Check Out時

「シティーハンター −XYZ−」の複製原稿です。  
「City Hunter −XYZ−」的复制原稿。  

高精細なプリントで再現しています。  
以高清晰度印刷品再现。   

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
北条司先生的漫画原稿经过扫描和复制，看起来与原作完全一样。  

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!   
複製原稿は原寸に近いB4Size。  
复制原稿为B4尺寸，接近原尺寸。  

シティーハンターの世界観が楽しめるコレクションアイテムです。  
这是一个收藏品，可以让你享受City Hunter的世界。 

【仕様】
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1985

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4) 
¥290.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  
City Hunter XYZ漫画原稿以高清印刷方式复制，看起来与原作完全一样。   

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines,which which aren’t visible in the comic, can be seen clearly.  
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  
尺寸：B4 (364 x 257mm)  
技术：按需印刷  
材料：Fine paper 
无框/无签名/无版号   

---    

---  

---  

### 複製原稿5 (B4) /CityHunter  
![](img/hukuseigenkou_CH_5_B4_thumb.jpg)  

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript5-b4)  
¥1,100

税込み 送料計算済み Check Out時

「シティーハンター −XYZ−」の複製原稿です。  
「City Hunter −XYZ−」的复制原稿。  

高精細なプリントで再現しています。  
以高清晰度印刷品再现。   

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
北条司先生的漫画原稿经过扫描和复制，看起来与原作完全一样。   

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!   

複製原稿は原寸に近いB4Size。  
复制原稿为B4尺寸，接近原尺寸。  

シティーハンターの世界観が楽しめるコレクションアイテムです。  
这是一个收藏品，可以让你享受City Hunter的世界。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1985

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4) 
¥290.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  
City Hunter XYZ漫画原稿以高清印刷方式复制，看起来与原作完全一样。  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines,which which aren’t visible in the comic, can be seen clearly.
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  
尺寸：B4 (364 x 257mm)  
技术：按需印刷  
材料：Fine paper 
无框/无签名/无版号   

---    

---  

---  

### 複製原稿6 (B4) /CityHunter  
![](img/hukuseigenkou_CH_6_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript6-b4)  
¥1,100

税込み 送料計算済み Check Out時

「シティーハンター −XYZ−」の複製原稿です。  
「City Hunter −XYZ−」的复制原稿。  

高精細なプリントで再現しています。  
以高清晰度印刷品再现。   

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
北条司先生的漫画原稿经过扫描和复制，看起来与原作完全一样。  

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!   

複製原稿は原寸に近いB4Size。  
复制原稿为B4尺寸，接近原尺寸。  

シティーハンターの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1985

[City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  
¥290.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  
City Hunter XYZ漫画原稿以高清印刷方式复制，看起来与原作完全一样。

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines,which which aren’t visible in the comic, can be seen clearly.
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  
尺寸：B4 (364 x 257mm)  
技术：按需印刷  
材料：Fine paper  
无框/无签名/无版号   

---    

---  

---  

### 複製原稿7 (B4) /CityHunter  
![](img/hukuseigenkou_CH_7_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript7-b4)  
¥1,100

税込み 送料計算済み Check Out時

「シティーハンター −XYZ−」の複製原稿です。  
「City Hunter −XYZ−」的复制原稿。   

高精細なプリントで再現しています。  
以高清晰度印刷品再现。   

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
北条司先生的漫画原稿经过扫描和复制，看起来与原作完全一样。  

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!    

複製原稿は原寸に近いB4Size。  
复制原稿为B4尺寸，接近原尺寸。  

シティーハンターの世界観が楽しめるコレクションアイテムです。  
这是一个收藏品，可以让你享受City Hunter的世界。   

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1985

[City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  
¥290.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  
City Hunter XYZ漫画原稿以高清印刷方式复制，看起来与原作完全一样。 

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines,which which aren’t visible in the comic, can be seen clearly.  
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  
尺寸：B4 (364 x 257mm)  
技术：按需印刷  
材料：Fine paper  
无框/无签名/无版号 

---    

---  

---  

### 複製原稿8 (B4) /CityHunter  
![](img/hukuseigenkou_CH_8_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript8-b4)  

¥1,100

税込み 送料計算済み Check Out時

「シティーハンター −XYZ−」の複製原稿です。  

高精細なプリントで再現しています。  

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  

複製原稿は原寸に近いB4Size。  

シティーハンターの世界観が楽しめるコレクションアイテムです。  

  
【仕様】
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

©北条司／Coamix 1985

[City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  
¥290.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines,which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  


---    

---  

---  

### 複製原稿9 (B4) /CityHunter  
![](img/hukuseigenkou_CH_9_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript9-b4)  
¥1,100

税込み 送料計算済み Check Out時

「シティーハンター −XYZ−」の複製原稿です。  

高精細なプリントで再現しています。  

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  

複製原稿は原寸に近いB4Size。  

シティーハンターの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙   

©北条司／Coamix 1985

[City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  
¥290.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  

---    

---  

---  

### 複製原稿10 (B4) /CityHunter  
![](img/hukuseigenkou_CH_10_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript10-b4)  
¥1,100

税込み 送料計算済み Check Out時  

「シティーハンター −XYZ−」の複製原稿です。  
高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
シティーハンターの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

©北条司／Coamix 1985

[City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  
¥290.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

The City Hunter XYZ comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of City Hunter.  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  



---    

---  

---  

### 複製原稿1 (B4) /Cat's♥Eye  (第7話 泥棒紳士登場の巻)  
![](img/hukuseigenkou1_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript1-b4)  
¥1,100

税込み 送料計算済み Check Out時  

「第7話 泥棒紳士登場の巻」の複製原稿です。  
高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

©北条司／Coamix 1981  

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)
¥348.00 CNY  

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.  
Cat's Eye漫画原稿以高清印刷方式复制，看起来与原作完全一样。  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。    

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。   

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper    
Unframed / No signed /No edition number  
尺寸：B4 (364 x 257mm)  
技术：按需印刷  
材料：Fine paper 
无框/无签名/无版号   

---    

---  

---  

### 複製原稿2 (B4) /Cat's♥Eye  (第47話 スクープを狙え！の巻)  
![](img/hukuseigenkou2_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript2-b4)  
¥1,100

税込み 送料計算済み Check Out時

「第47話 スクープを狙え！の巻」の複製原稿です。  
「第47话--获取独家新闻! 」的复制原稿。  

高精細なプリントで再現しています。  
以高清晰度印刷品再现。   

北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
北条司先生的漫画原稿经过扫描和复制，看起来与原作完全一样。  

黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!    

複製原稿は原寸に近いB4Size。  
复制原稿为B4尺寸，接近原尺寸。  

CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  
这个收藏品可以让你享受CAT'S ♥ EYE的世界。 

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1981

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.
Cat's Eye漫画原稿以高清印刷方式复制，看起来与原作完全一样。  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。    

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。   

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  
尺寸：B4 (364 x 257mm)  
技术：按需印刷  
材料：Fine paper 
无框/无签名/无版号   

---    

---  

---  

### 複製原稿3 (B4) /Cat's♥Eye  (第94話 あぶないサードラブ！の巻)  
![](img/hukuseigenkou3_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript3-b4)  
¥1,100

税込み 送料計算済み Check Out時

「第94話 あぶないサードラブ！の巻」の複製原稿です。  
「第94集：堕落的第三次爱情! 」的复制原稿。  

高精細なプリントで再現しています。
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！
複製原稿は原寸に近いB4Size。
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。

【仕様】
●Size：B4(H364×W257mm)
●技法：オフセットプリント
●素材　紙：上質紙
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1981

[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  

---    

---  

---  

### 複製原稿4 (B4) /Cat's♥Eye  第96話 浅谷のファーストキスの巻  
![](img/hukuseigenkou4_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript4-b4)  
¥1,100

税込み 送料計算済み Check Out時

「第96話 浅谷のファーストキスの巻」の複製原稿です。  
「第96話 浅谷的初吻 」的复制原稿。  

高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1981

[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.
Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.
This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  

---    

---  

---  

### 複製原稿5 (B4) /Cat's♥Eye  第107話 子供じゃないもんの巻  
![](img/hukuseigenkou5_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript5-b4)  
¥1,100

税込み 送料計算済み Check Out時

「第107話 子供じゃないもんの巻」の複製原稿です。  
「第107话：不是孩子」的复制原稿。  

高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】
尺寸：B4 (高364 x 宽257毫米)  
技术：胶印  
材料：纸张：高档纸  

©北条司／Coamix 1981

[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  

---    

---  

---  

### 複製原稿6 (B4) /Cat's♥Eye  第133話 暗闇の誘惑！の巻  
![](img/hukuseigenkou6_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript6-b4)  

¥1,100

税込み 送料計算済み Check Out時

「第133話 暗闇の誘惑！の巻」の複製原稿です。  
「第133集 - 黑暗的诱惑」的复制原稿。  

高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：A4(H297×W210mm)  
●技法：オンデマンドプリント  
●素材　紙：マーメイド紙  
【规格】  
尺寸: A4 (H297 x W210mm)  
技术: 按需印刷  
材料：Marmaid纸  

©北条司／Coamix 1981

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.  
Sales period: November 25 to 29 Japan time  

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  
尺寸：B4 (364 x 257mm)  
技术：按需印刷  
材料：Fine paper  
无框/无签名/无版号  

---    

---  

---  

### 複製原稿7 (B4) /Cat's♥Eye  第137話 予告状の秘密の巻
![](img/hukuseigenkou7_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript7-b4)  
¥1,100

税込み 送料計算済み Check Out時

「第137話 予告状の秘密の巻」の複製原稿です。  
「第137集：通告信的秘密」的复制原稿。  

高精細なプリントで再現しています。
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！
複製原稿は原寸に近いB4Size。
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1981

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  

---    

---  

---  

### 複製原稿8 (B4) /Cat's♥Eye  
![](img/hukuseigenkou8_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript8-b4)  
¥1,100

税込み 送料計算済み Check Out時

「第140話 夕陽とCat'sEyeの巻」の複製原稿です。   
「第140集，夕阳和Cat's Eye」的复制原稿。  

高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
尺寸：B4 (高364 x 宽257mm)  
技术：胶印  
材料 纸：高档纸  

©北条司／Coamix 1981

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.
Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines,
which which aren’t visible in the comic, can be seen clearly.
This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  

---    

---  

---  

### 複製原稿9 (B4) /Cat's♥Eye  第139話 風のデジャブーの巻  
![](img/hukuseigenkou9_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript9-b4)  

¥1,100

税込み 送料計算済み Check Out時

「第139話 風のデジャブーの巻」の複製原稿です。  
「第139话：风中的似曾相识」的复制原稿。

高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

©北条司／Coamix 1981


[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  


---    

---  

---  

### 複製原稿10 (B4) /Cat's♥Eye  第148話 風に舞った涙の巻  
![](img/hukuseigenkou10_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript10-b4)  
¥1,100

税込み 送料計算済み Check Out時

「第148話 風に舞った涙の巻」の複製原稿です。  
「第148集：泪水在风中飞舞」的复制原稿。  

高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

©北条司／Coamix 1981

[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  

---    

---  

---  


### 複製原稿11 (B4) /Cat's♥Eye  
![](img/hukuseigenkou1_B4-11-eg_thumb.jpg)  
[日文商品链接](https://edition-88.com/products/)  
无

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.  

Size: B4 (364 x 257mm)  
Technique: On-demand printing  
Material: Fine paper  
Unframed / No signed /No edition number  

---    

---  

---  

### 複製原稿12 (B4) /Cat's♥Eye  第158話 恋ふたたびの巻  
![](img/hukuseigenkou12_B4_thumb.jpg)  
[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript12-b4)  
¥1,100

税込み 送料計算済み Check Out時  

「第158話 恋ふたたびの巻」の複製原稿です。  
「第158话 再一次恋爱」的复制原稿。   

高精細なプリントで再現しています。  
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。  
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！  
複製原稿は原寸に近いB4Size。  
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  

©北条司／Coamix 1981  

[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)
¥348.00 CNY

Tax included. Shipping calculated at checkout.
Sales period: November 25 to 29 Japan time

The Cat's Eye comic manuscripts are replicated in high-definition printing to look exactly like the original.  

Uneven black paint, parts used with white correction pens, and blue instructions and drafting lines, which which aren’t visible in the comic, can be seen clearly.  

This comic manuscript is a collectible item that allows you to enjoy the world of Cat's Eye.  

Size: B4 (364 x 257mm)
Technique: On-demand printing
Material: Fine paper
Unframed / No signed /No edition number


---    

---  

---  

### 「Cat's♥Eye40周年記念原画展～そしてシティーハンターへ～」図録

![](img/zuroku01_thumb.jpg)

[「Cat's♥Eye40周年記念原画展～そしてシティーハンターへ～」図録](https://edition-88.com/products/catseye40th-catalog1)  

¥2,200

税込み 送料計算済み Check Out時

「Cat's♥Eye40周年記念原画展 ～そしてシティーハンターへ～」に展示された作品をまとめた図録です。  
「Cat's ♥ Eye 40周年纪念原画展--然后向着City Hunter」中展出的作品汇编于这本画册。  

『Cat's♥Eye』『シティーハンター』などを約90点収録。  
包含『Cat's ♥ Eye 』『City Hunter』等约90幅作品。  

アナログの原稿から1点1点スキャン。  
每件作品都是从原稿中单独扫描出来的。  

北条司先生の作品を色鮮やかに再現しています。  
北条司先生的作品被生动而多彩地再现。  

原画展の開催を記念して、北条司先生と、『Cat's♥Eye』の担当編集者をしていた堀江信彦氏(株式会社Coamix 代表取締役)のスペシャルインタビューを掲載。  
为了庆祝原画展，还包括对北条司先生和曾是『at's ♥ Eye 』责编的堀江信彦（Coamix Co., Ltd.总裁）的特别采访。  
 
※本書の内容はCat's♥Eye40周年記念原画展 ～そしてシティーハンターへ～にて展示された作品の一部となります。全ての展示品を掲載してはおりません。  
本出版物的内容是Cat's ♥ Eye 40周年纪念原画展--然后向着City Hunter中展出的部分作品。 并非所有的展品都包括在内。 

【仕様】  
●著者：北条司  
●発行：株式会社VISION8  
●Size：A4(約H297×W210×D5mm)  
●ページ数：フルColor84ページ(表紙含む）  
【规格】  
作者: 北条司    
发行：VISION8公司  
尺寸：A4（约H297 x W210 x D5mm）  
页数：84页全彩页（包括封面）  

©北条司／Coamix 1981
©北条司／Coamix 1985

注意：納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  
如果你购买了几件交货时间不同的商品，一旦你订单中的所有商品都准备好了，它们就会被一起运走。 如果你希望将物品分开运送，请将篮子分开。

---    

---  

---  

### アートトレーディングカード （ランダム全19種）/Cat's♥Eye

https://edition-88.com/products/catseye-arttradingcard1

キャッツ・アイ４０周年を記念して販売された商品！  
为庆祝Cat's Eye诞生40周年而出售!  

絵柄は全18種+シークレット。ブラインド仕様なので、どれが出るかお楽しみ♪  
共有18个图案+秘密图案。 盲盒设计，让你迫不及待地想知道你会得到哪一个!  

Sizeは名刺Size(55×91mm)。  
尺寸：名片大小（55 x 91mm）。  

北条先生がColorイラストを描かれる際に使用している用紙と同じ紙を使用しています。  
所用的纸是北条老师画彩色插图时用的纸。  

表面はColor、裏面はモノクロのイラストを使用しており、連載当時の作品の色味に近づくよう、鮮やかさにこだわり製作しました。  
正面是彩色的，背面是黑白的，插图的制作注重生动性，使其接近于连载时的作品色彩。  

【仕様】
● Size：H91×W55mm  
●素材　紙：ケント紙  
【规格】  
尺寸：H91xW55 mm  
材料：Kent纸  

---    

---  

---  


## 名古屋展新增的展品数据（2023年03月）  

### 「Cat's♥Eye」版画9
（Cat's♥Eye40周年記念原画展の描き下ろしイラストより）/北条司（直筆Signed/国内版限定200枚）  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_new_hanga_cats_02_01_100x.jpg)  
https://edition-88.com/products/catseye-hanga9

¥61,600

税込み 送料計算済み Check Out時

Cat's♥Eye40周年記念原画展の描き下ろしイラストの88Graphです。
Sheet Sizeは、原画の原寸Sizeで再現しました。より原画に近い状態でお楽しみいただけます。
Cat'sEyeの88Graphでは、1枚毎に手彩色を施しています。
イラスト全体に細かいラメをちりばめ、Key ItemのCat's Cardには派手に大粒のラメを乗せ、華やかな印象に仕上げました。
額装については、velvet生地のMatに、右下には40周年記念ロゴを使用した、金属PlateのCat's Cardをつけた、記念原画展豪華特別仕様となりました。  
（Sheet Size以原始Size再现。在更接近原画的状态下可以欣赏。
在Cat'sEye的88Graph中，每1张都施以手工彩绘。
在整个插图中镶嵌着细小的线，Key Item的Cat's Card上放着华丽的大粒线，给人留下华丽的印象。
关于裱框、Velvet面料的Mat、右下角使用40周年纪念标志、带有金属Plate的Cat's Card，成为纪念原画展豪华特别规格。）

北条司先生からのComment
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは、印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆Sign・Edition・手彩色入り、木製額入り、velvetMat仕様、金属Plate（Cat's Card）付  
●技法：[88Graph](./about_edition88(2023).md#88Graph)    
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：White  
　MatColor：Red  
●総Edition数：380　（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
●Size  
　イラストSize：338×328mm  
　額Size：635×490×厚さ18mm  

（●规格：北条司亲笔Sign Edition、手工上色、木制框架、velvetMat规格、带金属Plate（Cat's Card）  
●技法：[88Graph](./about_edition88(2023).md#88Graph)  
●材料  
　紙：版画用中性紙  
　框：木製、亚克力（封装）  
　框Color：White  
　MatColor：Red  
●Edition总数：380　（国内版200、International版180）  
（无法选择EditionNumber）  
●Size  
　插画Size：338×328mm  
　框Size：635×490×厚18mm  
）  

●作品証明書付  

©北条司／Coamix1981  

発送時期：ご注文から約2ヶ月以内に発送致します。  
（发送时间：从订单开始约2个月内发送。）  

納期の異なる商品を複数購入された場合、ご注文いただいた全ての商品が揃い次第まとめて発送いたします。
個別配送をご希望の場合は、お手数ですがカゴを分けて購入をお願い申し上げます。  
（如果购买了多件交货日期不同的商品的话，我们会在您订购的所有商品集齐后一并发送。  
如果希望个别配送的话，麻烦您分开购物篮购买。）  

---

国际版  
![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_cats_02_12_100x.jpg)  
https://edition88.com/products/catseye-hanga9-1
Cat's Eye, Art Print #9 / Tsukasa Hojo
¥59,400 JPY

Tax included. Shipping calculated at checkout.

Free international shipping

This art print (88Graph) is from the original illustration newly drawn by Tsukasa Hojo for ‘Cat‘s Eye 40th Anniversary Original Art Exhibit －And to City Hunter’.
We hand-sprayed with pearlescent paint on the entire image and applied large glitter to the card of Cat's Eye that Hitomi is holding as a key item, to create a gorgeous impression.
As for the framing, a velvet fabric mat was adopted, and the metal plate, Cat's Eye card with the 40th-anniversary logo, is attached at the lower right. This is a luxurious and special product to commemorate the original exhibition.
The 88Graph is the same image size as the original piece.  
（这幅艺术版画(88Graph)是由北条司为“猫眼40周年纪念原画展--然后向着城市猎人”最新绘制的原始插图。
我们在整个图像上手工喷洒了珠光涂料，并在瞳手中作为关键物品的猫眼卡片上涂上了大片闪光剂，以创造出华丽的印象。
至于边框，采用了天鹅绒布垫、金属板--带有40周年标志的猫眼卡片--附在右下角。这是为纪念原展而推出的奢华特殊产品。
88Graph的图像大小与原始作品相同。）  

Comments from Tsukasa Hojo  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway.
At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance.
I am delighted that these quality art prints can find their way to you.

●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Medium: [88Graph](./about_edition88(2023).md#88Graph) (Giclée and UV on fine art paper for international edition)  
●Unframed (With mat and metal plate)  
- 北条司亲笔签名  
- 限量380张(国际版180张，普通版200张)  
- 介质：88Graph（国际版为Giclée、UV印刷、美术纸）。  
- 无框 (带衬垫和金属板)  

This item is so large that it cannot be shipped overseas with a frame. Please note that the mat and metal plate are included, but the frame is not.  For framing, please ask your local picture frame shop to frame it for you.  
这件物品非常大，不能与画框一起运往海外。请注意，垫子和金属板包括在内，但框架不包括在内。 如需装裱，请当地的画框店为你装裱。  

●Size  
　▸Image size 338 x 328mm / 13.31 x 12.91inch  
　▸Sheet size 606 x 455mm / 23.86 x 17.91inch  
　▸Mat size:  Inner  440 x 350mm / 17.32 x 13.78inch  
　　　         Outer 606 x 455mm / 23.86 x 17.91inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  
●Certificate of Authenticity  

©Tsukasa Hojo/Coamix 1981  

SHIPPING DATE:Within 2 months after receiving your order  

Important Notice    
●Since this product is made to order, please allow a certain amount of time for shipping.  
●Our products are shipped from Japan and may be subject to customs duties, import taxes, customs fees, etc. in the destination country. EDITION88 DOES NOT collect those fees directly and therefore those are not included in your total amount due. Please read our policies before placing your order.  
重要提示    
由于本产品是按订单生产的，请允许一定的运输时间。   
我们的产品从日本发货，可能需要支付目的地国家的关税、进口税、海关费用等。EDITION88不直接收取这些费用，因此这些费用不包括在您的总金额中。请在下订单前阅读我们的政策。  

---    

---  

---  



### 「Cat's♥Eye」版画10
（北条司ILLUSTRATIONS1991年ポスター（裏）より）/北条司（直筆サイン入り/国内版限定200枚）  
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_new_hanga_cats_01_02_200x.jpg)  
https://edition-88.com/products/catseye-hanga10

¥44,000

税込み 送料計算済み Check Out時

北条司ILLUSTRATIONS1991年ポスター（裏）イラストの88Graphです。
退色してしまった原画を、北条先生ご監修の下、描かれた当時の色鮮やかさを再現しました。
1991年に描かれた当時の雰囲気を、88Graphではお楽しみいただけます。
キャッツ・アイの88Graphでは、1枚毎に手彩色を施しています。
こちらのの88Graphには、瞳のハイライト部分と髪のツヤ部分に手彩色を施し、原画が持つWhite部分のマチエールを表現しています。  
退色了的原画，在北条老师的监修下，再现了被画当时的鲜艳的颜色。
在88Graph中可以享受到1991年描绘的当时的氛围。
在猫眼的88Graph中，每一张都进行了手工上色。
在这里的88Graph中，对瞳的Highlight部分和头发的光泽部分进行了手工上色，表现了原画所拥有的White部分的Matière。(译注：matière：质感)  

北条司先生からのComment
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは、印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
●技法：[88Graph](./about_edition88(2023).md#88Graph)  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：White  
　MatColor：Cream  
●総Edition数：380  
（国内版200、International版180）  
（EditionNumberはお選びいただけません。）  
●Size  
　イラストSize：293×261mm  
　額Size：525×410×厚さ20mm  
●作品証明書付  
规格：由北条司亲笔签名版、手工上色，木质画框。  
技术：[88Graph](./about_edition88(2023).md#88Graph)  
材料  
　纸：版画用中性紙   
　框：木质，亚克力（封装）  
　框颜色：白色  
　MatColor：奶油色  
总版数：380（国内200版，国际180版）（不能选择版号）。  
尺寸  
　插图尺寸: 293 x 261 mm  
　画框尺寸：525 x 410 x 20 mm（厚度）  
包括作品证书  

©北条司／Coamix1981  

---
国际版  
https://edition88.com/products/catseye-hanga10

This art print (88Graph) is from the original illustration for the poster attached in ‘Tsukasa Hojo’s illustrations’ published in 1991.

Under the supervision of Mr. Hojo, the original artwork, which had faded, was replicated in its original vivid colors through 88Graph.

In the 88Graph, the highlights of the three sisters' eyes and hair are hand-colored with white ink to express the matiere on the original that resulted from correction fluid.

On the Original, from Tsukasa Hojo
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway. At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance. I am delighted that these quality art prints can find their way to you.

On the Art Print, from EDITION88
The contrast between the deep blue of the background and the vivid colors of the skiwear was so beautiful that we spent most of the time replicating the colors in that area. The white snow was partially hand-colored with white ink to express the snow shining against the blue sky.
 
●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Medium: [88Graph](./about_edition88(2023).md#88Graph) (Giclée and UV on fine art paper for international edition)  
●White Frame / Unframed  
●Size  
　▸Image size 365 x 261mm / 14.37 x 10.28inch  
　▸Sheet size 400 x 276mm / 15.75 x 10.87inch  
　▸Frame size 525 x 410 x 20mm / 20.67 x 16.14 x 0.79inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  
●Certificate of Authenticity  

3张新局部图。例如：  
![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hojo_new_hanga_cats_01_08_100x.jpg)  

---    

---  

---  

### 「シティーハンター」 版画5
（週刊少年ジャンプ 1988年 第30号 表紙より）/北条司（直筆サイン入り/国内版限定200枚）  
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_new_hanga_city_01_02_100x.jpg)  
https://edition-88.com/products/cityhunter-hanga5  

¥44,000

税込み 送料計算済み Check Out時

週刊少年ジャンプ 1988年 第30号 表紙イラストの88Graphです。
鮮やかな洋服のピンク色と肌色をしっかり再現しました。画面に奥行きを出す目的で、全体に薄くパールを吹き付けています。
SheetSizeは原画の原寸Sizeとなっており、より原画に近い状態でお楽しみいただけます。  
1988年周刊少年Jump第30期 封面插图的88Graph。
生动的衣服的粉红色和皮肤颜色被很好地再现了出来。 在整个画面上喷了一层薄薄的珍珠，以增加深度。
SheetSize是原版插图的原始尺寸，因此您可以尽可能地欣赏到接近原作的效果。

北条司先生からのComment
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは、印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
●技法：88Graph  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：Black  
　MatColor：Black  
●総Edition数：380　（国内版200、International版180）
（EditionNumberはお選びいただけません。）  
●Size  
　イラストSize：350×267mm  
　額Size：525×410×厚さ20mm  
●作品証明書付  

©北条司／Coamix1985  

----
国际版  
https://edition88.com/products/cityhunter-hanga5

City Hunter, Art Print #5/ Tsukasa Hojo
¥38,500 JPY

Tax included. Shipping calculated at checkout.

This art print (88Graph) is from the illustration for the front cover of ‘Weekly Shonen Jump No.30’ published in 1988.

We paid attention to replicating the vivid pink color of the cloth and the skin tone. A thin layer of pearlescent paint is hand-sprayed over the entire surface to make it have a luminosity that shifts with the viewer’s perspective, displaying depth.

The 88Graph is the same image size as the original piece.

On the Original, from Tsukasa Hojo  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway. At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance. I am delighted that these quality art prints can find their way to you.
 
●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Medium: 88 Graph (Giclée and UV on fine art paper for international edition)  
●Black Frame / Unframed   
●Size   
　▸Image size 350 x 267mm / 13.78 x 10.51inch  
　▸Sheet size 400 x 288mm / 15.74 x 11.34inch  
　▸Frame size 525 x 410 x 20mm / 20.67 x 16.14 x 0.79inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  
●Certificate of Authenticity  

©Tsukasa Hojo/Coamix 1985  

Please choose 'Frame' or 'Unframed'.  

SHIPPING DATE:Within 2 months after receiving your order  


---    

---  

---  

### 「シティーハンター」 版画6
（週刊少年ジャンプ 1991年 第15号 表紙より）/北条司（直筆サイン入り/国内版限定200枚）  
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_new_hanga_city_02_02_200x.jpg)  
https://edition-88.com/products/cityhunter-hanga6  

週刊少年ジャンプ 1991年 第15号 表紙イラストの88Graphです。
鮮やかな洋服のピンク色と肌色をしっかり再現しました。画面に奥行きを出す目的で、全体に薄くパールを吹き付けています。
引き込まれるような迫力がある作品なので、SheetSizeは原画のSizeより大きめで制作しました。  
1991年周刊少年Jump第15期 封面插图的88Graph。
生动的衣服的粉红色和皮肤颜色被很好地再现了出来。 整个画面被轻轻地喷上了珍珠，以创造深度。
**这张纸的尺寸比原作的尺寸要大**，因为作品有一种强大的力量，吸引着你。  

北条司先生からのComment
連載当時、Color原稿には紫を含む色や、紫寄りの青やピンクなどは、印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。
今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。
あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。

●仕様：北条司直筆サイン・Edition・手彩色入り、木製額入り  
●技法：88Graph  
●素材  
　紙：版画用中性紙  
　額：木製、Acryl（表面Cover）  
　額Color：Black  
　MatColor：Black  
●総Edition数：380　（国内版200、International版180）
（EditionNumberはお選びいただけません。）  
●Size  
　イラストSize：253×305mm  
　額Size：410×525×厚さ20mm  
●作品証明書付  

©北条司／Coamix1985  

---  
国际版  
https://edition88.com/products/cityhunter-hanga6

City Hunter, Art Print #6/ Tsukasa Hojo
¥38,500 JPY

Tax included. Shipping calculated at checkout.

This 88Graph is from the illustration for the front cover of Weekly Shonen Jump No.30 published in 1988.  
这张88Graph来自1988年出版的《周刊少年Jump》第30期的封面插图。  

We paid attention to replicating the vivid pink color of the cloth and the skin tone. A thin layer of pearlescent paint is hand-sprayed over the entire surface to make it have a luminosity that shifts with the viewer’s perspective, displaying depth.  
我们细心地复制了衣服的鲜艳粉红色和肤色。在整个表面上手工喷洒了一层薄薄的珠光颜料，使其具有亮度，随着观众的视角变化而变化，显示出深度。  

Since the original work is so powerful that it draws you in, we decided to make the 88Graph larger than the original size.  
由于原作是如此强大，吸引着你，我们决定将88Graph做得比原来的尺寸更大。  

On the Original, from Tsukasa Hojo  
When I was working on these manga series, for my color pages, I was told to avoid colors with a purple tinge or blue and pinks that were close to purple because those shades were hard to print in the magazines. But I would use them anyway. At the time, available printing technologies could not convey some of my unique colors. Now, 40 years later, advancements in printing mean colors and shades from the original can be replicated. These art prints carry the colors that I used precisely, and even the color of the paper is almost identical to the original; it is hard to tell the art print from the original at a glance. I am delighted that these quality art prints can find their way to you.
 
●Hand-signed by Tsukasa Hojo  
●Limited edition of 380(International edition 180 , Regular edition 200 )  
●Medium: 88 Graph (Giclée and UV on fine art paper for international edition)  
●Black Frame / Unframed  
●Size  
　▸Image size 253 x 305mm / 9.96 x 12.00inch  
　▸Sheet size 290 x 340mm / 11.42 x 13.39inch  
　▸Frame size 410 x 525 x 20mm / 16.14 x 20.67 x 0.79inch  
●Officially licensed by Coamix Inc.  
●Manufactured by EDITION88  
●Certificate of Authenticity  

©Tsukasa Hojo/Coamix 1985  

Please choose 'Frame' or 'Unframed'.  

SHIPPING DATE:Within 2 months after receiving your order  


---    

---  

---  

### 実物大「喫茶Cat'sEye」 看板
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_kanban_1_100x.jpg)  
https://edition-88.com/products/catseye-signboard

¥119,900

税込み 送料計算済み Check Out時
受注商品 / 数量限定（订单商品/限量）  

ファン必見アイテム！
瞳・泪・愛の三姉妹が働く喫茶店「喫茶Cat'sEye」の看板が実物大Sizeで登場！！  
粉丝必看道具！
瞳、泪、爱三姐妹工作的咖啡屋「Cat‘sEye咖啡」的招牌以实物大小Size登场！！  

こちらの商品は、北条司先生の要望で商品化しました。
表にはCat'sEyeのロゴと喫茶店の営業時間、裏には準備中の文字が入った、漫画に登場する看板のデザインをそのまま再現しています。  
我们的商品是根据北条司先生的要求商品化的。
正面是Cat‘sEye的logo和咖啡屋的营业时间，背面是准备中的文字，直接再现了漫画中出现的招牌的设计。  

「喫茶Cat'sEye」の世界観に浸れること間違いなし！
撮影用や照明インテリアとしてもお使いいただけます。
一定会沉浸在“Cat‘sEye咖啡屋”的世界观中！
也可以作为摄影用和照明室内使用。  

＊スタンド脚は組み立て式です。  
＊数量限定のため、無くなり次第終了になります。  
支架脚是组合式的。  
由于数量有限，售完即止。  

【仕様】  
●Size：H870×W450×D80mm  
●重さ：6kg  
●素材：フレーム/アルミ押出形材  
●照明：直管型LED プラグ付キャブタイヤケーブル2m  
●消費電力：12W  
规格  
Size: H870×W450×D80mm  
重量:6kg  
素材:框架/铝挤压形材料  
照明:带有直管型LED插头的轮胎和电缆2m  
耗电量:12w  

---    

---  

---  

### 高級アートプリント11 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_11_1_4961c8ca-a276-4219-b681-dc79968d5146_200x.jpg)  
https://edition-88.com/products/catseye-artprint11-a4

¥1,540

税込み 送料計算済み Check Out時

「北条司 ILLUSTRATIONS 1991年」のイラストを使用した高級アートプリントです。
高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。  
是使用了「北条司ILLUSTRATIONS 1991年」的插图的高级艺术印刷。
高精细的全彩色印刷，色彩鲜明地再现着北条司老师的作品。
Size是容易收藏的A4 Size。
能让人感受到自己最喜欢的作品就在身边。  

【仕様】  
●Size：A4(H297×W210mm)  
●技法：オンデマンドプリント  
●素材　紙：マーメイド紙  
规格  
Size: A4(H297×W210mm)  
技法:按需打印  
素材　纸:美人鱼纸  

©北条司／Coamix 1981


---    

---  

---  

### 高級アートプリント1(A4)/シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/city_art_print_1_1_200x.jpg)  
https://edition-88.com/products/cityhunter-artprint1-a4

¥1,540

税込み 送料計算済み Check Out時

「北条司 ILLUSTRATIONS 1991年」のイラストを使用した高級アートプリントです。
高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。
是使用了“北条司ILLUSTRATIONS 1991年”的插图的高级艺术印刷。
高精细的全彩色印刷，色彩鲜明地再现着北条司老师的作品。
Size是容易收藏的A4 Size。
能让人感受到自己最喜欢的作品就在身边。  

【仕様】  
●Size：A4(H297×W210mm)  
●技法：オンデマンドプリント  
●素材　紙：マーメイド紙  
规格  
Size: A4(H297×W210mm)  
技法:按需打印  
素材　纸:美人鱼纸  

©北条司／Coamix 1981


---    

---  

---  

### 高級アートプリント2(A4)/シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/city_art_print_2_1_200x.jpg)  
https://edition-88.com/products/cityhunter-artprint2-a4

¥1,540

税込み 送料計算済み Check Out時

「北条司 ILLUSTRATIONS 1991年」のイラストを使用した高級アートプリントです。
高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。  
是使用了“北条司ILLUSTRATIONS 1991年”的插图的高级艺术印刷。
高精细的全彩色印刷，色彩鲜明地再现着北条司老师的作品。
Size是容易收藏的A4 Size。
能让人感受到自己最喜欢的作品就在身边。  

【仕様】  
●Size：A4(H297×W210mm)  
●技法：オンデマンドプリント  
●素材　紙：マーメイド紙  
规格  
Size: A4(H297×W210mm)  
技法:按需打印  
素材　纸:美人鱼纸  

©北条司／Coamix 1985


---    

---  

---  

### トートバッグ（译注：Tote Bag）(bouquet) / シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_tote_bouquet_001_200x.jpg)  
https://edition-88.com/products/cityhunter-totebag2  
¥2,200

税込み 送料計算済み Check Out時

販売期間：2023年4月5日まで

持ち歩きに便利なアイテムが新登場！
「週刊少年ジャンプ 1991年 第15号 表紙イラスト」を使用した、ファスナー付きトートバッグです。
しっかりとした厚みがある、丈夫な生地です。
袋部分のSizeは約H380×W370mm。
収納口は約350mm。
天ファスナー付きなので、貴重品もバッチリ保護できます。
マチが約113mmあり、A4クリアファイルもすっぽり入るSize感。
内ポケットも外ポケットもあるので、鍵やパスケースを入れておくのにも便利です。
ハンドル部分はH570×W30mm。肩掛けできる、ちょうど良い長さです。  
携带方便的物品新登场!
是使用了“周刊少年jump 1991年第15号封面插图”，带拉链的手提袋。
是一种厚实、结实的面料。
袋子部分的Size约为H380×W370mm。
收纳口约350mm。
因为是带天拉链的，贵重品也能很好地保护。
边缘约113mm, A4 ClearFile也完全能放入的Size感。
因为有内袋和外袋，所以放钥匙和卡包也很方便。
手柄部分为H570×W30mm。正好可以披肩的长度。

【仕様】  
●Size：本体 約H380×W370mm、マチ 約113mm / 収納口 約350mm / 持ち手 H570×W30mm  
●素材：コットン  
规格  
Size：主体 约H380×W370mm，边缘约113mm / 收纳口约350 mm / 手柄H570×W30 mm  
材质：棉  

©北条司／Coamix 1985



---    

---  

---  

### トートバッグ（译注：Tote Bag）(bang) / シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_tote_bang_001_200x.jpg)  
https://edition-88.com/products/cityhunter-totebag1

¥2,200

税込み 送料計算済み Check Out時

販売期間：2023年4月5日まで

持ち歩きに便利なアイテムが新登場！
「週刊少年ジャンプ 1991年 第15号 表紙イラスト」を使用した、ファスナー付きトートバッグです。  
随身携带方便的道具新登场！
这是使用“周刊少年JUMP 1991年第15号封面插图”的带拉链的手提包。  

しっかりとした厚みがある、丈夫な生地です。
袋部分のSizeは約H380×W370mm。
収納口は約350mm。
天ファスナー付きなので、貴重品もバッチリ保護できます。
マチが約113mmあり、A4クリアファイルもすっぽり入るSize感。
内ポケットも外ポケットもあるので、鍵やパスケースを入れておくのにも便利です。
ハンドル部分はH570×W30mm。肩掛けできる、ちょうど良い長さです。

【仕様】  
●Size：本体 約H380×W370mm、マチ 約113mm / 収納口 約350mm / 持ち手 H570×W30mm  
●素材：コットン  

©北条司／Coamix 1985



---    

---  

---  

### メガネ拭き（译注：眼镜布） (aim)/シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_megane_aim_001_200x.jpg)  
https://edition-88.com/products/cityhunter-glassescloth6

¥990

税込み 送料計算済み Check Out時

シティーハンターのイラストを使用したメガネ拭きが新登場！
使用城市猎人插图的擦眼镜布新登场！  

プリントの工程は1つ1つ、手作業で行っています。
昇華転写プリントのため、発色がとても綺麗です。
約18×25.5cmと、少し大きめSize感です。
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。  
每一道印刷工序都是手工完成的。
因为升华转印印刷，所以显色非常漂亮
约18×25.5 cm，有点大的Size感。
因为材料使用的是微纤维，所以可以用细小的纤维收集污垢，好好地去除。
除了眼镜，它还适用于擦拭电脑监视器和智能手机屏幕等。  

【仕様】  
●Size：約H180×W255mm  
※素材の製法上、形に多少個体差ございます。予めご了承ください。  
●素材：ポリエステル100%(マイクロファイバー生地)  
规格  
Size:约H180×W255mm  
※素材的制法上，形状多少有个体差异。请事先谅解。  
素材:聚酯100%(微纤维面料)  

©北条司／Coamix 1985


---    

---  

---  

### メガネ拭き (coat)/シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_megane_coat_001_200x.jpg)  
https://edition-88.com/products/cityhunter-glassescloth5

¥990

税込み 送料計算済み Check Out時

販売期間：2023年4月5日まで

シティーハンターのイラストを使用したメガネ拭きが新登場！

プリントの工程は1つ1つ、手作業で行っています。
昇華転写プリントのため、発色がとても綺麗です。
約18×25.5cmと、少し大きめSize感です。
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。

【仕様】
●Size：約H180×W255mm
※素材の製法上、形に多少個体差ございます。予めご了承ください。
●素材：ポリエステル100%(マイクロファイバー生地)

©北条司／Coamix 1985

---    

---  

---  

### メガネ拭き (up)/シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_megane_up_001_200x.jpg)  
https://edition-88.com/products/cityhunter-glassescloth4
¥990

税込み 送料計算済み Check Out時

シティーハンターのイラストを使用したメガネ拭きが新登場！

プリントの工程は1つ1つ、手作業で行っています。
昇華転写プリントのため、発色がとても綺麗です。
約18×25.5cmと、少し大きめSize感です。
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。

【仕様】
●Size：約H180×W255mm
※素材の製法上、形に多少個体差ございます。予めご了承ください。
●素材：ポリエステル100%(マイクロファイバー生地)

©北条司／Coamix 1985



---    

---  

---  

### メガネ拭き（Night view）/シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_megane_Nightview_001_200x.jpg)  
https://edition-88.com/products/cityhunter-glassescloth3
¥990

税込み 送料計算済み Check Out時

販売期間：2023年4月5日（水）まで

シティーハンターのイラストを使用したメガネ拭きが登場！

プリントの工程は1つ1つ、手作業で行っています。
昇華転写プリントのため、発色がとても綺麗です。
約18×25.5cmと、少し大きめSize感です。
素材はマイクロファイバーを使用していますので、細かい繊維で汚れを集め、しっかり落とすことができます。
メガネ以外にも、パソコンのモニターやスマホの画面などを拭くのに適しています。

【仕様】
●Size：約H180×W255mm
※素材の製法上、形に多少個体差ございます。予めご了承ください。
●素材：ポリエステル100%(マイクロファイバー生地)

©北条司／Coamix 1985

---    

---  

---  

### アートタイル （Night view）/シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/coa1_1_200x.jpg)  
https://edition-88.com/products/cityhunter-arttile8

¥900

税込み 送料計算済み Check Out時

販売期間：2023年4月5日（水）まで

シティーハンターのイラストを使用したアートタイルが登場！

直径約90mm、厚さ5mmと片手に乗るSizeです。
国産の粘土を使用して焼成した、美濃の素焼きの陶器です。
表面に肉眼では見えない大変細かい穴が空いているため吸水性が高く、耐熱性もあるため、コースターとしても機能性抜群です！
来客時のおもてなしにもオススメ♪
アートとしてお部屋に飾るのにも場所を選びません。

【仕様】
●Size：直径約90mm 厚さ5mm
●重さ：約80g
●素材：陶器
●生産国　日本
※珪藻土(アスベスト)は使用しておりません
※水滴以外は吸水面にシミが生じる場合があります。
※食器洗い機は非対応です。
※素材の性質上、細かい傷がある、吸水の穴が目立つことがございます。

©北条司／Coamix 1985


---    

---  

---  

### 高級アートプリント2 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_2_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint2-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第2巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981


---    

---  

---  

### 高級アートプリント3 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_3_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint3-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第6巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981

---    

---  

---  

### 高級アートプリント4 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_4_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint4-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第5巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981

---    

---  

---  

### 高級アートプリント5 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_5_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint5-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第7巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981


---    

---  

---  

### 高級アートプリント6 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_6_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint6-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第9巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981

---    

---  

---  

### 高級アートプリント7 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_7_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint7-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第4巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981

---    

---  

---  

### 高級アートプリント8 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_8_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint8-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第3巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981

---    

---  

---  

### 高級アートプリント9 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_9_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint9-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第10巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981
---    

---  

---  

### 高級アートプリント10 (A4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/cat_art_print_10_1_200x.jpg)  
https://edition-88.com/products/catseye-artprint10-a4
¥1,540

税込み 送料計算済み Check Out時

「愛蔵版コミックス 1944年 第8巻/表紙Cover」のイラストを使用した高級アートプリントです。

高精細なフルColorプリントで、北条司先生の作品を色鮮やかに再現しています。
SizeはコレクションしやすいA4Size。
大好きな作品を身近に感じられるアイテムです。

【仕様】
●Size：A4(H297×W210mm)
●技法：オンデマンドプリント
●素材　紙：マーメイド紙

©北条司／Coamix 1981


---    

---  

---  

### クリアファイル（译注：ClearFile/透明文件夹）(XYZ/Navy) /シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_CF_CH_Navy001_200x.jpg)  
https://edition-88.com/products/cityhunter-clearfile2

¥500

税込み 送料計算済み Check Out時

ファン必見のアイテムが新登場！  
粉丝必看的新品登场!  

手描きで描かれた原画を、高精細のスキャナーでデジタル化。
シックな色合いで丁寧なデザイン。
透けないタイプで白打ちがされている仕様のクリアファイルです。
普段使いにも、コレクターズアイテムとしてもおすすめの一品です。
保管に便利なA4Sizeで、書類やプリントが楽しく整理できます。  
用高精密的扫描仪将手绘的原画数字化。
雅致的色调，细致的设计。
这是一种不透明的透明文件夹。
无论是日常使用，还是作为收藏单品，都是非常推荐的一款。
保管方便的A4 Size，可以愉快地整理文件和打印。


【仕様】  
●Size：H310×W220mm  
●オフセット印刷  
●素材：PP(ポリプロピレン)  
【规格】  
●Size: H310×W220mm  
●胶印  
●材料:PP(聚丙烯)  

©北条司／Coamix 1985

---    

---  

---  

### クリアファイル（译注：ClearFile/透明文件夹）(XYZ/Green) /シティーハンター
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_CF_CH_Green001_200x.jpg)  
https://edition-88.com/products/cityhunter-clearfile1

¥500

税込み 送料計算済み Check Out時

ファン必見のアイテムが新登場！

手描きで描かれた原画を、高精細のスキャナーでデジタル化。
シックな色合いで丁寧なデザイン。
普段使いにも、コレクターズアイテムとしてもおすすめの一品です。
保管に便利なA4Sizeで、書類やプリントが楽しく整理できます。

【仕様】
●Size：H310×W220mm
●オフセット印刷
●素材：PP(ポリプロピレン)

©北条司／Coamix 1985


---    

---  

---  

### Acryl Stand Keyholder（ランダム全8種）/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hojo_acryl-stand-keyholder_002_f189c119-f470-42c4-aae8-ee3455075d3e_200x.jpg)  
https://edition-88.com/products/catseye-acrylickeychain1
¥880

税込み 送料計算済み Check Out時

Cat's♥Eye４０周年を記念して販売された商品！  
纪念Cat's♥Eye40周年而销售的商品!  

絵柄は全8種、ブラインド仕様なのでどれが出るかお楽しみ♪
Sizeは本体50×60mm 台座26×41mm 。
Acrylの本体、台座にボールチェーンがついた仕様になっています。
本体を台座に挿せば、まるで予告状のように飾ることができます。
お部屋に小さな展示スペースを作ってみてはいかがでしょう？
ボールチェーンもついてるので、鞄や鍵に付けるのもオススメです！  
图案全8种，因为是盲盒规格，所以很期待能出哪个♪
机身尺寸为50×60mm，底座为26×41mm。
Acryl的本体，成为在台座上附有球链的规格。
把实体插在底座上的话，就像预告信一样可以装饰。
在你的房间里设计一个小小的展示空间怎么样?
因为有球链，所以推荐挂在包和钥匙上!

【仕様】  
● Size：約H50×W60mm / 台座H26×W41mm  
●素材　Acryl、金属  

©北条司／Coamix 1981

---    

---  

---  

### 複製原稿11 (B4)/Cat's♥Eye
![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/hukuseigenkou11_B4-01_200x.jpg)  
https://edition-88.com/products/catseye-duplicate%EF%BD%8Danuscript11-b4
¥1,100

税込み 送料計算済み Check Out時

「第158話 恋ふたたびの巻」の複製原稿です。
高精細なプリントで再現しています。
北条司先生の漫画原稿をスキャンし、本物そっくりに再現。
黒ベタや、Whiteを使用した部分、青色で書かれた指示や下書きの線等も見ることができます！
複製原稿は原寸に近いB4 Size。
CAT’S♥EYEの世界観が楽しめるコレクションアイテムです。  
这是「第158话 再一次恋爱之卷」的复制稿。
以高清晰度打印再现。
扫描北条司老师的漫画原稿，再现得和真的一模一样。
也可以看到黑色粘稠、使用White的部分、用蓝色写的指示和草稿的线条等！
复制文稿接近原尺寸的B4 Size。
CAT‘S♥是可以欣赏EYE世界观的收藏品。  

【仕様】  
●Size：B4(H364×W257mm)  
●技法：オフセットプリント  
●素材　紙：上質紙  
【规格】  
●Size: B4(H364×W257mm)  
●技法:胶印  
●材料　纸:优质纸  

©北条司／Coamix 1981

---    

---  

---  

### Cat's Eye, Art Print #8 / Tsukasa Hojo

https://edition88.com/products/catseye-hanga8

5张新局部图：  
![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat08_08_100x.jpg) 
![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat08_06_100x.jpg) 
![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat08_07_100x.jpg) 
![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat08_09_100x.jpg) 
![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/products/hanga_cat08_sign_en_100x.jpg) 


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
