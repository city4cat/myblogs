
# About EDITION88, 88Graph, PAPER FOR ART PRINTS, and etc ...

## [Japanese version](https://edition-88.com/pages/about-us)

### WHO IS EDITION88?

私たちは主に日本の漫画家、アニメーター、そしてイラストレーターの方々が生み出すアートを用いて、展示会の企画開催から版画、グッズの制作までトータルにプロデュースしています。  
我们主要利用日本漫画家、原画师、插画家创作的艺术作品，从展会的策划到版画、周边的制作，进行全方位的策划。  

私たちが作り出す版画は、EDITION88の独自技術である88グラフを活用しアーティストたちとの共同作業で制作されています。  
我们制作的版画是利用EDITION88的独特技术88Graph，与艺术家们共同完成的。  

いくつもの制作工程を重ねて仕上がった88グラフは、最後に厳しい検品を経て合格したものだけに、アーティストの直筆サインもしくは落款が入ります。　
经过多道制作工序完成的88Graph，最后经过严格的检验合格后，会有艺术家的亲笔签名或落款。  

<a name="88Graph"></a>  
### 88グラフ（译注：88Graph）

88グラフ（エイティーエイトグラフ）とは、EDITION88が独自に開発した混合版画技法を活用したアートの表現方法です。  
88Graph（Eighty-eight Graph）是利用EDITION88自行开发的混合版画技术来表现艺术的方式。  

88グラフは版画用紙のみならず、金属、木材、アクリル、陶器、ガラスなど、幅広いものに対応可能で、複数の表現技法を組み合せることで、独自性の高い表現を追求しています。  
88Graph不仅适用于版画用纸，还适用于金属、木材、丙烯酸、陶器、玻璃等多种表现手法，通过组合多种表现手法，追求独特性的表现。  

主な技法としては、ジクレ、シルクスクリーン、UVプリント、箔押し、エンボス等が活用されるほか、EDITION88の職人によるハンドペインティングやハンドスプレイも含まれます。  
主要技术包括[Giclée](./extra_info.md#Giclée)、[SilkScreen](./extra_info.md#SilkScreen)、[UV print](./extra_info.md#UVPrint)、[烫金](./extra_info.md#HotStamp)、[Emboss](./extra_info.md#Emboss)等，还包括EDITION 88工匠的Hand Painting和[Hand Spray](./extra_info.md#SprayPrinting)。  


### 全て直筆サインもしくは落款入り (所有签名或题词的作品)

世の中には複製、高級印刷物と呼ばれているものが多く存在しますが、88グラフには必ず、直筆サインもしくは落款が入ります。  
虽然市面上有很多复制的、被称为高级印刷品的东西，但88Graph一定会有亲笔签名或落款。  

なぜなら版画は、昔から存在する「絵画」の表現方法の一つであり、アーティストの承認の元、アーティストと版画工房（私たち）が協力して制作するものだからです。  
这是因为版画是 "绘画 "的一种表现形式，已经存在了很长时间，是艺术家和印刷厂（我们）之间的合作，并得到艺术家的认可。   

アーティストと共に、出来上がった版画を入念にチェックします。  
与艺术家一起，对完成的印刷品进行仔细检查。  

十分に納得できる仕上がりであることを確認した上で、版画一枚一枚に直筆サインもしくは落款が入ります。  
在确认工艺令人满意后，每幅印刷品都会有个人签名或题词。  


### エディションナンバー（版本编号）
88グラフには必ず限定数が決められています。  
88Graph中一定限定数量。  

8/100という記載があれば、分母の100が限定枚数になります。分子の数字は一枚一枚の版画に振られる数で、つまり、8/100は世の中に一枚しか存在しません。  
如果印刷品上标有8/100，分母100就是有限的印刷品数量。 分子中的数字是分配给每个印刷品的数字，也就是说，世界上只有一个8/100。  

この表記はエディションナンバーと言われており、版画には必ず記載されます。エディションナンバーこそが本物の価値、希少性を証明するものになります。    
这个符号被称为版本编号，并且总是在印刷品上标注。版本号是真实性和稀有性的证明。  


### アーティストとの共同作業（与艺术家合作）

アーティストが生み出すアートやデザインを、EDITION88が独自に開発した88グラフの技術を生かして、オリジナリティー溢れる表現方法で再現していくことが私たちスタジオの仕事です。  
我们工作室的工作就是利用EDITION88独自开发的88Graph技术，以充满独创性的表现方式再现艺术家创作出的艺术和设计。  

そのために私たちは、アーティストと話し合いを重ねながら、アーティストが表現したいものを正確に理解した上で、それを88グラフの表現に落とし込んでいきます。  
为此，我们一边与艺术家反复讨论，一边正确理解艺术家想要表现的东西，然后将其落实到88Graph的表现上。  

例えば、紙の質感、インクの微妙なラインの表現など、幾度にもわたる修正を重ねながら、表現したいものに近づけていく制作プロセスは、まさにアーティストと共同作業です。  
例如，纸张的质感、墨水微妙的线条表现等，一边反复修改，一边接近想要表现的东西的制作过程，正是和艺术家共同作业。  

### 展覧会開催（举办展览会）

私たちはアーティストの展覧会を、期間限定で開催しています。  
我们在限定时间内举办艺术家的展览会。  

アーティストの原画展示のほか、88グラフやグッズも並べておりますので、ぜひ現物をその目でお確かめ下さい。  
除了展示艺术家的原画外，还陈列了88Graph和周边商品，请一定要亲眼确认实物。  

展覧会開催の告知は、本ECのNEWSやSNSにて案内してまいります。  
展览会召开的通知，会在本EC的NEWS和SNS上介绍。  



## [English version](https://edition88.com/pages/about-us)

### WHO IS EDITION88？

We curate and produce everything from exhibits to products, working mainly with Japanese creators such as manga artists, animators, and illustrators.    
我们策划和制作从展览到产品的一切，主要与日本创作者合作，如漫画家、动画师和插画家。  

In particular, our products, by applying printmaking techniques, are produced in collaboration with artists using EDITION88 Studio's proprietary technique, "88Graph".  
特别是，我们的产品--通过应用版画技术，与艺术家合作--使用EDITION88工作室的专有技术"88Graph"来制作。   

To create 88Graph products, a complex process with numerous steps is required. Only those that pass our rigorous inspections are made available and are hand-signed by the artist or have the artist’s seal.  
为了创造88Graph产品，需要一个复杂的过程，有许多步骤。只有那些通过我们严格检查的产品才会被提供，并由艺术家亲笔签名或盖上艺术家的印章。  

<a name="88Graph_en"></a>  
### 88Graph

88Graph is a method of artistic expression utilizing a mixed printmaking technique originally developed by EDITION8 studio.  
88Graph是一种利用混合版画技术的艺术表现方法，最初由EDITION8工作室开发。   

88Graph can be applied not only to papers but also to a wide range of materials, including metal, wood, acrylic, ceramics, glass, etc.  
88Graph不仅可以应用于纸张，还可以应用于各种材料，包括金属、木材、丙烯酸、陶瓷、玻璃等。   

The main techniques utilized include giclée, silkscreen, UV printing, foil stamping, embossing, etc., as well as hand painting and hand spraying by EDITION88's artisans, and we pursue highly original expression by combining these multiple techniques.  
利用的主要技术包括[giclée](./extra_info.md#Giclée)、[silkscreen](./extra_info.md#SilkScreen)、[UV printing](./extra_info.md#UVPrint)、[foil stamping](./extra_info.md#HotStamp)、[embossing](./extra_info.md#Emboss)等，以及EDITION88的工匠们的hand painting和[hand spraying](./extra_info.md#SprayPrinting)，我们通过结合这些多种技术，追求高度原创的表达。  

### COLLABORATION WITH ARTISTS（与艺术家协作）

Our job at EDITION88 studio is to replicate the art and design created by artists in a way that is full of originality and expression, utilizing the 88Graph technique originally developed by us.  
我们EDITION88工作室的工作是利用我们最初开发的88Graph技术，以充满原创性和表现力的方式，复制艺术家创造的艺术和设计。  

We discuss with artists extensively to fully understand what they wanted to explore and express through their creations and reflect their intent into the expression of 88Graph.  We also talk about details, such as the texture of papers, delicate lines drawn by subtle ink, and so on. The process of discussions and numerous modifications to bring 88Graph products closer to the artist’s vision is truly a collaborative effort.  
我们与艺术家广泛讨论，充分了解他们想通过创作探索和表达什么，并将他们的意图反映到88Graph的表达中。 我们还讨论了一些细节问题，如纸张的质地、微妙的墨水所绘制的精致线条等等。为了使88Graph的产品更接近艺术家的设想，我们进行了讨论和多次修改，这个过程确实是一种合作。  

### PAPER FOR ART PRINTS（艺术印刷品用纸）

To create prints that satisfy the original artist, the paper that the art print is printed on needs to meet many conditions, such as the matière of the surface, wear resistance to printing pressure, fibers that hold up even when submerged in water, and ink compatibility. Below are some of the papers we often use.  
为了创作出令原作者满意的印刷品，艺术印刷品所使用的纸张需要满足许多条件，如表面的matière、对印刷压力的耐磨性、即使浸泡在水中也能保持的纤维、以及墨水的兼容性。以下是我们经常使用的一些纸张。  

・Bamboo-based Japanese Paper (washi)  
竹制日本纸(和纸)  (译注： 详见[Washi](./extra_info.md#Washi)。)  

Papermaking or washi making is a long-established, traditional technology in Japan. As such, we work with a highly qualified hand-made washi studio in Tokushima prefecture to prepare a unique bamboo-based paper just for us. Bamboo-based washi is soft and finely textured, similar to silk, and allows for intricate printed designs.  
造纸或制作washi是日本一项历史悠久的传统技术。因此，我们与Tokushima县一家高素质的手工和纸工作室合作，为我们准备了一种独特的竹制纸。竹制washi质地柔软，纹理细腻，与丝绸相似，可以进行复杂的印刷设计。   

・Kakita, an Acid-free Paper  
Kakita，一种无酸纸  

The Kakita brand is named after the Kakita River that runs near its factory. Only neutral agents are used in the development of acid-free print paper; the paper is highly light-resistant, allowing the work to be exhibited or stored for an extended time without being affected.  
Kakita品牌是以其工厂附近的Kakita河命名的。在开发无酸印刷纸时，只使用了中性制剂；这种纸具有很强的耐光性，使作品可以长期展出或储存而不受影响。   


### SIGNED BY HAND OR STAMPED WITH THE ARTIST’S SEAL（亲笔签名或盖上艺术家的印章）

All of our 88Graph products are either signed by the artist or have their seal.  
我们所有的88Graph产品都有艺术家的签名或盖有他们的印章。  

88Graph is created as a joint effort by the artist and EDITION88 studio. The signature or seal captures our cooperative relationship.  
88Graph是由艺术家和EDITION88工作室共同创作的。签名或印章体现了我们的合作关系。   

We carefully check the finished 88Graph together with the artist; once the artist is fully satisfied with the finished piece, they sign by hand or apply their seal on each approved piece.  
我们与艺术家一起仔细检查完成的88Graph；一旦艺术家对完成的作品完全满意，他们就会在每件批准的作品上亲笔签名或盖上印章。  

Of note, in Japan, seal impressions are deemed to have the same authority as signatures. Japanese drawings or calligraphy pieces often carry the artist’s seal as proof that the artist created the work. Even today, official documents such as contracts are often “sealed” instead of signed.  
值得注意的是，在日本，印章印记被认为具有与签名相同的权威。日本的图画或书法作品往往带有艺术家的印章，以证明艺术家创作了该作品。即使在今天，像合同这样的官方文件也经常是 "盖章 "而不是签名。   

### edition number(版本编号）

An edition number is proof of authenticity and verification of its rarity. All authentic 88Graph products carry an edition number. 
版本号是真实性的证明，也是对其稀有性的验证。所有真实的88Graph产品都有一个版本号。  

If the number on a product says 8/100, it means only 100 pieces were made. The number before the slash is the number assigned sequentially to each product, meaning that there is only one 8/100
product in the world.    
如果产品上的数字是8/100，这意味着只生产了100件。斜线前面的数字是按顺序分配给每个产品的数字，这意味着全世界只有一个8/100产品。  



--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
