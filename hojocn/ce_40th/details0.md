部分展品的细节目录： 
    [0](./details0.md), 
    [1](./details1.md), 
    [2](./details2.md), 
    [3](./details3.md), 
    [总结](./details3.md#SummaryOfDetails), 

## 「Cat's Eye 40周年纪念原画展」部分展品的细节0（[hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96963)）  

「Cat's Eye 40周年纪念原画展」以下简称“画展”。  


-----  

### 活动概述  

[1](https://hojo-tsukasa.com/cat-c/1131.html)  
イベント名称：キャッツ♥アイ40周年記念原画展　〜そしてシティーハンターへ〜  
開催期間：2022年5月13日[金]　～　5月23日[月]  
営業時間：11：00～20：00　※展示の最終入場は19：00まで  
開催場所：3331 Arts Chiyoda / アーツ千代田 3331　1階 メインギャラリー  
会場ホームページ：https://www.3331.jp/access/  
译文:  
Event名称：Cat's♥Eye 40周年纪念原画展～然后向着CityHunter～  
时间：2022年5月13日[星期五] - 5月23日[星期一]  
开放时间：11:00 - 20:00 ※展览的最后入场时间为19:00。  
地点：3331 Arts Chiyoda / 3331 Arts Chiyoda 1F Main Gallery / Arts千代田3331 1楼主画廊  
场地网站：https://www.3331.jp/access/  

[2](https://hojo-tsukasa.com/cat-c/1237.html)  
イベント名称：キャッツ♥アイ40周年記念原画展　〜そしてシティーハンターへ〜  
開催期間：2022年11月19日(土)～11月30日(水)  
営業時間：10：00～21：00　※展示の最終入場は20：30まで  
開催場所 ：キャナルシティ博多　サウスビルB１　バンダイナムコ Cross Store博多 内  
施設ホームページ： https://canalcity.co.jp/  
[译文](https://weibo.com/ttarticle/p/show?id=2309404841217266680050):  
Event名称：Cat's♥Eye 40周年纪念原画展～然后向着CityHunter～  
举办时间：2022年11月19日（周六）～11月30日（周三）  
营业时间：日本时间10:00～21:00 ※最终入场时间为20:30  
举办地点：CANAL CITY博多 南座B1 Bandai Namco Cross Store博多 内  
机构网站：https://canalcity.co.jp/  

[3](https://hojo-tsukasa.com/cat-c/1369.html):  
イベント名称：キャッツ♥アイ40周年記念原画展 〜そしてシティーハンターへ〜  
開催期間：2023年3月21日[火・祝] 〜4月5日[水]  
営業時間：10：00〜18：00 ※展示の最終入場は 17：30 まで  
開催場所：松坂屋名古屋店 本館7階 大催事場  
会場ホームページ：https://www.matsuzakaya.co.jp/nagoya/  
译文:  
Event名称：Cat's♥Eye 40周年纪念原画展～然后向着CityHunter～  
举办时间：2023年3月21日（周二）～4月5日（周三）  
营业时间：10:00～21:00 ※最终入场时间为17:30  
举办场所：松坂屋名古屋店　本馆7楼　大展示会场  
会场主页：https://www.matsuzakaya.co.jp/nagoya/  


译注：  
"キャナルシティ博多"的[官方](https://canalcity.co.jp/zh-cn)翻译为"CANAL CITY HAKATA"。其[楼层指南](https://canalcity.co.jp/files/floormap_zh-cn.pdf)。我猜，它是一个综合购物商场(类似北京的大悦城(西单店))。   
"バンダイナムコ"的[官方](https://bandainamco-am.co.jp)翻译为"BANDAI NAMCO"。其logo与淘宝上的"万代官方旗舰店"相同。    
"Cross Store"的[官方链接](https://bandainamco-am.co.jp/crossstore/store/)显示目前有2家店，博多店是其中之一，位于上述博多的楼层指南中的B1 001。我猜：1)Cross Store是BANDAI NAMCO公司的实体店;2)"Cross Store博多"是BANDAI NAMCO公司在博多开的实体店（类似Apple公司在大悦城(西单店)里开设的Apple实体商店）。    
综上所述，上述举办地点的意思是："CANAL CITY博多"综合购物商场的南座B1层 "Bandai Namco"公司的"Cross Store博多"实体店内。  

---  

### 贺词  

下图中从左至右分别是：1）... 2)堀江信彦的贺词；3）株式会社アニプレツクス(Aniplex) 岩上敦宏; 4)集英社コンテンツ(Contents)事业部；5)集英社少年ジヤンプ(Jump)编辑部。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_4441_600x600.jpg) (译注：[大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_4441_9600x9600.jpg) )  


-----  

### 北条司的寄语（完整版）  
虽然该画展的小册子的前言里有此内容（日文和英文），但似乎不完整。以下照片里是完整版。  
![](img/forward_hojo.jpg) 
（大图：[原图](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821ulme5j20ku0v9dis.jpg)，或访问[网页[^1]](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)）  

**OCR提取的日文**:  
ごあいさっ  
この度は「キャッツアイ」40周年記念原画展にご来場いただきありがとうございます。  
自分に才能があるかどうかも分からずにやつてきましたが、ふと振り返ると40年以上も経つて  
いて...なぜここまでやってこれたのだろうというのが正直な思いです(笑)。  
昔の作品を忘れずにいてくれるというのは、凄いことだなと思います。  
僕らの時代では、漫画は「消耗品」という感覚だつたんです。時代を映すもの、「今」を映す  
のが漫画であり、描いたものはすぐ忘れ去られてしまうものでしたので、常に前を向いて  
作品を作らなくてはという感覚でいました。  
だから、まさか40年も経って、こうやって目の前に「キャッツアイ」があるとは思いもして  
いませんでした(笑)。  
何の魅力があるのかは、作者には本当にわからないのですが、それでもまだまだ觉えて  
くださつている読者がいるということは、有難い事だなあと...つくづく感じています。  
なにぶん昔の作品なので、粗は色々あります。原画を観て「あーこりやひどいな」など  
思われるかも知れませんが、確かににひどいのですが(笑)、そこはこ了承ください(笑)。  
本当に40年も覚えていてくださって、ありがとうございます。  
どうぞごゆっくりご観覧くだきい。  
2022年  北条司  


**译文**：  
寄语  
感谢您参观「Cat's Eye」40周年記念原画展。  
回顾我的职业生涯，我甚至不知道自己是否有天赋，但当我回头看时，已经过去40多年了...坦白地说，我不知道自己是如何做到的（笑）。  
昔日的作品没有被忘记，这真是太神奇了。  
在我那个时代，漫画被认为是「消耗品」。漫画是时代的反映、是「现在」的反映，我所画的东西很快就会被遗忘。所以我觉得必须始终着眼于未来，创造我的作品。  
所以我从来没有想过，40年后，我会像这样把「Cat's Eye」放在面前（笑）。  
作品有何种魅力，身为作者的我真的不知道，但我很感激仍有读者关注这部作品...真的很感激。  
由于这是一部旧作，有许多粗糙之处。 当看到原画时，您可能会想「哦，这太糟糕了」，它确实很糟糕（笑），但请您理解（笑）。  
非常感谢您40年后还记得这些。   
请您慢慢浏览。  
2022年  北条司  


[twitter](https://twitter.com/tohnomiyuki00/status/1595801328197857281)中的细节：  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVsu9rUAAA7Pqz_crop0.jpg"/> 
](https://pbs.twimg.com/media/FiVsu9rUAAA7Pqz?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVsu9uUcAAWZbU_crop0.jpg"/> 
](https://pbs.twimg.com/media/FiVsu9uUcAAWZbU?format=jpg&name=large)  
以及评论：    
ご挨拶の部分アップと福岡展のサイン  
ちなみに獠のスケベ顔が作者そのものというのは、本人も言っている通り本当  
福冈展览中的问候语和签名的部分特写。  
顺便说一下，獠的色色的面孔确实是漫画家本人，正如他自己所说。(译注：此处翻译待校对)


------

### 堀江信彦的寄语（完整版）    
虽然该画展的小册子的前言里有此内容（日文和英文），但似乎不完整。以下照片里是完整版。  
![](img/forward_nobu.jpg) 
（大图：[原图](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821unvnvj20ku0v9q6d.jpg)，或访问[网页[^1]](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)）  

**OCR提取的日文**：  
ごあいさっ  
40周年記念原画展の開催おめでとうございます。  
「キャッツアイ」は編集者の私にとっても初めての連載作品でしたから、私を漫画の世界に  
居続けられるようにしてくれた作品で、そういう意味でも非常に感慨深いです。  
この作品のおかげで今の私があると思っています。  
北条君は飄々としていて、あまりやる気や情熱みたいなものを表に出さないタイプの人で  
すが、相当努力も試行錯誤もしていました。  
今「キャッツアイ」の時は下手だったよれって言えるのは成長したからこそ言えるのです。  
また、未熟だからこそ良かった部分だってあります。  
例えば、かっこいいセリフは未熟じゃないと言えません。大人になってくると、かっこいい  
セリフは言えなくなってきます。  
だから未熟さっていうのは、実は宝物です。  
いつも若い編集者たちに言っているのですが、作家が描いてきたものに、未熟なセリフや  
シーンがあってし、馬亜にしたらだめだよ、若いから描けてるんだよって。  
それが光を放っている場合もあるから、そこを大人面で、くさしたりしちやダメだよって。  
そうしないと作家を潰すよ、と。  
だから40周年の原画展では、北条君の40年間の変化というのを楽しんで欲しいと思います。  
描いている最中に絵が上手くなる作家は伸びるのですが、北条君は描いている最中から  
ずっと成長していましたを。  
読者はそういう変化をしていく漫画家が好きなんですよね。  
北条君の40年の変化に思いをはせて、一人の無名だった新人が大家になつていく過程を  
観ていただけたら、より楽しめると思います。  
まさに私がそうでした。  
北条司40年の「進化」をどうか見届けてください。  

北条司 初代担当编集  
元 週刊少年ジャンプ5代目編集長  
株式会社コアミックス  
代表取締役 堀江信彦  


**译文**：  
寄语    
祝贺40周年記念原画展的举办。   
「Cat's Eye」是我作为编辑的首部连载作品，它的成功令我留在了漫画世界里，从这个意义上说，该周年展对我来说非常重要。  
我想我之所以成为今天的我，就是因为这部作品。   
北条君是一个清心寡欲的人，没有表现出太多的动力和热情，但他做了相当多的努力和尝试。（译注：待校对）    
我现在可以说，我负责的「Cat's Eye」有不足之处，因为我已经成长了。（译注：待校对）  
它的优秀在一定程度上也是因为它的经验不足。  
例如，你只有不成熟时，才会说酷炫的台词。随着年龄的增长，你不能再说酷炫的台词了。  
所以不成熟其实也是一种财富。    
我总是告诉年轻的编辑，如果漫画家画的东西里有不成熟的台词或场景，不要嘲笑他们。他们还年轻，能画。（译注：待校对）  
在某些情况下，它反而可以是一个闪亮点，所以你不要以成人的眼光鄙夷它。   
如果你这样做，你会毁掉漫画家。  
因此，我希望在40周年的原画展上，人们能够享受到北条君40年来的变化。   
作品画得越来越好的那些漫画家会成功，北条君也不例外。（译注：待校对）      
读者喜欢这种精益求精的漫画家。    
如果你想想北条君在过去40年中所经历的变化，看看一个不知名的新人是如何成为该领域的大师，我想你会更喜欢这个展览的。  
这正是这个展览带给我的感受。（译注：待校对）  
请见证北条司40年的「进化」吧。  

北条司的初代责任编辑  
前週刊少年Jump 5代目编辑長  
株式会社Coamix CEO  
堀江信彦  

---  


### 小瞳的半边像（千代田场展）    
![](img/IMG_3099_4800x4800_thumb.jpg) 
(源自[链接](https://edition-88.com/blogs/blog/catseye40th-report02))  
细节：  

- 眼部：   
![](img/IMG_3099_4800x4800_crop0.jpg)  
- 嘴部：  
![](img/IMG_3099_4800x4800_crop1.jpg)  
- 发丝线条流畅。有粗线条。不知道粗线条表示什么[疑问]。        
![](img/IMG_3099_4800x4800_crop2.jpg)  
- 常见的"Cat's❤Eye"logo中是符号"❤"，这里的签名画成了猫眼卡上的符号：  
![](img/IMG_3099_4800x4800_crop4.jpg) 
![](img/IMG_3099_4800x4800_crop5.jpg)  
![](img/IMG_3099_4800x4800_crop3.jpg)  


---  

### 小瞳的半边像（博多场展）   
![](img/FiEH0bQaUAAt_gJ_thumb.jpg)  

A: [官方Report](https://edition-88.com/blogs/blog/catseye40th-report02)  
B: [现场实拍 - Twitter](https://twitter.com/33Aquamarine3/status/1593935187343446016)  
C: [现场实拍 - Twitter](https://twitter.com/chisa_ryoyuki/status/1593905991363399680)  
D: [现场实拍 - Twitter](https://twitter.com/QQzMG9Ned67rF2G/status/1593792706550976514)  
E: [现场实拍 - Twitter](https://twitter.com/SaebaRyo0326/status/1593786320195039232)  
F: [现场实拍 - Twitter](https://twitter.com/DionJim/status/1594573801064255488)  
G: [现场实拍 - Twitter](https://twitter.com/SASAME828/status/1594564365856157696)  
H: [现场实拍 - Twitter](https://twitter.com/moryricka_ch/status/1597145555980849152)  

[<img title="A缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_4800x4800.jpg) 
[<img title="B备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh7LmfNaAAcfNZp.jpg"/> 
](https://pbs.twimg.com/media/Fh7LmfNaAAcfNZp?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh6w2QFUcAAeM5J.jpg"/> 
](https://pbs.twimg.com/media/Fh6w2QFUcAAeM5J?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh5KAfTUcAAXmTs.jpg"/> 
](https://pbs.twimg.com/media/Fh5KAfTUcAAXmTs?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh5ENHvaMAAghfX.jpg"/> 
](https://pbs.twimg.com/media/Fh5ENHvaMAAghfX?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/FiEQaXYUcAEfhvu.jpg"/> 
](https://pbs.twimg.com/media/FiEQaXYUcAEfhvu?format=jpg&name=large) 
[<img title="G备份缩略图(点击查看原图)" height="100" src="img/thumb/FiEH0bQaUAAt_gJ.jpg"/> 
](https://pbs.twimg.com/media/FiEH0bQaUAAt_gJ?format=jpg&name=large) 
[<img title="H备份缩略图(点击查看原图)" height="100" src="img/thumb/Fioza1aWYAAx_c1.jpg"/> 
](https://pbs.twimg.com/media/Fioza1aWYAAx_c1?format=jpg&name=large)  

细节：  

- 眼部。  
![](img/FiEH0bQaUAAt_gJ_crop1.jpg)  
- 嘴部。  
![](img/FiEH0bQaUAAt_gJ_crop2.jpg)  
- 发丝。  
![](img/FiEH0bQaUAAt_gJ_crop3.jpg)  
- 右下角有作者的印章。  
- 对比：  
![](img/IMG_3099_4800x4800_thumb.jpg) 
![](img/FiEH0bQaUAAt_gJ_thumb.jpg)  


---  

## 三姐妹 - 泪  

为简便，采用如下符号简记：  
A: [现场实拍 - Weibo](https://weibo.com/3970723970/LsINkbbVn)  
B: [现场实拍 - Twitter](https://twitter.com/mihohi1/status/1595417702843428864)  

A(下图左一)、B(下图左二):  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/ecac7082gy1h262fmgrp6j20ku0v9n99.jpg"/> 
](https://wx2.sinaimg.cn/large/ecac7082gy1h262fmgrp6j20ku0v9n99.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiQP8egUAAEq_o6.jpg"/> 
](https://pbs.twimg.com/media/FiQP8egUAAEq_o6?format=jpg&name=large) 

- B中的评论：  
北条先生のカラー原画は素晴らしくてため息が出る。殆ど修正が無いんだけど、僅かにホワイトがある。印刷したら当然消えるから、肉眼で見見ることができてテンションあがる。  
（北条老师的彩色原画非常精彩，令人感叹。 几乎没有任何修正，但有一个轻微的白色。 自然，这在印刷后就消失了，所以能用肉眼看到它是很令人兴奋的。）  

- 从B中的评论推测，该展品为原画。  

更多细节见[「キャッツ♥アイ」版画9](./details3.md#SistersInOneImage)。  


---  

## 三姐妹 - 瞳  
[现场实拍 - Weibo](https://weibo.com/3970723970/LsINkbbVn)  

[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/ecac7082gy1h262fnkmopj20ku0v9k3q.jpg"/> 
](https://wx2.sinaimg.cn/large/ecac7082gy1h262fnkmopj20ku0v9k3q.jpg) 

更多细节见[「キャッツ♥アイ」版画9](./details3.md#SistersInOneImage)。  


---  

## 三姐妹 - 爱  
[现场实拍 - Weibo](https://weibo.com/3970723970/LsINkbbVn)  

[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/ecac7082gy1h262foj6chj20ku0v9k3i.jpg"/> 
](https://wx4.sinaimg.cn/large/ecac7082gy1h262foj6chj20ku0v9k3i.jpg)  

更多细节见[「キャッツ♥アイ」版画9](./details3.md#SistersInOneImage)。  


-----  

### 北条司简介  
下图左侧是"北条司プロフィール"（北条司简介）  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04156_1024x1024.jpg) (译注：[大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04156_4800x4800.jpg) )  
"北条司プロフィール"里有排版错误（CityHunter连载开始于1985年，而不是1993年）；后续作品及事件的日期也随之排版错误（博多场展修复了该错误）。更正后如下图：  
[<img title="(点击查看原图)" height="100" src="img/DSC04156_4800x4800_crop0_corrected.jpg"/>
](img/DSC04156_4800x4800_crop0_corrected.jpg)  
小瞳画像下部配文："40周年 描きおろし..."（40周年 纪念画作...）。遗憾的是其中大部分文字看不清楚。  


-----  

### 作品创作资料、画材等  

- 下图墙上的配文："幼少期と漫画との出会い..."（童年和与漫画的邂逅....）。遗憾的是其中大部分文字看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0258_600x600.jpg) (译注：[大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0258_4800x4800.jpg) )  


- "北条先生近照的自画像等"。遗憾的是其中大部分文字看不清楚。      
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04035_600x600.jpg)  (译注：[大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04035_4800x4800.jpg) )   
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04037_600x600.jpg?v=1653016450)    
Twitter上的[链接1](https://twitter.com/yae_ch3/status/1526555056224583683), 
[链接2](https://twitter.com/Nicokun77/status/1525456274044751872), 
[链接3](https://twitter.com/MasaDirector/status/1528650996817092608), 
[链接4](https://twitter.com/tohnomiyuki00/status/1595780126788136960) 
有上图的大图:  
[<img title="1备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9psvLWUAAjDGl.jpg"/> 
](https://pbs.twimg.com/media/FS9psvLWUAAjDGl?format=jpg&name=large) 
[<img title="2备份缩略图(点击查看原图)" height="100" src="img/thumb/FTbb9izUEAA4WaH.jpg"/> 
](https://pbs.twimg.com/media/FTbb9izUEAA4WaH?format=jpg&name=large) 
[<img title="3备份缩略图(点击查看原图)" height="100" src="img/thumb/FSuCZH8UAAIrddS.jpg"/> 
](https://pbs.twimg.com/media/FSuCZH8UAAIrddS?format=jpg&name=large) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVZOCCVQAEB6dF.jpg"/> 
](https://pbs.twimg.com/media/FiVZOCCVQAEB6dF?format=jpg&name=large) 
[<img title="4备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVZOCKUcAA2y5W.jpg"/> 
](https://pbs.twimg.com/media/FiVZOCKUcAA2y5W?format=jpg&name=large)  
其中的文字是： ...    
[链接4](https://twitter.com/tohnomiyuki00/status/1595780126788136960)中的评论：  
マーカーをセットするタイプが出て画期的だと思ったが、デジタル作画普及が早すぎて、マーカータイプは短命に終わったようだ。まあ、レトラセットも潰れたしな……ネームはもちろん、完成までフルアナログの時代。  
（标记集类型的出现，被认为是革命性的，但数字绘画传播得太快了，标记类型似乎是短命的。嗯，Letraset也倒闭了......这是一个完全模拟的时代，当然，从名字到作品的完成都是如此。）  
注： 关于Letraset的一些资料：[Letraset: The History of Dry Transfers](https://imagetransfers.com/blog/history-letraset-instant-transfers/)  


- "用于Cat's♥Eye和CityHunter的背景资料"。遗憾的是其中大部分文字看不清楚。        
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04041_600x600.jpg)  (译注：[大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04041_4800x4800.jpg) )  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h28226d774j20ku0dwmyq.jpg"/> 
](https://wx1.sinaimg.cn/wap720/bcb839c9ly1h28226d774j20ku0dwmyq.jpg)  


- 下图里的文字有：“車両資料用 模型”，“作画に使用された画材”，“開明 まんが墨汁 MANGA INK”,"MGC CUSTOM GUN WORKS 43"， “SMITH & WESSON”  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04040_600x600.jpg)  (译注：[大图,可看清物品、枪支上的文字](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04040_4800x4800.jpg) )   
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2821ueirqj20ku0dwjsl.jpg"/> 
](https://wx4.sinaimg.cn/wap720/bcb839c9ly1h2821ueirqj20ku0dwjsl.jpg)  
Twitter上的[链接1](https://twitter.com/tohnomiyuki00/status/1595779511278219264)有大图：  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVY_EeUAAIs4k5.jpg"/> 
](https://pbs.twimg.com/media/FiVY_EeUAAIs4k5?format=jpg&name=large)  
以及评论：  
入るとわりとすぐに政策道具や資料の展示。ペンはともかく、エアブラシとはお懐かしや。使うのにコツが要ったし、色を変えようと思えば、いちいち洗わなければならんかった。  
（当你入场时，立刻看到的是政策工具和材料的展览。撇开笔不谈，AirBrushes是我所怀念的东西。使用它需要很多技巧，如果你想改变颜色，你必须每次都要清洗它。）  


----------------------

### 留言 & 明信片  

- 留言1:      
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FS8TjizakAA0z0M.jpg"/> 
](https://pbs.twimg.com/media/FS8TjizakAA0z0M?format=jpg&name=large)  
大图: 
[原图](https://pbs.twimg.com/media/FS8TjizakAA0z0M?format=jpg&name=large)，
[原网页](https://twitter.com/yae_ch3/status/1526460307845312518)，
[备份图](http://www.hojocn.com/bbs/attachments/20220520_bfe9de72c60c733cd1eerxWEKnDViYV4.jpg) ，
[备份网页](http://www.hojocn.com/bbs/viewthread.php?tid=96791)  
其中的文字： ...（待识别）  

- 留言2:  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBz0q1UsAMG59e.jpg"/> 
](https://pbs.twimg.com/media/FTBz0q1UsAMG59e?format=jpg&name=large)   
大图: 
[原图](https://pbs.twimg.com/media/FTBz0q1UsAMG59e?format=jpg&name=medium)，
[原网页](https://twitter.com/TheEdition88/status/1526847632999522305)，
[备份图](http://www.hojocn.com/bbs/attachments/20220520_0f2425e1a8e18d18c4fa2yOPCtRU8Mf6.jpg) ，
[备份网页](http://www.hojocn.com/bbs/viewthread.php?tid=96791)  
其中的文字： ...（待识别）  

- 留言3:  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBz0q8VEAEAVST.jpg"/> 
](https://pbs.twimg.com/media/FTBz0q8VEAEAVST?format=jpg&name=large)   
大图: 
[原图](https://pbs.twimg.com/media/FTBz0q8VEAEAVST?format=jpg&name=large)，
[原网页](https://twitter.com/TheEdition88/status/1526847632999522305)，
[备份图](http://www.hojocn.com/bbs/attachments/20220520_bf9f5a33ebc16a97533aQgJFJp5NY7IV.jpg) ，
[备份网页](http://www.hojocn.com/bbs/viewthread.php?tid=96791)  
其中的文字： ...（待识别）  

- 留言4:  
[现场实拍 - Twitter](https://twitter.com/TheEdition88/status/1526847632999522305)  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBz0q8UUAAXosN.jpg"/> 
](https://pbs.twimg.com/media/FTBz0q8UUAAXosN?format=jpg&name=large)   
其中的文字： ...（待识别）  

- 留言5:  
[现场实拍 - Twitter](https://twitter.com/soramaru_z/status/1528324609153859584)    
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTWzHwnagAEbKmf.jpg"/> 
](https://pbs.twimg.com/media/FTWzHwnagAEbKmf?format=jpg&name=large)  
其中的文字： ...（待识别）  

- 留言6:  
[现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1528305049554030592)    
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTWhVTUagAA0FuM.jpg"/> 
](https://pbs.twimg.com/media/FTWhVTUagAA0FuM?format=jpg&name=large)  
其中的文字： ...（待识别）  

- 留言7:  
[现场实拍 - Twitter](https://twitter.com/ryosaeba357xyz1/status/1528214275939921920)    
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTVOyQWUAAAvq9x.jpg"/> 
](https://pbs.twimg.com/media/FTVOyQWUAAAvq9x?format=jpg&name=large)  
其中的文字： ... （待识别） 

- 留言8:  
[现场实拍 - Twitter](https://twitter.com/ha43zu/status/1527965733245726722)    
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTRsuWtVUAA_WwQ.jpg"/> 
](https://pbs.twimg.com/media/FTRsuWtVUAA_WwQ?format=jpg&name=large)  
其中的文字： ...（待识别）  

- 明信片1:  
[现场实拍 - Twitter](https://twitter.com/planar6ele/status/1525744023037616130)  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FSyIFu0VsAEleKW.jpg"/> 
](https://pbs.twimg.com/media/FSyIFu0VsAEleKW?format=jpg&name=large)  
其中的文字： ...（待识别）  

- 明信片2:  
[现场实拍 - Twitter](https://twitter.com/planar6ele/status/1525744023037616130)  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FSyIFu3VsAALD8j.jpg"/> 
](https://pbs.twimg.com/media/FSyIFu3VsAALD8j?format=jpg&name=large)  
其中的文字： ...（待识别）  


-------------------  

### Cat's♥Eye 40周年纪念原画展～然后向着城市猎人～ 千代田场展的一些细节  

摘自[Cat's♥Eye 40周年纪念原画展～然后向着城市猎人～展示会Report](https://edition-88.com/blogs/blog/catseye40th-report), 
（[中文翻译](../zz_edition-88.com/catseye40th-report(2022-05-21).md)  ）。    


- "登场人物的介绍面板"。遗憾的是其中大部分文字看不清楚。          
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04046_990d0745-ccb2-42fc-8aa4-d65bd95932ac_600x600.jpg)  (译注：[大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04046_990d0745-ccb2-42fc-8aa4-d65bd95932ac_4800x4800.jpg) )   


- "「小偷绅士的老鼠和犬鳴署Cat's Eye特搜班」展览继续进行"。遗憾的是其中大部分文字看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04051_600x600.jpg?v=1653018795) (译注：[大图,左侧配文仍看不清](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04051_9600x9600.jpg?v=1653018795) )   


- 下图里多幅展画的右下角有疑似汉字"不要"的红色字样。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04066_600x600.jpg)  (译注：[大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04066_4800x4800.jpg) )   


- "「City Hunter -XYZ-」展出了单话作品XYZ的原画和原稿。（译注：待校对）"。遗憾的是中部配文看不清楚。      
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04194_600x600.jpg)     (译注：[大图,展品的配文仍看不清](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04194_4800x4800.jpg) )   


- [Cat's♥Eye 40周年纪念原画展～然后向着城市猎人～展示会Report](https://edition-88.com/blogs/blog/catseye40th-report)（[中文翻译](../zz_edition-88.com/catseye40th-report(2022-05-21).md)）中提到："版画也在展出，所以你可以近距离地看到实物。在销售区播放的视频中，详细解释了这种技术和制作方法"。遗憾的是中部配文看不清楚；也不知道视频里介绍的这种技术和制作方法是什么[疑问]。    
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog02_600x600.jpg)  (译注：[大图,可惜看不清配文](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/blog02_4800x4800.jpg) )   


- 配文看不清：  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823oxeduj20ku0dwaak.jpg"/> 
](https://wx2.sinaimg.cn/wap720/bcb839c9ly1h2823oxeduj20ku0dwaak.jpg)  

- 一处配文。  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Fr244NBaUAAmqra.jpg"/> 
](https://twitter.com/ryosaeba357xyz1/status/1638669117040525312)  
    - 原文：  
    槇村のエピソード  
    〔北条先生〕  
    「キャツッアイ」の連載が終わった頃に堀江さんが担当編集から変わり、「シティーハンター」の連載企画は他の担当者と進めていました。  
    最後のネームを作って、それが編集部の連載会議にかけられて連載スタートが決まった時に堀江さんが担当編集に戻りました。  
    元々、僕が描いた連載第1話では、すでに香がアシスタントという設定から始まっていましたが、場江さんから香がアシスタントになる前の話から連載を始めていこうと提案を受けました。  
    好みの違いだと思いますが、堀江さんは最初からストーリーに沿って素直にやっていくのが好きなんです。  
    僕は途中からスタートし、段々過去が明らかになっていくストーリー構成が好きですけどね。  
    だから香がなぜアシスタントになったかという話は、後に出していこうと思てはいましたが、それを最初に書くことになりました。  
    「キャツッアイ」を18巻分連載しても、2年半か3年くらいの新人です。だから目上の人の意見は聞きます。段々と反感は出てくるものですが(笑)。  
    それで香がアシスタントになる経緯をどうしようかと考える中で、獠の初代相棒である兄の槇村が亡くなる事によって、妹の香がアシスタントになるというエピソードが生まれました。  
    だから亡くなる前提で槇村は出てきているのです。  
    - 译文：  
    槙村的轶事  
    〔北条先生〕  
    「Cat'sEye」连载结束后，换了责任编辑堀江，「CityHunter」的连载企划由其他负责人负责。  
    制作了最后的Name(译注：name指草稿。似乎是术语或行业黑话)，在编辑部的连载会议上决定开始连载的时候，堀江又回到了责任编辑的位置。  
    本来，我画的连载第1话，设定是香已经开始是助手了，但是堀江提议从香成为助手之前的故事开始连载。  
    可能是喜好不同吧，堀江喜欢从一开始就按照故事情节顺其自然。  
    我喜欢的故事结构是从中途开始，逐渐揭开往事。  
    所以香为什么会成为助手的话题，我本来打算放在后面写，但最终还是把它放在了开头。  
    即使连载了18卷的「Cat'sEye」，也不过是2年半或3年的新人。所以要听长辈的意见。渐渐地反感起来(笑)。  
    因此，在思考应该如何处理香成为助手的原因时，由于獠的初代搭档槙村去世，妹妹香成为助手的小插曲便产生了。  
    所以槙村是以去世为前提而登场的。  




-------------------

### Cat's♥Eye 40周年纪念原画展～然后向着城市猎人～ 博多场展的一些细节  

摘自[博多场展的报道](../zz_weibo.cn/catseye40th-report02(2022-11-29).md)。    

- 遗憾的是图中左侧的配文看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_4800x4800.jpg))   

- 遗憾的是图中左侧的配文看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_4800x4800.jpg))   

- 遗憾的是图中左侧的配文看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_4800x4800.jpg))   

- 遗憾的是图中左侧的配文看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_4800x4800.jpg))   

- 遗憾的是图中的配文看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_4800x4800.jpg))   

- 遗憾的是图中卡片上的文字看不清楚。
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_4800x4800.jpg))   

- 遗憾的是图中下方的配文看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_4800x4800.jpg))   

- 遗憾的是图中的配文看不清楚。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_4800x4800.jpg))   


-------------------

### Cat's♥Eye 40周年纪念原画展～然后向着城市猎人～ 名古屋场展的一些细节  

- 入口处有《CityHunter剧场版 天使之泪》的海报。  
[<img title="(名古屋展，点击查看原图)" height="100" src="img/thumb/Fs6uibPaQAA_gx-.jpg"/> 
](https://twitter.com/TheEdition88/status/1643442829308596225) 

- 《CityHunter - Double Edge -》的原画。遗憾的是图中的配文看不清楚。  
[<img title="(名古屋展，点击查看原图)" height="100" src="img/thumb/Fss828vaEAECcbI.jpg"/> 
](https://twitter.com/TheEdition88/status/1642473408259829760) 
[<img title="(名古屋展，点击查看原图)" height="100" src="img/thumb/Fss828xaEAEWp1E.jpg"/> 
](https://twitter.com/TheEdition88/status/1642473408259829760) 

- 似乎有一面墙贴满了Cat'sEye的漫画页面。     
[<img title="(名古屋展，点击查看原图)" height="100" src="img/thumb/Fsl6g-baAAET8Ds.jpg"/> 
](https://twitter.com/hidyk623/status/1641978254935597056) 


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
