# Leotard v.s. Bodysuit

Contents:  

- [Source1: Difference: Leotards VS Bodysuits |Speerise](#speerise)
- [Source2: Difference Between Leotard and Bodysuit |DifferenceBetween](#differencebetween)
- [Source3: Bodysuits vs. Leotards for Tokyo? Team USA Gymnasts Share Their Thoughts |TeamUSA](#TeamUSA)


<a name="speerise"></a>  
# Source1: [Difference: Leotards VS Bodysuits](https://www.speerise.com/blogs/news/difference-leotards-costume-vs-bodysuits-women)

By Sofia Garcia， Jul 29, 2022

![](https://cdn.shopifycdn.net/s/files/1/0365/8003/2645/articles/73d3cf9d190e5f1378f86820f68fe593.jpg?width=500)  

Bodysuits are designed to go under clothes as an alternative to a tank top or lingerie. They are tight-fitting and have elastic bands at the bottom and top for easy use with underwear. Leotards for women are also called "unitards," pant-leotards, or one-piece leotards. They're usually used for sports like dance, gymnastics, skiing, skating, wrestling, etc. You might have been confused by them in the past, but if you read on, you'll understand everything.  
Bodysuits可以穿在衣服里面，作为背心或内衣的替代品。它们很贴身，底部和顶部都有松紧带，方便与内衣搭配使用。女性的Leotards也被称为“unitards”，裤子leotards，或连体leotards。它们通常用于舞蹈、体操、滑雪、滑冰、摔跤等运动。过去你可能对它们感到困惑，但如果你继续读下去，你就会明白一切了。  


## What is a leotard?  

A leotard is a skintight fabric covering the chest, back and upper stomach. Typically, it is a single piece of fabric with a zipper and has long sleeves. A leotard costume is designed for use by gymnasts, ballerinas, and cheerleaders. They are made of stretchy spandex or Lycra material and have elastic bands at the top to help them stay in place.  
leotard是一种覆盖胸部、背部和腹部上部的紧身面料。通常情况下，它是一块带拉链的长袖面料。leotard是为体操运动员、芭蕾舞演员和拉拉队队员设计的。它们由弹性氨纶或莱卡材料制成，顶部有松紧带帮助它们固定。  

Leotards can be sleeveless, short-sleeved, or long-sleeved. Those worn by gymnasts can be very closely fitted and have leg holes that are open at the bottom, similar to the design of a skirt. A ballet leotard is slightly different from the standard gymnastic leotard because it has longer sleeves, shorter legs, and is more form-fitting.  
Leotard可以是无袖、短袖或长袖的。体操运动员穿的短裤非常贴身，裤腿底部有开口的洞，类似于裙子的设计。芭蕾leotard与标准的体操leotard略有不同，因为它有更长的袖子，更短的腿，更合身。

## What is a bodysuit?
A bodysuit is a very close-fitting garment that covers your torso and bottom. A body suit has snaps or zippers down its center, making it easy to get on and off. It is usually made of stretchy material that hugs your body closely. They are often worn with short jackets, leggings or skirts and are generally easier to move in than a leotard. Like the leotard, they can be sleeveless or have long sleeves. Many ballerinas wear long sleeve bodysuits instead of long sleeve leotards because they find them more comfortable during their performances.  
bodysuit是一种非常贴身的衣服，覆盖了你的躯干和臀部。bodysuit中间有拉链，穿脱都很方便。它通常由有弹性的材料制成，可以紧紧包围你的身体。它们通常搭配短夹克、打底裤或裙子，通常比leotard更容易穿。和leotard一样，它们可以是无袖的，也可以是长袖的。许多芭蕾舞演员穿长袖bodysuits而不是长袖leotards，因为他们觉得在表演时更舒服。  

Bodysuits can be part of casual wear, but they are also worn in some sports such as swimming and diving, ice skating, horse riding and motorcycle riding. Women wear bodysuits for their slimming effects, although this depends on the suit you choose.  
Bodysuits可以是休闲服的一部分，但在游泳、跳水、滑冰、骑马和骑摩托车等运动中也会穿。女性穿Bodysuits是为了显瘦，不过这取决于你选择的着装。

## The similarities between leotards and bodysuits  
leotards和bodysuits的相似之处  

Leotards and body suits are similar in that:  
紧身衣和紧身衣的相似之处在于:  

1. Both are made of tight-fitting fabric and are worn by ballerinas, gymnasts, ice skaters and cheerleaders.  
这两种服装都是由紧身面料制成的，芭蕾舞演员、体操运动员、滑冰运动员和啦啦队员都穿。  

2. They come in short or long sleeves, depending on the activity.  
它们有短袖或长袖，取决于活动。  

3. They don't cover the legs of the wearer.  
它们不覆盖穿着者的腿。  

4. Both are made of stretchy material that holds their shape and helps you move more quickly, with elastic bands around the top for further support and comfort.  
这两款都是由有弹性的材料制成的，可以保持形状，帮助你更快地移动，顶部还有松紧带，可以提供进一步的支撑和舒适。  

## The Differences between leotards and bodysuits  

Leotards and body suits are different in that:  
Leotards和Bodysuits的区别在于:  

1. A leotard is a skintight, a one-piece garment that covers the chest, back and upper stomach, while a body suit is a close-fitting garment that covers only the torso and bottom.  
leotard是一种紧身的、覆盖胸部、背部和腹部上部的连体服装，而bodysuit是一种只覆盖躯干和臀部的贴身服装。

2. A bodysuit has a skirt-like design, therefore more suitable for females, while a leotard doesn't have one, and thus is unisex.  
bodysuit有类似裙子的设计，因此更适合女性；而leotard没有，因此是男女通用的。

3. A bodysuit is worn with short jackets, leggings or skirts, whereas a leotard can be worn with pants or skirts depending on the style of dancing and the dancer's preference.  
bodysuit可以搭配短夹克、紧身裤或裙子，而leotard可以搭配裤子或裙子，这取决于舞蹈的风格和舞者的喜好。  

4. A bodysuit has zippers at its center front, which are easy to put on and take off, while a leotard has one large grouping of snaps or buttons around the neck area as a fastener.  
bodysuit的正前方有拉链，穿着和脱下都很方便，而leotard在颈部有一大块扣子或纽扣作为扣紧。

## Conclusion

Leotards and bodysuits are similar in design and function. A leotard is generally designed for gymnasts, ballerinas and cheerleaders, and a bodysuit is worn for swimming or athletics. Depending on your preference and the activity you are involved in, a bodysuit can provide the same protection and comfort as a leotard. The body suit is usually worn as casual wear, whereas leotards are reserved for formal dance performances such as ballet or cheerleading.  
Leotards和bodysuits在设计和功能上是相似的。leotard通常是为体操运动员、芭蕾舞演员和啦啦队队员设计的，而bodysuit则是为游泳或运动而穿的。根据你的喜好和你参加的活动，bodysuit可以提供和leotard一样的保护和舒适。Bodysuit通常作为休闲装穿，而leotards则是在芭蕾舞或啦啦队等正式舞蹈表演时才穿。






<a name="differencebetween"></a>  
# Source2: [Difference Between Leotard and Bodysuit](https://www.differencebetween.com/difference-between-leotard-and-vs-bodysuit/)

June 23, 2017 Posted by [Dili](https://www.differencebetween.com/author/dilini/)


## **Key Difference – Leotard vs Bodysuit** 

Leotard and bodysuit are two garments that look almost the same; this is why the difference between leotard and bodysuit is unknown to most. The key difference between leotard and bodysuit is that **a leotard is a skintight, one-piece garment which covers the torso of the wearer but leaves the legs exposed** whereas **a bodysuit is a one-piece, form-fitting garment that covers the torso and the crotch of the wearer.** The event or occasion of wearing either is also largely different from one another.  
Leotard和bodysuit是两种看起来几乎一样的衣服;这就是为什么leotard和bodysuit之间的区别不为大多数人所知。leotard和bodysuit的主要区别在于，**leotard是一种紧身的连体衣，遮住了穿着者的躯干，但露出了腿**，而**bodysuit是一种连体、合身的衣服，遮住了躯干和穿着者的胯部。**穿这两种衣服的场合也有很大不同。  

## What is a Leotard?

A leotard is a skintight, one-piece garment which covers the torso of the wearer but leaves the legs exposed. In that sense, a leotard is very similar to a swimsuit. Unisex garments, leotards are worn by performers who require overall body coverage without hindering flexibility. Leotards are commonly worn by dancers, gymnasts, acrobats, and contortionists. Leotard is also a part of the [ballet](https://www.differencebetween.com/difference-between-hip-hop-and-ballet/) dress and is worn beneath the ballet skirt.  
leotard是一种紧身的连体服装，它覆盖了穿着者的躯干，但露出了腿。从这个意义上说，leotard和泳装非常相似。作为男女通用的服装，leotards是由那些需要覆盖全身而又不妨碍灵活性的演员穿的。Leotards通常是舞者、体操运动员、杂技演员和柔术演员穿的。Leotard也是芭蕾舞裙的一部分，穿在芭蕾舞裙下面。

Leotard has an extended history; it was first introduced in the 1800s, by the French acrobatic performer Jules Léotard (1838–1870), from which the name of the garment was derived. Originally leotard was designed for male performers, but it became popular with women in the early 1900s as a swimsuit. The Early leotard was referred to as the maillot by Jules Léotard.  
Leotard有着悠久的历史;它最早是在19世纪由法国杂技演员Jules Léotard(1838-1870)引入的，这种服装的名字也由此而来。leotard最初是为男性表演者设计的，但在20世纪初作为泳装在女性中流行起来。早期的leotard被Jules Léotard称为maillot。  

Today, leotards are available in a variety of colors and materials, preferably [lycra or spandex](https://www.differencebetween.com/difference-between-lycra-and-vs-spandex/) (a material with an exceptional elasticity that helps to shape the body better). There are also sleeveless, short-sleeved and long-sleeved leotards. Further, various necklines can also be found in modern leotards such as crew neck, polo neck, and scoop-neck.  
如今，leotards有多种颜色和材质可供选择，最好是莱卡或氨纶(一种具有特殊弹性的材料，有助于更好地塑造身材)。还有无袖、短袖和长袖的leotards。此外，各种领口也可以在现代紧身衣中找到，如船员领，马球领和勺领。  

For performers such as gymnasts, contortionists and circus performers, it is important that their precise body movements are seen clearly by the audience. Leotard enables this due to its skintight nature. Many dancers use leotards instead of adorned costumes since leotards are very simple in nature and do not divert attention from the dance like a decorated costume.  
对于体操运动员、柔术演员和马戏团演员等表演者来说，让观众清楚地看到他们精确的身体动作是很重要的。Leotard之所以能做到这一点，是因为它的紧身性质。许多舞者使用Leotard来代替装饰的服装，因为Leotard本质上非常简单，不会像装饰的服装那样转移舞蹈的注意力。

![Difference Between Leotard and Bodysuit ](https://i0.wp.com/www.differencebetween.com/wp-content/uploads/2017/06/Difference-Between-Leotard-and-Bodysuit-2.jpg?resize=551%2C410&ssl=1 "Difference Between Leotard and Bodysuit 1")  
**Figure 01: Leotard**


## What is a Bodysuit?

A bodysuit is a one-piece, form-fitting garment that covers the torso and the crotch of the wearer. A bodysuit is seemingly very similar to a leotard or a swimsuit. The major difference is that a bodysuit has snaps or hooks at the crotch unlike in a leotard or a swimsuit. Bodysuits are available in a variety of materials (such as lycra and spandex) and colors. Bodysuit is not considered as a form of athletic wear or sportswear. A progression from the leotard, bodysuit was first presented in the 1950s by fashion designer Claire McCardell. Bodysuit became a fashion item for both men and women in the 1980s.  
bodysuit是一种连体的、合身的衣服，覆盖了穿着者的躯干和胯部。bodysuit看起来很像leotard或泳装。与它们不同的是，bodysuit在裆部有扣或挂钩。Bodysuits有多种材质(如莱卡和氨纶)和颜色可供选择。Bodysuit不被认为是一种运动服装或运动服。bodysuit由leotard演变而来，于20世纪50年代由时装设计师Claire McCardell首次推出。20世纪80年代，Bodysuit成为男性和女性的时尚单品。  

Today, bodysuits are generally worn by women with trousers or a skirt and available with sleeves and without sleeves. Bodysuits can be used as a part of [casual wear](https://www.differencebetween.com/difference-between-casual-and-vs-formal-wear/) and [semi-formal wear](https://www.differencebetween.com/difference-between-formal-and-vs-semi-formal/), paired with long [sweaters](https://www.differencebetween.com/difference-between-pullover-and-vs-sweater/) and [blazers](https://www.differencebetween.com/difference-between-blazer-and-vs-sportcoat/#ab). They are usually worn with [skinny jeans](https://www.differencebetween.com/difference-between-skinny-jeans-and-vs-carrot-jeans/), high waist jeans, and skirts of various styles.  
如今，bodysuits通常是女性搭配长裤或裙子穿的，有袖和无袖之分。Bodysuits可以作为休闲服和半正装的一部分，搭配长毛衣和运动夹克。它们通常搭配紧身牛仔裤、高腰牛仔裤和各种款式的裙子。  

Bodysuits are also available for younger children and toddlers and are called **Onesies** or **snapsuits**. A counterpart to bodysuit is also available as a close-fitting shirt or blouse which is referred to as a **bodyshirt**.  
Bodysuits也适用于年幼的儿童和学步儿童，被称为**连体衣**或**snapsuits**。与bodysuit相对应的还有贴身衬衫或衬衫，称为**紧身衬衫**。

![Key Difference - Leotard vs Bodysuit ](https://i2.wp.com/www.differencebetween.com/wp-content/uploads/2017/06/Difference-Between-Leotard-and-Bodysuit-1.jpg?resize=333%2C500&ssl=1 "Difference Between Leotard and Bodysuit 2")  
**Figure 02: Bodysuits can be coupled with trousers.**

## What are the similarities between Leotard and Bodysuit?

*   Both leotard and bodysuit are one-piece skintight garments.  
两者都是连体紧身衣。  

*   Both of these garments do not cover the legs of the wearer.  
两者都不覆盖穿着者的腿。  


## What is the difference between Leotard and Bodysuit?（Leotard和Bodysuit有什么区别?）  
### Leotard vs Bodysuit

Leotard is a skintight, one-piece garment which covers the torso of the wearer but leaves the legs exposed.  
Leotard是一种紧身的连体服装，它覆盖了穿着者的躯干，但露出了腿。

Bodysuit is a one-piece, form-fitting garment that covers the torso and the crotch of the wearer.  
Bodysuit是一种连体的、合身的衣服，覆盖了穿着者的躯干和胯部。

**Use**  

Leotard is most commonly worn by dancers, gymnasts, athletes, and contortionists.  
Leotard是舞蹈家、体操运动员、运动员和柔术演员最常穿的。  

Bodysuit is used as a piece of style garment often coupled with trousers and skirts.  
Bodysuit是一种风格的服装，通常与裤子和裙子搭配使用。  


**Gender**

Leotard is a unisex garment.  
Leotard是一种男女皆宜的服装。  

Bodysuits are worn by females.  
Bodysuits是女性穿的。  


**Origins**

Leotard was introduced by acrobatic performer Jules Léotard in the 1800s.  
Leotard是由杂技演员Jules Léotard在19世纪引入的。  

Bodysuit was introduced by fashion designer Claire McCardell in 1950s.  
Bodysuit是由时装设计师Claire McCardell在20世纪50年代推出的。  

## Summary – Leotard vs Bodysuit

The difference between leotard and bodysuit is not a seemingly distinct one. However they mainly differ in their use; leotards are used by performers such as dancers, gymnasts, acrobats, and contortionists while bodysuits are worn by women in general as casual and as a part of professional wear. Further, while leotard is a unisex garment, bodysuits are worn by females.  
leotard和bodysuit之间的区别看起来并不明显。然而，它们主要不同的用途;leotards是舞者、体操运动员、杂技演员和柔术演员的服装，而bodysuits一般是女性的休闲服装和职业服装的一部分。此外，leotard是男女通用的服装，而bodysuits是女性穿的。  

##### References:

1.”Unitard Vs. Leotard.” EHow. Leaf Group, 26 July 2011. Web. [Available here.](https://www.google.lk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjJzPib7dDUAhUIq48KHXPgDGQQFggkMAA&url=http%3A%2F%2Fwww.ehow.com%2Finfo_8789132_unitard-vs-leotard.html&usg=AFQjCNGk9dD4bFGitLcR-Mb9JDw_W9AumQ) 18 June 2017.  
2.”Leotard – Ballet Term Definition.” BalletHub. N.p., n.d. Web. [Available here](https://ballethub.com/ballet-term/leotard/). 19 June 2017.  
3.”20 Style Tips On How To Wear A Bodysuit This Summer.” Gurl.com. N.p., 15 July 2016. Web. [Available here.](http://www.gurl.com/2016/07/16/style-tips-on-how-to-wear-a-bodysuit-this-summer-outfit-ideas/) 19 June 2017.

##### Image Courtesy:

1\. “Jade Barbosa, floor routine, 2007” By Original photograph by Wilson Dias/AbrDerivative by Keraunoscopia – Derived from File:Jade Barbosa 16072007.jpg [(CC BY 3.0 br)](http://creativecommons.org/licenses/by/3.0/br/deed.en) via [Commons Wikimedia](https://commons.wikimedia.org/w/index.php?curid=26364370)  
2\. “[Pastel Donut Print Bodysuit, White Jeans, a Mint Green Satchel and Blue Wedges”](https://www.flickr.com/photos/63405864@N04/19485974301/in/photostream/) By Jamie –  [(CC BY-SA 2.0)](http://creativecommons.org/licenses/by-sa/2.0) via [Commons Wikimedia](https://commons.wikimedia.org/w/index.php?curid=48619117)








<a name="TeamUSA"></a>  
# Source3: [Bodysuits vs. Leotards for Tokyo? Team USA Gymnasts Share Their Thoughts](https://www.teamusa.org/News/2021/June/22/Bodysuits-vs-Leotards-for-Tokyo-Team-USA-Gymnasts-Share-Their-Thoughts)

By Blythe Lawrence | June 22, 2021, 2:51 p.m. (ET)

Full-length bodysuits are having a moment in gymnastics, and while U.S. gymnasts are supportive of the trend and its far-reaching message, they are unlikely to wear them at this summer’s Olympic Games.   
长身bodysuits在体操界大行其道，尽管美国体操运动员支持这一趋势及其深远意义，但他们不太可能在今年夏天的奥运会上穿这种衣服。
  
The long-legged garment made international headlines when German national team members  Sarah Voss, Kim Bui and Elisabeth Seitz wore it at April’s European Championships in Switzerland, eschewing the traditional leotard, which leaves the legs bare.   
德国国家队队员萨拉·沃斯、金·布伊和伊丽莎白·塞茨在今年4月瑞士举行的欧洲杯上穿着这种长腿服装，避开了传统的露腿leotard，登上了国际头条。
  
Part of the reason Voss, Bui and Seitz opted for bodysuits was to illustrate to younger gymnasts that elite gymnasts did not have to compete in more revealing options, which might make some feel uncomfortable, Voss said.  
沃斯说，沃斯、布伊和塞茨选择bodysuits的部分原因是为了向年轻的体操运动员说明，精英体操运动员不必穿着更暴露的衣服参加比赛，这可能会让一些人感到不舒服。
  
Doing gymnastics in leotards is fraught with a kind of peril that those who have been there can easily identify with.  
穿着leotards做体操充满了一种危险，那些经历过的人很容易就能体会到。
  
“One has the feeling that they slip out of place, and perhaps that cameras or photographers can catch this poor moment,” Voss explained to Reuters. “We can always freely decide if we want to wear a leotard or a full bodysuit.”  
沃斯向路透社解释说:“人们感觉它们不合时宜，也许相机或摄影师可以捕捉到这个可怜的时刻。”“我们总是可以自由决定是穿leotard还是全套bodysuit。”
  
While U.S. gymnasts embrace the idea of choice, all who were asked said they would stick to traditional leotards — for now, anyway.  
虽然美国体操运动员接受选择的想法，但所有被问及的人都表示，他们会坚持传统的leotards——至少目前是这样。
  
“Personally, I feel comfortable in a leo, and that’s more my style,” said Simone Biles, who at 4 feet, 8 inches tall prefers the leg-lengthening look of leotards.      
身高4英尺8英寸的Simone Biles说:“就我个人而言，我觉得leo很舒服，这更符合我的风格。”她更喜欢leotards的修长效果。

  
“I feel like (a bodysuit) might shorten me,” Biles added, “but I stand with their decision to wear whatever they please and whatever makes them feel comfortable. So if anyone out there wants to wear a unitard or leotard, it’s totally up to you.”  
Biles补充说:“我觉得(一件bodysuit)可能会让我显矮，但我支持他们的决定，穿任何他们喜欢的、让他们感觉舒服的衣服。”所以如果有人想穿unitard或leotard，完全取决于你。”
  
Bodysuits have been permitted competition attire in international gymnastics competitions for several years, though the vast majority of female gymnasts opt to wear leotards instead. There have been a few exceptions: Marina Nekrasova of Azerbaijan wore one at a world cup event in 2019, and Jana Elkeky of Qatar performed in a bodysuit that covered her upper legs at the 2018 world championships in Doha.   
多年来，Bodysuits一直被允许作为国际体操比赛的比赛服装，尽管绝大多数女子体操运动员选择穿leotards。也有一些例外:Marina Nekrasova在2019年的世界杯比赛中穿着紧身裤，卡塔尔的Jana Elkeky在2018年多哈世界锦标赛上穿着遮住大腿的bodysuit表演。
  
“Perhaps someone is stopped by the leotards, and I wanted to show that it’s possible (to be competitive in a bodysuit), there are no limits,” said Nekrasova, who like Elkeky competes for a nation where leotards are not always accepted as appropriate attire for women and girls. “I often hear that parents take the girls out of gymnastics classes because of the leotards and it’s a pity that they leave. You can change the uniform … so you can train and try. I hope I managed to show that.”  
涅克拉索娃说:“也许有人被leotards挡住了，我想证明(穿着bodysuit参加比赛)是有可能的，没有限制。”她和Elkeky一样，在这个国家，leotards并不总是被认为是女性和女孩的合适服装。“我经常听到父母因为孩子们穿leotards而让她们退出体操课，很遗憾她们最终还是离开了。你可以换制服，这样你就可以训练和尝试了。我希望我做到了。”
  
Others have staunchly defended their right to wear the leotard. When Farah Ann Abdul Hadi of Malaysia was criticized for wearing a leotard several years ago, Hadi hit back at her online detractors. To the cleric who declared that “gymnastics is not for Muslim women,” the seven-time Southeast Asian Games gold medallist responded on Twitter with a virtual eyeroll, writing that “empty cans make the most noise.”  
其他人则坚决捍卫自己穿leotard的权利。几年前，马来西亚的Farah Ann Abdul Hadi因穿leotard而受到批评，Hadi在网上回击了她的批评者。对于宣称“体操不适合穆斯林女性”的神职人员，这位七次东南亚运动会金牌得主在推特上用一个虚拟的眼珠回应，写道“空罐子发出的声音最大”。
  
U.S. Olympic hopeful Sunisa Lee has declared herself pro-bodysuit, at least in theory.  
有望参加奥运会的美国选手李宗伟(Sunisa Lee)宣布自己支持bodysuit泳衣，至少在理论上是这样。
  
“It’s a really good idea,” said Lee, the runner-up to Biles at the national championships earlier this month. “I think those are really cool. I like it a lot because people should be able to wear what they feel comfortable in, and it shouldn’t be a leotard if you don’t want to wear it.”  
“这真是个好主意，”李宗伟说，她在本月早些时候的全国锦标赛上获得了Biles的亚军。“我认为这些真的很酷。我非常喜欢它，因为人们应该能够穿他们觉得舒服的衣服，如果你不想穿，就不应该是leotard。”
  
She isn’t sure if she would wear one, though.  
不过，她不确定自己是否会穿上它。
  
“I like leotards,” she said.  
“我喜欢leotards，”她说。
  
The feeling of something around the legs would likely take some getting used to, commented Kara Eaker, one of 18 Olympic hopefuls who will be competing at this week’s U.S. Olympic Trials in St. Louis.   
“腿部周围有东西的感觉可能需要一段时间才能适应，”Kara Eaker评论道，她是将参加本周在圣路易斯举行的美国奥运选拔赛的18名奥运选手之一。  
  
Eaker praised the German gymnasts for doing something different.  
艾克称赞德国体操运动员做了一些不同寻常的事情。  
  
“It’s different, it stands out, and it’s a power move,” she remarked.   
“这是不同的，它引人注目，是一种权力之举，”她说。  
  
That said, “I haven’t really trained in one before, so I wouldn’t know how that would affect my training and gymnastics ability,” she added. “But I’d be open to trying anything.”  
也就是说，“我以前没有真正训练过，所以我不知道这会对我的训练和体操能力产生什么影响，”她补充说。“但我愿意尝试任何方法。”  

#### Blythe Lawrence
Blythe Lawrence has covered two Olympic Games and is a freelance contributor to TeamUSA.org on behalf of [Red Line Editorial, Inc](https://reditorial.com). Follow her on Twitter @rockergymnastix.
