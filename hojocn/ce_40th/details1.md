部分展品的细节目录： 
    [0](./details0.md), 
    [1](./details1.md), 
    [2](./details2.md), 
    [3](./details3.md), 
    [总结](./details3.md#SummaryOfDetails),  

## 「Cat's Eye 40周年纪念原画展」部分展品的细节1  

「Cat's Eye 40周年纪念原画展」以下简称“画展”。  


-----  

## 搜集到的图片  
本文的图片源自以下链接：  

- [最新一次展会的所有展品的信息](https://edition-88.com/collections/hojotsukasa)（及其[海外版](https://edition88.com/collections/tsukasa-hojo)）  
- [「Cat's♥Eye」相关商品](https://edition-88.com/pages/catseye-goods)  
- [「City Hunter」相关商品](https://edition-88.com/pages/cityhunter-goods)  
- [2022年5月，画展结束后的官方报道](https://edition-88.com/blogs/blog/catseye40th-report)  
- [猫眼40周年记念原画展 - Weibo](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)  
- Twitter上的相关tag（已转载至[Twitter上与北条司及其作品相关的信息 - hojocn](http://www.hojocn.com/bbs/viewthread.php?tid=96791)）:  
    - [キャッツアイ40周年記念原画展 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年記念原画展)  
    - [キャッツアイ40周年原画展 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年原画展)  
    - [キャッツアイ40周年 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年)  
    - [キャッツアイ - Twitter](https://twitter.com/hashtag/キャッツアイ)  
    - [CatsEye - Twitter](https://twitter.com/hashtag/CatsEye)  


## 说明  
- [Natalie的报道](../zz_natalie.mu/2022-05-12_ce-40th.md)有如下信息：  
北条と堀江社長のコメントもいたるところに掲出され、3姉妹のモデルや作画上でのこだわり、作品の舞台となった街は実際にどこかをイメージしているのかなどについて明かされている。  
（到处都张贴着北条和堀江社长的评论，说明了三姐妹的模特和作画上的讲究、作品的舞台所在的街道实际上是什么地方。）  
可见，这些张贴的评论很有价值！很可惜，因为无法去到展览现场，无法得知这些评论的内容；而网上所展示的该展览的照片中很少是关于这些评论的。  

- 预备资料。以下资料或许有助于更好地理解该展览的展品信息：    
    - [关于官网展览和展品的一些说明](./about_edition88.md)  
    - [关于纸张和印刷技术的资料](./extra_info.md)  

- 名古屋展时，部分商品使用了EDITION88自行开发的混合版画印刷技术：[88Graph](./about_edition88(2023).md#88Graph)  

- https://edition-88.com/collections/hojotsukasa  
    - 该页面列出了最新一次展会的商品，包括版画、复制原稿、周边。  
    - 虽然都是「Cat's Eye 40周年纪念原画展」，但不同的展会（例如，千代田展、福冈展、名古屋展）有不同的展品。从迄今的三次展会来看，每次都有新展品、新商品，但似乎每次也会少一些曾经的商品。粉丝们如果喜欢搜集这些图片的话，请不要错过。    
    - 每个商品的页面有带水印的高清图（可下载4472x4472的分辨率的图片）。版画、复制原稿的商品页面有局部特写，其细节度更高。  
    - 从迄今三次展会来看，每次展会时，即便是同一个商品的页面，也会新增不同的图片。例如，名古屋展时，很多商品页面增加了签名的局部图（例如，[Cat's Eye, Art Print #8](https://edition88.com/products/catseye-hanga8)）、一些商品页面有新的局部图（例如，[「Cat's♥Eye」版画10](https://edition-88.com/products/catseye-hanga10)和[Cat's Eye, Art Print #8](https://edition88.com/products/catseye-hanga8)）。所以，粉丝们如果喜欢搜集这些图片的话，请不要错过。    

- https://edition88.com/collections/tsukasa-hojo（edition-88.com的海外版。以下简称海外版）。  
    - 展示猫眼40周年纪念原画展的商品包括版画、复制原稿。每个商品的页面有带水印的高清图（可下载4472x4472的分辨率的图片）。  
    - 海外版的不同之处：1)缺少一些商品；2)某些商品有新的局部图（例如，「Cat's♥Eye」版画8在[edition88.com](https://edition88.com/products/catseye-hanga8)比在[edition-88.com](https://edition-88.com/products/catseye-hanga8)多几个局部图）；所以，粉丝们如果喜欢搜集这些图片的话，请不要错过。  

- 官网可下载4472x4472的分辨率的图片(带水印)。这意味着如果按照[精致照片的效果(300ppi)](https://blog.csdn.net/wzr1201/article/details/17302761)打印，则可打印出378.5x378.5mm的照片。这个大小介于官网商品A4(297×210mm)和B2(728mmx515mm)之间。  

- 官网展示的图片的分辨率很大，使得图片的细节度是前所未有的！  

- 因为如下原因，所以无法确定原画的真实色彩：  
    - 同一个官网展品图片，在不同浏览器里显示的颜色不同。如下图所示：左一为Chromium浏览器显示的官网网址图片，左二为Firefox浏览器显示的官网网址图片，左三为下载该图片后用Gimp软件打开。  
    ![](img/color_diff_0.jpg)  
    目前不知道如何校准该颜色偏差[疑问]。    
    
    - 下载官网展品图片后，本地图片在不同浏览器（或图片编辑器）里显示的颜色不同。如下图所示：左一为Chromium浏览器显示的该图片颜色，左二为Firefox浏览器显示的该图片颜色，左三为Gimp软件打开后显示的该图片颜色。    
    ![](img/color_diff_1.jpg)  
    目前不知道如何校准该颜色偏差[疑问]。（绝大多数展品图片下载时为webp格式，内含color profile）    

- 猫眼40周年纪念原画展福冈站时，该展东京站时的部分商品页面已经不存在了。由此看出，该官网的有些网页会随着展览的结束而被删除。所以，粉丝们如果喜欢搜集这些信息的话，请尽早保存。  

- 按作品的创作时间顺序排列下文展品，这或许有利于看出作者的创作变化。有些展品暂无法获得其准确的创作年月，将这些展品放于与其绘画风格近似的展品附近。    

- [展品信息](./goods_info.md)里多次提到的内容（以及译文）或许有助于理解以下展品：  
シティーハンターの版画では1枚毎、パール絵の具の粒子を吹き付けています。  
（CityHunter的每张版画上都喷涂了珍珠漆颗粒。）  
見る角度によって光沢が強調され、作品の奥行きが表現できました。  
（从不同的观察角度强调光泽，使艺术品的深度得到表达。）  
シートサイズは原画の原寸サイズとなっており、より原画に近い状態でお楽しみいただけます。  
（这张纸的尺寸是原画的尺寸，所以你可以更接近于欣赏原画。）  
北条司先生からのコメント  
連載当時、カラー原稿には紫を含む色や、紫寄りの青やピンクなどは印刷した時に色の再現が難しいため、あまり使わないようにと言われていましたが、私は好んで使っていました。今回の版画作品は実際に描いた色も、地の紙の色も原画にかなり近く、原画か版画か一目では分からないくらいの仕上がりになっています。あれから40年後の今、昔は印刷で出せなかったオリジナルの色味が再現できました。版画になった作品が、皆さんのお手元に渡ることを嬉しく思っています。  
（北条司先生的评论   
在连载时，我被告知不要在彩色手稿中使用包括紫色或偏向紫色的蓝色加粉色的颜色，因为这些颜色很难在印刷中呈现。 我还是想使用它们。这一次，印刷品的颜色与原作非常接近，无论是实际绘画的颜色还是原纸的颜色。其结果是如此之好，以至于很难一眼就看出它是原作还是印刷品。40年后的今天，我已经能够重现原作的色彩，这在过去是无法用印刷品再现的。我很高兴这些版画现在在你手中。）    
国际版彩色原画的不同之处：  
The international editions are developed in mixed media, using both giclée (water-based pigments) and UV printers (UV curing ink) for an exquisite expression of color and texture.  
（国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）对色彩和纹理进行精致的表达。）  


## 文本讨论什么、不讨论什么
本文主要关注作者北条司的绘画。  
图片上有些效果是印刷方法导致的，有些效果是纸张导致的，本文不讨论这些话题。    


## 商品的官方认证  
![](https://cdn.shopifycdn.net/s/files/1/0614/5123/9648/files/20220823115629_700x.jpg)  


---  

<a name="1981-04-i1"></a>  
## CE 复制原稿/1 (B2) /Cat's♥Eye (1981.04)  

![](img/B2_fukuseigenko_01_thumb.jpg)  
CE(18卷版)01卷01话，据该话末页的时间戳推测为1981年04月。  

[日文版商品链接 複製原稿/1 (B2) /キャッツ♥アイ](https://edition-88.com/products/catseye-duplicateｍanuscript1-b2)  
胶印,高档纸,  

[英文版商品链接：无]()  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CE(18卷版)01卷01话扉页；  
C: 源自https://edition-88.com/catseye40th-exhibition  

A(下图左一)，B(下图左二), C(下图左三),：  
![](img/B2_fukuseigenko_01_thumb.jpg) 
![](img/ce_v01_0002_thumb.jpg) 
![](img/cat_s-eye_tenji1_00_crop0.jpg)  

细节：   

- 画面左、右、下侧均显示出更多的内容。例如，下侧显示出了三人的脚。  
- 小瞳的衣服为浅色。  
- 小瞳的头发。A(下图左一)对比B(下图左二)。  
![](img/B2_fukuseigenko_01_crop0.jpg) 
![](img/ce_v01_0002_crop0.jpg)  
- 阴影渐变更细腻。A(下图左一列)对比B(下图左二列)。  
![](img/B2_fukuseigenko_01_crop1.jpg) 
![](img/ce_01_0002_crop1.jpg)  
![](img/B2_fukuseigenko_01_crop2.jpg) 
![](img/ce_01_0002_crop2.jpg)  
- 纯黑色区域能看出纸张的凹凸不平。A(下图左一)对比B(下图左二)。  
![](img/B2_fukuseigenko_01_crop3.jpg) 
![](img/ce_01_0002_crop3.jpg)  
- A中(下图左一)排线阴影(下图手心)有更多细节。对比B(下图左二)。  
![](img/B2_fukuseigenko_01_crop4.jpg) 
![](img/ce_01_0002_crop4.jpg)  
- A中(下图左一)黑色阴影区域能看出笔触。对比B(下图左二)。  
![](img/B2_fukuseigenko_01_crop7.jpg) 
![](img/ce_01_0002_crop7.jpg)  
- A中(下图左一)排线阴影的渐变，包括在网点纸上画出(擦出?)高光。对比B(下图左二)。  
![](img/B2_fukuseigenko_01_crop8.jpg) 
![](img/ce_01_0002_crop8.jpg)  
- A中能看到网点纸覆盖着部分线条：  
![](img/B2_fukuseigenko_01_crop5.jpg)  
- A中能看到宝石的光芒用白色线条绘制，并覆盖在网点纸之上：  
![](img/B2_fukuseigenko_01_crop6.jpg)  
- A中能看到面部、颈部的网点阴影的边缘有白色涂抹的痕迹。我猜，这是为了让阴影过渡更平滑：  
![](img/B2_fukuseigenko_01_crop9.jpg) 
![](img/B2_fukuseigenko_01_crop10.jpg)  
- A中能看到：1)网点纸遮盖了黑色边线，2)网点的不均匀说明使用了多层网点纸。  
![](img/B2_fukuseigenko_01_crop11.jpg)  

- A中眼部的细节：  
![](img/B2_fukuseigenko_01_crop12.jpg)  

- A中边缘有手写文字。   

- A(下图左一)、B(下图左二)、C(下图左三)中的猫眼卡对比。C中的泛黄：  
![](img/B2_fukuseigenko_01_crop13.jpg) 
![](img/ce_01_0002_crop9.jpg) 
![](img/cat_s-eye_tenji1_00_crop1.jpg)  



---  

<a name="1981-04-i2"></a>  
## CE 官网报道中的图片06 (1981.04)

![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_1_thumb.jpg)  
Cat's Eye 18卷版第1卷第11页。据该话末页的时间戳推测为1981年04月。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 18卷版第1卷第11页；  

A(下图左一)，B（下图左二）：  
![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_1_thumb.jpg) 
![](img/ce_01_011_thumb.jpg)  

细节：   

- A中右侧有蓝色标记。似乎有标记从上至下的最小范围。  

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1981-04-i3"></a>  
## CE 原画 (1981.04)  
Cat's Eye 18卷版第1卷第13-14页、27-28页。据该话末页的时间戳推测为1981年04月。  

[现场实拍 - Twitter](https://twitter.com/TOKYO_GENSO/status/1525013304812244996)  

[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnvggQUAAAxJsE.jpg"/> 
](https://pbs.twimg.com/media/FSnvggQUAAAxJsE?format=jpg&name=large) 

细节：  

注：因为A的分辨率小于B，所以无法对比查看A的细节。  


---  

<a name="1981-04-i4"></a>  
## CE 原画 (1981.04)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTbcgD-VEAEwu2U.jpg"/> 
](https://pbs.twimg.com/media/FTbcgD-VEAEwu2U?format=jpg&name=large)   
Cat's Eye 18卷版1卷27页。据该话末页的时间戳推测为1981年04月。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第1卷第27页；  
C:[现场实拍 - Twitter](https://twitter.com/MasaDirector/status/1528651575224197120)  
D:[现场实拍 - Twitter](https://twitter.com/TOKYO_GENSO/status/1525013304812244996)  

A(下图左一)，B(下图左二), C(下图左三)，D(下图左四)：  
![](img/not_given.jpg) 
![](img/ce_01_027_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTbcgD-VEAEwu2U.jpg"/> 
](https://pbs.twimg.com/media/FTbcgD-VEAEwu2U?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnvggQUAAAxJsE.jpg"/> 
](https://pbs.twimg.com/media/FSnvggQUAAAxJsE?format=jpg&name=large) 

细节：  

- C(下图左二)中有手写文字："白フチ文字"（译：白边文字）。对比B(下图左一)。    
![](img/ce_01_027_crop0.jpg) 
![](img/FTbcgD-VEAEwu2U_crop0.jpg)  

- C(下图左一)与D(下图左二)的不同。我猜，C可能是D旁边的配图。    
![](img/FTbcgD-VEAEwu2U_crop0.jpg) 
![](img/FSnvggQUAAAxJsE_crop0.jpg)  



---  

<a name="1981-04-i5"></a>  
## CE 官网报道中的图片07 (1981-04)

![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_2_thumb.jpg)  
Cat's Eye 18卷版第1卷第49页。据该页的时间戳推测为1981年04月。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 18卷版第1卷第49页；  

A(下图左一)，B(下图左二)：  
![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_2_thumb.jpg) 
![](img/ce_01_049_thumb.jpg)  


细节：   

- A中右下角有红色标记。[疑问]红色标记是什么意思？    
![](img/B2_fukuseigenko_02_mark2.jpg)  

- A中右侧有蓝色标记。似乎有标记从上至下的最小范围。 

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  

更多细节参见[北条司展](../ch_40th/details1.md#1981-04-i0)


---  

<a name="1982-vol01-cover"></a>  
## CE 官网报道中的图片00 (1982)

![](img/cat_s-eye_tenji_96_00_thumb.jpg)  
Cat's Eye 18卷版第1卷封面。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 18卷版第1卷封面；  
C: [现场实拍 - Twitter](https://twitter.com/kirakuni91/status/1527217808375152640)  
D: [现场实拍 - Twitter](https://twitter.com/thelowtierchara/status/1528027698114834434) 

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![](img/cat_s-eye_tenji_96_00_thumb.jpg) 
![](img/ce_01_000_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTHEffYaQAA4CYb.jpg"/> 
](https://pbs.twimg.com/media/FTHEffYaQAA4CYb?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTSg6tFUcAIQ_bI.jpg"/> 
](https://pbs.twimg.com/media/FTSg6tFUcAIQ_bI?format=jpg&name=4096x4096) 

细节：   

- A的烟雾逼真。  
![](img/cat_s-eye_tenji_96_00_crop0.jpg)  

- A的电线杆着色逼真。  
![](img/cat_s-eye_tenji_96_00_crop1.jpg)  

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1981-09-i1"></a>  
## CE 複製原稿1 (B4) /Cat's♥Eye  (1981.09)  
![](img/hukuseigenkou1_B4_thumb.jpg)  
Cat's Eye 完全版第1卷第7話"紳士大盜登場"第5页。  
Cat's Eye 18卷版第2卷第7话"小偷紳士登場"第5页。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript1-b4)  
[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CE(18卷版)第2卷第7话"小偷紳士登場"第5页；  

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou1_B4_thumb.jpg) 
![](img/ce_02_005_thumb.jpg)  

细节：   

- A的底部比B的底部显示的范围更多。  

- A(下图左一)的头发的纯黑区域能看到不规则的反光。B（下图左二）则没有该效果。    
![](img/hukuseigenkou1_B4_crop0.jpg) 
![](img/ce_02_005_crop0.jpg)   

- A(下图左一)能看到墨色覆盖（我猜，先画脸部的线条，它随后被头发的浅墨色覆盖）。B（下图左二）则没有该效果。   
![](img/hukuseigenkou1_B4_crop1.jpg) 
![](img/ce_02_005_crop1.jpg)   

- 对于网点阴影，，A(下图左一列)的效果比B（下图左二列）细腻。  
![](img/hukuseigenkou1_B4_crop9.jpg) ![](img/ce_02_005_crop9.jpg)  

- 对于排线阴影，A(下图左一列)的效果比B（下图左二列）细腻很多。    
![](img/hukuseigenkou1_B4_crop2.jpg) ![](img/ce_02_005_crop2.jpg)  
![](img/hukuseigenkou1_B4_crop3.jpg) ![](img/ce_02_005_crop3.jpg)  
![](img/hukuseigenkou1_B4_crop4.jpg) ![](img/ce_02_005_crop4.jpg)  
![](img/hukuseigenkou1_B4_crop6.jpg) ![](img/ce_02_005_crop6.jpg)  
![](img/hukuseigenkou1_B4_crop5.jpg) ![](img/ce_02_005_crop5.jpg)  
![](img/hukuseigenkou1_B4_crop7.jpg) ![](img/ce_02_005_crop7.jpg)  
![](img/hukuseigenkou1_B4_crop8.jpg) ![](img/ce_02_005_crop8.jpg)  
    注：  
    1. 由以上对比发现，原画里的排线阴影、网点阴影在当时(1981年)的印刷技术下均有失真。  
    2. 由以上对比发现，排线阴影的失真更大。由此一个猜测是：最初漫画制作时使用网点阴影，可能因为排线阴影在当时的印刷技术下严重失真、无法令人满意。      

- A(下图左一)中上部有手写文字“Cat's❤Eye”字样，这在降低图片gamma值(下图左二)后更容易看出。    
![](img/hukuseigenkou1_B4_crop11.jpg) 
![](img/hukuseigenkou1_B4_crop11_gamma.jpg)  

- 眼部的画法：  
![](img/hukuseigenkou1_B4_crop10.jpg)  


---  

<a name="1981-09-i2"></a>  
## CE 官网报道中的图片03 (1981.09)  
![](img/cat_s-eye_tenji_96_10_thumb.jpg)  
Cat's Eye 完全版第1卷第7話"紳士大盜登場"第7页。  
Cat's Eye 18卷版第2卷第7话"小偷紳士登場"第7页。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 18卷版第2卷第7话"小偷紳士登場"第7页；  

A(下图左一)，B(下图左二)：  
![](img/cat_s-eye_tenji_96_10_thumb.jpg) 
![](img/ce_02_007_thumb.jpg)  

细节：   

- A(下图左一)中纯黑色区域的墨色不均匀.对比B（下图左二）：  
![](img/cat_s-eye_tenji_96_10_crop0.jpg) 
![](img/ce_02_007_crop0.jpg)  

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1981-11-i1"></a>  
## CE 原画（1981.11.03）   
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVivbkUYAEUefo.jpg"/> 
](https://pbs.twimg.com/media/FiVivbkUYAEUefo?format=jpg&name=large)  
Cat's Eye 18卷版第2卷“父亲的画像”137页。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第2卷“父亲的画像”137页；  
C：[现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595790290543341568)  

A(下图左一)，B(下图左二),C(下图左三、左四)：  
![](img/not_given.jpg) 
![](img/ce_02_137_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVivbkUYAEUefo.jpg"/> 
](https://pbs.twimg.com/media/FiVivbkUYAEUefo?format=jpg&name=large) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVivb-VUAA-O9P.jpg"/> 
](https://pbs.twimg.com/media/FiVivb-VUAA-O9P?format=jpg&name=large) 

细节：   

- C中的评论：  
"かつての溜まり場だった『獏』と『Poem』の店名が見える袋。  
九産大芸学棟近くの獏はまだあるのだろうか。  
Poem はなくなって久しい。「喫茶キャッツアイ」の店内什器やテーブルは Poem を模した物になっている。”  
（可以看到印有以前挂牌的『獏』和『Poem』名称的袋子。  
九州産業大学艺术学部楼附近的獏还在吗？  
Poem早已消失了。「猫眼咖啡屋」里的装置和桌子都是仿照Poem的模式。）  
注："九産大芸学棟"可能指"九州産業大学 芸術学部棟"。     

- 袋子上的商标。B(下图左一列),C(下图左二列)。  
![](img/ce_02_137_crop0.jpg) 
![](img/FiVivb-VUAA-O9P_crop0.jpg)  
![](img/ce_02_137_crop1.jpg) 
![](img/FiVivb-VUAA-O9P_crop1.jpg) 



---  

<a name="1981-11-i2"></a>  
## CE 原画  （1981.11）
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM_4SqaAAAw2QD.jpg"/> 
](https://pbs.twimg.com/media/FTM_4SqaAAAw2QD?format=jpg&name=large)  
Cat's Eye 18卷版第2卷157页。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第2卷164页。  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527634946239889408)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_02_157_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM_4SqaAAAw2QD.jpg"/> 
](https://pbs.twimg.com/media/FTM_4SqaAAAw2QD?format=jpg&name=large) 

细节：  

- C(下图左二)中衣服褶皱逼真。对比B(下图左一)。  
![](img/ce_02_157_crop0.jpg) 
![](img/FTM_4SqaAAAw2QD_crop0.jpg) 

- 图中画像的对比。B(下图左一)，C(下图左二)。    
![](img/ce_02_157_crop1.jpg) 
![](img/FTM_4SqaAAAw2QD_crop1.jpg) 


---  

<a name="1981-11-i3"></a>  
## CE 原画  （约1981.11）
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNAs8ZaUAAwy59.jpg"/> 
](https://pbs.twimg.com/media/FTNAs8ZaUAAwy59?format=jpg&name=large)  
Cat's Eye 18卷版第2卷164页。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第2卷164页。  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527635847772016640) 

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_02_164_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNAs8ZaUAAwy59.jpg"/> 
](https://pbs.twimg.com/media/FTNAs8ZaUAAwy59?format=jpg&name=large) 

细节：  

- C中可看到网点纸覆盖拟声词。    
![](img/FTNAs8ZaUAAwy59_crop0.jpg) 

- C(下图左一)中可看到白色颜料块，这在降低gamma后(下图左二)更容易看出。   
![](img/FTNAs8ZaUAAwy59_crop0.jpg) ![](img/FTNAs8ZaUAAwy59_crop0_gamma.jpg) 

- C(下图左二)中头发花白的画法。对比B(下图左一)。    
![](img/ce_02_164_crop1.jpg) 
![](img/FTNAs8ZaUAAwy59_crop1.jpg)  

- 线条粗细不均匀。  
![](img/FTNAs8ZaUAAwy59_crop2.jpg)  


---  

<a name="1982-07-i1"></a>  
## 「Cat's♥Eye」 版画6 (1982.07)
![](img/hanga_cat06_thumb.jpg)  
Cat's Eye 18卷版第2卷封面。  

[商品链接](https://edition-88.com/products/catseye-hanga6)及描述：  
"Jump Comics 1982年第2卷封面的版画。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  
技法：giclée  
版画用中性紙"  

[商品链接 Cat's Eye, Art Print #6](https://edition88.com/products/catseye-hanga6)及描述：  
"国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）精致地表现色彩和纹理。   
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  
国际版在美术纸上采用混合媒体（Giclée和UV）"   

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第2卷封面；  
C: [现场实拍 - Twitter](https://twitter.com/MituruSouda/status/1527972554526568448) 

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/hanga_cat06_thumb.jpg) 
![](img/ce_02_000_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTRy0WnacAAwrRI.jpg"/> 
](https://pbs.twimg.com/media/FTRy0WnacAAwrRI?format=jpg&name=4096x4096) 

细节：   

- A(下图左一)比B（下图左二）显示范围更大。B的颜色更泛红。  
![](img/hanga_cat06_thumb.jpg) 
![](img/ce_02_000_thumb.jpg)  

- A的猫眼卡上白色字迹似乎有凸起的效果：    
![](img/hanga_cat06_crop0.jpg) ![](img/hanga_cat06_crop1.jpg)   

- A(下图左一)比B（下图左二）的细节对比：  
![](img/hanga_cat06_crop2.jpg) ![](img/ce_02_000_crop2.jpg)   
![](img/hanga_cat06_crop3.jpg) ![](img/ce_02_000_crop3.jpg)   
![](img/hanga_cat06_crop4.jpg) ![](img/ce_02_000_crop4.jpg)   

- A(下图左一)展示的两张局部图有差异：局部图一（下图左二）比局部图二（下图左三）的每一个星光中心少加了亮点。    
![](img/hanga_cat06_crop5.jpg) ![](img/hanga_cat06_03_crop5.jpg) ![](img/hanga_cat06_04_crop5.jpg)     

- A画面凹凸不平的效果，可能与纸张有关。  
![](img/hanga_cat06_crop6.jpg)  

- 眼部的画法。虹膜画为蓝色。  
![](img/hanga_cat06_crop7.jpg)  


---  

<a name="1982-i2"></a>  
## CE 原画   (1982)  
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB_crop2_thumb.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096)  
Cat's Eye 18卷版第3卷封面。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B1: 常见来源：Cat's Eye 18卷版第3卷封面,  
B2: 常见来源：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/hojo_official/status/1525002979723517952)  

A(下图左一)，B(下图左二、左三), C(下图左四、左五)：  
![](img/not_given.jpg) 
![](img/thumb/ce_03_000_crop0_thumb.jpg) 
![](img/thumb/illustrations_20th_054_crop0_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB_crop2_thumb.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096) 

细节：  

- 虹膜为蓝色。  

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1982-i3"></a>  
## CE 原画 (1982)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB_crop1_thumb.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096)  
Cat's Eye 18卷版第3卷封面书脊。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 常见来源：第3卷封面书脊。    
C: [现场实拍 - Twitter](https://twitter.com/hojo_official/status/1525002979723517952)  

A(下图左一)，B(下图左二), C(下图左三、左四)：  
![](img/not_given.jpg) 
![](img/thumb/ce_03_000_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB_crop1_thumb.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096) 

细节：  

- 配文:  
![](img/raw_rep.jpg) 
大图: 
[原图](https://pbs.twimg.com/media/FSwYtOwaAAArAAw?format=jpg&name=large)，
[原网页](https://twitter.com/hyperDora/status/1525621623302541312)，
[备份图](http://www.hojocn.com/bbs/attachments/20220520_f01f823e7149cd2f1028dKhB0cBBT30r.jpg) ，
[备份网页](http://www.hojocn.com/bbs/viewthread.php?tid=96791)  
    - **OCR**（因图片不够清晰，所以OCR可能有误）:  
        カラー原稿での様々な表現について  
        [北条先生]  
        前と同じものは描きたくないので、常に新しい表現は摸索していましに。
        当時は広告などで西海岸の文化がきていて、ヤシの木と青空なども
        綺麗だなと思い描いていました。
        最初は透明水彩だったのですが、エフープラシを使い始めて、カラー
        インクに移行していきました。たまにカラートーンなども使っていま
        したね。
        こういう風に描けるかなあ...と思いながら試行錯誤して描いていました。
    - **譯文**：  
        关于彩色原稿中的各种表现形式  
        [北条先生]  
        我不想画和以前一样的东西，所以我一直在寻找新的表现形式。  
        当时，西海岸的文化正通过广告传来，我认为棕榈树和蓝天很美。（译注：待校对）      
        起初是透明水彩，但后来我开始使用エフープラシ，并转而使用Color Ink。 有时我也会使用色调等等。  
        我想知道我是否可以把它画成这样...我是通过试错来画画。   

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="unknown-i1"></a>  
## CE 插画  (作品创作年月未知)  

[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FipPU5NagAAo4d_.jpg"/> 
](https://pbs.twimg.com/media/FipPU5NagAAo4d_?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 展品的官方照片无；    
B: 常见处出：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)；  
C: [现场实拍 - Twitter](https://twitter.com/33Aquamarine3/status/1597176242033549313)  ;

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/20th_anni_054_thumb.jpg) 
![](img/thumb/FipPU5NagAAo4d_.jpg) 

细节：   

- 从人物造型来看，应为猫眼早期的作品，但具体创作时间未知。  

- 头发的高光为蓝色，这比较特别。头发的着色有4层：白色、浅蓝色、深蓝色、黑色。  
![](img/FipPU5NagAAo4d_crop0.jpg)  

- 眼睛虹膜为蓝色。瞳孔为深蓝色。眉毛的高光为蓝色。这比较特别。     
![](img/FipPU5NagAAo4d_crop2.jpg)  

- C的头发的高光处(下图左一)。当降低gamma值(下图左二)后，其颜色不突兀。我猜，有两种可能：1)头发的白色高光是用白色颜料画的，但该白色颜料在降低gamma之后不显得突兀；2)头发的白色高光不是用白色颜料画的，而是白纸的颜色（即留白）。        
![](img/FipPU5NagAAo4d_crop0.jpg) ![](img/FipPU5NagAAo4d_crop0_gamma.jpg)  

- C的头发、花饰、衣服部位(下图左一列)。当降低gamma值(下图左二列)后，其呈荧光，不知道这能说明什么？[疑问]    
![](img/FipPU5NagAAo4d_crop0.jpg) ![](img/FipPU5NagAAo4d_crop0_gamma.jpg)  
![](img/FipPU5NagAAo4d_crop1.jpg) ![](img/FipPU5NagAAo4d_crop1_gamma.jpg)  



---  

<a name="unknown-i2"></a>  
## CE 插画  (作品创作年月未知)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBCMnSaIAAoBjD.jpg"/> 
](https://pbs.twimg.com/media/FTBCMnSaIAAoBjD?format=jpg&name=large) 

为简便，采用如下符号简记：  
A: 展品的官方照片无；    
B: 常见处出：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)；  
C: [现场实拍 - Twitter](https://twitter.com/megenna_968/status/1526793131857166336)  ;  
D: [现场实拍 - Weibo](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)  

A(下图左一)，B(下图左二),C(下图左三), D(下图左四)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBCMnSaIAAoBjD.jpg"/> 
](https://pbs.twimg.com/media/FTBCMnSaIAAoBjD?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2821uemwbj20ku0dwjrw.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821uemwbj20ku0dwjrw.jpg)  

细节：   

- 从人物造型来看，应为猫眼早期的作品，但具体创作时间未知。  

- C中能看到纸张的横向纹理。  
![](img/FTBCMnSaIAAoBjD_crop0.jpg) 
![](img/FTBCMnSaIAAoBjD_crop1.jpg) 

- C中面部细节。  
![](img/FTBCMnSaIAAoBjD_crop2.jpg)  
![](img/FTBCMnSaIAAoBjD_crop3.jpg)  

- 头发的着色有4层：白色、浅蓝色、深蓝色、黑色。  
![](img/FTBCMnSaIAAoBjD_crop6.jpg)  

- A的头发的高光处(下图左一)。当降低gamma值(下图左二)后，其颜色不突兀。我猜，有两种可能：1)头发的白色高光是用白色颜料画的，但该白色颜料在降低gamma之后不显得突兀；2)头发的白色高光不是用白色颜料画的，而是白纸的颜色（即留白）。        
![](img/FTBCMnSaIAAoBjD_crop6.jpg) 
![](img/FTBCMnSaIAAoBjD_crop6_gamma.jpg) 

- C中衣服褶皱、泳池的水，从远处看的话，效果逼真。    
![](img/FTBCMnSaIAAoBjD_crop8.jpg) 
![](img/FTBCMnSaIAAoBjD_crop4.jpg) 
![](img/FTBCMnSaIAAoBjD_crop7.jpg) 

- 叶子高光的笔触。当降低gamma值(下图左二)后，其颜色不突兀。这种笔触不太可能是留白。所以，我猜，白色高光是用白色颜料画的，但该白色颜料在降低gamma之后不显得突兀。  
![](img/FTBCMnSaIAAoBjD_crop5.jpg) 
![](img/FTBCMnSaIAAoBjD_crop5_gamma.jpg)  


---  

<a name="unknown-i3"></a>  
## CE 插画  (作品创作年月未知)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/illustration_47_crop0.jpg"/> 
](https://pbs.twimg.com/media/?format=jpg&name=large) 

为简便，采用如下符号简记：  
A: 展品的官方照片无；  
B: 常见处出：北条司イラスト集；  
C: [现场实拍 - Twitter](https://twitter.com/AoiTakarabako/status/1637937678632255489);  

A(下图左一)，B(下图左二),C(下图左三)：  
![](img/not_given.jpg) 
![](img/thumb/illustration_47_crop0.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Frsfoq8agAEnLe5.jpg"/> 
](https://pbs.twimg.com/media/Frsfoq8agAEnLe5?format=jpg&name=large) 

细节：  

无法看到足够的细节，因为C图的分辨率太小、B图的分辨率更小。  


---  

<a name="1982-04-i1"></a>  
## CE 插画  (1982.04)
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTShHhTUYAA14zh.jpg"/> 
](https://pbs.twimg.com/media/FTShHhTUYAA14zh?format=jpg&name=4096x4096)  
Cat's Eye 18卷版第5卷116-117页（黑白）。  

为简便，采用如下符号简记：  
A: 展品的官方照片无；    
B: 常见处出：18卷版第5卷116-117页（黑白）；  
C: [现场实拍 - Twitter](https://twitter.com/thelowtierchara/status/1528027698114834434)  
D: [现场实拍 - Twitter](https://twitter.com/Draichi_/status/1528343076758237184)  

A(下图左一)，B(下图左二), C(下图左三),D(下图左四)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTShHhTUYAA14zh.jpg"/> 
](https://pbs.twimg.com/media/FTShHhTUYAA14zh?format=jpg&name=4096x4096) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTXD5KDaQAEmlLe.jpg"/> 
](https://pbs.twimg.com/media/FTXD5KDaQAEmlLe?format=jpg&name=large) 

细节：  

- D中的细节。线条不平滑(下图左一)、红白纹理覆盖在排线阴影之上(下图左二)、排线阴影(下图左三)。    
![](img/FTXD5KDaQAEmlLe_crop0.jpg) 
![](img/FTXD5KDaQAEmlLe_crop1.jpg) 
![](img/FTXD5KDaQAEmlLe_crop2.jpg) 

- 腿部的高光的笔触。 对比C(下图左一)、D(下图左二)。  
![](img/FTShHhTUYAA14zh_crop3.jpg) 
![](img/FTXD5KDaQAEmlLe_crop3.jpg) 



---  

<a name="1982-05-i1"></a>  
## CE 複製原稿  （1982.05）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM-9nQakAEHCOa.jpg"/> 
](https://pbs.twimg.com/media/FTM-9nQakAEHCOa?format=jpg&name=large)  
Cat's Eye 18卷版第5卷136-137页。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第5卷136-137页；  
C：[现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527633943000121344)  

A(下图左一)，B(下图左二),C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_05_136_137_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM-9nQakAEHCOa.jpg"/> 
](https://pbs.twimg.com/media/FTM-9nQakAEHCOa?format=jpg&name=large) 

细节：   

- B(下图左一)中有文字，C(下图左二)中没有文字：  
![](img/ce_05_136_137_crop0.jpg) 
![](img/FTM-9nQakAEHCOa_crop0.jpg)  

- B(下图左一)中线条断续，C(下图左二)中效果更好：  
![](img/ce_05_136_137_crop1.jpg) 
![](img/FTM-9nQakAEHCOa_crop1.jpg)  

- C中有白色颜料涂抹的痕迹。  
![](img/FTM-9nQakAEHCOa_crop2.jpg) 
![](img/FTM-9nQakAEHCOa_crop3.jpg)  


---  

<a name="1982-0607-i1"></a>  
## CE 插画（约1982.06或07）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2dub6qu11j30ir0p0q5n.jpg"/> 
](https://wx4.sinaimg.cn/large/008rpAF8gy1h2dub6qu11j30ir0p0q5n.jpg)  
Cat's Eye 18卷版第6卷封面。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第6卷封面。  
C: [官方blog - Twitter](https://twitter.com/cityhunter100t/status/1526384130438508544)，  
D: [官方blog - weibo](https://m.weibo.cn/detail/4770890476422470)，  
E: [官方blog - weibo](https://m.weibo.cn/detail/4840011021422113)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四), E(下图左五)：  
![](img/not_given.jpg) 
![](img/ce_06_000_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2dub6qu11j30ir0p0q5n.jpg"/> 
](https://wx4.sinaimg.cn/large/008rpAF8gy1h2dub6qu11j30ir0p0q5n.jpg) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2dub736g7j30ir0p0adb.jpg"/> 
](https://wx3.sinaimg.cn/large/008rpAF8gy1h2dub736g7j30ir0p0adb.jpg) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2dub7f3klj30ir0p0gqk.jpg"/> 
](https://wx2.sinaimg.cn/large/008rpAF8gy1h2dub7f3klj30ir0p0gqk.jpg) 

细节：  

- C中的评论：  
为了挑战与以往不同的画法。小爱是在透明片材上绘制，背景是水彩画，前面的建筑物是剪纸。非常有立体感。顺便一提，封面中的粉红色背景不在原画中，并不是因为它被剥除掉了。其实是作为单独的素材提交，在印刷编辑阶段合成的~  

- C中人物背景泛黄。  
![](img/008rpAF8gy1h2dub6qu11j30ir0p0q5n_crop1.jpg) 

- C中背景事物逼真。   
![](img/008rpAF8gy1h2dub6qu11j30ir0p0q5n_crop0.jpg) 

- 对比B(下图左一)、 C(下图左二、左三)。  
![](img/ce_06_000_crop2.jpg) 
![](img/008rpAF8gy1h2dub6qu11j30ir0p0q5n_crop2.jpg) 
![](img/008rpAF8gy1h2dub736g7j30ir0p0adb_crop2.jpg) 


---  

<a name="1982-07-i1"></a>  
## CE 複製原稿2 (B4) /Cat's♥Eye  (1982.07)  
![](img/hukuseigenkou2_B4_thumb.jpg)  
完全版第5卷第47話"追擊獨家新聞"第？页。  
18卷版第6卷第47话”追擊獨家新聞“第143页。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript2-b4)及描述：  
“「完全版第5卷第47话 追擊獨家新聞! 」的复制原稿。  
技术：胶印  
材料 纸：高档纸”  

[英文版商品链接Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: 18卷版第6卷第47话”追擊獨家新聞“第143页；  
C: [现场实拍 - Twitter](https://twitter.com/MituruSouda/status/1527972554526568448)  

A(下图左一)，B(下图左二)， C(下图左三)：  
![](img/hukuseigenkou2_B4_thumb.jpg) 
![](img/ce_06_0071_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTRy1uSacAEXAy2.jpg"/> 
](https://pbs.twimg.com/media/FTRy1uSacAEXAy2?format=jpg&name=large) 

细节：   

- A比B的显示范围大。  

- A(下图左一)比B（下图左二）显示更多的细节：  
    - A的头发黑色区域不均匀。  
    ![](img/hukuseigenkou2_B4_crop0.jpg) 
    ![](img/ce_06_0071_crop0.jpg)  
    - 黑色网点纸覆盖了头发边缘的部分区域。  
    ![](img/hukuseigenkou2_B4_crop0.jpg) 
    ![](img/ce_06_0071_crop0.jpg)  
    - A中表示动作的白线隐约透出下层的头发的黑色。    
    ![](img/hukuseigenkou2_B4_crop1.jpg) 
    ![](img/ce_06_0071_crop0.jpg)  

- A第二个panel的上边框处白色痕迹。     
    ![](img/hukuseigenkou2_B4_crop2.jpg)   

- A(下图左一列)的头发的细节，与B(下图左二列)的对比：     
    ![](img/hukuseigenkou2_B4_crop3.jpg) ![](img/ce_06_0071_crop3.jpg)  
    ![](img/hukuseigenkou2_B4_crop4.jpg) ![](img/ce_06_0071_crop3.jpg)  
- A(下图左一)的地面上的网点阴影的渐变更均匀：     
    ![](img/hukuseigenkou2_B4_crop5.jpg) ![](img/ce_06_0071_crop5.jpg)  
- A(下图左一列)能清晰地看出阴影的排线，与B(下图左二列)的对比：    
    ![](img/hukuseigenkou2_B4_crop6.jpg) ![](img/ce_06_0071_crop6.jpg)  
    ![](img/hukuseigenkou2_B4_crop7.jpg) ![](img/ce_06_0071_crop7.jpg)  
- A(下图左一列)的阴影细节更丰富。A中最下面的panel中的大面积阴影可能是用喷枪。    
    ![](img/hukuseigenkou2_B4_crop8.jpg) ![](img/ce_06_0071_crop8.jpg)  

- 眼部的画法：  
    ![](img/hukuseigenkou2_B4_crop9.jpg) 
    ![](img/hukuseigenkou2_B4_crop10.jpg)  





---  

<a name="1982-07-i2"></a>  
## CE 插画  （约1982.07）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTSia82VsAAikT1.jpg"/> 
](https://pbs.twimg.com/media/FTSia82VsAAikT1?format=jpg&name=4096x4096)  
Cat's Eye 18卷版7卷"最后一击"第26-27页。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第7卷26-27页。  
C: [现场实拍 - Twitter](https://twitter.com/thelowtierchara/status/1528027705265950720)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_07_26_27_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTSia82VsAAikT1.jpg"/> 
](https://pbs.twimg.com/media/FTSia82VsAAikT1?format=jpg&name=4096x4096) 

细节：  

- B(下图左一列)对比C(下图左二列)。  
![](img/ce_07_26_27_crop0.jpg) ![](img/FTSia82VsAAikT1_crop0.jpg)  
![](img/ce_07_26_27_crop1.jpg) ![](img/FTSia82VsAAikT1_crop1.jpg)  
![](img/ce_07_26_27_crop2.jpg) ![](img/FTSia82VsAAikT1_crop2.jpg)  
![](img/ce_07_26_27_crop4.jpg) ![](img/FTSia82VsAAikT1_crop4.jpg)  
![](img/ce_07_26_27_crop5.jpg) ![](img/FTSia82VsAAikT1_crop5.jpg)  
![](img/ce_07_26_27_crop3.jpg) ![](img/FTSia82VsAAikT1_crop3.jpg)  



---  

<a name="1982-09-i1"></a>  
## CE 复制原稿（1982.09）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM-9qkaIAIAYwd.jpg"/> 
](https://pbs.twimg.com/media/FTM-9qkaIAIAYwd?format=jpg&name=large)  
Cat's Eye 18卷版第7卷“渐灭的灵魂”156-157页。

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第7卷26-27页。  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527633943000121344)  
D: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527634946239889408)  

A(下图左一)，B(下图左二), C(下图左三)， D(下图左四)：  
![](img/not_given.jpg) 
![](img/ce_07_156_157_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM-9qkaIAIAYwd.jpg"/> 
](https://pbs.twimg.com/media/FTM-9qkaIAIAYwd?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM_4VUaUAAX5kj.jpg"/> 
](https://pbs.twimg.com/media/FTM_4VUaUAAX5kj?format=jpg&name=large) 

细节：  

- C中有多处发黄的痕迹。  
![](img/FTM-9qkaIAIAYwd_crop0.jpg) 
![](img/FTM-9qkaIAIAYwd_crop1.jpg) 
![](img/FTM-9qkaIAIAYwd_crop2.jpg) 

- 沙发的光泽逼真。  
![](img/FTM-9qkaIAIAYwd_crop4.jpg) 

- C中对话框内文字像是粘贴去的。  
![](img/FTM-9qkaIAIAYwd_crop3.jpg) 

- C中(下图左一)、D中(下图左二至六)的一些细节。下图人物边缘有阴影，如红箭头所示。我猜，这几个人物是粘贴上的。    
![](img/FTM-9qkaIAIAYwd_crop5.jpg) 
![](img/FTM_4VUaUAAX5kj_crop5.jpg) 
![](img/FTM_4VUaUAAX5kj_crop6.jpg) 
![](img/FTM_4VUaUAAX5kj_crop7.jpg) 
![](img/FTM_4VUaUAAX5kj_crop8.jpg) 

- D中(下图左一)顶灯的光线是白色颜料画的，这在降低gamma值后(下图左二)更容易看出。  
![](img/FTM_4VUaUAAX5kj_crop9.jpg) ![](img/FTM_4VUaUAAX5kj_crop9_gamma.jpg) 



---  

<a name="1982-11-i1"></a>  
## 「Cat's♥Eye」 版画8 (1982.11)   
![](img/hanga_cat08_thumb.jpg)  

[商品链接 Art Print #8](https://edition-88.com/products/catseye-hanga8)及描述：  
"周刊少年Jump 1983年第1、2期合并号/封面插图的版画。   
Cat'sEye的版画使用白色颜料，每一张都有手工上色。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  
来自版画工房的评论:      
背景的深蓝色和滑雪服的鲜艳色彩之间的对比是如此美丽，以至于花了大部分时间来再现该区域的色彩。白雪部分是用白颜料手绘的，以表现雪在蓝天下闪闪发光的样子。  
技法：giclée  
版画用中性紙"  

[英文版商品链接 Art Print #8](https://edition88.com/products/catseye-hanga8)及描述：  
"国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）精致地表现色彩和纹理。  
国际版在美术纸上采用混合媒体（Giclée和UV）"  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文原版)中的该图片；  
C：[现场实拍 - Twitter](https://twitter.com/TOUYA561/status/1595396581728997376)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/hanga_cat08_thumb.jpg) 
![](img/20th_anni_illustrations_053_thumb.jpg) 
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FiP8u7iVQAADoKQ.jpg"/> 
](https://pbs.twimg.com/media/FiP8u7iVQAADoKQ?format=jpg&name=medium) 

细节：  

- A比B左侧显示范围大、上部显示范围小很多。  

- A(下图左一)与B（下图左二）的细节对比：  
![](img/hanga_cat08_crop0.jpg) ![](img/20th_anni_illustrations_053_crop0.jpg)   
![](img/hanga_cat08_crop4.jpg) ![](img/20th_anni_illustrations_053_crop4.jpg)   

- A(下图左一)与B（下图左二）的细节对比。A画面凹凸不平的效果，可能与纸张有关。  
![](img/hanga_cat08_crop1.jpg) ![](img/20th_anni_illustrations_053_crop1.jpg)   
![](img/hanga_cat08_crop5.jpg) ![](img/20th_anni_illustrations_053_crop5.jpg)   

- A展示的两张局部图有差异：雪片处，下图左二比下图左一增加了颜料。    
![](img/hanga_cat08_crop2.jpg) ![](img/hanga_cat08_03_crop2.jpg)  

- 眼部的画法。泪（下图左一）、爱（下图左二）瞳、（下图左三）。泪的虹膜画为蓝色。    
![](img/hanga_cat08_crop8.jpg) 
![](img/hanga_cat08_crop7.jpg) 
![](img/hanga_cat08_crop6.jpg) 

- 从远处看，A中右上部蓝色背景（下图左一）有不均匀蓝色块。因为不太明显，所以用红色标记出（下图左二）。  
![](img/hanga_cat08_crop9.jpg) 
![](img/hanga_cat08_crop9_mark.jpg)  

- 名古屋展时，新增了局部图。降低gamma值后，可以看到有些雪片上涂了厚厚的白颜料。    
![](img/hanga_cat08_08_crop0.jpg) 
![](img/hanga_cat08_08_crop1.jpg) 
![](img/hanga_cat08_08_crop2.jpg) 
![](img/hanga_cat08_08_crop3.jpg) 

- A中的图片颜色鲜艳。  
![](img/hanga_cat08_08_crop4.jpg) 
![](img/hanga_cat08_08_crop6.jpg) 

- A中图片按实际分辨率所显示的细节。  
![](img/hanga_cat08_08_crop5.jpg) 
![](img/hanga_cat08_08_crop6.jpg) 
![](img/hanga_cat08_08_crop7.jpg) 


---  

<a name="1983-02-i1"></a>  
## CE 官网报道中的图片05 (约1983.02)

![](img/cat_s-eye_tenji_96_12_thumb.jpg)  
18卷版第9卷“雪女传说”172页。  

“雪女传说”的扉页没有时间戳。其前几话的时间戳为“83JAN”，所以推测该画创作于1983年2月前后。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 18卷版第9卷第172页；  
C: [现场实拍 - Twitter](https://twitter.com/MituruSouda/status/1528395345436495873)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/cat_s-eye_tenji_96_12_thumb.jpg) 
![](img/ce_09_172_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTXzdzwaMAAA-yK.jpg"/> 
](https://pbs.twimg.com/media/FTXzdzwaMAAA-yK?format=jpg&name=large) 

细节：   

- A(下图左一)的很多雪花是用白色颜料画的，这在降低gamma值(下图左二)后更容易看出。    
![](img/cat_s-eye_tenji_96_12_crop0.jpg) 
![](img/cat_s-eye_tenji_96_12_crop0_gamma.jpg)  

- 降落伞的褶皱逼真。A(下图左一列)，B(下图左二列), C(下图左三列)。    
![](img/cat_s-eye_tenji_96_12_crop1.jpg) 
![](img/ce_09_172_crop1.jpg) 
![](img/FTXzdzwaMAAA-yK_crop1.jpg)  
![](img/cat_s-eye_tenji_96_12_crop2.jpg) 
![](img/ce_09_172_crop2.jpg) 
![](img/FTXzdzwaMAAA-yK_crop2.jpg)  

- 原画（A、C）的阴影更细腻。A(下图左一列)，B(下图左二列), C(下图左三列)。      
![](img/cat_s-eye_tenji_96_12_crop3.jpg) 
![](img/ce_09_172_crop3.jpg) 
![](img/FTXzdzwaMAAA-yK_crop3.jpg)  

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  



---  

<a name="1983-03-i1"></a>  
## CH 官网报道中的图片15 (1983.03)

![](img/cityhunter_tenji_96_10_thumb.jpg)  
City Hunter -XYZ-第1页。    
这个故事最后一页有时间戳“83MAR”。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: CITYHUNTER-完全版-Z；  

A(下图左一)，B（下图左二）：  
![](img/cityhunter_tenji_96_10_thumb.jpg) 
![](img/chce_z_009_thumb.jpg)  

细节：   

- A中上部有旧纸张遮盖的痕迹。  
![](img/cityhunter_tenji_96_10_crop0.jpg)  

- A(下图左一列)中枪支的着色。对比B（下图左二列）。  
![](img/cityhunter_tenji_96_10_crop1.jpg) 
![](img/chce_z_009_crop1.jpg)  
![](img/cityhunter_tenji_96_10_crop2.jpg) 
![](img/chce_z_009_crop2.jpg) 
 
- 有些原画旁边附有相同但缩小的页面。我猜，这是因为该原画上无对白。为了让观众了解剧情，所以在旁边附上了印刷出版时的该页面，上面有对白文字。  
![](img/FSncDfXVUAAVQ4h_crop0.jpg)   

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1983-03-i2"></a>  
## CH 複製原稿1 (B4) /CityHunter (1983.03)  
![](img/hukuseigenkou_CH_1_B4-01-eg_thumb.jpg)  
City Hunter -XYZ- 第3页，   
CITYHUNTER-完全版-Z(日文版)第11页。  
本故事末页有时间戳：'83 MAR。  

[日文版商品链接 无]()  
[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4)及描述：  
"City Hunter XYZ漫画手稿以高清印刷方式复制，看起来与原作完全一样。  
不均匀的黑色油漆，使用白色修正笔的部分，以及蓝色的指示和起草线，这些在漫画中看不到的，都可以清楚地看到。  
这本漫画手稿是一个收藏品，可以让你享受City Hunter的世界。  
技术: 按需印刷  
材料: Fine paper  
无框/无签名/无版号"  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CITYHUNTER-完全版-Z(日文版)第11页    
C: [现场实拍 - Twitter](https://twitter.com/N7Obo2E2S3a3Xw4/status/1525415306138595329) 
D: [现场实拍 - Twitter](https://twitter.com/megenna_968/status/1526801666238201857) 

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![](img/hukuseigenkou_CH_1_B4-01-eg_thumb.jpg) 
![](img/CH-Z_011_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FStdHprUEAAFmsR.jpg"/> 
](https://pbs.twimg.com/media/FStdHprUEAAFmsR?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBJ_SPUAAAEzWL.jpg"/> 
](https://pbs.twimg.com/media/FTBJ_SPUAAAEzWL?format=jpg&name=large) 

细节：  

- A(下图左一)枪口处加了网点阴影，似乎还有白色涂层。     
![](img/hukuseigenkou_CH_1_B4-01_crop1.jpg) 
![](img/CH-Z_011_crop1.jpg)   

- A(下图左一列)射击时枪口涂了白颜料。与B(下图左二列)的对比：     
![](img/hukuseigenkou_CH_1_B4-01_crop15.jpg) 
![](img/CH-Z_011_crop15.jpg)   
![](img/hukuseigenkou_CH_1_B4-01_crop16.jpg) 
![](img/CH-Z_011_crop16.jpg)   

- A(下图左一)与B(下图左二)的镜片高光的对比：  
![](img/hukuseigenkou_CH_1_B4-01_crop7.jpg) 
![](img/CH-Z_011_crop7.jpg)   

- A(下图左一)与B(下图左二)的着色的对比。A中黑线勾勒头发轮廓线和发丝，头发大面积黑色不均匀：  
![](img/hukuseigenkou_CH_1_B4-01_crop8.jpg) 
![](img/CH-Z_011_crop8.jpg)   

- A(下图左一列)与B(下图左二列)的排线阴影的对比。  
![](img/hukuseigenkou_CH_1_B4-01_crop9.jpg) 
![](img/CH-Z_011_crop9.jpg)   
![](img/hukuseigenkou_CH_1_B4-01_crop10.jpg) 
![](img/CH-Z_011_crop10.jpg)   

- A(下图左一)网点纸覆盖了黑色边线：    
![](img/hukuseigenkou_CH_1_B4-01_crop11.jpg) 
![](img/CH-Z_011_crop11.jpg)   

- A(下图左一)射击时枪口迸发火花的效果：  
![](img/hukuseigenkou_CH_1_B4-01_crop14.jpg) 
![](img/CH-Z_011_crop14.jpg)   

- 关于文字：  
    - A(下图左一)此处的文字可能是打印后贴上的,且字体和B(下图左二)不同。说明印刷前字体做了修改。    
    ![](img/hukuseigenkou_CH_1_B4-01_crop0.jpg) 
    ![](img/CH-Z_011_crop0.jpg)   

    - B(下图左二)中的这类文字是用毛笔写的(下图左一)。  
    ![](img/hukuseigenkou_CH_1_B4-01_crop2.jpg) 
    ![](img/CH-Z_011_crop2.jpg)   
    
    - 为了突出文字，将与背景交叉的文字边缘描白(下图红色箭头处能看到A(下图左一列)中描白的笔触)。B(下图左二列)为印刷后的效果：    
    ![](img/hukuseigenkou_CH_1_B4-01_crop3.jpg) 
    ![](img/CH-Z_011_crop3.jpg)  
    ![](img/hukuseigenkou_CH_1_B4-01_crop4.jpg) 
    ![](img/CH-Z_011_crop4.jpg)  
    
    - A(下图左一、左二)上部边沿处有手写文字。文字可能为“ブブ 覚のいかゆむ うきー?ブ て(?)ばす パインー 35?マグ----”('?'表示该位置的文字无法识别；'(?)'表示该位置前面的文字无法确认)[+]。  
    ![](img/hukuseigenkou_CH_1_B4-01_crop5.jpg) 
    ![](img/hukuseigenkou_CH_1_B4-01_crop6.jpg)  
    这些文字和B(下图)中的不一致。  
    ![](img/CH-Z_011_crop5.jpg)  
    [+] 感谢网友“知更鸟”帮忙识别这些手写日文。  
    
    - A(下图左一)某些拟声词(枪击)。蓝箭头所示的白色两侧是同一种网点阴影(红箭头)。推测红箭头所示的两块网点阴影是一次贴上去的，然后在蓝箭头处图上白色。      
    ![](img/hukuseigenkou_CH_1_B4-01_crop12.jpg) 
    ![](img/CH-Z_011_crop12.jpg)  
    
    - A(下图左一)背景与某些拟声词(枪击)、角色之间有白线间隔(红箭头)。拟声词与角色之间无白线间隔(蓝箭头)。   
    ![](img/hukuseigenkou_CH_1_B4-01_crop13.jpg)  
    
    - 因为没有对话框，所以A(下图左一)将文字写在旁边空白处。B(下图左二)为印刷后的效果。  
    ![](img/hukuseigenkou_CH_1_B4-01_crop17.jpg) 
    ![](img/CH-Z_011_crop17.jpg)  
    
- 有些原画旁边附有相同但缩小的页面。我猜，这是因为该原画上无对白。为了让观众了解剧情，所以在旁边附上了印刷出版时的该页面，上面有对白文字。  
![](img/FSncDfXVUAAVQ4h_crop1.jpg) 



---  

<a name="1983-03-i3"></a>  
## CH 原稿 (1983.03)  
[<img title="B备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVsdjLUcAAv0oz.jpg"/> 
](https://pbs.twimg.com/media/FiVsdjLUcAAv0oz?format=jpg&name=medium)   
City Hunter -XYZ- 第7页，   
CITYHUNTER-完全版-Z(日文版)第15页。  
本故事末页有时间戳：'83 MAR。  

为简便，采用如下符号简记：  
A: [官方blog - Twitter](https://twitter.com/cityhunter100t/status/1526044112490618881)  
B: [现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595800908910448640)   
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527633943000121344)  

A(下图左一)，B(下图左二), C(下图左三)：  
[<img title="A备份缩略图(点击查看原图)" height="100" src="img/thumb/FQ2rTI4VgAANP0j.jpg"/> 
](https://pbs.twimg.com/media/FQ2rTI4VgAANP0j?format=jpg&name=medium) 
[<img title="B备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVsdjLUcAAv0oz.jpg"/> 
](https://pbs.twimg.com/media/FiVsdjLUcAAv0oz?format=jpg&name=medium) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM-9-yacAA7HT7.jpg"/> 
](https://pbs.twimg.com/media/FTM-9-yacAA7HT7?format=jpg&name=large)  

细节：   

- A中可见很多黄色的胶带。  

- A中贴了半透明纸，其上有红色手写文字。A(下图左一)对比B(下图左二)。    
![](img/FQ2rTI4VgAANP0j_crop0.jpg) 
![](img/FiVsdjLUcAAv0oz_crop0.jpg)  

- C中(下图左一)显示了留言板的着色。其上的白色文字使用白色颜料写的，这在降低gamma值后(下图左二)容易看出。  
![](img/FTM-9-yacAA7HT7_crop1.jpg) ![](img/FTM-9-yacAA7HT7_crop1_gamma.jpg) 

- 可能是该展品的配文：  
![](img/Xiang_hair.jpg) 
大图: 
[原图](https://pbs.twimg.com/media/FS8Tjg2aMAA7Tsf?format=jpg&name=large)，
[原网页](https://twitter.com/yae_ch3/status/1526460307845312518)，
[备份图](http://www.hojocn.com/bbs/attachments/20220520_ac96c35056a5aa1eb185HcBgM9voGnJb.jpg) ，
[备份网页](http://www.hojocn.com/bbs/viewthread.php?tid=96791)  
    - **OCR**:  
        香の髪型  
        [北条先生]  
        ジャンプではヒロインは髪の毛が長くないと人気が出ないというジンクス  
        があったらしいです。  
        最初は連載ではなく読切でしたので、髪の毛を短くしてもいいだろうと  
        思い短くしました(笑)。  
        私は女性の髪形に特に好みが無いので、似合っていたら全て素敵だと  
        思います。  
    - **譯文**：  
        香的髪型  
        [北条先生]  
        我听说在Jump有一个魔咒，女主角必须有长发才能受欢迎。  
        因为作品最开始是短篇而不是连载，所以我觉得把她的头发留短也行吧，所以我就把它留短了（笑）。  
        我对女性的发型没有特别的偏好，所以如果我画起来合适，我觉得都是好的。（译注：待校对）  



---  

<a name="1983-03-i4"></a>  
## CH 複製原稿2 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_2_B4_thumb.jpg)  
City Hunter -XYZ- 第9页，   
CITYHUNTER-完全版-Z(日文版)第17页。  
本故事末页有时间戳：'83 MAR。  

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript2-b4)及描述：  
"「City Hunter −XYZ−」的复制原稿。  
技术：胶印  
材料 纸：高档纸"  

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4)及描述：  
"City Hunter XYZ漫画原稿以高清印刷方式复制，看起来与原作完全一样。  
在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  
这本漫画手稿是一个收藏品，可以让你享受城市猎人的世界。  
技术：按需印刷  
材料：Fine paper  
无框/无签名/无版号"   

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CITYHUNTER-完全版-Z(日文版)第17页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_2_B4_thumb.jpg) 
![](img/CH-Z_012_thumb.jpg)    

细节：  

- A(下图左一)用白色颜料画出蒸汽，对比B(下图左二)的效果。   
![](img/hukuseigenkou_CH_2_B4_crop0.jpg) 
![](img/CH-Z_012_crop0.jpg)    

- A(下图左一)杯柄的排线阴影，对比B(下图左二)的效果：  
![](img/hukuseigenkou_CH_2_B4_crop1.jpg) 
![](img/CH-Z_012_crop1.jpg)    

- A(下图左一列)杯子阴影的边缘有类似蓝色的线条。B(下图左二列)中的效果。    
![](img/hukuseigenkou_CH_2_B4_crop2.jpg) ![](img/CH-Z_012_crop2.jpg)  
![](img/hukuseigenkou_CH_2_B4_crop3.jpg) ![](img/CH-Z_012_crop2.jpg)  
![](img/hukuseigenkou_CH_2_B4_crop4.jpg) ![](img/CH-Z_012_crop4.jpg)  
<a name="BluePencil"></a>
这种蓝色的线条可能是用"动画用蓝色铅笔"画的。参考资料：  
    1. [Why do animators use blue pencil?](https://www.dependablepickup.com/why-do-animators-use-blue-pencil)：  
"Blue pencils were used by people in the design industry as well as the animation industry **because it was a tone that was visible to our eyes on roughs**, but when scanned in with corrections, was not visible by the scanner and so would make copies that didn’t have the ‘editing lines’ on it....Red and blue pencils have been traditionally used with copy machines because the blue pencil becomes invisible and the red pencil appears black. They’re also less reflective when drawings are scanned.（蓝色铅笔被设计行业以及动画行业的人们所使用，**因为它是一种我们的眼睛可以在毛坯上看到的色调**，但是当经过修正扫描进来的时候，扫描仪是看不到的，所以会使复印件上没有'编辑线'... 红色和蓝色铅笔历来被用于复印机，因为蓝色铅笔变得不可见，而红色铅笔显示为黑色。当图纸被扫描时，它们的反射率也较低。）"  
    2. [The Best Non-Photo Blue Pencils and Pens](https://www.jetpens.com/blog/The-Best-Non-Photo-Blue-Pencils-and-Pens/pt/339)  :  
"Non-photo blue is easy to remove in photo editing programs, leaving perfectly inked lines. If you like to sketch and ink traditionally and finish your work digitally, non-photo blue allows you to skip erasing your sketch, avoiding any risk of smears or faded ink. Read on for our favorite pens and pencils in this useful color....Early photocopiers used chemical film that wasn't sensitive enough to “see” a certain shade of light blue. This bug turned into a feature: artists, designers, and editors used blue for revisions or sketches that wouldn’t carry through to the final product. This color was named non-photo blue, also known as non-repro blue (short for “non-reproduction”)... Non-photo blue should only be used for black and white drawings like manga pages. （非照片蓝（non-photo blue）很容易在照片编辑程序中删除，留下完美的墨线。如果你喜欢用传统方法画草图和墨水，并以数码方式完成你的工作，非照片蓝允许你不必擦除草图，避免任何涂抹或墨水褪色的风险.... 早期的复印机使用的化学胶片不够敏感，无法"看到"某种浅蓝色的阴影。这个错误变成了一个特点：艺术家、设计师和编辑们将蓝色用于修改或画草图，而这些草图不会贯穿到最终产品中。这种颜色被命名为非照片蓝（non-photo blue），也被称为non-repro blue（"non-reproduction"的简称）... 非照片蓝应该只用于像manga这样的黑白图画）"  

- A(下图左一)发丝的墨色有深浅--细发丝墨色深；粗发丝墨色浅，墨色与头发轮廓内部的着色一致。我猜，粗发丝是后来在为头发轮廓内部着色时添加的。B(下图左二)的效果：    
![](img/hukuseigenkou_CH_2_B4_crop5.jpg) ![](img/CH-Z_012_crop5.jpg)  

- A(下图左一)浓眉的画法，对比B(下图左二)中的效果。      
![](img/hukuseigenkou_CH_2_B4_crop6.jpg) ![](img/CH-Z_012_crop6.jpg)  

- A(下图左一)中头发着色不均匀，这不影响B(下图左二)中的效果。   
![](img/hukuseigenkou_CH_2_B4_crop7.jpg) ![](img/CH-Z_012_crop7.jpg)  

- A(下图左一)中背景的特效竖线颜色变化更均匀，对比B(下图左二)中的效果。  
![](img/hukuseigenkou_CH_2_B4_crop8.jpg) ![](img/CH-Z_012_crop8.jpg)  

- A中眼仁下部和侧部(下图红箭头所示)用排线阴影。这和作者后期的画法不一样。    
![](img/hukuseigenkou_CH_2_B4_crop12.jpg)  

- 文字：  
    - A(下图左一)中这个拟声词墨色浅，可能是用毛笔画的。对比B(下图左二)中的效果。  
    ![](img/hukuseigenkou_CH_2_B4_crop9.jpg) ![](img/CH-Z_012_crop9.jpg)  

    - A中有些类型的文字直接以手写体小字写入对话框。  
    ![](img/hukuseigenkou_CH_2_B4_crop10.jpg)  

    - A中（下图左一）对话框内的文字边缘有白色涂抹痕迹，这在调整图片gamma值后（下图左二）更容易被看出。我猜，话框内的文字是打印后贴上的。   
    ![](img/hukuseigenkou_CH_2_B4_crop11.jpg) 
    ![](img/hukuseigenkou_CH_2_B4_crop11_gamma.jpg)  



---  

<a name="1983-03-i5"></a>  
## CH 複製原稿3 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_3_B4_thumb.jpg)  
City Hunter -XYZ- 第11页，   
CITYHUNTER-完全版-Z(日文版)第19页。  
本故事末页有时间戳：'83 MAR。  

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript3-b4)及描述：  
"「City Hunter −XYZ−」的复制原稿。  
<a name="BlueMark1"></a>
甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!   
技术：胶印  
材料 纸：高档纸"  

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4)  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CITYHUNTER-完全版-Z(日文版)第19页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_3_B4_thumb.jpg) 
![](img/CH-Z_013_thumb.jpg)    

细节：  

- 表示动作的白色线条。A(下图左一)似乎能看到线条边线用白色涂抹的痕迹（显出虚实相间的效果）。对比B(下图左二)。    
    ![](img/ukuseigenkou_CH_3_B4_crop2.jpg) 
    ![](img/CH-Z_013_crop2.jpg)  
- 头发的细节：    
    ![](img/ukuseigenkou_CH_3_B4_crop3.jpg) 
    ![](img/CH-Z_013_crop3.jpg)  
- A(下图左一)和B(下图左二)的特效线条的对比。A的更细腻。    
    ![](img/ukuseigenkou_CH_3_B4_crop4.jpg) 
    ![](img/CH-Z_013_crop4.jpg)  

- A中显示出特效线条未被裁减的部分。    
    ![](img/ukuseigenkou_CH_3_B4_crop5.jpg)  

- A(下图左一)中用网点阴影表示云，调整图片的gamma值后(下图左二)容易看到，其在网点纸擦出的白色区域上又加了白色颜料。对比B(下图左三)的效果：  
    ![](img/ukuseigenkou_CH_3_B4_crop6.jpg) 
    ![](img/ukuseigenkou_CH_3_B4_crop6_gamma.jpg) 
    ![](img/CH-Z_013_crop5.jpg)  

- A(下图左一)中飞机尾部的白烟涂了白色颜料，这在调整图片的gamma值后(下图左二)容易看出。对比B(下图左三)的效果  
    ![](img/ukuseigenkou_CH_3_B4_crop7.jpg) 
    ![](img/ukuseigenkou_CH_3_B4_crop7_gamma.jpg) 
    ![](img/CH-Z_013_crop7.jpg)  

- A(下图左一)中左侧树木上似乎有符合透视的线条(下图左二)。我猜，这个树木是画在透明纸，然后贴在这里。这里原本画的是建筑物。      
    ![](img/ukuseigenkou_CH_3_B4_crop9.jpg) 
    ![](img/ukuseigenkou_CH_3_B4_crop9_mark.jpg)  

- A(下图左一)中蓝线处有拼接的痕迹，这在调整图片的gamma值后(下图左二)容易看出。  
    ![](img/ukuseigenkou_CH_3_B4_crop8.jpg) 
    ![](img/ukuseigenkou_CH_3_B4_crop8_gamma.jpg)  

- 香的眼部：  
![](img/ukuseigenkou_CH_3_B4_crop0.jpg) 

- 文字：  
    - A(下图左一)中的这几个文字透出了背景的特效线条。对比B(下图左二)中的效果。    
    ![](img/ukuseigenkou_CH_3_B4_crop1.jpg) 
    ![](img/CH-Z_013_crop1.jpg)  


---  

<a name="1983-03-i6"></a>  
## CH 複製原稿4 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_4_B4_thumb.jpg)  
City Hunter -XYZ- 第22页，   
CITYHUNTER-完全版-Z(日文版)第30页。  
本故事末页有时间戳：'83 MAR。  

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript4-b4)及描述：  
"甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!   
技术：胶印  
材料 纸：高档纸"  

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4)及描述：  
"在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  
材料：Fine paper"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: CITYHUNTER-完全版-Z(日文版)第30页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_4_B4_thumb.jpg) 
![](img/CH-Z_014_thumb.jpg)    

细节：  

- A(下图左一)在网点纸上加白色颜料，这在降低gamma值(下图左二)后更容易看出。对比B(下图左三)。  
![](img/hukuseigenkou_CH_4_B4_crop0.jpg) ![](img/hukuseigenkou_CH_4_B4_crop0_gamma.jpg) 
![](img/CH-Z_014_crop0.jpg)    

- A(下图左一)中某些玻璃上的反光效果加了白色颜料，这在降低gamma值(下图左二)后更容易看出。对比B(下图左三)。    
![](img/hukuseigenkou_CH_4_B4_crop1.jpg) ![](img/hukuseigenkou_CH_4_B4_crop1_gamma.jpg) 
![](img/CH-Z_014_crop1.jpg)    

-  A(下图左一)中某些物体的阴影边缘加了白色颜料，这在降低gamma值(下图左二)后更容易看出。对比B(下图左三)。    
![](img/hukuseigenkou_CH_4_B4_crop2.jpg) ![](img/hukuseigenkou_CH_4_B4_crop2_gamma.jpg) 
![](img/CH-Z_014_crop2.jpg)    

-  A(下图左一)中某些物体的表面加了白色颜料，这在降低gamma值(下图左二)后更容易看出。对比B(下图左三)。我猜，这可能是为了表现物体表面的高光导致的"飞白"效果。对该图来说，飞白效果就是线条的虚实相间。      
![](img/hukuseigenkou_CH_4_B4_crop12.jpg) ![](img/hukuseigenkou_CH_4_B4_crop12_gamma.jpg) 
![](img/CH-Z_014_crop12.jpg)    

- A(下图左一列)中某些物体的阴影边缘有蓝色线条。对比B(下图左二列)。  
![](img/hukuseigenkou_CH_4_B4_crop4.jpg) ![](img/CH-Z_014_crop4.jpg)   
![](img/hukuseigenkou_CH_4_B4_crop3.jpg) ![](img/CH-Z_014_crop3.jpg)   
![](img/hukuseigenkou_CH_4_B4_crop5.jpg) ![](img/CH-Z_014_crop5.jpg)   

- A(下图左一列)中黑色直线的笔触。对比B(下图左二列)：  
![](img/hukuseigenkou_CH_4_B4_crop6.jpg) ![](img/CH-Z_014_crop6.jpg)   
![](img/hukuseigenkou_CH_4_B4_crop7.jpg) ![](img/CH-Z_014_crop7.jpg)   

- A(下图左一列)中某些黑色区域的着色不均匀。对比B(下图左二列)：  
![](img/hukuseigenkou_CH_4_B4_crop10.jpg) ![](img/CH-Z_014_crop10.jpg)   

- A(下图左一)中某些拟声词的着色不均匀。对比B(下图左二)。    
![](img/hukuseigenkou_CH_4_B4_crop13.jpg) ![](img/CH-Z_014_crop13.jpg)  

- A(下图左一列)黑色阴影上有网点。我猜，这是先画上黑色阴影，然后将网点阴影覆盖在上面。  
![](img/hukuseigenkou_CH_4_B4_crop11.jpg) ![](img/CH-Z_014_crop11.jpg)   

- A(下图左一列)网点纸的边缘，以及露出的线条。这些线条在排版印刷时被裁掉。    
![](img/hukuseigenkou_CH_4_B4_crop8.jpg) 

- A(下图左一列)黑色阴影上有网点。我猜，这是先画上黑色阴影，然后将网点阴影覆盖在上面。    
![](img/hukuseigenkou_CH_4_B4_crop9.jpg) 


---  

<a name="1983-03-i6"></a>  
## CH 複製原稿5 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_5_B4_thumb.jpg)  
City Hunter -XYZ- 第38页，   
CITYHUNTER-完全版-Z(日文版)第46页。  
本故事末页有时间戳：'83 MAR。  

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript5-b4)及描述：  
"甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!   
技术：胶印  
材料 纸：高档纸"  

[英文版商品链接](https://edition88.com/products/cityhunter-comicmanuscript-aset-b4)及描述：  
"在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  
技术：按需印刷  
材料：Fine paper"  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CITYHUNTER-完全版-Z(日文版)第46页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_5_B4_thumb.jpg) 
![](img/CH-Z_015_thumb.jpg)    

细节：  

- A(下图左一)的大面积面部阴影是网点阴影。网点纸覆盖一部分头发黑色区域。    
![](img/hukuseigenkou_CH_5_B4_crop0.jpg) 
![](img/CH-Z_015_crop0.jpg)  

- A(下图左一)的网点阴影覆盖在眉毛上。  
![](img/hukuseigenkou_CH_5_B4_crop1.jpg) 
![](img/CH-Z_015_crop0.jpg)  

- A(下图左一)的网点阴影内部有蓝色线条。B(下图左二)中没有相应内容。    
![](img/hukuseigenkou_CH_5_B4_crop2.jpg) 
![](img/CH-Z_015_crop2.jpg)  

- 有些汗滴为白色，有些汗滴有阴影：  
![](img/hukuseigenkou_CH_5_B4_crop3.jpg) 
![](img/hukuseigenkou_CH_5_B4_crop4.jpg) 
![](img/hukuseigenkou_CH_5_B4_crop5.jpg) 

- A(下图左一)的网点纸边缘，及其擦出的渐变效果。B(下图左二)中的效果：     
![](img/hukuseigenkou_CH_5_B4_crop6.jpg) 
![](img/CH-Z_015_crop6.jpg)  

- A(下图左一)金属(例如：子弹、枪)的高光处多涂有白色颜料,这在低gamma值时(下图左二)更容易看出。B(下图左三)中的效果：  
![](img/hukuseigenkou_CH_5_B4_crop7.jpg) 
![](img/hukuseigenkou_CH_5_B4_crop7_gamma.jpg) 
![](img/CH-Z_015_crop7.jpg)  
<a name="Pipeline"></a>
我猜，其制作流程是：  
    1. 画物体的形状;  
    2. 画排线阴影;  
    3. 覆盖网点纸；  
    4. 在网点纸上擦出高光，因为网点纸本身有一定灰度，使得擦出的部分不是纯白；  
    5. 在高光处图上白色颜料。  

- A(下图左一)大面积的平行线条不是由网点纸制作的。B(下图左二)中的效果：  
![](img/hukuseigenkou_CH_5_B4_crop8.jpg) 
![](img/CH-Z_015_crop8.jpg)  

- A(下图左一)大面积黑色过渡到白色的飞白效果。B(下图左二)中的效果：      
![](img/hukuseigenkou_CH_5_B4_crop9.jpg) 
![](img/CH-Z_015_crop9.jpg)  

- 线条有粗有细：  
![](img/hukuseigenkou_CH_5_B4_crop10.jpg) 



---  

<a name="1983-03-i7"></a>  
## CH 複製原稿6 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_6_B4_thumb.jpg)  
City Hunter -XYZ- 第40页，   
CITYHUNTER-完全版-Z(日文版)第48页。  
本故事末页有时间戳：'83 MAR。  

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript6-b4)及描述：  
"甚至可以看到纯黑色、使用白色的地方，用蓝色写的说明，起草的线条，等等!   
技术：胶印  
材料 纸：高档纸"  

[City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)及描述：  
"在漫画中看不到的不均匀的黑色油漆、使用白色修正笔的部分、以及蓝色的指示和起草线，都可以清楚地看到。  
材料：Fine paper"  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CITYHUNTER-完全版-Z(日文版)第48页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_6_B4_thumb.jpg) 
![](img/CH-Z_016_thumb.jpg)    

细节：  

- A中左侧空白处有“|<----- 天地?? ----->| ”的蓝色笔迹。我猜，可能是与最大可裁减范围相关。    

- A(下图左一)上边缘的头发被裁减(下图左二)。  
![](img/hukuseigenkou_CH_6_B4_crop0.jpg) 
![](img/CH-Z_016_crop0.jpg)    

- A(下图左一)页面背景为黑色，涂抹黑色时的笔迹。对比B(下图左二)中的效果：    
![](img/hukuseigenkou_CH_6_B4_crop1.jpg) 
![](img/CH-Z_016_crop1.jpg)    

- A(下图左一)眼部的画法，上眼线处有凹凸不平的痕迹。对比B(下图左二)中的效果：  
![](img/hukuseigenkou_CH_6_B4_crop2.jpg) 
![](img/CH-Z_016_crop2.jpg)    

- A(下图左一)枪支画得很精细。对比B(下图左二)中的效果：  
![](img/hukuseigenkou_CH_6_B4_crop3.jpg) 
![](img/CH-Z_016_crop3.jpg)    

- 獠的眼仁里映出了望远镜里的图像：  
![](img/hukuseigenkou_CH_6_B4_crop4.jpg) 



---  

<a name="1983-03-i8"></a>  
## CH 複製原稿7 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_7_B4_thumb.jpg)  
City Hunter -XYZ- 第41页，   
CITYHUNTER-完全版-Z(日文版)第49页。  
本故事末页有时间戳：'83 MAR。  

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript7-b4)  

[City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: CITYHUNTER-完全版-Z(日文版)第49页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_7_B4_thumb.jpg) 
![](img/CH-Z_017_thumb.jpg)    

细节：  

- A(下图左一)的枪火被裁减成B(下图左二)：  
![](img/hukuseigenkou_CH_7_B4_crop0.jpg) 
![](img/CH-Z_017_crop0.jpg)    

- A(下图左一)的白色颜料的排线。对比B(下图左二)：    
![](img/hukuseigenkou_CH_7_B4_crop1.jpg) 
![](img/CH-Z_017_crop1.jpg)    

- A(下图左一)枪械绘制逼真。对比B(下图左二)：  
![](img/hukuseigenkou_CH_7_B4_crop2.jpg) 
![](img/CH-Z_017_crop2.jpg)    

- A(下图左一)枪的带子处涂有白色颜料,这在低gamma值时(下图左二)更容易看出。B(下图左三)中的效果：  
![](img/hukuseigenkou_CH_7_B4_crop3.jpg) 
![](img/hukuseigenkou_CH_7_B4_crop3_gamma.jpg) 
![](img/CH-Z_017_crop3.jpg)  

- A(下图左一)玻璃裂痕处和眼仁高亮处涂有白色颜料,这在低gamma值时(下图左二)更容易看出。B(下图左三)中的效果：     
![](img/hukuseigenkou_CH_7_B4_crop4.jpg) 
![](img/hukuseigenkou_CH_7_B4_crop4_gamma.jpg) 
![](img/CH-Z_017_crop4.jpg)   

- A(下图左一)玻璃裂痕处的细节。对比B(下图左二)：    
![](img/hukuseigenkou_CH_7_B4_crop5.jpg) 
![](img/CH-Z_017_crop5.jpg)   

- A(下图左一)的特效线条的笔触：    
![](img/hukuseigenkou_CH_7_B4_crop6.jpg)  



---  

<a name="1983-03-i9"></a>  
## CH 複製原稿8 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_8_B4_thumb.jpg)  
City Hunter -XYZ- 第20页，   
CITYHUNTER-完全版-Z(日文版)第28页。  
本故事末页有时间戳：'83 MAR。    

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript8-b4)  

[英文版商品链接City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CITYHUNTER-完全版-Z(日文版)第28页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_8_B4_thumb.jpg) 
![](img/CH-Z_018_thumb.jpg)    

细节：  

- A(下图左一)中边缘不规则的云，对比B(下图左二)。云的白色是在网点纸上擦出的，A中云的白色实际上是网点纸的灰色：      
![](img/hukuseigenkou_CH_8_B4_crop0.jpg) 
![](img/CH-Z_018_crop0.jpg)    

- A(下图左一)中边缘规则的云，对比B(下图左二)。云边缘有蓝色线条。A中云的白色应该是切除网点纸后透出的白纸的颜色：  
![](img/hukuseigenkou_CH_8_B4_crop1.jpg) 
![](img/CH-Z_018_crop1.jpg)    

- 地砖的线条不直。  
![](img/hukuseigenkou_CH_8_B4_crop2.jpg)  

- 靴子上的线条比较粗犷。  
![](img/hukuseigenkou_CH_8_B4_crop3.jpg)  



---  

<a name="1983-03-i10"></a>  
## CH 複製原稿9 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_9_B4_thumb.jpg)  
City Hunter -XYZ- 第26页，   
CITYHUNTER-完全版-Z(日文版)第34页。  
本故事末页有时间戳：'83 MAR。  

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript9-b4)  

[英文版商品链接City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: CITYHUNTER-完全版-Z(日文版)第34页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_9_B4_thumb.jpg) 
![](img/CH-Z_019_thumb.jpg)     

细节：  

- A(下图左一)中特效线条的笔触。对比B(下图左二)：    
![](img/hukuseigenkou_CH_9_B4_crop0.jpg) 
![](img/CH-Z_019_crop0.jpg)   

- A(下图左一)中灯光的十字光芒是用白颜料画的。对比B(下图左二)：    
![](img/hukuseigenkou_CH_9_B4_crop1.jpg) 
![](img/CH-Z_019_crop1.jpg)   

- 拟声词上的花纹多变。    
![](img/hukuseigenkou_CH_9_B4_crop2.jpg) 
![](img/CH-Z_019_crop2.jpg)   

- A(下图左一)中椅子下的阴影似乎有两层深浅不一的排线。对比B(下图左二)：     
![](img/hukuseigenkou_CH_9_B4_crop3.jpg) 
![](img/CH-Z_019_crop3.jpg)   

- A(下图左一)中黑色拟声词上有黑色边框的痕迹。对比B(下图左二)：     
![](img/hukuseigenkou_CH_9_B4_crop4.jpg) 
![](img/CH-Z_019_crop4.jpg)   



---  

<a name="1983-03-i11"></a>  
## CH 複製原稿10 (B4) /CityHunter   (1983.03)  
![](img/hukuseigenkou_CH_10_B4_thumb.jpg)  
City Hunter -XYZ- 第39页，   
CITYHUNTER-完全版-Z(日文版)第47页。  

週刊少年Jump 1983年18号。本页左下角有时间戳：'83 MAR。      

[商品链接](https://edition-88.com/products/cityhunter-duplicateｍanuscript10-b4)  

[英文版商品链接City Hunter, Comic manuscript B (B4 size / 5 sheets per set)](https://edition88.com/products/cityhunter-comicmanuscript-bset-b4)  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: CITYHUNTER-完全版-Z(日文版)第47页    

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou_CH_10_B4_thumb.jpg) 
![](img/CH-Z_020_thumb.jpg)     

细节：  

- 拟声词上的花纹多变。   
![](img/hukuseigenkou_CH_10_B4_crop0.jpg) 
![](img/CH-Z_020_crop0.jpg)   

- 海水的网点不均匀，应该是用了多层网点纸。A(下图左一)对比B(下图左二)。       
![](img/hukuseigenkou_CH_10_B4_crop1.jpg) 
![](img/CH-Z_020_crop1.jpg)   

- A(下图左一)中头发轮廓的描边的笔触。对比B(下图左二)：  
![](img/hukuseigenkou_CH_10_B4_crop2.jpg) 
![](img/CH-Z_020_crop2.jpg)   


---  

<a name="1983-03-i12"></a>  
## CE 原稿（约1983.03）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTXD4WlaMAIeI2t.jpg"/> 
](https://pbs.twimg.com/media/FTXD4WlaMAIeI2t?format=jpg&name=large)  
Cat's Eye 18卷版第10卷“离南岛来的邀请函”第142页。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第10卷“离南岛来的邀请函”第142页。  
C: [现场实拍 - Twitter](https://twitter.com/Draichi_/status/1528343076758237184)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_10_142_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTXD4WlaMAIeI2t.jpg"/> 
](https://pbs.twimg.com/media/FTXD4WlaMAIeI2t?format=jpg&name=large) 

细节：  

- C中(下图左二)海浪泛起的白色浪花用白色颜料画。这在降低gamma值后(下图左三)更容易看出。对比B(下图左一)。    
![](img/ce_10_142_crop0.jpg) 
![](img/FTXD4WlaMAIeI2t_crop0.jpg) 
![](img/FTXD4WlaMAIeI2t_crop0_gamma.jpg) 

- B中(下图左一)水滩上月光的倒影逼真。对比C(下图左二)。    
![](img/ce_10_142_crop1.jpg) 
![](img/FTXD4WlaMAIeI2t_crop1.jpg) 

- B中(下图左一)水滩上的着色（红箭头所示）逼真。对比C(下图左二)的画法，可以看出其叠加了多层网点阴影。  
![](img/ce_10_142_crop3.jpg) 
![](img/FTXD4WlaMAIeI2t_crop3.jpg) 

- B中(下图左一)水滩上人影的倒影逼真。对比C(下图左二)的画法，可以看出其是(交错)排线。    
![](img/ce_10_142_crop4.jpg) 
![](img/FTXD4WlaMAIeI2t_crop4.jpg) 

- 头发的细节。B(下图左一)对比C(下图左二)。  
![](img/ce_10_142_crop2.jpg) 
![](img/FTXD4WlaMAIeI2t_crop2.jpg) 



---  

<a name="1983-04-i1"></a>  
## CE 原稿  （1983.04）  
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVdxRrUAAAwOI1.jpg"/> 
](https://pbs.twimg.com/media/FiVdxRrUAAAwOI1?format=jpg&name=large)  
Cat's Eye 18卷版第11卷“求婚记”第167页。   

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第11卷“求婚记”第167页。  
C:[现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595785220997132291)     

A(下图左一)，B(下图左二), C(下图左三、左四)：  
![](img/not_given.jpg) 
![](img/ce_11_167_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVdxRrUAAAwOI1.jpg"/> 
](https://pbs.twimg.com/media/FiVdxRrUAAAwOI1?format=jpg&name=large) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVdxSIUAAASGDY.jpg"/> 
](https://pbs.twimg.com/media/FiVdxSIUAAASGDY?format=jpg&name=large)  

细节：  

- panel3(下图左一)和panel4(下图左二)的画面相同，只有文字不同。  
![](img/FiVdxRrUAAAwOI1_crop0.jpg) 
![](img/FiVdxRrUAAAwOI1_crop1.jpg)  
<a name="review_copy_and_paste"></a>  
对此，C中有评论：  
3コマ目をコピーして4コマ目に貼り、ホワイトで吹き出しを描き、文字を貼り重ねている。ペーパーセメントは貼った時ヨレないのはいいが、紙自体の経年劣化は免れない。文字の紙が白いままなのは写植用紙だからか。  
（第三板块被复制并粘贴到第四板块上，用白色画出一个说话的气泡，然后把文字粘贴过去。 纸胶合剂是好的，因为它在粘贴时不会下垂，但纸张本身不能逃脱随着时间推移而变色的命运。文字的纸张仍然是白色的，因为它是誊写纸。）  

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1983-0407-i1"></a>  
## CE 複製原稿3 (B4) /Cat's♥Eye  (1983年04月～07月)  
![](img/hukuseigenkou3_B4_thumb.jpg)  
完全版第10卷第94話"危險的三角戀"第？页。  
18卷版第12卷第80话"致命的吸引力"第27页（黑白）。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript3-b4)及描述：   
「完全版第10卷 第94集：堕落的第三次爱情! 」的复制原稿。  

[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: 18卷版第12卷第80话"致命的吸引力"第27页（黑白）.      
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526552964495646721)  

A(下图左一)，B(下图左二), C(下图左三、左四)：  
![](img/hukuseigenkou3_B4_thumb.jpg) 
![](img/ce_12_027_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9n0r3XwAMq8Wq.jpg"/> 
](https://pbs.twimg.com/media/FS9n0r3XwAMq8Wq?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9n0vgX0AIWCup.jpg"/> 
](https://pbs.twimg.com/media/FS9n0vgX0AIWCup?format=jpg&name=large) 

细节：  

- A(下图左一列)对比B(下图左二列)：  
![](img/hukuseigenkou3_B4_crop1.jpg) ![](img/ce_12_027_crop1.jpg)  
![](img/hukuseigenkou3_B4_crop2.jpg) ![](img/ce_12_027_crop2.jpg)  
![](img/hukuseigenkou3_B4_crop3.jpg) ![](img/ce_12_027_crop3.jpg)  
![](img/hukuseigenkou3_B4_crop5.jpg) ![](img/ce_12_027_crop5.jpg)  
![](img/hukuseigenkou3_B4_crop6.jpg) ![](img/ce_12_027_crop6.jpg)  
![](img/hukuseigenkou3_B4_crop7.jpg) ![](img/ce_12_027_crop7.jpg)  

- A(下图左一)中对话框的边线有白色痕迹。对比B(下图左二)：  
![](img/hukuseigenkou3_B4_crop4.jpg) ![](img/ce_12_027_crop4.jpg)  

- A(下图左一)中泪的虹膜为蓝色。    
![](img/hukuseigenkou3_B4_crop0.jpg)  


---  

<a name="1983-0407-i2"></a>  
## CE 複製原稿4 (B4) /Cat's♥Eye  （1983年04月～07月）   
![](img/hukuseigenkou4_B4_thumb.jpg)  
完全版第10卷第96話“浅谷的初吻”第？页。  
18卷版第12卷第80話“致命的吸引力”第80页。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript4-b4)及描述：  
“「完全版第10卷 第96話 浅谷的初吻 」的复制原稿。”  

[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: 18卷版第12卷第80“致命的吸引力”第80页.      

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou4_B4_thumb.jpg) 
![](img/ce_12_080_thumb.jpg)        

细节：  

- A(下图左一)中加了稀疏网点纸，其边缘清晰可见。对比B(下图左二)：    
![](img/hukuseigenkou4_B4_crop0.jpg) 
![](img/ce_12_080_crop0.jpg)  

- A(下图左一)中下颏骨末端加了白色颜料(下图左二)。对比B(下图左三)：    
![](img/hukuseigenkou4_B4_crop1.jpg) 
![](img/hukuseigenkou4_B4_crop1_gamma.jpg) 
![](img/ce_12_080_crop1.jpg)  

- A(下图左一)中二人黑色头发之间的分隔。对比B(下图左三)：  
![](img/hukuseigenkou4_B4_crop2.jpg) 
![](img/ce_12_080_crop2.jpg)  

---  

<a name="1983-07-11"></a>  
## 「Cat's♥Eye」 版画1 (1983.07.11)  
![](img/hanga_cat01_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga1)及描述：  
“1983年周刊少年Jump第36期封面的版画。  
在Cat'sEye版画中，每幅版画都是用白颜料手工上色。   
这种手工上色的过程使表现了原版画中白色区域的不均匀性（matiere）。  
这张纸的尺寸是原画的原始尺寸，所以你可以尽可能地欣赏到接近原作的作品。  
技法： giclée  
紙：版画用中性紙”  

[商品链接 Cat's Eye, Art Print #1](https://edition88.com/products/catseye-hanga1)及描述：  
“技法： giclée  
版画用中性紙  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。”  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: 北条司イラスト集(北条司Illustrations)(日文版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/megenna_968/status/1526801666238201857)  
D: [现场实拍 - Twitter](https://twitter.com/planar6ele/status/1543117116588535808)  
E: [现场实拍 - Twitter](https://twitter.com/planar6ele/status/1543117347606499328)  
F: [现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595791673493176324)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四), E(下图左五), F(下图左六)：  
![](img/hanga_cat01_thumb.jpg) 
![](img/20th_anni_illustrations_040_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBGZ4MaAAAMIyC.jpg"/> 
](https://pbs.twimg.com/media/FTBGZ4MaAAAMIyC?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FWpA1M8VQAAFDLa.jpg"/> 
](https://pbs.twimg.com/media/FWpA1M8VQAAFDLa?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVj4YDVQAEbB_9.jpg"/> 
](https://pbs.twimg.com/media/FiVj4YDVQAEbB_9?format=jpg&name=large) 

细节：  

- A(下图左一)比B（下图左二）在上部、下部、右侧都显示出更多范围，左侧显示出稍许范围；  

- 因为A在右侧显示出更多范围，因此显示出签名和时间戳。  

- A的细节：  
    - 眼睛。   
    ![](img/hanga_cat01_crop0.jpg)  
    - 下颌处的白色线条和发丝：   
    ![](img/hanga_cat01_crop2.jpg)  
    - 局部大图里能看到纸的凹凸不平：  
    ![](img/hanga_cat01_crop3.jpg)  
    - 被左侧光源照亮时，绳子两侧边缘处的着色；以及绳子的白色纹理：  
    ![](img/hanga_cat01_crop4.jpg)  
    - 深色区域比浅色区域更容易显出纸的凹凸不平：  
    ![](img/hanga_cat01_crop7.jpg) 
    ![](img/hanga_cat01_crop6.jpg)  
    - 高光的笔触：  
    ![](img/hanga_cat01_crop8.jpg)  

    - 眼部的画法。小瞳(下图左一)、小爱（下图左二），小瞳的虹膜画为蓝色：  
    ![](img/hanga_cat01_crop0.jpg) 
    ![](img/hanga_cat01_crop10.jpg)  

- A(下图左一列)和B（下图左二列）的细节对比：  
    ![](img/hanga_cat01_crop1.jpg) 
    ![](img/20th_anni_illustrations_040_crop1.jpg)  
    ![](img/hanga_cat01_crop5.jpg) 
    ![](img/20th_anni_illustrations_040_crop5.jpg)  
    ![](img/hanga_cat01_crop9.jpg) 
    ![](img/20th_anni_illustrations_040_crop9.jpg)  

- D、E中的细节：  
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FWpA1MOUEAEFooj.jpg"/> 
](https://pbs.twimg.com/media/FWpA1MOUEAEFooj?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FWpBC0oVEAAVeus.jpg"/> 
](https://pbs.twimg.com/media/FWpBC0oVEAAVeus?format=jpg&name=large) 

- F中的评论：  
"デビュー当初からの高い画力、構図、センス、色彩感覚。  
今展には出て無かったが、デビュー前の手塚賞応募作品ですでにプロレベルだった。"  
(自出道以来，他的绘画能力、构图、风格感和色彩感都很高。  
虽然他没有被列入这次展览，但他在出道前的手冢奖作品已经达到了专业水平。)  


---  

<a name="1983-07-i1"></a>  
## CE 原画（约1983.07）  
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVhD0LVsAIHXKh.jpg"/> 
](https://pbs.twimg.com/media/FiVhD0LVsAIHXKh?format=jpg&name=large)  
Cat's Eye 18卷版第12卷“金发女郎”第122页。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第12卷“金发女郎”第122页。  
C: [现场实拍 - Weibo](https://twitter.com/tohnomiyuki00/status/1595789043891318794)     
D：[现场实拍 - Twitter](https://twitter.com/shotikubuyka/status/1525872516098449408)  

A(下图左一)，B(下图左二), C(下图左三、左四)，D(下图左五)：  
![](img/not_given.jpg) 
![](img/ce_12_122_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVhD0LVsAIHXKh.jpg"/> 
](https://pbs.twimg.com/media/FiVhD0LVsAIHXKh?format=jpg&name=large) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVhD0PVQAA8ReD.jpg"/> 
](https://pbs.twimg.com/media/FiVhD0PVQAA8ReD?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz88u0VIAA5787.jpg"/> 
](https://pbs.twimg.com/media/FSz88u0VIAA5787?format=jpg&name=medium) 

细节：  

- C、D中的画像的区域发黄色，原因可能如上[所述](./details1.md#review_copy_and_paste)：此区域的画面是复印粘贴上的；因粘贴所用的胶水，所以纸张经年累月后变黄。    
![](img/FSz88u0VIAA5787_crop0.jpg)  

- C中的评论：  
"納得がいかなかったのか、切貼りの上にさらにホワイトで消してまで修正してある。デジタルは上書きしてしまうので、こういう試行錯誤の跡が残らない。将来の原画展はどうなっていくのだろう。"  
(也许他们并不相信，但他们甚至在剪贴的基础上用白色抹去了图像，对其进行了修正。  
由于数字技术的覆盖，这种试错的痕迹就不存在了。  
未来的原创艺术作品展览会有怎样的结果。)  



---  

<a name="1983-07-i2"></a>  
## CE 原画（约1983.07）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz88u0VIAA5787.jpg"/> 
](https://pbs.twimg.com/media/FSz88u0VIAA5787?format=jpg&name=medium)  
18卷版第12卷“金发女郎”第123页。  

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B: 18卷版第12卷“金发女郎”第123页。  
C: [现场实拍 - Twitter](https://twitter.com/shotikubuyka/status/1525872516098449408)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_12_123_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSz88u0VIAA5787.jpg"/> 
](https://pbs.twimg.com/media/FSz88u0VIAA5787?format=jpg&name=medium) 

细节：  

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  



---  

<a name="1983-11-i1"></a>  
## CE 插画 (周刊少年Jump 1983 Issue#48[^jump198348] Front Cover Poster。可能为1983.11)    
[^jump198348]: Cat's Eye Chapter 110 (Lead Color Pages) [Weekly Shonen Jump #784 - No. 48, 1983 released by Shueisha on November 14, 1983](https://comicvine.gamespot.com/weekly-shonen-jump-784-no-48-1983/4000-514944/)

[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTcltLYaIAEODKc.jpg"/> 
](https://pbs.twimg.com/media/FTcltLYaIAEODKc?format=jpg&name=medium)  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)。  
C: [现场实拍 - Twitter](https://twitter.com/garnetlynx777/status/1528732065419894784)     
D: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527630390835683329)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![](img/not_given.jpg) 
![](img/illustrations_47_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTcltLYaIAEODKc.jpg"/> 
](https://pbs.twimg.com/media/FTcltLYaIAEODKc?format=jpg&name=medium) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM7u8uakAAMwsS.jpg"/> 
](https://pbs.twimg.com/media/FTM7u8uakAAMwsS?format=jpg&name=large) 

细节：  

- 黄绿配色是一个不错的选择。  

- 用白色竖线虚化背景人物。对比B(下图左一)，C(下图左二)，D(下图左三)。  
![](img/illustrations_47_crop0.jpg) 
![](img/FTcltLYaIAEODKc_crop0.jpg) 
![](img/FTM7u8uakAAMwsS_crop0.jpg) 

- D中能清晰地看出画作表面的凹凸不平。   
![](img/FTM7u8uakAAMwsS_crop1.jpg) 

- 绿色裙子的着色。对比B(下图左一)，C(下图左二)，D(下图左三)。  
![](img/illustrations_47_crop2.jpg) 
![](img/FTcltLYaIAEODKc_crop2.jpg) 
![](img/FTM7u8uakAAMwsS_crop2.jpg) 

- D中绿色边线粗细不均。  
![](img/FTM7u8uakAAMwsS_crop3.jpg)  

- D中能看到拇指处有淡淡的褶皱。    
![](img/FTM7u8uakAAMwsS_crop4.jpg)  

- D中能看到浅色线条（下图红箭头所示）。[疑问]这表示褶皱的高光吗？  
![](img/FTM7u8uakAAMwsS_crop5.jpg)  

- D中能看到有些褶皱的墨色深（下图蓝箭头所示），有些褶皱的墨色浅（下图红箭头所示）。我猜，浅色褶皱是在裙子着色绿色之前画上的；深色褶皱是在裙子着色绿色之后画上的。      
![](img/FTM7u8uakAAMwsS_crop6.jpg)  



---  

<a name="1983-12-i1"></a>  
## CE 官网报道中的图片03 (1983.12)  
![](img/cat_s-eye_tenji_96_11_thumb.jpg)  
Cat's Eye 18卷版第14卷“富勇气的决定”第194页。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 18卷版第14卷第194页；  
C：[现场实拍 - Weibo](https://twitter.com/tohnomiyuki00/status/1595787797449043969)  

A(下图左一)，B(下图左二)， C（下图左三）：  
![](img/cat_s-eye_tenji_96_11_thumb.jpg) 
![](img/ce_14_0194_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVfoY1UYAA2fzP.jpg"/> 
](https://pbs.twimg.com/media/FiVfoY1UYAA2fzP?format=jpg&name=large) 

细节：   

- C中的评论：  
“二色用原稿とフルカラー原稿  
フルカラーの方は、コラージュの上に影を描き加え、表現の仕方を探っている感じ。デジタルならレイヤーを駆使すれば作業自体は簡単になる。  
二色はデジタルでも製版工程が以外と面倒。”  
（双色和全色的手稿  
全彩的更像是在拼贴的基础上加入阴影，探索表达方式。 对于数字绘画，如果你充分利用图层，工作本身是很容易的。  
对于双色印刷来说，制版过程比数字印刷更加繁琐。）  

- 上部边缘处有一处黄颜色标记，可能是胶带。  
![](img/cat_s-eye_tenji_96_11_crop1.jpg)  

- 对话框内的文字区域呈凹陷，应该是该区域的画纸被割掉了。  

- A(下图左一)的画纸有年代感，降低gamma值(下图左二)后更容易看出。    
![](img/cat_s-eye_tenji_96_11_crop0.jpg) 
![](img/cat_s-eye_tenji_96_11_crop0_gamma.jpg) 

- A文字背景的纹理(下图左一)，及其灰度效果(下图左二)，对比B(下图左三)。  
![](img/cat_s-eye_tenji_96_11_crop2.jpg) 
![](img/cat_s-eye_tenji_96_11_crop2_gray.jpg) 
![](img/ce_14_0194_crop2.jpg) 

- A衣服、背景沙发的着色(下图左一)，及其灰度效果(下图左二)，对比B(下图左三)。背景沙发的着色逼真。  
![](img/cat_s-eye_tenji_96_11_crop3.jpg) 
![](img/cat_s-eye_tenji_96_11_crop3_gray.jpg) 
![](img/ce_14_0194_crop3.jpg) 

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  



---  

<a name="1983-12-i2"></a>  
## CE 插画  （1983.12）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM7u7zacAALc4o.jpg"/> 
](https://pbs.twimg.com/media/FTM7u7zacAALc4o?format=jpg&name=large)  
Cat's Eye 18卷版第14卷“富勇气的决定”184-185页（黑白）。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)，18卷版第14卷“富勇气的决定”184-185页（黑白）。  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527630390835683329)    

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/illustrations_50_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM7u7zacAALc4o.jpg"/> 
](https://pbs.twimg.com/media/FTM7u7zacAALc4o?format=jpg&name=large) 

细节：  

- C中(下图左二)能看出用白色颜料勾边。对比B(下图左一)。   
![](img/illustrations_50_crop1.jpg) 
![](img/FTM7u7zacAALc4o_crop1.jpg)  
C中(下图左二)能看出勾边的白色线条压在黑线上。对比B(下图左一)。  
![](img/illustrations_50_crop0.jpg) 
![](img/FTM7u7zacAALc4o_crop0.jpg) 

- C中似乎能看出纸面凹凸不平。    
![](img/FTM7u7zacAALc4o_crop2.jpg)  

- 手部的细节。B(下图左一)对比C(下图左二)。     
![](img/illustrations_50_crop3.jpg) 
![](img/FTM7u7zacAALc4o_crop3.jpg)  

- 褶皱的高光和阴影。B(下图左一)对比C(下图左二)。     
![](img/illustrations_50_crop4.jpg) 
![](img/FTM7u7zacAALc4o_crop4.jpg)  


---  

<a name="1983-12-i3"></a>  
## 原稿 CityHunter - Double Edge - （1983.12）  
![](img/thumb/CHce_Z_069.jpg)  
CITYHUNTER-完全版-Z第69页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: CITYHUNTER-完全版-Z第69页。  
C: 官方介绍页。  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/thumb/CHce_Z_069.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/double-edge01.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/double-edge01_4800x4800.jpg)  

细节：  

- C降低gamma后似乎透出了背面的墨色。所以这应该不是原画。    
![](img/double-edge01_crop0.jpg) 

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  

---  

<a name="1983-12-i4"></a>  
## 原画 CityHunter - Double Edge - （1983.12）  
![](img/thumb/CHce_Z_074.jpg)  
CITYHUNTER-完全版-Z第74页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: CITYHUNTER-完全版-Z第74页。  
C: 官方介绍页。  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/thumb/CHce_Z_074.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/double-edge01.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/double-edge01_4800x4800.jpg)  

细节：  

- C降低gamma后似乎透出了背面的墨色。所以这应该不是原画。    

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1983-12-i5"></a>  
## 原画 CityHunter - Double Edge - （1983.12）  
![](img/thumb/CHce_Z_084.jpg)  
CITYHUNTER-完全版-Z第84页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: CITYHUNTER-完全版-Z第84页。  
C: [现场实拍 - Twitter](https://twitter.com/pwfregonese/status/1638542481355833345)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/thumb/CHce_Z_084.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Fr1Ef-daUAcHZ21.jpg"/> 
](https://pbs.twimg.com/media/Fr1Ef-daUAcHZ21?format=jpg&name=large)   

细节：  

- C降低gamma值后（左一）能看到颜料的凸起。对比B（左二）。  
![](img/Fr1Ef-daUAcHZ21_crop0_gamma.jpg) 
![](img/CHce_Z_084_crop0.jpg) 



---  

<a name="1983-i1"></a>  
## CE 插画  (1983)  
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB_crop3_thumb.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096)  
Cat's Eye 18卷版第7卷封面。    

为简便，采用如下符号简记：  
A: 展品的官方照片无;    
B1: 常见来源： 第7卷封面,  
B2: 常见来源： 北条司イラスト集(北条司Illustrations) 。    
C: [现场实拍 - Twitter](https://twitter.com/hojo_official/status/1525002979723517952)  

A(下图左一)，B(下图左二、左三), C(下图左四、左五)：  
![](img/not_given.jpg) 
![](img/thumb/ce_07_000_crop0_thumb.jpg) 
![](img/thumb/illustrations_44_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnkE9lVEAAwobB_crop3_thumb.jpg"/> 
](https://pbs.twimg.com/media/FSnkE9lVEAAwobB?format=jpg&name=4096x4096) 

细节：  

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1983-i2"></a>  
## 「Cat's♥Eye」 版画5 (1983年底？)
![](img/hanga_cat05_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga5)及描述：  
"周刊少年Jump1983年第46期封面的版画。  
技法：giclée  
版画用中性紙  
Cat'sEye的版画使用白色颜料，每一张都有手工上色。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。"   

[英文版商品链接Cat's Eye, Art Print #5, Hand Signed by Tsukasa Hojo](https://edition88.com/products/catseye-hanga5)及描述：  
"国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水）精致地表现色彩和纹理。  
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  
国际版在美术纸上采用混合媒体（Giclée和UV）"  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: 常见来源：未知。  
C: [现场实拍 - Twitter](https://twitter.com/nanacy774/status/1528831530931367937)  

A(下图左一), B(下图左二), C(下图左三):  
![](img/hanga_cat05_thumb.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTeAmkUVIAAxATI.jpg"/> 
](https://pbs.twimg.com/media/FTeAmkUVIAAxATI?format=jpg&name=large) 

细节：  

- C中显示A是两张图组合而成的。  

- 高光的笔触：  
![](img/hanga_cat05_crop0.jpg) ![](img/hanga_cat05_crop1.jpg)  

- 蒸汽：  
![](img/hanga_cat05_crop2.jpg)  

- 头发的光泽：  
![](img/hanga_cat05_crop3.jpg) ![](img/hanga_cat05_crop4.jpg)  

- 衣服褶皱：  
![](img/hanga_cat05_crop5.jpg)  

- 眼部。小爱（下图左一）、小瞳（下图左二）、泪（下图左三）。泪的虹膜画为蓝色：  
![](img/hanga_cat05_crop10.jpg) ![](img/hanga_cat05_crop11.jpg) 
![](img/hanga_cat05_crop6.jpg)  

- A(下图左一列)和A的局部图(非正面)(下图左二列)有差异。后者似乎在高光处添加了白色颜料。能看出白色颜料的凸起：   
![](img/hanga_cat05_crop4.jpg) ![](img/hanga_cat05_crop7.jpg)  
![](img/hanga_cat05_crop9.jpg) ![](img/hanga_cat05_crop9_2.jpg)  

- A的局部图(非正面)容易看出纸面凹凸不平：  
![](img/hanga_cat05_crop8.jpg)  



---  

<a name="1983-i3"></a>  
## 「Cat's♥Eye」 版画7 (1983)
![](img/hanga_cat07_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga7)及描述：  
“Jump Comics 1983年第5卷封面的版画。  
Cat'sEye的版画使用白色颜料，每一张都有手工上色。  
这种手工上色的过程表达了原画中白色部分的不均匀性（matiere）。  
技法：giclée  
版画用中性紙”  

[英文版商品链接 Cat's Eye, Art Print #7](https://edition88.com/products/catseye-hanga7)及描述：   
“国际版是用混合媒体开发的，同时使用giclée（水性颜料）和UV打印机（UV固化墨水），以精致地表现色彩和纹理。   
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  
国际版在美术纸上采用混合媒体（Giclée和UV）”   

第5卷的签名多为1982年4月～5月。  

为简便，采用如下符号简记：  
A: 展品的官方照片；  
B: 北条司イラスト集(北条司Illustrations)(日文版)中的该图片；  
C: Cat'sEye(18卷版)第5卷封面;  
D: [现场实拍 - Twitter](https://twitter.com/kirakuni91/status/1527217808375152640)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![](img/hanga_cat07_thumb.jpg) 
![](img/20th_anni_illustrations_045_thumb.jpg) 
![](img/ce_05_000_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTHEffaakAAJxeb.jpg"/> 
](https://pbs.twimg.com/media/FTHEffaakAAJxeb?format=jpg&name=large) 

细节：  

- A与B显示的大小范围一致。  

- A(下图左一)涂抹掉部分黑色边线，B（下图左二）的效果：    
![](img/hanga_cat07_crop0.jpg) 
![](img/20th_anni_illustrations_045_crop0.jpg)  

- A(下图左一列)和B（下图左二列）的水珠对比。  
![](img/hanga_cat07_crop1.jpg) 
![](img/20th_anni_illustrations_045_crop1.jpg)  
![](img/hanga_cat07_crop2.jpg) 
![](img/20th_anni_illustrations_045_crop2.jpg)  
![](img/hanga_cat07_crop17.jpg) 
![](img/20th_anni_illustrations_045_crop17.jpg)  

- 橘子的高光逼真。A(下图左一)和B（下图左二）的对比。  
![](img/hanga_cat07_crop3.jpg) 
![](img/20th_anni_illustrations_045_crop3.jpg)    

- A的橘子的高光逼真。逐步放大其细节：   
![](img/hanga_cat07_crop3.jpg) 
![](img/hanga_cat07_crop5.jpg)  
![](img/hanga_cat07_crop6.jpg) 
![](img/hanga_cat07_crop7.jpg) 
![](img/hanga_cat07_crop8.jpg) 

- 眼仁处的高光似乎有凸出的笔触：    
![](img/hanga_cat07_crop9.jpg) ![](img/hanga_cat07_crop11.jpg)  

- A的局部高清图显示纸面的凹凸不平：  
![](img/hanga_cat07_crop10.jpg)  

- 背景椰树的粗树叶为剪影，细树叶是由白色颜料画的。  
![](img/hanga_cat07_crop12.jpg)  

- 眼镜片上的高光：  
![](img/hanga_cat07_crop13.jpg)  

- 发丝的笔触和着色细节：  
![](img/hanga_cat07_crop14.jpg) 
![](img/hanga_cat07_crop16.jpg)  

- 眼部的细节。泪的虹膜画为蓝色：  
![](img/hanga_cat07_crop18.jpg)  

- 嘴部的细节：  
![](img/hanga_cat07_crop15.jpg)  





---  

<a name="1983-i4"></a>  
## CE 原画  （1983年？月）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS8hlUvVsAA_kET.jpg"/> 
](https://pbs.twimg.com/media/FS8hlUvVsAA_kET?format=jpg&name=large)  
Cat's Eye 18卷版第13卷第91话“我已不是孩子了”第106页。   

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 18卷版第13卷第91话“我已不是孩子了”第106页。  
C: [现场实拍 - Twitter](https://twitter.com/ha43zu/status/1526475738655772672)    

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_13_106_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS8hlUvVsAA_kET.jpg"/> 
](https://pbs.twimg.com/media/FS8hlUvVsAA_kET?format=jpg&name=large) 


细节：  

- C(下图左二)中蒸汽的网点阴影边缘涂抹白色颜料。对比B(下图左一)。    
![](img/ce_13_106_crop0.jpg) 
![](img/FS8hlUvVsAA_kET_crop0.jpg)  

- C(下图左二)中排线阴影。对比B(下图左一)。    
![](img/ce_13_106_crop1.jpg) 
![](img/FS8hlUvVsAA_kET_crop1.jpg)  

- C(下图左二)中着色。对比B(下图左一)。    
![](img/ce_13_106_crop2.jpg) 
![](img/FS8hlUvVsAA_kET_crop2.jpg)  

- C(下图左一)中pannel边线外侧有白色涂抹痕迹。这在降低gamma值后更容易看出(下图左二)。    
![](img/FS8hlUvVsAA_kET_crop3.jpg) 
![](img/FS8hlUvVsAA_kET_crop3_gamma.jpg)  

- C(下图左一)中排线阴影边缘有白色涂抹痕迹。这在降低gamma值后更容易看出(下图左二)。  
![](img/FS8hlUvVsAA_kET_crop4.jpg) 
![](img/FS8hlUvVsAA_kET_crop4_gamma.jpg)  

- C(下图左一)中对话框边线有白色涂抹痕迹。这在降低gamma值后更容易看出(下图左二)。  
![](img/FS8hlUvVsAA_kET_crop5.jpg) 
![](img/FS8hlUvVsAA_kET_crop5_gamma.jpg)  

- C(下图左二)中有颜色很浅的着色。对比B(下图左一)。  
![](img/ce_13_106_crop6.jpg) 
![](img/FS8hlUvVsAA_kET_crop6.jpg)  

- C中有三个家具摆设(下图左一)，其在B中被裁剪掉一半。这三个小动物摆设出现在了[商品](https://edition-88.com/products/catseye-arttile4)的左上角(下图左二)。  
![](img/FS8hlUvVsAA_kET_crop7.jpg) 
![](img/coa5_crop0.jpg)  
我猜：  
    - 1)这三个小动物摆设可能有特别的意义。[疑问]如果是这样，那么意义是什么？   
    - 2)用三个小动物遮盖原图片里的敏感信息。[疑问]如果是这样，那么原图中的敏感信息是什么？是马路上的标号吗？       



---  

<a name="1983-i5"></a>  
## CE 複製原稿5 (B4) /Cat's♥Eye  （1983年？月）  
![](img/hukuseigenkou5_B4_thumb.jpg)  
完全版第11卷第107话“我已不是孩子了”第？页。  
18卷版第13卷第91话“我已不是孩子了”第114页。   

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript5-b4)  
[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)及描述：  
“「完全版第11卷第107话：我已不是孩子了」的复制原稿。”    

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第12卷第80“致命的吸引力”第80页.  
C: [画展现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526551921489612800)    
D: [画展现场实拍 - Twitter](https://twitter.com/thelowtierchara/status/1528027711800680449)     

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou5_B4_thumb.jpg) 
![](img/ce_13_114_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9m3ypaUAELFd2.jpg"/> 
](https://pbs.twimg.com/media/FS9m3ypaUAELFd2?format=jpg&name=large) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9m38PaUAEd4xy.jpg"/> 
](https://pbs.twimg.com/media/FS9m38PaUAEd4xy?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTSczXYUEAEhLJV.jpg"/> 
](https://pbs.twimg.com/media/FTSczXYUEAEhLJV?format=jpg&name=4096x4096) 

细节：  

- A(下图左一列)背景加了白色竖线。对比B(下图左二列)：   
![](img/hukuseigenkou5_B4_crop0.jpg) ![](img/ce_13_114_crop0.jpg)  
![](img/hukuseigenkou5_B4_crop1.jpg) ![](img/ce_13_114_crop1.jpg)  
![](img/hukuseigenkou5_B4_crop2.jpg) ![](img/ce_13_114_crop2.jpg)  

- A(下图左一)对比C(下图左二)的背景白色竖线：   
![](img/hukuseigenkou5_B4_crop9.jpg) 
![](img/FS9m38PaUAEd4xy_crop9.jpg)  

- A(下图左一)背景树叶是贴上的，且边缘加了白色颜料。这在降低gamma值后容易看出。对比B(下图左三)：   
![](img/hukuseigenkou5_B4_crop3.jpg) ![](img/hukuseigenkou5_B4_crop3_gamma.jpg) ![](img/ce_13_114_crop3.jpg)  

- 背景是贴上的，A(下图左一)和C(下图左二)的贴纸边缘处：  
![](img/hukuseigenkou5_B4_crop10.jpg) 
![](img/FS9m38PaUAEd4xy_crop10.jpg)  
A(下图左一)和C(下图左二)降低gamma值后容易看出贴纸本身似乎有竖向纹理。    
![](img/hukuseigenkou5_B4_crop10_gamma.jpg) 
![](img/FS9m38PaUAEd4xy_crop10_gamma.jpg)  

- A(下图左一)左膝盖处的网点阴影，对比B(下图左二)：   
![](img/hukuseigenkou5_B4_crop4.jpg) ![](img/ce_13_114_crop4.jpg)  

- A(下图左一)头发的墨色较均匀，对比B(下图左二)：   
![](img/hukuseigenkou5_B4_crop5.jpg) ![](img/ce_13_114_crop5.jpg)  

- 服饰上使用不同灰度的网点纸。  
![](img/hukuseigenkou5_B4_crop6.jpg)  

- 眼部的细节：  
![](img/hukuseigenkou5_B4_crop7.jpg)  

- A(下图左一)与C(下图左二)右下角的对比。展览的画框遮盖了部分边缘。  
![](img/hukuseigenkou5_B4_crop8.jpg) 
![](img/FS9m3ypaUAELFd2_crop8.jpg)  



---  
<a name="unknown-i4"></a>  
## CE 插画（作品创作年月未知）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS7NQG-UcAEPG2J.jpg"/> 
](https://pbs.twimg.com/media/FS7NQG-UcAEPG2J?format=jpg&name=medium)  
Cat's Eye 18卷版14卷“燃烧的过去”6～7页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations),11卷封面, 11卷108页。    
C：[官方blog - Twitter](https://twitter.com/cityhunter100t/status/1526384130438508544)  
D：[现场实拍 - Twitter](https://twitter.com/Draichi_/status/1528343076758237184)  
E：[现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595787797449043969)  
F：[现场实拍 - Twitter](https://twitter.com/karakuri_pig/status/1525428290504118272)  
G：[现场实拍 - Twitter](https://twitter.com/mihohi1/status/1595417709432668160)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)，E(下图左五)，F(下图左六)，G(下图左七)：  
![](img/not_given.jpg) 
![](img/ce_14_6_7_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS7NQG-UcAEPG2J.jpg"/> 
](https://pbs.twimg.com/media/FS7NQG-UcAEPG2J?format=jpg&name=medium) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTXD7HPaAAAP6XQ.jpg"/> 
](https://pbs.twimg.com/media/FTXD7HPaAAAP6XQ?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVfoZPUoAA45o7.jpg"/> 
](https://pbs.twimg.com/media/FiVfoZPUoAA45o7?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/FSto8hyUUAEZbUL.jpg"/> 
](https://pbs.twimg.com/media/FSto8hyUUAEZbUL?format=jpg&name=large) 
[<img title="G备份缩略图(点击查看原图)" height="100" src="img/thumb/FiQP8y7VUAIEVKe.jpg"/> 
](https://pbs.twimg.com/media/FiQP8y7VUAIEVKe?format=jpg&name=large) 

细节：  

- [官方blog的评论 - weibo](https://m.weibo.cn/detail/4770164672824122)：  
"这张图的背景画面，是将画过一次的原稿用复印机超缩小后取出，再进行超扩大处理，形成这种粒子加工的感觉"  

- E中的评论：  
“二色用原稿とフルカラー原稿  
フルカラーの方は、コラージュの上に影を描き加え、表現の仕方を探っている感じ。デジタルならレイヤーを駆使すれば作業自体は簡単になる。  
二色はデジタルでも製版工程が以外と面倒。“  
（双色和全色的手稿  
全彩的更像是在拼贴的基础上加入阴影，探索表达方式。 对于数字绘画，如果你充分利用图层，工作本身是很容易的。  
对于双色印刷来说，制版过程比数字印刷更加繁琐。）  

- G中的评论：  
”これこれ、大好きな絵の一つ。どうやって書いたんだろ？と子供の頃何度も見た絵が目の前に。背景と人物を立体的に貼り合わせてた。コラージュ？美術家らしいテクニックね“  
（这，这，这是我最喜欢的画之一。 他是怎么写的呢？ 而我小时候看到过很多次的照片就在我面前。 背景和人物是以三维方式粘贴在一起的。 拼贴？ 这是一个艺术家的技术。）    

- 对比C(下图左一)、F(下图左二)、G(下图左三)。F、G中的阴影可能意味着人像是贴上去的。      
![](img/FS7NQG-UcAEPG2J_crop0.jpg) 
![](img/FSto8hyUUAEZbUL_crop0.jpg) 
![](img/FiQP8y7VUAIEVKe_crop0.jpg) 

- F(下图左一)、G(下图左二)中能看到人像边缘的破旧。  
![](img/FSto8hyUUAEZbUL_crop1.jpg) 
![](img/FiQP8y7VUAIEVKe_crop1.jpg) 

-  F(下图左一)、G(下图左二)中手表的细节。    
![](img/FSto8hyUUAEZbUL_crop2.jpg) 
![](img/FiQP8y7VUAIEVKe_crop2.jpg) 

- C(下图左二)中能看到背景建筑物的纸面泛黄，且有白色颜料勾边。对比B(下图左一)。  
![](img/ce_14_6_7_crop3.jpg) 
![](img/FS7NQG-UcAEPG2J_crop3.jpg) 


---  

<a name="1984-01-i1"></a>  
## CE 原稿（约1984.01)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2f12d3y0zj30ex0iwt9m.jpg"/> 
](https://wx4.sinaimg.cn/large/008rpAF8gy1h2f12d3y0zj30ex0iwt9m.jpg)  
Cat's Eye 18卷版第15卷第001页。  

从其后的“假如是一个美丽的谎言”扉页时间戳“84JAN”推测，该画作创作时间约为1984年01月。
  
为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 18卷版第15卷第1页。  
C：[官方blog - weibo](https://m.weibo.cn/detail/4771262742659434)  

A(下图左一)，B(下图左二), C(下图左三、左四)：  
![](img/not_given.jpg) 
![](img/ce_15_001_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2f12d3y0zj30ex0iwt9m.jpg"/> 
](https://wx4.sinaimg.cn/large/008rpAF8gy1h2f12d3y0zj30ex0iwt9m.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/008rpAF8gy1h2f12da9fdj30e60iwt9s.jpg"/> 
](https://wx1.sinaimg.cn/large/008rpAF8gy1h2f12da9fdj30e60iwt9s.jpg) 


细节：  

- C中的评论：      
“和泪姐一模一样构图的阿香  
虽然很像…但是有点微妙的不同…  
北条司老师说‘为什么要向泪致敬呢…？不记得了（笑）’ ”  

- C(下图左二)比B(下图左一)的网点阴影变化更均匀。  
![](img/ce_15_001_crop0.jpg) 
![](img/008rpAF8gy1h2f12d3y0zj30ex0iwt9m_crop0.jpg) 

- 正如C中的评论所说，C中泪和香的图有很多相似之处，甚至左膝盖处的尖角（下图红箭头所示）都一致。  
![](img/008rpAF8gy1h2f12d3y0zj30ex0iwt9m_crop1.jpg) 
![](img/008rpAF8gy1h2f12da9fdj30e60iwt9s_crop1.jpg) 

<a name="HeadBodyRatio"></a>

- C中泪的头身比大约介于1:7~1:9之间；香的头身比大约介于1:6.5~1:7.5之间。    
![](img/008rpAF8gy1h2f12d3y0zj30ex0iwt9m_head-body-ratio.jpg) 
![](img/008rpAF8gy1h2f12da9fdj30e60iwt9s_head-body-ratio.jpg) 

注：因为C的分辨率小于B，所以无法对比查看C的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="1984-03-i1"></a>  
## 「Cat's♥Eye」 版画2 (1984.03)  
![](img/hanga_cat02_thumb.jpg)  

[商品链接](https://edition-88.com/products/catseye-hanga2)及描述：  
“1984年周刊少年Jump第18期封面的版画。   
技法：Giclée  
版画用中性紙”  

[商品链接 Cat's Eye, Art Print #2](https://edition88.com/products/catseye-hanga2)及描述：  
“国际版的作品是用混合媒体开发的，同时使用giclée（水基颜料）和UV打印机（UV固化墨水），精致地表达色彩和质地。   
每一幅Cat’s Eye印刷品都是用白颜料手工处理的，以复制原画上因使用修正液而产生的不均匀性（matière）。  
国际版作品在美术纸上的采用混合媒体（Giclée和UV）”  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文原版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595791673493176324)  

A(下图左一)，B(下图左二)， C（下图左三、左四）：  
![](img/hanga_cat02_thumb.jpg) 
![](img/20th_anni_illustrations_043_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVj_EJUoAAjenB.jpg"/> 
](https://pbs.twimg.com/media/FiVj_EJUoAAjenB?format=jpg&name=large) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVkAO8UYAEC577.jpg"/> 
](https://pbs.twimg.com/media/FiVkAO8UYAEC577?format=jpg&name=large) 

细节：  

- A的四周均比B显示了更多范围。因此A的右下角显示出签名和时间戳。  

- A(下图左一)的颜料浸润的细节。对比B(下图左二):    
![](img/hanga_cat02_crop0.jpg) 
![](img/20th_anni_illustrations_043_crop0.jpg)   

- A(下图左一)的着色不均匀的细节。对比B(下图左二):    
![](img/hanga_cat02_crop1.jpg) 
![](img/20th_anni_illustrations_043_crop1.jpg)   

- A(下图左一)的能看到更多的着色层次。对比B(下图左二):   
![](img/hanga_cat02_crop2.jpg) 
![](img/20th_anni_illustrations_043_crop2.jpg)   

- 角色背景阴影的边缘线不光滑。我猜，背景这阴影是剪影。    
![](img/hanga_cat02_crop3.jpg) 

- C中的评论：  
"デビュー当初からの高い画力、構図、センス、色彩感覚。  
今展には出て無かったが、デビュー前の手塚賞応募作品ですでにプロレベルだった。"  
(自出道以来，他的绘画能力、构图、风格感和色彩感都很高。  
虽然他没有被列入这次展览，但他在出道前的手冢奖作品已经达到了专业水平。)    



---  

<a name="1984-03-i2"></a>  
## CE 複製原稿6 (B4) /Cat's♥Eye  (1984.03)  
![](img/hukuseigenkou6_B4_thumb.jpg)  
完全版第13卷第133话"黑夜的誘惑"第？页。  
18卷版第16卷第117话“黑夜的誘惑”第50页。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript6-b4)及描述：  
“「完全版第13卷第133集 - 黑夜的誘惑」的复制原稿。    
【规格】  
尺寸: A4 (H297 x W210mm)  
技术: 按需印刷  
材料：Marmaid纸”  

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第16卷第117话“黑夜的誘惑”第50页中的该图片；  

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou6_B4_thumb.jpg) 
![](img/ce_16_50_thumb.jpg)   

细节：  

- A中左侧空白有竖排的蓝色手写文字（下图为了排版紧凑，将图片旋转90度）。  
![](img/hukuseigenkou6_B4_crop0.jpg)  

- A中对话框中先手写文字，后贴上打印的文字。  
![](img/hukuseigenkou6_B4_crop1.jpg)  

<a name="BaronKent"></a>  

- A中左下角有“BARON KENT”字样。可能是[Orion Baron Kent纸](https://www.k-orion.co.jp/product/paper/orion-baronkent.html)。此外，Baron Kent纸应该是Kent纸的一种，详见[Kent纸](./extra_info.md#Kent)。最后，[新版SpaceAngel的原画用纸](./details2.md#DeleterKent)显示作者目前使用Deleter(灵猫)牌Kent纸。注：  
    - [Japanese Manga101 #40 @SMA](../zz_www.manga-audition.com/zz_japanese_manga101/040.md)中提到Kent纸是日本漫画家用的最多的纸。  
    - [Manga S.O.S. #3 @SMA](../zz_www.manga-audition.com/manga-sos/003.md)中提到这类漫画用纸的注意事项。  

- A(下图左一)发丝的边缘并没有涂满墨色。对比B(下图左二)：    
![](img/hukuseigenkou6_B4_crop2.jpg) 
![](img/ce_16_50_crop2.jpg)   

- A(下图左一列)深色泳衣的高光画为交叉排线。对比B(下图左二列)：     
![](img/hukuseigenkou6_B4_crop3.jpg) ![](img/ce_16_50_crop3.jpg)   
![](img/hukuseigenkou6_B4_crop4.jpg) ![](img/ce_16_50_crop4.jpg)   

- A(下图左一)角色破格，其边缘用白线与背景分割开。降低图片的gamma值后(下图左二)更容易看出白线的笔触。对比B(下图左三)：  
![](img/hukuseigenkou6_B4_crop5.jpg) ![](img/hukuseigenkou6_B4_crop5_gamma.jpg) ![](img/ce_16_50_crop5.jpg)   

- A(下图左一)远景角色的着色。对比B(下图左二)：     
![](img/hukuseigenkou6_B4_crop6.jpg) ![](img/ce_16_50_crop6.jpg)   

- A(下图左一列)的着色。对比B(下图左二列)：     
![](img/hukuseigenkou6_B4_crop7.jpg) ![](img/ce_16_50_crop7.jpg)   
![](img/hukuseigenkou6_B4_crop8.jpg) ![](img/ce_16_50_crop8.jpg)   



---  

<a name="1984-03-i3"></a>  
## CE 官方报道图片01  (1984.03)
![](img/cat_s-eye_tenji_96_01_thumb.jpg)  
周刊少年Jump 1984年17号扉页。  
图片下方有签名"MAR",所以应该是1984年3月。    

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 北条司イラスト集(北条司Illustrations)  

A(下图左一)，B（下图左二）：  
![](img/cat_s-eye_tenji_96_01_thumb.jpg) 
![](img/illustrations_42_thumb.jpg)  


细节：   

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  



---  

<a name="1984-04-i1"></a>  
## CE 原稿 (1984.04)
![](img/FS9m38qaMAAbs9_thumb.jpg)  
Cat's Eye 18卷版第16卷第121话“通告信的秘密”第156页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 18卷版第16卷第121话“通告信的秘密”第156页。  
C: [现场拍摄 - Twitter](https://twitter.com/yae_ch3/status/1526551921489612800)  

A(下图左一)，B(下图左二), C(下图左三、左四)：  
![](img/not_given.jpg) 
![](img/ce_16_156_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/FS9m38qaMAAbs9_thumb.jpg"/> 
](https://pbs.twimg.com/media/FS9m38qaMAAbs9_?format=jpg&name=large) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/FS9m39waUAA-enA_thumb.jpg"/> 
](https://pbs.twimg.com/media/FS9m39waUAA-enA?format=jpg&name=large) 

细节：   

- C中的面部细节：    
![](img/FS9m39waUAA-enA_crop0.jpg)  
![](img/FS9m39waUAA-enA_crop1.jpg)  

- C中不同的条带上的网点方向不同。    
![](img/FS9m39waUAA-enA_crop2.jpg)  

- B(下图左一)对比C(下图左二、左三)腿部的网点阴影。C中看出网点阴影呈pattern，说明使用了多层网点。腿部阴影的边线流畅、有力！        
![](img/ce_16_156_crop3.jpg) 
![](img/FS9m38qaMAAbs9_crop3.jpg) 
![](img/FS9m39waUAA-enA_crop3.jpg)  

- [疑问]这样密集的黑点是如何制作的？手工点的吗？  
![](img/FS9m39waUAA-enA_crop4.jpg)  

- C中有数字"1"、"2"标记。[疑问]这是做什么的？  
![](img/FS9m39waUAA-enA_crop5.jpg) 
![](img/FS9m39waUAA-enA_crop6.jpg)  

- 背景用密集的网点表示夜色，且有渐变。B(下图左一列)对比C(下图左二列)。      
![](img/ce_16_156_crop8.jpg) 
![](img/FS9m38qaMAAbs9_crop8.jpg)  
![](img/ce_16_156_crop7.jpg) 
![](img/FS9m38qaMAAbs9_crop7.jpg)  

- 对于页面左下角的panel，C(下图左二、左三)中看不出着色的笔触。B(下图左一)对比C。    
![](img/ce_16_156_crop9.jpg) 
![](img/FS9m38qaMAAbs9_crop9.jpg) 
![](img/FS9m39waUAA-enA_crop9.jpg)  




---  

<a name="1984-04-i2"></a>  
## CE 複製原稿7 (B4) /Cat's♥Eye  (1984.04)  
![](img/hukuseigenkou7_B4_thumb.jpg)  
完全版第13卷第137集“預告狀的秘密”第？页。  
18卷版第16卷第121话“通告信的秘密”第157页。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript7-b4)及描述：  
“「完全版第13卷第137集：預告狀的秘密」的复制原稿。  
技术：胶印  
材料 纸：高档纸”  

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第16卷第121话“通告信的秘密”第157页。   

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou7_B4_thumb.jpg) 
![](img/ce_16_157_thumb.jpg)   

细节：  

- A(下图左一)黑色旗子上的白色排线。对比B(下图左二)：  
![](img/hukuseigenkou7_B4_crop0.jpg) 
![](img/ce_16_157_crop0.jpg)   

- A(下图左一)金发的着色。对比B(下图左二)：  
![](img/hukuseigenkou7_B4_crop1.jpg) 
![](img/ce_16_157_crop1.jpg)   

- A(下图左一)阴影的渐变更均匀。对比B(下图左二)：  
![](img/hukuseigenkou7_B4_crop3.jpg) 
![](img/ce_16_157_crop3.jpg)   

- A(下图左一)中Cat'sEye logo的眼睛是用白颜料画出的。降低图片的gamma值后(下图左二)更容易看出笔触。对比B(下图左三)：  
![](img/hukuseigenkou7_B4_crop2.jpg) ![](img/hukuseigenkou7_B4_crop2_gamma.jpg) ![](img/ce_16_157_crop2.jpg)     



---  

<a name="1984-05-i1"></a>  
## CE 原稿 (1984.05)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnvggPVEAAMMyS.jpg"/> 
](https://pbs.twimg.com/media/FSnvggPVEAAMMyS?format=jpg&name=large)  
Cat's Eye 18卷版第16卷第122话“夕陽與貓之眼”第187页（黑白）。  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第16卷第122话“夕陽與貓之眼”第187页（黑白）。   
C: [现场实拍 - Twitter](https://twitter.com/TOKYO_GENSO/status/1525013304812244996)  

A(下图左一)，B(下图左二), C：  
![](img/not_given.jpg) 
![](img/ce_16_187_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FSnvggPVEAAMMyS.jpg"/> 
](https://pbs.twimg.com/media/FSnvggPVEAAMMyS?format=jpg&name=large) 

细节：  

- 头发着色层次有多有少。B(下图左一列)对比C(下图左二列)。    
![](img/ce_16_187_crop0.jpg) ![](img/FSnvggPVEAAMMyS_crop0.jpg)  
![](img/ce_16_187_crop2.jpg) ![](img/FSnvggPVEAAMMyS_crop2.jpg)  

- 酒杯的折射效果。  
![](img/FSnvggPVEAAMMyS_crop1.jpg) 

- 衣服的着色。B(下图左一)对比C(下图左二)。  
![](img/ce_16_187_crop3.jpg) ![](img/FSnvggPVEAAMMyS_crop3.jpg) 

- 道具的着色。B(下图左一列)对比C(下图左二列)。  
![](img/ce_16_187_crop4.jpg) ![](img/FSnvggPVEAAMMyS_crop4.jpg)  
![](img/ce_16_187_crop5.jpg) ![](img/FSnvggPVEAAMMyS_crop5.jpg)  
![](img/ce_16_187_crop6.jpg) ![](img/FSnvggPVEAAMMyS_crop6.jpg)  



---  

<a name="1984-05-i2"></a>  
## CE 複製原稿8 (B4) /Cat's♥Eye (1984.05)  
![](img/hukuseigenkou8_B4_thumb.jpg)  
完全版第14卷第140集“夕陽與貓之眼”第？页。  
18卷版第16卷第122话“夕陽與貓之眼”第193页（黑白）。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript8-b4)及描述：  
“「完全版第14卷第140集，夕陽與貓之眼」的复制原稿。”  

[Cat's Eye, Comic manuscript A (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-aset-b4)  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第16卷第122话“夕陽與貓之眼”第193页（黑白）。   
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527637602140327937) 

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/hukuseigenkou8_B4_thumb.jpg) 
![](img/ce_16_193_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNCTH0aQAEmE24.jpg"/> 
](https://pbs.twimg.com/media/FTNCTH0aQAEmE24?format=jpg&name=large) 

细节：  

- A(下图左一)中对话框似乎是被挖空以显示出背面贴上的文字。降低图片的gamma值后(下图左二)更容易看出。  
![](img/hukuseigenkou8_B4_crop0.jpg) ![](img/hukuseigenkou8_B4_crop0_gamma.jpg)    

- A(下图左一)中酒杯的细节1。对比B(下图右一(黑白))：  
![](img/hukuseigenkou8_B4_crop1.jpg) 
![](img/hukuseigenkou8_B4_crop2.jpg) ![](img/hukuseigenkou8_B4_crop3.jpg) 
![](img/hukuseigenkou8_B4_crop4.jpg) 
![](img/hukuseigenkou8_B4_crop5.jpg) ![](img/hukuseigenkou8_B4_crop6.jpg) 
![](img/ce_16_193_crop1.jpg)  
- A(下图左一)中酒杯的细节2。对比B(下图右一(黑白))：  
![](img/hukuseigenkou8_B4_crop7.jpg) 
![](img/hukuseigenkou8_B4_crop8.jpg) 
![](img/hukuseigenkou8_B4_crop9.jpg) ![](img/ce_16_193_crop10.jpg)     

- A(下图左一、左二)中头发的着色细节。对比B(下图左三):    
![](img/hukuseigenkou8_B4_crop11.jpg) ![](img/hukuseigenkou8_B4_crop12.jpg) 
![](img/ce_16_193_crop11.jpg)  

- A(下图左一)中眼部细节。对比B(下图左二):  
![](img/hukuseigenkou8_B4_crop13.jpg) 
![](img/ce_16_193_crop13.jpg)  

- A(下图左一)中嘴细节。对比B(下图左二):  
![](img/hukuseigenkou8_B4_crop14.jpg) 
![](img/ce_16_193_crop14.jpg)  

- A(下图左一)中脸颊侧面的阴影过渡均匀。对比B(下图左二):  
![](img/hukuseigenkou8_B4_crop15.jpg) 
![](img/ce_16_193_crop15.jpg)  

- A(下图左一)中背景纹理渐变均匀。对比B(下图左二):  
![](img/hukuseigenkou8_B4_crop16.jpg) 
![](img/ce_16_193_crop16.jpg)  


---  

<a name="1984-05-i3"></a>  
## CE 複製原稿9 (B4) /Cat's♥Eye  (1984.05)  
![](img/hukuseigenkou9_B4_thumb.jpg)  
完全版第14卷第139话“似曾相識的風”第？页。  
18卷版第17卷第123话“似曾相識的風”第21页。  

[商品链接](https://edition-88.com/products/catseye-duplicateｍanuscript9-b4)及描述：  
“「完全版第14卷第139话：似曾相識的風」的复制原稿。”    

[英文版商品链接 Cat's Eye, Comic manuscript B (B4 size / 6 sheets per set)](https://edition88.com/products/catseye-comicmanuscript-bset-b4)  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 18卷版第17卷第123话“似曾相識的風”第21页（黑白）。   

A(下图左一)，B(下图左二)：  
![](img/hukuseigenkou9_B4_thumb.jpg) 
![](img/ce_17_021_thumb.jpg)     

细节：  

- A(下图左一)在头发网点阴影上洒了白色颜料，这在降低gamma后容易看出(下图左二)。对比B(下图左三)  
![](img/hukuseigenkou9_B4_crop0.jpg) 
![](img/hukuseigenkou9_B4_crop0_gamma.jpg) 
![](img/ce_17_021_crop0.jpg)    

- A(下图左一)不规则的网点pattern应该是覆盖了多层网点纸后的效果。这使得网点阴影加重、阴影有深有浅。对比B(下图左二)。      
![](img/hukuseigenkou9_B4_crop1.jpg) 
![](img/ce_17_021_crop1.jpg)    
![](img/hukuseigenkou9_B4_crop4.jpg) 
![](img/ce_17_021_crop4.jpg)    

- A(下图左一)在海滩上涂抹白色颜料，这在降低gamma后容易看出(下图左二)。对比B(下图左三)  
![](img/hukuseigenkou9_B4_crop2.jpg) 
![](img/hukuseigenkou9_B4_crop2_gamma.jpg) 
![](img/ce_17_021_crop2.jpg)    

- A(下图左一)的裙子上有蓝色，可能是标记裙子上的纹理。对比B(下图左二)。      
![](img/hukuseigenkou9_B4_crop3.jpg) 
![](img/ce_17_021_crop3.jpg)    

- A(下图左一)的月光下的影子用的不是排线阴影。      
![](img/hukuseigenkou9_B4_crop5.jpg) 

- A(下图左一)的裤子上黑色小叉，可能是标记该区域要涂黑。对比B(下图左二)。      
![](img/hukuseigenkou9_B4_crop6.jpg) 
![](img/ce_17_021_crop6.jpg)    

- A(下图左一)的白色发丝。对比B(下图左二)。      
![](img/hukuseigenkou9_B4_crop7.jpg) ![](img/ce_17_021_crop7.jpg)   
![](img/hukuseigenkou9_B4_crop8.jpg) ![](img/ce_17_021_crop8.jpg)   

另，参见[北条司展](../ch_40th/details1.md#1984-05-i0)

---  

<a name="1984-05-i4"></a>  
## CE 插画  （1984.05）
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2bGQeVUAMyTAT.jpg"/> 
](https://pbs.twimg.com/media/FS2bGQeVUAMyTAT?format=jpg&name=large) 

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见来源（未知）。  
C: [现场实拍 - Twitter](https://twitter.com/dasuke05/status/1526046389607624704)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2bGQeVUAMyTAT.jpg"/> 
](https://pbs.twimg.com/media/FS2bGQeVUAMyTAT?format=jpg&name=large) 

细节：  

- C中树叶逼真，笔触细腻。    
![](img/FS2bGQeVUAMyTAT_crop0.jpg)  

- C中的金属扣(下图左一)从远处看(下图左二)逼真。    
![](img/FS2bGQeVUAMyTAT_crop1.jpg) ![](img/FS2bGQeVUAMyTAT_crop2.jpg)  

- C中的面部细节。  
![](img/FS2bGQeVUAMyTAT_crop3.jpg)  
![](img/FS2bGQeVUAMyTAT_crop4.jpg)  

- C中身体透过薄纱的效果。[疑问]这种效果是如何画出的？      
![](img/FS2bGQeVUAMyTAT_crop5.jpg)  

- C中的海浪(下图左一)从远处看(下图左二)逼真。    
![](img/FS2bGQeVUAMyTAT_crop6.jpg) ![](img/FS2bGQeVUAMyTAT_crop7.jpg)  



---  

<a name="1984-07-i1"></a>  
## CE 原稿（约1984.07）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTXzb6zacAA0qax.jpg"/> 
](https://pbs.twimg.com/media/FTXzb6zacAA0qax?format=jpg&name=4096x4096)  
Cat's Eye 18卷版第17卷“冷笑的女神”146页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;  
B: 第17卷第146页。  
C: [现场实拍 - Twitter](https://twitter.com/MituruSouda/status/1528395345436495873)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_17_146_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTXzb6zacAA0qax.jpg"/> 
](https://pbs.twimg.com/media/FTXzb6zacAA0qax?format=jpg&name=4096x4096) 

细节：  

- C中(下图左二)左下角有拼接的痕迹。对比B(下图左一)。     
![](img/ce_17_146_crop0.jpg) 
![](img/FTXzb6zacAA0qax_crop0.jpg)  

- 有一处排线似乎不一致。B(下图左一)对比C(下图左二)。  
![](img/ce_17_146_crop1.jpg) 
![](img/FTXzb6zacAA0qax_crop1.jpg)  

- C中(下图左二)氧气瓶的气泡逼真。对比B(下图左一)。这些气泡是用白色颜料画的，这在降低gamma之后(下图左三)更容易看出。  
![](img/ce_17_146_crop2.jpg) 
![](img/FTXzb6zacAA0qax_crop2.jpg) 
![](img/FTXzb6zacAA0qax_crop2_gamma.jpg) 


---  

<a name="1984-07-i2"></a>  
## CE 原稿 （1984.07）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Fjuad3aaEAASPHo.jpg"/> 
](https://pbs.twimg.com/media/Fjuad3aaEAASPHo?format=jpg&name=large)  
Cat's Eye 18卷版第17卷“冷笑的女神”162-163页。  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;  
B: Cat's Eye 18卷版第17卷第146页。  
C: [现场实拍 - Twitter](https://twitter.com/AoiTakarabako/status/1602043936713109504)    

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/ce_17_162_163_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Fjuad3aaEAASPHo.jpg"/> 
](https://pbs.twimg.com/media/Fjuad3aaEAASPHo?format=jpg&name=large) 

细节：  

- C中有如下图标。[疑问]这些标记是什么意思？    
![](img/B2_fukuseigenko_02_mark2.jpg) 
![](img/B2_fukuseigenko_02_mark1.jpg)  

- C中边缘有手写的文字。  

- C中(下图左二)的雕像整体泛灰色。因为图片分辨率不够大，所以不知道是如何画的[疑问]。对比B(下图左一)。  
![](img/ce_17_162_163_crop0.jpg) 
![](img/Fjuad3aaEAASPHo_crop0.jpg) 





--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
