部分展品的细节目录： 
    [0](./details0.md), 
    [1](./details1.md), 
    [2](./details2.md), 
    [3](./details3.md), 
    [总结](./details3.md#SummaryOfDetails), 

## 「Cat's Eye 40周年纪念原画展」部分展品的细节3  

「Cat's Eye 40周年纪念原画展」以下简称“画展”。  



---  

## 原画 (早于/等于 1991年)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9lYwvagAEUcC5.jpg"/> 
](https://pbs.twimg.com/media/FS9lYwvagAEUcC5?format=jpg&name=large)  
北条司イラスト集(北条司Illustrations)出版于1991年，所以该画创作时间早于/等于1991年。  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526550284796715008)  

A(下图左一)，B(下图左二)：  
![](img/not_given.jpg) 
![](img/illustration_4_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9lYwvagAEUcC5.jpg"/> 
](https://pbs.twimg.com/media/FS9lYwvagAEUcC5?format=jpg&name=large) 

细节：   

- C中(下图左二)能看到白色颜料涂抹的痕迹，这在降低gamma值后(下图左三)更容易看到。对比B(下图左一)。      
![](img/illustration_4_crop0.jpg) 
![](img/FS9lYwvagAEUcC5_crop0.jpg) 
![](img/FS9lYwvagAEUcC5_crop0_gamma.jpg) 

- C中(下图左二)能看到panel边缘处有白色颜料涂抹的痕迹，这在降低gamma值后(下图左三)更容易看到。对比B(下图左一)。      
![](img/illustration_4_crop1.jpg) 
![](img/FS9lYwvagAEUcC5_crop1.jpg) 
![](img/FS9lYwvagAEUcC5_crop1_gamma.jpg) 



---  

## 原画 (早于/等于 1991年)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9lYwragAI728A.jpg"/> 
](https://pbs.twimg.com/media/FS9lYwragAI728A?format=jpg&name=large)  
北条司イラスト集(北条司Illustrations)出版于1991年，所以该画创作时间早于/等于1991年。  

- [日版商品(名古屋展)](https://edition-88.com/products/catseye-artprint11-a4)的描述：  
“A4，美人鱼纸”  

为简便，采用如下符号简记：  
A: [日版商品](https://edition-88.com/products/catseye-artprint11-a4);    
B: 常见来源：北条司イラスト集(北条司Illustrations)；  
C: [现场实拍1 - Twitter](https://twitter.com/yae_ch3/status/1526550284796715008)  
D: [现场实拍2 - Twitter](https://twitter.com/megenna_968/status/1526793131857166336)  
E: [现场实拍3 - Twitter](https://twitter.com/Draichi_/status/1528338113302790145)  
F: [现场实拍4 - Twitter](https://twitter.com/AJK0161737/status/1526855443729235968)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四), E(下图左五), F(下图左六)：  
![](img/thumb/cat_art_print_11_2.jpg) 
![](img/illustrations_6_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9lYwragAI728A.jpg"/> 
](https://pbs.twimg.com/media/FS9lYwragAI728A?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTBCMnRacAAzR7-.jpg"/> 
](https://pbs.twimg.com/media/FTBCMnRacAAzR7-?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FTW_YZtaIAAuZCl.jpg"/> 
](https://pbs.twimg.com/media/FTW_YZtaIAAuZCl?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/FTB67fXacAEM4Bd.jpg"/> 
](https://pbs.twimg.com/media/FTB67fXacAEM4Bd?format=jpg&name=large) 

细节：   

- D中能看到白色颜料涂抹的痕迹(下图左一)。其在降低gamma值后(下图左二)更容易看出。    
![](img/FTBCMnRacAAzR7-_crop0.jpg) 
![](img/FTBCMnRacAAzR7-_crop0_gamma.jpg) 

- A降低gamma值后能看到白色颜料涂抹的痕迹。眼角(下图左一)、发丝（下图左二）、腰带（下图左三）、腿边沿（下图左四）：      
![](img/cat_art_print_11_1_crop0.jpg) 
![](img/cat_art_print_11_1_crop1.jpg)  
![](img/cat_art_print_11_2_crop2.jpg) 
![](img/cat_art_print_11_2_crop3.jpg) 

- A中头发和面部的细节。  
![](img/cat_art_print_11_1_crop4.jpg) 
![](img/cat_art_print_11_2_crop5.jpg)  

- 褶皱处的高光逼真。对比B(下图左一)、D(下图左二)、F(下图左三)、A（下图左四）。    
![](img/illustrations_6_crop3.jpg) 
![](img/FTBCMnRacAAzR7-_crop3.jpg) 
![](img/FTB67fXacAEM4Bd_crop3.jpg) 
![](img/cat_art_print_11_1_crop6.jpg) 

- 褶皱处的高光逼真。对比A(下图左一)，B（下图左二）。    
![](img/cat_art_print_11_1_crop7.jpg) 
![](img/BTS6_crop7.jpg) 

- 有一处发稍是钝的(而不是尖的)。    
![](img/FTBCMnRacAAzR7-_crop4.jpg)  

- D中腰带处褶皱的高光的笔触。      
![](img/FTBCMnRacAAzR7-_crop5.jpg)  

- D中腿部衣服的大面积高光上画了细的褶皱高光。      
![](img/FTBCMnRacAAzR7-_crop6.jpg)  

- A中的背景很漂亮。   
![](img/cat_art_print_11_1_crop8.jpg)    

- A中的“画中画”能看到点阵。    
![](img/cat_art_print_11_2_crop9.jpg)    

---  

## 原画 (早于/等于 1991年)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9lYx6aIAIcbIC.jpg"/> 
](https://pbs.twimg.com/media/FS9lYx6aIAIcbIC?format=jpg&name=large)  
北条司イラスト集(北条司Illustrations)出版于1991年，所以该画创作时间早于/等于1991年。  

- [日版商品(名古屋展)](https://edition-88.com/products/cityhunter-artprint1-a4)的描述：  
“A4，美人鱼纸”  

为简便，采用如下符号简记：  
A: [官方商品](https://edition-88.com/products/cityhunter-artprint1-a4);    
B: 常见来源：北条司イラスト集(北条司Illustrations)；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526550284796715008)  
  
A(下图左一)，B(下图左二)， C(下图左三)：  
![](img/thumb/city_art_print_1_1.jpg) 
![](img/illustrations_5_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9lYx6aIAIcbIC.jpg"/> 
](https://pbs.twimg.com/media/FS9lYx6aIAIcbIC?format=jpg&name=large) 

细节：   

- A中的面部细节：    
![](img/city_art_print_1_1_crop0.jpg)  
![](img/city_art_print_1_1_crop1.jpg) 

- 表现头发潮湿的效果：1）头发的高光与往常不同；2）发丝有更多弯曲。  
![](img/city_art_print_1_1_crop9.jpg) 
![](img/city_art_print_1_1_crop19.jpg) 

- 有两处发丝的线条不流畅。[疑问]作者想用不流畅的线条表现什么？    
![](img/city_art_print_1_1_crop11.jpg) 
![](img/city_art_print_1_1_crop12.jpg) 

- 嘴部：  
![](img/city_art_print_1_1_crop1.jpg) 
![](img/city_art_print_1_1_crop10.jpg) 

- 线条流畅：  
![](img/city_art_print_1_1_crop13.jpg) 
![](img/city_art_print_1_1_crop14.jpg) 

- 半透明衣服贴在身上的质感（左一、左二）。降低gamma值（左三、左四）后能看到白颜料的笔触。    
![](img/city_art_print_1_1_crop6.jpg) 
![](img/city_art_print_1_1_crop7.jpg) 
![](img/city_art_print_1_1_crop4.jpg) 
![](img/city_art_print_1_1_crop5.jpg) 

- 半透明衣服贴在身上的通透感：  
![](img/city_art_print_1_2_crop15.jpg) 
![](img/city_art_print_1_2_crop16.jpg) 
![](img/city_art_print_1_2_crop17.jpg) 
![](img/city_art_print_1_2_crop18.jpg) 

- A中的衣服褶皱高光笔触：  
![](img/city_art_print_1_1_crop2.jpg) 
![](img/city_art_print_1_1_crop3.jpg) 

- 垫子上的高光。  
![](img/city_art_print_1_1_crop20.jpg) 

- 背景：  
![](img/city_art_print_1_1_crop8.jpg) 


---  

## 原画 (早于/等于 1991年)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9lY7cagAA6pQl.jpg"/> 
](https://pbs.twimg.com/media/FS9lY7cagAA6pQl?format=jpg&name=large)  
北条司イラスト集(北条司Illustrations)出版于1991年，所以该画创作时间早于/等于1991年。  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司イラスト集(北条司Illustrations)；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526550284796715008)  
D: [现场实拍 - Twitter](https://twitter.com/yukimitsu/status/1525619305723076608)  
E: [现场实拍 - Twitter](https://twitter.com/Mie72343795/status/1595402943586619392)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四), E(下图左五)：  
![](img/thumb/city_art_print_2_1.jpg) 
![](img/illustrations_7_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9lY7cagAA6pQl.jpg"/> 
](https://pbs.twimg.com/media/FS9lY7cagAA6pQl?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FSwWqwwacAAIcGM.jpg"/> 
](https://pbs.twimg.com/media/FSwWqwwacAAIcGM?format=jpg&name=medium) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FiQBVY7VIAAG-pG.jpg"/> 
](https://pbs.twimg.com/media/FiQBVY7VIAAG-pG?format=jpg&name=medium) 

细节：   

- A中头发的细节。  
![](img/city_art_print_2_1_crop0.jpg) 
![](img/city_art_print_2_1_crop1.jpg) 

- A中面部的细节。  
![](img/city_art_print_2_1_crop2.jpg)  
![](img/city_art_print_2_1_crop3.jpg) 

- A中衣服的褶皱。只在粉红色上衣上喷洒了白色颜料。    
![](img/city_art_print_2_1_crop4.jpg) 

- E（左一、左二）衣服的褶皱与高光，逼真。A中的大图（左三、左四）。    
![](img/FiQBVY7VIAAG-pG_crop4.jpg) 
![](img/FiQBVY7VIAAG-pG_crop5.jpg) 
![](img/city_art_print_2_1_crop5.jpg) 
![](img/city_art_print_2_1_crop6.jpg) 

- A降低gamma值后可看到这里涂了白色颜料，而不是纸的白色。    
![](img/city_art_print_2_1_crop7.jpg)  
作者的意思是：图中的香是真人，她从画框里跳了出来。我可以理解作者的意思，但我的疑问是：[疑问]为了表达上述意思，可以使用下面2种方式。为什么作者选择将这里涂为白色？  
    - ![](img/city_art_print_2_1_kaori-mod0.jpg)  
        上图表示画框里的香是真人，她从画框里跳出。画框内显出了蓝色背景。  
    - ![](img/city_art_print_2_1_kaori-mod1.jpg)  
        上图表示画框里的香不是真人。香在别处看到獠看画后的反映，然后她生气地跑出来。  



---  

## 官网报道中的图片10 (1992.02)
![](img/cityhunter01_0_thumb.jpg)  
CH 35卷版第35卷封面。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)；  
C: [现场实拍 - Twitter](https://twitter.com/chisa_ryoyuki/status/1595254252183560193)  
D: [现场实拍 - Twitter](https://twitter.com/MituruSouda/status/1528396349758390272)  
E: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527628538807209984)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四), E(下图左五)：  
![](img/cityhunter01_0_thumb.jpg) 
![](img/20th_illustrations_049_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiN7Rz0VIAALGXG.jpg"/> 
](https://pbs.twimg.com/media/FiN7Rz0VIAALGXG?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTX0W5RagAIdl64.jpg"/> 
](https://pbs.twimg.com/media/FTX0W5RagAIdl64?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FTM6DG1aMAA-sTH.jpg"/> 
](https://pbs.twimg.com/media/FTM6DG1aMAA-sTH?format=jpg&name=large) 

细节：  

- C的评论：  
"この原画  
今回の展示で1番好きなのに、1番照明が…  
なので、必ず自分が反射する  
出来ればもう少し照明落とすか、角度変えてください"  
(这幅原画。  
在这个展览中我最喜欢它，但灯光是最好的...  
所以我总是看到自己的影子。  
如果可能的话，请将灯光调得更暗一些，或改变角度。)  

- 头发的细节。对比A(下图左一)，B(下图左二), C(下图左三), D(下图左四) 。      
![](img/cityhunter01_0_crop0.jpg) 
![](img/20th_illustrations_049_crop0.jpg) 
![](img/FiN7Rz0VIAALGXG_crop0.jpg) 
![](img/FTX0W5RagAIdl64_crop0.jpg) 

- 眼部的细节。对比A(下图左一)，B(下图左二), C(下图左三), D(下图左四) 。      
![](img/cityhunter01_0_crop1.jpg) 
![](img/20th_illustrations_049_crop1.jpg) 
![](img/FiN7Rz0VIAALGXG_crop1.jpg) 
![](img/FTX0W5RagAIdl64_crop1.jpg) 

- 嘴部的细节。对比A(下图左一)，B(下图左二), C(下图左三), D(下图左四) 。      
![](img/cityhunter01_0_crop1.jpg) 
![](img/20th_illustrations_049_crop2.jpg) 
![](img/FiN7Rz0VIAALGXG_crop2.jpg) 
![](img/FTX0W5RagAIdl64_crop2.jpg) 

- 头发的细节。对比A(下图左一)，B(下图左二), C(下图左三), D(下图左四) 、E(下图左五)。      
![](img/cityhunter01_0_crop3.jpg) 
![](img/20th_illustrations_049_crop3.jpg) 
![](img/FiN7Rz0VIAALGXG_crop3.jpg) 
![](img/FTX0W5RagAIdl64_crop3.jpg) 
![](img/FTM6DG1aMAA-sTH_crop3.jpg) 

- 面部的细节。对比A(下图左一)，B(下图左二), C(下图左三), D(下图左四) 、E(下图左五)。      
![](img/cityhunter01_0_crop3.jpg) 
![](img/20th_illustrations_049_crop4.jpg) 
![](img/FiN7Rz0VIAALGXG_crop4.jpg) 
![](img/FTX0W5RagAIdl64_crop4.jpg) 
![](img/FTM6DG1aMAA-sTH_crop4.jpg) 

- E中的细节。  
![](img/FTM6DG1aMAA-sTH_crop10.jpg)  
![](img/FTM6DG1aMAA-sTH_crop11.jpg) 

- 枪械的细节。对比A(下图左一)，B(下图左二), C(下图左三), D(下图左四) 、E(下图左五)、E降低gamma(下图左五)。      
![](img/cityhunter01_0_crop5.jpg) 
![](img/20th_illustrations_049_crop5.jpg) 
![](img/FiN7Rz0VIAALGXG_crop5.jpg) 
![](img/FTX0W5RagAIdl64_crop5.jpg) 
![](img/FTM6DG1aMAA-sTH_crop5.jpg) 
![](img/FTM6DG1aMAA-sTH_crop5_gamma.jpg) 

- E中的细节。牛仔服上的线(下图左一)、用毛笔画的衣服的褶皱(下图左二)、武器上的伤痕(下图左三)、背景上喷洒的白色颜料(下图左四)及其降低gamma值后(下图左五)的效果。  
![](img/FTM6DG1aMAA-sTH_crop6.jpg) 
![](img/FTM6DG1aMAA-sTH_crop7.jpg) 
![](img/FTM6DG1aMAA-sTH_crop8.jpg) 
![](img/FTM6DG1aMAA-sTH_crop9.jpg) 
![](img/FTM6DG1aMAA-sTH_crop9_gamma.jpg)  
胸前红色衣服上的高光(下图左一)、褶皱上的高光(下图左二)、裤子轮廓边缘的亮光(下图左三)、金属上的光泽(下图左四)、手臂上的着色(下图左五)、手背上的着色(下图左六)。  
![](img/FTM6DG1aMAA-sTH_crop12.jpg) 
![](img/FTM6DG1aMAA-sTH_crop13.jpg) 
![](img/FTM6DG1aMAA-sTH_crop14.jpg) 
![](img/FTM6DG1aMAA-sTH_crop15.jpg) 
![](img/FTM6DG1aMAA-sTH_crop16.jpg) 
![](img/FTM6DG1aMAA-sTH_crop17.jpg) 


---  

## 原画 (作品创作年月未知)  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTGtvDFVIAEgGoc.jpg"/> 
](https://pbs.twimg.com/media/FTGtvDFVIAEgGoc?format=jpg&name=large) 

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：未知。    
C: [现场实拍 - Twitter](https://twitter.com/Rosen_Lizard_/status/1527192778647609344)     

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTGtvDFVIAEgGoc.jpg"/> 
](https://pbs.twimg.com/media/FTGtvDFVIAEgGoc?format=jpg&name=large) 

细节：   

- 头发的细节：   
![](img/FTGtvDFVIAEgGoc_crop2.jpg) 

- 面部的细节：   
![](img/FTGtvDFVIAEgGoc_crop0.jpg)  
![](img/FTGtvDFVIAEgGoc_crop1.jpg) 


---  

## 原画 (1992.06)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNEYZSaUAAVpbC.jpg"/> 
](https://pbs.twimg.com/media/FTNEYZSaUAAVpbC?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527639890728751104)  
D: [现场实拍 - Twitter](https://twitter.com/LiLiY___88/status/1528576944111255552)       

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![](img/not_given.jpg) 
![](img/20th_anni_042_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNEYZSaUAAVpbC.jpg"/> 
](https://pbs.twimg.com/media/FTNEYZSaUAAVpbC?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTaYoC-acAAG9Ux.jpg"/> 
](https://pbs.twimg.com/media/FTaYoC-acAAG9Ux?format=jpg&name=large) 

细节：   

- 丝织物的细节。B(下图左一)对比D(下图左二)。    
![](img/20th_anni_042_crop0.jpg) 
![](img/FTaYoC-acAAG9Ux_crop0.jpg) 

- 手部的细节。B(下图左一)对比C(下图左二)。  
![](img/20th_anni_042_crop1.jpg) 
![](img/FTNEYZSaUAAVpbC_crop1.jpg) 


---  

## 原画 (1993年)  
[<img title="备份缩略图(点击查看原图)" height="100" src="img/thumb/FTHEffiaQAAOhSW.jpg"/>
](https://pbs.twimg.com/media/FTHEffiaQAAOhSW?format=jpg&name=large) 

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)  
C: [现场实拍 - Twitter](https://twitter.com/kirakuni91/status/1527217808375152640)   
D: [现场实拍 - Twitter](https://twitter.com/Polaris1725/status/1643590884188131328)(标明了1993年扉页)   
  
A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/20th_anni_046_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTHEffiaQAAOhSW.jpg"/>
](https://pbs.twimg.com/media/FTHEffiaQAAOhSW?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/Fs81MLEaQAEehpg.jpg"/>
](https://pbs.twimg.com/media/Fs81MLEaQAEehpg?format=jpg&name=large) 

细节：   

- C中(下图左二)发丝上的高光。对比B(下图左一)。    
![](img/20th_anni_046_crop0.jpg) 
![](img/FTHEffiaQAAOhSW_crop0.jpg)  

- C中(下图左二)裤子的线逼真。对比B(下图左一)。  
![](img/20th_anni_046_crop1.jpg) 
![](img/FTHEffiaQAAOhSW_crop1.jpg)  


---  

## 官网报道中的图片13 (1995)

![](img/cityhunter_tenji_96_00_thumb.jpg)  
jBOOKS 1995年CITY HUNTER SPECIAL / 图片海报。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)；  

A(下图左一)，B(下图左二)：  
![](img/cityhunter_tenji_96_00_thumb.jpg) 
![](img/20th_illustrations_040_crop0_thumb.jpg)  

细节：   

- B(下图左二)中的背景比A(下图左一)少显示了一部分。    
![](img/cityhunter_tenji_96_00_crop0.jpg) 
![](img/20th_illustrations_040_crop0_thumb_crop0.jpg)  

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

## 豪华艺术印刷品3（A4）(1994.01)  
![image](img/A4artprint03_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint2-a4)及描述：    
愛蔵版Comics 1994年 第2巻/封面。  
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸”  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文原版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/oMJDmiknIQ1VtRE/status/1526037998558924800)  
D: [现场实拍 - Twitter](https://twitter.com/QQzMG9Ned67rF2G/status/1593792706550976514)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![A](img/A4artprint03_thumb.jpg) 
![B](img/20th_anni_illustrations_056_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2TcX5VsAE5oWN.jpg"/> 
](https://pbs.twimg.com/media/FS2TcX5VsAE5oWN?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/Fh5R2-0VEAIAokM.jpg"/> 
](https://pbs.twimg.com/media/Fh5R2-0VEAIAokM?format=jpg&name=large) 

细节：  

- A左、右、下部均比B显示范围更多，A上部比B显示范围少。  
- A因下部均显示出更多内容，所以右下角显示出签名和时间戳。  
- A(下图左一)稍模糊：  
![](img/A4artprint03_crop0.jpg) 
![](img/20th_anni_illustrations_056_crop0.jpg)  
- A(下图左一)两处白点被涂为红色：  
![](img/A4artprint03_crop1.jpg) 
![](img/20th_anni_illustrations_056_crop1.jpg)  
- A(下图左一)一处白色发丝有凹凸感：  
![](img/A4artprint03_crop2.jpg) 
![](img/20th_anni_illustrations_056_crop2.jpg)  
- A(下图左一)更有通透的效果：  
![](img/A4artprint03_crop3.jpg) 
![](img/20th_anni_illustrations_056_crop3.jpg)  
- A(下图左一)腹部盆骨的凸起的高光更明显：  
![](img/A4artprint03_crop4.jpg) 
![](img/20th_anni_illustrations_056_crop4.jpg)  
- A(下图左一)每条大腿内侧有两列灰色阴影；B(下图左二)每条大腿内侧有一列灰色阴影；：  
![](img/A4artprint03_crop5.jpg) 
![](img/20th_anni_illustrations_056_crop5.jpg)  

- 降低gamma后可看到：白色高光处没有浓重的白颜料痕迹。  



---  

## 豪华艺术印刷品9（A4） （1994） 
![image](img/A4artprint09_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint8-a4)及描述：  
愛蔵版Comics 1944年 第3巻/封面  
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/furaikyo423/status/1527895581875916800)  
D: [现场实拍 - Twitter](https://twitter.com/taker0712/status/1594910678699151360)  

A(下图左一)，B(下图左二), C(下图左三、左四), D(下图左五)：  
![A](img/A4artprint09_thumb.jpg) 
![B](img/20th_anni_illustrations_065_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FTQs7mdaQAAk64O.jpg"/> 
](https://pbs.twimg.com/media/FTQs7mdaQAAk64O?format=jpg&name=large) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FTQs7mfaIAUNcjj.jpg"/> 
](https://pbs.twimg.com/media/FTQs7mfaIAUNcjj?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FiJCziwUYAA9hAS.jpg"/> 
](https://pbs.twimg.com/media/FiJCziwUYAA9hAS?format=jpg&name=large) 


细节：  

- A里的高光笔触：  
![](img/A4artprint09_crop0.jpg) 
![](img/A4artprint09_crop2.jpg) 
![](img/A4artprint09_crop3.jpg) 
![](img/A4artprint09_crop4.jpg)   
- 褶皱逼真。A(下图左一)对比B（下图左二）。    
![](img/A4artprint09_crop1.jpg) 
![](img/20th_anni_illustrations_065_crop1.jpg)   


---  

## 豪华艺术印刷品8（A4）（作品创作年月未知）  
![image](img/A4artprint08_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint7-a4)及描述：  
愛蔵版Comics 1944年 第4巻/封面
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文版)中的该图片；  
C： [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1526553920943824896)，某些部位比A更清晰。    
D： [现场实拍 - Twitter](https://twitter.com/Rosen_Lizard_/status/1527192778647609344)  
E： [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1531558636027543552)  
F： [现场实拍 - Twitter](https://twitter.com/taker0712/status/1594910678699151360)  

A(下图左一)，B(下图左二), C(下图左三、左四), D(下图左五), E(下图左六), F(下图左七)：  
![A](img/A4artprint08_thumb.jpg) 
![B](img/20th_anni_illustrations_061_thumb.jpg) 
[<img title="C1备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9osd9WAAE2VAz.jpg"/> 
](https://pbs.twimg.com/media/FS9osd9WAAE2VAz?format=jpg&name=large) 
[<img title="C2备份缩略图(点击查看原图)" height="100" src="img/thumb/FS9osgAX0AQGwMu.jpg"/> 
](https://pbs.twimg.com/media/FS9osgAX0AQGwMu?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTGtvDEVUAY1IPp.jpg"/> 
](https://pbs.twimg.com/media/FTGtvDEVUAY1IPp?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FUEwdX5aMAEvG5K.jpg"/> 
](https://pbs.twimg.com/media/FUEwdX5aMAEvG5K?format=jpg&name=large) 
[<img title="F备份缩略图(点击查看原图)" height="100" src="img/thumb/FiJCzBeVsAEGWo5.jpg"/> 
](https://pbs.twimg.com/media/FiJCzBeVsAEGWo5?format=jpg&name=large) 

细节：  

- 无签名和时间戳。  

- 以下两图里女子服饰的款式和颜色似乎有相似之处：    
![](img/20th_anni_043_thumb.jpg) ![](img/A4artprint08_thumb.jpg) 

- A下部的显示范围比B、C少，导致A的手指未显示完全。  

- A(下图左一)大片的黑色区域有不规则的反光，这可能是由纸张导致的。  
![A](img/A4artprint08_thumb.jpg) 
![B](img/20th_anni_illustrations_061_thumb.jpg)  

- A(下图左一列)比B(下图左二列)的某些部分更暗：  
![A](img/A4artprint08_crop0.jpg) 
![B](img/20th_anni_illustrations_061_crop0.jpg)  
![A](img/A4artprint08_crop1.jpg) 
![B](img/20th_anni_illustrations_061_crop1.jpg)  

- A中边线外侧的黑色笔触：    
![A](img/A4artprint08_crop2.jpg) 
![A](img/A4artprint08_crop3.jpg)  

- A(下图左一)左上角比B(下图左二)的渐变更均匀：  
![A](img/A4artprint08_crop4.jpg) 
![B](img/20th_anni_illustrations_061_crop4.jpg)  

- 1）嘴下方的颈部肌肉线条是弯曲的，这种画法有些特别。2）锁骨末端很突出。     
![A](img/A4artprint08_crop5.jpg)  

- A(下图左一)胳膊与背景都为黑色，但用不同的反光来区分。    
![A](img/A4artprint08_crop6.jpg)  

- A(下图左一列)对比C(下图左二列)：  
![A](img/A4artprint08_crop10.jpg) 
![A](img/FS9osd9WAAE2VAz_crop10.jpg)  
![A](img/A4artprint08_crop7.jpg) 
![A](img/FS9osd9WAAE2VAz_crop7.jpg)  
![A](img/A4artprint08_crop9.jpg) 
![A](img/FS9osd9WAAE2VAz_crop9.jpg)  
![A](img/A4artprint08_crop8.jpg) 
![A](img/FS9osgAX0AQGwMu_crop8.jpg)  


---  

## 豪华艺术印刷品 5（A4）  （1994.01）
![image](img/A4artprint05_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint4-a4)及描述：     
愛蔵版Comics 1994年 第5巻/封面
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文原版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527642080574849026) 

A(下图左一)，B（下图左二）, C(下图左三)：  
![A](img/A4artprint05_thumb.jpg) 
![B](img/20th_anni_illustrations_063_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNGXq2agAAJvgz.jpg"/> 
](https://pbs.twimg.com/media/FTNGXq2agAAJvgz?format=jpg&name=large) 

细节：  

- A比B左侧、上部显示范围稍少。  
- A(下图左一)的左上角更蓝、颗粒感更弱。对比B(下图左二)。  
![A](img/A4artprint05_crop0.jpg) 
![B](img/20th_anni_illustrations_063_crop0.jpg)  
- A(下图左一)的衣服细节更多。对比B(下图左二)。    
![A](img/A4artprint05_crop1.jpg) 
![B](img/20th_anni_illustrations_063_crop1.jpg)  
- A(下图左一列)的皮肤上有灰色阴影。对比B(下图左二列)。     
![A](img/A4artprint05_crop2.jpg) 
![B](img/20th_anni_illustrations_063_crop2.jpg)  
![A](img/A4artprint05_crop4.jpg) 
![B](img/20th_anni_illustrations_063_crop4.jpg)  
![A](img/A4artprint05_crop3.jpg) 
![B](img/20th_anni_illustrations_063_crop3.jpg)  
- A(下图左一)的高光的笔触。对比B(下图左二)。  
![A](img/A4artprint05_crop5.jpg) 
![B](img/20th_anni_illustrations_063_crop5.jpg)  


---  

## 豪华艺术印刷品4（A4） （1994.02）  
![image](img/A4artprint04_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint3-a4)及描述：   
愛蔵版Comics 1994年 第6巻/封面  
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文原版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/oMJDmiknIQ1VtRE/status/1526037998558924800)  
D: [现场实拍 - Twitter](https://twitter.com/thelowtierchara/status/1528027705265950720) 

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![A](img/A4artprint04_thumb.jpg) 
![B](img/20th_anni_illustrations_058_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS2TcX0UcAAMGO5.jpg"/> 
](https://pbs.twimg.com/media/FS2TcX0UcAAMGO5?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FTSios7UcAAgYYb.jpg"/> 
](https://pbs.twimg.com/media/FTSios7UcAAgYYb?format=jpg&name=large) 

细节：  

- A比B左侧显示范围稍多，A比B上部显示范围稍少。  
- A、B的左下角均有签名和时间戳，但其他版本(例如，B的中文版、北条司美女写真馆)中的该图无签名和时间戳。    
- A的整体颜色更白，可能是介绍中说的“在Cat'sEye版画中，每幅版画都是用白颜料手工上色”；  
- A(下图左一)的黑色背景不均匀，可能和用纸有关。从[官方介绍](./about_edition88.md)来看，该效果可能更接近原画。对比B(下图左二)。     
![A](img/A4artprint04_crop0.jpg) 
![B](img/20th_anni_illustrations_058_crop0.jpg)  
- A(下图左一)的灰色阴影的范围更大。对比B(下图左二)。    
![A](img/A4artprint04_crop1.jpg) 
![B](img/20th_anni_illustrations_058_crop1.jpg)  
- A(下图左一)的颗粒感更弱、月球的黑色边线更不明显。对比B(下图左二)。  
![A](img/A4artprint04_crop2.jpg) 
![B](img/20th_anni_illustrations_058_crop2.jpg)  
- A中泪的头部剪影(下图左一)的画法：在黑色基色上加少量发丝、喷墨。爱的头部剪影(下图左二)的画法：在黑色基色上加少量发丝、喷墨、重影。    
![A](img/A4artprint04_crop3.jpg) 
![A](img/A4artprint04_crop4.jpg)  
- A(下图左一)的内侧眼角处能看到2~3根睫毛。对比B(下图左二)。  
![A](img/A4artprint04_crop6.jpg) 
![B](img/20th_anni_illustrations_058_crop6.jpg)  
- A(下图左一)的猫眼标志上左半部有白色涂抹的痕迹。对比B(下图左二)。    
![A](img/A4artprint04_crop7.jpg) 
![B](img/20th_anni_illustrations_058_crop7.jpg)  
- A(下图左一)的指甲高光能和指甲区分开。对比B(下图左二)。     
![A](img/A4artprint04_crop8.jpg) 
![B](img/20th_anni_illustrations_058_crop8.jpg)  
- A(下图左一)的发丝更不透明（没有透过背景月球的暗影）。对比B(下图左二)。    
![A](img/A4artprint04_crop10.jpg) 
![B](img/20th_anni_illustrations_058_crop10.jpg)     
- A(下图左一)的高光的笔触清晰可见：    
![A](img/A4artprint04_crop9.jpg) 
![A](img/A4artprint04_crop11.jpg) 
- 泪的剪影的臀部的这个形状意味着她背面更多地朝向镜头。尖角为左右臀部形成的。  
![A](img/A4artprint04_crop5.jpg)  

- 降低gamma后可看到：白色高光处没有浓重的白颜料痕迹。  


---  

## 豪华艺术印刷品6（A4） （1994.02） 
![image](img/A4artprint06_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint5-a4)描述：  
愛蔵版Comics 1994年 第7巻/封面
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文原版)中的该图片；  

A(下图左一)，B（下图左二）：  
![A](img/A4artprint06_thumb.jpg) 
![B](img/20th_anni_illustrations_060_thumb.jpg)   

细节：  

- A比B右侧显示范围更多。  
- A(下图左一)的高光的笔触。对比B(下图左二)。  
![A](img/A4artprint06_crop0.jpg) 
![B](img/20th_anni_illustrations_060_crop0.jpg)   
- A(下图左一)的毛发的着色细节更丰富。对比B(下图左二)。  
![A](img/A4artprint06_crop1.jpg) 
![B](img/20th_anni_illustrations_060_crop1.jpg)   
- A(下图左一)的肤色更逼真。B(下图左二)的肤色泛红。    
![A](img/A4artprint06_crop2.jpg) 
![B](img/20th_anni_illustrations_060_crop2.jpg)   

- 降低gamma后可看到：白色高光处没有浓重的白颜料痕迹。  


---  

## 豪华艺术印刷品11（A4） （1994.02） 
![image](img/A4artprint11_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint10-a4)及描述：  
愛蔵版Comics 1994年 第8巻/封面  
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527642080574849026) 

A(下图左一)，B（下图左二）,C(下图左三)：  
![](img/A4artprint11_thumb.jpg) 
![](img/20th_anni_illustrations_064_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNGX4caQAYXEwy.jpg"/> 
](https://pbs.twimg.com/media/FTNGX4caQAYXEwy?format=jpg&name=large) 

细节：  

- 褶皱的笔触--A(下图左一)，B（下图左二）：     
![](img/A4artprint11_crop0.jpg) 
![](img/20th_anni_illustrations_064_crop0.jpg)    
- 头发的细节--A(下图左一)，B（下图左二）：  
![](img/A4artprint11_crop1.jpg) 
![](img/20th_anni_illustrations_064_crop1.jpg)    
- 头发的细节--A(下图左一)，B（下图左二）：  
![](img/A4artprint11_crop2.jpg) 
![](img/20th_anni_illustrations_064_crop2.jpg)     

- 降低gamma后可看到：白色高光处没有浓重的白颜料痕迹。  


---  

## 豪华艺术印刷品7（A4）  (1994.03)
![image](img/A4artprint07_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint6-a4)及描述：  
愛蔵版Comics 1994年 第9巻/封面  
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527642080574849026)  
D: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1531559103906344962)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![A](img/A4artprint07_thumb.jpg) 
![B](img/20th_anni_illustrations_062_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNGXqOakAEagW-.jpg"/> 
](https://pbs.twimg.com/media/FTNGXqOakAEagW-?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FUEw4i-akAEgO_-.jpg"/> 
](https://pbs.twimg.com/media/FUEw4i-akAEgO_-?format=jpg&name=large) 

细节：  

- A右侧显示范围更多，A左侧显示范围稍少。  
- B（下图左二）右下角有时间戳： TSUKASA '94 Mar。从背景墨点的对应关系推测，A该处显示范围足够，但缺少签名。  
![A](img/A4artprint07_crop0.jpg) 
![B](img/20th_anni_illustrations_062_crop0.jpg)   
- A（下图左一）背景的墨渍更清晰。对比B(下图左二)。    
![A](img/A4artprint07_crop1.jpg) 
![B](img/20th_anni_illustrations_062_crop1.jpg)   
- A的手镯上的光影的笔触：    
![A](img/A4artprint07_crop2.jpg)  

- 降低gamma后可看到：除金属高光外，其余白色高光处没有浓重的白颜料痕迹。  


---  

## 豪华艺术印刷品10（A4） （1994.03） 
![image](img/A4artprint10_thumb.jpg)  

[日文版商品(名古屋展)](https://edition-88.com/products/catseye-artprint9-a4)及描述：    
愛蔵版Comics 1944年 第10巻/封面  
[英文版商品(博多展)](https://edition88.com/products/catseye-artprint1-set-a4)及描述：  
"Marmaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527637602140327937)  
D: [现场实拍 - Twitter](https://twitter.com/taker0712/status/1594910678699151360)  

A(下图左一)，B(下图左二), C(下图左三), D(下图左四)：  
![](img/A4artprint10_thumb.jpg) 
![](img/20th_anni_illustrations_059_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNCTAQacAAiSa_.jpg"/> 
](https://pbs.twimg.com/media/FTNCTAQacAAiSa_?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FiJCyUiUUAA830s.jpg"/> 
](https://pbs.twimg.com/media/FiJCyUiUUAA830s?format=jpg&name=large) 

细节：  

- A的左侧和右侧比B的范围稍大。  
- A(下图左一)比B(下图左二)有些局部呈现出横向条纹，可能和用纸有关。    
![](img/A4artprint10_crop0.jpg) 
![](img/20th_anni_illustrations_059_crop0.jpg)  
- A(下图左一)比B(下图左二)有些局部更均匀：    
![](img/A4artprint10_crop1.jpg) 
![](img/20th_anni_illustrations_059_crop1.jpg)   
- 高光笔触：A(下图左一)、B(下图左二)：  
![](img/A4artprint10_crop2.jpg) 
![](img/20th_anni_illustrations_059_crop2.jpg)   
- 高光笔触：A(下图左一)、B(下图左二)：      
![](img/A4artprint10_crop6.jpg) 
![](img/20th_anni_illustrations_059_crop6.jpg)   
- A里下图的效果可能源于其纸张的纹理：    
![](img/A4artprint10_crop3.jpg) 
![](img/A4artprint10_crop4.jpg)  
- A(下图左一)比B(下图左二)的背部的阴影更重：    
![](img/A4artprint10_crop5.jpg) 
![](img/20th_anni_illustrations_059_crop5.jpg)   

- 降低gamma后可看到：除金属和星光外，其余白色高光处没有浓重的白颜料痕迹。  



---  

## 原画 (1994.07)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTGtvDEUcAAjbU_.jpg"/> 
](https://pbs.twimg.com/media/FTGtvDEUcAAjbU_?format=jpg&name=large)  

[《经典品鉴|“新宿的清道夫”城市猎人——北条司周年画集精选 下》](https://mp.weixin.qq.com/s/oWV5pgpNNcdjcVT5CG6GDg)一文中该图左下角显示签名：TSUKASA '94 JUL。所以该作品的创作日期为1994年07月。  

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)  
C: [现场实拍 - Twitter](https://twitter.com/Rosen_Lizard_/status/1527192778647609344)   

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/20th_anni_043_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTGtvDEUcAAjbU_.jpg"/> 
](https://pbs.twimg.com/media/FTGtvDEUcAAjbU_?format=jpg&name=large) 

细节：   

- C中左下角的签名(和时间戳?)未显示完整。  

- 以下两图女子服饰的款式和颜色似乎有相似之处：    
![](img/20th_anni_043_thumb.jpg) ![](img/A4artprint08_thumb.jpg) 

- 发丝的细节。B(下图左一)对比C(下图左二)。    
![](img/20th_anni_043_crop0.jpg) 
![](img/FTGtvDEUcAAjbU__crop0.jpg) 

- 眼部的细节。B(下图左一)对比C(下图左二)。    
![](img/20th_anni_043_crop1.jpg) 
![](img/FTGtvDEUcAAjbU__crop1.jpg) 

- 鼻子的细节。B(下图左一)对比C(下图左二)。    
![](img/20th_anni_043_crop2.jpg) 
![](img/FTGtvDEUcAAjbU__crop2.jpg) 

- 颈部的细节。B(下图左一)对比C(下图左二)。    
![](img/20th_anni_043_crop3.jpg) 
![](img/FTGtvDEUcAAjbU__crop3.jpg) 

- 项链的细节。B(下图左一)对比C(下图左二)。    
![](img/20th_anni_043_crop4.jpg) 
![](img/FTGtvDEUcAAjbU__crop4.jpg) 

- 手指的细节。B(下图左一列)对比C(下图左二列)。    
![](img/20th_anni_043_crop5.jpg) 
![](img/FTGtvDEUcAAjbU__crop5.jpg)  
![](img/20th_anni_043_crop7.jpg) 
![](img/FTGtvDEUcAAjbU__crop7.jpg) 

- 金属的质感。B(下图左一)对比C(下图左二)。    
![](img/20th_anni_043_crop6.jpg) 
![](img/FTGtvDEUcAAjbU__crop6.jpg) 


---  

## 原画 （1995） 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTQs7mtagAABVVk.jpg"/> 
](https://pbs.twimg.com/media/FTQs7mtagAABVVk?format=jpg&name=large) 

为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)。    
C: [现场实拍 - Twitter](https://twitter.com/furaikyo423/status/1527895581875916800)          

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/20th_anni_045_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTQs7mtagAABVVk.jpg"/> 
](https://pbs.twimg.com/media/FTQs7mtagAABVVk?format=jpg&name=large) 

细节：   

- C中的细节。B(下图左一列)对比C(下图左二列)。    
![](img/20th_anni_045_crop0.jpg) ![](img/FTQs7mtagAABVVk_crop0.jpg)  
![](img/20th_anni_045_crop1.jpg) ![](img/FTQs7mtagAABVVk_crop1.jpg)  
![](img/20th_anni_045_crop2.jpg) ![](img/FTQs7mtagAABVVk_crop2.jpg)  
![](img/20th_anni_045_crop3.jpg) ![](img/FTQs7mtagAABVVk_crop3.jpg)  
![](img/20th_anni_045_crop4.jpg) ![](img/FTQs7mtagAABVVk_crop4.jpg)  
![](img/20th_anni_045_crop6.jpg) ![](img/FTQs7mtagAABVVk_crop6.jpg)  

- C中(下图左二)衣服透光的细节。应该是覆盖了白色颜料，这在降低图片gamma值后(下图左三)容易看出。    
![](img/20th_anni_045_crop5.jpg) ![](img/FTQs7mtagAABVVk_crop5.jpg) ![](img/FTQs7mtagAABVVk_crop5_gamma.jpg)   


---  

## 原画（作品创作年月未知）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTRy7nXaQAIdBSs.jpg"/> 
](https://pbs.twimg.com/media/FTRy7nXaQAIdBSs?format=jpg&name=large) 


为简便，采用如下符号简记：  
A: 官方图片（暂无）;    
B: 常见来源：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations) 。    
C: [现场实拍 - Twitter](https://twitter.com/MituruSouda/status/1527972554526568448)           

A(下图左一)，B(下图左二)， C(下图左三)：  
![](img/not_given.jpg) 
![](img/20th_anni_51_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTRy7nXaQAIdBSs.jpg"/> 
](https://pbs.twimg.com/media/FTRy7nXaQAIdBSs?format=jpg&name=large) 

细节：   

- C中的细节。B(下图左一列)对比C(下图左二列)。    
头发上加了白色细线表现高光：    
![](img/20th_anni_51_crop0.jpg) ![](img/FTRy7nXaQAIdBSs_crop0.jpg)  
鬓角加了黑色排线阴影：  
![](img/20th_anni_51_crop1.jpg) ![](img/FTRy7nXaQAIdBSs_crop1.jpg)  
面部侧面有暗色排线阴影：  
![](img/20th_anni_51_crop2.jpg) ![](img/FTRy7nXaQAIdBSs_crop2.jpg)  
枪支着色逼真：  
![](img/20th_anni_51_crop3.jpg) ![](img/FTRy7nXaQAIdBSs_crop3.jpg)  
![](img/20th_anni_51_crop4.jpg) ![](img/FTRy7nXaQAIdBSs_crop4.jpg)   



---  

## 原画 (1996)  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS3TFPMUEAAMXS_.jpg"/> 
](https://pbs.twimg.com/media/FS3TFPMUEAAMXS_?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: [官方blog - Twitter](https://twitter.com/cityhunter100t/status/1526107940637114368)  ;    
B: 常见来源:未知；  

A(下图左一)，B（下图左二）：  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS3TFPMUEAAMXS_.jpg"/> 
](https://pbs.twimg.com/media/FS3TFPMUEAAMXS_?format=jpg&name=large) 
![](img/not_given.jpg)  

细节:  

- [官方blog - Weibo](https://m.weibo.cn/detail/4770162420486881)中的评论：  
"下图出自小说版猫眼三姐妹  
将瞳的头发作为剪影，表现夜景中奔跑的三姐妹  
电子绘画的话或许可以通过蒙版处理来绘制，手绘的话这样的表现力、精细度……超级厉害。  
在原画展就有机会观赏原始尺寸‼"  


---  

## 原画 （作品创作年月未知）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS6DxM6aMAECD1w.jpg"/> 
](https://pbs.twimg.com/media/FS6DxM6aMAECD1w?format=jpg&name=large) 

为简便，采用如下符号简记：  
A: 官方图片（暂无）    
B: 常见来源可能是猫眼小说版；  
C：[现场实拍 - Twitter](https://twitter.com/AoiTakarabako/status/1526302213228085249)  

A(下图左一)，B（下图左二）：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FS6DxM6aMAECD1w.jpg"/> 
](https://pbs.twimg.com/media/FS6DxM6aMAECD1w?format=jpg&name=large) 

细节:  

- 泪、瞳的面部特征类似F.Compo里的若苗紫和若苗紫苑。    
![](img/FS6DxM6aMAECD1w_crop0.jpg) ![](img/FS6DxM6aMAECD1w_crop2.jpg)   
![](img/FS6DxM6aMAECD1w_crop1.jpg) ![](img/FS6DxM6aMAECD1w_crop3.jpg)  

---  

## 原画  （作品创作年月未知）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNBh7RakAE88yA.jpg"/> 
](https://pbs.twimg.com/media/FTNBh7RakAE88yA?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见出处不详。  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527636760104402945)  
  
A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNBh7RakAE88yA.jpg"/> 
](https://pbs.twimg.com/media/FTNBh7RakAE88yA?format=jpg&name=large) 

细节：  

- C中光芒的笔触：  
![](img/FTNBh7RakAE88yA_crop0.jpg)   

- C(下图左一)中在网点阴影上添加了白点，这在降低gamma值(下图左二)后更容易看出。    
![](img/FTNBh7RakAE88yA_crop1.jpg) 
![](img/FTNBh7RakAE88yA_crop1_gamma.jpg)  

- C中某些光芒周围有圆形的白色光晕。  
![](img/FTNBh7RakAE88yA_crop2.jpg) 

- C中的面部细节。  
![](img/FTNBh7RakAE88yA_crop4.jpg)  

- C中棉衣的质感。  
![](img/FTNBh7RakAE88yA_crop3.jpg)  



---  

## 原画 （作品创作年月未知）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNBh8laIAE9kvJ.jpg"/> 
](https://pbs.twimg.com/media/FTNBh8laIAE9kvJ?format=jpg&name=large) 

为简便，采用如下符号简记：  
A: 官方原画（暂无）;    
B: 常见出处不详。  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1527636760104402945)    
  
A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FTNBh8laIAE9kvJ.jpg"/> 
](https://pbs.twimg.com/media/FTNBh8laIAE9kvJ?format=jpg&name=large) 

细节：  

- C中光芒的细节：  
![](img/FTNBh8laIAE9kvJ_crop0.jpg) 

- C中呼出的蒸汽逼真。这种效果是如何画的？[疑问]    
![](img/FTNBh8laIAE9kvJ_crop1.jpg) 

- C中头发的细节。网点的细节。  
![](img/FTNBh8laIAE9kvJ_crop2.jpg) 

- C中棉衣的质感。  
![](img/FTNBh8laIAE9kvJ_crop3.jpg) 

- C中部分网点处有交错的暗线。这是什么？[疑问]    
![](img/FTNBh8laIAE9kvJ_crop4.jpg) 

- 角色的面像类似『F.Compo』里紫苑和雅彦，所以，我猜，该画作可能创作于『F.Compo』前后。    
![](img/FTNBh8laIAE9kvJ_crop6.jpg) ![](img/FTNBh8laIAE9kvJ_crop5.jpg) 


---  

## 高級アートプリント1 (B2) /Cat's♥Eye (1996.10)  
![](img/B2_Artprint_01_thumb.jpg)  
本画展的这张图（上图）以前多见于画册《北条司漫画家20周年記念》(20th anniversary illustrations)和《北条司美女写真馆》。

[日文版商品链接 高級アートプリント1 (B2) /キャッツ♥アイ](https://edition-88.com/products/catseye-artprint1-b2)  
[英文版商品链接 Cat's Eye, Art print #1 (B2)](https://edition-88.com/products/catseye-artprint1-b2)及描述：  
"Mermaid纸"  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/yae_ch3/status/1531558128936173568) 

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/B2_Artprint_01_thumb.jpg) 
![](img/20th_anni_illustrations_057_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FUEv_1SaAAATkSb.jpg"/> 
](https://pbs.twimg.com/media/FUEv_1SaAAATkSb?format=jpg&name=large) 

细节：  

- A（下图左一）的底部比B（下图左二）少一部分：  
![](img/B2_Artprint_01_crop1.jpg) 
![](img/20th_illustrations_057_crop1.jpg)  

- A（下图左一）修复了B（下图左二）中角色左膝盖处的色彩瑕疵。  
![](img/B2_Artprint_01_crop0.jpg) 
![](img/20th_illustrations_057_crop0.jpg)  

- A（下图左一）与B（下图左二）相比，A的右手中指半截是白色（瑕疵？）：  
![](img/B2_Artprint_02_crop2.jpg) 
![](img/20th_illustrations_057_crop2.jpg)  

- 由于分辨率巨大，A（下图左一列）比B（下图左二列）显示出上色的笔触，例如：  
    - 1）衣服上的浅色遮盖了一缕发丝，  
    ![](img/B2_Artprint_02_crop3.jpg) ![](img/20th_illustrations_057_crop3.jpg)      
    - 2）衣服上的浅色遮盖了一部分衣服的黑色描线。  
    ![](img/B2_Artprint_02_crop4.jpg) ![](img/20th_illustrations_057_crop4.jpg)  
    ![](img/B2_Artprint_02_crop5.jpg) ![](img/20th_illustrations_057_crop4.jpg)  

- 降低gamma后可看到：大多数白色高光处没有浓重的白颜料痕迹。  


---  

## 原画 （1996.11）
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Fik7bZHaYAEnFsN.jpg"/> 
](https://pbs.twimg.com/media/Fik7bZHaYAEnFsN?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 《北条司漫画家20周年記念 イラストレーションズ》(20th Anniversary Illustrations)(日文版)中的该图片；  
C: [现场实拍 - Twitter](https://twitter.com/berrypafe/status/1596872896932384768)  
D: [现场实拍 - Twitter](https://twitter.com/tohnomiyuki00/status/1595791673493176324)  
E: [现场实拍 - Twitter](https://twitter.com/thelowtierchara/status/1528027698114834434)  

A(下图左1)，B(下图左2), C(下图左3), D(下图左4), E(下图左5)：  
![](img/not_given.jpg) 
![](img/20th_anni_055_thumb.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/Fik7bZHaYAEnFsN.jpg"/> 
](https://pbs.twimg.com/media/Fik7bZHaYAEnFsN?format=jpg&name=large) 
[<img title="D备份缩略图(点击查看原图)" height="100" src="img/thumb/FiVj6-UVUAAh9_d.jpg"/> 
](https://pbs.twimg.com/media/FiVj6-UVUAAh9_d?format=jpg&name=large) 
[<img title="E备份缩略图(点击查看原图)" height="100" src="img/thumb/FTSilYDVsAAIP2h.jpg"/> 
](https://pbs.twimg.com/media/FTSilYDVsAAIP2h?format=jpg&name=4096x4096) 

细节：  

- D中的评论：  
"デビュー当初からの高い画力、構図、センス、色彩感覚。  
今展には出て無かったが、デビュー前の手塚賞応募作品ですでにプロレベルだった。"  
(自出道以来，他的绘画能力、构图、风格感和色彩感都很高。  
虽然他没有被列入这次展览，但他在出道前的手冢奖作品已经达到了专业水平。)  

- 裤子着色的笔触。对比B(下图左一),C(下图左二),E(下图左3)。  
![](img/20th_anni_055_crop0.jpg) 
![](img/Fik7bZHaYAEnFsN_crop0.jpg) 
![](img/FTSilYDVsAAIP2h_crop0.jpg) 

- C中显示出喷洒的白色墨点。  
![](img/Fik7bZHaYAEnFsN_crop1.jpg) 

- 褶皱逼真。B(下图左一)对比C(下图左二)。  
![](img/20th_anni_055_crop2.jpg) 
![](img/Fik7bZHaYAEnFsN_crop2.jpg) 


---  

## 原画 （作品创作年月未知）  
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiQP9MwUYAIEOhn.jpg"/> 
](https://pbs.twimg.com/media/FiQP9MwUYAIEOhn?format=jpg&name=large)  

为简便，采用如下符号简记：  
A: 本画展该展品在官网的图片；  
B: 常见来源：未知；  
C: [现场实拍 - Twitter](https://twitter.com/mihohi1/status/1595417717154385922)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/not_given.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/FiQP9MwUYAIEOhn.jpg"/> 
](https://pbs.twimg.com/media/FiQP9MwUYAIEOhn?format=jpg&name=large) 

细节：  

- C中头发的细节。  
![](img/FiQP9MwUYAIEOhn_crop0.jpg) 
![](img/FiQP9MwUYAIEOhn_crop1.jpg) 
![](img/FiQP9MwUYAIEOhn_crop2.jpg) 

- C中眼部的细节。  
![](img/FiQP9MwUYAIEOhn_crop3.jpg) 
![](img/FiQP9MwUYAIEOhn_crop4.jpg) 
![](img/FiQP9MwUYAIEOhn_crop5.jpg) 

- C中的面部能看到纸张的凹凸。  
![](img/FiQP9MwUYAIEOhn_crop6.jpg) 

- 用白色颜料画线表示枪支上的划痕。  
![](img/FiQP9MwUYAIEOhn_crop7.jpg) 

- C中高光的笔触。  
![](img/FiQP9MwUYAIEOhn_crop8.jpg) 
![](img/FiQP9MwUYAIEOhn_crop9.jpg)  
![](img/FiQP9MwUYAIEOhn_crop10.jpg)  
![](img/FiQP9MwUYAIEOhn_crop11.jpg) 

---  

## 官网报道中的图片 (1999.10)

![](img/cityhunter01_2_thumb.jpg)  
City Hunter 2000年 Perfect Guide Book封面。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 常见来源：北条司漫画家20周年記念 イラストレーションズ(20th anniversary illustrations)；  

A(下图左一)，B（下图左二）：  
![](img/cityhunter01_2_thumb.jpg) 
![](img/20th_anni_044_thumb.jpg)  

细节：   

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

## Space Angel  

- 遗憾的是中部配文看不清楚。   
[<img title="(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2821ugft2j20ku0dw3z8.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821ugft2j20ku0dw3z8.jpg) 
[<img title="(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04138_600x600.jpg"/>
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04138_600x600.jpg)  (译注：[大图,可惜看不清配文](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04138_4800x4800.jpg) )   

- 相比Arts千代田场展，博多场展的《Space Angel》的展品更紧凑。  
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_4800x4800.jpg))   

- [官方报道(博多场)](https://edition-88.com/blogs/blog/catseye40th-report02)里可以看到新画的《Space Angel》的全部画面(小图)。可以看到其中使用了[新的黑洞形状](https://iopscience.iop.org/article/10.1088/0264-9381/32/6/065001)：     
![](img/space_angel_crop0.jpg)  


---  

## 官网报道中的图片 (作品创作年月未知)

![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_3_thumb.jpg)  
新版Space Angel的封面。    

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: 新版Space Angel的封面；  
C: [现场实拍 - Twitter](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)  

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_3_thumb.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2823vsca0j20ku0v9gop.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2823vsca0j20ku0v9gop.jpg) 

细节：   

- A中"Space Angel"的字样似乎在模仿影片《Star Wars》(1977)，后者的特点是：["S"和"R"的尾巴都被拉到了两侧](https://blog.logomyway.com/star-wars-logo/)：  
![](img/cat_s-eye-tenji_blogbanner_46d8a6ca-e388-431e-9af1-4f84625d0a60_3_crop0.jpg) 
![](img/star_wars_logo.jpg)  

- 如果按照《Star Wars》的字体风格，"SPACE ANGEL"可以写成下图左一。但A图中"ANGEL"的"L"的尾巴似乎拉成了"S"，且"L"的尾巴与"S"的尾巴之间有竖线间隔。所以更像是写成了"SPACE ANGELS"(下图左二)：  
![](img/my_space_angel1.jpg) 
![](img/my_space_angel2.jpg)  

- 角色的服饰似乎在模仿影片《Star Wars》(1977)中的Jedi的服饰。  

- [官方blog - Weibo](https://m.weibo.cn/detail/4770169039884953)  
"仔细观察的话，原画展上其他的画也能发现利用复印机的纹理加工技术留下的粒子痕迹  
特别是全手绘完成的最新版《空间天使》  
这种技术的活用无处不在！"  

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

## 官网报道中的图片 (作品创作年月未知)
![](img/spaceangel_tenji_360_1_thumb.jpg)  
新版Space Angel。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: [官网 - Twitter](https://twitter.com/cityhunter100t/status/1526386475398356993)；  
C: [现场实拍 - weibo](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)   

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/spaceangel_tenji_360_1_thumb.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2821ugft2j20ku0dw3z8.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821ugft2j20ku0dw3z8.jpg)  

细节：   

- 所用纸张：DELETER  
![](img/spaceangel_tenji_360_1_crop0.jpg) 
![](img/spaceangel_tenji_360_1_crop1.jpg)  
![](img/spaceangel_tenji_360_1_crop2.jpg) 
![](img/spaceangel_tenji_360_1_crop4.jpg)  
右下角有数字"4"。  
![](img/spaceangel_tenji_360_1_crop3.jpg)  

该纸上的标线等记号均为蓝色。该展览的原画的商品说明里多次提到“甚至可以看到...用蓝色写的说明，起草的线条”(例如[这里](./details1.md#BlueMark1))。这些记号都为蓝色，我猜，印刷时可能有专门的步骤过滤这些蓝色标记，详见[动画用蓝色铅笔 - details1](./details1.md#BluePencil)    

- 不同情况，对话框的形状不同。分别有：圆形、折线形、爆炸形。  
- A(下图左一)对话框内的文字是打印后、裁剪，然后贴上的。这在降低gamma值后(下图左二)更容易看到。       
![](img/spaceangel_tenji_360_1_crop5.jpg) ![](img/spaceangel_tenji_360_1_crop5_gamma.jpg)  

- [官方blog - Weibo](https://m.weibo.cn/detail/4770169039884953)  
"仔细观察的话，原画展上其他的画也能发现利用复印机的纹理加工技术留下的粒子痕迹  
特别是全手绘完成的最新版《空间天使》  
这种技术的活用无处不在！"  

- [Manga S.O.S. #3 @SMA](../zz_www.manga-audition.com/manga-sos/003.md)中提到这类漫画用纸的注意事项。  

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="new-space-angel"></a>  

## 官网报道中的图片 (作品创作年月未知)
![](img/spaceangel_tenji_360_2_thumb.jpg)  
新版Space Angel。  

为简便，采用如下符号简记：  
A: 源自[官网 catseye40th-exhibition](https://edition-88.com/catseye40th-exhibition);    
B: [官网 - Twitter](https://twitter.com/cityhunter100t/status/1526386475398356993)；  
C: [现场实拍 - weibo](https://weibo.com/ttarticle/p/show?id=2309404769077813183265)   

A(下图左一)，B(下图左二), C(下图左三)：  
![](img/spaceangel_tenji_360_2_thumb.jpg) 
![](img/not_given.jpg) 
[<img title="C备份缩略图(点击查看原图)" height="100" src="img/thumb/bcb839c9ly1h2821ugft2j20ku0dw3z8.jpg"/> 
](https://wx3.sinaimg.cn/wap720/bcb839c9ly1h2821ugft2j20ku0dw3z8.jpg)  

细节：   

- 所用纸张：  
DELETER Comic Book KENT Paper Type AK 135kg ?4，  
ケント紙 135kg ブロ-投稿用[220*310mm] DELETER INC.     
![](img/spaceangel_tenji_360_2_crop3.jpg) 
![](img/spaceangel_tenji_360_2_crop0.jpg)  
![](img/spaceangel_tenji_360_2_crop1.jpg) 
![](img/spaceangel_tenji_360_2_crop2.jpg)  
右下角有数字"5"。  

<a name="DeleterKent"></a>  

注1： 该纸张应该是：[Kent Paper, A4, with scale AK, 135kgs, 40 sheets (A4,AK,135kg) - Deleter OnlineManga*shop](https://deleter-mangashop.com/goods_en_usd_2103.html)。Deleter品牌的中文名为"灵猫"。  
([![](img/kent_paper_A.jpg)](https://deleter-mangashop.com/ori/30033/goods_img/goods_2103_2.jpg) )  

- High quality paper, "Kent"  
    - Kent paper fiber is tighter than other paper, with additional process, fiber is arrayed and tight.  
    - white color paper,  tight fiber paper  
    - The drawn ink stay at exact place on paper (feel more sharp lines)  
    - fiber is tight, so with high moisture, the paper tend to bend.  
- Size and thickness：  
    - A4(210x297mm) size, 135kg (about 2 times thickness of Copy paper),  
    - A Type：Scaled Lines, center marks are printed with blue lines which do not show up with copy.  

注2： [Cat's Eye的某些原画用纸](./details1.md#BaronKent)显示作者当时曾使用BaronKent纸。  
注3： 关于"Kent纸"的更多信息，参见[这里](./extra_info.md#Kent)。  
注4： [Japanese Manga101 #40 @SMA](../zz_www.manga-audition.com/zz_japanese_manga101/040.md)中提到Kent纸是日本漫画家用的最多的纸。  
注5： [Manga S.O.S. #3 @SMA](../zz_www.manga-audition.com/manga-sos/003.md)中提到这类漫画用纸的注意事项。  

- A(下图左一)中的爆炸效果逼真，且有层次感。降低gamma值(下图左二)后能看出喷洒的白色颜料的痕迹。  
![](img/spaceangel_tenji_360_2_crop4.jpg) 
![](img/spaceangel_tenji_360_2_crop4_gamma.jpg) 
 
- [官方blog - Weibo](https://m.weibo.cn/detail/4770169039884953)  
"仔细观察的话，原画展上其他的画也能发现利用复印机的纹理加工技术留下的粒子痕迹  
特别是全手绘完成的最新版《空间天使》  
这种技术的活用无处不在！"  

注：因为A的分辨率小于B，所以无法对比查看A的细节。若有必要，可对比查看B的细节，但这与本画展无关。  


---  

<a name="SistersInOneImage"></a>  

## 「キャッツ♥アイ」版画9 (作品创作年月未知)  
![](img/thumb/hojo_new_hanga_cats_02_02.jpg) 

- [日文版商品链接(名古屋展)](https://edition-88.com/products/catseye-hanga9)及描述：  
    - 规格：北条司亲笔签名、手工上色、天鹅绒衬垫、金属板（Cat's Card）  
    - 印刷技术：[88Graph](./about_edition88(2023).md#88Graph)  
    - 材料：  
        紙：版画用中性紙    
        框：木製、亚克力（封装）  
- [英文版商品链接(名古屋展)](https://edition88.com/products/catseye-hanga9-1)及描述：  
我们在整个图像上手工喷洒了珠光涂料，并在瞳手中作为关键物品的猫眼卡片上涂上了大片闪光剂，以创造出华丽的印象。  
    - 规格：北条司亲笔签名、衬垫、金属板   
    - 印刷技术：[Giclée](./extra_info.md#Giclée)、[UV print](./extra_info.md#UVPrint)  
    - 材料：  
        紙：美术纸  
        框：无  

为简便，采用如下符号简记：  
A: [官网图片](https://edition88.com/products/catseye-hanga9-1)，[官网图片](https://edition-88.com/products/catseye-hanga9)；  
B: 常见来源(无)；  

A(下图左一)，B(下图左二)：  
![](img/thumb/hojo_new_hanga_cats_02_02.jpg) 
![](img/not_given.jpg) 

细节：  

- [Natalie的报道](../zz_natalie.mu/2022-05-12_ce-40th.md)有如下信息：  
"北条によると連載当時、3姉妹の衣装はレオタードではなく“泥棒用のボディースーツみたいなもの”として描いていたとのこと。今回はあえてレオタードの質感で描いてみたそうなので、その違いに注目だ。"  
（据北条说，连载当时，三姐妹的服装并不是Leotard，而是“像小偷用的Bodysuits一样的东西”。据说这次特意用Leotard的质感画了，那个差别引人注目。）  
译注： [Cambridge Dictionary](https://dictionary.cambridge.org)中，“leotard”被译为“（女舞蹈演员或女运动员穿的）紧身衣，体操服”；“bodysuit”被译为”（女子上半身穿的）连体紧身内衣“。[这里](./zz_leotard_vs_bodysuit.md) 有更详细的比较。  

- 降低A的gamma值后，更容易看到高光的笔迹。  
![](img/hojo_new_hanga_cats_02_07_crop0.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop1.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop2.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop3.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop4.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop5.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop6.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop7.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop34.jpg) 

- 降低A的gamma值后，能看到三人的发丝有不同程度的灰色阴影 。泪（下图左一）的较明显。   
![](img/hojo_new_hanga_cats_02_07_crop8.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop9.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop10.jpg) 

- 降低A的gamma值后，更容易看到：1）有些发丝的灰色阴影变化不均匀（下图左一、左二）；2）有些发丝的边缘不光滑（下图左三）；3）极少数发丝线条有断裂（下图左四、左五）；     
![](img/hojo_new_hanga_cats_02_07_crop11.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop14.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop12.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop13.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop15.jpg) 

- 头发的高光处，有深浅两个颜色层次。瞳的头高光颜色饱和度较低（下图左二）。    
![](img/hojo_new_hanga_cats_02_07_crop16.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop17.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop18.jpg) 

- 黑色区域能看到一些纹理。可能是印刷时纸张凹凸不平导致的效果。  
![](img/hojo_new_hanga_cats_02_07_crop19.jpg) 

- 有些皮肤阴影次有深浅，且阴影边缘明显：  
![](img/hojo_new_hanga_cats_02_07_crop20.jpg) 

- 泪有眼影。  
![](img/hojo_new_hanga_cats_02_07_crop21.jpg) 

- 三人的眉毛各有不同。  
![](img/hojo_new_hanga_cats_02_07_crop21.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop22.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop23.jpg) 

- 黑色睫毛和背景黑色头发之间的过渡。  
![](img/hojo_new_hanga_cats_02_07_crop24.jpg) 

- 嘴唇的细节。  
![](img/hojo_new_hanga_cats_02_10_crop43.jpg) 

- 排线阴影：  
![](img/hojo_new_hanga_cats_02_07_crop25.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop26.jpg) 

- 物体的边缘线条粗细相间，有节奏。例如，小瞳的胳膊（下图蓝色，图片旋转了90度）。  
![](img/hojo_new_hanga_cats_02_02_crop27.jpg) 

- 局部图(B)，由于视角倾斜，能看到猫眼卡上的闪光剂。  
![](img/hojo_new_hanga_cats_02_05_crop28.jpg) 

- 衣服褶皱逼真。  
![](img/hojo_new_hanga_cats_02_01_crop29.jpg) 
![](img/hojo_new_hanga_cats_02_01_crop30.jpg) 
![](img/hojo_new_hanga_cats_02_01_crop31.jpg) 

- 展示了人体肌肉和骨骼(例如膝盖)的特征。（这可能意味着猫眼紧身衣非常薄）  
![](img/hojo_new_hanga_cats_02_07_crop35.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop36.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop37.jpg) 
![](img/hojo_new_hanga_cats_02_02_crop38.jpg) 
![](img/hojo_new_hanga_cats_02_02_crop39.jpg) 

- 着色过渡平滑、有颗粒感（左一、左二、左三）。左四按实际分辨率显示。[疑问]这种颗粒感是如何画的？    
![](img/hojo_new_hanga_cats_02_07_crop35.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop36.jpg) 
![](img/hojo_new_hanga_cats_02_02_crop38.jpg) 
![](img/hojo_new_hanga_cats_02_08_crop44.jpg) 

- 泪展示的是背影。其衣服在脊柱处有明显的黑色边线。这意味着它可能是布料的缝合线，或此处是拉链的凸起。  
![](img/hojo_new_hanga_cats_02_01_crop30.jpg) 

- 泪的紧身衣在臀部有一条长的黑线（从下至上延长至高处，如下图左一红色箭头所示）。[另一幅图](./details2.md#CE_1990_12)(下图左二)中的画法也是如此。我猜，这条长的黑线可能是布料的缝合线（下图左三）。  
![](img/hojo_new_hanga_cats_02_01_butt0.jpg) 
![](img/hojo_new_hanga_cats_01_06_butt0.jpg) 
![](img/pant_with_sew.jpg)  
让这条黑线短一些，做个比对。这类似此处无缝合线（下图左三）。    
![](img/hojo_new_hanga_cats_02_01_butt1.jpg) 
![](img/hojo_new_hanga_cats_01_06_butt1.jpg) 
![](img/pant_without_sew.jpg)  

- 和瞳、爱不同，泪的中指是紫色。我猜，这是她紧身衣的指套。  
![](img/hojo_new_hanga_cats_02_02_crop32.jpg) 
![](img/hojo_new_hanga_cats_02_07_crop33.jpg) 

- 泪的鞋跟（下图左一）比瞳的鞋跟（下图左二）高。爱的鞋跟高度（下图左三）似乎不容易看出。  
![](img/hojo_new_hanga_cats_02_02_crop40.jpg) 
![](img/hojo_new_hanga_cats_02_02_crop41.jpg) 
![](img/hojo_new_hanga_cats_02_02_crop42.jpg) 

- 对比图中猫眼三姐妹的头身比（角色有高跟鞋或踮脚尖）。泪和瞳的头身比均约为1:7；爱的头身比约为1:6.5，这符合她年龄小的设定。同时，作者通过让爱踮脚尖，也达到了头身比1:7的视觉效果，这增加了人像的美感。  
![](img/hojo_new_hanga_cats_02_02_head-body-ratio.jpg) 

- 名古屋展时，有发售随画附带的金属猫眼卡。  
![](img/hojo_new_hanga_cats_02_04_crop0.jpg) 
![](img/hojo_new_hanga_cats_02_04_crop1.jpg) 


---  

---  


---  

## 「キャッツ♥アイ40周年記念原画展～そしてシティーハンターへ～」図録
![](img/zuroku01_thumb.jpg)

[商品链接 -「キャッツ♥アイ40周年記念原画展～そしてシティーハンターへ～」図録](https://edition-88.com/products/catseye40th-catalog1)  
[封面大图](https://edition-88.com/catseye-goods)  

细节：  

- 三姐妹的相貌：  
![](img/499ccc41625b1ac1b81d1c05622d422e_crop0.jpg) 
![](img/499ccc41625b1ac1b81d1c05622d422e_crop1.jpg) 
![](img/499ccc41625b1ac1b81d1c05622d422e_crop2.jpg)  

- 泪的紧身衣的背部脊柱处有竖线褶皱。  



---  

## 商品  
[<img title="C备份缩略图(点击查看原图)" height="100" src="https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/bigtowel-KV02_600x.jpg"/> 
](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/products/bigtowel-KV02_4800x.jpg)  
[商品链接](https://edition-88.com/products/catseye-bigtowel1)  

商品标签上有疑似下列文字：  
中國製 中國て°生地生產 ？製たな行い  日本て°染色加工したものて°す  
中國製 中國で生地生產 ？製たな行い  日本で染色加工したものです  

可见该商品是在中国生产，在日本印刷。  

---  

## 商品  
[<img title="C备份缩略图(点击查看原图)" height="100" src="https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04180_600x600.jpg"/> 
](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04180_4800x4800.jpg)  
 (译注：[大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/DSC04180_4800x4800.jpg)， 
[Twitter上](https://twitter.com/cityhunter100t/status/1524938565309767681)相关大图: ![](https://pbs.twimg.com/media/FSmriQsVUAAzegK?format=jpg&name=large), 
(
![备份缩略图](http://www.hojocn.com/bbs/attachments/20220520_c43d87c2a27c40e4a9b2M56UTJdUiSfJ.jpg.thumb.jpg) 
[备份大图链接](http://www.hojocn.com/bbs/attachments/20220520_c43d87c2a27c40e4a9b2M56UTJdUiSfJ.jpg) 
)  

图中右侧红册子上的文字可能是“速作...画...隱された秘密”。[GalleryZenon解释说](https://twitter.com/GalleryZenon/status/1790925135014440976)这是吉祥寺举办的现实寻宝游戏「狙われた天使のレクイエム」（目标天使的安魂曲）。详见：  
该游戏的报道： [2020-01-26](../zz_ddnavi.com/2020-01-26.md), 
[2020-01-16](../zz_natalie.mu/2020-01-16_ch_menu_puzzle.md), 
[2019-10-31 ](../zz_natalie.mu/2019-10-31_ch.md),  
该游戏的官方网站： [https://takarush.jp/promo/cityhunter/](../zz_natalie.mu/zz_takarush_promo_cityhunter.md)  


---  
<a name="kanban"></a>

## 商品 实物大「Cat'sEye咖啡屋」 看板
![](./img/thumb/hojo_kanban_1.jpg)  

[商品链接(名古屋展)](https://edition-88.com/products/catseye-signboard)

- 关于该看板，[Twitter上](https://twitter.com/tohnomiyuki00/status/1595777478211305472)有评论：  
"一部を除き、撮影?SNSアップOKという太っ腹な展覧会。昭和な看板が否が応でも懐かしさを倍増する。   
(除了一些例外情况，摄影和社交网络上传在这个慷慨的展览中是允许的。无论你喜欢与否，昭和时代的招牌让人倍感怀旧。)"  
关于昭和风格，[解析昭和复古设计风格的设计秘密](https://zhuanlan.zhihu.com/p/359504079)中提到：  
“昭和设计之所以能给人留下深刻印象，是由于使用了特别的配色和字体，同时与叠加的印刷特点相结合，最终形成了独特的氛围感 ...... 昭和设计的配色，在整体上偏向使用让人感觉深沉与浓厚的颜色，在细部用色上偏向使用红色、橙色、黄色等令人兴奋的颜色。即便是出现蓝色、绿色这样的冷色，也会尽量挑选带有暖色调的蓝绿色，让画面更偏暖。另外，颜色的饱和度和明度都非常高，这样在作为海报等宣传的时候，会使设计内容既抢眼又干净 ...... 昭和时期的海报等宣传物料在字体的选择上，不仅较多地使用粗体，文字间隔也比较宽。这个时期的主要使用两种字体：一种是明朝体（相当于宋体），一种是哥特体。  
[<img title="(点击查看原图)" height="100" src="https://pic3.zhimg.com/80/v2-f04b233ca102d1deb3bb6cffc3043f52_720w.webp"/>](https://pic3.zhimg.com/80/v2-f04b233ca102d1deb3bb6cffc3043f52_720w.webp)
”  
从上述评论里得知，该招牌具有昭和风格是指其字体为明朝体（相当于宋体）、颜色为红色。    

---  

## 商品  Tote Bag(bouquet) / CityHunter
![](./img/thumb/hojo_tote_bouquet_001.jpg)   
[商品链接](https://edition-88.com/products/cityhunter-totebag2)  

细节：  

- 按该布料上的纹理颗粒的实际大小，将官方展示的图片缩小。所得的图像大小应该接近实物，如下图。可以看出：1）图像似乎是直接印刷在布料上；2）该印刷品的图片细节度还是很不错的。      
![](./img/hojo_tote_bouquet_002_crop0.jpg)  
![](./img/hojo_tote_bouquet_002_crop1.jpg) 

---  

## 商品  Tote Bag(bang) / CityHunter
![](./img/thumb/hojo_tote_bang_001.jpg)   
[商品链接](https://edition-88.com/products/cityhunter-totebag1)  

细节：  

- 比原画多了背景裂纹。  

- 按该布料上的纹理颗粒的实际大小，将官方展示的图片缩小。所得的图像大小应该接近实物，如下图。可以看出：1）图像似乎是直接印刷在布料上；2）该印刷品的图片细节度还是很不错的。   
![](./img/hojo_tote_bang_001_crop0.jpg)  
![](./img/hojo_tote_bang_001_crop1.jpg) 






---  

---  

<a name="SummaryOfDetails"></a>

## 细节 - 总结

- 彩图里，有相当数量的泪或小瞳的虹膜被画为蓝色。  
- 在角色的头身比例方面，[CE比CH更小](./details1.md#HeadBodyRatio)，即，CE比CH更理想化，CH比CE更接近现实。  
- [对比CE、CH的头发光泽](./details2.md#CompareHairInCEAndCH)。  
- 白色颜料用处广泛。例如，修改错误、画高光等。  
- [猜测的绘画流程](./details1.md#Pipeline)。  
- 漫画原稿多使用Kent纸。  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
