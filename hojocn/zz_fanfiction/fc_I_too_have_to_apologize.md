source:  
[https://www.fanfiction.net/s/10578021/1/Anch-io-devo-chiederti-scusa](https://www.fanfiction.net/s/10578021/1/Anch-io-devo-chiederti-scusa)  
([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96848))  

## Anch'io devo chiederti scusa

By: Megnove

I pensieri di Shion alla fine della serie. In fondo mi è sempre dispiaciuto veder descrivere lei come sicura di sé e Masahiko come un fessacchiotto, quando invece...
>Shion's thoughts at the end of the series. After all, I've always been sorry to see her described as self-confident and Masahiko as a jerk, when instead ...
>紫苑在本系列结尾处的想法。 毕竟，看到作品里她很自信而雅彦却被认为是一个傻瓜，这让我一直很觉得很抱歉，而... 

- Rated: Fiction K +   
- Italian   
- Romance / Hurt / Comfort  
- Shion  
- Words: 696  
- Published: Jul 30, 2014  
- Status: Complete  
- id: 10578021 

## Anch'io voglio chiederti scusa.
>## I too want to apologize.
>我也想道歉 

Perché ti ho sempre rimproverato, ma anch'io mi sono comportata male con te. Fin dal primo giorno che ti ho conosciuto.
>Because I've always scolded you, but I too have been bad to you. From the first day I met you.
>因为我一直数落你，但我也对你不好。从我认识你第一天开始。 

Ti ho fatto scoprire il nostro segreto di famiglia nel modo peggiore. Lo sapevo benissimo che non sarebbe stato facile da digerire, e forse l'ho fatto proprio per questo. E non una sola volta, ma DUE. Ti ho causato un trauma, ti ho praticamente costretto a scappare… chissà, forse non volevo altre delusioni. Mi eri simpatico, e pensavo che se fossi entrato nella mia vita poi mi avresti fatto soffrire, com'era già successo. Poi ho visto la tristezza di mamma e papà e sono venuta a riprenderti… nello stesso modo prepotente. Ancora una volta, non ti ho dato scelta. Avrei potuto parlarti sinceramente, ma non sapevo se avrebbe funzionato. Probabilmente no… e poi… non è questo il mio modo di fare!
>I made you discover our family secret in the worst way. I knew perfectly well that it wasn't going to be easy to digest, and maybe I did it for that very reason. And not once, but TWO. I caused you a trauma, I practically forced you to run away… who knows, maybe I didn't want other disappointments. I liked you, and I thought that if you came into my life then you would make me suffer, as it had already happened. Then I saw the sadness of Mom and Dad and I came to take you back ... in the same overbearing way. Again, I gave you no choice. I could have talked to you honestly, but I didn't know if it would work. Probably not ... and then ... this is not my way of doing!
>我让你以最坏的方式发现了我家的秘密。 我非常清楚它不易被接受，也许正是出于这个原因。 而且不是一次，是两次。 我给你造成了创伤，实际上我迫使你逃离了...谁知道呢...也许我不想再有别的令人失望的事情了。 我喜欢你，我想如果你走进我的生活的话，这会让我痛苦，这已经发生了。 然后我看到了父母的伤感，我以一种专横的方式把你带回来。 同样，我令你没有选择。 我本可以诚恳地与你交谈，但是我不知道它是否可行。 很可能不行然后……这不是我做事的方法！ 

Credo di averti continuamente messo alla prova già da allora. Ti sfidavo. Ti mettevo davanti a dilemmi che una persona qualunque non avrebbe retto, e pretendevo che tu invece ce la facessi. Non era giusto da parte mia, lo so. Ma come potevo dirti che ciò che desideravo in realtà… era che tu non fossi una persona qualunque? Che tu non fossi come tutti gli altri e riuscissi ad accettarmi per quello che ero… chiunque io fossi? Non avevo il diritto di pretenderlo da te. Ma sicura di me come fingo di essere, dentro ho tanta paura… e come potevo essere chiara e onesta con te, quando non capivo io stessa i miei sentimenti? Ho continuato semplicemente a stuzzicarti. Mi divertiva vederti reagire da semplicione. Ma più spesso di quanto tu non creda, il tuo comportamento mi dava invece speranza.
>I believe I have continually tested you ever since. I challenged you. I was putting you in front of dilemmas that an ordinary person would not have stood up to, and I expected you to succeed instead. It wasn't fair of me, I know. But how could I tell you that what I really wanted… was that you weren't just an ordinary person? That you weren't like everyone else and could accept me for who I was ... whoever I was? I didn't have the right to demand that from you. But sure of myself as I pretend to be, inside I am so afraid ... and how could I be clear and honest with you, when I didn't understand my feelings myself? I just kept teasing you. It amused me to see you react like a simpleton. But more often than not, your behavior gave me hope instead.
>我觉得从那以后我一直在不断地试探你。 我在考验你，让你面对一个普通人都无法承受的困境，而我却希望你能经得起考验。 我知道这不公平。 但我怎能告诉你，我真正想要的是……你不仅仅是一个普通人？ 我怎能告诉你，你不像其他人那样，你可以接受我...无论我是谁？ 我无权要求你这样做。虽然我看起来很自信，因为我假装成这样，但在我内心却如此恐惧……而且，当我自己也不了解自己的感受时，我又该如何对你坦白和诚实？ 我就一直在逗你。你单纯的反应让我很好笑。 但，更多时候，你的所作所为给了我希望。 

Ti ho accusato di scappare sempre, ma non è vero. I tuoi problemi li hai affrontati. Credendo nella famiglia, nelle persone… anche in me… con un'ingenuità e una sincerità che ti invidiavo tanto. Alla fine sei riuscito ad accettare la nostra situazione molto meglio di me. Eppure sei rimasto te stesso. Non sei cambiato come tutti quelli che erano passati prima per la nostra casa.
>I accused you of always running away, but that's not true. You have faced your problems. Believing in family, in people… even in me… with an ingenuity and sincerity that I envied you so much. In the end, you were able to accept our situation much better than me. Yet you remained yourself. You haven't changed like everyone who passed through our house before.
>我奚落你总是逃避，但这不属实。 你已经面对了问题。 独特且真诚地相信家人、相信别人，甚至相信我，这使我非常羡慕你。 最后，你能比我更好地接受我家的情况。 而你仍保持自我。不像以前经历过我家的那些人，你没有变。 

E non scappavo forse anch'io? Ti ho rimproverato di non riuscire a farti avanti, ma io ci sono forse riuscita? Non ti spingevo a forza verso un'altra per non ammettere che ero gelosa di lei? Non mi nascondevo dietro la maschera della dura o della finta ingenua… come dietro la maschera di maschio o di femmina… tanto da non sapere neanche io chi sono in realtà? Riuscivo ad esprimerti ciò che sentivo davvero solo da ubriaca… quand'ero fuori di me. Ti avrei voluto più forte di me, più duro, in grado di cambiarmi, di impormi chi dovevo essere… di farmi da punto di riferimento. Ma tu mi piaci come sei. Se fossi stato diverso non saresti riuscito a entrare tanto nel mio cuore. Ero io a dover trovare la forza di essere me stessa. Nessuno può cambiarci contro la nostra volontà. Stavo solo riversando le mie insicurezze su di te.
>And wasn't I running away too? I scolded you for not being able to step forward, but did I succeed? Didn't I forcefully push you towards someone else so as not to admit I was jealous of her? I didn't hide behind the mask of the tough or the naïve fake ... as behind the mask of a male or a female ... so much so that I don't even know who I really am? I could only express what I really felt when drunk… when I was out of my mind. I would have liked you stronger than me, harder, able to change me, to impose who I was to be ... to be my point of reference. But I like you the way you are. If you had been different you would not have been able to enter so much into my heart. It was I who had to find the strength to be myself. Nobody can change us against our will. I was just pouring out my insecurities on you.
>我不是也在逃避吗？ 我奚落你不勇敢向前，但我自己成功了吗？ 为了不承认我嫉妒某人，我不是强行将你推向她了吗？ 我没有躲在棘手的或幼稚的假象面具后面……因为我躲在了男性或女性的面具后面……我躲得如此频繁，以至于我都不知道自己究竟是谁？ 我只能喝醉后、疯狂时才能表达我的真实感受…… 我曾期望你比我更坚强、更努力，希望你能够改变我、能够让我变成原本的我...希望你能够作为我的参照物。 但是我喜欢你的方式和你一样。 如果你变了，那么你也不会在我的内心的位置这么深。是我必须要足够强大来成为我自己。 没有人可以改变我们的意愿。 我只是向你倾诉我缺乏安全感。 

E tu l'hai capito benissimo che la mia era una posa, un modo di scappare. Non credere di essere stato l'unico ad essere scosso, quando nel film ci siamo dovuti scambiare le parti.
>And you understood very well that mine was a pose, a way to escape. Don't think you were the only one who was shaken when we had to switch sides in the film.
>你非常了解我是故作姿态----一种逃脱方式。 当我们不得不调换影片里的角色时，别以为你是唯一一个动摇的人。

Può essere che fosse l'ultima prova a cui ti ho sottoposto, costringerti a fare quel film. Per vedere se riuscivi a vincerla contro Asagi, che mi voleva uomo ma diceva di amarmi comunque. Per vedere che avresti fatto contro una simile rivale. Ancora una volta sono stata ingiusta verso di te.
>Maybe it was the last test I put you through, forcing you to make that movie. To see if you could win it against Asagi, who wanted me to be a man but he said he loved me anyway. To see what you would do against such a rival. Once again I have been unfair to you.
>也许这是我对你的最后一次试探，迫使你拍摄那部电影。为的是看看你是否可以胜过浅葱(她希望我成为男人，但无论如何她说她都会爱我)。 为的是看看你将如何对付这样的对手。 我再一次对你不公平了。 

E alla fine sono stata anche meno coraggiosa di te. Tu hai avuto il coraggio di cambiare, di parlarmi dei tuoi sentimenti e metterti in gioco… di prendermi a scatola chiusa, uomo o donna che io sia. Io invece non ci sono riuscita. Mi sono di nuovo nascosta alla minima occasione dietro un dito, sono di nuovo fuggita avanti per farmi rincorrere da te… e forse così sarà sempre.
>And in the end I was even less courageous than you. You had the courage to change, to talk to me about your feelings and get involved ... to take me in a poke, man or woman I am. I, on the other hand, did not succeed. I hid myself again at the slightest opportunity behind a finger, I ran ahead again to be chased by you ... and maybe it will always be like that.
>最后，我比你还没有勇气。你有勇气去改变，有勇气与我谈你的感受并参与其中……让我陷入困境，无论我是男是女。 另一方面，我没有成功。 我又一次一有机会就躲在手指后面，我再次向前跑，被你追赶...也许它永远都是那样。 

Anch'io devo chiederti scusa, Masahiko.
>I have to apologize too, Masahiko.
>我也必须道歉，雅彦。

Ma ti prometto una cosa… lo scoprirai presto… e nel modo più giusto… quello che hai voluto sapere da sempre. Che io sono una donna.
>But I promise you one thing… you'll find out soon… and in the right way… what you've always wanted to know. That I am a woman.
>但是我向你保证一件事……你很快就会发现……并以正确的方式……你一直想知道的事情。 我是女人



