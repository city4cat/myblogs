
# Users' Comments on F.Compo at MangaHot  
Subtitles数据来源：[卷-章节-起始页码-标题](../fc_information/readme.md) (14卷版)  



### Vol01	CH001	page003		[「家族」初体験 / "家庭"的初体验   	 / First "family" Life Experience](./comment/fc/ch001.md)  


### Vol01	CH002	page043		[裸天(ラテン)な夜に / 天体之夜   	 / A Torrid Night](./comment/fc/ch002.md)  


### Vol01	CH003	page073		[温かいご飯 / 温暖的饭   	 / A Hot Meal](./comment/fc/ch003.md)  


### Vol01	CH004	page099		[入学式の条件 / 开学礼的条件   	 / Entry Ceremony](./comment/fc/ch004.md)  


### Vol01	CH005	page125		[キャッチボールの思い出 / 棒球的回忆   	 / The Glove Of Memories](./comment/fc/ch005.md)  


### Vol01	CH006	page153		[遠くで見ていた初恋 / 在远处遥望的初恋   	 / A First Discrete Love](./comment/fc/ch006.md)  


### Vol01	CH007	page179		[一肌　脱ぎます / 助一臂之力   	 / Just A Little Service](./comment/fc/ch007.md)  


### Vol02	CH008	page003		[母の思いで / 母亲的回忆   	 / Mother's Gift](./comment/fc/ch008.md)  


### Vol02	CH009	page031		[家族旅行！！ / 家庭旅行!！   	 / Family Trip!](./comment/fc/ch009.md)  


### Vol02	CH010	page059		[おしかけアシスタント / 不请自来的助手   	 / The Stubborn Assistant](./comment/fc/ch010.md)  


### Vol02	CH011	page087		[憧れの人 / 仰慕的人   	 / The One I Admire](./comment/fc/ch011.md)  


### Vol02	CH012	page115		[紫苑、援助交際！？ / 紫苑的不道德交际!？   	 / Shion Courtesan!?](./comment/fc/ch012.md)  


### Vol02	CH013	page141		[スクリーンに映るもの / 银幕上的紫苑   	 / The Girl On The Screen](./comment/fc/ch013.md)  


### Vol02	CH014	page169		[思い出の同窓会 / 充满回忆的同学会   	 / A Day To Remember Another Life](./comment/fc/ch014.md)  


### Vol03	CH015	page003		[女優誕生！？ / 女星的诞生!？   	 / A Star Is Born!?](./comment/fc/ch015.md)  


### Vol03	CH016	page031		[女装テスト / 扮女人的测试   	 / Transvestite Apprentice](./comment/fc/ch016.md)  


### Vol03	CH017	page059		[追い討ち / 追击   	 / Repetition](./comment/fc/ch017.md)  


### Vol03	CH018	page087		[意外な協力者 / 意想不到的助手   	 / An Unexpected Collaborator](./comment/fc/ch018.md)  


### Vol03	CH019	page113		[辰巳　純愛す・・・！？ / 辰已，纯情的爱…!?   	 / Tatsumi's Unwavering Love](./comment/fc/ch019.md)  


### Vol03	CH020	page141		[18年ぶりの妹 / 阔别了18年的妹妹   	 / A Little Sister Not Seen For Over 18 Years](./comment/fc/ch020.md)  


### Vol03	CH021	page169		[父と娘！ / 父与"女"!   	 / The Father And The "daughter"!](./comment/fc/ch021.md)  


### Vol04	CH022	page003		[順子の結納 / 顺子的过文定日子   	 / An Engagement Gift For Yoriko](./comment/fc/ch022.md)  


### Vol04	CH023	page029		[回り道 / 冤枉路   	 / Turnabout](./comment/fc/ch023.md)   


### Vol04	CH024	page057		[父親　倒れる！？ / 爸爸倒下了？   	 / Father Has Fainted?](./comment/fc/ch024.md)   


### Vol04	CH025	page085		[どっちが幽霊！？ / 谁是鬼魂？   	 / Where's The Ghost?](./comment/fc/ch025.md)   


### Vol04	CH026	page113		[二人の観客 / 两位观众   	 / Two Spectators](./comment/fc/ch026.md)   


### Vol04	CH027	page141		[雅彦を奪還せよ！ / 把雅彦还给我！   	 / Rescue Masahiko!!](./comment/fc/ch027.md)   


### Vol04	CH028	page169		[19歳の誕生日 / 19岁的生辰   	 / His Nineteenth Birthday](./comment/fc/ch028.md)   


### Vol05	CH029	page003		[女装コンテスト / 扮女人比赛   	 / Crossdressing Competition](./comment/fc/ch029.md)   


### Vol05	CH030	page031		[死のドライブ / 亡命驾驶   	 / Maniac At The Wheel](./comment/fc/ch030.md)   


### Vol05	CH031	page059		[婦人科はお好き！？ / 你喜欢妇科吗？   	 / Gynaecology? You're Kidding Me!](./comment/fc/ch031.md)  


### Vol05	CH032	page086		[それぞれのイブ / 各人的平安夜   	 / A Xmas Like The Others](./comment/fc/ch032.md)  


### Vol05	CH033	page113		[紫苑の初恋 / 紫苑的初恋   	 / Shion's First Love](./comment/fc/ch033.md)  


### Vol05	CH034	page141		[葉子の求愛！？ / 叶子的求爱!！   	 / Yoko Makes Her Move!!](./comment/fc/ch034.md)  


### Vol05	CH035	page169		[母の墓参り / 拜祭母亲   	 / At Mother's Graveside](./comment/fc/ch035.md)  


### Vol06	CH036	page003		[雅彦の独立戦争 / 雅彦的独立作战   	 / Masahiko's Plan For Independence ](./comment/fc/ch036.md)  


### Vol06	CH037	page031		[危ないバイト / 危险的兼职   	 / A Dangerous Job](./comment/fc/ch037.md)  


### Vol06	CH038	page057		[葵の純情 / 纯情葵   	 / The Feelings Of Aoï](./comment/fc/ch038.md)  


### Vol06	CH039	page085		[ファミリー・若苗 / Family-若苗   	 / My Family The Wakanaes](./comment/fc/ch039.md)  


### Vol06	CH040	page111		[憧れのウェディングドレス / 憧憬的婚纱   	 / Wanted](./comment/fc/ch040.md)  


### Vol06	CH041	page137		[コレズ・茜 / 同性恋师妹-茜   	 / Akane's Idol](./comment/fc/ch041.md)  


### Vol06	CH042	page165		[秘密の上京 / 秘密赴东京   	 / The Secret Trip](./comment/fc/ch042.md)  


### Vol07	CH043	page003		[養子に行きます / 愿当养子   	 / Become My Son!](./comment/fc/ch043.md)  


### Vol07	CH044	page031		[結婚式の秘策 / 婚礼的秘密对策   	 / A Super Secret Plan](./comment/fc/ch044.md)  


### Vol07	CH045	page061		[鍾乳洞は危険な香り / 钟乳洞内的危险幽香   	 / The Smell Of Danger From Limestone Cave](./comment/fc/ch045.md)   


### Vol07	CH046	page089		[悪夢の露天風呂 / 恶梦般的露天温泉   	 / The Nightmare Otherdoor Bath](./comment/fc/ch046.md)  


### Vol07	CH047	page115		[二人のマサヒコ / 两个雅彦   	 / The Two Masahikos](./comment/fc/ch047.md)  


### Vol07	CH048	page143		[マサヒコの正体 / "雅彦"的真正身份   	 / The Identity Of Masahiko](./comment/fc/ch048.md)  


### Vol07	CH049	page171		[父と子 / 两父女   	 / Father And Child](./comment/fc/ch049.md)  


### Vol08	CH050	page003		[若苗家の新居候 / 若苗家的新住客   	 / A New Member Of The Wakanae Family](./comment/fc/ch050.md)  


### Vol08	CH051	page031		[薫の策略 / 薰的诡计   	 / Kaoru's Stratagem](./comment/fc/ch051.md)  


### Vol08	CH052	page059		[男へのステップ / 迈出男人的第一步   	 / The First Step As A Man](./comment/fc/ch052.md)  


### Vol08	CH053	page087		[偽装家族 / 伪装家庭   	 / Camouflaging As A Family](./comment/fc/ch053.md)  


### Vol08	CH054	page115		[働くパパ / 工作中的爸爸   	 / Papa's Work](./comment/fc/ch054.md)  


### Vol08	CH055	page143		[葉子のロストバージン作戦 / 叶子的献身大作战计划   	 / Yoko's "first Time" Plan](./comment/fc/ch055.md)  


### Vol08	CH056	page171		[葉子の変身 / 叶子大变身   	 / Yoko's Transformation](./comment/fc/ch056.md)  


### Vol09	CH057	page003		[二人のバースデー / 二人世界的生日   	 / Their Birthday](./comment/fc/ch057.md)  


### Vol09	CH058	page031		[メールラバー / 网上情人   	 / Mail Lover](./comment/fc/ch058.md)  


### Vol09	CH059	page059		[ライバルに投げ勝て / 战胜劲敌   	 / Victory Pitch](./comment/fc/ch059.md)  


### Vol09	CH060	page085		[辰巳の願い / 辰已的心愿   	 / Tatsumi's Wish](./comment/fc/ch060.md)  


### Vol09	CH061	page111		[辰巳の告白 / 辰已的表白   	 / Tatsumi's Confession](./comment/fc/ch061.md)  


### Vol09	CH062	page139		[母のイメージ / 母亲的印象   	 / Mother's Image](./comment/fc/ch062.md)  


### Vol09	CH063	page167		[早紀の誘惑 / 早纪的诱惑   	 / Saki's Temptation](./comment/fc/ch063.md)  


### Vol10	CH064	page003		[薫の告白 / 薰的表白   	 / Kaoru's Confession](./comment/fc/ch064.md)  


### Vol10	CH065	page031		[親子の値段 / 亲情的价值   	 / The Price Of Kinship](./comment/fc/ch065.md)  


### Vol10	CH066	page059		[紫苑の第一志望 / 紫苑的第一志愿   	 / Shion's First Choice](./comment/fc/ch066.md)  


### Vol10	CH067	page087		[二人の取材旅行 / 二人的取材之旅   	 / Their Material Gathering Journey](./comment/fc/ch067.md)  


### Vol10	CH068	page115		[雅美の値段 / 雅美的身价   	 / Masami's Values](./comment/fc/ch068.md)  


### Vol10	CH069	page143		[恐怖のストーカー / 恐怖的跟踪者   	 / The Frightening Stalker](./comment/fc/ch069.md)  


### Vol10	CH070	page171		[紫苑誕生 / 紫苑的诞生   	 / Shion's Birth](./comment/fc/ch070.md)  


### Vol11	CH071	page003		[身代わりデート / 约会的替身   	 / Substitute Date](./comment/fc/ch071.md)  


### Vol11	CH072	page031		[それぞれのＸマス / 各人的圣诞节   	 / Separate Christmas](./comment/fc/ch072.md)  


### Vol11	CH073	page061		[すれ違いのイブ / 阴错阳差的平安夜   	 / An Eve Of Fated Encounters](./comment/fc/ch073.md)  


### Vol11	CH074	page089		[似た者同士 / 物以类聚   	 / It's All The Same](./comment/fc/ch074.md)  


### Vol11	CH075	page117		[紫苑の入試 / 紫苑的入学试   	 / Shion's Admission Exams](./comment/fc/ch075.md)  


### Vol11	CH076	page145		[紫苑の卒業旅行 / 紫苑的毕业旅行   	 / Shion's Graduation Trip](./comment/fc/ch076.md)  


### Vol11	CH077	page173		[紫苑と雅彦一夜を共に!! / 紫苑和雅彦共渡一夜   	 / Shion And Masahiko's Night Together](./comment/fc/ch077.md)  


### Vol12	CH078	page003		[旅は道連れ…!! / 出外靠旅伴…！   	 / Dragged Along On The Trip!](./comment/fc/ch078.md)  


### Vol12	CH079	page031		[雅彦 孤軍奮闘!! / 雅彦孤军作战！   	 / Masahiko Fighting Alone!!](./comment/fc/ch079.md)  


### Vol12	CH080	page059		[成人式はミステリアス / 充满神秘的成人节！   	 / The Mysterious Coming-Of-Age Day](./comment/fc/ch080.md)  


### Vol12	CH081	page087		[男か女か？ / 是男是女？   	 / Man Or Woman!?](./comment/fc/ch081.md)  


### Vol12	CH082	page115		[クラブ入部攻防戦 / 加入学会攻防战   	 / Club Recruiting Battle](./comment/fc/ch082.md)  


### Vol12	CH083	page143		[足は口ほどにモノを言う… / 脚如嘴巴般道出真相   	 / The Foot Speaks As Well As The Mouth...](./comment/fc/ch083.md)  


### Vol12	CH084	page171		[薫の進路 / 薰的前途   	 / Kaoru's Path](./comment/fc/ch084.md)  


### Vol13	CH085	page003		[失楽園（ロスト・パラダイス） / 失乐园   	 / Lost Paradise](./comment/fc/ch085.md)  


### Vol13	CH086	page031		[親心 / 父母心   	 / Parental Feelings](./comment/fc/ch086.md)  


### Vol13	CH087	page059		[親離れ子離れ / 孩子要独立   	 / Cutting The Umbilical Cord](./comment/fc/ch087.md)  


### Vol13	CH088	page087		[近くて遠い東京 / 似近亦远的东京   	 / Tokyo, So Near And Yet So Far](./comment/fc/ch088.md)  


### Vol13	CH089	page115		[母と娘 / 母与女   	 / Mother And Daughter](./comment/fc/ch089.md)  


### Vol13	CH090	page141		[衝突 / 冲突   	 / Clash ](./comment/fc/ch090.md)  


### Vol13	CH091	page169		[一日だけの女将 / 只做一天老板娘   	 / Okami For A Day](./comment/fc/ch091.md)  


### Vol14	CH092	page003		[恐怖の新歓コンパ / 恐怖的迎新联欢会   	 / Scary Meeting](./comment/fc/ch092.md)  


### Vol14	CH093	page031		[再会 / 再会   	 / Reunion](./comment/fc/ch093.md)  


### Vol14	CH094	page057		[浅葱来訪 / 浅葱到访   	 / Asagi's Visit](./comment/fc/ch094.md)  


### Vol14	CH095	page083		[浅葱陽動作戦 / 浅葱突击行动   	 / Asagi's Plan](./comment/fc/ch095.md)  


### Vol14	CH096	page109		[どっちがいいの!? / 哪方面较好呢？   	 / It Doesn't Matter?!](./comment/fc/ch096.md)  


### Vol14	CH097	page137		[一日だけの恋人 / 一天的情人   	 / Couple For A Night](./comment/fc/ch097.md)  


### Vol14	CH098	page163		[雅彦のシナリオ / 雅彦的剧本   	 / Masahiko's Scenario](./comment/fc/ch098.md)  


### Vol14	CH099	page189		[流れを変える / 改变剧情   	 / Change Of Course](./comment/fc/ch099.md)  


### Vol14	CH100	page215		[浅葱の反撃 / 浅葱的反击   	 / Asagi's Retaliation](./comment/fc/ch100.md)  


### Vol14	CH101	page243		[奥多摩の別れ / 奥多摩的分手   	 / Split At Okutama](./comment/fc/ch101.md)  


### Vol14	CH102	page269		[家族（ファミリー）(完結) / 家庭(最终话)   	 / Last Day - End](./comment/fc/ch102.md)  

