# Users' Comments at MangaHot  

Subtitles数据来源：[北条司 Short Stories Vol.2 @MangaHot](https://web.mangahot.jp/works/detail.php?work_code=j_R0017)   


## [おれは男だ！ / 我是男子汉！](./comment/ss2/ch001.md)  


## [桜の花 咲くころ / 樱花盛开时](./comment/ss2/ch002.md)  


## [American Dream](./comment/ss2/ch003.md)  


## [蒼空の果て… ー少年たちの戦場ー / 苍天的尽头 -少年们的战场-](./comment/ss2/ch004.md)  


## [少年たちのいた夏　〜Melody of Jenny〜 / 少年们的夏天 〜Melody of Jenny〜](./comment/ss2/ch005.md)  

