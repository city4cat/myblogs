
# Users' Comments at MangaHot  

Subtitles数据来源：[AH2 - Subtitles](../comment/ah2_info/subtitles.md)   


## Angel Heart (2nd Season) Vol01

	
### 第001话  [運命のペンダント時計 / 命运的吊坠手表](./comment/ah2/ch001.md)  Page030


### 第002话  [封印していた想い / 封印了的思念](./comment/ah2/ch002.md) Page073    


### 第003话  [本当に好きな人 / 真正喜欢的人](./comment/ah2/ch003.md) Page113    


### 第004话  [人生の忘れ物 / 人生遗留之物](./comment/ah2/ch004.md) Page149  


### 第005话  [桜と家族 / 樱花与家庭](./comment/ah2/ch005.md)  Page183  



## Angel Heart (2nd Season) Vol02
### 第006话  [桜に重ねる想い / 与樱花重叠的思念](./comment/ah2/ch006.md) Page011  



### 第007话  [桜炎上・・・！ / 樱花在燃烧](./comment/ah2/ch007.md) Page047  



### 第008话  [桜の足あと / 樱花的足迹](./comment/ah2/ch008.md) Page085    



### 第009话  [エゴノキの香り / 茉莉花“香”](./comment/ah2/ch009.md) Page123   



### 第010话  [特別なコーヒーカップ / 特别的咖啡杯](./comment/ah2/ch010.md) Page161  



## Angel Heart (2nd Season) Vol03
### 第011话  [想い出の写真館 / 充满回忆的照相馆](./comment/ah2/ch011.md) Page009  



### 第012话  [香の声 / 香的声音](./comment/ah2/ch012.md) Page045   



### 第013话  [期待のホープ / 期待之星](./comment/ah2/ch013.md) Page087  



### 第014话  [出世しないタイプ / 不会出人头地的类型](./comment/ah2/ch014.md) Page123  



### 第015话  [息子の門出と親心 / 儿子的新生活与亲情](./comment/ah2/ch015.md) Page161  


## Angel Heart (2nd Season) Vol04
### 第016话  [二人の父親 / 两个父亲](./comment/ah2/ch016.md) Page005  


### 第017话  [宝物と約束 / 宝物和约定](./comment/ah2/ch017.md) Page043  


### 第018话  [40年前から来た男 / 从40年前而来的人](./comment/ah2/ch018.md) Page089  



### 第019话  [裏切りと誤解 / 背叛和误解](./comment/ah2/ch019.md) Page127  



### 第020话  [ここに居る理由 / 留在这里的理由](./comment/ah2/ch020.md) Page165  



## Angel Heart (2nd Season) Vol05  

### 第021话  [時の止まった店 / 停止时间的咖啡厅](./comment/ah2/ch021.md) Page005  



### 第022话  [天使がくれた時間 / 天使给予的时间](./comment/ah2/ch022.md) Page043  



### 第023话  [瞳の魔力 / 眼睛的魔力](./comment/ah2/ch023.md) Page091  


### 第024话  [もうひとつの瞳 / 另一个眼睛](./comment/ah2/ch024.md) Page131  



### 第025话  [理不尽な対価 / 荒谬的代价](./comment/ah2/ch025.md) Page167  


## Angel Heart (2nd Season) Vol06
### 第026话  [本当の再会 / 真正地重逢](./comment/ah2/ch026.md) Page005  
 

### 第027话  [未熟な足長おじさん / 不成熟的长腿叔叔](./comment/ah2/ch027.md) Page041  

 

### 第028话  [わくわくする秘密 / 令人雀跃的秘密](./comment/ah2/ch028.md) Page079  



### 第029话  [優しい雨 / 温柔的雨](./comment/ah2/ch029.md) Page115  



### 第030话  [包み込む香り / 包容的香](./comment/ah2/ch030.md) Page155  


## Angel Heart (2nd Season) Vol07
### 第031话  [海坊主からのXYZ / 来自海坊主的XYZ](./comment/ah2/ch031.md) Page005  



### 第032话  [キケンな落し物 / 危险的失物](./comment/ah2/ch032.md) Page043  



### 第033话  [姿の見えぬ依頼人 / 隐身的委托人](./comment/ah2/ch033.md) Page075  


### 第034话  [本当の依頼 / 真正的委托](./comment/ah2/ch034.md) Page115  



### 第035话  [妹の面影 / 妹妹的面容](./comment/ah2/ch035.md) Page155  



## Angel Heart (2nd Season) Vol08  
### 第036话  [へたすぎる似顔絵 / 令人怜爱的画像](./comment/ah2/ch036.md) Page005  


### 第037话  [思い出の果実 / 回忆的水果](./comment/ah2/ch037.md) Page041  


### 第038话  [禁忌な名前 / 禁忌的名字](./comment/ah2/ch038.md) Page075  


### 第039话  [聖夜の贈り物 / 圣诞夜的礼物](./comment/ah2/ch039.md) Page115  


### 第040话  [4人の父親候補 / 4个候补父亲](./comment/ah2/ch040.md) Page157  



## Angel Heart (2nd Season) Vol09  
### 第041话  [心の栄養ドリンク / 心灵的能量饮料](./comment/ah2/ch041.md) Page005  


### 第042话  [偽りの葬儀 / 虚假的葬礼](./comment/ah2/ch042.md) Page041  


### 第043话  [19年前の出来事 / 19年前发生的事](./comment/ah2/ch043.md) Page079  


### 第044话  [ファルコンの娘 / 隼人的女儿](./comment/ah2/ch044.md) Page115  


### 第045话  [娘が選んだ父親 / 女儿选择的父亲](./comment/ah2/ch045.md) Page153  

 
## Angel Heart (2nd Season) Vol10  
### 第046话  [顔のない女 / 没有脸的女人](./comment/ah2/ch046.md) Page005  


### 第047话  [ミキビジョン / 未来所见的世界](./comment/ah2/ch047.md) Page039  


### 第048话  [カメレオンからの依頼 / 变色龙的委托](./comment/ah2/ch048.md) Page075  


### 第049话  [夢の残骸 / 梦想的残骸](./comment/ah2/ch049.md) Page111  


### 第050话  [小さなC・H / 小小城市猎人](./comment/ah2/ch050.md) Page147  


## Angel Heart (2nd Season) Vol11
### 第051话  [サウスポー父娘 / 左撇子父女](./comment/ah2/ch051.md) Page005  


### 第052话  [パパの決め球 / 爸爸的决胜球](./comment/ah2/ch052.md) Page041  


### 第053话  [転がり込んだ希望 / 转动的希望](./comment/ah2/ch053.md) Page079  


### 第054话  [似た者父娘 / 相似的父女](./comment/ah2/ch054.md) Page113  


### 第055话  [憧れの投球フォーム / 憧憬的投球动作](./comment/ah2/ch055.md) Page149  


## Angel Heart (2nd Season) Vol12
### 第056话  [バレてはいけない秘密 / 不能暴露的秘密](./comment/ah2/ch056.md) Page005  


### 第057话  [償えない想い / 无法偿还的心愿](./comment/ah2/ch057.md) Page041  


### 第058话  [味覚に宿る記憶 / 寄宿在味觉里的记忆](./comment/ah2/ch058.md) Page079  


### 第059话  [希望のスクラップブック / 希望的剪贴簿](./comment/ah2/ch059.md) Page115  


### 第060话  [かけがえのない切れっ端 / 不可代替的碎片](./comment/ah2/ch060.md) Page151  



## Angel Heart (2nd Season) Vol13
### 第061话  [獠の手料理 / 獠亲手做的菜](./comment/ah2/ch061.md) Page005  


### 第062话  [黒い噂は街を翔ぶ / 黑色流言纷飞于街市](./comment/ah2/ch062.md) Page041  


### 第063话  [少女の今を縛る過去 / 束缚少女的过去](./comment/ah2/ch063.md) Page077  


### 第064话  [傷の記憶 / 受伤的记忆](./comment/ah2/ch064.md) Page113  


### 第065话  [少女の見た青空 / 少女所见的蓝天](./comment/ah2/ch065.md) Page151  



## Angel Heart (2nd Season) Vol14

### 第066话  [優しさという弱み / 温柔和弱点](./comment/ah2/ch066.md) Page005  


### 第067话  [烏籠の外へ / 鸟笼之外](./comment/ah2/ch067.md) Page039  


### 第068话  [逃げ出した雨 / 逃避之雨](./comment/ah2/ch068.md) Page081  


### 第069话  [母の望みと少女のこれから / 女儿的将来就是母亲的希望](./comment/ah2/ch069.md) Page115  


### 第070话  [本当の誕生日 / 真正的生日](./comment/ah2/ch070.md) Page153  


## Angel Heart (2nd Season) Vol15
### 第071话  [既知の真実 / 理想的临终](./comment/ah2/ch071.md) Page005  


### 第072话  [喜ぶべきこと / 应该高兴的事](./comment/ah2/ch072.md) Page041  


### 第073话  [古びたプレゼント / 陈旧的礼物](./comment/ah2/ch073.md) Page077  


### 第074话  [人生の彩り / 人生的色彩](./comment/ah2/ch074.md) Page113  


### 第075话  [MからのXYZ / 来自M的XYZ](./comment/ah2/ch075.md) Page153  


## Angel Heart (2nd Season) Vol16
### 第076话  [背負う者  / 承担者](./comment/ah2/ch076.md) Page005  


### 第077话  [純白の戦姫 / 純白的战姫](./comment/ah2/ch077.md) Page041  


### 第078话  [秘めたる誓い / 隐秘的誓言](./comment/ah2/ch078.md) Page079  


### 第079话  [Angel City  / 凶弹的行踪](./comment/ah2/ch079.md) Page115  
