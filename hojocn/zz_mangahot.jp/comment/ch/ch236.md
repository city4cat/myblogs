
---  
[source: シティーハンター 236.揺れる乙女心!?の巻 コメント一覧](https://web.mangahot.jp/works/comment.php?story_id=3309)  

1.本当に素直じゃないね撩ちゃん。なんだかんだ言いながら眠っている香ちゃんに、自分の部屋を譲ってあげてるくせに。ホント、切ないね...    2018/11/13  


2.獠さんも香ちゃんも、お互い信頼関係がありすぎて、今の関係を壊したくないのかな？ だからああいう態度とっちゃうのかもね    2018/11/13  


3.獠が素直になれば丸く収まるだけなのに・・・と、思いつつも。この、今で言うツンデレ系も魅力だしね    2018/11/13  


4.揺れる乙女心ってタイトルだけど、本当に動揺して揺れ揺れになってるのはリョウちゃんだと思う！！　そして、登場だとのファッションは肩パットすごくて、めちゃ古いけど、絵柄がステキ過ぎて古さを感じない！    2018/11/13  


5.伊集院隼人は恋の話になるとてんで役に立たんが、美樹さんとゴールした点で言えば獠より一歩先を行ってるから、伊集院隼人の華麗なるアシストに期待しよう。てか獠よ!しっかりしてくれや!!    2018/11/13  


6.獠の「おま〜は」って、ここで初登場だったかなー。この言い方何気に好きだわー。まったく、素直じゃないのにこの優しさ。なかなか伝わらない香ちゃんへの優しさ。冴羽獠の半分は優しさで出来ているw    2018/11/13  


7.獠ちゃんの縄の縛られ方絶対危なくて怪しい。（笑）    2018/11/13  


8.リョウちゃんが、眠ってる香ちゃんをお姫様抱っこしてベッドに連れてってるんだよね？きっと。 それを起きてる時に出来ればいいのに、もどかしいわぁ。    2018/11/13  


9.まゆちゃんにさりげなくおじさん連発されてんのかわいそう……笑    2018/11/13  


10.最後のあ…れ…？ってどういう気持ちの顔なのかなぁ。    2019/01/28  


11.表情のひとつひとつが美しく心理描写されて素晴らしい。 䝤のこんな顔、香本人に見せてやれよ・・・    2019/02/19  


12.ズルイ優しさだなあ〜。香のこと大事すぎるのが伝わってくるよ。    2019/02/26  


13.きっとお姫様抱っこで香を自分の部屋に連れていく優しい獠。目覚めてからのいいわけ迷ってどのくらいの時間、香ちゃんの寝顔見てたんだ？ しかも香の温もり残るベッドで寝直し。素直にとられたくないって言え(笑)    2019/03/03  


14.香の寝顔見つめながら「…眠ってても 口の悪い奴だな…」って、こんな台詞なのに愛情が溢れ出て、こっちに伝わってくるのはなぜなんだ？最後「あ…れ？」って、自分が撒いた種なんだからちゃんと処理してよ！    2019/03/06  


15.読者にしかわからないリョウの優しさ。香に知られないようにしか優しさ発揮できないなんて、拗らせてるね～。    2021/02/04  


16.まゆ子ちゃん…もっこりの意味わかるの！？    2021/02/26  


17.まあモテる奴はしょっちゅう下着盗まんよな    2021/08/31  


18.揺れる乙女心ってりょーちゃんのことでない？？www    2023/08/05  





