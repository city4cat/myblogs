
---  
[source: シティーハンター 28.亡霊を撃て!の巻 コメント一覧](https://web.mangahot.jp/works/comment.php?story_id=3101)  

1.目が見えなくなるエピソードはありますから、出てくるまで是非読んでください。    2018/01/04  


2.元々エンジェルハートとは別世界の設定だし、目が見えなくとも普通に車も運転するぞ    2018/04/19  


3.getwildが聞こえてくる    2018/04/19  


4.最初の方からずーっとゆで理論て言いたいだけのがいるよね。ゆで理論って別に設定がぶれたことを指すものでも無いし、この作品に関してはブレてるわけじゃないし。    2018/05/05  


5.かっこいい    2018/05/17  


6.別のお話です。    2018/07/04  


7.コメントで普通にネタバレされて草    2018/11/25  


8.はあ、ええハナシだったわ…    2018/12/27  


9.シティハンター初期の傑作。 ええ話だったわー。    2019/02/17  


10.もっこりのアフターサービス付き!?    2019/12/13  


11.シティーハンターとエンジェルハートは、パラレルの別話。    2020/01/02  


12.獠ちゃんと同じ誕生日なのこっそり自慢。    2021/02/14  


13.断末魔のもっこり というパワーワード😂    2021/08/05  


14.カッコよ！！✨✨    2022/05/02  


15.16ページ右下、由美子さんが横顔で涙流してるシーン… 美しすぎて見惚れてしまった。綺麗    2023/08/24  


16.やっぱカッコいいなぁ〜    2023/09/18  


17.>2 ソフィアの話までは視力弱いけど見えてるよ。 盲目になるのはその後    2024/08/18  


18.うほお良かったというか魅せるねぇ 海ちゃんのセリフで地味に「僚」って誤植をしているのも発見    2024/08/20  






---  
[source: CITY HUNTER English version 28.Shoot the Specter! コメント一覧](https://web.mangahot.jp/works/comment.php?story_id=24518)  




