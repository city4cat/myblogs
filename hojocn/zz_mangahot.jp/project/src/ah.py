import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown


def generateConfigFile_AH(config_file):

    # AH2, ch046 ~ story_id=2594
    ch_id_start = 1
    story_id_start = 1218
    ch_id_end = 363
    story_id_end = 1518

    ch_id = ch_id_start
    story_id = story_id_start
    cfg = my_config.Config('AH2')
    # for 1st chapter
    cfg.setChStoryidList(ch_id, [story_id, story_id+1])
    ch_id += 1
    story_id += 2
    
    for i in range(ch_id_start+1, ch_id_end+1):
        cfg.setChStoryidList(ch_id, [story_id])
        ch_id += 1
        story_id += 1
        
    #cfg.print('')
    
    cfg.serialize(config_file)     
  
    
if '__main__' == __name__:
    config_file = './config/ah.json' 
    generateConfigFile_AH(config_file)  
    
    mdd = my_markdown.MarkdownData('ah', 
                dst_dir = '../comment_updated/ah',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
