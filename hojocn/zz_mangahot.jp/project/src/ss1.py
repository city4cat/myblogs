import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown

def generateConfigFile_ShortStoryVolume1(config_file):

    cfg = my_config.Config('SS1')
    cfg.setChStoryidList(1, [33225,33226])
    cfg.setChStoryidList(2, [33223,33224])
    cfg.setChStoryidList(3, [33227,33228])
    cfg.setChStoryidList(4, [33229,33230])    
    cfg.setChStoryidList(5, [33231,33232])    
    cfg.setChStoryidList(6, [33233,33234])  
    cfg.setChStoryidList(7, [33235,33236])  
        
    #cfg.print('')
    
    cfg.serialize(config_file)      

    
if '__main__' == __name__:
    config_file = './config/ss1.json' 
    generateConfigFile_ShortStoryVolume1(config_file)  
    
    mdd = my_markdown.MarkdownData('ss1', 
                dst_dir = '../comment_updated/ss1',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
