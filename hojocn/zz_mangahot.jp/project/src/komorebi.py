import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown

def generateConfigFile_Komorebi(config_file):

    # 
    ch_id_start = 1
    story_id_start = 33196
    ch_id_end = 25
    story_id_end = 33222

    ch_id = ch_id_start
    story_id = story_id_start
    
    cfg = my_config.Config('Komorebi')
    
    # for the 1st chapter
    cfg.setChStoryidList(1, [33196, 33197])
    
    ch_id = 2
    story_id = 33198
    for i in range(2, 24+1):
        cfg.setChStoryidList(ch_id, [story_id])
        ch_id += 1
        story_id += 1
        
    # for the last chapter
    cfg.setChStoryidList(25, [33221, 33222])
        
    #cfg.print('')
    
    cfg.serialize(config_file)  
    
if '__main__' == __name__:
    config_file = './config/komorebi.json' 
    generateConfigFile_Komorebi(config_file)  
    
    mdd = my_markdown.MarkdownData('komorebi', 
                dst_dir = '../comment_updated/komorebi',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
