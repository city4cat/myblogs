import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown


def generateConfigFile_Rash(config_file):

    # 
    ch_id_start = 1
    story_id_start = 15458
    ch_id_end = 16
    story_id_end = 15481

    ch_id = ch_id_start
    story_id = story_id_start
    
    cfg = my_config.Config('Rash')
    
    # 
    cfg.setChStoryidList(1, [15458, 15459, 15460])
    cfg.setChStoryidList(2, [15461, 15462])
    cfg.setChStoryidList(3, [15463, 15464])
    cfg.setChStoryidList(4, [15465, 15466])
    cfg.setChStoryidList(5, [15467, 15468])
    cfg.setChStoryidList(6, [15469])
    cfg.setChStoryidList(7, [15470])
    cfg.setChStoryidList(8, [15471, 15472])
    cfg.setChStoryidList(9, [15473])
    cfg.setChStoryidList(10, [15474])
    cfg.setChStoryidList(11, [15475])
    cfg.setChStoryidList(12, [15476])
    cfg.setChStoryidList(13, [15477])
    cfg.setChStoryidList(14, [15478])
    cfg.setChStoryidList(15, [15479])
    cfg.setChStoryidList(16, [15480, 15481])
    
    #cfg.print('')
    
    cfg.serialize(config_file)  
    
    
if '__main__' == __name__:
    config_file = './config/rash.json' 
    generateConfigFile_Rash(config_file)  
    
    mdd = my_markdown.MarkdownData('rash', 
                dst_dir = '../comment_updated/rash',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
