import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown


def generateConfigFile_CE(config_file):

    # CE, ch046 ~ story_id=2594
    ch_id_start = 1
    story_id_start = 5168
    story_id_en_start = 33025
    ch_id_end = 158
    story_id_end = 5330

    ch_id = ch_id_start
    story_id = story_id_start
    story_id_en = story_id_en_start
    
    cfg = my_config.Config('CE')
    cfg.setChStoryidList(1, [5168, 5169,    33025, 33026])
    cfg.setChStoryidList(2, [5170, 5171,    33027, 33028])
    cfg.setChStoryidList(3, [5172, 5173,    33029, 33030])
    cfg.setChStoryidList(4, [5174,          33031]) # 金髪は　おすき？の巻
    cfg.setChStoryidList(5, [5175, 5176,    33032, 33033]) #夜間飛行は危険な香りの巻
    ch_id = 6
    story_id = 5177
    story_id_en = 33034
    for i in range(ch_id, 157+1):
        if ch_id <= 28: 
            # English comment is updated to chapter 28 only
            cfg.setChStoryidList(ch_id, [story_id, story_id_en])
            ch_id += 1
            story_id += 1
            story_id_en += 1
        else:  
            cfg.setChStoryidList(ch_id, [story_id])
            ch_id += 1
            story_id += 1
        
    assert ch_id == 158, '158 == ch_id(%d)'%(ch_id)
    cfg.setChStoryidList(158, [5329, 5330])
        
    #cfg.print('')
    
    cfg.serialize(config_file)   
    
    
if '__main__' == __name__:
    config_file = './config/ce.json' 
    generateConfigFile_CE(config_file)  
    
    mdd = my_markdown.MarkdownData('ce', 
                dst_dir = '../comment_updated/ce',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
