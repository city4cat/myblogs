import json

class Config(object):  
    m_title = None
    m_ch_storyid_dict = None  
    
    def __init__(self, title):
        self.m_title = title
        self.m_ch_storyid_dict = {}
        
    def setChStoryidList(self, ch_id, story_id_list):  
        self.m_ch_storyid_dict[int(ch_id)] = story_id_list
        
    def addChStoryId(self, ch_id, story_id):
        if story_id in self.m_ch_storyid_dict[int(ch_id)]:
            print('Already has it. %s:%s has %d '%(ch_id, self.m_ch_storyid_dict[int(ch_id)], story_id))
        else:  
            self.m_ch_storyid_dict[int(ch_id)].append(story_id)
        
    def print(self, msg):  
        print("%s %s:  \n"%(msg, self.m_title))
        print(self.m_ch_storyid_dict)
        pass
        
    def serialize(self, filepath):
        #print(json.loads(self.m_ch_storyid_dict, indent=2, sort_keys=True))
        
        with open(filepath, "w" ) as write:
            json.dump(self.m_ch_storyid_dict, write, indent=2, sort_keys=True)

    def deserialize(self, filepath):  
        with open(filepath, "r" ) as read:
            self.m_ch_storyid_dict = json.load(read)
            
#--------------------------------------

if '__main__' == __name__:
    config_dir = './config'
    
    #generateConfigFile_CE(config_dir)  
    #generateConfigFile_CH(config_dir)   
    #generateConfigFile_Komorebi(config_dir)
    #generateConfigFile_Rash(config_dir)
    #generateConfigFile_FC(config_dir)
    #generateConfigFile_AH(config_dir)    
    #generateConfigFile_AH2(config_dir)

    

