import time
import random
import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.parse_html_comment as parse_html_comment

class MarkdownData(object):
    m_title = None
    m_config = None  
    m_dst_dir = ''
    
    def __init__(self, title, dst_dir, config_filepath):
        self.m_title = title
        self.m_dst_dir = dst_dir
        self.m_config = my_config.Config(title)
        self.m_config.deserialize(config_filepath)
        #self.m_config.print('config file loaded') 

    def get_comment_and_output(self):  
        total = len(self.m_config.m_ch_storyid_dict)
        i = 0
        
        for ch_id, story_id_list in self.m_config.m_ch_storyid_dict.items():
            #print(ch_id, "->", story_id_list)

            # cook the output string
            str_to_md = ''
            for story_id in story_id_list:
            
                # sleep for seconds ranging from ... to ...
                sleep_seconds = random.randint(10, 30)
                print('Sleep %.1f(sec) ... '%(sleep_seconds))
                time.sleep(sleep_seconds) 
                
                url = 'https://web.mangahot.jp/works/comment.php?story_id=%d'%(story_id)
                comments_object = parse_html_comment.CommentsOnAPage(url)
                #comments_on_a_page.print('')
                str_to_md += comments_object.output_str('')
                str_to_md += '\n\n\n\n'
                print('Finished: %s'%(url))
            
            # output
            filepath = "%s/ch%03d.md"%(self.m_dst_dir, int(ch_id))
            #print('%s:  \n%s'%(filepath, str_to_md))
            self.output(filepath, str_to_md)
            
            i += 1
            print("Progress: %d/%d"%(i, total))
            
        print("Complete!: %s\n\n\n\n"%(self.m_title))
            

    def output(self, filepath, str_to_md):
        with open(filepath, "w" ) as file:
            file.write(str_to_md)

if '__main__' == __name__:

    mdd = MarkdownData('ah2', 
                dst_dir='./md',
                config_filepath='./config/test.json')
    
    mdd.get_comment_and_output()
    
    
    
    
    
