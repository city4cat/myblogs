import os, sys, re
import random
import requests
from functools import total_ordering 

from urllib.parse import urljoin
from bs4 import BeautifulSoup

@total_ordering
class CommentWrapper(object):  
    m_comment_no = -1
    m_cheering_comment = None
    m_created_at = None
    m_cheering_count = None
    

    def __init__(self, obj_tag):
        #print('>', obj_tag.attrs)
        assert obj_tag.attrs['class'] == ['comment_wrapper'], 'This must be a [comment_wrapper], (%s)'%(obj_tag.attrs['class'])
        
        #a = BeautifulSoup(str(obj_tag), 'html.parser')
        #print('aa', a.div["class"])
        #print('obj_tag.contents=%s'%(obj_tag.contents))
        
        ctnt = [i for i in obj_tag.contents if i != '\n']
        #print('trimed contents=%s'%(ctnt) )

        for a in ctnt:
            #print(a.name, a.__class__) #<class '...NavigableString'>
            #print('a=%s'%(a))
            b = BeautifulSoup(str(a), 'html.parser')
            #print('bb=', b.div["class"], str(b.string).strip())
            
            if 'comment_no' == b.div["class"][0]:
                s = str(b.string).strip() # e.g. '10.'
                s = s.split('.')[0] # '10'
                self.m_comment_no = int(s)
            
            if 'cheering_comment' == b.div["class"][0]:
                self.m_cheering_comment = str(b.string).strip()
            
            if 'comment_footer' == b.div["class"][0]:
                #ctnt2 = [i for i in b.contents if i != '\n']
                #print('trimed contents2=', ctnt2 )
                #assert len(ctnt2) == 1, 'it should have 1 item (%d)'%(len(ctnt2)) 
                
                created_at = b.find_all('div', 'created_at')
                assert len(created_at) == 1, 'created_at should have 1 item (%d)'%(len(created_at)) 
                
                #print('created_at[0].contents=', created_at[0].contents)
                t = str(created_at[0].contents).strip()
                
                bad_chars = ['\\r', '\\t', '\\n', '[', '\'']
                for i in bad_chars:
                    t = t.replace(i, '')

                self.m_created_at = str(t.split(' ')[0]).strip()
                #print('%s'%(self.m_created_at) ) 
                
                cheering_count = b.find_all('div', 'cheering_count')
                assert len(cheering_count) == 1, 'cheering_count should have 1 item (%d)'%(len(cheering_count)) 
                
                #print('cheering_count[0].contents=', cheering_count[0].contents)
                cnt = cheering_count[0].contents[2]
                bad_chars = ['\\r', '\\t', '\\n', ' ']
                for i in bad_chars:
                    cnt = cnt.replace(i, '')
                self.m_cheering_count = int(cnt)
                '''
                c = BeautifulSoup(str(ctnt2[0]), 'html.parser')
                for d in c.children:
                    print('d=', d)
                    e = BeautifulSoup(str(d), 'html.parser')
                    print('e=', e)
                    for f in e.children:
                        if 'created_at' == f.div["class"][0]:
                            self.m_created_at = str(f.string).strip()
                            print('at=', self.m_created_at)
                '''
                #for child in b.descendants:
                #    print('child=', child)
    def __eq__(self, other):
        return (self.m_comment_no == other.m_comment_no)
    def __lt__(self, other):
        return (self.m_comment_no <  other.m_comment_no)
                
    def print(self, msg):  
        print(self.output_str(msg))
    
    def output_str(self, msg):
        s = ''
        s += '%s\n'%msg
        s += '%s.%s    %s  \n\n'%(self.m_comment_no, self.m_cheering_comment, self.m_created_at) 
        #s += '%s\n'%(self.m_cheering_count)
        return s
    
    
class CommentsOnAPage(object):
    m_url = None
    m_header_title = None    
    m_commentwrapper_list = None
    
    def __init__(self, url):
        self.m_url = url

        #
        '''
        headers = {
            'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
        }
        #This code would request a page simulating to be Googlebot.
        response = requests.get('https://www.example.com', headers=headers)

        proxies = {
          'http': 'http://10.10.1.10:3128'
        }
        response = requests.get('https://www.example.com', proxies=proxies)
        '''
        #session = requests.Session()
        #response = session.get(url)
        response = requests.get(url, headers = self.get_header())
        #print(response.text)
        soup = BeautifulSoup(response.text, 'html.parser')
        
        #
        #t = soup.find('div', 'header_title' ) # type: bs4.element.Tag,   value:<div class="header_title"> A<br/>B<br/></div>
        #t = soup.find('div', 'header_title').contents # type: list of string.   value: ['\r\n\tA', <br/>, '\r\n\tB', <br/>]
        t = soup.find('div', 'header_title').text # type: str.  value: A\nB\n
        t = t.replace('\t', '')
        t = t.replace('\r\n', ' ')
        t = t.strip()
        self.m_header_title = t
        
        #
        div_comment_wrapper_list = soup.find_all('div', 'comment_wrapper')

        self.m_commentwrapper_list = []
        for dcw in div_comment_wrapper_list:
            self.m_commentwrapper_list.append(CommentWrapper(dcw))
            
        self.sort()
        
    def get_header(self):
        user_agent_list = [
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 14_4_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Mobile/15E148 Safari/604.1',
            'Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 Edg/87.0.664.75',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363',
        ]

        header = {
            "User-Agent": user_agent_list[random.randint(0, len(user_agent_list)-1)]
        }
        #print('header:%s'%(header))
        return header
        
            
    def sort(self):
        '''
        print('------before sorted:')
        for i in self.m_commentwrapper_list:
            i.print(msg)
        '''    
        self.m_commentwrapper_list.sort()
        '''
        print('------after sorted:')
        for i in self.m_commentwrapper_list:
            i.print(msg)
        '''
                        
    def print(self, msg):
        print(self.output_str(msg))
        
        
    def output_str(self, msg):
        s = '\n---  \n'
        s += '%s[source: %s](%s)  \n'%(msg, self.m_header_title, self.m_url)
        for i in self.m_commentwrapper_list:
            s += i.output_str('')
        return s



def main():
    url = "https://web.mangahot.jp/works/comment.php?story_id=2594"

    cmt_on_url = CommentsOnAPage(url)
    cmt_on_url.print('')


if '__main__' == __name__:
    main()

    


