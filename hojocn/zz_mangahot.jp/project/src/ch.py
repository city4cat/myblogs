import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown


def generateConfigFile_CH(config_file):

    # 
    ch_id_start = 1
    story_id_start = 3074

    ch_id_end = 336
    story_id_end = 3409

    cfg = my_config.Config('CH')
    
    ch_id = ch_id_start
    story_id = story_id_start
    for i in range(ch_id_start, ch_id_end+1):
        cfg.setChStoryidList(ch_id, [story_id])
        ch_id += 1
        story_id += 1
        
    # for English comments        
    story_id_en = 21871 # start  
    for i in range(1, 10+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1
        
    story_id_en = 22060 # start  
    for i in range(11, 21+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1

    story_id_en = 24512 # start  
    for i in range(22, 32+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1
        
    story_id_en = 25278 # start  
    for i in range(33, 43+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1
        
    story_id_en = 26778 # start  
    for i in range(44, 54+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1
        
    story_id_en = 27188 # start  
    for i in range(55, 65+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1
        
    story_id_en = 28613 # start  
    for i in range(66, 76+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1
        
    story_id_en = 31448 # start  
    for i in range(77, 87+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1
        
    story_id_en = 33993 # start  
    for i in range(88, 98+1):
        # English comment is updated to chapter 98 only
        cfg.addChStoryId(i, story_id_en)
        story_id_en += 1

    #cfg.print('')
    
    cfg.serialize(config_file)  
    
    
if '__main__' == __name__:
    config_file = './config/ch.json' 
    generateConfigFile_CH(config_file)  
    
    mdd = my_markdown.MarkdownData('ch', 
                dst_dir = '../comment_updated/ch',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
