import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown


def generateConfigFile_AH2(config_file):

    # AH2, ch046 ~ story_id=2594
    ch_id_start = 1
    story_id_start = 2504
    ch_id_end = 79
    story_id_end = 2662

    ch_id = ch_id_start
    story_id = story_id_start
    cfg = my_config.Config('AH2')
    for i in range(ch_id_start, ch_id_end):
        cfg.setChStoryidList(ch_id, [story_id, story_id+1])
        ch_id += 1
        story_id += 2
    # for the last chapter
    cfg.setChStoryidList(ch_id, [story_id, story_id+1, story_id+2])
        
    #cfg.print('')
    
    cfg.serialize(config_file)      

    
if '__main__' == __name__:
    config_file = './config/ah2.json' 
    generateConfigFile_AH2(config_file)  
    
    mdd = my_markdown.MarkdownData('ah2', 
                dst_dir = '../comment_updated/ah2',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
