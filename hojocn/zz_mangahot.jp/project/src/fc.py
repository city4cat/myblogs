import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown


def generateConfigFile_FC(config_file):

    # AH2, ch046 ~ story_id=2594
    ch_id_start = 1
    story_id_start = 2894
    ch_id_end = 102
    story_id_end = 7151


    cfg = my_config.Config('FC')
    # for 1st chapter
    cfg.setChStoryidList(1, [2894, 2895])
    cfg.setChStoryidList(2, [2896])
    cfg.setChStoryidList(3, [2896+1])   
    cfg.setChStoryidList(4, [2896+2])      
    cfg.setChStoryidList(5, [2896+3])  
    cfg.setChStoryidList(6, [2951])  
    cfg.setChStoryidList(7, [3023])  
    cfg.setChStoryidList(8, [3420])  
    cfg.setChStoryidList(9, [3526])  
    cfg.setChStoryidList(10, [3605])  
    cfg.setChStoryidList(10+1, [3605+1]) 
    cfg.setChStoryidList(10+2, [3605+2])  
    cfg.setChStoryidList(10+3, [3605+3])  
    cfg.setChStoryidList(10+4, [3605+4])  
    cfg.setChStoryidList(10+5, [3605+5])  
    cfg.setChStoryidList(10+6, [3605+6])  
    cfg.setChStoryidList(10+7, [3605+7])      
    cfg.setChStoryidList(10+8, [3605+8]) 
    cfg.setChStoryidList(10+9, [3605+9]) 
    cfg.setChStoryidList(10+10, [3605+10]) 
    cfg.setChStoryidList(21, [3836]) 
    cfg.setChStoryidList(22, [4054]) 
    cfg.setChStoryidList(23, [4136]) 
    cfg.setChStoryidList(24, [4217]) 
    cfg.setChStoryidList(25, [4295]) 
    cfg.setChStoryidList(26, [4402]) 
    cfg.setChStoryidList(27, [4528]) 
    cfg.setChStoryidList(28, [4607]) 
    cfg.setChStoryidList(29, [4748]) 
    cfg.setChStoryidList(30, [4825]) 
    cfg.setChStoryidList(31, [4896]) 
    cfg.setChStoryidList(32, [5019]) 
    cfg.setChStoryidList(33, [5123]) 
    cfg.setChStoryidList(34, [5364]) 
    cfg.setChStoryidList(35, [5441]) 
    cfg.setChStoryidList(36, [5504]) 
    cfg.setChStoryidList(37, [5575]) 
    cfg.setChStoryidList(38, [5634]) 
    cfg.setChStoryidList(39, [5701]) 
    cfg.setChStoryidList(40, [5797])     
    cfg.setChStoryidList(41, [5930]) 
    cfg.setChStoryidList(42, [6036]) 
    cfg.setChStoryidList(43, [6144]) 
    cfg.setChStoryidList(44, [6246]) 
    cfg.setChStoryidList(45, [6361]) 
    cfg.setChStoryidList(46, [6439]) 
    cfg.setChStoryidList(47, [6553]) 
    cfg.setChStoryidList(48, [6602]) 
    cfg.setChStoryidList(49, [6797]) 
    cfg.setChStoryidList(50, [7004]) 
    
    ch_id = 51
    story_id = 7100
    
    for i in range(ch_id, ch_id_end+1):
        cfg.setChStoryidList(ch_id, [story_id])
        ch_id += 1
        story_id += 1

    #cfg.print('')
    
    cfg.serialize(config_file)   
    
    
if '__main__' == __name__:
    config_file = './config/fc.json' 
    generateConfigFile_FC(config_file)  
    
    mdd = my_markdown.MarkdownData('fc', 
                dst_dir = '../comment_updated/fc',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
