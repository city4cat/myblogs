import my_utility_mangahot.my_config as my_config
import my_utility_mangahot.my_markdown as my_markdown

def generateConfigFile_ShortStoryVolume2(config_file):

    cfg = my_config.Config('SS2')
    cfg.setChStoryidList(1, [33250,33251])
    cfg.setChStoryidList(2, [33237,33238, 33239])
    cfg.setChStoryidList(3, [33240,33241, 33242])
    cfg.setChStoryidList(4, [33243,33244, 33245])    
    cfg.setChStoryidList(5, [33246,33247, 33248, 33249])   
        
    #cfg.print('')
    
    cfg.serialize(config_file)      

    
if '__main__' == __name__:
    config_file = './config/ss2.json' 
    generateConfigFile_ShortStoryVolume2(config_file)  
    
    mdd = my_markdown.MarkdownData('ss2', 
                dst_dir = '../comment_updated/ss2',
                config_filepath = config_file) # config_file #'./config/test.json'
    
    mdd.get_comment_and_output()
