# Users' Comments at MangaHot  
Subtitles数据来源：[Komorebi Info](../komorebi_info/./komorebi_subtitles.md)  

## 第一幕 少女與樹之精靈  

### 第一場 [少女と木の精      / 少女與樹之精靈](./comment/komorebi/ch001.md)       Page5  


### 第二場 [七年前の少女      / 七年前的少女 ](./comment/komorebi/ch002.md)       Page53  


### 第三場 [偲び花          / 思念里的小花 ](./comment/komorebi/ch003.md)       Page79  


### 第四場 [心の花           / 心裏的花朵  ](./comment/komorebi/ch004.md)       Page101


### 第五場 [呪いの樹         / 受詛咒的大樹](./comment/komorebi/ch005.md)       Page127  


### 第六場 [哀しき大樹       / 哀傷的大樹  ](./comment/komorebi/ch006.md)       Page147  


### 第七場 [花がとりもつ縁   / 鮮花結良緣  ](./comment/komorebi/ch007.md)       Page166



## 第二幕 秋陽杲杲  

### 第八場 [初恋 －幸福の光る花－ / 初戀 -發出幸福之光的小花-](./comment/komorebi/ch008.md)       Page7  


### 第九場 [小さな大冒険        / 小小大冒險          ](./comment/komorebi/ch009.md)       Page26  


### 第十場 [光 満ちあふれて    / 萬點熒光             ](./comment/komorebi/ch010.md)       Page45  


### 第十一場 [秋の陽のデジャヴー  / 秋陽杲杲           ](./comment/komorebi/ch011.md)       Page65  


### 第十二場 [スパイ大作戦      / 狙擊間諜大作戰       ](./comment/komorebi/ch012.md)       Page85  


### 第十三場 [思い出づくり     / 留下買好回憶         ](./comment/komorebi/ch013.md)       Page104  


### 第十四場 [何時か何処かで    / 某時某地           ](./comment/komorebi/ch014.md)       Page123  


### 第十五場 [BODY JACKER    / BODY JACKER        ](./comment/komorebi/ch015.md)       Page143  


### 第十六場 [Night crawler  / NIGHT CRAWLER      ](./comment/komorebi/ch016.md)       Page166


### 第十七場 [CRESCENDO －クレッシェンド－    / CRESCENDO -危機逼近-](./comment/komorebi/ch017.md)       Page185  



## 第三幕 Heart and Soul  

### 第十八場 [Bad Soul           / Bad Soul   ](./comment/komorebi/ch018.md)       Page7   


### 第十九場 [Heart and Soul     / Heart and Soul](./comment/komorebi/ch019.md)       Page26  


### 第二十場 [嵐のあと            / 暴風雨後    ](./comment/komorebi/ch020.md)       Page46  


### 第二十一場 [コルチカムの咲く家  / 水仙盛放的家園](./comment/komorebi/ch021.md)       Page66


### 第二十二場 [冬の嵐 with LOVE     / 寒風有情](./comment/komorebi/ch022.md)       Page87  


### 第二十三場 [友情の木          / 友誼之樹   ](./comment/komorebi/ch023.md)       Page106  


### 第二十四場 [いいだせなくて…   / 盡在不言中  ](./comment/komorebi/ch024.md)       Page126  


### 第二十五場 [───終幕───      / 終幕       ](./comment/komorebi/ch025.md)       Page146  

