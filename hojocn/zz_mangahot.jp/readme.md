# MangaHot上的相关信息    

## Cat's Eye   (1981 ~ 1985)
可在线浏览(前2话免费）:  
[URL1](https://mangahot.jp/site/works/j_R0001), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=j_R0001)  
English Version:  
[URL1](https://mangahot.jp/site/works/e_R0027), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=e_R0027) 
(2024/12, it's updated to "32.Lost Memories")  

[Comments](./comments_ce.md)  



## City Hunter  (1985 ~ 1991)
可在线浏览(前2话免费）:  
[URL1](https://mangahot.jp/site/works/j_R0006), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=j_R0006)  
English Version:  
[URL1](https://mangahot.jp/site/works/e_R0007), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=e_R0007) 
(2024/12, it's updated to "98.Ryo's Story of Innocence ")  

[Comments](./comments_ch.md)  


## 阳光少女  (1993～1994)
可于官方在线浏览（前2话免费）:  
[URL1](https://mangahot.jp/site/works/j_R0018), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=j_R0018)  
English Version:  
Unknown  

[Comments](./comments_komorebi.md)  



## Rash!! (1994～1995)
可在线浏览(前2话免费）:  
[URL1](https://mangahot.jp/site/works/j_R0020), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=j_R0020)  
English Version:  
Unknown  

[Comments](./comments_rash.md)  



## F.Compo  (1996 ~ 2000)
可在线浏览(前2话免费）:  
[URL1](https://mangahot.jp/site/works/j_R0002), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=j_R0002)  
English Version:  
Unknown  

[Comments](./comments_fc.md)  


## 北条司短編集　Parrot ~幸福の人~ (1997)
Unknown  


## Angel Heart (1stSeason) (2001 ~ 2010)
可在线浏览（前2话免费）：  
[URL1](https://mangahot.jp/site/works/b_R0001)， 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=b_R0001)  
English Version:  
Unknown  

[Comments](./comments_ah.md)  



## Angel Heart (2ndSeason) (2010 ~ 2017)
可在线浏览（前2话免费）：  
[URL1](https://mangahot.jp/site/works/z_R0009)， 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=z_R0009)  
English Version:  
Unknown  

[Comments](./comments_ah2.md) (Updated to 2024-12-03)     


## 北条司 Short Stories Vol.1
> 『City Hunter -XYZ-』(1983)  
> 『City Hunter -DoubleEdge-』(1984)  
> 『ネコまんまおかわり♡』(1986)  (白猫少女)  
> 『天使の贈りもの』(1988) （天使的礼物）  
> 『TAXI DRIVER』(1990)  
> 『少女の季節 -Summer Dream-』(1992)  
> 『Family Plot』(1992)  (此处称该作品为F.Compo的前作)    

可在线浏览(前2话免费）:  
[URL1](https://mangahot.jp/site/works/j_R0016), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=j_R0016)  
English Version:  
Unknown  

[Comments](./comments_ss1.md) (Updated to 2024-12-03)    


## 北条司 Short Stories Vol.2
> 『おれは男だ！』(1980)  (我是男子汉！)  
> 『桜の花 咲くころ』(1993)  （樱花盛开时）  
> 『American Dream』(1995)  
> 『蒼空の果て-少年たちの戦場-』(1995)  （苍天的尽头 -少年们的战场-）  
> 『少年たちのいた夏〜Melody of Jenny〜』(1995)  （少年们的夏天 〜Melody of Jenny〜）   

可在线浏览(前2话免费）:  
[URL1](https://mangahot.jp/site/works/j_R0017), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=j_R0017)  
English Version:  
Unknown  


[Comments](./comments_ss2.md) (Updated to 2024-12-03)  


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  