# Users' Comments  
Subtitles数据来源：[RASH!! @Wikipedia](../rash_info/./rash_subtitles.md)  


## 1.  [Rash Rush !!](./comment/rash/ch001.md)  
   

## 2.  [Sleeping Beauty](./comment/rash/ch002.md)  


## 3.  [Father of the Bride](./comment/rash/ch003.md)  


## 4.  [Cold Sweat](./comment/rash/ch004.md)  


## 5.  [Conversation Piece](./comment/rash/ch005.md)  


## 6.  [Wages of Fear](./comment/rash/ch006.md)  


## 7.  [Looking for Mr.Goodbar](./comment/rash/ch007.md)  


## 8.  [The Getaway](./comment/rash/ch008.md)  


## 9.  [Close to You](./comment/rash/ch009.md)  


## 10.  [True Confessions](./comment/rash/ch010.md)  


## 11.  [Beauty and Beast](./comment/rash/ch011.md)   


## 12.  [Scar Face](./comment/rash/ch012.md)   


## 13.  [The Friendly Persuasion](./comment/rash/ch013.md)   


## 14.  [Hang'em High](./comment/rash/ch014.md)   


## 15.  [The Fugitive](./comment/rash/ch015.md)   


## 16.  [Starting Over](./comment/rash/ch016.md)  

