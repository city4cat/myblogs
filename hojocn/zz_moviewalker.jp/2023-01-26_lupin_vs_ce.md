source:  
https://moviewalker.jp/news/article/1121406/

[hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96969)    


![](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455139?h=500&f=webp)

# 北条司「キャッツ・アイ」原作40周年を超えて明かす、最終回の名セリフ秘話＆続編を描かない理由
北条司「Cat's Eye」超过原作40周年，揭示最終回里名台词秘话&不画续集的理由  

2023/1/26 20:30

泪、瞳、愛の美人三姉妹が怪盗として躍動する様子を描いた、北条司による人気漫画「キャッツ・アイ」。それぞれタイプの違う三姉妹をはじめとする魅力的なキャラクター、“怪盗の瞳＆刑事の俊夫”による恋の行方も注目を集め、アニメ化も実現するなど1980年代に一世を風靡した。2022年には原作40周年を迎えた同作だが、このたびモンキー・パンチの名作とのコラボが実現した「ルパン三世 VS キャッツ・アイ」(1月27日よりPrime Videoで世界独占配信)がAmazon Originalのアニメとして登場。アニメ史を代表する泥棒たちが夢の共演を果たし、大いに楽しませてくれる。  
泪、瞳、愛美女三姐妹作为神秘怪盗进行活动，这出自北条司的一部人气漫画「Cat's Eye」。该系列因其富有魅力的角色，包括三个不同类型的姐妹，以及"身为小偷的瞳和身为刑警的俊夫"之间的恋情而引起关注，并被制作成动画片，在1980年代风靡一时。2022年为庆祝原作40周年，现在与Monkey Punch的杰作合作的「鲁邦三世 VS Cat's Eye」（从1月27日起在Prime Video全球独家发售）将作为Amazon Original Anime推出。代表动画历史的盗贼们以梦幻组合的方式联袂出演，将给你带来极大的娱乐。  

いまなお忘れがたい作品として愛されている「キャッツ・アイ」だが、“感動的な最終回を迎えた漫画”として同作を思い出す人も多いはず。連載デビュー作にして大ヒットを生みだした原作者の北条を直撃し、「ルパン三世」とのタッグが叶った感慨や、最終回での「瞳ともう一度…もう一度恋ができる」という俊夫の名セリフに隠された秘話、続編を描かない理由まで語ってもらった。  
「Cat's Eye」作为一部令人难忘的作品仍然受到人们的喜爱，许多人记得它的"最終回令人感动"。我们采访了这部在首次连载时就大受欢迎的漫画的作者北条，问他能够与「鲁邦三世」合作的感受，俊夫在最終回的名台词「我和小瞳可以再一次...再一次恋爱」背后的秘密故事，甚至还有他不画续集的原因。  

## ## 「『ルパン三世』とのコラボなんて、恐れ多いなと思いました」  
「与『鲁邦三世』的合作让我有些担心」  

[![アニメ史を代表する泥棒たちが夢の共演を果たした「ルパン三世VSキャッツ・アイ」](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455126?w=615)](https://moviewalker.jp/news/article/1121406/image11455126/)  
アニメ史を代表する泥棒たちが夢の共演を果たした「ルパン三世VSキャッツ・アイ」[c]モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会  
「鲁邦三世 VS Cat's Eye」，动漫史上最著名的盗贼的梦幻合作 [c]Monkey Punch Tsukasa Hojo/鲁邦三世 VS Cat's Eye制作委员会  

[すべての写真を見る(33件)（译：查看所有照片）](https://moviewalker.jp/news/article/1121406/image11455139/)

「ルパン三世」のアニメ化50周年と、「キャッツ・アイ」の原作40周年を記念して制作された「ルパン三世VSキャッツ・アイ」。キャッツアイの父親が遺した3枚の絵をめぐって、ルパンとキャッツアイが泥棒対決を繰り広げるなか、絵に隠された秘密が明らかになっていくさまを描く。監督を、[静野孔文](https://moviewalker.jp/person/230980/)と[瀬下寛之](https://moviewalker.jp/person/182844/)が務めた。  
为纪念「鲁邦三世」改编动画50周年、「Cat's Eye」原作40周年而制作了「鲁邦三世 VS Cat's Eye」。为了争夺Cat's Eye的父亲留下的3幅画，鲁邦和Cat's Eye进行了一场盗贼决斗，影片讲述了如何揭开隐藏在画中的秘密。导演：[静野孔文](https://moviewalker.jp/person/230980/)和[瀬下寛之](https://moviewalker.jp/person/182844/)。  

[![【写真を見る】名作が生まれたデスク、資料の詰まった本棚…北条司の貴重なアトリエショットを公開](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455241?w=615)](https://moviewalker.jp/news/article/1121406/image11455241/)  
【写真を見る】名作が生まれたデスク、資料の詰まった本棚…北条司の貴重なアトリエショットを公開  
【请看照片】杰作诞生的工作台、摆满文件的书架...北条司工作室的罕见镜头  

ルパンファミリーとキャッツアイが同じ画面に存在するという、ワクワク感満載の内容に仕上がった本作だが、企画が立ちあがった当初、北条は「恐れ多いなと思いました」と苦笑いしたそう。「『ルパン三世』は、いまだに現役バリバリです。『キャッツ・アイ』は連載が終了してだいぶ経ちますし、『ルパン三世』と『キャッツ・アイ』では知名度も圧倒的に違うんじゃないかなと思って。ただモンキー・パンチ先生の息子さんである加藤(州平)氏によると、以前モンキー・パンチ先生が『キャッツアイとルパンが対決したら、きっとおもしろい作品になるだろう』とお話しされていたそうなんです。ものすごくありがたいことだなと思いました」と、モンキー・パンチも待ち望んでいた企画の実現に喜びをかみ締める。  
这部电影充满了刺激，鲁邦家族和猫眼同框，但在最初构思这个项目时，北条苦笑着说「我认为这很令人担心了」。他说「『鲁邦三世』仍在创作，『Cat's Eye』连载结束很久了，『鲁邦三世』和『Cat's Eye』在知名度上有天壤之别。 然而，据Monkey Punch的儿子加藤(州平)说，Monkey Punch曾告诉他『如果猫眼和鲁邦对决，肯定会是一部有趣的作品』。 我对此非常感激」，并补充说他很高兴Monkey Punch也一直在等待的这个项目能够实现。  

[![ルパンファミリーとキャッツアイが邂逅！ドラマチックな展開に注目だ](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455142?w=615)](https://moviewalker.jp/news/article/1121406/image11455142/)  
ルパンファミリーとキャッツアイが邂逅！ドラマチックな展開に注目だ[c]モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会
鲁班团伙和Cat's Eye的遭遇！ 注意戏剧性的发展[c]Monkey Punch Tsukasa Hojo/鲁邦三世 VS Cat's Eye制作委员会  


劇中では、ルパンとキャッツアイの三女・愛が行動を共にし、父と娘のようなやり取りを見せる。北条は「これは僕の希望でもあったんです」と打ち明け、「もしルパンとコンビを組むのが泪だとしたら、(峰)不二子とかぶってしまう印象になってしまうかもしれないし、ちょっと生々しい雰囲気が出てしまうかも。瞳には俊夫という恋人がいますから、それもちょっと問題かなと(笑)。ルパンにとってまだ女性として見ることがない愛、愛にとっては“おじさん”に見えるルパンという組み合わせが、お互いに本音も言える、おもしろい関係になるだろうなと思いました。完成作を観ても、その2人のやり取りはとてもうまくいっていましたね」とにっこり。  
在影片中，鲁邦和Cat's Eye的三女儿爱一起行动，像父女一样互动。 北条坦言「这也是我的希望」，并补充说「如果是泪与鲁班搭档，可能会给人一种像(峰)不二子的感觉，可能会给人一种原始的气氛。 瞳的恋人是俊夫，所以我想这可能也是一个问题（笑）。 我想，还没有见过小爱和鲁邦搭档，后者视前者为小孩。鲁邦对小爱来说像个 "叔叔"，我认为这将是一种有趣的关系，因为他们可以向对方表达自己的真实感受」。    

[![ルパンと愛が特別な絆を育んでいく](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455133?w=615)](https://moviewalker.jp/news/article/1121406/image11455133/)  
鲁邦和爱发展出一种特殊的关系[c]Monkey Punch Tsukasa Hojo/鲁邦三世 VS Cat's Eye制作委员会  

  

「ルパン三世」の銭形警部と、「キャッツ・アイ」の内海俊夫刑事が協力して捜査する場面も両作のファンにとっては心躍る瞬間で、北条も「最高でしたね！」と笑顔を弾けさせつつ「銭形警部が、内海俊夫に向かって『熱海！』と呼びかけるセリフがありました。脚本でそのシーンを見た時に、『悔しい！僕もこういうギャグをやりたかった！』と思いました」と銭形＆俊夫コンビの掛け合いをたっぷりと楽しんだ様子。アクションや三姉妹の父親をめぐるドラマも本作の見どころとなり、北条は「1本の作品として、とてもおもしろいものになっていました」と満足気な表情を浮かべる。  
「鲁邦三世」中的钱形警官和「Cat's Eye」中的内海俊夫刑警合作调查的场面对两部作品的粉丝来说都是一个激动人心的时刻，北条说「这很！」。 「剧本里有一句是：钱形警官向内海俊夫喊道『热海！』。 当我看到剧本中的这一幕时，我想『这太令人沮丧了！。 我也想做这种搞笑的事!』」。銭形＆俊夫似乎很喜欢这样的对话。围绕三姐妹的父亲的行动和剧情也是该片的亮点，北条表示满意「作为一个单一的作品，它非常有趣」。（译注：待校对）  






## 「読み切りのつもりで描いたものが連載に。焦りました(笑)」
「本来是一个短篇故事，却变成了一个连载。 我当时很着急（笑）」  

「キャッツ・アイ」は、「週刊少年ジャンプ」にて1981年に連載がスタート。レオタード姿で夜を駆ける美しい怪盗三姉妹という斬新な設定も読者の興味を引き、瞬く間に人気が上昇した。北条が大学を卒業した直後に、地元の福岡県で「読み切りのつもり」で描いたのが始まりだ。誕生のきっかけについて北条は、こう語る。  
「Cat's Eye」于1981年开始在「周刊少年Jump」上连载。 美丽的怪盗三姐妹穿着紧身衣在夜色中奔跑的新颖设定也引起了读者的兴趣，该作人气瞬间上升。 这部作品最初是在他的家乡福冈县画的，当时北条刚从大学毕业，"作为短篇故事"。 北条描述了其诞生的推动力。   

[![「キャッツ・アイ」誕生秘話や最終回への想いを語った北条司](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455125?w=200)](https://moviewalker.jp/news/article/1121406/image11455125/)  
北条司讲述了「Cat's Eye」的诞生以及他对最終回的看法  


「大学の卒業制作に向けて飲み会をしていて『なんかおもしろいネタがないかな』という話をしていたら、“父親が刑事で、母親が泥棒の夫婦”というアイデアが浮かんで。妻は夫から情報を聞き出して、裏をかいてしまう…というストーリーはどうだろうかと。でも少年誌に書くとしたら、夫婦よりも恋人同士のほうがいいなと思い設定を変更しました。また三姉妹の設定にしたのは、大学の近くにカフェがあって、そこで3人の女の子が働いていたから(笑)。『じゃあ、三姉妹で描いてみるか』と、単純な発想が積み重なって生まれたのが『キャッツ・アイ』です」。  
「我们在大学里为毕业设计开了一个酒会，我们在讨论『什么会是一个有趣的故事』，于是我们想到了刑警父亲、小偷母亲的夫妇。 妻子从丈夫那里得到信息并得手...我想，一个妻子胜过丈夫的故事如何呢？但我想，如果我是为少年杂志创作，最好是两个恋人，而不是丈夫和妻子，所以我改变了设定。 我还选择了三姐妹的设定，因为大学附近有一家咖啡馆，三个女孩在那里工作（笑）。 我想『好吧，让我们试着画一个有三姐妹的故事』，结果就是『Cat's Eye』，它是由一系列简单的想法产生的」。    

[![キャッツアイは昼は喫茶店を営み、夜は怪盗として世間を騒がす三姉妹](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455132?w=615)](https://moviewalker.jp/news/article/1121406/image11455132/)  
Cat's Eye是三姐妹，她们白天经营一家咖啡店，晚上当小偷，引起骚动[c]Monkey Punch Tsukasa Hojo/鲁邦三世 VS Cat's Eye制作委员会    

また、キャッツアイがレオタードで盗みをする設定にしたことについては「僕はボディスーツのつもりで描いていたんですよ！」と笑う。「そうしたら編集部が『これはレオタードだ』と言いだして、僕も驚きました(笑)。当時はジャズダンスなどが流行っていて、レオタードを着て踊る人もよくいましたから。そういう時代なので、レオタードだと捉えられたんだと思います」と時代が生んだ勘違いが、ヒロインたちのトレードマークになったのだという。  
至于Cat's Eye穿着紧身衣行窃的设定，他笑着说「我把它画成了一件连体衣！」。 然后编辑部说『这是一件紧身衣』，这让我也很吃惊（笑）。 当时爵士舞很流行，人们经常穿着紧身衣跳舞。 我想正是因为那个时代，人们才把它当成了紧身衣。」 时代造成的误解成为女主角的一个标志。  

読み切りのつもりで描いたものが好評を得て、連載が決定した。北条は「焦りました。いきなり連載だと言われても、それ以降の物語が描けるような設定として考えていないですから。担当編集者からは『家は探しておくから、すぐに上京しなさい』と言われて、『ええー!?』と慌てて上京してきました」とドタバタとした連載デビューを回顧。同作は瞳と俊夫の繰り広げるラブコメでありつつ、“三姉妹が泥棒をしながら、行方不明の父親を探す”というドラマチックなテーマが根底に流れているが、北条によると「もともとは読み切りのつもりでしたから、それらはすべて後付けです」と告白する。  
原本打算作为短篇故事的内容受到好评，并决定连载。 北条说「我很不耐烦。 尽管我突然被告知这个故事将被连载，但我并没有想到这些设定可以进一步拓展故事。责任编辑告诉我『我会给你找房子，所以你应该马上到东京来』。我赶到了东京」，回顾了漫不经心的连载出道。这个故事是一部关于小瞳和俊夫的浪漫喜剧，但也有一个 "三姐妹一边偷东西一边寻找失踪的父亲"的剧情作为主线，但据北条说「它最初的目的是读出，所以所有的东西都是后来添加的」。   

[![「キャッツ・アイ」には、「がむしゃらが詰まっている」](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455138?w=615)](https://moviewalker.jp/news/article/1121406/image11455138/)  
「キャッツ・アイ」には、「がむしゃらが詰まっている」[c]モンキー・パンチ 北条司/ルパン三世 VS キャッツ・アイ製作委員会  
「Cat's Eye」充满了热情[c]Monkey Punch Tsukasa Hojo/鲁邦三世 VS Cat's Eye制作委员会  
  

「最初は、三姉妹は泥棒の家系に生まれて盗みを働いている…くらいの感じで考えていました。ところが連載が始まって何週間か経ったころに、担当編集から『彼女たちはなぜ泥棒をしているんだ。少年誌らしい、熱い理由がほしい』と言われまして(笑)。『いまさらそんなことを言う？』と思いながらも、なんとかアイデアを捻りだして、“画家だった父親の絵を取り返すために、盗みをしている”という設定にたどり着きました。でも最初のころのキャッツアイはダイヤも盗んじゃっているし、どうしよう…と悩んだりもしましたが、そうやってすべて後付けで描き進めて。僕は連載も初めてで、それまでストーリーもろくに考えたことがない。すべてが素人なわけです。なにが正解なのか、なにが悪いのかもわからぬまま、てんてこ舞い。とにかく毎回必死でした」と自身のがむしゃらが詰まっているのが、同作なのだと話す。  
「起初，三姐妹出生在一个小偷家族，正在偷东西......大概就是这样。 然而，在开始连载的几个星期后，责任编辑问我『为什么她们是小偷？ 在少年漫画杂志上，需要一个热血的理由（笑）』。 我想『你为什么现在才告诉我这些？』但我设法想出了一个主意，"她们偷东西是为了拿回她们父亲的画，他是个艺术家"。 但一开始，Cat's Eye偷了钻石，我很担心如何处理它...但后来我把一切都加到了故事里。这是我第一次做连载，我以前从未真正想过这个故事。 我对什么都是外行。 我不知道什么是对的，什么是错的，我处于一种变动的状态。 我每次都很绝望」他说，这部电影充满了他自己的热情。







## 「俊夫のあのセリフは実話をもとにしています」「希望の先は、皆さんに想像してほしい」  
「俊夫的这句话基于一个真实的故事」「我把它留给你，任由你去想象希望在哪里」  

**※以下部分涉及「Cat's Eye」原作的故事结局的描述。 没有读过这本书的人应该注意了。**  

必死に描き進めた同作は多くのファンの心をつかみ、キャラクター同士の掛け合いや、父親の行方などドラマ性もどんどん魅力的なものになっていった。怪盗であるキャッツアイと、彼女たちの逮捕に執念を燃やす熱血刑事の俊夫との攻防戦、キャッツアイだということを隠して交際を続ける瞳と俊夫の恋の行方に注目が集まるなか、「週刊少年ジャンプ」1984年44号で同作の連載が終了。さらに1985年6号に後日譚が掲載され、こちらが単行本の最終話として収録されている。  
这部拼命画出的作品赢得了许多粉丝的喜爱，戏剧性的内容，如人物之间的对话和她们父亲的下落，变得越来越吸引人。 怪盗猫眼和一心想要抓住她们的热血刑警俊夫之间的攻防战，以及隐瞒自己是猫眼真相、继续交往的小瞳和俊夫之间的爱情，吸引了很多人的注意，但故事的连载在「周刊少年Jump」1984年第44期结束。 后来的一个故事发表在1985年第6期上，被收录为单行本的最后一个故事。  

最終話にかけてキャッツアイの正体が明らかとなり、俊夫はそのうえで瞳への愛を再確認。後日譚では、瞳を追いかけて俊夫がアメリカ入りしたものの、瞳は病気によって記憶を失くし、キャッツアイだったことも、恋人の俊夫の存在すらも忘れてしまった…という展開がつづられ、読者を驚かせた。そこで俊夫が放つ「こんなにすばらしいことってありませんよ…。瞳ともう一度…もう一度恋ができる…」というセリフは、忘れられない名言として心に刻んでいるファンも多いはず。  
在最终话里，Cat's Eye的真实身份被揭穿，然后俊夫重申了他对瞳的爱。后传里，俊夫跟随小瞳来到美国，但她因病失去了记忆，忘记了自己是猫眼，也忘记了她的爱人俊夫的存在......这一发展让读者感到惊讶。 然后许多粉丝会记得俊夫说的那句「没有比这更好的事了....我和小瞳可以再一次 ...再一次恋爱...」这是一句令人难忘的话。  

[![俊夫の名言には、真実が込められていた](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455129?w=615)](https://moviewalker.jp/news/article/1121406/image11455129/)  
俊夫的名台词背后的真实故事[c]Monkey Punch Tsukasa Hojo/鲁邦三世 VS Cat's Eye制作委员会  


後日譚を描いた経緯について、北条は「『キャッツ・アイ』の連載が終わって、次の連載を考えている時に読み切りを描いてほしいと依頼されたんですが、担当編集から『友人の話なんだけど、発熱をしたカミさんが、そのウイルスが頭に入って記憶喪失になってしまった…という人がいて。意識を回復したカミさんに会いに行ったら、あなた誰？と言われたらしい。そいつは落胆するかと思いきや“またカミさんと恋ができる”と言ったらしい。これをネタになにか描けないか？』と言われて。すごくいい話だなと思って」となんと俊夫の名言は実話をもとにしているのだという。  
关于他如何画后传，北条说「『Cat's Eye』的连载结束后，正在考虑下一部连载时，有人让我画一个短篇故事，但我的时任编辑说『这是一个关于一个朋友卡米的故事，她发烧了，脑子里有那种病毒，失去了记忆 ...然后她恢复知觉，当她丈夫去看她时，她问'你是谁？' 那人以为他要失望了，但他说“我可以再次爱上我的妻子”』。 他说:"你能用这个画点什么吗？我认为这是一个很好的故事」。 这就是俊夫的名台词背后的真实故事。  

後日譚では以前登場したオルゴールが物語の鍵を握るが、「後日譚に話をつなげるなら、あのオルゴールを使うしかないと思った」とまるでもとから用意された伏線のような役割を担い、すばらしいゴールを迎えた。感動的な最終話を描ききった北条は、「自分としてはいい最終話になったなと思いましたが、果たして少年誌で受け入れられるのかどうか、不安ではありました。人によっては、残酷な終わり方だと思う人もいるでしょう。100％のハッピーエンドとは言えないですから」と胸の内を吐露しながら、「さらに言えば、父親のハインツ、どうなったんだよ！って話ですよね(笑)」と三姉妹の父親の行方について結末が描かれていないことについてコメント。ファンからも「続編を読みたい」というファンレターが届くこともあるというが、謎を残したままでありながらも北条は「自分としてはお父さんも見つけて、みんなで幸せに暮らしていてほしいなと感じていますが、続編を描こうと思ったことはない」と語る。  
在后传中，之前出现的音乐盒在故事中起到了关键作用，「我想，如果我想把故事和后传联系起来，就必须使用那个音乐盒」，仿佛它起到了从一开始就准备好的预示作用，故事也达到了一个美妙的结局。 完成了动人的最終話的北条说「我认为这对我来说是一个很好的最終話，但我担心它是否会被少年杂志接受。 有些人可能认为这是一个残酷的结局，我也不能说这是一个100%的快乐结局」，他坦言「此外，父亲海因茨怎么样了！就是这样(笑)」。他甚至收到粉丝的来信「要求看续集」。尽管谜团依然存在，北条说「至于我，我觉得，我希望找到父亲，让大家从此过上幸福的生活，但我从未想过要画续集。」（译注：待校对）     

[![北条司先生のアトリエには「キャッツ・アイ」や「シティーハンター」グッズが！](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455238?w=615)](https://moviewalker.jp/news/article/1121406/image11455238/)  
北条司先生のアトリエには「キャッツ・アイ」や「シティーハンター」グッズが！  
北条司先生的工作室里的「Cat's Eye」和「City Hunter」的商品!  

「『キャッツ・アイ』は原作誕生から40年。『キャッツ・アイ』を描いていたころの自分はもういません。40年も経てば、感性も変わるものです。それに僕の好みとして、あまり大団円というのが好きではなくて。『このラストはちょっと悲しいけれど、希望もある。その希望の先になにがあるのかは、皆さんが想像してください』と思いながら描いているところがあります。そして『キャッツ・アイ』の場合、読者の皆さんのなかには、自分なりの続きを考えている人もいると思います。こういうのを描いてくださいと思ってくださっていても、漫画家としてはそれに応えることはできません。その想像と違うものを描いても、きっと納得してもらえないでしょうし(笑)。これだけ年月が経ってしまったら、それぞれの続きを想像で楽しんでいただくのが一番いいのかなと思っています」。  
「自『Cat's Eye』原作问世以来，已经过去了40年。 我已经不再是画『Cat's Eye』时的那个我了，40年后感性也会发生变化。我也不喜欢做一个大团圆的结局。『结局有点悲伤，但有希望。 希望之后是什么，取决于你的想象力』。 对『Cat's Eye』而言，我认为有些读者对故事的后续部分有他们自己的想法。 即使你认为这是你想让我画的东西，作为一个漫画家，我不能对此作出回应。 如果我画的东西与他们想的不同，我肯定他们不会接受（笑）。 经过这么多年，我认为最好是让读者在他们的想象中享受每一个续篇」。（译注：待校对）  

[![数々の名作が生まれたデスク](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455236?w=615)](https://moviewalker.jp/news/article/1121406/image11455236/)  
数々の名作が生まれたデスク  
创作了许多杰作的工作台  
  

北条のなかでいま、「キャッツ・アイ」はどのような作品として残っているのだろうか。「漫画を描くこと自体が自分の恥をさらしているようなものですが、『キャッツ・アイ』は恥をすべて出し切ったような作品」と微笑む。「稚拙で下手くそなところから始まって、もっといいものが描けるんじゃないかと思いながら机に向かっていました。僕は、いまだに絵やストーリー、感情表現にしても、きちんとしたものが描けないなという感覚が強くて。『次はよくなるんじゃないか』『次こそは』と感じながらも、全然思うようにいかない。でもその想いがあるからこそ、ここまで続けてこられたんじゃないかと思っています」と名作誕生の裏側には、飽くなき向上心とたゆまぬ努力が隠れていた。  
今天，「Cat's Eye」在北条的脑海中留下了什么？ 他笑着说「画漫画本身就像暴露我的羞耻心，但『Cat's Eye』暴露了我所有羞耻心」。我从最初很笨拙、画得很丑，走到现在，我认为可以画出更好的东西。 我仍然有一种强烈的感觉，就是我画的不够好，无论是画面、故事还是情感表达。 我一直在想『下次会更好』或『下次会更好』，但结果并不像我想的那样」。 创作这些杰作的幕后，隐藏着精益求精的欲望和不懈的努力。   

[![「ルパン三世VSキャッツ・アイ」は1月27日よりPrime Videoで世界独占配信](https://moviewalker.jp/api/resizeimage/news/article/1121406/11455127?w=615)](https://moviewalker.jp/news/article/1121406/image11455127/)

「鲁邦三世 VS Cat's Eye」将于1月27日起在全球范围内由Prime Video独家提供[c]Monkey Punch Tsukasa Hojo/鲁邦三世 VS Cat's Eye制作委员会    

取材・文/成田おり枝

