
## 若苗家  
首先，若苗家的住址在现实中不存在。  
[FC中多次显示若苗家的住址](../fc_details/readme.md#wakanae-address)为：(东京)丰岛区杂司谷4丁目24-15。但Google Map显示杂司谷只有1丁目、2丁目、3丁目，没有4丁目。    

其次，猜测若苗家的原型可能是"杂司谷旧宣教师馆"。  
FC里若苗家的大门口有灌木丛和围墙：  
![](img/01_018_4__crop0.jpg) 
![](img/08_123_0__crop0.jpg) 
![](img/09_066_2__.jpg) 
![](img/10_150_1__.jpg) 
![](img/11_091_0__crop0.jpg)   
由此猜测其原型可能是"杂司谷旧宣教师馆(McCaleb Old Missionary House / Zoshigaya Missionary House Museum)"([杂司谷1丁目25-5](img-large/02_060_0__real_map.jpg))。  

旧宣教师馆大门口(大门朝北)：  
![](img/02_060_0__real_1-chome-25-5_label.jpg) 
![](img/02_060_0__real_1-chome-25-5_porch.jpg) 
![](img/02_060_0__real_1-chome-25-5_porch-2.jpg) 
![](img/02_060_0__real_1-chome-25-5_porch-3.jpg) 

旧宣教师馆后院(视角朝南):  
![](img/02_060_0__real_1-chome-25-5_yard_south.jpg) 

旧宣教师馆室内照片如下，透过窗子可以看到旧宣教师馆东西两侧的建筑(邻居?)：  
![](img/02_060_0__real_1-chome-25-5_inner_0.jpg) 
![](img/02_060_0__real_1-chome-25-5_inner_1.jpg) 
![](img/02_060_0__real_1-chome-25-5_inner_2.jpg) 
![](img/02_060_0__real_1-chome-25-5_inner_3.jpg)  
以上这些建筑的屋顶的风格类似FC里若苗家大门对面的邻居（只是类似，目前还没有找到实物）：  
![](img/09_064_2__.jpg)  
 

目前(2021年)旧宣教师馆北侧的邻居是一块空地；从google map上可以看到其2008年是如下的民居：  
![](img/1-chome-25-5_north0.jpg) 
![](img/1-chome-25-5_north1.jpg) 

旧宣教师馆南侧的邻居是“平井医院”：  
![](img/1-chome-25-5_sourth.jpg) 

旧宣教师馆到鬼子母神前站[约720m(步行约8min)](img/02_060_0__real_to_station.jpg)。  


## 若苗空的工作台
![](./img/01_185_0__crop1.jpg) 
![](./img/01_185_0__crop2.jpg) 
![](./img/03_007_4__crop0.jpg) 

作者北条司的工作台如下（注意那个白胖矮的钟表、侧前方的挂板）：  
![0](./img/hojo_studio_0.jpg) 
![1](./img/hojo_studio_1.jpg) 
![2](./img/hojo_studio_2.jpg) 
![3](./img/hojo_studio_3.jpg) 
[![4](./img/hojo_studio_4.jpg)](../zz_natalie.mu/2019-02-01_ch_movie01.md)   
  

