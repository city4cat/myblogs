
## 叶子家附近-多摩川浅間神社(Sengen Shrine)

叶子姓氏“浅冈”。家在奥多摩，[据猜测是御岳渓谷，门前的河流是"多摩川"](./yoko-house.md#叶子家周围-御岳溪谷)。  
此外，多摩川的下游有[多摩川浅間神社(Sengen Shrine)](https://pixta.jp/photo/64779711)，距离御岳渓谷直线距离约50km。  
[疑问]叶子的姓氏“浅冈”和浅間神社是否有关系?  
![](img/sengen_shrine_map.jpg) 
![](img/sengen_shrine.jpg)  