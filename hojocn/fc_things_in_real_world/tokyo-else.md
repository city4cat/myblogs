
------------------------  

## 新宿-高楼
02_115_0,03_097_5可能是新宿的高楼。    
![](img/02_115_0__.jpg) 
![](img/03_097_5__.jpg) 

![](img/shinjuku_buildings.jpg) 
![](img/shinjuku_buildings_2.jpg)[2]  

住友大厦的横截面是六边形(或者说是三棱形)。  

实物链接：  

[1] [新宿三井大厦](https://cn.tripadvisor.com/Attraction_Review-g14133673-d1373759-Reviews-Shinjuku_Mitsui_Building-Nishishinjuku_Shinjuku_Tokyo_Tokyo_Prefecture_Kanto.html)   
[2] [东瀛归来(八)：都厅45层俯瞰东京](https://yjx1963.blog.sohu.com/219096621.html)   
[3] [新宿三井大厦（Shinjuku Mitsui Building）](https://zh.wikipedia.org/zh-hans/新宿三井大厦)    
[4] [新宿 住友大厦 （Shinjuku Sumitomo Building）](https://zh.wikipedia.org/wiki/新宿住友大厦)    
[5] [新宿 中心大厦](https://zh.wikipedia.org/wiki/新宿中心大厦)    
[6] [京王廣場大飯店（Keio Plaza Hotel）](https://zh.wikipedia.org/wiki/京王廣場大飯店)  
[7] 更多图片: [新宿三井ビル](https://pixta.jp/tags/新宿三井ビル), [住友ビル](https://pixta.jp/tags/住友ビル)  




------------------------  

## 街道

第47话，熏冒名雅彦玩弄女生。雅彦、紫苑、江岛商量后决定，在假雅彦经常勾搭的那条街上(07_126_0)，由雅彦异装作诱饵。这条街的样子：07_125_1，07_125_2，07_125_4，07_126_0


------------------------  

### 东京-表参道(Omotesando)  

雅彦和叶子第一次圣诞夜约会时，这里应该是东京的表参道：  
![](img/05_098_1__.jpg) 
![](img/05_098_1__real_omotesando.jpg)  
(图片源自[东京爱情故事30年：恋爱教科书还藏着旅行宝典](https://www.latiaozixun.net/20210312/644146F9E8066EF3A5F7E7A139CB3C6C-1.html?li=BBOXPUT))  


------------------------  

### 雅彦和叶子吵架分手的地方 - 新宿地铁站外
14_206   
![](img/14_206_3__.jpg)  
![](img/14_206_4__.jpg)  
实景（图片源自[《北条司研究序说》](http://chiakik.la.coocan.jp/htkj/ch/chwords-places.htm)）：  
![](img/higasiguchi-2.jpg) 
![](img/higasiguchi-4.jpg) 
![](img/higasiguchi-3.jpg) 
