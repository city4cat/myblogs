
------------------------  



## 高知-桂浜(Katsurahama)  {#高知风景-桂浜}  

14_276，大结局里雅彦表白紫苑的海滩应该是高知县高知市的"桂浜(桂滨)公园"里的桂浜海岸。  

### 桂浜公园地图[5]：
(点击图片查看大图)  
[![](img/Katsurahama_map__s.jpg)](img/Katsurahama_map.jpg)  

### 高知机场 --> 桂浜水族館

用[地图](https://cn.gullmap.com)查询“高知机场”到“桂浜水族館(Katsurahama Aquarium)”的驾车路线(约16分钟)：  
![](img/kaochi_airport__aquarium_00.jpg) 
![](img/kaochi_airport__aquarium_01.jpg) 
![](img/kaochi_airport__aquarium_02.jpg) 
![](img/kaochi_airport__aquarium_03.jpg) 
![](img/kaochi_airport__aquarium_04.jpg) 
![](img/kaochi_airport__aquarium_05.jpg) 
![](img/kaochi_airport__aquarium_06.jpg) 
![](img/kaochi_airport__aquarium_07.jpg) 

### 桂浜海岸(Katsurahama Beach)[2,3]

![](img/14_276_4__.jpg) 
![](img/14_276_4__real2.jpg)  

### 山上的亭子(Ryuogu Shrine)[5]：

![](img/14_278_4__crop0.jpg) 
![](img/14_278_4__real.jpg) 
![](img/14_278_4__real_crop0.jpg) 

### 坂本龙马铜像

同样在上图的小山上有坂本龙马铜像(Ryoma Sakamoto Bronze Statue)[5]，新版FC第5卷封面的取景地就是它：  
![](img/fc-new-edition-5.jpg)  


### 桂浜水族館(Katsurahama Aquarium)[3,5]:

![](img/14_279_0__.jpg) 
![](img/14_292_4__.jpg)  
![](img/14_279_0__real_0.jpg) 
![](img/14_292_4__real_1.jpg)  

### 背景里的大石头[3]：

![](img/14_287_4__.jpg) 
![](img/14_287_4__real.jpg)  



参考链接：  

1. [百度百科-高知县-桂滨](https://baike.baidu.com/item/高知县#7_1)  
2. [百度百科-高知县](https://baike.baidu.com/pic/高知县/6618091)  
3. [Bing图片-桂浜](https://cn.bing.com/images/search?q=桂浜)  
4. [日本四国旅游攻略：高知](https://jp.hjenglish.com/new/p440722/) "位于高知市浦户桂浜公园境内有一片濒临太平洋的桂浜海岸"。  
5. [桂浜公園．日本名海岸百選．坂本龍馬銅像](https://www.travalearth.com/post-31405926/)  
6. [高知县十大人气景点](https://www.517japan.com/viewnews-77908.html)  

