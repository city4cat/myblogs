
------------------------  

### 飲料-New Calorie Mate  
03_004_0, 雅彦喝的饮料: New Calorie Mate  
![](img/CALORIE-MATE-Energy-Drink.jpg) 
 
CALORIE MATE能量饮料咖啡馆Au Lait。 Calorie Mate是一种营养均衡的食品，旨在为当今人们的忙碌生活提供饮食支持。 它可以在任何地方饮用，并提供人体所需的营养。 包含人体所需的蛋白质、脂肪、11种维生素和6种矿物质的良好平衡。 （饮料类型含有10种维生素和5种矿物质）

生产商：Otsuka Japan  
产地：日本  
价格：48.5美元/200ml*6罐，(约8美元/罐)  

参考资料： 
[Calorie Mate Energy Drink Cafe Au Lait 200mlx6 cans-Made in Japan](https://www.takaski.com/product/calorie-mate-energy-drink-cafe-au-lait-200ml-x-6-cans-made-in-japan/)


------------------------  

### 飲料-啤酒  

07_178_6。熏家里的啤酒是：Heinebn Premium Beer All Malt 100% . 可能指的是Heineken

12_067_0，成人礼当天凌晨，雅彦和江岛在雅彦卧室里喝的各种酒。


------------------------  

### 飲料-咖啡  
12_148，Blue Thunder, UGG Coffee  
![](img/12_148_3__.jpg)  

网上可以查到Klatch Coffee的Blue Thunder咖啡。[疑问]但我想这里暗指的是不是：UCC(悠诗诗)的Blue Mountain（蓝山）咖啡？  

UCC咖啡是以定点精心培养、种植的咖啡豆为原料，由日本UCC上岛咖啡株式会社生产、销售的世界著名品牌咖啡。UCC在牙买加生产世界顶级的Blue Mountain（蓝山）咖啡。  

参考链接： 
[百度百科-UCC咖啡](https://baike.baidu.com/item/UCC咖啡/)


### 12_173_1，12_173_2  
盲人乐队喝的饮料。  


### 07_157
熏常喝马爹利路易13. 顶级酒，价格约2W RMB。  
![](./img/07_157_1__cn.jpg)  
![](./img/07_157_1__real_Martell.Louis.XIII.jpg)  


### 12_032
（紫苑高中毕业履行。三个男生要把雅美灌醉）他们除了喝啤酒之外，还喝烈酒。  
![](../fc_food/img/12_032_3__.jpg) 
![](../fc_food/img/12_032_4__.jpg)  
应该是日本“一甲单一麦芽威士忌-余市12年”(Nikka Single Malt Whisky Yoichi 12 Year Old)，700~750ml，售价约650美元，2016年获得全球威士忌大奖（World Whiskies Awards）的金奖：  
![](./img/12_032_3__nikka-yoichi-12yo-old-bottling_2.jpg)  

参考链接：   
1. [Nikka Yoichi 12 Year Old Single Malt Japanese Whisky (750ml)](https://www.nicks.com.au/nikka-yoichi-12-year-old-single-malt-japanese-whisky-750ml)  
2. [Yoichi 余市](https://www.wine-world.com/winery/yoichi)  






### 14_018, 14_021，14_022，
影研社迎新聚会(仁科)。他们喝的酒：  
![](../fc_food/img/14_018_0__.jpg) 
![](../fc_food/img/14_021_6__.jpg)  
紫苑和江岛拼酒时喝的酒是：  
![](./img/14_022_5__.jpg)  
![](./img/14_022_5__real_Four.Roses.Black.Label.Fine.Old.Bourbon.jpg)  

Four Roses Black Label, Fine Old Bourbon  
![](./img/14_022_0__.jpg)  
![](./img/14_022_0__real_Austin.Nichols.Wild.Turkey.Straight.Bourbon.jpg)  

Austin Nichols Wild Turkey Straight Bourbon  

参考链接：  
1. [Four Roses Black Label 75cl](https://www.kabukiwhisky.com/besides-japan/bourbon-whsiky/category-166/black-pre2002/four-roses-mr-9-2-90255/)  
2. [Austin Nichols Wild Turkey Kentucky Straight Bourbon Whiskey](https://www.oldspiritscompany.com/products/austin-nichols-wild-turkey-kentucky-straight-bourbon-whiskey-bottled-1995-43-4-75cl)  
