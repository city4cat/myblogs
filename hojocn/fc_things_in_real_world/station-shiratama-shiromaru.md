
## 叶子家附近-白玉-车站  {#白玉-车站}  

叶子坐电车回奥多摩是在白玉站下车。站牌显示：东京都奥多摩町，白玉/Shiratama。可能对应改造前（2011年）的"白丸站/Shiromaru"(青梅线)。  
“白丸站位于日本东京都西多摩郡奥多摩町。该站隶属于JR东日本青梅线，编号JC73。”[1]    

- 站牌：  
![](img/14_247_6__.jpg)  
![](img/14_247_6__real.jpg)  

>文字对比：

>> "白玉/Shiratama" v.s. "白丸/Shiromaru"  
>> "Hatomune" v.s. "Hatonosu"  
>> "Okusama"  v.s. "Oku-tama"  

- 站台侧面:  
![](img/14_260_0__.jpg) 
![](img/14_260_0__crop0.jpg) 
![](img/14_260_0__real.jpg)

- 站台斜侧面:  
![](img/13_096_4__.jpg) 
![](img/14_213_5__.jpg) 
![](img/14_259_1__.jpg) 
![](img/13_096_4__14_213_5__14_259_1__real.jpg)  

- "于2011年进行的改造中，车站候车室外观被改建为了与站名“白丸”非常契合的白色球体状"[1]。  
2011年之后的样子：    
![](img/after_2011_00.jpg)

参考链接：  
1. [这是一座你没见过的，候车室与站名一模一样的车站](https://zhuanlan.zhihu.com/p/61501414)。  
2. [白丸駅 (東京都)](https://ja.wikipedia.org/wiki/白丸駅_(東京都))  
3. [白丸駅コンパクト化 | aran.or.jp](http://www.aran.or.jp/works/4812.html)  
4. [東京都で最も乗降客数が少ないのがここ！ – 珍鉄](http://www.chin-tetsu.com/article/3333)  
5. [JR青梅線 白丸駅/奥多摩周辺/CHSいろいろサイト](https://www.chspmedia.com/ShiromaruSt_Area.html)  
