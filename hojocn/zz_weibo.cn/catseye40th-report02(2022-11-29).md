source:  
https://weibo.com/ttarticle/p/show?id=2309404841217266680050

译注：  

- 本文的[日文版](../zz_edition-88.com/catseye40th-report02(2022-11-28).md)  
- 以下为每幅图配上了高分辨率的图片链接。但遗憾的是，这些高分辨率的图片里的大多数配图文字依然看不清。  

![](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/articles/IMG_3110_1214ccd0-575f-43a2-8ab5-410086b7f0cc_1400x.jpg) ([大图](https://cdn.shopifycdn.net/s/files/1/0587/8009/0526/articles/IMG_3110_1214ccd0-575f-43a2-8ab5-410086b7f0cc_4800x.jpg))  

# 「猫眼三姐妹40周年纪念原画展～然后向着城市猎人～」博多场展会报道  
猫眼三姐妹-城市猎人_官方 作者： EDITION 88 11-29  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3139_600x600.jpg) 
([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3139_4800x4800.jpg))    

东京（Arts 千代田）举办的“猫眼40周年纪念原画展”广受好评，本次来到了北条司老师的故乡，福冈开设了巡回展！  
展会将在CANAL CITY博多举办到11月30日（周三）为止📢  

【展会信息】  
举办时间：2022年11月19日（周六）～11月30日（周三）  
举办地点：CANAL CITY博多 南座B1 Bandai Namco Cross Store博多 内  
营业时间：日本时间10:00～21:00 *最终入场时间为20:30  

展览会场的入口处，装饰着以泪、瞳、爱为印象制作的花篮！是非常华丽的花朵🌹✨  

 

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0985_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0985_4800x4800.jpg))   

为了展会绘制的主视觉图  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3097_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3097_4800x4800.jpg))   

进入后，能看到迎接各位的三姐妹🥰  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3098_e781449e-7090-4fda-bccb-bbdcc355cbb4_1024x1024.jpg)  
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3098_e781449e-7090-4fda-bccb-bbdcc355cbb4_4800x4800.jpg))   

北条老师在东京会场的签绘也在福冈展出了✒️  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3099_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3099_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3100_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3100_4800x4800.jpg))   

接下来…为了来到福冈会场的大家，也准备了新的签绘！！  
感谢北条老师！  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3101_4800x4800.jpg))   

充满存在感、装帧豪华的美丽原画  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3109_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3109_4800x4800.jpg))   

展示柜内可以看到《猫眼三姐妹》的完全版漫画、难得一见的贵重原稿、老师使用的画材和资料等等物品。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3107_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3107_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3106_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3106_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3105_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3105_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3112_4800x4800.jpg))   

有机会可以近距离观看《猫眼三姐妹》彩色原画👀  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3113_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3115_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3120_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3122_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3127_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3127_4800x4800.jpg))   

从《城市猎人-XYZ-》的珍贵原稿开始，可以看到的众多经过严选的原画作品。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3128_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3131_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3131_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3132_4800x4800.jpg))   

可以一口气阅读全83页短篇作品《宇宙天使》的展示区域。  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3136_4800x4800.jpg))   

打卡点在展览会场内部有一个，外部有2个。  
来到现场的各位请一定要来拍照哦📸  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3118_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_3118_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0986_1024x1024.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0986_4800x4800.jpg))   

最后来介绍一下周边贩售区域👐  
预定生产的版画商品，以及B2复制原稿、B2高级艺术打印稿全部实物展示中。  
\EC也同时销售中！/    
[如需购买请点此处](https://edition-88.com/collections/hojotsukasa)  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0997_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0996_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0996_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0998_4800x4800.jpg))   

図録はもちろん、通販で完売していた商品も一部再販中！  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0994_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0994_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_1004_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_1004_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0989_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0989_4800x4800.jpg))   

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0993_600x600.jpg) 
 ([大图](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/IMG_0993_4800x4800.jpg))   

可以一口气买到很多《猫眼三姐妹》和《城市猎人》的周边！  
不仅有适合日用的产品，也有众多适合收藏的商品🌟  
欣赏完展览后，也来慢悠悠地逛逛周边区购买喜爱的东西吧😊  

 

*上次东京场的展会报告请看​​​​[这里](https://weibo.com/7734122862/LumA0qOim?ua=Mozilla%2F5.0+(X11%3B+Ubuntu%3B+Linux+x86_64%3B+rv%3A107.0)+Gecko%2F20100101+Firefox%2F107.0&amp%3Bwvr=6&amp%3Bmod=weibotime&amp%3Btype=comment&type=comment)  




















































