
## Subtitles一览[^wiki]  
[^wiki]: [RASH!! @Wikipedia](https://ja.wikipedia.org/wiki/RASH!!)  

本作では第1話、第2話…ではなく、KARTE1、KARTE2…と表記される。  

1.  Rash Rush !!  (注：Wikipedia中的原文为“Rash Rash !!”，应为错误。)  
2.  Sleeping Beauty  
3.  Father of the Bride  
4.  Cold Sweat  
5.  Conversation Piece  
6.  Wages of Fear  
7.  Looking for Mr.Goodbar  
8.  The Getaway  
9.  Close to You  
10.  True Confessions  
11.  Beauty and Beast  
12.  Scar Face  
13.  The Friendly Persuasion  
14.  Hang'em High  
15.  The Fugitive  
16.  Starting Over  

サブタイトルには、一部を除き、映画のタイトルが引用されており、以下がその引用元である。  

第2話『[眠れる森の美女](https://ja.wikipedia.org/wiki/眠れる森の美女)』　第3話『[花嫁の父](https://ja.wikipedia.org/wiki/花嫁の父)』　第4話『夜の訪問者』　第6話『[恐怖の報酬](https://ja.wikipedia.org/wiki/恐怖の報酬)』　第7話『[ミスター・グッドバーを探して](https://ja.wikipedia.org/wiki/ミスター・グッドバーを探して)』　第8話『[ゲッタウェイ](https://ja.wikipedia.org/wiki/ゲッタウェイ)』　第10話『[告白](https://ja.wikipedia.org/wiki/告白)』　第11話『[美女と野獣](https://ja.wikipedia.org/wiki/美女と野獣)』　第12話『[スカーフェイス](https://ja.wikipedia.org/wiki/スカーフェイス_(映画))』又は『[暗黒街の顔役](https://ja.wikipedia.org/wiki/暗黒街の顔役_(1932年の映画))』　第13話『[友情ある説得](https://ja.wikipedia.org/wiki/友情ある説得)』　第14話『奴らを高く吊るせ!』　第15話『[逃亡者](https://ja.wikipedia.org/wiki/逃亡者)』  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  