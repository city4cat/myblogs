
# 角色的名字

## 朝霞勇希（Rash）  
英文"rash"意为[^rash0]：(adj.)轻率的、鲁莽的;(n.)皮疹、疹子。该词可能源与古英语词缀"-ræsc"或德语，且都有"快速"的意思。[^rash05]  

[^rash0]: [rash @Cambridge Dictionary](https://dictionary.cambridge.org/dictionary/english-chinese-simplified/rash)  
[^rash05]: [rash@Online Etymology Dictionary](https://www.etymonline.com/word/rash)   

[单行本的卷首语](../zz_preambles/rash_review.md)中提到：因为角色性格鲁莽，故作者为其取名为"Rash"。  

英文中的确有姓氏"Rash"：  
- 源于德国的姓氏Rasch、Raschau[^Rash1]。  
- 相近的姓氏有：Rasch, Raasch, Rasche, Raschers, Raschig，等等[^Rash1]。  
- 在日本，姓氏Rash的人多数位于东京、大阪。[^Rash2]  

[^Rash1]: [Rash History, Family Crest & Coats of Arms](https://www.houseofnames.com/rash-family-crest)  
[^Rash2]: [Rash Surname](https://forebears.io/surnames/rash)  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
