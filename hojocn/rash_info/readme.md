# 《RASH!!》的相关信息
该作品连载于1994年～1995年週刊少年Jump，官网标记其为“短篇”。[^official]可[在线浏览](https://mangahot.jp/site/works/j_R0020)（前2话免费）。[^rashol]

[^rashol]: [RASH Online @mangahot](https://mangahot.jp/site/works/j_R0020)  

[卷首语/Author's Review](../zz_preambles/rash_review.md)  
[角色名字](./rash_char_name.md)  
[每话标题](./rash_subtitles.md)  

[^official]: [北条司 Official Website](https://hojo-tsukasa.com/gallery)  

## Links  
[RASH!! @Wikipedia](https://ja.wikipedia.org/wiki/RASH!!)  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  