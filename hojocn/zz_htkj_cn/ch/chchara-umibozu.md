INDEX > CITY HUNTER > ミニ研究 >

# 人物简介 / 海坊主

## 基本信息

<table class="mt11" border="1">
  <tbody>
    <tr>
      <td class="nh8" width="90" valign="bottom" bgcolor="#F0F8FF"><font color="#003366">假名</font></td>
      <td class="nh8" valign="bottom">ウミボウズ／ファルコン／イジュウイン　ハヤト</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">全名</td>
      <td class="t11">海坊主／Falcon／伊集院 隼人（本名）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">生年月日</td>
      <td class="t11">不明</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">年齢</td>
      <td class="t11" valign="top">不明</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">性別</td>
      <td class="t11">男</td>
    </tr>
    <tr>
      <td class="tit11" bgcolor="#F0F8FF">血液型</td>
      <td class="t11">不明</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">住所</td>
      <td class="t11">？(目前是一家商店和住所: CAT'S EYE咖啡厅）他有很多藏身之处。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">職業</td>
      <td class="t11">
      <p>Sweeper/CAT'S EYE咖啡厅老板</p>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">身体的特徴</td>
      <td class="t11">大个子，据认为身高超过2米。 钢铁之躯，即使是38口径的枪弹也无法穿透(!?) (#24)。力大无比。<br>
      光头、小胡子、总戴着墨镜（因为他是盲人）。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">別名</td>
      <td class="t11">
      <table>
        <tbody>
          <tr>
            <td class="t11" width="80" valign="top">「海坊主」</td>
            <td class="t11" valign="top">獠听到他的真名后觉得太严肃，便给他起了绰号。 绰号很快在他的朋友中传开了，没有人再叫他的真名。</td>
          </tr>
          <tr>
            <td class="t11" width="80" valign="top">「Falcon」</td>
            <td class="t11" valign="top">来自雇佣兵时代的名字。 这个名字在国外显然很常见。美樹也叫他这个名字。 猎鹰是 "falcon"，意思是「隼」。</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">家庭</td>
      <td class="t11">直系亲属关系不明。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">最喜欢的枪</td>
      <td class="t11">SMITH & WESSON M629 44Magnum</td>
    </tr>
  </tbody>
</table>

## 経歴

-    据信他曾经作为雇佣兵周游世界。 (没有关于他职业生涯的进一步细节）。  
-    在一个国家的战场上，他遇到了约8岁的孤儿美樹，并教她生存技能。 后来她加入了他所在的部队，成为一名游击队员，但为了让她远离战场，他假装自己不再做雇佣兵。  
-    有一次，他在战场上遇到了还是少年游击队员的獠。 当时，因天使尘而错乱的獠，歼灭了海坊主所在的部队，他的眼睛因此受伤。  
-    他以隼（Falcon）的名义向冰室刚司的女儿冰室真希提供援助，冰室刚司是他雇佣兵时期的上级军官，也是救了他一命的人。(#24)  
-    1990年，他受伤的眼睛恶化了，几乎失明。 在这之前，他向獠挑战，要求决斗以解决这个问题，但他们打成平手。(#47)  
-    1991年、和美樹結婚。(#60)   

## 特殊技能

-    布置陷阱。 尽管他身体魁梧，但据说他设置的陷阱却很精致。 他还擅长爆破，如设置炸药打穿建筑物的地板。
-    当然，他也能熟练使用枪支和其他武器。 他经常使用大型武器，如火箭筒。  
-    他力大无比，有出色的格斗技巧。   

## 性格

-    勇敢但温柔。（译注：待校对）  
-    他不爱说话，但在与獠交谈时却说得很多。 口头禅「哼」  
-    与獠相反，他对女人纯情。 他是一个害羞的人，谈到他生活的那一面时很容易脸红。 (但他结婚太早了…)  
-    仔细想想，我觉得他对香的态度特别好…   

## 弱点

猫。　一碰就晕倒。  

## 兴趣爱好
◆ 	？　取笑獠？    
◆ 	波本酒（Wild Turkey）是首选。  

[BACK](./chchara-saeko.md)  
[NEXT](./chchara-miki.md) 