INDEX > CITY HUNTER > ミニ研究 >

# CITY HUNTER　年表

从人物明确的年龄和原作的情节中可以推断出过去的事件和年代。  
槇村的年龄是基于他早产的假设（1/1～3/31）。  

Jump to：[从年表来看…](./chnempyo.md#kousatsu)  


<center>
<table width="95%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
      <table width="100%" border="1">
        <tbody>
          <tr>
            <td class="tit10" bgcolor="#F0F8FF" align="center">年</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">香</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">獠&amp;槇村</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">其他</td>
            <td class="tit10" bgcolor="#F0F8FF" align="center">事件</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF" align="center">19??</td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF">小时候，獠乘坐的飞机坠毁在中美洲一个内战中的小国。<br>
            到达一个游击队村庄，被海原和其他人训练成士兵。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#F7F7F7" align="center">1956</td>
            <td class="t10" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7" align="center">槇村0歳</td>
            <td class="t10" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7">槇村出生</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF" align="center">1965</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">0歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">槇村9歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF">香出生。<br>
            香的亲生父亲・久石纯一，在杀人后逃亡时死于一场事故。<br>
            香成为槇村家的养女。</td>
          </tr>
          <tr>
            <td class="t10" rowspan="2" bgcolor="#F7F7F7" align="center">196?</td>
            <td class="t10" rowspan="2" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" rowspan="2" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7">海原营救被俘的獠时失去了左腿。<br>
            海原给獠注射A.D。（译注：待校对）<br>
            遭遇注射了A.D的獠，海坊主眼睛受伤。（译注：待校对）<br>
            海原被逐出游击队。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#F7F7F7" align="center">美樹8歳</td>
            <td class="t10" bgcolor="#F7F7F7">美樹在一个国家遇到了海坊主，并接受了生存训练。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF" align="center">1970</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">5歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">槇村14歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF">槇村的父亲去世。 槇村负责保管戒指。</td>
          </tr>
          <tr>
            <td class="t10" rowspan="2" bgcolor="#F7F7F7" align="center">1971?<br>
            ～<br>
            1978?</td>
            <td class="t10" rowspan="2" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" rowspan="2" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7">獠所在国家的内战结束？<br>
            獠、Mary的父亲和Mary去到美国，成为一名清道夫。<br>
            此后不久，Mary的父亲去世。 獠与Mary合作。<sup>*1</sup><br>
            獠和Mick搭档，自称City Hunter。<br>
            獠与肯尼・菲特搭档。<br>
            獠与肯尼决斗；肯尼死亡。<br>
            獠抵达日本。 他从绑匪手中救出了年幼的片岡優子，并给了她吊坠。<sup>*2</sup></td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#F7F7F7" align="center">美樹14歳</td>
            <td class="t10" bgcolor="#F7F7F7">Falcon让美樹不要再当雇佣兵。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF" align="center">1979</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">14歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF">獠由真柴憲一郎校准了枪，并见到了他女儿由加里。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#F7F7F7" align="center">1981?</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">16歳</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">槇村26歳</td>
            <td class="t10" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7">槇村辞去了他的刑警工作，并与獠搭档</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF" align="center">1982</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">17歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">槇村27歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF">3月26日 香（高2，当时只有16岁）初次见到獠。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#F7F7F7" align="center">1985</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">20歳</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">槇村29歳</td>
            <td class="t10" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7">3月26日　香与獠重逢。<br>
            3月31日　槇村被乐生会杀害。香，成为獠的搭档。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF" align="center">1986</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">21歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF" align="center">霞17岁<br>
            小百合23岁</td>
            <td class="t10" bgcolor="#FFFFFF">獠见到麻生霞（译注：待校对）<br>
            小百合和香的母亲去世。 小百合得知她的妹妹还活着，便开始寻找。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#F7F7F7" align="center">1987</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">22歳</td>
            <td class="t10" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7" align="center">麗香24歳</td>
            <td class="t10" bgcolor="#F7F7F7">RN侦探社开业</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF" align="center">1988</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">23歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF" align="center">霞19岁<br>
            小百合25岁</td>
            <td class="t10" bgcolor="#FFFFFF">獠与霞重逢<br>
            小百合寻找香，但没有告诉她真相就去了美国。（译注：待校对）</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#F7F7F7" align="center">1989</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">24歳</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">(獠30歳?)</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">美樹2?歳<br>
            霞20岁<br>
            </td>
            <td class="t10" bgcolor="#F7F7F7">Mary到访日本；1月15日，美樹和霞生日聚会。<br>
            香得知獠的往事。獠的生日（3月26日）。(顺带一提，北条司这一年30岁。）</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#FFFFFF" align="center">1990</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">25歳</td>
            <td class="t10" bgcolor="#FFFFFF" align="center">(獠31歳?)</td>
            <td class="t10" bgcolor="#FFFFFF" align="center"><font color="#FFFFFF">-</font></td>
            <td class="t10" bgcolor="#FFFFFF">桑妮亚・菲特到访日本。<br>
            獠、海坊主决斗，但打平手。</td>
          </tr>
          <tr>
            <td class="t10" bgcolor="#F7F7F7" align="center">1991</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">26歳</td>
            <td class="t10" bgcolor="#F7F7F7" align="center">(獠32歳?)</td>
            <td class="t10" bgcolor="#F7F7F7" align="center"><font color="#F7F7F7">-</font></td>
            <td class="t10" bgcolor="#F7F7F7">Mick到访日本。<br>
            乐生会再次来到日本。（译注：待校对）<br>
            獠在决斗中击败海原。<br>
            美樹和海坊主的婚礼仪式。<br>
            獠从克洛兹将军的部队中救出香。</td>
          </tr>
        </tbody>
      </table>
      </td>
          </tr>
        </tbody>
      </table>

<table width="95%">
  <tbody>
    <tr>
      <td class="t10" valign="top">*1</td>
      <td class="t10" valign="top">从獠的台词中可以推断出獠与Mary共事的时期。<br>
      JC第23卷84页「十几年前的事还记在心里，企图让我们搭档互相残杀」<br>
      这句话是在1989年说的。 如果是十年前，那就是1975年～1979年。</td>
    </tr>
    <tr>
      <td class="t10" valign="top">*2</td>
      <td class="t10" valign="top">有人认为獠在抵达日本后不久就遇到了片冈优子，但如果是在1985年的十年前，即1975年，这与其他事件不符。<br>
      …这是，嗯，时间的缺失环节(?)那么…(^^;)</td>
    </tr>
  </tbody>
</table>
</center>
      

## 从年表来看…

獠・海坊主・冴子3人不适合编入年表。（译注：待校对）  

獠和槇村谁的年龄大呢？　獠看起来比较年轻（在我看来），因为槇村的脸比较老，但根据他的过去，我感觉槇村比獠大。 在时间轴上，出生年月是模糊的，年龄是香决定后的临时年龄，但如果獠和槇村一样，在連載終了時，他可能是35岁甚至更大（香在JC第24卷123页的评论似乎是对的）。 #48中，出现了自称是獠的祖父的神宮寺先生，但我认为他不是獠的祖父。  

考虑到海坊主与美樹的关系，海坊主在连载开始时可能已经是40岁左右，但他看起来不太像。 (好吧，考虑到施瓦辛格出演终结者时已经38岁，不能说这不可能…？）  
…作者在JC26卷的封面折页评论中直言不讳地指出「应该把他们设定为不会变老」，但似乎这3人已经成为永恒的人物。(^^;)  

<font color=#004d99>
…啊，所以我想知道我是否在Angel Heart的世界中转世（？）？（译注：待校对）  
而其余的成员因为年事已高而无法跟上？  
…开个玩笑。    
</font>

[BACK](./chchara-makimura.md)  
[NEXT](./chname.md)  