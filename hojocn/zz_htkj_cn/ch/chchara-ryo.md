INDEX > CITY HUNTER > ミニ研究 >

# 人物简介 / 冴羽 獠
## 基本信息


<table class="mt11" border="1">
  <tbody>
    <tr>
      <td class="nh8" width="90" valign="bottom" bgcolor="#F0F8FF"><font color="#003366">假名</font></td>
      <td class="nh8" valign="bottom">サエバ　リョウ</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">全名</td>
      <td class="t11">冴羽　獠　<span style="font-size:9pt">（但他出生时的名字未知。<img src="../ref.gif" width="13" height="12" border="0"><a href="chname.md">「名字的秘密」</a>）</span></td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">生年月日</td>
      <td class="t11">不明　<span style="font-size:9pt">（他本人也不知道。 香选择的生日是3月26日）</span></td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">年齢</td>
      <td class="t11" valign="top">不明　<span style="font-size:9pt">（他本人也不知道。 根据香确定的年龄，他在该连载结束时是32岁）</span></td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">性別</td>
      <td class="t11">男</td>
    </tr>
    <tr>
      <td class="tit11" bgcolor="#F0F8FF">血液型</td>
      <td class="t11">AB型　（动画里的設定）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">住所</td>
      <td class="t11">東京都新宿区大字6-???　Saeba公寓601号</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">職業</td>
      <td class="t11">
      <p>清道夫/表面上只是个花花公子，或自称是公寓管理员。</p>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">身体的特徴</td>
      <td class="t11">
      <table>
        <tbody>
          <tr>
            <td class="tit11" width="45" valign="top">原作</td>
            <td class="t11" valign="top">身長186cm、体重72kg<br>
            （在Jump20周年纪念时发表的人物数据<sup>*</sup>。然而，它有可能后来被改编为适合动画的设定。）</td>
          </tr>
          <tr>
            <td class="tit11" width="45" valign="top">アニメ</td>
            <td class="t11" valign="top">身長191.4cm、体重77.3kg<br>
            （「百万美元阴谋」中出现的CIA文件指出，他身高6英尺4英寸，体重176磅。 其他信息：性别-男、出生日期-未知、国籍-未知（日本？）、眼睛颜色-灰、头发颜色-黑、住址-日本东京新宿，等。）<br>
            肌肉发达，身体匀称。</td>
          </tr>
          <tr>
            <td colspan="2" class="t10" align="right">*表示参考了Saeba Ryo的网站「Sweeper
            Office」。</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">別名</td>
      <td class="t11">
      <table>
        <tbody>
          <tr>
            <td class="t11" width="125" valign="top">「City Hunter」</td>
            <td class="t11" valign="top">毋庸置疑，獠的别名。 最初是他在美国时与Mick搭档时起的代号。</td>
          </tr>
          <tr>
            <td class="t11" width="120" valign="top">「新宿种马」</td>
            <td class="t11" valign="top">当獠给海坊主起绰号「海坊主」时，作为回报他给獠的名字。 这是他在#11里访问教授的住处时透露的，当时教授通过对讲机问他是谁，他回答说的。</td>
          </tr>
          <tr>
            <td class="t11" width="120" valign="top">「BabyFace」</td>
            <td class="t11" valign="top">#11里，教授这样称呼。（译注：第4卷160页）（<img src="../ref.gif" width="13" height="12" border="0"><a href="chpast.md">「獠的過去」</a>）</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">家庭</td>
      <td class="t11">父母在他小时候死于飞机失事。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">最喜欢的枪</td>
      <td class="t11">Colt Python 357 Magnum</td>
    </tr>
    <tr>
      <td class="tit11" bgcolor="#F0F8FF">备注</td>
      <td class="t11">吸烟习惯。 香烟的品牌不详。 打火机显然是Zippo。(#44)</td>
    </tr>
  </tbody>
</table>


## 经历

另见「[獠的過去](./chpast.md)」和「[年表](./chnempyo.md)」。  

-    小时候，他与父母乘坐的飞机在中美洲一个内战中的小国坠毁，他是唯一的幸存者，到达一个反政府游击队的村庄，在那里他被培养成游击队士兵。  
-    内战结束后，他和一个游击队战友一起去了美国，开始和他一起做清道夫工作。  
-    在他的搭档去世后，与他的女儿Mary和同为清道夫的Mick Angel合作，但在一次决斗中再次失去搭档肯尼・菲特后，他独自去了日本，在新宿落脚。  
-    他与前侦探槇村秀幸合作，但槇村被一个贩毒团伙杀害后，他的妹妹香一直作为他的搭档至今。  

## 特殊技能

他的武器一般都很出色，但他的枪法特别神。  


<table class="mt11" border="1">
  <tbody>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">射撃</td>
      <td class="t11">有可能在大约10米远处将六颗子弹全部射入一个孔中（又称单孔射击）。 甚至可以直接向对手的枪口射击。<br>
      他也是一个很好的狙击手，可以在1km以外打爆对手衣服上的纽扣(!)。 他技术高超，以至于他能够做到这一点。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">刀</td>
      <td class="t11">仅次于枪支的第二大常用武器。 特别是小型投掷刀。 有时穿在衣服里面。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">格斗能力</td>
      <td class="t11" valign="top">即使赤手空拳也相当强（獠说）。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">发声模仿写</td>
      <td class="t11">自称是「有七种声音的人」。 然而，它在后半部分没有出现。</td>
    </tr>
    <tr>
      <td class="tit11" bgcolor="#F0F8FF">料理</td>
      <td class="t11">令人惊讶的是，他似乎是个好厨师，#8里他作为家庭教师为沙也加做饭和午餐盒，在#18里，他作为厨师做了一大桌丰盛的饭菜（尽管从未显示实际的烹饪过程）。 …但自从香和他一起生活后，他下厨似乎减少了…？　他自己也是个贪吃鬼。 他对咖啡的口味似乎也很挑剔。（译注：待校对）(<img src="../ref.gif" width="13" height="12" border="0"><a href="chsyokutaku.md">「冴羽家的餐桌」</a>)</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">博学</td>
      <td class="t11">他在一所女子大学讲授社会心理学（#11。…虽然有作者的注释「到底在上什么课？」（译注：4卷68页）），并熟悉源氏物语与和歌诗选等经典作品（#33、#34）。<br>
      这些知识来源于书吗？ 公寓6层～7层中庭的墙壁上有书架，上面似乎摆放着报纸、杂志和其他过去的资料。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">记忆力</td>
      <td class="t11">「过去十年的所有案件都在我脑袋里」（#9）。 甚至在#44中，他也是这样做的，他偷偷拿出了一篇关于过去案件的文章，而香在书架上拼命地找。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">语言能力</td>
      <td class="t11">在语言方面，CH世界是<s>不错的</s>相当充分的<span style="font-size:8pt;">（嘿，删除线是什么意思？）</span>。我敢说，獠至少会说4种语言。（译注：待校对）
      <table>
        <tbody>
          <tr>
            <td class="t10" width="70" valign="top">日本語：</td>
            <td class="t10" valign="top">通常情况下的对话。 然而，他刚到日本时说话并不流利(#11)。</td>
          </tr>
          <tr>
            <td class="t10" width="70" valign="top">英語：</td>
            <td class="t10" valign="top">他在美国的时候似乎说这个语言。 #47的回忆的对话中包含了「NO...」这个词。</td>
          </tr>
          <tr>
            <td class="t10" width="70" valign="top">西班牙語：</td>
            <td class="t10" valign="top">虽然他在作品中没有实际说这个语言，但在他度过童年的中美洲国家，他应该会说话。 另外，在小说「CITY HUNTER SPECIAL」中，西班牙语对话是用西班牙语说的。</td>
          </tr>
          <tr>
            <td class="t10" width="70" valign="top">塞利斯纳語：</td>
            <td class="t10" valign="top">#27「机场示爱」的结尾，他对阿露玛公主耳语的那几句话是横着写的，似乎是用塞利斯纳语写的。 此外，其内容让人很难相信他是用一种语言学习的。 他是如何知道的，这是一个谜。<br>
            （JC第15卷138页，当被绑架的阿尔玛公主被救出时，有一个分格是她用日语回答用塞利斯纳语说的敌方线路，但这似乎是一个印刷错误，并BUNCH WORLD里，敌人的对话已被纠正为日语。（译注：待校对））</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
  </tbody>
</table>

## 性格

-    工作中的冷静和硬朗（…或应该是这样）。  
-    喜欢女人。  
-    出乎意料的妈妈式性格（尤其是在与女孩的关系上）。 到目前为止，已经确定了几份秘密备忘录。（译注：待校对）  
-    （「渚的经理和親衛隊女孩的数据」「东京制服清单」等…^^;）（译注：待校对）  
-    尽管女性経験豊富(?)，但他笨拙而害羞。  
-    他说不喜欢孩子，但最后还是照顾得很好。  
-    自称是「日本最阳光的人」…但事实上，据说他内心深处有阴影。（译注：待校对）   

## 弱点

-    飞机恐惧症（害怕飞行）。 显然因童年时的一次坠机事故而受到创伤。  
-    美女。…另外，当看到可爱的内衣时，无法控制自己。(^^;)  
-    香。（￣ー￣）   

## 嗜好　(…?)

-    搭讪美女，然后带她们到酒店去❤…这爱好真是…  
-    喝酒。 酒量特别大。   

## 秘密兵器

有各种奇怪的秘密武器…(^^;)  

-    藏在后牙的速效安眠药（#12，#39）：逼不得已时，把它嚼碎，然后亲吻对方，让对方吞下去（…嗯，对方必须是女人才行^^;）。  
-    百衲裤(#12)：由不同颜色的布料拼接而成的内裤。 红色是麻醉药，白色是自白药，黄色是强力安眠药。 诀窍是，如果你把它们切开，并把它们浸在60°C或更高温度的热水中，药物就会渗出。 (…是什么…^^;)  
-    腰带扣：小型折刀。  
-    大衣：经常在作品后半部分穿的长而破的大衣。 它可能藏了枪支和其他武器、小型定时炸弹、炸药或催泪瓦斯。 有些很危险…(^^;)   

总之他是个很奇怪的英雄…  

[BACK](./chkenkyu.md)  
[NEXT](./chchara-kaori.md)  