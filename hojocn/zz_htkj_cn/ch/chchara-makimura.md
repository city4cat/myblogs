INDEX > CITY HUNTER > ミニ研究 >

# 人物简介 / 槇村 秀幸
## 基本信息


<table class="mt11" border="1">
  <tbody>
    <tr>
      <td class="nh8" width="90" valign="bottom" bgcolor="#F0F8FF"><font color="#003366">假名</font></td>
      <td class="nh8" valign="bottom">マキムラ　ヒデユキ</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">全名</td>
      <td class="t11">槇村 秀幸</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">生年月日</td>
      <td class="t11">1956年?月?日</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">年齢</td>
      <td class="t11" valign="top">享年29歳</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">性別</td>
      <td class="t11">男</td>
    </tr>
    <tr>
      <td class="tit11" bgcolor="#F0F8FF">血液型</td>
      <td class="t11">不明</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">住所</td>
      <td class="t11">一个普通的公寓楼</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">職業</td>
      <td class="t11">
      <p class="t11">獠的合伙人。 曾是警視庁的一名刑警</p>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">身体的特徴</td>
      <td class="t11">身高和体重不详（比獠矮）。 眼睛略微倾斜，戴着眼镜。 略微驼背。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">家族</td>
      <td class="t11">父亲（刑警。 在香5岁时去世），收养的妹妹（香）。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">最喜欢的枪</td>
      <td class="t11">COLT LAWMAN MK III .357 MAGNUM</td>
    </tr>
  </tbody>
</table>

## 経歴

-    父亲去世后，似乎他和香就一直生活在一起，但细节不详。 
-    在警視庁担任刑警时，他与冴子搭档。 两人当时的绰号是「警視庁的月亮和勺子」（据獠说）。  
-    在他负责的一起贩运人口案件的卧底行动失败后，他辞职了，并被追究责任。 后来，他开始与獠合作。  
-    1985年3月31日，他被毒品团伙乐生会的杀手袭击，死前未能告诉香的出生秘密，并把香托付给獠。  

## 特技

-    他们说他很会用枪和刀。（#4）。  
-    他的厨艺很好，甚至比香(#50)还要好。 是不是因为他为了抚养妹妹而扮演父母的角色？  

## 性格

有强烈的正义感和一颗温暖的心。 他不善言辞。  
一个男子汉，不帅，但饱经风霜。（译注：待校对）   
他对妹妹香感情深厚，也深爱着恋人冴子。 …但他们没有结婚，也许是因为香的存在。  

## 弱点

当涉及到妹妹(香)时，他的性格似乎发生了变化…我不知道这是否能算作他为弱点… 

## 兴趣爱好

不明。

[BACK](./chchara-miki.md)  
[NEXT](./chnempyo.md)  