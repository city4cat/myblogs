INDEX > CITY HUNTER > アニメーション >

# Main Staff　－劇場版・Special－

<CENTER>
<TABLE cellpadding="0" width="95%">
  <TBODY>
    <TR>
      <TD bgcolor="#191970">
<TABLE border="1" cellpadding="2" width="100%">
  <TBODY>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10" rowspan="2">Title</TD>
      <TD bgcolor="#F0F8FF" class="tit10">愛与宿命的连发枪</TD>
      <TD bgcolor="#F0F8FF" class="tit10">Bay City Wars</TD>
      <TD bgcolor="#F0F8FF" class="tit10">百万美元阴谋</TD>
      <TD bgcolor="#F0F8FF" class="tit10">The Secret Service</TD>
      <TD bgcolor="#F0F8FF" class="tit10">Goodbye My Sweetheart</TD>
      <TD bgcolor="#F0F8FF" class="tit10">紧急直播 !?<BR>
            凶恶罪犯冴羽獠的死</TD>
    </TR>
    <TR>
            <TD bgcolor="#ffffff" class="t10">1989年/87分</TD>
      <TD bgcolor="#ffffff" class="t10">1990年/42分</TD>
      <TD bgcolor="#ffffff" class="t10">1990年/47分</TD>
      <TD bgcolor="#ffffff" class="t10">1996年1月5日/100分</TD>
      <TD bgcolor="#ffffff" class="t10">　1997年4月25日</TD>
      <TD bgcolor="#ffffff" class="t10">　1999年4月23日</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">企画</TD>
      <TD bgcolor="#ffffff" class="t10">諏訪道彦（读卖电视台）</TD>
      <TD bgcolor="#ffffff" class="t10">諏訪道彦</TD>
      <TD bgcolor="#ffffff" class="t10">諏訪道彦</TD>
      <TD bgcolor="#ffffff" class="t10">杉本昌信<BR>
      諏訪道彦<BR>
      植田益朗</TD>
      <TD bgcolor="#ffffff" class="t10">位寄雅雄<BR>
      諏訪道彦<BR>
      植田益朗</TD>
      <TD bgcolor="#ffffff" class="t10">諏訪道彦<BR>
      山下準<BR>
      指田英司</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">原作</TD>
      <TD bgcolor="#ffffff" class="t10">北条司</TD>
      <TD bgcolor="#ffffff" class="t10">北条司</TD>
      <TD bgcolor="#ffffff" class="t10">北条司</TD>
      <TD bgcolor="#ffffff" class="t10">北条司</TD>
      <TD bgcolor="#ffffff" class="t10">北条司</TD>
      <TD bgcolor="#ffffff" class="t10">北条司</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">制片人 </TD>
      <TD bgcolor="#ffffff" class="t10">植田益朗（Sunrise）<BR>
      諏訪道彦（读卖电视台）<BR>
      小松茂明（日本Victor）</TD>
      <TD bgcolor="#ffffff" class="t10">植田益朗（Sunrise）<BR>
      諏訪道彦（读卖电视台）<BR>
      小松茂明（日本Victor）</TD>
      <TD bgcolor="#ffffff" class="t10">植田益朗（Sunrise）<BR>
      諏訪道彦（读卖电视台）<BR>
      小松茂明（日本Victor）</TD>
      <TD bgcolor="#ffffff" class="t10">諏訪道彦（读卖电视台）<BR>
      指田英司（Sunrise）</TD>
      <TD bgcolor="#ffffff" class="t10">諏訪道彦（读卖电视台）<BR>
      指田英司（Sunrise）</TD>
      <TD bgcolor="#ffffff" class="t10">諏訪道彦（读卖电视台）<BR>
      山下準（日本电视网公司）<BR>
      赤崎義人（Sunrise）</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Assistant Producer</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">大橋千恵雄</TD>
      <TD bgcolor="#ffffff" class="t10">大塚峰子（读卖电视台）<BR>
      馬田享子（日本电视网公司）</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">監督</TD>
      <TD bgcolor="#ffffff" class="t10">儿玉兼嗣</TD>
      <TD bgcolor="#ffffff" class="t10">儿玉兼嗣</TD>
      <TD bgcolor="#ffffff" class="t10">儿玉兼嗣</TD>
      <TD bgcolor="#ffffff" class="t10">儿玉兼嗣</TD>
      <TD bgcolor="#ffffff" class="t10">山崎和男</TD>
      <TD bgcolor="#ffffff" class="t10">奥脇雅晴</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">脚本</TD>
      <TD bgcolor="#ffffff" class="t10">遠藤明範</TD>
      <TD bgcolor="#ffffff" class="t10">平野靖士</TD>
      <TD bgcolor="#ffffff" class="t10">外池省二</TD>
      <TD bgcolor="#ffffff" class="t10">遠藤明範<BR>
      儿玉兼嗣</TD>
      <TD bgcolor="#ffffff" class="t10">日暮裕一</TD>
      <TD bgcolor="#ffffff" class="t10">岸間信明</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Storyboard</TD>
      <TD bgcolor="#ffffff" class="t10">不明…（儿玉先生?）</TD>
      <TD bgcolor="#ffffff" class="t10">大熊朝秀</TD>
      <TD bgcolor="#ffffff" class="t10">儿玉兼嗣</TD>
      <TD bgcolor="#ffffff" class="t10">儿玉兼嗣</TD>
      <TD bgcolor="#ffffff" class="t10">山崎和男</TD>
      <TD bgcolor="#ffffff" class="t10">奥脇雅晴</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">演出</TD>
      <TD bgcolor="#ffffff" class="t10">山口美浩</TD>
      <TD bgcolor="#ffffff" class="t10">今西隆志</TD>
      <TD bgcolor="#ffffff" class="t10">江上潔</TD>
      <TD bgcolor="#ffffff" class="t10">儿玉兼嗣<BR>
      山口裕司</TD>
      <TD bgcolor="#ffffff" class="t10">杉山慶一<BR>
      渡邊哲哉</TD>
      <TD bgcolor="#ffffff" class="t10">大原実</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">演出助手</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">吉村章</TD>
      <TD bgcolor="#ffffff" class="t10">喜多幡徹</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">演出協力</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">片山一良</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Character Design</TD>
      <TD bgcolor="#ffffff" class="t10">神村幸子</TD>
      <TD bgcolor="#ffffff" class="t10">神村幸子</TD>
      <TD bgcolor="#ffffff" class="t10">神村幸子</TD>
      <TD bgcolor="#ffffff" class="t10">神村幸子</TD>
      <TD bgcolor="#ffffff" class="t10">神村幸子<BR>
      佐藤敬一</TD>
      <TD bgcolor="#ffffff" class="t10">佐藤敬一</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">総作画監督</TD>
      <TD bgcolor="#ffffff" class="t10">北原健雄</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">佐藤敬一</TD>
      <TD bgcolor="#ffffff" class="t10">佐久間信一</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">作画監督</TD>
      <TD bgcolor="#ffffff" class="t10">北原健雄<BR>
      神志那弘志</TD>
      <TD bgcolor="#ffffff" class="t10">北原健雄</TD>
      <TD bgcolor="#ffffff" class="t10">神村幸子</TD>
      <TD bgcolor="#ffffff" class="t10">北原健雄</TD>
      <TD bgcolor="#ffffff" class="t10">羽山賢二<BR>
      河南正昭</TD>
      <TD bgcolor="#ffffff" class="t10">佐久間信一</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">作画監督補</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">佐藤K<BR>
      齋藤千春<BR>
      大河原春男</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">机械设计</TD>
      <TD bgcolor="#ffffff" class="t10">明貴美加</TD>
      <TD bgcolor="#ffffff" class="t10">小原渉平</TD>
      <TD bgcolor="#ffffff" class="t10">小原渉平</TD>
      <TD bgcolor="#ffffff" class="t10">ヲギミツム</TD>
      <TD bgcolor="#ffffff" class="t10">ヲギミツム</TD>
      <TD bgcolor="#ffffff" class="t10">宮沢努</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">机械动画导演</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">宮澤努</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">美術監督</TD>
      <TD bgcolor="#ffffff" class="t10">東 潤一</TD>
      <TD bgcolor="#ffffff" class="t10">東 潤一</TD>
      <TD bgcolor="#ffffff" class="t10">本田修</TD>
      <TD bgcolor="#ffffff" class="t10">東 潤一</TD>
      <TD bgcolor="#ffffff" class="t10">宮前光春</TD>
      <TD bgcolor="#ffffff" class="t10">菅原清二</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">撮影監督</TD>
      <TD bgcolor="#ffffff" class="t10">古林一太</TD>
      <TD bgcolor="#ffffff" class="t10">長谷川洋一</TD>
      <TD bgcolor="#ffffff" class="t10">長谷川洋一</TD>
      <TD bgcolor="#ffffff" class="t10">長谷川洋一</TD>
      <TD bgcolor="#ffffff" class="t10">桶田一展</TD>
      <TD bgcolor="#ffffff" class="t10">平田隆文</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">音響監督</TD>
      <TD bgcolor="#ffffff" class="t10">浦上靖夫</TD>
      <TD bgcolor="#ffffff" class="t10">浦上靖夫<BR>
      小林克良</TD>
      <TD bgcolor="#ffffff" class="t10">浦上靖夫<BR>
      小林克良</TD>
      <TD bgcolor="#ffffff" class="t10">浦上靖夫</TD>
      <TD bgcolor="#ffffff" class="t10">浦上靖夫</TD>
      <TD bgcolor="#ffffff" class="t10">浦上靖夫</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">音楽</TD>
      <TD bgcolor="#ffffff" class="t10">矢野立美</TD>
      <TD bgcolor="#ffffff" class="t10">矢野立美</TD>
      <TD bgcolor="#ffffff" class="t10">矢野立美</TD>
      <TD bgcolor="#ffffff" class="t10">矢野立美</TD>
      <TD bgcolor="#ffffff" class="t10">西田マサラ<BR>
      矢野立美</TD>
      <TD bgcolor="#ffffff" class="t10">矢野立美</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">編集</TD>
      <TD bgcolor="#ffffff" class="t10">鶴渕映画</TD>
      <TD bgcolor="#ffffff" class="t10">鶴渕友彰</TD>
      <TD bgcolor="#ffffff" class="t10">鶴渕友彰</TD>
      <TD bgcolor="#ffffff" class="t10">鶴渕友彰（鶴渕映画）</TD>
      <TD bgcolor="#ffffff" class="t10">鶴渕友彰</TD>
      <TD bgcolor="#ffffff" class="t10">鶴渕友彰</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Title Listwork</TD>
      <TD bgcolor="#ffffff" class="t10">マキ・プロ</TD>
      <TD bgcolor="#ffffff" class="t10">マキ・プロ</TD>
      <TD bgcolor="#ffffff" class="t10">マキ・プロ</TD>
      <TD bgcolor="#ffffff" class="t10">マキ・プロ</TD>
      <TD bgcolor="#ffffff" class="t10">マキ・プロ</TD>
      <TD bgcolor="#ffffff" class="t10">マキ・プロ</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">効果</TD>
      <TD bgcolor="#ffffff" class="t10">松田昭彦 (Fizz Sound Creation)</TD>
      <TD bgcolor="#ffffff" class="t10">松田昭彦 (Fizz Sound Creation)</TD>
      <TD bgcolor="#ffffff" class="t10">松田昭彦 (Fizz Sound Creation)</TD>
      <TD bgcolor="#ffffff" class="t10">松田昭彦 (Fizz Sound Creation)</TD>
      <TD bgcolor="#ffffff" class="t10">松田昭彦 (Fizz Sound Creation)</TD>
      <TD bgcolor="#ffffff" class="t10">松田昭彦 (Fizz Sound Creation)</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">特殊効果</TD>
      <TD bgcolor="#ffffff" class="t10">千場豊(Marix)</TD>
      <TD bgcolor="#ffffff" class="t10">千場豊(Marix)</TD>
      <TD bgcolor="#ffffff" class="t10">千場豊(Marix)</TD>
      <TD bgcolor="#ffffff" class="t10">千場豊(Marix)</TD>
      <TD bgcolor="#ffffff" class="t10">長谷川敏生(Marix)</TD>
      <TD bgcolor="#ffffff" class="t10">長谷川敏生(Marix)<BR>
      千場豊(Marix)</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">整音</TD>
      <TD bgcolor="#ffffff" class="t10">大城久典<BR>
      程原浩美</TD>
      <TD bgcolor="#ffffff" class="t10">大城久典</TD>
      <TD bgcolor="#ffffff" class="t10">大城久典</TD>
      <TD bgcolor="#ffffff" class="t10">内山敬章<BR>
      山本寿</TD>
      <TD bgcolor="#ffffff" class="t10">田中章喜<BR>
      内山敬章<BR>
      山本寿</TD>
      <TD bgcolor="#ffffff" class="t10">无?</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">音響制作</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">Audio Planning U</TD>
      <TD bgcolor="#ffffff" class="t10">Audio Planning U</TD>
      <TD bgcolor="#ffffff" class="t10">Audio Planning U</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">録音制作</TD>
      <TD bgcolor="#ffffff" class="t10">Audio Planning U</TD>
      <TD bgcolor="#ffffff" class="t10">Audio Planning U</TD>
      <TD bgcolor="#ffffff" class="t10">Audio Planning U</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Desk</TD>
      <TD bgcolor="#ffffff" class="t10">石井睦子</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">录音Studio</TD>
      <TD bgcolor="#ffffff" class="t10">APUStudio</TD>
      <TD bgcolor="#ffffff" class="t10">APUStudio</TD>
      <TD bgcolor="#ffffff" class="t10">APUStudio</TD>
      <TD bgcolor="#ffffff" class="t10">APUStudio</TD>
      <TD bgcolor="#ffffff" class="t10">APUStudio</TD>
      <TD bgcolor="#ffffff" class="t10">APUStudio</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">显影</TD>
      <TD bgcolor="#ffffff" class="t10">東京現像所</TD>
      <TD bgcolor="#ffffff" class="t10">東京現像所</TD>
      <TD bgcolor="#ffffff" class="t10">東京現像所</TD>
      <TD bgcolor="#ffffff" class="t10">東京現像所</TD>
      <TD bgcolor="#ffffff" class="t10">東京現像所</TD>
      <TD bgcolor="#ffffff" class="t10">東京現像所</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Desiging協力</TD>
      <TD bgcolor="#ffffff" class="t10">山内則康</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Special Designer</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">佐藤千春</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Visual Director</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">佐藤敬一</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">Gun Action Composer</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">小峰隆生</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">文学</TD>
      <TD bgcolor="#ffffff" class="t10">外池省二</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">設定制作</TD>
      <TD bgcolor="#ffffff" class="t10">秋山浩之</TD>
      <TD bgcolor="#ffffff" class="t10">稲荷昭彦</TD>
      <TD bgcolor="#ffffff" class="t10">稲荷昭彦</TD>
      <TD bgcolor="#ffffff" class="t10">稲荷昭彦</TD>
      <TD bgcolor="#ffffff" class="t10">相田和彦</TD>
      <TD bgcolor="#ffffff" class="t10">相田和彦</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">制作助手</TD>
      <TD bgcolor="#ffffff" class="t10">外池葉子<BR>
      佐藤あさみ</TD>
      <TD bgcolor="#ffffff" class="t10">外池葉子</TD>
      <TD bgcolor="#ffffff" class="t10">外池葉子</TD>
      <TD bgcolor="#ffffff" class="t10">小山恭子</TD>
      <TD bgcolor="#ffffff" class="t10">小山恭子</TD>
      <TD bgcolor="#ffffff" class="t10">香西千草</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">制作協力</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">豊住政弘（迪恩工作室）<BR>
      高野俊幸（迪恩工作室）</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">製作担当</TD>
      <TD bgcolor="#ffffff" class="t10">望月真人</TD>
      <TD bgcolor="#ffffff" class="t10">望月真人</TD>
      <TD bgcolor="#ffffff" class="t10">望月真人</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">製作Desk</TD>
      <TD bgcolor="#ffffff" class="t10">南雅彦</TD>
      <TD bgcolor="#ffffff" class="t10">池部茂</TD>
      <TD bgcolor="#ffffff" class="t10">池部茂</TD>
      <TD bgcolor="#ffffff" class="t10">大橋千恵雄</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">田村一彦</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">制作進行</TD>
      <TD bgcolor="#ffffff" class="t10">池部茂<BR>
      間野隆明</TD>
      <TD bgcolor="#ffffff" class="t10">武井良幸<BR>
      井口雅晴</TD>
      <TD bgcolor="#ffffff" class="t10">近藤康彦</TD>
      <TD bgcolor="#ffffff" class="t10">小川比呂美<BR>
      荒井亮</TD>
      <TD bgcolor="#ffffff" class="t10">峰岸功<BR>
      松元晃<BR>
      木村暢</TD>
      <TD bgcolor="#ffffff" class="t10">井上泰浩<BR>
      村上健一<BR>
      五十嵐達也</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">音楽Producer</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">佐々木史朗(Victor Entertainment）<BR>
      野崎圭一(Victor SPEEDSTAR RECORDS）</TD>
      <TD bgcolor="#ffffff" class="t10">佐々木史朗(Victor Entertainment）<BR>
      野崎圭一(Victor Entertainment）</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">音楽制作</TD>
      <TD bgcolor="#ffffff" class="t10">佐々木史朗（Victor 音楽産業）</TD>
      <TD bgcolor="#ffffff" class="t10">佐々木史朗（Victor 音楽産業）</TD>
      <TD bgcolor="#ffffff" class="t10">佐々木史朗（Victor 音楽産業）</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">音楽制作協力</TD>
      <TD bgcolor="#ffffff" class="t10">イマジン</TD>
      <TD bgcolor="#ffffff" class="t10">イマジン</TD>
      <TD bgcolor="#ffffff" class="t10">イマジン</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">千石一成（日本电视网公司音楽）<BR>
      組橋彰（日本电视网公司音楽）</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">宣传</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">-</TD>
      <TD bgcolor="#ffffff" class="t10">隅田壮一（读卖电视台）<BR>
      安生泰子（日本电视网公司）<BR>
      富田民幸（Sunrise）</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">製作</TD>
      <TD bgcolor="#ffffff" class="t10">森本博政（读卖电视台）<BR>
      長谷川誠（日本Victor）<BR>
      山浦栄二（Sunrise）</TD>
      <TD bgcolor="#ffffff" class="t10">森本博政（读卖电视台）<BR>
      長谷川誠（日本Victor）<BR>
      山浦栄二（Sunrise）</TD>
      <TD bgcolor="#ffffff" class="t10">森本博政（读卖电视台）<BR>
      長谷川誠（日本Victor）<BR>
      山浦栄二（Sunrise）</TD>
      <TD bgcolor="#ffffff" class="t10">長谷川国夫<BR>
      吉井孝幸</TD>
      <TD bgcolor="#ffffff" class="t10">丸山和男<BR>
      吉井孝幸</TD>
      <TD bgcolor="#ffffff" class="t10">位寄雅雄<BR>
      伊藤和明<BR>
      植田益朗</TD>
    </TR>
    <TR>
      <TD bgcolor="#F0F8FF" class="tit10">制作</TD>
      <TD bgcolor="#ffffff" class="t10">Sunrise</TD>
      <TD bgcolor="#ffffff" class="t10">Sunrise</TD>
      <TD bgcolor="#ffffff" class="t10">Sunrise</TD>
      <TD bgcolor="#ffffff" class="t10">读卖电视台<BR>
      Sunrise</TD>
      <TD bgcolor="#ffffff" class="t10">读卖电视台<BR>
      Sunrise</TD>
      <TD bgcolor="#ffffff" class="t10">读卖电视台<BR>
      日本电视网公司<BR>
      Sunrise</TD>
    </TR>
          <TR>
            <TD bgcolor="#F0F8FF" class="tit10" rowspan="5">Main Cast</TD>
            <TD bgcolor="#ffffff" colspan="6" class="t10" align="center">冴羽撩：神谷明</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" colspan="6" class="t10" align="center">槇村香：伊倉一恵</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" colspan="6" class="t10" align="center">海坊主：玄田哲章</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" colspan="6" class="t10" align="center">野上冴子：麻上洋子</TD>
          </TR>
          <TR>
            <TD bgcolor="#ffffff" class="t10" align="center">美樹：小山茉美</TD>
            <TD align="center" bgcolor="#ffffff" colspan="2" class="t10">美樹：伊藤美紀</TD>
            <TD bgcolor="#ffffff" colspan="3" class="t10" align="center">美樹：小山茉美</TD>
          </TR>
          <TR>
            <TD bgcolor="#F0F8FF" class="tit10">Guest Character Cast<FONT color="#FFFFFF">-</FONT></TD>
            <TD bgcolor="#ffffff" class="t10" valign="top">Nina：岡本茉利<BR>
            Helsen：木原正二郎<BR>
            Honda：加藤正之<BR>
            Eugen：仲木隆司<BR>
            Gunter：矢尾一樹<BR>
            Leibniz：池田勝<BR>
            Kirchman：加賀谷純一<BR>
            Klaus：永井一郎<BR>
            Steiner：家弓家正</TD>
            <TD bgcolor="#ffffff" class="t10" valign="top">Luna：小林優子<BR>
            Gilliam：阪脩<BR>
            Claude：岸野一彦<BR>
            Gomez：渡部猛<BR>
            Norton：原田一夫</TD>
            <TD bgcolor="#ffffff" class="t10" valign="top">Emily：佐々木優子<BR>
            Douglas：石丸博也<BR>
            Dick：屋良有作<BR>
            Daniel：竹村拓</TD>
            <TD bgcolor="#ffffff" class="t10" valign="top">安奈：天野由梨<BR>
            McGuire：大塚明夫<BR>
            Rosa：戸田惠子<BR>
            Gonzalez：麦人<BR>
            Dunkirk：中田和宏<BR>
            日影：市川治<BR>
            SP班長：KONTA<BR>
            警視総監：有本欽隆<BR>
            源さん：田中昴</TD>
            <TD bgcolor="#ffffff" class="t10" valign="top">武藤武明：山寺宏一<BR>
            真風笑美：岩男潤子<BR>
            野上警視総監：有本欽隆<BR>
            源さん：田口昴<BR>
            山内：飛田展男<BR>
            藤岡：西凛太朗<BR>
            Erica：安部譲二（特別出演）</TD>
            <TD bgcolor="#ffffff" class="t10" valign="top">Sayuri Claudia/朝霧朝香：高山南<BR>
            Jack Douglas：高田裕司<BR>
            野上警視総監：有本欽隆<BR>
            Mad Dog：森川智之</TD>
          </TR>
          <TR>
            <TD bgcolor="#F0F8FF" class="tit10"><FONT color="#F0F8FF">-</FONT></TD>
            <TD bgcolor="#F0F8FF" class="tit10">愛与宿命的连发枪</TD>
            <TD bgcolor="#F0F8FF" class="tit10">Bay City Wars</TD>
            <TD bgcolor="#F0F8FF" class="tit10">百万美元阴谋</TD>
            <TD bgcolor="#F0F8FF" class="tit10">The Secret Service</TD>
            <TD bgcolor="#F0F8FF" class="tit10">Goodbye My Sweetheart</TD>
            <TD bgcolor="#F0F8FF" class="tit10">紧急直播 !?<BR>
            凶恶罪犯冴羽獠的死</TD>
          </TR>
        </TBODY>
</TABLE>
</TD>
          </TR>
        </TBODY>
      </TABLE>
</CENTER>

(参考：工作人员名册和视频包)  

[BACK](./chanimestaff.md)  
[NEXT](./chanime1subtitles.md)  