INDEX > CITY HUNTER >

# Another CITY HUNTER

关于CITY HUNTER的其他作品，包括小说和电影。  

## [小説](./chnovel.md)  
Jump小说等，其中有原作者的参与。  

## [映画](./chmovie.md)  
关于成龙主演的真人电影。  

## [Game](./chgame.md)  
CH也曾在游戏中出现过…  

## [法语版CITY HUNTER](./chfrench.md)  
在法国销售的法国版CH。  

[BACK](./chanimegoodbad.md)  
[NEXT](./chnovel.md)  