INDEX > CITY HUNTER >

# Animation

本节总结了每部动画片，包括TV剧和视频。  

## [动画作品解说](./chanimekaisetsu.md)

对每部动画片的个人解说和感想。  

## 关于Staff
>● 	Main Staff  
>  [TV剧](./chanimestaff.md) 
>| [劇場版・OAV・TVSpecial](./chanimespecial.md)  
>●	TV连续剧每一集的标题和Staff一览  
>	[CITY HUNTER](./chanime1subtitles.md) 
>| [CITY HUNTER 2](./chanime2subtitles.md) 
>| [CITY HUNTER 3](./chanime3_91subtitles.md) 
>| [CITY HUNTER '91](./chanime3_91subtitles.md#91)  
>	每集动画片的标题和工作人员一览，电视剧每集的标题和工作人员一览。  
>●	[动画导演的故事](./chanimesakuga.md)  
>	关于电视剧的动画导演，我的自己的意见。  

## 关于主題歌&BGM
>●	[主題歌一覧](./chanimetheme.md)  
>●	[Opening、Ending和BGM的故事](./chanimethemeimpress.md)  
>每部动画片的主題歌的列表和感想。  

## [与原作的不同之处](./chanimediffer.md)  

动漫与原著的区别和解读。  

## [动画的好与坏的方面](./chanimegoodbad.md)  

你是一个动画党?还是原作党?  

[专栏「读卖电视动画30年史！」](../column/col_yomiuri.md)

[BACK](./chwords-other.md)  
[NEXT](./chanimekaisetsu.md)  
