INDEX > CITY HUNTER >

# 角色&术语集

## 角色集
[ [あ](./chpeople-a.md) 
| [か](./chpeople-ka.md) 
| [さ](./chpeople-sa.md) 
| [た](./chpeople-ta.md) 
| [な](./chpeople-na.md) 
| [は](./chpeople-ha.md) 
| [ま](./chpeople-ma.md) 
| [や](./chpeople-ya_ra.md) 
| [ら](./chpeople-ya_ra.md#ra) ]  
（译注： 分别包含角色有：  

- あ： 赤川会長, 浅香亜美, 浅香麻美, 麻上亜紀子, 浅野麗香, 麻生霞, 麻生弥生, 阿斯巴王子, 亜月菜摘, 天野翔子 , 阿露玛公主, 伊集院隼人, 稲垣, 岩井善美, 岩崎惠, 岩瀬明, 岩館, 上杉貴子, 海坊主, 浦上真由子, 浦上, 雲竜会的会長, 雲竜会的会長的儿子（政弘）, Elan Dayan, 艾力, 大利根, 及川優希（優希・格蕾斯）, 男老板, 小倉经理,  
- か： 海原神, 片岡優子, 片岡優子的母亲, 鹿島悦子, 柏木圭子（圭一）, 加納典宏, 神村愛子, 神村幸一郎, 川田温子, 北尾裕貴, 喜多川英二, 喜多川直也, 北原絵梨子, 教授, 銀狐, 日下美佐子, 串田, 黒川, 黒蜥蜴, 军士, Kenny Field, 蝙蝠, 伍藤梓, 伍藤広喜, 小林美幸,  
- さ： 冴羽獠, 佐藤由美子, Salina, Sanchos, General(将军), Shlomo Dayan, 柴田洋一, 神宮寺遙, 神宮寺道彦, 人口贩运集团的老板, 鈴木, Snake, 芹沢綾子, Sonia Field,  
- た： 崇司, 高塚秀司, 高宮, 泷川护士, 武田季实子, 武田老人, 立木小白合, 橘葉月, 田宮教授, 長老, 次原舞子, 土屋, 手塚明美, 手塚篤, David Clive, 冬野葉子, 毒针鼠, 友村刑事, 寅吉,  
- な：野上警視総監, 野上冴子, 野上唯香, 野上麗香, 名取和江, 西九条紗羅, 西九条定光,  
- は： 萩尾直行, 萩尾道子, 畠岡, Harano Kreuz, Baron（男爵）, 氷室剛司, 氷室真希, 日向敏夫, 平山希美子, Falcon, 深町, 伊東定雄, 伏見, Bloody Mary, 不律乱寓, 北條,  
- ま： 牧野陽子, 牧原梢, 牧原友佳, 槇村香, 槇村秀幸, 政一, 真柴憲一郎, 真柴由加里, 真柴小由里, 松岡拓也, 松岡, 松村渚, Mary, 马鲁麦斯大臣, 美樹, Mick Angel, 皆川由貴, 森居和枝, Morton总统,  
- や： 柳原, 結城礼子, Yuki Grace, 四谷政男,  
- ら： 竜, 竜神沙也加, 竜神信男, 罗丝玛丽・梦,  

）  

收集了原故事中主要人物。 它可能与故事评论有很多重复内容。    
可以被认为是常客和准常客的角色，在其名字后面标★。  
\#数字是指该角色出现的集数（准常客初登場話数）。（译注：对应[Episode List](./chsubtitles.md)里"No."列）  
（所有人物的集合，包括动画人物，可以在"[Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/)"中找到。）  

## 用語集
[ [枪](./chwords-guns.md) 
| [车](./chwords-cars.md) 
| [舞台（獠的公寓）](./chwords-apart.md) 
| [舞台（其他）](./chwords-places.md) 
| [Item](./chwords-items.md) 
| [其他](./chwords-other.md) ]  

至于「枪」，要列出整部作品中出现的所有枪支是一件大工程，所以暂时只列出主要人物最喜欢的枪支。  

（关于「枪」「车」的详细解释，可以在"[Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/)"上找到。）  

[BACK](./chyomikiri.md)  
[NEXT](./chpeople-a.md) 