INDEX > CITY HUNTER > キャラクター&用語集 >

# 用語集　車

## ミニクーパー（Morris Mini Cooper）

獠的愛車。在作品中经常出现。  
在日本很受欢迎的一款英国汽车。  
为什么獠的车有天窗？  
【备忘】  
◆ 	次出现在#17（JC卷8第132页）。 出乎意料的迟?  
◆ 	还携带了一个用于发射器的无线电接收器。(#30)  
◆ 	车牌号：  

|            |
| ---------- |
| 歌舞伎町 69 |
| あっ 19-19 |

…有点令人吃惊的数字(^^;)…  
顺便说一句，它也可以是「歌舞伎町55」「歌舞伎町57」或「あ・19-19」。  
◆ 	我认为它对一个大块头来说有点太小了，无法驾驶(^^;)。 顺便说一句，在CAT'S EYE中，俊夫的爱车是「N Cooper」。 作者的爱好？	  
◆ 	MINI.JP（[http://www.mini.ne.jp/](http://www.mini.ne.jp/)）  
mini的官方网站…也许是。  

## CR-X

HONDA的CR-X。  
獠的第二辆车。 主要由香使用。  
【备忘】  
◆ 	首次出现在#6（JC第2卷，第56页），实际上比Mini Cooper更早。  
◆ 	#50中，香和北尾用它来追踪暗殺集団，整个车辆掉进了大海。 看来它从未…被找回(^^;)。  
◆ 	官方网站（[http://www.honda.co.jp/HOT/ModelData/crx/8cx-ka-709/](http://www.honda.co.jp/HOT/ModelData/crx/8cx-ka-709/)）  

## Land Cruiser 

海坊主的爱车辆之一，是TOYOTA的越野车--Land Cruiser的简称。  
【备忘】  
◆ 	#40，我想是Land Cruiser40（我认为），用它把獠带到和蝙蝠的决斗地点。  
◆ 	#60，海坊主和獠在追击特种部队时驾驶的汽车是Lancre 80；顺便说一下，Lancre 60和其他车型在动画中出现。 (信息提供：Saeba Ryo)  
◆ 	海坊主似乎有很多其他的车。 #48中，有香撞墙撞坏引擎盖的(^^;) FORD BRONCO，还有SUZUKI的越野车。  

## TOYOTA スープラ　（TOYOTA SUPRA）

\#35出现的冴子的专用车。TOYOTA代表的大型跑车。  
冴子的是一辆通常被称为A70 Supra 后期车型的汽车。  
【备忘】  
◆ 	动画里，冴子的爱车，保时捷911 Turbo，在小说「CITY HUNTER―明天的复仇―」中被描绘成保时捷911 Turbo Flat Nose（单行本未收录），但在原作中从未被描绘过。 似乎是这样。  

<font color=#004d99>
（关于冴子的车的信息是由Saeba Ryo和康诺提供的。）  
</font>

## 獠的摩托车

\#6中，当獠去乐生会的男爵（Baron）赌场时，他和香一起骑车。 …此后一直没有出现。  
似乎是在公寓的停车场…  

## 装甲車

\#6里，将军用来给长老们的钱被獠拿走了。  
它似乎还在公寓的停车场里。   

[BACK](./chwords-guns.md)  
[NEXT](./chwords-apart.md)  