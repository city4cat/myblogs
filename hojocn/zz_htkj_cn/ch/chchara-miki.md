INDEX > CITY HUNTER > ミニ研究 >

# 人物简介 / 美樹

## 基本信息


<table class="mt11" border="1">
  <tbody>
    <tr>
      <td class="nh8" width="90" valign="bottom" bgcolor="#F0F8FF"><font color="#003366">假名</font></td>
      <td class="nh8" valign="bottom">ミキ</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">全名</td>
      <td class="t11">美樹　（名字不明）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">生年月日</td>
      <td class="t11">?年1月15日</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">年齢</td>
      <td class="t11" valign="top">25+岁（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">性別</td>
      <td class="t11">女</td>
    </tr>
    <tr>
      <td class="tit11" bgcolor="#F0F8FF">血液型</td>
      <td class="t11">O型 （动画里的設定）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">住所</td>
      <td class="t11">？（店铺兼住所的CAT'S EYE咖啡厅）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">職業</td>
      <td class="t11">
      <p>CAT'S EYE咖啡厅的女主人／海坊主的搭档</p>
      </td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">身体的特徴</td>
      <td class="t11">气质出众。右眼下方有一颗痣。（译注：待校对）</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">家庭</td>
      <td class="t11">双亲在她小时候就去世了。</td>
    </tr>
    <tr>
      <td class="tit11" width="90" bgcolor="#F0F8FF">最喜欢的枪</td>
      <td class="t11">Colt King Cobra</td>
    </tr>
  </tbody>
</table>

## 経歴

她大约8岁时在一个处于战争中的国家成为孤儿，由作为雇佣兵来到这个国家的海坊主教她生存技能。14岁时，她加入了他的部队，成为一名游击队员。 海坊主后悔把她变成了一个雇佣兵，对她撒谎，让她不再做雇佣兵，离开他，但她没有放弃，后来在他不再做雇佣兵的时候，她也跟着他。  
1991年、和海坊主結婚。(#60)  

## 特殊技能

-    作为一名前雇佣兵，她在战斗和射击方面技能全面。  
-    催眠术高手。   

## 性格

她对海坊主如此着迷，以至于她声称「除了Falcon，我的眼睛里没有别的男人」。 她很有热情、意志坚定。  
另一方面，她也有一种少女般的可爱，如"我想和我的爱人开一家可爱的商店"、"用手工制作的婚纱和花束举行婚礼"。 她也是香的好顾问。  

## 弱点

不明

## 兴趣爱好

不明

[BACK](./chchara-umibozu.md)  
[NEXT](./chchara-makimura.md)  