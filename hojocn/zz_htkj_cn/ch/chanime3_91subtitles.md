INDEX > CITY HUNTER > アニメーション >

## CITY HUNTER 3

(译注：[城市猎人 - Wikipedia](https://zh.wikipedia.org/wiki/城市猎人)里的中文标题包含：中文标题、香港TVB標題。下表采用其中的"中文标题"。)  

<CENTER>
<TABLE cellpadding="0" width="90%">
  <TBODY>
    <TR>
      <TD bgcolor="#191970">
      <TABLE border="1" cellpadding="2" width="100%">
        <TBODY>
          <TR>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">放送日</TD>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">話数</TD>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">Subtitle（译注：每集标题）</TD>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">脚本/構成</TD>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">故事板</TD>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">演出</TD>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">动画导演</TD>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">Guest</TD>
            <TD align="center" class="tit10" bgcolor="#F0F8FF">原作</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">10/15</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">115</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">脫離風流宣言!<BR>
            XYZ將拯救世界</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">外池省二</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">儿玉兼嗣</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">藤本義孝</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">北原健雄</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">森脇美鈴：林原めぐみ</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">10/22</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">116</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">無敵的戀愛現行犯!<BR>
            美女律師追求法</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">平柳益実</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">江上潔</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">江上潔</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">神村幸子</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">浅岡明：高山みなみ</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">10/29</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">117</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">阿香也火大!<BR>
            獠與令媛的代打結婚物語</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">日暮裕一</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">儿玉兼嗣</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">山口美浩</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">本橋秀之</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">伊達加奈子：岡雅子</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">11/5</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">118</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">危險的偵探遊戲!<BR>
            持槍的千金小姐(前篇)</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">(構)港野洋介</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">港野洋介</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">藤本義孝</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">北原健雄</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">神村愛子：速水圭</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">38</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">11/12</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">119</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">危險的偵探遊戲!<BR>
            持槍的千金小姐(後篇)</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">(構)港野洋介</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">港野洋介</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">江上潔</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">谷口守泰</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">神村幸一郎：北川米彦</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">38</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">11/19</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">120</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">頑固的海怪!<BR>
            嫉妒的小貓物語</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">平野靖士</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">儿玉兼嗣</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">山口美浩</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">神村幸子</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">左門寺伊太郎：辻谷耕史</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">11/26</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">121</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">潛水之戀!<BR>
            換上泳裝後的美女</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">平柳益実</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">今西隆志</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">今西隆志</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">逢坂浩司</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">綾乃：色川京子</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">12/3</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">122</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">阿獠是什麼身份?<BR>
            喜歡驚險的女大學生</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">外池省二</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">江上潔</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">江上潔</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">高岡希一</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">鷹野瞳：渕崎ゆり子</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">12/10</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">123</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">雨後天晴的戀愛預報!!<BR>
            與美女播報員的愛之傘</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">今西隆志</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">今西隆志</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">今西隆志</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">北原健雄</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">雨宮恵：佐々木優子</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">12/17</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">124</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">聖誕節的結婚禮服…(前編)</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">平野靖士<BR>
            久保田圭司</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">儿玉兼嗣</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">藤本義孝</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">本橋秀之</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">山岡椿：高田由美</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">12/24</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">125</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">聖誕節的結婚禮服…(後編)</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">平野靖士<BR>
            久保田圭司</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">儿玉兼嗣</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">山口美浩</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">神村幸子</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">海野和彦：難波圭一</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">90/1/14</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">126</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">Goodbye City<BR>
            臨別的禮物(前編)</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">日暮裕一</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">江上潔</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">江上潔</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">高岡希一</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">Sophie Silberman：佐々木るん</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t10" bgcolor="#FFFFFF">1/21</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">127</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">Goodbye City<BR>
            臨別的禮物(後編)</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">日暮裕一</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">儿玉兼嗣</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">今西隆志</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">谷口守泰</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">Guinness：木原正二郎</TD>
            <TD align="center" class="t10" bgcolor="#FFFFFF">-</TD>
          </TR>
        </TBODY>
      </TABLE>
      </TD>
    </TR>
  </TBODY>
</TABLE>
</CENTER>


## CITY HUNTER '91  {#91}

<CENTER>
<TABLE cellpadding="0" width="90%">
  <TBODY>
    <TR>
      <TD bgcolor="#191970">
      <TABLE border="1" cellpadding="2" width="100%">
        <TBODY>
          <TR>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">放送日</TD>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">話数</TD>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">Subtitle（译注：每集标题）</TD>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">脚本/構成</TD>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">故事板</TD>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">演出</TD>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">动画导演</TD>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">Guest</TD>
            <TD align="center" bgcolor="#F0F8FF" class="tit10">原作</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">91/4/28</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">128</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">麻煩搭檔大復活!<BR>
            從天而降的美女</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">外池省二</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">神志那弘志</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">天野翔子：松井菜桜子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">40</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">5/5</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">129</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">再見阿香!<BR>
            城市獵人逮捕令</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">平野靖士</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">藤本義孝</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">藤本義孝</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">丹澤学<BR>
            高岡希一</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">桑田：池田秀一</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">50</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">5/12</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">130</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">招惹危險的美女!<BR>
            回憶在光的彼方</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">後藤信平<BR>
            外池省二</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山口美浩</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山口美浩</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山内則康</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">及川優希：高田由美</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">51</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">5/26</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">131</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">戀愛也是A級的<BR>
            美人逃跑專家駕到!</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">外池省二</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">神志那弘志</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">小林美幸：山本百合子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">52</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">6/16</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">132</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">恐怖!　新宿怪談!!<BR>
            徬徨的美女幽靈</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">後藤信平</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山口美浩</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山口美浩</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山内則康</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">浅香亜美：佐々木優子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">42</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">6/30</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">133</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">離別的鎮魂曲<BR>
            希望能再見一面</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">後藤信平<BR>
            外池省二</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">藤本義孝</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">藤本義孝</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">丹澤学</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">浅香麻美：渕崎ゆり子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">42</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">7/21</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">134</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">伊集院隼人<BR>
            極平穩的一天</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">稲荷昭彦</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">神志那弘志</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">予備校生：山口勝平<BR>
            強盗：塩沢兼人</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">49</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">7/28</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">135</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">復仇的美女!<BR>
            為獠演奏哀傷的藍調</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">平野靖士</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山口美浩<BR>
            江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山口美浩</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">丹澤学</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">桑妮亚・菲特：松本梨香</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">47</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">8/4</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">136</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">硝煙的去處…<BR>
            城市獵人黎明身亡!</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">平野靖士</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">藤本義孝</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">藤本義孝</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">北原健雄</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">肯尼・菲特：柴田秀勝</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">47</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">9/22</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">137</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">唯有在今晚的愛…<BR>
            城市灰姑娘物語</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">外池省二</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">神志那弘志</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">北原絵梨子：篠原恵美</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">46</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">10/10</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">138</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">傷痕累累的槍手!<BR>
            冴子愛過的刑警</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">後藤信平<BR>
            外池省二</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">西森明良</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">藤本義孝</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">神志那弘志</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">朝倉：中村秀利</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">-</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">10/10</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">139</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">回憶中的首飾案!<BR>
            獠、壞女人和槙村</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">外池省二</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">江上潔</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">高岡希一</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">Rika：小林優子<BR>
            Merald国王：堀内賢雄</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">-</TD>
          </TR>
          <TR>
            <TD align="center" bgcolor="#FFFFFF" class="t10">10/10</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">140</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">安魂搖籃曲<BR>
            來自遠方國家的貴公子</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">遠藤明範</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山口美浩</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山口美浩</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">山内則康</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">Hans Siegfried：佐々木望</TD>
            <TD align="center" bgcolor="#FFFFFF" class="t10">-</TD>
          </TR>
        </TBODY>
      </TABLE>
      </TD>
    </TR>
  </TBODY>
</TABLE>
</CENTER>


[BACK](./chanime2subtitles.md)  
[NEXT](./chanimesakuga.md)  