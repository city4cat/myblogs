INDEX > CITY HUNTER >

# Episode Guide

## [Episode List](./chsubtitles.md)　（ [在新窗口中打开](./chsubtitles.md)）

CITY HUNTER每1～6周的连载做一个故事（1 Episode）。（译注：此句待确认）  
本节列出了subtitles（译注：每一话的标题）、客串角色和动画剧集。 它也可以作为Jump Comic和文库版所含剧集的对照表。  
在「研究序説」中，对CH剧集的引用是基于这个列表。  

●关于剧集数  
「CITY HUNTER Perfect Guide Book」（集英社）有55集，但出于解说的原因，我们自己将其定为60集。 (共同的剧集名称是根据Perfect Guide Book改编的)。  

## 剧情简介和评论

[◆#1 不光彩的心下! ～  #10 裸足女星](./chepisode1-10.md)  
[◆#11 随着钟声的命运! ～ #20 不可碰护士啊!](./chepisode11-20.md)  
[◆#21 槇村遗下的东西 ～ #30 危险的一对!](./chepisode21-30.md)  
[◆#31 大家姐是女大学生! ～ #40 天空任飞翔](./chepisode31-40.md)  
[◆#41 天使遗忘的礼物!? ～ #50冴子的相亲!!](./chepisode41-50.md)  
[◆#51 将记忆抹去… ～ #60 FOREVER, CITY HUNTER !!](./chepisode51-60.md)  

每一集的内容提要（到每集中间为止），以及个人印象和提示。   
（包括结尾在内的简介请参阅Saeba Ryo的"[Sweeper Office](http://www.netlaputa.ne.jp/~arcadia/)"。）  

## [关于短篇「City Hunter」](./chyomikiri.md)  

在该连载之前，有两个「City Hunter」的短篇故事，这是对它们的解说。  

[BACK](./chstory-charactors.md)  
[NEXT](./chsubtitles.md)  