INDEX > CITY HUNTER > アニメーション >

# Main Staff　－TV剧－

<center>
<table width="95%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
<table cellpadding="2" border="1">
  <tbody>
    <tr>
      <td class="tit10" width="12%" bgcolor="#F0F8FF" align="center"><font color="#F0F8FF">-</font></td>
      <td class="tit10" width="22%" bgcolor="#F0F8FF" align="center">CITY HUNTER</td>
      <td class="tit10" width="22%" bgcolor="#F0F8FF" align="center">CITY HUNTER 2</td>
      <td class="tit10" width="22%" bgcolor="#F0F8FF" align="center">CITY HUNTER 3</td>
      <td class="tit10" width="22%" bgcolor="#F0F8FF" align="center">CITY HUNTER '91</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">話数</td>
      <td class="t10" bgcolor="#ffffff">第1話～第51話</td>
      <td class="t10" bgcolor="#ffffff">第52話～第114話</td>
      <td class="t10" bgcolor="#ffffff">第115話～第127話</td>
      <td class="t10" bgcolor="#ffffff">第128話～第140話</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">放送日<sup>[^1]</sup></td>
      <td class="t10" bgcolor="#ffffff">1987年4月6日～<br>
            1988年3月28日</td>
      <td class="t10" bgcolor="#ffffff">1988年4月2日～<br>
            1989年7月1日</td>
      <td class="t10" bgcolor="#ffffff">1989年10月15日～<br>
            1990年１月21日</td>
      <td class="t10" bgcolor="#ffffff">1991年4月28日～<br>1991年9月22日<sup>[^2]</sup></td>
    </tr>
          <tr>
      <td class="tit10" bgcolor="#F0F8FF">企画</td>
      <td class="t10" bgcolor="#ffffff">諏訪道彦 (读卖电视台)</td>
      <td class="t10" bgcolor="#ffffff">諏訪道彦 (读卖电视台)</td>
      <td class="t10" bgcolor="#ffffff">諏訪道彦 (读卖电视台)</td>
      <td class="t10" bgcolor="#ffffff">諏訪道彦 (读卖电视台)</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">原作</td>
      <td class="t10" bgcolor="#ffffff">北条司</td>
      <td class="t10" bgcolor="#ffffff">北条司</td>
      <td class="t10" bgcolor="#ffffff">北条司</td>
      <td class="t10" bgcolor="#ffffff">北条司</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">制片人</td>
      <td class="t10" bgcolor="#ffffff">植田益朗 (Sunrise)<br>
            諏訪道彦 (读卖电视台)</td>
      <td class="t10" bgcolor="#ffffff">植田益朗 (Sunrise)<br>
            諏訪道彦 (读卖电视台)</td>
      <td class="t10" bgcolor="#ffffff">植田益朗 (Sunrise)<br>
            諏訪道彦 (读卖电视台)</td>
      <td class="t10" bgcolor="#ffffff">植田益朗 (Sunrise)<br>
            望月真人 (Sunrise)<br>
            諏訪道彦 (读卖电视台)</td>
    </tr>
          <tr>
      <td class="tit10" bgcolor="#F0F8FF">監督</td>
      <td class="t10" bgcolor="#ffffff">儿玉兼嗣</td>
      <td class="t10" bgcolor="#ffffff">儿玉兼嗣</td>
      <td class="t10" bgcolor="#ffffff">儿玉兼嗣</td>
      <td class="t10" bgcolor="#ffffff">江上潔</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">Character Design</td>
      <td class="t10" bgcolor="#ffffff">神村幸子</td>
      <td class="t10" bgcolor="#ffffff">神村幸子</td>
      <td class="t10" bgcolor="#ffffff">神村幸子</td>
      <td class="t10" bgcolor="#ffffff">神志那弘志</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">首席动画导演</td>
      <td class="t10" bgcolor="#ffffff">北原健雄</td>
      <td class="t10" bgcolor="#ffffff">北原健雄</td>
      <td class="t10" bgcolor="#ffffff">北原健雄</td>
      <td class="t10" bgcolor="#ffffff"><font color="#FFFFFF">-</font></td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">美術監督</td>
      <td class="t10" bgcolor="#ffffff">宮前光春<br>
            東 潤一</td>
      <td class="t10" bgcolor="#ffffff">宮前光春<br>
            東 潤一</td>
      <td class="t10" bgcolor="#ffffff">宮前光春<br>
            東 潤一</td>
      <td class="t10" bgcolor="#ffffff">東 潤一<br>
            田原優子</td>
    </tr>
          <tr>
      <td class="tit10" bgcolor="#F0F8FF">撮影監督</td>
      <td class="t10" bgcolor="#ffffff">古林一太</td>
      <td class="t10" bgcolor="#ffffff">古林一太</td>
      <td class="t10" bgcolor="#ffffff">古林一太<br>
            伊藤修一</td>
      <td class="t10" bgcolor="#ffffff">長谷川洋一</td>
    </tr>
          <tr>
      <td class="tit10" bgcolor="#F0F8FF">音響監督</td>
      <td class="t10" bgcolor="#ffffff">浦上靖夫</td>
      <td class="t10" bgcolor="#ffffff">浦上靖夫</td>
      <td class="t10" bgcolor="#ffffff">浦上靖夫<br>
            小林克良</td>
      <td class="t10" bgcolor="#ffffff">浦上靖夫<br>
            小林克良</td>
    </tr>
          <tr>
      <td class="tit10" bgcolor="#F0F8FF">音楽</td>
      <td class="t10" bgcolor="#ffffff">国吉良一<br>
            矢野立美</td>
      <td class="t10" bgcolor="#ffffff">矢野立美</td>
      <td class="t10" bgcolor="#ffffff">矢野立美</td>
      <td class="t10" bgcolor="#ffffff">矢野立美</td>
    </tr>
          <tr>
      <td class="tit10" bgcolor="#F0F8FF">編集</td>
      <td class="t10" bgcolor="#ffffff">鶴渕映画</td>
      <td class="t10" bgcolor="#ffffff">鶴渕映画</td>
      <td class="t10" bgcolor="#ffffff">鶴渕映画</td>
      <td class="t10" bgcolor="#ffffff">鶴渕友彰<br>
            片石文栄</td>
    </tr>
          <tr>
      <td class="tit10" bgcolor="#F0F8FF">Title</td>
      <td class="t10" bgcolor="#ffffff">マキ・プロ</td>
      <td class="t10" bgcolor="#ffffff">マキ・プロ</td>
      <td class="t10" bgcolor="#ffffff">マキ・プロ</td>
      <td class="t10" bgcolor="#ffffff">マキ・プロ</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">効果</td>
      <td class="t10" bgcolor="#ffffff">松田昭彦 (フィズサウンド)</td>
      <td class="t10" bgcolor="#ffffff">松田昭彦 (フィズサウンド)</td>
      <td class="t10" bgcolor="#ffffff">松田昭彦 (フィズサウンド)</td>
      <td class="t10" bgcolor="#ffffff">松田昭彦 (フィズサウンド)</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">整音</td>
      <td class="t10" bgcolor="#ffffff">大城久典</td>
      <td class="t10" bgcolor="#ffffff">大城久典</td>
      <td class="t10" bgcolor="#ffffff">柴田信弘</td>
      <td class="t10" bgcolor="#ffffff">柴田信弘</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">音響制作</td>
      <td class="t10" bgcolor="#ffffff">Audio Planning U</td>
      <td class="t10" bgcolor="#ffffff">Audio Planning U</td>
      <td class="t10" bgcolor="#ffffff">Audio Planning U</td>
      <td class="t10" bgcolor="#ffffff">Audio Planning U</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">录音Studio</td>
      <td class="t10" bgcolor="#ffffff">APUStudio</td>
      <td class="t10" bgcolor="#ffffff">APUStudio</td>
      <td class="t10" bgcolor="#ffffff">APUStudio</td>
      <td class="t10" bgcolor="#ffffff">APUStudio</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">显影</td>
      <td class="t10" bgcolor="#ffffff">東京現像所</td>
      <td class="t10" bgcolor="#ffffff">東京現像所</td>
      <td class="t10" bgcolor="#ffffff">東京現像所</td>
      <td class="t10" bgcolor="#ffffff">東京現像所</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">机械设计</td>
      <td class="t10" bgcolor="#ffffff">明貴美加<sup>[^3]</sup></td>
      <td class="t10" bgcolor="#ffffff">明貴美加</td>
      <td class="t10" bgcolor="#ffffff">小原渉平</td>
      <td class="t10" bgcolor="#ffffff">山内則康</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">設定</td>
      <td class="t10" bgcolor="#ffffff">山本之文（～26話）<br>
            秋山浩之（27話～）</td>
      <td class="t10" bgcolor="#ffffff">秋山浩之</td>
      <td class="t10" bgcolor="#ffffff">-</td>
      <td class="t10" bgcolor="#ffffff">-</td>
    </tr>
          <tr>
      <td class="tit10" bgcolor="#F0F8FF">文学</td>
      <td class="t10" bgcolor="#ffffff">外池省二</td>
      <td class="t10" bgcolor="#ffffff">外池省二</td>
      <td class="t10" bgcolor="#ffffff">外池省二</td>
      <td class="t10" bgcolor="#ffffff">-</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">文学設定</td>
      <td class="t10" bgcolor="#ffffff">-</td>
      <td class="t10" bgcolor="#ffffff">-</td>
      <td class="t10" bgcolor="#ffffff">稲荷昭彦</td>
      <td class="t10" bgcolor="#ffffff">稲荷昭彦</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">製作担当</td>
      <td class="t10" bgcolor="#ffffff">望月真人</td>
      <td class="t10" bgcolor="#ffffff">望月真人</td>
      <td class="t10" bgcolor="#ffffff">望月真人</td>
      <td class="t10" bgcolor="#ffffff">-</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">製作Desk</td>
      <td class="t10" bgcolor="#ffffff">-</td>
      <td class="t10" bgcolor="#ffffff">南雅彦</td>
      <td class="t10" bgcolor="#ffffff">池部茂</td>
      <td class="t10" bgcolor="#ffffff">池部茂</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">制作進行</td>
      <td class="t10" bgcolor="#ffffff">-</td>
      <td class="t10" bgcolor="#ffffff">-</td>
      <td class="t10" bgcolor="#ffffff">-</td>
      <td class="t10" bgcolor="#ffffff">秋山浩之</td>
    </tr>
    <tr>
      <td class="tit10" bgcolor="#F0F8FF">制作</td>
      <td class="t10" bgcolor="#ffffff">读卖电视台<br>
            Sunrise</td>
      <td class="t10" bgcolor="#ffffff">读卖电视台<br>
            Sunrise</td>
      <td class="t10" bgcolor="#ffffff">读卖电视台<br>
            Sunrise</td>
      <td class="t10" bgcolor="#ffffff">读卖电视台<br>
            Sunrise</td>
    </tr>
        </tbody>
</table>
</td>
    </tr>
  </tbody>
</table>
</center>

[^1]:  
 	CH1～3：播出日期和时间以主要电视台--大阪读卖电视台为准  
	CH '91：播出日期和时间以东京为准  
[^2]: 	最后3集于10月10日一起播出  
[^3]: 	在一些资料中，这是从第27集开始的，但实际上他从一开始就参加了。 他画的Mini Cooper、Python和其他设置后来被采用。  

## Main Cast
冴羽撩 		神谷明  
槇村香 		伊倉一恵  
海坊主 		玄田哲章  
野上冴子 		麻上洋子  
美樹 		小山芙美  
野上麗香 		鷹森淑乃  
槇村秀幸 		田中秀幸  

(参考：CD Dramatic Master II Booklet/City Hunter Anime Special)  

[BACK](./chanimekaisetsu.md)  
[NEXT](./chanimespecial.md)  
