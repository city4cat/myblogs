INDEX > CITY HUNTER >

# Mini研究

CITY HUNTER相关的更多思考（又名吐糟?）的角落。    

## 人物简介  

关于主要人物。    
基于原作的最终设定，因此是完全剧透。    
◆[冴羽 獠](./chchara-ryo.md)  
◆[槇村 香](./chchara-kaori.md)  
◆[野上 冴子](./chchara-saeko.md)  
◆[海坊主](./chchara-umibozu.md)  
◆[美樹](./chchara-miki.md)  
◆[槇村 秀幸](./chchara-makimura.md)  

## [年表](./chnempyo.md)  

基于人物传记的CH世界年表。  

## CH背后的世界

CH世界的另一面(?)的探讨（译注：待校对）  
◆[名字的秘密](./chname.md)  
◆[獠的原型？](./chmodel.md)  
◆[獠的過去](./chpast.md)  
◆[City Hunter站在正义的一边?](./chjustice.md)  
◆[为什么香有「女人缘」？](./chkaori.md)  
◆[在槇村名字之下](./chmakimura.md)（译注：待校对）  
◆[槇村、冴子、獠](./chm_s_r.md)  
◆[如何称呼](./chnamecall.md)  

## [冴羽家的餐桌](./chsyokutaku.md)

关于在CH中出现的餐饮场景的列表。  

[BACK](./chfrench.md)  
[NEXT](./chchara-ryo.md)  