INDEX > CITY HUNTER > キャラクター&用語集 >

# 角色集　あ行

## 赤川会長 (#11)

赤川集团的会長。 他本想从竞争对手片冈集团中除掉继任者優子，将片冈集团置于赤川集团之下，但在獠的狙击的无声威胁下，他被迫退出了片冈集团。    
顺便说一句，动画中也设定了名字「赤川つぎ次郎」（汉字不明）…(笑)    

## 浅香亜美 (#42)

浅香麻美的双胞胎姐姐。 她目睹了一起街头谋杀案，并被凶手杀害。 为了保护麻美不受街头杀手的伤害，她在死后变成了鬼魂，并借用了麻美的身体，要求獠做她的保镖。 她的梦想是让妹妹成为一名钢琴家，所以她自己努力工作，供她上大学。 与麻美相比，她的性格随和、开朗。  

## 浅香麻美 (#42)

浅香亜美的双胞胎妹妹，一个有抱负的钢琴家学生。 她好不容易才从姐姐的死亡中恢复过来，但当她意识到亜美通过獠守护着她时，她逐渐恢复了体力，并能弹出她原来优美的音色。 与亜美相比，她的性格比较安静。  

## 麻上亜紀子 (#33)

一个年轻、漂亮的超级保姆，曾在英国的保姆学校学习，从学术研究到自卫、射击和家务都掌握得很好。 她为西九条家族的第18任族长西九条紗羅工作，紗羅非常信任她。 为了逃离追捕她的亲戚的魔掌，她委托獠保护她。 在她的脑海中，对紗羅深深的爱着已故的父亲重信。  

## 浅野麗香 (#10)

在电影「白昼的谋杀」中扮演多情女子的女演员。 据传与由美子的伙伴加納同居。 她嫉妒由美子。  

## 麻生霞 (#16, 34, 其他) ★

一个来自麻生家族的女孩，其祖先世世代代都是契约盗贼。 她被公众称为怪盗305号。獠称之为「空中飞臀」。 当她第一次出现时（#16），她是一名高中生，要求獠帮助她偷黑色的郁金香。 后来，当她成为一名大学生时，被家规强迫与一个不受欢迎的伴侣结婚，并请求獠帮助她结束婚约。 然而，在婚约被取消后，她真的爱上了獠，并在CAT'S EYE咖啡馆做了一份住家的兼职工作，以便「偷走獠的心」。 (#34)  
1969年1月15日出生。  

<font color=#004d99>
…但她可能无法永远偷下去…^^;
</font>

## 麻生弥生 (#34)

麻生家族--一个契约盗贼家族--的长老，也是小霞的祖母。 她有一段经历，年轻时曾与一个家族外的男人相爱，但因为家规而被拆散。  

## 阿斯巴王子 (#51)

阿斯巴王国的王子。 由于他是国王第二任妻子的儿子而无法夺取王位，他谋划杀死王位继承人--日本第一夫人的女儿優希格蕾斯（及川優希）。 他雇佣了一个有催眠技能的杀手，并试图通过使其看起来像一场意外来杀死她，但由于獠的努力而失败。  

## 亜月菜摘 (#2)

他要求獠逮捕杀害他妹妹夕子的凶手。 她是一个美丽的女子，但她的性格却很少被提及，从某种意义上说，她是一个不寻常的委托人。  

## 天野翔子 (#40)

属于xxx航空公司的飞行员。 她突然驾驶塞斯纳飞机坠落在獠的公寓里。 她不知道自己参与了xxx公司的毒品走私活动，并几乎被诬陷为犯罪。她是飞机的忠实粉丝，甚至住在一个美国军事基地附近。 她心智坚定。（译注：xxx部分待补全）  

## 阿露玛公主 (#27)

xxx王国王位的下一个继承人。 她在王位继承前抵达日本，但她的女伴沙莉娜给她出主意，让她秘密地和她一起溜出机场，并与沙莉娜交换身份。要求獠护送她并带他观光，这也是为了逃避追逐王位的马鲁麦斯大臣的魔爪。她高贵的气质压制了獠的勃起(笑)。（译注：xxx部分待补全）   
她直到最后也没能亲口告诉獠的真相，但她完成了她的「梦想」，带着美好的回忆返程。 
  

## 伊集院隼人

海坊主的本名。（[海坊主](./chpeople-a.md#umibozu)）  

## 稲垣 (#1)

拳击手。 他杀死了对手荻野俊一，然后试图勒索冠军以夺取他的头衔，但被荻野的恋人岩崎惠杀死。  
按照獠的说法，他「只是一个没有技术拳击手，只是幸运」。  

## 岩井善美 (#20)

佐佐木综合医院的新护士。  
至少可以说，她的护理技能很糟糕，入院保护她的人都有一个地狱般的日子。 但她自己只是急于帮助她的病人。  

## 岩崎惠 (#1)

女医生。 她委托獠为她的爱人稻城报仇。 她的费用是为她自己购买的人寿保险，她患有癌症，生命时间所剩无几。 在完成工作后，獠敦促她在有生之年做獠的恋人。  

## 岩瀬明 (#13)

牧野洋子（鬼英会的女荷官）爱上的男人。 洋子相信他有和她一样的超能力，但这其实是他在酒吧里玩的一个把戏。  
他受到鬼英会的威胁，并试图抛弃阳子独自逃离城市，为此他受到了獠的小惩罚。  

## 岩館 (#36)

警視庁的刑警。 由于与黑帮有经济纠纷，他枪杀了一名黑帮分子。 被流弹击中下半身瘫痪的牧原梢不得不接受手术，由于担心子弹被取出而被抓住，他试图冒充色狼杀死她。（译注：此段待校对）  

## 上杉貴子 (#37)

上杉财阀的总经理。 他反对儿子的婚姻，强迫他离开，派他去做海外项目，10年都不能回家，结果导致儿子死亡。 她对自己的行为感到后悔，并决定找到他的孙女季实子，以继承他的遗产。 在求婚者的风波中与武田老人和解了。  

## 海坊主 ★  {#umibozu}

清道夫，獠的对手。（[详细信息](./chchara-umibozu.md)）  

## 浦上真由子 (#44, 47)

香某因阑尾炎入院治疗，与香同房的女孩。 她的母亲在一次车祸中去世，她自己也失明。 她意外地目睹了一起谋杀案（但只听到声音，没有看到），并成为了别人的目标。为了有母亲，她想了很多办法让父亲和香结婚。  
现在，她的视力已经痊愈，正在耐心地等待獠和香的婚礼…  
她在#47中短暂出场，用来宣布海坊主的眼睛的不寻常。  

## 浦上 (#44)

真由子的父亲。 职业是一家电视台的制片人。 性格帅气温柔，香一时为之心动。  
真由子说「他对漂亮的女人一点也不感兴趣」。 事实证明，他喜欢的不是香，而是泷川护士（她很漂亮，但看起来像他的前妻）。  

## 雲竜会的会長 (#29, 35)

关东云龙会的会长。 他试图对付伤害他儿子政弘的人（实为獠），但被轻易击败。(#29)  
他绑架了主编小百合，应要求抢夺Weekly News杂志的一篇文章，但由于同时绑架了香，他很倒霉(?)地被獠绑架。(#35)  

## 雲竜会的会長的儿子（政弘） (#29)

一个暴走族的头目（他们根本不暴走？） 。 他们试图在父亲的帮助下打败獠，但却被打败了。  

<font color=#004d99>
好吧，这是个糟糕的合作伙伴～
</font>

## Elan・Dayan (#12) （译注：勒尼-达宁）

罗克西亚公国的2王子。 席洛蒙的双胞胎兄弟。 他怨恨同时出生但却夺取王位的哥哥，想带领自己的军队造反。  
他对男人过敏，如果有男人靠近他，他就会起痘。 为此，他随行的士兵都是经过严格挑选的美女，基地就像一个后宫。  

## 艾力 (#39)

模特罗斯玛丽梦的经纪人，也是她的恋人。 在与玛丽到达日本后不久，她就被对她心怀怨恨的大卫・高飞绑架为人质。 即使在发现她的真实身份后，他对她的爱也没有改变。（译注：此段待校对）  

## 大利根 (#31)

前吉祥組的干部。 在他的老板芹沢大助死后成立了西荻会，经常攻击绫子，目标是吉祥組的领地。 最后，整座大楼被獠毁了。  

## 及川優希（優希・格蕾斯） (#51)  {#yuki}

JAS（Japan Action Stunt）的受欢迎的女特技演员，四个月前才开始从事特技工作，不记得以前的特技工作。 她实际上是xxx王国王位的下一任继承人優希・格蕾斯。  
当她发现自己的真实身份时，她试图逃避自己的命运，但獠告诉她应该去哪里后，她决定做回公主。 相反，她封存了与獠相处的这几天的记忆…说想带着自己的记忆去远方。  

## 男老板 (#21)

贩卖人口组织的老板。 她看起来完全是女性，而獠很容易就被骗了。 她的战斗风格就像一个带着鞭子的SM…（コラコラ^^;）  

## 小倉经理 (#18)

负责电影「思い出の渚」中女主角试镜的经理。 委托獠保护参赛者。  

[BACK](./chpeople-words.md)  
[NEXT](./chpeople-ka.md)  