INDEX > Angel Heart > Angel Heart 随想 >

# 对人的描写缺乏力度

Angel Heart中的许多人物都不讨人喜欢，例如，他们的言行不一、缺乏必然性。 这是我不喜欢这个故事的主要原因之一。   
例如，冴羽獠言行脱节，而且不知道他到底想做什么。   
从第1话的台词来看，「既然他偷了心脏，肯定是移植给别人了。 所以他一定有信念，认为它会回来」。 他甚至说，他泡妞的原因是为了寻找胸前有手术疤痕的女人。 然而，后来，大约在第5～8话，他突然跑到镇上，说他的意思是香回来了，好像他完全忘记了他的所说所做。（译注：此段翻译需确认）
他是想表达他的精神状态因失去香而不稳定吗？ 但即便如此，它还是在某种程度上有偏差。 

我想这种情况会发生，例如，以一种随性的、临时的方式来画，而没有留意所画的整个场景、没有留意角色说的话、没有留意场景的情感起伏。  

同在Comic Bunch上连载的「山下たろーくん」中有这样一个场景。 一个曾经非常成功但消失了一段时间的漫画家又画了一幅漫画，编辑看了之后对漫画家说，他所画的漫画中的人物都没有说实话，所以他不明白角色在想什么，而且画他们的人自己似乎一点都不开心。  

当我读到这里时，我觉得Angel Heart正是如此。 作者只是用一种方便的方式说话......换句话说，人物并没说出他们的真实感受，所以对话根本没有进入读者的心。   

最有问题的是，主人公香瑩特别容易出现这种情况。   
...和獠一起生活真的是香瑩的愿望吗？   

香瑩，直到故事的中间部分，当然是把她的感情从心里分离出来，即从香的影响中分离出来，并且憎恨香对她的误导的记忆，她甚至一度试图杀死香--向自己的心脏开枪。 
正是信広的生还决定了她要活下去的意志。 她告诉他「从现在开始，你提醒我如何笑」，并试图挽救他的生命，即使她自己也受伤了。 她选择与她的同伴信弘一起生活。   

然而，后来，在与在梦中出现的香互动时，由于某种原因，香瑩突然相信她真正想要的是獠父亲。 她不再关心她非常讨厌的香，也不顾及她极力保护的信宏。    

...在此之前，她一定对她的父亲没有任何感情。 即使作者有这样的设定，只要没有描写，读者就只能认为它不存在。 如果我可以冒昧地猜测一下，我会说第1话开头的那一幕--她杀了一个人，紧接着看到他的女儿出现而感到震惊--我认为这不一定是表达她需要父亲。   
他还说「她在梦中被香瑩抱着的那种安全感，就是香瑩要找的」，但这也是不对的。 GH在昏迷期间所做的梦是香被獠抱住时的记忆（这就是她的心率异常高的原因......），而这绝不应该是对他是她父亲的渴望。  
在我看来，这只是一种诱导她成为新的"CITY HUNTER"的方式，让她成为Saeba的女儿。   
这让人非常不适。 这是因为她原来的情感状态--一路发展而来所能想象到的--被忽略了。 

后来，也许是为了补充这部分故事，插入了一集（第48集），讲述獠和她的生父之间的关系，以及她对父母也有感情，但如果原意是让獠和她的 "「親子」作为CH，这个故事应该在故事的开头讲述。    

[BACK](./ahessay03.md)  
[NEXT](./ahessay05.md)  