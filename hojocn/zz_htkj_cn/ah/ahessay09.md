INDEX > Angel Heart > Angel Heart 随想 >

# CH补充 ～连载五周年的后记～

我想知道，我们的读者中是否有人曾经读过他们之前写的漫画或小说，或在写完之后立即想到，「我希望我当时应该这样做」或「我现在好多了，我想再重写一次」。  
由于CH中的主要人物以不同的身份和设定相继出现，我的感觉是AH对北条来说正是如此，是对CH的「补充｣之作。  

正如在CH章节中提到的，相当一部分应该在CH中描写的关系没有被描写出来（见「CITY HUNTER随想」内[『獠和香的神秘关系』](../ch/chessay04.md)）。例如，獠和香的恋爱关系，槇村和香的关系，槇村和冴子的关系，以及獠在遇到香之前究竟在做什么…这些细节都没有在故事中得到深入的描述。 也许这就是为什么作者说"我想有一天为CH画一个续集，我觉得它没有完成得很好"。   
当时，会有出版商/编辑部的意愿所带来的各种限制。 但这一次，他已经成为一名受欢迎的漫画家，并在他自己成立的公司出版的杂志上连载他的作品。 认为现在是重新绘制之前没有绘制的部分的最佳机会，这不是没有道理的。  
我想表达的是，这就是我现在按照北条司的风格画画时的情况，他已经建立了自己的事业，这就是我最初想画的东西。…要做到这一点，只借用人物并将其设置在不同的环境中，比制作一部接管CH的续集要容易得多。

看了AH中对香的处理，有人可能会认为北条司不喜欢香，但情况真的如此吗？   
『City Hunter S最強読本』(宝島社)有一篇相当有趣的文章。 对导演的采访：  

<font color=#660066>
    「最难把握的角色其实是香。 这是因为她只是北条自己理想中的女人形象(笑)」  
</font>

这是一个声明。 与北条关系密切的儿玉导演的话是可信的。    
人们可以由此想象，由于她是一个 「理想的女性」，她更有可能「超越普通人」。   
今天的AH里的香更像是一个"圣母"，她拥有男人在女人身上所追求的方方面面（妻子、爱人或母亲），当男人呢陷入困境时，她也会出现帮助他们。 如果是一个有血有肉的普通女性角色，那就太完美了，有点让人反感，但一个灵性的存在是可以被原谅的。    
虽然这种观点略显偏颇，但可以说他让她死是因为他爱她，想让她成为永恒。   
香的存在的意义对于獠的性格来说是相当重要的。   
CH中，在与香相遇之前，獠首先与槇村相遇，然后香以「槇村的妹妹」的身份出现。 
对于AH，獠先遇到香，而槇村是「香的哥哥」；此外，香救了他的命，是一种教父的角色。 换句话说，AH中的香也扮演了CH中的「父亲」海原的角色。 
因此，可以说，对于AH的香来说，也许比CH的香更不可替代。 也可以感受到作者对香这个人物的强烈感情。 
他们的关系，在CH中有些半信半疑，在AH的描写中得到了「补充」。   

我不希望北条司画一个作品只是为CH清理边角料。  
AH当然是一部独立于CH的作品，当然也有读者通过AH认识了北条司，并独自享受这部作品。 但至于AH是否超越了CH，我认为当它借用人物、从强迫性的故事情节开始、重新烘托故事时，它最终制造了一堵无法超越的墙。  
现在，我们只是期待着看到故事的结局。 如果以后要对AH说些什么，那将是在该系列结束时、在故事完結时。     
并希望有一天，他能画出真正超越这两部，甚至超越他以前所有作品的作品。 

(2005/1/24)  

[BACK](./ahessay08.md)  
[HOME](../index.md)  