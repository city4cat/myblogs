INDEX > Angel Heart >

# Angel Heart 随想

正如「My First Impression」中所指出的，「香之死」的设定无疑是一种冲击。 然而，这并不是完全不可接受的。 这是因为，比如说，有粉丝模仿小说有这样的设定。   
反之，如果作者自己要做，就必须达到让你说「这就是原作者，这不是模仿」的水平，否则就没有意义。  

然而，Angel Heart似乎还没有达到这样的水平。  
我将在下面讨论一些具体的问题。   

- [好的構成](./ahessay01.md)    
- [空白的一年](./ahessay02.md)    
- [世界観的危机](./ahessay02.md#sekai)    


- [GH的存在意義](./ahessay03.md)    
- [对人的描写缺乏力度](./ahessay04.md)    
- [獠和香的egotism](./ahessay05.md)    


- [品牌化的「北条司」](./ahessay06.md)    
- [Angel Heart描绘了什么](./ahessay07.md)    
- [关键人物是谁](./ahessay08.md)    
- [CH补充 ～连载五周年的后记～](./ahessay09.md)    


[BACK](./ahface.md)  
[NEXT](./ahessay01.md)  