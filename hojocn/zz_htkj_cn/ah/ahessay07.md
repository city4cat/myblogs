INDEX > Angel Heart > Angel Heart 随想 >

# Angel Heart描绘了什么

Angel Heart可以说是北条司的第一部剧情片。
…可能看起来很奇怪。 但事实上，CAT'S EYE、CITY HUNTER、F.COMPO是从「一个小偷和侦探作为恋人」、「一个肮脏但很酷的英雄」、「一对性别颠倒的夫妻」开始的，故事通过一系列的小插曲进展。 没有什么主要的故事情节。 
相比之下，在这部Angel Heart中，设定，即结局，似乎在手法之前就已明确。  
首先，正如新潮访谈中所说，它是一个 「少女的成長故事」。  

<font color=#660066>
    对于这期『Angel Heart』，我最初的想法是描绘一个与『City Hunter』完全不同的故事。 最初，我想的是描绘一个女孩的成长故事，我在为它制定世界观，但经过反复思考，我想到如果我把『City Hunter』的世界观中也涉及到冴羽獠，那会很有趣。  
    (新潮社「波」2001年７月号对Comic Bunch的采访)  
</font>

而且，毕竟，「结合」可能是他们想要描绘的东西之一。  
这是一个在CAT'S EYE、CITY HUNTER、F.COMPO中多次描绘的主题。  
…在这些作品中，他们不会从一开始就把它作为一个主题来考虑。 然而，在Angel Heart中，这句话是作为背景写在Bunch创刊之前分发的小册子--創刊0号--的预览页上，暗示了作品的主题。  

「在城市猎人的世界里画出一个女孩的成长故事」或许可以方便地同时表达这两个主题。 
...但这真的是正确的决定吗？   

CITY HUNTER的故事本质上是娱乐。 各种事件发生在冴羽獠周围，动作和浪漫喜剧与之交织在一起。 这些人物从未「成长」，尽管他们彼此之间的关系发生了变化。 这听起来很奇怪，但人物基本上没有变化（作者在CH第26卷的封面折页上说，他希望把这个故事设定在一个人物不变老的环境中，这就是证明）。  
那么，AH这个以成长中的女孩为主角的故事，应该与CH天生就不相容。   
既然他们要这么做，就必须有充分和足够的准备。 主人公的生活故事将如何讲述，冴羽獠和其他类似CH的人物将如何交织在一起？ 这个女孩为什么选择死亡，又为什么选择重生？   
事实是，这是一个非常沉重的主题，以我们迄今为止所做的方式，即通过观察人物的动作，让事情发生，来描绘它，应该真的不可能。 
这对北条来说是一个相当大的挑战，他以前从来没有画过这样的故事。 

然而，看看已经开始的连载，很明显，作者显然没有准备，像以前一样草草了事，结果，很明显，他画得很混乱。   

打个比方，这就像一辆有缺陷的汽车，由于装配不良而无法正常运行，尽管每个零件都是精心制作的。 除此之外，司机还经常想知道该走哪条路以及如何驾驶。 导航仪只告诉你要去哪里，不看路线和司机的能力。   

正如动画制作人諏訪所说（「北条司Illustrations」P102），CITY HUNTER在第1卷和后面的流程上有很大不同，但它的一致性在于，有些东西是以角色獠的行动为起点。 因此，读者可以轻松地享受这个故事。 
然而，在AH中，香瑩在形式上是主角，但有些软弱，而獠不一定是故事的驱动力。 无论在哪里，无论谁参与其中，都缺少一些东西。 主题应该是明确的，但不知何故，没有任何一致性。  

难道不仍然需要一个主要的故事情节来把这些故事联系起来吗？    
至少这就是读者所期望的。 如果你不让读者期待接下来会发生什么，这个女孩接下来会做什么，读者就会跟不上思路。 只有作者这样认为是不够的。 只要是漫画，如果不传达给读者，就没有意义。  
 
仅仅包括你想做的所有故事是不够的。 读者将不再满足于单元剧式的连载。  


[BACK](./ahessay06.md)  
[NEXT](./ahessay08.md)  