INDEX > CAT'S EYE >

# Impression

从整体上看，CAT'S EYE不是一部杰出的作品。    
但毋庸置疑，这是北条司的最著名作品之一。    

后来在各种采访和后记中，北条并没有对CAT'S EYE做出非常正面的评价，他说「我画了6周画腻了」或者「我只记得那些艰辛」之类的。  
然而，这对他后来的作品影响很大，Cat's Eye所在的咖啡馆将在「CITY HUNTER」和「Angel Heart」中以不同的场景出现。    
如果没有CAT'S EYE，CITY HUNTER本身就不会诞生。  

正如北条所感叹的那样，早期的绘画并不是很好。 这些场景在质量上也参差不齐，不是一直有趣。（译注：此句待核实）  
尽管如此，CAT'S EYE还是很有吸引力。   
这是因为这是一部只有北条，一个刚从大学毕业的、年轻的、对很多事情都不了解的、新人作者才能画出的作品。   
因为他年轻，所以无知，因为无知，所以可以无视细节、继续画下去。 即使作品做得不好，它也是有活力的，能产生能量。 这种能量吸引着读者。 当然，对知识和技能的吸收和掌握是了不起的。 对比该书的第1卷和最終巻，令人惊讶的是，他的绘画水平有了很大提高，以至于你会认为它不是同一个人画的。 

在1996年出版的小说版本中，十多年来第一次重新绘制了插图和漫画，但我无法感受到连载时的能量。 显然有些东西是不同的。 在后记中，北条还写道，一位熟悉的编辑（堀江先生，在CE连载时的编辑，现在可能是Coamix的总裁？） )评论说「对这些画没有感觉」。 这些画说明了很多问题。

这可能不限于漫画的任何方式。 可以说，图片、诗歌、故事、音乐和其他创意作品都包含了它们被创作时的能量。 不可能、也没有必要在这么多年后重现艺术作品的力量，哪怕是一件糟糕的作品。 现在应该而且必须能够创造新东西...如果你是一个专业人士。  

＊

到目前为止，CE的成功背后的驱动力之一可能是动画。 时尚的主题音乐、美丽的画面和有趣的故事情节，即使作为一部单一的动画片来看，也令人印象深刻。  
那些通过动画片发现CE、并阅读原作的人，肯定会对动画片和原作之间的差异感到惊讶。  
如果你还没有读过原作，不妨参考一下Episode Guide。 在动画的大多数情节中，CE偷盗，故事是一个关于小偷和侦探的惊心动魄的故事；但，如你所见，随着剧情的进展，这样的情节在原作中少了。 相反，原作展开的是一个以咖啡馆为背景的浪漫喜剧和人情剧Cat's Eye。 剧中人物有笑有哭，有时还很痛苦，但始终有一种温暖的气氛。  

如果换成别人来画CE的故事，而不是北条司，作品会不会受到欢迎？  
或者说它可能是一个热门。 因为这就是动画。   
那部动画也被很多人接受，因为它没有失去原作的任何独特的温情。    

正如北条在采访和后记中所写的那样，CE的设定是根据他的一个朋友的建议、在与他的朋友们的讨论中诞生。 但是，是北条司有能力将其画成漫画，并以吸引人的画面和角色将作品带给世界、取得了巨大的成功（当然，肯定有负责人的支持）。  

CAT'S EYE意义重大，可以说它让世界认识了漫画家北条司，以及他作品的内涵。     

[BACK](./ceanother.md)  
[NEXT](../index.md)  