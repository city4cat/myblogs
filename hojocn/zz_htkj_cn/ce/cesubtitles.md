INDEX > CAT'S EYE > エピソードガイド >

# Episode (Subtitle) List  

点击每一话(译注[^1])的编号，可以跳到「剧情简介和评论」部分的相关章节。   

[^1]: 表格内每话/每卷的中文标题采用《猫眼》香港玉皇朝版，共18卷135话。《猫眼》完全版共15卷158话，无法与下列表对应。  

[关于北条司短編集「Parrot ～幸福の人」収録の短編「CAT'S EYE」](./cesubtitles.md#shortce)

<CENTER>
<TABLE cellpadding="0" width="95%">
  <TBODY>
    <TR>
      <TD bgcolor="#191970">
      <TABLE border="1" cellpadding="3" width="100%">
        <TBODY valign="top">
          <TR>
            <TD colspan="2" class="tit10" bgcolor="#F0F8FF" align="center">収録巻数</TD>
            <TD colspan="2" class="tit10" bgcolor="#F0F8FF" align="center">Subtitle</TD>
            <TD colspan="3" class="tit10" bgcolor="#F0F8FF" align="center">被盗财物</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="50" rowspan="2" align="center" valign="middle">Guest</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="40" rowspan="2" valign="middle" align="center">动画<BR>
            对应的<BR>
            话数</TD>
            <TD class="tit10" bgcolor="#F0F8FF" rowspan="2" valign="middle">备注<BR>
            (括号内的数字是对相关动画片集数的补充)</TD>
          </TR>
          <TR>
            <TD class="tit10" bgcolor="#F0F8FF" width="100" align="center">Comics</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="90" align="center">文庫本</TD>
            <TD class="tit10" bgcolor="#F0F8FF" align="center" width="8">No</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="110" align="center">Title</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="70" align="center">名字</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="70" align="center">种类</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="70" align="center">収蔵场所/<BR>
            所有者</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" rowspan="8" width="100"><B>&lt;1&gt;<BR>
            性感的火爆女郎</B><BR>
            S56(1981)29号・40号～44号</TD>
            <TD class="t9" bgcolor="#FFFFFF" rowspan="15" width="100"><B>&lt;1&gt;</B><BR>
            S56(1981)29号・40号～51号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" rowspan="3" width="10"><A href="ceepisode1-20.md#1">1</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" rowspan="3" width="120">性感的火爆女郎</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">罗马之女</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">国立新渋美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#1</TD>
            <TD class="t9" bgcolor="#FFFFEB">（对话的一部分，被盗)</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#FFFFEB" width="70">王者之星</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">200克拉的钻石</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">東部宝石</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#3</TD>
            <TD class="t9" bgcolor="#FFFFEB">（猫眼小瞳打俊夫耳光的故事）</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#FFFFEB" width="70">皇后之泪</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">150克拉的红宝石巨星</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">南州美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#1</TD>
            <TD bgcolor="#FFFFEB" class="t9">（被盗)<BR>
            Queen Tear未曾尝试。</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#2">2</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">摩天大廈走鋼線</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">读书</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">新宿国際貿易大厦</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#3">3</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">麻煩的戀情</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">英国王朝展的王冠的红宝石</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">红宝石</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">大橋美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">河野君</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">#23</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#4">4</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">你喜歡金髮嗎？</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">在法国国际艺术展上展出的作品</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">大日本貿易</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">鲁邦的新娘</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#4</TD>
            <TD bgcolor="#FFFFFF" class="t9">偷了一卡车的画…</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#5">5</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">夜間飛行是危險的香味</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">莱茵河</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">都立美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#5</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#6">6</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">小貓送給你</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">北条司?</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD bgcolor="#FFFFFF" class="t9">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" rowspan="8" width="100"><B>&lt;2&gt;<BR>
            父親的畫像</B><BR>
            S56(1981)45号～52号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#7">7</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">小偷紳士登場</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">出浴的女子</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画<BR>
            （克拉纳夫所画）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">德国美術展</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD bgcolor="#FFFFEB" class="t9">現在归冴羽所有!?</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#8">8</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">討厭的老鼠</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">蓝宝石</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">50克拉的蓝宝石</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">光栄宝石</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD bgcolor="#FFFFFF" class="t9">换成老鼠偷之前的假货</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#9">9</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">對父親的回憶</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">MY DAUGHTER HITOMI</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画<BR>
            （Heinz作）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">東京港停泊中的船</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD bgcolor="#FFFFEB" class="t9">画作的标题刻在相框上。</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#10">10</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">我愛小瞳</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">爆弾魔</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#7</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#11">11</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">貓顯神通!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">瀬口広幸</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#12">12</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">貓靈現身</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">流泪的少女和其他6件</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画<BR>
            （Heinz作）</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">瀬口邸</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">瀬口広幸等6名画商</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#2</TD>
            <TD class="t9" bgcolor="#FFFFFF">(部分剧情)</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#13">13</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">父親的畫像</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">海因茨肖像画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画<BR>
            （Heinz作）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">守屋の美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">守屋（三宅）</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#2</TD>
            <TD bgcolor="#FFFFEB" class="t9">（部分谈话）<BR>
            被盗但送人</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#F5F5F5" width="100" rowspan="15"><B>&lt;2&gt;</B><BR>
            S56(1981)52号～S57(1982)15号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#14">14</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">目標戰鬥女神</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">爆弾魔</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD bgcolor="#FFFFFF" class="t9">不幸的是，它是假的。</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" rowspan="8" width="100"><B>&lt;3&gt;<BR>
            天降神鷹!</B><BR>
            S57(1982)1・2合併号?～9号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#15">15</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">天降神鷹!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">火星女神</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画<BR>
            （Cranaff作）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">瀬口記念館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#1</TD>
            <TD class="t9" bgcolor="#FFFFEB">(盗窃的方法）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#16">16</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">惡犬危機!!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">河野君</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#17">17</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">武裝犬VS.小愛</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">河野君</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#10</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#18">18</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">貓之眼是女人…</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">火星女神</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画<BR>
            （Cranaff作）</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">近代美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode1-20.md#19">19</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">希望時間就此停頓<BR>
            (できることなら　このままで)</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#9</TD>
            <TD bgcolor="#FFFFEB" class="t9">括号内的标题是文库本的副标题。…哪个是真的？</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode1-20.md#20">20</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">只要10分鐘</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">？銀行</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">銀行の店長</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#16</TD>
            <TD class="t9" bgcolor="#FFFFFF">(工作组的精心制作团队服饰)</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#21">21</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">女人心海底針</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">放火犯</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#II-6</TD>
            <TD class="t9" bgcolor="#FFFFEB">(俊夫搬到了小瞳家里）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#22">22</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">不准碰日記！</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">凄井美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD bgcolor="#FFFFFF" class="t9">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" rowspan="9" width="100"><B>&lt;4&gt;<BR>
            Little Hurricane</B><BR>
            S57(1982)10号～18号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#23">23</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">淺谷小姐鬧雙胞</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">母子像</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">像<BR>
            （彫刻?　鋳造?）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">神保美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD bgcolor="#FFFFEB" class="t9">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#24">24</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">雪地裏的訪客</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">由起子</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#22</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#25">25</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">Little Hurricane</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">?美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center" valign="middle">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#22</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#26">26</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">也來當小偷…</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center" valign="middle">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#8</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#27">27</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">背對背的兩個人</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画<BR>
            （Cranaff作）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">S国大使館?</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">S国大使（绘画贩卖组织老大）</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#14</TD>
            <TD class="t9" bgcolor="#FFFFEB">(逃离小屋，背靠背）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#28">28</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">喪失記憶</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">少女与灯塔</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">?美術館?</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#28</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#FFFFFF" width="100" rowspan="17"><B>&lt;3&gt;</B><BR>
            S57(1982)16号～31号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#29">29</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">小偷學入門</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">九段美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#30">30</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">人無完美</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">胸像</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">鷺宮美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#31">31</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">追擊指紋!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">?美術館?</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" rowspan="10" width="100"><B>&lt;5&gt;<BR>
            貓之眼是情敵!</B><BR>
            S57(1982)19号～27号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#32">32</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">刑警咬到了小貓!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#29</TD>
            <TD class="t9" bgcolor="#FFFFFF">（不间断的玩笑）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#33">33</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">反向射擊烏龍刑警</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">似勢丹美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#34">34</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">鬼貓</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">红宝石</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">高衣宝石店</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#19</TD>
            <TD class="t9" bgcolor="#FFFFFF">(Cat's Eye的幻影）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10" rowspan="2"><A href="ceepisode21-40.md#35">35</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120" rowspan="2">功夫警探</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center" rowspan="2">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center" rowspan="2">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" rowspan="2">-</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">佛像</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">?美術館?</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#36">36</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">魔術手</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">泉台美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">河野君</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD bgcolor="#FFFFFF" class="t9">这是一幅小而无价的画。</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#37">37</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">三重衝擊!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#38">38</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">貓之眼是情敵!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">彫刻</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">板橋美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#7</TD>
            <TD class="t9" bgcolor="#FFFFFF">(瞳一边听唱片一边担心）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode21-40.md#39">39</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">警察難為!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">在莱茵河畔</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">堀江美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode21-40.md#40">40</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">倒數計時的戀情</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">四井大厦美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#33</TD>
            <TD bgcolor="#FFFFFF" class="t9">（用钟摆偷东西 ）</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" rowspan="9" width="100"><B>&lt;6&gt;<BR>
            紅色情書</B><BR>
            S57(1982)28号～36号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#41">41</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">我的貼身保鏢</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#42">42</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">雨中追緝令</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#43">43</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">多愁善感的小愛</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">銀行強盗</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#44">44</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">紅色情書</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">根岸美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#21</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#F5F5F5" width="100" rowspan="14"><B>&lt;4&gt;</B><BR>
            S57(1982)32号～46号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#45">45</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">貓之眼給的情書</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#46">46</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">月光下目擊者</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">和美</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#3</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#47">47</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">追擊獨家新聞!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">太田美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">和美</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#48">48</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">煩人小惡魔</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">基栄美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">和美</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#3</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#49">49</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">不入虎穴焉得虎子…</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">裸婦</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">西村氏</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD bgcolor="#FFFFEB" class="t9">实际盗窃的是犬鳴署的拘留所。</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" rowspan="8" width="100"><B>&lt;7&gt;<BR>
            最後一擊!</B><BR>
            S57(1982)37号～44号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#50">50</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">給風抱擁</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#6</TD>
            <TD bgcolor="#FFFFFF" class="t9">短篇集「Parrot」中「CAT'S EYE」的原创故事之一。</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#51">51</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">最後一擊!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">城北美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">館長</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#17</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#52">52</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">感受一下戀愛</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#6</TD>
            <TD bgcolor="#FFFFFF" class="t9">短篇集「Parrot」中「CAT'S EYE」的原创故事之一。</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#53">53</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">這也算失戀?</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD bgcolor="#FFFFEB" class="t9">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#54">54</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">漂亮的逃脫</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">摩天大楼的42层 </TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#1</TD>
            <TD class="t9" bgcolor="#FFFFFF">（乘飞机降落在高尔夫球场）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#55">55</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">夢幻信差</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#56">56</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">漸滅的靈魂</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">哥达鲁</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#36</TD>
            <TD class="t9" bgcolor="#FFFFFF">(美人鱼号游轮，国际艺术协会主席认识海因茨）&nbsp;</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#57">57</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">完美的約會</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" rowspan="5" width="100"><B>&lt;8&gt;<BR>
            超級賭徒</B><BR>
            S57(1982)45号～S58(1983)3号?</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#58">58</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">超級賭徒</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">赌场发牌人</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#11</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#FFFFFF" width="100" rowspan="8"><B>&lt;5&gt;</B><BR>
            S57(1982)48号～S58(1983)11号<BR>
            （计算结果表明，缺少47号…一回休み?）</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode41-60.md#59">59</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">爲了快樂的日子</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">危険女人</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画<BR>
            （海因茨作）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">赌场老板（久寿美）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">久寿美</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#12</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode41-60.md#60">60</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">愛情呀！求你不要走</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#13</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#61">61</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">別纏上差勁的女子</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">安岡弘喜</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#62">62</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">油輪上的小伙子</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">优雅的贵妇</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">伪钞工厂的油轮</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">香椎第二貿易的女社長</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#15</TD>
            <TD bgcolor="#FFFFFF" class="t9">(画作保管地点（油轮）</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" rowspan="4" width="100"><B>&lt;9&gt;<BR>
            雪女傳說</B><BR>
            S58(1983)4号～11号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#63">63</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">小愛的快樂聖誕</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#24</TD>
            <TD class="t9" bgcolor="#FFFFEB">(海因茨的录音）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#64">64</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">新年舞會</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">大酒店</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#65">65</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">婚禮中的扔批…</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#66">66</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">雪女傳說</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">雪之女王</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">铜像</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">座王美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">蔵元麗華</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#25</TD>
            <TD bgcolor="#FFFFFF" class="t9">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" rowspan="3" width="100"><B>&lt;10&gt;<BR>
            離南島來的邀請函</B><BR>
            S58(1983)12号～20号</TD>
            <TD class="t9" bgcolor="#F5F5F5" width="100" rowspan="11"><B>&lt;6&gt;</B><BR>
            S58(1983)12号～28号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#67">67</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">離南島來的邀請函</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">光之変奏曲</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">東洋財団</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">海原神</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#26、27</TD>
            <TD bgcolor="#FFFFEB" class="t9">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#68">68</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">美好的生活</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#69">69</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">特製貓之眼式犯人烹調法</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">次原美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">怪盗28号</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#19</TD>
            <TD bgcolor="#FFFFEB" class="t9">（伪装成浅谷的小瞳在犬鳴署。通过下水道逃跑）<BR>
            怪盗28号早些时候偷了赃物，又把它们还了回去。</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" rowspan="9" width="100"><B>&lt;11&gt;<BR>
            求婚記</B><BR>
            S58(1983)21号～29号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#70">70</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">滿園春色迷你裙</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">阿蒙雷博物馆</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD bgcolor="#FFFFFF" class="t9">图片与JC第3卷74页的小爱的插画（Collection No.8）相似。</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#71">71</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">貓之眼特搜組成立！！</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">所泽广</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#72">72</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">相親惡作劇…</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#73">73</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">好強的特搜組</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">银星</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">宝石</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">白泉堂宝石</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD bgcolor="#FFFFEB" class="t9">为了提高Cat's Eye特搜组的地位而被盗。与海因茨收藏无关。</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#74">74</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">男人的承諾！</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#75">75</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">訂婚介指是誰的？</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">香菜</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#76">76</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">單身宿舍戀愛狂想曲</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#77">77</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">求婚記</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#FFFFFF" width="100" rowspan="15"><B>&lt;7&gt;</B><BR>
            S58(1983)29号～45号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#78">78</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">被遺忘的求婚</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" rowspan="7" width="100"><B>&lt;12&gt;<BR>
            金髮女郎</B><BR>
            S58(1983)30号～38号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode61-80.md#79">79</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">把戒指套在他手上!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode61-80.md#80">80</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">致命的吸引力!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">武岡高原美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">#II-33</TD>
            <TD bgcolor="#FFFFFF" class="t9">(浅谷爱上了男装的猫眼）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#81">81</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">愛情的力量</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#82">82</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">金髮女郎</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">母の肖像</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画<BR>
            （海因茨作）</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">都立美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#83">83</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">抓住我一輩子!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#33</TD>
            <TD class="t9" bgcolor="#FFFFEB">（开头与重先生的对话）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#84">84</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">不許花心!!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#85">85</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">舊愛與新歡</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">夏目慶子</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" width="100" rowspan="9"><B>&lt;13&gt;<BR>危險的疑惑</B><BR>S58(1983)39号～47号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#86">86</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">仲夏夜的噩夢</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">下着泥棒</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#87">87</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">危險的疑惑</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">小倉美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#88">88</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">九月的白日夢</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#89">89</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">有聲情書</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#90">90</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">我只在乎你</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#91">91</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">我已不是孩子了</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#92">92</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">30億的鑽石大餐</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#F5F5F5" width="100" rowspan="17"><B>&lt;8&gt;</B><BR>
            S58(1983)46号～S59(1984)12号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#93">93</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">籠中鳥</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#94">94</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">沉重的扳機</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">大関莫人</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" width="100" rowspan="10"><B>&lt;14&gt;<BR>
            燃燒過去</B><BR>S58(1983)48号～S59(1984)7号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#95">95</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">燃燒過去</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">不明</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画（海因茨作）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">葉月八郎</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD bgcolor="#FFFFEB" class="t9">没有机会偷，也不能公开，所以俊夫被迫发射烟火装置来烧毁它。</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#96">96</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">男朋友的資格</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#97">97</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">夜之吻!!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">小旅舍老板</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#98">98</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">超越時空的戀情</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">不明（真璃絵的肖像画）</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画<BR>
            （海因茨作）</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">巽忠恭</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">巽忠恭</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD bgcolor="#FFFFFF" class="t9">这东西不能偷，是巽忠恭给她的，但她最终还是归还了。 (放在巽的棺材里）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode81-100.md#99">99</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">教師風波</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">岡山先生</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode81-100.md#100">100</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">淚小姐的情人</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">歯医者</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#101">101</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">愛的拼圖</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#102">102</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">給我一個熱吻</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#103">103</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">刑警的西裝</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#104">104</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">富勇氣的決定!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" width="100" rowspan="10"><B>&lt;15&gt;<BR>
            漫長的一天…</B><BR>
            S59(1984)8号～17号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#105">105</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">假如是一個美麗的謊言…</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#106">106</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">充滿魅力的男人!?</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#107">107</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">我是一家之主?</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD bgcolor="#FFFFEB" class="t9">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#108">108</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">三個臭皮匠，勝過一個諸葛亮</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#109">109</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">可怕的底褲!!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#FFFFFF" width="100" rowspan="16"><B>&lt;9&gt;</B><BR>
            S59(1984)13号～29号-</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#110">110</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">爲了某人而努力…</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#111">111</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">我最重要的女人!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#112">112</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">發出挑戰書!!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#113">113</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">劫新娘!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#114">114</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">漫長的一天…</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">俊夫的祖母</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" width="100" rowspan="8"><B>&lt;16&gt;<BR>
            通告信的秘密</B><BR>
            S59(1984)18号～25、27号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#115">115</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">歪曲的時間!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">操纵玩偶的人</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">重村</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">俊夫的祖母</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">#II-25</TD>
            <TD bgcolor="#FFFFEB" class="t9">(影片开始时，俊夫和女警之间的互动）</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#116">116</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">俊夫的爛攤子</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">渡辺正明<BR>
            俊夫的祖母</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#117">117</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">黑夜的誘惑!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#118">118</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">老鼠也瘋狂…</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode101-120.md#119">119</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">小貓頸上繫鈴!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode101-120.md#120">120</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">飯盒恐懼癥!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode121-135.md#121">121</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">通告信的秘密</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">遙远的旅人</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">新宿灯塔酒店</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode121-135.md#122">122</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">夕陽與貓之眼</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#F0FFF0" width="100" rowspan="9"><B>&lt;17&gt;<BR>
            阿佛洛狄忒的醒覺</B><BR>
            S59(1984)26号・28号～36号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode121-135.md#123">123</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">似曾相識的風</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">美術館館長<BR>
            陽子（館長的女儿）</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode121-135.md#124">124</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">煙消雲散的畫</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">阿芙洛狄忒</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">長丘美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode121-135.md#125">125</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">甜蜜的輕聲細語</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD class="nh9" bgcolor="#F5F5F5" width="100" rowspan="11"><B class="t9">&lt;10&gt;</B><BR>
            S59(1984)30号～44号・S60(1985)6号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode121-135.md#126">126</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">掉入陷阱的貓之眼</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">静止的時間</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">日本文化中心大楼内美術館</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode121-135.md#127">127</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">你的眼只許望著我!</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode121-135.md#128">128</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">阿佛洛狄忒的覺醒</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode121-135.md#129">129</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">冷笑的女神</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode121-135.md#130">130</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">風中飛舞的淚珠</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode121-135.md#131">131</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">來自過去的訪客</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">香月均</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD class="t10" bgcolor="#FFFFFF" width="100" rowspan="5"><B>&lt;18&gt;<BR>
            永遠的牽絆（とわ）に!</B><BR>
            S59(1984)37号～44号・S60(1985)6号</TD>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10" rowspan="2"><A href="ceepisode121-135.md#132">132</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120" rowspan="2">危險的遊戲</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">西風</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画<BR>
            （海因茨作）</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">克拉纳夫</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">美術品辛迪加管家</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD bgcolor="#FFFFFF" class="t9">由克拉纳夫送来的。</TD>
          </TR>
          <TR>
            <TD class="t9" bgcolor="#FFFFFF" width="70">季節的女神</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">絵画<BR>
            （海因茨作）</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">克拉纳夫</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50">克拉纳夫</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode121-135.md#133">133</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">最後的決戰</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">天空神</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">絵画<BR>
            （海因茨作）</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">私立蔵内美術館</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50">克拉纳夫</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">-</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFFF" width="10"><A href="ceepisode121-135.md#134">134</A></TD>
            <TD class="t9" bgcolor="#FFFFFF" width="120">永遠的牽絆!</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFFF">連載最終回</TD>
          </TR>
          <TR>
            <TD align="center" class="t9" bgcolor="#FFFFEB" width="10"><A href="ceepisode121-135.md#135">135</A></TD>
            <TD class="t9" bgcolor="#FFFFEB" width="120">再一次戀愛</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="70">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" width="50" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB" align="center">-</TD>
            <TD class="t9" bgcolor="#FFFFEB">完結編</TD>
          </TR>
          <TR>
            <TD class="tit10" bgcolor="#F0F8FF" width="100" align="center">Comics</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="100" align="center">文庫本</TD>
            <TD class="tit10" bgcolor="#F0F8FF" align="center" width="10">No</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="120" align="center">Title</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="70" align="center">名字</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="70" align="center">种类</TD>
            <TD class="tit10" bgcolor="#F0F8FF" width="70" align="center">収蔵场所/<BR>
            所有者</TD>
            <TD rowspan="2" class="tit10" bgcolor="#F0F8FF" align="center">Guest</TD>
            <TD rowspan="2" class="tit10" bgcolor="#F0F8FF" align="center">动画<BR>
            对应的<BR>
            话数</TD>
            <TD rowspan="2" class="tit10" bgcolor="#F0F8FF">備考<BR>
            (括号内的数字是对相关动画片集数的补充)</TD>
          </TR>
          <TR>
            <TD colspan="2" class="tit10" bgcolor="#F0F8FF" align="center">収録巻数</TD>
            <TD colspan="2" class="tit10" bgcolor="#F0F8FF" align="center">Subtitle</TD>
            <TD colspan="3" class="tit10" bgcolor="#F0F8FF" align="center">被盗财物</TD>
          </TR>
        </TBODY>
      </TABLE>
      </TD>
    </TR>
  </TBODY>
</TABLE>
</CENTER>


# 关于短篇「CAT'S EYE」 {#shortce}

（1997年？）    
短編集「Parrot」収録的番外編。它似乎是根据连载时的第50话「給風抱擁」和第52话「感受一下戀愛」画的。CG合成的画面很美。      
最后一幕里，三姐妹穿着不是熟悉的紧身衣，而是1997年真人电影中的服装。 这可能是为了纪念电影改编而画的。      

[BACK](./ceepisodes.md)  
[NEXT](./ceanime.md)  