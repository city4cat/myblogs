INDEX >

# CAT'S EYE　 キャッツ❤アイ

今天，可能没有一个20多岁、30多岁的人不知道CAT'S EYE这个名字。   
CAT'S EYE是让北条司作为漫画家获得重大突破的作品。     
他的处女作「CAT'S EYE」首次创作并发表在1981年第29期周刊少年Jump上。 这部作品广受好评，很快就被连载了，这也是北条司的第一部连载作品。      
从1983年起，又有两个系列的TV动画片相继推出。 由杏里演唱的主题曲「CAT'S EYE」大受欢迎，并使这部作品出名。    
三个穿着紧身衣的漂亮姐妹做小偷、一个侦探和一个小偷做恋人，这种不太可能的设定吸引了许多粉丝。    

虽然原来的故事早已结束，但一直有真人剧和电影，最近也有利用其形象制作的电视剧，如「木更津CAT'S EYE」。    
"CAT'S EYE"这个名字与"鲁邦3世"一起，在日本漫画和动漫界成为怪盗的代名词。      

**掲載**： 	  
周刊少年Jump・1981年40号～1984年44号, 1985年6号    
**収録**： 	  
集英社 JUMP COMICS 「CAT'S EYE」全18巻  
集英社 集英社文庫（Comic版）「CAT'S EYE」全10巻  
集英社 愛蔵版「CAT'S EYE」全10巻  
新潮社 BUNCH WORLD 「CAT'S EYE」全21巻  
集英社 Playboy Comic BART EDITION　北条司短編集「Parrot ～幸福の人」全1巻（短編）  

## CONTENTS
■ 	[Story&Main Charactors](./cestory-charactor.md) 	：剧情简介和主要人物  
■ 	[Episode Guide](./ceepisodes.md) 	：原作的剧集清单（Subtitle） (译注：Subtitle指每话/每集的标题)  
■ 	[Animation](./ceanime.md) 	：关于TV动画系列  
■ 	[Another CAT'S EYE](./ceanother.md) 	：漫画和Anim之外的发展  
■ 	[Impression](./ceimpress.md) 	：总体印象。有一点害羞…？  

[HOME](../index.md)  
[NEXT](./cestory-charactor.md)  
