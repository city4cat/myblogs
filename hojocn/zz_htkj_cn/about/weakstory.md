INDEX > About 北条司 > 北条作品の特徴と弱点

# 故事情节的弱点

在我看来，北条司作品的最大弱点是其叙事或结构。  
特别是在CITY HUNTER中，故事情节非常重要，因为这部作品（至少在早期到中期阶段）用来娱乐的。 不仅要通过环境和人物来吸引读者，还要通过故事的发展和情节来吸引读者（即让读者紧张，或者让读者因一个转折而惊讶）。  
有时我们在日常对话中寻找一个主线，甚至一个结论。 如果你的故事没有一个结论，你很可能会让人失望，人们会问"你想说什么?"。  
作为一个专业漫画家，必须取悦读者。 这就是读者买漫画的原因。 如果你只画你想画的图画，那就不专业。 当然没有必要强迫自己去画不想画的东西，但为了把你想画的东西和你想说的东西传达给观众，并让他们一路欣赏下去，你需要有一定的「技巧」。  

然而，实际作品中很少有伏笔，即使有，也几乎是杂乱无章的。    
他们试图在没有伏笔的情况下快速出作品，所以他们增加了一个新的设定（ 例如，这个人实际上是一个双胞胎！或者这个人和这个人互相认识! )，这往往与已经写过的内容相矛盾。 这可能是随着我对人物的深入研究而自然发生的，但当我后来重读他们时，他们的言行并不一致，尽管他们是同一个人。 当这种情况发生时，读者会怀疑先前的描述是否是一个谎言。 甚至你现在读到的这个人物的言行也令人怀疑是否真实。（译注：此段待确认）  

不仅他的作品如此，那些长期连载的作品也是如此，可以说，故事的简单性增强了人物的魅力。   
北条还指出，CH的侧重点不是事件，而是男人和女人的心灵互动（「北条司漫画家20周年記念画册」P104）。  
然而，如果故事没有发展、不足够可信以说服读者，这种心理描写就会失去效果。    

如果你想保持你目前的立场，你应该只画不需要精心设计故事的日常场景，如F.COMPO。 如果是这样的话，你已经证明了你的能力，你很可能会说服读者。 但如果你想做一个像CH那样富于娱乐的故事，依你目前的方式，你前进的空间是有限的。  
-换句话说，我认为如果你能将当前的表现力与一个有转折的好故事相结合，你就能画出真正有趣的东西。  

<font color=#004d99>就个人而言，我希望看到他做原创作品，他似乎有这方面的弱点，就像新潮社很久以前出版的(他那一代的)爱情故事。 因为有时当我读一本小说时，我会想「我想在北条司的漫画中读到这个」。</font>（译注：此段待确认）  

我认为北条司内心有很多美妙的形象。我希望他能研究研究如何把这些形象以读者能理解的方式呈现出来。  

[BACK](./ugly.md)  
[NEXT](./frenchdoujin.md)   
