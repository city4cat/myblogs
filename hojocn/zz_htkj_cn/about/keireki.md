[INDEX](../index.md) > [About 北条司](./abindex.md) >

# 职业生涯  

这是根据已发表的传记和访谈的摘要。  
只列出了最重要的作品。  
（全部名单见[下页](./list_works.md)。 请酌情参考。）  


【参考資料】  
「北条司Illustrations」（译注：ISBN 4-08-858150-4）  
「シティーハンターパーフェクトガイドブック」（译注：《城市猎人完全指导手册》，需确认该书中文译名）  
「北条司漫画家20周年記念イラストレーションズ」（译注：《北条司漫画家20周年纪念插图》，需确认该书中文译名）  
週刊コミックバンチ 2000年7・8合併号、9号「漫画家のかたち　北条司」（译注：漫画周刊，2000年第7、8期，第9期「漫画家的形态 北条司」）  



<center>
<table width="95%" cellpadding="0">
  <tbody>
    <tr>
      <td bgcolor="#191970">
      <table width="100%" cellpadding="3" border="1">
        <tbody>
          <tr>
            <td class="tit10" width="50" bgcolor="#F0F8FF">年份</td>
            <td class="tit10" width="75" bgcolor="#F0F8FF">年龄</td>
            <td class="tit10" bgcolor="#F0F8FF">事件</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1959年<br>
            (昭和34)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">0歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">3月5日出生在福冈县小仓市（现在的北九州市）</p>
            他的父亲在一家建筑公司工作，母亲是一名家庭主妇。 他有一个比他大三岁的哥哥。<br>
            他父亲的手艺很好。 他是一个工作狂，但在休息日，他制作家具和玩具，而且还非常擅长绘画。 何祚庥对绘画的兴趣和天赋可能主要是受到他父亲的影响。 从我有记忆起，我就喜欢画画。</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1965年<br>
            (昭和40)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">6歳<br>
            （小学校1年）</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">小学入学</p>
            这是他第一次画类似故事漫画的东西。<br>
            当上小学的时候，他不看漫画，而看小说、电视电影。</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1966年<br>
            (昭和41)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">7歳<br>
            （小学校2年）</td>
            <td class="t10" valign="top" bgcolor="#ffffff">在被老师批评了他的画之后，他就很少再画画了。</td>
          </tr>
          <tr>
            <td class="t10" rowspan="2" valign="top" bgcolor="#ffffff">1971年<br>
            (昭和46)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">12歳<br>
            （初中1年级）</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">中学校入学</p>
            读小说、看电影，而不是电视或动画。大多是科幻小说，但他并不太挑剔。买的第一本平装小说是A.E.ヴァン・ヴォークト的《比格尔号宇宙飞船》</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">14歳<br>
            （初中3年级）</td>
            <td class="t10" valign="top" bgcolor="#ffffff">在看过学校推荐的索菲亚・罗兰的「向日葵」后，他开始观看各种电影。 他最多一个月才去一次电影院，而且只在电影院看西方电影。 在一位正在画漫画准备参加手冢奖的同学的影响下，他在笔记本上用签字笔画了一本根据星野新一的长篇小说「声の網」改编的漫画，但结果是页数庞大，一直没有完成。<br>
            （当时，他不知道手冢奖是由周刊少年Jump赞助的。）</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1974年<br>
            (昭和49)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">15歳<br>
            （高中1年级）</td>
            <td class="t10" rowspan="3" valign="top" bgcolor="#ffffff">
            <p class="t11">高中入学</p>
            他和同学们上的是不同的高中，但他和他的朋友们，四男两女，组成了一个漫画绘画小组，每人拿一个角色，共同画一幅漫画。 就在这个时候，他们第一次一起工作，做出了一个看起来像名字的东西，并提笔写了下来。 这个故事是根据丰田有恒的科幻短篇小说改编的，显然是关于超自然的力量。 北条司负责主要人物和背景机械。 这个故事一直持续到大学第一年的开始，但从未完成。<br>
            只在高中二、三年级时自己画了两个类似SF的故事。<br>
            （在JC CAT'S EYE第四卷「コレクション」第12～14期中，介绍了他15～17岁时画的漫画《宇宙》。）</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1975年<br>
            (昭和50)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">16歳<br>
            （高中2年级）</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1976年<br>
            (昭和51)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">17歳<br>
            （高中3年级）</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1977年<br>
            (昭和52)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">18歳<br>
            （大学1年）</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">就读于九州产业大学，艺术学院，设计系<br>
            <span style="font-size : 10pt;">夏天，父亲因癌症去世</span></p>
            在前两年，他学习了色彩构成和绘画实践等基础知识，在第三年，他转向了广告设计。<br>
            当时，他有一个模糊的想法，即他想在电影业工作。<br>
            他还制作了8毫米电影和动画。 负责为一部动画制作故事板，但它有一个小时长，而且没有完成。 (不过据说他为这部动画所画的人物图案是『宇宙・天使』，他后来将其提交给手冢奖。)<br>
            他被一个朋友邀请为一个同好杂志画漫画和插图，它一个由艺术俱乐部的一个学长经营的、针对诗歌、插画和漫画的杂志。 他用铅笔画了草稿，但从未完成它。</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1979年<br>
            (昭和54)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">20歳<br>
            （大学3年）</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">参加集英社周刊少年Jump・第18届手冢奖的评选，以『宇宙・天使』获得二等奖</p>
            听说朋友参加了手冢奖的评选，他把自己一直在创作的漫画送去参加奖金评选（100万日元），结果获得了二等奖。
            <table width="100%">
              <tbody>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t11" width="35" valign="top">12月</td>
                  <td class="t11" valign="top">出席了在东京举行的颁奖仪式</td>
                </tr>
                <tr>
                  <td class="t10" valign="top"></td>
                  <td class="t10" valign="top">他第一次知道，手冢奖是新的漫画家通往成功的通道。<br>
                  然而，20万日元的奖金被用来购买电影制作的放映机，而他每年获得的36万日元的研究经费则全部用于电影。<br>
                  当时，他仍然没有打算以漫画家的身份出道。</td>
                </tr>
              </tbody>
            </table>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1980年<br>
            (昭和55)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">21歳<br>
            （大学4年）</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">周刊少年Jump 1980年8月増刊号连载『我是男子汉！』。 他的职业首秀。 </p>
            (JC CAT'S EYE 第4卷160页, collection NO.17, 「My・History」, 显示了他入选手冢奖后画的第一幅作品的封面，但被退稿。 我想知道他是否在「我是男子汉！」之前画的。)<br>
            当还是个学生的时候，曾经画过漫画，作为挣钱的一种方式。<br>
            夏天，画了一个侦探故事，有一个原创故事，但被拒绝了。</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1981年<br>
            (昭和56)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">22歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <table width="100%">
              <tbody>
                <tr>
                  <td class="t11" width="35" valign="top">1月</td>
                  <td class="t11" valign="top">週刊少年Jump1981年1月増刊号发表『三級刑事』（原作：渡海風彦）</td>
                </tr>
                <tr>
                  <td class="t10" valign="top"></td>
                  <td class="t10" valign="top">他在截止日期前得了流感，在朋友和已经出道的次原隆二的帮助下，他设法熬了三天的夜才完成。</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t11">3月</td>
                  <td class="t11">完成了『猫・眼』的手稿。 </td>
                </tr>
                <tr>
                  <td></td>
                  <td class="t10">在完成大学的毕业设计后，当他必须写出书名时，他有了"猫・眼"的想法，这是在发布会上与朋友喝酒聊天时想到的。</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" class="t11">大学毕业</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t11">6月</td>
                  <td class="t11">周刊少年Jump第29期上发表短篇『猫・眼』</td>
                </tr>
                <tr>
                  <td></td>
                  <td class="t10">该作品受到好评，以至于有人要求他写两部续集，他写了，并把它们寄出去。 他很惊讶地接到一个连载会议的电话，说他被选为连载对象，并被邀请去东京。<br>
                  由于他是在画漫画，而不是在找工作，所以他想他可以仅仅做一个漫画家而慢慢地画短篇故事，同时能在电影或动画方面找到工作。 他认为，由于他的绘画速度，不可能做连载。</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t11">7月</td>
                  <td class="t11">到东京</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t11">8月</td>
                  <td class="t11">开始画漫画</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t11">9月</td>
                  <td class="t11">周刊少年Jump第40期开始连载『CAT'S EYE』</td>
                </tr>
                <tr>
                  <td></td>
                  <td class="t10">在他成为漫画家的前三个月里，他的绘画速度很慢，整天都在画画。<br>
                  <font color="#660066">「当时，我担心如果错过了连载的机会，我可能会放弃生活。 我什么都不知道，只知道怎么画画......」（「北条司Illustrations」第95页，原文）</font><br>
                  这段时间的心情也浓缩在JC CAT'S EYE第一卷封底的卷首语中。 对于一个新的漫画家的第一本书来说，这是一个相当不令人鼓舞的评论。<br>
                  『CAT'S EYE』本身就是连载的练习，他说通过这个连载逐一学会了漫画的技巧。</td>
                </tr>
              </tbody>
            </table>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1982年<br>
            (昭和57)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">23歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">经过大约一年的连载（1982年左右：JC第四卷），事情开始变得更加有趣，他逐渐找到时间来发挥他的风格感。<br>
            <font color="#660066">「一路走来，我开始想，"也许漫画比拍电影更有趣"」（《北条司漫画家20周年纪念插图》，第103页）</font>）（译注[^1])<!-- --></td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1983年<br>
            (昭和58)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">24歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <table width="100%">
              <tbody>
                <tr>
                  <td class="t11" width="35" valign="top">7月</td>
                  <td class="t11" valign="top">日本电视网公司的『猫眼』系列电视动画片 </td>
                </tr>
                <tr>
                  <td class="t10" valign="top"></td>
                  <td class="t10" valign="top"><font color="#191970"><font color="#660066">「『猫・眼』和『城市猎人』被拍成动画的事实让我很感动。 我也喜欢动画，我在大学里做过独立电影和动画。」 (「北条司Illustrations」第96页)</font></font></td>
                </tr>
              </tbody>
            </table>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1984年<br>
            (昭和59)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">25歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <table width="100%">
              <tbody>
                <tr>
                  <td class="t11" colspan="2" valign="top">『CAT'S EYE』的连载在周刊少年Jump第44期结束</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t10" width="35" valign="top">12月</td>
                  <td class="t10" valign="top">女儿出生（摘自JC CAT'S EYE第13卷封面）</td>
                </tr>
              </tbody>
            </table>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1985年<br>
            (昭和60)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">26歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">周刊少年Jump第6期发表『CAT'S EYE』完結篇<br>
            周刊少年Jump第13期上『CITY HUNTER』连载开始</p>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1987年<br>
            (昭和62)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">28歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">读卖电视台的『CITY HUNTER』电视系列动画片 </p>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1989年<br>
            (平成元年)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">30歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff"><font color="#660066">"患有每年一次的『城市猎人』"结束了也没关系"的疾病（笑）。 当它是一个连载的周年纪念时，我只是去画一个相当严肃的故事，并认为，'如果它结束了也没关系。 前年我做的关于獠的往事也是如此。（下文略）"（「北条司Illustrations」P96）</font></td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1991年<br>
            (平成3)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">32歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
<table width="100%">
              <tbody>
                <tr>
                  <td class="t11" width="35" valign="top">3月</td>
                  <td class="t11" width="387" valign="top">集英社出版《北条司Illustrations》</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" class="t11">周刊少年Jump第50期『城市猎人』连载结束</td>
                </tr>
                <tr>
                  <td colspan="2" class="t10"><font color="#660066">"说实话，当『城市猎人』结束时，我已经疲惫不堪。我在职业上和个人上都很疲惫，而且我对编辑和作家之间的关系感到厌倦。但这种压力反过来又给了我制作短篇小说的力量。" (「北条司漫画家20周年記念插图」第104页)</font></td>
                </tr>
              </tbody>
            </table>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1993年<br>
            (平成5)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">34歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">周刊少年Jump第31期『阳光少女』连载开始</p>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1994年<br>
            (H6)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">35歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">周刊少年Jump第5・6期『阳光少女』连载结束<br>
            周刊少年Jump第43期『RASH!!』连载开始</p>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1995年<br>
            (平成7)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">36歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">周刊少年Jump第9期『RASH!!』连载结束</p>
            『RASH!!』开始后，北条觉得他已经达到了自己的极限。 他说，如果这个连载继续下去，他会崩溃。 他也质疑出版商的方法，画漫画变得很枯燥，他不愿意再画了。当时，他画了一个他想了很久的战争故事，渐渐地，他克服了这个问题，能够画下一个『F.COMPO』。</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1996年<br>
            (平成8)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">37歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">Manga ALLMAN第10期《F.COMPO》连载开始</p>
            <font color="#660066">「这很有意思。 我很喜欢画它。 这是我第一次有信心说，'我将在我的余生中作为一个漫画家来做。 我认为《F.COMPO》是我的一个「康复之作」（笑）」(漫画周刊2000年第9期第192页)</font></td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1998年<br>
            (平成10)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">39歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">BART3230 12月号作品『Parrot』开始连载（译注："BART3230"后增加了空格符）</p>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">1999年<br>
            (平成11)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">40歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11">『Parrot』结束</p>
            年底，他开始筹备成立Coamix</td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">2000年<br>
            (平成12)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">41歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
<table width="100%">
              <tbody>
                <tr>
                  <td class="t11" width="35" valign="top">1月</td>
                  <td class="t11" valign="top">集英社出版愛蔵版「城市猎人完美指南」</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" class="t11">Manga ALLMAN第23期『F.COMPO』连载结束</td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t11" width="35" valign="top">6月</td>
                  <td class="t11" valign="top">成立了Coamix公司</td>
                </tr>
                <tr>
                  <td></td>
                  <td class="t10" valign="top"><font color="#660066">「从现在开始，我打算不仅作为一名漫画家参与到漫画中来，而且还作为Coamix的董事之一和编辑顾问参与到漫画中来。」 (「为北条司漫画家20周年記念而创作的插图」第107页)</font></td>
                </tr>
                <tr>
                  <td colspan="2">
                  <hr>
                  </td>
                </tr>
                <tr>
                  <td class="t11" width="35" valign="top">12月</td>
                  <td class="t11" valign="top">集英社出版「为北条司漫画家20周年記念而创作的插图」</td>
                </tr>
              </tbody>
            </table>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">2001年<br>
            (平成13)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">42歳</td>
            <td class="t10" valign="top" bgcolor="#ffffff">
            <p class="t11" valign="top">5月　漫画周刊创刊<br>
            从创刊号开始连载『Angel Heart』</p>
            </td>
          </tr>
          <tr>
            <td class="t10" valign="top" bgcolor="#ffffff">2002年<br>
            (平成14)</td>
            <td class="t10" valign="top" bgcolor="#ffffff">43歳</td>
<td class="t11" valign="top" bgcolor="#ffffff">『Angel Heart』連載中</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
  </tbody>
</table>
</center>


[^1]  原网页此处有HTML注释： ちなみにこの頃ご結婚?　（神野がそう推理したワケ:JC CAT'S EYE3巻カバー折り返し（1982年11月15日第1刷発行）の作者コメントは、キャラクター（瞳）の日記風に書かれているが、どことなく意味深で、必ずしも作品の内容にそったものとは思えない。他の作品を含めた全単行本を通しても、キャラクターのセリフの形で書かれているのは、この巻のみ。単行本をお持ちの方は、読んでみて下さい。)  
译文： 顺便说一下，你是在这个时候结婚的吗？　(神野如何推断出这一点:在JC CAT'S EYE第三卷（1982年11月15日首次印刷出版）封面上作者的卷首语是以人物（瞳）的日记的方式写的，但有些意味深长，不一定与作品内容相符。 这是唯一一卷作者的评论是以人物台词的形式写的，尽管该系列的其他书都是以同样的风格写的。 如果你有这本书的副本，请阅读它。)  


[BACK](./abindex.md)  
[NEXT](./list_works.md)  