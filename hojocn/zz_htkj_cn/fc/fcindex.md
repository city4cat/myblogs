INDEX >

# F.COMPO　ファミリー・コンポ

这是第一部在青年杂志上连载的作品。  
最初，它准备在少年杂志上连载，但「一对性别颠倒的已婚夫妇」的设定没有被编辑部接受，因为他们说「这对少年杂志来说是变态漫画」。因此，决定在「ALLMAN」上连载这个故事，该杂志大约在那时发行。  
结果，它成为自CITY HUNTER以来最受欢迎的作品，该系列作品持续了4年。  

当你听到北条司正这个名字时，大多数人都会想到他最著名的作品CAT'S EYE和CITY HUNTER。 但如果你对他的其他作品感兴趣，你一定要读这部。 这是北条最有成就的作品之一，是必读之作。  
这部作品以喜剧形式严肃地审视了家庭和性别问题，这样的故事也曾被多次塑造出来，相信会令你印象深刻的。（译注：该句译文待确认）  

关于F.COMPO的更多信息，请访问りっくす氏的网站「[北条司応援隊」](http://fcompo_sp.tripod.co.jp/)，在那里你可以找到更多关于该作品的细节，包括每集的内容。  

在此，我想说说我读这部作品时的一些感受。 
掲載： 	漫画ALLMAN 1996年(平成8)No.10～2000年(平成12)No.23  
収録： 	
集英社 SC Allman「F.COMPO」全14巻  
新潮社 BUNCH WORLD 「F.COMPO」出版中 (現在是2002年8月)  


## CONTENTS
■ 	[Story&Main Charactors](./fc_story-charactors.md) 	：简介和主要人物  
■ 	[品味F.COMPO](./fcenjoy.md) 	
	：以下是我认为在阅读F.COMPO作品时需要注意的要点的总结。  
[作为跨性别者](./fcenjoy.htm#1) | 
[追求真实](./fcenjoy.htm#2) | 
[难以拍成电影的作品](./fcenjoy.htm#3) | 
[漫画家的生活一瞥…](./fcenjoy.htm#4) | 
[多种多样的剧集](./fcenjoy.htm#5)  
■ 	[Impressions](./fcimpressions.md) 	
	：我读完F.COMPO后的各种印象。 请原谅文字的僵硬。  
[First Impression](./fcimpressions.htm#1) | 
[若苗家3个有趣的人](./fcimpressions.htm#2) | 
[我的第一个「爱情故事」](./fcimpressions.htm#3) | 
[从家庭剧到青春剧](./fcimpressions.htm#4) | 
[不负责任的结局](./fcimpressions.htm#5) | 
[最后…](./fcimpressions.htm#6) 


[BACK](../index.md)  
[NEXT](./fc_story-charactors.md)  

