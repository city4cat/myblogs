INDEX >

# 其他作品

对其他主要作品的简要解说和感想。  
（完整的作品清单，见「About 北条司」[这里](../about/list_works.md)）  

[额外专栏「类似北条作品的戏剧」](../column/col_hojolike.md)    

## 长篇-连载  
■ [阳光少女](./komorebi.md)[^2] 	  
■ [RASH!!](./rash_splash.md#rash)  
■ [SPLASH!](./rash_splash.md#splash) 	
■ [Parrot](./parrot.md)  

[战争系列](./war.md) 	
■ [蓝天的尽头…－少年们的战场－](./war.md#soku)  
■ [少年们的夏天 ～Melody of Jenny～](./war.md#jenny)  
■ [American Dream](./war.md#dream)  

## 短篇
■ [我是男子汉！](./oreha_neko.md) 	
■ [白猫少女](./oreha_neko.md#neko)[^1]  
■ [天使的礼物](./tenshi_taxi.md) 	 
■ [TAXI DRIVER](./tenshi_taxi.md#taxi)  
■ [樱花盛开时](./sakura_plot.md) 	
■ [Family Plot](./sakura_plot.md#plot)[^3]  
■ [少女的季节 －夏之梦－](./summer_air.md) 	
■ [AIR MAN](./summer_air.md#air)  
■ [THE EYES OF ASSASSIN](./assassin_portrait.md) 	
■ [Portrait of father](./assassin_portrait.md#portrait)  

译注：  
[^1] 《白猫少女》原著名为"ネコまんまおかわり❤"，直译可能为"再来一份猫饭❤"。中文译名有："白猫少女"、"再续猫缘"、["再来一客猫饭"](https://www.bilibili.com/read/cv15635629)     
[^2] 《阳光少女》原著名为"こもれ陽の下で…"，直译为"在阳光下…"。该作品的其他译名有："Komorebi no Shita de"，"Komorebi no Moto de"，"Under the Dapple Shade"。  
[^3] 原文"ファミリー・プロット"是"Family Plot"的日文发音。国内多译为"家庭故事"。  

[HOME](../index.md)  
[NEXT](./komorebi.md)  