
## http://ckanno.hp.infoseek.co.jp/to_bbs.htm
it is unavailable.  
it is not backuped at web.archive.org.  


## https://hpmboard3.nifty.com/cgi-bin/bbs_by_date.cgi?user_id=YFA76534
[首页http://chiakik.la.coocan.jp](https://web.archive.org/web/20040801000000*/http://homepage3.nifty.com/chiakik/)的[BBS链接](https://web.archive.org/web/20000801000000*/http://artgallery.co.jp/glight/kanno/glight.cgi)。  

:: Chiaki's Guest Book ::

- [2007年的部分帖子](./2007.md)  
- [2006年的部分帖子](./2006.md)  
- [2005年的部分帖子](./2005.md)  
- [2004年的部分帖子](./2004.md)  
    - 福井县的北部被称为“岭北”，南部被称为“岭南”; 
    - 6月，网址迁至:[homepage3.nifty.com/chiakik/index.htm](https://web.archive.org/web/20040805025406/homepage3.nifty.com/chiakik/index.htm)
    - ODN的旧网站在6月30日关闭;
    - CH2里"北条老师亲自写的场景"是第114话的最后；详见[HTKJ制作日志2003年3月30日](../appendix/productionnote4.md#20030330)；
    - HTKJ专属BBS：[http://ckanno.hp.infoseek.co.jp/cgi-bin/sr2_bbs/sr2_bbs.cgi](http://ckanno.hp.infoseek.co.jp/cgi-bin/sr2_bbs/sr2_bbs.cgi)（译注：无法访问，web.archive.org上无备份。）
- [2003年的部分帖子](./2003.md)  
- [2002年的部分帖子](./2002.md)  
    - 要更改网页的URL;  
    - 大阪iblard展(粉丝线下聚会)
- [2001年的部分帖子](./2001.md)  
    - 5月15日发行的Comic Bunch；
    - 池袋展的报告，报告的终点是北条司；
    - 对AH的看法；
    - 高知美食，皿鉢料理；
    - 旧网址:  
        - nakaharamigiwa.tripod.co.jp  
        - www2.odn.ne.jp/iblard/chiaki/comic/comic.htm   
        - www2.odn.ne.jp/iblard/chiaki/index.htm   
- [2000年的部分帖子](./2000.md)  


## http://isweb40.infoseek.co.jp/computer/ckanno/to_bbs.htm
[http://isweb40.infoseek.co.jp:80/computer/ckanno at web.archive.org](https://web.archive.org/web/20020630000000*/http://isweb40.infoseek.co.jp:80/computer/ckanno)

ckannoのHomepage

ckannoのHomepageへようこそ。  
欢迎访问ckanno网站。  

あなたは、人目の訪問者です。  
你是别人眼中的访客。

このページは……  
这一页……

ネットワークＲＰＧ、UltimaOnlineについてのＨＰです。  
是关于网络RPG, UltimaOnline的HP。
