【はみ出しコラム】

# 读卖电视动画30年史!

「读卖电视动画 30 年历史！～Giant Yamato is Bakabon Lupin～」是1988年8月17日，在CITY HUNTER 2播出期间，作为读卖电视台「我喜欢动漫！」的人气系列节目。 该节目作为一个特别节目播出。 它介绍了读卖电视台到那时为止制作的21部动画（有些是由NTV制作的），包括OP、ED、预告片和有关人员的秘密制作故事，时间为2小时。 虽然这是读卖电视台的一个广告节目，但内容与通常的「怀旧动画」类型的节目非常不同，动漫迷对这个节目相当满意。 能看到初代鲁邦的试验片是非常难得的。（译注：待校对）  
顺便说一下，我记得CH的制作人诹访先生也在企画者之列（录音带末尾的工作人员名单出了问题，我没能查到（T_T））。 我深信不疑。  

神谷明是主持人，「巨人之星」、「宇宙战舰大和号」、「鲁邦3世」角逐的是各自的制片人和配音演员古谷徹（星飛雄馬）、（已故）富山敬（古代進）和麻上洋子（森雪）。 当然，也有一个CITY HUNTER的角落。   


对CITY HUNTER部分的简单介绍：嘉宾是麻上洋子配音的野上冴子。 麻上女士暂时担任了主持人的角色，首先接受了神谷先生的采访。  
对神谷来说，冴羽獠这个角色是他喜欢配音的角色，仿佛一直扮演的男性角色与北斗神拳和金肉人扮演的角色合二为一。 有人说他和神谷先生一模一样，但他觉得很难演好这个角色。   
在工作室里，只有神谷先生（饰獠）和伊倉小姐（饰香）是常客，这很孤独，但自从CH2之后，海坊主和冴子的出场次数增加，变得更加风光。   
神谷先生接着问麻上女士，当冴子说出她的性感台词时，她有什么感觉。 麻上女士回答说「我很高兴，我很高兴」。 在录制过程中，她说她的表演非常有热情。 （译注：待校对）  
我们还听到了国生小百合作为嘉宾出现的那一集（#52, 53）以及她对北条老师的印象，中间还播放了电视剧的OP和ED，让人感到非常愉快。 

<font color=#004d99>
（我想知道这个节目是否是北条先生在 JC 第 23 卷封面上发表评论的来源之一。）
</font>  