
source:  
https://www.crunchyroll.com/anime-news/2020/07/21-1/japanese-jump-fans-rank-which-80s-manga-series-theyd-like-to-see-rebooted-in-anime-form  
([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96827))  

## Japanese Jump Fans Rank Which 80s Manga Series They’d Like to See Rebooted in Anime Form  
日本Jump粉丝排名了那些想继续看到的80年代漫画

From Dragon Ball to Kimagure Orange Road, your favorite 80s Jump series are here  
从《龙珠》到《橙路》，你最喜欢的80年代Jump系列在这里。

Daryl Harding, July 21, 2020 12:16pm PDT (7/21/20)

Kimagure Orange Road  
《橙路》
 

People say that franchises created in Japan’s Weekly Shonen Jump stand the test of time, and when you have series like Dragon Ball, One Piece, and even JoJo’s Bizarre Adventure, those people are probably right.  
人们都说日本周刊《少年Jump》创造的系列动画经得起时间的考验，当你拥有《龙珠》、《ONE PIECE》甚至《乔乔的奇幻冒险》等系列时，这些人可能是对的。
 

With the recent trend of new anime series based on older series, Japanese survey site Voice Note, in conjunction with FutabaNet asked 200 male and female Japanese Jump fans aged between 30 and 50 what “series you’d like to see animated again” from the pages of Shonen Jump in the 1980s and this is their Top 10:  
最近有一种根据老系列改编新动画的趋势，日本调查网站Voice Note联合FutabaNet向200名年龄在30岁到50岁之间的日本Jump男女粉丝询问了上世纪80年代周刊《少年Jump》版面上的 "你希望看到哪些系列再次被动画化"，这是他们的Top 10。
 

10. Tsuide ni Tonchinkan by Koichi Endo  
远藤浩一的 "辻井井口馆"。
 

9. Kimagure Orange Road by Izumi Matsumoto
松本泉的《木暮橙道》
 

8. High School! Kimengumi by Motoei Shinzawa
高中! Kimengumi by Motoei Shinzawa
 

7. Saint Seiya by Masami Kurumada (though the series has been continuing on Netflix with Knights of the Zodiac: Saint Seiya)
车田正美所著的《圣斗士》（不过这个系列一直在Netflix上延续着《十二生肖骑士：圣斗士》）
 

6. Cat's Eye by Tsukasa Hojo
《猫眼》 作者：北条司
 

5. Kinnikuman by Yoshinori Nakai and Takashi Shimada
Kinnikuman，作者：Yoshinori Nakai和Takashi Shimada；
 

4. Dragon Ball by Akira Toriyama
鸟山明的《龙珠》
 

3. City Hunter by Tsukasa Hojo (the latest anime film in the franchise, City Hunter the Movie: Shinjuku Private Eyes, opened in Japan 2019)  
《城市猎人》的作者是北条司（该系列的最新动画电影《城市猎人 大电影：新宿私人侦探》，2019年在日本上映）。
 
City Hunter  
城市猎人
 

2. Fist of the North Star written by Buronson and illustrated by Tetsuo Hara
由武论尊撰写、原哲夫插图的《北斗神拳》
 

Fist of the North Star  
北斗神拳
 

1. Dr. SLUMP by Akira Toriyama
鸟山明的《阿拉蕾》
 

Dr. SLUMP  
阿拉蕾  
 

It’s no surprise that the father of modern shonen manga is up on the list twice, especially with Dr. SLUMP which is seeing a renewed interest due to Arale appearing in Dragon Ball Super and on a JR East train campaign across Tokyo. 
现代短篇漫画之父两次上榜并不奇怪，尤其是《阿拉蕾》由于阿拉蕾出现在《龙珠超》中，以及出现在JR东日本列车横跨东京的宣传活动中，让人重新关注。
 

But what series would like to see get a fresh anime reboot? Let us know down in the comments!
不过，有哪些系列想看的动画得到全新的重启呢？请在评论中告诉我们吧!
 

Source: [Futaba Net](https://futabanet.jp/articles/-/81612?page=1)
