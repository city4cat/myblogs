
(欢迎补充，欢迎指出错误)

# 北条司作品中的确定日期(The Big Days in the Works of Tsukasa Hojo)  
([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96850))  

**符号说明**：  

- "CE"是北条司作品"Cat's Eye"的缩写，该作品的其他名称有: キャッツアイ / 貓眼三姐妹(大陸) / 貓之眼(港) / 貓眼(台)  
- "CH"是北条司作品"City Hunter"的缩写，该作品的其他名称有: シティーハンター / 城市猎人(大陸) / 城市獵人(港/台) / 侠探寒羽良(大陆 盗版)  
- "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭(大陆) / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  
- 《こもれ陽の下で･･･》是北条司的作品，该作品的其他名称有: Komorebi No Moto De... / Under the Dapple Shade / Beneath the Dappled Shade / 阳光少女(大陆) / 艷陽少女(港)  
- "AH"是北条司作品"Angel Heart"的缩写，该作品的其他名称有: エンジェルハート / 天使之心(港) / 天使心(台)  

---------------------------------

- 1月  
    
    - 11日，(FC)柳叶雅彦的母亲的忌日(1986年)。柳叶雅彦遇见初恋若苗紫苑(1987年, 母亲的墓地)。 [3][4]  
    - 15日，(CH)麻生霞(Kasumi)生日(1969年)[1][6]。美树生日(年龄不详)[6]。   
    - 20日，(CH)美树(Miki)生日(1960年)。 [1]  
    




- 2月  
    - 20日，冴羽獠(Ryo Saeba)第一次开始向外界公布自己的生活(1985年)。[6]  


- 3月

    - (5日，北条司(Tsukasa Hojo)生日(1959年)。 [2])  
    - 19日，(FC)若苗紫苑(Shion Wakanae)生日(1981年)。 [3][4]  
    - 25日，(CH)槙村香挥出250T大锤(1990年)。[6]  
    - 26日，(CH)槙村香(Kaori Makumura)与冴羽獠(Ryo Saeba)第一次见面(1983年)[6]。槙村香决定将冴羽獠生日定为这一天(1989)，年龄定为30岁(1962~1964年生[8])[1][6]。   
    - 30日，(CH)槙村和人遇害，临终前将妹妹阿香托付于冴羽獠(1985年)。[6]  
    - 31日，(CH)槙村香(Kaori Makumura)生日(1965年[6][8]或1967年[1]生)。CH成立(1985)[6]。冴羽獠与海怪在墓园决斗(1990年)[6]。   



- 4月



- 5月

    - 5日，(FC)浅冈叶子(Yoko Asaoka)生日(1978年)(推测)。[3][4]  
    - 12日，(AH2)槙村香忌日(2001年[6])。(AH2-Volume14-Page087)  



- 6月  
    - 5日，(CE)内海俊夫向来生瞳求婚(他向来生家提婚)(1983年)。[5]  


- 7月

    - 20日，(FC)若苗空的妹妹菊地顺子(Yoriko Kikuchi)过文定日。[3][4]  
    - 27日，(FC)柳叶雅彦表白若苗紫苑(1999年, 桂浜海滩)。[3][4]  


- 8月
    - 24日，(CH)警视厅拨乱反正，丽香恢复名誉(1987年)。[6]  


- 9月



- 10月

    - 21日，(FC)柳叶雅彦(Masahiko Yanagiba)生日(1978年)。[3]  
    - 23日，(CE)来生瞳生日(1959年)。[5]  
    - 26日，(CH)《百万美元阴谋》上映[6]。

- 11月



- 12月



### 参考资料  

1. [北条司作品中人物的生日](http://www.hojocn.com/bbs/viewthread.php?tid=351)  
2. [北条司中文简介 By CatNj v4.0 updated](http://www.hojocn.com/bbs/viewthread.php?tid=3094)  
3. [FC时间线](https://gitlab.com/city4cat/myblogs/-/blob/master/hojocn/fc_information/fc_timeline.md)  
4. [FC的一些细节](https://gitlab.com/city4cat/myblogs/-/blob/master/hojocn/fc_details/readme.md)  
5. [《猫眼三姐妹》漫画部分7-10卷内容解析品读](https://g.nga.cn/read.php?tid=30102233)  
6. [城市猎人编年史](http://www.hojocn.com/bbs/viewthread.php?tid=1774)  
7. [小议《城市猎人编年史》和獠的年龄](http://www.hojocn.com/bbs/viewthread.php?tid=4991)  
8. [RYO年龄小考](http://www.hojocn.com/bbs/viewthread.php?tid=4114)  

[1]: <http://www.hojocn.com/bbs/viewthread.php?tid=351> (北条司作品中人物的生日)  
[2]: <http://www.hojocn.com/bbs/viewthread.php?tid=3094> (北条司中文简介 By CatNj v4.0 updated)  
[3]: <https://gitlab.com/city4cat/myblogs/-/blob/master/hojocn/fc_information/fc_timeline.md>(FC时间线)  
[4]: <https://gitlab.com/city4cat/myblogs/-/blob/master/hojocn/fc_details/readme.md> (FC的一些细节)  
[5]: <https://g.nga.cn/read.php?tid=30102233> (《猫眼三姐妹》漫画部分7-10卷内容解析品读)  
[6]: <http://www.hojocn.com/bbs/viewthread.php?tid=1774> (城市猎人编年史)  
[7]: <http://www.hojocn.com/bbs/viewthread.php?tid=4991> (小议《城市猎人编年史》和獠的年龄)  
[8]: <http://www.hojocn.com/bbs/viewthread.php?tid=4114> (RYO年龄小考)  

--------------------------------------------------

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

