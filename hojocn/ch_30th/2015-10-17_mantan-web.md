source:  
https://mantan-web.jp/article/20151017dog00m200007000c.html  


# シティーハンター：30周年記念で原画展　ミニクーパーや100トンハンマーも
City Hunter:30周年纪念原画展 MiniCooper和100tHammer  

2015年10月17日

[![「シティーハンター30周年記念　北条司原画展」初日に来場した上川隆也さん](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/001_size8.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=001)  


「シティーハンター30周年記念　北条司原画展」初日に来場した上川隆也さん
“城市猎人30周年纪念 北条司原画展”第一天到场的上川隆也  

[![](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/001_thumb.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=001)
[![](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/002_thumb.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=002)
[![](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/003_thumb.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=003)
[![](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/004_thumb.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=004)
[![](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/005_thumb.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=005)
[![](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/006_thumb.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=006)
[![](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/007_thumb.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=007)
[![](https://web.archive.org/web/20190518145431im_/https://storage.mantan-web.jp/images/2015/10/17/20151017dog00m200007000c/008_thumb.jpg)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=008)

[もっと見たい方はクリック！(全11枚)](https://web.archive.org/web/20190518145431/https://mantan-web.jp/photo/20151017dog00m200007000c.html?page=002)  
想看更多的点击!(共11张)  

　マンガ家の北条司さんの原画展「シティーハンター30周年記念 北条司原画展」が17日、新宿住友ビル（東京都新宿区）でスタートした。代表作「シティーハンター」と「エンジェル・ハート」の原画30点のほか、ファンにはおなじみの“100トンハンマー”や主人公・冴羽りょうの愛車・ミニクーパーの約4分の1サイズの立体造形が展示されている。関係者によると「北条さんの作品はこれまで複製原画の展示が行われたり、原画が数点のみ公開されたことはあるが、原画がこれだけ集まるのは珍しい」という。11月8日まで。  
漫画家北条司的原画展“城市猎人30周年纪念 北条司原画展”17日在新宿住友大厦(东京都新宿区)开幕。除了代表作《城市猎人》和《天使心》的30幅原画之外，还展示了粉丝们熟悉的"100吨Hammer"以及主人公冴羽獠的爱车MiniCooper约四分之一尺寸的立体造型。据相关人员说:“北条的作品在此之前曾进行过复制原画的展示，也只公开过几幅原画，但原画如此集中是很少见的。”截止日期为11月8日。

　「シティーハンター」は、週刊少年ジャンプ（集英社）で1985～91年に連載されたマンガ。東京・新宿で、ボディーガードや探偵などを請け負う冴羽りょうの活躍を描いたハードボイルドコメディーで、80～90年代にテレビアニメが放送されるなど人気を集めた。「エンジェル・ハート」は「シティーハンター」の世界観を基に、パラレルワールドとして描いたマンガで、ドラマが日本テレビ系で毎週日曜午後10時半から放送されている。  
《城市猎人》是1985~91年在周刊少年Jump(集英社)上连载的漫画。这是一部描写在东京新宿担任保镖、侦探等工作的冴羽獠活跃的硬汉喜剧，在80~90年代曾播出过电视动画，人气很高。《天使心》是以《城市猎人》的世界观为基础，将其描绘成一个平行世界的漫画。电视剧于每周日晚10点半在日本电视台播出。

　展示されているのは、「シティーハンター」「エンジェル・ハート」の原画がそれぞれ15点ずつで、りょうや槇村香など人気キャラクターが描かれている。また、ミニクーパーには作中でおなじみの100トンハンマーでたたかれた状態が立体化されている。  
此次展出的是《城市猎人》、《天使心》的原画各15幅，描绘了獠、槙村香等人气角色。另外，MiniCooper在作品中被熟悉的100吨锤子敲打的状态被立体化了。

　初日の17日は、ドラマ「エンジェル・ハート」（日本テレビ系）でりょうを演じる俳優の上川隆也さんが来場し、「北条さんは画力が高い方なのは存じていましたが、原画があるとすごみがありますね」と感想を語っていた。  
首日17日，在电视剧《天使心》(日本电视台)中饰演亮的演员上川隆也到场，并发表感想说:“我知道北条先生的绘画能力很高，但如果有原画的话，还是很厉害的。”。  
