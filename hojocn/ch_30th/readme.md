
# City Hunter 30周年

## 原画展  

- [[2015-10-17] City Hunter:30周年纪念原画展 MiniCooper和100tHammer | mantan-web](./2015-10-17_mantan-web.md)  
- [[2015-10-17]《天使心》上川隆也，感谢冴羽獠一角的反响「大家都很高兴」 ](./2015-10-17_crank-in.net.md)  
- [[2015-10-22]『城市猎人』的粉丝们坐不住了……“北条司原画展”将持续到11月8日!  | scf-web](./2015-10-22_scf-web.md)  
- [[2015-11-08] City Hunter 30周年纪念 北条司原画展 | Livedoor](./2015-11-08_blog.livedoor.jp.md)  
- [[2015-11-30] City Hunter 30周年記念 北条司原画展 | Ameblo](./2015-11-30_ameblo.jp.md)  
- [北条司３０周年記念トリビュートピンナップ第３弾 | Youtube](https://www.youtube.com/watch?v=2TBXCWg3BF8)  
- [シティーハンター30周年記念｜北条司原画展｜2015年10月17〜11月8日までのイベントです｜CITY HUNTER | Youtube](https://www.youtube.com/watch?v=m5nCKK91CHg)  
- [上川隆也、『エンジェル・ハート』実写化で絶賛の声](http://www.crank-in.net/entertainment/news/39350)(链接无法访问？)  
- [『City Hunter』『エンジェル・ハート』イベントにて](http://www.crank-in.net/entertainment/news/40107/gallery/#26)(链接无法访问？)  

## 其他  
- [[zz]City Hunter 30周年 北条司老师30问](./30QA.md)  

---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown), [Convert HTML to Markdown](https://html-to-markdown.com/demo)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  
6. [Bing Translator](https://cn.bing.com/translator?ref=TThis&from=ja&to=zh-Hans&isTTRefreshQuery=1)  
7. [OCRSpace](https://ocr.space/)  
8. [www.hojo-tsukasa.com | Wayback Mechine](http://web.archive.org/web/20011129230020/http://www.hojo-tsukasa.com/)  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处