
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


说明：  

- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这2个分格/分镜编号分别为：01_003_0, 01_003_1。  
- 人物右脸对应图片左侧，人物左脸对应图片右侧。  
- 如果图片或格式有问题，可以访问[这个链接](readme.md)  



# FC的画风-颈部  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96884))  

## 结构，词义辨析  
rib: 肋骨    
clavicle：锁骨[^yd]  
collarbone：锁骨[^yd]  
scapula：肩胛骨[^yd]  
humerus：肱骨[^yd]  
hyoid：舌骨[^yd]  
sternum：胸骨[^yd]  
breastbone：胸骨[^yd]  
salivary glands: 唾液腺[^yd]  
Parotid：腮腺  
Submandibular：颌下  
clavicle (collarbone)：锁骨[^yd]  
scapula (shoulder blade)：肩胛骨[^yd]  
humerus：肱骨[^yd]  
hyoid bone：舌骨[^yd]  
sternum (breastbone)：胸骨[^yd]  

注:(?)表示该词条的翻译为本文作者的猜测、没有明确的出处。以下同。  

[^cd]: [Cambridge词典](https://dictionary.cambridge.org/zhs/词典/英语-汉语-简体/)  
[^li]: [Linguee词典](https://cn.linguee.com/中文-英语/)  
[^qq]: [腾讯翻译](https://fanyi.qq.com/)  
[^yd]: [有道翻译](https://fanyi.youdao.com/index.html)  
[^dl]: [DeepL翻译](https://www.deepl.com/)  
[^bi]: [Being翻译](https://cn.bing.com/translator)  
[^mw]: [Merriam-Webster词典](https://www.merriam-webster.com/dictionary/)  
[^tf]: [thefreedictionary](https://www.thefreedictionary.com/)  
[^imaios]: [英文IMAIOS搜索](https://www.imaios.com/en/imaios-search/(search_text)/)或[中文IMAIOS搜索](https://www.imaios.com/cn/imaios-search/(search_text)/)  
[^scid]: [SCIdict](http://www.scidict.org/items/lip%20vermilion.html)  
[^etym]: [etymonline](https://www.etymonline.com/cn/)  


## 如何描述颈部  
### 资料3[^3]中的术语  

1R  : 1st rib  
2R  : 2nd rib  
Cl  : Clavicle (collarbone)  
Sp  : Scapula (shoulder blade)  
Hm  : Humerus  
Hb  : Hyoid bone  
St  : Sternum (breastbone)  

![](img/Form-of-the-Head-and-Neck_neck-bones.jpg) 


C1-C7  : cervical (neck) vertebrae  
C8-C13 : thoracic vertebrae  

Scm  : Sternocleidomastoid  
Sm   : Strap muscles (infrahyoid muscles)  
Om   : Omohyoid  
Dg   : Digastric muscle  
Tr   : Trapezius  
Spb  : Scapula bone (shoulder blade)  
Clb  : Clavicle (collarbone)  

Scm  : Sternocleidomastoid  
Ls   : Levator scapulae  
Sc   : Scalene muscles group  
Tr   : Trapezius  
Spb  : Scapula bone (shoulder blade)  
Clb  : Clavicle (collarbone)  
Sm   : Strap muscles (infrahyoid muscles)  

Scm  : Sternocleidomastoid  
Spc  : Splenius capitis  
Ls   : Levator scapulae  
Tr   : Trapezius  
Om   : Omohyoid  
Clb  : Clavicle (collarbone)  
Spb  : Scapula (shoulder blade)  

Sc   : Scalene muscles  
Smg  : Submandibular gland (salivary gland)   
Dg   : Digastric muscle  
Hb   : Hyoid bone  
Thc  : Thyroid cartilage  
Sm   : Strap muscles (infrahyoid muscles)  
Pg   : Parotid gland (salivary gland)  

Snl  : Superior nuchal line  
Mp   : Mastoid process (temporal bone)  
Ea   : Epicranial aponeurosis (scalp)  
Eop  : External occipital protuberance  

Sh   : Suprahyoid neck  
H    : Hyoid bone  
Ih   : Infrarahyoid neck  
Na   : Neck attachment line  

  
SALIVARY GLANDS (p119 in [1])  
**Parotid** (**Pg**) and **Submandibular** (**Sg**) glands also take part in forming connection of the head and neck by softening the transition between the neck muscles and the mandible  


性别差异：  
![](img/Form-of-the-Head-and-Neck_neck-form-diff.jpg)   
**Male neck** general shape is roughly cylindrical in shape,  
**Female necks** tend to be more conical  


Male:  
The **sternocleidomastoid muscle** (**Scm**) passes obliquely across the neck, from the **sternum** (**S**) and **clavicle** below, to the **mastoid process** (**Mp**) and superior nuchal line above and divides the neck into two large triangles.
The triangular space in front of the **Scm** is called the **anterior triangle** (**A-Triangle**) of the neck; and laterally behind it, the **lateral triangle** of the neck (**L-Triangle**).  
![](img/Form-of-the-Head-and-Neck_male-neck1.jpg) 
![](img/Form-of-the-Head-and-Neck_male-neck2.jpg)  
![](img/Form-of-the-Head-and-Neck_male-neck0.jpg)  

Female:  
The **sternocleidomastoid muscle** (**Scm**) divides the neck into two large triangles.
The triangular space in front of the **Scm** is called the **anterior triangle** (**A-Triangle**) of the neck; and laterally behind it, the **lateral triangle** of the neck (**L-Triangle**).  
![](img/Form-of-the-Head-and-Neck_female-neck0.jpg)  

**Female necklace lines (Fnl)**  
Also known as horizontal neck wrinkles and they can appear on female necks at any age. Some amount of Fnl, usually 2–3, are inevitable. If a woman or young girl has more of these lines, it doesn’t mean she is spending more time on the phone. People with lighter skin tend to be more susceptible to environmental aging and develop Fnl at an earlier age than those with darker skin types. Black women have the fewest Fnl and other wrinkle scores.

  

运动：(p140 in [1])   
If the head is rotated, the **lateral triangle** (**Lt**) of the neck on the turning side gets squeezed between the **Scm** and **trapezius**.  
![](img/Form-of-the-Head-and-Neck_rotate-scm.jpg) 

The **7th cervical** (**C7**) vertebra is the largest and most inferior vertebra in the neck region. Unlike the other cervical vertebrae, the **C7** has a large spinous process that protrudes posteriorly toward the skin at the back of the neck. This spinous process can be easily seen and felt at the base of the neck, making it a prominent landmark of the skeleton and giving the **C7** the name vertebra prominens.


## 角色颈部的特点   
   

## 部分角色的颈部
### [(侧面)喉结(Adam's apple)](./adams-apple.md)

### 颈部至背部的曲线  
下图所示的颈部到背部的曲线, 我觉得很美，不知道为什么：  
![](img/06_015_5__mod.jpg) 
![](img/09_048_1__mod.jpg)  


### 下颌投影在脖子处的阴影  
不同的表现方式：  

- 不同形状（应该和光源有关）：圆弧形，漏斗形，不规则形。  
![](img/01_150_7__mod.jpg) ![](img/01_150_7__crop0.jpg)  
![](img/01_161_2__mod.jpg) ![](img/01_161_2__crop0.jpg)  
![](img/02_053_2__mod.jpg) ![](img/02_053_2__crop0.jpg)  
![](img/02_119_6__mod.jpg) ![](img/02_119_6__crop0.jpg)  
![](img/02_135_2__mod.jpg) ![](img/02_135_2__crop0.jpg)  
![](img/08_176_6__mod.jpg) ![](img/08_176_6__crop0.jpg)  
![](img/09_045_5__mod.jpg) ![](img/09_045_5__crop0.jpg)  

- 排线方式不同(平铺、交错、旋转)、排线方向不同：  
![](img/01_150_7__mod.jpg) ![](img/01_150_7__crop0.jpg)  
![](img/01_161_2__mod.jpg) ![](img/01_161_2__crop0.jpg)  
![](img/09_045_5__mod.jpg) ![](img/09_045_5__crop0.jpg)  

- 以上用排线表示阴影。还可以用网点、"排线+网点"表现阴影：  
![](img/08_176_6__mod.jpg) 
![](img/07_188_0__mod.jpg)  

- 同一镜头里，每个角色该处的阴影可以近似(漏斗形)，也可以不同：  
![](img/07_097_0__mod.jpg) 
![](img/04_139_5__mod.jpg)  

- "下颌与脖子交界处"与“阴影区域”之间不连接：  
![](img/03_187_3__mod.jpg) ![](img/03_187_3__crop0.jpg) 


### 颈部转动
- 04_063_0, 转脖子，符合实际情况[1]：  
![](img/04_063_0__mod.jpg)  
![](img/Form.of.Head.and.Neck.2021.p142.jpg) 
![](img/Form.of.Head.and.Neck.2021.p148.jpg) 


## 参考资料  
[1]: Form of Head and Neck, Uldis Zarins, 2021.  
[^1]: Form of Head and Neck, Uldis Zarins, 2021.  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



