
##  Skills for Asking a General Question

- Be specific with your request. Give strict parameters for your query.   
For example, instead of asking, “Tell me about climate change”, you could ask, “What are the primary causes of climate change, and what are some potential solutions to mitigate its effects?”

- Provide context and background information  
For instance, if you want to discuss a specific book, providing the title, author, and a brief synopsis can help the AI generate more meaningful and targeted responses.

- Use explicit constraints and guidelines  
For example, if you need a summary of an article, you could say, “Please provide a summary of the following article in 100-150 words, focusing on the main points and conclusions.”

- Experiment with various phrasings and approaches  
For example, if asking a question doesn’t yield the desired response, you could try framing it as a statement or instruction, like “Explain the process of photosynthesis in simple terms” instead of “What is photosynthesis?”

- Tell ChatGPT what you DON’T want.  
It’s a great way to ensure that the tool excludes results that you’re just not interested in.  

- Keep your own language objective.  
Avoid loaded language and influencing the tool too much.

- Give ChatGPT examples of what you’re looking for.  
For example, if you’re creating some text, tell it what style you’d like it in, or if you’d like it to emulate a specific author.

- Give ChatGPT a role to play.  
Sometimes you’ll get better results if you ask ChatGPT to give them in the style of a specific person, such as a professor or expert in the field.  

- Be polite. This might sound odd, but there has been evidence to suggest that using kind language, and peppering your requests with ‘please’ and ‘thank you’ yields better results. It has to be worth a shot, right?


- Summerize the answer as a meta description.  


## Skill for Asking a Research Question

- Brainstorm. For example:  
>I have to write a research paper on "Monumental technological inventions that caused pivotal changes in history." It needs to be ten pages long and source five different primary sources. Can you help me think of a specific topic? 

- Generate an outline  
>Can you give me an outline for a research paper that is ten pages long and needs to use five primary sources on this topic, "The Printing Press and the Spread of Knowledge"? 

- Tell ChatGPT your topic and ask for sources  
>Can you give me sources for a ten-page long paper on this topic, "The Printing Press and the Spread of Knowledge"?

- Describe a specific idea and ask for sources  
>Can you give me sources for the social and intellectual climate of when the printing press was generated?

- Ask for examples of a specific incident  
>What was a time in history when implementing technology backfired on society and had negative impacts?

- Generate citations  
>Please provide sources for the previous answer.  
Please provide URL sources.  
Please provide me with reputable sources to support my argument on (whatever the topic is you're looking at)  
Please recommend peer-reviewed journals that discuss (and here, repeat what you discussed earlier in your conversation)  
Please provide me with sources published within the past five years  
Please provide sources published from 2019 through 2021.  



## about the references
don’t be afraid  to ask ChatGPT what studies and articles it’s using to draw its conclusions. 
don’t hesitate to (politely) tell ChatGPT how and why the answer missed the mark. It helps you hone the context of your query 


## Template For Querying the Chatbot 
Japanese manga artist Tsukasa Hojo is a great artist.  

I want you to act as a professional comic critic, so you must tell the truth and be as accurate as possible. 

You are going to do the following tasks:  
-Answer the question:  
Why is the female character's face in his works so attractive?  

-Show me the reputable sources of your answer, such as academic papers, articles or URLs.  





## References
- https://gptbot.io/master-chatgpt-prompting-techniques-guide/  
- https://www.zdnet.com/article/how-to-use-chatgpt-to-do-research-for-papers-presentations-studies-and-more/