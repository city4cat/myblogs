
[Questions](./questions.md) about the works of Tsukasa Hojo.    

Answers:  

- [Bard](./answer_bard.md)  
- [ChatGPT35](./answer_chatgpt35.md)  
- [ChatGPT40](./answer_chatgpt40.md)  
- [HuggingFace](./answer_huggingface.md)  
- [Monica](./answer_monica.md)  
- [Perplexity](./answer_perplexity.md)  
- [QianWen(千问)](./answer_qianwen.md)  

