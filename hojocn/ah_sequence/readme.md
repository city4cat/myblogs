
# AH的分镜  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96863))   

## 说明    
- "CE"是北条司作品"Cat's Eye"的缩写，该作品的其他名称有: キャッツアイ / 貓眼三姐妹(大陸) / 貓之眼(港) / 貓眼(台)  
- "CH"是北条司作品"City Hunter"的缩写，该作品的其他名称有: シティーハンター / 城市猎人(大陸) / 城市獵人(港/台) / 侠探寒羽良(大陆 盗版)  
- "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭(大陆) / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  
- 《こもれ陽の下で･･･》是北条司的作品，该作品的其他名称有: Komorebi No Moto De... / Under the Dapple Shade / Beneath the Dappled Shade / 阳光少女(大陆) / 艷陽少女(港)  
- "AH"是北条司作品"Angel Heart"的缩写，该作品的其他名称有: エンジェルハート / 天使之心(港) / 天使心(台)  
- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
- 如有错误，请指出，非常感谢！  


如果说北条司在CH、FC里的分镜已经炉火纯青，那么AH里的分镜可以说是天马行空。由于AH两季共49卷，可以说AH为分镜爱好者提供了一个近乎无限的资源宝库！  

----------------------------------------------------------

- **Vol09	CH093	page077		比恋爱更深的感情**  		 

    - 獠在夏目芳树的个展看到了香的画像。这部分剧情的镜头统计：  
        [fc_shot_emotion_vol14_ch102.pdf](https://gitlab.com/city4cat/myblogs/raw/master/hojocn/ah_sequence/shot_emotion/ah_shot_emotion_vol09_ch093.pdf)  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



