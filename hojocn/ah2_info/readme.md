# Angel Heart (2nd Season)的相关信息 

Angel Heart (1st Season)完结于第363话。Angel Heart (2nd Season)与Angel Heart (1st Season)的故事是连续的，所以Angel Heart (2nd Season)第1话相当于Angel Heart (1st Season)的第364话。  

## [Subtitles](./subtitles.md)  


## 轶事
以下数据源自：https://zh.wikipedia.org/zh-hans/天使心_(北條司)

- 《天使心》漫画第二部在结集成单行本时，将在月刊 Comic Zenon 时的倒数第二回(第79回“凶弹的行踪”)及最终回(第80回 “Forever, Angel Heart!!”)合二为一，并将标题改为“Angel City”。所以如果根据单行本回数计，《天使心》全篇漫画为442回（第一部363回+第二部79回）；而实际上连载回数是443回。（番外篇“一夜的友情”及将第109回大幅加笔的特别篇“獠的求婚”不包括在内。）

## Links
https://ocr.space/  
