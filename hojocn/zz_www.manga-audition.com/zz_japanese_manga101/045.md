source: 
https://www.manga-audition.com/manga-creation-with-pro-editor-3-japanese-manga-101-045/

# Manga creation with Pro editor 3- Japanese Manga 101 #045

Taiyo Nakashima 23/08/2016 6 min read

https://www.youtube.com/embed/8pO_f5uvlGI

Hey guys! This is the last of this series of simulating how a pro manga creator and an editor would work in a meeting!  
[![](./img/jm101_45_01.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/08/jm101_45_01.png)   

To close the series, we will now use “Communication Tool” as the Main, CORE theme, and create an actual storyline.  

> <**STORY**\> Protagonist A, often talks on the phone with Friend B, who studies abroad. They’ve been good friends. Friend B is enjoying his stay abroad so much, sometimes he doesn’t pick up calls from A. Back in the day, when A was in trouble, B was always there to cover his back. A felt a little lonely, but he accepted the change. It’s just the way things go. Then the town A lives in, is struck by a big earthquake. Luckily, A survived the quake, but he was forced to live in hardships, of temporary housing. Then A’s mobile phone, now covered in dust and mud, rings. It’s from B. Not wanting to make B worried, A acts tough and says, everything is okay and there’s nothing to worry about. While still on the phone, someone taps on A’s shoulder. A looks back while on the phone to B. There’s a man, covered in mud and with a backpack full of food and water, with big smile on his face. It’s the friend B. He returned from his studies abroad. Public transport was not available so he walked back to their hometown on his own feet. A seems nothing like he said on the phone. He looks completely exhausted. B looks awful too. He is completely covered in mud, from his walk back to town. A and B, feels the friendship. They both hang up the phone, and strongly hug each other in absolute joy.  

We stick with the basics. Characters A and B are “Best friends”. Place them far apart – Hometown and Abroad. The communication tool, is a mobile phone, again a simple everyday item.[  

[![](./img/jm101_45_02.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/08/jm101_45_02.png)   
Just those, won’t make the story moving. So we inject some “Negative Factors”, to the otherwise good relationship between A and B. “B not picking up calls” and “Big Earthquake” By injecting “Negative Factors”, like hardships and obstacles, the emotion of A is affected, and this makes the story moving. And the climax, A confirming the friendship with partner B, is easier to create. When we have an outline like this, we try to improve it further. Like : “The protagonist A is a bit passive, let’s make him go save friend B in trouble”. “The disaster causes disruption in the phone network, we should bring some other tool for communication”. And we keep on trying to brush-up, making the story even better! That really was a basic outline, but once you organize ideas like this, it really helps you to understand, what messages, you wanted to get across to the readers. Once you have that understanding, of the message, now all you need is to keep on asking yourself, “Will the readers like this?”, “Is this, clearly illustrated and understandable?” “Will they be moved?” That is how, you create your own, unique manga and styles. The way we just demonstrated, is just one of many ways. We may not always be right. Our master judge Tsukasa Hojo sensei, said the greatest joy of creating Manga is how one re-invents oneself, with unique ways of coming up with ideas, creating characters and stories, then directing the scenes, and drawing.   

We hope our channel helps you, to create your own, original silent manga stories. We are sincerely looking forward to reading your manga! Please do your BEST!  
