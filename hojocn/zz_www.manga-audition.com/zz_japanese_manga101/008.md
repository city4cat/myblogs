https://www.manga-audition.com/japanesemanga101_008/

# 5 Rules of character Imageboard, JapaneseManga101-#008

Taiyo Nakashima 05/03/2015 4 min read

[Video@Youtube](https://www.youtube.com/embed/2Ie_7OxqrMs)

Today, we are in Singapore!!


[![](./img/08-singapore.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/03/08-singapore.jpg)  
When we pros work on creating the character, first we talk and talk about the “elements” we’ve talked about. With those, we draw the “Image board” of that character. and We call this…

**The Character Chart**!  

To illustrate, we asked Ryuji Tsugihara sensei, who has over 30 years of drawing excellence, to create one for us! This is his character, _**"Japan’s most arrogant, yet caring, police detective, who loves martial arts"**_. Let’s take a look!  

[![](./img/08-imageboard1.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/03/08-imageboard1.jpg) 
[![](./img/08-imageboard2.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/03/08-imageboard2.jpg) 
[![](./img/08-imageboard3.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/03/08-imageboard3.jpg) 
[![](./img/08-imageboard4.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/03/08-imageboard4.jpg) 

As you draw, add finer details of his personalities. Like, “He speaks super loud”, “He loves starting troubles, or just taking part in one”. Or, how about “Because he loves martial arts, he never use the gun, he prefers physical combat!” As you draw him out with those details, you will see clearly, the charm points of that character. With those in mind, it will become so much easier, to come up with episodes, moments and scenes, which will then drive the story line.   To create the image board, follow these five simple rules:  

[![](./img/08-rules.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/03/08-rules.jpg)  

1.**Be clear about the character**: Age, personalty, occupation… Imagine it as a real person.  
2.Remember the “Six Faces of emotion” we talked? **Draw “Happiness” “Anger” “Sadness” “Hate” “Surprise” “Fear”** of that person.  
3.Then **draw him, from all angles**. Right in front, from the side, looking up, down… so on.  
4.**Draw the whole body**_._  
5.**Draw his actions, the small things** like “scratching his nose when he’s happy”, “how does he laugh, walk, shout, Jump…”. How will his body move? That defines him, as the person / actor?  

Japanese Manga artists, put MUCH effort into creating characters. Not just the hero, but nearly every one, of them. So they can act, in a way “actors and actresses” do.  

[![](./img/08-gotit.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/03/08-gotit-300x165.jpg)  
GOT IT!? We are looking forward to seeing great characters, coming from you! In the next episode, we’re gonna come back to the **“Art of Panneling”**! See you next time!  