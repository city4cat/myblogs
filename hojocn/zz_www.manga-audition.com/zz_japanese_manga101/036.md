source: 
https://www.manga-audition.com/japanesemanga101_036/

# Manga NAME Practicals 4 : “BONOLON the Forest Warrior” – Japanese Manga 101 #036

Taiyo Nakashima 28/09/2015 5 min read

https://www.youtube.com/embed/_8IfOL6-dc0

There’s only a few days left until the deadline of SILENT MANGA AUDITION 4! You can still make it! Please keep drawing until the last possible moment! We’re looking forward to seeing all your entries!! Today we’ll continue talking about “name” creation while looking at **Forest Warrior Bonolon**. The story is finally reaching its climax!  
  

[![](./img/40b9042071a91086c47ce15b0d77df2e.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/10/40b9042071a91086c47ce15b0d77df2e.png)   
The weapon merchant Zunga poisoned the well, but Bonolon taught him a lesson, and the following day, when the villagers drink the water, they quickly become healthy again. Through the villager’s happy expressions, the reader can clearly see that everything has turned out alright. The villagers also worry about Lemo, which once again shows their kindness. Lemo refuses to drink the water from the well to cure herself. This is a preparation for the miracle that happens near the end.  
 

[![](./img/52496f4e954a7235f25bc4426a84a646.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/10/52496f4e954a7235f25bc4426a84a646.png)   
This is the climax scene with Bonolon and Lemo. As per Lemo’s wish, Bonolon put an antidote into the well, and saved the villagers. With the last of her strength, Lemo thanks Bonolon. Bonolon asks why Lemo didn’t wish for her own illness to be cured. She says this:  

[![](./img/9f765cb5820d8c35d6bd1a8a2f377344.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/10/9f765cb5820d8c35d6bd1a8a2f377344.png)   
_“I don’t want to be alone. I want to meet my parents. If I die, I can see them in heaven, right? But before that, I wanted all the villagers to be healthy”_ Bonolon is touched by Lemo’s kindness. Lemo passes out in his hands.  

Through their facial expressions, Lemo’s bravery, and Bonolon’s desperation come across very clearly.  
  

[![](./img/7987f2067a4f4dad8f94a69335c8bec5.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/10/7987f2067a4f4dad8f94a69335c8bec5.png)   
In the next scene, Bonolon explodes into a fountain of tears. Bonolon’s crying is extremely over-the-top, and a pool of water forms in his hands. Pros often say that when drawing a tearful moment like this, **“e****ven if you overdo it like 120%, only about 70% gets through to the readers”**. So don’t be shy about making it, as dramatic as possible!  

[![](./img/4f3cf862cc9105c394dc8bd697d8530e.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/10/4f3cf862cc9105c394dc8bd697d8530e.png)   
GOT IT !?    And we have more!    

[![](./img/618af4ba4bf567f912e256e9bff1857c.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/10/618af4ba4bf567f912e256e9bff1857c.png)   
Those warm tears of Bonolon cause a miracle. They revive Lemo and cure her sickness. When Lemo asks _“Why?”_ the Giant Tree answers _“The tears of the Tazmun tribe_ _have the power to cause miracles. Bonolon’s tears have cured your sickness.”_ Bonolon’s miraculous tears… note that it isn’t Bonolon who explains this, but a third party character. Letting a third party character explain the hero’s actions is another is basic technique of manga creation. Finally, the giant tree says _“but there are very few crybabies like Bonolon_ _in the Tazmun tribe!”_ embarrassing Bonolon. This scene shows us that while Bonolon is a “Forest Warrior”, he’s also a very tenderhearted character. Big and strong, but also a kind crybaby. These contrasts are what make Bonolon so lovable as a character.    

[![](./img/f472b9d0cde4d0c545d12080fa699cb2.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/10/f472b9d0cde4d0c545d12080fa699cb2.png)   
GOT IT!? Next time we’ll finally be getting to the last scene. We’ll be showing you how to make the readers want to meet your characters again!
