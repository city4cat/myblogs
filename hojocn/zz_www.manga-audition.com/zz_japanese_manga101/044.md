source: 
https://www.manga-audition.com/japanesemanga101_044/

# MANGA PLANNING with a pro CLIMAX! – Japanese Manga 101 #044

Taiyo Nakashima 14/06/2015 6 min read

https://www.youtube.com/embed/XXxRaZCmOI4

We’ve been showing, how you can tackle SILENT MANGA AUDITION Round5. By simulating, how a pro Creator, and an Editor would work in a meeting. We are nearing the climax of this series! To close off the series, we are now using “Communication Tool” as the Main theme. Let’s see how we can come up with episodes, the building block of the storyline! When we editors and manga creators meet and discuss this, we’d probably start with the fundamental question : “What is a COMMUNICATION Tool”? Like this : “Communication Tool” means that, there are minimum of Two people. Protagonist A and the partner B, are trying to “Communicate”, something to one another. But if A and B, are close to each other, and they have no issues talking, then there is no need for a “Tool”. They can just talk, to communicate. Therefore, there are certain situations, in which a “Communication Tool” is a requirement. Like these:  

**“A and B, are not able to talk directly, to each other.” “Something one says, is not understood by the other”**  

We just stated the obvious, but once you have that organised as words, there are actually many opportunities for interesting stories. Let’s see:  

**“A and B, are not able to talk directly, to each other.”**  
・They are physically far apart  
・They have NOT MET yet  
・They are FORBIDDEN to meet in person  
・They keep missing each other.  

**“Something one says, is not understood by the other”**  
・Something one doesn’t want to say  
・Partner doesn’t want to listen  
・One is unable to talk  
・They speak different languages  

Already sounding good isn’t it? Now let’s dig deeper into the details.  

**“A and B, are not able to talk directly, to each other.”**  
・They are physically far apart:  Abroad / Another planet / Another dimension…  
・They have NOT MET yet: Pen-Pals / HAM Radio Friends / Mail Friends…  
・They are FORBIDDEN to meet in person: Rival / Enemies / Time Traveller…  
・They keep missing each other: Too busy / Wrong timing / Not being honest…  

**“Something one says, is not understood by the other”**  
・Something one doesn’t want to say: Shy / Idol / Shameful / Fear…  
・Partner doesn’t want to listen: Busy / Uninterested / Unnoticed / Hearing impaired…  
・One is unable to talk: Animal / Plantation / Machines / Ghosts…  
・They speak different languages: Foreigner / Enemies / Alien lifeform…  

There are many ideas, you can come up with, by methodically digging into the meanings around “Communication Tool.” You can already see some great ideas, or “seeds” of ideas, for episodes, storyline, characters and their relations, can’t you? Combine those ideas, and randomly collected ideas from the theme “Communication tool” like we did earlier on: We then begin to write a draft plot, story and episodes ideas. IMPORTANT THING to remember here, is to KEEP IT SIMPLE. Do NOT start with grand ideas or tiny details, and keep it to a minimum.   
GOT IT!? 

The Next episode, is the final in this series of drawing for SMA05! Make sure you come back, and keep on enjoying!  
