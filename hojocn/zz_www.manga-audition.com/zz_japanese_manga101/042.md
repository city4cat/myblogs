source: 
https://www.manga-audition.com/japanesemanga101_042/

# MANGA PLANNING THE PROFESSIONAL WAY! 2 – Japanese Manga 101 #042

Taiyo Nakashima 06/11/2015 6 min read

https://www.youtube.com/embed/WU7chHxOIjk

We’ll continue the “Simulated Meeting” that would happen between a manga creator, and a manga editor. This is a straight-to-the-point topic that will help you draw a better story for SMA05.   We’ll begin with a quick re-cap!  

[![](./img/c267e0d9ffa889b888fc34b6babbcdc6.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/11/c267e0d9ffa889b888fc34b6babbcdc6.png)  
**FRIENDSHIP and COMMUNICATION TOOL are the two themes of round 5. Combine BOTH themes, as a practical challenge, to create stories.** Last week we showed you how we’d approach story creation, like this… **THINK about the meanings, of the theme.**  

[![](./img/54a4f3ae199e87981d66d922bb3f21dd.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/11/54a4f3ae199e87981d66d922bb3f21dd.png)  
Write down all the random words that come to your mind, and use them as the “Seeds” of ideas. **Determine the MAIN THEME, that will be the CORE of the story.**  

[![](./img/0d3a7e5837c4a4f843d3e332cc5bae4a.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/11/0d3a7e5837c4a4f843d3e332cc5bae4a.png)  
Do NOT make BOTH the main theme, instead make one the main theme, and use the    other as a SPICE, to enhance the effect of the MAIN theme. The most important thing here is to keep asking yourself “How do we entertain the reader?” And this **MAIN THEME** is the very tool that we utilise to meet that objective. For example, _“If Friendship is the main theme, we use human drama to ignite the emotions of the readers”_ _“If Comm-Tool is the main theme, we use amazing ideas for a gadget to EXCITE the readers”_ and so on. You can already see the direction we are heading, from those statements.   And on to the next step! We’d then, work with the **CHARACTERS**. Their roles and placement, like this : To illustrate “Friendship”, there must be at least two characters. Friendship is an emotion that happens between two people,  

[![](./img/1ed16e8e77fd0cc5b764ffe6a97ed26d.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/11/1ed16e8e77fd0cc5b764ffe6a97ed26d.png)  
So let’s say, we have protagonist A, and his partner B. Jot down random roles, making pairs of them as you write them down like this,  
  
[![](./img/c42bdea789bb6b8f03af4518af20dc36.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/11/c42bdea789bb6b8f03af4518af20dc36.png)  
Then imagine what episodes of “Friendship” could happen between these roles. But, looking at this list, manga creators and editors would immediately know that by using only these, you can only really make an “Ordinary” tale. To entertain the readers, we need to add one more element to the mix. That magic ingredient, ladies and gentlemen, is the **“Element of Surprise”!** To add the surprise, into character B, we’d have a list like this.  

[![](./img/5592716b30d9f7f5436eefb2303a5632.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/11/5592716b30d9f7f5436eefb2303a5632.png)  
_Me and the Bully,  a baseball pitcher and the opposing batter, a cop and a thief, a hunter and his dog, a soldier and an enemy, a football player and a referee, and so on._ These may not at first seem like likely combinations for friendship. **_But WHAT IF they become friends?_**  That’s the moment, we’d think “Hey, the readers might be interested in this!” So, IMAGINE, WHAT IF there were a friendship between these roles pairs, and **add the SEEDS of Ideas, the words we imagined earlier on.**   

[![](./img/c4f0471fdf4f0ec66d2d13f8eb1d8c99.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/11/c4f0471fdf4f0ec66d2d13f8eb1d8c99.png)  
NOW, it is much easier to imagine some kind of stories, or episodes, fragments of storyline that you can weave together into a full storyline. Work like this, to create a   FRIENDSHIP story CORE, then use Communication tool, as the SPICE, to spice up the core story! SEE, having set themes actually makes it easier to create stories!   

Next week, we’ll continue the simulated meeting, on how to SPICE up! the story. Using “Communication Tool”. Making the best of it! Every creator and editor has his or her unique ways of coming up with ideas. But IF, you are feeling a bit lost about how to make use of the themes, we strongly recommend that you to try the method we just showed you. We’re sure this will help you build a great story, and develop your own method!   
GOT IT!?  

[![](./img/68ca4f1a0cf0e7fba657e248e0d9171b.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/11/68ca4f1a0cf0e7fba657e248e0d9171b.png)  
See you next time!