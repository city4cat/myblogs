source: 
https://www.manga-audition.com/smas-italian-job/  

[![](./img/30223467_192560714870573_1877736116_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30223467_192560714870573_1877736116_o.jpg)    


# SMA’s Italian Job  

Enrico Croce, 14/04/2018, 4 min read  

Italy, the BEL PAESE, the birthplace of world renowned dishes such as Pizza, Pasta and the versatile Panini; the land of great poets like Dante and Boccaccio and not forgetting pioneers of the art world like Leonardo Da Vinci, Michelangelo and…Salvatore Nives from SMAC!!!  
意大利，BEL PAESE，世界著名美食的发源地，如披萨、意大利面和多才多艺的帕尼尼；伟大诗人但丁和薄伽丘的故乡，还有艺术界的先驱，如达芬奇、米开朗基罗和......来自SMAC的Salvatore Nives！!  


READ ATENA’S ONE SHOT  
阅读阿特纳的一鸣惊人  
 

Friends, Manga Creators, Community, lend me your ears! Enrico here!  
朋友们、漫画创作者们、社区们，请听我说! 我是Enrico!  

[![](./img/def-igp-decaux-romics-prim2018-3-1-maxw-654-1.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/def-igp-decaux-romics-prim2018-3-1-maxw-654-1.jpg)  

The organizers of one of the biggest Italian manga and anime event, ROMICS recently extended an invitation to our beloved Hojo Tsukasa sensei…an invitation Hojo sensei was honored to accept.  
意大利最大的漫画和动画活动之一Romics的主办方最近向我们敬爱的北条司老师发出了邀请......北条老师很荣幸地接受了邀请。  

[![](./img/29749613_1396737373792272_3113906666830488028_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29749613_1396737373792272_3113906666830488028_o.jpg) 
[![](./img/29872153_1396735563792453_3807728486306924230_o-1.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872153_1396735563792453_3807728486306924230_o-1.jpg) 
[![](./img/29872477_1396737287125614_6556160121111092854_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872477_1396737287125614_6556160121111092854_o.jpg) 
[![](./img/29872622_1396736840458992_1270893175676185105_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872622_1396736840458992_1270893175676185105_o.jpg) 
[![](./img/29873210_1396737557125587_2832235600174929139_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29873210_1396737557125587_2832235600174929139_o.jpg) 
[![](./img/29983070_1396736007125742_7443919834417214145_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29983070_1396736007125742_7443919834417214145_o.jpg) 
[![](./img/29983088_1396737380458938_1487819625150128812_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29983088_1396737380458938_1487819625150128812_o.jpg) 
[![](./img/29983324_1396736490459027_7352347448343097500_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29983324_1396736490459027_7352347448343097500_o.jpg) 

Nestled amongst the ancient monuments of antiquity, the event proved a huge success, with the organizers bestowing our legendary mangaka with the prestigious Romics d’Oro – Litt. Golden Romics Award in gratitude for a career that captured reader’s hearts the world over (especially Italian hearts!).  
在古老的古迹中，活动取得了巨大的成功，主办方授予我们的传奇漫画家以著名的Romics d'Oro - Litt. 金Romics奖，以感谢他在职业生涯中抓住了世界各地读者的心（特别是意大利的读者的心！）。  

[![](./img/29750126_1398179446981398_5028957963559501827_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29750126_1398179446981398_5028957963559501827_o.jpg) 
[![](./img/29750205_1398182623647747_7693679451849711114_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29750205_1398182623647747_7693679451849711114_o.jpg) 
[![](./img/29871643_1398180426981300_2506036074472405737_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29871643_1398180426981300_2506036074472405737_o.jpg) 
[![](./img/29871981_1398179440314732_8142894540805466897_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29871981_1398179440314732_8142894540805466897_o.jpg) 
[![](./img/30052291_1398182400314436_1197419219181256443_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30052291_1398182400314436_1197419219181256443_o.jpg) 
[![](./img/30052362_1398180596981283_1930974627781360382_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30052362_1398180596981283_1930974627781360382_o.jpg) 
[![](./img/30171364_1398181863647823_5352101506127296810_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30171364_1398181863647823_5352101506127296810_o.jpg) 
[![](./img/30420495_1398180043648005_8831935983739468695_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30420495_1398180043648005_8831935983739468695_o.jpg) 


The event also proved an excellent opportunity to extend the message of the SILENT MANGA AUDITION®, and our mission to grow our Community of International Manga creators!  
这次活动也是一个很好的机会，让我们可以将 "默白漫画大赛®"的信息、以及我们发展国际漫画创作者社区的使命推广出去！  

From launching a special Italian language page on the SMAC! Web Magazine, containing all the news and information about the Audition, to enthusiastically talking to every passerby about what we do, we are confident the people Rome are now very familiar with SMAC!  
从在SMAC!网络杂志上开设意大利语专页(其中刊登所有关于选拔赛的新闻和信息)，再到热情地向每一位路人介绍我们的工作，我们相信罗马人现在已经非常熟悉SMAC!了。  

[![](https://www.manga-audition.com/wp-content/uploads/2018/04/Screen-Shot-2018-04-12-at-17.27.14-768x447.png)](https://www.manga-audition.com/it/)

To help publicize the appearance of Hojo Tsukasa sensei, we designed a bespoke flyer featuring the temptresses from Cat’s Eyes, especially for the Romics event!  
为了宣传北条司老师的出场，我们特别为Romics活动设计了以《猫眼》中的充满诱惑的女子为主题的定制传单!  

[![](./img/Screen-Shot-2018-04-13-at-13_003.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/Screen-Shot-2018-04-13-at-13.27.53-480x336.png)  

These “Eye Cat-ching” flyers were also featured on the stand’s of our friends [Jungle Entertainment Hobby Shop](http://jungle-scs.co.jp/sale_en/) and [Panini Comics](http://www.paninicomics.it/).
这些吸引人的"猫眼"传单也出现在我们的朋友Jungle Entertainment Hobby Shop和Panini Comics的展台上。  

Both Jungle and Panini created some amazing event exclusive products to celebrate the life and work of Hojo sensei, paying particular attention to Cat’s Eye and CITY HUNTER!  
Jungle和Panini都制作了一些令人惊艳的活动专属产品，以庆祝北条老师的生活和工作，特别是猫眼和CITY HUNTER!  

[![](./img/Screen-Shot-2018-04-13-at-13_002.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/Screen-Shot-2018-04-13-at-13.36.13-768x253.png)  
http://jungle-scs.co.jp/  
Jungle X Tsukasa Hojo & SMAC!   
Jungle X 北条司 & SMAC!
 
[![](./img/Screen-Shot-2018-04-13-at-13_004.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/Screen-Shot-2018-04-13-at-13.35.32-768x278.png)  
http://www.paninicomics.it/  
Panini Comics X Tsukasa Hojo & SMAC!  
Panini Comics X 北条司 & SMAC!

We were delighted to meet many people interested in the competition, making sure they were fully up to speed on information regarding future round application, not to mention thrusting a flyer in their unsuspecting hands!  
我们很高兴见到了许多对比赛感兴趣的人，确保他们完全掌握了未来一轮申请的信息，更不用说将一张张传单插入他们毫无戒备的手中了!  

We also announced our event to eager audiences during two stage appearances, including Hojo sensei’s talk show session, and a talk with the Europe Design Institute ([IED](![](https://www.ied.edu/)) during a Manga Master lecture delivered by Hojo sensei to a packed audience of Design, Animation, CG and Fashion students!  
此外，我们还在两个舞台上向热心的观众宣布了我们的活动，包括北条老师的脱口秀环节，以及与欧洲设计学院(IED)合作的，由北条老师向设计、动画、CG和时装专业的学生进行的漫画大师讲座中的演讲!  

[![](./img/Screen-Shot-2018-04-13-at-13.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/Screen-Shot-2018-04-13-at-13.51.48-768x257.png) 
[![](./img/29749907_1397863313679678_2280129294926259202_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29749907_1397863313679678_2280129294926259202_o.jpg) 
[![](./img/29750001_1397866520346024_7912873013565531655_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29750001_1397866520346024_7912873013565531655_o.jpg) 
[![](./img/29872227_1397863393679670_2042690133794084129_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872227_1397863393679670_2042690133794084129_o.jpg) 
[![](./img/29872474_1397862753679734_7972309235120856475_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872474_1397862753679734_7972309235120856475_o.jpg) 
[![](./img/29872693_1397866450346031_9106834996185572209_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872693_1397866450346031_9106834996185572209_o.jpg) 
[![](./img/29872932_1397862210346455_3332683312980660178_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872932_1397862210346455_3332683312980660178_o.jpg) 
[![](./img/29873183_1397862487013094_3824239504036168191_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29873183_1397862487013094_3824239504036168191_o.jpg) 
[![](./img/29873476_1397864267012916_3322559463648705012_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29873476_1397864267012916_3322559463648705012_o.jpg) 
[![](./img/30072887_1397864093679600_5620722664523318494_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30072887_1397864093679600_5620722664523318494_o.jpg) 
[![](./img/30072969_1397866587012684_5829494188294358885_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30072969_1397866587012684_5829494188294358885_o.jpg) 
[![](./img/30073461_1397862667013076_6784811145077201215_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30073461_1397862667013076_6784811145077201215_o.jpg) 
[![](./img/30167883_1397864100346266_983348349743616434_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30167883_1397864100346266_983348349743616434_o.jpg) 
[![](./img/30171194_1397862280346448_1064398315890858553_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30171194_1397862280346448_1064398315890858553_o.jpg) 
[![](./img/30420519_1397863973679612_4415511272751313004_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30420519_1397863973679612_4415511272751313004_o.jpg)  

[![](./img/29872006_1399225370210139_8055139016858683987_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872006_1399225370210139_8055139016858683987_o.jpg) 
[![](./img/29872236_1399225443543465_3423556755744428353_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872236_1399225443543465_3423556755744428353_o.jpg) 
[![](./img/29983169_1399225373543472_2640895153916765133_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29983169_1399225373543472_2640895153916765133_o.jpg) 
[![](./img/30167787_1399225376876805_8271593499769296055_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30167787_1399225376876805_8271593499769296055_o.jpg) 
[![](./img/29872142_1399225450210131_6071404385799748630_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/29872142_1399225450210131_6071404385799748630_o.jpg)  

We received an outstanding response from the public during the entire event, offering the chance to support many Italian creators, old and new, over SNS in the future.  
在整个活动中，我们得到了公众的热烈反响，提供了今后通过SNS支持众多意大利新老创作者的机会。  

**But what now…?**  
但现在呢...？  

We fully intend to build on this success and offer the chance for Italian creators to message us with their queries, join in on live video support and give one-to-one support, all in Italian!! We will also continue to work with our Italian Masterclass Members in supporting them with One-shot creation (**one particularly exciting One-shot is almost finished, stay tuned for its unveiling!**) and also join Italian manga workshops to inform participants of professional manga editing procedures!  
我们完全打算在这个成功的基础上再接再厉，为意大利的创作者们提供机会，让他们可以给我们留言，提出他们的疑问，加入现场视频支持，并给予一对一的支持，所有这些都是用意大利语进行的！！！！。我们还将继续与意大利大师班成员合作，支持他们进行单镜头创作（**其中一个特别激动人心的单镜头即将完成，敬请期待它的揭幕！**），还将参加意大利漫画工作坊，向学员传授专业的漫画编辑程序。  

[![](./img/30709202_1384367475043039_2015072765496786944_n.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/30709202_1384367475043039_2015072765496786944_n-768x433.jpg)    
http://www.scuolacomics.com/  
Scuola Internazionale di Comics X SMAC!  
国际漫画学校 X SMAC!

[![](./img/Screen-Shot-2018-04-13-at-14.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/04/Screen-Shot-2018-04-13-at-14.02.18-768x591.png)  
http://www.scuolacomix.net/  

Salvatore Nives Workshop & SMAC! in manga school  
漫画学校的Salvatore Nives 工作坊 & SMAC!  
 

If you’d like your Manga school to collaborate with us, or you’re wondering how to join the Masterclass, or even information on applying to the next SMA round, please do not hesitate to contact me at [Enrico](https://www.facebook.com/profile.php?id=100017371275440) or via our [FACEBOOK](https://www.facebook.com/SilentMangaAudition/)/[TWITTER](https://twitter.com/SilentMangaComm) pages!  
如果你想让你的漫画学校与我们合作，或者你想知道如何加入大师班，甚至想知道如何申请下一轮SMA的信息，请不要犹豫，联系我Enrico或通过我们的FACEBOOK/TWITTER页面！所以，为什么不加入我们的BELLISSIMA社区？  

So why not join our **BELLISSIMA** Community?! We cannot wait to meet you!  
为什么不加入我们的BELLISSIMA社区呢？我们迫不及待地想见到你  

CIAO!  
再会！  
 

 

(All Hojo sensei pictures are taken with kind permission from [ROMICS OFFICIAL FACEBOOK PAGE](![](https://www.facebook.com/RomicsOfficial/))   
(所有北条老师的照片都是经过ROMICS官方FACEBOOK页面的许可的)  
