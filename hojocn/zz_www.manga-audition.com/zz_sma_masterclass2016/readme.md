source:  
https://www.manga-audition.com/mc2016-day-2-the-ceremony-guest-workshop/

[![](./img/mc2016_d2.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/mc2016_d2.jpg)  

# The SMA MASTERCLASS 2016 Special Report Day 2 : The Ceremony + Guest workshop!!!  

Taiyo Nakashima	Taiyo Nakashima 16/03/2016 7 min read  


 Today is the big day! When we introduce the new members to the elites. Can’t wait! The day begins with guest workshop with Hojo Tsukasa sensei. He joined us out of his busy schedule, to provide the new MASTER  CLASS with a workshop of Manga Panelling “Basics”.   

[![](./img/IMG_2383.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/IMG_2383-1280x960.jpg)  
The meeting room, in theatre mode!  

[![](./img/20160301_110217.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/20160301_110217.jpg)  
Real HOJO sensei!  

[![](./img/IMG_20160301_114714.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/IMG_20160301_114714.jpg)  
See the red squiggles? That’s “the Flow of the eyepoint”  

Sensei shared with us, many of his trade secrets! (Some of which even our long term editor didn’t know.) Exciting one hour workshop soon ends! It was so fun, felt like 15 mins. 😉 The workshop was exciting, MASTERCLASS 2016 goes for lunch @ Sora Zenon…. Lovely weather today! 

[![](./img/IMG_20160301_012244463.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/IMG_20160301_012244463.jpg)  
[IMG_2385.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/IMG_2385-1280x960.jpg)     
Meanwhile, at Kichijoji Dai-ichi hotel…  
[![](./img/FFG_2159.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2159-1280x853.jpg)  
Ceremony is being prepared!  

All three sensei, judges are already waiting in the other room… Yes it’s time for the MASTER CLASS Certification ceremony 2016!  
[![](./img/FFG_2160.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2160-1280x853.jpg)  
Let’s the ceremony begin!

All judges of SILENT MANGA AUDITION are here, and they are happy to see the chosen elites, new members of our manga creation team!  
[![](./img/FFG_2162.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2162-1280x853.jpg)  
Mr. Horie, looking happy to see future hopefuls 😉  

[![](./img/FFG_2168.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2168-1280x853.jpg)  
“WELCOME!”  

[![](./img/FFG_2169.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2169-1280x853.jpg)  
Maybe a bit nervous?
 
[![](./img/FFG_2192.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2192-1280x853.jpg)  
“Hi again, my students” It’s Tsukasa Hojo sensei again!  
 
[![](./img/FFG_2199.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2199-1280x853.jpg)  
Tetsuo Hara sensei, looking as tough as always!  
 
[![](./img/FFG_2221.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2221-1280x853.jpg)  
Ryuji Tsugihara sensei, sharing precious tips with everyone!  

After the speech, it’s time for everyone to be certified, one by one. ( Our special mentions goes to Inma.R from Spain. Even though she couldn’t make it to Japan on this occasion, you are now a proud member of the SMA Master Class! )  
[![](./img/FFG_2174.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2174-1280x851.jpg)    
“You are now certified as a proud new member of the SMA MASTERCLASS!”  

[![](./img/FFG_2196.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2196-1280x853.jpg)  
Everybody cheers 😉  

[![](./img/FFG_2238.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2238-1280x854.jpg)  
Behold the COAMIX Manga creatives 2016!  

Following the ceremony, is our afternoon tea with the sensei! All masterclass can ask our judges, anything and everything 😉   
[![](./img/FFG_2244.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2244-1280x854.jpg)   
An opportunity like don’t come very often. Make it count 😉  
[![](./img/FFG_2252.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2252-1280x854.jpg)  
Sensei Hojo and the manga creators of the round table!

[![](./img/FFG_2255.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2255-1280x854.jpg)  
Everybody maxing out the opportunity!
 
[![](./img/FFG_2305.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2305-1280x853.jpg)  
The reading art of the fist of Hara sensei!  

[![](./img/FFG_2271.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2271-1280x854.jpg)  
“NOT TO MISS A WORD!”

[![](./img/FFG_2292.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2292-1280x853.jpg)  
The editors join the Tsugihara sensei’s table!
   
[![](./img/FFG_2295.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/FFG_2295-1280x853.jpg)  
“Now, to make your manga even better….” Horie-san shares many secrets!  

But best of the times must come to an end!  
[![](./img/DSC_0928.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/DSC_0928-1280x852.jpg)  
“You guys had fun?” says Mr. Negishi, our dependable seasoned editor  

It has been an exceptional 90 minutes of non-stop manga talk, with legendary creators as guests. Many inspirational talks and tips, details of which are for the masterclass to take home…   And the day is too exciting to end here! We moved into CAFE ZENON, for more endless talk of manga in to the night… 🌛  

[![](./img/12806215_221867028164321_5094025528550576235_n.jpg)](https://www.manga-audition.com/wp-content/uploads/2016/03/12806215_221867028164321_5094025528550576235_n.jpg)    

Taiyo Nakashima 
[VIEW ALL POSTS ](https://www.manga-audition.com/author/nakashima/)  	
