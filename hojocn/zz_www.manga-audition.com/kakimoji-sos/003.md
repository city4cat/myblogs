source: 
https://www.manga-audition.com/kakimoji-sos-03-lets-gogogo-into-action/ 

# Kakimoji SOS – 03 LET’S “GOGOGO” INTO ACTION

Enrico Croce 08/03/2018 9 min read  

The night is in the grip of a violent thunderstorm. Clutching your teddy bear in terror, you almost jump out of your skin as a bolt of lightening rips open the ground, just feet from your house. With your big fight with the World’s Greatest Fighter just hours away, sleep would be elusive on the quietest of nights. But this night is charged with an electric atmosphere, making sleep impossible, enveloping the room with… **“GOGOGOGOGO!”**   

[![](./img/28822193_173396036787041_119029581_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/28822193_173396036787041_119029581_o-300x225.jpg)   
The day of the Big Fight finally dawns. You and your fellow opponent approach the fighting ground as the frenzied crowd scream out your names. The deafening roar swirls around the stadium like a bird-of-prey in flight, searching you out, stalking your every step, as you draw ever closer to your destiny… **“GO GO GO GO!”**  

[![](./img/28876850_173396090120369_1134002104_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/28876850_173396090120369_1134002104_o-300x225.jpg)  
**GO! THE MATCH BEGINS!**  

…But, there’s something wrong…your opponent is a GIANT, his presence twisting the very air around you! The terror of the previous night begins to swallow you up. This giant’s towering presence alone reduces you to a quivering mess, making you stagger back in… **“GOGOGOGO!!!**  

[![](./img/Screen-Shot-2018-03-08-at-12.22.41.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-08-at-12.22.41-300x258.png)   
Hello, my NOISY friends! This week’s article will bring you directly to the action (actually, I hope I already did that, with my nail biting intro!), allowing us to analyze a very special Kakimoji. One that was specifically requested by our beloved SMAC! mascot, Penmaru…  

> _“Enrico, when are you going to talk about the sound of action…GOGOGOGOGO?!”_  

In Japanese, it’s possible to know EVERYTHING about a particular character from their name alone. For example, names beginning with the letter ‘G’ will involve a guttural sound emanating from the throat when pronounced, making a very strong, tough and solid sound. Take the iconic characters, **Goku**, **Gundam** and **Godzilla**. They not only share names beginning with the letter ‘G’, they also happen to be the strongest in their own, respective world. Whether it’s a man, gigantic power suit or Kaiju! Do you think this is pure coincidence? How do you think this power is expressed by Kakimoji? **With the sound of action or course: “GOGOGO!!!”**   
在日语中，仅从一个人物的名字就可以了解他的一切。例如，以字母“G”开头的名字在发音时会从喉咙发出喉音，发出非常有力、坚韧和坚实的声音。以标志性角色为例，悟空、高达和哥斯拉。他们不仅名字都以字母“G”开头，而且在各自的世界里都是最强的。无论是一个人，巨大的力量服或怪兽!你认为这纯粹是巧合吗?你认为拟声词是如何表达这种力量的?**伴随着动作或过程的声音:“GOGOGO!!”“**  

(_Editor’s note:_  
if you can read Japanese, I strongly suggest the book _Kaiju no na ha naze Ga-Gi-Gu-Ge-Go na no ka_ ISBN: 978-4106100789  – Litt. _Why Kaiju’s names start with Ga Gi Gu Ge Go?_)   
(_编者按:_  
如果你能读日文，我强烈推荐《Kaiju no na ha naze Ga-Gi-Gu-Ge-Go na no ka_ ISBN: 978-4106100789 - Litt》这本书。_为什么怪兽的名字都以Ga Gi Gu Ge Go开头?_)

So, how did you FEEL when you soaked up that pre-fighting scene? How do you FEEL by look at their stance and posture with their auras surrounding all the scene? Do you the air that brings war and destruction also bring on the Kakimoji “GOGOGO”?! Now try to “read” the Kakimoji flowing through the panel, giving the scene an incredible sense of action: prepare your throat for a second and chant “GO GO GO GO”. Can you sense it?! Can you feel an atmosphere of expectant dread?! This is the power of manga! You’ve just proved that words on a printed page, here, Kichijoji, Japan can be understood and felt around the world, as it was originally intended! …sorry, but I have to say it again…AREN’T KAKIMOJI AMAZING?!?!  
那么，当你沉浸在打斗前的场景中时，你有什么感觉?看着他们的姿态和气场，你有什么感觉?你们带来战争和破坏的空气也带来了拟声词“GOGOGO”吗?!现在试着“读出”在分格上流动的拟声词，给这个场景一种难以置信的行动感:准备你的喉咙一秒钟，然后高呼“GO GO GO GO GO”。你能感觉到吗?!你能感觉到一种期待的恐惧气氛吗!这就是漫画的力量!你刚刚证明了印在纸上的文字，在这里，木城路，日本，全世界都能理解和感受到，正如它最初的目的一样!抱歉，但我必须再说一遍，拟声词是不是很神奇?  
[![](./img/Screen-Shot-2018-02-27-at-10.08.49.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/02/Screen-Shot-2018-02-27-at-10.08.49-300x279.png)   

But what does this word mean? Let’s keep in mind the importance of the letter G at the beginning of certain words, while we focus on how this sound has been categorized, making this, perhaps one of the most famous onomatopoeia (Kakimoji) in manga. When we editors at SMAC! have any doubts, we always ask to our Editor-in-Chief, Mocchi san…and this is what he has top say about this particular Kakimoji:    
但是这个单词是什么意思呢?让我们记住字母G在某些单词开头的重要性，同时我们关注这个音是如何被分类的，这可能是漫画中最著名的拟声词之一。当我们编辑在SMAC!有任何疑问，我们总是问我们的总编Mocchi先生，以下是他对这个特别的拟声词的看法:  
[![](./img/2018/03/28945246_1357996004346853_1283487615_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/28945246_1357996004346853_1283487615_o-225x300.jpg)   
_There’s no definite origin of the “GOGOGO” Kakimoji. It could have its roots in the Kakimoji “GOROGORO”, the sound of clouds before releasing thunder, or maybe the sound of a traditional Chinese gong when rung, “GON GON”. In both cases, they produce an atmosphere of expectation, building up excitement and promising an incredibly entertaining scene!_      
_**↓↓ Clouds “GOROGOROGORO” Kakimoji’s example! ↓↓**_  
拟声词“GOGOGO”没有确切的起源。它可能源于拟声词的“GOROGORO”，即云层在打雷之前发出的声音，也可能是中国传统的锣发出的“GON GON”声。在这两种情况下，它们都创造了一种期待的氛围，制造了兴奋感，并承诺了一个令人难以置信的娱乐场景!＿  
_**↓↓云拟声词“GOROGOROGORO”的例子!↓↓* * _  
[![](./img/Screen-Shot-2018-03-07-at-18.13.35.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-07-at-18.13.35-300x144.png)   
Thank you, Mocchi san! I can finally see the roots of “GOGOGO”! Now let’s take a look at the following scene, with a great gong being beaten just before battle…    
谢谢你，Mocchi先生!我终于看到“GOGOGO”的根源了!现在让我们来看看下面的场景，在战斗开始之前，一个巨大的锣被击响了……  
[![](./img/Screen-Shot-2018-03-05-at-10.50.27.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-05-at-10.50.27-300x242.png)   
**“GOOOON!”**  

This panel TALKS for itself, effectively showing the resounding power of the Kakimoji used. Take a look! The first and last “letters” are written bigger than the rest, letting the reader focus mostly on those two letters. The “OOOO” part is then perceived as an echo! In this panel, time stops as the Kakimoji expands in “time” to generate a sound effect! The direct ACTION of the original Kakimoji, “GON” and the echoed REACTION to signify the passing of time. **ACTION AND REACTION IN THE SAME KAKIMOJI! OH HOW MUCH I LOVE THEM!**     

> [![](./img/Screen-Shot-2018-03-07-at-20.09.07.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-07-at-20.09.07-276x300.png)   
Penmaru says: I LOVE KAKIMOJI TOO! Thank you Enrico, for explaining all the wonders of the Kakimoji “GOGOGO”! But now that you’ve talked about the sound of nature, like storms and clouds in building up tension, I see there are subtle differences in the Kakimoji itself… Are you telling me that there are varying degrees in each Kakimoji?! 💕  

Ahahah, Penmaru, you are a wise scholar!  Yes, there are different Kakimoji classifications, depending on what they express.  Do you want to know more about these, too? Well, see you next week then! **CIAO!**  

* * *

  **FOLLOW ME:**  Twitter – [@kenrico7](https://twitter.com/kenrico7) Facebook – [Enrico Croce Smac](https://www.facebook.com/profile.php?id=100017371275440)     Banner picture background photo by Courtney Collison