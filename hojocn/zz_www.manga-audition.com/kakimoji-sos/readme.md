source:  
https://www.manga-audition.com/tag/kakimojisos/  
https://www.manga-audition.com/tag/kakimojisos/page/2/  


# Kakimoji S.O.S.  
拟声词 S.O.S.  



- [023 Q&A Session (…and a little announcement!)](./023.md)  
- [022 Tsugihara Sensei's Kakimoji](./022.md)  
- [021 SPECIAL INTERVIEW with Jonathan Tarbox](./021.md)  
(Jonathan Tarbox: Raijin Comics（Comic Bunch英文版）的资深编辑。)  
- [020 Tomizawa sensei Kakimoji special! (PART 02)](./020.md)  
- [019 Tomizawa sensei Kakimoji special! (PART 01)](./019.md)  
- [018: City Hunter: the Hard Boiled Protagonist](./018-city-hunter-the-hard-boiled-protagonist.md)  
城市猎人:冷酷的主角
- [017: City Hunter: the Comical Protagonist](./017-city-hunter-the-comical-protagonist.md)  
城市猎人: 喜剧主角  
- [016 Kakimoji are not SFX!?](./016.md)  
- [015 Laughing with Kakimoji](./015.md)  
- [014 KAKIMOJI 3D WORLD!](./014.md)  
- [013 Master Kakimoji](./013.md)  
- [012 The Tone of Kakimoji](./012.md)  
(拟声词的历史)
- [011 Be SILENT! Kakimoji Time](./011.md)  
- [010 "The sound of Silence", Kakimoji style!](./010.md)  
(“Shima music”/“incidental music”)  
- [009 Kakimoji of Time](./009.md)  
- [008 Kakimoji DOC](./008.md)  
- [007 Fist of Kakimoji](./007.md)  
- [006 KAKIMOJI HUNTER](./006.md)  
(以CH为例)  
- [005 Kakimoji's Gender Design](./005.md)  
- [004 KAKIMOJI RANGER](./004.md)  
- [003 LET’S “GOGOGO” INTO ACTION](./003.md)  
- [002 The Sound of Rivalry](./002.md)  
- [001 An Introduction to the Art of Visual Noise](./001.md)  
