source: 
https://www.manga-audition.com/kaki-moji-s-o-s-2-the-sound-of-rivalry/

# KAKI-MOJI S.O.S. – #2 The Sound of Rivalry

Enrico Croce 01/03/2018 7 min read

> **Rivalry**  
> ˈrʌɪv(ə)lri/  
> _noun_  
> competition for the same objective or for superiority in the same field.  
> “there always has been intense rivalry between the clubs”  
> _synonyms: competitiveness, competition, contention, vying;_  
> ([dictionary.com](http://dictionary.com))  

How many times has the concept of rivalry been utilised in your favourite movies and comics? **What is Batman without the Joker? What is Rocky Balboa without Apollo Creed? What is Kenshiro without Raoh?**   Whether in sports, work or even when eyeing up the last remaining seat on a packed train, rivalry affects all of us on a daily basis.   In Italy, we have a specific word to describe the football matches between the most competitive football teams in the country: “Derby d’Italia”! (lit. trans: Derby of Italy). Think **Inter** vs **Juventus**, where family members, close friends and even star crossed lovers can come to blows in the name of fierce rivalry! _(Ed Note: Vivi didn’t waste a breath just then, in smugly reminding me that my beloved national team is now out of the World Cup_ 😣😣_)_  

[![](./img/Screen-Shot-2018-02-26-at-16.08.07.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/02/Screen-Shot-2018-02-26-at-16.08.07-300x239.png)  
Vivi, why are you doing this to me.?!  🙁 

  So, how can such an abstract and subtle concept like “Rivalry” be EXPRESSED on the pages of manga? Well, I’m here with an answer, plus some exciting examples, that will ensure your SMA9 entry will shudder with the very essence of rivalry!  

[![](./img/Screen-Shot-2018-02-15-at-13.47.36.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/02/Screen-Shot-2018-02-15-at-13.47.36-300x69.png)   We last left these two guys from **Souten no Ken REGENESIS** enveloped in an aura of antagonism, staring menacingly at each other in the most manly way imaginable. Can you feel it? Can you sense the rivalry in this scene? With the help of those emotionally charged Kaki-Moji behind them, you KNOW this is a tense scene, even if you don’t know Japanese!   

**AREN’T KAKI-MOJI INCREDIBLE?!**  
[![](./img/Screen-Shot-2018-02-27-at-10.08.49.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/02/Screen-Shot-2018-02-27-at-10.08.49-300x279.png)  

The Kaki-Moji here spells the word _“GUOOO”_ and represents the sound of wind passing through the scene. It is designed in such a way as to resemble an arch, much like the wind, whipping around the scene, that gives the reader a sensation of movement in a seemingly static scene. For a final flourish, the cherry on the cake so to speak, the manga creator added three dots, giving the scene a sense of undeniable suspense!   

Penmaru will definitely say I talk about Hokuto too much, so let’s now take a look at something else… 😏   

**ARTE**, one of the most voted and loved manga in _COMIC ZENON_ and _COMIC ZENON INTERNATIONAL_, is jam packed with effective Kaki-Moji. In this page, two merchants are having a heated argument, while our protagonist is placed in the middle of the action.    

[![](./img/Screen-Shot-2018-03-01-at-11.35.03.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-01-at-11.35.03-300x140.png)  
This panel comes directly from COMIC ZENON INTERNATIONAL’s ARTE Manga! Take a look at the page clicking [HERE](https://www.manga-audition.com/comic-zenon/arte-by-kei-ohkubo/)!  

**WHAT DO YOU FEEL WHEN YOU LOOK AT THIS PANEL?**  Do you feel a sense of anxiety in the protagonist? What is happening between the two men? Why have they ended up arguing in front of such a cute girl?! All these sensations are expressed by the Kaki-Moji _“GOGOGOGO”_. In this case, too, the Kaki-Moji represents rivalry, followed by an escalation of emotion in this antagonistic situation!  

Once again, Kakimoji is the silent, yet resounding emotional power of manga! That is why they are the real **S**ounds **O**f **S**ilence!  

I hope you enjoyed this week’s KAKIMOJI SOS, and you are brimming with ideas of how to use Kaki-Moji in your stories of rivalry!  

> [![](./img/PenMaru2-SURPRISE.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/02/PenMaru2-SURPRISE-258x300.png)   
**_WHAAAT?!_** You can’t just end it now! I want to know more about “GOGOGOGO” Kaki-Moji! It’s one of my favourite Kaki-Moji’s of all time! Please Enrico, tell me more!  

Ok guys, you heard him! Next time we will focus solely on “GOGOGO”, PenMaru’s favourite Kaki-Moji! Why is it his favourite? Why is it so famous? WHAT DOES IT ACTUALLY MEAN? Tune in next week to find out! See you next week! **Ciao!**   

---

**FOLLOW ME:**  Twitter – [@kenrico7](https://twitter.com/kenrico7) Facebook – [Enrico Croce Smac](https://www.facebook.com/profile.php?id=100017371275440)
