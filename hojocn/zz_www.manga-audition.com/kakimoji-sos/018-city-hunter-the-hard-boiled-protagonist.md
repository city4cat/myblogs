source: 
https://www.manga-audition.com/kakimoji-s-o-s-18-city-hunter-the-hard-boiled-protagonist/


# Kakimoji S.O.S. #18: City Hunter: the Hard Boiled Protagonist  
# Kakimoji S.O.S. #18:城市猎人:冷酷的主角

Enrico Croce 21/06/2018 6 min read 

Kakimoji fans, ciao! Today is the second installment of ‘Kakimoji in City Hunter’…the hard boiled chapter! Do you remember the protagonist Ryo’s “comical Kakimoji” that I introduced in the last week’s article? …yeah, this one!     

[![](./img/a01.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/a01-300x129.jpeg)  
**※ First of all, let’s be honest and recognise this man’s pure passion for beautiful women!**  

Now, I want you to see the expression of the hero in the next moment when the gag scene turns around and a serious story develops. Now, take a look at Ryo’s expression once the gag scene ends a serious moment develops…    

[![](./img/a02.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/a02-300x193.jpeg)  
_**※ Not the same person, you say?**_

As explained last week, “City Hunter” is often presented with the genre “hard boiled comedy”. Although it is a comedy but not too jokingly, it is a genre that is based on the super talented and professional ability of Hojo sensei who makes the reader laugh and constantly excites with hard boiled (but not too hard!) scene – always with an exquisite balance. As mentioned last week, _City Hunter_ is often described as a “hard boiled comedy”, with its heady mix of outlandish hijinks and tense shoot outs.  

In today’s article, I will introduce you to the serious, hard-boiled and powerful elements of _City Hunter,_ and the Kakimoji used to enhance these scenes. By all means, compare the following Kakimoji with their comedic counterparts, but please take note of the variety of expression that can only be found in manga!    

[![](./img/a03.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/a03-194x300.jpeg)  
**・ドッドッド(DoDoDo) – Helicopter engine sound**  
**・キュイーン(Qyuiin) – Sounds that determine a missile is ready to be shot**  
**・シュバム(Shbam) – Sound of a missile gun**  
**・ドゴオオオオン(Dogooooon) – Impact sound**

Did you notice? The page above is full of Kakimoji, representing the sounds emitted from machines. Take a look at all the sounds emitting from a single helicopter! Such a rich variety of Kakimoji in each panel. Look at the arrangement and design of the Kakimoji, they are rough, bold and striking, suggesting a comical scene. I can even imagine the comical stylized Kakimoji, introduced last week appearing in the next page…    

[![](./img/a05.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/a05.jpeg)  
**_“Did you call me?”_**        

[![](./img/PenMaru2-SURPRISE.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/02/PenMaru2-SURPRISE-258x300.png)      
**_HEY! Is this guy trying to taking my place as funny mascot of the SMAC! team?!_**    

Ahem… Let’s continue… The Kakimoji on this page seem to say to the reader “We speak louder than words!”.  

[![](./img/a06.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/a06-210x300.jpeg)   
The design of the Kakimoji **ググッ (Gugu)** is black and sharp.   
The design of **ババッ(Baba)**, representing the sound of torn clothes, is drawn very roughly, contrary to the softness of the fiber and the intensity of the impact and magnitude of the feeling are felt.  
It’s impressive how **ギラッ (Gira)**, which is a “no sound Kakimoji” (silent Kakimoji introduced in the previous article), leaves a strong feeling in the reader! The strong power of the eyes is transmitted by only the picture. I was thinking of finishing this week’s article about the Kakimoji of _CITY HUNTER_ with this final page, but I have to tell you one very important thing, that can only be learned from this great sensei’s work.  

[![](./img/a07.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/a07-208x300.jpeg)   
As you can see, by NOT using Kakimoji on this page, the manga creator (Hojo sensei here and you in your first professional manga!) can make readers focus only on the conversations!  

Remember that the depth of the work will become even bigger with balance and speed!   

See you next time for more Kakimoji magic!   
**CIAO!!**
