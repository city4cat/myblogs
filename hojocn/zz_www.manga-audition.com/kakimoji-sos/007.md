source: 
https://www.manga-audition.com/kakimoji-s-o-s-7-fist-of-kakimoji/

# Kakimoji S.O.S. 7 – Fist of Kakimoji 

Enrico Croce 05/04/2018 7 min read  

> 2018! SMA9 deadline has just passed and the SMAC! team is working around the clock to read all your marvelous manga entries! BUT, this editor is still writing about Kakimoji!!!
> 
> **You are already designed!**  

[![](./img/29633555_184222849037693_1800334924_o.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/29633555_184222849037693_1800334924_o-300x225.jpg)   
Hello my NOISY friends and welcome to the 7th installment of Kakimoji S.O.S.! Seven? Seven like the seven stars of… of… Oh, darn, I’m sure I’ve forgotten something important, but I can’t quite put my finger on it… PenMaru, maybe you know?  

[![](./img/Screen-Shot-2018-03-19-at-16.25.19.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-19-at-16.25.19-266x300.png)       
**_What are you talking about, Enrico?! Aren’t you the world’s expert on a CERTAIN series and a CERTAIN manga master? Do you really NOT remember what the number “Seven” is linked to?_**         
How could I forget?! Seven stars? It’s HOKUTO! Today’s 7th article will be all about the first chapter of Hokuto no Ken, a series that still keeps conquering readers’ heart the world over, even 35 years on from its debut!! How can a hero be established in his first appearance? How are the Kakimoji of mobs represented to effectively show their ferocity and power? Hero Kakimoji VS Mobs Kakimoji!   

Let’s join the action! Just like any super-hero, Kenshiro’s first appearance in the manga wasn’t that of a strong guy (to allow the reader to really feel the WOW moment when Kenshiro turns himself into a war-machine at the end of the episode!). The Kakimoji represented here shows a fragility to this mysterious  character.  

[![](./img/Screen-Shot-2018-03-26-at-10.27.45.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-26-at-10.27.45-193x300.png)  
**Fura Fura (****フラフラ****)**  

It represents the unsteady steps of feet on the ground, plus the shaking of the body (because of the emptiness of the stomach). Because of the numerous spasms in Kenshiro’s body and his slow way of walking, this Kakimoji is written very small, allowing us to immediately see the “weak appearance” of our hero. By contrast, this is how the very first enemy of Kenshiro appears. His is Kakimoji is shown in the following panel:  
[![](./img/Screen-Shot-2018-03-26-at-10.29.01.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-26-at-10.29.01-300x273.png)  
**NUU~ (ぬう～, ROAR)**  

An enormous giant, with spiked clothes, tattoos and a Mohican is snarling and growling before us, squeezing his colossal fists of fury! Your instinct is to run away from him fast… that’s if you’re not Kenshiro! Even if though the Kakimoji **NUU** is powerful and intimidating, our hero doesn’t bat an eyelid, deciding to walk into this crazy mob full of punks.    
[![](./img/Screen-Shot-2018-03-26-at-10.30.00.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-26-at-10.30.00-300x171.png)  

[![](./img/Screen-Shot-2018-03-26-at-10.30.11.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-26-at-10.30.11-207x300.png)  
**ZA ZA (ザッ ザッ)**  

What was weak before is now full of energy, even in the very design itself! “ZA” is the sound made by walking steps and the shoes hitting the ground, foretelling the justice about to be delivered by Kenshiro. The walking sound is utilizes sharp, straight lines initially, which then give out to an explosion of power on this beautiful page. Now our hero is face to face with his enemy! Let’s look at how their strength is designed:    

[![](./img/Screen-Shot-2018-03-26-at-10.31.38.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-26-at-10.31.38-300x300.png)  
**UNU~ (うぬ〜,** **GRR)**  

This Kakimoji is shown the same way as **NUU** before, making the enemy appear more animal-like than human. But it is very weak! In fact, now we see a more frightful tone before it is weakened by Kenshiro’s Kakimoji!  

**GAYA (ガヤッ, to be noisy, clamorous)**  

OOOOH! Kenshiro is ready to attack!!! The tight-lipped enemy’s growl is represented by a black colour and softer design VS the incredibly strong scream of our hero’s sharp design sharp! All this tension and action was expressed solely by Kakimoji! Aren’t they the strongest weapon of any manga creator?!  

[![](./img/Screen-Shot-2018-03-19-at-18.27.16.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/03/Screen-Shot-2018-03-19-at-18.27.16-300x296.png)       
_**PenMaru: YES, THEY ARE! I can’t wait to use them in my new Kumamoto EX3 round manga! I want to show my hero fighting a rival while he’s trying something new…maybe a car race could work?**_   
I think that’s a great idea, PenMaru! The innovation of cars are synonymous with human history, and with the theme WASAMON for Kumamoto ROUND 3, innovation is what we’re looking for! Don’t miss the next article, the manga that made a thousand petrol heads in Japan! See you next week with the YOROSHIKU MECHA-DOC Kakimoji article! CIAO!  

---

FOLLOW ME for more Kakimoji tips! [Twitter](https://twitter.com/kenrico7) [Facebook](https://www.facebook.com/profile.php?id=100017371275440)
