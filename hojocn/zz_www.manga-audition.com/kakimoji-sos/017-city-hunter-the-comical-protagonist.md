source: 
https://www.manga-audition.com/kakimoji-s-o-s-17-city-hunter-the-comical-protagonist/


# Kakimoji S.O.S. #17 – City Hunter: the Comical Protagonist
# Kakimoji S.O.S. #17 -城市猎人: 喜剧主角  

Enrico Croce 14/06/2018 8 min read

Today marks the first installment of a two part, “S.O.S. Kakimoji” special, focussing on a spectacular manga that has stood the test time…I give you “The Kakimoji of City Hunter”!  

**“City Hunter: The chapter of the Comical Protagonist” START!**  
[![](./img/6c78985d.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/6c78985d-300x195.jpg)   
“City Hunter” is a hard-boiled comedy, involving the adventures of “sweeper” Saeba Ryo, who investigates, or “sweeps” the criminals of Tokyo as both bodyguard and detective. Have you noticed something here…? Yes, “hard-boiled comedy”! This wording is often used to describe the genre “City Hunter” belongs to. But then, don’t you think the words “hard-boiled” and “comedy” sound like opposing themes?  

Saeba Ryo is the NO1 specialist to call when you are in danger. Just by leaving a special card in Shinjuku, you will be getting the attention of the BEST! Naturally, the work he undertakes is filled with many dangers. Powerful and thrilling action scenes make up “City Hunter”, with stories full of the term “hard boiled” has been well and truly earned.  However, where is the comedy? As any “City Hunter” fan will tell you, Ryo Saeba’s prowess when dealing with the Tokyo underworld is only matched by his sky high libido! A result of drug experimentation, his over powering urge to give any beautiful client “Mokkori” service is the perfect opportunity to inject comedy in Hojo sensei’s beloved manga.  

[![](./img/CITY_2.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_2-237x300.jpeg)  
**_Look away kids!_**  

Thus, “City Hunter” pulls the reader into a maelstrom of comedy and action, allowing them to both laugh and feel excitement. So, what establishes a hard-boiled moment, over a comical one? Kakimoji, of course!    

**Difference between serious Kakimoji and comical ones.**  
严肃的拟声词和滑稽的拟声词的区别。  

Professional manga creators generally arrange various concepts on a page, adding personal touches to the drawings, dialogues, backgrounds and SFX lines. But what about Kakimoji? How can Kakimoji help to establish a comedic moment?      
专业的漫画创作者通常会在一页上安排各种概念，在绘画、对话、背景和特效台词上添加个人的风格。但是拟声词呢？ 拟声词是如何帮助建立一个喜剧时刻的？  

[![](./img/CITY_3.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_3-300x113.jpeg)  
[![](./img/CITY_4.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_4-300x254.jpeg)  
**“GUOO” (to express resolution and determination)**  

**“GUOO”** is used when Ryo is unable to contain himself when in the company of a beautiful woman. His determined to “MOKKORize” looses any sinister intensions, is replaced with comedy, exactly because it is Saeba!  The Kakimoji “GUOO” is a relatively strong one, directing a “stupid” atmosphere with designed with simple hiragana fonts. (In fact, even if these lines are forgettable, the readers will definitely laugh at the main character speaking with strong determination)  

[![](./img/CITY_5.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_5-240x300.jpeg)  
**“NUU” (eerie Kakimoji –** **an onomatopoeia often used when unexpected things appear)**     

[![](./img/CITY_6.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_6-300x194.jpeg)  
[![](./img/CITY_8.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_8-275x300.jpeg)  
**“KUNE KUNE” (bending bashfully like a snake)**  

To complete a mission (for a beautiful women!) our hero occasionally has to crossdress – a task our her hilariously enacts with enthusiasm. The action here is eerie in itself, so it expressed with Kakimoji: when his appearance is full of suspicion then the Kakimoji that follows his actions is "**NUU”**, when instead he tries to act in a feline way, then the Kakimoji **“KUNE KUNE”** will be used. It is a scene that can be laughed along with, just by the very fact it’s a muscular man wearing a dress, but the fascination will be doubled by using Kakimoji!  

[![](./img/CITY_9.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_9-247x300.jpeg)  
**“RIKISETSU” (To express emphasis\*** **This is not an onomatopoeia, but a word.)**  

[![](./img/CITY_10.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_10-213x300.jpeg)  
**“HAN!!”** **(Like “SGRUNF”. More a colloquialism rather than a Kakimoji)**  

Here’s something different: words and colloquialisms used as Kakimoji. **“RIKISETSU!”** shows the positive resolution the protagonist shows, when on a mission to conquer the heart of a beautiful woman. This is shown by using a half word/half Kakimoji. **“HAN!”** is arranged in a balloon, further enhanced by using “katakana” at the end of the Kakimoji (ッinstead ofっ). A glimpse of the hero’s childish side can thus be obtained. After all, the content of the dialogue is “stupid”, but the hero is “serious” as hell!  Learning Kakimoji usage is very important to manga creators, in their mission to raise an ambivalent laugh!     

[![](./img/CITY_11.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/06/CITY_11-300x239.jpeg)  
**“CHIKA CHIKA” (dazzling eyes)**  

This Kakimoji is very cute! The eyes, dazzled by the sight of beautiful women: something like a flash of light electrifying our hero’s from the eyes, giving the ballooned Kakimoji **“CHIKA CHIKA”.** Even if the protagonist is a professional “sweeper”, with this design all his weak points when confronted by a sexy body is shown instantly. “Chika Chika” will jump out of the eyes of Ryo and jump into yours!  

Today I introduced some comical Kakimoji from “City Hunter”. I want you to think carefully about what kind of features each had, and in the next article I will introduce you to City Hunter’s hard boiled Kakimoji!  Prepare yourself!  **CIAO!**  

