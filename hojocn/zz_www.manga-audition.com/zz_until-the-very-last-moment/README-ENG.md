
原链接: 
https://www.manga-audition.com/hojo-sensei-interview-until-the-very-last-moment/

![](./img/top_ad_hojo2015.jpg)

# Hojo Tsukasa sensei Interview – “Until the very last moment”
Taiyo Nakashima 
04/09/2015 
8 min read 

**First of all, can you give please tell us overall comment?**  

I think what stood out to us the most was: the youth of the entrants. When we sat down for the judging finals, I think we were all expecting a lot of tear-jerkers. But in retrospect, for young artists,  the theme “mother” isn’t necessarily connected to tears.  

For example, when I first read the Honorable Mention “Tales from the Jungle”, I thought “But this is just the food chain. It’s part of nature as we know”. But as I thought more about it, the simple fact that “the carnivore mother kills the herbivore child so she can give the meat to her own child” is really very moving, and the young artist was able to catch the beauty of it. The works this year really shed some light, on the young contestants’ view of the world.  

![](./img/smC4358_03-932x1280.jpg)  

We’ve experienced a lot of things in our lives, and as creators, we’re always trying to “aim higher”. Perhaps because of this mentality, we’ve become a little desensitized to some of the small wonders of life.  

Another point to mention, is that there was so much variety among the entries. We again received a great number of works from around the globe, resulting in the award winners, being a diverse collection of “senses”, of youthful minds from great many parts of the world.  

 

**What does the theme “mother” mean to you?**  

Hmmm… to be honest, if we’re talking about my own parents, it’s not such a relevant theme in my life anymore. I mean, I already have grandchildren. I’m the father of a mother. It could be that I’ve forgotten what mothers are all about. So in that sense, this round of SILENT MANGA AUDITION felt very fresh.  

I can see that for this generation of artists, their mother is still an important authority figure in their lives, at times annoying, but always there to save them in a pinch. So the “tear-jerkers” that I guess we judges were looking for, weren’t many. When the contestants become mothers and fathers themselves, their manga may also change dramatically.  

![](./img/DSC0037-e1442312133740.jpg)  


**If you could sum up this round in one word, what would it be?**  

I would say “fresh”… which may be a little cliché (laughs). Really, reading all these fresh works was so enjoyable, that made me feel glad somewhat. We all tend to assume that people from other countries or generations are very different, but through this round of SILENT MANGA AUDITION, I started to realise that we’re not so different after all.  

What’s actually happening is that, regardless of country or generation, young people are young people. Growing up through the same kind of hardships, living energetically all the same. Realization of that really lifted my spirit. Seeking and connecting all those young, like-minded youth through events like SILENT MANGA AUDITION is something we grown-ups should really be doing, instead of starting wars or other atrocities…  

Anyways this was a great opportunity for me to experience the feelings and senses of many fresh artists through those pages. I am sincerely grateful to everyone, for reminding us of feelings that we may had forgotten somewhere along the line.  

 

**When it comes to drawing manga, I think that “youth” could be both a powerful weapon and a weakness. How do you think they should take advantage of it?**  

There really is no need to do anything special. If I had to put into words, it’s just a matter of “trial and error” – keep on drawing, and learn from one’s own mistakes. If you’re looking at youth as a weakness, then the only way to overcome it is to accumulate experience. Maturity is not something that can be taught, nor learned from another person. They just have to draw something that’s full of errors, then reflect “What could I have done better? What could I have done differently?”, and then try again. By repeating this process, one will naturally improve, little by little.  

 

**For this youthful round of SILENT MANGA AUDITION, some said that the entries were a little difficult to empathize with, or that the theme wasn’t expressed very well. What do you think?**  

My guess is that the judges may be too old! It’s not that the works were hard to empathize with, it’s more like we’ve forgotten what it’s like to be young. Perhaps we were forcing our adult image of mothers onto them. The “mothers” depicted in their work no doubt matches their reality.  

The works that we judges couldn’t empathize with might be very understandable to young readers. This round made me realize that we probably should invite young guest judges for SILENT MANGA AUDITION! (laughs)  

 

**Speaking of which, during the judging, you said that there were many “curve ball” pieces.**  

Since “Mother” may have been a difficult theme for many artists, many tried some truly creative ideas, projecting their own image of “mother”, ending up to feel a bit of a “curve ball” to everybody else. To put it other way, I was very impressed with the way how many  challenged to be unique. However, that doesn’t mean we give extra points for pieces that are “different” or “unusual”.  No matter how extraordinary or otherwise, as long as it’s enjoyable to read, it’s good for us and worthy of an award.  

![](./img/DSC0081.jpg)  


**Which of the award winning titles would you recommend?**  

The Grand Prix winner Ichirou’s work springs to mind. It felt like reading a good novel. But it’s precisely because he’s so good that makes me want just a little bit more from him. May be at the end, it could have been like : “the Earth is shaped of a giant womb, with the protagonist’s silhouette as an unborn embryo, projected on the womb…”, “Any way to leave even stronger impression?” – ideas like this kept on coming in my mind.  

![](./img/smC4390_Ichirou_HA02-915x1280.jpg)  

Some of the other Master Class members seemed to struggle a little this time round. Ever year the overall standard goes up, and it’s precisely because they’re the master class, that makes us feel “We know you can do even better!” This strict reviewing policy is to continue.  So dear masterclass, please do stay with us. (Smiles)  

Among the others, I personally really liked Sires Jan Black’s “Aerials”. It was a wonderful, bright work, a piece that really reminds us what makes a good manga, a good manga.  

![](./img/smC5673_SiresJanBlack_Aerials01-908x1280.jpg)  

**Did you feel that the standard is rising every year?**  

It’s not so much that there’s been a huge jump. Rather, SILENT MANGA AUDITION is already on a higher level than many domestic manga awards in Japan. If you were to enter these winners into Comic ZENON’s regular manga awards, they’d put up a good fight I’d imagine.  

Put it other way, if the fight is between “Artists of the world” vs “Artists of our humble islands, Japan”,  it’s only natural that the diversity of the world’s talents will win over Japan. I’d say the time is now for the artists in Japan, to grow up, and out of the past.  

I feel that at the moment, most manga is created by manga fans. Both the artists and the editors love manga, so they’re pouring their hearts into it. However, you can’t grow by reading manga alone. It’s better to experience a wide variety of media, like movies, music, novels, plays… Without that, the world as an artist will shrink, and keeps on shrinking. You’ll just sink deeper and deeper into old patterns.  

That is why I feel the future possibilities, when I meet an artist like Ichirou, who also loves novels, movies… and just about everything! But with high hopes comes high expectations, I can’t help myself  expecting that extra inch, every time.  (Smiles)  

![](./img/DSC0010.jpg)  

**What do you look for when you’re judging?**  

We don’t look for anything complicated. We simply ask “Is it enjoyable to read, or not?” – we can always analyze it later. Evaluating manga isn’t the same as evaluating art. Even if it’s poorly drawn, manga can still be entertaining!  

All the entrants are still very young, with lots of room to grow. In your teens, you can still be wonderfully reckless. Then, your 20s are usually the greatest period of growth in your life. Getting into your 30s, things might slow down, and you might hit a few blocks, but this is where your style really matures! To begin with, a manga artist is not something that you can become with half-hearted feelings.  

Have determination and beliefs at all times, and utilize concentration as the tool, to keep on sharpening the skills.  

**Finally, one last message for the readers please!**  

Please do your best, until the very last moment.  

Even after you’ve “finished”, take one last look at your work, and change what you can. Think what you could do to improve it. Even if the results aren’t great, just compare it to the old version, and use the best one. So it helps to save the old version to go back to.   

Continue trying things right until the very end, and send us the one, that touches your own soul.  

In this way, I’m sure you’ll be able to complete a piece, that charms yourself, and everyone who reads it.  

![](./img/DSC0029.jpg)  

--------------------------------------------------
[CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc/4.0/80x15.png)

转载请注明出处

