source: 
https://www.manga-audition.com/judges/tsukasa-hojo/  


# Biography  
传记  

Born in and raised in the Kita-Kyushu region of Fukuoka, Tsukasa Hojo began his foray into manga creation while studying technical design at Kyushu Sanyo University. Entering his one-shot ‘Paapurin’ (The simpleton) to girl’s manga magazine ‘Ribon’, Hojo was selected to win an honorable mention award, a feat he actually outdid within the same year when his one-shot ‘Space Angel’ went on to win the 18th Shueisha Tezuka Award in 1979.  
出生于福冈的北九州地区，在九州Sanyo大学学习技术设计时开始涉足漫画创作。通过在女性漫画杂志《Ribon》上发表他的单话作品“Paapurin”（The simpleton），北条获得荣誉奖，这一壮举他实际上在同年（注：1979年）被超越--他的单话作品“Space Angel”在1979年赢得了第18届手冢奖。  

Riding the wave of success, Hojo made his full debut in the pages of ‘Weekly Shonen Jump’ with the one-shot ‘Ore wa Otoko da!’ (I am a man!). After graduating university, Hojo once again put pen to paper and created the storyboard of was to become his breakout hit ‘Cat’s ♥ Eye’ before sending it in to the top brass at ‘Weekly Shonen Jump’. The editor in charge at the time was none other than COAMIX CEO Nobuhiko Horie and, seeing promise in Hojo, he decided to not only run with the idea, but also to pay for the young manga creator to leave his native Kyushu and work on a full serialization.  
乘着成功的浪潮，北条在《周刊少年Jump》上以单话作品“Ore wa Otoko da!”(我是个男子汉!)首秀。大学毕业后，北条再次动笔，创作了后来成为他的爆款之作《猫眼》的故事板，然后把它交给了《周刊少年Jump》的高层。当时负责的编辑正是COAMIX的CEO堀江信彦，他看到了北条的前途，决定不仅实现这个想法，而且还出钱请这位年轻的漫画创作者离开他的家乡九州，从事完整的连载。  

Adjusting to his new life in Tokyo and the realities of working as a full-time manga creator, Hojo toiled away for the next few years on ‘Cat’s ♥ Eye’, the popularity of which was keenly felt across the nation with an anime adaption being produced. However, it wasn’t until 1985 that Hojo really made his mark on the world of manga, when his new serialization ‘City Hunter’ burst onto the pages of ‘Weekly Shonen Jump’. Running for 35 volumes and spawning numerous anime adaptions, television dramas and movie adaptions, both in Japan and overseas, ‘City Hunter’ was a runaway hit that placed Hojo firmly in the hearts of fervent manga readers across the globe.  
为了适应东京的新生活和全职漫画家的工作，北条在接下来的几年里埋头苦干，创作了《猫眼》，这部漫画在全国范围内都很受欢迎，并被改编成动画。然而，直到1985年，当他的新连载《城市猎人》出现在《周刊少年Jump》上时，北条才真正在漫画界留下了自己的印记。《城市猎人》共有35卷，在日本和海外都有大量的动漫改编、电视剧和电影改编，《城市猎人》一炮而红，使北条深深地留在了全球狂热漫画读者的心中。  

Penning other titles such as ‘Komorebi no Moto de’ (Under the dapple sun), ‘Rash!!’ and ‘Family Compo’ throughout the nineties, Hojo kicked off the 21st century by joining forces with fellow manga creators Tetsuo Hara, Ryuji Tsugihara, and his former editor Nobuhiko Horie to establish COAMIX Inc. Blessing the newly founded publication Weekly Comic Bunch with his famous “Hojo Magic”, he revisited his beloved characters of Ryo Saeba and Kaori Makimura to give the voracious manga reading public ‘Angel Heart’, a brand new serialization set in a parallel universe from that of the much loved ‘City Hunter’. The series was an instant hit with fans and even got a “2nd Season” with the inception of the now current ‘Monthly Comic Zenon’.  
在整个90年代，他还创作了作品“Komorebi no Moto de”（在斑驳的阳光下）、《Rash!!》以及《Family Compo》，通过与其他漫画创作者原哲夫、次原龙二、他的前编辑堀江信彦一起成立了COAMIX Inc，从而开启了21世纪。以他著名的“Hojo Magic”为新成立的Weekly Comic Bunch增添了祝福，重新创作了他心爱的人物——冴羽獠和槇村香，为如饥似渴的漫画读者带来了《天使心》，这是一个全新的连载，背景是在一个平行宇宙中深受喜爱的《城市猎人》的故事。该连载一经推出就受到了粉丝们的欢迎，甚至还推出了“第二季”，即现在的“月刊漫画Zenon”。

In addition to working closely with the numerous anime and manga projects associated with his works, Hojo is also on the judges panel in the SILENT MANGA AUDITION® Committee.  
除了与众多与其作品相关的动漫项目密切合作外，北条还担任SILENT MANGA AUDITION®委员会的评委。  

