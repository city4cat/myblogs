
# [zz] www.manga-audition.com上与北条司及其作品相关的信息 ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96828))  

## Reports
- [(2019.11)2019熊本国际漫画营的报道](./zz_kimc2019/readme.md)  
- [(2018.05)成为漫画家 之二 北条司](./zz_making_a_mangaka/readme.md)  
- [(2018.04)SMA’s Italian Job](./zz_sma_italian_job/readme.md)  
- [(2017.01)井上雄彦 & 北条司：漫画师徒 - 独家报道！](./zz_takehiko_hojo/readme.md)  
- [(2016.03.16)SMA MASTERCLASS 2016](./zz_sma_masterclass2016/readme.md)  
- [(2015.07.25)北条司签售会](./zz_AutographSessionWithHojoSensei/readme.md)  
- [(2015.05.05)THE SHOW AIN'T OVER YET!!](./zz_the-show-aint-over-yet/readme.md)  
- [(2015.04)北条司访谈: "Until the very last moment"](./zz_until-the-very-last-moment/README.md)  
- [(2015.02)北条司访谈: "Music to my Eyes"](./zz_music_to_my_eyes/README.md)   
- [(2013.08)Message from Tsukasa Hojo](./zz_message/README.md)  


## [Japanese Manga 101](./zz_japanese_manga101/readme.md)  
日本漫画入门  

## [Kakimoji S.O.S.](./kakimoji-sos/readme.md)  
拟声词 S.O.S.  

## [Manga S.O.S.](./manga-sos/readme.md)  

## Miscellaneous
- [北条司简介](./hojo-biography.md)  
    - 1979年，向杂志《Ribon》投稿作品《Paapurin》，并获荣誉奖。    
    - 《阳光少女》的原著名‘Komorebi no Moto de’ (Under the dapple sun)。  

---  

manga-audition上关于北条司的更多报道：  
https://www.manga-audition.com/tag/hojo-tsukasa/  

---  

### Links:  
1. [codebeautify](https://codebeautify.org/html-to-markdown), [Convert HTML to Markdown](https://html-to-markdown.com/demo)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  
6. [Bing Translator](https://cn.bing.com/translator?ref=TThis&from=ja&to=zh-Hans&isTTRefreshQuery=1)  
7. [OCRSpace](https://ocr.space/)  
8. [www.hojo-tsukasa.com | Wayback Mechine](http://web.archive.org/web/20011129230020/http://www.hojo-tsukasa.com/)  
