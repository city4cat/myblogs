source: 
https://www.manga-audition.com/manga-s-o-s-2-paneling-of-the-north-star/  

# Manga S.O.S. #2 – Paneling… OF THE NORTH STAR!

Enrico Croce 09/08/2018 6 min read  

Mangaka’s of the world, CIAO! Today we will continue talking about paneling. Specifically, how to create the perfect “Koma-wari” (“paneling” in Japanese) by analysing, in great detail, the very first chapter of my favourite manga… “Fist of the North Star” by Hara Tetsuo sensei! But before we start… do you remember what we covered last week? There are 2 different types of paneling pages, the rough ones (a page with a small number of panels) and dense (a page with many panels, containing a lot of information).  

[![](./img/Screen-Shot-2018-08-02-at-13.00.54.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Screen-Shot-2018-08-02-at-13.00.54-300x221.png)   
The proper order of Koma-wari for a long story, is to start with a rough panelling approach at the start of the manga, then dense in the middle, followed by rough at the end: this will ensure your manga is easy to read while giving the story a dynamic tempo!  

[![](./img/Screen-Shot-2018-08-02-at-13.02.33.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Screen-Shot-2018-08-02-at-13.02.33-300x197.png)   
So, now that we have recapped the basic structure of a professional manga, let’s dig further into Koma-wari by looking at a professional title… **CHAPTER 1 OF FIST OF THE NORTH STAR: A CRY FROM THE HEART”!**  

[![](./img/Enrico1.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Enrico1-209x300.jpeg)   
The opening scene of episode one is a powerfully HUGE single panel! As we discussed last week, the principle of any good manga is to start the story with a “Rough” Koma-wari, exactly like the one here, that grabs the reader instantly!! Furthermore…  

[![](./img/Enrico2.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Enrico2-300x229.jpeg) 
[![](./img/Enrico3.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Enrico3-300x232.jpeg)   
_**DON!**_  

This theory applies from pages 2 to 5, with a coarse frame rate which boldly spreads out the action. Indeed, the total number of frames on page four is eleven! Can you believe it?! A bold line-up of less than three frames with one page creates this unique power and impact! Leading to the middle of the story with a “dense” Koma-wari.  

[![](./img/Enrico4.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Enrico4-209x300.jpeg)   
Even though it has higher density rate than the previous one, impact and readability are not impaired. As this is Rin’s first appearance in the story, the standing appearance of Rin (on the left side of the page) is laid out in a form of a drawing, outside of the paneling. Consequently, the appearance of Rin stands out on this “dense” page, making her literally stand out.  

[![](./img/Enrico5.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Enrico5-208x300.jpeg)   
This is also a dense page, but the reader never gets lost in the order of reading. Look carefully at the page, the narrow width and thick width described last week has been used successfully!  

[![](./img/Screen-Shot-2018-08-02-at-13.12.37.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Screen-Shot-2018-08-02-at-13.12.37-300x283.png)   
Manga creators should never forget how to guide their readers. Finally, the last page of episode one.  

[![](./img/Enrico6.jpg)](https://www.manga-audition.com/wp-content/uploads/2018/08/Enrico6-209x300.jpeg)   
Again, we see a charming use of “rough” panelling, leaving the reader to ponder “what will happen next?”. I hope you enjoyed this little journey into the manga making world! Alas, I can’t explain everything in such short an article, but I am sure that by using what you have learned in today’s episode, you can go and find your personal examples of “Dense” and “Rough” Koma-wari! So the next time you read your favorite manga, focus on the “Koma-wari” and the way it manipulates the reading order. When you do, make sure to share them on your SNS by tagging me or using the hashtag **#mangasos** 😉     Next week, I will introduce techniques to manipulate time with the frame rate. Looking forward to to! CIAO!  

---

FOLLOW ME for more Kakimoji and manga tips! [Twitter](https://twitter.com/kenrico7) [Facebook](https://www.facebook.com/profile.php?id=100017371275440) …and remember to use the hashtag **#mangasos** 😉