
source:  
https://www.manga-audition.com/making-a-mangaka-2-tsukasa-hojo/  

![](./img/Tsukasa_Hojo_Banner-830x467.jpg)

## Making a Mangaka: #2 Tsukasa Hojo  
## 成为漫画家: #2 北条司

Christopher Tordoff, 16/05/2018, 6 min read  

This week, I look to one of our own. Hojo sensei, one of the founders of COAMIX, has built an illustrious career that boarders on the cinematic. With Femme Fatale’s aplenty, the Manga Master has made liberal use of classic movie tropes that shine a light on the good, the bad and the ugly of life, all carried out with tongue firmly in cheek.  
本周，我期待一个我们自己的人。北条老师--COAMIX的创始人之一--已经建立了辉煌的职业生涯，作品多次搬上荧幕。作品里不仅有美丽的女性，而且漫画大师成功地运用了经典电影中的比喻，以北条式的幽默向人们展示了生活的好、坏和丑陋。  
 

Name: Tsukasa Hojo (北条 司 Hōjō Tsukasa)  
Born: March 5, 1959  
Place of Birth: Kokura, Kitakyushu, Japan  
Speciality: Urban crime thrillers with a twist.  
名字： 北条 司 Hōjō Tsukasa (Tsukasa Hojo)  
出生日期：1959年3月5日  
出生地：日本北九州市小仓 日本北九州小仓  
特长：一波三折的都市犯罪惊悚小说。都市犯罪惊悚小说，但有一个转折。  

![](./img/30223467_192560714870573_1877736116_o-150x150.jpg) 
![](./img/30420519_1397863973679612_4415511272751313004_o-300x200.jpg)  
Tsukasa Hojo, entertaining the crowds at Romics Comic Festival, Rome, Italy  
北条Tsukasa Hojo，在意大利罗马举行的Romics Comic Festival上与读者见面。    
 

### About…  

A native of Kyushu, Tsukasa Hojo’s career in manga could well be viewed as ‘accidental’. While studying at Kyushu Sangyo University, Hojo spent much of his time balancing film and illustration. These two, albeit similar forms of visual expression would come to a head when he submitted a piece, “just for fun” to girl’s manga magazine Ribon. Unexpectedly receiving an Honorable Mention Award for his efforts, this seemingly innocuous event decided the future of Hojo’s artistic output.  
北条司是九州人，在漫画界的职业很可能被视为“偶然的”。在九州产业大学读书期间，北条花了很多时间在电影和插画之间取得平衡。尽管这两种视觉表达形式相似，当他向女性漫画杂志《Ribon》提交一件“只是为了娱乐”的作品时，这两种形式成形了。这项看似无害的活动意外地因他的努力而获得了荣誉提名奖，这决定了北条的艺术作品的未来。  

While living the typical, penniless student life, Hojo’s friend brought the fledgling manga creator’s attention to Shonen Jump’s Tezuka Award. With a 1 million Yen top prize, the competition was too tempting to pass up, so empowered by his modest achievement with Ribon, Hojo hastily got together a 31 page story, submitted and against all odds, won!   
在过着平淡无味的学生生活的同时，北条的朋友将这位刚起步的漫画创作者的注意力吸引到了少年跳跃的手冢奖。由于获胜者可以得到100万日元的最高奖金，所以比赛太有吸引力了以至于无法错过。因此，凭借与Ribon的小小成绩，北条匆匆忙忙整理了31页的故事，提交后与人角逐，最后胜出！  

Not long after graduation, Hojo sent a storyboard to Jump, of what would later become his breakout hit, Cat’s Eye. The editor was so impressed by the potential of this young manga creator, that he insisted he up-sticks, leave Kyushu behind and set up shop in Tokyo, going so far as to pay for his accommodation. That editor was a certain Nobuhiko Horie, future CEO of COAMIX and the driving force behind SMAC!  
毕业不久后，北条向Jump杂志社发送了一个故事板，该故事板后来成为他的成名作《猫眼》。这位年轻漫画创作者的潜力给编辑留下了深刻的印象，以至于编辑坚持要他离开九州、搬去东京，然后在东京开始事业，甚至自己为他担负食宿。那位编辑就是COAMIX的未来首席执行官，SMAC背后的推动力的堀江信彦。  

Hojo was more than daunted by the grueling work load of the tireless Mangaka. Advised to “grind out” work for 5 years, the standard “probation period” of professional manga creators, the author of Cat’s Eye, City Hunter and Angel Heart stuck to his convictions, toiled away at a frantic pace and created some of the most enduring characters ever to fill the pages of manga.  
不知疲倦的漫画家的艰巨的工作量使北条感到不知所措。被人建议“咬牙坚持”工作5年（职业漫画家的标准“试用期”），作为《猫眼》、《城市猎人》和《天使之心》的作者，他坚持自己的信念，常年以紧张的节奏辛勤劳作，在一页又一页的漫画里创造了一些最能经得起时间考验的角色。  

![](./img/Screen-Shot-2018-03-26-at-11.jpg) 
![](./img/Screen-Shot-2018-03-26-at-11_003.jpg)  

### MY THOUGHTS  
个人想法  

I’ve always had a passion for detective stories of the 40’s and 50’s, from the screen work of the eternally grumpy Humphrey Bogart to the gritty world of Raymond Chandler. The characters of that time were larger than life, almost caricatures of emotion who dominated the stories they inhabited. The downtrodden ‘gumshoe’ detective; the stoic and all knowing bartender and the lustful, yet unattainable Femme Fatale, these were characters that spoke to me on a personal level. Veritable “anti-heroes” that are flawed, almost vindictive at times, who use whatever methods they have to get the job done, not only make for strong and interesting characters, but allow us to identify with the stories unfolding before us. Tsukasa Hojo took this concept of character driven narratives, warts and all, and ran with it!  
我一直对四五十年代的侦探故事充满热情，从永远脾气暴躁的汉弗莱-鲍嘉(Humphrey Bogart)的银幕作品到雷蒙德-钱德勒(Raymond Chandler)的粗犷世界。那时候的人物高于生活，几乎是充满感情的讽刺漫画人物，他们主导了他们所在的故事。穷困潦倒的"gumshoe"侦探；坚忍不拔、知无不言的酒保，以及欲罢不能的"蛇蝎美人"，这些角色都是我个人的心声。有缺点的这些十足"反派"几乎经常报复别人，他们为达目的不择手段，这不仅塑造了强大和有趣的角色，而且通过这些故事展现给我们。北条司完整地借鉴了这种以人物为主导的叙事理念，并把它发扬光大了!  

![](./img/Screen-Shot-2018-03-26-at-11_002.jpg)  

Debuting in 1981, Cat’s Eye was Hojo’s breakout work. The tale of three sisters engaged in art theft looks, on the face of it, a tale of daring anti-heroes with a devil-may-care attitude. But more than that, Cat’s Eye, like real life, is a story steeped in duality. Hitomi, the eldest sister and the driving force of their criminal behavior, loves the inept police officer in charge of bringing them to justice. She also displays incredible levels of jealousy, going so far as to resent her own alter-ego when Toshio, the bumbling cop, spends more time in pursuit of his nemesis than he does wooing his beloved. These all too real emotions are the secret to Cat’s Eye success, firmly placing the morality of Hojo’s characters well and truly in the grey area.  
1981年首次亮相的《猫眼》是北条的成名作。这部作品讲述了三姐妹从事艺术品盗窃的故事，从表面上看，这是一个大胆的反派故事，具有无所顾忌的态度。但更多的是，《猫眼》和现实生活一样，是一个浸透着双重性的故事。瞳，是大姐(译注:应为二姐)，也是她们犯罪行动的主力，她爱着负责将她们绳之以法的无能警察。当笨拙的警察俊夫花更多的时间去追求他的死对头而不是他的爱人时，瞳也表现出令人难以置信的嫉妒心，她甚至对自己的另一个自己感到不满。这些太过真实的情感是《猫眼》成功的秘诀，牢牢地将北条的人物道德观置于灰色地带。  

Arguably Hojo’s most famous work, City Hunter, we meet characters that we can’t help but fall in love with, precisely because they are wading around in this murky grey area. We have Ryo Saeba, a killer and borderline sex offender tasked with righting the wrongs within Tokyo’s seedy underbelly. His ever suffering partner Kaori, a tomboyish moody character, is constantly rebuffing Ryo’s advances, yet secretly exhibits dismay when ignored by him. In a perfect example of life imitating art, fans of the manga started leaving XYZ calling cards, the means of contacting the City Hunter agency in stations around Tokyo, further exhibiting how Hojo expertly crafts characters that resonate on a personal level.  
可以说，在北条最著名的作品《城市猎人》中，我们遇到的人物，正是因为他们在这阴暗的灰色地带中游走，我们才会情不自禁地爱上他们。冴羽獠，一个杀手、时常游走在性骚扰的边缘，他的任务是纠正物欲横流的东京腹地的错误。他的搭档香，是一个男人婆、很情绪化，经常挫败獠的挑逗，但当她被獠忽视时，却又暗自表现出失望。漫画迷们在东京各车站都留下了城市猎人的联络方式--XYZ留言板，这是生活与艺术的完美结合地的一个例子，进一步展现了北条独具匠心的人物角色是如何让人产生共鸣的。  

![](./img/IMG_20180406_170622-220x300.jpg)  

City Hunter’s spiritual successor, Angel Heart is a mark of maturity, both for the author and his creations. Set in an alternate reality, where Kaori’s heart is transplanted into an assassin driven to suicide for the lives she has taken, signifies a turning point in Hojo’s work. Yes, Ryo is still as rampant as ever, but in place of feverishly exerting wanted/unwanted sexual advances, we have a paternal figure who quickly falls into a father role by caring for his 15 year old ward. ‘Glass Heart’, the recipient of Kaori’s heart, herself on a mission of redemption, exhibits a will to crawl out of her murky past and reinvent herself as better person. With this sense of responsibility, coupled with the incessant gnawing of guilt, Hojo abandons the urges of youth and places his characters firmly in the modern age.  
《城市猎人》一脉相承的作品--《天使心》是一个成熟的标志，无论是对作者还是对作者的创作来说，都是一个成熟的标志。故事发生在另一个世界，香的心脏被移植到一个因杀人而被迫自杀的杀手身上，这标志着北条作品的一个转折点。是的，獠还是一如既往地嚣张，但他不再狂热地(有意/无意地)展示性挑逗，我们看到了一个父亲的形象，他通过照顾15岁的被监护人，迅速进入了父亲的角色。'玻璃心'是香的心脏的接受者，她自己也肩负着救赎的使命，要摆脱自己阴暗的过去、重塑更好的自己。带着这种责任感，再加上被内疚感不断地折磨，北条摒弃了年轻时的冲动，将人物牢牢地置于现代社会中。  

With themes covering everything from questionable morality in the pursuit of doing the right thing to transgender issues and a need for acceptance, as featured in Hojo’s spectacular manga Family Compo, the veteran manga creator brilliantly and expertly holds up a mirror to reality. We are neither good, nor evil. We all make mistakes and when we do, we strive to make them right. We live in a grey area but are always searching for acceptance and a sense of home. Tsukasa Hojo weaves exciting stories with one eye firmly fixed in reality, giving us a body of work that transcends the pages they were illustrated on and reassures of our own shortcomings. In a word, we are not “perfect”.  
从"追求做正确事情的是否道德"，到"跨性别问题"，再到"需要被接受"的问题，北条在引人入胜的漫画《非常家庭》中所涉及的主题涵盖了这一切。这位资深漫画家出色而专业地为现实生活举起了一面镜子。我们既不是好人，也不是坏人。我们都会犯错，当犯错时，我们会努力去改正。我们生活在灰色地带，但一直在寻求被接纳、一直在寻求家的感觉。北条司编织着一个个精彩的故事，始终不脱离现实，作品超越了漫画本身、也让我们对自己的不足感到欣慰。一句话，我们并不 "完美"。  

The legacy of City Hunter lives on in Monthly Comic Zenon.  
城市猎人遗留给我们的东西还继续活在《Zenon漫画月刊》上。  

![](./img/IMG_20180514_170300-225x300.jpg)  

### MANGA  

Space Angel – 1979   
Ore wa Otoko Da! – 1980  
三級刑事 – 1981  
キャッツ♡アイ – 1981  
Cat’s Eye (キャッツアイ) – 1981 – 1984  
Space Angel – 1982  
City Hunter XYZ – 1983  
City Hunter Double Edge – 1984  
Cat’s Eye – 1985 I  
City Hunter (シティーハンター) – 1985  
ねこまんまお変わり♡ – 1986  
Splash! – 1987  
Splash! 2 – 1987  
天使の贈りもの – 1988  
Splash! 3 – 1988  
Splash! 4 – 1989  
Taxi Driver -1990  
Komorebi no Moto de (こもれ陽の下で…, Under the Dapple Shade) – 1993–1994  
Rash!! – 1994–1995  
Family Compo (ファミリー・コンポ) – 1996–2000  
Angel Heart (エンジェル・ハート) – 2001–2010  
Angel Heart 2nd Season – 2010-2017  



NEXT WEEK: I get my Shojo on as I trip into the fantastical world of Sailor Moon with Naoko Takeuchi!  
Twitter – [@chris_smac](https://twitter.com/chris_smac)  
Facebook – [Chris Smac](https://www.facebook.com/chris.smac.585)  