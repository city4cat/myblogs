
source:  
https://www.manga-audition.com/takehiko-inoue-hojo-tsukasa-manga-master-and-apprentice-exclusive-report/

[![](./img/eyecatch_20170126-1.jpg)](https://www.manga-audition.com/wp-content/uploads/2017/01/eyecatch_20170126-1.jpg)  

## Takehiko Inoue & Hojo Tsukasa : Manga Master and Apprentice – Exclusive Report!  
## 井上雄彦 & 北条司：漫画师徒 - 独家报道！

Penmaru, 27/01/2017, 5 min read  
唐泽和也,27/01/2017, 5 min read

Tsukasa Hojo-sensei and Takehiko Inoue-senesi, two legendary manga creators known around the world for their intense storytelling.  Two manga creators who were once master and apprentice.  The two sensei’s were brought back together by Monthly Comic ZENON for a rare double interview!  What sort of truly memorable conversation ensued between the two manga legends and former master and apprentice?  SMAC! THE WEB MAGAZINE brings you a small look into Hojo and Inoue-sensei’s long relationship.  
北条司老师和井上雄彦老师，两位以故事性强而闻名世界的传奇漫画家。两位漫画创作者曾是师徒关系。此次《月刊漫画ZENON》将两位老师再次请到一起，进行了一次难得的双重采访! 接下来，两位漫画传奇人物、曾经的师徒之间会有怎样真正难忘的对话呢?  《SMAC!网络杂志》为您带来了北条和井上老师的长久关系的小故事。  

[![](./img/1485420886809-02.jpg)](https://www.manga-audition.com/wp-content/uploads/2017/01/1485420886809-02.jpg)  
 

### Hojo-sensei and Inoue-sensei talk about City Hunter  
北条老师和井上老师谈《城市猎人》  

“I would leave the studio at 1 AM and come back in the morning and find BB’s in the blinds.” -Hojo-sensei  
"我会在凌晨1点离开工作室，早上回来发现百叶窗里有BB弹。" -北条老师  

Hojo-sensei and Inoue-sensei’s conversation began with Inoue-sensei remembering the time he spent studying under Hojo-sensei in 1988.  At that time City Hunter was in full weekly production, a very busy time for Hojo and Inoue-sensi.  Busy, but there was always time for fun.  “I would leave the studio at 1 AM and come back in the morning and find BB’s in the blinds.” Hojo-sensei remembered.  Inoue-sensei recalled one of the sempai (senior) assistants would go overboard after a few drinks a would shoot the BB gun’s in the assistant’s room.  A common occurrence when the deadlines were near.  
北条老师和井上老师的对话，首先是井上老师回忆起1988年跟随北条老师学习的时光。 那时候每周都在全力制作《城市猎人》，对于北条和井上老师来说，是非常忙碌的一段时间。虽然忙，但总有时间找点乐子。"我会在凌晨1点离开工作室，早上回来发现百叶窗里有BB弹"。北条老师回忆道。 井上老师回忆说，有一位前辈（高级）助理喝了几杯酒后就会很过分，会在助理的房间里打BB枪。 在临近最后期限的时候，这是经常发生的事情。  
 

### Inoue-sensei’s first Manga  
井上老师的第一本漫画  

“Take-chan, you still lived in Kichijoji when you went independent right?” -Hojo-sensei  
"小雄，你独立门户的时候还住在吉祥寺吧？" -北条老师  

Hojo and Inoue-sensei’s conversation moved on to when Inoue-sensei branched off independently.  Hojo-sensei took the opportunity to say, “Take-chan, you still lived in Kichijoji when you went independent right?”  Kichijoji is a town full of memories for Inoue-sensei, some good, some not so good.  Inoue-sensei recalled, “Kichijoji was where I was told my first series would be discontinued.”  Shocking words, but Inoue-sense found relief in them because in his heart wanted to create a basketball manga.  Like a true master and apprentice, Hojo and Inoue-sensei both agreed sometimes a manga creator can only make a series he thinks is interesting.  
北条和井上老师的话题转到了井上老师独立门户的时候。 北条老师趁机说："小雄，你独立门户的时候还住在吉祥寺吧？"  吉祥寺对于井上老师来说，是一个充满回忆的城市，有些好，有些不好。 井上老师回忆说："吉祥寺是我被告知我的第一个连载被终止的地方。"  这话听起来让人震惊，但井上老师却从中找到了解脱，因为在他的内心深处，想要创作一部篮球漫画。 就像真正的师徒一样，北条和井上老师都认为有时候一个漫画创作者只能做一个他认为有趣的漫画连载。  
 

### Create manga for different people  
为不同的人创作漫画  

“I create manga for people like me.” -Inoue-sensei  
"我为像我这样的人创作漫画。" -井上老师  

Hojo and Inoue-sensei then agreed a manga is brought to life through the readers and the two thought about who their readers were.  “I create manga for people like me.” Inoue-sensei said.  A little vague for some people, but he never creates a manga for himself.  And Hojo-sensei?  He creates manga for his wife, manga that would make her cry.  Hojo-sensei’s wife isn’t the type of person to read manga regularly, so showing her the story first is Hojo-sensei’s chance to gauge the reaction of those type of readers.  
北条和井上老师然后一致认为，一部漫画有了读者才有生命力，两人就想到了自己的读者是谁。"我为像我这样的人创作漫画"，井上老师说。 对某些人来说有些模糊，但他从不为自己创作漫画。 而北条老师呢？ 他为他的妻子创作漫画，会让她哭的漫画。北条老师的妻子不是那种经常看漫画的人，所以先把故事拿给她看，北条老师正好借此机会估计这类读者反应。  
 

### The sensei’s changing creative processes  
创作过程的变化  

“The manga NAME is the frame and the art is the manga’s body.” -Inoue-sensei  
"漫画NAME是框架，艺术是漫画的主体。" --井上老师  

（注：我猜，这里的manga NAME（漫画NAME）可能指漫画的主题。例如，漫画连载每一章有一个标题，该标题概括了这一章的主题。）  

As Hojo and Inoue-sensei’s conversation progressed they thought about how their creative process evolved.  For Hojo-sensei, as he aged creating the manga NAME has become enjoyable and inking difficult.  Not because he older, but because his experience in constructing a story made him lose a bit of his sense of wonder for the story, thinning the emotional content.  A different point of view came from Inoue-sensei.  For Inoue-sensei, the final art is where the creators plays and the NAME is the framework for the manga.  Inoue-sensei eloquently stated, “The manga NAME is the frame and the art is the manga’s body.”  
随着北条和井上老师的谈话，他们思考了自己的创作过程是如何演变的。 对于北条老师来说，随着年龄的增长，创作漫画NAME变得愉快，而把它画出来却变得困难了。 并不是因为他年纪大了，而是因为他构建故事的经验让他失去了一点对故事的惊喜感，淡薄了情感内容。井上老师提出了不同的观点。 对于井上老师来说，最终的艺术效果是创作者发挥的地方，而NAME是漫画的框架。 井上老师精辟地指出："漫画NAME是框架，艺术是漫画的主体。"  



### Humor in manga  
漫画中的幽默  

“They want to breath when tense scenes continue.” -Hojo-sensei  
"当紧张的场面持续下去的时候，读者就会想喘口气。" -北条老师  

Hojo and Inoue-sensei may have disagreed about the creative process, but both sensei’s agreed about mixing humor into serious stories.  Hojo-sensei was still young when he realized “People can’t stand a heavy atmosphere in a story for to long.  They want to breath when tense scenes continue.”  It was the movie Japan Sinks that led Hojo-sensei to believe this.  Learning from this Hojo-sensei always adds humor to his emotional stories.  To Hojo-sensei studied way of including humor, Inoue-sensei believes comedy should come about naturally.  It’s almost as if Inoue-sensei pokes fun at himself to keep from becoming embarrassed if a serious scene continues to long.  
北条和井上老师或许在创作过程中存在分歧，但两位老师都同意在严肃的故事中加入幽默感。 北条老师还很年轻的时候就意识到"人们不能长时间忍受故事中沉重的气氛。紧张的场面持续下去，人们就会想喘口气。"  是电影《日本沉没》让北条老师相信了这一点。 学会了这一点的北条老师总是在他的情感故事中加入幽默。 对于北条老师研究的加入幽默的方式，井上老师认为喜剧应该是自然而然出现(在故事里)的。 这几乎就像是井上老师的自嘲一样，为的是防止严肃的场景持续时间过长而变得尴尬。  
 

### It’s all about how manga is read  
这都是漫画的阅读方式问题  

“I read manga magazines outside a candy shop when I was a kid.” -Inoue-sensei  
"我小时候在糖果店外看漫画杂志。" -井上老师  

Hojo and Inoue-sensei’s conversation came to a close with Hojo-sensei asking Inoue-sensei about his manga roots.  “I read manga magazines outside a candy shop when I was a kid.” Inoue-sensei recalled.  Hearing this Hojo-sensei mused how he’d love to see his apprentice create manga that are read like that forever.  It may be difficult to write shonen comics as both sensei’s get older, but they will keep finding ways to make manga that are enjoyed outside a candy store.  
北条和井上老师的谈话进入了尾声，北条老师向井上老师询问了他的漫画根源。 "我小时候在糖果店外看漫画杂志"。井上老师回忆道。 听了这句话，北条老师思索着，他很希望看到自己的徒弟能创作出让人永远读下去的漫画。 随着两位老师的年纪越来越大，可能会很难画少年漫画，但他们会一直想办法创作出让糖果店以外的人喜欢的漫画。  


---------------

It’s not often we get to hear the story of a manga master and apprentice.  
我们很少能听到漫画师徒的故事。  

The bond between master and apprentice lasts a life time in the manga world.  Hojo-sensei has taken in many apprentices since he first became a pro in the 1980s.  Now he’s taking on new apprentices from around the world with SMA MASTERCLASS members.  We believe you have it in you to become the next star apprentice of Hojo-sensei to!  
在漫画界，师徒之间的感情是一辈子的。 自上世纪80年代第一次成为职业漫画家以来，北条老师已经收了很多徒弟。 现在，他和SMA MASTERCLASS成员一起，从世界各地收了新的徒弟。 我们相信你有能力成为北条老师的下一个明星学徒!  
 

Please show your worth, by joining & start creating for SILENT MANGA AUDITION® today!  
请展现您的价值，今天就加入SILENT MANGA AUDITION®并开始创作吧!  

