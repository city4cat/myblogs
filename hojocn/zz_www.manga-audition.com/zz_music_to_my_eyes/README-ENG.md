
source: https://www.manga-audition.com/music_to_my_eyes_tsukasa_hojo/  
（Here is [the Chinese version](./README.md)）  

![](./img/h_img_01_Interview.jpg "SILENT Manga is "Music to my Eyes", "Message to those entering the audition", Interview with Tsukasa Hojo")

# Tsukasa Hojo Interview : “Music to my Eyes”  

Taiyo Nakashima 
23/02/2015 
18 min read 

Tsukasa Hojo sensei gave us some strong words of advice and some insight into the art of creating beautiful manga scenes. All those thinking of drawing for the audition, please consider this as his endorsement and encouragement in drawing your next piece.  

With incredible artistic and storytelling skills, Tsukasa Hojo has been at the forefront of the manga industry for decades. His works are well-received all over the world, leaving strong impression and influencing many artists. In his capacity as a judge of the 1st round of **SILENT MANGA AUDITION®**, he gave us overall comments and some valuable advice for aspiring artists.  

**“Music to my Eyes” interview with Tsukasa Hojo sensei on SILENT MANGA.**  

![](./img/DSC_6441-681x1024.jpg)  


After reading through the 26 winning pieces again as a compilation, what kind of impression were you left with? 

>    Hojo: Well, they were a lot easier to read through than some of the entries for domestic Japanese manga competitions. I suppose that’s because there are a lot less distractions when you read them. With a normal short story, it might be difficult to understand the writer’s complex messages, or the story might jump around a bit too much. These things can distract you. But with SILENT MANGA, the approach is much more direct, straight to one’s emotions. Sometimes simple really is best.  

It was a surprise, to be honest, that we were able to understand manga from all over the world. The authors may have completely different languages, nationalities and cultures, yet their messages still get across.  

>    Hojo: It’s sort of like with music and songs. I’m one of those guys who doesn’t really listen to the lyrics. Sometimes I think the lyrics are actually in the way. The beauty of the melody, the rhythm and the sound itself is often more than enough for me to feel the emotions and the storyline. Music without lyrics might not work to explain a complex fictional world, but you don’t need lyrics to move someone. That’s how I felt about SILENT MANGA.  

That is so true. Even without dialogue, the characters’ movement and their “acting” is enough to move someone emotionally.  

>    Hojo: I guess that’s manga’s original strength, making the reader feel like they’re listening to a good piece of music, but via drawing. Classics like Tezuka sensei’s New Treasure Island†1 and The Adventures of Tintin†2 were almost completely silent. Another one that left a really deep impression on me was Tom & Jerry†3. It’s a cartoon, not a comic, but it originally had no speech, only music and movement. When I was a 3rd year elementary school student, we happened to watch the original version of Tom & Jerry in film club. That was a great experience for me. I noticed then that someone had added dialogue to the version that they broadcast on TV, and I remember thinking “Wow. It’s actually a lot better without the dialogue!”  

So manga’s strength lies in its dynamic scenes? It plays out like a movie?  

>    Hojo: You could say the opposite as well. A static scene can also be effective. You can do that with just a single picture, like in the grand prize winner “Sky Sky”. That piece hardly had any action. It used static moments to create scenes and emotions. Even without dialogue, this is quite do-able.  

At the final selection meeting, they said it was almost like a heartwarming picture book that makes you feel safe and secure when you read it.  

>    Hojo: I thought that the story was excellent. On the other hand, the other winning story from Indonesia, “Excuse Me”, had very good dynamic energy. It used multiple frames that built upon each other to show changes in the character’s emotional state. As a manga artist, when I’m trying to decide if someone is good or not, this is what I look for. If someone who’s bad at directing tries to do this, you won’t know what’s going on. It was only an episode of a girl who got a love letter and ran to the toilet, but because it was done in manga style, it was really fun!  

 
![](.//img/DSC_6375-681x1024.jpg)  
Manga is about showing both the “dynamic” and the “static” side of a story  

 

Indeed, I did get the impression that most of the winners took their time in paneling their art to express their characters’ emotions.  

>    Hojo: I believe young Japanese authors also want to draw their manga like that. But because of time constraints, they tend to make do with adding more dialogue and close-ups. Like by using a big final panel and finishing with an impact.  

There are certainly a lot of new artists who say “Drawing all those detailed scenes takes way too long!”  

>    Hojo: It’s a lot simpler just to do it with dialogue and close-ups. But for example, say you have a fight scene and you want to make the hero strut his stuff. If you use an entire page with the line “Bring it on!” then I think there’s just too little information. You have to show him and his opponent glaring at each other, show how he takes his jacket off and folds it perfectly, make him gesture with fingers as if to say “come on!” If you do all that, it’s a lot more interesting! The readers will notice these things and they might say something like “why the heck does he have to fold up his clothes!?” And that’s how you create character traits.  

So one of the basics of manga is how you have your characters “act”.  

>    Hojo: Also, this is really typical of old fight scenes, but the hero flips a coin and while the bad guys are staring at it, he moves like lightning and knocks them all out. Then as the coin lands, you see them all lying on the floor. I think that the accumulation of scenes like that, is how you make your manga flow like a piece of music.  

It’s also necessary to employ a variety of slow and fast-paced scenes. This gives the manga its own unique pace and rhythm.  

>    Hojo: That’s quite right. Perhaps due to the influence of anime, young authors nowadays only think about the fast scenes. Manga is about utilizing both “static” and “dynamic” scenes to mesmerize the reader.  

Apart from that, were there any of the works that left an impression?  

>    Hojo: The “Winner runner-up” award piece from Jordan, “A Pure Love”, was extremely well drawn, but when I first read it, I thought it was a little hard to understand. The main character is struggling with her work and her pet rabbit cheers her up, but the heroine’s expressions of frustration were so strong, that in comparison the rabbit’s actions seemed rather weak. It didn’t really feel like she would be cheered up. Maybe a memorable episode, a unique moment with the rabbit somewhere in the storyline might have made the reader feel more involved. Perhaps author’s feelings for her pet are so strong that it becomes difficult for her to have an objective point of view.  

So if you put too much of yourself in the main character, then it becomes a little difficult to see your work objectively.  

>    Hojo: If you draw only from your own point of view, you become complacent and this can derail your story. Of course, there is a certain pleasure in drawing manga only for oneself, so I can’t say that this is a hard-and-fast rule, but if you are drawing for a magazine and you want to please your readers, then I think it’s really important to view your work from above, like an impartial god.  

On that note, I’d like to ask you, how does one go about creating a story for your manga? This is something that people from overseas often ask, and I think it’s what the winners most want to know.  

>    Hojo: To begin with, I never start from the story. First, I think of some interesting characters and an interesting situation. Like in Cat’s Eye†4, I started from the idea “Wouldn’t it be interesting if a thief and a detective were married to each other?” If you’re drawing a non-serialized manga, then you have to come up with everything at once, since you have to include the beginning and the ending in a single chapter. So you generally come up with the characters and the story at the same time. And very often, the stories development and conclusion come out naturally. But in the case of a weekly manga, you have a long period of time to complete the story, so I don’t think about it much at first.  

So first you create interesting characters and by using them, the story comes about naturally. Sort of like that?  

>    Hojo: When you have the right characters, then there’s no need to come up with a story. If you understand the characters and you know how they think, you just plug them into a situation and see what they do. Your characters will take the bait, so you just need to put them down on paper. At first, you might have a certain story in mind, but then you start thinking “this character wouldn’t do this” and then your story will diverge from its original course, and it’s all for the better. It will turn out more interesting that way. When I use this method, I’m often left amazed, thinking “Oh, I didn’t see this coming!”  

That’s the real pleasure of weekly serials, right? And that way of creating the story might be unique to Japanese manga.  

>    Hojo: Certainly the “weekly” part is significant. Most people won’t work if they don’t have a deadline! Back in the day, when there were only monthly magazines, I think a lot was left to the artist. The editors didn’t really give much input. But once the weekly magazine was born, there were more magazines, competition became fiercer, new artists were given the chance, people were forced to get better, and editors started helping out. The Weekly Shonen Jump†5 definitely stands out here. In the beginning, I was a complete amateur who didn’t know the first thing about making manga. My editor, Mr. Horie, taught me the ABCs of manga when I first started drawing serials. Having one’s work exposed to the entire country week after week is pretty embarrassing, but in the beginning, I sort of had the attitude “Nothing can scare me anymore. Do your worst!”  

![](./img/DSC_6447-681x1024.jpg)  
After 35 years of drawing, he has never left the front line of manga creation.

     

To keep a weekly serial going, or in other words, to keep the readers happy, I think you really need to use every trick in the book.  

>    Hojo: Well, if you don’t use a few tricks, it’s not very interesting, is it? One of the fun parts of drawing manga is experimenting as you go along and discovering new techniques. There is no “right answer” in manga, and there are as many ways to draw as there are people. If you tried to teach “right answers” at a manga school and forced them onto the students, then it would be very sad. Experimenting is half the fun of drawing manga. Also, people think that if your time is limited, you can only do shoddy work, but that’s not true. In fact, one might say that if you have too much time, then you can’t collect your thoughts properly. Limited times means “Keep it simple” is the golden rule, and manga artists at the time all improved their skills through accumulation of trial and error. I think this is how the manga of today was developed.  

Do you have any other advice that you would like to give the winners?  

>    Hojo: This is something that can also be said to new Japanese artists: this is more a way of thinking than a drawing technique, but first you have to decide where your character is and what kind of position they are in, otherwise you really can’t draw a scene. To determine the lighting, you have to consider various factors, like if there is a window where the light is coming in. If it’s night, then they might be illuminated from above or there might be indirect illumination. It’s not possible to start drawing until you have all of this set-up in your mind.  

So first you decide on the scene and then you start thinking about the composition, angles and the movements of the characters. That actually sounds like shooting a film. In your case, how many cameras would you be using?  

>    Hojo: I guess it would be four or five fixed cameras plus a mobile camera on a crane. The one that would be able to zoom in from above or film from a low angle.  

So we assume that we have those cameras in place and then we consider the composition and angles that would construct the scene, that expresses a character’s feelings.  

>    Hojo: It’s more instinctive in a sense, but yes, that’s what it’s like. If the character is apologizing, then you can’t shoot from a low angle looking up, right? You shoot from a low angle when the character is reclining on a bench, throwing his head back and saying something like “Obey me, you little creature!”  

You often tell new artists to watch Akira Kurosawa’s movies†6 and Charlie Chaplin†7. Is this so they can pick up techniques for communicating a characters feelings through images?  

>    Hojo: There’s that as well, but the “black and white” nature of those films is the point. Most likely, the young artists of today have never seen the beautiful clarity and contrast of black and white films. Think of it like drawing a picture with your color palette converted into shades of grey. I’ve wondered why the pictures in recent manga have become so indistinct and I think it’s because young artists don’t know about the world of shading, the beauty of contrast in a monochromatic view.  

Because the pictures in manga have evolved and gradually become more realistic, I sometimes get the impression that the pages feel rather dark and heavy.  

>    Hojo: It’s because there’s no light source there. You need to use proper shading that supports your characters. A white background is okay if it helps to express emotion. Even if you change the light source to support the tone, it will still feel natural. But, if there’s no light source in the panels, an artist will feel scared of leaving white spaces, and so they tend to draw in a lot of unnecessary background detail.  

Well then, lastly, as a representative of the judging panel for SILENT MANGA AUDITION®, do you have a message for the winners and for those who are planning to enter the competition in the future?  

>    Hojo: Manga is actually a really inconvenient form of media. There are no colors, no movement, no voices and no music. Yet you must express all those things, through drawing only. This is the challenge that makes it so interesting and fuels my desire to draw more. Because everyone competed with each other and developed new methods, it has evolved into something truly unique, despite being so inconvenient. But there is still room for it to develop further and there is a kind of joy in making your own discoveries. So for the winners and those who are planning to enter, I want you to take a step outside of Japan’s world of anime and manga, add some of your own culture and customs and draw something that is unique and distinctive of your part of the world. Through this competition, Japan’s manga formula will continue to evolve all over the world, and I’m looking forward to something completely new being born. I guess for that to happen, there’s still a lot of work to be done.  

>    Do your best! We will.  

![](.//img/DSC_6412-681x1024.jpg)  


Translation by Andrew Le Reux and Taiyo Nakashima.  

Original published in December 2013, as the final words to the first ever compilation of SILENT MANGA AUDITION, 2013.  

 

Tsukasa Hojo Profile :  
Born on 5th March 1959 in Kokura City (Currently Kita-Kyushu City), Fukuoka Prefecture. In 1979, he won 2nd place at the Tezuka Award with Space Angels, and in 1980, he debuted with Ore wa otoko da! (“I am a man!”) in the Weekly Shonen Jump (Shueisha). The following year, CAT’S EYE was very successful. After that, CITY HUNTER was a huge hit not only within Japan, but overseas as well. In 2000, he founded Coamix together with his former editor Nobuhiko Horie. In 2001, he began publishing Angel Heart in the newly launched Magazine Weekly Comic Bunch (Shinchosha). At present, he is writing Angel Heart Season 2 for Monthly Comic Zenon (NSP/Tokuma Shoten) while also putting effort into training new manga artists and editors.  

[![Screen Shot 2015-02-25 at 8.51.06 PM](https://www.manga-audition.com/wp-content/uploads/2015/04/Screen-Shot-2015-02-25-at-8.51.06-PM-1024x777.png)](http://www.hojo-tsukasa.com)  
Tsukasa Hojo Official Site http://www.hojo-tsukasa.com  


![](./img/DSC_6466-1024x681.jpg)  
His trusty tools  


References :  

†1 New Treasure Island  
(Original script and composition by Shichima Sakai, artwork by Osamu Tezuka) An original comic book released in 1947. For Tezuka, it was his first long story, and the work through which he made a name for himself. Next to the manga of that time, which were generally short and simple, this extraordinary 200 page adventure story became a bestseller. Its movie-like pictures, which seemed almost to be moving, became a topic of conversation among the children of the time. Fujiko Fujio, Shotaro Ishinomori, Tetsuya Chiba and many others were deeply influenced and decided to become manga artists. It was known as the post-war starting point of Japanese manga.  

†2 The Adventures of Tintin  
A bandes dessinées (a style of comic from France/Belgium) drawn by Hergé from Belgium in 1929. It’s the story of a young reporter named Tintin who, together with his white dog Snowy, travels all over the world, has many adventures and gets involved in various incidents. It has been translated into more than 80 languages and worldwide sales exceed 350 million copies.  

†3 Tom and Jerry  
An animation that has received many academy awards within America. A slapstick comedy about Tom, a cat and Jerry, a mouse, which incorporates a lot of silly humor. In Japan, TBS Corporation started broadcasting it in 1964 and it has been continually re-run due to its popularity. In this Japanese version, voices were added, but in the original, there were almost no voices, only narration, screams, sound effects and music.  

†4 Cat’s Eye  
One of Tsukasa Hojo’s works. It was his debut serial, published on Weekly Shonen Jump from 1981 to 1984. It’s the story of a group of female art thieves called “Cat’s Eye”, which is made up of the 3 Kisugi sisters Hitomi, Rui and Ai. The detective who is after them, Toshio Utsumi, is also Hitomi’s fiancé and is unaware of their identity as Cat’s Eye. In 1983, it was adapted into a TV anime, drama, live action film and novel. It is also popular within Asia and Europe.  

†5 Weekly Shonen Jump  
A weekly manga magazine published by Shueisha. When it was originally launched as Shonen Jump in 1968, it was published twice a month. The following year, it changed to Weekly Shonen Jump and was published once a week. Due to it starting later than other shonen manga magazines, it had difficulty in securing popular manga artists, so it made it a policy to train up new talent. In this way, they created a strong core of artists. 45 years after its initial launch, this policy is still going strong and Weekly Shonen Jump has the highest sales of any shonen manga magazine. The magazines catchphrase is “Friendship, Effort, Victory”.  

†6 Akira Kurosawa  
1910 – 1998. A master film maker whose name is known all over the world. Having even influenced Steven Spielberg and George Lucas, he is considered one of the best film makers of all time. His first movie was Sanshiro Sugata in 1943. In 1951, his film Rashomon won the grand prize at the Venice Film Festival. In 1951, the Seven Samurai, with its cutting edge camerawork and action, took the world by storm with its horseback combat scene in the rain. Among others, Throne of Blood, The Hidden Fortress, Yojimbo and Sanjuro influenced many film makers of the black and white film period.  

†7 Charlie Chaplin  
1889 – 1977. A British actor and director, known as the King of Comedy. Moving to America, he appeared in several short comedy films and became popular. With his peerless skill for physical gestures, which were developed through pantomime, he created a character that represented both poverty and dignity. He left the audience not knowing whether to laugh or cry as he appealed to their sense of human dignity. Some of his notable works include The Gold Rush, City Lights, Modern Times, and Limelight. Osamu Tezuka loved and respected him just as much as he did Walt Disney.  
