source: 
https://www.manga-audition.com/manga-insider-mayu_09/  


[![](./img/mim09_DSC04809.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04809.jpg)  

[MANGA LAB](https://www.manga-audition.com/category/manga-lab/)


# MANGA INSIDER MAYU #9 – THE SHOW AIN'T OVER YET!!  
# MANGA INSIDER MAYU #9 - 表演还没结束！！

[Mayuna Mizutani](https://www.manga-audition.com/wp-content/uploads/2018/06/prof_mayu-150x150.jpg) 
05/05/2015 11 min read  

Finally… FINALLY… The magazine is (almost) on sale!!  
终于... 终于... 杂志(几乎)在销售了！！  

[![](./img/mim09_DSC04739.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04739.jpg)  

Here’s my copy! See how thick it is!? Also, do you see the last pages are in different colors?  
这是我的copy！ 看它有多厚！ 还有，你看到最后几页颜色不同了吗？  

[![](./img/mim09_DSC_2001.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC_2001.jpg)  

These colored pages are all reserved for the rookie manga artists! It’s a ZENON tradition! Hot on print, you may think that our work is all over… Well, our job isn’t over! There’s still lots more to do to sell the magazines, so that we can create the next issue!  
这些彩色页面都是为新手漫画家保留的！ 这是ZENON的传统！ 热销中，你可能会认为我们的工作已经结束了... 好吧，我们的工作还没有结束！ 要销售杂志的话，还有很多事情要做，这样我们才能出下一期!  


## 1. Taking care of the original drawings  
1.保管好原画  

The original drawings we handed to the printing partners for scanning are sent back to us. Originals drawn on paper must be handled with lots of care. They are like the children of the manga artists – they are all equally precious! The drawings are all kept in a secret chamber, so that they don’t get stolen or damaged. Sorry guys, TOP SECRET!!  
我们交给印刷合作方扫描的原画被退回给我们。纸上的原画必须小心处理。他们就像漫画家的孩子——他们都一样珍贵!这些画都保存在一个秘密的房间里，这样它们就不会被盗或损坏。抱歉，这是最高机密!!  


## 2. Making/sending the unbound printouts (Galley prints)
2.制作/发送未装订的打印稿(Galley打印)  

After the publication, we send some unbound printouts – called “Galley prints” – to the manga artists who had their works published. In this way the artists can know what their works look like in the magazine. It is usually the editor apprentice’s job to make these “galleys”. Making the “galleys” are super simple, but also SUPER STRESSFUL! I always felt myself going slightly mad during the process.  
出版后，我们会发送一些未装订的打印稿——称为“Galley prints”——给那些作品被出版的漫画家。这样，艺术家们就可以知道他们的作品在杂志上是什么样子。制作这些“Galleys”通常是学徒编辑的工作。制作“Galleys”非常简单，但也非常有压力！我总是觉得自己在这个过程中有点疯狂。  

[![](./img/mim09_DSC_1997.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC_1997-1280x748.jpg)

Here is a sample of a colored page of a galley. It is cut out from an enormous sheet with the pages printed. It is our job to cut it out, and staple it with the other pages!  
这是一个画册的彩色页面的样本。它是从一个巨大的纸张上打印出来的。我们的工作是把它剪下来，并与其他页面订在一起！  

We usually send several copies to the artist, and keep one for the editor in charge. That keeps us busy cutting and stapling! The “galleys” need to be very neat. As said before, every manga on the page is all like a children of the artists! Be careful not to slice your finger off!  
我们通常会给艺术家发送几份副本，并保留一份给负责编辑。这让我们忙于剪切和装订！ “画册”需要非常整洁。正如之前所说，页面上的每个漫画都像艺术家的孩子！小心不要把你的手指切下来！  


## 3. Sending the complimentary copies  
3.发送赠阅本  

You may already know that a magazine is created thanks to the contributions of so many people… of course we have the manga artists and the editors, but we also have designers, the printing partners, the sponsors, the advertising agencies who advertise our magazines… the publication would be impossible without them! So, to notify them that our magazine is done, and to show our thanks to them, we send all the partners a copy of the completed magazine. This is also done by the editor apprentices.  
你可能已经知道，创建一本杂志多亏了这么多人的贡献......当然我们有漫画艺术家和编辑，但我们也有设计师、印刷合作方、赞助商、为我们的杂志做广告的广告代理商...... 没有他们，就不可能出版！因此，为了通知他们我们的杂志完成了，并向他们表示感谢，我们向所有合作伙伴发送了一份完成的杂志。这也是由学徒编辑完成的。  

[![](./img/mim09_DSC04735.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04735-1280x960.jpg)  

The magazines are usually brought to us a few days before it is on sale. Look at all the copies on this wagon! We have to manage about five of these every month!  
杂志通常在上市前几天送到我们这里。看看车上的复印件!我们每个月要处理5车这样的东西!  

[![](./img/mim09_DSC04742.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04742-1280x960.jpg)  

Comparing the size to a normal pen, you can easily see that there are tons of magazines here!!  
与普通的一摞相比，你可以很容易地看到这里有大量的杂志!!  

[![](./img/mim09_DSC04736.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04736-1280x960.jpg)

Oh boy, here’s another pile of the magazines!  
哦，天哪，这又是一堆杂志！  

[![](./img/mim09_DSC04748.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04748-1280x960.jpg)  

We tuck the magazine into an envelop like this…  
我们把杂志像这样塞进信封里。  

Look! The envelope has the name “ZENON” on it!  
看！信封上有“ZENON”的名字!  

[![](./img/mim09_DSC04753.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04753-1280x848.jpg)  

All these magazines are sent to every partner we worked with… Better send it by the sales day!  
所有这些杂志都寄给了我们合作过的每一个合作伙伴……最好在销售日之前寄出去!  

## 4. Write a blog  
4.写博客  

The editor’s blog is now the best way for the editors to communicate with the readers! So, on the sales day, the young editors write an editor’s blog on our official website.  
编辑的博客现在是编辑与读者沟通的最好方式!所以，在销售日，年轻的编辑们在我们的官方网站上写一篇编辑博客。  

[![](./img/589359a4b3f1c2dea3c4410de8ecd346.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/589359a4b3f1c2dea3c4410de8ecd346-1280x843.png)  
http://www.comic-zenon.jp/blog/    

The blog is about the highlights of the magazine- like the popular titles, the up-coming manga, and various extras. The secret of grabbing the reader’s attention is to be personal sometimes. Gee, there’s so much for me to learn from our blog!   
博客是关于杂志的亮点--像流行的作品、即将到来的漫画、和各种额外的。吸引读者注意力的秘诀是有时要有个性。哇，我可以从我们的博客中学到很多东西!  


## 5. Research at books stores
5.书店调研  

Have you ever seen a Japanese book store? It is definitely a marvelous thief of time, where you can spend all day! The stores are full of effort put into the layout, with lots of signage and stuff! The book stores reflect the current state of the society very well, so even just walking around helps editors to identify current trends. That’s why editors often visit these stores, especially when the magazine goes on sale, to know how much the magazine was able to follow the trend, and get the idea of what the readers are wanting! What is your local book store like?  
你见过日本的书店吗?这绝对是一个奇妙的消磨时间的地方，在那里你可以度过一整天!商店在布局上投入了很多精力，有很多标牌和东西!书店很好地反映了当前的社会状况，所以即使只是四处走走也能帮助编辑识别当前的趋势。这就是为什么编辑们经常访问这些商店，特别是当杂志开始销售时，了解杂志能够跟上潮流的程度，并了解读者想要什么!你们当地的书店怎么样?  

[![](./img/mim09_DSC04809.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04809.jpg)  

This book store has it’s own corner for the ZENON comic books!! Awesome! XD The board says: “Comic ZENON is a manga magazine created in Kichijoji”!  
这家书店有自己的ZENON漫画书角落！太棒了！XD 板上写着：“Comic ZENON 是一本用吉祥寺创作的漫画杂志”！   

[![](./img/mim09_DSC04814.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04814.jpg)  

What a huge space! And we can see a lot of tips to sell the comic books…  
多么巨大的空间！我们可以看到很多销售漫画书的技巧......   

[![](./img/mim09_DSC04812.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04812-1280x1034.jpg)

Oh, they have our Silent Manga Audition book too!  
哦，他们也有我们的 Silent Manga Audition 书！  

[![](./img/mim09_DSC04810.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04810-1280x1221.jpg)  

Here they are exhibiting some post cards, to show off the glamorous drawings by the author!  
在这里，他们展示了一些明信片，以炫耀作者的迷人画作！  

[![](./img/mim09_DSC04808.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04808-1280x960.jpg)  

Such a cute pop-up ! Pushuuuuuu…  
如此可爱的pop-up！Pushuuuuuu…  

[![](./img/mim09_DSC04807.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04807-1280x960.jpg)  

This book has a sample comic for the consumers to read before buying! It’s effective for selling a serialized manga, as it introduces the first part of the story. Oh, and you can’t ignore the cute sign in front of it either!  
这书有一本漫画样本，供消费者在购买前阅读！它对于销售连载漫画很有效，因为它介绍了故事的第一部分。哦，你也不能忽视它前面的可爱标志！  

[![](./img/mim09_DSC04805.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04805-1280x1009.jpg)  

Ah, here’s my favorite manga at ZENON… It also has a sign pasted on the shelf. A summary of the story is written on it!  
啊，这是我在ZENON最喜欢的漫画......它还在架子上粘贴了一个标志。故事的摘要写在上面！  

[![](./img/mim09_DSC04802.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04802-1280x913.jpg)  

Gourmet mangas are so popular in Japan! Mmm, this curry signage makes me hungry…  
美食漫画在日本非常受欢迎！嗯，这个咖喱标牌让我饿了......  

[![](./img/mim09_DSC04798.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04798.jpg)  

There’s even a poster on the side of the shelf! There is no such thing as wasted space in a book store!  
架子的侧面竟然还有一张海报！书店里可不能浪费空间！  

[![](./img/mim09_DSC04791.jpg)](https://www.manga-audition.com/wp-content/uploads/2015/05/mim09_DSC04791-1280x1011.jpg)  

Oh, here’s our magazine! Looks like it’s selling well! I’m glad.  
哦，这是我们的杂志！看起来卖得很好！我很高兴。  

Now, the publication of ‘Monthly Comic ZENON’ is finally over!  We repeat this whole process from the meeting to the things above, every month. Next week, I will be answering some questions from you guys! If you have anything you want to know, please let me know in the comments!   
现在，《月刊漫画 ZENON》的出版终于结束了！ 我们每个月都会重复从会议到上述事情的整个过程。下周，我将回答你们的一些问题！如果您有任何想知道的事情，请在评论中告诉我！    


[#MANGA INSIDER](https://www.manga-audition.com/tag/manga-insider/) 
[#Mayu](https://www.manga-audition.com/tag/mayu/) 
[#MIM](https://www.manga-audition.com/tag/mim/) 
