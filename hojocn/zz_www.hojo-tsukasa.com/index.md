(注：本目录下的访问入口页面为：[./readme.md](./readme.md))

http://www.hojo-tsukasa.com/index.html

https://web.archive.org/web/20010707004742/http://www.hojo-tsukasa.com/index.html


注：该页面更新至2002年06月10日。后续数据待整理。  

<a name="top"></a>    
# 2001年07月03 ～     

![](./img/headder.jpg)  
![](./img/title.gif)  
![](./img/s_title1.gif)  


  
## CITYHUNTERクリスタルべ-バ-ウェイト発売!
[![](./img/whatsnew/paper_weight.gif)](https://web.archive.org/web/20020610080843/http://www.coamix.co.jp/)  
**４月１日正午発売開始!  **　  
北条司作品にはなくてはならない名脇役たちを、限りなく透明なクリスタルの中に封じ込めました！ 出荷限定220個で発売開始します！[[申し込みはCOAMIXのページから]](https://web.archive.org/web/20020610080843/http://www.coamix.co.jp/)

  
## AngelHeart单行本第3卷3月8日(金)発売!!
[![](./img/whatsnew/bc_angelheart03.gif)](https://web.archive.org/web/20020610080843/http://www.coamix.co.jp/bunchcomics/bc_200112.html)  
　リョウとグラスハートに殺人部隊が迫る！ 戦場と化す新宿の街、青龍との戦いの火蓋が切って落とされた！一方青龍の陰謀に玄武の制裁が発動する！緊迫の第３巻！

「AngelHeart」３巻／Ｂ６／本体価格505円＋税

## [AngelHeart单行本第2卷12月9日(日)発売!!](https://web.archive.org/web/20011211165220/http://www.coamix.co.jp/comic/bc_200110.html)  
![](./img/whatsnew/bc_angelheart02.gif)  
　危険すぎる街の中、ようやく再会するリョウとグラス・ハート。しかし、衝撃！の瞬間、闇の武装集団が忍び寄る。ストーリーは加速度をつけて動き出す……。

「AngelHeart」２巻／Ｂ６／本体価格505円＋税

  
## [オンデマンドカレンダー発売！](https://web.archive.org/web/20011211165220/http://www.d-pub.co.jp/c_kobo/coamix/index.html)  
[![](./img/whatsnew/od_calen.gif)](https://web.archive.org/web/20011211165220/http://www.d-pub.co.jp/c_kobo/coamix/index.html)  
厳選されたカラーイラストと月を自由に組み合わせることができるオンデマンドカレンダー。表紙にはあなたのお名前、さらに記念日を１０個まで自由に設定することができます。いよいよ卓上カレンダーもスタート！ [[申し込みはこちらから]](https://web.archive.org/web/20011211165220/http://www.d-pub.co.jp/c_kobo/coamix/index.html)

## ページリニューアルのご挨拶    
Page Renewal的问候  

　お陰様で、「週刊コミックバンチ」も、順調な出足でスタートを切ることができました。愛読者の皆様には、感謝の気持ちでいっぱいです。本当に心よりお礼申し上げます。  
　 さて、私も「週刊コミックバンチ」の創刊！ということで、まずは新連載に専念するため、ホームページもしばらく、休止させて頂いておりましたが、新連載も動き始め、ここにホームページも再開できる運びとなりました。  
　 僕も、出来る限りですが、メッセージを書き込んでいけたらと思っております。（忙しい時はごめんね！） まずは、ホームページ再開！よろしくお願いいたします。  
托您的福，「周刊ComicBunch」也顺利地开了个好头。我对各位读者充满了感谢之情。在此表示衷心的感谢。  
那么，我也要创刊「周刊ComicBunch」了!因为这样，首先为了专心于新连载，主页也暂时休止了，不过，新连载也开始动了，这里主页也能重新开始了。  
我也想尽可能地写留言。(忙的时候对不起了!)首先，重新开始主页!请多关照。  

北条司

  
### [「CITY HUNTER」ZIPPO 7月17日販売開始決定!](./project/project.md)  
[![](./img/zippo_fukuro.jpg)](./project/project.md)  
今回北条司が描き下ろした、北条司オリジナルZIPPOが完成しました。  
7/17日より、オンライン販売を開始いたします。ご期待ください！

  
### [「AngelHeart」週刊コミックバンチにて好評連載中！](https://web.archive.org/web/20010707004742/http://www.coamix.co.jp/)  
[![](./img/whatsnew/angelheart_image.jpg)](https://web.archive.org/web/20010707004742/http://www.coamix.co.jp/)  
2001年、史上最強の暗殺者が、日本にやってきた。美しすぎる人間兵器だった。彼女のコードネームは「グラス・ハート」。  
この作品は、漫画家生活２０周年を超えた北条司が渾身の力で世に問う！男と女の壮絶なドラマである！

  
### **「BunchWorld」全国コンビニにて絶賛発売中！**  
![](./img/whatsnew/bw_fcon02.gif)   
**毎週水曜日**発売 定価**286円**＋税  
「ファミリーコンポ」シリーズ スタート！

![](./img/whatsnew/bw_hyousi.jpg)  
6/27 「シティーハンター」13巻  
7/4 「キャッツアイ」 7巻  
7/11 「シティーハンター」14巻  
7/18 「キャッツアイ」 8巻  
7/25 「シティーハンター」15巻  
8/ 1 「キャッツアイ」 9巻  
8/ 8 「シティーハンター」16巻  
8/22 「キャッツアイ」 10巻  
**毎週水曜日**発売 定価**286円**＋税  



  

## ![Mokkori更新信息](./img/s_title2.gif)  
![](./img/icon.gif)CITYHUNTER クリスタルペーパーウェイトの情報を追加！（2002.3.28）  
![](./img/icon.gif)オンデマンドカレンダー締め切り迫る！(2002.3.18)  
![](./img/icon.gif)AngelHeart 第３巻発売中！(2002.3.8)  
![](./img/icon.gif)[CITYHUNTER オンデマンドカレンダー販売開始！](https://web.archive.org/web/20011211165220/http://www.d-pub.co.jp/c_kobo/coamix/index.html)（2001.10.1）  
![](./img/icon.gif)速報、AngelHeart単行本情報(2001.9.11)  
![](./img/icon.gif)総集編情報・メディア出演情報追加(2001.7.28)  
![](./img/icon.gif)[CITYHUNTER ZIPPO販売開始！](./project/project.md)（2001.7.17）  
![](./img/icon.gif) [リンク](./links/links.md)を更新しました！（2001.7.16）  
![](./img/icon.gif)掲示板が復活しました！（2001.7.3）  
![](./img/icon.gif)CITYHUNTER ZIPPOの情報を追加！（2001.7.3）  
![](./img/icon.gif)サイトを大幅リニューアル！（2001.7.3）

[[PAGE TOP]](./index.md#top)


---  


<a name="2010-08"></a>
# 2010年08月 ～     
![](./www.hojo-tsukasa.cust.jp/mtadm/mt-static/themes/minimalist-red/main.jpg)  
[![](./images/home/banner_zenon.jpg)](https://web.archive.org/web/20141221222144/http://www.comic-zenon.jp/)   
  

# ニュース

...  

[アーカイブ](./archives.md)  （注：待整理）