http://www.hojo-tsukasa.com/about.html

https://web.archive.org/web/20010410220213/http://www.hojo-tsukasa.com/about.html


<tr bgcolor="#FFCC33"> 
<td colspan="2">
<table width="700" border="0" cellspacing="0" cellpadding="0">
    <tbody>
      <tr> 
        <td rowspan="2" width="400"><img src="./img/hojoheader_01.gif" width="400" height="100"></td>
        <td width="300"><img src="./img/hojoheader_02.gif" width="300" height="50"></td>
      </tr>
      <tr> 
        <td width="300"><img src="./img/hojoheader_about.gif" width="300" height="50"></td>
      </tr>
    </tbody>
</table>
</td>
</tr>



## 「いい漫画を創つていきたい！」そう思っていた者たちがー同に集いCoamixは誕生はしました！  
「想创作出好的漫画!」这样想的人们聚集在一起，Coamix诞生了!  

　Ｃｏａｍｉｘの社是は「漫画事業に専念する」「漫画の発展に寄与する」、ただこれだけです。漫画は、世界に誇れるメディアであり文化であると思います。ただ私は、ここ最近の漫画状況に不安と不満があります。漫画はこれほど面白かったのだと、むしろ人を元気にさせる懐かしさがある漫画が必要ではないでしょうか？

  

　だからこそＣｏａｍｉｘは、２１世紀に向けた漫画事業における新しいシステムやルールをつくりあげ、創作力の触媒となるべき編集者の質を高めながら、いろんな雑誌へ良質なコンテンツを供給していくことを目指します。

  

　（株）Ｃoaｍｉｘ 代表取締役社長　堀江信彦

  
[![](./img/banner.gif)](./artroom/artroom01.md)  
  

## Coamix Inc. Company Data

【設立】

平成１２年６月１４日  
新潮社をパートナーとし、漫画家の原哲夫、北条司、次原隆二、声優の神谷明、 前「月刊少年ジャンプ」編集長の根岸忠らが参加し、会社を設立。

【代表者】

代表取締役社長　堀江信彦  
昭和５４年、集英社に入社。平成５年、 「週刊少年ジャンプ」編集長に就任。史上最高部数を記録。平成８年「メンズノ ンノ」、平成９年「ＢＡＲＴ」編集長を歴任。平成１２年５月退社。現在４５ 歳。

【資本金】

８２００万円

【社員数】

２０名

【事業内容】

漫画雑誌編集、アニメーション制作、キャラクター事業、版権事 業、ネット販売事業など。

【住所】

〒１８０―０００３  
東京都武蔵野市吉祥寺南町２―６―１０富士パームビル６ Ｆ  
ＴＥＬ　０４２２-４０-１０５１  
ＦＡＸ　０４２２-４０-１０５２

  

## Recruit  

![](https://web.archive.org/web/20010410220213im_/http://www.hojo-tsukasa.com/img/recruit_a.gif)

新潮社が今春創刊する新コミック雑誌『コミック・バンチ』に描いていただく漫画家を広く募集いたします！

[詳細はこちら](./recruit_a.md)

## 來たれ！漫画編集スタッフ！

(株)新潮社が今春創刊する新コミック雑誌『コミック・バンチ』の編集スタッフを大募集します！  
ともに２１世紀の「編集王」をめざしましょう!!

[詳細はこちら](./recruit_e.md)