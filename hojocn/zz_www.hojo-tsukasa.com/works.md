http://www.hojo-tsukasa.com/works.html  

https://web.archive.org/web/20010411000456/http://www.hojo-tsukasa.com/works.html


<tr bgcolor="#FFCC33"> 
<td colspan="2">
<table width="700" border="0" cellspacing="0" cellpadding="0">
    <tbody>
      <tr> 
        <td rowspan="2" width="400"><img src="./img/hojoheader_01.gif" width="400" height="100"></td>
        <td width="300"><img src="./img/hojoheader_02.gif" width="300" height="50"></td>
      </tr>
      <tr> 
        <td width="300"><img src="./img/hojoheader_works.gif" width="300" height="50"></td>
      </tr>
    </tbody>
</table>
</td>
</tr>

![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/80.gif)

![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/otoko.jpg)

![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_otoko.gif)  

週刊少年ジャンプ  
'80年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/otoko.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/81.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/3rd.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_3rd.gif)

原作：渡海風彦  
週刊少年ジャンプ  
'81年（短編）  
[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/third.html','works','width=540,height=370'))

　 ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/cats.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_cats.gif)

週刊少年ジャンプ  
'81年～'85年

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/cats.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/82.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/space.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_space.gif)

週刊少年ジャンプ  
'82年（短編）  
[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/space.html','works','width=540,height=370'))

![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/83.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/ch_xyz.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_ch_xyz.gif)

週刊少年ジャンプ  
'83年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/ch_xyz.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/84.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/ch_de.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_ch_de.gif)

フレッシュジャンプ  
'84年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/ch_de.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/85.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/ch.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_ch.gif)

週刊少年ジャンプ  
'85年～'91年

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/ch.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/86.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/nekomanma.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_neko.gif)

週刊少年ジャンプ  
'86年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/neko.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/88.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/tensi.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_tensi.gif)

週刊少年ジャンプ  
'88年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/tensi.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/90.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/taxi.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_taxi.gif)

週刊少年ジャンプ  
'90年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/taxi.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/92.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/family.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_family.gif)

週刊少年ジャンプ  
'92年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/family.html','works','width=540,height=370')) 　 ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/shoujo.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_shoujo.gif)

週刊少年ジャンプ  
'92年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/shoujo.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/93.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/sakura.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_sakura.gif)

週刊少年ジャンプ  
'93年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/sakura.html','works','width=540,height=370')) 　 ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/komorebi.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_komorebi.gif)

週刊少年ジャンプ  
'93年～'94年

![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/94.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/rash.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_rash.gif)

週刊少年ジャンプ  
'94年～'95年

![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/95.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/aozora.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_aozora.gif)

原作：二橋進吾  
週刊少年ジャンプ  
'95年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/aozora.html','works','width=540,height=370')) 　 ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/shounen.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_shounen.gif)

週刊少年ジャンプ  
'95年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/shounen.html','works','width=540,height=370')) 　 ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/american.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_american.gif)

原作：二橋進吾  
週刊少年ジャンプ  
'95年（短編）

[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/american.html','works','width=540,height=370')) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/96.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/fcompo.jpg) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/parts/blank_bk.gif)![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_fcompo.gif)

マンガオールマン  
'96年～'00年  
[![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/shousaidata.gif)](javascript:OpenWin('works/fcompo.html','works','width=540,height=370'))

![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/97.gif) ![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/parrot.jpg) 北条司短編集  
![](https://web.archive.org/web/20010411000456im_/http://www.hojo-tsukasa.com/img/works/t_parrot.gif)  
'97年6月号～  
コミックとＣＧの素敵なコラボレーションで作り上げた一冊。  
集英社『メンズノンノ』と『ＢＡＲＴ』 で連載された北条司がデジタル時代におくる新感覚ラブストーリー。北条司が、すべての「恋愛中毒者」に捧げる大人の恋愛テキストブック！