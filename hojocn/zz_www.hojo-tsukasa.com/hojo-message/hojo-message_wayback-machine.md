source:  
http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi  
http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi?function=loglist  
http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi?function=log&logno=3
http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi?function=log&logno=2
http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi?function=log&logno=1  

https://web.archive.org/web/20100801000000*/http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi


（译注：Wayback Machine上没有2010.02.05之后的"北条司Message"的存档了。虽然2010年以来最早的一次存档为2019.12.11，但显示www.hojo-tsukasa.com此时已改版，没有"北条司Message"这个页面了。  ）

## 2010  

### 銃撃戦の跡も  2010.02.05 FRI 17:26:51　  
海上保安庁に北朝鮮の工作船が展示されているのを見た。  
船体は弾痕で穴だらけになっていて、生々しくて怖かった。

  

### 見れば見る程  2010.01.22 FRI 15:44:21　  
仕事場の男子トイレのマークが、ミュージシャンのＳＥＡＭＯのシルエットに見える…。

  

### 新年のご挨拶  2010.01.08 FRI 15:20:15　  
あけましておめでとうございます。今年も宜しくお願いします。

## 2009

### 時は流れて   2009.12.24 THR 16:24:07　  
どうやら30周年らしいです。来年だけどね。

  

### 活気が溢れていました  2009.12.04 FRI 15:38:28　  
近所で「全国農業高校収穫祭」をやっていた。野菜の安さと高校生の元気さに驚いた。ちなみに第１回だそうです。

  

### 壮絶な復讐劇  2009.11.20 FRI 15:46:25　  
舞台の「蛮幽鬼」を見てきました。楽しかったです。

  

### その理由は？  2009.11.06 FRI 16:12:58　  
里帰りしたらお肌がツルツルに…。仕事のストレスから逃れたせい?

  

### 季節を感じるひととき  2009.10.23 FRI 17:57:38　  
最近、街中にキンモクセイがの香が溢れていていいですね。秋だなぁって感じがします。

  

### 昔から言われ続けているようですが  2009.10.09 FRI 19:57:56　  
「最近の若者はなっとらん」って言う人がよくいるけど、だいたいが「お前が言うか…」みたいなヤツが言うんだよなぁ。…俺の周りだけかもしれないけれど――。  

  

### 夏も終わってしまいましたが  2009.09.18 FRI 15:50:54　  
今年の夏が冷夏だったせいか、通常５月頃に花を咲かす藤の花が、７月の終わりから再び咲き始めて、９月まで咲いていた。  
花も季節を勘違いしちゃってるんだろうな。


（译注：不确定2009.02.21～2009.09.17之间是否有留言。  ）  


### 興味がふつふつとわいてきます	2009.02.20 FRI 18:06:35　
毎日夕方から夜、路上にスウィーツを自転車で売りにくるお兄さんがいる。
見ているとお客さんとの会話が丁寧で好感が持てる。
店とかあるんだろうか？ なんで自転車なんだろうか？　
とか彼のバックグラウンドが色々気になる。

### マスクの効果	2009.02.13 FRI 18:22:17　
電車に乗っていて気づいたんだけど、咳をしている人に限ってマスクをしてない。
咳してない人はしてるのに。マスクをしている人は風邪をうつされたくなくてしているのかも。でもこれって逆じゃねーか?

### 朝でも容赦なし…	2009.02.06 FRI 16:53:57　
初めて通勤時間に電車に乗った。
電車に乗る前に、改札口の段階で二人（女性）に突き飛ばされた。
通勤ラッシュ恐るべし。

### 舞台もいいものですね	2009.01.30 FRI 16:03:27　
前橋の中学生が僕の短編「少年たちのいた夏」を演劇にしてくれて、そのＤＶＤを送ってくれました。とても楽しく見ました。

### 寝起きにうっかり	2009.01.23 FRI 15:37:21　
朝寝ぼけててヘアトニックをつけるつもりがスキンローションを…。スキンヘッドの人は毎日これつけてるのかなぁ？ 


## 2008  

### こんな所にも影響が	2008.12.26 FRI 16:05:05　
レストランで食事中、親子連れに遭遇した。
メニューの漢字が読めない子どもに父親が一言。
「空気と漢字が読めないやつは首相になっちゃうぞ」。
ちょっとうけた。

### ちょっと変わった例え方	2008.12.19 FRI 16:13:41　
かかりつけの医者で毎年恒例の血液検査をした。血圧も一緒に計ったのだが、「女子高生みたいな血圧ですね」と言われた。
それっていいのか悪いのかよく分からん。で、なんで男子高生じゃないの!?

### どう聞こえますか？	2008.12.05 FRI 16:12:50　
今年話題になった言葉、水泳・北島選手の「なんも言えねえ」。
ラジオやテレビの放送で何度聞いても「なんも言えない」に聞こえる。
なんか納得いかねえ。 

### 青空に鳩と言えば	2008.11.28 FRI 16:32:20　
朝ベランダで煙草を吸うのは爽快で気持ちがいい。
そこから見える公園の上空を鳩が集団で飛びまわっているのも
とてもいい風景なのですが、頭にはロート製薬の音楽しか浮かんでこなくて…。

### かなりさっぱりしましたね…	2008.11.21 FRI 16:02:33　
周りの人間が皆大好きだった大銀杏。
その枝が、すべて切られて幹だけになってしまっていました。残念。

### 街はゴミ捨て場なんでしょうか？	2008.11.07 FRI 22:30:13　
道を歩いていたら、歩き煙草をしている恰幅の良い部長クラスの方がいた。
彼は吸っていた煙草を気取った感じで指でピンと弾いて捨てた。
すごく格好悪かった。 

### 外れる事もあるけれど	2008.10.31 FRI 11:33:38　
みんな天気予報をあまり気にしないのかなぁ。雨が降っているのに傘を持たずにあたふた人が多すぎる気がする。

### 秋の花も咲いています	2008.10.24 FRI 16:28:29　
朝顔に続いてコスモスが咲いたのですが、直径２センチぐらいです。

### ちょっと遅れての開花	2008.10.17 FRI 15:00:04　
10月に入ってようやく朝顔が咲きました。

### 季節はもう秋になっているのですが	2008.10.03 FRI 15:34:07　
朝顔が未だに咲きません…が、葉っぱは元気。
ベランダに出た時、目に入る葉っぱのハートマークが可愛い。 

### 残念なお知らせ	2008.09.26 FRI 15:45:58　
種から育てたコスモスが、夏の暑さにやられて半分枯れてしまいました…。

### 早い、早いよ	2008.09.19 FRI 16:22:30　
9月に入ったとたん、ハロウィンのディスプレイってどうなんだ？いいかげんにしろ！冗談じゃない!!

### 感覚の違い？	2008.09.05 FRI 20:31:08　
女の子って「ディズニーランドに行ったことない」って言ったら、なんであんなに驚くんだろうね？ 

### もう9月になるんですが	2008.08.29 FRI 20:12:33　
朝顔が今年もまた咲きません…。咲くのは秋かな……。

### リンク集ページに追加があります	2008.08.25 MON 14:11:34　
ＨＰのリンクが増えたので、よろしければ見てください。

### かなりもったいない	2008.08.04 MON 14:27:19　
10年ぐらいに前に取材で行った古い図書館に久しぶりに行ったら、リニューアルしてて新しくなってました。味わいのあるイイ雰囲気だったのにブチ壊しでした…。 

### 報道のあり方	2008.07.18 FRI 23:27:09　
日本のニュース番組って異常だね。声優さんを使ったり、効果音を入れておどろおどろしくしてみたり、演出に凝ってばっかりでワイドショー化している。

視聴率とれれば、なんでもイイって考え方なんだろうね。

### まさに百花繚乱	2008.07.14 MON 21:26:01　
たまに路地裏を通って出勤したりしてるんですが、どこの家にも季節の花が咲いていて、すごくキレイです。路地裏全体が花でいっぱいなんです。

写メを撮ったりして、楽しんでます。

### 賑やかな所が好きなんでしょうか	2008.07.07 MON 15:36:49　
新宿に行った時、燕を見ました。吉祥寺ではめっきり見ることがなくなったので、びっくりしました。逆に新宿の方がビルがいっぱいあって、巣とか作りやすいんですかね。 

### それは甘美な響き？	2008.06.20 FRI 16:39:07　
編集部で一夫多妻制がブームみたいです。でも俺は否定派です。賛成の人は女性のことを知らなすぎる。死んじゃうよ。冗談じゃない！

### 何でも調べられるけれど	2008.06.13 FRI 19:55:25　
ネットの某百科事典に自分のことで根も葉もないことを書かれてました。……でも、めんどくさいのでそのままです。信用できないもんですね。 

### 感謝！	2008.06.06 FRI 18:11:44　
5月17日のサイン会に来てくださった皆様、本当にありがとうございました!!

### そちらで北条司が飲んでいるそうですが	2008.05.26 MON 14:09:28　
久々に会った知り合いに「最近、練馬に行った？」と聞かれました。どうやら練馬の飲み屋に「北条司」だと名乗って、飲んでる男が出没しているみたいです。

練馬のみなさんそいつニセモノですよ。

### 広がる捜査網	2008.05.19 MON 14:28:51　
何回か流れてない大便の話を書きましたが、編集部でも何度か発見されたらしい…。話題騒然です。

### 証言者が現れました	2008.05.09 FRI 17:15:15　
前回に引き続き、流れてない大便の話です。仕事をしているとバンチの目次コメントを見た某アシスタントが「僕も一回ありました」と言いにきました…。

みんなちゃんと流そうよ！ 

### どんな対応策を取ろうか	2008.04.25 FRI 20:06:15　
今の事務所に引っ越して2年半経ちますが、その間2回も流してない大便に遭遇しました…。流そうとしたけど、流れなかったモノではなく、流す意志を全く感じない状態なんです。

３回目は貼り紙を貼ろうと考えています。

### どう思っているんだろう	2008.04.18 FRI 17:45:22　
カミさん以外の知り合いの女性と歩いている時に限って、道端で編集の人に会います……。誤解されてんだろうな…。

### 先述の物件ですが	2008.04.11 FRI 17:44:22　
この前、仕事に向かう途中、古いラブホテルから出てくるカップルと鉢合わせしました。目がバッチリ合ったんですが、武士の情けで見て見ぬフリ……。

昼間からお盛んだなぁ～っと、思いました。

### 春の風物詩ですが	2008.04.04 FRI 13:56:10　
桜が咲きました。今年も花見はしないでしょう。

### 意外と賑わっているようです	2008.03.28 FRI 16:11:05　
出勤途中に昔からある小さいラブホテルがあります。こんな昼間から誰が入るんだろうって、思ってたらけっこういるんですね、入ってる人たち…。週末なんて2回に1回ぐらいの割合で目撃します。

不思議です。

### 哀愁漂う…	2008.03.14 FRI 16:30:26　
冬に外を歩いてて、一番多い落しモノは…手袋だと思います。しかも片方だけ落ちているので、それがまた悲しいです。

ちなみに夏は軍手がよく落ちてます。

### いつもそうではないらしい。	2008.03.07 FRI 18:04:42　
地元の丸井の辺りを大声で、演歌をずっと歌いながら歩いている50歳ぐらいのオジサンがいます。そのオジサンが先日、知り合いが働いているケーキ屋さんにお客として来たみたいなんですが、とても礼儀正しく普通の人だったらしいです。意外な一面を見た気分です。

ちなみに「スティックケーキ」を一本買っていったみたいです。 


### カメレオンめ…	2008.02.29 FRI 19:18:17　
義理の母に「エンジェルハート」の最新刊が出来るたびに送っています。いつも携帯のメールで読んだ感想が来るんですが、25巻の感想が「カメレオンけしからん！」でした。

すっごいおかしかったです。

### 次に見られるのはいつ？	2008.02.15 FRI 11:31:59　
夜中にショーウインドウを鏡代わりにして、フラメンコを踊っている女性がいました。独特なスカートを履いていて、結構本格的でした。しかし冬になる前にいなくなったので、もっとしっかり見ときゃ良かったな、と後悔してます。

早く暖かくなって見せてほしいです。

### 約7833万kmの彼方で	2008.02.08 FRI 21:11:41　
この季節は星がキレイですね。火星が明るく輝いて見えます。

そういえば、火星にかなりの確率で隕石が衝突するという発表をNASAがしてました。衝突予定日まで残り2～3ヶ月みたいな感じで……。わかってから衝突までの期間って案外短いですよね。これが地球だったら、世の中どうなるんだろ？って思いました。

### 色々な要因が重なって	2008.02.01 FRI 17:28:15　
今年は金曜の夜でもタクシーの空車待ちが目立ちます。去年はタクシーを拾うのも一苦労で、空車なんてありえなかったのに……。これも「値上げ」「禁煙」の影響なんですかね……。

### 深夜の街を颯爽と	2008.01.25 FRI 15:33:00　
仕事帰りの夜中に2、3人～5、6人の自転車に乗った女性達を見かけます。聞こえてくる会話は「アフターどうする？」「ムシムシ」といった内容です。この女性達を僕は「キャバクラ銀輪部隊」と呼んでいます。

ちなみに冬は見かけません。春から秋にかけての風物詩のようなモノです。

### 見上げてみれば	2008.01.18 FRI 14:22:01　
この季節は星や月がキレイなので、上を見てることが多いです。周りの人からしたら、不思議でしょうね。

ちなみに金曜日は酔っ払いのゲロとかがあるので、下に注意しながらでないと危なくて歩けません。踏んじゃったら落ち込むもんね……。

### 音楽のススメ	2008.01.10 THU 16:33:13　
伊倉一恵さん（CITY HUNTER /Angel Heart 槇村香役）がここ数年すごく力を入れて応援している根岸弥生さんというピアニストがいる。
どのような活動をしている人なのかは（一音入魂音楽館）というHPをリンクしたのでそちらをみてください。
家族は何度もコンサートに行ってもう家族ぐるみで応援する側にまわっているんだけど、俺だけ…時間が…
俺も早くウワサの素晴らしい演奏を聴きたいよ。

### 一体何が！？	2008.01.04 FRI 14:08:30　
仕事帰りのある夜、酔っ払った若い女の子が道路の真ん中で「人生最悪の日ーッ！」と泣き叫んでいました。

どんなに不幸な事があったんだろう……。 

## 2007  

### 意外な発見	2007.12.10 MON 22:02:04　
夜、仕事を終えて帰ると、昼間では気づかない色んな事があります。最近、特に気になっているのが、道理標識の影が街灯の光でハンマーに見えることです。それを見るたびに「100t」って書きたい衝動にかられます…。 

### もう秋なんですけど	2007.11.30 FRI 15:18:47　
ベランダの朝顔がまだ咲いてます。紅葉が始まっている公園を背景に朝顔を見るのは変な感じです。そういえばこの前、里帰りした時、コスモスの中にヒマワリが咲いていて変でした…。

異常気象のせいですかね…。

### そう言えば自分も…？	2007.11.22 THU 14:13:22　
前回、「イタいオバサン」のことを書きましたが、大学生の頃から20年、服装が全く変わってない俺が、もしかしたらイタい人になってるんじゃないかと…。

イタかったらゴメンね。

### 着る人にもよるけれど	2007.11.16 FRI 16:51:44　
最近、変に若作りしている「イタいオバサン」が多いな、と思っていたら、そのオバサンたちが見本にしているであろう雑誌を見つけました。しかもモデルがやってても痛々しいんです。普通のオバサンだと、なおさらです…。オジサンにも「チョイ悪」を意識しすぎた人は多少はいるけど、オバサンの方が圧倒的に多いです。

年相応の格好が一番だと思います。 

### 実はその他にも	2007.11.09 FRI 22:54:08　
この前「エンジェル・ハート」というカクテルの話をしましたが、実は「リョウ」「香」「ＸＹＺ」というカクテルもその店にはあります。Ｃ・Ｈが終わった、ちょっと後ぐらいからあるので、もう随分古いメニューになります。

### それは天使の心臓	2007.10.26 FRI 15:23:00　
場所は言えないが、行きつけのバーで「エンジェル・ハート」というカクテルを作ってもらいました。白色のカクテルの中にチェリーが沈んでいて、うすボンヤリと赤が見えるんですが、それがハート（心臓）を表しているのである（笑）。

ちなみに甘くて飲みやすいが、度数は強めなので、飲みすぎに注意…。試飲中に酔っ払ってしまいました……。

### もう一色、咲きました	2007.10.19 FRI 15:29:09　
この前の白い朝顔に引き続き、もう一色の朝顔もキレイに咲きました。時期も時期ですし、あきらめていたので嬉しかったです。ちなみに紫色だと思っていたら、青色でした……。これはこれで、とてもキレイで良かったです。

### 秋も中盤なんですが	2007.10.12 FRI 15:05:48　
以前、言っていた咲かない朝顔がやっと咲きました。白色と紫色の2色のうち、白色だけですが見事に咲きました。枯れかかってるし、ダメかなぁと思っていたので、良かったです。また不思議な事にベランダいっぱいにツタがはびこる夢を見た次の日に咲いたんです。

こんなことってあるんですね。

### どっちがメイン？	2007.10.05 FRI 21:47:50　
たまに若い人で鼻がなんのためについているか、知らない人がいます。鼻はニオイを嗅ぐためだけについているわけではなく、呼吸も鼻が基本なんです。口呼吸は体にとって良くないので、口で呼吸してる人は鼻呼吸をするようにした方がイイですよ。

ちなみに口をポカンと開けてる人はだいたい口呼吸の人です。

### 時代の流れなんでしょうか	2007.09.28 FRI 15:05:36　
最近のＦ‐1は盛り上がりに欠けます。昔のドライバーはもっとハートがありました。リスクを恐れず、勝負をして、見ていて興奮したものです。それが今は規制が厳しいこともあり、コース上で順位が変わることはほとんどなく、熱いバトルが見れません。

だんだん興味が薄れてきてます……。

### 首都圏も暴風雨でし	2007.09.14 FRI 15:04:46　
台風がきてました。子供の頃からなんですが、台風のニュースを見るとなぜだかワクワクします。東京に来て、久々の台風らしい台風で地元にいた頃を思い出します。昔は直径2ｍもある樫の木が倒れてたり、田舎のカヤブキ屋根が飛ばされそうになったりで、台風が来るととてもドキドキしてました。 

### 原因は一体…	2007.09.07 FRI 18:00:44　
家で育てている朝顔の花が咲きません。葉っぱは天井に達するぐらいスクスクと育っているのに、蕾すらならないのです…。やっぱり、異常気象のせいでしょうか？

このままじゃ「朝顔」じゃなくて「葉さ顔」だよ！

### 視点を変えてみると…？	2007.08.31 FRI 19:26:35　
よく「もう30歳だから…」とか「40歳なっちゃった」と自分のことを年寄り扱いする人がいますが、人生を損してるなぁと思います。40歳から見れば、30歳なんて子供だし、50歳から見れば40歳も子供なんです。そういう人は人生ず～っと年寄りってことになっちゃいますもんね。33

### 舞台が終わってからも。	2007.08.24 FRI 17:35:50　
3年越しでようやく「blast」のライブを観に行きました。幕間の時もメンバーによるパフォーマンスがあったりして、飽きることなく楽しめました。中でもすごいな、と思ったのは舞台が終わると帰るお客さんをメンバーが見送ってくれることです。ただ、最初から最後というわけではなく、最初の方に帰るお客さんだけなので、それを知っているリピーターの人達は舞台が終わると出口にブワーッと殺到するのです。やっぱり見送ってほしいですよね。

すごくおもしろくて、良かったです。 

### もう何年になるのか  2007.08.10 FRI 15:22:35　
そう言えば21歳で海に行って以来、約四半世紀、一度も泳いでません。取材なんかでグアムやハワイとか、海の近くには何度か行ってるのに泳いでません。海を見るだけで終わってます……。

### 雨上がりに見たもの  2007.08.03 FRI 16:06:45　
街中を閉じた傘を持ったまま、勢いよく手を振って歩く人っていますよね。とくにオバサンに多くて、危ねーよって思うんですが、わざわざ注意するのもシャクなんで、わざとぶつかってやります。もう少し周りに気を使おうよ。

### 雨の日の気遣い   2007.07.20 FRI 00:07:07　
「傘かしげ」。狭い路地や人混みなどで、傘をさしてすれ違うときに、互いに傘を少し斜めにさし合うことを言います。こういった相手を気遣う行為や粋な行いの出来る人を本来「江戸っ子」と言うみたいです……って、傘を傾けてるの俺だけじゃねーか！ 江戸っ子はどこいったんだ!?

### 言葉の魔力  	2007.07.13 FRI 00:07:10　
最近、流行の軍隊式ダイエット。でも、ラジオ体操を同じ時間しても痩せそうな気がします。それにちょっと前に流行った納豆ダイエットでも、「喰ってて痩せるわけないだろ」って思います…。

「痩せる」って言葉に弱い人が多すぎます。

### ちょっとした技能？	2007.07.06 FRI 00:00:22　
この季節は湿気のせいで紙の状態が悪く、下書きが大変になってきます。なので、漫画家になってから湿気には敏感になりました。乾燥した季節は雨が降るかどうか漫画を描いていてわかるぐらいです。

### その買い方って？	2007.06.29 FRI 00:13:54　
「大人買い」って言葉、変じゃありませんか？ だってあんな買い方をする人、大人じゃないでしょう？ あれは「大人買い」じゃなくて「子供買い」だよって、ふと思いました。

### 少し驚いたこと	2007.06.26 TUE 13:14:53　
最近、知り合いから聞いた話なんですが、正社員で27歳の男が仕事の事で注意されただけで、次の日から仕事に来なくなったらしいのです。そして、そのまま退職……。元々、使えないダメ男だったようなんですけど、ちょっとありえないですよね。

使えない、弱い男が年々増えてきてるような気がします……。

### 買いはしたものの…	2007.06.08 FRI 00:03:56　
一ヶ月前、買い物に行ったときにすごく高いお箸を買いました。別に高いモノが欲しかったわけではないんですが、「これイイなぁ」と思うやつに限って不思議と高かったです。結局悩んだ挙句、買ったんですが、いまだに一度も使えてません…。正月にでも使おうかな……。

### 季節も変わり	2007.06.01 FRI 00:00:20　
暖かくなってきたので、自転車でも久しぶりに乗ろうかな。でも、その前に被っているホコリを磨かないとですね…。

### 現在の体調	2007.05.25 FRI 00:00:09　
全身のコリがひどいです。どんなに強い力で指圧、マッサージされても、僕にとっては「イタ気持ちいい」でしかないです。なのでよく行く整体師さんに意地でも「イタイって言わせる」って言われました…。  

（译注：WaybackMachine上相邻的两次存档为：  
[2007-03-12的存档](https://web.archive.org/web/20070312231822/http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi)，其显示2005.10.04～2007-03-12无留言。  
[2007-08-13的存档](https://web.archive.org/web/20070312231822/http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi)，其显示了最早2007.05.25的留言。  
综合这两个信息，无法确定2007-03-13～2007.05.26之间是否有留言。   ）

（译注：[2007-03-12的存档 | WaybackMachine](https://web.archive.org/web/20070312231822/http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi)显示，2005.10.04～2007-03-12无留言。   ）

## 2006  


## 2005  

### 管理人よりお知らせ	2005.10.03 MON 18:06:50　
現在、週刊連載の他、アニメ化に伴う作業に集中しているため北条先生がもっこりメッセージを更新できない状態にあります。
更新を楽しみに待っておられる方々にはすみませんが、多忙を極める先生の状況をご理解いただければと思います。
一段落着き次第、是非またご登場頂きたいと管理人としても願っております。
今後ともコミックバンチの連載とアニメ放送をお楽しみ下さい!


## 2004  

### 大晦日	2004.12.31 FRI 19:18:39　
(译注：该留言似乎被删去了。以下源自[2005-02-09的存档 | WaybackMachine](https://web.archive.org/web/20050209142759/http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi))  
まもなく2004年も暮れようとしています。
今年は世界的にとても自然災害の多い年だった気がします。
被災地のみなさんにとっては大変苦労の多い
年末年始でしょう。
心よりお見舞い申し上げます。

来年がみなさんにとってよい年でありますように。 

### 暑い・・・これ読むと苛つくかも・・・	2004.08.12 THU 23:53:41　
(译注：该留言似乎被删去了。以下源自[2005-02-09的存档 | WaybackMachine](https://web.archive.org/web/20050209142759/http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi))    
暑いなぁ・・・
ホント
暑い・・・・
暑いよねぇ・・・
夏とはいえ暑いよねぇ
ねぇ、暑いよね
暑いもんねぇ・・・
眠ってても暑くて目がさめるもん
あぁ　暑い
去年の冷夏の復讐か・・・これは・・・
暑くない人っているのかなぁ
いるかもなぁ・・・・
暑いの好きな人もいるんだうなぁ
こんなにも暑いのに　この暑さが好きなのかぁ
すごいなぁ・・・
暑い
ひたすら暑い
暑いと思うから暑い
暑くないと思えば・・・・やっぱり暑いよ
暑い　暑い
とっても暑い
ひどく暑い
暑い
暑い
嫌がらせのように暑い

暑い・・・・


### そでネームの代原   2004.06.12 SAT 17:26:16　
(译注：该留言似乎被删去了。以下源自[2005-02-09的存档 | WaybackMachine](https://web.archive.org/web/20050209142759/http://www.hojo-tsukasa.com/cgi-bin/message/message.cgi))  
「仕事用の眼鏡を新調した。
　これで以前よりハッキリと手元が見えるようになったが、
　逆に手元以外はまるっきりボケボケ。
　人が仕事場に訪ねてきても、顔を上げると顔の判別もつか
　ず目眩すらする。
　だから執筆中は顔をまるっきり上げないようになってしま
　った。
　ただでさえ執筆中は、原稿に集中したいので人が来てもあ　　
　まり顔を上げずに対応していたのだが、これでますます、
　無愛想だとか、気難しいヤツだとか、思われるかもしれな
　いなぁ・・・・・」

上の文は、エンジェル・ハート１１巻のそでネーム（カバーの折り返しにある作者のコメント）の代原です。
今回のそでネームはいささかシモネタっぽかったので、ボツになるかもしれないなぁ・・・・と思って、代わりの文章を用意しておいたのだが、別に何も言われず採用されてしまった。
行き場を失ったこの代原。かわいそうなので、ここに書き込んでみました。　

### 横山光輝様    2004.04.19 MON 14:30:21　
物心つくかつかない頃から鉄人２８号の絵を模写していた。
粘土でもいろんなポーズの鉄人をつくっていた。
漫画の本など買ってくれない親だったが、なぜか一冊だけ鉄人の本を僕に与えてくれた。
主題歌のソノシートも誕生日プレゼントに買ってもらった。
友達と鉄人ごっこをして泥まみれになっていた。みんな鉄人のつもりで戦っていたから、しまいには本当のけんかになった。
鉄人は僕の漫画の原点、いや幼少時代の大部分を占めていた大切なものだった。

横山光輝先生　御冥福をお祈りします。



### ありがとです	2004.03.31 WED 01:29:43　
忙しくて、管理人に任せっぱなしで、なかなか自分のホームページを見る時間がない。
僕の誕生日にBBSへ皆さんからたくさんのお祝いが書き込まれている・・・との知らせを聞いて、お礼のメッセージを書かなくては・・・と思いつつ、ズルズルとこんなに時間がたってしまいました。
皆さん申し訳ありませんでした、そしてありがとうございました。立派な中年親父になりました。
もう三月も終わりか・・・・
どこから舞い込んだのか桜の花びらが足もとに・・・・

### 雪	2004.01.18 SUN 23:48:36　
雪・・・積もんないかなぁ・・・・

### 2004 新春　	2004.01.06 TUE 12:46:22　
明けまして　おめでとうございます

お久しぶりです。
私の事　覚えておいででしょうか？

今年もよろしくお願いいたします。 


## 2003  

### 虹の橋  2003.05.09 FRI 00:46:17　  
虹の橋の事を知ってますか?  
  
いま、A・H の舞台になっている都庁と新宿中央公園を結ぶ橋の事ではありません。  
  
ご存じの方も多いとは思いますが、ネット上で結構話題になっている童話　「虹の橋」　の事です。  
  
僕は、都庁の虹の橋についてネットで調べているときに偶然発見しました。  
何て事ない単純な短い話ですが、戦争中という事もあってか、ほろりときました。  
  
「虹の橋」で検索すれば、かなりの数のHPがあるので、もし興味のある方は、そのどれでもいいので（内容はみな同じようなものなので）見てみてはいかかでしょうか。  
  

  

### 祈願   2003.03.19 WED 12:05:29　  
  
  
  
  
  
　　　　　　　　　love & peace  
　  
  
  
  
　　　　　　　　　　　　　　　　　　　　　　　　　　  
　　　

  

### 告白  2003.03.02 SUN 19:29:59　  
夕べ・・・・いやもう今朝か?  
仕事場から家に帰って、ケーブルテレビのスイッチ入れたら、懐かしの「CITY HUNTER 2」の最終回をやっていた。  
  
ふーん・・・と思って眺めていたら、とあるシーンで「んッ」と目が止まった。  
  
すっかり忘れていたけど・・・・・  
  
当時、サンライズに遊びに（いやいや、打ち合わせの仕事で・・・かな?）行ったとき、スタッフに「最終回ですし、記念に原画なんて描いてみます?」と言われた。  
で、面白がって、一枚描いていたことを思い出した。  
  
そのシーンがまさに、その時俺が原画を描いたものだったのだ。  
  
もちろん、編集部には内緒で。あの頃の編集部って何だかとてもうるさくて、アニメスタッフと作家が会うことさえいい顔しなかった。そんな事する暇あったら、原稿を描けといった感じだったなぁ。だから、ずっと秘密にしてたけど、もう時効だよね。  
  
そうそう、そのシーンは当時のアニメ－ジュだか、ニュータイプだかの「今週の名シーン」ってコーナーで選ばれたっけ。俺に気を使って、スタッフが裏工作したんじゃないのかなぁ。  
  
そんな懐かしい絵と思いがけず対面して、妙に気恥ずかしい思いをしました。  
  
  
  

  

### う、うらやましい!!   2003.01.15 WED 12:44:46　  
去年の年末　「北斗の拳」のフルカラーの本が　コアミックスから出版された。  
その見本本を先日　見せてもらった。  
  
「北斗」のコミックス一巻分が　CGの着色で全ページ　カラーになっている。その色つけの仕方が　凝っていて感心!  
アニメーションのセル画のような色つけとは全く違う、ちゃんとした塗りなのだ。  
  
思わず　うらやましい　と思った。  
自分もこういう本を出したい・・・・と  
  
「北斗」のフルカラー版は　２巻３巻・・・と発売が予定されているが、  
「シティハンター」もそのフルカラー版を出す話が進んでいる。  
  
自分の白黒の本がカラーになる・・・・  
考えただけで　わくわくする。  
売れなくてもいいから、自己満足のためだけでも、この話を実現させたい・・・・  
などと思ってる、わがままな俺です。

  

### 2003  2003.01.01 WED 14:04:33　  
謹賀新年  
  
今年もよろしくお願いします  
  
２００３年元旦  
  
　　　　　　北条　司

## 2002  

### うそ・・・  2002.11.01 FRI 12:17:51　  

八月十五日!?  
前回の書き込みが!!　  
  
うそぉ・・・・今、十月?  
二ヶ月以上も　留守にしてたのか・・・  
  
そういや、寒いよな・・・今日  
前回　書き込んだときは　滅茶　暑かったよな・・・  
  
何なんだろう　この季節の移り変わりの早さは  
  
どんどん世の中から　取り残されてるなぁ・・・  

  

### 八月十五日  2002.08.15 THU 12:01:02　  
　すべての戦争犠牲者に  
  
　　　　　　　黙とう

  

### 百人に聞きました  2002.07.25 THU 01:33:04　  
アメリカ人ほぼ百人に聞きました。  
  
「『もっこり』は英語にどう訳したらいいですか?}  
  
答え  
  
「MOKKORI　!!」  
  
  
マジっすか・・・・・?  
  
詳しくはバンチ３４号をみてくだされ  

  

### 出張  2002.07.12 FRI 17:35:04　  
今週号の編集者あとがきまできっちり読まれる読者の皆さんは、  
もう御存じでしょうが・・・・・  
ちょっくらアメリカのカリフォルニアに出張してました。  
  
何の出張だったかは三十四号で報告いたします。  
  
そのせいで来週三十三号は  
「エンジェル・ハート」を休載させていただきます。  
何とぞご了承を。  
  
それにしてもひどい時差ボケでした。  
向こうではほとんど眠れず、帰ってきてからも、まだホゲホゲ頭です。


### 夏期限定メニュー  2002.06.17 MON 11:58:39　  
　　『冷やしもっこり』  
　　　　　　　はじめました。  
  
　　　　　　　　　　中華料理　玄武門

  

### mokkori   2002.06.03 MON 01:08:40　  
とーとつに・・・  
  
「もっこり」って英語にどう訳すよ!?  
  
「RAIJIN」のシティではどう訳しゃあいいの。  
  
あの状態の様を表す言葉としては、  
「woody」だったか「my woody」だか、どっちか忘れちゃったけど・・・あるらしいが。  
  
それって、何つぅーか　カタい（そりゃ硬いけど）。つーか、まんまっつーか・・・・。  
  
何か、こう・・「もこっ」と「ころりん」とした・・・そのぉー「こんもり」っつーか、もっとかわいく・・・そのぉ・・・  
  
もう、ええわ。  
わからんわ。  
  
セインさん、よろしく。

  

### ごっつい・・・?   2002.05.17 FRI 12:13:18　    
24号  
なんだか少年誌みたいでしょ。  
  
上げ底でしょ。  
  
電車のなかでは読みにくいよね。  
  
カッター等で上げ底を切り取ってから読むことをお勧めします。読みやすくなります。  
  
上げ底部には、フィギアを立てる台も入っているとか。  
  
一周年記念号・・・・厚くてどうもすみません。

  

### 念のため  2002.05.05 SUN 11:45:29　  
今週はバンチお休み。  
  
次回からバンチは火曜発売から・・・  
  
　　　　毎週　金曜日発売  
　　　　　　　　　　・・・となります。  
  
バン金です。  
念のため・・・・・よろしく。  
  
さらに念のため・・・・次号は、来週五月十七日金曜日に発売。

  

### あらあら、どれくらいぶり？  2002.04.22 MON 03:12:42　  
ここはどこの　もっこり？  
私は誰の　もっこり？  
  
って・・・何を書いているんだ・・・俺。  
今何時？  
ここに来るのどれくらいぶりなんだろう。  
今何日、何曜日？  
  
はぁ・・・月日の経つのは早い・・・・  
  
もうすぐ五月・・・・かな？  
バンチが創刊して・・・一年たつんだなぁ。  
早いなぁ・・・・  
長かったなぁ・・・・  
  
よくもったなぁ・・・俺。  
  
来年の今はどうしているんだろうなぁ・・・・  
  
あゆあゆの新曲いいよなぁ・・・  
何だか、エンジェル・ハートの  
アシャンの世界だなぁ・・・・  
一回しかまともに歌詞追ってないけど・・・  
  
ああ、とりとめがない。  
  
来月の一周年のために編集も作家も頑張った・・・  
いや、今も頑張っている。  
  
今・・・何時？  
  
何曜日？  
  
なちゅらる・はい・・・・だなぁ。  
  
ああ、酒飲みたい・・・・

  

### 第36話  2002.03.12 TUE 12:28:44　  
今週号の裏テーマ  
  
　　「色男は太ってはならない!」  
  
　　　　　　　　　　　　　　　・・・・・嘘です。

  

### 名前  2002.02.04 MON 01:42:44　  
「人の名字を呼ぶとき、人は自然の名前を呼んでいる」  
なんてコピーのCMをラジオで耳にした。  
名字には自然の名前が多いってことなんだけどね。  
  
田中、林、佐藤、川上、森、泉・・・・などなど確かに多いよなぁ。  
  
俺の名字はどうなんだ?  
「北条」・・・・北は方位であって自然とはいえないし、条は・・・・あれっ「条」ってどういう意味?  
  
パラパラ・・・辞書をめくってみる。  
「えだ。小枝」・・・・とあった。  
ふむふむ、枝は、自然のモノだよな。俺の名前にも自然が含まれていたわけだ・・・・が、ちょっと待て、  
「北の小枝」って・・・・どういう意味だ?  
  
さらに辞書を見る・・・・  
「筋みち。箇条書き。箇条の規則。細長い・・・・」  
などとでてきた。  
ますます、意味がわからん。  
  
しばし、考える・・・・「あっ、ひらめいた!!得意の屁理屈が!!」  
  
強引に自分の名前を解釈すると・・・・  
「条」の意味は、  
「小枝」・・・つまり先端部ということではなかろうか。それと、「筋道、規則」ってことから、掟とか、国の法律、それを守るという意味にもとれる。「細長い」も柵とか城壁とかを連想させる。それらを総合すると・・・・  
  
北の先端部で国の掟を守るための城壁。  
つまり、「北からの敵の侵入を警戒する者」という解釈ができる。「北条」はもともとは武士の名だから、ふさわしい意味のような気がする。  
  
それと北条と似た名字の仲間に、「南条」「東条」「西条」というのもある。  
それらも同じように、南の守り、東の守り、西の守り、ということになる。  
それぞれが国を中心として、東西南北を守る要の者ということではないだろうか。  
  
と、ここまで考えてたら、最近似たようなものが「エンジェル・ハート」に登場していることに気がついた。  
「玄武」「朱雀」「青龍」「白虎」（白虎はまだ登場していないが）の四神!!  
北条をはじめとする、この四つの名字は、この中国の四神と意味を同じくしているっ!!  
日本の四神のことなのだ、きっとこれは!!  
  
てことは、北条は北の守りの神、「玄武」なのだっ!!  
  
おお、すばらしい、なんて強引な解釈だろう。こんな下らないことをぐたぐた考える自分に、我ながら笑ってしまう。  
  
みんなも自分の名前の意味を面白おかしく解釈してみては?  
結構、暇つぶしになるし、面白いかも・・・・・  
  
え・・・暇つぶし?  
暇じゃねぇよ、こんなことで何、時間つぶしてるんだ俺っ!?  
  
ネームやらなきゃ!!  
  
  

  

### 2002・・・・   2002.01.12 SAT 00:15:15　    
　明けましておめでとうございます!  
今年も（は?）よい年でありますように  
  
　・・・・て、今頃こんなこと書くか  
　　　俺も・・・・（汗）



## 2001  

### アイ   2001.12.23 SUN 00:29:00　 
この年になっても愛といものがよくわからない。  
  
こんな事書くと笑われそうだが、正直、ピンとこないのだ。  
  
「愛」・・・英語では「LOVE」。  
そんなこと誰でも知っているって?  
ホントにそうかな・・・・?  
  
英語のLOVEという言葉が日本語に初めて訳された時、その訳語は「御大切(おんたいせつ)」だったらしい。  
  
その頃の日本語の「愛」は「LOVE」とは違う意味あいだったとか。愛憎とか、愛欲とかいう単語に代表されるように、当時は愛という言葉はあまりいい意味ではなかったらしい。  
「LOVE」が「愛」と訳されたのは明治に入ってからだろう。ちょいと記憶が曖昧なので、知ってる人、突っ込まないように!!  
  
話がそれたが・・・「LOVE」を日本語に初めて訳そうとした人(多分、キリスト教宣教師だろう)は、随分と苦労したそうだ。当時の日本には「LOVE」という概念というか、言葉が見当たらなかった。  
  
昔、日本には「LOVE」・・・「愛」がなかったのだ!!  
  
そこで彼は必死こいて新しい単語をつくったのだ。  
「御大切」・・・・と。  
  
しかし、実は仏教用語に「LOVE」に近い単語があった。  
  
「慈悲」・・・・それがそうだ。  
  
まさかキリスト教を広めに来たのに仏教用語を使うのは、ちと、まずいということになったのだろう。  
  
「御大切」「慈悲」。  
これが「LOVE」に近い単語となると・・・・今、一般に人々が認識している「愛」とは、かなり異なるような気がする。  
  
「LOVE」はかなり広い意味あいを持つ単語のようだ。  
  
海外から持ち込まれた「LOVE」・・・・「愛」という概念。  
うーむ・・・ますます、わからなくなったぞ・・・。  
  
  
  

  

### 追悼   2001.12.08 SAT 23:10:20　  
小松崎　茂　様  
  
僕らにたくさんの夢をありがとうございました。  
あなたは僕らの世代の憧れであり、アイドルでした。  
  
心よりご冥福をお祈りいたします。



### ねぇどうして・・・・・   2001.11.24 SAT 20:42:42　    
ずいぶん更新してないので、何か書かねば・・・・  
と思ったが、たくさん書く時間もネタもない。  
  
んーっと・・・・  
  
古い話だけど・・・。  
ドリカムの「ラブ・ラブ・ラブ」って曲のプロモビデオを初めて見たとき・・・・  
「ねぇどうして・・・」と歌いだした彼女のヘアスタイルを見て・・・・  
「おまえこそどうした!?」  
と思ったのは俺だけだろうか・・・・?  
  
  
うう・・・しょうもない事書いてしまった・・・・。

  

### 時間のずれ   2001.11.02 FRI 22:50:40　   
久しぶりにタイムマシンに乗ってしまったようだ。  
光子ロケットに乗った・・・と言った方がいいかもしれないけれど・・・。  
  
自分と世間の時間がどんどんずれていく。  
週刊連載を長く続ければ続けるほど、自分の時間感覚が世間より遅くなっていく。  
そう感じはじめる。  
世間の時間がどんどん早くなっていく。  
  
光子ロケットに乗り、どんどん加速していくと、ロケット内とその外の時間がずれていくというアインシュタインおじさんの有り難いウラシマ効果を実践している気分。  
  
これを週刊連載相対性理論という・・・・かどうかは知らないが・・・・。  
もう半年もしないうちに、世間の出来事にまったくついていけなくなるだろうな。  
  
この「もっこりメッセージ」の更新、本人は一週間に一度くらいの間隔で更新してるつもりでも、世間では一ヶ月、二ヶ月に一度の間隔になってちゃったりして・・・・。  
ははは・・・。  

  

### 運動会     2001.10.09 TUE 16:38:50　    
運動会の季節である。  
体育の日の天気が悪かったので、延期された学校もあるのでは?  
天気が悪くても中止にならないのは、戦争くらいのものか。  
  
ま、それはおいといて、またまたくだらない事考えた。  
  
［世界大運動会］の提案である。  
  
世界中の人々が集って、大運動会をしようというのである。  
  
オリンピックがあるじゃないか、という人もいるだろうが、あれとは根本が違う。  
  
オリンピックは平和の祭典とは名ばかりの、きな臭いものがある。  
大国の代理戦争のようなところもあるし、ドーピングや肉体改造までして、記録に挑戦して何が面白い、とかひねくれ者の自分は思ってしまう。  
別に、オリンピック批判をしているわけではないけれど・・・どっちにしろ、一部の特別な人々の催し物である。  
  
で、世界大運動会。これは、スポーツを思いっきり世界の人々と楽しもうというもの。  
競技は何でもよろしい。  
球入れでも大玉ころがしでも、借り物競争でも、くだらなければくだらいほど面白いかも。  
  
選手の選抜は、各国抽選。  
誰でも自由に応募できる。その中から公平に抽選。  
ただし、選手はどの競技に出るかは大会当日までわからない。  
極力、その競技を専門にしている人がその競技に出ないようにするためだ。  
あくまで、ド素人が競技するのが基本。  
  
優勝商品は、牛乳瓶のフタのメダルでもよろしい。参加者に作らせてもいい。  
  
チームも別に国別でなくてもいい。  
その場で適当にチーム別けしてもいいし、気の合う者同士が組んでもいい、もうとにかく何でも適当、楽しんだ者勝ち。  
  
大会が終わった後は、盛大に競技場で酒盛り。  
未成年は? かまうもんか!　飲んじまえ。キスだって、もっこりだって気が合えばどっかでやりゃあいい。  
ある意味これもスポーツか?  
結婚式だって挙げちまえ、みたいな勢いで夜は深けていく・・・・・。  
  
そんな、でたらめな妄想を秋の夜長にしてみた・・・・。  
  
  
  
  

  

### 原始人    2001.10.06 SAT 01:51:35　    
妻子を殺された。  
あるいは、親兄弟、親友を殺された。  
復讐のため、犯人を探し出し、殺す。  
  
よくある話。映画、テレビ、マンガ等で・・・。  
でも現実にそんなことしたら、間違いなく殺人犯だ。  
いかな理由があろうと、罰せられる。  
  
私たちは、そういうことをしたくても、してはならない、  
と知っている。  
  
法律があるから?  
倫理観から?  
宗教観から?  
復讐は、その親しい人を殺した犯人と同じになるから?  
  
まぁ、理由はいろいろ。人それぞれ。  
  
人間社会では、してはいけないこと。  
文明人として、してはいけないこと。  
  
  
でも、これは個々の国としての話。  
これを地球規模、  
国際社会で見てみると、事情は変わってくる。  
  
しかけられれば、しかえす。  
破壊されたら、破壊仕返す。  
  
これが当たり前。  
  
世の人々もこれを支援する。  
反対の声は、ほとんど聞こえてこない。  
  
何か変。  
  
地球的に見ると、  
人間、原始人と何ら変わってない・・・・  
  
ただ、そういうことなのか?  

  

### テロ道・・・こんな言葉はないだろうけど    2001.09.14 FRI 00:33:24　   
どんな理由があろうとテロというものは俺は認めない。  
卑劣きわまりない行為だ。  
しかし、それをおいといて・・・・テロというものを考えてみた。  
  
正確な意味を俺は知らないが、  
「自分の主義主張を認めさせるために、無差別な破壊活動に出る」  
・・・・これが俺の思っている正しい（?）「テロ」のあり方だ。  
つまり、破壊活動をしたあと（する前でもいいが）「我々はなにがしである。我々はなになにの要求をするため、この行動に出た」と正体と目的を世界に声明することによって成立するものだ（少なくとも俺はそう思う）。  
  
しかし、今回のテロは（「も」と言うべきか）犯行声明なるものはまるでない、やりっぱなしの破壊。  
テロ道（どう）にも劣る所業だ。卑怯極まりない。もはやこれはテロ以下。卑劣の極み。悪魔の所業っ!!  
  
うう、怒りのために、わけのわかんないこと書いてない?　俺  
・・・・。大丈夫か、俺?  
  
ちょっと落ち着いて・・・と、もう一言。  
  
マスコミ、見てると、いつも通り、報道は邦人の安否しか発表していない。それはそれで大切なのはわかる、しかし。  
  
俺のまわりにさえも、日本人だけでなく、あのビルで働いていた現地の知り合いの安否を気遣う人が何人もいる。  
しかも、それを知る方法がない。  
  
こんなときこそ、インターネットの出番ではないの?  
この情報網を駆使して、被災者、生存者のリストなり、何なりを世界中に配信できないの? やれるだろ? 誰かやらないの? もう、やってる?  
くだらないホームページやチャットのためにあるわけじゃないだろ? インターネットは!?　  
もっと真剣に役立てようぜ。  
  
うう・・・やはり、今日の俺は感情的だ。  
  
どつぼにハマリそうなので、このへんで失礼。

  

### 不謹慎な台風ネタ    2001.08.28 TUE 01:30:23　   
先週、日本列島は台風に縦断され、各地に被害が出たようだ。  
幸いここ関東地方近辺は、大山鳴動して鼠一匹という感じだったが、被害にあわれた地方の方々には、心よりお見舞いを申し上げます。  
  
などとは思いつつ、不謹慎ながら台風の迫り来る状況というのは、妙に血沸き肉踊る。  
  
不謹慎ついでに、台風の速報やニュースを聞きながら変なことを思いついた。  
日本もアメリカみたいに台風に名前をつけたら面白いんじゃないかな・・・・と。  
  
それも、アメリカのように人名ではなく、日本らしく（？）怪獣の名前をつけたらどうか。  
  
映画、テレビに登場する怪獣は星の数ほどいる。名前がつきることもないだろう。  
もし、そうなったら、天気予報はさぞ楽しかろう。  
  
「台風○○号、ゴジラは時速三〇キロで北上中。今夜半には、関東地方上陸の恐れ・・・・・」  
  
「モスラの最大瞬間風速は、五〇メートル・・・・」  
  
「キングギドラ接近による、高波により、沿岸部に避難警報が・・・」  
  
などと何だかわくわくするニュースにならないだろうか。  
  
気象庁さんそういう決定してくれません？  
だめだろうなぁ・・・・やっぱ。  
  
失礼しました。

  

### なんとなーく・・・それだけ     2001.08.09 THU 01:04:51　    
「喜怒哀楽」  
「喜」と「楽」だけで過ごせれば  
人生さぞ平穏に過ごせることだろう。  
とは、思うけど、「怒」と「哀」もなければ、  
幸せは感じないんだろうな。  
「喜怒哀楽」。素敵な言葉だと思う。

  

### メイキング    2001.07.29 SUN 00:05:46　    
よく映画の公開前などにその映画のメイキングをテレビ放送することがある。  
映画に限らず、音楽やドラマなど必要以上に苦労話を盛り込んだメイキングを宣伝や企画モノとして流している。  
スタッフや創作者の苦労話がぎっしり詰め込まれ、それでもめげず、作品を完成させる・・・・・てなヤツ。  
確かに、面白いし興味もそそる、感動すらある。  
が、あれを宣伝や売りにするのはいかがなものか。  
ハッキリ言って、創作者が苦労するのは当たり前の話で、それは作品とは何の関係もない。  
逆にプロは苦労を観客なり、購入者なりに悟られてはならないと思う。  
苦労話を見せられれぱ誰だって同情するし、作品には関係なく肩入れしてしまいたくなる。  
そんなモノを売りにするのはプロの恥である。  
先月のコアミックスの「バンチ」創刊までのドキュメントも実は自分としては不承不承だった。  
だから、自分の創作現場の取材は一切断った。  
そういうわけで、このコーナーでは自分の制作裏話や苦労話は一切書かない。  
それを期待している人たちには、それをあらかじめ言っておきます。  
前回の自分のメッセージは、疲れていたせいもあるが、ちょっと感情的に走り、苦労話ぽかった。  
反省している。

  

### 時間がねぇ!!    2001.07.13 FRI 12:42:38　    
時間が足りない。  
日々があっという間に過ぎる。  
一日が三十時間あればいいのに。  
この身体も睡眠も休息も食事も必要のない身体だったらいいのに。  
それが叶うなら、悪魔にだって魂を売る。  
ずーっと仕事をし続けたい。  
それ以外のことはしたくない。  
妥協はしたくない。  
後々で後悔したくない。  
してたまるかっ!!  

  

### ホームページリニューアル！    2001.07.03 TUE 12:50:27　    
　お陰様で、「週刊コミックバンチ」も、順調な出足でスタートを切ることができました。愛読者の皆様には、感謝の気持ちでいっぱいです。本当に心よりお礼申し上げます。  
　さて、私も「週刊コミックバンチ」の創刊！ということで、まずは新連載に専念するため、ホームページもしばらく、休止させて頂いておりましたが、新連載も動き始め、ここにホームページも再開できる運びとなりました。  
　僕も、出来る限りですが、メッセージを書き込んでいけたらと思っております。（忙しい時はごめんね！）  
　まずは、ホームページ再開！よろしくお願いいたします。  
  
　北条司



## 2000  








