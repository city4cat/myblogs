http://www.hojo-tsukasa.com/info.html  
https://web.archive.org/web/20010302142750/http://www.hojo-tsukasa.com/info.html  
https://web.archive.org/web/20010411000041/http://www.hojo-tsukasa.com/info.html  
https://web.archive.org/web/20010605041909/http://www.hojo-tsukasa.com/info.html  

<tr bgcolor="#FFCC33"> 
<td colspan="2">
<table width="700" border="0" cellspacing="0" cellpadding="0">
    <tbody>
      <tr> 
        <td rowspan="2" width="400"><img src="./img/hojoheader_01.gif" width="400" height="100"></td>
        <td width="300"><img src="./img/hojoheader_02.gif" width="300" height="50"></td>
      </tr>
      <tr> 
        <td width="300"><img src="./img/hojoheader_news.gif" width="300" height="50"></td>
      </tr>
    </tbody>
</table>
</td>
</tr>

[![](./img/banner.gif)](./artroom/artroom01.md)  


## INFORMATION!!  
■**おまたせしました!!**  
**「CITY HUNTER オリジナルTシャツ」**  
  
昨年、漫画家生活２０周年を迎えた北条先生が、自らの記念、神谷明氏の声優生活３０周年記念、そしてコアミックス創設記念という３大記念パーティーにご参加いただいた方へのプレゼントにするために書き下ろした作品です。その作品がいよいよ皆様の元へお届けできる日がやってまいりました。  

[ご購入はこちら](./project.md)


## INFORMATION!!  
[![](./img/bunch_banner.gif)](https://web.archive.org/web/20010605041909/http://www.coamix.co.jp/)

■**5月15日火曜日**に新潮社より創刊された新コミック雑誌『週刊コミックバンチ』（発行:新潮社/編集:コアミックス）のHPがオープン！！  
  
北条司が「キャッツ・アイ」「シティーハンター」を超えた作品『Angel Heart』を誕生させた!!　破壊、創造、構築…、構想5年にわたる新作品を発表！

## INFORMATION!!  

![](https://web.archive.org/web/20010411000041im_/http://www.hojo-tsukasa.com/img/bw_hyousi.jpg)

![](https://web.archive.org/web/20010411000041im_/http://www.hojo-tsukasa.com/img/bunch_title.gif)  

  
●全国のコンビニエンスで大好評の『シティーハンター』に引き続き、同じB6判サイズの【Bunch world】にて『キャッツ　アイ』が新登場！  
  
**●毎月2回、第1・3水曜日発売**  
●初回は4月4日発売！　2回目は4月18日発売！  
●ダイナマイト定価286円+税

## INFORMATION!! 

![](./img/fcompo14.jpg)

![](https://web.archive.org/web/20010302142750im_/http://www.hojo-tsukasa.com/img/fcompo14_text.gif)  

集英社『オールマン』に連載された  
『ファミリー・コンポ』のコミックス最終巻！  
**定価（本体590円＋税）集英社**  
**全国書店にてお求め下さい！**

---

![](./img/tanpen1.jpg)

![](https://web.archive.org/web/20010302142750im_/http://www.hojo-tsukasa.com/img/tanpen_text.gif)  

北条司が描いた6つの短編を完全収録。 美しく、優しく、心に響く珠玉の作品。 ブックレット付の超豪華愛蔵版です。  
**  
定価（本体1429円＋税）集英社**  

![](./img/20th_1.jpg)

![](https://web.archive.org/web/20010302142750im_/http://www.hojo-tsukasa.com/img/20th_text.gif)  

北条ファン待望のイラスト集がとうとう発売になりました。  
美しいカラーのイラストは迫力満点！北条ファンには堪らない仕上がり。 完全なる保存版として、お見逃しなく！  
  
**定価（2400円＋税）集英社**  

![](https://web.archive.org/web/20010302142750im_/http://www.hojo-tsukasa.com/img/zessan_text.gif)


## INFORMATION!!

![](./img/b6_ch1.jpg)![](./img/b6_hokuto1_s.jpg)

![](https://web.archive.org/web/20010302142750im_/http://www.hojo-tsukasa.com/img/b6_copy.gif)  

その前哨戦を独走する、怒涛のコミックシリーズ **『ＢＵＮＣＨ　ＷＯＲＬＤ』**が１２月２６日より発刊！  
まずは**『シティーハンター』（北条司）**、 **『北斗の拳』（武論尊、原哲夫）**の ２大名作が本体２８６円（税別）で登場！ 毎月１２日、２７日の月２回、 全国書店＆コンビニで発売だ！

  

## INFORMATION!!  
![](./img/cats_slot.jpg)  
**紧急告知！！**  
北条司の **『キャッツ　アイ』**がパチスロで登場！  
超液晶画面のリアクション！  
エンドレス（？）に続く、 斬新なＡＲ機能！！！  
泪・瞳・愛の３姉妹が、 華麗に舞う！

* * *

Sammyホームページ  
[http://www.sammy.co.jp/](https://web.archive.org/web/20010302142750/http://www.sammy.co.jp/)  
CAT'S EYE 紹介ページ  
[http://www.sammy.co.jp/product/pachislot/cat/index1.html](https://web.archive.org/web/20010302142750/http://www.sammy.co.jp/product/pachislot/cat/index1.html)

## INFORMATION！！
![](./img/caluculater.jpg)  
![](https://web.archive.org/web/20010302142750im_/http://www.hojo-tsukasa.com/img/present.gif)  
『キャッツ・アイ』電卓付名刺入れを 抽選で**２００名**プレゼント！（提供：サミー株式会社）

* * *

ご希望の方は、名刺入れ希望とご記入の上、北条司先生へのメッセージを添えて、郵便番号、住所、氏名、電話番号、職業を明記の上、[こちら](./contact.md)からご応募ください！抽選で２００名の方に、商品を発送いたします。尚、当選者の発表は、発送をもってお知らせ致します。  
[->](./contact.md)

## UPLOAD INFO

-> 最新情報を追加しました。 (2001.1.28)  
-> [作品紹介](./works.md)を追加しました。 (2001.1.28)  
-> 最新情報を追加しました。 (2000.12.27)  
-> サイトオープンしました。 (2000.12.26)  