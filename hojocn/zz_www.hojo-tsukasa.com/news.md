http://www.hojo-tsukasa.com/news.html  

https://web.archive.org/web/20010411000254/http://www.hojo-tsukasa.com/news.html  
https://web.archive.org/web/20010617102126/http://www.hojo-tsukasa.com/news.html  


---  


# 

[\>>>HTML VERSION>>>](./info.md)   

[ご利用に際しての注意事項](./readme.html.md)


注: 似乎是一个flash页面。但在web.archive.org上无法显示。  


---  


# 
[![北条司 OFFICIAL WEB SITE ](./wp-content/themes/hojo-tsukasa/shared/images/logo.png)](http://www.hojo-tsukasa.com/)  


## シティーハンターが生誕30周年を迎えました!!  
城市猎人迎来了诞生30周年!!  

News  2015.02.25

2月26日、『CITY HUNTER』の連載が始まった1985年からちょうど30年を迎えました！  
2月26日，《CITY HUNTER》连载开始的1985年正好迎来了30周年!

ファンの皆様のおかげで、ここまでやってくることができました。  
多亏了各位粉丝，我才能走到今天。

これからもどうかよろしくお願いいたします。  
今后也请多多关照。

[![北条司 OFFICIAL WEB SITE ](./wp-content/themes/hojo-tsukasa/shared/images/mylogo.jpg)](http://www.hojo-tsukasa.com/)  

[2022-04-01](./2022-04-01.md)  