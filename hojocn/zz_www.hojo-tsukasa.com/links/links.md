http://www.hojo-tsukasa.com/links/links.html

https://web.archive.org/web/20021017130959/http://www.hojo-tsukasa.com/links/links.html


![](../img/headder.jpg)  
![](./img/title.gif)  

![Icon](./img/icon.gif)

**[![](./img/b_hara.gif)Hara Tetsuo Official Website](https://web.archive.org/web/20021017130959/http://www.haratetsuo.com/)  
**[http://www.haratetsuo.com/](https://web.archive.org/web/20021017130959/http://www.haratetsuo.com/)**[](https://web.archive.org/web/20021017130959/http://www.haratetsuo.com/)  
**漫画家・原哲夫先生公式ホームページ

  

![Icon](./img/icon.gif)

**[Inoue Takehiko on the web](https://web.archive.org/web/20021017130959/http://www.itplanning.co.jp/)  
**[http://www.itplanning.co.jp/](https://web.archive.org/web/20021017130959/http://www.itplanning.co.jp/)**[](https://web.archive.org/web/20021017130959/http://www.haratetsuo.com/)  
**漫画家・井上雄彦先生公式ホームページ

  

![Icon](./img/icon.gif)

[![](./img/b_imaizumi.gif)](https://web.archive.org/web/20021017130959/http://www06.u-page.so-net.ne.jp/yc4/nachu/)[**今泉伸二 site1**](https://web.archive.org/web/20021017130959/http://www06.u-page.so-net.ne.jp/yc4/nachu/)[  
](https://web.archive.org/web/20021017130959/http://www06.u-page.so-net.ne.jp/yc4/nachu/)[http://www06.u-page.so-net.ne.jp/yc4/nachu/](https://web.archive.org/web/20021017130959/http://www06.u-page.so-net.ne.jp/yc4/nachu/)漫画家・今泉伸二先生公式ホームページ

  

![Icon](./img/icon.gif)

**[MonkeyPunch.com](https://web.archive.org/web/20021017130959/http://www.monkeypunch.com/)  
**[http://www.monkeypunch.com/](https://web.archive.org/web/20021017130959/http://www.monkeypunch.com/)**[](https://web.archive.org/web/20021017130959/http://www.haratetsuo.com/)  
**漫画家・モンキー・パンチ先生公式ホームページ

  

![Icon](./img/icon.gif)

[](https://web.archive.org/web/20021017130959/http://www.d6.dion.ne.jp/%7Ek.kodama/index.htm)[![](./img/b_kodama.gif)**こだま兼嗣の演出手帳**](https://web.archive.org/web/20021017130959/http://www.d6.dion.ne.jp/%7Ek.kodama/index.htm)[  
](https://web.archive.org/web/20021017130959/http://www.d6.dion.ne.jp/%7Ek.kodama/index.htm)[http://www.d6.dion.ne.jp/~k.kodama/index.htm](https://web.archive.org/web/20021017130959/http://www.d6.dion.ne.jp/%7Ek.kodama/index.htm)言わずと知れた こだま監督のHPです。アニメCHでお世話になりました。公私に渡 り仲良くして頂いています。

  

![Icon](./img/icon.gif)

[![](./img/b_kamimura.jpg)**屋根裏部屋へようこそ**](https://web.archive.org/web/20021017130959/http://www2.justnet.ne.jp/%7Ebluetrain/)[  
](https://web.archive.org/web/20021017130959/http://www.d6.dion.ne.jp/%7Ek.kodama/index.htm)[http://www2.justnet.ne.jp/~bluetrain/](https://web.archive.org/web/20021017130959/http://www2.justnet.ne.jp/%7Ebluetrain/)アニメCHでキャラクターデザイン、作画監督、原画等やって頂きました。CHファンには神村さんファンが多い気がします。

  

![Icon](./img/icon.gif)

[![](./img/b_eldorado.jpg)**A's El Dorado**](https://web.archive.org/web/20021017130959/http://www2.musical.ne.jp/eisuke/)[  
](https://web.archive.org/web/20021017130959/http://www.d6.dion.ne.jp/%7Ek.kodama/index.htm)[http://www2.musical.ne.jp/eisuke/](https://web.archive.org/web/20021017130959/http://www2.musical.ne.jp/eisuke/)ミュージカル　‘レ・ミゼラブル’のマリウス役、他 舞台などで活躍中の「爽やか」という言葉がぴったりの役者さん。僕とは‘お祭り仲間’　です。

  

![Icon](./img/icon.gif)

[**BLACK LIST　クロイヌノ逆ギレ**](https://web.archive.org/web/20021017130959/http://www.asahi-net.or.jp/%7Epx5y-inr/)[  
](https://web.archive.org/web/20021017130959/http://www.d6.dion.ne.jp/%7Ek.kodama/index.htm)[http://www.asahi-net.or.jp/~px5y-inr/](https://web.archive.org/web/20021017130959/http://www.asahi-net.or.jp/%7Epx5y-inr/)吉本興業所属の増本庄一郎（元インパクト） が自らプロデュースするサイト。現在はお笑い、というより役者としての活動が中心。 ボクとは、ひょんなことから　ひょんな仲に・・・。

  
  

![Icon](./img/icon.gif)

**[![](./img/b_yomiuritv.gif)よみうりテレビ](https://web.archive.org/web/20021017130959/http://www.ytv.co.jp/)  
**[http://www.ytv.co.jp/](https://web.archive.org/web/20021017130959/http://www.ytv.co.jp/)**[](https://web.archive.org/web/20021017130959/http://www.haratetsuo.com/)  
**

  

![Icon](./img/icon.gif)

**[![](./img/b_sunrise.gif)サンライズ](https://web.archive.org/web/20021017130959/http://www.nifty.ne.jp/station/sunrise/)  
**[http://www.nifty.ne.jp/station/sunrise/](https://web.archive.org/web/20021017130959/http://www.nifty.ne.jp/station/sunrise/)**[](https://web.archive.org/web/20021017130959/http://www.haratetsuo.com/)  
**

  

![Icon](./img/icon.gif)

**[![](./img/b_sammy.gif)Sammy](https://web.archive.org/web/20021017130959/http://www.sammy.co.jp/%20)  
**[http://www.sammy.co.jp/](https://web.archive.org/web/20021017130959/http://www.sammy.co.jp/)**[](https://web.archive.org/web/20021017130959/http://www.haratetsuo.com/)  
**

  
  



**本ホームページへのリンクは自由です。**  
もしよろしければ、事後で構いませんので、ホームページのＵＲＬを左問い合わせフォームよりご連絡ください。  
またバナーは右のものをご利用ください。

![](../img/banner200.gif)

[PAGE TOP](./links.md#top)