
官网经历了几次改版。这里搜集了部分的历史数据。


-----------------------------------------  


## 2000年12月26日 ～ 2001年06月（？）    

<tr bgcolor="#FFCC33"> 
<td colspan="2">
<table width="700" border="0" cellspacing="0" cellpadding="0">
    <tbody>
      <tr> 
        <td rowspan="2" width="400"><img src="./img/hojoheader_01.gif" width="400" height="100"></td>
        <td width="300"><img src="./img/hojoheader_02.gif" width="300" height="50"></td>
      </tr>
      <tr> 
        <td width="300"><img src="./img/hojoheader_03.gif" width="300" height="50"></td>
      </tr>
    </tbody>
</table>
</td>
</tr>

[![](./img/menu_00.gif)Top News](./news.md)  
[![](./img/menu_01.gif)What's New 最新情报](./info.md)  
[![](./img/menu_02.gif)北条司 Message](./hojo-message.md)（注：所记录的数据最早是2001.01.08）  
[![](./img/menu_03.gif)Discography 作品绍介](./works.md)  
[![](./img/menu_04.gif)Gallery 北条司美术馆](./gallery.md)  
[![](./img/menu_over_05.gif)BBS](./bbs.md)(注：所记录的数据最早是2001.02.07)  
[![](./img/menu_06.gif)Project BBS 商品开发企画室](./project.md)  
[![](./img/menu_07.gif)Editor's Diary 编集者山-chan的Dahaha日记](./editor.md)（注：所记录的数据为2001.01.15 ~ 2001.04.05）  
[![](./img/menu_08.gif)Links](./links.md)  
[![](./img/menu_10.gif)About Coamix](./about.md)  
[![](./img/menu_09.gif)Contact](./contact.md)  
[![](./img/harabanner130.gif)Hara Tetsuo](https://web.archive.org/web/20010411000041/http://www.haratetsuo.com/)  
[![](./img/sinchobanner130.gif)Web新潮](https://web.archive.org/web/20010411000041/http://www.shinchosha.co.jp/)

（注：网站起始日期见[What's New 最新情报](./info.md)页面底部。  ）


-----------------------------------------  


## 2001年07月03日     

![](./img/headder.jpg)  
[![](./img/menu1a.gif)What's New 最新情报](./index.md)(注：待完善)  
[![](./img/menu2a.gif)北条司 Message](./hojo-message.md)  
[![](./img/menu3a.gif)Portfolio 作品绍介](./portfolio/portfolio.md)  
[![](./img/menu4a.gif)Gallery 北条司美术馆](./gallary/gallary.md)  
[![](./img/menu5a.gif)BBS](./bbs.md)(注：待完善)  
[![](./img/menu6a.gif)Project BBS 商品开发企画室](./project/project.md)(注：待完善)  
[![](./img/menu7a.gif)Links](./links/links.md)  
[![](./img/menu8a.gif)Contact](./contact/contact.md)  
[![](./img/button_chui1.gif)本ホームページご利用に際しての注意事項](./readme.html.md)  
[![](./img/button_gohara.gif)原哲夫公式ホームページへ](https://web.archive.org/web/20020617002204/http://www.haratetsuo.com/)  
[![](./img/button_goweb.gif)Web新潮ホームページへ](https://web.archive.org/web/20020617002204/http://www.webshincho.com/)  
[![](./img/button_gocoamix.gif)ページへ](https://web.archive.org/web/20020617002204/http://www.coamix.co.jp/)  
[![](./img/button_heiwamura.jpg)ドイツの平和村](https://web.archive.org/web/20100227042459/http://www.friedensdorf.de/welcome3.html)  
[![](./img/button_heiwamura_text.gif)](https://web.archive.org/web/20100227042459/http://www.friedensdorf.de/japan/home.htm)  
[![](./img/button_northangel.jpg)救える命を救いたい 沖縄民間ドクターヘリNorthAngelを応援しています！](https://web.archive.org/web/20100227042459/http://www.meshsupport.net/)

(注：网站新版的默认页面为[What's New 最新情报](./index.md)，老版的默认页面依然存在，但网址改为[www.hojo-tsukasa.com/index2.html](https://web.archive.org/web/20010620074315/http://www.hojo-tsukasa.com/index2.html))  
(注：后来网站新版的默认页面为[http://www.hojo-tsukasa.com/](https://web.archive.org/web/20090220182101/http://www.hojo-tsukasa.com/))  
(注：这个banner应该源自这张图：  
![](https://web.archive.org/web/20010804063140im_/http://www.coamix.co.jp/comic/angelheart/img/top.gif)  
源自Coamix官网中AH连载时的介绍页面(2001年):[北条司『Angel Heart』 | Coamix | web.archive.org](https://web.archive.org/web/20010804063140/http://www.coamix.co.jp/comic/angelheart/index.html)  )  


-----------------------------------------  


## 2010年08月1日     
（注：开始日期是从[http://www.hojo-tsukasa.com/archives.html](https://web.archive.org/web/20130623100725/http://www.hojo-tsukasa.com/2010/08)推测的）  
![](./www.hojo-tsukasa.cust.jp/mtadm/mt-static/themes/minimalist-red/main.jpg)  
[![](./images/base/1.jpg)](./index.md#2010-08)（注：待完善）  
[![](./images/base/2.jpg)](./portfolio/index.md)    
[![](./images/base/3.jpg)](./gallery/index.md)  
[![](./images/base/4.jpg)](./links/index.md)  
[![](./images/base/5.jpg)](./contact/index.md)  
[![](./images/base/button_bbs.jpg)](./bbs.md)  
[![](./images/base/button_gotokuma.gif)](https://web.archive.org/web/20111225024138/http://www.tokuma.jp/)  
[![](./images/base/button_gocoamix.gif)](https://web.archive.org/web/20111225024138/http://www.coamix.co.jp/)  
[![](./images/base/button_cafezenon.gif)](https://web.archive.org/web/20111225024138/http://www.cafe-zenon.jp/)   
[![](./images/base/button_heiwamura.jpg)](https://web.archive.org/web/20111225024138/https://www.friedensdorf.de/Welcome-102.html)  
[![](./images/base/button_northangel.jpg)](https://web.archive.org/web/20111225024138/http://www.meshsupport.net/)  
[![](./images/base/banner_onlinestore.gif)](https://web.archive.org/web/20111225024138/http://store.comic-zenon.jp/)  
[![](./images/base/banner_onlinecatalogue.gif)](https://web.archive.org/web/20111225024138/http://catalog.comic-zenon.jp/)

注：[web.archive.org](https://web.archive.org)上显示，有段时间，该页面顶部的图交替地换成了如下的图：    
![](./www.hojo-tsukasa.cust.jp/mtadm/mt-static/themes/minimalist-red/main4.jpg)  
![](./www.hojo-tsukasa.cust.jp/mtadm/mt-static/themes/minimalist-red/main6.jpg)  


---  


## 2015年02月25(?)日 ～ 现在     
30周年纪念之际改版：  
[![北条司 OFFICIAL WEB SITE ](./wp-content/themes/hojo-tsukasa/shared/images/logo.png)](https://web.archive.org/web/20150315054447/http://www.hojo-tsukasa.com/)  
后来改为：  
[![北条司 OFFICIAL WEB SITE ](./wp-content/themes/hojo-tsukasa/shared/images/mylogo.jpg)](http://www.hojo-tsukasa.com/)  

- [お知らせ](./news) (News)  
- [作品について](./works_2015.md) (About Works)  
- [リンク集](https://web.archive.org/web/20150315054447/http://www.hojo-tsukasa.com/links) (Link集)  
- [コンタクト](https://web.archive.org/web/20150315054447/http://www.hojo-tsukasa.com/contact) (Contact)  
- [北条司公式twitter](https://web.archive.org/web/20150315054447/https://twitter.com/hojo_official)  






---  

### Links:  
1. [html-to-markdown](https://markdown.co/tool/html-to-markdown), [Convert HTML to Markdown](https://html-to-markdown.com/demo)  
2. [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
3. [DeepL](https://www.deepl.com)  
4. [腾讯翻译君](https://fanyi.qq.com/)  
5. [有道翻译](https://fanyi.youdao.com)  
6. [Bing Translator](https://cn.bing.com/translator?ref=TThis&from=ja&to=zh-Hans&isTTRefreshQuery=1)  
7. [OCRSpace](https://ocr.space/)  
8. [www.hojo-tsukasa.com | Wayback Mechine](http://web.archive.org/web/20011129230020/http://www.hojo-tsukasa.com/)  

--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



<font color=#ff0000></font>
<a name="comment"></a>  
<s></s>