https://web.archive.org/web/20020819073926/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=320

### [6400] ちゃこ	2002.04.14 SUN 21:33:09　
	
お久しぶりでございます。ちゃこです。
初めての方は、はじめまして。
最近なかなか訪れませんが、長文感想文書きを得意（？）としております。
以後、宜しくお願いします。
で、早速ですが、久しぶりに「今週号の感想文」です。

「奇跡について」
この間観た映画で「奇跡は、何度も起きない」という台詞がありました。
主人公にとって、大切になりつつある人が、死んでしまった後の台詞で
私は無茶苦茶、切なかった。奇跡が起きると信じたかったから。

人が生きていく上で、「死」は、どうしたって避けられない。
それが、現実。そして、そんな風に辛いことがあっても、
「それでも人は生きていかなければならない」のも現実。
でも、だからこそ余計に、フィクションの世界では、
人に死んで欲しく無いというのが、私の思いです。
それは単純に「『死』が見たくない」って事ではなくて、
「奇跡があるということを信じたいから」なんです。
「もしかしたら、現実にだって起こるのでは？」と夢見たいからなんです。

香と阿香の「奇跡の適合性」
それは多分、現実の世界で考えれば、夢物語でしょう。
でも、すごく嬉しかった。
「二度とリョウを悲しませない」と、香に誓った阿香。
「そんな親不孝は絶対しない」と、本当に「子供の笑顔」で言った阿香。
リョウにとって、本当に「香瑩（香からの贈り物＝宝物）」になった瞬間。
そんな場面が見られたから。
そして家族は「血」ではなくて「相手を思う心」で繋がってるんだと、
考えさせられました。
先生、これからも「優しい奇跡」を描き続けて下さいね。

あとは「リョウ＝45歳説」と「陳侍従長の料理」について
延々と述べたいところだけど、それは、また別の機会ということで（？）
今回はこれにて、失礼します。

香与阿香“奇迹般的契合性”  
在现实世界中，这恐怕是痴人说梦。
但是，我非常高兴。
阿香向香发誓“不再让獠伤心”。
阿香真的带着“孩子的笑容”说:“我绝对不会做那种不孝的事。”
对獠来说，是真正成为“香莹(来自香的礼物=宝物)”的瞬间。
因为看到了那样的场面。
这让我认识到，家人不是靠“血缘”，而是靠“为对方着想的心”维系的。
老师，今后也请继续描绘“温柔的奇迹”吧。

### [6399] 玉井真矢	2002.04.14 SUN 18:03:23　
	
＞[６３９２]無言のパンダ様
本当にうれしかったです。
今度は無言のパンダ様のお名前が読まれるといいですね！


### [6398] ぽぽ（兵庫助）	2002.04.14 SUN 14:54:42　
	
　深夜のＣ･Ｈの再放送にはまっているのですが､テレビで､雑誌で､毎週りょうちゃんに会えたかつて(?)のドキドキわくわく♪感が懐かしい｡
　放映当時Ｃ･Ｈのアニメスタッフの方が､｢今までのアニメにないものを―――｣とおっしゃってたのですが､｢今もこんなアニメ､漫画､他にないよ～～!｣と思うこの頃｡フィクションだけど､本当にこんな人どこかにいるんじゃないかと思ってしまうリアルさ､キャラクターの人間臭さが大好き!
　シャンちゃん､りょうパパに出会えて良かったねーw(＾-＾)w＆昔の冴子しゃんも｢強い女ー！｣でカッコよくて好きだけど､今の冴子しゃんも人間味？がより見えて､ぐ～～っと素敵(o＾_＾o)

### [6397] Ｂｌｕｅ　Ｃａｔ	2002.04.14 SUN 11:18:02　
	
　こんにちは。
＞［６３９１］Ｍａｎｔａさま
　小説版Ｃ・Ｈ、わたしはアニメＳＰのノベライズ版のやつは大好きなんですが、あとの２つのオリジナルのやつは・・・あまりにも、ちょっと、違和感なので、これは原作とは別物、パラレルなんだと言い聞かせて読むようにしています。たとえ続編と思わせるシーン（海坊主と美樹さんの結婚式のエピソードとか）があったとしても。でもほとんど今は、イラスト目当てで開くくらいかな・・・。アニメＳＰのノベライズは、アニメ版よりキャラの過去とか丁寧に描かれてて、はっきりいってアニメより面白いと思います。あくまでわたしには、ですけど。

　「神谷明リターンズ！」わたしも聴きましたよ。拳志郎（ｂｙ蒼天の拳）はケンシロウ（ｂｙ北斗の拳）より軽いとこがあるんで、Ｃ・Ｈのリョウと演じわけるのが大変、て言ってましたね。

“神谷明归来!”我也听了。他说拳四郎(by苍天之拳)有比健四郎(by北斗之拳)更轻松的地方，和C·H的獠分开演很辛苦。


### [6396] ひろ	2002.04.14 SUN 01:59:42　
	
こんばんは～(^o^)丿

今日は、一日いい天気でしたね。

千葉のOBA3[6394]さん
「釣果は？」、っと聞かれると、頭をコリコリ掻きたくなります(苦笑)。珍しく全員(６人)ダメでしたよ！　釣れるのはハゼとメゴチとヒトデだけ･･･(それもたま～に)、サメを釣っている人もいましたが･･･。
でも、そんなことはどうでもいいんです。暖か～い日差しの下、船の上で揺られながら、の～んびり竿を出す。それだけで、す～っごく気持ちいいんですよね。
子どもの頃は、一時、毎日って言っていいほど海釣りに出かけていました。釣れなくても、海を見ているだけで心が落ち着くというか、気持ちいいものでした。ついつい昔のことを思い出しちゃいます。

ゆうちゃん[6393]さん
お互い天気が良くてよかったですね！
息子さん、泣いてばっかりのようでしたが、可愛いものじゃありませんか。
そのうち、自分を主張するようになったり、反抗期があったりで、別の大変さが出てきますが、それもまた人生。親も子も、苦労があってお互い成長していくんですから･･･。それが当然だと思って、お互い頑張りましょうよ！(いっしょにするナァ～って)

明日は、おばあちゃんとお母さんと息子二人、あと甥っ子で映画を観に行くそうなんで、私とおてんば娘は、二人っきりの水入らずで留守番です。愛車のエスティマで、ドライブでも行こうかナ！

さてＡ・Ｈの話題です。陳さんが、オープンさせる中華料理って、やはり気になりますよネ。いきなり、ドあっぷの顔で現れたり、新宿であの派手(陳さんの趣味かも)な伝言板を置いたり･･･。
もしかしたら、ド派手でおっきな建物(もろ中華風)じゃないでしょうね。どちらにしても、楽しみだなぁ。

あっ、もうこんな時間だ！
パソコンの中を整理して、そろそろ寝よ～っと。
それじゃ、サイナラ(^^)/~~~

### [6395] 葵	2002.04.13 SAT 21:28:42　
	
　最近キムチが好きでバカ食いしてたら、新陳代謝が良くなったのか４キロダウン！！うれしいです♪陳侍従長がオープンさせる中華料理店はのキムチは、おいしいかな…？それともリョウの家の階下にある「朋友」のほうがおいしいかしら？（笑）

＞Ｍａｎｔａさま［６３９１］
　どもです。ノベライズ、アニメの脚本を書かれてた外池省三さん（だっけ？！）の作品は、けっこうイイ味出してて好きですよ。原作とは一味違った、ちょこっとオトナの二人を見られるのではないでしょうか。何冊か出てますから、いろいろ味わってみてくださいね♪

### [6394] 千葉のOBA3	2002.04.13 SAT 20:46:26　
	
こんばんはー・・・すでにトドの水死体状態になってしまった、
も、つっかれたー千葉のＯＢＡ３です。
で・・・。

６３９２無言のパンダさま
わー、うれしいっす！パンダさまも,いて座のＢ型なんて！
結構Ｂ型っていうと,周りから引かれちゃうんですけどね。
私ダンナがＡ型なんで、子供がＡ，ＡＢ，Ｏ（上から順）と
３つの血液型になったんですよ。その中で、ＡＢの真ん中のが
一番手ごわい・・・。すごく優しい子なんだけど、何考えてるのか、ぜーんぜんわかりませーん。確かりょうちゃんもＡＢと聞いたので・・・・あー私には、たちうちできない？？？と、思ったのでありました。　しかし、同じいて座のB型ということで、
これからもよろしくお願いします.

６３８７ひろさま　釣果はいかがでしたでしょうか？
今日は朝のうちは風もなく絶好の釣り日和でしたね。
いつか私も船釣りにチャレンジしたいです。

なんか、疲れすぎて視力も落ちてるみたい・・今夜は早く寝ます。

### [6393] ゆうちゃん	2002.04.13 SAT 15:47:26　
	
こんにちは。レスおんりーです。
ひろ様。[6387]ｓａｋｕｒａ様。[6388]お祝いメッセージありがとうございます。恐縮です。おかげさまで、午前中は快晴でした。天気とは逆に、息子はやっぱり式で泣いてしまい、ずっとそばについているはめになりました。来週からが思いやられます・・。

ｍａｎｔａ様[6391]。ＣＨのノベルス。あれは、北条先生が書いたものではないので。私は、基本的にはパラレルと考えています。でも、自分の中で面白いところは、続編と考えて楽しんだりもします。リョウのセリフが、結構ストレートでうれしかったりもするんですが、これはないよな・・と思うこともあるし。ＣＨの原作って、ここぞというシーンは表情だけで描かれてあることが多いですが。それを文章で表現しようとすると、あんな感じになっちゃうのかも。この点については、アニメでも同じですが。

### [6392] 無言のパンダ	2002.04.13 SAT 13:35:28　
	
こんにちわ☆

昨晩っていうか深夜、「神谷明のリターンズ！」聞きました！
残念ながら「今週のバンチラ」、ＡＨじゃなく「蒼天の拳」だったけど、かっこよかったです♪
＞玉井真矢様！またまたメール読まれてましたネ！プレゼント希望の！いいな～いいな～。ハガキを読まれたのは雪乃さんでしたが、神谷さんも「真矢」と呼んでました。もう鳥肌ものでしたよ、自分の名前じゃないのに！(^_^;)

＞千葉のＯＢＡ３様（６３７４）
私も射手座のＢ型なんですよ～♪
だから何？と言われてもなんなんですが、なんだかうれしかったので書いてみました！

それではまた。(^_^)/~

### [6391] Ｍａｎｔａ	2002.04.13 SAT 12:00:06　
	
こんにちは☆☆☆
C.Hに小説があること知ってました？？私、知らなくて、それを見つけたときは、も～～～～～嬉しくて、しかもマンガでもアニメでもやったことのない内容だったんで即買いしたんですよ。あっ、でも「アニメスペシャル」の小説もあったんですけどね(ﾟーﾟ）それで、家に帰って読んだんですよ。そしたら、設定とか微妙に違うしリョウの性格とか香の性格とかも違うんですよ→！それはそれの、良さなんだと思います。でも、なんだかガッカリした気分になっちゃいました（；；）
どなたか、この小説を読んだ事のある人でどう思ったか、どんな意見でもかまわないので、よかったら教えて下さいm(_ _)m　

Mantaでした(^-^)v

### [6390] yaeko	2002.04.13 SAT 01:03:31　
	
こんばんは。
「クタクタ世界漫画愛読者大賞の巻」
ハガキ作成に２時間もかかってしまった。１０週分の応募シールを貼るためにハサミを探し、ノリを探し（なんで家の連中は使ったら元の場所に戻さないのだろう？）何とか道具も揃ってやっとこさ応募シールを貼って、１０週分の漫画を検めて読んで、選んで○して、感想書いて、はあぁ～疲れた～。後は住所で終わりだ～と思ってら書き間違えちゃって（裏面で燃え尽きてました）なんとか誤魔化してついに完成させました。こんなに苦労したんだから私の５ポイント入った人に１位になってもらいたいな★１位にならなかったら堂本剛のドラマ見過した責任とってよ！←どうとるんだよ！（三村風）
それでは。ってぜんぜんAHの話題に触れてないじゃん！え～と、来週のAHお休みじゃなくて良かった～。そろそろ休みかなって思っていたので。再来週あたり休みかな？AHのない１週間て長いんですよね～。
ではでは、おやすみなさい。

### [6389] いも	2002.04.13 SAT 00:59:27　
	
こんばんわーい。
酔っ払い参上です！！（って、今日はそんなに飲んでないので、
しらふ同然なんですけど‥。ちょっとはしゃいでみました・汗）

レスです。
＞ミルクさま[6380]
はじめまして。いもと申します。
私の個人的な気持ちとしては、リョウには恋してほしくないです。いや、「恋」ならいいか‥。でもなあ‥。
いくらＣＨとは別物と言われても、やっぱりリョウには香しかいないと思うし。わがままかもしれないけど、リョウの新しい恋人は見たくないです。
でも幸せにはなって欲しい。‥‥う～ん、難しいですね。（苦笑）

＞sakuraさま[6388]
はじめまして。
何故か大酒飲みに見られがちな、いもです。
恥ずかしいですか。そういえば、結構スゴイ格好してますよね、リョウったら。（笑）
場所が場所だけに、香が目隠しする気持ちも分かるかも！？

明日早いので、この辺で寝ます。
いもでした。

### [6388] sakura	2002.04.13 SAT 00:12:41　
	
こんばんわ。
今日から仕事始めの私としてはちょっとくたびれてます。
みなさまは、有意義な金曜日の夜を過ごしてらっしゃるのでしょうか？！でも世の中には、土日、仕事をしている人はたくさんいらっしゃいますよね！主婦の方は、年中無休と言ってもいいくらいですし...　めげずに、気力で一週間がんばろーっと。

レスを
（６３８３）ゆうちゃん様　はじめまして。
明日の、入園式晴れるといいですね！！大変そうですねがんばってくださいねっ。

（６３８１）いも様　はじめまして。こんばんわ！！
飲み会ですか。いいですねー　でも飲み過ぎに注意です。（笑）
りょうちゃんの「おしりぐりぐり」の話、知ってます！おもしろいですよねっ。でも私、なんかちょっと恥ずかしくって、まともに読めないんですよ...　変ですかね？！（笑）

書いてるうちに、１２時まわってしまいました（－－；）
明日も朝早いのでもう寝まーす。おやすみなさい。
レスおんりーでごめんなさいです。　

### [6387] ひろ	2002.04.12 FRI 23:39:21　
	
こんばんは～(^o^)丿

ワーイ！！　明日は大ダ～イスキな釣りだ～い！！！
会社の人のマイボートで、木更津沖にカレイ(マコガレイ)釣りで～す。海ほたるの横を通って木更津沖にいきます(ちょっとはしゃぎ過ぎ)。千葉のOBA3さん、メバルじゃなくてすみません。メバルはおいしいんですけどね。次回といううことで･･･。
でも、ちょっと天気が心配。それと、娘が起きないように願って･･･、絶対に連れて行けって騒ぐから･･･。
もう少し大きくなったら、連れてってあげるからネ！　飽きるほど。

いも[6381]さん
やっぱりグリグリ見たくなった。思いっきり笑えそう。
詳細な情報ありがとうございました。

ゆうちゃん[6383]さん
息子さんのご入園、おめでとうございます。ここに来ている方は、みんな日ごろの行いがいいから晴れますヨ！
うちの息子達は、もう中学、高校生なんで入園の頃なんか忘れてしまいました。でも、すごく可愛かった記憶だけはあります(親バカ！？)。いまは、マジで親子げんかしていますけど･･･。

それでは、明日がありますので、今日はこの辺で･･･。
おやすみなさ～い(^^)/~~~

### [6386] もっぴー	2002.04.12 FRI 21:57:09　
	
みなさまこんばんわ。
超超久しぶりに書き込みする者です。今のBBＳの状況がよくつかめてないですけど。
グラスハートちん。（呼び方古い？）
リョウちゃんと香ちゃんの娘になったのね。
良かった。と思っている今日この頃。でも、たまにセツナイ今日この頃であります。


### [6385] 葵	2002.04.12 FRI 21:15:23　
	
　昨日の某ケーブルテレビでやってた「ＣＥ」、海原が出てました。「ＣＨ」の海原とは違うものの、見た目はソックリさん。「海原」という名の悪役、よほど北条せんせ、お気に入りなんでしょうね。でも、全国の北条ファンの「海原さん」は…悲しいだろうなぁ…☆（笑）

＞ミラクルハートさま［６３８４］
　「伊集院隼人氏と劉信宏君の平穏な一日」…ぜひっ！（笑）

### [6384] ミラクルハート	2002.04.12 FRI 19:50:54　
	
リョウと阿香のペアの行く末も気になりますが、海坊主さんと信宏のペアの行く末も気になるところ。以前の２人の掛け合いは、とても面白かったです。もう少し時が経ったときの、「伊集院隼人氏と劉信宏君の平穏な一日の巻」を見てみたいですね。

＞おっさん様（６３５７）
レスありがとうございました。
リョウのような人は全盛期は確実に視力２.０以上はあったのでは？老眼が少し出てきた今で、ちょうど人並みだったりして・・・。老眼鏡はまだ先かもしれませんね。

### [6383] ゆうちゃん	2002.04.12 FRI 15:49:25　
	
こんにちは。明日は息子の入園式！母の方がドキドキ状態です。とりあえず、晴れることを祈って・・。ひさびさのスーツで、大荷物もってかなきゃならないんで。体操服やら、教材やら。がんばるぞーー！

レスです。
ひろ様。[6377]そうですよね。「もう、りょうより先に死んだりしない」は，香ちゃんの気持ちですね。あのシーンが映像になって、阿香と香の声がかぶる感じ、すごいハマりました。
私って、まだまだ浅いなあ・・。ＢＢＳの方々のおかげで、ＡＨが数倍楽しめます。

### [6382] いも	2002.04.12 FRI 15:10:25　
	
申し訳ないです！！下の私のカキコＵＲＬ出てますが、押すとここのトップページ（？）に飛びます。（滝汗）
なんでこうなったんでしょう？？？？？
ホントごめんなさい！！

### [6381] いも	2002.04.12 FRI 15:04:33　
	
皆様こんにちは。
こんなに早い時間に来たの初めてかも‥。なんだかいい気分♪
昨日電車の中吊りで、バンチの広告見ました。
世界漫画愛読者大賞（←ちょっと違うかも・汗）の集計結果の中間報告をしていました。
なんとなく自分が「いいなあ」と思った作品がどうなってるのか気になってたので、中吊り見てさらにグランプリ（？）発表が楽しみになりました。

さて、レスをば‥。
＞無言のパンダさま[6365]
こんにちは！
そうなんですよね。診察の後すぐだからシャツが出てて、その後の会話の途中でしまったのかなとも考えました。
でも、それにしては阿香素早すぎ！？と思いまして‥はい。
もしイタズラならこういうのは大歓迎ですよね。北条先生には大いに私たちを試して（？）、楽しませてもらいたいです。(^^)

＞ひろさま[6377]
はじめましてです。こんにちは！
あるんですよ！！「お尻の穴に指を突っ込まれてグリグリされたこと」（←これツボでした・爆笑）
ラストの方のお話で、海原との戦いが終わって、最終回までの間くらいのところです。（コミック３４巻・文庫１８巻）
リョウの負った怪我の中で一番恥ずかしい怪我だと思います。（笑）
一人で薬を塗るリョウの姿は何とも言えないです！！是非読んでみて下さい！

今日は飲み会！酔っ払い状態でもまた夜来ます。（←迷惑・汗）
それでは、この辺で。
いもでした。
