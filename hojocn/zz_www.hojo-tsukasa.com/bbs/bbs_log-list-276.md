https://web.archive.org/web/20020619170051/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=276

### [5520] 三毛猫	2002.02.04 MON 17:45:06　
	
もっこりメッセージ更新されていましたね。
先生の名前の場合、苗字だけでなく、名前も合わせて「北を司る」っていう意味になりますからバランスいいですよね。
最後の一言「ネームやらなきゃ！！」はタイトルのひっかけですか？
ドラマなど作るときの土台を、「シナリオ」とか「プロット」とかって言い方しますけど、「ネーム」って言い方は漫画の世界独特ですよね。
今、ネームを起こしているってことは、ＡＨまだまだ続きますよね？青龍を倒した事で一区切りついた感じがしたんでちょっと心配したのですが･･･。
李大人が普通の娘には戻ることは出来ないだろうと言っている事から、やっぱりリョウの後継ぎになるのかな？
密かに信宏も合わせて冴羽商事が復活することを期待していたりして･･･☆

・ＦＣ12巻・80th.day「成人式はミステリアス」の不思議
　この話では、泥酔した雅彦が女の子になりきって暴れまくったと、江島のウソにだまされたというオチになっていますが、最後に何故か紫さんの振袖に傷があったことや、会場で寝たまま起きなかった事を考えると、パラレルワールドの雅彦が暴れまくったと思っちゃいます。
　昔、ある番組で子供の頃に二階から落ちた時にもう一人の自分が現れて見ていたのを目撃して以来、もう一人の自分がお酒を飲み歩いていて困っているいう人がいました。
その人みたいに雅彦も、もう一人の雅彦が存在するのでは？
と、なると現れる原因になったのは、子供の時に紫苑に宙吊りにされた事だったりして･･･^^;

ＴＭの「GIFT FOR FANKS」買おうと探しているのですが、心あたりのＣＤショップも古本屋も全滅だよ～(T T)
ネットでも品薄みたいだし･･･。
しばらくの間、探すのに夢中になりそうです。

FC12卷·80th.day“成人式是神秘的”的不可思议  
在这个故事中，雅彦喝得烂醉如泥，变成了一个小女孩，发疯了，这是江岛的谎言，最后不知为何紫的振袖上有伤痕，想到她在会场上躺着没醒，真让人哭笑不得。我觉得Parallel World的雅彦发狂了。
以前，在某个节目中，有一个人看到自己小时候从二楼掉下来的时候，另一个自己也出现了，之后看到另一个自己一边喝酒一边走，感到很困扰。
雅彦是不是也像那个人一样，存在另一个雅彦呢?
和，成为了鸣人出现的原因的，是小时候被紫苑悬空的事···^^;

### [5519] Ｂｌｕｅ　Ｃａｔ	2002.02.04 MON 17:44:15　
	
　リニューアル前のＨＰ、わたしもやっと見ることができました！　右上にはコアミックスのマークがあるだけで、真っ白、ってかんじだったので「どうして！」とか思いながらマウスを動かしてたら、カーソルが指になるとこがあって・・・で「ひょっとして」と思ってクリックしたら・・・黄色い文字が出て、またクリックしたりしてたら、できました～、めちゃくちゃ嬉しかったです♪
　「もっこりメッセージ」、読んでて、懐かしかったですね～、鬼束ちひろさんの「月光」のエピソードも読めたし♪　娘さんがＶ６の岡田君のファンだ、というエピソードにはびっくりしてしまいました、「坂モト冬樹」は覚えてたんですけどね(笑）　娘さん、「木更津キャッツアイ」はきっとすごく喜んでるんだろうな、なんて思ってしまいました。でもこれを読んじゃったから、これからこのドラマを見るたびに、岡田君の髪が気になりそうだ(笑）

　更新された「もっこりメッセージ」、強引な解釈についふきだしてしまいました(笑）　白虎部隊（白虎隊といいそうになる　笑）、そのうち出てくるんですよね、楽しみです。

　明日はバンチの発売日♪　早くＡ・Ｈの続きに逢いたいなっ。

更新前的HP，我也终于能看到了!右上方只有Coamix的标志，空白的感觉，“为什么!”一边这样想着一边移动鼠标，光标变成手指的地方……于是心想“难道”就点击了……弹出黄色的文字，又点击了一下，成功了~，高兴得不得了♪

读了《Mokkori message》，觉得很怀念呢~，鬼束千弘的《月光》的小插曲也读了♪女儿是V6的冈田君的粉丝，这个小插曲让我很吃惊，《坂元冬树》我还记得呢(笑)女儿，我想她一定很喜欢《木更津猫眼》吧。不过我看了这个，以后每次看这部电视剧，都会很在意冈田君的头发(笑)


### [5518] ゆうちゃん	2002.02.04 MON 14:27:49　
	
こんにちは。
５４９６Blue Catさま。はじめまして。うちのは、ＩＭＥパッドで「瑩」出せました。教えてくださってありがとうございます。

５５０７サボテンさま。丁寧に説明してくださってありがとうございます。白い画面で、雑な私はあきらめてました。で、以前のＨＰの、トップページまでいけたのですが、「強制終了」のメッセージがでてしまいました。せっかく見れると思ったのに、んーー残念。
ここのＢＢＳは優しい方ばかりで本当、うれしいです。
では、失礼します。

### [5517] ちあん	2002.02.04 MON 13:56:54　
	
初めまして、ちあんと言う者です。いつも皆様のご意見等、楽しく読ませて頂いておりました。私も先生の作品にはまっている一人として、皆さんの輪に加わりたく、今回初参加させて頂きました。今後、たびたびお邪魔すると思いますので宜しくお願いします！！
最近、会社に出社する前に「Ｃ・Ｈ」のビデオを見るのが日課になってま～す。見た日は、一日楽しく仕事が出来るので･･･☆

### [5516] black glasses	2002.02.04 MON 13:26:24　
	
どうもこんにちは
先生のもっこりメッセージ読みました。思わず大笑いしてしまいました。なんという解釈だろうか。でも確かに「北条」という名前は武士の名前ですよね。ということは当時は本当にそういう感じでつけたのかもしれませんね。でもゲーム「信長の○望」では確か北条氏は千葉のあたりを守っていたような気が･･･。

### [5515] 高田	2002.02.04 MON 12:06:11　
	
北条先生
きっと、それは・・・・・・・・・・違います。

### [5514] 海里	2002.02.04 MON 01:35:25　
	
こんばんは♪
今日はとても寒かったですね。
寒がりなのに、「今日は家にいよう」と思いながらも、
何故か出かけてました。

昨日、バンチワールドのCHを読んでて、
冴羽さんがしおりちゃんとゆー赤ちゃんになつかれる話に、
自分の娘とそのしおりちゃんを重ねて見てしまいました。
ちょーど、娘としおりちゃんが同じぐらいの年だったので。

☆sweeperさま[5513]☆
こんにちは♪
レスありがとうです！
実は、海里じゃなく違う名前で書き込んでた時、
sweeperさまに何回かレスしたことがあります。
リニューアル前からsweeperさまのことは存じておりました。
常連中の常連ですよね？
あたしもいちお、リニューアル前は常連でした…
（自分で言うなっ！）
でも、sweeperさまほどではありません。
sweeperさまは、いつも丁寧にレスをしてくれて、
あたしはよく見習ってます♪

### [5513] sweeper	2002.02.03 SUN 22:07:14　
	
こんばんは。今日は節分ですね。

[レス]
＞kinshi様。[５５００]
キリ番ゲットおめでとうございます。それから、「GIFT　FOR　FANKS」私も持ってますよ。「Get　wild」をはじめいい曲ばかりですよね。私はこのアルバムに入っている「８月の長い夜」が好きです。でも、「１９７４」「COME　ON　LET'S　DANCE」なんかも捨てがたいです。ちなみに私がはじめてネットショッピングで買ったものは、「CITY　HUNTER３」のサントラでした。(笑)

＞無言のパンダ様。[５５０４]
「あ、あたしもあげなきゃ・・・。時計、こわれちゃったもんね。」
「いや、おれもおまえと同じものでいい・・・。」
↑お誕生日プレゼントはこれでよろしいでしょうか？(＾－＾i)
昨日が、「本家無言のパンダ様」ことお嬢様のお誕生日だったそうで、お誕生日おめでとうございます！誕生日は自分がこの世に生を受けた日。私は自分の誕生日が来るたびに産んでくれた母に感謝感謝です。(*＾－＾*)

＞海里様。[５５０９]
私もリニューアル前からカキコしに来てました。でも、HNはリニューアル前から使ってました。海里様がリニューアル前からカキコしていたとは知りませんでした。

＞千葉のOBA３様。[５５１２]
はじめまして。それから、あまり緊張しなくても大丈夫ですよ！ここのＢＢＳの皆さんは優しい方たちばかりですから。

レスのみになりますがこれにて失礼します。

### [5512] 千葉のOBA3	2002.02.03 SUN 19:42:06　
	
>5506kinshi様5503おっさん様
有難うございます．年齢を忘れて（？）がんばります。
>5504無言のパンダ様
そのとおり、素直に読んで下さい。
ハンドルネームを何にしようかなー？と考えた時長男が色々言うのですが、みんなかわいい？名前で（なぐられまいと、気を使ったらしい）私らしくないので、そ、わたしはOBA3さ、ということで。これにしました。「A・H｣では、りょうちゃんが自分のこと「おじさん」て言うのが、イヤ．自分がすごい年とった気がして。なんか最近のりょうちゃんは、以前より猫背に見えて，早くあの，そっくり返ってるといえるくらい背筋ののびたりょうちゃんに戻ってほしい・・・なんて私としては，思うんですが・・・。でも無理なのかな？香ちゃんがいなくちゃ・・・。
ということで、次号からの展開が、とても楽しみです。




### [5511] らぶりい	2002.02.03 SUN 12:53:13　
	
初めまして！らぶりいというものです。この半年間傍観者してきましたが、意を決してＢＢＳに飛び込んできました。ちなみにＣＨが大大大っっ好きで、その中でも香ちゃん激ラブです！皆さんと仲良くなりたいです。よろしくお願いします！

### [5510] キャサリン	2002.02.03 SUN 11:18:59　
	
おはようございます。

なんか遅れちゃったけど今週のAHの感想です。
りょうの最後の言葉・・・
自分にも言い聞かせてるような感じがしました。
この言葉を言ったからにはもうりょうは香さんの後を追って死のう何ては考えないよね？？それも娘＆息子のような存在のGH（なんかこっちの方が定着が・・・）＆信宏もいるし・・・
これからどうなっていくかが凄く楽しみです！
「阿香」可愛いですvvv

～[5506] kinshiさん～
私も「りょう」という名前よく使ってます！
ゲームの主人公を「りょう」パートナーの女性を「かおり」ライバルを「はやと」とか、ジャンル関係なしに使ってます（笑）
嗚呼、私だけじゃなくってよかったですvvv

### [5509] 海里	2002.02.03 SUN 02:35:41　
	
サボテンさまの情報を頼りに、
インターネット＠ーカイブでリニューアル前のHPを見ました。
ちょっぴり懐かしかったです。

☆yaekoさま[5508]☆
こんにちは、初めまして♪
海里と申します。
記憶が曖昧なので、正確にお伝えできるかわかりませんが、
確かに、バンチが発売された5月15日には
BBSに書き込めなくなってます。
で、セイラの話題は、
書き込めなくなる前から、かなり盛り上がってました。
それに対して、北条先生がセイラの裏話を教えてくれました。
実は、あの話で最終回だったそーです。
だから、あの話は特別に盛り上がってるんですね♪
先生は、あたしたちの書き込みを、
ちゃ～んと読んでくれているんだなぁ～と、
とても感激しました！
先生のこの書き込みは、5月14日より以前に書いてます。
合ってるのかなぁ～？
yaekoさま、間違ってたらすいません！！

実は、あたし、
リニューアル前から書き込んでました。
一時、書き込んでなかったので、
名前を変えて、気持ちを新たにして書き込みたかったんです…
ではまた！！

yaeko[5508]☆  
你好，初次见面♪
我叫海里。
因为记忆模糊，不知道能不能正确传达，确实，在bunch发售的5月15日BBS上写不出来了。
在我写不下去之前，关于セイラ的话题就已经很热烈了。
对此，北条老师告诉了我セイラ背后的故事。
其实，那个故事是最后一集。
所以，那个话题特别热烈呢♪
老师非常感激你在读我们的留言啊~~！
老师的这个留言是5月14号以前写的。
对吗~ ?
yaeko先生，错了的话对不起! !

### [5508] yaeko	2002.02.03 SUN 01:39:39　
	
[5507]サボテン様
インターネットアー○イブの情報ありがとうございました。リニューアル前の懐かしいＨＰが見れました♪

「実はリニューアル前も毎日見に来てたんですの巻」
わがままを言えば、もっと細かく日付が指定できるといいですね。私は５月１４日をもう一度見たいんです～！！！バンチ増刊号発売の前日。ＢＢＳでちらほらと香が死んでいるとか、りょうが出ているとかいう月曜日組のネタバレの中、もう１つセイラの話題で盛り上がってて、その話題に対して５月１４日に北条せんせがメッセージを書かれてたと思ったんですが…。夜中の２時には（５月１５日ね）まだ読めたのですが、（りょうが出るということでなかなか寝付けなかった）朝７時には（バンチが読みたくて早起きした）もう読めなかったんですよ～。ＨＰが停止していて。一回しか読んでないから内容をまったく覚えてな～い。←あほです。もう一度それが読みたかったよ～。
内容を覚えてる方がいましたら、ぜひぜひ教えて下さいませませ。（なるべく詳しく）

“其实翻新前我每天都来看之卷”  
任性地说，如果能更详细地指定日期就好了。我想再看一次5月14日~!!!Bunch增刊号发售的前一天。BBS上零零星星地和香死啦、獠出来啦这样的星期一组的剧透中，还有一个塞拉的话题很热烈，那个话题在5月14日被北条老师写了留言。……。半夜2点的时候(5月15日呢)还能看，但是(因为要发獠所以怎么也睡不着)早上7点的时候(因为想看bunch所以早起了)已经看不到了哦~。HP停止了。因为只看了一遍，所以完全没记住内容。←笨蛋。我还想再读一次那个呢~。

如果有记住内容的人，请务必告诉我。(尽可能详细)

### [5507] サボテン	2002.02.03 SUN 00:23:19　
	
こんばんは。

＞[5491]kinshi様
　[5492]ゆうちゃん様
やっぱり私の文章、分かりづらいですね。すみません；
日付けのリストをクリックすると、リニューアル前のページだと、真っ白な画面が出ると思います。その画面の右上をよーく見てみると、黄色い字で「MOVIE SKIP」と書いてあると思います。そこをクリックすると、今度はまた黄色い字で「HTML VERSION」と出てくるので、またクリック。そうすると見られるのではないかと思います。
私も最初どうやって見るのか分かりませんでした。本当にここのHPで見られるようになるといいですね。

＞[5494]*rose*様
　[5497]sweeper様
はじめまして。私のあんな文章を分かってくださってありがとうございます＾-＾ 見られる人がいて良かったです。
こちらこそ、これからよろしくお願い致します。

これからは、もっと分かりやすい文章を書けるようにしたいと思います。では、おやすみなさい。

### [5506] kinshi	2002.02.02 SAT 23:45:45　
	
こんばんは、（笑）
今日　２度目のカキコです。　思わず来てしまいました。

おっさん様・・大爆笑です！！大声で、笑ってしまいました。（笑）
おっさん様　最高！！！
無言のパンダ様もそうだったと　思います。

千葉のＯＢＡ３様　　ご安心を！ここの方々は、それはそれは　
優しい方々で、私も一著前にどさくさにまぎれながらも、
参加しています。
きっと私が、最年長でしょう。・・いや、やっぱ
神谷　明さん・・・ですよね、みなさん！（笑）（笑）


元祖無言のパンダ様　
本家無言のパンダちゃんのお誕生日　おめでとうございます。
それと　はじめてのキリ番ＧＥＴでした。考えてもいませんでしたが、
うれしいもんですね、ハハハ；（＾０＾）

年のことは、おいといて、
リョウ・・という名前　　わたしは、あらゆるところで、使っています。
ＧＡＭＥに使ったり・・
ほか、仁　とか、斗　とか好きです。（仁は　エー＠をねらえ！から・・）
学生時代　塾の先生に　斗　という名前の人が、いたのですが、
なんと読むか　知っていますか？　「ますめ」です。
でも好きな理由は、アニメからですが・・最近は、いろいろあるようで、
そういえば、「あ＠ま」って名前が、新聞を賑わせていましたね、
漢字にすると　けっこう強烈ですが・・・
かなり脱線しましたね、では、また。　　

### [5505] hiromi	2002.02.02 SAT 23:43:26　
	
お久しぶりです。仕事が遅くまでなのでなかなかこられません・・・。AHは読んでますけどね。
５５０４＞無言のパンダ様
「本家無言のパンダ」さーん、お誕生日おめでとうございます。家族みなさん冬生まれなんですね。プレゼントはなんだったんでしょう？？？
今週号＞
なんといっても、リョウちゃんかっこよすぎー♪でした。

### [5504] 無言のパンダ	2002.02.02 SAT 22:52:48　
	
こんばんわ★
今日は「本家無言のパンダ」←（娘デス）の誕生日ということで、プレゼント探しに翻弄して疲れきってしまった「元祖無言のパンダ」です(ｰｰ;)前にも書きましたが、うちはみんな冬生まれで、ここんとこずっと誕生日続きの上にクリスマス、お正月と、冬の寒さが身にも、ふところにも凍みちゃってます・・・。

＞ｋｉｎｓｈｉ様（５５００）
初ネットショッピング＆５５００のキリ番ゲットおめでとうございます！「ＣＨカレンダーもネットで・・・」おー、そうでした！私もうっかりしてました(^_^;)
なんとなく、自然に買っちゃってましたネ♪
「ケーシー高峰さん」私が子供の頃に見た姿とほとんど変わってらっしゃらないけど本物かしら？（笑）

＞千葉のＯＢＡ３様（５５０１）
はじめまして(^^)
おっさん様のおっしゃるとおり、私も（私のほうが？カモ）いい年ですよ～。だいじょうぶ♪だいじょうぶ♪（ｋｉｎｓｈｉ様は、まだまだ若いと思いますけど！）
でも、年のことは言いっこなしですヨー（お互い）
千葉のＯＢＡ３様のＨＮ、もしかして、まさか「千葉のオ○サン」と読むのでしょうか？！最初は全然気づきませんでしたけど・・・（違っていたらゴメンナサイ！）
「りょう」の付く名前。私も付けたかったです！
初恋の人の名前が（小学校ン時）りょうすけ君だったから、なんですけどね（笑）

ではでは。(^_^)/~

### [5503] おっさん（岐阜県：２月だね～）	2002.02.02 SAT 21:22:21　
	
毎度、２月に入りましたね。バレンタインの季節ということで今日もいつものご挨拶せ～のおっは～！！
レスっす。
千葉のＯＢＡ３様（５５０１）、初めましておっさん（２６歳の女です）と申します。宜しくお願いします。いやいやいいんじゃないっすか。ここに来る人は年齢関係なしっすよ。無言のパンダちゃん・kinshi様に聞いて見てください。（お二人すんませんＴ－Ｔ）「りょう」の付く名前よさそう・・・今度は孫のときに・・・使うのもいいんじゃないっすか？？？息子さんたちにも「これが私の趣味」って言いきるのも一つの考えかもしれません。私の母親も別の角度になりますが格好が派手なんで（トラの付いたものを着たりするので）おかんはおかんや！って思ってますから。変なこと書いてしまってすみません。
次回のＡＨやっぱ先が見えないっすね。う～んそれがいいのかもということで今日はこの辺で・・・？？？？？

### [5502] 海里	2002.02.02 SAT 16:55:27　
	
こんにちは～
海里です。

前回の書き込みで☆九郎さま☆と☆black glassesさま☆
に指摘され、
「自分はまだまだだなぁ～」と実感しました。
あたしは今まで何をしていたんだろうと…
お二方の意見は、とっても勉強になりました。
ありがとうございました。

また来ます～♪

### [5501] 千葉のOBA3	2002.02.02 SAT 15:05:18　
	
２度目のカキコです。いやー、ゆうべは始めてのカキコ，緊張しました．私め、ここに参加されてる、みなさんの中では、かなり年くってるほうだと思いますが、息子達にバカにされながらも（？）北条先生の作品とりょうちゃんを愛しております。なんせC.Hの連載が終わった年に生まれた三男がすでに１０歳，あの時
「りょう」のつく名前にしようと考えてたのに，長男に大反対され、ダンナちゃんの決めた名前にしたけど、あの時のうらみは、孫の時にはらしてやる。？？？・・・で、「A.H」の展開はショックでした。香ちゃんも大好きだから。ホント、ショックで体重減りましたもん。だから、コミックスで先生がパラレルって言って下さって、なんか救われた気がしました。これからの展開も楽しみです。できれば、もっと香ちゃんにでてきてほしいです、私としては。なんか、とりとめなく書いてしまいましたが，以後よろしく。・・・あー、ここまで書くの、スゲー大変だった。
