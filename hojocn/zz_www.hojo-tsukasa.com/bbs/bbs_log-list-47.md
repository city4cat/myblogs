https://web.archive.org/web/20011122064604/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=47


### [940] りなんちょ（ふぅ～暑い）	2001.07.15 SUN 11:23:25　
	
☆康子さま☆
そうです。
１話で、グラスハートが仕事をしていた場所は、
新宿中央公園です。
ちなみに、冴羽さんがジェネラルと闘ったのも、
新宿中央公園ですよ。

### [939] ネット（熊本）	2001.07.15 SUN 11:17:46　
	
おはようございます。
昨日、アニメの「セイラ」の話をレンタルして見てしまいました。
ふ・・・いいっすねえ。（←おやじ）
康子さんも書かれていましたが、（はじめまして、康子さん）
「お前を泣かす男が、今はお前を・・・」
この後、リョウはなんて言う気だったんでしょう。気になるなあ。

それにしても、「カウボーイビバップ」を制作した時も
只者ではないと思っていたが・・・ＣＨもか！やるな、サンライズ！

気づいたらもう明日は月曜日。都市の方はバンチが読めるのですね。
ＡＨが始まってから一週間が経つのがとても速いです。

### [938] kasumi	2001.07.15 SUN 11:16:23　
	
みなさまおはようございます！

＞ａｋｉさま
小説のタイトルは「シティハンターⅡ」でした。で、私の好きな話は最後の「罠」っていうやつです。さっきちょっと読み返したのですが、リョウの香を思う気持ちがよーく表されていいですよ！

ムックっていうのがあるなんて知りませんでした～すっごい読みたいけど近くの古本屋さんに無いのはもう分かりきっているので、どっか行った事ない古本屋さんに行ったときに探してみよー・・・

### [937] 法水	2001.07.15 SUN 10:56:32　
	
はやねさんや北条義政さんが書かれているようにムックはもう古本屋でしか手に入らないでしょう。今回久しぶりに読み返してみましたが、北条先生、神谷明さん、伊倉一寿（当時はこの芸名）さん、その他アニメのスタッフとの座談会の中でサンライズの植田プロデューサーが「次は『２００１年シティーハンター』で会いましょう（笑）。」とおっしゃってました。実現しませんかねえ？

早根和北条义政写的那样，Mook已经只能在旧书店买到了吧。这次久违地重读了一下，北条老师、神谷明先生、伊仓一寿(当时是这个艺名)，和其他动画的工作人员的座谈会中，Sunrise的植田制作人说“下次在《2001年城市猎人》见面。走吧(笑)。”。实现不了吗?

### [936] 姫 (クーラーかけっぱなし）	2001.07.15 SUN 10:51:26　
	
ありがとう（；；）りなんちょさん！！
今、この辺かな？って所を探してるんですけど、私の天敵が登場で一時中断。
何って？
「蜘蛛」しかもでかい「家蜘蛛」でかいって言っても５CMくらいですが私は大嫌いなんです。
いなくなったらまた探してみます。無かったらぜひお願いします。
とりあえず、９１のビデオだけ出せたのでちょっと見てます。
すごい久しぶり！！なんか楽しい。
結構忘れちゃってる自分がいます。（薄情者と言ってくれ）

### [935] りなんちょ（神奈川県南西部晴れ）	2001.07.15 SUN 10:32:05　
	
☆姫さま☆
おはようございます！
あたしも、結婚した時に、
シティーハンターグッズを処分しようと思ったんです。
でも、何故か捨てられずに、
実家の部屋の片隅に、今もいっぱいあります。
CDやビデオは、売ろうかな？
と、思ったこともありました。
今は、あたしの手元にあり、
大事に整理されてます。
「セイラ」は、テレビのですが
ビデオにとってあります。
よければ、お貸ししますよ？
姫さん、横須賀だし
行こうと思えば、行けますし…

### [934] 康子(横浜市民だけど横浜まで電車で20分)	2001.07.15 SUN 10:26:24　
	
＞りなんちょさま　serieさま
公園情報ありがとうございます！公園好きなので、両方とも行ってみます。
新宿中央公園て、GHが１話で仕事してたところかな？

＞ゆなつさま
わ～い、近くですね。（といっても私は金沢区。ほとんど横須賀よりなんだけど・・・）
川原泉さんは、兄が好きで、「笑う大天使(ミカエル)」を熱心に薦められました。

ところで、私はアニメムックを友達にもらいました。
「お見合い狂想曲」は冴子さんたくさん載ってるからうれしいです♪バスローブ姿とか・・・

### [933] りなんちょ（今日は祭か？）	2001.07.15 SUN 10:24:44　
	
家の外から、おはやしの音が聞こえる…
今日は、お祭りかな？
引っ越してきたばかりだから、町のイベントがわからない…

☆orphanwolfさま☆
ドラマティック マスターのことですが、
２人が言ってるのは、
２枚組の「シティーハンター ドラマティック マスター２」
のことなんですが、中身は一緒みたいなんでです、
でも、紫音♪さんの方には、
「ドラマティック マスター２」に「（監）」
とゆー、文字が入ってるよーなんです。
で、何が違うのかな？と思って…
どちらにも、「Television Data」はあるんです。
謎は深まるばかり…
誰か教えてぇ～！！！

### [932] 姫　（本日もよろしく）	2001.07.15 SUN 10:16:08　
	
おはようございます。起きたばっかりでここに来てしまいました。

＞康子さん
まあ、なんて横須賀よりの横浜なんでしょう！！
近いですね。私は中央まで１０分です。

この前部屋をリフォームの際、結構物を処分してしまったんです。ビデオとかも・・・まさかここに来てこんなにCH熱がぶり返すなんて思っても見ませんでした。
なんか捨ててしまったような気もするし、捨てなかったようなきもします。心配なのはビデオ。今更「セイラ」が見たくなってしまった。９１は全部取って有るんですけどね。
こうなったらDVDを出るのを期待しよう！！
（今のBGM　TMのGIFT FOU FANKS ）


### [931] 北条義政	  2001.07.15 SUN 10:04:31　
	
↓のサウンドドラックはサウンドトラックの間違いです。

### [930] 北条義政	  2001.07.15 SUN 10:00:58　
	
＞ミラクルハートさん
ところでＣＨは、コミック版と文庫版とパンチ版で内容とか違うところがあるんですか？どなたか教えてください。

これに違いはありません。内容も同じです。違う点といえば出版社が違うことと、文庫版は全１８（？）巻ということです。基本的には違いはありません。

＞冴子さんとリョウのお見合いの話、見てみたいです。
ムックって何ですか？それは今でも手に入るものですか？

これは９１年に集英社から発売された本です。イラストや対談、全話のダイジェスト、短編小説が収録されてますが、たぶん手に入らないと思います。あるとすれば確率は低いけど古本屋ですかね。あと、登場人物の声優やスタッフの名前もあります。

＞冴子和獠相亲的故事，我想看看。
什么是Mook?那个是现在也能到手的东西吗?

这是91年集英社发售的书。书中收录了插图、对谈、全集文摘和短篇小说，但我想可能买不到。如果有的话概率很低，是旧书店吧。还有登场人物的声优和工作人员的名字。

### [929] ＴＡＫ（テスト期間中にバイト）	2001.07.15 SUN 09:58:15　
	
うーん。今日はバイトだ、行きたくない。今日もゆっくりカキコを読んでたいよー。

＞あずみちゃん
会員番号一万代っすか!?結構前からのファンなんですね。カラオケっすか？Ｂ'zにＴＭにタ○チまでそれは楽しそうだ。

カラオケといえば僕の好きな「スーパー　ガール」が歌えるとこが少なくなってきたのがカラオケの楽しみを少し減らしてるんだよなぁ。「stil love her」なんかはＴＭが歌っているだけあって強いんだけど、ファンの数の違いでしょうか？

あー、バイト行きたくねぇ。

### [928] 北条義政	  2001.07.15 SUN 09:52:19　
	
シティーハンター（１と２の総集編）
ドラマティック・マスター
ドラマティック・マスター２

シティーハンター
オリジナル・アニメーション・サウンドドラック１
オリジナル・アニメーション・サウンドドラック２

シティーハンター２
オリジナル・アニメーション・サウンドドラック１
オリジナル・アニメーション・サウンドドラック２

シティーハンター３
オリジナル・アニメーション・サウンドドラック

シティーハンター91
オリジナル・アニメーション・サウンドドラック

シティーハンター　愛と宿命のマグナム

シティーハンター　ベイシティーウォーズ/１００万ドルの陰謀

シティーハンタースペシャル　ザ・シークレット・サービス

シティーハンタースペシャル　グット・バイ・マイ・スイート・ハート

シティーハンタースペシャル　「緊急生中継!?　凶悪犯　冴羽りょうの最期」

ＣＤはこんな感じですね。

アニメは現在、スカパーのキッズステーションで放送してます。もち、ＣＡＴＶでも見れます。ＤＶＤは出るとしたら、キャッツ・アイのあとになるのでは？　キャッツ・アイは２もＤＶＤになるんですかね？

早くアニメのほうも再開してほしいです。「シティーハンター２００Ｘ」とかだったりして。それか、漫画のその後の展開からＡＨに移るのもいいかも。

### [927] 康子(横須賀までチャリで１５分)	2001.07.15 SUN 09:48:41　
	
おはようございます！

アニメ「セイラ」でのリョウの命ゼリフは確かこうです。

「いいさ、射てよ。
　お前が元に戻らないなら、生きていてもしょうがない。
　お前を泣かす男が、今はお前を・・・」（ブチュッ）（←こらっ）

うわぁ～～リョウちゃんたら！！
すっごいきれいなKissシーンでしたよね。
原作でもしてないのに、アニメでキスさせちゃっていいのだろうかという声があったけど・・・
そっか～あれほんとは最終回だったんだ。

### [926] はやね（大阪秘密基地）	2001.07.15 SUN 09:44:31　
	
＞ゆなつさま、リンダさま
ムックは集英社から'91年発売です。頑張って探してみて下さい(･･･きっかり十年前）。でももう出版社にはないと思うので古本屋とかの方が可能性は高いかな？こないだインターネットオークションに出てるのを見ました（爆）。薄いんだけど、神村さんがこれのために描いたイラストが山ほどあったり、アニメスタッフと神谷さん・伊倉さん・北条先生で対談していたりして今でも大切にしています。

＞それとゆなつさま、横レスなんですが神村さんのＨＰにはこのページの左の「リンク集」から行けますよ。こだま監督のＨＰにもＣ・Ｈの絵コンテとかあるので共に行ってみて下さい。リンク先が増えて、北条先生の交友関係が垣間見れて楽しいですね～。

＞ゆなつさま、リンダさま  
Mook是集英社91年发售的。请努力找找看(…整整十年前)。不过出版社应该已经没有了，所以旧书店之类的可能性更大吧?前几天看到网上拍卖了(爆)。虽然很薄，但是神村先生为这个画的插图堆积如山，动画工作人员和神谷先生·伊仓先生·北条老师对谈过，现在也很珍惜。

### [925] ゴールデンハンマー	  2001.07.15 SUN 09:40:46　
	
～実葉さま～
重ね重ね有り難うございます。
自分も当時は録画して持ってたのに、何時の間にか何かが重なってしまって、今や１話もない状態です。十年一昔ですから。
再放送しないかなぁ。

昔のアニメってかなり面白かったと（違った意味も含めて）思うのに、最近は全然再放送しないので残念。結構放送コードにひっかかるみたいで、自分がガキの頃見てた時ですら「このピーーー野郎！！」とかってあったから。それより最近のＴＶの方がひどい気がするんだけどなぁ。人の欠点ネタにして笑ったり、もっこりネタがゴールデンタイムでも平気で流れてるし。これって時代なんだろうか・・・。

～康子さま、ゆなつさま～
レス有り難うございます。
ゆなつさまの言われた「セーラ」は別の話だと思われます。何故なら、そういえばそんな話もあった気がすると思ったから。（いい加減。）誰か知ってる人いないかな？
やっぱり再放送しかない！

### [924] リンダ	2001.07.15 SUN 08:43:26　
	
＞法水さま
なるほど、ムックですね。でも１０年前に出た本じゃ、本屋で普通に購入するのは無理でしょうねえ。古本屋があれば立ち寄ってみます。（と言いつつ、ゆなつさんの情報を期待してます。ゆなつさん、がんばってくださ～い。）

＞りゅうさま
いいですよ、「リンダ」でも「りんだ」でもどっちでも（笑）このＨＮ、もう少し真面目に付ければ良かったと後悔してたりして…。ＡＨのショックが大きすぎて、真面目にＨＮ考える余裕がなかったんです。こうなりゃ、「山本」（本名）とでも呼んでください。（嘘です）

＞法水さま  
原来如此，是Mook。但是10年前出版的书，在书店一般买不到吧。如果有旧书店的话就顺路去看看。(话虽如此，我还是很期待ゆなつさん的消息。ゆなつさん，加油哦。)


### [923] orphanwolf（奈良市・隠れ家在住）	2001.07.15 SUN 04:22:48　
	
∑ｂ（≧▽≦）やっとログインＩＤ覚えたッス！！（←頭悪そ）

＞りなんちょ様・紫音様
ドラマティックマスタ－1・2の違いなんですが、話の食い違いが出ているのは、もしかして初回限定か否かの違いなのではないでしょうか？ＤＭ1は初回限定でＣＤ同サイズのブックレットが封入されていて、それに歌詞、ＣＤドラマの絵コンテ、スタッフメッセ－ジ、ＴＶデ－タが書いてあります。初回以外ではこれがついてないハズなのでそれが原因だと思われますよ。

＞357Ｍ様
質問1について
恐らく、ミックはリョウよりすこしはマジメに仕事してるのでは？でも先生も書いていましたが、リョウとにている分かずえが香と同じ苦労してますよ、確実に（笑）
質問2について
・・・好きな歌が多過ぎて決められないです（Ｔ＿Ｔ）「ゴ－ゴ－ヘヴン」「ス－パ－ガ－ル」「エンジェルナイト～天使の居る場所～」「ＥＡＲＴＨ～木の上の箱舟～」「ＧＥＴＷＩＬＤ」「セイラ」「ＲＯＣＫ　ＭＹ　ＬＯＶＥ」「十六夜」「still love her」はテ－マソングの中では捨てがたいです！！挿入歌は「forever in my heart」「footstep」「what your love」が好きです！！（←優柔不断っぽい《汗》）

＞ゆなつ様
91は固定話数が決まっていたのでアニメを一回みる度に「あ-あと6話かぁ（ため息）」とかいう状態で見ていた記憶がありますよ。

＞玉井真矢様
おめでとう御座います！！是非家宝にして下さい！！スゴイうらやましいですね。私もノドから脚が出る位欲しいです（笑）直筆サイン色紙にはかないませんが、パ－フェクトガイドブックのアンケ－トを送ったら表紙の絵のテレカが送られてきて、良い年して大喜びした覚えがあります。

### [922] りゅう	2001.07.15 SUN 03:34:22　
	
ミニクーパーの話が出ていたので。
この夏に欧州で新しいミニクーパーが売り出されるんですよね。
ＢＭＷから。
うちは車ない家なので、何にも分からないけど、日経のページでその話題見ました。
でもなんか丸っこくってちょっと違うって気が･･･。
ミニワンで２００万くらいとかなんとか書いてあったかな。
あ、もし前に書いてらっしゃる方がいたとしたらごめんなさい。
いちお、全部目は通させてもらってるんですけど･･。

### [921] ミラクルハート	2001.07.15 SUN 03:29:43　
	
こんばんは。
夜はダンナとパソコンの取り合いで、やっと奪還しました。
過去ログ見るのに時間かかるのに・・・もう、こんな時間。
子供ネタ、ちょっと盛り上がりましたね。リョウや海坊主さんたちの子供も気になりますが、私はリョウの子供のころが知りたいです。きっと壮絶な過去に違いないけど、どうしたらあんなもっこり男に育つのか是非知りたいっ！番外編で書いてくれないかなぁ？
ところでＣＨは、コミック版と文庫版とパンチ版で内容とか違うところがあるんですか？どなたか教えてください。
法水様
冴子さんとリョウのお見合いの話、見てみたいです。
ムックって何ですか？それは今でも手に入るものですか？ 

