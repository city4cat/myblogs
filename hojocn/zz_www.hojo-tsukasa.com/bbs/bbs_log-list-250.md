https://web.archive.org/web/20020817222902/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=250

### [5000] sweeper(おりていた。相手の拳銃に目が―)	2001.12.13 THU 21:58:59　
	
こんばんは。また来ました。
今回は感想を。

[今週号の感想]
コヨーテ君。グラスハートとの過去が切なかったです。
最後のページの２人のセリフに少し救われました。でも、コヨーテ君の裏切りがわかった時、部隊はどう動くんだろうか・・・。それがちょっと気になります。２人の関係。何となくりょうと香の関係を思い出してしまいました。「笑い方を教えてくれ」(間違っていたら失礼します。)というセリフの通り２人が心から笑える日が来ることを待ってます。

それでは失礼します。

### [4999] sweeper(互いに向けられた銃口は―)	2001.12.13 THU 21:40:08　
	
こんばんは。今回はレスです。

[レス]
＞無言のパンダ様。[４９４１][４９４７]
レスありがとうございます。「りょうちゃんナンパが大失敗！」です。難波圭一さんと言えばやっぱりタ＠チの「カッちゃん」です。(私はタッちゃん派でしたが。)「タンノ君」意外ですねー。「海野ぐりお」と「伊＠家の食卓」のナレーションは知っていたのですが・・・。それからセリフの引用、よろこんでいただけたようでありがとうございます。「あなたの愛」私も言ってしまいます。

＞さえむら様。[４９４２]
レスありがとうございます。「りょうちゃんナンパが大失敗！」です。お仕事頑張って下さいね。

＞K.H様。[４９７５]
「ワオ！やったじゃないカオリ君！」
「きゃー！当たった！真ん中よォ！」(間違ってたら失礼します。)
いいえ！覚えていますよ！セリフの引用いつも楽しみにしていたんですから。
お久しぶりです！お帰りなさいませ♪

＞まろ様。
遅くなりましたが、SS完結とHP開設おめでとうございます！早速、寄らせていただきました。それから、K.H様とのセリフの引用とParrot様との造語。楽しんでいただきありがとうございます。「りょうちゃん大カンゲキー！」です。

それではこれにて。

### [4998] 葵	2001.12.13 THU 21:31:55　
	
＞まろさま
　遅ればせながらＨＰ開設、おめでとうございます♪先ほど覗いて来ましたが、あいにく今日はチョット忙しいので、また今度じ～っくりとお邪魔させていただきますね♪実はこれから、ＣＥのビデオを見るのさっ☆（笑）

### [4997] おっさん(岐阜県：おいっす～！！）	2001.12.13 THU 21:19:40　
	
毎度、雨やみましたがなんか明日雪になりそうで・・・（家は愛知県の県境ですが橋越えると雪の量は違うんだなこれが）ということで今日もいつものご挨拶せ～のおっは～！！
Adolph様（４９９５）、はじめまして。台湾の方ですかね？広東語か何語か分かりませんが＾～＾；バンチが台湾でも出てるのでしょうか？こんなこと書いてしまってすみません＾～＾；もっとうまく書きたいです。
明日は仕事休みですが、雪が・・・ということでこの辺で・・・・？？？？？？

### [4996] Adolph	2001.12.13 THU 16:03:04　
	
大家好
我覺得gh的感覺沒有ch的好

### [4995] Adolph	2001.12.13 THU 14:53:25　
	
Hi I'm Taiwan Fans

### [4994] 無言のパンダ	2001.12.13 THU 14:11:34　
	
こんにちわ。
今日は雨あがりの、うっとうしいお天気で頭痛～い(ｰｰ;)
でも、やっと年賀状が出来上がってホッとしている無言のパンダです。あ～、でももうそんな時期なんだなぁって感慨深いですね。

＞まろ様（４９９３）
ＨＰの「参加の仕方」見ましたー♪
ここのＢＢＳもですが、私の友人もＨＰ持ってて、そこの掲示板に参加する時にもアドレスやパスワードを入力して手続きしたことあったけど、それと同じようなことなんですね。な～る！
アドレスですが、私はメインのアドレスともう一つ、ポスペ用のを持ってるんですが、ホットメールにも登録したほうがいいのでしょうか～？ホットメールの内容は魅力的だけど、もともとメールのやりとりする相手も少ないのに、これ以上増やすのもなぁと思うし、どうしようかなぁ。

### [4993] まろ（神奈川県：長い・・・汗）	2001.12.13 THU 08:28:16　
	
おっは～っす！

くほ～（涙）聴コミの当選確率って、１００万分の１０００、つまりは１０００分の１。まァ「万に一つ」程ではないにしろ、こら・当たらんわな（冷や汗）
商品開発企画室の方、何とか市販して欲しいです、ハイ。

あと、昨日はボキのＨＰの突貫工事してたので、感想文書けなかったのだ
（↑ハ○太郎風・さぶっ！）
さて、レスです。

[４９８１]・[４９８８]ｌｕｎａさま
　レス、ありがとうございます！
「りょうちゃん、槇ちゃんに感謝しちゃうー」です（ＣＨ「墓地のナンパンスト」より・笑）
　ＨＰの方、お越し下さりましてありがとうございました（謝謝！・しぇい×２、と読んでネ）
これで、気兼ねなく（？）暴走できるゾ！（笑）

[４９８７]無言のパンダさま　
　レス、ありがとうございます！
「りょうちゃん、槇ちゃんに感謝しちゃうー」です（笑）
え、と、ボキのＨＰのＢＢＳ等にカキコする時、自分のトコのメールアドレスと、パスワードを入れないと出来ないんですヨ。
詳細は、ボキのＨＰ上にてご説明しませうネ。
　左のメニュー欄に『参加の仕方』を追加しますので、それを・ご参照下さい。

[４９８９]おっさんさま　
　レス、ありがとうございます！
「りょうちゃん、槇ちゃんに感謝しちゃうー」です（笑）
早速のご参加、誠にありがとうございます！
ＢＢＳとか、ジャン×２使ったって下さいネ！

[４９９０]クロックさま　
　レス、ありがとうございます！
「りょうちゃん、槇ちゃんに感謝しちゃうー」です（笑）
ＳＳを最後までご愛顧下さり、誠にありがとうございました（謝謝！）
　北条先生の短編集に似ているだなんて、とんでもない！
（比べるだに畏れ多すぎマス！・滝汗）
とてもじゃないっすケド、あのＳＳ・ボキは何一つ関与してないシロモノなんスよ（ＣＨ文庫版の最終巻の、北条先生の『あとがき』のように、ボキの頭の中で、それぞれの登場人物が個性をぶつけあって自然にできちゃった、というのが正しいです）。
　ですから、なんというか（照）第８話　ｲ尓已経死了！（にい・い・ちん・す・ら！。北斗神拳？を使うトコ）や、第１０話　四面楚歌での（下手な言い訳するトコ）俊夫の行動なんか、
「おいおい・そんな事するかァ？！・爆」
とか、腹抱えながら（下書きの際、その情景を）書きとめてましたし（汗）
あ、あと、ラストのリョウのセリフのアドバイス、ありがとうございました！（→早速、ＨＰ版では使わせて戴きます・笑。ヘコんだ雰囲気（？）が出てる、って感じしますしネ）
一応、アレは音・訓読みが可能（訓読みは、る○うに剣心風に・笑）だった為、採用してみただけなので（微笑）。

[４９９２]あおさま　
　レス、ありがとうございます！
「りょうちゃん、槇ちゃんに感謝しちゃうー」です（笑）
ＨＰ、実はすごくカンタンに作れちゃいます。
（ＭＳＮでは誰でも、何らかのネタ（写真・絵などの素材。ＪＰＥＧ（ファイルに付ける拡張子は『．ｊｐｇ』でＯＫ）さえあれば、あとはココのカキコをする感覚でお手軽に出来ちゃいますので）
　ちなみにボキしゃん、Ｍａｃで描いた絵（※１）をＪＰＥＧ（→ジェイペグ、と読む）形式にしてフロッピーで保存、ネットに常時接続してるＷｉｎのＰＣでアップロードしてます。

※１・・・例えば、ホームの左側のメニューで『ＴＶを観る』→『ＣＥ２００１』の順にジャンプした先の『内海　幸』の絵なんかそうです。


[今週号の感想]
　劉（多分、読みは『ラウ』だと思う。セ○の格闘ゲーム、ＶＦ（バー○ャファイター）のキャラで同名のが居る）クンとＧＨの過去、すごく切なくなって、涙が出そうになりました。
　極限状態での、劉（以後『ラウ』と読んでネ！）のＧＨに対する思いやり（懲罰で食事を与えられなかった劉を心配したＧＨが、こっそり自分の食事を与える所からも窺い知れる）を観た時（劉のＧＨに寄せる想いの『深さ』を知れば知るほど）、私の頭の中で、リョウと香の関係とオーバーラップしてしまいました。
　『心から大切に想う人を、生命（いのち）懸けて護りたい』という、切なるまでの熱き想いが。
　それ故、ＧＨには、劉だけでなく・リョウと香の３人の愛情が注がれ、そして支えられているのだ、という事を彼女自身・気付き始めているのかも知れません。だから、２１話以降、他人（ひと）の身を案じ、他人に生きて欲しいと願うようになったのかなぁ、とか思います（最初は『罪の意識』からか、自らの死を願っての事で心配しましたが）。
　もう、心配は要らないよね、ＧＨ。ボキも応援しているから！
（モチ、劉（ラウ）クンの事も応援してるゾ！←気持ち悪いゾ！）

### [4992] あお	2001.12.13 THU 01:01:30　
	
解禁ですね♪

＞まろ様
HP開設おめでとうございます！！私もチャレンジしてみたい･･･。

こんしゅうごうのばんち＞
ようやくGHがはっきりと｢生きる｣ことを意思表明しましたね。あの冴羽氏の言葉が効いているのでしょうか(2巻の最終話参照)？？｢死にたい｣という気持ちが｢生きたい｣に変わるのは大変良い傾向♪あの台詞がとても嬉しかったです～。
ところで･･･最近のアフガンの映像を見るにつけ思ったのですが･･･、コヨーテ君も、GHも、2人とも、本当に小さい子供の頃から、訓練が当たり前であり、｢誰かを殺す｣ために鍛えているっていうのが日常だったんですよね。あんなに小さな子供の頃からだと、それが当然となり、洗脳されることのほうがはるかに多いはず。でも2人はそれを苦しんでいる･･･これはやっぱりすごいことだと思うのです。お互いに大切だったから･･･GHは｢人を殺す｣＝｢36番を殺す｣になって、とても辛かったのでしょうねえ。

来週は冴羽氏がきっと出てくるはず～～！！そろそろまた香さんにも会いたくなってきました(笑)。この傾向をきっと喜んでいることでしょう。

### [4991] kinshi	2001.12.13 THU 00:17:34　
	
こんばんは、いや～１２月は、やっぱ　いそがしいね～なかなかカキコできないよ　でも　ちゃんと　ロムっています。

・・・・で、どなたか詳しい方、いらしたら、教えてほしいんですが、去年から、ついこの間まで、何回か　蜂の夢を見るのですが、なんなんでしょう？　数回も見るので、なんかあるのかな？って思ってしまうんですが、刺されたり　襲われる　というのは、ないのですが、寄ってくるのです。なんで蜂なんでしょうか？しかも季節外れに・・・
そういえば、蜂も、ゴキちゃんも　ＣＨに、出演してましたね、次はなんだ？

では、また。

### [4990] クロック	2001.12.12 WED 23:50:25　
	
　こんばんは　クロックです。

　どうして私がバンチを買う時はいつも売り切れているのだろう…？先週は何とか手に入れられたからいいものの、誰かの裏工作なのかしら…？もしや…青龍部隊の趙チーフが…!?（んなことないって）

　＞まろ様[4968]
　こんばんは、クロックです。ＨＰ開設おめでとうございます！今は事情があって訪れることができませんが、訪れたときはよろしくお願いします！
　小説、終わってしまいましたね。とってもよかったです。読んでいて何故か切なくなって…涙ぐんでしまいました。北条先生の短編の一つ「少女の季節・サマードリーム」のラストに自分の中でかぶっていたので…。ところで、一つこうしたらいいのではないかというセリフがあるんです。最後の「愚っ！」を「屈っ！」にしてみては…と。提案なので聞き流してくださって結構です！

　＞ちゃんちゃん様[4976]
　こんばんは、クロックです。「イッシータ」の話、大笑いです！私は「シータ」と聞こえていたのですが。やはり人によって受け止め方が違うのが、漫画やアニメ、物語の面白さですよね。

　ここまでの同じ文を３回も打ち直してしまいました。何故か強制終了になってしまって…よくわかりません。パソコンを勝手に信用していた私は何なのでしょう…？管理人様もお疲れ様です
！

　それでは皆様よい夢を…。

### [4989] おっさん(岐阜県：おいっす～！！）	2001.12.12 WED 23:05:42　
	
毎度、今日はなんだかんだとドタバタしてたおっさんっす。皆様おげんこ～ということで今日もいつものご挨拶せ～のおっは～！！
今週号カキィーンじゃなく解禁、信宏くんあんさん切ない人かもね。かすかにＧＨに対しての感情あったと思いますけど・・・。
ということでレスっす。
まろはん（４９８０）、おいっす～！！ＨＰ開設おめでとはん！！登録したじょ～。また覗くので・・・。
lunaちゃま（４９８１）、おいっす～！！一応、仕事先のＳＶ（スーパーバイザー、店長よりも上の位の人）には話しました。
今日は、奴が休みだったので平和でした。
ということで、今日はこの辺で・・・？？？？？

### [4988] ｌｕｎａ	2001.12.12 WED 22:51:04　
	
こんばんわ
まろさん
ただいま、行ってきましたよ
これからですねぇ
楽しみにしてますよ

### [4987] 無言のパンダ	2001.12.12 WED 22:37:36　
	
＞まろ様（４９８０）
まずはＨＰ開設おめでとうございます！
あの～いきなりＵＲＬをクリックしたら、ＨＰに行けたので
お気に入りに追加したのですが、いいのでしょうか？
まろ様のカキコにあった順序を踏んでないけど、大丈夫かなぁ。
ＰＣ用語もホットメールについても、よくわかりませーん（大汗）私のような者にもわかるように今一度ご説明を！

### [4986] 葵（ゴメンネ☆意味はこちら…）	2001.12.12 WED 21:53:23　
	
　儚い→①将来確実にどうなるという目当てが無い。
　　　　②いつ終わるかわからない。
　慟哭→声を立てて泣き悲しむこと。

　　　　　　　　（「三省○国語辞典」より抜粋）
　　　

### [4985] 葵	2001.12.12 WED 21:46:21　
	
＞けいちゃんさま［４９８３］
　…次号予告の読めない漢字って…「せんせい」の中のやつ？（それしか見つかんなかったけど…）
「ＧＨと劉信宏の儚（はかな）い運命に新宿慟哭（どうこく）!!」で、いいはずですよ。他の個所だったらごめんなさい☆

### [4984] 葵	2001.12.12 WED 21:40:07　
	
　解禁ですね。今回のオハナシ、ずいぶんメルヘンチックで可愛らしかったです。（前半だけね☆）ＧＨも信宏もあの状況下だからこそ、ああなったんじゃないか…と思いました。しっかし二人の淡いラブラブの瞬間を盗み見ているカメラ、こわいなぁ…。いやらしいっ☆信宏と再会して、ＧＨに笑顔が戻りますように…。でもこれで、リョウの存在ってホント「父親」みたいになっちゃったね☆（笑）

＞三毛猫さま［４９８４］
　信宏を探せっ！…実は私も二巻を見てやってました☆そうですね、わりと早い時期から出てますよね。趙チーフを父親のように…っての、妙に説得力あってドキッとしちゃいました。でもそうすると信宏くんは、想像以上の固い決意でＧＨの元へ行ったんでしょうね。がんばれっ!信宏っ!!

＞無言のパンダさま［４９７７］
　わははっ☆と笑ってしまいました。そうですね、やはり無言のパンダさんは「パンダ」以外ないでしょう。しかも、スペシャルでかいヤツ…ね♪

### [4983] けいちゃん	2001.12.12 WED 21:16:44　
	
解禁～
ということで・・
うむ。信宏がＧＨの笑顔に、ぽっ♪っとなっていましたが、こっちがぽっとなっちゃう。でも、笑うことを忘れたなんていうＧＨ，やっぱかわいそうですね。これからいい方向にむかってほしいです。それにしてもあの殺人部隊、なんとかならんかね～？？
李さん、あんたの部下だろうが！なんとかしなさい！！といいたいよ～。そいえば、りょーさん今週出なかったねえ。ちょっとさびしい。でも、今後とても一途で純粋な信宏君にと教えるんじゃないかってちょっと心配・・そこでＧＨのハンマーがとんできたりして！！な～んてね。
そいえば、国語に強いかた、来月の予告の見出し（他のマンガの横の部分にあったはず）のが、エンジェル・ハートは漢字が難しくてしたがっていみもわからないんです～。だれか読み方といみがわかればおしえてください～。
なさけないのですが。。。

### [4982] やまねこ	2001.12.12 WED 19:14:21　
	
こちらは超久々になります。
今日A.H２巻を買いました。私はリョウとG.Hが出会った時を知らなかったので、読んでて涙…でした。リョウが香さんの記憶が残っていると知ってあんなに動揺したなんて…。そして香さんがドナーカードを持ったわけも。もともとはリョウのためだったのですね…。そりゃあ香さんの心臓がどうなったか余計気になりますよね。あー、リアルタイムでみなさんと感想言い合いたかったな！！『A.H』はいつも私に基本的なことを思い出させてくれます。
好きな人の側にいる幸せ。生きていてくれる幸せ。

★今週のA.H
北条先生って大人の恋バナだけじゃなく、子供の恋バナも上手。以前、沙羅ちゃんのシリーズの頃も思いましたけど。
でも組織の中で人を思いやる心なんてよく育ったなぁって思います。あの組織では人を殺してなんぼでしょうし、まずは自分が生き残ることが先決ですものね。自分を差し置いても誰かを守りたいって気持ち、大切ですね。
今週号を先に読んだときは、なんだかリョウが寂しく感じた。G.Hと信宏の先輩（？）たち、リョウと香さんという素敵なカップルもいたこと、知ってほしいなって。けど２巻を読んで、大丈夫そうって安心しました。心臓だけじゃなく、受け継ぐ者として、この二人ならきっと大丈夫！信宏がリョウみたいにエッチになったらどうしよ…（＾＾；

### [4981] ｌｕｎａ	2001.12.12 WED 19:11:25　
	
こんばんわ
解禁になりましたねぇ
でわ、書きますぅ
今回、リョウさん、まーーったく、ちらっとも出てこなかったですねぇ
まぁＧＨの思い出？やから、仕方ないけど・・・
リョウさんを心の恋人にしてるあたしとしては、
さびしーーいすっ
ねっ無言のパンダさん（又もや同意を求めてますぅ）
やっぱ、これは、ＧＨの本ってつくづく思ってますよ
けど、リョウさんをバンバン出して欲しいよぉ
もちろん香さんも・・・ｌｕｎａのささやかなお願いですぅ
おーーーっ
やっぱり、ストーカーに狙われているおっさんさん
ほんま、気ぃつけてやぁ（大阪人なんで）
やっぱ、ＸＹＺだあーーーー
リョウさーーーーん、おっさんさんを守って下さーーい！

まろさーん
おめでとうですぅ
しばらく、毎日、ＵＲＬ出しておいて下さいねぇ
いやぁ、めでたいなあ
