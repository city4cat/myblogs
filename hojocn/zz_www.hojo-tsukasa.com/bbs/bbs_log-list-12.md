https://web.archive.org/web/20011121230819/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=12


[240] ｍａｍａ  2001.07.06 FRI 21:17:01   
ようやく出せるー。みんなの楽しい掲示板も読める。  
ＣＨのキャラが、出ていてうれしいー。でも、香の事故死は泣きました。リョウの「香は生きている。」の言葉も感動しました。  
香が死んで自分の居場所を無くしたように感じた。「最後の仕事をやったらやめるさ」って言っていた言葉が、そう感じた。  
リョウの、香に対する愛がＣＨよりすごく感じました。  

终于可以拿出来了。也能阅读大家的快乐掲示板。  
很高兴能看到CH里的人物。但是，香的意外死亡让我哭了。獠的“香还活着。”的话也让我很感动。  
香死了，我感觉自己失去了容身之处。“做完最后一份工作就不干了”，这句话让他感觉到了。（译注：待校对）  
比CH更能感受到獠对香的爱。  


[239] momo  2001.07.06 FRI 20:35:30  
こんばんわ。はじめて書き込みします。  
このＨＰでＡＨの連載を知って以来楽しみに毎週がすごく早く感じる今日この頃です。  
リョウと香の関係が進展していたという嬉しい反面、香がすでに死んでしまったというのがすごくショックです！  

晚上好。第一次发帖。
在这个HP知道AH的连载以来，我就一直在期待，每一周似乎都比上一周过得快。    
獠和香的关系有了进展，这让我很高兴，但香已经死了，这让我很受打击!  


[238] 金田　朗   2001.07.06 FRI 20:09:55  
リョウと香ちゃんの関係がすごく進展しているのってお互いの気持ちが報われたって感じで嬉しいですよね。  
でも、回想シーンとかでその関係を見る度『あぁ、ＣＨでは表現できない関係だったんだ。ＡＨだから二人のこの関係が見れるんだろうな』と思います。そしてやっぱり、違うコンセプトの話なんだなぁと実感するんですよね。私って勘ぐりすぎ？  

獠和香的关系进展得很好，感觉彼此的心意得到了回报，很开心呢。  
但是，每次在回忆场景中看到那种关系的时候都会想『啊，是CH无法表现的关系啊。因为是AH，所以能看到二人的这种关系吧』。而且，果然是不同概念的故事啊。我太过分了吗？    （译注：待校对）  


[237] リンダ  2001.07.06 FRI 20:01:51　  
リョウと香の結婚、すっごく気になりますねっ！記念写真を撮るくらいだから、きっとプロポーズもあったんでしょうし、リョウがどんなプロポーズをしたのか知りたいです。きっとこちらまでメロメロになるような事言ったんだろうなあ。（←妄想？）でも、じれて香から言い出したって事もあるし…う～ん？？？  
北条せんせ～い！！絶対その辺りも教えて下さいね！！  

獠和香结婚，我很在意呢!既然拍纪念照，肯定是求婚了吧，我想知道獠是怎么求婚的。我想他一定说了些让我也心软的话吧。(←妄想?)但是，也有可能由香着急说出来…嗯? ? ?  （译注：待校对）  
北条老师! !这一点也一定要告诉我哦! !    


[236] モネ(新宿まで30分くらい）  2001.07.06 FRI 19:19:12  
やっぱり皆さん同じような事気になっているのだな―、と感動です．リョウと香があのマンションでどんなかんじで暮らしてたのか、とか回想シーンで出てこないかな―と期待してます。あと、やっぱり美樹さん気になりますね。海坊主さんが一人でやってるなんて変だし・・。子供とかできたのかなー？それで忙しいとか？とか勝手に考えていますが・・。携帯の待ち受けってあるんですね！探してみなくっちゃ！！私も今度のバンチ楽しみでしかたありません・・。月曜夜12時すぎというか（正式には火曜になってるんですけど）に、こそこそとコンビニに買いに行っています。まだ紐に括られてるので、店員さんに「バンチ欲しいので開けてください」とかって図々しくもお願いしてしまってます。普通だったら絶対出来ないんですけど、リョウのためなら？ってかんじで。。毎週欠かさず懸賞も出してます！もちろん、感想とアンケートのため！です。一冊ずつしか買ってないので、とっても読みたい方にお譲りできなくてごめんなさい・・。

果然大家都在意同样的事情啊——很感动。我期待着能不能出现在回忆场景中，比如獠和香在那栋公寓里是以怎样的感觉生活的呢？还有，还很在意美树。海坊主一个人在做，很奇怪…。有孩子了吗?所以很忙吗?我会想到类似这些...。有手机待机画面啊!必须找找看! !我也很期待这次的Bunch。星期一晚上12点多(正式是星期二)，偷偷摸摸地去便利店买东西。因为还绑在绳子上，所以厚脸皮地对店员说:“我想要一根带子，请打开。”一般情况下我是绝对做不到的，但是为了獠呢?这样的感觉。。每周都有悬赏!当然，是为了感想和问卷调查!因为只买了一本，所以不能让给非常想看的人，对不起。（译注：待校对）


[235] 優希(大阪）2001.07.06 FRI 19:17:40　  
最近気がつけばシティハンターの歌を口ずさんでいます。覚えているものですよねー。  

最近一不留神就会哼唱《城市猎人》的歌。我还记得呢。  


[234] ｓａｎｄ（三重県山奥）2001.07.06 FRI 18:47:14  
またＣＨのメンバーに会えるなんて！  
でもまさか香が亡くなっていたなんて・・・  
しかもＡＨの連載知ったのがバンチの６号から！  
あー不覚！！  
コアミックスのＨＰであらすじは読んだけど  
最初から読みたーい！！  
＞美雨さん  
私も回想シーンすんごく楽しみです  
＞夏兎さん  
私も「初キス」ってとこ気になりました  
知りたいですよねー  

又能见到CH的成员了!  
但是没想到香已经死了…  
而且从Bunch的6号开始就知道了AH的连载！  
啊不经意！！   
虽然在Coamix的HP里读了故事梗概，但是想从最初开始读! !  
＞美雨さん  
我也很期待回忆的场景。  
＞夏兎さん    
我也很在意「初吻」。  
我很想知道。  

[233] はむとらお(博多）・今日は残業中  2001.07.06 FRI 18:38:35　  
ちょっと私信になりますが・・・  
  
＞花見　様  
ありがとうございます！！私の周りのコンビニは全滅だったんです。そうかぁ、博多駅ですね。自宅からチャリで１０分です！  
あ、雨降ってますね・・・　行ってみます。もう悔しくて悔しくて・・。だって、初キスですよ！あの二人の！絶対探します。  
山笠、毎年きれいですよね。みてると心が晴れますね。  
  
＞Ｂｌｕｅ　Ｃａｔ　様  
私がＡＨを読み始めてから感じていたＣ・Ｈとの心の隙間っていうか、喪失感っていうか、なんかそんなものをきちんと文章にしてくださってて、妙に安心できました。有難うございます。  
第１話で香の「死」を知ってから、いくら漫画の世界なんだと分かっていても、１０数年心に染み付いていたものを、取り去られるというのは、私にとってとても耐えがたいものでした。  
でも、段々とストーリーが進んで、すこ～しづつ受け入れられるようになったようです。  
  
これからも、先行きを見守りつつ、リョウと香の思い出を暖かくしまっていきたいなぁと思ってます。  

这有点像私信，但是......  

＞花見　様    
谢谢! !我周围的便利店全军覆没。是吗，是博多车站吧。从家骑自行车10分钟就到!  
啊，下雨了呢……我去看看。我已经很不甘心了……因为是初吻啊!那两个人的!一定要找。  
山笠，每年都很漂亮呢。看着心情就好了。

＞Ｂｌｕｅ　Ｃａｔ　様  
我开始读AH之后，觉得我和C.H.之间有一种隔阂，或者说是一种失落感，或者说是类似的东西，你把它写了出来，莫名地让我安心了。谢谢。  
在第1话中知道了香的“死”之后，虽然知道这是漫画的世界，但是10多年来深深烙印在心里的东西被取走，对我来说是非常难以忍受的。  
但是，随着故事情节的逐渐发展，渐渐地就能接受了。  

我想今后也一边关注着将来，一边温暖地收藏獠和香的回忆。  


[232] Goo Goo Baby　（表向きは学生）2001.07.06 FRI 17:23:16　  
ちょい～ンす！  
たびたびすいません  
  
そういえば  
香の誕生日って３月３１日でしたっけ？  
  
関東圏だけだったと思いますが  
今年はその日に雪が降りましたよね  
  
桜もチョ～満開だった！  
  
雪と桜を同時にナマで見たのは  
あの日が初めてなのよンあたくし♪  
  
理由も無く切なかったわぁ・・・  
  
夜になっても雪ふり続いてたから  
私の家の近くの雪夜桜はキレイだったワ～ん ♪(>v<)♪  

好久~不见!（译注：待校对）  
一再对不起  
说起来  
香的生日是3月31日来着?  
我想只是关东地区，今年那天下雪了。  
樱花也盛开了!  
那是我第一次亲眼看到雪和樱花  
没有理由的难过啊…  
因为到了晚上还在下雪，所以我家附近的雪夜樱很美哦♪(>v<)♪   


[231] Ｂｌｕｅ　Ｃａｔ　（　三河国　）2001.07.06 FRI 15:50:52　  
　　さっき、わたしにとってＣ・Ｈは想い出のアルバムのようなもの、なんて書いたんですけど、想い出にできるまでにはけっこう時間がかかったんですよね・・。連載が終わったばかりの頃は、なんか感情がマヒしちゃってたのか、哀しいとか寂しいとかそういうのが全然わいてこなくって・・・そのまましばらくたって。単行本が出てそれを何度も読んでるうちに、もう逢えないんだと思うと本当に寂しく切なくなってきて・・・海原との戦いの話とか、最後の話とか読むたびに本当に切なくって。・・・でも、いろんなとこでの北条さんの発言を読んだりするうちに、いつか絶対に再会できる日がくるはず、と信じて待てるようになってきたんです。まさかこんな再会になるとは思いませんでしたが^_^;　いつしか、連載していた期間より終わってからの方が長くなってきて、だんだんと自分の中で　“想い出”　としてとらえられるようになってきた気がします。それと、寂しさを癒すかのように入ったＳＰＲＩＴＺＥＲという私設のファンクラブが解散した、というのもきっかけになったのかもしれません。少しづつＣ・Ｈのことを考えなくても過ごせるようになってきたのはその後くらいだから・・。  
　　ここ１年くらいはマンガより小説を読むことが多くなってたんですが、このＨＰに来るようになったらなんかどうしても久しぶりにＣ・Ｈに逢いたくなって、読み始めて、やっぱり楽しいな、なんて思ってたら新連載がこれだったから・・・またしばらくは辛くて読めませんでしたよ。でも思い切って読んでみたら、あ、わたし大丈夫だ、なんて。そりゃあ、前と同じ気持ちで、ってわけにはいかないけど、でもギャグシーンとかではちゃんと笑えるし。今は想い出のアルバムを開くカンジで、Ｃ・Ｈと過ごしてます。  
　　なんか変な文章になってしまったけど、これで。  

刚才我写了，对我来说C·H就像回忆的相册一样，不过，能成为回忆花了很长时间呢··。连载刚结束的时候，好像感情麻痹了，悲伤啦寂寞啦完全不涌现出来···就那样过了一段时间。单行本出版后，我读了好几遍，一想到再也不能见面了，我就觉得很寂寞很难过……每次读到和海原战斗的故事或者最后的故事，我都很难过。···但是，读了北条先生在各种各样的地方的发言的时候，相信一定会有再会的那一天，开始等待了。真没想到会这样再会^_^;不知不觉间，结束后的时间比连载的时间长了，我觉得自己渐渐把它当成了“回忆”。还有，为了治愈寂寞而加入的名为SPRITZER的私人粉丝俱乐部解散了，这或许也是一个契机。在那之后，慢慢地即使不想C·H的事也能度过了……。  
最近1年看小说比看漫画多了，不过，来了这个HP之后无论如何都想见到久违的C·H，开始看了，果然还是很开心的，但后来我看到新的连载是这个...我有一段时间很难再读它。但是下定决心读了之后，啊，我没事。当然，我不可能抱着和以前一样的心情，但搞笑场面还是能让人笑出来的。现在就像翻开回忆的相册一样，和C·H一起度过。  
这篇文章写得很奇怪。  

[230] Goo Goo Baby　（表向きは学生）2001.07.06 FRI 15:34:48　  
なんか  
最近１週間たつのがも～早くて早くて・・・  
  
首を長くして待っていなくても  
来週号のバンチは勝手にやって来てる～ぐらいのイキオイ。  
  
それどころか  
気が付いたら火曜日終わってるし。  
  
きっと毎日忙しいせいですかねぇ  
（そのわりには書き込みしてるし）  
  
あ、生息地と性別の次は身分だね♪  
（ってシツコイってば）  
  
私、表向きは学生♪  
皆さんも表向きは主婦とか自営業とか会社員とかだけど  
ホントは何者でしょう  
  
悪女？　子悪魔？　死神？　人間に化けた宇宙人？  
もっこり男？　  
  
あ、でもリョウって死神っぽいイメージかも。  
  
だって  
本来はガードマンじゃなくて殺し屋だから、  
大切な人が自分のせいで死んじゃったりしてるじゃん？  
  
てゆうか  
最後にリョウが死んじゃう予感がするのは私だけ？  

总觉得最近一周过得好快好快…  
即使不翘首以盼，下周号的Bunch也会自动到来。  
不仅如此，等回过神来，星期二已经结束了。  
一定是每天都很忙的缘故吧(尽管如此还是发帖了)  
啊，所在地和性别之外，还有身份。  
我表面上是学生♪大家表面上是主妇、个体户、公司职员，但实际上是什么人呢?  
恶女?小恶魔?死神?伪装成人类的外星人?Mokkori男子?    
啊，不过，獠可能给人死神的印象。  
因为本来不是保镖而是杀手，重要的人会因为自己的关系而死，不是吗?  
我预感最后獠会死，只有我一个人这样吗?  


[229] りなんちょ（神奈川南西地方曇り）  2001.07.06 FRI 13:04:05　  
あたしのハンドルネームは、愛娘の名前からとりました。  
ちょっとアレンジして、「りなんちょ」になりました。  
リニューアル前から、使ってまぁ～す！  
  
「カミサン」発言について…  
何があったにせよ、香さんは冴羽さんの奥さんなんだぁ～！  
そして、周りのみんなもそれを認めてる…  
２人にとって、籍を入れることが結婚ではなく、  
お互いがお互いを、本当に必要な存在と感じた時、  
「夫婦」と言っていーんではないでしょーか？  
シティーハンターの設定だと、  
冴羽さんは戸籍がないわけですし、  
記念写真をとることで、けじめをつけたかったのでは？  
と、思うんです。  
冴羽さんの性格を考えると、  
結婚式をやるには無理がありそうですし…  
写真を撮るのだって、嫌がってたんですよ？  
結婚式なんて、もってのほかですな…  

我的网名取自爱女的名字。  
稍微改编了一下，变成了「りなんちょ」。  
从更新前就开始使用了~ !  

关于「カミサン」的发言…  
不管发生什么事，香小姐都是冴羽先生的妻子啊~ !   
而且，周围的大家也都认同这个…  
对于两个人来说，入籍并不是婚姻，而是彼此都觉得彼此是真正的必要存在的时候，才称得上是「夫妻」吧?  
如果设定为城市猎人的话，冴羽先生是没有户籍的，拍纪念照是为了划清界限吧?  
我想。  
考虑到冴羽先生的性格，举办婚礼好像有点勉强…  
连拍照都不愿意呢。  
婚礼什么的，真是荒谬啊…  


[228] maica(東京のはずれ)  2001.07.06 FRI 12:50:45　  
こんにちわ。  
以前の掲示板の時からROMしてたんですが、我慢できなくなって書き込んじゃいます。  
  
＞姫さん  
あっほんとだ。多分そうです。全然気がつかなかった!  
ってことは今後「出てくるとき」はいつもあの服装になるのかしらん?  
  
それにしても今週最後のシーンのGHを見おろすリョウの厳しい目。  
この後どうなるんでしょ。気になるー。  
ああ、早く来週にならないかな、、、。  

你好。  
从以前的公告栏的时候就开始写ROM了，不过，忍无可忍写了。  

＞姫さん  
啊，真的。大概是这样。完全没有注意到!  
也就是说，今后“出现的时候”总是穿那件衣服吗?  

话说回来，这周最后一幕的GH低头看獠的严厉的眼睛。  
之后会怎么样呢?好在意啊。  
啊，下周还不快点吗、、、。  


[227] 姫 (海軍カレーの街）2001.07.06 FRI 12:19:13　  
ちわ！！  
  
ちょっと気になったんですが、今週香が新宿の駅に出てきたじゃないですか。  
その時の服って死んだときの服ですよね。  
違うかな？  

你好! ！  
我有点好奇，这周香不是在新宿站出现过吗?  
那时候的衣服就是死时的衣服吧。  
不是吗？  


[226] 瑞樹（さいたま）2001.07.06 FRI 12:18:04　  
ログ、はやっ！！  
過去ログがもー少し見やすくなるといいなー……（贅沢）  
  
皆様、生息地はもうかかれないのかと思ってました。慌てて復活です（笑）  
  
ところでＡＨの方ですが、ラブ系の展開は遅かれ早かれ避けて通れないと思うのですが、ＧＨって思いっきりリョウの守備範囲外では……？＜年齢  
今後どうなっていくのか、かなり楽しみです。  
ではまた。  

日志，流行! !  
如果过去的日志能变得更容易看就好了……(奢侈)  

大家，我还以为已经没有栖息地了呢。马上就复活了。(笑)  

顺便说一下AH，我觉得love系的展开迟早是无法避免的，但是GH完全不在獠的狩猎范围之内……?<年龄  
今后会如何发展，相当令人期待。  
再见。  


[225] Ｂｌｕｅ　Ｃａｔ　（　三河国　）2001.07.06 FRI 12:05:00　  
　　わたしはＡ・Ｈでの回想シーンをなんとなく、Ｃ・Ｈを読むのと似たような感覚で見ている気がします。（連載２年目くらいから）ほぼリアルタイムでＣ・Ｈと一緒に過ごしてきたんで、Ｃ・Ｈを読み返すというのはわたしにとって、想い出のアルバムをめくるのと同じような感覚なんですよね。だから、Ａ・Ｈの回想も・・ああこんなことがあったんだ、なんて、想い出として楽しんでいる部分もあるような・・・。ちょっと会わないうちにいろんなことがあったんだねぇ、なんて話し掛けたくなったりして。  
  
　　ところで、わたしのハンドルネームなんですが。休止する前のここのＢＢＳでは、　Ｃ　、という名前を使ってた（わたしの本名のイニシャルがＣなのと、Ｃ・ＨとＣ・Ｅにも入ってるし、ってことで）んですが、あまりにも味気なかったのと同じような名前を見つけてしまったことで、休止を機に変えることにしたんです。とりあえず猫にちなんだものにしたくて、カクテルの本とか調べたんですがめぼしいものが見つからず・・・自分なりに考えて、結局語呂とかでこれにしました。で、決めてからなんか聞いたことある響きだなぁと思っていろいろ記憶を辿ったら・・・Ｃ・Ｈに出てきた佐藤由美子さんの初主演映画のタイトルでした^_^;　気に入っちゃったんで、そのまま使ってますけどね。他のＨＰのＢＢＳでもこれで書き込んでますよ。  

我总觉得自己在看A·H时的回忆场景时，感觉和看C·H时差不多。(从连载第2年左右开始)几乎每时每刻都和C·H在一起，重读C·H对我来说，就像翻看回忆的相册一样。所以当我回顾A.H.的回忆时......我想'哦，这发生过'，我把它当作记忆来享受……。我很想跟他说，没见面就发生了很多事啊。  

对了，这是我的网名。在这里的BBS暂停之前，我用的是C这个名字(因为我的本名的首字母是C, C·H和C·E里也有)，但是我发现一个和我一样的名字，实在是太乏味了。因为等太久了，我决定以暂停为契机。总之想要跟猫有关的东西，查了鸡尾酒的书也没找到像样的东西···根据自己的想法，最后还是用谐音什么的定了这个。然后，决定了之后觉得好像听过的声音啊，追溯了很多记忆……原来是C·H中出现的佐藤由美子主演的第一部电影的名字^_^;我很喜欢，就一直用着。其它HP的BBS也用这个写哟。  

[224] sweeper(栃木のこんな所)  2001.07.06 FRI 11:39:34　  
　Web新潮での北条先生のインタビュー読みました。  
そういわれれば、北条先生はＡＨを楽しんで描いているようにみえます。  
　テレビで北条先生が出ていたというカキコがあったけれど、「週間朝日」（たぶん)でバンチの記事があったのをみました。  
「ＡＨにはＣＨの冴羽りょうが出ている。」という一文がきになりました。  
  
　＞Ｇｏｏ　Ｇｏｏ　Ｂａｂｙ様  
　ハンドルネームのことですが、私の場合は、すごく単純です。  
　ＣＨが好きということもあったので、ＣＨ＝りょうと香、りょ　うと香＝スイーパーということでスイーパーを英語読みにして　sweeperになりました。(ちなみにリニューアル前から使ってい　ます。)  
　  
　でも、２人がタキシードとウェディングドレスを着て並んだ　ころ見てみたかったです。ビシッと決めれば何を着ても似合う２人なんですから!!  
  
　どなたかのカキコで登場人物の服装にも目が離せないってありましたよね。そういえばグラスハートもヒップハングジーンズはいてたし、２話のりょうの服装もラフだったし。今回はカーゴパンツはいてましたよね。そのときどきの流行を取り入れる細かさに脱帽です。  
  
　＞ＳＰＲＩＮＧ様、夏兎様  
　私もです。  
　そこらへんがすごくきになってしまいます。  

读了北条老师在Web新潮上的采访。  
这么说的话，北条老师好像很享受画AH。  
电视上有北条老师上过的书，但我看到《朝日周刊》(可能)上有关于Bunch的报道。  
“AH有CH的冴羽獠。”这句话让我很在意。  

　＞Goo Goo Baby様  
关于网名，我的情况非常简单。  
也因为喜欢CH, CH=獠和香，獠和香=スイーパー，所以スイーパー的英文读音就变成了sweeper。(顺便说一下，更新前就开始用了。)  
但是，我很想看看两个人穿着无尾晚礼服和婚纱并肩站在一起的时候。严格地说的话是2人穿什么都适合!!  
在某本书中，登场人物的服装也让人目不转睛。这么说来，玻璃心也穿了背带牛仔裤，第2集的服装也很随意。这次穿了工装裤吧。对那个时候的流行的细微之处脱帽致意。  

　＞SPRING様、夏兎様  
我也是。  
我非常在意这一点。  

[223] 前川  2001.07.06 FRI 11:27:56　  
二人が一線をこえて、幸せそうな回想シーンの風景は私はとても嬉しいですね。  
ＣＨの最終話からどのような過程を得て、二人が結ばれていくのかはおいおい北条先生が書いて下さるかもしれませんが、とにかく二人が結ばれて幸せを掴んだのは私にとっては本当に嬉しい。  
そう考えると香サンの悲劇がとても悲しいものにもなるのですが・・・。  
ＡＨの一話を読んだ時は衝撃的でしたねえ・・・やっぱり。

两个人跨越一线，幸福的回忆场景的风景让我非常高兴。  
从CH的最终话中得到怎样的过程，两人结合在一起，北条老师也许会慢慢写出来，但不管怎样，两人结合在一起，抓住了幸福，对我来说真的很高兴。  
这样想的话香的悲剧就会变得非常悲伤···。  
读了AH的一集的时候真的很震撼啊···果然。  

[222] ネット  2001.07.06 FRI 11:27:09　  
＞夏兎さん  
「かみさん」発言のことについて。  
彼らにとってはウェディング姿で写真をとるということが  
結婚式のような儀式だったのではないでしょうか。  
実現はしなかったけれど・・・（号泣）  
１話の「かみさん」発言で、リョウの中では香は妻なんだ、と  
思い、その言葉にこめられたリョウの深い愛情と悲しみに涙しました。  
  
ＣＨを読んだのは小学生の頃でしたが、  
コミックス３５巻もかけてキスさえさせない北条先生に脱帽でした。  
ある意味すごい（笑）  
子供心にも、二人の精神的な大人の繋がりに憧れを抱いたのを  
覚えています。  

＞夏兔小姐  
关于「かみさん」的发言。
对他们来说，穿着婚纱拍照不就是像婚礼一样的仪式吗?  
虽然没有实现……(大哭)  
在第1集的「かみさん」发言中，我想到在獠心中香是他的妻子，被这句话中透露的獠深深的爱情和悲伤感动得热泪盈眶。

读CH是在小学的时候，对漫画35卷都不让接吻的北条老师脱帽。  
在某种程度上，这很厉害(笑)  
我还记得，在我幼小的心灵中，憧憬两个人精神上的成年人的联系。  


[221] Goo Goo Baby (性別：夜行性)  2001.07.06 FRI 10:21:21　  
なんか  
掲示板というより  
チャットと化してる気が・・・  
でも皆さんのカキコ面白いです  
  
＞夏兎さん  
  
　確か  
　香はウェディングドレス着る前に  
　死んだ・・・  
　そう、結婚直前で。  
　バンチの２号か何かでその回想シーンありました  
  
　そういえば  
　あなたの言うとおり、  
　何があって２人の距離があそこまで縮まったかを  
　私なりに考えたところ、  
　  
　先生も言っていたとおり、  
　ＣＨとＡＨは設定が違うからだと  
　私は思います  
  
　何が違うって  
　ＡＨでの設定は、結婚前に２人は一線をこえてた  
　という設定なのか  
  
　それどころか  
　もしかしたら香のお腹にはリョウの子がいるんだけど  
　彼に知らせる前に死んだっていう設定かもしれないし。  
　  
　そう考えると香の死は余計泣けてくる  
  
　あ、これは私が勝手に想像した勝手な設定なので  
　気にしないで下さい（逃）  
  
話は変わって、  
どなたかがイラスト投稿コーナーを作って下さいと  
言っていましたね  
私もそれ言おうと思ってました♪  
面白そうですよね  
  
でもまぁそうゆう重要なことは  
お問い合わせの方に直接メールした方がいいのでは？  
  
だって  
こんな膨大な量の書き込みの中に書いたら  
管理人さんやスタッフの方は読み逃してしまうかもなので・・・  
（じゃ私がメール送ればいいぢゃんっ）  
  
てゆうか  
もしイラストコーナーを設置したとしたら  
投稿数スゴそ～  
  
だって  
たった４日しか経過してないのに  
掲示板の回転がこんなに早いんだもん（汗）  

与其说是论坛，不如说是聊天…
但这很有趣。

＞夏兔小姐  
香在穿婚纱之前死了……  
对，就在结婚前夕。  
是在Bunch第2期还是其他什么地方。  
这么说来，就像你说的那样，发生了什么事情，两个人的距离缩短到了那个程度，我想老师也说了，是因为CH和AH的设定不同。  
有什么不同?  
AH的设定，是结婚前两人就已经超越了界限的设定吗  
不仅如此，说不定香的肚子里有亮的孩子，但在告诉他之前就死了。这么一想，香的死就更让我哭了，这是我自己擅自想象出来的设定，请不要在意(逃)  

换个话题，有人说要建立一个插画投稿区，我也想这么说呢♪听起来很有趣。  
不过，这么重要的事情还是直接给咨询的人发邮件比较好吧?   
因为在这么多的留言中，  
管理员和工作人员可能会看漏……  
(那我发邮件就行了。)

如果你设置了一个插画区  
投稿数好厉害哦~  

因为仅仅过了4天，掲示板刷新得这么快。(汗)