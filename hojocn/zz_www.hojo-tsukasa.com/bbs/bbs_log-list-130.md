https://web.archive.org/web/20011006053643/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=130

### [2600] おっさん（岐阜県：お～い！！）	2001.08.29 WED 22:56:39　
	
たびたび、失礼します。
もっこりメッセージ読んだんですけど、正直言って笑いましたね。日本の台風って番号じゃないですか、アメリカみたいに名前つければいいのにって私も思いました。付けるとしたら、日本にしかない名前ですね。しかも英語とかに訳し難いもの。（どんなんや！！）
あとは、もっこりメッセージを見ましょう。
おっさんでした。（ちなみに私も女です。お間違いなく！！）

### [2599] おっさん（岐阜県：おっは～！！）	2001.08.29 WED 22:45:26　
	
毎度、昨日はスーパー銭湯でマッサージをしてもらったら肩が（左）ゴリゴリになってたおっさんです。皆さんせ～のおっは～！！
今週号の感想。李大人の双子の弟ですか、待従長でさえ双子の弟が分からなかった。りょうは傷口で弟が分かるんですからね。いくら年取っても。改めて思ったんですけど（ベリー様と重なりますが）りょうってやっぱり年齢不詳だと思いましたね。ほんまに。それと、李大人の実の娘！？のＧＨ。李大人が言ってた「私の・・・不幸な娘をいかすために・・・！」何か、意味ありげですね。また来週が楽しみです。
さえむら様、あほな事をこの間かいちゃいました。すんません＾～＾；。
今週末(９月２日）、２６歳の誕生日を迎えてしまうおっさんでした。


### [2598] ちゃこ	2001.08.29 WED 22:24:16　
	
２度目の書き込みです。
主にレスです。

＞あずみちゃん♪様「２５８７」
教えて下さって有り難うございます（＾＾）
次回、レンタルしてきて聴いてみますね。
本日のＢＧＭは「Ｔｒｅａｓｕｒｅ」でした。
「いつかのメリークリスマス」と「ＴＩＭＥ」に、
しんみりしつつ運転してました。

「恋愛のこと」
何かＣＨ＆ＡＨを読むようになってから、
私もよく考えるようになりました。
「人を好きになるって、どういうものだっけ？」みたいな。

この２人の凄さは、どんなにじゃれあっていても、根底に
「あなたが生きて、ここにいてくれるだけで嬉しい」という
シンプルな、でも強い想いがあって、それが時を重ねても
色褪せないところだと思うのです。
３年付き合って、結婚して７年。
同居人と一緒に歩き出して、丁度１０年になるけど、
要求し合うばかりの関係になってしまってるかなぁ・・・と
反省してる今日この頃です。

「盆踊りネタ」
「リョウちゃん音頭」に「海ちゃん音頭」ですか。
面白そうですね。（＞葵様、sweeper様）
「海ちゃん音頭」は、やっぱりあの、筋肉ムキムキした感じを
表現して、猫でオチを付けないと（！？）
でもって、「リョウちゃん音頭」は、やっぱりあの
「もっこり♪」をどう振り付けるかですね（笑）
やっぱり「ナ＠パオ」のＣＭの、ヒロミＧＯ！みたいに
踊るしかない？！・・・失礼しました（＞＿＜）

### [2597] 葵（一週間は長い…）	2001.08.29 WED 22:03:50　
	
☆今週号の感想
　李大人、いい弟さん持ってますねぇ。兄貴とは大違いっ☆でも、あの弟さんの謝り方からすると、ＧＨに移植した心臓の持ち主が香だった事、知ってたって感じだなぁ。やだなぁ…香の心臓欲しさに事故が仕組まれてたとかなんてのは。（それにしたってリョウ、妙に冷静でない？）
　でも、若かりしリョウ、カッコいかったね♪（ほれぼれ♪）香を亡くしてからの失意の日々から、徐々にもとのリョウへと戻っていくのがうれしい♪それもこれも、心臓（香）に会いたいが為…なんだよね。来週号、まぁだぁ？

　☆盆踊りネタ
　［２５６７］sweeperさまの「海ちゃん音頭」、ナイスです♪「冴子さん音頭」では、要所要所に「ウッフ～ン♪」なんて入るのかなぁ…？！リョウの「もっこり音頭」といい、放送コードに引っかかりそうですねぇ。（笑）

### [2596] ワタル／Wataru（宮城県）	2001.08.29 WED 21:59:30　
	
【りょうの年齢】
１６号のりょうは，私は１３歳くらいと思ったのですが…？

【The age of Ryo】
I think Ryo in the 16th issue is about 13 years old.
What do you think?

### [2595] さえむら	2001.08.29 WED 21:42:30　
	
解禁♪

[今号について]
やっぱり、リョウって四十才近いんだ..。
死んだ李さん（弟？）と再会した事で、リョウは『やるべき事』を見つけたから、スイーパーCity Hunterの冴羽リョウが復活！みたいなカオをしてた訳か..。
それまで、本当に痛々しくて、見ていられなかったものなぁ...。
生きている李さん（兄？）は、グラスハートを「私達の不幸な娘」と言ってるけど、これは、やっぱり幹部の精子を使って作った子ども、というイミなのか？
それとも、中国語的比喩なんだろうか？組織の部下は全て子ども、というイミの..？
あと、船での移植だったんだー。台湾とかに空輸しても間に合うだろうと思ってたんだけど。
香ちゃんの心臓が移植されるまで、グラスハート、人工心臓でもつけていたんだろうか？（日数的にタイムラグがあるし）即死出来なかった事が、再び生きなければならなくなった、原因なわけかぁ。

とりあえず、Ａ・Ｈを楽しめるようになってきたかも。

[ふとした疑問]
リョウって、何か国語話せるんだろう？
日本語、英語、中国語。あとＣ・Ｈと設定が一緒で南米のジャングルで育ったなら、スペイン語も話せるのかな。
生き抜く為に必要とはいえ、軽く五、六か国語話せるのかも。

[朝もっこ＠について]
それとは関係ないんですが、男性は、子どもの頃から、できるそうです。朝とかではなくて、日常で。で、その為に、男性用ブ＠ーフの腿の付け根の所はゆるゆるらしいです。（怪しい話ですみません。ハンマーくらうかも。）

[レス]
＞おっさん様
有給休暇だったんですけどね。

長々、失礼いたしました。
（ちなみに、私は女性です....）

### [2594] ありあり（さっ寒い）	2001.08.29 WED 21:40:01　
	
こんばんは～。
【今週号の感想♪】
うわっ、本当に双子だったのね。いつだったか「まさか双子じゃないよね。」って書いたような気が・・・。りょうはかっこ良くてほれぼれしたけど、李大人は分からなくなってきた。
りょうに心臓の事を謝っているけど、じゃあ６話でのあの高笑いは一体何？！双子の上に二重人格じゃあないよね。心配。まだ腹に一物ある感じだね。
【sweeper様】
御賛同ありがとうございます。歌も歌詞もいいけど、やっぱり声がさいこ～♪ですよね。初めての方、TMN三色シリーズをぜひどうぞ。（って宣伝してどーする）
【久々もっこり研究会の皆様】
面白過ぎです♪これからも熱い議論楽しみにしてます（＾o
＾）
北海道はすでに秋風。寒いくらいです。本州以南の皆様、残暑見舞い申し上げます。ではまた。

### [2593] ベリー	2001.08.29 WED 21:21:27　
	
こんばんわ。解禁ですな。

まさか、リョウと李兄弟が22年前に接点があったなんて思いもしませんでした。しかし、GHは本当に実の娘なのかしら？まだまだ、謎は多い･･。私のからっぽの頭じゃ何も考えられないです。GHの手術が船で行われていたというのも驚き。そりゃ、冴子さんら警察も分からないかな？
リョウはしっかり、動いていましたね。私もスイーパーとしての、リョウの顔が垣間見られたなって思いました。どんどん、こういう顔が増えるのかな？香に向ける優しくて穏やかなリョウの顔も素敵だけど。

今週はGHはもちろんのこと、香も出てこなくて残念です。
しかし、22年前のリョウっていくつなんだろう･･?本当に、年齢不詳すぎるぞ、リョウ･･。

### [2592] もっぴー	2001.08.29 WED 20:56:14　
	
こんばんわ（＾＾）v

【今週の感想】
リョウちゃん可愛いー！かっけー！ラブ（x＾-＾x)
でも、GHの心臓が香ちゃんの心臓と分かった時あんなに、
怒ってたのに、今日の落ち着いた状態は何なの？
なんか今週のを見てたらどんどん香チンの存在が薄くなっているようで、悲しかった。です。
毎週出てくるのを期待する私が悪いか（－－；）

【レス】
＞sweeper様
レスどうも！
私、『WANDS命！』って訳じゃ無いんですが、
なんか、リョウちゃんにあってるの多いですよね。
（お若い方、WANDS知らない方もいるでしょう。ごめんちゃい。）

＞chikka様
先生ってお忙しいでしょう？大変なのに色々すごいですよね。
勉強熱心ですよね。すばらしい。
やっぱり、先生って感じですね。

他の方も、出張ある方とか、仕事忙しい方とか、主婦の方とか
大変ですよね！皆さん頑張ってください！（＾＾）/

【最近の１５歳】
最近の１５歳の方って大人ですよね。発育もいいし。
グラスハートは大人っぽいけど、
今の時代、結構それが普通なのかな？とも思った・・。
でも、普通の人より断然大人だけど。



### [2591] ちゃこ	2001.08.29 WED 20:30:10　
	
さて、解禁です。
とりあえず、自分の感想を書き込みます。

何と「李兄妹」が過去にリョウと関わりがあった人だったとは。
何となく予想していたものの、こんな風な再会をさせるなんて
随分、皮肉な運命・・・

来週、具体的な話が判るのですね。
きっと、また頭が白くなる展開なんだろうな・・・
１つ願っているのは、香の「事故」が仕組まれたもので
ありませんように！ということ。
ＧＨの為の心臓欲しさに、何カ所かで事故を起こし、たまたま
強奪可能な心臓が、香のものだったなんて場合、
悲しさを通り越して、怒ってしまうかもしれない。

なんでそんな風に思ったかというと、「李兄妹」が
あんなに謝るからです。
ただ心臓を「強奪」したくらいでは（その時はまだ、
恩あるリョウのパートナーの心臓とは知らないわけだし）
あんな風に「李弟」が涙を流さないのではないかと。

いずれにしても、リョウの心はどうなってしまうんだろう。
ただでさえ傷ついているのに。
先週とはどちらというと、感動！の衝撃だったんだけど、
今週はその後の内容を考えて、ブルーになってしまいました。
（今週号自体は、面白かったんですけどね）

では、失礼します。

### [2590] よしき（週一参加…？）	2001.08.29 WED 20:13:34　
	
今週号の僚、推定２０歳前後ですか？！ＦＱの雅彦くらいに見えました。それにしてもかわいーー！！（腐ってる）
李大人の弟、爪はがされてた…痛…。

そして悪い人かと思ってた李大人がなんかいい人っぽいですね…。
でもＧＨを娘と呼ぶには歳が離れて過ぎているような？
たまたま移植された心臓が香の心臓だったってことなんでしょうけど、理由が気になります。
ってゆ～か、モッチー！ちゃっかり同席してるよ。（笑）

### [2589] Ｂｌｕｅ　Ｃａｔ	2001.08.29 WED 19:45:32　
	
　　今回はリョウが全編出ずっぱりで幸せ♪でも、昔のリョウがあまりにもあっけなく殺しをしてるのを見て、何故Ｃ・Ｈがあんなに恐れられているのかが少しわかった気がしました。今のリョウなら気を失わせる程度にするんだろうな、って思うから・・・ちょっと恐かった。でも、見た目はなんか可愛い♪
　　リョウと李兄弟にあんな過去があったなんて、ちょっと驚きでした。男の依頼を受けたってことは、よっぽど心震わす何かがあったってことですよね、う～ん・・・。
　　２５８６ネットさん、わたしも李大人の言い分はエゴって気もします。どういう理由でグラスハートを朱雀に入れたのかまだわかんないけどさぁ、そこまでするか？心臓移植ってグラスハートをまだまだ暗殺者として働かせるためじゃなかったのか？なんとなくリョウの表情が怒りを押し殺してるようにも見えたんだけど・・・・。リョウは、どうするんだろ。

### [2588] sweeper（解禁～♪）	2001.08.29 WED 19:41:48　
	
こんばんは。やっと解禁ですね！
最新号、読みました。

りょうの過去。２２年も前から李大人知ってたんですね。
モールス信号もできるというのにはびっくりしました。
ネット様［２５８６］で書いているようにりょうの顔、ＣＨの頃に近くなってましたね。同時に「スイーパー」としてのりょうの顔が戻ってきたので、「本領発揮！」という感じです。
むむう。目が離せなくなってきました。でも、今回の回想シーン。りょうと李大人の過去だったのでめずらしかったです。そしてまた１つりょうの過去が明かされましたね。今まではりょうと香が多かったので。でも、「李大人の娘＝グラスハート」説。何気に考えてしまいます。そうだとすると、グラスハートの心臓手術跡もしっくり来るので。でも、心臓のことでりょうに李大人が謝っているところを見ると、「そんなに悪い人ではないのでは？」と思いました。ということは、「香の心臓」ということを知っているってことですよね。（うーん。ますます謎が謎を呼ぶなぁ。）

＞キャサリン様。[２５８４]
そういえば、ありました。グラスハートのあの撃ち方。
りょうも黒蜥蜴の話で同じ撃ち方してましたね。でも、アングルは違ってましたが。かっこいいですよね。
私は、りょうのガンアクションはほとんどが好きですが中でも、葉月さんの話でりょうがミックのダーツをパイソンのグリップでよけていたやつと、海原の話で海原とのサシでの決闘の時に命運がわかれたときの撃ち方も好きですね。

＞chikka静岡県支部長様。[２５８２](笑)
もっこり。奥が深いですね。創始者のくせに勉強不足でした。(滝汗。)私は、女なのでよくわからないのですが、精神的な悩みも影響したりするんですね。あと、毒蜂の話での教授のこと。６０までもったのはすごいです。しかも復活させようとしてましたしね。「６０で引退」はまだ若い方に入るんですか！そうしたらりょうはいつまでもつんでしょうか。死ぬまでもっこりしてそうな気がしますが、法水名誉会長様、chikka静岡県支部長様、まみころもっこり研究会特派員様、他もっこり研究会の皆様。どう思いますか。(・・・撃沈。)

[もっこりネタ]
優希さんの話での「生もっこり」もやばかったですが、「隆々泡盛」も「生もっこり」と同じ位きわどかったです。
あと毒蜂の話で出て来た喜多川父子のダブルもっこり。りょうのより小さいような気がしました。(・・・撃沈。本日２度目。)

長々失礼しました。
それではまた来ます。(えっ！また来るのかこいつはッ！！)

### [2587] あずみちゃん♪	2001.08.29 WED 19:34:50　
	
[今週のバンチの解禁！]
やっぱりそうだったんだぁ...の一言。でも、どんなに悪い奴と思っていても最後はいつも優しい顔をして亡くなる姿を見るとどうしても心が揺れる。それなりに事情があるのかもしれないと思うのは私だけでしょうか？そこにたまたまその心臓があったのではないのか...偶然か？必然か？それによっては今後の展開が変わるんでしょうね...。

[レス]
＞まみころりんちゃん♪[2568]
基本的にCHの時はTM NETWORKだったよね♪TMNになってからはどちらかと言うとスピードのある曲が増えたって気がするけど...。1日だけ今までのベストツアーやってくれたら絶対行くんだけどな。UTUにも会いたいです。

＞ニーナさま♪[2573]
こちらこそ、初めまして。私の「ふと思った事」にレス頂いてとても嬉しかったです。ニーナさまの恋愛もある種、いろいろな恋愛の1つなんですよね。素敵な恋愛だといいですね。リョウと香ちゃんのような恋愛は違う意味で大変かも...笑。あれは香ちゃんだから出来るようなものだと思ってますから...。リョウのような彼だったら心配で仕方が無いものね�

＞ちゃこさま♪[2579]
おすすめですか？[LOOSE]と[RUN]は結構いい感じですよ。[RUN]もまた2人の関係を歌った感じの曲が多いです。最後の曲はすごく家庭的な曲でしたから...。良かったら聞いてみて下さいね。

＞キャサリンちゃん♪[2583]
そうだったんですか～？すごいですね！気付かなかったです。早速「こもれ陽～」をチェックします。（恥）

### [2586] ネット	2001.08.29 WED 18:49:13　
	
ええと、１８時過ぎたからネタバレしていいんですよね？
では、まず一言。リョウの顔がＣＨの頃に戻ってる～！嬉しい！
しかしリョウと李大人が知り合いだったことで心臓問題が結構あっさりと片付いてしまったような・・・？もはや私のごとき凡人には１週先も予想がつきません。来週は不幸なＧＨの生い立ち話か？う～ん、北条先生の頭の中を一度拝見させていただきたいです（笑）

しかし李大人はずいぶん勝手な言い分ですね。不幸な娘を助けるためだったら人の心臓盗んでもいいんかい。人のものを盗るってことは人の思い出も盗るってことなんだぞ・・・。今、リョウがどんな想いを抱えているのか。怒りか、諦めか、悲しみか・・・早く来週のＡＨを読みたいです。

### [2585] あお	2001.08.29 WED 14:58:38　
	
…しばらく来なかったら過去ログがすごいことに…。
おひさしぶりです。あおです。

最近ようやく、｢なぜCHの2人なのか？｣が分かった気がします。
この話に必要なのは、自ら死を選んだ何も持たない少女、移植された心臓、その心臓の持ち主の、深く優しい人柄と思い出、彼女と強い絆を持つ男性、ですかね。絆、の存在がAHの話の土台となっているのなら、CHの2人が1番分かりやすいかなと。他のどの話を読んでも、この2人ほど強い絆で結ばれている人はいない。新しくキャラを作り出すと、まずその説明から入らなくては説得力がなくなる。CHの2人が最適だということなのでしょう。
だからといってココロは別物。たとえパラレルと分かって(？)いても、どうしても納得できないのが人の心ですわ。別物なら別物として、CHのあの2人をまたどこかで書いて欲しい･･･。あう。

イメソン＞
私はあまり最近の音楽は聞かない方なので･･･かなりマイナーな2曲を。
谷山浩子さんてご存知ですか？？
あのかたの「しまうま」というアルバムのなかに「ねこ曜日｣という曲があるのですが、これが冴羽氏と香さんにぴったり。
｢あなたの膝は暖かくてほかには何もいらない｣
｢にぎやかな表通りよりも、2人で過ごすこの部屋が1番｣
という趣旨の歌詞もいいんですが、
｢この広い世界の中に、たった1つだけ探し当てた。大切で、安らげる、本当の自分に帰れるところ｣
というサビが･･･聞くたびに泣けます･･･。
もう1つも同じ谷山浩子さん。この曲は西田ひかるさんに差し上げた曲なんですが･･･タイトルが出てこない･･･。
｢たとえどんなに離れても、私の心はいつもあなたの傍にいてあなたを抱きしめている｣という内容の歌詞です。AHの2人にぴったり･･･ああ、香さんかむばああっく！！

解禁が今から待ち遠しい。だって突っ込みたいんだもの･･･。

### [2584] キャサリン（度々すいません）	2001.08.29 WED 06:54:07　
	
さっきも書いたのに度々すいませんm(__)m
今日バンチ発売ということで前回を読み返してたんですが
G・Hが組織の人を撃った時の銃の持ち方は
「黒蜥蜴」と戦った時にりょうもやってませんでした？
私はあの撃ちかた大好きです！だってホントに強い人じゃないと
出来ないじゃないですか！！
あっミーハーモードに入ってしまった・・・

我很抱歉早些时候写了这个，但我很抱歉一直在重复它。
我重读了上一期，因为《Bunch》今天开卖。 G.H.射杀歹徒时握枪的方式...獠在和黑蜥蜴战斗的时候不也是这样吗？
我喜欢这样的射击方式! 你必须要有真正的实力。 除非你真的很强壮，否则你是做不到的!
我现在是米哈模式...

### [2583] キャサリン	2001.08.29 WED 06:46:30　
	
おはようございます
そうそう「こもれ陽～」の漫画見て思ったんですが、
3巻の沙羅ちゃん達のクラス分けの名簿に「浅谷光子」
「槙村かほり」「上谷あきら」「方城司」という4人の
名前があったんですけどこれも先生のイタズラですかねぇ？
あと、沙羅ちゃんの四年の時のクラスの女の子に「野上冴子」
という子もいました（笑）
もしどなたか書かれていたらすいませんm(__)m

早上好  
对了对了看了「艳阳少女」的漫画，
第3卷沙罗、达也们的分班名单中有“浅谷光子”、
“槙村香堀”、“上谷亮”、“方城司”这4个人的名字，这也是老师的恶作剧吗?
还有，沙罗四年级时班上有个女生叫“野上冴子”(笑)
如果是谁写的，对不起m(__)m


### [2582] chikka(Eastern Shizuoka)	2001.08.29 WED 02:42:01　
	
〔２５７６〕法水名誉会長兼愛知県支部長様
そうなんですよ。３０代に入ると１０代のようにはいきませんね。運動する時はきちんと準備体操をして、終わったら整理体操をしないと疲れは残るわ、筋肉痛になるわで、もう若い時みたいに無理はできませんね。え・・・違うの。「朝もっこ」のことなの・・・。そうですね・・・。何とも言えませんが、肉体的な事よりも精神的に悩みがあったりすると影響はするとは思いますが・・・。「Ａｎｇｅｌ Ｈｅａｒｔ」では「ＥＤ」の話が出てきましたし、「精神的」に追いつめられると身体のあちらこちらに影響が出てくるのは確かですね。
そう言えば名取かずえさんの時の話で教授が冴羽さんに「その若さで打ち止めになるとは情けない。わしは６０までもったぞ。薬に頼ることを恥と思えよ」と言っていましたが、あの話をジャンプや単行本で読んだ高校生の時は「すごいなあ。」と感心しましたが、今では「６０で終わってしまったのか。教授。引退するにはあまりにも若すぎましたね。」と思うようになりました。だって「ファンファン大佐」こと俳優の岡田真澄は６５歳で子供を作ったし、今は亡き上原謙（加山雄三のお父さん）や三船敏郎も６０過ぎで子供を作りました。（ちなみに三船敏郎のその時の子供は大ヒット曲「ロード」の「ＴＨＥ 虎舞竜」のリーダーの高橋ジョージの奥さんで女優の三船美佳です。結婚した時の年齢は高橋ジョージ４２歳で三船美佳１６歳です。犯罪ですね・・・。）ですから教授はあまりにも若く「現役引退」をしてしまったのかと思っています。せめてあの時「バイアグラ」が出ていれば、まだまだ「現役」で活躍できたのに・・・。（もっとも「早死に」したかもしれませんが・・・。でも、好きなことをして「早死に」すればある意味、男としては「本望」かもしれませんが・・・。）私が出会った人で７２歳で子供を作った男性を知っています。最初に見た時は「おじいちゃん」と「孫」かと思いました。すごい・・・・。（しかし、いいのだろうか。こんなネタ書いて。学校の先生なのに・・・。生徒が見たら、なんて言われるか。）←結構小心者です。

### [2581] ちゃこ	2001.08.28 TUE 23:43:10　
	
訂正。
「２５７９」で思いっきり「ソニア」とか書いてますが、
「セイラ」の間違いです！！
ごめんなさい～（Ｔ＿Ｔ） 
