https://web.archive.org/web/20040117125443/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=403

### [8060] マイコ	2002.09.30 MON 19:38:30　
	
こんばんは！皆さん昨日の「ベスト100」のやつ見ましたか？「シティーハンター」出るの楽しみにしていたのですが、でなかったので残念です。ほかに私の好きな「うる星やつら」「北斗の拳」など入っていたけど、紹介なしだったのでイライラしてました。でも8位に「めぞん一刻」が入っていたのでとっってもうれしかったです！！

★どなたか高橋留美子先生（うる星やつら、めぞん一刻、らんま１/２、犬夜叉の作者）が好きな方、いませんか？

### [8059] ぽぽ（Hyogo)	2002.09.30 MON 18:44:44　
	
昨日､｢RAIJIN｣が届きました｡
英語で読むのも興味はあったけれど､購入した一番の理由｡“ジャンプ(雑誌)”サイズでＣＨが読めること！(動機が不純だ･･･)｡え､だってＣＥは愛蔵版も出てるけど､ＣＨは大きくてもバンチコミックスサイズまでだったし･･･｡雑誌サイズでまたＣＨを読めるなんて､しゃ～わせ♪北条先生のきれいな絵をコミックサイズだけにとどめておくのはもったいない！
ええ､これを良いきっかけに英語もしっかり勉強しますよん(^o^)

### [8058] Ａｙｕ	2002.09.30 MON 18:42:43　
	
ＢｌｕｅＣａｔ様
初レスありがとうございます。遼が受け持つ部活は（女子）バレー部、サッカー部、バスケ部とかはどうでしょう？？冴子は文化系ではなく、ナイスＢｏｄｙを生かして水泳部とか。遼達が教師なら当然女子校ですよね！！男子校はボツ（笑）

### [8057] やすこ	2002.09.30 MON 11:32:29　
	
こんにちは
月曜なのにだらだらと家におります。
何にもすることがないので
ＣＨのサントラでも聞いてようかな、
と思ってます。

＞［8056］千葉のＯＢＡ3様
昨日新宿行ったので
もう一度ＸＹＺ・ＢＯＸ見に行きました。
でもやっぱり書けなかったですよ。
ＢＯＸをちらちら見てる人もいました。
小心者の私にはやっぱり無理そうです

＞［8036］いちの様
青梅なんですか？
私は八王子ですよ
田舎度では同じくらいかな。
地方都市から来た人に、
「ここは東京じゃない！」とよく言われます。
結構悲しいですよ。

### [8056] 千葉のOBA3	2002.09.30 MON 09:00:01　
	
おはようございます！なんか、またまた台風接近中！？明日には千葉県に接近または上陸ですか？今年は台風の当たり年のようです。（もっといいものに当たりたい・・・。）

８０３６　いちのさま　そうですね。もしＸＹＺのカードが変わったら、それをいただきにもう一度マイシティに行きます。それが「勇者の会」に入会した者の使命さー。

８０４０　葵さま　おじいさまが入院されたとのこと、大丈夫ですか？この所、暑かったり、涼しくなったりと気温差が激しかったですものね。お大事にして下さい。・・・で、もう一度行きましょうよー！ＸＹＺしに！やはり、あのカードは貴重品！せっかく手にいれられる地域に住んでいるのだからして・・・。そしてぜひ「勇者の会」の長老に！！（ワハハハ・・・。）

８０３８　無言のパンダさま　いや、あの下敷きが貴重な品になるとは思ってもみませんでした。もらっておくものですね。今回ＸＹＺカードは３枚ほど記念にも持って帰りましたが・・・。大切にしまっておきます。（一枚は飾ってあります。）

８０４２　里奈さま　そうですねー！あの唯香ちゃんの時の「愛妻弁当」は最高でした。私はぜーったい、あんな手のこんだこといたしません。愛情の差ね・・・。（ダンナにも息子にも。）ところで夜更かしクイーン、すごいですね、それでお仕事できるんだから、やっぱり若いのねー！お肌荒れないように要注意ね！（こういうのを、老婆心といいます。）

８０４６　将人さま　私がＣＨ関連で持っているグッズといったら、この下敷きくらいです。昔ヤマトのとかは、ものすごく集めてました（富山さんのファンでしたから。）それこそ絵コンテから、アフレコ台本から持ってましたが、結婚する時かな？みんな処分しちゃったと思います。今思うともったいなかったです。とっておけば良かった｡

８０５２　いもさま　なんか、チラチラＸＹＺ．ＢＯＸを見ている人を観察するのも楽しかったりして。皆さん、ＸＹＺしたい、でも恥ずかしいって,あの辺をウロウロしてるみたいなので・・・。（それを、恥ずかしいと思わなかった私って、いったい・・・大汗。）

それでは、これにて。金曜まで長いですね！！

### [8055] 里奈	2002.09.30 MON 01:50:33　
	
今夜は友達と夜更かしして遊んでたんでまた深夜のカキコになってしまいましたぁ～。夜更かしクィーン今夜も空元気（笑）
みなさん『日本のアニメベスト１００』見ましたか？Ｃ･Ｈが出なかったのが残念でなりません…（悲）

☆８０５３　たま様
あらやだ、Ｃ･Ｈ関連のお店を里奈が偵察しに行くんですか？これは依頼？ＸＹＺ？？まっかせなさぁ～い！！バー『シティーハンター』だけじゃなくって、ラブホテル『ＸＹＺ』も偵察に行ってみようかしらん…♪でも一人では行けませんよね。一緒に行ってくださる殿方はいずこへ！？さぁー皆さん挙手スタート！

☆８０５４　いちの様
夜更かしプリンスのいちのさん、まだまだ甘いですわ！夜更かしってのは、窓の向こうで小鳥のさえずりが聞こえてきて、新聞配達のおじさんから直接新聞を受けとってこそ夜更かしです！！夜はこれからですわよ（笑）そんな里奈ちんは明日（今日？）も朝早くから仕事でやんす。


### [8054] いちの	2002.09.30 MON 00:38:10　
	
こんな時間にこんばんは。夜更かしプリンスいちのです（汗）。明日学校で寝てそうな予感大です（笑）。
それと、少し訂正を・・・[8037] いちののカキコ、「実家」ではなく正しくは「地元」です。実家がネオンギラギラだったら帰りたくなくなりますよね（嫌）。

＞[8039] TAKESHI様
大丈夫です。カキコの内容を見て、私宛のレスなんだと気づきましたから。だって漢（オトコ）ですから！（なんのこっちゃ）
それにしても、いろいろ名前をパクっているお店（？）ってあるんですね（驚）。付けた人もねらっているんでしょうかね？たくさん見つけると面白いですよね（笑）。

＞[8040] 葵様
祖父様は大丈夫でしょうか？お体にはお気をつけ下さいませ。私たちもね。季節の変わり目ってホントに風邪とかひきやすいんですね・・・そーいえば、私もなんだか咳がでる、ゴホゴホ。
ＸＹＺ２度目頑張って下さい！！（尊敬の眼差し☆）さすが、勇者の中の勇者！！２回も行くとは流石です☆（笑）

＞[8042] 里奈様
早速キリ番効果が現れましたね！！いいな～。私も今度からキリ番狙ってみようかな（笑）。狙って取れるものじゃないっすよね。
宝くじとか私も買ったことがないので分かりませんが（汗）、ツイテル今なら本気（マジ）で当たるかもよ♪

＞[8046] 将人（元・魔神）様
「自信」が「確信」に変わりました。（どっかで聞いたことある台詞）ＯＶＡがなんたらかんたらって言ってました（なんじゃそりゃ）。カッコよかったですよー。初めて動くゲッター見たのですが、まさか３機の組み合わせでロボが変わるなんて想像もしていなかったので、とても新鮮でした。

＞[8052] いも様
漢（オトコ）はスナックに行かないと思いますが・・・。漢（オトコ）はバーのカウンターで美女の隣にさりげなく座り『バーボン。』・・・これですよ！！（詳しくは「ザ･シークレットサービス」に出てくるシーンを思い浮かべてください）
ローズ「あら？今日はもっこりのお誘い、無いの？」
リョウ「君の青春を踏みにじりたくないんでね。もっこりは、答えが出た後にしよう。・・・おやすみ。」

か、カッコイイ～☆☆☆☆☆☆☆
たしかこんな感じだったとは思うのですが、細かいところで間違っているかもしれません。あしからず。
最後の「・・・おやすみ。」は妄想かもしれません（笑）。

另外，稍作更正...[8037]いちの帖子，正确地“本地”而不是“父母的房子”。 如果你父母的房子是刺眼的霓虹灯，你不会想回去的（我讨厌它）。

### [8053] たま	2002.09.29 SUN 23:22:29　
	
世の中には、色々なネーミングがあるものなのですね。（笑）
レスが大変なので、皆様の話をまとめてみました。
（手抜きって言わないでね。　　　　　　大汗）

福岡に、ラブホ「ＸＹＺ」
石川に、バー「シティーハンター」
栃木に、怪しい店？「ＣＡＴ’ｓ　ＥＹＥ」
東京かな？に、バー「ＣＡＴ’ｓ愛」
東大阪市に、何の店？「キャッツアイ」

その他「というわけで…」「男はつ＠い」「美味しん＠」「しずかちゃん」
「ホテルいいとも！」「ダウンタウン」「スナック・サザエさん」
（こらぁ～皆の衆！話がずれてきれるぞっ！）

私的には
石川県のバー「シティーハンター」が気になる所です。
何か関連グッツとか置いてあるかもしれませんね？！
TAKESHI様＆マイコ様（兄妹デュオ）は未成年？！なので偵察は無理ですか？
では、里奈さま偵察よろしくぅ～です♪（笑）

福冈的 "XYZ "情趣酒店  
石川县的"CityHunter"酒吧  
枥木的怪店？ “Cat'sEye“  
东京？ 东京的 "CAT's爱"酒吧  
东大阪的什么店？ "Cat'sEye"  

### [8052] いも	2002.09.29 SUN 22:03:15　
	
こんばんわ～。
レスのみです～。長くなりそう・・・。

＞たまさま[8026]
私がＸＹＺしてる間になんてオイシイ名前のホテル（笑）を見つけてるんですか～！！油断のならない人だなあ。(^_^;)
ちなみに私の知ってる珍店名は「スナック・サザエさん」です。

＞無言のパンダさま[8027]
頑張りました。好きなだけ褒めて下さい♪
褒めて、褒めて、褒め称えて下さい！！
そして、葵さまにもう一度ＭＹＣＩＹへ行くように説得して下さい。

＞千葉のOBA3さま[8030]
何となく気にしてるっぽい人とか結構いましたよ。(^_^;)
私はとりあえず用紙をゲットしたら一目散にＨＭＶを駆け抜け、エレベーター横の階段のとこまで逃げました。（怪しいな・汗）
で、なにくわぬ顔で記入した紙を入れにまた戻ったりして。何か今思うと「勇者」には相応しくない行動でしたね、コレ。

＞いちのさま[8036]
「やっと」とは何だー！！？（笑）
お礼（？）と言ってはナンですが、「スナック・サザエさん」の歌い放題券を差し上げます。漢ならば、スナックで歌の一つも歌えないとねー。え？そういうモンでしょ？

＞葵さま[8040][8041]
昨日は本当にありがとうございました。
お祖父様入院されたそうで・・お見舞い申し上げます。
やっぱり真の勇者としてはもう一度ＭＹＣＩＴＹに行くべきですよ！ホントにこのままでいーんですか！？（脅し）

やっぱ長くなってしまいました。申し訳ないです。
では、また～。いもでした。

### [8051] TAKESHI	2002.09.29 SUN 21:37:43　
	
＞[8050]Blue Cat様
そうでしたか！ごめんなさい！聞く限りでは富永みーなさんにそっくりだったので、ぜんぜん気づきませんでした。

### [8050] Ｂｌｕｅ　Ｃａｔ	2002.09.29 SUN 17:37:55　
	
　こんにちは～。
　コ○ンに“カオリ”って名前のキャラが出た回、わたしも見た記憶がありますよ～、少年探偵団がビラ配りしてるシーンや、コナ○が車のトランクに隠れてついてくシーンとかにもC・Hっぽさを感じたし、カオリって名前のキャラを伊倉さんに演じさせるなんて、絶対わざとだよねぇ～、って思いましたもん。しかも初めてのアニメ・オリジナルの話でだったし。

　C・Hキャラの教師ネタ、楽しく読ませてもらってます、で、わたしもつい、もしリョウたちが部活の顧問をするとしたら、なんて考えてしまいました（笑）
　リョウだったら・・・女子体操部のコーチとかをやりたがりそうですね、でも認められなくて、それでもめげずにしょちゅう手伝い（笑）とかしに行きそう、でやっぱり香におん出されたりして。
　槇村は・・・とっさに浮かんだのは囲碁とか将棋のクラブだったんだけど、意外と柔道部とかもいいかも。
　香は・・・何がいいかなぁ、手作りマスコットとかの腕からして、けっこう手芸部とかいいかもね。ちなみにわたしも高校時代は手芸部に入って、布とワイヤーで造花作ってました、ちゃんと色を考えて染めるとこからやったんだよ。
　で、冴子さんは、そのメス（ナイフ）さばきを生かして生物部とか（笑）

　そういえば今夜６時５６分から２時間、テ○ビ朝日系で「アニメ名シーン　ベスト１００」とかってのがやりますね、Ｃ・ＨやＣ・Ｅのシーンは流れるかなぁ。

＞［8019］葵さま
　あはは、わたしも他の方と意見がかぶらないうちに、と思って時間があるときに先にＷｏｒｄで文章作っといて、解禁したら早めに送信するようにしてるから（笑）
　はい、わたしはドラ・ファンです、首位打者争い、松井も福留もどっちもプレッシャーからか、ヒットがなかなか出なくなってますねぇ、今夜はどうなるかな？

＞［8039］ＴＡＫＥＳＨＩさま
　Ｃ・Ｈ実写版でのゴクミの吹き替えは、岡本麻弥さんじゃなかったですか？

我记得我也看了《柯南》中出现名为"Koari"的角色的那一集--我从少年探偵団发传单的场景和柯南躲在汽车后备箱里跟踪他们的场景中感受到了 C.H. 的风格，我认为让伊倉先生扮演名为 "Kaori "的角色绝对是有意为之。 这也是第一个原创动画故事。

### [8049] Ａｙｕ	2002.09.29 SUN 11:16:46　
	
里奈様・将人様
将人さんのレス爆笑しました(^-^)遼とミックの居る女子校には香&海坊主という説教係が絶対必要ですね（笑）健康診断の内科検診会場というのは女性教師のみが係員なのに遼&ミックはそれを差し置いてでも会場に侵入しそうですよね。生活指導を昔の三角関係メンバーの槇村・遼・冴子でやったら面白そうですよね！？ミックと遼の生活指導は超やばいですよね。

### [8048] TAKESHI	2002.09.29 SUN 11:11:48　
	
[8039]の書き込みで、いちのさん宛のレスで名前が抜けてました、申し訳ありません。

＞いちの様＞葵様＞将人様
結構アニメの名前を使っているお店あるんですね。他にもおもしろい名前のラブホテルありましたよ。「しずかちゃん」とか「ホテルいいとも！」とか「ダウンタウン」というのがありました。国道沿いにあるのでよく見かけます。

＞[8046]里奈様＞[8047]将人様
僕も海坊主役はシュワちゃんが適役だと思います！実は僕、シュワちゃんの大ファンで映画は必ずみてます。来年公開の「ターミネーター３」がとても待ちどおしい！！！やはり吹き替えを玄田さんがやってるので最近はシュワちゃんも適役だなーと思いました。

### [8047] 里奈	2002.09.29 SUN 03:22:45　
	
☆８０４６　将人（元･魔人）様
『ＸＹＺラブホテル』の内装の話し、面白かったですぅ～。でもそんなラブホテル行きたくねぇ～（笑）リョウが作った覗き穴＆通路がたくさんあって、しかも香のトラップがいたるところに仕掛けられてるんでしょ？しかもしかも、リョウはトラップがあるのを知ってて突進していくもんだから朝までリョウの悲鳴が響いてるんですよね。間違い無くお客はＣ･Ｈファンのみ？でもそんなトラップだらけのホテル、行くほうも命賭けじゃん！ひぃぃー！

実写版の話し、アーノルド･シュワルツネッガーが海坊主だったらほんと適役！もちろん、しっかりスキンヘッドにしてもらわなきゃねっ！

### [8046] 将人（元・魔神）	2002.09.29 SUN 02:43:24　
	
今日、BooK@ffに行ってきましたが、残念ながらCHのアニメ
コミックとかCH関連の物がが、無かったです。
僕が既にもっているコミックはあったんですけどね。
また明日も、古本屋さんを色々と探してみますね。

＞[8030] 千葉のOBA3さま
ＣＨの下敷きの裏が「気まぐれ＠レンジロード」と
「ＹＡＷＡ＠Ａ」ですか？
もうすっかり忘れてましたけど、ＣＨの番組に続いて
そのアニメ番組が放送されてましたね。懐かしいです。
そういう番組宣伝の物なんか持ってないんで、うらやましいです。
欲しいです。

それで思い出して「サンライズ・アニメ大全史」って
ＣＨのアニメ制作会社の作品が載っている本をひっぱり
出して見たら、ＣＨアニメの紹介で、たぶん放送関係者
などに配る資料の表紙の写真が載ってました。

「あの「キャッツ・アイ」の北条司が描く都会派サスペンス
コミックをアニメ化！」
ってキャッチコピーが表紙に書いてましたよ。

ついでに劇場版1作目の「愛と宿命のマグナム」のチラシの
写真には「ハードボイルドが心に染みる」ってキャッチコピーみたいでした。

＞[8031]TAKESHI様[8032]マイコ様
「XYZ」ホテルに続き「シティーハンター」って名前のバーがあるんですか？なんかリョウが通ってそうですね。

そうえいば私の住んでいる東大阪市にも「キャッツアイ」って店ありましたよ。
（ちょうどＴＶアニメ版がやっていた時期）その後、どの順番で名前が変わったか、忘れましたが「男はつ＠い」（よ）が無いとか、「美味しん＠」とかなんか、どっかで聞いたような名前に、よく変わっていましたけど・・・。（汗）(^^;
店の方か、オーナーの方の趣味なんだろうか？

＞[8016]yaeko様[8033]千葉のOBA3様
もう来年のカレンダーが出だす時期なんですね。
10月になってから宣伝するのかな～？どうなんでしょう。

今度はAHのカラーイラストも含まれるのかな？
CHとAHが別々の製品のカレンダーになったら悩むやろな。
北条先生の作品という範囲で、色々なイラストを選べたら
いいんですが～♪。
（去年はAHで香さんが亡くなっているのにショックを
受けていたのでCHの2人の絵なんて見れないと思って
カレンダー買えなかったんですけど。）

＞[8034]Ａｙｕ様
学校に、僚とミックがいたら、健康診断とか体育の着替え
とかの時って、たぶん持ち場を離れているでしょうね。
「いや、先生がのぞいてる」「この僚、何やっとんじゃ」ボカン
香さんの100ｔハンマーで殴られて、海ちゃんに
「それでも教師か！ちゃんと仕事しろ」と傷だらけで
ズルズルと引きずられて行く2人。

＞[8036] いちの様
「真チェンジ！ゲッターロボ」だったんですね。
そちらもＴＶで放送されていたんですね。

→元はOVAビデオ作品で、でるたんびに1巻ずつ借りていたら
なんと最終巻を借りた直後に、大阪の深夜にTVで放送
されたので「今までレンタルで借りていたボクって何」
って思い出がありますよ。

＞[8038]無言のパンダ様
オンラインでの本屋さん、セブンイレブンだけなんですね。
ファミリーマートだったら近いんですけどね。

コ＠ンに「カオリ」という役で伊倉さんが出ていたビデオ
の事、今日レンタルで借りるつもりでいたのに
すっかり忘れてました。

天王寺のツタヤにCHスペシャルのビデオを返却→
→お寺にお参り→日本橋電気街でパソコン関連の
掘り出し物が無いか探す→なんばのBooK@ffで本を探す

とかして、昼1時から夕方8時ぐらいまで、ずっと歩きずめ
で、疲れ果ててしまって、近所の駅前のツタヤに行くの
忘れてました。
（コ＠ンは近所でも揃ってそうなので）
明日か明後日、足が筋肉痛やろうな～。

＞[8039]TAKESHIさま
実写版ＣＨ見られましたか？たしかに一瞬ＣＨではなく、
実写版ストリートファイター2になってましたね。(汗)(^^;
なんか漫画的な変な誇張をしたコメディって感じですね。

他にリョウを演じられる俳優さんって、思いつかないですね
アクションをされる俳優さんは色々いますけど、きめる時は
きめて、ギャグとかも出来る人になると・・・

アーノルド・シュワルツネッガーさんは、アクションと
コメディをされていましたが、リョウ役よりも、どちらかと
言えば、海坊主役のイメージがありますね。
（スキンヘッドの頭を、どうするかは置いておいて）

＞[8042]里奈さま
スロット行ったら１万ですか？すごいですね。
8000番ゲットの効果なんでしょうね？

＞[8040] 葵さま
おじいさまが入院ですか？大変ですね。
季節の変わり目ですから、僕も気を付けないと・・・。

「ＣＡＴ’ｓ愛」という名前のバーがあるんですか？
北条先生の作品関連の名前、日本中探すと、どのぐらい
あるんでしょうね。

＞[8042]里奈さま
＞『ＸＹＺ』というラブホテルですかっ！？
＞中はどんな部屋なんだろぉ～（笑）

たぶんリョウが作った「のぞき」用の通路があって、
そこに香さんがトラップを仕掛けている部屋でしょうね（笑）
毎日、爆弾などのトラップの音と、リョウの悲鳴が聞こえる噂だそうです。（笑）

＞[8043] 里奈さま
コ＠ン見られたんですね。私も、できるだけ明日借りに
行こうと思ってます。

＞[8045] yaeko様
コ＠ンの、その話って、やっぱりスタッフとかのイタズラ
なんでしょうかね。明日、レンタルビデオ探してみますね。


＞[8031]TAKESHI様[8032]マイコ様  
在"XYZ"酒店后面有一家名为"CityHunter"的酒吧？ 听起来像是獠会去的地方。

我住的东大阪市就有一家叫"Cat'sEye"的酒吧。(就在电视动画版播出的时候）。  
我不记得之后改名的顺序了，但他们经常把名字改成我以前听过的名字，比如 "Otoko-tatsu@i"（哟）不见了，或者 "Mijishin@"之类的......。（汗） (^^;)

这是店家的爱好还是店主的爱好？

### [8045] yaeko	2002.09.29 SUN 01:20:58　
	
こんばんは。

「アニメスタッフの遊び心の巻」
コ○ン、ここまで反響があるとは思いませんでした。。。ちょっちビックリです。[8043]里奈さま、面白かったでしょ☆絶対ＣＨを意識してキャラ設定してますよね。脚本もＣＨでも書かれていた方ですし。
でも、そうなんですよ。コ○ンに出てくるカオリはＣＨの香とは違うんです。「みたいな」キャラなんです。パラレルなんですよね。あぁ！なんて都合の良い言葉。なので、あまりに期待して見るとガッカリされるかも。皆さまアニメスタッフの遊び心を楽しみましょう☆
それでは。

### [8044] Kathine	2002.09.29 SUN 00:10:56　
	
http://kimbumsoo.co.kr/mid/mv/day.asf
この悲哀のMTV、希望皆さんはこれを好む!

### [8043] 里奈	2002.09.29 SUN 00:03:36　
	
名探偵コ○ン第６話『バレンタイン殺人事件』見ました。
私これテレビで見てました！でもあの二人らしき人物が出てただなんて全然気付かなかったなぁ～。『かおり』という女性は確かに少し香っぽい容姿でしたね。ショートカットにジーンズ、声も伊倉さんでした。でもリョウみたいな赤いＴシャツ着た筋肉ムキムキ男、『俊秀』は声が違うから気付きませんでした。ってゆうか、よく考えたら神谷明さんは『毛利小五郎』役で毎回出ておられますもんね（笑）『俊秀』はリョウみたいなあからさまではありませんが、少しもっこり男でしたね。ハンマー食らわす香の姿が見たかったですが、ここに出てくる『かおり』はＣ･Ｈの香とはやっぱり違う。でもエンディング後に出てくる『俊秀』と『欄』のやりとりがリョウと香っぽかったです！
Ｃ･Ｈファンには面白いと思います。見てなくて興味のある方、是非見てみて下さい（＾ヮ＾）

我看了《名侦探柯南》的第6集《情人节谋杀案》。  
我是在电视上看的 但我根本没注意到，里面有两个人长得很像他们。 那个叫Kaori的女人当然有点像香。 她剪着短发，穿着牛仔裤，声音也很像伊倉小姐。 但我没认出 "俊秀"，一个和獠一样穿着红色 T 恤的肌肉男，因为他的声音不一样。 我的意思是，如果你仔细想想，神谷明在每一集里都扮演毛利小五郎（笑）。 我很想看到香挥锤子，但这里的Kaori与 CH的香不同。 但结局后 "俊秀 "和 "兰 "的交流就像是獠和香织！
我觉得这对 C.H. 的粉丝来说很有趣。 如果您还没看过，又有兴趣的话，请看一看！

### [8042] 里奈	2002.09.28 SAT 23:19:03　
	
知らぬ間にすごいカキコの量！失礼とは思いますが、めっちゃお腹減ってるもんでカップヌードル（カレー味）食べながらレスさせて頂きまっす！
あっ、それから８０００番ＧＥＴでまず最初に起こったイイコトを報告…。今日仕事の休憩中に友達に誘われてスロット行ったら１万勝ちました♪スロットとか全然しないからワケわかんなかったんですけどね（笑）

☆８０２５　いも様
なるほど、８０００ＧＥＴが誕生日プレゼントか！たしかになかなか手に入らないイイプレゼントでした♪このカキコに参加されている皆様からのプレゼントだと解釈させていただきます☆
みんなありがとぉー（＾０＾）／

☆８０２６　たま様
『ＸＹＺ』というラブホテルですかっ！？中はどんな部屋なんだろぉ～（笑）名前だけで普通かしらん？Ｃ･Ｈとは関係ありませんが、『というわけで…』っていうラブホテルもありますよね。このネーミングに騙された女性は何人いるんだろうか…。

☆８０２７　無言のパンダ様
へぇ～、ネットショッピングなのに本の代金だけでイイんですか？送料とか無いんだぁー。それだと本屋いくより便利ですね。一生懸命探す必要ないし！

☆８０２８　将人（元･魔人）様
では、とりあえずビデオ探してそれ見てダビングして、その後に小説読んで、またビデオを見るコースに決定！楽しみ♪
もしビデオ全部撮り終えてからＤＶＤが出たら大ショックなんですけどっ！！？でも全部借りて撮った達成感で自己満足できるからいいか（笑）

☆８０３０　千葉のＯＢＡ３様
香ちゃんの女の子らしさと家庭的な面が表れている場面はたくさんありますが、私は香の手作り弁当も大好きです♪原作３１巻でリョウが野上唯香ちゃんと一緒に学校へ行く時に香がリョウのお弁当を渡すんですが、これがまたすごい愛妻（？）弁当なんですよね！読んでる私まで恥ずかしくなるくらい…。『りょう』ってでっかく書かれてて、ミニハンマーにミニタコ（海坊主）とか入っててすごく可愛い！あぁゆうお弁当作ってくれる人、もういないんじゃない？？

☆８０３２　マイコ様
小説にパラパラ漫画付いてるんですか？そんなの付いてたら、私電車の中でずっとパラパラしてそう（笑）

☆８０３４　Ａｙｕ様
私も教師ネタ考えてる時すごく楽しかったです♪
『害虫の標本』っていう表現、けっこう自分でも好きだったりします。（てへへ…）また何かお題くださいねっ☆

☆８０３５　ＯＲＥＯ様
今日さっそくツタヤに行ってオンラインクーポン使って来ましたよぉー！ほんとに画面見せるだけで感激しちゃいました！こんなにお得なのにどうして今まで知らなかったんだぁー！しかも良く見ると、まわりのオジサマ達なんかも携帯見せて半額にしてもらってて、全然気付いてなかった自分が情けない…。今まで損してた分、イッパイ借りて取り戻すぞぉー！！

☆８０３６　いちの様
『買いなさい』って、命令形ですか！？（笑）でも私宝くじなんか買ったことなぁーい！！でもでも、スロットで１万勝ったからほんとに今ならイケルかも…？でへへ…何買おう♪

☆８０３９　ＴＡＫＥＳＨＩ様
そうですよね。やっぱりリョウを演じられる人は彼だけですよね！日本人だと誰だろう？あんまり想像できないなぁ～。

☆ｙａｅｋｏ様
今日、リョウ＆香ちゃんが出てると噂される名探偵コ○ン借りてきました！早速今から見ます♪見たらまた感想カキコ致しまぁ～っす♪


### [8041] 葵	2002.09.28 SAT 22:42:48　
	
　１００トンハンマー管理人さま、私信でスミマセン☆

　まろてぃメンバー、葵までメール下さい。いもさまが一人でお待ちしてます。
