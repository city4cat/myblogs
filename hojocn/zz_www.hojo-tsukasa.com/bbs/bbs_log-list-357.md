https://web.archive.org/web/20050313021048/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=357

### [7140] たま	2002.07.20 SAT 22:33:35　
	
こんばんわ。「たま」でっす。
昨日のリターンズ聞きましたよん。
昨日はＡＨを始めるっということで、ラジ＠に聞き耳を立てていましたが、なんと、終わり３分くらい？に少しだけ？・・・・・でしたねぇ。
少し残念っというか、物足りないっというか、～ん、何と申しましょうか。
でも、神谷さんの話「冴羽リョウに対する思い」とかが聞けて、神谷さんの思いの深さを知ることができ、良かったです。

＞ちょこ様[７１２５]ＣＤロムの件お返事ありがとうございます♪んんんが、やっぱりＣＤロムには入れられないみたいですねぇ～。何かそれ専用のソフトとかがあるんですかねぇ～。

＞まろ様[７１３１][７１３６]はじめましてです。そして、★耳寄り情報★ありがとうございます。
コンポを使うなんて、なるほどぉぉぉぉ！ですね。
さっそく、接続コード買って試してみます。

でもやっぱり、ＣＤに録音したいんですけど、無理なんでしょうか？
皆様へ、引き続き　―情報求む―　です。

北条センセー♪　センセーの公式ページなのに、あまり関係ないことばっか書いてすみませ～ん。
来週号のＡＨ(冴子の誕生日)＆雷神(ＣＨスペシャル)＆雷神の付録楽しみに待ってま～す♪

昨天因为要开始AH的事，竖起耳朵听了广播@，竟然，结束3分钟左右?只有两一点点?是·····啊。
是有点遗憾呢，还是有点遗憾呢，嗯，该怎么说呢?
但是，听了神谷先生的话“对冴羽獠的想法”等，知道了神谷先生的想法的深度，太好了。


### [7139] 葵	2002.07.20 SAT 22:12:15　
	
　リターンズ、聞きました！聞けました！（笑）なんとか復旧させて聞けましたよぉ～っ☆目を閉じて聞くと、ホントにリアル。これから毎週聞けるなんてうっとりです♪

＞まろさま［７１３６］
　ＡＨの時間を教えていただきまして感謝です♪（キチンと全部聞いてない、あさはかなＦＡＮでした☆）聴コミの保存方法、私もやってみますね♪

### [7138] ハル	2002.07.20 SAT 20:12:19　
	
お久しぶりです！こんばんわ！
いや～！自分の家にＰＣが無いものだから、久しぶりに見るとｶｷｺがごっつ進行している（＾＾；）
今週のバンチは私も立ち読み（笑）
ＡＨのみが私をバンチ購入へと導いてたりなんかしたりして・・。

### [7137] 冴子ファン	2002.07.20 SAT 19:31:24　
	
はじめまして！
シティーハンターの野上冴子刑事（今は署長ですね）
の大ファンです。
ストーリー、各キャラとも完成された作品の中にあって、
強気かつエッチそうな冴子の存在は
最高の華ともいえるでしょう。
最近、エッチな冴子さんがあまり見れませんが
一ファンとして、ぜひ、
冴子さんのビキニ姿を拝んでみたいものです。
（もち超ハイレグの紐ビキニっすよ！）
不純ですが、こういうファンも存在するのが
北条ファン層の懐の広さとも言えましょう。
期待しております。

### [7136] まろ（神奈川県：いけね・・・汗）	2002.07.20 SAT 18:50:39　
	
むっちゃ暑かったですねぇ、今日一日は・・・(滝汗)

度々の書き込み、どうかお許しの程を・・・(苦笑)

[７１３４] 『元祖』無言のパンダさま他、皆さま
説明不足になってしまいまして、申し訳ありません（汗）
基本的にオーディオ機器（ＣＤラジカセ・ミニミニコンポなど）を
お持ちの方で、それらの機器の後ろに外部入力端子という、
『赤』と『白』のプラグ（この２色は音声端子を表します）を
挿すところがあるもの
（３万円以上のものなら大抵・付いてます）ならば、まずＯＫです。

次に、ヘッドホン端子と、外部入力端子とを
『接続できるコード』（家電量販店などで売ってます）を用意して頂き、
それぞれＰＣのヘッドホン端子と、オーディオ機器の外部入力端子に
つなぎます。

そして、オーディオの方を、まずＡＵＸというモードに合わせ、
各自の用意した記録媒体（ＭＤ・カセットテープなど）を
セットしておきます。
序で、ＰＣの音量を最大にして、
（今回の場合ですと、番組再生時間２３分０２秒開始なので）
２２分５０秒前後にプレイヤーを一時停止させておきます。
そして、神谷さんが「どうぞ！」と言った瞬間に（笑）
録音開始ですぅ！！
あとは、ＢＨＫのニュース（？）の放送終了まで、そのままを維持（笑）
最後、停止ボタンを押して無事終了です！

ただし、ご注意頂きたいのは、サーバーが混雑する、
夜間は避けていただきたいのと、
回線が不調に陥ると、番組の再生が強制終了される可能性がある、
という事です。

この場合は、再度チャレンジして頂く他はありません。
基本的に日曜の午前中辺りに録音するとスムーズにいく事と思います。

是非お試しください！　では×２！

### [7135] 千葉のOBA3（あっつーい！）	2002.07.20 SAT 16:00:26　
	
こんにちは！聞きましたよー！リターンズ。私めも神谷さんのりょうちゃんへの思い入れの深さに超カンゲキ！なんか、すっごくうれしかったー！！来週も楽しみだなー！

今週号のバンチのＡＨ登場人物相関図。おもしろいんだけど、なぜ海ちゃんと信宏がつながってないの？「師弟」と結んでほしかった・・・。あと香ちゃん、もっとかわいいカットにして。（ホントうるさいやっちゃなー、我ながら。）来週は、ななにっ！？冴子さんの誕生日？お年ばれちゃうの読者に？やっぱ、オーバーフォーティー？？？なのかなー？アーーー，来週が待ちきれない！

７１０８　無言のパンダさま　私はいちおうＡＨが休載でもバンチは買ってるんですよ。子供たちが「ワイルドリーガー」とか、「わーた」を楽しみにしてるんで。でも買うのは金曜日になりますね、やっぱり。


７１２４　いちのさま　寝違えですか。あれは痛い。なるべくムリに動かさないほうがいいと思いますよ。私は小鮒をとっていて土手でしりもちをつきむち打ち症になったことがあります。（オバカでしょ。）スイカも１／２食べたら気分はカブト虫。実家に帰る前にお腹こわさないように・・・。（今年はまだやってませんよね。）

７１１２　おっさんさま　ムリしないでくださいねー！今大丈夫でも年とってからまたぶり返したりするといけませんよ。テーピングとか膝ベルトとか、膝をカバーしたほうがいいかもしれない。（お仕事中特に！）

ひょっとして，今日あたり梅雨明けですかねー。風がすごく強くて窓があけられないんです。この強風なんとかならんかいね。

### [7134] 無言のパンダ	2002.07.20 SAT 15:58:55　
	
こんにちわ☆

昨日のリターンズの中での神谷さんのお話、あらためてご本人から聞いて本当にジーンとしてしまいました。
ただ神谷さんが、このところの強行スケジュールのせいか夏バテ（？）でお疲れのようでしたので心配になりました(^_^;)早く回復して、また元気バリバリのお声を聞かせてくださいね！

＞まろ様（７１３１）
さすが、まろ様！耳寄りな情報をありがとうございました！
そーんな方法があったなんて、目からうろこです☆
私も、もっと早くまろ様から聞いて「リターンズ」でメールを読んでいただいたところだけでも残せればよかったデス！
あ、でもＭＤ、プレイヤーごと持ってない・・・ですけどネ。って、やっぱダメじゃ～ん！(T_T)

### [7133] Ｂｌｕｅ　Ｃａｔ	2002.07.20 SAT 11:55:11　
	
　こんにちは。
　昨夜のリターンズを聴いて、神谷さんの中でリョウがどんなに大きな存在なのかをあらためて思い知らされた気がして・・・・演じる役者さんにここまで思わせる作品を産み出した北条さんってあらためてすごいって思いました。リョウと神谷さんはまさに一心同体なんですね、そんな神谷さんの演じるＡ・Ｈのリョウを、いつかアニメで堪能できる日が来たらいいな・・・なんて思ってしまいました。ますます切なくなりそうだけどね。
　聴コミといえば、バンチが創刊されたばかりの頃コアミックスのＨＰでやってた聴コミを聴くために、ほとんど勢いとカンでＲｅａｌＰｌａｙｅｒをダウンロードしたときのことを思い出します。途中英語だらけで「えっ」と思ってよく見たら地名みたいだったんで、とりあえずたぶんと日本の地名らしきもの（たしかＯＳＡＫＡだったような気が・・）をクリックして先に進んだ記憶があるんですが・・・１年以上前なのですでに曖昧です、しかもカンでやってたので本当にあれでよかったのかもさだかじゃないし^^;　とりあえず聴けたんで大丈夫だったのかな、とは思ってるんですけど・・。

＞桃子さま
　Ｍａｃなら最初からＲｅａｌＰｌａｙｅｒが入ってるかもしれないので、それならわざわざダウンロードしなくてもいいし、一度確認してみてはいかがでしょうか。入っていれば、「Ｒｅａｌ」ってとこをクリックすれば聴けると思いますよ。

你好。  
听了昨晚的回归，感觉重新认识到了在神谷先生的心中獠是多么大的存在····饰演的演员到这里产生了让人想到作品的北条先生再次觉得厉害。原来獠和神谷先生真的是一体的，我希望有一天，我们能在动画中欣赏到神谷先生配音的A.H的獠……我想。好像越来越难过了。

### [7132] いちの	2002.07.20 SAT 11:07:45　
	
みなさん、こんにちは。今日の首の具合は「昨日よりマシ」です。
いや～、そんなことよりもラジ＠ですよ。神谷さんの涙ながらに語ったオープンキャンパスでの話し。そりゃあ泣きますよ。これが泣かずにいられますか？「ＡＨで香が死ぬなんて・・・。」（うるうる；）

＞[7127] いも様
スイカなら私も負けませんよ（笑）。去年の話しですが幻の『スイカ１/２』を贅沢にも実現させてしまいました。半分に割ったスイカをスプーンで食すのです・・・美味。

＞[7128] OREO様
１行レス気に入って頂けたでしょうか？（手抜きではござらん）首の方はリポ●タンＤ飲んだので大丈夫です（笑）。
ラジ＠すごくよかったですね。神谷さんの潤んだ声にもらい泣きしてしまいそうでした。

＞[7126] 桃子様
今週のラジ＠は逃せませんよ！！！絶対聴かないと損です！！！ＲｅａｌＰｌａｙｅｒ８Ｂａｓｉｃなら無料ですし、日本語の説明も付いている・・・はずです。たぶん。頑張ってラジ＠をモノにしてください。

来週も楽しみどすえ。

不，比起这种事，我更喜欢。神谷先生在校园开放日上声泪俱下的演讲。 没错，我哭了。 怎么能不哭呢？“真不敢相信香竟然在AH中死了..."


### [7131] まろ（耳寄りな情報を・・・）	2002.07.20 SAT 09:39:19　
	
押覇っス！
いよいよ始まりましたね！
ＡＨの聴コミ（ちょうコミ）！
”あの”抽選に漏れて以来、長い道のりでした（苦笑）
んで、ちょっと『タメになる（？）』情報を・・・

[７１２５]ちょこさま
はじめまして。
北条センセの公式ＨＰにお邪魔するようになって９ヶ月の、まろと申します。
例の聴コミの保存方法ですが――、
そんなに、ムズカシク考えなくても良いですよ(微笑)

実は私の場合、ＰＣにあるヘッドフォン端子から、
ミニコンポの外部入力端子につないで、ＭＤに落として（→録音して）います。

こうすれば、何もＣＤ－ＲやＣＤ－ＲＷ等に焼く
（→記録する）必要はありませんし、何より極論から云えば、
カセットテープにも落とすことさえ出来ますよ(微笑)

こーゆー時は、ローテクもアリってことで(笑)
しかも、ヘッドフォンステレオ（ウォー○マンは商標なので・汗）や、
カーステレオなど、いつでも何処でも聴コミを堪能できますよ！

と、ゆーワケで、他の皆さまも、是非×２お試し下さいませ。

### [7130] yaeko	2002.07.20 SAT 01:10:09　
	
こんばんは。
「今週号も買いましたよんの巻」
予告の「今日は冴子さんの誕生日！しかし気を遣うりょうちゃんと海ちゃんの行動が裏目に！？」が、気になる～！！ドタバタ話かな～？感動話かな～？とにかく早く読みたいよ！
あと1週間は長い。。。

「雷神っての巻」
雷神CHスペシャルのおまけってパソ持ってない人には魅力ないかもね。またコンビニで何週間も余ってそうな気がする。雷神の企画って成功してるのかな？おまけを付けないで内容で勝負しないのかな？おまけ欲しさで今回の雑誌は買うけどさ、雑誌自体に魅力って感じないんですけど。英語嫌いだしさ。。。ま、とにかく、CHのスクリーンセイバーが早く見たい！欲しい！！だから買う！！！今使っているCHのスクリーンセイバーもかなりお気に入り！それ以上のできだといいな★期待しちゃおっと。
それでは。

“雷神之卷。”  
雷神CH特别版的赠品对于没有Paso的人来说可能没有魅力。感觉又要在便利店多待几个星期了。雷神的企划成功了吗?不加赠品，不以内容取胜吗?因为想要赠品才买了这本杂志，但杂志本身并没有什么魅力。我讨厌英语…总之，我想快点看到CH的screen saber!想要!!所以买!!!现在使用的CH的screen saber也相当喜欢!如果比那个更好就好了★期待一下。
阿

### [7129] うみこ	2002.07.20 SAT 00:59:09　
	
こんばんは～。今週は「Ａ・Ｈ」はお休みだったのですね。
先週は冴羽さんの涙に私も涙が出ました。
冴羽さんの涙は本当に数回しかなくて、初めて見た涙は「Ｃ・Ｈ」の時でしたが本当に衝撃的で涙が出たのを覚えてます。今読んでも泣ける・・・しかしその直後の展開に泣き笑いになってしまうという・・・おそるべし（笑）。
とにかく冴羽さんの涙は切なすぎてだめです。
あんまり悲しかったので「Ｃ・Ｈ」を読みかえすことにします。って余計にメロメロかも・・・？
それではまたこっそりお邪魔します。

### [7128] OREO	2002.07.20 SAT 00:34:37　
	
こんばんわ～皆様。
先ほどまでラジ＠に夢中でわれを忘れてたＯＲＥＯです。
手元にコミックスそろえて聞いちゃいました。
今回のラジ＠、神谷さんが先月のオープンキャンパスでの
事をお話していらっしゃって、僚に対する思いってのを
たっぷりと聞かせていただいた気がします。先週のバンチ、
神谷さんも泣いたみたいでしたよ（泣きますよねぇそりゃ）

＞７１２７いも様
はじめまして、こんばんわ。すごいですね。スイカ１/４
食べれちゃうんですか？実はそれやってみたいという夢
でもあるのですが、私はどうもスイカの種をぜ～んぶ
最初にとらないと気がすまないので、１/４なんて
食べるまでに気が遠くなってしまいます・・・（悲）
＞７１２６桃子様
まだ諦めないでください！Ｌｉｖｅには間にあわなかった
けれども、再放送がラジ＠にはありますから！
明日から一週間は今日の放送聞けますから、
なんとかダウンロード頑張ってください！
＞７１２５ちょこ様
私もそのラジ＠聞きましたよ。でもどうやるんだろうって
方法がわからなくって・・・私も方法探ってみますね。
でも、もし判明した暁にはゼヒ教えてください！
＞７１２４いちの様
一行レス、ありがとうございます。（笑）
しかし・・・首、大丈夫ですか？お大事にデス。
＞７１２１Kinshi様
はじめまして、こんばんわ。
一人だとそうですよね。一個はねぇ・・・大きいし。
でもその黒ストライプのこだわり。わかります♪
たまたま今は田舎に帰ってるので食べれてるという幸運です

来週のバンチは冴子さんが絡んでくるっぽいですよね。
どんなお話になるのか２週間分楽しみ♪
それに付け加えて来週のバンチには北条センセの
ロスレポートがカラー特集されてるとか！！
これは絶対見逃せませんね！
な～んか、私っていっつも長い。すみません。幅とって・・
それでは失礼します。

### [7127] いも	2002.07.19 FRI 23:33:55　
	
こんばんわ～。
一週間ぶりです、いもです。
みなさん、今頃は「ラジ＠」に夢中ですかね。

ＡＨがない今週のバンチは、バイト先のセ○ンイレブンで、仕事の後立ち読み（爆）してきました。
無言のパンダさま[7122]・葵さま[7123]がおっしゃられてるように、ＡＨ相関図はよかったです。リョウから阿香への矢印のとことか、そうくるかって感じで。
しかもどーゆうわけか、海ちゃんが白いんです。（笑）
明らかにＡＨの世界の海ちゃんなんですけど、まだ処理前（笑）だったんでしょうか。(^_^;)
このネタ、さっき（昼間）書こうと思ったんですが、一応最新号の話題なのでやめました。

＞kinshiさま[7121]
こんばんわ～。
私はすいかなら、１／４切れ（丸いのの）いけます。（笑）
夏はすいかがご飯がわりでも全然オッケーです！
ちなみに昆虫ではないのです。
失礼しました。（汗）

ではでは～、いもでした。

### [7126] 桃子	2002.07.19 FRI 23:08:05　
	
みなさんありがとうございます。でも・・・せっかくおしえてもらったのに、ぜんぜんだめでした。ダウンロードとかインストールとか、ほとんどパソコンの事分からなくて、画面の指示どうりにやってみたんですが、途中から前文英語が出てきて何をどう入力すればいーのかさっぱり分からなくなってしまいました。もう諦めるしかないのでしょうか・・・自分の頭の悪さがとてもはがゆいです。

### [7125] ちょこ	2002.07.19 FRI 22:12:32　
	
＞７１１６たま様＞７１１８OREO様＞７１２１ｋｉｎｓｈｉ様
こんばんは～たまたま今日は早くきてみてよかったです。ＣＤロムの件ですが、私も以前ラジ＠で読まれたお便りの中に「神谷さんにお便りで名前を呼んでいただいたのをＣＤロムに録音して保存版にしました」とゆうのを聞いて、じゃーＡＨ放送も保存できるのかな？と思いＣＤロムを買ってみたのですが、やり方が分からない！！とゆー訳でＣＤロムのお客様相談室とやらに電話してみたのですが、ま～２２時じゃやってないわけで（＾＾；明日かけてみようと思います。ごめんなさいどこが準備バンタンじゃーって感じですね。

### [7124] いちの	2002.07.19 FRI 22:07:56　
	
みなさん、こんばんは。
今日は朝から寝違えて首が痛い・・・（涙）。
そういえば、もう今日は金曜日なんですね（遅い？）学校がないとどうしても曜日の感覚がなくなってしまって、今日が何曜日なのか、何日なのかわからなくなってしまいます（汗）。
バンチ読んでないし、首は痛いし・・・。早く故郷に帰りたいなぁ～・・・。

＞[7112] おっさん様　
“いちの”と申します。こちらこそよろしくお願いします。お互い身体には気をつけましょう・・・。

＞[7113] 葵様
コンタクトは付けるのが怖いです。目に指で・・・はー！！

＞[7114] 桃子様　はじめまして。
ラジ＠はこの後すぐですぞ！！（どの後？）急いで！！！

＞[7118] OREO様　スイカとは夏してますね。
８月には帰る予定です。早くしないと廃人になりかねません。

以上、いちのの１行レスでした。（手抜きではござらん。）

### [7123] 葵	2002.07.19 FRI 21:31:29　
	
　ＡＨがない…。でも無言のパンダさま［７１２２］が書かれてる相関図には笑えました。来週は冴子さんのお誕生日とか…。冴子さん、とうとう年齢、ばらしちゃうんですか？！（☆∧☆）

＞たまさま［７１１６］
　ふふふ…何気に暑いので気になっちゃったんですよ。冴羽アパートにエアコンなんてモノはなかったかなぁ…。（依頼がなくて金欠状態の彼らに、エアコンは要らない？！）

＞Ｋｉｎｓｈｉさま［７１２１］
　頭痛もバカにできないですよ。私も以前、バファリ○飲んで寝たら薬が切れた頃に目が覚めて、眠れなくなりましたもん。頭痛いと、何事もやる気がなくなりますもんね。治ってよかったです。

### [7122] 無言のパンダ	2002.07.19 FRI 18:53:21　
	
こんばんわ★

今日発売のバンチ、コンビニでちょっと見てきたんですが、ＡＨがお休みの代わり？に「ＡＨ相関図」なるものが載っていました☆出来ればこの１ページだけコピーしたくなりましたヨ♪（笑）

ＰＳ：このＢＢＳの時計表示、かなり遅れてません？


### [7121] kinshi	2002.07.19 FRI 14:41:01　
	
こんにちは、

＞７１１３　葵さま
ご心配ありがとうございます。今日は、すっかり元気になりました（笑）
頭痛ってやっかいですね　原因がわからないのがほとんどですもの。
直ればすっかり忘れちゃって。単純なんですね、私って・・・。


＞おっさんさま
膝、だいじょうぶですか？無理しないでください。
仕事仲間に迷惑をかけるからって　今、無理をしたら、
この先　もっと大変になりますよ。今のうちに　休んで、
直しておかないと・・・・自分の体なんですから。
ナンテ・・・ちょっとお母さんしてみたりして・・・ゴメンンサイ。


＞７１１８　ＯＲＥＯさま　はじめまして。
すいか食べたんですか～いいな～私も食べた～イ！。
一人でいると　すいか　買うチャンスが　無いんですよね、
デザート用に切って売っていても、なぜかスイカは、
あの緑の皮というか　黒のストライプ模様が　ないと
スイカって感じがしなくって・・・買う気がしないんです。
変ですかね？


今日はラジ＠放送ですね、私もＣＤ入れられるか
試してみようかな？と　思っていますが、なんせ初めてのＴＲＹ
なんで、誰か知っていたら・・・特に　３０分全部ではなく、
ＡＨの所だけ、毎週入れていって、一枚におさまれば・・・・
こんなことって　出来ますかね？無理でしょうかね？
・・・何言ってるんだか　自分でもわからなくなった！
では、また。
