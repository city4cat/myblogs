https://web.archive.org/web/20021031225845/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=248

### [4960] ｌｕｎａ	2001.12.10 MON 21:35:52　
	
みなさん
こんばんわ
風邪はやってますねぇ
あたしのまわり、風邪ひきさんばっかりですよ
よく寝て、よく食べて、抵抗力をつけて、風邪に負けないよーに
あーあ
ＣＨの再放送始まってから、ずーっとなんとか、見れてたのに
１３話抜けてしまったよ
いつ、放映されたんやろ？
深夜、不規則な時間帯で放映するし
Ｇコードとかも、他の番組と一緒になってる時あるから
ＣＨの表示が抜けてたかも？
ＤＶＤ化してくれないかなあ
すっごい希望しますぅ
おっさんさん
元気ですかあ
なにかの検査の結果、なんともなくってよかったですぅ
おっさんさん、やっぱり、その人ストーカーですよぉ
変な人に気に入られましたねぇ
超迷惑だよねぇ
もぉーほんと、気ぃつけて下さいよ！
無言のパンダさーん
こんばんわですぅ
もちろん、リョウさんを心の恋人と思う気持ち、認めさせて
頂きまーす
それと、あたしも、そんな無言のパンダさんが、期待してるよーな趣味は、残念ながらないですぅ
けど、かなりの女好きですよぉ　あたしは！
あらあら引かないでぇーーー
ノーマルやからさあ
あるでしょ
同じ女として、この人いいなあってゆーの
それですよぉ
さあっ
明日は、バンチの日だあ
気合入れて、明日を迎えるぞぉ（おおげさなヤツやなぁ）
ちゃこさん
愛猫ちゃんは、元気にしてますかあ？

### [4959] 葵	2001.12.10 MON 21:26:14　
	
　くっそ寒いっ☆明日は「バンチ」ですね。みなさま風邪に気をつけて、元気に「バンチ」しましょ♪

＞まろさま［４９５２］
　ルイ１６世、あってましたね。やった♪（←それほどのものか？！）しかしルイ１６世といい、俊夫といい、安原さんてどこか３のセンがありませんか？しかも、頼りなさそうな…。昔、好きで見てた「Ａチ○ム」では、二枚目の「フェイスマン」をやってらしたっけ。でも、彼もどこかやっぱり３のセンだったなぁ…。（笑）

### [4958] おっさん(岐阜県：本日休み）	2001.12.10 MON 19:59:04　
	
毎度、今日は仕事が休みでしたが病院へいってきました。先週の結果でしたので以上はなかったのでよかったのです。ということで、今日もいつものご挨拶せ～のおっは～！！
レスっす。
black glasses様（４９５３）、おっは～！！ドラゴンズファン私もそうです。星野監督にはなってほしくないなやっぱり。阪神ファンの方すみません＾～＾；。
まろはん（４９５２）、おいっす～！！一回昔本当に信頼できる友達がいましたが、おもいっきり裏切られたので本当に信頼できる友達がいるのか不安です。一番話すことの出来る相手彼（前の上司）に話すことかな・・・？？？
明日はバンチ発売日、ということで今日はこの辺で・・・・？？？？？

### [4957] black glasses（愛知県）	2001.12.10 MON 16:56:49　
	
どうも、こんにちは！ただいま学校より帰りました。

＞無言のパンダ様[4956]
静電気のことですが２年ほど前に「伊東家の食卓」でやってたことがあります。（僕は静電気があまり起きない体質なので僕自身試したことはありませんが、伊東家の方たちが静電気が起きやすい環境のもと実験していてかなりの効果が期待できるようでした）それでそのやり方ですが、手の平全体で物にさわればよいとのことでした。車の場合、まず手の平をしっかりと広げてドアに対して平行にさわった後にレバーに指をやって開ければ静電気はあまり起きないようです。同様にドアのノブもドアに対してノブの平行な面に手の平伸ばしてさわればおきないようでした。その起きない理由は普通に指先で触れるとその触れたところが点となりその1点に静電気が集中するため痛いくらいの強い静電気が起き、逆に手の平全体でさわると触れたところが面になるので、面になる分、静電気が拡散して弱く発生するらしいです。言葉で表すと難しいのでわかりづらかったかもしれませんが、もしそうでしたら言ってもらえれば今度こそはわかりやすいように説明します。
あの番組は基本的には主婦業を中心に役立つことが多いですが、たまに僕みたいな若者にも役立つことがあって助かっています。（例えば真夏、炎天下の中においておいた車の中の温度を下げる方法とかありました）

1度みなさんも試してみてくださいね～（伊東家の食卓風に）

### [4956] 無言のパンダ	2001.12.10 MON 13:33:10　
	
こんにちわ(^^)

ハイ。私もついさっきＡＨ第２巻ゲットしてきました～！
北条センセー！うちの子は「もっこりって何？」とも聞いてきましたが、それよりも一番困ったのが「一発って何？」でした。
これにはどう答えたらよいのでしょうか？？？

＞ｋｉｎｓｈｉ様（４９５０）（４９５１）
さっそくの静電気系レス（？）ありがとうございます！
ツバはちょっとアレですが（笑）ウェットティッシュなどを使ってもいいでしょうか？ぜひ実行してみます！静電気防止グッズもたくさん出ているけど、どれが一番使いやすくて効果があるのかわからなくて悩んでます。とにかく痛いんですよねぇ、静電気！(~_~;)

＞まろ様（４９５２）
レス、ありがとうございます。(^^)
千葉さんてアドリブが多そうですけど、かえって私なんか期待しちゃいますよ。それから難波圭一さんですが、Ｓムーンの「ゾイサイト」や「海野ぐりお」（このへんで思い出したでしょ？）古くは聖＠士星矢の「アフロディーテ」タ＠チの「上杉和也」南＠少年パ＠ワくん初期の「タンノくん」など・・・。最近は「伊東＠の＠卓」やワイドショーのナレーションなどに出てらっしゃいます。アニメでは「ナルシスト」や「オカマ？」ぽい役が多いですよね。またそれがハマってらっしゃる！（笑）

では今からＡＨ２巻読みます♪

### [4955] つかさ	2001.12.10 MON 13:14:14　
	
　こんちわっです。
Ａ･Ｈ2巻、買いました。んで、思ったことがあるんですが、
りょうちゃんは、いったい何ヶ国語話せるんだろう？
誰か知っています？
Ｃ･Ｈの時も、確か何語だったか忘れちゃったんですけど、
王女様と侍女のサリナとのお別れの時に話してましたよね？
これから先、りょうちゃんが何語を話せるのかちょっとわくわくです。


　＞まろ様
次回でＳＳ最終回なんですね。私としては、もっと読みたかったです。(T-T)　　なんで、第2弾を期待してますです。

それでは、今日はこれにて。

### [4954] Ｂｌｕｅ　Ｃａｔ	2001.12.10 MON 10:46:21　
	
　Ａ・Ｈ２巻、発売日にゲットしました！　カバーが描き下しじゃなかったのはちょっと残念かなとも思いましたが、この絵のリョウは大好きなんで、こうして使ってもらえてやっぱり嬉しいです。ずっとそばにいられるもの♪

＞さえむらさま、ｂｌａｃｋ　ｇｌａｓｓｅｓさま
　わたしもドラゴンズ・ファンなんで、星野さんには阪神の監督になってほしくないです！　あの女のせいで、とばっちりじゃないかぁ！　星野さんにはずっとドラゴンズのものでいて欲しい・・・なんで岡田監督じゃだめなのっ。うるうる。

### [4953] black glasses（愛知県）	2001.12.10 MON 09:35:59　
	
どうも、おはようございます
先日、とあるアーティスト（昔は初登場１位はあたりまえで韓国で日本人初のコンサートをやった人たちです。ここまで言えばほとんどの方がわかると思いますが）のコンサートチケットを手に入れるため電話を受付開始と同時にかけたのですがなかなかつながらず４日間も名古屋でやるし、最近の曲はあまりヒットしていないので大丈夫だろうと思い、３０分ごとに何回かかける程度でやっていたのですが受付開始から３時間もたたないうちに売り切れになってしまい、自分の考えが甘かったと痛感させられました。

＞さえむら様[4936]
どうもお久しぶりです。
僕も星野さんには阪神に行って欲しくありません。大反対です。ずっと中日でやってきたのですから中日以外のところで監督をやらないでー！この気持ち他のファンの方にはわからないかもしれませんが、わかりやすく言うと長嶋さんが巨人以外のチームで監督やるかもしれないという話が出た時の巨人ファンの気持ちといえばわかるでしょうか？長嶋さんがミスタージャイアンツなら星野さんはミスタードラゴンズです。

今回は私信でした。ごめんなさい。
そろそろ学校へ行かねばならないのでこの辺で失礼します。
それでは、みなさんGOOD LUCK！

### [4952] まろ(神奈川県：明日は・・・）	2001.12.10 MON 09:18:16　
	
おっは～っす！
ＡＨ２巻のユニク”ラ”（ＵＮＩ　ＫＵＲＡ）の店長、ちょっちカワイソ（笑）とか思う、まろでやんす。
じゃ、なぜか彼のセリフ引用ネタからいってみよォ～！
「・・・またやられた・・・冷や汗（後頭部にタンコブ）」

なお、本日の[まろのＳＳ]は、第１９話のみ掲載、最終話（第２０話）は明日の予定です。
また、今回・特別に、最終話の予告を、ＳＳの終わりに追加しております。

[４９４２]さえむらさま
　レス、ありがとうございます。
私的な話になってしまいますが、あのクルマ、プリムス　ダラクーダ（愛称はクーダ）というそうです。
何でも、’７１年式のクーダは、十数台しか存在しない『特別な』モノとのこと。なのに壊されても、翌週にはシッカリ直ってたりして（笑）
　そーいえば、リョウのクーパー、ＣＨの柏木　圭子さんの話で、誘拐された香を救出する際、今でいうＧＰＳ（※１）を積んでましたが、ＡＨの彼のクルマには、やっぱ何かのシカケ（特殊装備）があるのかなァ？とか思ったりして（汗）

[４９４３]・[４９４７]無言のパンダさま
　レスありがとうございました！
「よ～し！ポ○モン、ゲットだぜ！」（←知らないので許したって下さい・汗）
松本さん、言われてみるとナルホドっす！智ちゃんてば、若干トーンが低い感じしますよネ！（←勝手に同意を求めないように！）
熊ママに千葉さんですか（爆）イケてます！ところで、難波さんって、思いっきり・ド忘れしてしまってるんですが、どんな方でしたっけ？（出ている作品・役名など教えてくださ～い！）

[４９４５]葵さま
　安原さんが、ル○１６世っての、あってますよ～（笑）

[４９４６]おっさんさま
　レス、ありがとうござんス。
んと、『元祖』無言のパンダさまやｌｕｎａさまのおっしゃるように、いざと言うとき、最も信頼できるお友達に気付いてもらえるよう、前もって知らせておくの、必要だと思いますヨ。

　
[まろのＳＳのコーナー]

第１９話　『本家』と『元祖』

「そー言われると、そうかもしれない」
「フ・・・やはりアイツ（『本家』ＣＨ）――か。
　念の為に訊くが、そいつ、ホントに髪の色・黒かったかい？」
「う～ん、そう言われると、やや金髪っぽかったかしら」（←こらこら・汗）
「やれやれ・・・
　折角ココ（ヨコハマ）で、海ちゃんや冴子達と水入らず
　呑み明かそうと思ったのに、世話の焼ける・・・
　（たくぅ、外人のやることは、たまに意味不明だぜ・冷や汗）
　で、どこの署へ連れてかれたんだ？そいつぁ」
「あ、それなら・さっき、西新宿署の野上署長が引っぱっていきましたよ」
　俊夫が付け加える。
「フッ（冴子らしいな）・・・サンキュー。じゃあな、皆さん！」
　　　　「（な、俺達も帰ろーゼ、タカ）」
　　　　「（ああ、カチョーもウルサイシし、な・笑）」
　　　　　港署の二人は、それとなく去っていった。
「み・・・皆さん・・・て・・・」――（つづく）

　　　　　　　　　　　　　　　予　　告

　突如、自宅を襲った悪夢に放心状態の俊夫。
それに構わず、帰ろうとするリョウ達を引き止める者とは？
　次回、最終話「覚醒の瞬間（とき）」、おたのしみに。

※１・・・『汎地球測位システム』の略。人工衛星を使い、自身の正確な現在地を測定する。近年の地上部隊の斥候（せっこう。スカウトともいう）能力を飛躍的に高める軍事技術である。この衛星回線の一部を民間に許可したものが、あのカーナビである。

### [4951] kinshi	2001.12.10 MON 00:32:34　
	
すみません。車のドア　開けるとき　でしたね。
車に手を　触れる前に　手を湿らすと　よいそうです。
でも、湿らすっていうのも・・・ちょっと　湿ったハンカチを持つ、とか、ええぃ！いっそうのこと　ツバで・・・いやでしょうけど・・・

私も、毎年冬の課題です。
では、また。

### [4950] kinshi	2001.12.10 MON 00:22:57　
	
こんばんは、この間のカキコに、日曜日がコワイ　と書きましたが、残念ながら、待ち人来たらず・・・どうやら、次回までのおあづけのようです。　すみませんかなり私的なことで。

＞無言のパンダ様
返信ありがとうございます。
車から、降りるときの静電気、とっておきの方法　教えます。ぜひやってみてください。
車から、降りるとき、ドアをあけたら、外に足がつく前に、車のボディに　手をふれてください。できれば、降りて、ドアをしめるまで、触れているのが、ＢＥＳＴですが、とにかく　ガラスでなく、外側のボディに触れながら、降りてみてください。

もし　すでに　ご存知でしたら、すみません。

では、また。

### [4949] chikka(Eastern Shizuoka)	2001.12.09 SUN 22:35:03　
	
【４９４２】さえむら様
それですが、「北条司スペシャルイラストレーションズ」のインタビューの記事で書いてありました。

「３６番」こと「劉信宏君」の声ですが、私は「田中真弓」さんがいいかなあと思います。その理由は「グラス・ハート」の声が「高山みなみ」さんだからです。この二人はＮＨＫ教育で放送中の「忍たま乱太郎」で共演しているので（高山さんは乱太郎役で、田中さんはきり丸役）「Ａｎｇｅｌ Ｈｅａｒｔ」で「乱きりコンビ」を聞きたいなあと思うので・・・・。

【4942】さえむら様  
我在《北条司特别画报》的采访报道中提到过。

“36号”刘信宏的声音，我觉得“田中真弓”比较好。理由是“GlassHeart”的声音是“高山南”。因为这两个人在NHK教育放送中的《忍者乱太郎》中有过共演(高山饰演乱太郎，田中饰演桐丸)《Angel Heart》中想听“乱桐组合”…


### [4948] タウンハンター(雪が降ってる！）	2001.12.09 SUN 22:31:56　
	
こんばんわ！
毎日寒いですね～。
こんな夜は、ぬくぬくとコタツに猫のように
丸くなっていたいものです。

いよいよ５０００に近づいてきましたね～
いっちょ狙ってみよっかな？なんてね、
子供みたいですんまそん。

【コミックス】
すっかり忘れてた！まだ買ってないです。
１巻の時みたいにまた若干変わっているのでしょうか？
小遣いをまだカミサンから貰ってないので
手に入るのはいつになるのやら・・・
「いい歳してマンガなんて」と言われないようにしないと（汗）

【レス】
＞あおさん
多分初めましてかどうかはわすれちゃいました（汗）
どうもありがとう、これからもヨロシク！
同じく、海里さん、さえむらさんもお言葉感謝デス！
みなさんも良いお年を・・・ってまだ早いか（笑）

### [4947] 無言のパンダ	2001.12.09 SUN 22:24:04　
	
レス第三弾。すいません、いつまでも・・・(^_^;)

追加
ｓｗｅｅｐｅｒ様（４８９９）
セリフの引用のプレゼント、ありがとうございます！
実際リョウにああ言われたら、即「あなたの愛！」と言っちゃいます！（笑）

＞ちゃんちゃん様（４８９４）
プレゼントは靴をもらいました♪
あんまり、ぼろっちいの履いてたから見るに見かねたみたい！

＞まろ様（４８９８）
ＡＨの声優さん、まろ様が言ってたのは、もっちーではなくて
智ちゃんでしたね（汗）う～ん、じゃあ意外なところで「松本梨香さん」ポ＠モンで有名な松本さんですが、ここは「ママ＠ぽよぽよザウ＠スがお好き」のママ役の声のイメージで。(^_^;)

ここから第三弾。(~_~;)
＞海里様（４９１３）
バースデイメッセージ、ありがとうございました！
「先輩ママ」なんてとんでもないです。
「先輩不良ママ」←（ママとして）ですが、これからもよろしくお願いします(^^)

＞葵様（４９１６）
バースデイメッセージ、ありがとうございました！
風邪は良くなりましたか？長引くと聞いてますが・・・(~_~;)
おばあさまの「肺炎」も早く回復されるといいですね。
それから、「コヨーテの早業」にウケてくださってありがとうございます（笑）あのシーンを見た時、不謹慎ですが私の頭の中ではピンクレディの「♪くち＠る盗む早業は～」（ｂｙ渚のシンドバット）のフレーズが流れていたのですヨ（笑）←古い！

＞まりも様（４９２０）
バースデイメッセージ、ありがとうございました！
わかります！プレゼントは生活から離れたモノがいいに決まってますよ。女性って「プレゼント」に関しては、いつまでも「夢」を持ち続けてますもんね(^_^)ここらへんが男の人にはなかなか理解されないんですよね。私もプレゼントに「現金」もらった時は興ざめでしたよ。

＞あお様（４９３４）
バースデイメッセージ、ありがとうございました！
論文ｅｔｃの件は解決しましたか？
バイト先の店長さんも風邪を引いてらっしゃるそうですが、うつされないように気をつけてくださいね(^_^;)
それから、梅干＋ねぎ＋生姜＆味噌って、おいしそうで効きそうですね♪

＞さえむら様（４９４２）
バースデイメッセージ、ありがとうございました！
さえむら様も楽しいクリスマスと新しい年を迎えて、まだまだ若さを謳歌してくださいね。

本当にみなさまありがとうございました。もし万が一抜けてる方がいらしたらごめんなさい。(~_~;)
それからＢＢＳのみなさま、掲示板を私的に使わせてもらってすみませんでした。あまりにうれしくて、どうしてもお一人お一人にお礼を言わなくては気が済まなかったもので・・・
ご迷惑をおかけしました。(._.)これからもよろしくお願いします。失礼しました★ｆｒｏｍ無言のパンダ★

### [4946] おっさん（岐阜県：即脱走・・・）	2001.12.09 SUN 22:13:00　
	
毎度、今日は仕事忙しかったっす。ということで、今日もいつものご挨拶せ～のおっは～！！
レスっす。
無言のパンダちゃ～ん（４９４１）・さえむらは～ん（４９４２）・lunaちゃま（４９３９）・まろはん（４９３７）、おいっす～！！ほんまに有難うっす。何とか元気でやってますよ。無言のパンダちゃ～ん、おっつ～！！例の彼も誕生日でしたがまだ、何もしてないというか行く暇がなくてＴ－Ｔあ～むなしいのう・・・。さえむらは～ん・lunaちゃま、おいっす～！！一応持ってます。すんっげ～音のするやつを車の中に携帯してます。まろはん、おいっす～！！怖いてマジで。特に一人のときは・・・駐車場で私は普通に出ましたがあの野郎追いかけてくるみたいに一緒に出てきましたのでめっちゃ嫌でしたＴ－Ｔ。途中で私は左であの野郎は右なのでいいのですが＾～＾；。
けいちゃ～ん（４９４４）、お久っす。おげんこでしたか？私もＨＰ昨日少しの間開けれませんでした。
ということで、今日はこのへんで・・・？？？？？

### [4945] 葵	2001.12.09 SUN 22:07:13　
	
　皆さまつながったみたいでヨカッタです。ココにこれない時って、マジであせっちゃいますよね。他に皆さんとお会いできるトコ、ないもんなぁ…。「予備の部屋」とかあったらイイですね。（笑）

　第二巻、私も風通しの良いトコに置いておいたのに…現在は妹に拉致されて、彼女の風通しの悪い部屋にいます。あわれ！第二巻！！北条せんせ、風通しの悪いトコに置いておくと、腐ったりしませんよねぇ…？！

　明日は月曜日。ＴＶＫでは朝のアニメタイムに「ベル○ら」やってます。（このトシで、しっかりビデオに入れてる私…☆）で、こないだ気になったんだけど、ルイ１６世の声って、安原さん（ＣＥの俊夫）かなぁ…。確認しわすれて、そのまま消しちゃいました☆明日、しっかりチェックしまぁ～す♪

### [4944] けいちゃん	2001.12.09 SUN 21:24:51　
	
昨日、ＨＰあけようとしたけど、だめだったの～。
なぜかしら～～～？？
もっかして、わたしだけ？？
あ、そうそう、ＡＨ２巻ＧＥＴしたぞ～～。
通して読むと、スピード感あるー！
あと、急がずじっくり展開してるなってのが、わかった。
読書上の注意をよんで、私も風通しのよい所におきましたよ。

### [4943] 無言のパンダ	2001.12.09 SUN 19:17:34　
	
さきほどは長々と失礼しました。
簡潔にと思っているのですが、ついつい・・・気をつけます！

＞ちゃんちゃん様（４８９４）
バースデイメッセージ、ありがとうございました！
「とろけるような愛のムチ！？」い、痛い・・・！
でもクセになりそう～(~_~;)
「じゃじゃじゃじゃ～ん」はすでに終了したのですが、ラストの「ハク＠ョン＠魔王」との別れのシーンは何年経って見ても、泣けますねぇ。それから「本家無言のパンダ」へのレス、ありがとうございました！感激してましたヨ～。

＞まろ様（４８９８）
バースデイメッセージ＆バースデイソング、ありがとうございました！彼女（愛車）にもよろしくお伝えくださいね(^^)
えー、もっちーと熊ママの声優さんですが、私もまろ様のイメージに同感かなぁ。んでも無理やり考えてみたのですが、もっちーに「茶風林さん」熊ママには、意表を突いてちょっとキレイ系のオカマ声ということで「難波圭一さん」。面白オカマ声で「千葉繁さん」というのはどうでしょう？やっぱり「玄田哲章さん」の二役っていうのも捨てがたいんですが・・・（笑）

＞ｌｕｎａ様（４９０５）
バースデイメッセージ、ありがとうございました！
それから私もリョウの心の恋人のひとりとして認めてくれてありがとう(^^)でもｌｕｎａ様とも「あやしい関係」ですかぁ？！
そ、そういう趣味はないんですが・・・(~_~;)←変な汗。
リョウがどんな設定の、どんな漫画に登場しても、「心の恋人」には変わりないというのには、もちろん同感ですヨ～。

＞ちゃこ様（４９１２）
バースデイメッセージ、ありがとうございました！
心温まる、お手紙風メッセージに感動してしまいました。
子供の頃から病気がちだったり、親の愛情を感じられなかったりで「生まれてきてごめんね」的な気持ちが大きかったけれど、ちゃこ様のメッセージにあったように「生まれてきてくれてありがとう」と言われる生き方を目標にがんばろうって元気が出ました！
(^^)

レス第三弾に続きます。(^_^)/~


### [4942] さえむら	2001.12.09 SUN 17:37:25　
	
本日は、レスメインで。

[コミックスの「読書上の注意」]
「北条先生の娘さんが３才の時、母親のお尻を....」うんぬんの所、なにかで以前に読んだような気がするんですが、テレビアニメのムックではないみたいだし、Ｃ・Ｈの単行本の冒頭も全部確認したけれど、載っていない。
おかしいなぁ。
記憶ちがいかなぁ（下らない事にかけての記憶力は、抜群なハズ、なのにな～）

[レス]
＞まろ様[4842]
レス、ありがとうございます。
今週は、見のがさずに「ナッシュ」見ました。（たまに、見逃しちゃうんです）
あの黄色いオープンカー、捜査車両には向いていない気がするのは、気のせいだろうか....。

＞タウンハンター様[4856]
遅くなりましたが、御結婚、おめでとうございます。
これからの人生を、よき伴侶と、末永くお幸せに暮らされますように...。

＞sweeper様[4870]
ありがとうございます。
仕事が不規則なのは、一月末までなので（三月末までの契約なので。派遣なもので）、２月３月はもうちょい、頻繁に来る予定(^^;)

＞「元祖」無言のパンダ様
12/6、お誕生日おめでとうございます。

＞おっさん様
そのアブない人が、危害を加えてこないことをお祈りします。（痴漢撃退グッズの携帯（香ちゃんのスプレー「女性を誘拐する、冴子さんからの依頼の話」は、行き過ぎかもしれないけど（汗）
髪をピンクに染めたそうで....、探そうとしなくても、みつけてしまいそうな....。
う～ん。いいのかなぁ？

それでは。
みなさま、お元気で。

### [4941] 無言のパンダ	2001.12.09 SUN 16:28:41　
	
こんにちわ。
冬になって、足の乾燥肌と静電気に困っています。車のドアをあける時もそうですが、スーパーなどで金属製の棚や缶詰などをさわろうものならビリビリッ！！思わず「痛ッ！」と声が出てしまって変な目で見られてしまい、とっても恥ずかしい無言のパンダです。

レスです。
＞ｓｗｅｅｐｅｒ様（４８１６）（すみません(__)）
バースデイメッセージ、ありがとうございました！
ほんと、最近のアニメの曲って有名アーティストが担当してるの多いですね。私もその代表的とも言えるコナンのベスト盤もってます♪とっても得した気分になりましたヨ(^_^)
それから「パ＠ワくん」のイトウくんはずっと、玄田さんでした。途中で変わられたのはタンノくんの難波圭一さんでした(^_^;)

＞まみころ様（４８７８）
バースデイメッセージ、ありがとうございました！
その後、ＰＣの調子はどうですか？
私はＰＣに関してはド素人なので、いつどうなるかわからないので日々ドキドキの毎日です(^_^;)
居酒屋、以前うちの近所に「養老＠滝」（おやじくさい？！）があって、結構お気に入りだったのですがいつのまにか消えちゃって寂しい限りです。(ｰｰ;)ああ、またあのチューハイ飲みたいなぁ（なんだか特別おいしかったんですヨ）

＞おっさん様（４８８８）
バースデイメッセージ、ありがとうございました！
そういえば、おっさん様の彼の誕生日も６日でしたね(^^)
なにかお祝い事はあったんですか？
でも今は、それどころじゃないのかもしれませんね・・・。
私もｌｕｎａ様と同様、まわりに声をかけておくことも大切だと思いますよ。おっさん様が仕事の仲間に迷惑かけられないと思う気持ちもよくわかりますが、もし何か変わったことが起きた時に
みんなも「どうしてもっと早く話してくれなかったのか」と思うんじゃないかなぁ？誰か信頼のおける一人か二人くらいにでも、話しておいたほうがいいように思うのですが・・・。
おっさん様は責任感が人一倍強いから、余計につらいのだと思います。あんまり一人で考え込まないでくださいね。
おっさん様から明るさが消えてしまったら、とっても悲しいですから。ほんとですよ。

＞ｋｉｎｓｈｉ様（４８８９）
バースデイメッセージ、ありがとうございました！
特別なことは何もなかったんですが、とってもひさしぶりに家族で外食しました。（費用は私持ち）一応お好み焼き屋さんなんですが、いろんなメニューがあって、とってもおいしいんですよ。
おすすめは「ネギ焼き」特にモチ入り。他にもカキや山芋のバター焼き、豆腐ステーキなんかもおいしかったです♪
ここのおばさん、実は弟の同級生のお母さまなんです。「男に頼って生きてる女が一番キライよ！」と言い放つ、とっても男っぽい人なんですが、お好み焼きはやさしい味でした。

長くなったので、ひとまずこれで失礼します。

