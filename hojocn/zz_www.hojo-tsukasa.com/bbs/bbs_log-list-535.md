https://web.archive.org/web/20031027183232/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=535

### [10700] ayako	2003.06.05 THU 19:05:32　
	
こんばんわ。今日が暑かったですね(^_^)A
みなさん、ビデオのあまり普及してなかったころは苦労してたんですね。私の生まれた頃はすでにビデオがあったからなあ。（1983年生まれ）

＜将人さま＞
台湾の人に聞いたんですけど、オークションに出てるのは全部海賊版だそうです。台湾の人はみんな海賊版だと知らずに買ってるらしいです。

＜ふじっちょさま＞
ウチの大学にはいっぱい留学生いますよ。留学生は秋学期入学で日本人の友達がなかなか出来ないそうです。私の居るゼミには台湾人と中国人とモンゴル人がいます。台湾では日本の漫画が大人気で北条作品の他にヒカル○碁も人気だそうです。

明日はバンチの日です！今週はどんな話だろう。ワクワク☆

### [10699] 無言のパンダ	2003.06.05 THU 18:34:59　
	
こんばんわ★

わーい！やっと涼しくなってきた♪（体調不良にご注意！）

＞ふじっ○ょさま（６８６）
「清い心は一日にしてならず！」これからもｋｉｎｓｈｉさまの愛のムチに耐えて精進してくれたまえ。(　▽｜｜｜)＼(￣￣＊)
・・・むむむっ！「偉大なる・・」というのはもしや・・・まさか肉体的に「偉大」という意味ではないでしょうね？！
Ψ（｀◇´）√~~~~~~~~ Σ（ 。。)ビシーッ！！（秘儀アメとムチ！）

＞きよさま（６８７）
スバラシイ！きよさま☆絶対素質あるわ～♪ちなみに私の「夢」に登場した友人は夢の中でもしっかりしていたけど現実にもリッパな看護婦さんになりましたヨ☆もしかしてあれって「お告げ」だったのかしらん？(^_^;)

＞ぽん☆さま（６８８・６９０）
リョウも香も、あまりに早く大人になってしまった・・・というか、ならざるを得なかった生い立ち＆環境だったので、もう少し無邪気に青春させてあげたかったなぁって気持ちもあるんですよね～(^.^)
あとリョウの「老けポイント」ですが（汗）みなさんのおっしゃるように髪型やシワに加え、顔に「影」が目立つような気がするんですが・・・あれは「やつれ」かなぁ？苦労してるもんね、リョウちゃん(>_<)無理もないか・・・。（勝手に納得^_^;）

＞葵さま（６８９）
だーれが怒ったってんだーっ！？心配してただけなのにさ！（｀ε´）でもパンダの陽動作戦に引っかかるなんて・・・フッ♪まだまだ甘いな。（￣ー￣）

＞ｋｉｎｓｈｉさま（６９１）
きゃは♪お疲れさまでした～！今日は目をゆっくり休ませてくださいネ☆私は「ハリセン」でしばかれたーい！かも♪(^_-)

＞将人さま（６９２）
たまには～？！結構最近ツッコミ多くないですか～？
※リョウちゃんの「老けポイント解説」お見事です！(ﾟoﾟ)

＞ｙｏｓｉｋｏさま（６９３）
回転グリグリって・・・なんか痛そう☆(>_<)話題に乗り遅れた時の「罰ゲーム」か～？！

＞千葉のＯＢＡ３さま（６９４）
ほんとに今回のｙｏｓｉｋｏさまのカキコは珍しく「ツッコミ所」に欠けてますねぇ～ちょっぴりサミシイかも（笑）

＞圭之助さま（６９５・６９６）
パンダの仲間☆めっけ♪ｖ(≧∇≦)ｖ
でも「ツッコミ所」のオイシイとこを自分で訂正しちゃダメじゃん！せっかく「ＸＹＺ」を一文字で書けるのか？！って期待したのに～！

＞Ｍａｍｉさま（６９７）
娘さんと鏡の前で「ＸＹＺ」を表現～？！きゃ～カワイイ♪
「Ｚ」って、ちょっとヨガっぽい？(^_^;)

ではまた。いつもながら長くてごめんちゃい！(^^ゞ

### [10698] ふじっちょ	2003.06.05 THU 18:20:04　
	
こんちわ～ふじっちょです。

ココに書き込み始めていつの間にか１ヶ月たっていることに今頃気づいた愚か者。日が経つのが早いと思う今日この頃…
なんて、黄昏てみたりしてちょっと憂鬱気味です(笑)

＞[688]＆[690] ぽん☆さま
ＡＨでは「美樹」はまだでてないですね。もし出るとしたら、多分ＣＨとは違う設定で出るんじゃないですか？今回はコマンドではなくて、泥棒とか、探偵とか、花屋とか…etc(笑)今度は逆に海坊主がゾッコンだったりして（『こもれ陽～』の沙羅のパパ見たいに）でも、美樹が登場してＣＥに住み込むようになったら信宏の居場所無くなるんじゃ…（玄武門に転職？）

＞リョウが老けている
確かに、明らかにＣＨのころよりは老けていますね。将人さまが書かれたとおり、北条先生の画風が変わった変わったのも関係あるでしょう。絵的な事は将人さまが言われた事と同意見です。それ以外には、リョウの行動がＣＨの頃より明らかに変わっていると思います。
ＣＨの頃は、ナンパのために新宿中を駆け回っていましたし、ノゾキのために壁に張り付いたりしていましたが、ＡＨではそれらはまだ行われていない。これは、年を取ったためにパワーが衰えたから？（J.M.Cなんて技はしていたが…）
また、「２０歳（ハタチ）」と言わなくなったのも関係してるんじゃないかな？やっぱり、リョウも年相応の行動は弁えているんですかね？(^_^)

アニメでもリョウは自分の事「２０歳（ハタチ）」とはアンマリ言って無かったような…’９１で誕生日が話題に上がった時、ちゃんと（？）２歳老けて「２２歳」って言ってましたよ（それでも、２０代なんだけどな＜笑＞）

用事が出来たので他の人へのレスはまた後でm(__)m

### [10697] Ｍａｍｉ	2003.06.05 THU 16:24:27　
	
またまたおひさしぶりっこのＭａｍｉでーす。

あっというまに回転してますね。本当に自分のカキコ探すの大変ですっ（汗）

＞圭之助（けーの）さま
　体調の心配してくださってありがとうございます。けーのさまは復活されたようでよかったですね☆　私も早く治します～。

＞ぽぽ（Hyogo）さま
　槇ちゃん役の声優さんからの命名、初めて知ったので興味深く読ませていただきました。私、田中秀幸さんの声大好きなんですよ～。だから槇村役で聞けた時は嬉しかったです♪

＞ＯＲＥＯさま
　お久しぶりです。お元気そうで何よりです。お忙しいでしょうが体に気をつけて頑張ってくださいませ。そして、ココでもまた楽しいカキコしてくださいね～。

＞たまさま
　お久しぶりです～。また以前のようにパワー発揮してくださいませ。

＞千葉のＯＢＡ３さま
　里奈さまのＸＹＺの手ほどきからえらいモノが飛び出してしまいましたね。カキコ見て爆笑とともに想像いたしました。ＣＨでの「Ｈｅｌｌｏ頭だし」も思い出しました。

＞里奈さま
　お忙しーて商売繁盛、何よりです。体気をつけてね。ＸＹＺの件初めて知って「ほー！そーなんかー」と思いました。雷神のこと全然しらなくてセインさんがりょうの声なんですね。いい感じかも～。

＞ＸＹＺの体表現に燃えるみなさま
　いろいろなバージョンが出できて嬉しいです。娘と一緒に鏡の前でやってみましたよん。Ｚを二人でしようと思ったけど寝転ばなきゃムリでした～。

＞ぽん☆さま
　私もＣＨの時のような、じれったい関係大好きです♪はまっていたのが思春期だったからかよけいにそう思いましたね～。ＡＨ
では「めおと」になってしまっているので、「こいびと」時代の話が見たいです。ＣＨでもＡＨでも香ちゃんはよき「ぱーとなー」。また登場して欲しいです。

＞ａｙａｋｏさま
　私もテレビの前でマイク持ってたくちです（笑）

＞将人（元・魔人）さま
　レスみて思いましたが、全然注意力落ちてるとは感じませんよ～。だって将人さまてばすごい研究してますよ。りょうの老若なんてページまで書けるなんて。んー、すごい！

＞葵さま
　お忙しそうですがお体気をつけてね。いろいろとお導きありがとうございます。虎嫌いでしたね。失礼しました～。

とても長くなってしまいました。すみません。明日はやっと金曜日。後一日頑張るゾー！

### [10696] 圭之助（けーの）	2003.06.05 THU 13:24:46　
	
あ！まちがってる！！

＞パンダさまのレス
「一文字」→「人文字」です…。

### [10695] 圭之助（けーの）	2003.06.05 THU 13:23:11　
	
こんにちは！圭之助です～ (^o^)/
ほんと２～３日来ないと自分のカキコ探すのも一苦労ね☆

＞将人さま
将人さまの胸の前で手を交差しての「Ｘ」…私は「ひょうきん族」のザンゲの神様を思い出してしまいました…(+_+;)!?

＞Blue Catさま
「Ｚ」はカキコを読みながら実際にやってみたところ…やはり「シェ～ッ」に近い…。そして「Ｘ」は…思わず秋刀魚ちゃんの「ダメダメ」を連想してしまい、一人笑い転げてしまいました☆すぐお笑いに走ってしまう。やっぱ私って関西人…（汗）

＞無言のパンダさま
「ＸＹＺ」一文字、そーか、とん○るずのもじもじ君なら確かに可能かも…みっ…見てみたいッ (~_~;)

＞葵さま
パンダ化してきてる!?
そ、それは大変!!!ナゼ、どーして、こんな事にィィ～ (T_T)
今ならまだ戻れるかしら（笑）

＞ayakoさま
いえいえ、バカだなんて。一度はその方法やってみた事あるって人、多いんじゃない？少なくとも圭之助はビデオ買うまで、ずーっとそうだったよ (^^)

＞ぽん☆さま
圭之助が感じたのは、基本的にふけ顔に見えるのは顔にシワ線（というのでしょうか？）があるだけで、随分違うと思いますよ。ＡＨではシワのない表情のコマ、少ないけれど探してみると若くないですか？１０６９２で将人さまが言っておられる内容も参考になるかナ！
あと、ＣＨに比べると体格が実際の人体に近くなってるようにも思うのですけど。ＣＨではガッチリしてるけど若者らしく(!?)スマートでしょ？ＡＨは、より人間臭くなってるというか何というか…そんな風にも思えるんですけど☆
う～～、うまく表現できないわ!ゴメンナサイ!!! (>_<)

でも、年をとるにつれて頬が下がり気味になる方もいるでしょ？今のリョウは確かに頬が少し落ちた感じはするけど「下がり頬派」にならなくて良かった…（整形前の宍○錠みたいに☆）

### [10694] 千葉のOBA3	2003.06.05 THU 07:45:49　
	
おはようございます。バタバタと毎日を送るうちに早木曜日・・・。うーーー、眠い！土曜の疲れが今ごろ出て来たワイ・・・。（？）

６９３　　ｙｏｓｉｋｏさま　　うーーーん。このカキコの中では「カキコに」を「カキコニ」と書いてあるくらいしかわからなかった。（笑）パンダさまは？（しかし、ＣＨ→ＡＨと繰り返し読んで行くって、うらやましすぎるよーーー！ｙｏｓｉｋｏさまぁー！）

６７３　　ＯＲＥＯさま　　お元気そうですね！！お仕事がんばってね！！体に気をつけて！（まあ、ＯＲＥＯさまは体力娘だから、心配ないか・・・。）

６７６　　里奈さま　　里奈さまいなくて寂しかったわよ－－－ん！ってだれも殿方が言ってくれてないような・・・。水着屋さんのお仕事波に乗ってきたかい？（水着だけに・・・。）また、がんばって暴走してねー！？

６７９　　きよさま　　きよさまは、看護学生さんなんですか！？香ちゃんのような白衣の天使をめざしてるんですね。がんばって下さい。しかしアンパ＠マンのびっくらたまごって、私が以前Ａさまからいただいたような、お風呂にいれるとキャラクターが出てくるヤツ？あれ、ＣＨのもできないかなぁ・・・。お風呂に入れると「もっこりー！」とか言ってりょうちゃんが飛び出してくるの。それをぶったたくハンマーも付いてて・・・。って、すごい凝りすぎやね・・・。（笑）

６８９　　葵さま　　まぁまぁ、元気をだして・・・。葵さまなど私にくらべたら、まだ青い青い・・・って笑ってくれない？ダメ？あまりにオヤジギャグか・・・。（反省）

６７２　　ぽん☆さま　　そうですねー。ＡＨでは、どうもりょうちゃんのほうが香ちゃんにメロメロのようなので（再び超うらやましい・・・。）ハチャメチャだったと言う香ちゃんに、随分りょうちゃんは振り回されたでしょうね・・・。そんなほほえましいシーンも見てみたいし、やはり二人がラブラブな所も見てみたい・・・。悲しくなっても、香ちゃんが幸せだったんだなぁー。と、思えるシーンが嬉しいんです私的には・・・・。りょうちゃんはＣＨの時より､老けて見えるのは、やはり設定的にＡＨでの年齢が高いからでは？と思うのですが、４０歳にはなってるよなー。と勝手に想像しています。

あー。こちらももうすぐ梅雨入りですねー。うっとおしい季節がやって来ます。

### [10693] yosiko	2003.06.05 THU 02:53:13　
	
こんばんは☆　ほんの数日来なかっただけなのに、早いですね～～回転グリグリ☆早くも話題に乗り遅れつつあるので・・う～んと・・・相変わらず、睡眠時間を削ってでも時間を確保しておるyosikoですが、ＣＨ→ＡＨ→ＣＨ→ＡＨという風に繰り返して読んでます。いっろんな読み方して楽しんでおります。友人に、卒論かけるのでは？と言われるくらい（笑）

★アニメ名セリフのハナシ←乗り遅れすぎ
残念でしたねえ。でも、インパクトのあるセリフとしたら、かなりの上位にくるのでは？ワタクシのＣＨ歴はアニメの再放送から入って、原作へと手をのばした、というタイプなんですが、アニメで”もっこり”って聞いた時、？！？！？！？！？という、かなりのインパクトがありましたよ・・・しばしの後、・・スバラシイ♪と思った記憶があります(^^)
ではでは、変なカキコニなりましたが。。。おやすみなさい♪

### [10692] 将人（元・魔神）	2003.06.05 THU 02:00:03　
	
こんばんは、将人（元・魔神）です。

＞[10683] 無言のパンダ様　2003.06.04 WED 08:53:18　
たまには僕もツッコミもさせて下さいよ→名セリフの事(笑)

＞[10684] Ｂｌｕｅ　Ｃａｔ様　2003.06.04 WED 10:54:27　
＞Ｚ！　左腕を頭の上に、右腕は顔の下に
僕も考えたＺは、それですよ。でもシェーを連想してしまうんですよね

＞[10685] ayako様　2003.06.04 WED 16:35:11　
台湾のCHのDVDって海賊版なんですね。よくオークションで見るのも
それなのかな？

＞[10686] ふじっちょ様　2003.06.04 WED 17:33:55　
たしかに北条司先生の作品って、細かいところに遊びのような事が
書いてありますね。CEの頃なんかは、新聞を読むシーンなんて
他の漫画だと「・・・」か「タイトルだけ変えての何かの新聞の
記事の貼り付け」が多いのに、北条司先生のコメントがギッシリ
入っていましたよね。
→そういうのを分っていて気付かないなんて注意力落ちたのかな？

＞[10688][10690] ぽん☆様　2003.06.04 WED 21:16:30　
とりあえず今までの時点では、AHには美樹さんは出ていませんよ。
CHの最後は夫婦になったんだから喫茶CEにいてそうなんですけどね。
北条司先生が何かの設定を変えてでも、美樹さんの登場しないかな？
って思っていますけど。
CHのようにファルコンを追いかけてきてから始まるのかな？
→AHの漫画の画面に実は出ないだけで、既に喫茶CEの影のオーナー
だったりして

ちょっとCHの最後の35巻とAHのコミックスの冴羽さんを比べて
みましたが、北条司先生の画風の違いもあるでしょうけど、

1冴羽さんの髪の毛が多少後退している？
2あごのラインが、スマートにとんがっていたのが、ちょっと
　角ばっていて、ほおにシワらしき物が時々でていてちょっと
　ほおが落ちたような感じ？
3、目尻とかもたまにアップの時にシワが描かれて出ているかな？

そのぐらいかな？

あ、今AH6巻を読み直して、若い冴羽さんと今の冴羽さんのよく似た
角度の顔のカット見つけました。
57話野良犬の告白のタイトルの次のページP42との病院を見てるのと
60話心の傷のタイトルがあるページP78の「MDが夢ちゃんの父親」
というのが、AHの回想シーンでの若い冴羽さんと、今の冴羽さんの
比較になるかなって思いました。

### [10691] kinshi	2003.06.05 THU 00:27:40　
	
こんばんは、たった今、ビデオ７本をぶっ続けでみ終わった所です。
目が、耳が、脳みそが　激疲れてます。

＞ふじっちょ様
なに、ドタバタしてるんだい？お子ちゃまの躾はキチンとしないとナ！。
覚悟は、出来ているようだね♪ムフフ＾０＾

＞無言のパンダ様
勝手に私を使っちゃぁ　イヤですよ。。。そうでなくても、
「ハリセン」使いたくて、ウズウズしてるのに・・・デヘッ！(^-~)

それでは、疲れたので、今夜はこのへんで・・・。

### [10690] ぽん☆	2003.06.04 WED 21:21:21　
	
追伸　後、AHでの遼は、ふけたなぁ～！とびっくりしてる
のですが、どの辺が違うのかよくわからないけど、ふけて
見えるのですが、若い時と髪形が違うのは判るのですが、
後はどこが違うのでしょうか？？？

### [10689] 葵	2003.06.04 WED 21:20:21　
	
　「サボるなよー」と、どこかのパンダに脅されちゃった…こわぁーいっ!!!(>_<；台風一過と思いきや、梅雨モードです。でもまだちょっと先…その前にお洗濯しましょ♪(o^-')b

＞千葉のマダムさま［６７１］
いやいや、凹んだのは某ＢＢＳのカキコにで…。(^^；でもウエストも凹んで欲しいわっ☆（←切望☆）

＞ＯＲＥＯさま［６７３］
お久しゅう！バスタイムでおくつろぎを♪

＞里奈さま［６７６］
余裕が出来ました？お仕事も大事、でも公式も大事…だよね？(^-^*)でも誰も淋しがってないような…？！（（ ( (ヽ(；^^)/

＞圭之助さま［６７７］
だんだんパンダ化してないか？心配じゃ☆(^^；テープに録音、我が家にビデオがない時期（ＣＨを)必死にやってました。いまだに宝物です…。（〃▽〃）

### [10688] ぽん☆	2003.06.04 WED 21:16:30　
	
ふじっちょさま＞ははは、違いますよ～香の事が好き
なんだけど、不器用？だからうまく言えなくて振り回され
てる・・って感じのストーリーが読みたいのですが☆
（じっれたい？というのか）CHの最後の巻の方では結構
そうゆう話多かった気もしますが♪

Ｂｌｕｅ　Ｃａｔさま＞お返事ありがとうございます☆
どんな風に仕事・・そうですよね、見てみたいですよね！
いつも伝言板の前で香がため息ついてるとか、そんなの
ばかりでしたよね（＾＾）＊CHの何巻か忘れたのですが
遼が筋トレしてるシーンがあって、それがすごくかっこ
よくて忘れられないです☆香も鍛えてたのかな？？

無言のパンダさま＞そうですよね♪けど、もっともっと
大人の愛を感じるような、そんな話もまたみたいです～！

将人さま＞お返事ありがとうございます。違う世界と
判っていても、つい同じに見てしまいますよね（；；）

ちなみに、あの、海坊主さんの恋人の美樹ちゃんは
どうなったかご存知ですか？（聞いてはいけない事
だったでしょうか（汗））


### [10687] きよ	2003.06.04 WED 19:43:27　
	
無言のパンダさま☆★
きよ手術室、たえましたよ；でも慣れたらグループの子たちと肉くいて～！！！って叫びましたｖ
今日もまたお勉強しないわたくし・・・。
やばやばですね；；

### [10686] ふじっちょ	2003.06.04 WED 17:33:55　
	
こんちわ？こんばんわ？この時間ってどっちが正しいの？（午後６時現在）
近所の本屋に未だに先週号のバンチが置いてある…買って無いので買うべきだろうか？（この本屋雑誌が売れないのが続くと取引止めちゃうんだよな…）

＞[677] 圭之助（けーの）さま
＞笑ってもた☆
フッ…狙いどおり（￣ー￣）さすが！わかっていらっしゃる♪(o^-')b

＞[682] 将人（元・魔神）さま
感想書くのに何回も読み直していたらたまたま気づきました(^_^）北条先生はそういう“遊び心”見たいな物が多いから自然と探しちゃうんですよね。２９巻のCAT'S EYEの時計とか…（今回の場合は遊びと言うより演出の方が正しいかな？）

＞[685] ayakoさま
台湾からの留学生がいるんですか？うちの学校、留学生おんのかな？ゲームの海賊版ですかーそういえばどっか海外に旅行に行った友達が安かったからって大量に買って帰ってきてたな…これって法律的にＯＫやったけ？(?_?)

＞[683] 偉大なる無言のパンダさま
スイマセン！！調子に乗りすぎていましたm(__)m
反省します！(ToT)許してください。お縄につきますんで…
でも、ｋｉｎｓｈｉさま使って“修正”しないで自分でしなさい。ガチャ(▼▼メ)(ＴoＴ｜｜)(▼▼メ)ピーポー×２

ガチャン！！||Φ|（|Ｔ|Д|Ｔ|;)|Φ||
ついにパンダさまに捕まりました(T_T)
今パンダさまから間接的（ｋｉｎｓｈｉさま）に“修正”を受けております
（＞＜；（☆◯(▼▼ ）ｏ　バキ！！
心入れ替え立派に成りたいと思う次第です…ハイ…

### [10685] ayako	2003.06.04 WED 16:35:11　
	
みなさん、こんにちは。
最近学校の同じゼミの台湾人の子と仲良くなりました。今日はいろいろな話を聞きました。ＡＨもＣＨも知っていて、やっぱり台湾で大人気だそうです。で、ＣＨのＤＶＤは全部海賊版が販売されているそうです。だから公式で発売になったわけではないようですね。台湾では海賊版が凄いみたいで、ゲームも普通日本で7000円の物が200円で買えるそうです。それに漫画も台湾の方が安いそうです。
ちょっとビックリしました。文化の違いかなあ。

### [10684] Ｂｌｕｅ　Ｃａｔ	2003.06.04 WED 10:54:27　
	
　こんにちは～。

＞[10672] ぽん☆さま
　わたしは、Ａ・Ｈで、Ｃ・Ｈでは見られなかったリョウと香のらぶらぶシーンが出てくるたびとっても嬉しくなってひたってしまうんですけど、でもやっぱり、“ケンカするほど仲がいい♪”二人の姿も見てみたいな～なんて思ってしまったりもするんですよね。らぶらぶばっかりだと物足りなく感じてしまうのって、わがままかな（笑）。あとは、リョウや香がどんなふうに“Ｃ・Ｈとしてのお仕事”をしてたのか、も見てみたいです☆

＞[10677] 圭之助さま
　ヤング○ン風に「ＸＹＺ」をするなら・・・やはり腕で表現するべきでしょう！
　てことで
　　　　Ｘ！　両腕を頭の上でクロス
　　　　Ｙ！　両腕を頭の上で開く
　　　　Ｚ！　左腕を頭の上に、右腕は顔の下に
　　　　　　　　　　　　　　　　　　　　　　　　　　なんてどうです？

### [10683] 無言のパンダ	2003.06.04 WED 08:53:18　
	
おはようございます☆

二日続けて朝からの登場で「朝型」と見せかけて、実は「夜型」パンダです♪朝から眠ーい！(-_-)zzz
※どうやら台風の上陸はまぬがれたようですね(^^)

＞千葉のＯＢＡ３さま（６７１）
そうですねぇ、おそらく連行されてるのは「ふ」のつく人。そして連行している片方の人物はｋｉｎｓｈｉさまだと思われますね～。（客観的予想）

＞ぽん☆さま（６７２）
はじめまして☆そうですねーリョウと香の微笑ましいエピソードをもっと見たいですよね♪(^.^)
※どこぞの「ワカゾー」がわけのわからんことを言っておるようですが気にしないでくださいね！

＞ＯＲＥＯさま（６７３）
はるかカナダからの愛情あふれるカキコ☆待ってました！ｖ(≧∇≦)ｖ 時間ができたらまたゆっくり来てね♪

＞ふじっちょ？さま（６７４）
根拠のない中傷はやめましょう。ファ＠タをこぼすくらいじゃ済まないことになっても知りませんよ～。↓
（＞＜　；)☆◯(▼▼ ）ｏ　バキ！！←（注：ｋｉｎｓｈｉさま）

＞里奈さま（６７６）
わお！「里奈ちんワールド」本格的復帰かーっ？！（￣□￣；）！！
・・・「淋しい」なんて誰か言ってたっけ～？(?_?)

＞圭之助さま（６７７）
「Ｘ・Ｙ・Ｚ」の実演☆ご苦労さまでした☆さあ！腹筋を鍛えてもう一度チャレンジよ♪(o^-')b

＞ａｙａｋｏさま（６７８・６８１）
そうなんですよー結構大きなレンタル店なのにＣＥがないんです！(T_T)なのでずっとＣＥがビデオ化（ＤＶＤ化）されてることを知らなかったです。
※私は今もＰＣの前にラジカセ置いて録音することあるぞ～！

＞きよさま（６７９）
私は昔、手術に立ち会って気絶する夢を見て看護学生になる道をあきらめました(^^ゞ
きよさまは香ちゃんのように優しい看護婦さん（最近は看護士っていうの？）を目指してがんばってね(o^∇^o)ノ　

＞将人さま（６８２）
＞「アニメ名セリフ」シティーハンター無かったですよ。
将人さままで静かにパンダを責めてる・・・！（￣□￣；）！！
ＸＹＺのポーズでねじれちゃえ～！(#▽皿▽）＝3
※↑とん＠るずの「もじ○じ君」だったら出来そう♪（笑）
「名セリフ」での「北斗の拳」の扱いにはかなり不満ですよね。

朝から長々すみませんでした！m(__)m

### [10682] 将人（元・魔神）	2003.06.04 WED 00:20:17　
	
こんばんは、将人（元・魔神）です。

＞[10636] ふじっちょ様　2003.05.30 FRI 22:50:23　
AHの最後のページは、普段は扉に来るタイトルで驚いたのか？
一輪だけ咲いた桜の花には気付きませんでした。
細かく見られていますね→僕がちゃんと見てなかったのかも(汗)(^^；

＞[10654] ぽん☆様　2003.06.01 SUN 00:34:51　
AH5巻の香さんと冴羽さんの出会いの話ですが、たぶんAH1巻の作者の
注意書きの部分で北条司先生が言われているように、
同じCHのキャラは出てるけど、AHは違うパラレルの世界なのかも
しれませんね。僕も、あれには戸惑いましたが・・・。

＞[10655][10664] 里奈様　2003.06.01 SUN 01:49:06　
お久しぶりです。水着の店長としてお忙しそうで大変ですね。
『ＸＹＺ』って、アメリカではそんな意味あったんですか？

＞[10657] たま様　2003.06.01 SUN 11:12:43　
公式HPではお久しぶりですね。AHの今回のシリーズの話はシリアスで
重たい話でしたけど、今までのたま様のコメントじゃないみたいですね

＞[10659] ぽぽ（Hyogo）様　2003.06.01 SUN 19:34:40　
アニメ版CHの槇村さんを声優・田中秀幸さんから名前を取ったという話
放送当時に、神谷明さんと伊倉一恵さんが答えられていたんですか？
そうなんだ～！そんなテレホンサービスがあったのすら知らなかったです

＞[10663] 無言のパンダ様　2003.06.02 MON 00:21:10　
「アニメ名セリフ」シティーハンター無かったですよ。
それでも神谷明さんの北斗＠拳は出ると思っていたのですが・・・
・・・上位なのに、漫画の絵が出ただけで飛ばされましたね。

うわちゃギャラクティカ・マグナムパンチでしたっけ？忘れいた！
車田＠美先生の漫画も読んでいたけどキッチリ覚えてなかったな～

＞[10668] ayako様　2003.06.02 MON 22:42:23　
CHじゃなくてキャッツアイの頃ですけど、僕も昔、学生の頃に
親父のビデオなんて触らせて貰えないので、ラジカセでTVの音声も
聞ける物をタイマー録音で、塾に行っている間に放送していた
アニメ版キャッツアイなどをカセットに録音していましたよ。
ただカセットテープも同じ1本を繰り返し使っていたから、
キャッツアイパート２最終回と同時期にやっていたウイング＠ンの
最終回だけが残っていました。
今は、そのカセットテープもどこに行ったのかな？

＞[10670] 無言のパンダ様　2003.06.03 TUE 08:03:40　
「YMCA」のように「XYZ」のジェスチャですか？
→[10677] 圭之助（けーの）様も言われていますが、

僕が想像したのは、Xは胸の前にクロスして人造人間キカ＠ダーの
ダブルチョップの構えで、Yはヤング＠ンですが、Ｚは・・・
・・・ダメだ！赤塚＠二夫先生のイヤミの「シェー！」の腕だけを
思ってしまいました。
２人いればゼンダマ＠の「Ｚ」のポーズが出来るのに・・・。

そういえば今、思い付いたけどベスト1＠0今回はアニメのセリフ
でしたけど、決めポーズや、決めの必殺技ってのが出ないかな？
→半分は特撮ヒーローになりそうですけど(汗)(^^；
　100tハンマーは入らないかな？

ＣＨで主役以外のキャラクター？香さんは冴羽さんと同じ主役扱いに
なるのかな？「カラス」＆「トンボ」を忘れてました。
僕は海坊主さんかな？最初は1回だけのと思っていたらドンドン
登場してレギュラーになり、別の漫画で花屋さんになってで、
AHでも存在感のある役ですからね。

＞[10675] 野上唯香さま　2003.06.03 TUE 17:44:42　
アニメのＤＶＤにユニホームなんかの特典があるんですね。
ＣＨなら何かな？コルトパイソン357マグナムかミニクーパー？
でもこれは一般の模型であるから100tハンマーとかかな？
ＣＨのフィギアでも、今ブームになっている技術で作ってくれたらな？
コンビニにでてる北斗＠拳の総集編のように、その回に出たキャラの
フィギアとか？
CHの完全フルカラー版出て欲しいですね。

＞[10676] 里奈様　2003.06.03 TUE 18:57:35　
最近来られてなくて、寂しかったですよ。余裕が出来たら復活ですか？

＞[10677] 圭之助（けーの）様　2003.06.03 TUE 19:01:52　
そのＸＹＺのＺの姿勢は、ちょっとキツイかも？

### [10681] ayako	2003.06.03 TUE 22:23:25　
	
圭之助さま
マイク録音はテレビと繋ぐコードが無かったから最初はテレビのスピーカーの前でずっとマイクを持ってやってました。でも疲れるので途中からマイクをスピーカーにセロテープで固定してやりました。（バカですよね。）
