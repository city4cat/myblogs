https://web.archive.org/web/20050313021048/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=476

### [9520] 里奈	2003.02.01 SAT 16:50:16　
	
あぁー 忙しい忙しい！
昨日オールで遊んでて一睡もしてない里奈ちん。
なのに今からそのまま仕事で深夜３時まで働かなきゃいけない！
睡眠不足で二日酔い並に頭いてぇー！（自業自得…）

今週の感想カキコしたいけど、タイムリミットが近づいてるんで また後で！
でも 一言だけ！里奈も葵様と同意見で、あの扉絵 遼と香でも見たかったなぁ～♪

☆マイコ様
香ちゃんイラストありがとぉぉぉーーっ！！
ＣＨ時代の香ちゃんだぁー！すごい！似てる！似てるってゆうか そのまんまだわネ！なんでＰＣであぁやって描けるの？？里奈はたぶん手描きじゃないと無理かなぁ…。マイコちゃんも里奈＆いちの様に負けずとイラストどんどん描いてきてるね♪もっとたくさん描いてね！いちの様は里奈に『宣戦布告！』とか言って燃えてるみたいだけど、あんなのほっといてマイペースで描こう（笑）里奈も香ちゃん描こう！描いたら勝手に送信させて頂きまぁ～っす♪

### [9519] 葵	2003.01.31 FRI 21:40:40　
	
　クリームつけて少しはよくなったけど、いまだに手荒れがひどいです。リョウちゃんの熱い両手で暖めてっ♪（←ただ今、妄想モード☆）

　感想です～♪北条せんせ、どこまでも引っ張りますねぇ～。早く阿香とリョウのＣＨを見せてくださいよ！北条せんせってば、きっと「クリームソーダのさくらんぼは最後派」でしょうね。実は私もだけど☆

　そんなことでなく！扉絵、素敵でした。欲を言えばＣＨ時代にリョウ＆香で見たい扉でした。親子の絆…たとえ血はつながらなくても、阿香はリョウの大事な娘。それを的確に表してますよね。リョウは阿香の「奥」にいる香の姿を見てる…そう感じました。そんなリョウの愛情に応えるかのように、殺人鬼でない、きちんとした（？）一人の人間として阿香は成長していってる。リョウぱーぱの喜びの声が聞こえて来そうです。だから優しい目になるんだろうな～。（うっとり♪）そして読者のブーイングにめげず（笑）金髪ヤローで、妙にしっかりもの信宏ともいいコンビを組んでいって欲しいです。ところで、海ちゃんに冴子さんはお元気かしら～？（^^;

＞たまさま［９５１０］
　キリンちゃんたらおドジさん☆動物園仲間のご冥福をお祈りいたします…。

＞無言のパンダさま［９５１１］
　中国は寒かろう…。雪と戯れるパンダは可愛いが、広島パンダは無理して遊びすぎて、風邪ひかないようにね♪

＞里奈さま［９５１２］　
　コスプレですか…似合いそう☆里奈さまってコスプレ顔だもんなぁ～。（どんな顔じゃい☆）マグナムの件、確認後にメールします♪

＞いちのさま［９５１４］
　え～？だって妙に売りこんでたから…。それにしても体調が悪いようですが…ひょっとして、つわり？！（^^;リョウちゃんのイラストの件、そんな体調悪い時に無理は言いませんからどうぞお大事にね。

### [9518] Ｂｌｕｅ　Ｃａｔ	2003.01.31 FRI 18:44:08　
	
　こんばんは～。　寒い日が続いて、ちょっとでも時間があると♪ね～こはコ○ツでまるく○る♪状態になってるＢｌｕｅ　Ｃａｔです。ネコだから寒さに弱いのさっ。それにしてもリョウってば、いつもコートの前はだけてて、よく平気だよなぁ、見てるこっちが寒くなるんですけど（ぶるぶる）。だって中、Ｔシャッツだよ！
　では、今週の感想です。
　扉絵の見つめあうリョウ＆阿香、ラストでリョウの手をぎゅって握った阿香、ぱーぱもずっと私のぱーぱだよね、って言ってるみたいで・・・なんか、いいかんじでした。
　陳さんの焼きいも、いいなぁ、わたしも食べたい（笑）リョウちゃ～ん、わたしと半分こしない？　あー焼きいもなんてずいぶん食べてないなぁ。でも、陳さん、もうすっかり情報屋さんですね。　リョウ、やっぱりちゃんと考えがあるんだ、どんなふうに解決するのか、楽しみです♪　カッコいいリョウが見たいな♪
　阿香と信宏、楽しそうだよね（笑）　お店にちゃ～んと代金置いてくとこがえらいっ、って思いました（笑）　でも、信宏、まだ髪の色そのままなんだ（苦笑）
　来週はＡ・Ｈが表紙だそうで、楽しみです☆

＞[9489]千葉のＯＢＡ３さま
　そういえばＣ・Ｈの香も、さゆりお姉ちゃんの話のとき似たようなことしてましたよね（笑）香の場合は電源が入ってなかったんだけど。壊す前に気が付いてよかったですね（笑）　でも、Ａ・Ｈの香はあのパソコンをはたして使いこなせていたんだろうか？

### [9517] 野上唯香	2003.01.31 FRI 16:34:37　
	
予告！！
関西地区の皆様へ。２／２のＣＨ３再放送の時間はＡ．Ｍ３：２９～３：５９です。

### [9516] 野上唯香	2003.01.31 FRI 16:31:03　
	
マイコ様
「僕の生きる道」は面白いですか？私はジャニーズのドラマだったら嵐・桜井翔の「よい子の味方」＆二宮和也の「熱烈的中華飯店」です。でも一番好きなのは藤木直人＆上戸彩の今夜１０：００～の「高校教師」です(^-^)この番組は１話からビデオに撮っています。実は去年（高１の時）槇村秀幸似の国語の講師（古典担当）だった人に恋をしてしまい、この人がまた月末から代行として私のクラスを受け持つことになりました。今ではこの先生と高校教師のような恋愛がしたいです。

里奈様
コスプレしたんですか！？良いなあ～(^_^)/~そのプリクラ欲しいなあ・・・。今度春休みに里奈さんと会ってプリクラ撮るとしたらコスプレしたいなぁ・・・。私の友達に里奈さんの写真見せるとみんな、「お姉さん可愛い♪」って言いますよ。時差ボケに負けずお仕事頑張って下さい。
中学時代の修学旅行で東京に来たとき渋谷１０９には行ったんですか？（しつこいけど）東京に遊びに来たらディズニーランドにも行きませんか？（気が早い・・・。）

### [9515] TAKESHI	2003.01.31 FRI 12:21:41　
	
こんちは～。

＞[9512]里奈様
ＦＦⅩ－２はかなり期待大のようですね。僕は最近アクションばっかやってますよ。今はＰＳ２の「ＳＰＩＤＥＲ－ＭＡＮ」と「００７：ナイトファイア」がすごく欲しいです。

＞[9510]たま様
石川はうまいラーメン屋がまったくないです。以前、東京に行ったとき、ＴＶでも紹介してたラーメンを食べたんですが、それがものすんごくまずかったです。まだカップめんのほうがうまかった・・・。あぁ、うまいラーメンが食べたい（ぐすん）
タモリさんは似合いますね。ただ、世にも～の時は少し怖いです（笑）

＞[9503]いちの様
平井堅は顔濃いですよね（笑）掘りが深いというか・・・南国育ち！？外人は何やってもカッコいいっすね。そいえば最近映画とか見てますか？

### [9514] いちの	2003.01.31 FRI 12:01:10　
	
こんにちは。
なんだか今日はスコブル体調が悪いです・・・。最近ろくな食べ物食ってないからかな・・・？昨日も結局コンビニでパンとオニギリ買って食べてたし・・・あ～、頭痛てぇ～。胸がムカムカする～。首も痛えぇ～。肩こりか？
早く実家に帰りたい・・・（涙）。

＞[9507] マイコ様
風邪治ったようで、よかったですね！！
でも、今度は腰ですか？なんでまたそんなに若いのに腰なんて痛めてしまったんでしょうね？寝すぎですかね？
少し動けば良くなるかもしれませんね。お大事にしてください。
そして、マイコ様の新作を是非見せてください！！明日までならこっち（東京）にいると思うので、すぐ送れるようでしたら、こっちに送ってください。２月に入ってしまうとたぶん実家にいますので、実家からメールします。

＞[9508] 葵様
おぉっと？どこからそういう話しになったのでしょうか？
通販のバイトなんてやってませんよ（汗）。つーか、ぶっちゃけバイトなんてやったことがねー・・・。日給のバイトなら１回だけあるけど。
まあ、マイコ様も頑張っていることだし、描きますか、リョウちゃん。フルカラーってーのが難しいですが、頑張ってみますよ。多分、実家に帰ってからだと思いますが・・・（汗）。

＞[9510] たま様
キリンも転倒して頭を打つことってあるんですね（汗）。ご冥福をお祈りいたします。
私が実家に帰っても心配しないで～。せっかくたま様が復活したのに、私が来ないわけないでしょう？
ま～、いざとなったら里奈様がいるしね。暇はないと思うよ。
病み上がりのせいか、たま様のキャラが少し違うような気がするのは気のせいですか？まだ本調子じゃないとか？
実は夫が変わりに打ってるとか？

＞[9512] 里奈様
おえっ。吐きそう（いろんな意味で）↑体調悪いから。
もう、許してください～（泣）。実家にこれ以上変なもの送り付けないで下さい～（泣）。＝（⊃ДT）
まあ、泣き寝入りすると、調子に乗るのでやりませんが、気持ち悪い・・・体調いとわろし。『あし』くらいのレベルの悪さだね（古典用語）。
これから私はイラストを描くよ。（宣戦布告）
描くったら描くよ。

### [9513] 里奈	2003.01.31 FRI 02:33:06　
	
☆９５０９　Ｄｅａｒ 謝麟
Did it already return to the hometown?
A family is good! （＾ヮ＾）
Please value time with a family.♪

The new story of "Angel Heart" will come out tomorrow.
If possible, I will want to explain a story to you.
Please wait to pleasure!
But!! I will not surely be well taught to you.
However!!! it tries hard! （;￣□￣）/

### [9512] 里奈	2003.01.31 FRI 02:13:52　
	
きゃいぃぃーん♪
皆 聞いて聞いて！里奈 今日友達と『コスプレ』しちゃった！
コ･ス･プ･レッ♪　　ヽ（≧ヮ≦）ﾉ
スッゴイ エロい ウエイトレスのカッコしてプリクラ撮ったの！
フリル満開で真っ赤なサテンの超ミニ！お尻見えちゃってる！
コスプレ初体験の里奈は興奮しちゃって悩殺ポーズ撮りまくってたら、勢い余って（？）胸ギリギリまで出しちゃったよ！（おいおい！）
なに！？なんなの！？この楽しさ！
　　　　　　　　　　　　　　　　　（滋賀県 リナ ２３歳）

☆９５１０　たま様
福岡動物園のキリンちゃんが亡くなっちゃったの！？
え…？頭 打ったから…？ホントにドジかも…。
この前ＴＶで見たんだけど、野生のキリンがイキナリ突進して走って行ったと思ったら、思いっきり木の枝に首引っ掛けて自爆的強烈ラリアートくらってたらしい…。世界のキリンさん達、自分の首の長さくらい把握しましょう…（汗）
『ヤマブキちゃん』の御冥福をお祈り致します。

☆９５０８　葵様
あ…あったかいＰＣ！？なんか怖い！常に不安！！
そういえば、ドコモの携帯で、急に１００℃まで温度が上がって激熱になる現象が最近よく起きてるらしいです。最新携帯って怖いですなぁ～。でもバッテリー切れた後、冷やすと回復するの知ってました？

３５７マグナムの件、もう一回確認してメール下さい！
聖母（あっ。ホントだ、歳暮って出た）葵様！お願ぁ～い！

☆９５０７　マイコ様
ハイ！ハイハイハァ～イ！！　（;ﾟヮﾟ）/゛
マイコ君！香ちゃんイラストを速やかに私に送信しなさい！
今ならまだ間に合う！おふくろさんが泣いているぞ！？

☆９５０４　野上唯香様
おうよ！日本にいながら時差ボケ中の里奈ちんです！
んでもって、とうとう頭マヒった！コスプレ最高ぉー！（笑）
里奈ちん まだまだ １０代で通る勢いな変身っぷり！

☆９５０３　いちの様
こらこら！キッチンをキレイにして料理したくなって、外食かよっ！？
そんなんじゃ体に良くありません！ダメ！全然ダメ！ダメ人間！！
（自分の事を棚に上げて板張りつけて釘打って密封して言ってみた）

な…なんじゃぁー！？この不等式はぁぁぁーっ！？
里奈ゼロにまで負けてんの！？手も足も顔も胸も出せんのか！？
（どこまで出す気だ…）
でもホントは里奈のコスプレ見たくて興奮して酸欠状態なんでしょ？
しかも里奈同様のチョコ好きと来たか！そんなあなたには里奈のセクシィ～ウエイトレス型の立体チョコを『実家に！』送っときます。

☆９５０２　ＴＡＫＥＳＨＩ様
マイコちゃん、風邪治らないままイラスト完成させたようですね！
その血(？)と涙の結晶を、里奈が余すことなく見てさしあげよう！
妹よ…我が妹よぉー！（←末っ子）

里奈は相変わらずゲーム好き♪でもＦＦばっかだよ。
Ⅹ-２が待ちきれなくて昔のＦＦやってます☆
里奈ママもはまってるから、ＰＳ２もソフトも２つづつ買おうかと二人きりの家族会議が夜な夜な行われております…。

☆９５０１　千葉のＯＢＡ３様
東京には中学の修学旅行でしか行ったことがない里奈ちんは、新宿には遼ちゃんがいると信じきっています。同一人物じゃなくて、そうゆう男はいると思ってる。そして香ちゃんも。もしこれが海外だったら…いるね！確実にいる！（あぁ～また根拠の無いことを言うぅ～…汗）

☆９５００　ＯＲＥＯ様
おぉっ！気がつけば、カキコの数がもう９５００に！
もう１００００まで半分来てるじゃん！早っ！
イラスト描かなきゃぁー！（￣□￣;）頑張れ里奈！

そっか★大学だから遅刻してもうるさく言われないんだよね。
里奈は大学の授業は出たり出なかったりでサボり常習犯だった。出席取る先生も少なかったし。テストなんて、教科書や資料持ち込みＯＫけっこう多かったし楽だったのよね。写すだけ（笑）
でも、今になって思う。勉強もっとしとけば良かった！って。
人間ってなんで気付くの遅いんだろうなぁ～。

### [9511] 無言のパンダ	2003.01.31 FRI 02:10:13　
	
こんばんわ★

キリン・・・昔付き合ってた彼氏の目はキリンに似ていたっけ・・・（感傷に浸るパンダの冬・最低気温０度であった）

＞将人さま（９４９６）
次にそこへ買い物に行ったら、苦情入れの箱にひと言書いて入れとこうかな？ちなみに今日そこで「ヤルギンＺ」なるもの（チョコ）まであるのを発見。ここはアダルトショップか～！？(ｰｰ;)

＞里奈さま（９４９８）
「しいたげられた私にしいたけ海苔～」は今も「ごはんで○よ（海苔の佃煮）」などで有名な○屋の、しいたけ海苔のＣＭで三木の○平氏（故人）が言ってたフレーズなんですよ(^_^;)当時これを聞いた私はすっごくウケちゃって、それからずぅっと頭から離れないのございます～♪(^^ゞだって「しいたげられた私に」「しいたけ海苔」だよ！！（まだ大ウケ）

＞ＯＲＥＯさま（９４９９）
昨日といい今日といい「学校運」悪いですねー！(>_<)ＯＲＥＯさまこそ、教室は寒くないのかな？気をつけてね(^_^;)例のアレ、話のネタにって買ったりしたら奴らの思うツボですぜ！（奴らって・・・？）他のものと一緒にって「ヤルギンＺ」とペアで～？そりゃかなりヤバイよ。このあいだはそこのレジに同じマンションの人がいて明るく挨拶されちゃったんだから！見てるだけで「変態扱い」かもよ（冷や汗＆ひきつった笑い）

＞千葉のＯＢＡ３さま（９５０１）
ミニクーパーチョコはオススメですよ♪運良く見つけたら即買いです！雪は広島市内や北部では、かなり降ったみたいですが、うちのほうではチラホラまるでホコリのように舞っていただけでしたー(~_~;)中国四川省はかなり都会のようで・・・（ちゃう！ちゃう！）

＞マイコさま（９５０７）
香ちゃん描けたの～？ぜひぜひ見せて♪
マイコさまは手作りチョコ派なんだって？エライなぁ～☆私はそんなの作ったことないよ(^^ゞ

＞葵さま（９５０８）
聖母・葵さま☆どうにかＰＣとデジカメを上手くダマして、仲良くさせてちょ♪大丈夫！いつものように「オドシ」をかければ！(^^)v・・・あ、メカには通じないかな？「オミソ直前←（これネ^_^;）パワー」は☆でも気長に待ってるよん♪(^o^)丿

＞たまさま（９５１０）
「どやさ、どやさ」の謎は解けた・・・！？う～ん、でも意味わかんなーい(>_<)でもさすが、お笑い通☆グラサン似合う人と言えばタモさん？！くやしいけど、これ以上似合う人が思い浮かばない！(･･;)

明日はバン金！うれしいな♪ではまた(^_^)/~

### [9510] たま	2003.01.30 THU 23:28:01　
	
うわぁ～ん。
今日、福岡動物園のキリン（雄）が亡くなりました。（涙）
ちなみに死因は転んで頭を打ったんだって。チョットどじな奴。
「ヤマブキぃーーーー」永遠に・・・。

＞無言のパンダ様　【9495】
「どやさ、どやさ・・・」は今くる○姉さんのモノマネよ。中川家（弟）さんがやってるんだぞ。
どうだ！たまのカキコにはちゃぁ～んと根拠があるのさっ！
どこぞの「しいたげられた私にしいたけ海苔」っとは違うんだ！

＞将人（元・魔神）様【9496】
「ポピ○」って「ホビー」からきてたの？なるほどねっ。
でも「車にポピ○」は違うよねっ（笑）

＞いちの様　【9497】
また実家に帰るの？この前帰ったばかりでしょぅ。
実家に帰ったら、あまりカキコしてくれなくなるじゃん。
せっかくたまサン復帰したのに･･･。たまサン寂しい。（可愛い女を演出）

＞里奈さま　【9498】
だねぇ～。香ちゃんがＣＨ３のＯＰでヘリに追われてるのって、メッチャ些細な理由なのかもねぇ～。
「ＣＨのパートナーってだけの理由」ありえるね。

＞千葉のOBA3様　【9501】
微妙ぉーー。天然と呆けは紙一重だわさ。
今のところ６：４で天然の勝ち？かな。
「どやさ・・・」は緻密な計算と確実な根拠の基に成り立っているのだよ。
分かったかね？！チミ達！ぬわっはっはっ！

＞TAKESHI様　【9502】
本場の豚骨はおいしいよぉ～。
グラサンの似合う人・・・・どうしてもタモさんしか浮かばない。

### [9509] 謝麟	2003.01.30 THU 22:59:34　
	
to dear里奈
yes! I have come back my hometown,It's so happy to see my family
I'm feeling good!It's very difficult to surf internet in my
hometown,but I'll try my best to contact with you.
I'm very like the <city hunter>.and all the comic of mr.hojo
some short story of mr.hojo's it's beautiful too.
I wish I can do like 西九条紗羅,to talk with nature.
I like plant, and like green.

### [9508] 葵	2003.01.30 THU 21:34:22　
	
　風邪が治ってルンルンしてたら手が荒れてた。キーボードって冷たいんだもん。暖かいのって、誰か開発してくれないかなぁ～。(^^；

＞無言のパンダさま［９４９５］
　いえいえ、素敵なカレンダーを一人で味わおうなんて思っちゃいませんよ。一応、聞く前に（非売品がもらえるとは思ってなかったので）デジカメで撮って来たんですよ。でもＰＣ退院してからいろいろあったでしょ？デジカメ使用についてのインストールを忘れてたんです☆しかもやってみたら上手くいかなくて…。コピーして郵送しようかとも思ったんだけど、かなりデカイし…どうしましょ。気長にＰＣとデジカメの相性を見守ってやって下さい。あ、それからいつもながら、ヒトコト（←どれかはわかるでしょ？！）よけいですよ！(>_<；

＞いちのさま［９４９７］
　いちのさまってば、キチンとした大学生かと思ったら通販の売り子さんなんてバイトやってたの？ちゃんとお勉強しなくちゃダメだよぉ～！バツとしてリョウちゃんイラスト１枚ね♪もちろんフルカラーでっ♪

＞里奈さま［９４９８］
　聖母・葵です。（←今「せいぼ」って入れたら「歳暮」が出て爆笑しちゃった☆）３５７マグナム、里奈さまがホントに欲しいなら送りますけどぉ…確かに安すぎるよね。「０」一個見逃したかしら？！後日、もう一度確かめてきますね～。（^^；

　明日は金曜、「バンチ」の日♪阿香とリョウの大活躍を見せてくれ～っ！！ではまた解禁で…♪（^▽^)/~

### [9507] マイコ	2003.01.30 THU 20:18:39　
	
皆さんこんばんわぁ★おかげさまで風邪も治りました。が、今度は腰が痛くて･･･。友達に言ったら「ババくさいよっ！」なんてこと言われてしまいました。(汗)

風邪を心配してくださった皆様、ありがとうございます！！
マイコは心から感謝しております！！

やっと香ちゃんの絵ができました～っっ！！見たいという人いませんか？？いるといいけどぉ･･･。

レスです♪

野上唯香様
初めまして～！ドラマですか？？私が見てるのは少ないのですが、草なぎ剛さんが出てる「僕の生きる道」と松島奈々子さんが出てる「美女か野獣」です★☆★野上唯香様は、どんなドラマを見てるんですか？
関西地区でＣＨ３やるんですか！？いいな～。石川県にもやってほしいっ･･･。

無言のパンダ様
「モッコリン」、探してみましたがありませんでした。って
18禁なのね～。手にしませんです･･･。

### [9506] ぽぽ（Hyogo）	2003.01.30 THU 18:09:37　
	
### [9484] 将人（元・魔神）様
そうみたいですね。でもCS放送加入してないので見れずじまいでした。残念無念･･･(｡･＞_＜･｡)
地上波で放送された時(10年近く？以上？前)ビデオに録画したのですが紛失してしまい、気づいた時はあとの祭り～～。
もういっぺん探してみよっかな(^^;)でも見つかってもテープ腐ってたりして･･･(どんな所に置いとんのじゃ)。

もう一度ゴールデンタイムの枠でCHを見てみたいですね。

### [9505] 野上唯香	2003.01.30 THU 16:12:22　
	
予告！！
関西地区の皆様！今週土曜日からスタートのＣＨ３の第一話再放送の時間はＡ．Ｍ２：５８～です。お見逃し無く(^o^)

### [9504] 野上唯香	2003.01.30 THU 16:10:19　
	
将人様
今日は痴漢に遭いませんでした。将人様は一昨日のＰ．Ｍ１０：３０～ＣＳ放送Ｃｈ．２７６での日替わりＣＨ２「さらばハードボイルドシティ（後編）」見ましたか？私にとっては２回目でしたけどまたビデオに撮りました（笑）あれはキスシーンも良いけれど、最後のナンパ＆１０１ｔハンマーも好きです。一番好きなのはＢＧＭで流れている「ＬＯＮＥＬＹ　ＬＵＬＬＡＢＵＹ」です。この曲のタイトルを日本語訳すると、「寂しい子守歌」です。ＣＨで泣ける話は格好いい系だと、「さらばハードボイルドシティ」で悲しい系だと「復讐の美女！遼に哀しみのブルースを」＆「硝煙の行方・・・シティーハンター暁に死す！」です。将人様はじめ皆さんのＣＨで泣ける話を教えて下さい。

里奈様
仕事の話を聞いているとどうやら時差ぼけしているようですね(-_-;)なんだか修学旅行中の私と同じ状態ですね。私はここ何日が体調不良で食欲がなくて吐き気がして２ｋｇ痩せてしまいました。この前友達に「こもれ陽の下で」１巻を見せてあげました。西九条隼人さんが教室のドアを壊したところ、生徒達が転校生・紗羅の顔を想像しているところ、大城先生のセーラー服のコスプレ写真、隼人さんが紗羅ちゃんと達也に髭を剃られてカツラをつけようかと打ち合わせしているところで爆笑でした★

ＯＲＥＯ様
感想ありがとうございます。また新作を考え中です。今回屋上に吊される場面は「簀巻き姿」ではなく、「褌（ふんどし）姿」ですよ（笑）

### [9503] いちの	2003.01.30 THU 15:17:13　
	
こんにちは。
今日も家のお掃除と旅立ちの準備で１日が終わりそうです・・・。しかも、気付けば、ここ数日（渋谷行った次の日から）ろくな食べ物を口にしていない（汗）。
キッチンをキレイにしたら料理したくなくなっちゃった（エヘッ）。
・・・なんか食ってこよー・・・。

＞[9498] 里奈様
あぶりだし止めとこうかな・・・（汗）。
あんまり興味ないや。ぶっちゃけ　０＞里奈様　くらいの不等式が成立しそうなくらいだ（笑）
バレンタインよりも節分のほうがいいなんて言っている純和風もどき娘は別として、やっぱりバレンタインは（恋人、又は、好きな人がいる人にとっては）大切な日なんでしょうね。（よって、私には関係ない）
ふん=3だ！！いいんだ、いいんだ。所詮僕なんていい人止まりさ！！（いや、そんなこと言われたことないけどね）
ちなみに、私もチョコ大好き派です。１度食べ始めると止まらなくなります。お買い得用の大きい袋に入った奴とかペロッと平らげてしまいます☆

＞[9499] OREO様
マジで一生使い切れないくらい稼いでみたいですよね。
自分の絵が売れて儲かることが出来ればこの上ない幸せなんですけど、今のような不景気ではそれも叶わぬ夢かもしれません・・・。でも、頑張りますけどね！！
ＣＨのＤＶＤも気になるのですが、カラー版のお値段も気になりますよね（汗）。いったい全巻でおいくらになるのだか・・・学生にも手が出せる範囲だといいのですが。

＞[9501] 千葉のOBA3様
あ！そうでした。すっかり忘れてたよ（アハッ）
笑って誤魔化せ♪
マンションか・・・デザイナーズマンションとか住んでみたいですよね。北条先生はどんな御家に住んでいらっしゃるのでしょうかね？先生の作業場を見てみたい☆
はい、マンション購入から上手く話しを逸らせたところで、絵が描けたらお知らせしますね。たぶん、この春休み中は絵を描こうと思っています。バイトするとしたら描けないかもしれないけど・・・。

＞[9502] TAKESHI様
そちらはそんなに雪が降っているんですか！？
こっち（東京の真ん中あたり）も寒いですけど、雪よりは全然マシですね（汗）。受験生も雪だと大変そうですね。
外人のグラサン姿はスゴクカッコイイですよね。なんつーか、骨格とマッチしてます。日本人の顔でグラサンを架けてもちょっとね・・・。キャップの帽子とかも似合いますよね。羨ましいです、ホントに。丸顔＆鼻ペちゃな私としては、平井ケンになりたいくらいです（笑）。

### [9502] TAKESHI	2003.01.30 THU 14:04:28　
	
こんにちは～・・・つ、疲れた。今日は明日からうちの高校が入試のため、授業１限だけで終わりました。昨日は雪がめっちゃ積もって電車は遅れたし、さらに踏み切りで脱輪事故がおきて、いつもより４０分も遅れました。しかも猛吹雪で帰りの方面の電車がまったく動かないということで家に帰れるか心配で（汗）学校は雪のため早めに終わったんですが、やっぱり電車は動かない！！仕方なく金沢駅までいって電車が来るのを待ってました。結果的に帰れたんですが、うちに着いたのは七時、もちろん電車はすし詰め状態。ほんとに大変でした。

＞[9487]里奈様
妹はまだ少し風邪っぽいようです。なんか腰が痛いっていってます。そういえば、里奈さんは結構ゲームするんですよね？アクションものはやらないんですか？

＞[9494]たま様
福岡も雪ですか！？雪はちらちら降るのは綺麗でいいですけど、積もると困りますね（汗）寒いときは、やはりラーメンに限りますね。いちど、本場のとんこつラーメンを食べてみたい・・・。

＞[9497]いちの様
またまた！いちのさんの方がかっこいいって！！自信持っていえますよ。僕なんか青二才のガキですから。グラサンもお似合いですよ！しかし、ハリウッドスターはサングラスが似合いますね。「マトリックス」のキアヌ・リーブスといい、「メンインブラック」のコンビといい、「ブレイド」のウエズリー・スナイプスといい、めっちゃきまってますよ。特に「ターミネーター２」のシュワちゃん！！最高にハードボイルドです。もちろん、海ちゃんも最高です！！


### [9501] 千葉のOBA3	2003.01.30 THU 08:47:00　
	
おはようございます。ホント寒いですねーーー！昨日は雪の降った地域も随分あったそうで（普段降らないような所で。）今日も千葉県寒いぞ！

９４９１　　将人さま　　昨日は東大阪の姉が「気温が一度しかない！寒い！！」ってメール送ってきました。寒いですねー！　　私はメカ全般がだめなようで、先日も仕事先でお店の人が私に、商品の数を確認する機械があるんですが、それを使わせようと説明していて、途中で断念してました・・・。（ワハハ、賢明な選択だよ。）

９４９４　　たまさま　　いや、ムチサバキって言うから、私が里奈さまみたいに、電気ムチ使うのかと思ったら､無知・・・。無知なのね・・・？「天然」のうちはカワイイけど、カンペキに呆けになりつつありチトコワイかも・・・。んでもって「無知」が「無恥」・・・これもすでにそうですーーー！！（年齢がものを言うぜ！）

９４９５　　無言のパンダさま　　私「どやさ」って「ウンヴァラヴァー」同様の呪いのまじないかと思ってたけど・・・？？？あーーー、そうだ、「もっこりん」チョコよりもミニクーパーチョコを捜したいんですよね。でも、「もっこりん」は、怖いもの見たさの魅力があります！昨日パンダさまお住まいの地域は雪でしたかー？（中国四川省・・・違う！中国地方！）

９４９７　　いちのさま　　だから・・、夢の印税生活して。たまさまにマンション、私に別荘を買って！！ＣＨ関連のイラストじゃなくても、いちのさまの自分の個性のイラストもホントいいよーーん！！（プレッシャーをかける）

９４９８　　里奈さま　　ゆで卵痛かった？これからの季節豆もチョコも食いまくりじゃ！そうですねー、北条先生の作品て、現実ばなれのようで、そのキャラクターがすごく身近に感じることができる・・・。それってどの作品にもあるようで・・・。ホントに新宿に行けばりょうちゃんがいる・・・。っていうカンジがしちゃう・・・。そこがいいですよねー☆しかし、いちのっち、実家に帰ったら、里奈さまの「白いハガキ」に火をつけるって？

９５００　　ＯＲＥＯさま　　スギ花粉て、日本でも北海道とか、南九州ってないらしいんですよね・・・。違うかな？？春は待ち遠しい。（霞ヶ浦に釣りに行きたい！）でも、花粉はイヤ。板ばさみなのね・・・。

今日ってバン木曜日ね。実は先週（子供インフルエンザのため）先々週（ミョーに疲れたため）バン木できなかたので、今日は行くぞーーー！！
