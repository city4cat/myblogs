https://web.archive.org/web/20011102120452/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=134

### [2680] ゆうこ	2001.09.03 MON 00:33:15　
	
みなさまこんばんは！
初めてカキコさせていただきます、ゆうこと申します。

わたしは、かれこれもう１０年来のＣＨ信奉者です。
最近も、ドラマティックマスター聴きながら、
また３５巻分一気読みして、笑いまくり、泣きまくり
しておりました。本当に大好きです。（*＾－＾*）

でも、どうしてもＡＨが読めてません。
北条先生のお話は、全部拝見させていただくことにしているのに、どうしても読めなかった。
バンチを買う前、友人から香の死を聞いてしまったから。
受け入れられなくて、どうしても手が出せませんでした。

でも、読んでみようかな。
みなさんの意見を拝見し、そう思いました。

ここへ来たきっかけは、ＣＨのＤＶＤを捜していて。
（絶対買う！　出て欲しいっ！！）　こんな素敵なＨＰが
あるなんて！　感激してしまい、早速カキコです。
また遊びに来ます。

これからもよろしくお願いします！！

### [2679] なお	2001.09.02 SUN 23:20:20　
	
自分は傍観専門（笑）だったんで，あまり書き込みはしてませんでした。
今日は一つ，言いことがあります

ここの掲示板は素晴らしいです。

はっきり言って最高のマナーです。
中傷する人もいないし，みんなレスつけてくれるし，Ｃ・Ｈファンの人は温かい人が多くてすごく嬉しいです。

普通２，３人くらいは毒舌な人がいるものですが，本当に感動しました。

実は，文打つのそんなに上手くないんです。
こんな自分ですが，これからちょくちょく書き込みしていいですか？

### [2678] おっさん（岐阜県：オオバか）	2001.09.02 SUN 23:10:04　
	
何度もすんまへん。
さっきの野猿の曲ですが、おもいっきり間違えてました。曲名は「star」で正式なユニット名はyaen front 4men feat.sakiです。
すみませんでした。

### [2677] sweeper（Good　evening！）	2001.09.02 SUN 23:07:04　
	
こんばんは。また来ました。

＞ありあり様。[２６６９]
レス、ありがとうございます。
りょうの目。回想シーンと現実で描き分けていたとしたら、北条先生のその描写の細かさは拍手モノですね。私は、よく香に見せていた優しい目が好きです。香に会ってから、こういう穏やかで優しい表情をするようになったのかなと思うと、「香がいない」という事実が哀しいです。

[ＣＨアニメのこと。]
原作がもとになっている話より、オリジナルの話が好きという方たちが多いような気がします。
私はあまり見ていないのでよくわからないのですが、クリスマスネタけっこうありましたよね。知っている限りで２回だと思うのですが・・・。

[りょうと香のこと]
皆さんのカキコを読んでいると、改めて「香はいない」ということに気付かされます。そして、２人のことでセンチメンタルになっている自分がここにいます。
りょうは２話のグラスハートの夢の中だけでしか涙を見せてはいないけれど、香の死を受け入れるまでかなり時間がかかったのではと思います。りょうは香の前に槙村兄を亡くしているから、「愛する人や大切な人を亡くすこと」の辛さや痛みを知っているんですよね。香が亡くなったあとも、おちゃらけとポーカーフェイスでふるまっていましたが、自分を見失わないようにしているためのような気がしてならないです。香の心臓が移植された相手がグラスハートだと知ったあとでも、りょうにとって香は「たった１人の愛する女」なんですよね。香を亡くしたことで１番辛いのはりょうですが、冴子と海坊主もりょうと同じ位辛いと思います。２人を１番長く見守っていたのは、この２人なのですから。
時折、香のことで見せるりょうの目が悲しみに満ちていて辛かったです。
ＣＨを読み返すたびに思ってしまいます。２人はものすごく奥手でプラトニックな関係だったけど、とても幸せだったんだなと。
ケンカばっかりしてたけど、(時々、キャッツアイをメチャクチャにしてましたが。)それだけ仲が良かったんだなと今更ながら思いました。ただ、お互いを気遣うあまりなかなか素直になれなかっただけなのかなとそう思うと切なかったです。

長々失礼しました。
それでは。

### [2676] Coo	2001.09.02 SUN 23:03:19　
	
こんばんは、Cooです。

［クリスマスにウエディングドレスを］
サブリミナルのお話が出ているので、私もちょっこっと
参加を・・・
事件で問題になっていた当時、私は妹と二人で、
なんとか問題のシーンを探そうと延々とビデオのコマ送り
を行い、確認を試みてたおバカさんでした。
にもかかわらず、問題となっていた麻○は発見できなかった
んです。でも、その事件とは関係ないと思われる画像で
サブリミナルだと思われるモノは見つけましたよ。
リョウちゃんのパンチシーン（もしかしたら香ちゃんの
ハンマーだったかも・・・）だったと思われます。
うろ覚えなんで、違ったらゴメンナサイ。

＝しゃる様［２６６８］＝
カレンダーの事、教えて下さりましてありがとう
ございました。早速見てまいりました。
ぜひぜひ欲しい！！

### [2675] ワタル／Wataru（宮城県）	2001.09.02 SUN 23:00:45　
	
【サブリミナル効果】
「麻＠しょ＠こう」の写真だったと思います。漢字忘れた…
あと，「伝染るん＠す」のカワ＠ソくんのもあったような。

【subliminal effects】
It was a photo of Sho@o Asa@ara.
Another one was a picture of Kawa@so-kun in Utsurun@esu.(comic)

### [2674] おっさん（岐阜県：うい～す！！）	2001.09.02 SUN 22:46:33　
	
度々、失礼します。
れい様、結構タメ多いっすよ。ここに来てる人は中学生（確かキャサリン様。間違えてたらごめんさい）から３０代の方々までよりすぐりですよ。もち、皆いい人ばっかりなんで！！タウンハンター様も私とタメです。
こんなこと書いちゃってすんません。おっさんでした。ごめんなさい！！

### [2673] おっさん（岐阜県：有難うごぜ～ます。）	2001.09.02 SUN 22:33:38　
	
毎度！本日誕生日を迎えたおっさんです。皆さんせ～のおっは～～！！
sweeper様・けいちん・リョウちん・べりー様・さえむらはん有難うごぜ～ます。何とか誕生日を迎えました＾－＾；。
最近、音楽のことが出てるじゃないっすか。私もよく聴いてるんですけど、野猿の「ster」って曲（正式なユニット名忘れました。大汗）気に入ってるんですけど、何かりょうと香の気持ちに近い感じの曲ですね。私が気に入ってるだけなんで・・・（自爆）。
ＣＨのアニメ、またこの地方でも再放送やらないですかね。何かまたみてみていっす。全部の話が好きですけど、私のお気に入りは「さらばハードボイルドシティ」の特に後編ですかね。また見てみたいけど、レンタルビデオ店に行っても見る余裕がない。あ～あ＞～＜。
あほなことを、書いてしまったおっさんでした。そいじゃまた！！

### [2672] ベリー	2001.09.02 SUN 22:18:43　
	
こんばんわ。

＞まみころ様[２６６４]
私のくだらないことにレスしていただいてありがとうございます。射撃したことあるのですか？私は怖くて出来そうに無いな･･。重いっていうし。

【クリスマスに～のお話】
そういえば、サブリミナルがどうとか結構、話題になりましたね。ニュースで報道されていたし。でも、あれから何年かあとに、私の住んでいるところで、深夜にCHの再放送がありましたが、その時は入っていなかったようです。私もサブリミナルのことを思い出して、自宅のビデオデッキでスローでコマ送りしてみたのですが、問題のシーンは無かったみたいで･･。局が修正したのか、それともうちのデッキでは見つけることができなかったのか、どちらか分かりませんが、もしDVD化することになったら、ぜひ、この話もいれてほし～！！(全話DVD化となると、かなりの本数になるでしょうね･･。)

### [2671] sweeper（レスでーす。）	2001.09.02 SUN 21:46:18　
	
Good　evening！！また来ました。

＞リョウちん様。[２６４４]
こちらこそレス、ありがとうございます。
２人のコンビプレーで思い出したんですが、葉月さんの話での香を救出する方法。絶妙ですよね。やっぱり、りょうのパートナーは香しかいないなぁと改めて思いました。アイコンタクトがあるからできることなんですよね。

＞まみころ様。[２６６４][２６６６]
「TITANIC」私も観ました。と言っても映画館でですが。
涙なくしては観られなかったです。私は沈没のときのシーンで、楽団の人たちが最期まで演奏していたシーンと、船長とタイタニック号設計者が自分の運命を船と共にするシーンで泣いてしまいました。でも、皆さんがカキコしているようにあの２人、りょうと香のようで切なかったです。
あと、B'zの歌。「BLOWIN'」もりょうと香に近い気がします。
前半がりょうで後半が香のことを思い出します。特に終わりの方の「＠＠＠風を受けて立ち上がろうとする君は美しい＠＠＠」という部分。香のイメージにピタリかなと思います。
あと、「裸足の女神」は香サイドの気持ちに近いかなと思います。「The　wild　wind」も切ないです。今のりょうに近いかなと思います。幸せだった時の２人なら、「恋心～Koigokoro～」かなと思います。(すいません。「pleasure」、「treasure」、「mixture」とシングル３枚しか持ってないです。ごめんなさい。)

それでは、また来ます。

### [2670] ☆けいちゃん☆	2001.09.02 SUN 21:38:58　
	
こんばんは～。
まずは。おっさんさま、お誕生日、おめでとー！私は５０年の早生まれで、１月で２７になっちゃいます。とほほ・・・でも、気持ちは（りょうと同じ）はたち☆です。あ、ちなみに、昨日の「タイタニック」のディカプリオと同じ年です。はい。

りょうちんさま、まみころさま、「タイタニック」レスさんきゅうです。もちろん昨日もみましたよー。よかった。最後女性が生き残る、というのは女性の生命力の強さを感じてしまいましたが、「エンジェル・ハート」では、その逆ですよね・・・（といっても、香は交通事故死ですが）。あ、私もレオはレオの声が一番だとおもいます。ここのセリフはレオの声で聞きたいというところは英語できいてましたよ、わたし。

カレンダーの話。私は卓上タイプをほしいと思います。予定とか書きこめる、というし、卓上タイプがそういう意味で一番見るんですよねー。ちなみに今使っているのはトトロです・・。トトロからＣＨ，随分変わるなあ・・・・ま、いっか。

### [2669] ありあり（主にレス）	2001.09.02 SUN 21:11:32　
	
ただいま～。２日ぶりです。
【葵様】
私のくだらない駄文にレスくださってありがとうございます（汗）金魚パキッについて言いますと、液体窒素にて瞬間冷却した場合可能のようです。間違ってもフツーに冷凍庫で凍らせないように（汗）時間がかかると細胞が壊れちゃいます。液体窒素はよく大学の実験なんかで使いますが、一般には手に入んないと思いますです。ハイ。
【sweeper様】
レスありがとうです。そっかー。言われてみればりょうの目もともわずかに違いますねー。鋭い。目もとが柔らかいのは香ちゃんだけ？だとしたら北条先生の微妙な描写に感服です。
【クリスマスにウエディングドレスを】
サブリミナル効果とはカットの中に通常の再生では認識できないくらい短いカットを折り込む事です。（コンマ何秒っていうくらい短い画像です）普通に見てる場合にはその映像はわからないのでなにも起こらないように思いますが、実は脳の中にはその画像が移り込んでいるので、一種の暗示効果を生み出す事があります。確か、オウムの事件の時は信者にそういった映像を見せる事によって暗示をかけ、いろいろ事件を起こさせるように仕向けたとか、どうとか、だったと思います。
でも肝心な事に「クリスマスにウエディングドレスを」のストーリーを全然覚えてない（汗）いったいいつ何処にそのような仕掛けをしたのか・・・。どなたか話の内容を教えてください（滝汗）

カレンダー、絶対ほしいっ。　　ありありでした。

### [2668] しゃる。（レスです）	2001.09.02 SUN 19:56:34　
	
〓Cooさん[2665]〓
カレンダーは左の『COAMIX』から『コミックバンチ』のＨＰに行って、そこの『商品開発企画室』に見ると詳しく分りますよ。皆さんが言ってるカレンダーとはそのことだと思います。違ってたらすみません。

〓さえむらさん[2662] 〓
もし、ＤＶＤ出ても『クリスマスにウエディングドレスを』は収録されないんですか？　なぜ？？？　
その「オウム事件」って？あの時何かあったんですか？
「サブリミナル効果」って？どんなカットですか？
どんなシーンか、ビデオあるんで後でCHECKしたいんですが、そういうのってダメなんですか？

### [2667] ますみ	2001.09.02 SUN 19:32:03　
	
＞冷凍保存のこと
心臓のない保存でも大丈夫なんじゃないですかね?細胞の組織自体は腐らないでしょうし｡ただ、いざ心臓を戻す時に､現に体は保存されていても､心臓は動きっぱなしだったという事で、かなり負担がかかるような気がするんですけれどね｡でも、冷凍保存していた人を生き返らせるって､今の技術じゃ出来ないような気も…。
＞B'z
　今のリョウの気分がB'zの曲だと「friensⅡ」のアルバムの中の「君をつれて」というのがあってると思います｡
「もう一度だけ　君をつれて　旅をしてみたい
　知らないうちに　忘れてきたもの探し　道をたどる」
　という歌詞があるんですが、じーんときます。お試しあれ｡

### [2666] まみころ	2001.09.02 SUN 17:55:25　
	
連続になってしまいました。

私もＴＩＴＡＮＩＣ見ました。
以前ビデオで見たので、見るつもりは無かったのですが、
やはりちょっと見てしまうとダメですね。（笑）
今回ＡＨがやってるせいか、リョウと香が思い出されました。
自己犠牲。愛する者を助ける為に自分を投げ出す。
香の場合は、愛する者だけでなく弱い人も助けてしまう。
放っておけない性格が、不幸な事故へと繋がったんですよね。
でも、そんな香だからこそリョウも好きになったはず･･･。
だからちょっと複雑･･･。
だんだん悲しくなってきたので、この辺で･･･。（笑）

### [2665] Coo	2001.09.02 SUN 17:52:00　
	
はじめまして！！Cooです。7月頃から毎日の日課としてずっと拝見させて頂いていたのですが、とっても楽しそうなので、ぜひともお仲間に入れて頂きたくて書かせて頂きました。

［カレンダーについて］
最近のログによくカレンダーの事が話題にされていますけど、
何の事ですか？？ファンとして失格な発言で申し訳ないの
ですが、どこから発売されるの？？？卓上タイプや壁掛け
タイプいろいろあるの？？って、とっても気になってるんです。
どなたか教えて下さい！

### [2664] まみころ	2001.09.02 SUN 17:20:11　
	
レスです。

［香のこと］
冷凍保存や、私の考えにコメントをくれた方どうもありがとうございます。
そうなんですよね、私も諦め悪いというか何と言うか･･･。
未だにまだ香がどうにかならないか考えてます。
＞葵様［２６３９］のおっしゃるように、心臓のない冷凍保存は
可能なのでしょうか。誰か詳しい方いませんかね。（笑）

＞ベリー様［２６４０］が言う薬莢の落ち方･･･。
私もあのかっこよい感じを想像してたんですよね･･･。
だから海外へ行った時に射撃をやる時は、リョウ達のように
立って銃を的へ撃ち、床に落ちる薬莢･･･。
しかし実際は、椅子に座り頭に当たる薬莢･･･。（笑）
カッコ悪いったらありゃしない。
しかも考えてたよりもすさまじい衝撃に驚き、手も震えました。
香が初めて銃を手にした時、震えながら構えた時を思い出しました。
普通の人はああなるもんだな、やっぱりリョウは違うな･･･と。
何だか話がそれてしまってすみません。（笑）

［Ｂ'ｚレス］
　Ｂ'ｚの曲にレスをいただきましてありがとうございます。
sweeper様は「ＫＡＲＡ・ＫＡＲＡ」ですか！？
乾いてるリョウに香の愛情たっぷりのしずくをあげたいです･･･。
今のリョウにＧＯＬＤはちょっと可哀相ですね。
幸せな頃だったらあうかも･･･。ちゃこ様の言うように声を聞く
だけで切なくなりそう･･･。（笑）

### [2663] sweeper	2001.09.02 SUN 16:11:58　
	
こんにちは。

[新宿歌舞伎町のビル火災のこと。]
皆さんと同じく、びっくりしました。
今朝新聞で知ったのですが、りょうと香達が住んでいる場所出火災事故が起きてしまったなんて！
もし、りょう達がまきこまれていたら・・・。一瞬考えてしまいました。(不謹慎ですいません・・・。)
亡くなった方々のご冥福をお祈りします。

[カレンダーのこと。]
記念日が入れられるそうなので楽しみです。
もし買うとしたら、皆さんはりょうと香の誕生日を入れますか？
私は、入れようと思っています。

＞おっさん様。[２６５７]
お誕生日、おめでとうございます！
ドラえもんと１日違いなんてうらやましいです。
身体に気をつけてお仕事、頑張ってくださいね！

＞あずみちゃん♪様。
私のくだらない余談にのって頂いてレス、ありがとうございます。「りょうちゃん大カンゲキー！」です。
そう言われれば、りょうは香にそんな風なこといわれてましたね。

＞さえむら様。[２６６２]
私も「クリスマスにウェディングドレスを・・・」好きです！
りょうの「ばか！ドレスなんてどうだっていい！！」セリフと表情すごく好きです。ところでそのサブリミナル効果のコマはどんなコマだったんですか？見たときは気付かなかったのですが・・・。
あと、香が記憶喪失になった話も好きです。
DVD化、して欲しいですね。(でも、プレーヤーが先だな・・・。)

＞ありあり様。[２６４５]
私の「大人のマンガ」カキコについてのレス、ありがとうございます。「りょうちゃん大カンゲキー！」です。
グラスハート。ありあり様のおっしゃる通り笑ってないですよね。とくに目が。りょうもそうですよね。回想シーンの時だけは穏やかだったけど、今のりょうも笑ってないですよね。

長々失礼しました。それでは。

### [2662] さえむら	2001.09.02 SUN 13:55:31　
	
さえむらです。久々にＣ・Ｈアニメを見ました♪（『愛と宿命のマグナム』『百万ドルの陰謀』『ベイシティウォーズ』を某店で手に入れたので�他のもあればなー）

[DVD化について]
出てくれると、すっごく嬉しい！
けど。『クリスマスにウエディングドレスを』は、収録されないんだろうな、きっと。
何年か前のオウム事件の時、さんざん、叩かれたし。
サブリミナル効果は、たしか、何度も何度も見せる事によって、下意識に刷り込まれるのではなかったでしたっけ？
１カット入っていただけの事に、あの頃は、皆過敏に反応していた。（無理は無いのだろうけれど）
だから、もしもDVD化されるとしても、『クリスマスに～』は収録されない気がする。
けれども、あの話は私はすごく好きだから、そのコマをカットする（技術的に可能かどうかは知りませんが）かして、是非、収録して欲しい！そのために、そのDVDだけ、多少の値段の上積みは仕方が無いと思うんですが、いかがでしょう？
（「ばか、ドレスなんかどうでもいい！」あのリョウの顔とセリフ、すごく好きなんだよなー）

[盆踊りネタについて]
以前、盛り上がっていたこのネタに、参加させて下さい！
リョウの『もっこり音頭』とかがあったら、劇場版Ｃ・Ｈの『愛と宿命のマグナム』（ニーナさん＜ピアニスト＞が依頼人）の中に出てくる、『もっこり体操』と似た動きをしそうですね。

[新宿のビル火災]
新宿歌舞伎町でビル火災！
びっくりしました。思わず、リョウはこういうのに巻き込まれても、平気だとは思うけど..等と考えてしまいました。（かなり不謹慎ですよね。すみません）
もし、香ちゃんがこういうニュース（歌舞伎町でのなんらかの事件）を聞いた時点で、リョウがまだ戻ってきてなかったら、リョウを探しに飛び出して行きそうですね。
怪しい電話があったというし、真相が気になります。（Ｃ・ＨとＡ・Ｈの舞台だから）
亡くなられた方々の御冥福をお祈りします。

[レス]
＞おっさん様
OKです～。
.....私は、そこの市民です。端っこだけど。
Happy Birthday!
この一年が、素敵な年でありますように.....。

では、長々失礼いたしました。
他のお誕生日を迎えられた方も、
Happy Birthday!



### [2661] リョウちん	2001.09.02 SUN 02:38:53　
	
ーけいちゃん様ー
レス、どうもありがとうございます。きっと全部見ていると思うのですが、今度もう一度見てみようと思いまーす。「１６夜」もすごく聞きたいっすー！！
話は変わりますが、『タイタニック・後編・』見ましたか？っやぱ、切ないですなぁ・・　すごく長い話だけど見る価値ありますね！でも、レオさまはレオさまの声がいいー！なんて、ブツブツ言ってしまう私でした・・。(笑）

ーれい様ー
ありましたねー！やっぱリョウほどの動きを実写できるのは、ジャッキーぐらいなんでしょうかねー。わたくし的には、もっと銃が似合う俳優を使ってくれたらよかったな・・と思ってしまうしだいでした。情報ありがとうございました。

ーおっさん様ー
お誕生日おめでとうございまーーす！健康第一★健やかな日々をねがいつつ・・私は、８月にハタチを迎えました。“大人の世界”グフフ・・に一歩足を踏み入れたということで、どうぞよろしくお願いいたします。

〈カレンダーー〉
私も、すんごい欲しいです！！リョウと香が、☆ちゅー☆してるとこかあったらレアモノですよね！無いかな・・・　

独り言のようになってしまいましたが、この辺で失礼いたします。

