https://web.archive.org/web/20031101133655/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=595


### [11900] kinshi	2003.09.30 TUE 14:20:44　
	
告知？？
来月から・・・・復活！
初めての方　始めまして、この名前だけ、おぼえて　いてください。来月１０日過ぎ　辺りから　復活に向けて
ファルコンケーキ　超特大級　作って配ります。（要注　トラップ）
では、また。

### [11899] こぶた	2003.09.30 TUE 12:39:26　
	
こんにちわ、こぶたです。
台風一家が東京の空を洗って行ってくれて、雲一つない晴々した陽気で気持ちがいいです。

＞[11897] いもさま
つらいです･･･気管支炎σ(>.<)
なかなか治らないものですね･･･布団に入って体が温まり眠気と同時にすごい咳･･･目が覚めちゃって(ToT)

＞[11893] リョウ357さま
こぶたも見ました心臓移植についてやっている番組。
エンジェルハートと同じだぁ(p^-^)p と･･･
本当にある話なんだ･･･と。感動してしまった。

＞[11890] 千葉のOBA3さま
そうそうビニール風船！
千葉のOBA3さまも振り回した？

＞[11891] UKさま
はじめまして、こぶたと申します。
槇村アニキのことでしょ。ＣＨのエンジェルダストはリセットということでこぶたほかＢＢＳの大半の人は考えているようです。
今回のさゆりさんのこともコミックス２０巻とは違うところがありますが、こぶたは、もう一つのストーリーとして槇村アニキのことも読みました。そう読んでいただけると面白くなると思うのですが･･･？
これからもよろしくお願いします。

最近ファミリーコンポはまりだしたこぶた･･･
おかまとおなべの逆転夫婦･･･北条先生はどうしてそんなこと思いつくの？でも面白い！最後まで紫苑ちゃんの性別がはっきりしませんよね？続きを書くんでしょうか？短編でもいいですよね。また、新たなファミリーコンポ読みたいです。

### [11898] Ｂｌｕｅ　Ｃａｔ	2003.09.30 TUE 11:41:38　
	
　こんにちは～。　めっきり肌寒くなってきて、寒さに弱い猫には辛い季節が近付いてます・・・。

＞[11884] みろり さま （腐゜リンセスじゃないのね　笑）
　ラ○ク王国、こっちはたしか木曜深夜の放送なんで、その“白黒どーぶつ”に注目して見てみますね（笑）

＞[11886] へそたろう さま
　バンチ本誌に予告文がなくても、ケータイ☆バンチには予告があるんですね。うちの夫の携帯機種では見られないので（そろそろ機種変更しようか、という話は出てるんですけど、いつになることやら）、羨ましいです。
　「早百合さんが香瑩をＮＹに連れて帰ると言いだす」んですか、じゃあそれをやめさせるためには、リョウと香の絆の強さを見せつけなきゃいけないしぃ、てことはぁ、らぶらぶ回想シーンが見られるかもっ☆てつい期待しちゃいました（あはは）。

＞[11888] ゆーご さま、[11896] 葵 さま
　そうなんですよね～、この番組、前はもっと他じゃ見られないシーンとか見せてくれて嬉しかったんですけど、最近はなんか同じものが多くなってきたようで・・・。でも、あの「ルパ○三世ｖｓオ○カル」は、わたしもびっくりでした（笑）　ストーリィがある程度進んだところでお遊びでならまだしも、復活して第１話であれはどうよ、ですよね、いったい誰があんな話考えたんだか。
　わたしも前はアニメ関連の募集があるとできるだけＣ･Ｈに投票するようにしてたんですけど、あまりにもランクインしないので、面倒になって最近してないなぁ。一応ＨＰはお気に入りに入れてあるんですけど。

### [11897] いも	2003.09.29 MON 23:14:41　
	
こんばんわぁ～。
だいぶ今更ですが、北海道にお住まいの方々、地震だいじょうぶでしたでしょうか？

レスのみです。
＞千葉のマダムさま[858]
「目を離すと～」のリョウのセリフは私もきゃ～vvでした！
コンビニでにやけちゃいましたもん（笑）。ほんとうらやましいわ、香ちゃん☆ですね～。

＞みろりさま[861]
みろりさまのＨＮの由来、とってもかわいらしいですね（*^-^*）
舌ったらずってのがキュートですvv

＞こぶたさま[871]
はじめまして。気管支炎はほんとツライですね。寝転がっても苦しいし、かといって咳してると疲れて眠くなるし。 (^_^;)
走るのがいいかどうかはわからないんですが（汗）、体力つけば免疫力も上がるし、風邪引いたりすることも少なくなるハズ。風邪から気管支炎になっちゃうこともありますしね。
でも、もし走るなら気候が安定して、自分の体調も落ち着いてからがいいと思いますよ。無理のないように～☆


それでは、また～。いもでした。

### [11896] 葵	2003.09.29 MON 21:42:55　
	
　きのうの「アニメ第1回＆最終回特集」、ビデオ録画で見たけどやはり…か。しかしル○ンＶＳオス○ルってのはすごかったですねー…キチンと見てみたいわ♪(^-^*)

＞へそたろうさま［１８８６］
はじめまして。阿香をＮＹに？！さゆりさん、荒っぽい手に出るんですねー…。しかし携帯のナイ私（この時代に…ですが☆）には、うらやましい情報源です…。（T_T)

＞ゆーごさま［１８８８］
はじめまして。バイトで家庭教師ですか。生徒さんがもっこり美人だったら、リョウのようにダテめがねを掛けるってのは…？（^_^;

＞リョウ３５７さま［１８９３］
はじめまして。私もその番組見てました。でもその仮説どおりだと、香の心（心臓）がリョウを求めてしまいますよね？そしたらリョウと阿香は、そして信宏はーっ？！o(´^｀)o ウー

　あ…なんか今日のレスは「はじめまして」ばかりだわ。常連さん、ヤキモチ焼かないでねー♪　d(^_^o)

### [11895] ふじっちょ	2003.09.29 MON 21:40:22　
	
おっと！？忘れていた…
＞AH専属ダンサー？パンダさま
メールどうもですm(__)m気に病んで無かったとは…くそ～(T_T)AHの右下で踊っている貴女は爆笑物です(^o^)丿やっぱりAHだから力はこもっていたんでしょうか？(笑)
本日の抗議でパンダが白黒な理由が分かりました。あれは保護色ではなく“威嚇”を表す色ではないかと言われているそうです。う～ん納得(-_- )
あと「パンダ科」はあまり学者的にダメだそうで、「クマ科」か「アライグマ科」をハッキリさせたいそうです(^_^)さて御本人に決断してもらいましょうか…さあどっち？(笑)

### [11894] ふじっちょ	2003.09.29 MON 21:11:43　
	
う～気持ち悪い…頭くらくらします…(-_-;)
授業中いきなり気持ち悪くなってグロッキー状態…今まで寝てたけどまだしんどいッス／(T△T)＼おまけに親が買ってきたアケビが全然熟れてなく味がしない…トドメ刺されました(笑)

＞[11880] Ｂｌｕｅ　Ｃａｔさま＆[11888] ゆーごさま
「日本のベストSP」みれなかったんですが結局CH・CEは出なかったんですね…残念(-_-;)でも、もしランクインしてたらCHの第１話とCH'91の最終回だったのか？それとも、CHの第１話と最終回だったのか…どっちなんでしょうね？最終回の数が多いと迷いますね(笑)

＞[11882] いちのさま
僕も似た様なもんですよ(^_^)確かに野郎同士の方が気を使わないしハジケられますよ(^o^)丿オールナイトなんかみんないい具合に壊れますからね(笑)そうなるとアニソンばっかし（爆）
満場一致でみろりさま「王子」改め「腐゜リンス」正式襲名決定！！皆様盛大な拍手を～！！(^o^)丿

＞[11883] 千葉のOBA3さま
幼稚園児のころ夕食時によそ見していて自分の飲み物取ろうとして親父の飲んでたビールを取って口に含みビックリして噴出すほどの酒豪ですから(爆)

＞ [11884] 腐°リンス★みろりさま
プロですから (￣ー￣)聞き逃しませんよ～(笑)
メールどうもですm(__)m右下で踊る“白黒いどーぶつ”爆笑モンです(^o^)丿AHの右下で踊れるとは流石というかなんというか…（￣□￣；）！！

＞[11887] ちゃぁさま
「備えよ常に」…といいつつ備えてるのか家？と考えたくなる今日この頃…(爆)でも本当にここ最近多いですからね、冗談言ってる場合では無いですね(-_-;)

＞[11892] ぽぽさま
「☆貴婦人☆みろりさま」じゃなくて「貴“腐”人」もしくわ「“腐゜”リンス」ですよちゃんと呼んで差し上げないと(爆)AHはやっぱり多くの皆さんに読まれてるんですね(^_^)

＞[11891] UKさま
はじめまして(^o^)丿ふじっちょと申しますどうぞヨロシク(^_^)
他の方がすでに答えられてますが、AHはCHの「パラレルワールド」と位置づけられて、設定は若干（かなり？）異なっています。ですので続編ととらえるにはちょっと無理がありますね。AH1巻の「※読書前のご注意」に詳しく（？）載ってますんで見てみて下さい(^_^)

それではもう寝ますお休みなさいませ(-_-)zzz

### [11893] リョウ357	2003.09.29 MON 20:15:13　
	
今日、心臓移植についてやっている番組を見ました★
心臓移植によって、ドナーの記憶が移植された人に乗り移ることが、実際にあるそうです★
なんでも、心臓にも脳のように記憶を残す細胞があって、移植後に移植された人間に、整然生きてた時の強烈な記憶のみ、蘇ってくることがあるそうです★
でもこれは、ごくまれなケースで奇跡に近いそうです★
でも、アシャンと香ちゃんみたいに『心と心が、お互い通じ合う』なんて言うのはリョウ・香織・アシャンの起こした奇跡なのかもしれませんね^^☆★☆

### [11892] ぽぽ（Hyogo）	2003.09.29 MON 18:56:26　
	
めっきり涼しくなりました。過ごしやすい季節だ～！
紅葉が楽しみだ～♪

＞[11891] UKさま
はじめまして、ぽぽといいます。
ＡＨはパラレルワールドとして描かれているので、ＣＨと少し違う設定にしているそうです。なので、キャラ設定で同じ部分があってもＡＨはＣＨの続編ではないそうです。
機会があればＡＨの１巻読んでみてください♪（´_｀）

＞[11886] へそたろうさま（お初ですm（_ _）m）
ぬぁにぃ～～～っ！！ （」+｀皿´）」
「香瑩をＮＹに連れて帰る」だぁ？！（ブチぶちブチッ！）
ケータイ☆バンチにそんな予告が書いてあったんですか（×д×;）
何なめたことぬかしてやがんでー、早百合っち。
阿香とかおリンの気持ちも聞かんかい！！リョウと香を
やっと出会えた親として暮らしてる阿香の気持ちも考えんかい！
･･･って、何キレてんのかな私。
予告だけで次号の詳しい内容まで分かるはずもないです。m（＿ ＿ι）m
阿香を普通の女の子として暮らさせたいって気持ちは分かるけど･･･。

＞[11878] ふじっちょさま
＞[11877] ☆貴婦人☆みろりさま
ＡＨ８巻、頑張ってますよねー、嬉しいですねー♪
諸手を上げて拍手！w（＾◇＾）w（パチパチパチ）

### [11891] UK	2003.09.29 MON 17:05:56　
	
どうもはじめまして。まだまだ初心者のUKです。一つ疑問に思った事があるんですが槙村秀幸の死はエンジェル･ハートとシティ･ハンターでは設定が違うような気がするのですが…教えてもらえれば嬉しいです。

### [11890] 千葉のOBA3	2003.09.29 MON 13:17:24　
	
こんにちわ♪本日は晴天でお休みナリ・・・。

１１８８９　　Ｓａｅｋｉさま　　おひさしぶりでーーーす！そうですよねー。「エンジェルハート」は、香ちゃんのハートだから・・・。なんですよね・・・。そうすると、やはりこの物語での香ちゃんの重要性を感じますよねー。しかし、年齢は塩爺に感銘を受け忘れました・・・っていいですねー、それ！今度私も使わせていただきたいです（笑）

１１８８８　　ゆーごさま　　はじめまして！家庭教師のバイトするんですか？りょうちゃんの家庭教師みたいだったらスゴイですねー。しかし、大丈夫スケバンは家庭教師は雇わないと思うふつう。でもウラバンはありえるなぁ、ウン。

１１８８６　　へそたろうさま　　え？ケータイバンチにそんなコトが・・・。私まだやってないんですよー。近々買い換えるつもりなので、その後にしようかと・・・。私の場合ほんと最近ケータイが仕事のみに使われてるので、悲しいなー。

１１８８５　　こぶたさま　　知ってるーー！あの夜店とかで売られてたビニールのでかいハンマーでしょ？うちにもありましたよ！もちろん私が「１００ｔ」って書きましたけど。（笑）

１１８８４　　腐＋みろりさま　　はい！今日の晩御飯が楽しみです！！闇の男さま、エコルさまへのレスで、私的にはやっぱり④かな？ありえそう・・・。でもなんとなく笑えたのは②・・・。なにげな言い方が笑える！そう・・・りょうちゃんの中にある後悔とか、そういうモノ、そう簡単に消えないよねー。「いつも一緒にいたのに、なんであの時・・・。」っていうのは・・・。やっぱりアシャンがそばにいて、りょうちゃんを癒してくれるのが一番でしょうね・・・。あとうずらの玉子は模様が一羽ずつ違うって、「へぇー！」で言ってたよ。

それでは本日はこれにて・・・。

### [11889] Saeki	2003.09.29 MON 11:52:40　
	
みなさんお久しぶりです。スタバのスコーンをがっつきながら、仕事休憩中・・・です。
最近金曜日にバンチが買えなくて、ようやく追いつきました。
新宿の天使・・・よかったですね。
リョウはシティハンターでしたが香はシティエンジェルだったんですねぇ。
最初、「エンジェルハート」って香が（亡くなって）天使になっちゃったから？と思ってましたが、生きている頃からもう天使だったのか～と、なんかタイトルにしっくりきてていいですね。
あと、若き日の二人が久々に見れて嬉しかったです。次回もぜひ見たいなぁ・・・。
初めましてという方多いですね。こちらこそ初めましてSaekiです。年齢は塩爺に感銘を受けて忘れてしまいました。
性別はＦ・Ｃの紫苑に感銘を受けて・・・なんて。
まぁちょっと謎めいた気分でいたかったんです（笑）。
L．A．と日本をいったりきたりでフラフラしています。
といっても仕事なんですけどね。
L．A．潜伏期間が長いと日本語が乱れ気味で、逆に日本にずっといると、英語の反応が悪くなる中途半端な日本人です。
今度遊びでパリかなんかに行きたいです・・・おいしいものが食べたいなぁ（笑）

新宿的天使···太好了。
獠是城市猎人，香是城市天使。
最初，“天使之心”是因为香(去世)变成了天使吗?，不过，从活着的时候开始就已经是天使了吗~，好像和标题很吻合真好。
还有，好久没看到年轻时的两个人了，我很高兴。下次也一定要看啊···。


### [11888] ゆーご	2003.09.29 MON 00:50:45　
	
今日バイトで家庭教師することにきまりました！ちゃんと教えられるかな～。女の子を教える予定なんですけどＣＨでリョウが家庭教師になった話みたいに教える子がスケバン系だったらどうしよう（笑）

＞[11880] Ｂｌｕｅ　Ｃａｔさま
僕も「これ○日本のベ○ト」（テ○ビ朝日系）見ましたよ！残念ながらＣＨ、ＣＥはランクにはいりませんでしたね＞＜ＣＨに一票いれたんですけど。アニメのときはいつもみてるんですけど、なんかいつも同じようなランキングになりますね。あれ全部流しいてほしいのに飛ばしたりするんだよな～。

### [11887] ちゃぁ	2003.09.29 MON 00:36:02　
	
こんばんは。
ちゃぁの持ってる携帯は、ａｕなので、
ケータイ・バンチが今週には、見られるのが楽しみで仕方ないです。

地震のことでは、たくさんの方から、レスいただきありがとうございます。

＞11884　腐°リンス★みろり　さま
震源地に近い海岸沿いの所は、断水がまだ続いているようですが、
ちゃぁのすんでるとこは、普段の生活に戻っています。
某油所の火災・・・とってもこわいです。きちんとしていただかないと・・・

＞11874 葵　さま
ちゃぁの住んでるところは、震度４だったらしいんですけどね。
周囲の人にも、「あれで、ねてた！？」と言われるんですが、寝てたんですよねぇ。

＞11873　ふじっちょ　さま
震源地付近では、まだまだ余震が続いてます。
一日に何度も、地震情報のテロップが流れてます。
「備えよ常に」その通りですね。我が家の非常袋確認しなくては。

＞11868　ぽぽ（Hyogo）　さま
何もなかったから良かったですが、
被害が起こっていたら、爆睡してたなんて、言ってられませんね。

### [11886] へそたろう	2003.09.29 MON 00:11:02　
	
こんばんは。今日はお仕事だったへそたろうです。
ケータイ・バンチの次週の予告を読みました。
早百合さんが「シャンインをＮＹに連れて帰る」と言い出すが、果たしてリョウはどうするのか・・・？！ってかいてあるんですよ。すごく気になって仕方ないです。リョウにとっても阿香は、かけがえのない存在だということや、カオリは幸せだったことがわかった上でそういう答えを出したのかなぁ・・・とか色々考えてしまいます。早百合さんの気持ち、わからなくはないけど、もし阿香がＮＹ行ったら、残されたリョウはどうなるの！？（多分行かないと思いますが）
あぅぅぅぅ・・・、次のバンチが待ち遠しいです！！

### [11885] こぶた	2003.09.28 SUN 22:19:39　
	
こんばんわ、こぶたどぇ～す。
くるしい～σ(>.<)席が･･･席が･･･隊長が暴走している･･･
元気付けに入ってきました。

＞[11884] 危腐人★みろり様
公式王子就任おめでとう！
ここはおそろしいですね。みろり様のＨＮがころころ変わってくぅ～(^.^;あまり腐りすぎないでくれぇ～
貴腐人？奇婦人？腐女子？腐°リンス？
いくつ出るのだろうか･･･たのしみぃ～(^.^)
みろり様はウズラの玉子･･･うちの子こぶたと同じ(^_-) ｰ★
こぶたは半熟玉子作るの得意でぇ～す(^-^)v
ところで･･･被っていたネコはどこに逃げたの？

＞[11881] 圭之助（けーの）さま
大丈夫？寒いと思ったら暑くなるし･･･今日は長袖着たり半袖着たり･･･服の調整がむずかしかった･･･
こぶたは熱は下がりました。平熱35度ちょっとで低いのから37度で寝込んでしまうんです。低体温に低血圧症･･･冷たい女です。
でも、心は暖かいよ(^_-) ･･･ほんとかよ･･･と思っている人いる？

＞[11883] 千葉のOBA3さま
ははははは･･･こぶたも同感！傷だらけになっていたりして･･･
りょうちゃんったら香ちゃんには素直じゃないんですもの(^.^;
４．５年前かな？遊園地に遊びに行った時、縁日とかに出てい露店で風船の１００ｔハンマーがあってゲットしたの。早速振り回していたこぶた。風船とはいえ最高でした！

＞[11882] いちのさま
バンチ今週号よみました？

ばいちゃ！きききききいぃ～～～ん

### [11884] 腐°リンス★みろり	2003.09.28 SUN 20:59:41　
	
こんばんわ
日曜にしてはカキコ多かったですねー。今日はちょっと暑かったなぁ。暑いか寒いかはっきりしてほしい…。なんか、隊長がグレてル方が多いようですけど、皆様お大事に。

＞[11882] いちのさま
よぉし！後悔しなさんな。｢俺が王子だったら…｣と思っても、もう遅い！税金じゃ、強制労働じゃ、酒池肉林じゃ～！それから、玉子はウズラに限るのよー!!いっひっひ
それより、早くバンチ、読んでね。

＞[11881] 圭之助（けーの)さま
風邪？お気をつけて。でも、微熱が続くなんて気になるね…。
カクテルの色より、スイカチュ－ハイの色は絶対赤だと思ってたみろりは実はちょっと残念だったよ。

＞[11878] ふじっちょさま
音楽までチェックなさってるなんて、流石クイズメーカ－・Ｆさま。なるほど、こうやってクイズネタを集めるのね。しかし、私はＴＶ見ながら、ランキング紹介右下で踊る白黒いどーぶつが気になっていました。はて?いつの間にあんなところに…

＞[11874] 葵さま
「女のコ」っていうと、私は26巻(だっけ？)のパ－ティドレスが大好き。香ちゃん真っ赤よりピンクとかの方が似合うよね。(しかし、24巻も26巻もリョウの手、見てるだけでくすぐったくなっちゃう)

＞[11870] 闇の男さま
＞[11869] エコルさま
はじめまして、みろりと申します。
遅れを取りましたが、美樹ちゃんの件、みんなが気になってることと思います。でも、どうなんでしょうねぇ？北条センセ？パラレルワールドとしては
①8歳の美樹ちゃんと海ちゃんの出会いが無かった。
②最初に(傭兵を辞めさせるため)別れてそれっきり
③ウェディングベルまでいったけど何かの理由で別れてしまった
④コレから海ちゃんとの出会いがある

＞[11871] こぶたさま
多分、中学生のころから、腐女子だったんだなぁ。ネコを被りつづけて苦節(？)●十余年！

＞[11866] へそたろうさま
解禁後初カキコ！あたしもやってみたいよォ！

＞[11865] ちゃぁさま
地震に気付かれず眠ってたのですね。でもご無事でなにより。断水とかは大丈夫なのですか？●光の火災とか、こわいね～

＞[11862] 阿樹さま
はじめまして～。みろりと申します。ヨロシク！ここって目が廻るくらい回転が速いから、あんまりなどと言わずしょっちゅう来てチェックしましょ～

＞[11858][11883]千葉のOBA3さま
＞つくづく、「あの時」にそばにいれなかった事…。そのりょうちゃんの気持ち…（¨)(．．)(¨)(．．)うんうん ｡･ﾟﾟ(>_<)ﾟﾟ･｡
誰が責めなくても自分が責めるんだろうなぁ、リョウは…。早百合さんに香ちゃんのこと教えていくその過程が、リョウにとってそうした気持ちが昇華していくものであればいいけど…。
HNはいっぱいついちゃうというより、自分で付けてたりして。てへっσ(＾┰＾)

### [11883] 千葉のOBA3	2003.09.28 SUN 20:20:39　
	
こんばんは☆今日はステキな秋日和でしたよ。（でも私は仕事さ。）

１１８８２　　いちのさま　　もーー！！ＡＨは立ち読みでもいいから読めーーー！！！今週号も感動しましたよん♪地震は怖いですよねー。富士山が噴火するかも？とか、東京直下型とか、イロイロな説があるようですが、いつかは来ると思うと心配です。

１１８８１　　圭之助さま　　体調わるそうですねー！大丈夫？お大事にしてくださいね。

１１８７３　　ふじっちょさま　　お酒強いとーーー！？コラコラ。しかし弱いより強い方がいいのさ。ただ介抱役に回らなければならないのが大変かも・・・。

１１８７１　　こぶたさま　　そうですよねー！あの時の「ＸＹＺ」の報酬は香ちゃん、どうしたのでしょう？？？りょうちゃんは照れ隠しに「金よこせ」とか心にもないこと言って、ハンマーくらいそう・・・？

１１８６６　　へそたろうさま　　「ゲ＠も吐くけど毒も吐く」ってサイコー！！私は飲めないけど、楽しいお酒は好きですよ！！

１１８６２　　阿樹さま　　お久しぶりですーーー！まだドシドシとカキコして下さいねー！！

☆☆香ちゃんスペシャルの色☆　　私は紫系かなー？とか思うのですが・・・？ところで、あの信ちゃんの回想シーンはいつ頃の事なんでしょう？もう槇ちゃんは亡くなって、香ちゃんがＣＨになった頃なのかなー？りょうちゃん簡単に人を殺そうとするし、香ちゃんの顔にまだ少女の面影があるので・・・いくつぐらいかなー？２０歳くらい？

＞貴腐人？奇婦人？それとも腐女子？のみろりさま　　ここにいるとＨＮがイッパイついちゃうのよねー！私もりょうちゃんにおぶってほしい！一緒に飲みに行きたいーーー！！！

＞おっさんさま　　２８歳になったんですかー。若いなー？ところでこの間に千葉が本店のお店って？わからないのー！イ＠－ヨー＠ドー？

また新しい方がたが増えましたね☆はじめまして！よろしくお願いしますーーー！！ここの平均年齢を一人で上げまくっています、千葉のＯＢＡ３です♪

### [11882] いちの	2003.09.28 SUN 12:03:40　
	
え～、、、例のごとく、今週号のバンチを読めていないわけだが、、、俺、金曜日何してた？？？
あ！そうだ。準備だ・・・。家着いたの１０時半だったな・・・。
最近寝ても疲れが取れない気がするのは歳のせい？？？

＞[11843] 圭之助（けーの）様
私の歌声は男女問わず虜にさせるみたいです（笑）。
ちと音痴気味ですけどね・・・。

＞[11844] 千葉のOBA3様
今年の文化祭は、個人的にはゆとりのある文化祭になりそうな予感だけしています・・・当日どうなるかはチョット謎。それが恐い。
北海道の地震では、「うち、地元なんだよね」という友達がけっこういまして、こっちも心配になってしまいました。

＞[11847] ふじっちょ様
もうかれこれ中学以来女人とカラオケにいったことがありませんね・・・。高校男子校でしたし。男と行ったほうがハジケられますし♪
今年成人式なので、そのときは懐かしいメンツと行くかもしれません。楽しみです。
王子は「みろり」様に決定でいいですねｗ。私は玉子のほうが気に入ってますｗｗ

＞[11848] 葵様
１年ってホントに早いものですねぇ・・・。もう大学２年ですよ。もうすぐ３年になってしまいます。そしたらすぐに４年に、そして卒業と・・・時の流れには逆らえないが、出来ることなら幼稚園かそこらの時代に戻りたい。目に映るもの全てが新鮮で、毎日が冒険だったころに・・・。ぐすん。

＞[11850] 無言のパンダ様←復活？
１年で人って変わるものですね・・・（意味深）。
２０のカツオは大人よ～ん♪・・・２９のカツオを見たくない気がするのは私だけでしょうか・・・？
最近、人を愛することがどういうことか分からなくなってきたよ・・・ジーザス・・・。

＞[11861] 腐女子★みろり様
王子決定。
これからは公式王子として日々の政をお任せしたいと思います。この国（サイト）をより良いものとしていってくださいませませ。あなたなら大丈夫、きっと出来るわ！！
腐女子やら貴腐人やら、腐ってばっかりですねぇ～ｗ


### [11881] 圭之助（けーの）	2003.09.28 SUN 12:00:14　
	
こんにちは、数日ぶりのけーのです☆体調不良で微熱続き、ロムもお休みしとりました…(^^ゞ

★感想★
出遅れちゃったので、近い感想は出尽くしてなんかして☆
リョウの冷酷(？)な表情を久々に拝見して「あー、こういう表情もやっぱいいかも…」とホレなおし、そんな彼をＸＹＺで止める香…。いっぱい香に救われてきたんだろうな、って思いました。

★カクテルの色★
色は漠然と…オレンジ系か、みろりさまのグリーン系なのですが、一番に浮かんだのは葵さまのグラデーション系だなぁ。強いってことは…ベースは何なんでしょ？？飲んでみたいな♪

まだ本調子じゃなくて体ダルダルなので、今日はこれにてサラバ★
