https://web.archive.org/web/20020617000827/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=193


### [3860] black glasses	2001.10.17 WED 15:57:31　
	
こんにちは

＞sweeperさん[3840]
僕も槇村兄の「悪魔に魂を売る気はない！悪魔はドブネズミにおとる」というセリフ大好きです。僕は槇村兄＝この言葉を思い出します。
それともうすぐ誕生日なんですか。それはおめでとうございます。僕は２２歳なので１つ違いなんですね。どうりで中学の時幽○白書がやってたとか同じ時代背景だと思ってたんですよ。ただ僕は実年齢は２２なのですがインターネットで精神年齢がわかるのをやってみたところ４０歳と出てこれはやばいと思いもう一度やってみても３０代後半と出てしまいました。確かにシブい人大好きで海ちゃんや芸能人だと舘ひろしさん、田村正和さん、柴田恭兵さん、あと「太陽に○えろ！」に出てくる方たちみたいにシブくなりたいと思って早く３０代後半～４０ぐらいの歳になりたいと思ってましたが、さすがに精神年齢だけが先走り実年齢と倍近く違うとちょっと抵抗があります。

＞無言のパンダさん
あの～、テニス肘ってどのような状態になることなのでしょうか？僕は中学、高校とテニスをやっていて今でもたまにやるのでどうかこの無知な男にぜひ教えて下さい。お願いします。

北条先生の作品とほとんど無関係のことばかりで申し訳ありません。

### [3859] sweeper(解禁まで我慢！)	2001.10.17 WED 15:35:34　
	
こんにちは。
栃木は朝から雨です。こんな日はＣＨでは槙村兄がＡＨでは香が亡くなった話を思い出します。

[レス。]
＞無言のパンダ様。[３８４２][３８５３]
「ル＠ン」＆誕生日レス、ありがとうございます。「sweeper大カンゲキー！」です。
いえいえ、車はそんなに詳しくないですよ。でも、「新ル＠ンのOP」かっこいいです。ルパンのドライビングテクニックもすごいけど、次元のガンアクションもいいです。冒頭がビリヤードをしているところからというのもかっこいい！
懸賞で思い出したのですが、私はつい最近出た「ル＠ン＠世」の総集編の懸賞で２枚組のテレカを当てました。１枚はルパンと不二子ちゃんが描いてあってモンキーパンチ先生のサインが、もう１枚はルパンが防弾ガラスをかざして弾丸をよけているやつです。懸賞はあまり当たらないほうなので、すごく嬉しかったです♪

＞まろ様。[３８４４][３８４９]、海里様。[３８４５]
はじめまして。sweeperです。
Macレス、どうもありがとうございます。｢sweeper大カンゲキー！｣です。Windowsしか使った事がないので、その操作になれちゃったというのもあるのですが・・・。Macは操作が難しいと言うのを聞いたことがあったので。あっ、簡単なんですか。でも、Mac対応のソフト。確かに少ないと言うかあまり見かけないですよね。でも、i－Macとi－Book。色とデザイン好きですよ。キレイだし。青色(ブルーーベリーでしたっけ？)が欲しいと思った事があります。
それから、車詳しいんですね。確か、フェラーリは黄色もあったような気がします。でも、うちの方(栃木某所)では、アメ車が走っているだけでも浮いてます・・・。フェラーリなんかは走っていたらなおさらです・・・。ヨタハチ(トヨタ８００)が出てくるマンガといえば、「交通事故鑑定人環＠一郎」にも、出てきます。この車以外にも、フェラーリやジャガーといった車から最近の車までたくさん出てきますよ。ミニクーパーもちょこっと出てます。でも、プジョーがいない・・・。
アルファロメオはやっぱり赤のイメージなんですね。「ロッソアルファ」っていうからかなぁ・・・。でも、メタリックのグレーは時々見ます。エンブレム好きです。
でも、プジョーは青系のイメージです。

＞玉井真矢様。[３８４７]
「ル＠ン」＆誕生日レスありがとうございます。「sweeper大カンゲキー！」です。
「新ル＠ンのOP」好きと言う方多いですね。あのOPの歌。カラオケでよく歌いますが、キーが低いので２つほどキーを高くしないと歌えないです・・・。歌詞は好きなんだけどな･･･。あとは、「旧ル＠ンのOP」ですね。山田康雄さんのナレーションが好きです。
バイトばっかりしてます。レジを打つ時、１９:１９のお客さんは誰かなーなどと思いながらレジ打ちしてます。

＞琴梨様。[３８４１]
レス、ありがとうございます。「sweeper大カンゲキー！」です。「HERE,THERE＆EVERYWHERE」夜空を連想させるような歌ですよね。歌詞に出てくる「アルテミス」も月と狩猟の女神の名前ですしね。月は人の気持ちみたいですよね。満ち欠けで見えない部分はなかなか見せられない相手への気持ちかもしれないし、表面に出ている部分は本心なのかフェイクなのか・・・。もしかしたら、月は人の心を隠してしまうような不思議な一面があるのかもしれませんね。りょう、紫苑、泪さん・・・。ミステリアスです。

それでは失礼します。
レスばかりで失礼しました。

### [3858] 海里	2001.10.17 WED 15:30:59　
	
今日は雨…
雨の日は、香さんのことを思い出されます…
香さん、あたしとひとまわり違いだったのに、
今は、４つ違い…
う～ん、まさにパラレルワールド！

そーいえば、C.HのDVDは発売されないんでしょうか？
絶対買うのになぁ～
うちは、DVDをPS2で見てます…
悲しい…
iMacでも見れるんですが、ずーっとつけっぱなしにしてると、
iMacが暑くなってきちゃうので…


### [3857] ちーさ	2001.10.17 WED 15:28:01　
	
初めてカキコいたします。
も～北条先生の作品を小学生の頃から愛読している、私。
旦那もファンでして、「Ａ・Ｈ」も旦那が見つけて買って来てくれました！
めっちゃ、話がリアルすぎますよ！今だからこそありえそうで、
あいかわらずの先生のすごさに感動です！（マジで！）

＞ちゃこさん
ＴＭファンですか？うちも今ＴＭの曲聞きながら書いてます。
あ、そういえば、ＴＭ好きになったのも「Ｃ・Ｈ」を
見てからだったは・・・。

### [3856] ちゃこ	2001.10.17 WED 15:24:09　
	
解禁前に、またまた登場。

「リョウ＆香のこと」・・・レス気味の書き込みです。
＞まりも様「3799」
お久しぶりです(^^)　レス有り難うございます。自分の拙い書き込みに
そんなにも共感してもらえて、本当に本当に嬉しいです。
考えてみればAHは、CHより過去に設定することも可能だったと思うんです。
（アニメSPのクラウディアみたいに微妙な存在の人がいて、その人の心臓とか）
そうすれば、完全に「別の話」として読めますから、
読者の拒否反応をこれ程出さ無かったと思う。（別のブーイングの嵐だった？！）
でも、先生はそうしなかった。それはつまり、リョウの心を本当に揺らす事が
できるのは、先生にも香しか思いつかなかったという事だと思うのです。
他の何処を変えようとも、そこだけは変えられなかった。
まぁ、実際どうだったのか先生にしかわかりませんが、そんな風に思えたとき、
私の「２人の絆は永遠」という「AHを読むベース」は、より強くなったのでした。

「リョウが好き」かぁ・・・。香がいなければ応援したいところだけど（笑）
「リョウ＆香」のコンビが好きなので、それは出来ないのでした。ゴメンネ（笑）
でも「リョウが好き」な人も「香が好き」な人も、「２人が好き」な人も。
AHのリョウが「リョウらしく生きてくれること」が何よりの願い。ですよね。

「好きな台詞」一度はやりたい（笑）台詞の引用♪
ソニアの話から・・・
「31日はおまえの誕生日だったな・・・プレゼントは何がいい？」
「いや・・・おれもおまえと同じものでいい」
遙の話に時は流れて・・・
「シティーハンターってのはな！！俺達２人のコンビのことを言うんだぜ！！」

目の前でリョウが撃たれた事で「自分が傍にいること」に初めて迷いを感じた香。
考えてみれば「表の世界」に返す唯一のチャンスだったと思うのです。
でも、リョウはそうしなかった。むしろ香が、絶対離れられなくなる事を言って。
本当「卑怯者！」なんだから（笑）
いつだって、どんなときだって「香を守る」と決めていたリョウ。
最初は「槇村兄との約束」だったかもしれない。
でも「約束」が「決意」に変わったとき。
どんな言葉より行動そのものが「愛してる」って言っていたと思います。

レスが書ききれない・・・(^^;;)　解禁後、また夜にでも。

### [3855] あお	2001.10.17 WED 15:20:37　
	
＞ちゃこ様(3836)
あはははは･･･妹さんを連想したあおは私でした～。3834をカキコした方(笑)。ネチケットやらテロやらでいろいろレスありがとうでした。あまり学校でこういう話が出ないんですよねえ。なんでだろ。私はこういう｢議論(?)」結構好きなんで、ちょっと消化不良気味だったんで、ここでカキコできて結構すっきりしました(笑)。

＞さえむら様(3838)
製本に関して。
私本屋でバイトしているんですよ。当然、入ってきたコミックスをビニール詰めするのも仕事のうち。やっていると製本がしっかりしているかどうか、きれいに作られているかどうか、いやというほどよく分かります。文庫に関しては持ったときの重さと、コミックビニール詰めで鍛えた勘が物を言っております。少し開いてみるだけでも、意外と差は歴然としているんですよ。新書で厚いのはまだ何とかなるけど･･･文庫はせめて分冊にしようよ･･･京極○彦先生･･･。

### [3854] ちゃこ	2001.10.17 WED 14:29:38　
	
こんにちは。美容院の帰りに「B@OK　O@F」にてTMN「BLUE」「RED」
購入してしまいました。（だってレンタルと値段変わらない・・・）
現在「GIRL」が掛かってます。

「パソコンの話」
書き込み見てると、意外にいらっしゃいますねぇ。マックユーザー。
かくいう私もそうでございます。Mac使いやすいですよ。親しい人にユーザーが
いるならお薦めです。（ソフトのやり取りとか頻繁にする人と、同じOSが便利）

＞Ｂｌｕｅ　Ｃａｔ様「3851」
「時に愛は」懐かしくて反応してしまいました（＾＾）
特に松本伊代さんのファンでは無かったのですが、何故かあの曲が主題歌の
ドラマは見ていて（笑）曲が気に入って買ったのです。
確か、初めて買ったレコードだったと思う・・・
「ボーイの季節」も懐かしい。歌詞がちゃんと思い出せないけど・・・(^^;;)
聖子ちゃん、色んな人に良い曲書いてもらってるんですよねぇ。

「誕生日」
sweeper様以外にも、今月誕生日の方いた気がする・・・ので
「皆様おめでとうございます☆」歳を重ねた自分をより好きになれますように。
そういえば、私は23才で結婚したので、そこが節目だったなぁ。

とりあえず、また。

### [3853] 無言のパンダ	2001.10.17 WED 13:36:55　
	
こんにちわ♪
昨日も今日も雨。たしか先週もそうだった・・・(-_-;)

さきほど、ビデオ＆本のお店に行ってバンチ買ってきました。
ちょっと、コミックスの置いてある所を見てみたら
なんと！ＡＨのコミックスがない！（他のバンチコミックスは、あった）これって売り切れ？！
発売された時は、ドーンと平積みで積んであったのに！
すごいねー☆無条件にうれしかったです(^^)
さあ、これで聴コミに応募できる！
でも私も、くじ運基本的に悪いんで・・・
ただ、この２～３年ちょっとしたものは当たってるんですよ。
「ク＠ヨンし＠ちゃん」のテレカとか、Ｓムーンのミュージカルのパンフレットに人形、「ハ＠ジ」のミュージカルチケットなど・・・（大きなものは当たったことないけど）
でも、これらのほとんどは娘の名前で出したもの。
はたして自分の名前で、運があるかどうか・・・！
また、娘の名前で出そうかしら？
あー、でも「バンチ」だからなぁ。年齢的に無理があるか・・・。う～ん、どうしよう？！

### [3852] まろ	2001.10.17 WED 13:18:06　
	
Ｒｅ：[ ３８５１]Ｂｌｕｅ　Ｃａｔさま
　初めまして。ハインツと三姉妹の結末のお話、正直驚きました。
私はてっきり親子水入らずの時を過ごしているものと・・・
（単純で申し訳ございません）でもそれは、瞳の記憶が失われてからの衝撃的な展開に、せめて最後だけは、みんなが幸せになって欲しい・・・無意識のうちに、そう強く想ったからなのかもしれません。
　余談ですが、ＣＨの”恋人はシティハンター”の巻で、冬野　葉子がファインダー越しに犬鳴署トリオ（俊夫・平野・武さん）を捉えているんですよね。俊夫（かれ）は何だか”うだつの上がらない”表情で・・・大丈夫なのか！？頑張れ俊夫！夫婦喧嘩がなんだ！！とか、その時思いましたけどね。
・・・あ、でもパラレルワールドなのかも・・・

### [3851] Ｂｌｕｅ　Ｃａｔ	2001.10.17 WED 11:18:07　
	
　遅ればせながら、「Ｃ・Ｅ」の話を。
　わたしは単行本で（十数年前に・・）初めて読んだのですが、あのラストを読んだとき、「今までずっと父親に逢いたいと思い続けてきた泪や愛の気持ちはどうなっちゃうんだろう・・・」と考えるてすごく哀しくなってしまいました。俊夫と瞳はこれでそれなりに幸せなのかもしれない、でも泪や愛はどうなるの？って。二人だけでもハインツに逢いにいってほしいし、逢えた、と信じていたいけど・・・。
　それに、もし瞳が父親に逢っても何も思い出せなかったら、よけいに切ない・・・し、Ｃ・Ｅとして過ごした苦しんだ日々はいったい何だったの！？とか思うと・・・・。だから、［３７６０］Ｋ．Ｈさまの「キャッツ・カードを見て『思い出せない』と言ったのは演技だったのでは？」という考え方は目からウロコでした。そういう考え方もあったんですね。瞳ももう一度俊夫と恋をしなおすために・・・。本当にそうだったらいいな、って思いました。そして、Ｃ・Ｅ時代のことは忘れたふりしててもいいから、ハインツが父親だということは判ってほしいな、なんて思いました。今となっては空想することしかできないけど。

　それから、ちょっとだけ音楽の話も。
　作品紹介を読むと、北条さんは尾崎亜美さんのコンサートで弾き語りの「ボーイの季節」を聴いて、『少女の季節～サマードリーム』を思いついたそうなんですが、この曲、ライブ版ではないんですが、『ＰＯＩＮＴ－２』という彼女のセルフカバーアルバムに入ってるんですよ。原曲が松田聖子さんのものなのは、知って・・ますよね？若い人は知らないかなぁ。
　わたしはずいぶん前に、別の曲が目当てで（笑）この『ＰＯＩＮＴ－２』を買ったんですが、この話を知ってなんだかすごく嬉しくなってしまいまいた。最近レンタル屋とか中古屋とかで、あるかな～と思って見てみたりもするんですが、見つからないんですよね・・・持ってて本当によかった、って思います♪　個人的には「時に愛は」（松本伊代さんに書いた曲）が好きです。が、「ボーイの季節」も好きですよ。
　
　还有一点是关于音乐的。  
　根据作品的介绍，北条先生在尾崎亚美的音乐会上听到了"男孩的季节"，于是萌生了"女孩的季节-夏之梦"的想法。这首歌不是现场版，但收录在她的自我翻唱专辑《POINT-2》中。 你知道这首歌的原唱是松田圣子吧？ 不知道年轻人是不是不知道。  
　这首《POINT-2》是我很久以前为了另一首歌而买的（笑），我听说后非常高兴。 最近我一直在租赁店和二手店找有没有，但是都没有找到...我真的很高兴我拥有了它...我个人很喜欢'有时爱是'（为松本依代写的歌）。 但我也喜欢《男孩的季节》。

### [3850] 春日	2001.10.17 WED 11:00:50　
	
10/22月曜ミステリー劇場で「移植のための心臓が盗まれ、違法移植された少女。」云々という2時間ドラマやりますね。
　に、にすぎている。設定がそっくりでびっくりです。

10月22日周一Mystery剧场将有一个2小时的drama"一个女孩的心脏被偷来移植，并被非法移植"。
　故事和原著太相似了。 我很惊讶故事背景如此相似。  

### [3849] まろ	2001.10.17 WED 09:27:38　
	
Ｒｅ：
[３８４５]琴梨さま
おはようございます。さて、質問の答えですが・・・
う～ん、間違っているかもしれないので、鵜呑みにして欲しくないのですが、確か、タバコメーカー（？）だったような気がします。でも、欧州ではタバコの宣伝はＮＧなので、ロゴを入れずに
　　　”５５５”ではなく”）））”と変更していると、
どこかで聞いた気がします。あと、ヨタハチですが、自分も実車は見た事ありませんね。１９６０年代の車ですから。でも、某自動車誌（ベスト○ー）の記事で観た事ありますので。（べ○トカーは、バンチと一緒に購読してます）

[３８４６]海里さま
　初めまして。フェラーリの色ですが、ウチの近くでは
黄色のＦ３５５走ってます。
　上の琴梨さんのところで触れましたが某自動車誌
（べス○カー）では、白のフェラーリをこよなく愛する
評論家さんのコーナーがありますし（笑）

[３８４０]ｓｗｅｅｐｅｒさま
　私もＭａｃ使ってますが、ｗｉｎｄｏｗｓより圧倒的に
使いやすいんですよ。今度、番外地にモノクロで投稿みようかと
思ってます（投稿暦ウン年、１度も採用された事は無いですが
（苦笑））

### [3848] つぐみ	2001.10.17 WED 09:06:05　
	
おはようございます。
最近の水曜日は雨ばっかりです。

昨日買った「千葉ウォーカー」に「Ａ．Ｈ」が紹介されてました。
でも「Ｃ．Ｈ」の続編みたいに書かれていたような。
北条先生のコメントも小さくですが載ってました。
他の「ウォーカー」はチェック見てないので分からないのですが。

### [3847] 玉井真矢	2001.10.17 WED 06:30:50　
	
[3840]sweeper様
もうすぐお誕生日！おめでとうございます。
幽＠白書を中学のときに見ていたと言うので年が近い方だと思っていたけど僕はまだ22歳だけど2月生まれなので同学年ですね！
なんか同学年の方がいらしゃると嬉しいです！！

無言のパンダ様、sweeper様
僕も｢新ル＠ンのOP」好きです。
それにあのおなじみの曲に歌詞があるのを知ったのも｢新ル＠ンのOP｣を聞いたからでした。
あの歌詞結構気に入っています。





### [3846] 海里	2001.10.17 WED 02:17:13　
	
寝ようと思ったんですけど、あと一言！！

☆まろさま［３８４４］
すごぉ～い！！
車のこと詳しいんですね！？
あたしの車の知識は、浅く広くなので、
ヨタハチと言われても、どの車種だかわかりません！
スバルの車が好きなんですか？
あたしは、スバルの車だとレガシーが好きです。
特に白がお気に入り♪
そーいえば、毎朝、家の前を黄色で「５５５」とボディーにかいてある、
ナンバー５５５の青のインプレッサを見ますが、
「５５５」って、どーゆー意味なんですか？

ではまた！

### [3845] 海里	2001.10.17 WED 02:02:36　
	
こんばんは、みなさん！
夜更かし野郎の、海里です！

解禁まで、あと１７時間を切ったってとこでしょうか？
うちの方は、火曜日発売なのですが、
毎週、火曜日の夜中にふと、
「バンチ、買いに行こうかなぁ～？」
と思いながら、布団に潜ってます。
行動力がないあたし…

☆琴梨さま［３８４１］☆
レス、ありがとうございました♪
遅ればせながら、初めまして！
フェラーリはカッコイイですよね？
ところで、フェラーリって赤しかないんですか？
他の色、見たことないので…
スタンドで働いてると、確かにいろんな車に乗れます。
洗車機に入れるだけの短い距離ですが…
でも、お客さまのお車なので気使います。
↓で、「夜中の誰もいない帰り道に晴れた星空を見上げて
TM Networkの‘Here,There&Everywhere’をつい歌ってしまう」
とおっしゃっていますが、
あたしは、夜、高速を走っていると、
つい、‘Get Wild’を口ずさんでしまいます。

☆sweeperさま［３８３７］☆
こちらこそ初めまして！！
あたしは、あんまり外車は詳しくないんですが、
アルファロメオは、赤のイメージが強いです。
それと、あたしMacを使っているんですが、
簡単ですよ？
初心者向きです。
Windowsも少し使ってますが、Macの方が簡単かな？って思います。
日本てWindowsが主流なんですよね？
なので、Mac用ソフトは極端に少ないです。
でも、イラストなどをやりたければ、
ぜったいにMacをお勧めします！！

ではまた！


### [3844] まろ	2001.10.17 WED 01:36:45　
	
レス
＞琴梨さま〔３８１４]　『車のこと』
　過去のログを観ていないので話が重複していたら申し訳無いのですが・・・北条先生の作品ＣＥ・ＣＨ等で登場人物が様々な
国産の名車に乗っているんですよね。例えばＣＥでは、浅谷刑事は初代フェアレディＺ、泪姉は初代ＲＸ－７（連載１１話他）、雨に日に俊夫が遭遇した人身事故の加害者の車がトヨタ２０００ＧＴ、ＣＨでは　槙村兄が、マツダ　キャロル（初代）、リョウがヨタハチ（トヨタ８００）を始め、初代ＣＲ－Ｘ、ＦＣ　ＲＸ－７、そして、ミニ（これは国産ではないが）、あとは逃がし屋の小林みゆき嬢がＲ３２　ＧＴ－Ｒなどなど・・・（某誌の「頭○字Ｄ」とか、「湾岸Ｍ○ＤＮ○ＧＨＴ」とかで主人公達が乗っていたりします。赤○の白い彗星、地○のＺ）
　何だかとりとめないですけど、北条先生の作品には随所にこだわりがあって、なおかつ遊び心が満載なんですよね。ちなみに私は初代インプレッサＳｔｉのＶ型が好きですね。なぜか近所（横浜某所）ではフェラーリＦ３５５（黄）や藤色のスバル３６０、レプリカの先代セリカＧＴ（ＷＲＣ仕様）とか見かけます（笑）

＞訂正とお詫び
先のカキコでエンジェルハートの頭文字を誤ってグラスハートと表記してしまいました。北条先生はじめ皆様方には不快感を与えてしまったことをお詫び致します。

Re  
＞琴梨〔3814]“车的事”  
因为没有看过去的日志的话重复了的话很抱歉…北条老师的作品CE·CH等登场人物各种各样的

开的是国产名车吧。例如CE中，浅谷刑警是第一代Fair Lady Z，泪姐是第一代RX-7(连载11话等)，俊夫在雨天遭遇的人身事故的肇事车辆是Toyota2000gt, CH中槙村哥哥是Mazda Carroll(第一代)，獠是以Toyota800为首，初代CR-X, FC RX-7，还有Mini(这不是国产的)，还有逃亡的小林美雪小姐的R32 GT-R等等……(某杂志的“头〇字D”，“湾岸M〇DN”〇GHT”之类的主人公们乘坐。红〇的白色彗星，地〇的Z)

北条老师的作品中处处都有讲究，而且充满了童心。顺便说一下，我喜欢初代Impreza Sti的V型。不知道为什么，在附近(横滨某处)能看到FerrariF355(黄)、紫藤色的Subaru360、仿制的前代CelicaGT (WRC规格)等等(笑)



### [3843] ちゃこ	2001.10.17 WED 01:34:24　
	
葵様の書き込み読んで思い出した。聴コミ、応募しなくっちゃ！
でも、くじ運の悪さには自信がある私(>_<)　
ダンナのお母さんの名前で出そうかなぁ？（←異様にくじ運が良い義母）
どうせだったら「全員プレゼント」にすれば良いのに。と思ったのは私だけ？
（そういえば、三毛猫様。無事、応募出来ると良いですね（＞3816））

＞Ｐａｒｒｏｔ様
「見ないとハンマーよ！」ヒットしました。造語センス、抜群ですね(^^)
今後も新作、期待してます。
世界が怠け者だらけになっても。「働き者の小人さん」を望むちゃこでした。

＞けいちゃん様「3812」
仕事レス（？）有り難うございます。自営なんでノルマは無いけど、
「話が出来て良かった」と思ってもらえる販売員を目指してます（笑）
販売している商品がオーダーメイド的な要素を含むものなので（眼鏡）
特に気になるのです。でも、おっさん様も書いてるけど（＞3809）
相手した全員が全員、波長が合う訳じゃ無いし、難しいですね。
それでも、嫁いだ先の家業が自分に向いていて良かったと思ってます（笑）

＞K.H様「3807」
「事務所」裏手が「消防署」＆「ベ＠クラッ＠ック」って事ですか（笑）
ダンナの会社は、商店街の１本手前の通りにあります。
他にも山梨県民いると思うけど・・・ここまでニアミスしないだろうな。

そうそう。「香から抱きついた場面は無い」私の書き込みかも。
ただ私の書いたのは「香がリョウを抱く場面」って事だったんだけど(^^;;)
そういうイメージの場面は「膝枕」と「看病するところ」位なんだけど、
私はいつも「香がリョウを守ってる気がする」って書き込みだったのでした。
（別に責めてないから、誤解しないで下さいね（大汗））
「柏木圭子の話」も「最後の話」も「抱きつく場面」好きですよ(^^)
リョウが照れて慌ててるところが、また良かったりする。
そういえば「立ち聞き」はリョウの専売特許なのに、屋上シーンでは
珍しく香が立ち聞きしてたんでした。

「それで考えてみて・・・本能だったか？」
「・・・じゃない（←って言い切ればいいのに）・・・と思う（小声）」

愛の天使（？！）ミックにかかると、リョウも結構素直なんですよねぇ。
相手が海坊主サンだと、こうはいかないと思うのでした（笑）

では、おやすみなさい。

### [3842] 無言のパンダ	2001.10.17 WED 01:27:36　
	
こんばんわ★
今日は何度もここに来ては、取り消しの繰り返し。
書きたいことはあるんだけど、どうしてもまとまらなくって・・・
とりあえずレスです。

＞ｓｗｅｅｐｅｒ様（３８２１）
いつもありがとうございます。
ｓｗｅｅｐｅｒ様がル＠ンにも精通してらして、うれしいです♪
私も「新ル＠ン」のＯＰ好きです！
今見ても、かっこいいですね！
それに、車のことにも詳しいですねぇ。
私は、あまりよく知らないので、珍しい車が走っていても、
名前が出てこないんですよ。(^_^;)
それから、もうすぐお誕生日ですか？おめでとうございます♪
２３歳ですかぁ？お若いですね！(^o^)
私にとって２２歳がひとつの節目だったので、２３歳になった時は、やっぱり複雑というか寂しかったのをおぼえています。

＞Ｐａｒｒｏｔ様（３８２３）
ご心配ありがとうございます。
はい、原因は謎なんですよ。それも左肘。右利きなのに！
でも、２０代でぎっくり腰を経験してますからねぇ、私。こういうのも有りってことかなぁ。
しかし！「老化現象」は今後禁句ですよ！（笑）
最初に言った友人にも、「ちがったぞ！」ってメール入れときました！(~_~;)

＞ちゃこ様（３８３６）
昨日いらっしゃらなかったから寂しかったですよー。
さてキートン山田さんですが、「サ＠ボーグ００９」の００４、
「ゲッ＠ーロボ」は、神隼人です。
「００９」私も見てたはずですが、あんまり覚えてないんです。
見れば思い出すと思うけど・・・。
リメイク版で、今やってるみたいですが、こっちではやってないので、よくわかりません。
それから、「う＠星や＠ら」私も好きです。
テレビも見てましたけど、劇場版はビデオに撮ってます。
こ＠つ猫は、ゲーセンで取ったぬいぐるみ持ってますヨ♪
いい味出してましたよね。あと、テンちゃん、かわいかったです！ちなみに声優さんは「ハ＠ジ」の杉山佳寿子さん。
ハ＠ジは、あまりにも好きなのでビデオ全巻揃えてます(^_^;)

今日のところは、このへんで・・・
では、おやすみなさい(-_-)zzz

### [3841] 琴梨	2001.10.17 WED 00:43:24　
	
こんばんは。琴梨です。

　バンチやっと買えました！今日に限って３件目でやっとＧｅｔ！しかも最後の一冊。みんな、「聴きコミ」狙いなのかな？それとも来週お休みだから？　今週の感想は頭を整理して明日に。

＞海里様　［３８２７］
　はじめまして。琴梨と申します。「かいり」さんとお読みするのでしょうか？　スタンドでバイトをしてると色んな車に乗れる特典があるんですね。車（フェラーリ　№１）好きのくせに私は免許がないので運転できません。バカヤロウです。

＞ｓｗｅｅｐｅｒ様　［３８３７］
　今日こちらはあいにくの雨ですが、私は夜中の誰もいない帰り道に晴れた星空を見上げてＴＭ　Ｎｅｔｗｏｒｋの「Ｈｅｒｅ，Ｔｈｅｒｅ＆Ｅｖｅｒｙｗｈｅｒｅ」をつい歌ってしまうような奴です。よく月は女性の象徴のようにいわれますが、男女関係なく、人の気持ちによく似ているな、と思います。いつも同じ表面を見せているのに、冷たく青く輝いていたり、不気味な程赤っぽく見えたり、満ち欠けを繰り返し、姿を隠してしまうときもある。ほんと、りょうちゃんみたいですね。

　北条先生の作品にはいつもミステリアスなキャラがでてきますが、りょうちゃんも筆頭だと思います。心の奥底（月の裏側）に秘めた深い感情が、時折言葉の端々（月の表面）にふっとでてくる。野上シスターズ唯香ちゃんとの会話がまさに”それ”だと思います。
