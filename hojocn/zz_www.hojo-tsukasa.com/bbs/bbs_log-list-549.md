https://web.archive.org/web/20031027165351/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=549

### [10980] ふじっちょ	2003.07.01 TUE 21:19:01　
	
皆さんこんばんわ！(^o^)丿
皆様のおかげで、頭痛より復調！おじいさまの事も記憶のかなたに封印できることができました(^▽^)今日のぼくは心が広いです！だから「ふいっちょ」と言われたって怒りませんよ…（￣ー￣ ）

＞[972]剣客 無言のパンダ 殿
＞リョウだって銀行の預金残高を見て一喜一憂してるじゃない、きっとわかってくれるわさ♪（余裕）
どうかな？CH14巻P141,1コマ目での麗香に対するリョウの言葉を思い出してみよう…┐（￣ー￣）┌
＞キミは暴れたりしなかったんだろうね～？
頭痛いのにあんな素敵なおじいさま（意味反対）の相手なんかしてられません(-"-)痛くなかったらそりゃ…（自主規制）（笑）

＞[973] みろりさま
(゜o゜)……あっ！いっ…いやあ～身に余る光栄(^_^；)＞
リョウの次ですか、うっ…嬉しいな。ハハハハ（乾いた笑い）ピュウ～ε＝┌（T◇T）┘

＞[974] 圭之助（けーの）さま
そうですよね！バチが当たりますよね？よし！バチが確実に当たるように「(#▽皿▽）＝３」にお祈りだ～さっ圭之助さまもご一緒に(笑)

＞[975] 千葉のOBA3さま
いいことか～７巻の懸賞当たったりして…(゜o゜)あっ！まだハガキ出してね～！！速攻で書かねば…(-_-)φカキカキ

＞[976] yosikoさま
♪おひさしぶーりね♪僕もちゃんと覚えていますよ(笑)
僕も、”お○さんといっしょ”を”おっさんといっしょ”って思ってた(^^ゞ「寝ぼけなまこ」はわざとですよね？ねっ？

＞[979] ayakoさま
「ふいっちょ」もドク○ムシ三太夫さんの言う事支持します。昨日の素敵なおじいさま（意味反対）は明らかにその態度がなっていなったですよ。たぶんあのおじいさまに譲っていても「ありがとう」は絶対聞けなったと思います(-_-)
気持ちよく譲って気持ちよい「ありがとう」が聞きたいものです。(^▽^)

一週間ぶりにテニスしてちょっとお疲れ、今夜はこの辺で帰ります。そんじゃまあサラバ(^o^)/

＞[979] ayako先生  
“不一著”也支持毒〇虫三太夫先生说的话。昨天那位漂亮的老爷爷(意思相反)很明显是那个态度变了哦。我想即使让给那个老爷爷，也绝对能听到“谢谢”(-_-)
想要心情愉快地谦让，听到心情愉快的“谢谢”。(^ ^)




### [10979] ayako	2003.07.01 TUE 20:28:30　
	
どうもこんばんわ。
やっぱり今日も我が家にＡＨとバンチがありません。シクシク(T_T)
みろりさま、ふじっちょさま、励ましをありがとう…。ウチにはＣＨもわずか７冊しか無いんです。実家に残りは置いてきたので。全部こっちに持ってくると大変ですから少ししか無いんです。ＡＨは全部こっちに持って来てるんですけどね。明日こそ帰って来て欲しいな。てゆうか少しでも返してもらえるように催促しよ。全部貸した私がバカだった。ＡＨの事となると心の狭い人間になるっす。
ふいっちょさま
この前、昼に「ごき○んよう」を見ていたらドク○ムシ三太夫さんが出てて、お年寄りに席を譲ることについて話してました。若者がせっかく譲っても「年寄り扱いするな」とか譲らないでいるとわざと聞こえるように文句を言う年寄りもいて、譲られる側の態度がいけないって言ってました。そういうことを言うから若者が席譲らないんだよって言ってた。たしかにそうですよね。私も譲った事あるけど、素直に「ありがとう」って言ってもらえて嬉しかったもん。

前几天，中午看“五〇五阳”的时候，毒〇虫三太夫先生出场了，说了给老年人让座的事。年轻人好不容易让了车，也有老人故意抱怨说“别把我当老年人对待”，说被让的一方的态度不好。说因为你这么说，年轻人才不让座的。的确如此。我也让过你，你能坦率地说“谢谢”，我很高兴。

### [10978] 葵	2003.07.01 TUE 19:11:20　
	
☆書き忘れ☆
ｙｏｓｉｋｏさま［９７６］
たまさまと相方でなかったら…母娘？姉妹？もっと危険なカンケイか？！…怒られる前に退散っ!!!
ヽ(゜▽゜　）－Ｃ＜（／；◇；）／

### [10977] 葵	2003.07.01 TUE 19:07:04　
	
　母が外出のお休みの日、一日主婦してました。今晩は鰹のタタキです。(^^;

　某（Ｍａｍｉ）さまに頼まれて３５７マグナムのプラモを探しに行ったのですが…行き付けのホビーショップ、フラワーアレンジメントのコーナーを拡大したらしく、プラモとかのホビー系が縮小されてました。ゴメンよ、某さま…入手は望み薄だわ…。 (；_；)


＞たまさま［９６９］
久々に来てそれだけ？おっさんさまを見習いなさい！（自己キャラを売るのに必死か？）もちっとアクの強い(笑)カキコをしてくれーっ!!!(^_^;

＞みろりさま［９７３］
葵は「れっきとした(?)」オナゴですたいっ！(笑)どこぞのパンダのコトバに惑わされちゃぁ～いけないよ☆だいたいパンダは日本語話せないでしょ、四川省語(?)だもん！щ(￣∀￣)ш

### [10976] yosiko	2003.07.01 TUE 17:09:59　
	
寝ぼけなまこのyosikoです♪
覚えていただいてる方もいらっしゃるようで、光栄(^-^)♪

またまたやってしまいました・・・「ＣＨ」→「ＡＨ」の通読！　すっごい泣いたうえに、その「ＡＨ」のあとに今週号を読み、おそろしく号泣（笑）嗚咽がもれてしまった(ToT) リョウの涙に心が温まったのか、痛んだのか、それはよく不明ですが・・でも今週号読んだ際には単純に”心が痛い～”って思ってしまった記憶があるなぁ～～読みごたえアリアリですな。改めて。

＞おっさんサマ
おっさんサマによるカキコのラスト、”お○さんといっしょ”って見たとき、寝ぼけてたワタクシは、てっきり”おっさんといっしょ”と思い込み、あぁ、最近はそんな番組があるんだなぁ～と思ってしまった・・体調にお気お付けて♪

＞たまサマ
葵サマから相方っていわれちまったよ～～♪岩○晃○と、○田純○のコンビって・・・（汗）

ではでは、買い物へ行くのでこの変で・・・大雨だけど♪

### [10975] 千葉のOBA3	2003.07.01 TUE 15:49:38　
	
こにゃにゃちわ・・・。ムチャ疲れちまった・・・。
転職したのはいいけど、働くのはどんな仕事でも大変・・・。

９７４　　圭之助さま　　そうでしよー、今回は大悶絶だったわ。しかし、頭部が強大な「？」マーク、あれ巨大じゃないのかぁーーー。あ、そうそう私「赤＠のアン」みたいに巨大な疑問符って言われるーーー！（意味不明）

９７３　　みろりさま　　アハハ！笑った！！懐かしいー！「べ＠ばら」ね。しかし、私はポリニ＠ク夫人の様に控えめとか、おとなしそうとかではなく、ただ「したたか」なだけかもねぇーーー。（爆）

９７１　おっさんさま　　そうですねー。りょうちゃん自身も気づかないほど香ちゃんの事を愛して、大切で、どうしていいかわからないくらいの愛なのかも知れない・・・。（あーーーー、うらやましい☆）ところで、これから本格的に暑くなるんだから、ちゃんと、食べないと倒れちゃいますよ！がんばって下さい！

９７０　ふじっちよさま　　バスでの出来事、体調悪い時は若い人だって座りたいですよね。私ぐらいの年だと（更＠期？）かえって８０歳くらいのお年よりの方が元気だったりするよ（笑）まぁ、今度は何かいい事がありますよ♪

９６９　たまさま　　出たなぁー、あざらし！しかし、鼻水ねー、本気で泣くと出るよねー。まったく、よくそういうコトを思いつくなーと、おばさんは感心。

９６７　　無言のパンダさま　　そうだ、次号は巻頭カラー。少なくとも香ちゃんはトビラになっていてほしいですー！（熱烈願望！）

９６４　将人さま　　そうですねー、考えてみれば、バンチが火曜発売の時は、私月曜に見てて、解禁が水曜だった（ですよね？）あの頃に比べれば、今の方が救われます。（笑）

それではこれにて。うっとおしいお天気。パァーッと晴れてほしいなぁ。

### [10974] 圭之助（けーの）	2003.07.01 TUE 14:39:01　
	
おこんにちわ！梅雨が復活しましたね～うっとしい雨です。
昨日、バン懸のハガキ出してきました。当たらないかな～(^-^)

＞ふじっちょさま
あるね、電車とかバスでのひとコマ！若者全てがいつでも元気ってわけじゃないんだよねぇ…。で、席譲ってもらって当然ってご年配者も、やはり存在したりするわけです。今回は「間が悪かった」のよ、ふじまるクン。きっと、そのおじいさんは何処かで何らかのバチが却ってくるよ。気にしなさんな☆

＞たまさま
圭之助も鼻水カピカピ同類です…。

＞おっさんさま
圭之助もココ最近、イマイチ食欲が沸きません…いやいや食べてるって感じ。ごはん食べることに意欲がわかない…（Ω_Ω）クスン…。あ？おっさんさまは「食べてない」んだっけ！（とんだ勘違い☆）

＞千葉のOBA3さま
バン木も内容によっちゃ酷ですね。悶絶マダムの姿が脳裏に浮かぶわ。お顔がわからないから、頭部が強大な「？」マークなんですが。

＞みろりさま
「文句があるならヴ○ルサイユへ…」ポ○ニ○ック夫人？懐かしい…（笑）控えめそうに見えてかなりしたたかな御婦人でしたねー。って、一体だれのことを？！

### [10973] みろり	2003.07.01 TUE 03:41:23　
	
おこんばんわ～かな、おはようございますかな
のぉぉおおお、今夜はすごいラッシュ！まぁぜて！どっぽ～ん。
知らない古参の皆様、はじめまして。６月からお邪魔してるみろりであります。ヨロシク～
明日、ってもう今日か、クーラーの修理の業者が来るんで家中掃除をしとりましたらこんな時間だわ！午前３時というのは何かをはじめるにしても早すぎる時間で、何かを終わらせるにも遅すぎる時間だわ(ｂｙ某メロドラマ）

＞[10963] 千葉のOBA3さま、[10964] 将人（元・魔神）さま
優しいお言葉ありがとう～ございます

次も＞[10963] 千葉のOBA3さま
＞☆（オーーッホッホ←マダム笑い）←をすると「文句があるなら○”ェルサイユへいらっしゃ」といいたくなりません？

＞[10965] ayakoさま
アタシも、最初の初恋（！？）の時そんなことありました。我慢できなくって２冊目買っちゃったなぁ
で、２週間ぐらいして手元に２冊そろった時は一冊を切り刻み用にしました。気に入ったショットを切りとって定期に入れたりして、一冊を保存用にしました。

＞[10966] 葵 さま
工事終了おめでとうございます。でも済みません、953の某パンダサマの発言読んで「え？葵さまって♂だったの？」って思っちゃいました～

＞[10967] 無言のパンダ さま
ＧＮＰ気に入って頂いて恐れ入ります。
ところでＫは可愛いでもか弱いでもいいそうですが(凶暴説もあり？)ＭはＭＡＺ○じゃいけない？…かな？やっぱり…
あ、伏字になってない…

 [10970] ふじっちょ さま
へぇ～、おちゃらけて見えて、結構、深慮遠謀して「マダム」とか「セレブ」とか言ってるのね。
よしよし、みろりの11番目の初恋の君にしてあげよう。・・・いま、銀ぎつねに香の写真見せられて勝負を挑まれたリョウのようなカオしてないかい！？

コレから4時間ぐらい寝て、業者迎えて午後から出社かぁ～。きついなぁ～。休みた～い

### [10972] 無言のパンダ	2003.07.01 TUE 01:45:14　
	
今夜は、やたらと出現！ＭＫＰです。

＞たまさま（９６９）
たまたまたまに出現たまさま～！やっと出てきたかと思ったら「鼻水ネタ」かいっ！？(~_~;)たぶんリョウちゃんは鼻腔がつまっていたのだと思うが(^_^;)ドラマなどの涙のシーンでの「鼻水」ってのは重要ポイントだと思うぞ☆何を隠そう私は「鼻水が出ているか否か」で嘘泣きかマジ泣きかを判断してるのだ！もちろん「鼻水」アリがマジ泣きよね？はい、ティッシュ。

＞ふじっちょさま（９７０）
＞そんなんじゃ愛しのリョウは愛してくれないぞ(笑)
いや～ん！ごめんよ～リョウちゃん！ダイヤモンドに目がくらんだパンダを許してぇ～☆・・・って、リョウだって銀行の預金残高を見て一喜一憂してるじゃない、きっとわかってくれるわさ♪（余裕）
※そうそう☆バスの中での一件、なんとなくわかる気がするよ。いっそのことおでこに「頭痛」って書いときゃよかったね。いや、それとも「肉」って書いたほうがウケるか？(^_^;)
昔、私のクラスメートは何が原因だったか忘れたけどバスの中でオバさんに「親の顔が見てみたい」と言われ「見せちゃろーっ！」とそのオバさんの手をつかんで引っ張っていったらしいぞ。で、キミは暴れたりしなかったんだろうね～？こんなふうに・・・(☆□◎；)シュパ！！≡≡(#▽皿▽）_/))"

＞おっさんさま（９７１）
ごはんが食べれないなんて、忙しさと暑さのせいでしょうか？ますますやせ細っちゃうよ～がんばって食べとくれよ～(*'へ'*)
ところで、でこぼ○フ○ンズ（Ｂｙお○さんといっしょ）ってとこの「お○さん」をつい「おっさん」と読んでしまったよ(^^ゞ
本当は○＝母でしょ？（実はよく知らない^_^;）

それでは、みなさま☆おやすみなさ～い。(-_-)zzz

偶尔出现的样子~ !好不容易出来了，“鼻涕梗”啊!?(~ _ ~;)我想应该是小獠的鼻腔堵塞了吧(^_^;)在电视剧等流泪的场景中，“流鼻涕”是很重要的一点☆别隐瞒什么，我是通过“有没有流鼻涕”来判断假哭还是真哭的!当然是“鼻涕”真的会哭吧?喂，纸巾。




### [10971] おっさん（岐阜県：たまには・・）	2003.06.30 MON 23:38:49　
	
毎度、おっさんで～ごじゃいます。ってここ何日か生きてないってな感じです。ご飯がまともに食べれない・・・つ～ことで、おっは～っす（テンション低い）

ちょっとだけ、遅くなりましたが感想を・・・
りょうの涙ですか・・・皆様とほぼ同じ感想になるかと思いますが、ビックリもありますが夏目さんと同様りょう本人は自分の涙に気づいてないですよね。（多分）深い愛情ですか、りょう自身も気づかないほど香に対しての愛情はあるのではないでしょうか。愛するものにまた出会えたというか愛しかったというか、はかりしれないものがありますね。何となくそう思ってしまいました。

さすらいのやさぐれあざらし　たまっぴぃ～（９６９）、自分すたこらと逃げないの！！レスしなさ～い！！


それじゃ～またね～！！ダッダ～ン　でこぼ○フ○ンズ（Ｂｙ　お○さんといっしょ）

### [10970] ふじっちょ	2003.06.30 MON 23:37:24　
	
こんばんわ～(-"-)

頭痛＋バスで良識のある？おじいさまに注意（文句）されて肉体的にも精神的にも疲れきっています…仕方ないのよ頭いたくて他人にまで気が回らなかったのよ、おじいさまがいるのはわかっていたんだよ…席譲る気力が無かったんだよ(T_T)でも俺が降りるときすれ違いざまに「気の利かん人やな」って聞こえるように言うなよ！(-"-)「譲られて当然！」みたいな言い方にカチンときてしまったふじっちょでした…（普段の僕はいい人全開で進んで席は譲りますよ！今日はたまたまこうだっただけ…-_-;）

＞[961] ヒデジローさま
コラッ勉教しなさい！！(笑)かという僕も勉強のふりは大得意でした（爆）けど親に見つかって血祭りに上げられた事は言うまでも無い…
僕は香の死はあっさりと受け入れられましたよ。北条先生も「今度CHを書く時は新設定で描きたい」とインタビューなどに書いていられましたからこんな展開もアリなんだって納得できました(^_^)

＞[962＆962] みろりさま
僕もよく千葉のOBA3さまの「さま」忘れかける事があります(^^ゞだから、葵さまみたく「マダム」なんかに変えてみたらいかがでしょうかね？ねえ、千葉のセレブさま？
あと、MKPは実は「マジで Kyouaku パンダ女王さま」が正解…ガチャン！強制(▼▼メ)(T△T||)(▼▼メ)連行中

＞[963] 千葉のセレブレティさま
そうか～一日早く手に入るとそんな弊害が起こるのか…僕だったら我慢しきれずに何か一言かいちゃいそう(^^ゞ（こらえ性無いもんですから…笑）

＞[964] 将人（元・魔神）さま
いや～(^_^)＞まあ、あれと比べるのは明らかに間違いですよね(笑)こちらは地道に１００問目指しましょうあと半分切りましたしね(^o^)丿（あれ？昨日と書いてる事一緒…汗）

『冷やしもっこり』今年は無いのかな？それとも新メニュー
が出るのかな？

＞[965] ayakoさま
僕も昔CHの２６巻友達に貸して半年ぐらい返ってこなかった事があります。もう返って来ないと覚悟して古本屋で再び買いましたよ、でも１週間後に返ってきました(T_T)
その友人とは持ちつ持たれずで借りパクのしあいしてます(笑)

＞[967＆968]無言のシンデレラもどきさま
＞女は「愛」だけでも「お金」だけでも生きていけないのっ！
そんなんじゃ愛しのリョウは愛してくれないぞ(笑)
抜刀したのはあなたさま！！なのに何で俺が連行されてんの？「銃刀法違反」で逮捕されるのはMKPさまだ～
強制(▼▼メ)(▽皿▽#||)(▼▼メ)連行中

＞「主人公よりも好きなキャラクターベスト３０」で「ドカ○ン」の岩鬼クンが出てましたねぇ～♪それも「玄田哲章さん」の顔出しで！かなり珍しくないですか～？

しまった！(゜o゜)TVあっち見こっち見してたら見逃してしまった(-_-;)特にどうせCHとか出ていないと踏んでいたから前半見てなかった…不覚(T_T)

長々と書いていたら若干ストレス発散になって気分が良くなった(^o^)今夜はこの辺でサヨウナラ(-o-)／

### [10969] たま　（久しぶりに出現）	2003.06.30 MON 23:14:18　
	
今週のＡＨは
まさか×２、僚ちゃんがあんなにポロポロと涙を流すなんて思ってもみなかったから、かんなりビックリ＆号泣してしまいました。
（僚ちゃんはさすがに鼻水は出してなかったな。あたしゃ鼻水までしっかと出ましたぞ。もう鼻の下がカピカピやわ）

悲しすぎ。切なすぎ。僚ちゃんの涙。(ToT)
あぁ～・・・心が苦しい。僚ちゃんの涙は私の心にも響・響・響。

ついでにもう一つ、
皆様の感想が素敵すぎて、感想読んでまた涙。
（皆様の感想をつなげたら一つの超大作が出来るかも）

皆様の感想を読んでたら、ついつい書き込みたくなる症候群になってしまいました。
かといって、レスするかと思いきや、あらよっとスタコラサッサ・・逃々
（このアザラシはレス無しで逃げるつもりらしい。）

では、失敬。
（何か書き方が変じゃないかしら？汗。）

这周的AH
真没想到獠会流那么多眼泪，吓了一跳，号啕大哭起来。
(獠果然没流鼻涕啊。鼻涕都流出来了。鼻子下面都快冻僵了。)

太悲伤了。太难过了。獠的眼泪。(tot)
啊~···心里好难受。獠的眼泪也在我的心中回响。

顺便说一句，大家的感想太精彩了，读完感想又流泪了。
(如果把大家的感想联系起来，也许能做成一部超级大片。)

读了大家的感想，不知不觉就产生了想写的症候群。
我还以为你会回帖呢，哎呀，快跑吧!
(这只たま似乎打算在没有回帖的情况下逃走。)



### [10968] 無言のパンダ	2003.06.30 MON 22:58:15　
	
そういえば☆昨日の「主人公よりも好きなキャラクターベスト３０」見ましたか～？ＣＨキャラもＣＥキャラも入ってなくて残念だったんだけど「ドカ○ン」の岩鬼クンが出てましたねぇ～♪それも「玄田哲章さん」の顔出しで！かなり珍しくないですか～？(^○^)

### [10967] 無言のパンダ	2003.06.30 MON 21:33:52　
	
こんばんわ★

今日は涼しい一日だった。明日はまた雨らしい・・・(*'へ'*)

＞みろりさま（９５６）
ＭＫＰの解釈は「か弱い」でも「可愛い」でも良いのですがＧＮＰ「実はナイーブなパンダ」も大当たりよ～ん♪←結構お気に入り☆
 [９６２]での「読者の心の動きに無関心でいてどうして、あのような私達を感動させる作品が描けるでしょう」って言葉はなんだか説得力あるわぁ～！(o^-')b

＞ふじっちょ夜更かし大王さま（９５８）
女は「愛」だけでも「お金」だけでも生きていけないのっ！シンデレラだって相手が王子だから惚れたのさ。ふふん♪（｀ε´）
※ところで千葉のＯＢＡ３さま（セ、セレブ～？！）へのレス中の「るろうに＠－ちゃん」って誰？！なんでそんなコワイ顔なんだ？あれはもう「逆○刀」じゃないね真剣だよ、真剣！「銃刀法違反」で逮捕じゃーっ！
強制(▼▼メ)(￣▽￣||)(▼▼メ)連行中（←「ふ」のつく人です。断じてＭＫＰじゃないぞ！）

＞葵さま（工事って♂から♀に？！冗談よ～♪^_^;）（９６０）
・・・ちょっと上↑で遊びつかれた・・・っていうのに「因幡の白兎食っちまった」だなんてまた誤解を招くようなことを！あれは単なる「やさぐれ番長」の妄想じゃん！(*/□＼*)うわぁ～ん （たまねぎなら食うぞー！それも「バン金似」のタマネギ♪（笑い））

＞千葉のＯＢＡ３さま（９６３）
今週のＡＨは巻頭カラーなんだよね！そこに描かれるのは「リョウと夏目さんの出会い」なのか？それとも香瑩を加えた奇妙な関係？(^_^;)それともリョウと香ちゃんが抱き合う「幻」のシーンとか？それはとても見たいけど切ないな・・・(σ_-)
※やったー！パンダ―Ｚ缶バッチゲットだーっ！（？？？）

ではまた。(^_^)/~

### [10966] 葵	2003.06.30 MON 21:18:33　
	
　わぁ～い、思ったよりあっけなく工事が終わり（といっても、補修だから完全ではないのだが☆）こうして無事にＰＣをいじれますぅ～極楽×２♪

＞千葉のマダムさま［９６３］
誰じゃいっ！室内に檻をつけるって言ったのはっ！（「マダム」の称号、剥奪よ☆）あーでも防音てのはイイかも。週末のチャットで、知らないうちに独りごと言ってる葵であった…。(;^_^A

＞ａｙａｋｏさま［９６５］
うわぁー新刊＆｢バンチ」を含むずべてのＡＨを貸して、返ってこない…お察ししますー…。キチンとした、マメなお友達？そーゆー場合はね、嫌がられても１・２冊ずつ貸したほうがイイです。でないと自分の禁断症状が押さえられなくなっちゃうからね。え、もうなってるっ？！（○_○）!!

### [10965] ayako	2003.06.30 MON 20:36:49　
	
みなさん…グスッ(T_T)こんばんわ。
実は一昨日、ＡＨ全巻と最新号のバンチと前に買ったバンチを友達に貸したんです…昨日返ってくると予想していたけど返って来なかったんです。今日は返ってくるだろうと思ってたのに今日も駄目でした…我が家には今、ＡＨが何もない！悲しいなあ。寂しいなあ。今週号見たいよう…あまりの見たさに買ったのに立ち読みなんてしてきちゃって悲しいなあ。買ったのになあ。明日はその人と会わないから返って来ないし。「面白い。ハマる」と言ってくれるのは大変うれしいが頼むから早く返してくれ！無いと余計に読みたくなるんです。

### [10964] 将人（元・魔神）	2003.06.30 MON 18:09:27　
	
こんばんは、将人（元・魔神）です。

＞[10961] ヒデジロー様　2003.06.30 MON 00:23:58　
＞またまた皆さんに質問。
＞北条先生が掲示板に顔を出してくれたことはあるのですか？

グスン(泣)(TT)僕は、[10951]でヒデジロー様の質問に答えたつもり
だったのに、読まれていなかったんだぁ～(泣)(; ;)ホロホロ
→でもマジで怒っているワケではないですよ。冗談ですよ。冗談。

北条司先生は「もっこりメッセージ」の過去のメッセージのログ1の
「メイキング」2001.07.29 SUN 00:05:46　という話題で
あまり自分の制作裏話や苦労話は書かないような事を書かれています。

でも実際は、フルカラー版の本の話や、シティーハンター2の最終回
の絵の話などのシティーハンターなどの漫画の裏話だとか、
連載中のエンジェルハートが雑誌に掲載された時期に合わせて
に関連する「色男は太ってはならない!」とか、『冷やしもっこり』
はじめました。という事や、虹の橋の話など、色々なメッセージを
書かれていますよ。

シティーハンターの続編らしい話が、新しい雑誌に連載されている
事を知って喜んで読んだもののエンジェルハートの
「香さんが亡くなる」という衝撃的な内容を読んだ時は
僕もショックでしたよ。今はエンジェルハートも好きになって
読んでいますが、エンジェルハートを認められない・今後は読まないと
言っていた友人もいました。

＞[10956][10962] みろり様　2003.06.29 SUN 14:41:43　
ハンドルネ－ム省略や呼び捨て、かまわないですよ。
僕のハンドルネーム長いし、この掲示板では似たようなハンドルネーム
の方がいないので間違えないと思いますから、誰宛か分ればいいですよ
（→他のジャンルのＨＰで「魔神」のＨＮの人が多くて紛らわしくて
変えた名残で「元」が付いていますので）

ヒデジロー様への僕の返事レスのフォローありがとうございます。

＞[10958] ふじっちょ様　2003.06.29 SUN 17:35:07　
他のクイズと比べるつもりじゃなかったんですけどね(汗)(^^；
同じクイズのCGIを使ったHPが参考になるかなぐらいだったので
問題数を比べても～(汗)(^^；　がんばってクイズ100問作りたいですね

＞[10963] 千葉のOBA3様　2003.06.30 MON 15:14:27　
バン木（バンチの発売日が木曜日）でいいな～って思っていましたが
掲示板で1日分多く、書き込むのをガマンしないといけない時間が長い
というデメリットもあるんですね。

### [10963] 千葉のOBA3	2003.06.30 MON 15:14:27　
	
こんにちは！暑いーーー！遅い昼ご飯を食べたら、汗ダラダラ・・・。夏なのねーーー。

９６２　みろりさま　　呼び捨てなど気にしないで。なんせ自分で３（さん）付けちゃってるくらいだから。（笑）ところで、総武線に乗ってバン木したいって？きっと他にもあるんだろうけどネー。バン木できる所。そう、でもバン木しちゃうと、今回のような、「わーー！」って言う内容の時、ネタバレしないように平然としてるのは、すごく苦しい・・・。でも、皆さんより一日早く読んでるんだから、それはガマンして・・・。というカンジです。

９６１　ヒデジローさま　　あれ、今期末試験中かな？がんばってくださいね。うちの次男はもう終わって、残りわずかな部活動に専念してます。・・・ところで、ＡＨを知って香ちゃんが亡くなったという事実にご飯もノドを通らずって、私もそうですよ。おかげで随分痩せました・・・？（もう戻ったけど？）

９６０　　葵さま　　ＰＣルームの壁を工事？檻つけるとかぁーー、葵さまがＰＣいじりながら叫ぶので防音にするとか？ところで、バン金似ってナニ？

９５８　　ふじっちょさま　　大丈夫だって夜更かしの理由をみんなの想像にまかせたって・・・・。ゲームしてたんでしょ☆（オーーッホッホ←マダム笑い）

９５３　　無言のパンダさま　　よっしゃ、パンダーゼットの缶バッヂ当たったら一つ進呈しよう。でも、当たるかなぁー。某田舎の一日に一便しか集配に来ないポストに懸賞ハガキ出しちゃった。（汗）私も、今週号のりょうちゃんに嫉妬はないと思います・・。「こんな所で香に会えるなんて。」って言ってるし、ホントにどんなふうに夏目氏と対面するんでしょうねぇー？

９４７　ｙｏｓｉｋｏさま　　ひさしぶりぶりー！またちょくちょく顔だして、ボケかまして私を元気づけてーーー！！

で、遅れましたが初めての皆さん、はじめまして千葉のＯＢＡ３ですよろしくーーー！ここの平均年齢を一人で上げている者です。（笑）
・・・それでは本日はこれにて・・・。

### [10962] みろり	2003.06.30 MON 12:38:41　
	
＞ 千葉のOBA3さま
すみませ～ん、ごめんなさい、【956】のカキコで呼び捨て！敬称なし！大変失礼しました。
そのすぐ下でも、将人（元・魔神）さまにハンドルネ－ムの件で謝罪してるのに、なんてことしてしまったんだ！あたし、何様！ほんとに失礼しました。

＞961　 ヒデジローさま
＞北条先生が掲示板に顔を出してくれたことはあるのですか？
この件は[10951] 将人（元・魔神）さまが答えてくれている通りだと思いますよ。私も新参者で過去ログ全部読んだ訳じゃないけど。通常この手のBBSに漫画家さん本人は書き込まれないと思います。何を書いても蜂の巣をつついた様な大騒ぎになるだろうし、かにレスしても不公平になるし。作品以外で何か仰りたいことは「もっこりメッセージ」にお書きになるでしょう。でも、私も将人（元・魔神）さま同様、私達の反応をチェックはしてくれてると信じてます。読者の心の動きに無関心でいてどうして、あのような私達を感動させる作品が描けるでしょう。
でも、本音を言えば1年に1回でもいいからここにカキコしてくれたら、うれしいですね♪

＞北条老师在公告栏上露过脸吗?  
我认为这件事就像将人(原魔神)大人回答的那样。我也是新人，过去的日志也没有全部读过。通常这种BBS漫画家本人是不会被写入的。不管写什么，都会像捅了马蜂窝一样大吵大闹，回复也会不公平。除了作品以外，还有什么想说的，可以写在“托词”里吧。但是，我相信我和将人(原魔神)一样，也会检查我们的反应。如果对读者的内心活动漠不关心，怎么能写出那样让我们感动的作品呢?
但是，说实话，如果能在这里写一年一次也好，我会很高兴的。

### [10961] ヒデジロー	2003.06.30 MON 00:23:58　
	
書き込みがまたまた二日ぶりになってしまいました・・・
やっぱり北条先生の漫画は面白い！！！
ココだけの話、今日の朝勉強している振りしてずっとエンジェルハート６巻読んでました。
あの二年目の阿香。変わりましたね～
さらに楽しくなってきそうですエンジェルハートも！
最初エンジェルハートを知ったのは、友達が教えてくれたからなんです。
教えてもらう前まではずっとシティーハンターにハマっていて、エンジェルハートの存在を知ったときにとても感動しました。
しかし、読んでみたら「香が死んだ」という事実にショックの連続！その日は昼飯が食べられませんでした。（本当です）
しかもリョウらしさが、ぜんぜんみられない！（もっこりの部分で）しかもハンマーの登場回数が激減！！！
しかしだんだん読んでいるうちに北条司ワールドにどんどん吸い込まれていきました。
今では毎週エンジェルハートを楽しみにしています！
・・・とか、僕とエンジェルハートの出会いを長く語ってしまいました。
またまた皆さんに質問。
北条先生が掲示板に顔を出してくれたことはあるのですか？

帖子又隔了两天了…
北条老师的漫画果然很有趣! ! !
今天早上装作学习的样子一直在看天使之心第6卷。
第二年的阿香。你变了呢~
天使之心也会变得更加有趣!
最初知道天使之心是朋友告诉我的。
在被告知之前，我一直对城市猎人很着迷，当得知天使之心的存在时，我非常感动。
但是，读了之后“香死了”的事实震惊连续!我那天没能吃午饭。(是真的)
而且完全看不出有獠!(有麻烦的部分)而且锤子的登场次数剧减! ! !
但是，在阅读的过程中，我渐渐陷入北条司的世界。
现在每周都期待天使之心!
···啦，我和天使之心的相遇说了很久。
再次向大家提问。
北条老师在公告栏上露过脸吗?