https://web.archive.org/web/20030901183756/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=518

### [10360] スウ	2003.04.23 WED 17:16:59　
	
お久しぶりです。
花粉症と寒暖の差が激しくて、嫌になってくる今日この頃です。iバンチは契約してから自分の機種が旧機種だったことに気づき、ちょっとだけブルーな気分になりました（笑）
説明読まない自分がいけないんですが（＾＾；

 [10354] ボノ様
はじめまして～！
インタビューでそのようなことをおっしゃっていたとは…。
ちょっと残念な気もしますが、よく考えるとそれが最善なのかもしれませんね。やはり、声が…ｗ
アニメ化してからそのキャラのイメージが壊れた、なんて話もよくありますし…。
らん○1/2みたいに完璧に入れ替わってるわけじゃないですもんね（＾＾
でも雅彦や紫苑を初め、紫さんとか空さんの声を、北条先生はどういう風に想像して描いていたんでしょうか…。
結構気になりますが、それはやっぱり読者一人一人の想像によるって感じなのかな…。

### [10359] 将人（元・魔神）	2003.04.22 TUE 21:37:40　
	
こんばんは、将人（元・魔神）です。

＞[10351] たま様　2003.04.20 SUN 01:20:01
「ｉバンチ」って会員限定なんですね？→携帯持ってないので(汗)(^^；
会費300円って1回のですか？月会費？年会費？
「MOKKORIハンター」ってゲーム僕もやってみたいな～！
でも、それにはド＠モの携帯で、しかもほぼ最新機種でしか出来ないなんて
どの業者のどの携帯でも(→PHSでも)出来るようにして欲しいですね。
この掲示板のようにパソコン用も欲しいですよ→掲示板は携帯用も希望！

＞[10352] のっつぃ様　2003.04.21 MON 01:55:39　
PCエンジンのゲームは、２人モードで海坊主さんも出ていたんですか？
見てみたいな～！→中古ゲーム屋さん覗いたけど無かったですね。
中古ゲーム機とか、すごく高いですし(汗)(^^；

＞[10354] ボノ様　2003.04.21 MON 23:56:32　
スマスマに神谷明さん出ていたんですか？見たかったな～！
キムタク同じ格好したドラゴン＠ール版は見たんですけど・・・。

＞[10355] 無言のパンダ様　2003.04.22 TUE 00:11:31　
あちゃ～！みずから言われましたか？文字サイズの事、この公式BBSで
言って良いかどうか某チャットで、ちょっと聞くつもりだったんですが
それが皆さんに老眼ってイジメられましたからね～！
だからこっちではツッコミ言わないつもりでしたが・・・(汗)(^^；

無言のパンダ様が老眼じゃなくてパソコンのIEの設定は標準でしたよ
IEの「表示」→「文字サイズ」→「最大・大・中・小・最小」で
このBBSの文章の折り返す部分は、変わりますよ。

→僕のパソコンは15インチCRTで、画面は1024x768サイズだけど、
それの大きさの設定によってもたぶん変わると思います。
使っていないけどネスケとかOperaとかいうブラウザーでは文字だけで
全体のデザインを何％とか拡大縮小出来るらしいですね。

今度のバンチ「蒼天の拳」のポストカードが付くんですか？
AHもその次週に付くのかな？

＞[10356] 圭之助（けーの）様　2003.04.22 TUE 15:19:42　
カミソリで切って足から出血ですか？コワイですね。
圭之助（けーの）様のパソの文字サイズもたぶん標準だと思いますよ。
→[10345] 葵様のコメントは文字サイズ「小」の設定だったと
某チャットで聞いていますので・・・。

＞[10357] 葵様　2003.04.22 TUE 19:26:55　
記念切手に「科学技術とアニメヒーロー・ヒロインシリーズ」ですか？
たぶん化学技術とついているから、40年以上たって今年に誕生日を
向かえたア＠ムが予想出来るんですけど（と言うかHP名前を書いていたから）
後は何が出るのかな？CEやCHも出て欲しいですね。

### [10358] 圭之助（けーの）	2003.04.22 TUE 21:16:11　
	
＞１０３５７　葵さま
特殊切手。そういえば、チョイ前にも何作かシリーズでサザエさんとかガンダムとか出てたような…違ったかな？
文字サイズ見たらば「中」でした。「小」「最小」どっちがどの位違うんだろ。ちなみに葵さまは何表示？

足はムダ毛のお手入れしようとしたのよ～。でも、圭之助ド近眼じゃん？風呂屋さんではメガネしてないから、どこ切ってんだか見えない（笑）足の悪い母を番台まで、ばんそうこう取りに行かせて、おまけに貼らせました（だって傷口わかんないんだもん）
ちょっと力加減まちがえちゃったのよ。

昔は、誰かが捨ててたカミソリに気づかないで踏んづけて切った。お風呂屋さん通いの方、最低限のマナーは守ってね… (-.-;) 皆がみんな目の良い人ばかりじゃないんだよ☆

### [10357] 葵	2003.04.22 TUE 19:26:55　
	
　いいお天気でした。のんびりと美術展巡りをして、ちょっとリッチな気分です。（え？似合わん？！）

　さて、文通魔の葵は郵便局（あ、今は郵政○社か☆）で切手のまとめ買いをしてきました。そこで今後の特殊切手（いわゆる記念切手）発売予定表なるモノを見てたらナント!!!「化学技術とアニメヒーロー・ヒロインシリーズ」なるものが出るというじゃないですか！しかもシリーズなので、当然のコトながら何作か出るわけで…そしたらいつかはリョウちゃんも？！…と、のけぞっちゃいました☆化学技術ってのは何でしょうね。「○ッチャマン」とか？「ギャ○ン」？それとも「ヤマ○」ですかね？発売は今夏８月１日に第１集が、そのあとは一ヶ月おきのようです。気になる方は郵政公○ＨＰへレッツゴー!!!

＞無言のパンダさま［３４８＆３５５］
　老眼パンダさま、ご自身でツッコミ入れて答えを出して…ご苦労さまでした。(笑)

＞圭之助さま［３５６］
　足から出血？！何をしたんだ？！そんなもったいない出血させるなら、献血しなさい！(笑)文字サイズは「表示」から「文字サイズ」を選べばイイのだよ♪

### [10356] 圭之助（けーの）	2003.04.22 TUE 15:19:42　
	
しばらく読む側にまわってました。いや、ネタが無かったもので（汗）バチが当たったのか、昨日風呂でカミソリで足切っちゃたよ♪温まってたから血が出ること☆

＞１０３５５　無言のパンダさま
圭之助のパソも文字サイズ、デカタイプみたいです。どーやったら変えられるのぉん？？まあ、特に支障はないけどさ☆

＞３５４　ボノさま
はじめまして…かな（もう、わかんないや　(^^;)）
圭之助もその放送、見てました。でも悲しいかな、今ほどＣＨにのめってなかったようで、録画してませんでした…今となっては残念無念… (T_T)

＞３５２　のっつぃさま
そのＰＣゲームソフトかどうかは分りませんが、先日オークションで開始価格１９８０円～っての、みかけましたよ。なんか希少価値みたいですね。

### [10355] 無言のパンダ	2003.04.22 TUE 00:11:31　
	
こんばんわ★

昨日も今日もさみしすぎっ！(>_<)
みんな！ＣＯＭＥ　ＯＮ！！だぞーーーっ！

☆みなさんはこのＢＢＳの文字サイズが変えられることをご存知でしたか？！私は土曜日にその事実を知り、愕然といたしました！(T_T)先日の葵さまのカキコ（３４５）につっこんだのはイイけど、どうも葵さまのＰＣでも文字はちゃんと1行目ラストになっていたそうで・・・。私が書いたように「ツッコミ部分」が二行目の初めになっている人は文字が大きいらしいです！だからって私は「老眼」じゃないですよっ！(@_@)（このネタでさんざんいじめられたのだ）さあ！みなさんの見ている文字サイズはどっちですか？！

☆次号のバンチには「蒼天の拳」のポストカードが付くそうですが、じゃあ！もしかしてその次はＡＨのが付くのかな～？

### [10354] ボノ	2003.04.21 MON 23:56:32　
	
＞里奈様
ケンシロウの登場は、宿題をみんなやってこなかったんで温泉マーク（温泉のマークが入った服をいつも着てる先生）が『宿題を忘れたヤツは名乗り出ろ！タダじゃすまさんぞ。』と言ったら、パーマ（パーマ頭の生徒）が『やや、これはこれは宿題を忘れたケンシロウ君。』と言って出てきました。当然先生はビビって許しました。
他、めし（たしかキツネうどん）をめぐってコタツネコとゆー猫と戦い『アチャチャチャチャチャチャチャ』と善戦しましたがあえなく敗れ去りました。


昨日北条先生の２０周年記念イラスト集を読み返したらインタビューで『Ｆ.ＣＯＭＰＯは映像化できないしアニメにしてもおもしろくない』と答えてました。声が無理だそーです。たしかに、性転換しても声までは無理ですもんね。

以前スマスマに神谷明さんが出てたの覚えてます？たまたまＨＥＲＯのビデオを漁ってたら発見しました。キムタクがキャプテンハ－ロックの真似をしててそこへ何者かから通信が入るって設定です。まずキン肉マンの声真似をしてキムタクが『もしかして毎日牛丼喰ってません？』みたいなコトを言うんです。
で、少しカラんで、『でも私はこれだけじゃないんだよ』と言っていきなり『香ぃー』と。で、キムタクがビビりながら『もしかしていつもトン単位のハンマー受けてません？』と。で、『いつも同じ服ですね。』と軽くツッコんだ後に『あれ言ってください』と言って『リョウちゃんもっこぉ～りっ！』と。微笑ましかったです。他にもケンシロウや面堂終太郎らが出てきました。

キムタクって年齢的にも、ひょっとしたらＣ.Ｈ.ファンかもしれませんね。

### [10353] 里奈	2003.04.21 MON 18:53:32　
	
香ちゃんステキ…ポッ　（感想…？笑）

☆１０３５２　のっつぃ様
へぇー！そのＣＨゲームって２プレイだと遼と海ちゃんでできるんだ！でも香ちゃんのほうがイイなぁ～（笑）エンディング気になるぅぅー！見てみたいわぁ～♪

☆１０３５１　たま様
うっひぃぃぃー！里奈の出没率が下がってきたから、たま様からビッシバッシとお怒りメ～ルが届いたでやんす（汗）
たま様はｉモードかい？だったらｉバンチ見れるんでしょ？迷ってないで登録しちゃいなよぉ～！里奈なんてドコモじゃないから見れないんだよぉー！（怒）ぶっひぃぃー！

☆１０３４６　Ｒｏｕａｒｋ様
そっかぁ～。いろいろ考えたら、確かに掲示板を携帯サイトで見るってのは難しいよねぇ～☆でも、掲示板じゃなくてもｉバンチみたいにゲームができたりする携帯サイトが欲しいな。

☆１０３４４　葵様
　１０３４２　圭之助様
里奈の兄上もＣＨファンだからねぇ～。何度か『一緒にカキコしようやぁ～』って誘ってみたんだけど、あの人某有名車会社の営業マンで忙しいのよね…。だから こっそり里奈のビデオや単行本を盗んでは消えて行くの。
そういえば、脈を動かす技は幼い頃に兄上に教わったような…？

### [10352] のっつぃ	2003.04.21 MON 01:55:39　
	
こんばんは…。
レスのみで申し訳ないですが、書き込ませていただきます。

>野上唯香様
PCエンジンのゲームについてですが、なにぶん、のっつぃがまだ小学生だった頃の記憶でして(10年以上前)、とってもおぼろげですが、覚えてる限りで述べさせていただきます。
音楽は、どんなのが流れてたか、申し訳ないですが、覚えてません。少なくとも、サントラに収録されている物ではありません。ゲームの内容は、依頼を解決するというものでした。この依頼の内容まではしっかり覚えてないんですけどねぇ…。ただ、実際プレイした時の印象が、｢アニメで遼がやってたのと同じゲームだ!｣というものでしたので、出てくる敵を撃っていくという物だったと思います。確か、二人プレイにすると、遼と海坊主でゲームができたはず…。で、ステージの最後には、ボス。ステージ内に出てくる雑魚敵は、生身の人間で、銃を撃ってきたり、手榴弾を投げて来たりします。ボスは人間が操縦する、大きめの機械だったと思います(ヘリとかそういうやつ)。ただ、残念な事に、全面クリアすることなく手放してしまったので、エンディングは見ていないのです。
と、大雑把な記憶はこんな感じです。もう、本当にうろ覚えなので、他のゲームとごっちゃになってる部分もあるかもしれません。なので、信頼性は薄いかな…。お役に立てずすいません。

>里奈様
そぉなんですよぉ～…。小学生ののっつぃ(当時3年生か4年生)は飽きっぽくって我侭な子だったので、上手く行かないからってすぐに投げ出してしまいました…。本当に今となってはもったいない事をしたと思っております。そうして、CHゲームができる本体のPCエンジンDUO(これもまたレア)も、ソフト出ないしと思ってゴミに出してしまいましたわ…(-_-;)これは、つい半月程前の話です…。大学を卒業したからって大掃除をした際に…。
もう、本当に里奈様のおっしゃる通り、のっつぃはバカ者ですとも…。とほほ…(T_T)

では、長々と失礼しました。

### [10351] たま	2003.04.20 SUN 01:20:01　
	
一週間ぶりのゴブサターン♪
仕事始めて忙しくなってきた「たま」でございます。

今週のＡＨ
「香ちゃんカッコイイぃ～」この一言に尽きます。
この香ちゃんの姿を僚ちゃんにも見せてあげたい。
この場面に早く僚ちゃん現れないかなぁ～？
遠山に関しては・・・？
無言のパンダ様【10348】がすんごい見事な迷言、いや名言してるおられるので、その感想に便乗ぉ♪
そうだね。子供の「ママ・・ママぁどこぉ～？」っていう言葉に反応した遠山は、やっぱり幾つになっても激しく母親の愛を求めているのだね。

さてさて、
今「ｉバンチ」見てました。
アシャンの待受画像をダウンロードしようと思ったら、会員限定だそうで・・・。
３００円かぁ・・・。どうしようかなっ・・。
んでも、
「MOKKORIハンター」したぁ～いしなぁ・・。
「１００ｔハンマーで、新宿の街を騒がす不埒な奴を叩きのめす」（笑）
お試し無料期間とかあったらイイのになぁ～・・・。

### [10350] ぽぽ（Hyogo）	2003.04.19 SAT 17:57:27　
	
いやぁぁ～～～っ！！w(*O*)w
最後の行の顔文字、なまずひげみたいになってる！
涙です、涙です！(´｀;)A

### [10349] ぽぽ（Hyogo）	2003.04.19 SAT 17:50:31　
	
今週のＡ･Ｈ。

「私の娘に手をだすなっ！！｣
香ちゃんカッコイー！！香ちゃんステキ～～！！
ヾ(≧∇≦)ﾉ″(パフパフパフ！)
めためたカンドーし、先週に続いてバンチ購入しました♪

遠山を見ていて、ＣＨスペシャル「グッド･バイ･マイ･スイート･ハート」の武藤武明を思い出します。
10代のころ家を出て行方をくらまし外人部隊に入り、のちに「プロフェッサー」と呼ばれる殺し屋になった彼だったけれど、愛を得られず(？)憎しみを社会にぶつける悲しみが、遠山と似ているのかなと思いました。

武藤にとっての宝は妹の笑顔だったけれど、遠山はデビルクロウ。人との温かい触れ合いが少ない分、武藤より遠山は悲惨といえば悲惨･･･。
遠山が鑑別所（でしたっけ？その週買ってないので記憶があいまいです^^;）を脱走したのも、その時すでに両親から「あの子は自分たちの子じゃない｣と言われたからなんじゃないかと思い、悲しかったです (;_;)。

### [10348] 無言のパンダ	2003.04.19 SAT 03:06:01　
	
こんばんわ★

「今週のＡＨ」
遠山は、もしかして冴子さんに「母親」を感じていたのか？現実の彼の中では実体のない両親・・・母親・・・。
男の子が生まれて最初に恋をするのは「母親」だと聞いたことあるけど、それに似た感覚で冴子さんに恋していたのかもしれない。そして小さな子供が母親を独占したがるのと同じように冴子さんに近づく者を遠ざけ、冴子さんの気を引くために時に命を奪った・・・。
時は流れ、自分自身の命の終わりを察して切ないくらいに激しく母親の愛を求めている遠山。そんな彼がとても哀しく感じてしまった・・・。今まではあんなに憎らしかったのに。
彼の「生まれ変わりたい」という気持ちは、実はとても純粋なのでは・・・？なんだかそんな気がしてしまいました。
もちろん、その表現方法は間違っているし、奪った命はもう戻ってこないし許されないことだけれど・・・
今回の香の存在と行動に、彼は「自分の求めていたもの」を見たのかもしれない。そして・・・？

と、いうわけで考えれば考えるほど混乱してしまうけど(~_~;)あの香の母親としての気迫に遠山はどういう行動に出るんだろう？来週が気になるー！(>_<)

＞千葉のＯＢＡ３さま（３３９）
そう！手首の脈のところと、親指の付け根のあたりのふくらんだ部分の血管がピクピク動いてるのーっ！まるで、中学ン時に解剖した「フナの心臓」みたいにぃーーー！・・・はっきり言ってキモイ！(>_<)

＞圭之助さま（３４２）
ボードゲーム☆たしかに大きさもそのくらいで箱に入ってるけど、そんなイイものじゃないっす！(^_^;)五色のコマとサイコロが入ってるの。で、「星空のスイーパーゲーム」と「リョウのラブハンティングゲーム」なるものがシステム手帳みたいに入ってます♪もともと８００～９００円くらいのものだし・・・(^^ゞ

＞葵さま（３４５）
つっこむ気はなかったのですが・・・訂正箇所は「1行目ラスト」ではなく「二行目の初め」では～？(^_^;)

ではでは。(^_^)/~

### [10347] 将人（元・魔神）	2003.04.18 FRI 23:00:55　
	
こんばんは、将人（元・魔神）です。
遠山の行動、どんどん過激になっていますね。
親の愛は無くても同じゲームを作っている仲間意識とかも無かったのかな？
親も世間体があるのかどうか知らんが自分の子供がこんな事件を
起こしているのに10年前に（殺人事件を起こした時に）死んだと
平気で言えるなんて・・・。

アシャンを守ると言った香さんは、あの遠山にはどう映ったんでしょうね
また来週の続きが気になりますね。
あれ？来週はＧＷで合同号！って事はその次はまた２週間待ちなのか？

PS.あのゲーム会社の爆発シーンの冴羽さんのセリフ「ＤＣ～」って
ついドリ＠ムキャ＠トと読んでしまいました。
昔、湯＠専務という方がTVCMに出て有名になったゲーム機の・・・。
まさかミサイルで狙われて、＠がた三＠郎の捨て身の行動で助かった
TVCMが放送された、あの会社がモデルじゃないですよね？

＞[10340] 野上唯香さま　2003.04.18 FRI 16:44:34　
僕のＣＨミニコントのリクエストですか？
はぁ～あんまり、すぐに出来る自信無いな～！(汗)(^^；
僕は、ひねくれ者なんで野上唯香さんのミニコントが正当なＣＨ風の
直球だとしたら、僕は変化球でワザとちょっと違った事をする
クセあるからな～。余計に思いつかなかったりします。

冴羽さんにＣＥの内海俊夫さんとケンカとか100tハンマーの身代わりに
させたり、修学旅行で冴羽さん達が覗く話だったのを、老人達にFCの
オカマさん達を覗かせたりとか色々と変えたりする悪いクセが・・・。

＞[10341] Ｂｌｕｅ　Ｃａｔ様　2003.04.18 FRI 19:11:18　
アニメ版の「都会のシンデレラ」の回で、そんな事があるんですか？
・・・覚えていないから、僕は、たぶん見ていなかったんだろな～！
あの服のデザイナーさんの話は無しでいきなりそのエピソードなんですか？
全然、知らなかったな～！

ゲスト出演ではないけど、アニメ放送の数年後にある事件（←あえて
書きませんが）に関連してＣＨのアニメのフィルムの1コマかに、
ある人物の写真が入っていてニュースやワイドショー・雑誌などで
問題になった事ありましたけど、ＣＨのファンとしては、作品とかと
関係の無い部分で取り上げられたのが、悲しかったです。

（→昔のジ＠ンプの永＠豪先生のハレ＠チ学園の問題のように
冴羽さんの「もっこり発言」がＰＴＡの問題になってしまうのも
イヤですけどね）

### [10346] Rouark	2003.04.18 FRI 22:15:20　
	
---返信---
＞[10334] 将人（元・魔神）さま
＞出来るかな？
実は僕も、できるかどうか ちと不安です…

＞[10337] 里奈さま
＞でも何すりゃイイの…？
とりあえず、Contactのmailフォームから
対応をお願いするとか…ですかな。

＞[10341] Ｂｌｕｅ　Ｃａｔさま
＞もし、ここの掲示板が携帯対応になったら、
＞ますますカキコミが多くなって読むのが大変になりそう
確かに、そこが盲点なのですよね…100t掲示板って、
他のサイトの掲示板の、数倍の速さで書き込まれるから
（１日に２～３件は必ず書き込まれている；）
記事を携帯と共有にするとパケ代キツいかもしれません…

ともあれ、少人数の意見で行動を起こすのは
問題がありますので、少し様子を見たいと思います。
何人の方が携帯対応化に賛成してくださるのでしょうか…
賛成票が集まってきたらmailしてみたいと思います。

以上、Rouarkでした。

### [10345] 葵	2003.04.18 FRI 21:33:27　
	
　１行目ラスト、香ちゃんのセリフ「私の娘に…」でした。どこかの誰かにつっこまれる前に訂正です☆ドキドキ☆（^^;

### [10344] 葵	2003.04.18 FRI 21:31:29　
	
　解禁です。香ちゃん…すっかり母の表情になってますねー…しみじみ…。「私に娘に手をだすなっ!!」この声は、阿香にも聞こえてるのかしら。両親の愛を知らずに（ホントは李パパがいるけど☆）生きてきた阿香にとって、リョウの存在以上に、この言葉は嬉しいだろうなぁ。親が子を守る…そんな当たり前のことを知らずに生きてきた遠山にとって、香の叫びは彼の心を変えられるかしら。（現実問題、実態は阿香だけど）頑張れ、香っ!!!

＞里奈さま［３３７］
　兄上は立派なドロボーさんですな。さすが里奈さまの兄上や！（笑）脈でコインを動かす…「すごい人コンテスト」とかに出なよ！公式メンバー一同で応援に行ってあげるからさっ♪

＞Ｂｌｕｅ　Ｃａｔさま［３４１］
　そうそう、ＹＡＷ○ＲＡちゃん一瞬でしたねー。コートの秘密は、さすがにゴールデンじゃムリよ☆そういやリョウのパンツにも秘密があったっけねー…。（^^;（←ロキシア共和国・アマゾネスと対決のお話）

＞圭之助さま［３４２］
　耳栓で痒くなった？お気の毒☆ぐっすり眠れるとイイね。羊より「リョウが一匹、香が２匹…」と数えていくのもおもしろいかもね？！（よけい寝られないか☆）

### [10343] 圭之助（けーの）	2003.04.18 FRI 19:43:00　
	
あ、レスのカキコしてる間にＢｌｕｅ　Ｃａｔさまが…（笑）

### [10342] 圭之助（けーの）	2003.04.18 FRI 19:41:18　
	
解禁だ。あれ、１番のり？？（恥ずかしいじゃん…汗）

イヤだ、もう…遠山ァ～～～！！！ＳＥＲＧＡの社員サンたち、めっちゃ近くに集まってたから、そうとう怪我人でてるよね…怪我どころじゃないか…。感情のままに走ってったアシャン、リョウは追いかけなかったけど、香もいるしアシャンの腕なら大丈夫って信じてるから？
それにしても娘を守る香ちゃん、すごい！感動 (T_T)
母娘（親子）なら当然の感情なんだけど…親に見捨てられた遠山にはショックだろうね。理解できるのかな…。
香、がんばれ！でも遠山は正気じゃないからリョウ、早く二人を助けに来て～～！！！

＞１０３３１　kinshiさま　　　１ヶ月お留守ですか！淋しいよぉん (ToT) 早く帰ってきてね☆バンチは葵さまが買っててくれるでしょう。でも報酬は…特大ファルコンケーキで手をうつ？

＞３３６　無言のパンダさま
ボードゲーム、携帯できるよ。折りたたみ式でビデオくらいの大きさになって箱に入ってるの。同じものかな？コマはリョウで、依頼カードをめくって進行するみたいです。組み立ての小さいルーレットがついた的の人型とかもないですか？

＞３３７　里奈さま
圭之助は、手首じゃなくて右手のひら（親指の付け根下のふくらみ）に７ミリ程だけ、小さく脈打ってるのが見えるけど… (^o^;) コインなんて全然…☆そ、それなのに里奈さまは５００円玉でも動く！？１円玉は飛んだ～！？足首でもって…何者なのぉ～（笑）兄上様の書置きもナイスですね～☆

＞３３９　千葉のＯＢＡ３さま
眠くて仕方ない？いいなぁ…。ちょっと前までは、圭之助もそうだったんだよぅ！しばらく耳栓して寝てたら、耳がカユクなっちゃった…☆花粉症もそろそろピークが過ぎてきたのでは？お互いはやく楽になるといいですね～♪

### [10341] Ｂｌｕｅ　Ｃａｔ	2003.04.18 FRI 19:11:18　
	
　こんばんは～。　今週の感想です。
　香ちゃん、カッコい～い！　「私の娘に手をだすなっ！！」、やっぱり香にはこの言葉使いが似合うわ♪　香を感じることで、遠山の気持ちがいい方に変化してくれたらな、なんて思いました。
　それにしても、遠山の行動、どんどんカゲキになっていきますね、パソコンに爆弾仕掛けてたなんて。あ、そのためにわざとわかりやすいパスワードにしてたとか？　あのゲームソフトも、あの場所に誘い込むために残していったの？？　にしても、遠山の両親って、どこまでひどいんだろ（ぷんぷん）。

　ところで、i バンチって有料だったんですね。３００円ってバンチ本体より高いじゃん！　しかも新しい機種じゃなきゃ出来ないコンテンツもあるなんて。てことはたとえＪ－○ｋｙで始まったとしてもうちのダンナの携帯じゃ、無理なのかも（しくしく）。

　もし、ここの掲示板が携帯対応になったら、ますますカキコミが多くなって読むのが大変になりそう^^;　もしできるなら、携帯は携帯専用の掲示板にした方がいいんじゃないかなぁ、なんて思ったりして。

　Ｃ・ＨアニメにＹＡ○○ＲＡ（役名は裕子ってなってましたけど）が登場したのって、「９１」の『都会のシンデレラ』の回ですよね。本編の依頼の話をはしょって、これだけをアニメでやったという（苦笑）。リョウのコートの秘密とか、アニメでも見たかったのにな（やはりゴールデンのアニメでは見せられないモノがあったからだろうか　笑）。あ、登場の仕方は、リョウにナンパされたシルエット姿のＹＡＷ○○Ａが、リョウを巴投げ（かな？）したんだったと思いますよ～。

　Ｆ．ＣＯＭＰＯのアニメ化もいいけど、わたしは「こもれ陽」のアニメも見てみたいです♪　力を出してるときの紗羅の髪とか、動きのある映像にしたらきっとキレイだろうなぁ（うっとり）。短編たちも、ＯＶＡとかにしてくれないかなぁ、「タクシー・ドライバー」とか「ねこまんまおかわり」とか・・。
