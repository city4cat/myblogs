https://web.archive.org/web/20011121230819/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=15


[300] Goo Goo Baby　（化粧の話）2001.07.07 SAT 18:15:45  
前回は私事でした　すいませんっ（逃）  
  
今回は化粧トーク Ｏｎｌｙ・・・・（おいっ）  
  
＞りなんちょさんへ  
　私はマスカラだけならメイビリンのを使ってます  
　￥１２００ぐらいで買えたし。  
　高校の時から愛用～～。  
　（でも、私もともとマツ毛長いホウなんで、  
　　ちょっと付けるだけでもファービーのようになるぅ～～～）  
  
　学生サンお金ないんで  
　マスカラ１こ￥３０００以上するランコムとか  
　マックスファクターとかは買えないっす（泣）  
  
そういえば  
あなたに言われるまで気が付きませんでしたが、  
  
香に限らず北条作品に登場する女性たちって  
確かに  
暖色系のアイシャつけてますよね、ピンクとか。  
  
青とか水色とかの寒色系だとギャルっぽくなるから  
それを避けるためなのかな？  
  
でも  
寒色アイシャ＝ギャルだけが使う色じゃないよ～～～（汗）  
  
だって  
私ギャルじゃないけど寒色アイシャ使ってますもん。  
平気、ウイテません♪

上次是我的私事，对不起!(逃)

这次是化妆谈话Only····

＞りなんちょさんへ  
我用的是mavilin的睫毛膏，￥1200左右就能买到。
高中的时候就爱用~ ~。
(不过，我本来就是长睫毛的，稍微戴上就会像furby一样~ ~ ~)
学生们没钱买不到一个￥3000以上的兰蔻啦max factor啦(哭)
这么说来，在你说之前我都没注意到，不只是香，北条作品中登场的女性确实都涂着暖色系的眼影，比如粉色。
难道是为了避免蓝色、淡蓝色等冷色系会变得像辣妹吗?
但是冷色眼影=不只是辣妹们使用的颜色哦~ ~ ~(汗)
我虽然不是辣妹，但用的是冷色眼影。
没关系，我不介意♪  

[299] 姫 (海軍カレーの街）2001.07.07 SAT 17:48:30  
こんにちは。  
皆さん火曜日・もしくは水曜日が待ち遠しいですね。  
  
＞りなんちょさん  
香以外の人をリョウが愛するはずはないですよ。  
けどなんらかの形でくっつく様な気はしてます。  
恋人ではなく「娘」みたいな感じで愛するのかな？と私は思いました。  
あとGHが人間としての感情や、いろんな認識を香と言う「意識」が手助けをして、成長を遂げたら香は満足して居なくなってしまう気がしてなりません。  
けどリョウにとっての最愛の人は香以外には成り得ないですよ！！  

你好。
大家都盼望着星期二或者星期三的到来。  
  
＞りなんちょさん  
獠不可能爱香以外的人。
但我有一种感觉，他们会以某种方式走到一起。  
我想，不是恋人，而是以“女儿”的感觉去爱吗?  
还有GH作为人的感情，各种各样的认识由香这个“意识”来帮助，我觉得一旦成长了，香就不会满足了。（译注：待校对）  
但是对獠来说最爱的人除了香以外别无他法! !  


[298] 優希 2001.07.07 SAT 17:44:30  
茶々さん。私はリョウと香の初キスの事だと思ってました。回想シーンでリョウが酔っ払って香にキスしていたから、リョウが覚えてないって言ったのを納得していたのかと思いました。  

茶々さん。我以为是关于獠和香的初吻。在回忆的镜头里，獠喝醉了，吻了香，我以为她接受了獠说不记得的事情。


[297] shan  2001.07.07 SAT 17:25:47  
Very nice to see a message here in English. Actually I don't know Japanese, but I really like C.H. so much. Therefore I try my best to imagin the meaning of chinese characters in Japanese here. I also really hope that A.H can be sold in HK soon (even in Japanese) T_T  

很高兴在这里看到英文留言。其实我不懂日语，但我真的很喜欢CH。所以我在这里尽我最大的努力去想象汉字在日语中的意思。我也真的希望A.H能尽快在香港出售(甚至是日文版)  

[296] 茶々  2001.07.07 SAT 17:24:38  
今日会員登録して初めてカキコする茶々と申しまーす！  
みなさんこれからよろしく！！  
あの早速なんですけど、あの今週号で香がリョウに言った  
「ねぇ初めてのキスって覚えてる？」ってセリフ。  
あれで香はリョウにとっての『ファーストキス』を聞いたんじゃなかったんですか！？  
えっ！？そうじゃなくってリョウと香にとっての  
『初めてのキス』を聞いてるんですか！？  
だれか、意見聞かせてください！！  

今天注册会员后第一次发贴的茶々と申しまーす!  
今后请大家多多关照! !  
顺便问一下，本周号上香对獠说的“你还记得第一次接吻吗?”的台词。   
香不是在那里听到了獠的“初吻”吗??（译注：待校对）    
诶!?不是的，是对獠和香来说（译注：待校对）    
你在听《初吻》吗??  （译注：待校对）  
有谁，请告诉我你的意见! !（译注：待校对）  

[295] ネット   2001.07.07 SAT 17:23:48  
>heart    
hi.I am CH fan,too!  
I am living in Japan,22years old girl.  
I cannot speak English very much.Sorry.  
  
But,I was very surprised that bunch was sold in LA!  
Is it written in English?  
And I do hope that Kaori live again,too!  
Kaori is pure and very kindhearted.  
she is ideal woman for me!  
  
I am very happy to see the fan who live in foreign country.  
Please write again,and talk about CH and Angel Heart^_^)  

>heart  
嗨。我也是CH粉丝!  
我住在日本，是个22岁的女孩。  
我不太会说英语。  
但是，我很惊讶Bunch在洛杉矶出售!  
它是用英语的吗?  
我真希望香也能活过来!  
香纯洁善良。  
她是我理想中的女性!  
我很高兴看到住在国外的粉丝。
请再来信，谈谈CH和天使心^_^)    


[294] リンダ  2001.07.07 SAT 17:18:44  
＞はやねさま  
２話のラストでタキシードを着ていたリョウ。私も、香はタキシードを着たリョウの腕の中で眠りにつく事ができたのだと思います。香は最期にリョウが自分の為にタキシードを着てくれた事が嬉しくて、幸せを感じていたんじゃないかなあ…。でも、その時のリョウを思うと、可哀相すぎて言葉も見つかりません。  
でも時間に厳しい香が、この日に限ってどうして遅れてしまったんでしょう？この辺りにも伏線が隠されていそうっ！？  
＞yaekoさま  
私も初キスは３１巻くらいだと思いました。あのガラス越しの名シーンをさしおいて、二人がそれ以降のキスを初キスとして捉えている訳ありませんよっ！！（なぜか興奮ぎみ…）  

＞はやねさま   
在第2集的最后穿着无尾晚礼服的獠。我也觉得，香在穿着无尾晚礼服的獠的怀里睡着了。香在最后时刻看到獠为自己穿上了无尾晚礼服，感到很开心、很幸福吧…。但是，一想到当时的獠，就觉得太可怜了，找不到合适的语言。  
可是对时间很严格的香，为什么偏偏在这一天迟到了呢?这附近似乎也隐藏着伏笔!?    
＞yaekoさま   
我也认为第一次接吻是在第31卷左右。抛开那个隔着玻璃的名场景，两人之后的接吻是不可能被当作初吻的! !(不知为何兴奋不已…)  


[293] heart  2001.07.07 SAT 17:04:10  
hi . I am the big CH fan who living in LA now. (sorry, I cannot type in Japanese). I buy bunch every week here.kiss scene is really cool!! Like most of CH fans, I also want kaori can live again. So far, I like AH ,it's interesting. If angel heart can become the animation, it will be great! It's both sad and happy to see AH. It's a really good thing to see kaori ,ryo and hojo sensei's new work again, also it's the very hard to accept kaori's death. I think GH will be a great hero. She is pretty and has high ability to be a sweeper, she has interesting background. However, Personally, I want to see more ryo and kaori in the story. (I really love CH so much...) this is my biggest motivation to read bunch every week. (I feel nervous every week too...^_^)  
I know the GH is real hero...but since I am the CH fan for ten year... No matter what will happen, I will support hojo sensei foever! hojo sensei always the best comic creator in my heart!(I'll still pray for kaori, I really want she can live again! I mean, physically^_^)  

嗨。我是CH的超级粉丝，现在住在洛杉矶。(对不起，我不会用日语打字)。我每周都在这里买Bunch。接吻的场景真的很酷!!和大多数CH的粉丝一样，我也希望Kaori能复活。到目前为止，我喜欢AH，这很有趣。如果天使心能变成动画，那该多好啊!看到AH既伤心又高兴。再次看到香、良和北条老师的新作品真是一件好事，同时也很难接受香的去世。我认为GH会成为一个很棒的主角。她很漂亮，有能力成为一个Sweper，她的身世很有趣。然而，就我个人而言，我希望在故事中看到更多的獠和香。(我真的很喜欢CH…)这是我每周读Bunch的最大动力。(我也每周都感到紧张……^_^)  
我知道GH是真正的主角…但既然我是10年的CH粉丝…无论发生什么事，我都会永远支持北条老师!北条老师永远是我心中最好的漫画创作者!我还是会为香祈祷，我真希望她能再活一次!我的意思是，身体上^_^)  


[292] Goo Goo Baby（ダイヤルマスカラ）  2001.07.07 SAT 16:16:32  
「ちょっと時間いい？」　　よくない！！！！！  
  
　　　どなただよアンタ～～～～！！！！（怯）  
  
午前中の出来事。  
私がコンビニから出てった直後、  
ヘンなオッサンがあわててコンビニから出てきて  
私に話し掛けてきた（怯）  
  
「ちょっとだけ時間いいかな？　きみ今バンチ買ったよね  
　僕も読んでるんだよ　で、ちょっとお願い聞いてくれるかな」  
  
「え、よくないです・・・・」  
  
「ホントにちょっとでいいから。実はヘンなお願いなんだけど  
　聞いて」  
  
しつこい。　しつこいしつこい（怒）　ヘンなお願いだぁ～？  
で、反撃してみた  
  
「ヘンなお願いなんて聞けません」  
  
だが！！！  
「じゃ、ここじゃナンだからソコの公園いこうよ  
　ヘンなお願いで悪いけど聞いて。バンチも語ろうよ」  
  
だ～か～ら～！！！  
人の話を聞かんか～～～い！　誰がイイって言った～～～！！  
勝手に話を進めるな～～～　コノあんぽんた～～～ん！！  
しかもバンチ語る気なんかい！！  
  
そこで私は最終反撃！  
  
ホントは今日は講義なんてないけど  
「講義に遅刻しそうなんでダメですね～～～」と言ってみた  
  
暑さとしつこさで、すでに怒りが顔に出ていた私を  
これ以上怒らせたらヤバイと思ったのか  
やっとあきらめてくれた  
  
てゆうか  
１９のコムスメをクドクな～～～！！（呆）  
しかもバンチをダシにすんなっ・・・  
  
どう見ても  
私ぐらいの年の娘がいそうなかんじのオッサンだった（呆）  
暑さのせいでラリってんのかしら  
  
てゆうか私、  
ヘンなオッサンによく話し掛けられる。マジで！！！（怯）  
今回は、まだマシなホウ・・・  
  
別に私は露出度高い服は着てない（多分）のにナゼ？？？？  
  
タンザクに  
「ヘンな人に話し掛けられませんように」って  
書いときゃよかった・・・  
  
私のように若いムスメの方は気をつけましょう  
こうゆう目にあったことある方も無い方も気をつけましょう  
北条先生の娘さんとかトクに！！！　心配です・・・

“有空吗?”不好！！！！！
你是谁啊～～～～！！！！(怯)
上午发生的事。
我刚从便利店出来，一个奇怪的大叔就慌慌张张地从便利店出来跟我说话。
“能给我一点时间吗?你刚刚买了一本书，我也在看，能不能拜托你一下?”“嗯，不太好……”“真的一点点就可以了。其实是很奇怪的请求，你听着。”啰唆啰唆啰唆啰唆(怒)奇怪的请求啊~ ?于是，我反击道:“奇怪的请求我可听不进去”! ! !
“那么，这里是什么呢，就去那边的公园吧，不好意思，拜托你了，听我说。我也来谈谈吧。”
要不要听听别人的话~ ~ ~好!谁说可以的~ ~ ~ ! !
不要随便推进话题~ ~ ~这个手提箱~ ~ ~ ! !
而且我也很想说! !
于是我最终反击!
其实今天没有课，但我说:“我上课要迟到了，不行啊~ ~ ~”
因为天气太热，我的怒气已经写在脸上了，如果再让我生气就糟了，所以终于放弃了。
不要讨厌手游花19的小姑娘~ ~ ~ ! !(呆)
而且还把bunch拿出来……
怎么看都像是有个和我差不多年纪的女儿的大叔。(呆)
不知道是不是因为太热了。?
我经常被奇怪的大叔搭话。真的! ! !(怯)
这次还算好的…
我明明没有穿暴露的衣服(可能)为什么？？？？
我应该在我的タンザク上写上一句话：“不要被奇怪的人搭话。”就好了……
像我这样年轻的姑娘要注意了，不管是有过约会的人还是没有遇到过约会的人，就像北条老师的女儿啊! ! !我很担心……

[291] たろー  2001.07.07 SAT 16:08:58  
初めまして。　『たろー』と申します。  
いつも見ているだけだったけれど、今日は、７／７七夕なので？　勇気を出して書き込んでみました。  
自分は、数ヶ月前に北条先生の作品にハマって、一気に集めてしまいました。　  
　皆さんと比べたら、自分は、まだまだ未熟ですが  
　　　　　　　　　　　　　　　　　よろしくおねがいします。  

初次见面。我叫『たろー』。  
一直都在看，今天是7/7七夕。鼓起勇气试着写了。  
我在几个月前迷上了北条老师的作品，一口气收集了很多。  
和大家相比，我还不够成熟  
请多关照。  


[290] れもん（東京都曲がった事が大嫌い市）  2001.07.07 SAT 16:08:13  
どうもこんにちは、sweeperさん。ビンゴです！！  
辺り、っていうかそこです。（笑）  
鬼束ちひろの曲だと、確かに先生がおっしゃってた「月光」も  
ＡＨのことを歌っているようなカンジするけど、  
「眩暈」の方も結構それっぽく聞こえたのは私だけ・・・？  
「あなたの腕が声が背中がここにあって・・・」以降。  
（スミマセン、歌詞うろ覚えですが。。）  
うーん、ちょっと違うかなあ。テーマソング探しもおもしろいね。  

你好，sweeper。bingo ! !  
就在附近，应该说是在那里。(笑)  
鬼束千弘的曲子，确实老师说的“月光”好像也是唱AH的，不过觉得“眩晕”听起来也很像的人只有我吗…?  
“你的手臂、声音、后背在这里……”之后。  
(对不起，歌词记不清了。。)  
嗯，有点不一样吧。寻找主题曲也很有趣呢。  


[289] あずみちん  2001.07.07 SAT 15:45:44  
こんにちは。私もmamaさんとおさるさんの意見と同じで～す。基本的にC.Hスペシャルは別ですよ～、ここでしかアニメのりょうちゃんには会えないんですし...。でもホントA.Hがアニメ化してもオリジナルバージョンで香ちゃんは生きていて欲しいですよね。みんなの希望をせめてアニメだけでも残してほしいですもの。それからおさるさんの言うA.Hの曲、ピアノを使った曲いいですね。私は愛と宿命のマグナムの時のあのピアノの曲凄く大好きでしたから...。寂しく切ない音だけどとても心に響く音でしたから。何か今からビデオみたくなってきちゃったわ！  

你好。我也和mama和おさる的意见一样。基本上C.H特别篇是不同的哟~，只有在这里才能见到动画里的獠酱…。但是即使A.H真的动画化了，也希望香酱在原创版本中还活着。希望至少能把大家的希望保留在动画上。还有おさる说的A.H的曲子，使用钢琴的曲子真好。因为我非常喜欢爱和宿命的连发枪时的那首钢琴曲…。虽然是寂寞悲伤的声音，但却是非常触动心灵的声音。我现在就想看录像了!


[288] おさる（愛媛のポンジュース）  2001.07.07 SAT 15:31:36  
アニメ化の話いいですね！  
連載始まってばっかりで気が早すぎでしょうか？  
mamaさんの言うとおりで、CHのスペシャルがみれなくなるのも  
嫌です。  
アニメの中だけでも、香に生きていて欲しい。  
私の独断ですが、AHの曲はピアノを使った繊細な曲が  
いいと思います。オーケストラの壮大なやつもいいな。  
CH'91のアスファルトムーンみたいなかんじで。  

动画化的故事很好呢!  
连载才刚开始是不是太早了?  
就像mama说的那样，看不了CH的特别篇  
我不喜欢这样。  
即使只在动画里，也希望香活着。  
在我自己看来，AH的歌曲是一首用钢琴创作的精致歌曲。  
我觉得很好。管弦乐的宏大的也不错。    
感觉就像CH’91的Asphalt Moon。

[287] sweeper（コードネームは新宿）  2001.07.07 SAT 15:24:34  
　こんにちは。３度目（なんか多すぎ･･･）のカキコです。  
　＞れもん様  
　ご生息地は、もしかして志＠けんとネ＠チューンの原＠泰造の出身地あたりでしょうか。（もし違っていたらごめんなさい）  
  
　リニューアル前のＨＰで北条先生が「鬼束ちひろの『月光』は主人公の心境に似ている。」というのがあったと思うのですが。  
　私はＴＲＦの｢ＳＩＬＥＮＴ　ＮＩＧＨＴ｣がグラスハートのことのような気がします。  
　特に「私を見ている誰か　早く声掛けて間に合って　とてもいい子だと思ってくれてた　応えられずにどこかに思い出だけ置き去りで」のところなんかが･･･。  
　あと｢Ｄｅｓｔｉｎｙ　ｔｏ　ｌｏｖｅ｣がＡＨのりょうのことみたいで･･･。聴いてて切なかったです。  
　歌い出しの「忘れないよ永遠に輝きを失くさず　こらえきれず落ちてゆく涙さえ恋しい　愛し合った現実は誰よりも誇れる　平和だけが真実と限らない　それがＤｅｓｔｉｎｙ　ｔｏ　ｌｏｖｅ｣なんかがそんな気がしてならないです。  
　皆さんはどう思いますか？意見聞きたいです。  
  
　それからＴＲＦ（わーっ!!３曲全部だ）の「ＪＯＹ」ってＣＨ連載終了直後の２人の関係に近くないですか?どっちかというとりょうサイドのほうに。  
　「Ｊｏｙｆｏｒｙｏｕ＆ｍｅ守りたい　夜が明けるまでこの温かい手をずっと･･･ずっと離さないように　Ｊｏｙｆｏｒｔｗｏａｇａｉｎ時を越えて陽が昇る窓に二人帰ろう」のところなんかが特にひしひしと伝わってくるなとおもいました。  
  
　だらだら長いカキコですいません。しかもＴＲＦばっかりだし。  
　前前回から思いつきでやってみた「コードネームシリーズ」もうちょっとひねりが欲しい･･･。（ベタすぎ･･･）

你好。这是第三次(好像太多了···)的发帖。  
　＞れもん様    
您的所在地，莫非是もしかして志＠けんとネ＠チューンの原＠泰造的所在地?(如果错了，对不起)  

北条老师在更新前的主页上说「鬼束ちひろ的『月光』和主人公的心境很相似。」我想应该是这样的。  
我觉得TRF的“SILENT NIGHT”指的是Glass Heart。  
特别是「有人在看我，快速地呼唤我，正好，他们认为我是一个很好的女孩，但我无法回答他们，只留下了回忆」这段话……。  
还有“Destiny to love”好像是说AH的獠···。听着很难过。  
歌开头唱“我不会忘记你，你永远不会失去光辉，忍不住掉下去的眼泪，怀念相爱的现实比谁都值得夸耀，只有和平并不一定是真实，那才是Destiny to love”之类的感觉。（译注：待校对）  
大家觉得呢?想听听意见。  

还有TRF(哇!!3曲全部)的“JOY”和CH连载刚结束的2人的关系不接近吗?更像是獠的一面。   
“Joy for you & me我想守护你直到天亮，一直保持这只温暖的手……永远不要放开Joy for two again穿越时光，在太阳升起的窗前，我们一起回家吧”这句话传达得特别真切。  

写了这么长的文，不好意思。而且全是TRF。  
我想给“Codename Series”来个转折，上次我是一时兴起开始写的···。(太老套了……)  


[286] りなんちょ（新宿放浪癖有）  2001.07.07 SAT 15:00:04  
たばこの味に、かすかな硝煙の匂い…  
いやぁ～ん、クラクラしちゃう♪  
Ｓｗｅｅｐｅｒさんて、ロマンティストですね。  
  
今日は七夕…  
冴羽さんも、１年に１回でいいから、  
香さんに会えればいいのにね。  
  
海ちゃんが冴羽さんに、  
「死に急いでんじゃねぇ…」って、言ってたけど、  
冴羽さんが、本当に「死」を考えているなら、  
やめてぇ～！！！  
そんな事しても、香さんは喜ばないぞっ！  
それより、生きて、  
香さんのことを、ずーっと思い続けていてほしい…  
ずーっと愛し続けていてほしい…  
  
それより、グラスハートとくっついちゃったら  
どーしましょう…  
でも、みなさんも  
ちょっとは、頭を横切りませんでした？  

香烟的味道，有微弱的硝烟的味道...。  
嗯，我会崩溃的♪  
Sweeperさん是个浪漫主义者。  

今天是七夕...  
冴羽先生一年一次就可以了，  
见到香小姐不就好了。  

小海对冴羽同学说过“不要急于死亡……“，如果冴羽先生真的在想着“死亡”，不要这样啊！
就算做了这种事，香小姐也不会高兴的！
比起这个，活着，  
希望你一直在想着香小姐……。  
希望你一直爱着...。  

比起这个，如果和玻璃心有了感情怎么办？  
我应该怎么做...  
但是，你们   
难道你们至少没有想过这个问题吗？  

[285] 花見　（福岡）  2001.07.07 SAT 14:57:18   
やっぱり、３１巻？！  
私も冬間近だとか、裏路地とか言う事で  
考えてみたんですが、カオリンの洋服が違うんですよね・・？  
  
ん～～でも、ぜーーったいその頃ですよぉ♪♪  

果然，31卷？！  
我还在想，现在快到冬天了，或者背街小巷之类的。  
我想过了，香的衣服不一样吧……？  

嗯~但是，到了想要的时候哦♪♪  


[284] のんち  2001.07.07 SAT 14:33:08    
＞yaeko様  
私も初キスの時期ってそのときじゃないかと思ってました！！  
それに花見さんが香の前髪のことについて触れてましたけど、  
海原との戦いの前に香は髪を切って前髪が変わってますよね。  
だから絶対初キスはその前ですよね！？  
うーん、それにしても皆さん見てるところがかなり細かい・・・。  
でもそれを書いてる北条先生はもっと細かい！  
ひょっとしてどこまで気づくのか私たちを試してるのか？  

＞yaeko様  
我也在想初吻的时期应该就是那个时候吧！！  
而且花見提到了香的刘海，在和海原战斗之前，香剪了头发，刘海变了吧。  
所以说初吻绝对是在那之前吧。  
嗯，话说回来，大家看到的地方都很细致···。  
但是写这件事的北条老师更细致。  
难道是在考验我们能注意到什么程度?  


[283] ミラクルハート  2001.07.07 SAT 14:29:04  
ＢＢＳ再開から間もないのにホント、書き込みの多さにびっくりです。すごく楽しいので暇さえあれば拝見させていただいてましたが、見ているだけでは我慢が出来ずについに初めて書き込みします。よろしく！  
私もＣＨ連載時からの大ファンです。当時の私は大学生でした。  
あれから月日は流れ、一度は手放したコミック全３５巻を急に読みたくなって再び買い集めた矢先、ＡＨの連載が始まりました。  
何やら自分の予知のような行動にとても不思議な気分です。  
ＯＬのころ駅の伝言板（新宿ではありません）によく「ＸＹＺ」  
と書かれているとなんかうれしくて、今って伝言板ホントになくなっちゃったんでしょうか？時代とはいえ、さびしいです。  
あの日香が着るはずだったウェディングドレス、やっぱりブーケは「ホトトギス」？リョウを表す花は何だったんだろう？

BBS重新开始不久，真的很吃惊，帖子的太多。因为非常开心，所以只要有时间就让我看看，但光是看着就忍不住，终于第一次发帖了。请多关照！  
我也是CH连载时的忠实粉丝。当时的我是个大学生。  
从那之后，时光流逝了，就在我突然想把曾经放弃的35卷漫画全部读完，又全部买了回来的时候，AH的连载就开始了。  
我对自己预知的行为感到非常不可思议。  
OL时代车站的留言板(不是新宿)经常出现“XYZ”  
这样写着总觉得很高兴，现在留言板真的没了吗？这是一种悲伤的感觉，尽管这是我们生活的时代。  
那天香应该穿的婚纱，果然花束是“杜鹃”？代表獠的花是什么呢？  


[282] sweeper（コードネームはＧＥＴＷＩＬＤ）  2001.07.07 SAT 13:06:20  
　＞はやね様  
２話のラストシーンですかー。うーん気づかなかった。でもこの回は読み返すのは辛いですよね。きっとりょうは香の脳波がとまってもずっと抱きしめていたと思います。  
　  
　＞りなんちょ様  
うかつでした。お酒の味まではおもいつきませんでした。なんせ、りょう＝たばこのイメージでしたから。  
自分でふっといてなんですがもし酔っ払ってなかったらかすかに硝煙の匂いもしていたでしょうね。  

　＞はやね様  
是第2话的最后一幕吗?嗯，没注意到。但是这一话重读起来很痛苦吧。即使香的脑波停止了，獠也一定会一直抱着她。  

　＞りなんちょ様  
我太大意了。没有想到酒的味道。因为獠=香烟的印象。  
我自己突然说，如果不是喝醉了，还能闻到淡淡的硝烟味。


[281] Takayuki  2001.07.07 SAT 13:02:07  
AHを読んで驚いた。  
リョウが出てきたと喜んだのだがもうしばらく読むと香が・・・  
ところでまだ出ていないCHのキャラはでてくるのかな？  
ミックや麗香や教授たちは今どうしてるのかな？  

读AH的时候吓了一跳。  
獠出现了，我很高兴，但再读一会儿，香……  
对了还没有出现的CH的角色会出现吗?  
米克、丽香和教授他们现在怎么样了?  



