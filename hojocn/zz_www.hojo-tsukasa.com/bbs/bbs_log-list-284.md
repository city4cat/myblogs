https://web.archive.org/web/20021101015353/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=284

### [5680] ちゃこ	2002.02.15 FRI 15:57:17　
	
こんにちは。
信宏君の３枚目キャラ化が進む今日この頃、皆様いかがお過ごしでしょうか？
常連の方々、すっかりご無沙汰しております。ちゃこです。
ずっと来られなくて・・・皆様に会いたかったですよ～！！
初めての方は、はじめまして。去年の夏から暮れにかけて、よく出没していた
ものです。以後、宜しくお願いします。
先程、実に一ヶ月半ぶりに、こちらへアクセスしました。
過去のログが膨大すぎて、どこから手を着けて良いか分からない・・・(^^;;)
ので、しばらく浦島太郎状態で参加させていただきます。ゴメンナサイ。
とりあえず、復活（？）のご挨拶。です。

### [5679] 千葉のOBA3	2002.02.15 FRI 13:31:00　
	
めずらしく、いそがしいと思ってる。今日は・・・(今日だけ？）

＞５６７８chikkaさま
そうでーす！我が家は全員熱狂的ヤクルトファンです。今年もがんばりましょう！毎年２－３回神宮に行ってます。うちはみんな古田さんのファンです。だから２７番、「古田の背番号やん！」と息子とわたしは叫びました。いやー、これからもヨロシク。

＞５６７１ニャンスキさま
私もそう思いました。これが、「C.H」の続編ですと言われたら、つらすぎて、読まないことはなくても今以上にテンションさがってたと思います。だからパラレルでよかったです。

＞５６７０roseさま
思ったことは、ためてないで、はきだしましょう。精神衛生上よくありません。ここのみなさん、みんな優しいし聞いてくれますよ。そう・・・。私もたかが漫画といわないでよね、私には大事な世界なのと、年がいもなく本気でおもってます。

### [5678] chikka(Eastern Shizuoka)	2002.02.15 FRI 00:39:27　
	
バレンタインデーいかがお過ごしでしたか。私は今年も義理チョコをもらいました。（一応入試試験お疲れ様の意味を込めてですが・・・。静岡県の私学の高校の入試はちょうどこの時期です。今年は１３・１４日でした。試験監督や、テストの採点、面接官、合否判定など本当に忙しいです。）

【５６３９】千葉のＯＢＡ３様
もしかして、あなたもヤクルトファンですか？そうだとしたらうれしいです。今年も優勝を願って応援しましょう。

【５６６１】さえむら様
この「大騒動」はうやむやのうちに処理されるでしょう。事を明らかにすれば大事になりますから。この事件は日本と台湾の友好にひびが入り、国際問題になりかねないほどのものでした。また、「正道会」が壊滅状態になってしまう危険性もあり、事が露見されれば台湾当局も「正道会」を本気でつぶしてしまうこともありえたと思います。だから、玄武部隊が青龍部隊の後始末をきちんとし、「正道会」はこの件には一切関係ないようにしたと思います。あとは、冴子さんや李堅強がいろいろな力やネットワークを使って事件を揉み消すように取り計らうでしょう。それに、今の東京都知事の石原慎太郎氏と台湾の李登輝総書記は「朋友」
ですし・・・・。おそらく彼らも何らかの形でこの事件の揉み消しに協力するのではないでしょうか。要は「国家間の友好関係保持」と「組織の安泰」、「国のメンツ」のためにうやむやにすると思います。幸いなことにこの大騒動で日本人は一人も死んでいませんから。（死んだのは「青龍部隊」のメンバーだけだし）もし、日本人が一人でも犠牲になっていれば大変でしたが・・・・。

### [5677] ゆうちゃん	2002.02.14 THU 23:37:54　
	
こんばんは。カキコ、多いですね～。レスです。
５６６１さえむらさま。はじめまして。１、死体の処理・・多分したんでしょうね。ＣＨの中でも、１巻とか結構リョウに殺された人いるけど、処理は誰がしたのかな、って思ってました。ソニアの話で、お墓壊しまくったり、薬莢落としまくったりしてるけど、誰が片付けたの、とか。２、警察官も、指紋登録されてるんですか？警察官の不祥事も、多いですけど。
５６７０でｒｏｓｅさまが書かれてる「フロスキー」。ＣＭのコピーで、当時かなり流行ってたと思います。ただ、なんのＣＭだったか分からない・・というか、秋田で放送されてた記憶がない。それでも知ってるくらい流行ってた、てことで。

＞木更津キャッツアイ
秋田でも土曜日１６：００から放送始まりました～。ですが、１話を見逃してしまった・・。中途半端な時間にやるんだもんな～ＡＢＳ！！２話で出てきた「チャカ」はパイソンでしょうか？
では、おやすみなさい。

### [5676] 無言のパンダ	2002.02.14 THU 22:42:58　
	
こんばんわ★
今日は「バレンタインデー」でしたが、みなさん何かイベントはあったんでしょうか？うちではなんと！娘が女の子にもかかわらず女子３人から手作りクッキーなどをもらって帰りました！？
私自身の思い出としては、高校時代に友人たちとプロ野球選手のキャンプ地にチョコを送ったことくらい・・・(^_^;)
当時同じ学校に、人気若手選手とお付き合いをしている女の子がいて（修学旅行先にも彼は会いにきていた！）彼女にかなり影響されていたなぁ。ちなみにうちの高校は「バレンタイン・キッス」でおなじみの元おニャン子「国＠さ＠り」さんも出身。

「海坊主さん」
今週の海ちゃんは、半ば冗談交じりにリョウの頭の打ち所を心配してたけど、この1年きっと本気で心配して見守ってきたんでしょうね。あんなふうに冗談が言い合えるようになったり、リョウが香瑩たちによって希望を見出していく様を見て（？）海坊主さん自身もきっとうれしいんじゃないかなぁ・・・。
今まで身近にいて何もできなくて辛い思いもしてきただろうし。
またあんな二人のやりとりが見れるとうれしいですね。

失礼しました。(^_^)/~

### [5675] おっさん（岐阜県：あ～も～）	2002.02.14 THU 22:21:58　
	
毎度、今日はバレンタインっすね。私も何とかやりましたが・・・ということで今日もいつものご挨拶せ～のおっは～！！
レスっす。
ＵＭＩ様（５６５８）、おいっす～！！今週号の海ちゃんマジナイスでしたよ。あの大きい巨体でりょうの頭をスプーンで「ツンツン」いや～ナイスでしゅたよ。ちょっと玉井真矢様と重なりますけど・・・玉井真矢様、すんません。
ひろ様（５６６３）、おっは～っす。確かにそう思います。１年ぶりの衝撃、めっちゃ笑ってしまった。次回の展開私もそうですが皆もそう思ってるんじゃないかなって思ったおっさんであります。
ま～今日は疲れたんでこの辺で・・・？？？？？

### [5674] K.H(in YAMANASHI)	2002.02.14 THU 22:15:12　
	
　あら、皆様の話題はすでに「木更津キャッツアイ」に行っている・・・。まあ、先日宣言した、レスを書きます。

＞まろ様[5646]

　ども！！変態一本槍のK.Hです。
　確かに槇ちゃんファンにとっては、イメージがた落ちになるでしょうな・・・でも、第1巻の女装写真も、結構崩れていたような・・・

＞sweeper様[5637]

「だめだわん、ボキには殺すなんてことできな～い、なんとか捕まえる方法考えよ～っと(できればおウチにもってかえりた～い！)」

　女性に聞くのもなんですが、バレンタインデイの今日、いかがお過ごしだったでしょうか？

＞さえむら様[5661]

　う～む、なかなか鋭いご指摘のような気がします・・・。でも、冴子の指紋以外は次号以降に登場するのでは？と思います。　指紋は、消せば済みますし、犯罪者リストに載っていなければ大丈夫なのでは？と思います。

### [5673] 葵	2002.02.14 THU 21:34:19　
	
　ども！朝、仕事をサボってきたオバカさんです。（笑）明日は「木更津キャッツアイ」ですね～。「ＣＨ」に「ＣＥ」…ときたら、次はナニ？「木漏れ日…」？それとも「ＦＣ」？まさかイキナリ「ＡＨ」登場？！楽しみです～♪これで「バンチ」の販売率、上がってるのかな？（笑）　

＞琴梨さま［５６６５］　
　リョウちゃんが香瑩のパパなら、信宏のパパは海ちゃん…ナイスッ！！爆笑もんです。でもホント、「大人の男」、「香瑩を守れる男」、「香瑩と共に戦える男」になれるように、海ちゃんからしっかりと教育してもらうんですな♪（笑）

### [5672] ニャンスキ	2002.02.14 THU 20:53:45　
	
連続でスイマセン・・失礼します。

[5670] *rose*さま
＞アノ１年、リョウだって乗り越えたんだもん。
＞パイソンを頭とか（香瑩のように）胸にあてたり１度はし＞た気がして・・・

てとこ読んでパソコンの前でウオーーー！！っと
のけぞりました…(>_<)
私も感受性が強く、人が5のところ10位感じてしまうので
落ち込みやすくて大変です。
私も今週あたりでふっきれました。ここまでくるには
大変だったけど…見守っていきたいです。
ほんとにこの掲示板に来る方々は同じような思いで
いるんですね…心強いです。

### [5671] ニャンスキ	2002.02.14 THU 20:36:40　
	
こんばんは。皆さんの感想にすごく共感しました。

私はいつもバンチを買う時は、購入前に
本屋で待切れずワ－！と表紙を見てしまい、
車の中で2,3ページ読んで高揚してボーっとして（危険）
さらに家でゆっくり読んでハァー、とため息、という
感じでいつも妙なテンションになっています。
読むと心が揺れてぐるぐるかき回されるので
いつでも手に取れる作品ではないです…。
### [5659] *rose*さまも言っておられましたが
ほんと読む側も必死で大変なんですよ…。

今週のリョウの、香とのやりとりを思い出す直前の
目の表情を見て、ＣＨの続編じゃなくて良かった、と
心から思いました。
続編だったら私はＡＨは無理でした。
ほんと切なくて。

香が子供の話をした時リョウがとても驚いていたので、
子供のことは彼の中でももっとも気掛かりなことだったの
かな。そしてリョウのことだから言い出せないで
いたのかもしれませんね。
二人のやりとりが以前と違い
すっかり夫婦、っていう雰囲気だったので嬉しかった。
あと香はＡＨではドッキリ発言が多いですね…。

これからは香瑩の戦闘シーンはもうないのでしょうか。
彼女には幸せになってほしいけど、戦う姿がやたらと
かっこよくて好きだった…（殺しは駄目だけど。）
これからリョウと香にしっかり守られていくであろう香瑩、
うらやましいです。最強の父と母ですね。

### [5670] *rose*	2002.02.14 THU 19:49:37　
	
こんばんわ、*ｒｏｓｅ*です。全部レスです。

[5631]さえむら　様、お久しぶりです。うわぁーウレシ！！過去ログ見て書いてくれたんですね。タイトル教えていただきありがとうございます。今度、本屋さんで探して見ます。なるほど、おれたち二人の事！！ですね。素敵！！で、昨日、ホント、たまたまなのですが過去ログでさえむらさんのカキコ見つけたのですが「フロスキー」についてです。あれは、すごかった（笑）かなり昔の事なので記憶定かではないけど、何かのＣＭだった気が・・・。では、また！！

[5651]海里様、ケガなくてよかった！！トラックだなんて、本当に怖かったと思います。スローモーションだったのでは？スピードそんなにでてなかったのカナ？もう２度とあわないよう遠い所から祈ってます。ハードカバーのＣ・Ｅって何の事でしょうか？
もし、良かったら教えて下さいませ！！

[5596]ｓｗｅｅｐｅｒ様、こんばんわ。いいなー！！カラオケでＴＭ歌う人（ＢＯＸでしょうか？）若かりし頃ってゆーか結婚前飲みに行ってスナックでカラオケあるトコあるでしょ、そこでＧＥＴ～とか歌ってたら絶対振り向いちゃうのにー、いなかったなーって・・・。私もＢＯＸで歌いますよ。（一応♀ですが）男のキーってゆーかＣＤと同じキーにして、声､低っっバージョンでネ。ウツの歌ってる時の声ってそんな低くないし・・・。頑張って下さい！！

[5601]Ｂｌｕｅ　Ｃａｔ様、こんばんわ。私も波ありますよ。大丈夫です！！せつないけど、苦しい事もあったけど、見届けるゾってゆーか・・・。この場合は考え方変わっても全然ＯＫだと思います。だって、リアルタイムで追ってるわけですし・・・。おそらくアトにもサキにもこんなハラハラする話ってないのではとさえ思います。これからもヨロシクです。

[5666]千葉のＯＡＢ3様、こんばんわ。いつも心強いお言葉ありがとうございます。同じ想いの人がいるってすごくウレシイ。ご理解頂き感謝です。そう、私も前々回カナ？でふっきった。けど、今週号のあのリョウの目ですよ・・・。私、目で見て頭で理解してるハズなのに胸がドクンって・・・。（このままじゃ狭心症になっちゃうー！！）こんな切ないハナシってない！！でもＡ・Ｈ支持します。アノ１年、リョウだって乗り越えたんだもん。パイソンを頭とか（香瑩のように）胸にあてたり１度はした気がして・・・。ってゆーか、この私の妄想を誰か止めてーーー！！余計な事考えるからブルーになるんだよね。自分が悪いってわかってるけど、どーも、小さな頃から感受性強くて・・・。
ここまでの考えになるのは大変だったけど、何があっても見守ります。北条先生の作品があって今の私がいるし・・・。
けど、切なくなったら、哀しくなったら、泣きたくなったら、ここに書いちゃうけど、それでもイイ？これからもヨロシクネ！！
（皆様にも宜しくお願い致します！！）
　

### [5669] Ｂｌｕｅ　Ｃａｔ	2002.02.14 THU 16:45:44　
	
　北条作品って、カップル（？）のキャラが同居することが何故か多いから（Ｃ・Ｅの瞳と俊夫、Ｃ・Ｈのリョウと香、ＲＡＳＨの勇希とチョロ、Ｆ．ＣＯＭＰＯの雅彦と紫苑、など・・）、香瑩と信宏も２人ともリョウんちに住むことになるのかな～、なんて思ってたんですけど、信宏は海ちゃんとこにこのままいる、ってのもありなんですよね。［5665］琴里さまの言うとおり、リョウが香瑩のぱぱで海ちゃんが信宏のぱぱ、ってのも面白そうですよね(笑）
　北条作品を読んでいると、「家族」に血縁なんて関係ないんだな、って思えてきます。槇兄と香や、リョウと海原、そして読み切りの「ファミリー・プロット」を読んだりすると・・・・だから、香瑩とリョウも、そんなの関係なく、素敵な「親子」になるんだろうな、って思います。
　Ｆ．ＣＯＭＰＯのときは性別以外はすごく普通の、家族の物語、だった気がするんですけど、Ａ・Ｈはどんな“家族”の物語になるんでしょうね、楽しみです♪

北条的作品，不知为何情侣(?)角色同居的情况很多(C·E的瞳和俊夫，C·H的獠和香，RASH的勇希和チョロ，F.COMPO的雅彦和紫苑，等等··)，香莹和信宏2人也会住在公寓里吗?我是这么想的，不过，信宏就这样待在小海那里也是有可能的。[5665]就像琴里说的那样，獠是香莹的爸爸，小海是信宏的爸爸，好像很有趣呢(笑)

读北条的作品，会觉得“家族”和血缘是没有关系的。槙哥和香，獠和海原，还有刚读完的《Family Plot》……所以我觉得香莹和獠也会成为完美的“亲子”吧。

F.COMPO的时候除了性别以外感觉是非常普通的家庭故事，不过，A·H变成怎样的“家庭”故事呢，期待♪

### [5668] ゆうちゃん	2002.02.14 THU 13:30:16　
	
こんにちは。
「今週のＡＨ」
北条先生は、一風変わった「家族」を表現されますね。ＦＣでは逆転夫婦。そして、「臓器を提供された人は私の子供のようなもの」。血のつながりが全てではない、っていうか。リョウも香も肉親の愛情をほとんど知らずに育って、でも愛すること、愛されることの大切さとか喜びをよくわかってて・・。これからリョウは香瑩に、香ちゃんにもらった愛情を、分けてあげるんだろうな。それで香瑩は香ちゃんの代わりにリョウのお目付け役・・？

結婚して６年になる私は、そんな感情を忘れかけているような・・。うらやましい、というかなつかしい・・
では、失礼します。

### [5667] 葵	2002.02.14 THU 10:21:16　
	
　おはようございます。仕事中に抜けてきた葵です。（笑）
今朝の読○新聞の一週間のテレビ情報版「ｚｉｐｚａｐ」に、「木更津キャッツアイ」の特集が出てました。わりと好感触のようですね。地元では１００％の視聴率とか…。（笑）「木更津でなければ、こんなにおもしろくなかっただろう」とまで書かれてたけど…そもそも、どうして木更津が舞台に選ばれたんでしょうね？フ・シ・ギ☆
　では、また今晩参ります。本日はバレンタイン。ここに来てらっしゃる男性諸君に、ラブラブビームッ！！（笑）

早上好。我是工作中溜出来的葵。(笑)
今天早上读〇报纸的一周电视情报版“zipzap”上，出现了“木更津猫眼”的特集。感觉还不错呢。在当地有100%的收视率……。(笑)甚至还写着“如果不是木更津，就不会这么有趣了”……说起来，为什么选木更津作为舞台呢?侯诗琪☆


### [5666] 千葉のOBA3	2002.02.14 THU 08:26:08　
	
おはようございます。いつも朝からカキコする、暇な主婦の千葉のOBA3でございます。いやー解禁でもりあがりましたね。今日はバレンタイン。香ちゃんなら、チョコは絶対手作りするだろうな、と思いつつ。うちのもてない男達のためにチョコ４個キッチンにかくしてあるワタシ。

＞５６５９roseさま　言いたいことすごくよくわかります。この前のカキコで書いたけど、気持ちはふっきったんですよ、私は。今ではA.Hがすごく好きだと言える。でも不安なの。roseさまの言うように。・・・だって前に誰かいった人いたかもしれないけど、エンジェルハートってりょうちゃんから見た香ちゃんのハート(心臓）だからそういうんだよね。香ちゃん、りょうちゃんの天使だから・・・。なんか勝手な想像だけどそういう意味でついたタイトルだと、なんか不安になっちゃうのです。・・・あーまた日本語になってないか・・・.意味不明のまま、家事してきます。

### [5665] 琴梨	2002.02.14 THU 00:40:37　
	
こんばんは。琴梨です。

解禁ですぅ
　香ちゃんが出てくるお話はいつもじぃ～んっときちゃいます。ドナーカードを持った時、ウェディングドレスが着たいって言った時、そして今回の自分の臓器を提供される相手の話をした時…　香ちゃんは自分がりょうちゃんを残して早く死んでしまう事がなんとなく分かっていたの？って思います。　りょうちゃんはおちゃらけた返事をしながらどんな事を考えていたんだろう。愛する人が自分より先にいなくなることも、愛する人に先立たれて一人残されるのも、どちらもつらいですよね。
　
　香瑩のパパになることを決めたのも香ちゃんとの会話もキーになってると思うし、そう考えるとりょうちゃんは今でも彼女を愛してるし、これからも香ちゃんのために生きていこうとしてるような気がします。「私が死んでもさみしくないでしょう？」と言った香ちゃんの言葉がとても切ないです。
　
　そして香瑩はこれからどんな出会いをして、どんな恋をするんだろう。その時りょうちゃんパパは何を思うんだろう。信宏君の行動もこれから結構見ものです。（ちょっとファンになりかけです。）
　
　香瑩のパパがりょうちゃんなら、信宏君のパパはやっぱり海ちゃん…？！

　
　それでは皆様、ハッピー・バレンタイン！

### [5664] あお	2002.02.14 THU 00:37:01　
	
かいき～ん♪

一言感想。｢衝撃｣…なるほど(大笑)。
香さんの思いが痛いほど伝わってきますね…。｢素直｣になってからわずかな間だったんでしょうけど…幸せそうな2人が見れて嬉しくもあり…悲しくもあり(泣)。
今週の笑いのツボはやっぱり海坊主さんだった(笑)。いや、こそこそと窺っている少年もなかなかツボだったんですけどね(笑)。

ところで…1つつっこんでいいですか？？先生？？
冴羽氏の台詞、あれ役不足じゃなくって、役者不足ですよ～。
あの台詞は、｢自分なんかに父親役なんて荷が勝ちすぎているのでは？｣の気持ちですよね。父親という｢役｣が不足なのではなく、自分という｢役者｣がその｢役｣をするのに不足しているということだから、役者不足が正しい言葉です。

＞Parrot様
お久しぶりです～！お帰りなさいませ♪とはいえ、私も｢復帰｣したばかりですが(笑)。本日が論文の口頭試問でした…気分はすっかりブルー…結果は…早いとこ教えて欲しいわ(泣)。

### [5663] ひろ	2002.02.14 THU 00:23:19　
	
こんばんは～＼(^o^)／

さすが、解禁になるとラッシュですね。
昨夜は、書き込み途中で地震(震度３)がきたから、文字が斜めになっていないかな、って心配したけど何とか大丈夫だったみたい。えー、んなことないって！

もうみなさん、ほとんど書き尽くして、こんな遅くじゃ書くこともなくなりますね。
今週のサブタイトル、『一年ぶりの衝撃』満喫しました。りょうちゃんの「な 何だか･･･懐かしい衝撃･･･　一年ぶりかしらん･･･」の言葉、一年前に元気な香に100ｔハンマーやられていたんですね。
花瓶が物理的な衝撃だとすると、香瑩を通して香を抱くのは精神的な衝撃だったのかも知れませんね。

この先、当分この穏やかな雰囲気が続くのか、急展開が待ち受けているのか、ちょっと目が離せない状況ですが、次回を楽しみにしています。

それでは、サイナラ(^_^)/~

### [5662] K.H(ふろむ、やまなし)	2002.02.14 THU 00:09:38　
	
　おや、すごいっすね、皆様。今週のA.Hのカキコが！！と、いうわけでボキは「皆様と一緒」ということで・・・
と、いうのはちょっと別のことを語りたくなりましたんで。

＞Salt lake city olympic！（綴りが間違っているような・・・）

　結構盛り上がって来ましたよね、オリンピック。ただ、ど～もボキには引っかかることが・・・
　それはズバリ、アナウンス。日本の放送局のアナウンサーのコメントが、結構競技者に失礼なことを言っている人が多いんですよね。
　今日のフィギュアスケート男子についても、「あと100分の3秒で金メダルでした」とか平気で言ってましたけど、それを縮めるために、選手の方々がどれだけ努力しているのかわかっていたら、そんなことは言えないと思うんですよね。
　あと、報道合戦。女子のモーグルのある選手のドキュメントを各社一斉に放送してましたが、メダルが取れなかったら、そのことは全く無かったかのような状況。これじゃあイ○ローがメジャーに行っちゃう気持ちもわかるなあ、とボキは思いました。　もうちょっと、分別ある放送や中継をして欲しい、と思いました。(国営放送が先陣きってそんなことやっているから、限りなく無理なんでしょうがね)　

＞Varentine Day!!

　先日、sweeper様にレスを送ったときに話題にしてしまったネタですが、ボキはあんまりいい思い出無いんですよね～
　一番印象深いのは、大学入試の時、つまり7年前の今日、ボキは左足首の靭帯切りました。チョコの代わりにギプスと包帯、松葉杖という、香瑩に匹敵するかの(大げさ)ような、不幸な時期がありました。ま、大学受かったからいいんだけどね・・・(遠い目)

というわけで、レスは明日にします！すんません！！

### [5661] さえむら	2002.02.13 WED 22:23:10　
	
[Ａ・Ｈ]
先週号について、どうしても気になる事が....。
１　SATが来た時、人影がなかったということは、玄武が死体も処理したって事？
２　宴会（というか、乾杯）の跡をSATが見つけたけど、指紋とかチェックするんじゃぁ...？そうしたら、冴子の指紋検出されると思うんだけれど？
３　青龍の趙チーフも、玄武に殺されたのかなぁ？それとも生きてて、今後の伏線に関わってくるのかなぁ？
みなさまは、どう思われますか？

[今週号のＡ・Ｈ]
海ちゃん、かわぇぇっ！！
今後どうなるのかが、楽しみだわ�

では。
