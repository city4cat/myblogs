https://web.archive.org/web/20011102105533/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=111

### [2220] ありあり	2001.08.14 TUE 19:57:05　
	
こんばんは。休みの方が多いのかな？
バンチも休みだからカキコも少ないね。
black glasses様
私も不思議です。「伊集院隼人」が本名ですからやっぱちゃんとした日本人ってことですよね。国籍もあるってことだよね？もっと気になるのは彼の身長と体重。服はどこで買ってるんだろう？大きい人専用のお店？うーん、なぞだ。

キャッツアイのDVDはvol.9で全部出揃ったようですね。次はCHってことはないのかな。

### [2219] 吟遊詩人	2001.08.14 TUE 18:56:49　
	
３ヶ月ぶり・・・かな？
久々に来てみたらリニューアルされ、さらにIDまで必要になっていた。

少々おどろいた。

＜ふと浮かんだ言葉＞
「慢心したな　嵩司！！」　（特に意味なし）

よし。３ヶ月後にまた来よう。
　　　　

### [2218] ベリー	2001.08.14 TUE 18:43:26　
	
はじめまして。ベリーといいます。
パソコンを購入して半年。やっと、二週間前に開通しました。
私もここにきてからCHを読み返したりしています。今週は
お休みですが、つづきが気になるのは皆さんも同じですよね。
とりあえず、総集編を何度も読んで来週を待っているベリーです。この総集編を手に入れたのは発売から10日後なんです。
どこの書店でも売り切れてて。秋には単行本コミックスが出る
とか？何度も総集編を読み返しているのに今から楽しみです。

分からないことも多いので、いろいろ教えて下さいね。
よろしくお願いします。

### [2217] 翔子	2001.08.14 TUE 16:57:01　
	
皆様はじめまして

このＨＰにきてから
ＡＨ、Ｆ．ｃｏｍｐｏを続けてよんで
めちゃくちゃはまっています
またまたＣＨも読み直しました
やはり北条先生の漫画は素敵だなあと
改めて感じています！

これからのＡＨも楽しみにしています！！

### [2216] まみころ	2001.08.14 TUE 14:20:58　
	
お久しぶりです、ただいま！！
長いようで短かった夏休みが終わり、また戻ってまいりました｡
初めての方、まみころと申します｡どうぞヨロシクです｡
もっこり研究会会員兼特派員です｡（本当か？笑）

最近本当に夏休みのせいなのか、少ないですね｡
もっこり研究の方もお休みなのかな？

随分と前の事になってしまいましたが、服装や体格の事がかかれてましたね｡
私は何の気なしに見てたのですが、言われてみて初めてそうだな、と思いました｡
私の方が、ファン失格？？（笑）
そうですね、現役（ＣＨ）の頃よりＡＨのリョウは細みな感じですよね｡
絵のタッチが変わったのか、それとも計算ずくなのかはよく分りませんが･･･｡
それにしても、皆さんの注意力等には頭が下がりますね｡（笑）

最後に私事のレスになってしまいますが、あずリンちゃま！
私の事可愛く感じてくれてどうもありがとうございます｡（笑）
これに甘んじる事無く、日々精進いたします！（爆）

### [2215] 和葉	2001.08.14 TUE 13:01:15　
	
＞（2208）葉月さん
神谷さんは「原作が単行本化もされていないのにアニメ化案が
出るというのは極めて異例」と強調された上、アニメ化はどの作品と
断定された訳ではありませんでしたが「もう一度ケンシロウや冴羽リョウが
演じられたら嬉しい」とおっしゃってました・・・
高山みなみさんは・・・コナンのお二人のコンビは好きだけど
AHやられるとどうしても思い出してしまうのが某CHSPアニメ。
CH3の浅岡明さん止まりにしてほしい・・・←個人的意見ですが。

＞（2212）みみごんさん
神谷さんは京都駅ビルのイベントは半年に一回くらい来られてる
気がします。前回も行ってしまいました・・・
JR西日本でコナンのミステリーツアーとかやってる関係かと。
年齢については私はCHアニメの再放送ではまり、原作は３３巻
くらいからなので２０代と言ってしまいましたがもう少し前後
するかもしれませんね。逆に文庫本からはまってアニメビデオ
レンタルとかケーブルTVで神谷さん＠冴羽リョウ　にはまった
更に若い方もいらっしゃるでしょうし。

それでは皆さんも残暑にお身体お気をつけて～

### [2214] sweeper(訂正！)	2001.08.14 TUE 09:58:55　
	
訂正です。

[２２１３]の「どうなってしまうのかわかっていても、香の死を認めたくない気持ちはいまだあるんだろうなー。」は「どうなってしまうのかわかっていても、香の死を認めたくないという気持ちがいまだ自分の中にあるんだなー。」でした。
失礼しました。訂正します。

### [2213] sweeper(Salty　Dog)	2001.08.14 TUE 09:50:45　
	
＞タウンハンター様。［２２０６］
レス、ありがとうございます。
いえいえ、私の方が失格です。ダメダメです。大馬鹿者です。
特製テレカのことは知らなかったし、コミックスも文庫版しか持ってません。まだいい方ですよ！
でも、ＡＨはコミックスの発売日に買います！

＞もっぴー様。［２２０７］
レス、いつもありがとうございます。
そうでしたか。やっぱり「おっはー」の方でしたか。
そういえば、香は原作以外にもアニメでウエディングドレス、着てましたね。確か、ＴＶシリーズパート３の「クリスマスにウエディングドレスを・・・」でしたよね。
ビデオをレンタルして見たのですが、演技とはいえりょうと一緒にウエディングドレスを着て歩く香のセリフ「だって、うれしくって。兄貴以外の人と～。」（すいません。覚えてないです。）
あとは、香の「ははっ、ドレスだめになっちゃったね。」（間違ってたらすいません。）それに対するりょうの「バカ！ドレスなんてどうだっていい！！」というセリフよく覚えてます。

＞おっさん様。［２２０３］
レス、ありがとうございます。
ダーク･アイのことですが、［２０２４］と［２０４３］でちょっとカキコしてました。その時には１２話の「昨日の・・・車炎上事件のことか？」の下のコマのりょうの目がミックの言っていた「ダーク･アイ」なのかなと思ってしまいました。と### [２０４３]でカキコしてます。でも、りょうのこういう目はしばらく出てきそうな気がします。

[ＡＨのこと。]
どうしてもＣＨの続編とは思えないです。
タイムラグがかなりあるし、もしＣＨの続編とするなら香の年齢は３０代のはずです。
それに、ＣＨのエピソードに関することが（依頼人が出てくるとか。）少しあってもいいのではと思ってしまいます。（すいません。ここは軽く読み流していいです。）

＞みみごん様。［２２１２］
はじめまして。
私は、小学生のときに「キン肉マン」を見てました。
確か日曜の朝、放送してたんですよね。オープニングだったと思うのですが歌いだしが「リングに～稲妻走り～♪」でしたね。
すいません。ＣＨは、再放送でちょっと見ていた程度なので多分中学生ぐらいだと思います。

余談ですが、キン肉マンの好物、牛丼でしたよね。
マリさんが好きで、キン肉星の王子様。
テリーマンとか、ラーメンマンとか思い出しました。
そういえば、キン消し。きっとあるな・・・。当時買ったやつが。（あっ、脱線だぁ！）

昨日、ＡＨの総集編読み返してました。
やっぱりどうしても２話の回想シーンの最後のページとその次のページがなかなか読めませんでした。どうなってしまうのかわかっていても、香の死を認めたくない気持ちがいまだあるんだろうなー。と思ってしまいました。
文庫版ＣＨ１８巻のりょうのセリフ「おれは何が何でも生きのびるし、何が何でも愛する者を守り抜く！」と｢死なせやしないよ・・・。｣今、読むと辛いです。「香を守れなかった。」、「死なせてしまった。」という自責の念があるから、いまだに自分を責めてしまうんでしょうね。

カキコいつも長々失礼しました。

それでは。


### [2212] みみごん	2001.08.14 TUE 01:07:57　
	
>[２２０５]和葉様
アニソンコンサート、行ってこられたのですか。いいですね～！私も神谷明さんが出演される、てチラシで見たので行きたかったのですが、ちょうどその日まで旦那の実家に帰省してたので行けなかったのです。京都でそういう催しってあんまりないですよね。すぐ横の伊勢丹では、あだち充原画展をやってるし、子供を連れて見に行きたかったです～。
でも、ＣＨや筋肉マンなんかを見てた人たちって、まだ２０代？！　いったい皆さん、いくつくらいの時に見てられたのでしょう？　自分の年齢がこわいです。

少し前、近くの古本屋さんで「コミックバンチフェア」とか称して北条先生や原先生はじめ、バンチに執筆されている先生方の以前の作品を並べたコーナーを作ってました。「今、マスコミで話題のコミックバンチ」と、バンチの表紙や漫画のページの切抜きを壁に貼ってバンチの紹介もしてるんですが、そこに並んでるコミックスは全部集英社時代のもの。これって、ちょっとちがいますよね？　

### [2211] まどか	2001.08.14 TUE 00:44:24　
	
【香ちゃんに甘い海ちゃん】
私、「ＣＨ」の連載中からずっと気になってたところがあるんですけど。未だ分からんので誰か推理してください。
美樹ちゃんの話のときのことなんですけど、１８巻の１８０頁で海坊主が香に「それにおれは・・・」ってとこで言葉を切ったじゃないですか。
私あれ、連載中「あ、あれ！？もしかして海坊主って香のこと好きなの？」って思ってしまったんですよね。
あの後美樹とはめでたくくっつきますが、だったらあのセリフは何を意味してたんでしょう？
目のことかなあ、とも思ったが、だったら何で照れて去るの？
何が「い・・・いや、何でもないっ！！」・・・んだあ！？
分からん。さっぱり分からん。

＞キャサリンさん
レスありがとうございます。そうっすよね！！海ちゃんて香にめちゃ甘々っすよね！！謎の１８巻のセリフについてはどう思います？

### [2210] black glasses	2001.08.14 TUE 00:13:07　
	
こんばんは。
僕も一応毎日来てるんですがあまり書くことがなくて最近はカキコしてなかったのですが、あまりに閑散としてさびしくなってきているのでなにか話題になることはないか考えましたが、結局は師匠（海坊主）のことしか思いつきませんでした。（北条ファンとしてこれでいいのか？）

というわけで海坊主ネタですが、海坊主がもし主人公だったら（個人的に）かなりおもしろいのではないかと思いました．彼の裏の世界においての仕事ってＣＨではリョウの補佐をしたり、リョウの依頼人の敵役がリョウを邪魔するために雇われたりと彼がメインとなる仕事している姿ってあまり描かれてないのでほとんど想像ができないんです。それがもし海坊主とリョウの関係が逆になった場合、師匠（海坊主）はどのように仕事をするのか、またリョウが邪魔する場合ではどう対処するのか（こういう時はたいてい邪魔するよう依頼してくるのは男なのでリョウはその仕事は引き受けない可能性もありますが）と考えているうちにだんだん考えが膨らみ、とても書ききることがここではできないくらいになってしまったのでもうこの辺でやめときます。

あ、でも最後にこれだけは言わせてください。なぜ彼は傭兵になったのでしょうか？あんなにも素晴らしくいい人がなぜ人を殺さなくてはならない兵士なんかに？リョウみたいに育った場所が戦場だったらわかるのですが。

### [2209] けいちゃん	2001.08.13 MON 23:08:13　
	
ＢＢＳもお盆休み？なのでしょうか・・・
毎日チェックしてるけど、回転がとてもおそくなってますね。
法水様、おっさんさまもおっしゃっていましたが・・
ちなみに私はばっちり仕事、普通の週明けです。
あ、バンチが明日は出ないのが違いますが・・
電車は混んでないけど、早く来週にならないかなーーーー
北条先生もお盆で帰省してらっしゃるのかなー。
今週ＡＨが見られないのは残念だけど、こんな時ぐらい、ゆっくりひと休みしてください。北条先生！

### [2208] 葉月	2001.08.13 MON 22:20:50　
	
＞葵さま
　遅ればせながら、そんなCMが流れてるんですか？知らなかった・・・是非是非、拝見しなくては。
＞和葉さま
　アニメ化、バンチで実現する作品があるならば、ＡＨか蒼天の拳なのかなぁ？創刊前から、話題の２作品だったし。
ＧＨの声は高山みなみサンがするのかな？
ベテランの（？）高山サンなら、ＧＨのいろんな感情をウマク
表現してくれそうな気がするけど・・・

### [2207] もっぴー	2001.08.13 MON 21:57:58　
	
ミナサマー！コンバンワー！（＾０＾）/

[はじめに]
土曜の夜から、留守にしてました。毎日掲示板読んでたので、
なんだか、話題に乗り遅れた気がしました。
また、留守にするので、乗り遅れるのがさみしいわ。（；；）

[レス]
＞初めてお目にかかる皆様
　よろしくお願いします。このホームページを会社のパソコンで　知って、参加したくてパソコン買いました！初心者なので、
　皆さんいろいろ、お願いします。

＞ニーナ様
　アリガトウございまーす。（＾＾）
　私も、友達にAHの事を話しまくってまーす！
　
＞あずみちゃん♪様
　色々、教えてくれてサンキューです！
　ハートマーク出ないー！（；；）ごめんチャイ。です。

＞sweeper様
　いっつも、レス、サンキューべりべりマッチです。
　あ、遅れましたが、山寺さんはそう、『おっはー』です。

[ハンドルネーム]
いろいろ、先生の作品に関する、ハンドルネームの方多いけど
『トラップ』さんて、いないなー。と、ふと思った。

[香ちゃんの花嫁衣裳]
そうですね。確か、原作で一回と、アニメでも一回着ましたよね。でも、本番は、着れなかったんだよね・・・。
えーん。せめて、花嫁さんになる夢を叶えた後だったら・・。
でも、リョウの回想シーンで香ちゃんの居なくなっちゃうシーンが出ない限り、まだ生きていると想っている私なのでした。
　

### [2206] タウンハンター(本当に閑静ですね）	2001.08.13 MON 21:24:14　
	
[帰省中？]
こんばんわ。やけに回転が遅くなってますが
みなさん帰省中ですかー？
私はお金無いので帰れません・・・
こんな時はCHのビデオでも借りて見ようっと！と
思って行って見たら劇場版とTVスペシャル版しかなかった。
第１話から見たいのにぃ。DVDとかは無いのかなー？

[まだある失格]
＞法水さん、sweeperさん
レスどうも！実はまだあるんですよ・・。
前にも書きましたが、特製テレカも使ってしまったし、
コミックスなんか途中古本屋で購入したし・・。
でも！AHは初版で買います！

### [2205] 和葉	2001.08.13 MON 20:23:47　
	
残暑お見舞い申しあげます
かなりご無沙汰しております。昨日、京都の駅ビルで神谷さんの
イベントがあり、行って参りました。
アニソンコンサートなのでお子様が対象だったと思いますが、
最も神谷さんを崇める（？）世代ってまさにCHやキン肉マンや北斗の拳などに魅了された我々２０代なんじゃないのかなあ。
そして願ってやまなかったリョウの声をやって下さった時に
一際拍手が大きかったと感じたのは私だけではないはず！
（でも「AH」でのリョウの荒みぶりを考えるとちょっと
悲しかった・・・）
リニューアル前は神谷さんご自身もこちらの掲示板にカキコミ
して下さってましたが、来て下さいませんね。入りにくいのかな？
バンチ作品アニメ化案浮上の話もなさってました・・・（ちょっと複雑）
もしこちらもご覧になってたら頑張って下さいね！

北条先生の短編集「少年たちのいた夏」を遅ればせながら購入し
読みました。北条先生が、外見的にはアクション漫画に見られ
かねない「CITY　HUNTER」などの作品を生み出しながら、根底に
人殺しや戦争の愚かさと人命や生きることの尊さを訴える
スピリットが感じられる素晴らしい作品でした。AH　でも頑張ってほしいです・・・　

### [2204] ニーナ	2001.08.13 MON 18:26:44　
	
皆様、こんにちわ！！
昨日の雨で、少しだけ水害に遭いました。怖かった～！

＞葵　様

　レスありがとうございます。ＨＮですが、そうです、「愛と宿命のマグナム」の、ニーナちゃんです。あの映画、とても好きで、頂いてしまいました。　特に、カオリがヘリに飛び乗りさらわれた時に、ニーナがリョウに「どうしてカオリさんの気持ちに答えてあげないのか？」と、ツッコんでいるところなんか、こっちまでドキドキしてしまいました。　その時の冴子さんのリョウを見た時の顔、自分もその場にいたら、同じ顔をしたと思います。　
これからも、ちょこちょこカキコしますので、よろしくお願いします。

＞お初の皆様
　
　ニーナと申します。文書力があまりなく、読みづらい点もあるかと思いますが、一生懸命カキコしますので、よろしくお願い致します。

### [2203] おっさん（岐阜県）	2001.08.13 MON 17:38:03　
	
毎度、こんちわ～！おっさんです。皆さんおげんこ～！！
法水様、私も何となくここ何日か閑散としてるかな～って思っちゃった。やっぱ皆さんお盆休みで帰省中ですか？私は仕事だし（今日は休み）新盆だし、ここに覗きまくってます。
sweeper様、ダーク・アイですか・・・。私もそんな気がしました。３話以外にもあるような気がしましたけど。りょうのシリアスな顔って何か多いような・・・＾～＾；。
初めましての方も見えますのでごあいさつ。
初めまして、おっさん（女です）と申します。へぼいおっさんですが宜しくお願い申し上げます。
以上、おっさんでした＾－＾。

### [2202] sweeper（Grass　Hopper）	2001.08.13 MON 17:23:25　
	
こんにちは。
カキコ少ないですねー。お盆だからかな？

＞タウンハンター様。［２２００］
わかります。その気持ち。
私も、集英社から出た小説をその時はもう読まないつもりでいたので、古本屋さんに売ってしまったことがあります。
そうしたら、また読みたくなってしまってまた買いなおしたことがあります。今思うともったいないことしたなーと思いました。

［コミックス３２巻のカバーイラスト。］
パーフェクトガイドブックではよくわからなかったのですが、２０周年記念イラストレーションズでやっとわかりました。
槙村兄は、にこにこしていたんですね。
今まで、困ったような表情にしか見えませんでした。（失礼しました。）

［香の鐘つき。］
一体、どこからでてくるんでしょう。
アパートのリビングなら、なんとなくわかるんですけど・・・。
文庫版１１巻の上村愛子さんの話では、どうやって設置したんでしょうね。

［依頼人で思ったこと。]
文庫版２巻で出てきた片岡優子さん。
いまだに香のことを「りょうの弟でおかま」だと思っているのでしょうか？
今、ふと気になりました。（すいません。皆さん。）

［りょうのこと。］
そういえば、ＣＨとくらべて胸板分厚くないですね。
個人的に仕草もそうですが、分厚い胸板に「男の色気」があるな。と思うのですが。どうでしょう。（いつも、すいませんこんなんで。）

［イメージソング。］
B'zの「愛しい人よ・・・Good　night」。
サビとか最後の方とか、特にです。
聴いていて、りょうの香に対する気持ちがこれかなーと思ってしまって、２人を思い出してしまいました。

［ガラス越しのキス］
何度読んでも、いつもこのシーンだけはドキドキします。
アニメでやったらもっといいかなーと、思ってしまいました。

カキコ長々失礼しました。

それでは。

### [2201] 法水	2001.08.13 MON 13:47:14　
	
何だか閑散としてますねー。皆さんお盆で帰省中かな？

＞タウンハンターさん[2200]。
それはいけませんねー。ファン失格です。それは冗談として、
20周年記念のイラストレーションズは去年の年末に出ました。
紫苑ちゃんが表紙でお値段は2400円。もちろん集英社から
出てます。そのちょっと前には同じく20周年記念ということで
『北条司短編集　天使の贈りもの』という愛蔵版コミックスが
出ています。中身は既にコミック化されたばかりのものだった
んですが、箱入りで装丁がなかなかよいです。 
