https://web.archive.org/web/20031101133543/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=593

### [11860] ゆーご	2003.09.26 FRI 22:32:03　
	
ども～！！今週の感想だ！！
香はやっぱり心がやさしいですね。香が銃の前にたってる絵がとても印象的です。自分に銃口を向けられてとても怖いはずなのにすごく大きく見えました。「この人も殺させないし・・・あなたも殺させやしない！！」←香らしい！
リョウにＸＹＺでたのむのもよかったです。香が最後に信さんにむけた表情は信さんにとって忘れられない天使のように見えたんだな～と思いました。

ＣＨは小学生の時かな（？）、日曜の昼にやってた時みてました。よく銃をうつまねをしてました。いつも、わくわくしてみてたのを覚えてます。また最後のエンディングがかっこいいんですよね。あの時はリョウみたいになれたらなっとおもってました（笑）

### [11859] リョウ357	2003.09.26 FRI 22:24:58　
	
 [11857] ふじっちょ さん
 [11853] Ｂｌｕｅ　Ｃａｔ　さん
レスありがとうございます^^
そうなんですよね。リョウの声、神谷明さんの声もいいですよね★リョウの声は神谷さん以外ないですね★
魅力的なCHをさらに持ち上げたのが、神谷さんの声だと思います★北海道の方、いらっしゃるんですかね?今日はホント大変でしたね。学校行っててさっき知りました★
北海道の方、大丈夫でしょうか?


### [11858] 千葉のOBA3	2003.09.26 FRI 22:14:34　
	
こんばんわ☆北海道にお住まいの方、大丈夫ですか？今朝はこちらでも随分長い間揺れを感じていましたが・・・。

感想です。ＡＨの香ちゃんは、お酒強かったんですねー。いいなぁー！飲めるならりょうちゃんとの晩酌なんて、二人で最高だったでしょうね！あーーー、うらやましい！！！もう一つ超うらやましいのが、「たく・・ちょっと目を離すとコレだ！おまえは・・」って、そんなにいっつも、りょうちゃんに見守られていたんだよねーーーー！！！いいなぁーーー！でも今週のお話で、何故早百合さんが出て来たのか、何となくわかってきたというか・・・。香ちゃんって言う天使のハートを持つ、このＡＨの核になる人間を描くのに、必要なお話なんだろうなぁーと・・・。うまく言えないけど、私なりに納得してきました。・・・でも、ホントにムチャするのね、香ちゃん。・・・つくづく、「あの時」にそばにいれなかった事・・・。そのりょうちゃんの気持ちを思うと悲しいですね。

１１８５１　　いもさま　　うちの息子も大学はじまりましたよ。中学生の次男がうらやましがる、長い夏休みでしたねー。学校もバイトも体調（隊長？）崩さないようがんばってね！

１１８５０　　無言のパンダさま　　おぉーー！生存確認！！香ちゃん、お酒強いんですって！しゅごーい酒豪のパンダさま、飲み比べしたかったですねー。

１１８４７　　ふじっちょさま　　そうそう。アタマを打ったから、今のスーパーふじっちょさまがあるのよん♪

１１８５２　　へそたろうさま　　はじめまして。女子プロって、ゴルフ？と一瞬思ってしまった千葉のＯＢＡ３です。へそたろうさまもお酒強いんですか？私はダメなのでうらやましいですよー！

それでは、なんか解禁直後に感想カキコできたの久しぶりで、うれしかったです？

### [11857] ふじっちょ	2003.09.26 FRI 21:40:19　
	
おこんばんわ～☆
朝から北海道の方で大きい地震がありましたが、大丈夫でしょうか？いつ何処で起こるか分からない天災…皆様も常日頃の注意を怠らないよう心がけましょう。

☆今週の感想☆
裏の世界で生きたものは安眠する事は無い…改めて阿香が殺し屋グラスハートだった事を思い知りました。リョウが何時も香に話しかける時、阿香は眠っているので忘れていました、やはり血縁関係にあるとはいえリョウ以外の人間がいるだけで気を張らなければいけない身体（本能）って悲しいですね…
今回はマスター信ちゃんの過去が明かされるってのは意外でしたね(^_^)彼も香の優しさに救われた一人だったんですね。自分の命も顧みず、殺されようとしている者も殺そうとする者も救おうとするまっすぐさ…香とであった人たちはみんなそうやて救われたんでしょうね(^_^)改めて“ホレ”ました(笑)
香が酒豪だったとは…“朝まで説教コース”ってのは酔ってクダを撒くのか？(笑)まあ、いつもブラブラしてるリョウを捕まえてその場で高まった想いをぶつけていたんだろうな
しかし、リョウが香を連れて飲みに行くってのは意外ですね、けど信ちゃんの店オンリーなのかな？
オリジナルカクテル“香ちゃんSP”はきっと香の“強さ”をストレートに表現したから強いんでしょうね(^_^)

＞[11848] 葵さま
確かに昨日は少なすぎですね…学校帰ってきてから何度も見に来てはいたんですが、ずっとトップは「ふじっちょ」…なんかものすごく心配になってしまいました(笑)

＞[11849] リョウ357さま
確かに最初の頃は女性が多くてビックリしました(^^ゞ未だに誰が女性で誰が男性か混乱したり(笑)ちなみに僕は“男”です。
中・高生の頃にCHにハマるのってやっぱり、多感な頃にリョウ達のカッコよさに惚れるからじゃあないでしょうか？キャラクターたち誰もが魅力的ですからね(^o^)丿

＞[11850] 無言のパンダさま（猛抗議）
＞「ぼんさん」は不滅だな
じゃあ「探偵（刑事）と泥棒に分かれてやる鬼ごっこ」ってなんていいます？ウチは「たんドロ」でしたね。決める時は「いろはにほへとちり“盗人”おわかよ“探偵”」で
大学の抗議は生物の抗議で第一回・二回がパンダが首題のようです(^_^)なかなか興味深いですよ(笑)パンダには「6本目」の指があるとか…

### [11856] 葵	2003.09.26 FRI 21:27:01　
	
　☆書き忘れ☆
Ｂｌｕｅ　Ｃａｔさまも書かれてますが、「香ちゃんスペシャル」…こんなステキな小道具を出すならば、カラーページの時にして下さい！私はゲコ下戸ガエルだけど、せめてカラーで見たかった…。（T_T)

### [11855] ゆーご	2003.09.26 FRI 21:26:33　
	
ども～！！今週の感想だ！！
香はやっぱり心がやさしいですね。香が銃の前にたってる絵がとても印象的です。自分に銃口を向けられてとても怖いはずなのにすごく大きく見えました。「この人も殺させないし・・・あなたも殺させやしない！！」←香らしい！
リョウにＸＹＺでたのむのもよかったです。香が最後に信さんにむけた表情は信さんにとって忘れられない天使のように見えたんだな～と思いました。

ＣＨは小学生の時かな（？）、日曜の昼にやってた時みてました。よく銃をうつまねをしてました。いつも、わくわくしてみてたのを覚えてます。また最後のエンディングがかっこいいんですよね。あの時はリョウみたいになれたらなっとおもってました（笑）

### [11854] 葵	2003.09.26 FRI 21:24:11　
	
　北海道で大きな地震がありました。しゃぼんさまを始め、北海道近県の方、お知り合いのおられる方、大丈夫でしたか？

　感想です♪扉絵を見て「あー！ＣＨの香だっ!!」と思いました。今回出てくる香はすべて、ＣＨの頃の香ですね。元気さも無鉄砲さも、そして優しさも…。なんか嬉しくなっちゃいました♪バーテン信ちゃんの過去に、こんなにも香がかかわってたなんて…そうすると、もしかしてあの時（青龍との戦）のメンバーの誰にも、香ってこんなカタチでかかわってたんじゃないのかなって思いました。なんか「香らしさ」満タンですね。（笑）

　さゆりさん…少しは「香」をわかってくれたかな。香がどんな女性だったか、どんなにみんなに幸せを与えていたか。そして約束の１週間の最後の日には「どんなにリョウを愛したか、そして愛されたか」を知って欲しいな。（香は恥かしがるかもしんないけど・笑）リョウにおぶわれたさゆりさん…シュガーボーイの香を思い出しましたねー。ラストページの左上のリョウの顔…その顔を知ってるのは、香だけなんだろうな…。（うらやましいっ☆）それにしたって阿香、そんなんじゃリョウ直伝の夜這い（?）を仕掛けた信宏の命がナイぞーっ？！(;^_^A

### [11853] Ｂｌｕｅ　Ｃａｔ	2003.09.26 FRI 20:10:53　
	
　こんばんは～。　今週の、感想です。
　リョウが早百合さんをはがいじめにした瞬間、てっきりもっこりギャグに走るのかと思ったら、違いましたね^^ゞ　「頭は眠っていても体は常に臨戦体制にある」かぁ・・・。そんな阿香でも、リョウぱーぱがすぐそばにいるときには顔に落書きされても気付かないくらい（笑）熟睡できるんだよね、香のそばでリョウが初めて熟睡できたように。
　今回やっと、元・殺し屋のマスター信ちゃんの“過去”が描かれて、ちょっと嬉しかったです☆　そっかぁ、信ちゃんにとっても香ちゃんは“天使”だったんだね。「絶対に殺させない！」という香ちゃんのまっすぐな信念、それが裏の世界で生きてきた者にはすごく新鮮で、リョウはそこにめろめろになっちゃったんだろうね。わたしも、そんな強い意志を持つ香ちゃんはとってもステキだと思います☆
　香って、お酒強かったんですね、“朝まで説教コース”ですか（笑）　なんか、「どんな理由があっても絶対殺してはだめなのっ」とかって命の大切さを語り続ける香を見て顔では「またか」なんてなりながらも心ではいとおしく思ってるリョウ、の姿を想像しちゃいました☆　ところで、“香ちゃんスペシャル”（すごい名だ・・）てどんなカクテルなんでしょーね、いつかカラーで描いてくれないかな♪

＞[11833] yosiko さま
　（笑）　ありがとです♪
　入院されてたんですか、わたしも入院したとき、ここに来られないのがすっごく寂しかったっけ。

＞[11845] 無言のパンダ さま
　ありがとうです♪　でもあんなへたっぴなものに、はたしてご利益があるんでしょうか（笑）

＞[11849] リョウ357 さま
　わたしは、北条ファン（特にＣ・Ｈファン）は圧倒的に女子が多いと思っていたので、最近ここの参加者に男子が増えてきてちょっと嬉しかったりします(=^-^=)。やっぱりあの絵の美しさと魅力的な登場人物、ストーリィが女子にも読みやすいからでしょうね。それに、アニメでの神谷明さんの演じるリョウの声があまりにもステキだから、あれが女心をめろめろにするんですよね（きゃっ☆）。

### [11852] へそたろう	2003.09.26 FRI 19:02:46　
	
こんにちは。渋谷で女子プロに勧誘されたへそたろうです（笑）。最近太ったので仕方ないか・・・（^^;）。
　前回のカキコみにお返事下さった方々、ありがとうございます。また、初めましての方々もどうぞよろしくお願いいたします。
　さて、今週の感想です。
阿香は睡眠時でも臨戦体制にあるとのことですが、今まで何度かリョウも彼女の睡眠時に近づくことがありましたよね？阿香が攻撃しなかったのは、やっぱりリョウぱーぱは、彼女にとって特別だからわかるのかしら・・・？それにしてもカオリが流血しながらも、殺し屋のマスターに懇願する表情がなんとも切ない・・・。マスターでなくとも心動かされます。
　ところでカオリってばお酒強い設定なんですネ。実は私も酒豪なんです。もっぱらビールばかりで、たまに梅酒を少々・・・。最近は、中ナマを注文すると何度も注文しなくてはならないので、はじめから大ジョッキで注文してます。複数で飲む場合はピッチャーが当たり前。こんなへそたろうは、おしゃれなバーとかで、ちびちびカクテルを飲む姿に憧れます。大人っぽくて素敵だなぁと思います。でも、私の彼はまったく飲めない人なので、お酒を飲むデートすらありえないです・・・。
　
　あ、もうこんな時間！夕食の準備をせねば・・・。
　また来ますネ～。

### [11851] いも	2003.09.26 FRI 01:16:10　
	
どうも、こんばんわ～。
明日から大学が始まる・・・。電車に乗りたくないなあ。
ほんとに今日はカキコが少ないみたいですね。こんな日こそ出現頻度の低い私が！と気合を入れてやってきました。（つなげたらたまたまカキコの少ない日だっただけ）

＞みろりさま[812]
香ちゃんの声は不思議ですよね。すぐソバで聴こえるような、遠くからぼんやり聴こえるようなそんな感じしますね。
ところでみろりさまＨＮの由来はなんですか？？？（いえ、気になったので・・・(^_^;)既出だったらごめんなさい）

＞マダムさま[816]
しゅご～いはダジャレです（断言）

＞無言のパンダさま[850]
そうです、気管支炎もちです。この時期ツライですよね。
でも今年は寒くなるのが遅かった＆体力づくりのために走っていたので、幸いまだ無事に過ごしてます。薬飲んだときの倦怠感を思い出すだけで憂鬱になりますよ。 (>_<);;;


さて、明日は花のバン金。解禁楽しみにしてます☆
ではまた～。いもでした。

### [11850] 無言のパンダ	2003.09.26 FRI 00:10:56　
	
こんばんわ～★
す、少なすぎるっ！（￣□￣；）！！みんな～！今だ！今こそ立ち上がってココに集結するのだ～！

昨夜来れなかったので。。。「今さらレス」だーっ☆

＞圭之助さま（７７７・８４３）
生きてるぞーい！飲んでるぞ～♪だってお酒飲むと痛いの和らぐのよね、なぜか（*￣▽￣*）ゞ

＞ぽぽさま（７７９）
言ってた！言ってた！「べべたやん！」とか♪(￣▼￣)ニヤッちなみに小学生時代の運動会はいつもドベやったしぃ～ｖ（▼∇▼）

＞ｂｌａｃｋ　ｇｌａｓｓｅｓさま（７８０）
坂口＠二さん☆いいよね～♪やっぱ男のＢＧさまでも惚れちゃう～？もう少し彼が年齢を重ねて深みを増したらリョウの役にピッタリなんじゃないかって以前から思ってたもん♪でも今は最終的にＢＧさまがどんなオトコになるのか興味津々！

＞たまさま（７８２）
「にわかファン」だなんて言わないよ(￣▼￣)ﾆ私だってＣＨから入ったから他の作品読んだのごく最近だも～ん！大きい声じゃ言えないけど、アノ人気作品でさえまだちゃんと読んでないし～（*￣▽￣*）ゞ

＞将人さま（７８５）
さすが将人さま！ガッ＠ャマンも完璧網羅してらっしゃる♪実は私もガッ＠ャマンのレコードまで持ってたんだよ～ああ、それなのにそれなのに～！脳細胞の減少は止められない～(σ_-) コ＠ドルのジ＠ーは「ささき○さお」さんだったのかぁ～かなりシブイ系のキャラだったんだな～（*￣▽￣*）ゞ（←いいかげん思い出せ！）

＞みろりさま（７８７）
おおっ？！とうとうカミングアウトされたか～？！（腐女子発言♪）（▼m▼）しかし純粋だったあの頃の私はコ＠ドルのジ＠ーのことはアウトオブ眼中だったような。。。てへっ♪（*￣▽￣*）ゞ

＞Ｂｌｕｅ　Ｃａｔさま（７８９）
神谷さん出演の「高橋留○子劇場」に愛しのアノ方がーっ？！あ～見たかった！聞きたかった～！(>_<)

＞ｂｌａｃｋ　ｇｌａｓｓｅｓさま（７９０）
玉子バトル。。。どうやったって王子で変換できないよね～(^_^;)おおっ♪そういえば、まだ２０代前半のＢＧさま！ぜひとも「王子バトル」に参戦を～♪⊂（▼（工）▼ﾒ）⊃==Ｃ----____≡￤-）┼<

＞いもさま（７９５）
そういえば、いもさまも気管支が弱いんだよね？気をつけてね☆

＞ふじっちょさま（７９６・８４７）
ワタシも「ぼんさんが屁をこいた」で遊んでたぞ♪わ～い！世代を越えて「ぼんさん」は不滅だな（*￣▽￣*）ゞ
ところで大学ではパンダの講義を受けてるの？それともパンダの講義なんてイヤだ！って「抗議」してンのか～？！Σ(▼□▼メ）

＞いちのさま（８０６）
（><　；）☆◯（▼▼ ）ｏ　バキ！！これも「愛」のなのよ～♪
やる気のないヤツはビシビシ喝入れちゃるぅ～！(￣▼￣)去年のカツオくんはもっとピュアだったゾ☆まあ、それだけ大人になったということか(σ_-)

＞ｙｏｓｉｋｏちゃん（８３３）
なぬっ！？入院ですとー？！こりゃ一大事！（￣□￣；）！！まさかもう部屋で水着なんて着てないだろうな～？もう☆無理しちゃダメだぞ！＼（▼▼ﾒ）

＞こぶたさま（８４１他）
しばらく来られなかったのは、こぶたさまも隊長がグレてたせいなのね？！気管支にくるとツライもんね～お大事にね。(^_^;)
そういえば１９歳にシルバーの指輪って、女の１９は「厄年」っていうのに関係あるのかなぁ？本来「厄年」には「長いもの」って言うケド。そういや私もロクなことなかったな、１９歳って。。。きっと誰もシルバーの指輪をくれなかったからだな！(#▽皿▽）＝３　

最後になりましたが、最近来られた新しい方々☆これからもよろしくお願いしま～す☆
久々に長々とすんません！(^^ゞではまた～(^_^)/~

### [11849] リョウ357	2003.09.25 THU 23:36:00　
	
 [11847] ふじっちょ さん★
レスありがとうございます^^
やっぱり皆さん、中・高あたりでどっぷりハマルんですね★
CH って男だけじゃなくって、女の子のこともガッチリ捕らえてるですね^^
ここに参加さしてもらって、女の人のおおさにちょっとびっくりしてます★でも、女性と一緒に楽しめる、誰からも愛される漫画って感じがします^^★

### [11848] 葵（パンダが生きててビックリッ☆）	2003.09.25 THU 21:23:51　
	
　え～？０時回ってすぐの、ふじっちょさまだけ…今日はみなさん、どうしちゃったの？！ヽ(。_゜)ノ

＞いちのさま［１８４０］
文化祭ご苦労さまです。「餃子がーっ!!!」と言ってたのは去年ですか、いや～１年て早い×２…。（‐_‐;　　


＞こぶたさま［１８４１］
ＣＨのラストですか…はい、私も本誌派です。なんか「バーン☆」と印象深いラストだったので、コミックのラストが変わってて「およ？」と思いました。まぁコミックの方も「ラストとしてのインパクトのあるページ」っ感じがしますけどね。しかしテスト期間中に最終回ってのはやめて欲しい…後のテスト、散々だったもんなぁ…。（T_T)

　明日はバン金♪バン木の方々、すでにお楽しみになられましたか？明日の解禁まで、今しばしらくお待ち下さいませ～♪d(^_^o)

### [11847] ふじっちょ	2003.09.25 THU 00:21:30　
	
こんばんわ～★
CEを全巻集めきったので、遂にFCに進出しました(^o^)丿ただ問題が一つ、高い…古本でも250円は痛い(T_T)集めるのに時間掛かりそうです。面白いから良いけど(^_^)
FCに登場する空さんは漫画家ですけど、あれはやっぱり北条先生ご自身をモデルにしているんでしょうね(^_^)リアルに（生生しく？）描かれてますよね？北条先生も締め切り前はああなのだろうか…

＞[11827] リョウ357さま
僕のファン暦は中学からで、CHファンからでした。出会いは小学生の頃ですねリアルタイムでは原作は見ていませんでしたがアニメは再放送を良くやっていたので何かと見る事がありました(^_^)
その後中学生になってちょっと遠くの塾に行っていて帰りの電車の中で読むマンガを古本屋で物色していて手にしたのがCHでした。そこから段々引き込まれていき、高校に入った頃には共通の趣味との友人出会いもあって深みにハマりましたね(笑)それから大学に入るにあったてPCを購入し、今年ここに来ていまにいたるわけです(^o^)丿今はCHファンではなく北条ファンに晴れてなる事が出来ました(笑)

＞[11833] yosikoさま
お～おひさしぶりです(゜o゜)入院してたんですか？(￣□￣;)!!半壊で？それとも半開で？(笑)ゆっくり身体を労ってください。リハビリが済んだらまた華麗なyosiko節を響かせてください（爆）

＞[11838] へそたろうさま
僕も学校のクラブの連絡用掲示板に「XYZ」って書きましたよ(笑)恥ずかしいからみんなの目を盗んで（爆）近所の駅にも掲示板あったけどそこには流石に…それももう無くなってしまったな(-_-;)

＞[11813] COOさま
前に書くの忘れてしまった…スイマセンm(__)m
「こもれ陽の下で…」映像化ですか、いいですね(^_^)やるんだったらTV放送やOVAじゃなく映画にするのが良いと思います。クオリティーの高い作品を作るんだったら映画でしょう。大画面であの美しい表現が出来たら素晴らしい作品になりますよ(^o^)丿

＞[11839] 将人（元・魔神）さま
＞ふじっちょ様には僕のHPでいつもクイズ問題でお世話になっています
いえいえこちらこそ(^_^)＞何時も勝手にしたい放題してご迷惑お掛けしていますm(__)m
＞戦闘の時にTV画面に閃光がピカピカ光ってオモチャが反応して動くんでしたよね。
欲しかったけど買って貰えなかった(T_T)男の子は一度は憧れる“あれ”…欲しかったな(-_-;)
＞→ピカ＠ュウ事件の後は再放送の映像は変わってるのかな?
僕等の頃はあんな“光”バンバン流してたし、身体に変調きたす事も無かったけどな～僕等が頑丈なのか、今の子がデリケートなのか…(ーー゛)

＞[11840] いちのさま
もう学園祭ですか？(゜o゜)早いですね～（ウチが遅いのか？無縁なもんで　汗）ウチはまだ表向きには動きは無いですね、水面下では…(笑)去年は準備で休みの学校に授業と思って行ってしまった…(^^ゞ
カラオケはCHの曲と＠’ｚばっかし歌ってますね(^_^)行くのはもっぱら野郎共…悲しすぎる(T△T)

＞[11842] 村田だ! さま
はじめまして(^_^)ふじっちょと申します。どうぞヨロシク(^o^)丿「村田だ！」ってカッコイイHNですね。これからも宜しくお願いしますm(__)m

＞[11844] 千葉のOBA3さま
＞階段から転げ落ちて、アタマを打って今のふじっちょさまがあるんだなぁーーー！
どういう意味ですかっ！？？(￣□￣;)!!あっ…頭打ったから今の天才的なふじっちょ様が存在するといいたいわけですな（…虚しい）

＞[11845] 復活のパンダさま ♪（*^▽^）<※★：ﾟ*☆
おお～生きておられたか(σ_- )それとも帰ってこられたのか？ちょうどお彼岸だったし(笑)抗議の内容になるくらいだから僕は忘れられませんでした（爆）パンダさまの中にいる隊長の機嫌をとりながらゆっくり直してきて下さいネ(^o^)丿ついでにお払いもして…お大事にして下さいm(__)m

＞みろりさま
メールどうもですm(__)m奇麗な画像も見れたし(^_^)なんか「王子ロード」のお返しにはもったいない気もします(笑)

それでは長さに恐怖しながらサヨウナラ(^_^)/~

＞[11827] 獠357さま  
我的Fan经历是从中学开始的，是CH迷。相遇是在小学的时候。虽然没有实时看原作，但是动画经常重播，所以有看的时候(^_^)

后来上了初中，我去了远一点的补习班，在回家的电车里，我在旧书店里物色看的漫画，拿到了CH。从那里开始慢慢被吸引，进入高中的时候因为有了共同的兴趣和朋友的相遇而变得更深入了呢(笑)后来进入大学的时候买了电脑，今年来到这里直到现在(^o^)不要现在是C不是H粉丝，而是北条粉丝(笑)


＞[11838] へそたろうさま  
我也在学校俱乐部的联络告示板上写了“XYZ”(笑)因为不好意思，所以趁大家不注意(爆)附近的车站也有告示板，不过那里没有…那个也已经没有了呢(-_-;)


### [11846] chikka(Eastern Shizuoka)	2003.09.24 WED 23:24:41　
	
こんばんは。chikkaです。皆さん、お久しぶりです。急に寒くなりました。健康管理には十分注意しましょう。今実は咳が止まりません・・・。

【明日バンチを買いますが・・・】
久しぶりに冴羽さんの「もっこり」を見ました。しかし、年齢とともに冴羽さんの「ストライクゾーン」が広がった感じがします。昔は「１８歳以上３０歳未満のもっこり美人だけ」だったのが今では「３０歳以上の女性もＯＫ」になりました。４０歳間近の冴子さんにももっこりを迫っていますからねえ・・・。年齢とともに好みが変わり、年増の女性の魅力が分かってきたのでしょうか？早百合さんも３３歳ですし、香さんも生きていれば３１歳ですからねえ・・・。よく考えてみれば早百合さんは私と同い年です。

【果たしてどれくらい知っていますか？（実は自分も１９７０年生まれの独身「男」です】
早百合さんと「同い年で結婚していない女性有名人（バツイチも含む）」をあげてみると次の人々です。（インターネットで検索しました。）
●渡辺満里奈さん（１９７０年１１月１８日生）
－元おニャン子クラブ会員
●水野真紀さん（１９７０年３月２８日生）
－早百合さん役にピッタシ
●永作博美さん（１９７０年１０月１４日生）
－アイドルグループ「ｒｉｂｂｏｎ」の一員（知っている人いるかなあ・・・?）
●坂井真紀さん（１９７０年５月１７日生）
●島田珠代さん（１９７０年５月１０日生）
－吉本新喜劇所属
●相田翔子さん（１９７０年２月２３日生）
－元ｗｉｎｋ
●加藤貴子さん（１９７０年１０月１４日生）
「温泉に行こう」シリーズの主人公。実は彼女とは高校の同級生です。
●忍足亜希子さん（１９７０年６月１０日生）
－聾唖の（耳の聞こえない）女優
●中村由真さん（１９７０年２月１６日生）
●あいだももさん（１９７０年６月１８日生）
－２０代の時はお世話になりました・・・・。（何で・・・？）
●武田祐子さん（１９７０年５月２９日生）
－フジテレビアナウンサー（テツ＆トモとは幼なじみ）
●濱田典子さん（１９７０年８月２９日生）
－フジテレビアナウンサー
●原久美子さん（１９７０年１１月２０日生）
●王理恵さん（１９７０年３月７日生）
－王貞治監督の次女
●大下容子さん（１９７０年５月１６日生）
－テレビ朝日アナウンサー
●和久井映見さん（１９７０年１２月８日生）

【ファン歴】
小学校の時からジャンプは読んでいました。「ＣＡＴ’Ｓ ＥＹＥ」から入りました。アニメはスポーツ少年団や塾に入っていたのであまり見る事ができませんでした。「ＣＩＴＹ ＨＵＮＴＥＲ」の頃にはビデオが手に入ったので録画してみていました。もうかれこれ２０年ぐらいですねえ。

【実写版「Ａｎｇｅｌ Ｈｅａｒｔ」の理想のキャスティング】
香瑩・香萍－加藤夏希さん 「燃えろ！！ロボコン」ロビーナ役、「仮面ライダー龍騎」霧島美穂（仮面ライダーファム）役
冴羽リョウ－寺脇康文さん（１０年くらい前に「リョウちゃん」やっていましたし・・・）
槇村香－井川遙さん
野上冴子－高島礼子さん
海坊主－武藤敬司さん（プロレスラー）
劉信宏－杉浦太陽さん（ウルトラマンコスモス春野ムサシ役）
槇村秀幸－岸谷五郎さん
李堅強・謙徳－北大路欣也さん
陳侍従長－橋爪功さん
ドク－愛川欽也さん
若き日の李堅強・謙徳－斎藤慶太さん・祥太さん（「キッズ・ウォー」シリーズや「三年Ｂ組金八先生６」に出ていた双子）
餅山秀夫－斎藤暁さん（「踊る！大捜査線」スリーアミーゴズの一人、副署長秋山役）
こんな感じですがいかがでしょうか？


晚上好。chikka。大家，好久不见。天气突然变冷了。注意健康管理吧。我现在咳嗽不止。

【明天去买买买……】  
好久没看到冴羽先生的Mokkori了。但是，随着年龄的增长，感觉冴羽先生的“strike zone”扩大了。以前是“18岁以上未满30岁的Mokkori美人”，现在是“30岁以上的女性也OK”。因为接近40岁的冴子也迫近着为难呢···。随着年龄的增长喜好也在改变，了解到年长女性的魅力了吗?早百合33岁了，香小姐如果还活着的话也31岁了。仔细想想早百合和我同岁。

你到底知道多少?(其实自己也是1970年出生的单身“男”)
如果举出早百合和“同龄但没有结婚的女性名人(包括离过一次婚)”的话，就是下面这些人。(在网上搜索的。)
●渡边满里奈(1970年11月18日生)  
原小猫俱乐部会员  
●水野真纪(1970年3月28日生)  
很适合出演早百合。  
●永作博美(1970年10月14日生)  
偶像组合“ribbon”的一员(有人知道吗?)  
●坂井真纪(1970年5月17日生)  
●岛田珠代(1970年5月10日生)  
吉本新喜剧所属  
●相田翔子(1970年2月23日生)  
-原wink  
加藤贵子(1970年10月14日生)  
《去泡温泉》系列的主人公。其实我和她是高中同学。  
忍足亚希子(1970年6月10日生)  
聋哑的女演员  
●中村由真(1970年2月16日生)  
●田田(1970年6月18日生)  
- 20多岁的时候承蒙关照·····。(为什么……?)  
●武田祐子(1970年5月29日生)  
富士电视台播音员(和铁&友是青梅竹马)  
●滨田典子(1970年8月29日生)  
富士电视台播音员  
●原久美子(1970年11月20日生)  
●王理惠(1970年3月7日生)  
-王贞治导演的次女  
大下容子(1970年5月16日生)  
朝日电视台播音员  
●和久井映见(1970年12月8日生)  

【粉丝经历】  
我从小学的时候就开始读jump了。是从“CAT’S EYE”开始的。因为进了体育少年团和补习班，所以没怎么看动画。“CITY HUNTER”的时候因为有了录像带试着录了。已经有20年了。


【真人版《Angel Heart》的理想角色】  
香莹·香萍-加藤夏希《燃烧吧! !机器人控》罗比娜、《假面骑士龙骑》雾岛美穗  
冴羽獠-寺胁康文(10年前做了“獠酱”…)  
槙村香-井川遥  
野上冴子-高岛礼子  
海坊主-武藤敬司(职业摔跤手)  
刘信宏-杉浦太阳(奥特曼·宇宙春野武藏役)  
槙村秀幸-岸谷五郎  
李坚强·谦德-北大路欣也  
陈侍从长-桥爪功先生  
毒-爱川钦也  
年轻时的李坚强、谦德-斋藤庆太、祥太(《Kids War》系列和《三年B班金八老师6》中的双胞胎)  
饼山秀夫-斋藤晓(《跳跃!大搜查线》Three Amigos之一，副署长秋山角色)  

是这样的感觉，怎么样?

### [11845] 無言のパンダ	2003.09.24 WED 18:13:04　
	
こんにちわ☆

あわわっ！「ＭＫＰついに野垂れ死にか～？説」が流れてる～！？っていうか寸前？Σ(▼□▼ﾒ)待ってくれ～ぃ！生きてるぞ～！（必死！）
たしかに、ここんとこの気温の変化に身体がついてゆけず隊長がグレて。。。じゃなくて、体調不良だけど・・・(σ_-) だって喘息持ちなんだもん、夜中におぼれるように苦しくなって（元々泳げないケド）目が覚めるし～薬飲んだら副作用（？）で一日中ボ～として気分悪いし、おまけに首も肩も背中も痛ーい！そして何かにとり憑かれてるように重～い！（まさかアザラシ？！）これじゃあ後から声をかけられても振り向けやしないよ～うううっ・・・(T_T)と、いうわけでまたまた集中力散漫で秋刀魚が三万円ですがよろしくですぅ～。

遅ればせながら先週のＡＨの感想。。。
早百合さんが香瑩の中の香ちゃんを受け入れることができて良かったなぁ・・・天涯孤独の身になってしまった早百合さんには「妹」の存在は一縷の望み・・・希望だったのでしょうね。
一週間。。。リョウと香瑩、だけじゃなく香ちゃんとの３人の生活を垣間見ることになるのかも☆(^^)

またまた遅ればせながら。。。
Ｂｌｕｅ　Ｃａｔさま☆おめでとう～～～っ！！！
★：ﾟ*☆※>（'-'*）♪ｵﾒﾃﾞﾄｳ♪（*'-'）<※★：ﾟ*☆
その超ラッキーな幸運にあやかりたいぞ～！そうだ！いつか送ってもらったイラストを毎日撫で回そう～♪（*￣▽￣*）ゞ

ではまた夜にでも来れたらレスしに来ます☆(^_^)/~

### [11844] 千葉のOBA3	2003.09.24 WED 16:01:59　
	
こんにちわ☆先日テレビ東＠で７０年代のいわゆる「フォークソング」の特集をやっていたんですよ。司会はみ＠も＠たさん。
懐かしかったーーー！一緒に歌いまくった。北条先生はご覧になれなかっただろうなぁ・・・お仕事で・・・。（同世代ですものねー。）か＠や姫の「妹よ」聞くと、槇ちゃんを思い出しますよーーー！

１１８４０　いちのさま　　そういや去年、学祭で忙しいって言ってて、あれからもう一年ですかぁーーー！早いねー！！今年ももりあがりそうですか？

１１８３３　ｙｏｓｉｋｏさま　　え！？入院してたの！？マジで！？きぇーーー！ｙｏｓｉｋｏちゃんがいなくて寂しかったよーん！で、もう大丈夫なのかい？

１１８２５　ふじっちょさま　　階段から転げ落ちて、アタマを打って今のふじっちょさまがあるんだなぁーーー！納得！！そういや、あなたの母上？パンダさまはどうした？最近でてこないけど・・・。

それでは、また！おっ！明日は木曜日！！


### [11843] 圭之助（けーの）	2003.09.24 WED 13:28:05　
	
昨夜から、雨降りで寒いッす(-.-;)
いま、本格的に降ってるので部屋が暗い…。

＞[11842] 村田だ!さま
はじめまして！圭之助と申します。Ｃ＆Ａ大好きッ娘として「リョウつながり」で考えた事は何度もありますヨ(^-^*)
私個人としましては、長年彼らのファンやってて、ある程度のキャラを把握してる方だと思うので、Ａさんの体格・年齢・髪型などの雰囲気は結構イイセンいってるかな？と思いますが…人それぞれのイメージもありますし。しいていうならリョウにしては身長が、ちと低いかな（笑）…ところでＨＮ「村田だ！」さんは、まさか彼らと行動を共にしていらっしゃるアノ村田さんと関係ありなのでしょうか？単なる同姓かナ(^^ゞ

＞[11837] 葵さま
噂の出所はどうなんでしょうかね？当時はけっこう、まわりでプレゼントねだってるのを目撃したんだけど。お店の陰謀ってヤツだったのかな（笑）

＞[11840] いちのさま
…いちのさま、いつから男性相手に「くどきソング」を歌うように…（汗）

＞[11833] yosikoさま
お帰り！半壊してたの？よかった～全壊じゃなくて☆養生しながら、yosiko節復活させてね♪

＞無言のパンダさま
葵さまもいってるけど、生きてますかぁー(?_?)

### [11842] 村田だ!	2003.09.24 WED 11:42:05　
	
もし、エンジェルハートが映画化するとしたら、
「りょう」役は「チャゲ＆飛鳥」の、
「飛鳥涼」しか、いないと思うのですが・・・。

### [11841] こぶた	2003.09.24 WED 02:31:13　
	
ううううう～～～んっ･･･寝れない！なぜだ？どうして？
何を考えてるのだ･･･ｺﾌﾞﾀ･･･ワカラナイ･･･？
昼寝もしていないのに･･･なぜだ？
みんな寝てしまい（猫まで）つまらないのでカキコしてみました。

ふっと思い出したんですけど･･･ＣＨの最終回の絵のこと･･･
教会から香がえ～っとクロイツ将軍だ･･･そいつにさらわれてしまいりょうが香を助け出してから抱き合うシーンありますよね。
「死なせやしないよ･･･」のところ。カットがジャンプ本誌とコミックスと違うの知っていました？
こぶたはジャンプ本誌の方がギュッツと抱きしめているように感じで好きなんだけど･･･ご存じのみなさんはどちらが好き？

こんなこと悩んだ訳じゃないけど･･･

＞[11837] 葵さま
うけちゃいました(^o_o^) ホホホ･･･

まあ、ひとまず布団に潜ります。
