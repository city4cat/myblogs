https://web.archive.org/web/20020621140718/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=304

### [6080] 無言のパンダ	2002.03.14 THU 14:24:20　
	
こんにちわ☆
雨が降ってきました・・・！

私は「あんちゃん」って言葉使ってましたよ♪
母の弟、つまり実際は「叔父」なんですけど、年齢も１０歳くらいしか違わないし他の従姉たちもそう呼んでいたので、物心ついた時から自然に「あんちゃん」って言ってました。
でも、その「あんちゃん」も３９歳の若さで３人の子供達を残して、ある朝突然亡くなってしまいました。
李大人、謙徳兄弟のように極道系（？）の、あんちゃんだったけど私には子供の頃からやさしい「あんちゃん」だったのでとてもショックでした・・・。すみません！関係ないこと書いてしまって！
とにかく私も「あんちゃん」って言葉、好きです。

失礼しました。(^_^)/~

### [6079] のんたん	2002.03.14 THU 10:14:06　
	
＞Teddy様
私も「あんちゃん」→福山雅治と思いました。ただ、やっぱり日常で聞かれることばではないですよね。なんかひと昔前の雰囲気だけどとてもやさしい感じがする言葉、と思います。李兄弟の仲のよさをよくあらわしてるなぁとひとり思ってみたりします。


### [6078] 高田	2002.03.14 THU 09:41:07　
	
以前もかきましたが、私は最近は、この会社の心意気いいよね。応援しようとおもってかっている状態です

グラスハートは美人できれいで、服のセンスとかも（扉絵）とか
魅力的なので見入ってしまいます。


実は、A.Hの3巻のバンチの広告にのっていた、AHにかかれていた
「ペンに神宿る・・・・・・日本全土を恐怖の・・・どん底につきおとした・・・問題作」を目にして、編集部？の読者に対しての見解に疑問を感じ、

HPみたら公式なのに・・・管理人と意思疎通がされていないか
うっかりかいたか、わかりませんが、地域で発売日がことなることを念頭にはいってなかったことがさらに拍車をかけて、ほかの掲載している人かわいそうとおもってしまいました。

いったい、この雑誌にお金をはらっているひとに心のそこは優しくはない、表面的なのかもとおもってしまいました。なんかここまで立て続けだと、ちょっとつらい。

最初はリアルに感じたこのAHも、どんなに、人物の心情をえがいても、底の浅さがかんじられてしまいます。やっぱり、私もジャ
●プをよんでいたころより、精神的に大人になったのかもしれません。通勤途中に読む同僚もそういっていたのをおもいました。

今週の感想
弟も香萍を愛していたこととよめるのですね・・・
よめなかったです。
うしろでマジ？とかいってるし
ただなんかつごうよくかかれたみたいでした。弟
兄もなんかもサラリーマンみたいでした。


迫力ないですね。ひきこまれませんでした
きれいごとばかりで、わー太のほうが人物心情が深かったと感じるのはわたしだけでしょうか？AH最初はよかったのにね。読むたびに胸に人物心情がきました。　

### [6077] Teddy	2002.03.14 THU 00:12:13　
	
＞*rose*さん　
　と同じく、私も一瞬ミック？とキャラがダブりました。ちょっと僚とミックに置き換えてみたりすると往年のＣＨとだぶって見えますね。
　私はＣＨとＡＨがパラレルか否かについては無党派層（←？）なのですが、ちょっと懐かしくなりました。

### [6076] Teddy	2002.03.14 THU 00:04:21　
	
【３６話感想】
　今回、李双子が別人だと再確認。兄弟愛っていいですね。ただ恋愛がからむとすごい難しいけど、弟･謙徳は謙虚に身を引くには相当の覚悟があったのでしょうね。弟は兄の良き理解者だったのですね。相手のことが解らなければ、影武者であることを見破られず、演じ続けられないですからね。今回のままのいい人モードの謙徳であって欲しいけれど･･･。
【３６話を読んでふと思い浮かんだ疑問たち】
　①芋頭米粉ってどんな料理なんだろう？気にしだすと気になってしかたない･･･状態です。誰かご存知の方は教えて下さい。私はとりあえず芋と米？（漢字のまま･･･（TーT;）
　②あんちゃんといえば、福山雅治を思い出す人もいるかもしれない(私だけ？)。私の周りでは日常生活であんちゃんと言ってる人はいないけれど、これって流行語？それとも、東京近辺では普通使う言葉なんだろうか？
　③この船って、シャンインがＧＨの時に、手術室があった船では？ごめんなさい、船っていろいろあるんだろうけど、私はあまり知らないのです。多分、重さや形や機能によっていろいろあるんだろう程度。詳しい人、ちょっと簡単に解説してくださいー。

### [6075] けいちゃん	2002.03.13 WED 23:49:01　
	
今週号の中でちょっとかんじたこと・・・
北条せんせがかいてた「少年たちのいた夏」シリーズを思い出しました。少し歴史がさかのぼったお話もお上手なんですねーーー。裏テーマにはちょっと、ガクッてか。まじめなお話の中でのオチですね。

### [6074] ひろ	2002.03.13 WED 23:39:28　
	
こんばんは～(^o^)丿

今週は、ギャグ全くなし(信宏がいなかったから？)だけど、李大人の追想シーンにはジーンとくるものがありますね。
今週の見所は、李謙徳との兄弟愛と香萍(香瑩ママ)との恋愛ですかネ！？

追想シーンからうかがえるのは、李弟(謙徳)が李兄(堅強)の影になった理由でしょうね。
兄弟愛が強のはもちろんですが、まじめなタイプの李兄は、口達者で要領よい李弟の分まで、苦労を背負ってきたのでは・・・。弟として、そんなまじめな兄貴を助けたいと思う気持ちが影になろうと思った理由のまずひとつ！？
もうひとつの理由は、李弟も香萍を愛していたこと。船上で「デートの約束してきちゃった」、後々「香萍は俺が貰うぜ！」って言った言葉の裏には本音も隠されていたのでしょう。そして、兄貴が危険な立場(ましてや暗殺など)にいれば、香萍が悲しみ不幸になる、心の中で愛している香萍を悲しませたくない、と言う気持ちがそうさせたではないでしょうか。
これって、兄弟愛がもたらした悲しい弟の運命(さだめ)なのかも。李兄弟は、二人とも一途だったのですね。

以上、わたしのただの推測です。気になさらないように・・・。

それでは今日はこの辺で、サイナラ～(^^)/~~~

### [6073] K.H(もっこりの会 山梨支部)	2002.03.13 WED 23:13:33　
	
＞rose様[6044]
　ども、ご無沙汰です。えっ、もっこりの会ご入会ですか？う～ん、相談役の北条先生に相談してみます。
　rose様も別個派なんですね。やはり、ここにはそれだけC.Hへの思い入れが強い方が多いんでしょうな。
　結構真剣にお答えくださり、有難うございます。rose様はリョウよりむしろ香ちゃんに対する思い入れが強そうですね。
　最初のストーリー展開、非常にヘヴィーな感じでしたしね。どちらかというと「これがC.Hの続編だと思いたくない!」というのが、ボキの本音でした。
　いずれにせよ、有難うございました。

＞ニャンスキ様[6043]

　ども、始めまして!同意していただき、有難うございました。
　F.C、どうやら最初はジ○ンプに連載したかったんでしょうね、北条先生は。内容が内容だけに青年誌に連載されたとか。
　そういうことを知ると、作品の別の顔が見えたような感じがして、面白みが増えるんですよね。以前、もっこりメッセージで北条先生は「裏方のドキュメント」にやや難色を示されていましたが、ボキはそういう「メイキング」的部分を知ると、なおさら面白く感じるんで、「製作秘話」を単行本に掲載して欲しいんです。
　というわけで北条先生、裏タイトルもいいっすけど、制作秘話をボキは知りたいです。(P.S ダメ？)

＞おっさん様[6040]＆[6072]
　
　まいどどうも！営業部長のK.Hです。毎日お疲れ様です。
　もし北条先生がボキの要望を聞いていただいたら、第4巻以降はF.Cのときのような「製作秘話」が登場するでしょう。
　ちなみにおっさん様のご要望は？

＞sweeper様[6031]

　「・・・オレ、この姉妹に祟られてんじゃないか･･･?」

　毎度毎度!実は004の顔がイマイチよく思い出せないK.Hです。　そういえばあのサイボーグ組織(でしたっけ？)って、結成された理由って何でしたっけ?たしか、時代設定が第2次世界大戦直後くらいでしたよね、アレは。(って、このサイトと全く関係ないことカキコしちゃいました)
　ちなみに、sweeper様はA.H第4巻の北条先生のコメント、何になると思います?　　


＞ニャンスキ様[6043]  
您好，初次见面!感谢您同意。
FC，看来北条老师一开始是想在《Ji*mp》连载吧。因为内容的关系，所以在青年杂志上连载。
知道了这些，就好像看到了作品的另一个面貌，从而更有兴趣。在之前的"Mokkori"留言中，北条老师曾表示对“幕后记录”为难，我觉得如果能了解到"幕后制作"的部分，觉得更加有趣，希望能在单行本中刊登“制作秘闻”。
所以北条老师，幕后标题也不错，不过我想知道制作秘闻。(p.s不行?)


### [6072] おっさん（岐阜県：も～うダメ！）	2002.03.13 WED 22:20:19　
	
毎度、春休みになったせいか店が忙しいの忙しいの～応援先でも今の仕事先でも・・・ふう～も～うダメ！ということで今日もいつものご挨拶せ～のおっは～！！
あ～解禁っすね。疲れてるＴ＿Ｔ。
李パパ（葵様からもらいます。）いいっすね～。もとは純粋なひとだったんじゃなかったのかな？奥さん一筋！しかも香瑩はもろ母親似でしたね。性格は父親譲りかな？弟も兄貴思いのいい人だったんっすね。う～ん納得。
ところで、メッセージ更新してましたね。見て一言、笑わせて頂きました。裏タイトル・・・う～んいいねいいね。見てない人はスグ見てみよう。
ちょこっとだけレスを・・・
ワタル様（６０５６）、ちょ～～～～～～～うお久じゃないっすか？来れるときは来て見てくださいね。

ほんまはもっと書きたいのですが、今日は疲れたんで（明日も忙しそう）今日はこの辺で・・・？？？？？

### [6071] 月海	2002.03.13 WED 22:08:41　
	
はじめまして、月海(つきみ)と申しますm(__)m　CHのファンなんですけど、去年の秋頃偶然本屋で｢AH｣を見つけて、ただ今どっぷりはまっております(笑)

｢パラレル｣となってますが、それでもCHのメンツがまた見れて嬉しいです。3巻も購入し、CSのアニメチャンネルでは｢CH｣が毎日放映されてるので、ここ1、2ヶ月ばかり毎日が楽しいです(^^)

北条先生の描く女性は相変わらず【強く･美しく】大好きです。

### [6070] 葵	2002.03.13 WED 21:54:39　
	
　解禁ですぅ～♪李パパ（←この呼び方、かわいくありません？）「イイ人」でしたね。もう登場の仕方から「悪役、しかも親玉！」って感じだったから…香瑩のママを愛してたんだなぁってのがわかって、よかったです。それにしても李弟（謙徳）、不器用な愛しかたですよね。兄貴（李パパ）が惚れちゃったからって香瑩ママ（香萍）譲っちゃうなんて…。「俺は兄貴の影…それでいい」なんて、悲しいヤツ。（こーゆー人、個人的に好きだわ♪）謙徳の主人公ストーリーなんてのも読みたいですね。香瑩は…まだ李パパに気づいてないと、私は思います。ハイ。

### [6069] さえむら（解禁!!）	2002.03.13 WED 21:51:41　
	
こんばんわ。
いつもは解禁日に来られないけれど、今日は来られましたので、今週の感想を...。

[今週号の感想]
リョウの「あんたにはそういうヤツが必要だったのさ」という台詞に、リョウにもそういう人がいたのかな、もしくはそういう人がいてくれれば良かったのに、などと考えていそうで、なんだか切なくなってしまいました。
それから、李堅強さん、2巻の22年前の頃の方が太って（年をとって）いるような...？
今週号の李さんたち（子どもを作れというシーン）は15年前くらいですよね、たぶん。

[レス]
＞chikka様[番号を忘れました...]
詳細な考察をありがとうございます。
結局、先週号を見られなかったので、有り難かったです。
（あとは、マンガ喫茶かな～）

＞桃子さま
今でこそ、私も「A.Hはパラレル」と割り切れていますが、最初の頃は、仕事も手につかず... 。
*rose*さま[6067]も書いていらっしゃるように、「パラレル、パラレル」と私も唱えていました。
慰めにはならないかもしれませんが、今もC.Hのリョウ＆香は、新宿の街で元気に暮らしていると思いますよ！

それでは。お休みなさい。

### [6068] 桃子	2002.03.13 WED 21:45:53　
	
みなさんありがとう。やっぱりCHファンはみんな同じ思いなんですよね。まだ当分ショックからは立ち直れそうにない状態ですけど、今までのCHとは別物なんだって自分に言い聞かせながら、AHも最後まで読んでいこうとは思っています。でもできることなら・・・実はこれは、事故で意識不明になった香が長い眠りから目覚めるまで見ていた夢で、やっぱり二人はちゃんと結ばれる、ってゆ－よーな感じのラストにしてほしい！！　北条先生、お願いします！！

### [6067] *rose*	2002.03.13 WED 21:21:24　
	
　こんばんわ、*ｒｏｓｅ*です。ママは香萍かぁ。萍と瑩は＠ャッキーチェン版Ｃ・Ｈのあちらの役者さんが多く使ってますね。
特に瑩の方・・・。以外と香がなかったです。（しかも、丁度ビデオ借りてるし・・・。）
　李さん（弟）いい人なのぉ？ぐははは（フォント14ｏｒ18）って笑ったの何故なんだろう？？？（Ａ・Ｈ１巻）しかし、Ａ・Ｈパーフェクトガイドブックなるものがあればリョウのウインクコーナー出来ちゃいそう！！台湾の人が展開してると安心して読める！！北条先生のもっこりメッセージ、納得です。太ってる？李さんがすぐ浮かんだのですが、哀しいカナ、色男って事でリョウも考えました（殴）。ラヴだわっっ、リョウ・・・。
　今週号の19ページのセリフ（半分から上）リョウに向かって、ミックあたりに言わせたいわっっ。パイソン６連発、確定！！（Ｃ・Ｈ続編？で・・・。切望！！）
　月がキレイでいいわぁ、素敵！！コレ４巻の表紙ですよ、きっと！！コレも、また希望！！

　[6053]桃子様、はじめまして、こんばんわ。昨年末からお邪魔してる*ｒｏｓｅ*です。お気持ちわよくわかります。突然ならビックリですよね。１巻の注意書きに続編ではない、と書いてるのでそうなんだ、と、頭でわかっていても体になじんだのは最近です。そして、ここにいる皆さんに大変、助けられました。１人で読むのはツライです。Ｃ・Ｈが好きであれば、なおさら。３５冊分の歴史＋アニメｅｔｃ・・・は重い想いです。私も頭ではわかっていても結構、月日かかってるみたいだし・・・。もう少し時間が経つと・・・。最終的にはオノオノの感じ方になるのかな。
哀しくなってきたら、私は、呪文を唱えます。パラレル・パラレル・・・。（マジです！！）けど、私はＦ・Ｃも好きだし、表情と温度のある絵から北条ワールドに入ったので、いつか、続編描いてくれるハズ、と今は自信を持ってＡ・Ｈも好きです。作家さんが描きたい事描けないってゆーのは作家さんやってる意味なくなっちゃうし・・・。けど、けどっっ、やっぱ、見たいよね。
３５巻以降・・・。（それまでも結構ラブラブなんだけどネ）

　[6060]海里様、こんばんわ。コレもある意味、キリバン？
ウチは５つ上のダンナです。ダンナは北斗、出身者？なのでそっちの方を最初に見てます。私はＡ・Ｈしか見てないです。好む物が違うけど、微妙な価値観が一緒？でナントカ一緒にいます。（笑）もちろんＡ・Ｈ二人で見てるのですが、Ａ・Ｈによって私のＣ・Ｈ熱が復活し私がリョウがねっとかＰＣに向かってるとやきもち妬いてるようで・・・。解決策ってあります？（殴・蹴）
　追加で描いた瞳（愛蔵版と小説）すごくキレイですよね。
　それでは、また来ます！！

### [6066] ニャンスキ	2002.03.13 WED 20:52:01　
	
こんばんは。
今日コンビニに行ったらバンチが売り切れで
本屋さんで下の方からとって購入しました。
立ち読みの人がすごく多いんですよね～。

[6053] 桃子様
はじめまして、お気持ちわかりますよゥー（涙）
私も最初は書店で見つけて嬉しくて嬉しくて‥
家で読んで絶句でしました。裏切られたような気持ちも
あったし何といっても生きて２人でシティーハンターなんじゃないの？！って‥哀しみと怒りとどうにもならない
気持ちでただただ立ち尽くしていました‥。
うちの妹も、この前１巻を買って読んだらしいので
感想を聞いたら「・・・なんか、かなしい」って
おし黙ってしまいましたし、ＣＨファンには残酷な展開
ですよね。ここにいる方々のほとんどが桃子様とおなじ思いを持っていらっしゃる様です。

[6058] 千葉のOBA3様も言っておられた通り皆さん
優しく答えてくださると思うので思いを打ち明けると
少し楽になるかもしれません‥。

[6057] shi-na様
初めまして。
ＡＨの後ＣＨを読む‥私もそういう時あるんです。
でも、何故か香が、生きているって思えないんですよね‥
「生きている時の」物語りとしてしかＣＨを読めなくなって
きている自分の心の変化が何とも哀しくて、淋しいです。
純粋な気持ちではもうＣＨが入ってこなくて、ギャグさえ
もどこか切なく映るんで参ります。
shi-na様がおっしゃる様な素敵な続編、いつか‥
本当にあればいいなぁ‥‥

### [6065] Ｂｌｕｅ　Ｃａｔ	2002.03.13 WED 20:22:01　
	
　何度もごめんなさい、ちょっとだけレスがしたくって。
＞［６０６２］無言のパンダさま、わたしも若いころの李大人はＦ．ＣＯＭＰＯの辰巳さんに似てるって思ってしまいました、あれで目のとこに傷跡があったらそのものですよね（笑）

### [6064] Ｂｌｕｅ　Ｃａｔ	2002.03.13 WED 20:13:05　
	
　あ、「いい男」じゃなく「色男」でしたね、すみません(汗)

### [6063] Ｂｌｕｅ　Ｃａｔ	2002.03.13 WED 20:09:38　
	
　もっこりメッセージの「いい男は太っちゃいけない」にわたしも思わず笑ってしまいました(笑）でも李兄弟の場合、太ったせいだけじゃない気も(笑）
　今週のＡ・Ｈを読んで、みなさんが言うとおり、最初の頃の李大人はやっぱり謙徳の方だったのかなぁ・・・なんて思ってしまいました。なんとなく雰囲気が・・・ね。弟を李大人として表に出しておいて、自分は妻と娘を殺した人間を裏でずっと探ってて・・・そしてやっと、娘が朱雀にいることをつきとめ、黒幕が青龍の張だとわかった、ってことだったのかな、なんて（これ、前に誰か書いてましたっけ・・？）。
　謙徳が兄の影武者として生きることを選んだのは、自分も香萍を愛してしまったから・・・そして他の女なんて愛せないと思ったから、だったらせめてそばにいたくて・・・なんて思ってしまったんですけど、どうなんでしょうね。兄の背中を押したのも、自分の恋が報われないのならその分まで兄と香萍には幸せになってほしい、見せつけるくらいに、なんて思ったのかな？
　今回の話で、堅強が奥さんを愛していたのがわかって、少しほっとしました、だって今まで娘への愛情しか見えてなかったから・・・。でもまさか、ここで李兄弟と奥さんとのなれそめが描かれるとは思わなかったので、ちょっとびっくりしました。
　ところで・・・香瑩はあの「李さん」が自分の父親だとうすうす気付いてるんでしょうか・・？　まぁたしかにあんな反応したらばれても仕方ない気も。だとしたら・・・香瑩は、どうするんだろう・・・。

### [6062] 無言のパンダ	2002.03.13 WED 18:39:32　
	
こんばんわ☆

娘の通う学校の近くに新しくコンビニ（ファ＠マ）が出来たので行ってみたら、なんとバンチを置いてない！信じらんな～い！
近隣にある「セブン＠レ＠ン」や「ロー＠ン」には、ちゃーんと置いてあるのに・・・思えば私とＡＨとの出会いはこの「ロー＠ン」だったなぁ。←（遠い目・・・）

「今週のＡＨ」
とりあえず今週号を読んだ限り、若い頃の李大人と謙徳の兄弟関係って親友同士のような素敵な関係だったのですね。
でもあれって、男同士の兄弟だからこそ成立するのだろうなぁと思ってしまいました。姉妹だったらあんな粋な真似は出来ないだろうなぁ。
それにしても、いい男でしたね！二人とも。（双子だから・・・ネ）特に李大人はＦＣの「辰巳」に似てましたね～(^^)

「本家無言のパンダ」の今週の一言
「謙徳ってこんないい人だったん？それをどんな理由があったとしてもコヨーテは殺したんだね・・・！」

とりあえず時間がないので、これにて失礼します(^_^)/~

### [6061] K.H（祝、解禁！！）	2002.03.13 WED 18:09:35　
	
　もっこりメッセージ、送信されましたね･･･なんか、意味深な・・・(笑)

＞今週のA.H
　文化大革命のころのエピソードとは、以外に驚きです。李兄弟、仲良し兄弟だったんですね～。
　しかも、あの「蛇頭」の一員だったとは。ウラ組織の一員だった割には、結構明るい二人でした。
　謙徳は一生、堅強の影でいるつもりだったとは。兄思いの弟ですなあ。
　ところで、やはり李大人は最初のころとキャラが変わっていますね(特に先週号)。彼の変化は何を意味するのか、全くもって謎であります。

　しばらく忙しかったんでカキコしなかったんですが、結構レスがたまってしまった！というわけで、日付が変わるころに再びお邪魔しますんで、その時に、レスします。
　