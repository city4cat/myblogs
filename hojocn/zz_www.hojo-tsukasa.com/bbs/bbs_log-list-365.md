https://web.archive.org/web/20031021104042/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=365

### [7300] いも	2002.08.03 SAT 23:24:23　
	
こんばんわ。
大分出遅れましたが、今週の感想書きにきました。

若い！！とにかく香もリョウも若い！
皆様のおっしゃるとおり、ＡＨとＣＨは違うんだぞって実感した気がします。北条先生の「パラレルだ」って意志が強く感じられると言うか‥。
気になる二人の年齢ですが、最初阿香が５歳くらいっていう言葉から、単純に１０年位前なのかなあって思いましたが、それにしては二人の顔が若い‥。看護学校時代から見習いで働いていたってとこからすると、香はまだ十代なのかも知れないですね。
ところで早くも来週が気になっているんですが、槇村兄が話に絡んでくるようで。槇村兄はやっぱ死んでしまうのかな？冴子との接点なんかが気になるところです。

ちょいレス。
＞yaekoさま[7289]
弟さんご無事でなによりですね、お大事に。
「海ちゃんが外国人！？」説に深く納得しちゃいました。
そうですよね、それならちょっと怪しい部分はほとんど片づいちゃいますもんね。（オイッ！）
私もyaekoさまが指摘するまで全く気付かなかったけど、その説に一票入れたいと思います。（^^）

来週は増刊号。スペシャルプレゼントはどんなもんなんでしょ？
それではまた。いもでした。

### [7299] 葵（れす・おんりぃ）	2002.08.03 SAT 21:16:08　
	
　草むらで虫の音を耳にしました。暑いようで…秋は着実に来てますねぇ…。（しみじみ）

＞ＯＲＥＯさま［７２８８］
　自転車から転倒ですか？全身負傷のご様子、お大事になさって下さい。
＞ｙａｅｋｏさま［７２８９］
　弟さん回復してらっしゃるようですね。よかったです。（しかし最近ここのＨＰ、事故ネタが多いなぁ…☆）

＞まろさま［７２９８］
　「香には頭が上がらなかった」…なるほど！と思いました。さすがに読みが深いですね～♪「命の恩人」たる香との今後…。あぁ～次週が待ち遠しい！！

　リターンズ聞き忘れたました。これから行ってきま～す！（恥）

### [7298] まろ（只今休憩中♪）	2002.08.03 SAT 17:43:00　
	
遅まきながらで恐縮ですが（汗）
今週号の感想です。

[今週号の感想]
こういう言い方は語弊があるのですが、二人の1ｓｔコンタクトが、
ＣＨとは全く逆な立場で･･･しかも、無敵な筈のリョウちんが、
ともすれば致命傷になる負傷をした所に、
正義感の強い香が通りかかるのを目の当たりにして、
改めて、ＡＨの『二人』の背負う、”過去の重さ”を考えさせられました。
（→コアミックスのＨＰのＡＨのリョウちんのコメント（※２）に
　思わず納得（？）してしまったりして・・・笑）
そこで気になったのが、彼女の兄（？）である、秀幸の存在ですね。
やっぱり、リョウのパートナーだったんでしょうかね？
日頃、兄からリョウのことを聞かされていたから、
負傷した彼に対して・ああ云う言葉をかけられたのかなぁ、とか（苦笑）

う～ん、奥が深いッス（汗）

あっ、そう×２･･･
香が生前、花園診療所の看護助手（でしたっけ？）をしていたって事っスが、
智ちゃんは彼女の後輩って事っスよね？
これまた色々考えてしまうんですが、キリがなくなるので（苦笑）
これにて退散します。


※１･･･多分、ＣＨの２ｎｄコンタクトの頃･･･１９歳位に見えますね。

※２･･･『香には頭が上がらなかった』という『くだり』です。
　　　　リョウにとっては、文字通り『命の恩人』ですもんね。

### [7297] 無言のパンダ	2002.08.03 SAT 17:23:16　
	
こんにちわ☆

今週のＡＨ、みなさんがおっしゃってるようにＣＨとは違う、パラレルだということをあらためて感じる内容でしたね。
今までパラレルだとわかっていても、どこかでＣＨにつなげていたイメージがいい意味で壊されそう・・・。
香は大人っぽくて冷静な女性みたいだし、リョウは・・・か、かっこいい～！（メロリ～ン♪）
来週は槙村のことも出てきそうですが、どんなふうに描かれるんでしょう？やはりどこか設定が違ってるんでしょうね。
信宏大好きな「子パンダ」は、いつもシャンちょんには嫉妬の炎メラメラなのですが（笑）今回のストーリーと５歳の香瑩のことはかなりお気に入りみたい☆今からふたりで来週のバン金を心待ちにしていまーす(^^)

### [7296] ハル	2002.08.03 SAT 15:59:43　
	
こんにちわ～！！とても暑くて溶けてます（￥O￥）

sophia様！！ビデオ届きました！！何回も何回もひたすら見てます！！御礼が遅れてすいません！！超感動です！！長年引きずっていたこの思いを浄化して下さってホンマにありがとうございます（＾３＾）
１日１回見る事が私の日課になっております！！
言葉足らずで申し訳ありません（・；）
では、良い週末を・・・。

### [7295] いちの	2002.08.03 SAT 15:55:57　
	
遅ればせながら今週号のＡＨをさっき読みました（汗）。最寄のコンビニに行ったのですが、この暑さのせいで汗ビッショリ！！なんでこんなに暑いのか・・・。日本おかしいんじゃない！？地球があぶないの！？

[今週のＡＨ感想]
ＣＨでの香との出会いを知らない私には面白さが半減したのか、こういう出会いなの？と思ってしまいました・・・。それにしても、香～！！野良犬呼ばわりはないでしょ～！！！リョウちゃん死にそうなのよ～！！！

でも、久しぶりに若い２人を見れてうれしいです。夏休みにＣＨ全巻制覇すればもっと面白く感じられるんだろうなぁ～・・・。古本屋にＣＨ全巻７０００円のところがあったんですけど、まだあるかなぁ？

### [7294] kinshi	2002.08.03 SAT 13:30:22　
	
こんにちは、
３１～１日にかけて、外国からの来客の東京案内に、足がどうにか
なりそうな、kinshiです。
こうゆう時こそ　ファルコンのカレーが食べたいです！。
あまりにも、つかれて、バン金　さえ、わすれていました。
・・・というより、金曜日は、午後１時ごろから、東京は、
すごい！！雷雨で、そこらじゅうに　落ちまくっていましたからね、
怖くって、外に出られませんでした。いい休養に当てた一日でしたが、
バンチがなかったのが、残念！これから、かってきま～す。
では、またね♪

### [7293] まろ	2002.08.03 SAT 10:29:03　
	
うわ～（滝汗）
スミマッセンッ！！
最近ワタシのＰＣ（Ｗｉｎ９８搭載モデル）が不調を来す事が多くって
（→ＩＥを５以降にしてから、急にクラッシュする事が多くなった）
さっきもオフライン作業をクリックしようとしたら、
イキナリ『送信』を始めてしまい、
挙句の果てには、そのままフリーズ（大激怒～！！）
全く、マイク○ソフトのＯＳって、こんなんばっかしやんけ（苛々）

ようやく復旧しまして、再投稿モードっス（苦笑）

[７２８０]ｓｏｐｈｉａさま
レス、有難うございます。
えっと、例の光ファイバーとＡＤＳＬの端的な違いについてと、
光（ファイバー）回線利用料について少々お話しようと思ったのですが、長くなるので、ワタシのＨＰでのＢＢＳ
（タイトルは『光ファイバーとＡＤＳＬの違い』です）にて
解説しておきました。
よかったら、目を通してみてください。

[今週号の感想]
う～ん(滝汗)
出勤時間になってしまいました(苦笑)
午後にでも、また伺いに来ます(汗)

### [7292] 千葉のOBA3	2002.08.03 SAT 07:40:58　
	
おはようございます。連日の雷雨、すごいですねー。夏ー！！ってカンジ。

今週号の感想です。　なんか、今までＡＨはＣＨとは別と思って、そう思おうと思ってもできなかった部分というのがあって、でも今回からのお話読んで、やっと完全にパラレルと思えるようになるかなと・・・。今週号読んでて自分もアシャンと一緒に不思議な夢の中に入って行くような気がしました。（連載開始されて１年ちょっとたって、ホントにホントでこれがパラレルの入り口なのね・・。みたいな・・・。）・・・こんな、間の抜けたコト思うの私だけかい？？？

しかし・・・。りょうちゃん若い！かっこいー！香ちゃんも、シャープでステキ☆ＡＨにはＡＨの、この二人の積み重ねた年月があるんだろうな・・・。と思うと、ずっと見ていたい・・そんな気がします。あーーー、また一週間が長いなーーー！

７２８８　ＯＲＥＯさま　日焼けの他に打撲ですかー！？でも、たいしたことなくて良かったですよね。本当に暑くてボーッとしがちなので、注意が必要です。私もドジだから気をつけます。

今日はこれからお仕事。夜は子供の野球部の飲み会。あーあ、野球おやじ達の能書き聞きたくないぜ・・・。（ホンネ）

### [7291] まろ	2002.08.03 SAT 07:36:42　
	
日が明けてしまいました（苦笑）
バン金、ちょっと来にくいなぁ、って思う今日この頃です。
（↑仕事の関係上）
それでは、レス＆[今週号の感想]です。

レスです。

[７２８０]ｓｏｐｈｉａさま
レス、有難うございます。
えっと、例の光ファイバーとＡＤＳＬの端的な違いについてと、
光（ファイバー）回線利用料について少々。

まず、ＡＤＳＬは既存の『アナログ』電話回線（ＩＳＤＮはで

### [7290] yaeko	2002.08.03 SAT 02:28:56　
	
また登場です。
「映画についての巻」
「この胸のときめき」米、2000年制作（すいません昨日2001年と書きましたが2000年の間違いでした）のラブストーリーです。内容は昨日書いた通りです。8月号のスカパーの番組情報誌（TVガ○ドみたいなの）に少し内容が載ってます。スターチャ○ネル　プ○スのページです。良かったら見てください☆役者さんですがあまり詳しくないので名前は？？？です。でも観た事あるんですよねこの顔。おそらく有名な人？
それでは。

“关于电影的那一卷。”  
这是一部美国2000年制作的爱情故事(对不起，昨天写的是2001年，错成了2000年)。内容和昨天写的一样。8月号的scaper的节目信息杂志(TV好像〇度)上有一些内容。是starcha〇奈尔普〇斯的网页。可以的话请看看☆演员，但是不太熟悉名字是? ? ?是。不过我看过这张脸吧。恐怕是有名的人?


### [7289] yaeko	2002.08.03 SAT 02:01:30　
	
こんばんは。皆さまあほ弟のこと心配していただきありがとうございます。少しずつですが回復してきています。交通事故って怖いですね。もし打ち所が悪かったらって思うとホント怖い。無事でホッとしましたが。

「今週の感想の巻」
「野良犬」って最近どこかで…？あ、ルパンだ！先週の金曜ロードショーのルパンで似たようなこと言ってたんだって思いました。次元がルパンに出会った時、「野良猫か」って言ったんですよ。え？今週のAHってパクリかい？？？って思ちゃいました。
あと、なぜ阿香はりょうと香の出会いの夢を見ているのかな？香は見せているのかな？何か阿香が成長するために必要なエピソードが二人の出会いにあるってことだよね？次回がますます楽しみだな♪来週号が合併号ってことは、その次は休みってこと？来週で何が何でもすべてが解明されるといいなぁ。
あとの感想は皆さまと同じってことで。（手抜きっす）

「海ちゃんって！の巻」
昨日書き忘れたんですが、映画「この胸のときめき」の主人公(夫）に黒人の友達がいるんです。それを観て思ったんですが、AHの海ちゃんって日本人じゃなくて外国人（黒人）って設定なのかなって。本名ファルコンって書いてあるし。ずっと日焼けしたって思ってたんですけど、パラレルって何でもありですもんね。もしかして今頃気付くのって遅すぎでした？？？皆さまどう思います？この発想。
それでは。

“这周的感想。”  
“野狗”最近在哪里…?啊，是鲁邦!我想上周五上映的《鲁邦》里也说过类似的话。次元遇到鲁邦的时候，我说:“野猫吗?”呃?这周的AH是山寨吗? ? ?我想。
还有，为什么阿香会梦见獠和香相遇呢?香给你看了吗?两人的相遇中一定有香成长所必需的插曲吧?越发期待下次了♪下周号是合并号，也就是说下次是休息吗?希望下周什么都能弄清楚。
之后的感想和大家一样。(偷工减料)


“叫小海的卷。”  
昨天忘记写了，电影《怦然心动》的主人公(丈夫)有个黑人朋友。看了那个想，AH的小海不是日本人而是外国人(黑人)的设定吗?还写着本名falcon。我一直以为自己被晒黑了，但其实什么都有。难道现在才意识到太迟了? ? ?大家怎么看?这个想法。


### [7288] OREO（首イタ腕イタ腹イタ）	2002.08.03 SAT 01:13:12　
	
こんばんわ～皆様。昨日の自転車から転倒のせいで昨日はなんともなかったのに首が鞭打ちのように、腕と腹が筋肉痛のようにイタイイタイ・・・シップべたべた張ってます。
腹が痛いと笑いたくても痛くて笑えない・・・クゥ～辛ッ！

今回のＡＨ。５歳の阿香かわいかったです。
７２８５たま様（こんばんわ）のおっしゃるとうり、
パラレル、パラレルってすごい実感しました。
私も一話完結かなって思ってたりしたので
続いてくれて嬉♪でも早く続き読みたいッ！！

＞７２８０sophia様
こんばんわ。ごめんなさい。ほんとはもっと詳しく
説明をしたかったのですがあまりスペースをとるわけにも
いかなく、簡素な説明になってしまいました。
説明不足なところがありましたら、言ってくださいね。
説明しなおしますから。（＾＾）

＞７２７８千葉のＯＢＡ３様
こんばんわ。キレイなせみですか？（笑）
私も脱皮するんだからなんかに進化するのだろうと（笑）
（でも後々日焼けってシミとかの原因と考えるなら退化？）
でもこんな中途半端なむけ方じゃ脱皮失敗な気がします。
（もぉ！！かゆくて、かゆくてたまらんっ！！！！）

＞７２７７yaeko様
こんばんわ。はじめまして。
弟さん、大変でしたね。でも無事でなにより。よかったです
その映画私も見てみたいです。ＴＳＵＴ＠Ｙ＠でバイトの身なので早速チェックしてみます。それってアクション系でしょうか？それとも人情ストーリー系でしょうか？

ハッ！！もう寝なくては・・・おやすみなさい。ｚｚｚｚ

### [7287] Beer	2002.08.02 FRI 23:56:47　
	
香が若くてびっくりしました。今までの香はやっぱり年をとったということであえて老けた感じで描いてあったんだなあと再確認しました。それにくらべて冴子は全然年をとった感じじゃなくていいですねえ(^^;)

しかし、看護学校に行っている＝医療知識があるはずの香が、ああいう理由でドナーカードを持つことにしたというのはあまりにあまりなんじゃないでしょうか。
そうじゃない私でも「ええっ？」と思いましたから。
リアリティを追求しているマンガではないとはわかっているんですが、（CHはエンターテイメントが一番なんですよね？）さすがにちょっと「そりゃないんじゃないの？」とつぶやいてしまいました。

### [7286] けいちゃん	2002.08.02 FRI 23:17:26　
	
今回の話
そっか～香ちゃんって看護婦見習いで、ドクのところに出入りしていたなんて、ＣＨと設定がちがいますよね
それに、最初からなんか、美人・・ぽい描かれ方してる
ＡＨとＣＨは別物ということ再確認しました
でも今後楽しみ～

### [7285] たま	2002.08.02 FRI 22:15:06　
	
パラレルじゃ～。パラレルじゃ～。
今週のＡＨはを改めてパラレルっというの実感しましたね。

ＡＨはアシャン中心の話かと思ってたので、
今週号見るまで「リョウと香の出会い」は一話完結かなって思ってたんです。
そしたら「野良犬と天使の新たなるドラマが始まる・・」って。

まじっすか～？！この話続くんすかぁ～？！
いやっっほぉ～う☆♪☆
北条せんせ、ありがとぉ～☆♪☆（ごさいます。）

若いリョウは本当にかっこ良くって、かっこ良くって、かっこ良くって。
香ちゃんも勇ましいよぉ。（えぇ～女やなぁ～）
初めて出会ったのに、リョウに向かって「野良犬」だなんて、さすが香ちゃん！パラレルでも男まさり(？)ですね。

来週もこの話の続き・・・だぁー！（とっても楽しみぃ♪）

＞yaeko　様[7277]こんばんわ。
そんな映画があるんですか？「この胸のときめき」とても見たいです。
yaeko様の話だと本当にそっくりですね。
主演は誰なんでしょうか？
それから、弟さん無事でなによりです。

### [7284] 葵	2002.08.02 FRI 21:30:59　
	
　ゴロピカドンと素晴らしかったですねぇ…。これで少しはらくに寝られる…かな？！（^_^;

　Ｂｌｕｅ　Ｃａｔさま［７２８３］同様、ＣＨとＡＨの違いを改めて実感☆香ちゃん、看護学校でしたか～。かすかにシュガーボーイの面影があるのがウレシイな♪リョウも若くてドキドキ。（しかしリョウってば、いつからアノ服装なの？！）子供の阿香が「パーパを助けて！」とさまよう姿はいじらしかったですね。涙まで流して…。阿香にとって、リョウは大切な「パパ」なんだなぁ…と実感です。さてさてリョウと香、ＣＨでない二人の出会い…。次週が待ち遠しいよぉ～っ！

＞ｙａｅｋｏさま［７２７７］
　弟さん、大丈夫ですか？暑さでモーローとしがちです。事故を起こさぬように、事故に遭わないように、注意しなくちゃいけませんね☆

### [7283] Ｂｌｕｅ　Ｃａｔ	2002.08.02 FRI 18:58:05　
	
　こんばんは。　今週のＡ・Ｈの感想です。
　やっぱりリョウと香の出逢いは、Ｃ・ＨとＡ・Ｈでは別物だったんですね。でも、Ａ・Ｈでの出逢いもすっごくわたし好みなかんじなので、続きが楽しみです。これで完全にパラレルとして割り切って見られるかな。
　もうっ、若いリョウ、なんてカッコいいんでしょう☆　すごく苦しそうなのに、助かるのがわかってるからなんか安心してどきどきできるんですよね。でも、あのときの香は、あの男（リョウ）の正体を知ってて「野良犬」なんて言ったんでしょうかね・・？
　それにしてもまさか香がドクんとこで看護婦見習いをしてたとは・・・じゃ、智ちゃんは、もしかして看護学校時代の同級生だったりとか？　仕事をしてるせいか、Ｃ・Ｈの頃より香ちゃんが大人っぽくみえました、ていうかなんか、Ｃ・Ｈの頃よりカッコよかったです♪

　あ、わたし今日やっと、Ｃ・Ｈの英語版ＣＤドラマをちゃんと聴いたんですけど、本を見ながら聴いたのに途中でどこのセリフなのかわからなくなったりして・・・なんか暗号聞いてるみたいでした（苦笑）

### [7282] ゆうちゃん	2002.08.02 FRI 15:12:43　
	
お久しぶりです。
７２７０ｋｍｋｍさま。
秋田市八橋にある、文教堂で売ってます。私ははじめ雑誌コーナーにあるのかと思って探したけど無くて、あきらめ帰ろうとしたらレジ前に平積みになってました☆
買ってきただけで、まだ見てませんけど・・。それでは。

### [7281] 無言のパンダ	2002.08.02 FRI 14:40:39　
	
こんにちわ☆

＞ｙａｅｋｏ様（７２７７）
弟さんが事故に遭ったという一報にさぞ驚かれたことでしょう。
幸い軽症ということですが、早く全快されるといいですね。
私も昔弟が近所の道路で事故ったと弟の友達が知らせにきて、現場に駆けつけたら倒れた自転車と血痕だけが残されてた・・・ってことがあったんです。
真相は事故った相手（幸い自転車）が頼りない人で、通りかかった大学生が病院に運んでくれたということだったんですけど、そんなこと知らないから、もうパニックですよ～(^_^;)
今でも、あの時のドキドキした気持ちとあの日が水曜日だったという記憶はずっと残っています。もう二度と身近な人間が事故ったというような知らせは受けたくないですよね。

今からバンチを買ってきますので失礼します。
