https://web.archive.org/web/20031027183422/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=538

### [10760] ayako	2003.06.09 MON 19:21:56　
	
AH⑦買いました！この巻はまだバンチを立ち読み（ごめんなさい。買ってません）してなかった頃の内容だったので初めて読みました。感想は長くなるので、一言で終わらせます。「すっげぇ良かった！」

⑧は９月発売ですね！私は１０月だと思ってたからちょっとうれしいです。でも三ヶ月読めないのは寂しいです。

⑦のプレゼントの応募は確かにＡかＢ、迷いますよね。

ふじっちょさま
私も＠’ｚのファンで、ファンクラブ入ってます。去年はＦＩＦＡワールドカップ公式イベントでエアロス○スとの共演チケットが当たって行きました。

### [10759] きよ	2003.06.09 MON 18:45:01　
	
ＡＨ⑦読みましたぁ！！！学校の帰りにコンビ二によったらありました。もしかして朝にはもう売ってたのかな・・・。阿香の変化がとてつもなく嬉しかったですｖ
次は９月なんですよね～・・・。くぅー・・・長いぃ・・・＞＜

★千葉のＯＢＡ３さま
きよの命じられたのは・・・ただのビラ配りです＾＾；
今日初めて配ってきましたｖ結構ドキドキしてた超小心者のきよです；

### [10758] Ｂｌｕｅ　Ｃａｔ	2003.06.09 MON 18:40:36　
	
やばい、プリマグラフィがプリマウラフィになってる（汗）つっこまれる前に訂正しとかなきゃ（あせあせ）

### [10757] Ｂｌｕｅ　Ｃａｔ	2003.06.09 MON 18:33:15　
	
　こんばんは～、Ａ・Ｈ７巻、買ってきましたよん（＾-＾）ノ
　表紙の作者コメント、今回も笑わせてもらいました（笑）　ホント、５年といわず１０年もっと連載が続いてくれるといいなぁ。
　カラー扉が、あの西部劇風のだったのも嬉しかったです（にこにこ）。このイラスト、大好きだったからね☆保安官りょうちゃんも、カッコよいです（*＾＾*)
　内容では・・・おんぶされた阿香の中の香と、リョウとの会話のシーンではやっぱりきゅんきゅんめろめろしてしまいました☆ここばっかりついつい何度もひたって読んでしまいます（はあと）。
　そういえば、単行本の最後の方に載ってた広告に、８巻は９月発売予定、ってありましたね～、あと３ヶ月、楽しみですね♪
　プレゼントは、応募するときにどっちか選ばなきゃいけないんですね、どうしよう・・・どっちにしようかなぁ。ホントはプリマウラフィが欲しいんだけど、競争率高そうだからなぁ。

### [10756] みろり	2003.06.09 MON 12:05:27　
	
いっちばぁ～ん！！
購入報告いちばん！！
ひひひひ、昼休みに近くのほほほほ、本屋でＧＥＴ！
まだ読んでないけど！
皆様と違い、ファン暦の浅いワタシはこれ、まだ内容知りませ～ん。ふふふふふ。
午後からへんな笑いを漏らしながら仕事しそうです。
しかし、巻末の予告は何なの！？３ヶ月、焦れてろってこと？
（ＴＴ）

### [10755] ふじっちょ	2003.06.09 MON 11:23:56　
	
おはようござります。
学校昼からなので行く前にＡＨを購入して向こうで読もうと思い、家を11時頃に出てＡＨを無事購入…しかし！定期が切れてる…(T_T)結局足止め。昼過ぎに母親に送ってもらう事になって今家にいる僕なのでした…

＞[750]＆[751] パンパン大佐
何をそんなに慌てているんだい？貴方らしくない(￣ー￣)
苦し紛れに捕まえようたって無駄ですよ…
だって(-。-)y-゜゜゜はシャボン玉ふいてんですから…苦しいいい訳…

＞[752] 千葉のOBA3さま
そうそう僕って黒くて長くてちょんまげみたい…って誰が「チョ＠ジ」やねん！！(-_-)-☆←のりツッコミ？
＞ふじっちょさまもたくさんＨＮあるよねー
全部非公認です！！パンパン大佐（自称三女）が言ってるだけであり、あたしゃ認めない！あたしゃそんなＨＮ認めないよ！！←誰？

そろそろ学校に行きます。また夜ぐらいに来るかも…

### [10754] 圭之助（けーの）	2003.06.09 MON 11:23:14　
	
あで？何か変な空白あるカキコ＆最後の「７４２」削除うるの忘れてますワ☆すんまそんm(_ _)m

### [10753] 圭之助（けーの）	2003.06.09 MON 11:21:04　
	


＞１０７４０　葵さま
遅ればせながら、ハッピバースデー♪同世代へようこそココへ～くっくくっく～♪　(*o*;)はっ！い、いかん。こんなノリしてるとまたＭＫＰ現象が起きてるって指摘されちゃう…（汗）しかし、圭之助は早いか遅いかでないと反応でけんのか？
あと、パンダさまからもご指摘がありましたが、私が今回買ったのは「百万ドルの陰謀」だよ～。知ってなのかマジボケなのか??
でも、そーいう葵さまが好きよん☆

＞７４２　yosikoさま
ＡＶって表現、ほんと紛らわしいですよね☆文字だけ見たら「ど、どっちの？！」って焦っちゃう（笑)

あー、本屋に行かなくちゃ…でも今日行きたいスーパーと反対方向なんだな…暑いし…お昼食べてからにしようかな(^-^*)皆さんはもう、買われたかな～？


１０７４２

### [10752] 千葉のOBA3	2003.06.09 MON 08:01:27　
	
おはようございます。今日は7巻の発売日！ルンルン♪

７５０　　無言のパンダさま　　度重なる「オ＠ソ」発言・・・。絶対計画的犯行と見た・・・。（笑）ふじっちよさまへの「ふじっちょんまげ」を見て、少年サ＠デーの「チョ＠ジ」思い出しました。アハハハ・・・。しかし、ふじっちょさまもたくさんＨＮあるよねー。（って、そんなワケないか・・・。）しかし、なぜ「ツッコミシスターズ」三女？？？

７４９　　Ｂｕｌｅ－Ｓｉｌｅｇｈｔｙさま　　お久しぶりです。そうですよねー。アニメＤＶＤって高すぎ！ＣＨがＤＶＤになったくれたら、すごく嬉しいけど、購入する予算をどこから計上すればいいのか・・・。主婦としては悩みそうですねー。

７４２　ｙｏｓｉｋｏさま　　はーーい！ほんと全開カキコのｙｏｓｉｋｏさま。またまた今回も笑わせていただきましたよん☆

７４１　きよさま　　えーーー！？何それ！？元カレが探偵で、きよさまにアシを頼んだ？それできよさま看護師さんのたまごでしょ？？キャー！！・・・で、アシって何するの？

７４０　　葵さま　　ミニの映画ってカワイイねー！「油断してろよデカイヤツ」って笑えるーーー！

７３９　銀駒さま　　いや、競馬好きなワタシは、ターフを駆け抜ける白馬のようね・・・。とか思ってしまいました。

７３４　　みろりさま　　私３５７マグナムのモデルガン、持ってますよん☆ダンナがくれたの（って何でそんなモノを！！）まだ箱入りのままなので、今度飾るケースとか買わなきゃなぁー。と思ってます。

７２５　ぽん☆さま　　不愉快な目に会いましたねー。最近はイロイロ物騒だから、巻き込まれないように注意！ですよねー。なんてそんな事思うのも、いやな世の中だよなーと思いますけど・・。

さ、お掃除等終わらせて、本屋さんへＧＯ！

### [10751] 無言のパンダ	2003.06.09 MON 01:02:05　
	
ごめんなさい！また間違えた！
「発売日が日曜に当たったら」のところ、「土曜」の間違いです。

### [10750] 無言のパンダ	2003.06.09 MON 00:58:46　
	
何度も現れてスミマセン！
「７４６」で訂正したのはいいけど、やっぱり言い回しがおかしかったですねぇ～。（気になると夜も眠れない！）
「いくらオ○ソになったとはいえ」もしくは「オ○ソになったばかりなのに」ですね。（どっちでもいいか！あははっ・・☆）
ごめんねー！葵さま！何度も「オ○ソ」を連発しちゃって・・・！え？悪気？ないよ～当然っ！信じて～☆(>_<)

＞ふじっちょんまげ様（７４７・７４８）
「パンダ大佐」って・・・「ファンフ＠ン大佐」じゃないんだから（知らないだろう～？）もう少し、女らしくて可愛い「パンダらしい」のにしてよ！ってゆーか、そろそろネタ切れだろー？ヽ(￣ー￣ ;)ノそれから・・・(-。-)y-゜゜゜ってなに？！アンタたしか未成年じゃなかったっけ～？コラッ！補導するぞーっ！
強制(▼▼メ)(￣▽￣||)(▼▼メ)連行中・・はい、逆戻り♪

※それから！平日ならコミックスが一日早く手に入るって～？そんなのアリ？！こっちなんか一日送れだぞ！その上、日曜が発売日に当たった日にゃあ二日遅れだよっ！なんで～？おんなじ西日本じゃないか～！(T_T)
だから明日ゲットされるみなさんも、あんまり羨ましがらせないでね☆(*'へ'*)

### [10749] Blue-Sileghty	2003.06.09 MON 00:27:27　
	
昨日、家の近所の家電屋が‘改装セール’をやっていたのでＤＶＤソフトを見に行ってきたら、息子が欲しがっていたソフトが売っていたんです～
で、価格を見たら‘１００、０００円’～　ｴｯ? (;ﾟ⊿ﾟ)ﾉ ﾏｼﾞ?
で、息子が言うには‘これで全話入ってるわけじゃない’と～・・・
なんでアニメって高いんでしょうね～・・・

### [10748] ふじっちょ	2003.06.08 SUN 23:26:57　
	
時間かけて書いてたら新し書き込みが…特に突っ込んでおかなくてはいけない人が一人…

＞[744]＆[746]無言のパンダ提督
ｙｏｓｉｋｏさまへのツッコミに夢中になるあまり自分もカキコミスするなんて(^。^)yププププ
弘法も筆の謝りなんて言い訳は聞きませんよ(-。-)y-゜゜゜

＞[745] chikkaさま
なるへそー。わざわざありがとうございますm(__)m
北條刑事に司刑事が捕まる…なんとも皮肉？（狙い？）な展開ですね～今日の「555」の放送ビデオに録った（朝弱いもんで…８時なんて殺人的な時間に起きれない）ので早速実写版「冴子」の活躍見てみたいと思います(^o^)丿

＞[740] 葵さま
パンダ提督のカキコ見て知りました。
お誕生日おめでとうございます！！
★:゜＊☆※＞（^▽^＊）♪Congratulations♪（＊^▽^）＜※★：゜＊☆
これからも楽しくやりましょう(^▽^)丿

お別れの曲にふさわしくＰＣから「ＧＥＴ　ＷＩＬＤ」が流れています。今日はこの辺でサヨナラ(^_^)/~

### [10747] ふじっちょ	2003.06.08 SUN 23:03:55　
	
おばんどす。
ＰＣ使ってる時はいつも音楽かけているんですが今日は久々に取り込んだＣＨの全サントラをかけています。（昨日までは＠’zばっかかけてた…だってファンだから…）やっぱいい曲が多いですね。今は「GIVE ME YOUR LOVE TONIGHT」が流れています(^^♪

＞[734] みろりさま
アドバイスありがとうございますm(__)m
これからその手段でやって見たいと思います。
「美術教師」説は思いつかなかった脱帽です。俺の中学校の時の美術の先生はシャクレのアディ＠スおたくでした(笑)（しかも、三年の時の担任…いい人やったけどね）

＞[736]＆[743]無言のパンダ大佐
あなた様の美しさに引かれて…ってそんなわけ無い無い
＼＾／(-_-；)
階級（？）は何となく偉そうなのを入れてるだけですから出世してるわけでも降格してるわけでもないですよ(^^♪似合ってそうなのをチョイスしてます(^_^)
えっ！？パンダ大佐が三女だったの？僕はてっきり…

＞[739] 銀駒さま
確かにやさしい方ばっかですよ表向きは…（意味深）
な～んて初心者の僕が毎日これるのもみんないい人ばっかだからなんですけどね(o^-')b　銀駒さまも毎日来ましょう
(^▽^)v

＞[740] 葵さま
「ミニミニ大作戦！」ですか。なかなか趣味的な題名な映画やなー(^o^)りょうがカメオ出演…なんて事は無いだろうなーアニメちゃうし。最近映画見に行ってないからな是非見に行きたいと思います。

＞[741] きよさま
なんともＣＨチックな展開ですね。
「私もこの街を出ていく気は無いの！！」
「この街でやらなきゃ…ならないことが…できたから！！」
「…………………！？」
「あんたには新しい相棒が必要でしょ！」
みたいなやり取りはあったんでしょうか？
＼(゜o゜（☆○＝(▽皿▽#）ｏアルワケナイダローガ！！

＞[742] yosikoさま
＞圭之助サマ、将人サマ、無言の・Ｕ・パンダ姫
↑僕もＯＶＡに付いて触れたのに…(T_T)

明日は、待ちに待ったＡＨ７巻の日！今日が平日だったらもう既にてにいれてんだけど（なぜかココら辺の本屋一日早く手に入る）皆さんの所はどうですか？

### [10746] 無言のパンダ	2003.06.08 SUN 22:47:31　
	
すみません！
↓「いくらオ○ソになったばかりなのに」×
　「いくらオ○ソになったばかりとはいえ」○

### [10745] chikka(Eastern Shizuoka)	2003.06.08 SUN 22:45:11　
	
【７３２】ふじっちょ様。
今日の「仮面ライダー５５５」見ましたか？ついに「冴子さん」が変身しました。「エビ」（「ロブスターオルフェノク」←名前は美味しそうですが）に・・・。しかも、胸がある・・・。（どこ見てんねん！！このもっこりスケベ～～～～～！！「１００ｔハンマー」ドゴン!!）しかし、色っぽいです。「５５５版」野上冴子こと影山冴子さん（設定は女性バーテンダー）を演じている和香さん。今回は仲間（琢磨逸郎役を演じている山崎潤さん。「ムカデ」をモチーフにした「センチピードオルフェノク」に変身）を色気で手玉に取るシーンがありましたが、微笑みながら顔のそばに近づいて耳元で囁く。もうイチコロでした。こちらの冴子さんも男を操る術を身につけている。おそるべし・・・・。あのシーンは色気を使って冴子さんが冴羽さんを手玉にとっているのを思い出しました。ちなみに手玉に取られた山崎潤さんは２年前に放送された「仮面ライダーアギト」で「北条刑事」役をやった俳優さんです。今回は敵キャラで出ています。それから「アギト」ではその北条刑事の先輩刑事として「司刑事」が出ていました。その時のストーリーは罪を犯した司刑事を北条刑事が逮捕するという物悲しいお話でした。

今天的“假面骑士555”看了吗?“冴子”终于变身了。“虾”(龙虾奥菲诺克←名字听起来很好吃)……。而且，还有胸…。(哪里看天然! !这个滑稽的好色之徒～～～～～! !“100t锤子”轰隆!!但是，很妩媚。和香饰演“555版”野上冴子即影山冴子(设定为女性调酒师)。这次是饰演琢磨逸郎的山崎润。变成以“蜈蚣”为主题的“多愁善感奥菲诺克”)性感地拿起手的场景，微笑着靠近脸旁边在耳边低语。已经差不多了。这边的冴子也掌握了操纵男人的技巧。可怕的…。那一幕让我想起了冴子用性感的手法把冴羽先生玩弄于股掌之间。顺便一提，被捉住的山崎润是在2年前播出的《假面骑士》中饰演“北条刑警”的演员。这次是以敌人的角色出现的。在《agito》中，北条刑警的前辈“司刑警”出场了。当时的故事是北条刑警逮捕犯罪的司刑警的悲伤故事。

### [10744] 無言のパンダ	2003.06.08 SUN 22:44:46　
	
ごめーん！もうひとつツッコミ忘れ！
＞ｙｏｓｉｋｏさま（７４２）
＞突っ込みシスターズの皆々様（←人数造花のため）
こ、これは・・・私たちがあまりに美しいため？（*￣▽￣*）ゞでも「造花」って・・・まだ生きてますわよっ！(#▽皿▽）＝３
特に葵さまは、いくらオ○ソになったばっかりなのにイキナリ「造花」だなんて！ホント、花の命は短いのね(σ_-)
葵さま☆こんな試練にも負けず、これからもたくましく強く生き抜いてね♪（今まで通り）
ところでｙｏｓｉｋｏさま☆今日は「見直し作業」をサボったの？（笑）
※そうそうＡＶって言い方、まぎらわしいわね～~(▼▼#)~~　

### [10743] 無言のパンダ	2003.06.08 SUN 22:18:05　
	
こんばんわ★
つっこみシスターズ三女（汗）突撃ーーーっ！

＞葵さま（７４０）
まずは・・・☆お誕生日おめでとうございます～！
★:゜＊☆※＞（’－’＊）♪オメデトウ♪（＊’－’）＜※★：゜＊☆
ところで、圭之助さまのゲットされたビデオはたしか「百万ドルの陰謀」だと・・・（*￣▽￣*）それから「愛と宿命のマグナム」を「愛と・・・」と略すところを見ると、まだアノ後遺症が残っているのね？(･･;)

＞ｙｏｓｉｋｏさま（７４２）
そうねぇ～「宴会」が楽しくなるのも、つまんなくなるのも「幹事」さん次第ですもんね～？（笑）「妖しげな幹事」さんだけは避けたいもんだっ！

※ミニの映画★見たいなー♪

### [10742] yosiko	2003.06.08 SUN 22:05:04　
	
こんばんわ☆たかだか数日振りなハズなのに、ワタクシの全開のカキコはイズコヘ・・っていう感じなくらい、早いどすな、グリグリ☆ヨカヨカ♪

【感想】（←遅っ）
皆様とほぼ同じですが・・（←毎度）、夏目氏、かなりニオイますね～～う～ん、ニオイニオイ。なにやら、かなり怪しげな幹事を受けるのですが・・・。コンゴの展開がまた楽しみ♪

＞突っ込みシスターズの皆々様（←人数造花のため）
全開のカキコでは”見直し”とよばれる作業を成し遂げたため、間違えなかったのよ♪たまにはね♪

＞葵サマ
先日、東京へ行って「ミニミニ大作戦」の広告を見た気がしますが、あれがＭＩＮＩの映画だったとは(*o*)!!ほんとに、リョウちゃんが言ってるみたい♪田舎物の私は、原宿で下車して、ああ、ここでリョウちゃんが活躍してるんだわ・・・と陶酔してしまった・・・新宿だっちゅうのさ。

＞圭之助サマ、将人サマ、無言の・Ｕ・パンダ姫
ＯＶＡって、そういういみだったんですね(*o*) おとつい、ＡＶがアニメビデオという意味ももつということを知って驚いてたんですが・・いや、真面目なはなし。

＞お初の皆様
始めまして♪yosikoと申します♪一部で年齢詐称疑惑アリ♪よろしくお願いいたします。

### [10741] きよ	2003.06.08 SUN 21:49:09　
	
今日、元彼に会ってきたのですが、探偵さんするみたいで、ちょっとしたアシを頼まれちゃいました。ＣＨぽくてちょっと嬉しいきよですｖ
早く明日にならないかなぁ・・・！！早くＡＨ⑦読みたい！！！テストだから昼までで終わるので、帰ったらソッコウげっちゅしにいきましゅ！！！！
