https://web.archive.org/web/20011006053643/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=42

### [840] カールスバーグ 2001.07.14 SAT 01:59:53　
はじめて書き込みします。  
つい２、３日ほど前にエンジェルハートの存在を知り、雑誌で拝見しました。  
皆さんと同じでかなり動揺したんですが・・  
つい先ほど北条先生のコメントを見て、絶対最後まで読もうと思いました。  
応援しています。頑張ってください。

  

### [839] りなんちょ  2001.07.14 SAT 01:58:58  
気になって眠れない！！！  
  
☆紫音♪さま☆  
「（監）」って、何～！？  
冴羽さんと香さんの、変なしりとりって何～！？  
あたしは、普通の２枚組しか持ってません！！！  
限定品？  
香「ルンルンギャルズにインタビュー」  
リョウ「有名人にサインをもらう」・・・  
そんなの、あたしのにはない！はず…

  

### [838] ネット 2001.07.14 SAT 01:55:46  
＞Goo Goo Babyさん  
情報ありがとうございます！早速調べてみたら、  
「この胸のときめき」（そのまんまやんか！/笑）という映画でした。  
原題は「Ｒｅｔｕｒｎ　ｔｏ　ｍｅ」。２０００年度公開作品でした。  
内容は結構よかったらしく、観た方の感想も結構好評でしたね。  
あ～、すっきりした！  
  
＞香の髪型  
個人的にはやっぱり切った後が大好きですｖｖ  
コミックス３３巻でカットして、３４，３５巻ともうホントに  
女性っぽくなって・・・子供心に憧れていました。  
現在ＡＨで出てくる香はまたちょっと違う髪形になってて素敵ですね。  
年相応の落ち着きと包容力を備えた彼女を見ているとなんだか嬉しいです。

＞Goo Goo Babyさん   
谢谢您的信息!我马上查了一下，发现那是一部叫「我的心怦怦直跳!」(这不正是它的特点吗？/笑)的电影。
原名为「Return to me」。是2000年公开的作品。
内容好像很好，观众的感想也很好。
啊，真让人耳目一新!


### [837] orphanwolf（奈良市・隠れ家在住）2001.07.14 SAT 01:52:34  
今晩は、孤児狼（何故か日本語読み）です。  
もう暑くて溶けそうです。  
北条先生のメッセ－ジを読んでスゴク気迫が伝わって来ました！！ＡＨ、ＣＨを超える作品になると確信しました！！  
  
＞sweeper様  
91で思い出したんですが、「ＤＯＷＮ　ＴＯＷＮ　ＧＡＭＥ」もユ－ロにすると素敵ですね！！（何気にこの歌の話は出ないですね《汗》）  
  
＞りなんちょ様  
91のサントラの表紙・・・∑（￣□￣；）ああ、何気に香ちゃん裸っぽい（照笑）・・・リョウってばモッコリスケベなんだから（笑）  
  
＞あずみちゃん様  
ＣＨ91のＯＰの絵、私は北条先生の絵に一番近かったのでスゴク嬉しかったです。（でもＣＨって神村幸子さんの絵が一番印象的でしたものね）自分が絵描きで先生の絵を尊敬しているので、どうも絵に五月蝿くて・・（汗）  
  
＞露崎なごみ様  
リョウのタバコの噛み癖が発覚してからふと、我に帰ったのですが、私も噛み癖があるんですよ（汗）ちょっと身体に悪そうですね（汗）でも、リョウと同じだからいっか（笑）  
  
＞ゆなつ様  
91って飛行機の話が一話目でしたよ（＾－＾）「伊集院隼人氏の極めて平穏な一日」や、ソニア、都会のシンデレラの話等、結構粒ぞろいだったと思います。惜しむらくは放送回数が最初から13話と決まっていた点ですか（泣）

  

### [836] ＴＡＫ(東京市部・２１歳)  2001.07.14 SAT 01:51:25  
長い・・・。一日半見ないだけで、膨大なログだった。これは何とかしないと。マメに見るしかないのかな？一時間半は読みに専念してたよ。  
さて問題のドイツ語のテストも自主持込（カン○ング）のおかげでなんとか乗り切り前期の山は越えたかな。  
で、そんなことしてるうちにここも色々あったな。  
  
＞Ｇｏ　Ｇｏ　Ｂａｂｙさん  
なんだかんだ言って結構来てますね。毎回楽しい話題をありがとう。ストラップ一揆は僕もかなりやってたんだけど・・・。もう下火？みんなもう一度立ち上がれ！  
  
＞ｔａｎｏさん  
オフ会で全員集合？それは是非やってみたいっす。  
  
＞あずみさん  
Ｂ'z　ＰＡＲＴＹ 僕もはいってます。ＨＮでバレてんのかな？  
  
＞りょうの子供  
僕的には女の子。天使の贈り物の杏奈ちゃんみたいのがいいなぁ。で、りょうは自分の子となると、すっげー子煩悩。小さい頃から自分の事は棚にあげて、男は野獣だから気をつけろとか言ってんの。  
＞時計  
ストラップの熱がこちらに逃げていってしまった気がするんで、ちょっと悲しいが、どの意見も流石ファンの声。すんばらしいっす。  
  
＞名前の後ろのカッコ  
確かに地名が消えつつあるかも。でも他の事書いてあったりして個人的にはそれも好きなんだけどなぁ。毎回違ったりして創意工夫があっていいかなぁと。  
  
さて寝るか。明日は休みなんでコマ目にきます。

  

### [835] りなんちょ（酔いさめた…）2001.07.14 SAT 01:49:23  

そろそろ、寝ようかな？  
って思うんだけど、ココに来てしまう…  
  
☆しゃる。さま☆  
ストレスたまっているんですね？  
ココで、思いっきり発散しましょう！！！  
あたしは、現実逃避もしています…  
でもそーゆー方、他にもいらっしゃいますよ？  
ログを見るのは、本当に大変です！！  
できる限りでよろしいですから、  
北条先生の作品の話をしましょうね？  
  
さぁ～て、そろそろ寝ようかな？  
…って、おい！  
娘（６ヶ月）が、バンチをかじったようです…  
娘は最近、紙がお気に入りなんです…  
今まで、気がつかなかった…  
いつ、やったんだろう？  
創刊３号が、ボロボロに…  
エンジェルハートが表紙なのに～！！！  
え～んっ！！ （号泣）

  

### [834] 紫音♪（一息中...）2001.07.14 SAT 01:49:21  
昨日岐阜はえらい天気で雷落ちて、もの凄い雨と風で家飛んでっちゃうかと思っちゃったよ～。まったくぅ。６階に住んでます。  
  
ありっ？！私が変なのか、確かに「ドラマティックマスター」の後には「Ⅱ」とある...誰か持ってる方いませんか～？？と思いきや、＊ゆなつさま＊ありがとです。持ってらっしゃいましたかっ!!  
＊りなんちょさま＊成生さま＊キャサリンさま＊  
「ドラマティックマスターⅡ」は２枚組みですよ。パイソンと香の写真が置いてあるジャケットです。1990年1.21って書いてあります。確かにその時は中学生でした...（＾＾；リョウと香のしりとりサイコーなんですよ。何回聴いても笑える。香「ルンルンギャルズにインタビュー」リョウ「有名人にサインをもらう」・・・変なしりとりやってます（＾＾；まだ持ってないＣＤもあるのでよく中古ＣＤ屋さんとか回ってゲットしてます♪  
  
＊姫さま＊  
私も「絶対やった！」に一票！（笑）しかしみなさんもカゲキだわっ☆おもしろすぎっ☆  
でも、香のおなかに赤ちゃんがいたのかな～って思ったりしてると、リョウの心の傷はもっと奥が深いのかも知れないですよね。。。「Ⅱ」頑張って探してくださいまし～☆  
  
＊あずみちゃん＊  
こちらも全然オッケーで～す♪さりげな～く待ってますよっ。  
私も岐阜に住んでますぅ。おいしいお寿司屋さん？？３時間もかけていらしてるなんてビックリしちゃいました☆  
ＴＭいいですよね～ＦＣ私も入ってました～♪  
  
＊おっさんさま＊  
身内ネタありがとです～（笑）私の庭です。（爆）  
そして友達ジャ○コにいました～。  
なんだかそのうち会えそうな勢いですよね☆  

  

### [833] ツバサ(もっこりLEVEL255）2001.07.14 SAT 01:42:18  
いや～～～～、終電が無い（爆死）  
まさに生命の危機でしたな。もう少しでドクの二の舞になるところだったぜ！  
  
やっと北条先生のメッセージが書かれましたなあ。  
ホント苦労人です。この苦労に報いるためにもバンチを買いつづけなければなりませんなあ。お金大丈夫？だめです！今日のタクシー代でかなりピンチでしょうなあ。  
だーがしかーし！この猫型貯金箱がある限り僕は負けませんぞ。  
廃人になるまで買いつづけて溜め込んで見せます（断言）

  

### [832] tano(久しぶり？？)  2001.07.14 SAT 01:33:22  
１日ご無沙汰していたら、ログがどっさりっ！  
皆様の気合いを感じます～♪  
  
＞まみころ様  
お姉さん、いましたよね～。名前はさゆりさんでしたっけ？  
香とお揃いの指輪を持ってましたね。  
・・色合いからしてルビーとサファイアかしら？  
ルビーとサファイアは、同じ性質（元素？）の石で  
赤い物がルビー、それ以外の石はサファイアなのだとか。  
宝石同志が姉妹のようでしょ？だからイメージピッタリ☆  
コミックス封印中ですか？我が家もタンスの奥の方に・・。  
手が届く所に入っているんですけどね（笑）。  
  
＞あずみちゃん（って呼び捨てok？）  
３６歳の香☆見てみたいようなショックなような？？  
自分のトシを考えるとフクザツですわぁ～。  
高校卒業してから、倍も生きてるんですもの・・・。  
  
＞リョウと香の仲について  
ＣＨの最終話あたりから一緒の部屋に住んでいるような気が  
していたのは、私だけでしょうか。。。考えすぎかな？  
  
＞こども・・・  
その存在は謎ですが、妊娠すると母体の体温が微妙に変わるんです。２人目以降だと本人より先に上の子が察知したりします。  
（それで何となく駄々っ子になったりする(^^;)）  
もし、香がそうだったら・・・リョウは気付いていたかも。。  
  
ああ、憶測で物言う私・・・忘れてください。。あはっ。

  

### [831] 絵里香  2001.07.14 SAT 01:32:41  
先生のメッセージへレス♪♪  
  
先生のカキコを拝見したので、足跡残しに参上<(\_ \_)>  
食事も睡眠も空気さえも吸わないで「ひとつのこと」だけ  
考える事を許されるのなら、わたしも「悪魔に魂売ります」  
  
わたしの今の「ひとつのこと」とは、勿論ＡＨの事。  
（と、ゆ～より、冴羽さんの事（笑））  
先生が妥協なんてしないのは、よ～く知ってますので、  
突っ走って下さいマセm(\_\_)m  
  
ＡＨに人生変えられたので、私も後悔しないように、全力で  
見守りたいと思います(\*^^\*)  
  
足跡カキコでした。（お目汚し失礼）  
あとは、ファンレターでも書きます～～。

  

### [830] しゃる。（秋田県。）2001.07.14 SAT 01:23:59  
〓まみころSAN〓  
いました､お姉さん。名前が｢立木さゆり」さん。文庫本だと１０巻です。  
あの時おしゃれした香さんって､今のＡ・Ｈの香さんそのものって感じがします。キレイすぎです～♪もう!大好き香さん！！  
  
〓りなんちょSAN〓  
私もストレス解消でここに来てます。  
みんなの書き込み読んでるのすごく楽しい♪  
でも、ログ読んでるうちに疲れきって､自分が書き込みできなかったりします。今日は書き込むことできたけど。  
今度はいつ書き込みできるんだか･･･  
  
でも、やっぱりログ読むの疲れる。１時間くらいかかっちゃう。  
もっと早く読めるようになりたいな。  
それじゃみなさん、おやすみなさ～い！  
しゃる。でした。

  

### [829] chikka(Eastern Shizuoka)  2001.07.14 SAT 01:23:26  
香さんのお姉さんの名前は「立木さゆり」です。「立木」というのは「母方」の名字です。元々は「久石」という名字でしたが、離婚してさゆりさんは母親に、香さんは無理矢理父親に引き取られましたが、この父親というのがろくでもない男らしくて、銀行強盗起こした挙げ句に警官に射殺されました。その警官というのが槙村さんのお父さんで、当時赤ん坊だった香さんを連れてきてそのまま槙村さんの妹として育てたそうです。その父親もそれから５年後に亡くなって兄一人妹一人一つ屋根の下で生活する事になりました。ちなみにこの頃、槙村さんは中学２年生、香さんは５歳でした。（槙村さんが死んだのは２９歳、この時香さんは２０歳でした。）  
さゆりさんの職業は雑誌「ウイクリーニュース」日本版の編集長で、そこでの活躍が認められてニューヨークに栄転する事になりました。今にして思うと、ニューヨークに旅立つ前に「実の妹」を見たかったのかもしれません。でも、結局それが「永久の別れ」になってしまうとは・・・。すごい悲しいです。（ちなみに、スイーパーを辞めざるを得なくなったミックはこの「ウイクリーニュース」の日本支部記者として第二の人生を歩む事になりました。）

香小姐的姐姐叫“立木小百合”。“立木”是“母亲”的姓。她原本姓“久石”，离婚后小百合由母亲收养，香小姐被强行由父亲收养，这个父亲是个不体面的男人，在抢劫银行时被警察击毙。那个警察就是阿真村的父亲，他把当时还是婴儿的阿香带回来，当作阿真村的妹妹抚养。那个父亲也在那之后的5年去世了，和哥哥妹妹一起生活在一个屋檐下。顺便说一下，当时槙村是初中2年级学生，香小姐才5岁。(阿真村死的时候是29岁，当时香才20岁。)  
小百合的职业是杂志《周刊新闻》日本版的主编，因为在那里的活跃表现，她被荣升到纽约。现在回想起来，也许是想在去纽约之前看看“亲妹妹”吧。但是，结果却成了“永久的分别”……我很悲伤。(顺便一提，不得不辞掉Sweeper工作的Mick成为了“Weekly News”日本分部的记者，开始了自己的第二人生。)


### [828] りなんちょ  2001.07.14 SAT 01:08:54  
間違えた！！！  
「立木 さゆり」だ！！！  
「久石」は、父方の姓でした！！！  
ごめんなさ～い  
酔っぱらっちゃって…

  

### [827] りなんちょ（まだ、寝ないぞ！）2001.07.14 SAT 01:05:32  
酒が良いカンジにまわってきた…  
うっぷ…  
  
☆Kasumiさま☆　☆ありありさま☆  
アニメでの、冴羽さんと香さんのキスシーンて、  
「セイラ」の、話ですよね？  
ココがリニューアルされる前に、かなり盛り上がりました。  
北条先生が、裏話を教えてくれたんですが、  
あの話で、本当は最終回だったそうです。  
だから、あんなに盛り上がった話になってるみたいですよ？  
あたしも、その話好きです。  
２本ほど、ビデオにとってあります。  
  
☆まみころさま☆  
たしかに、香さんにはお姉さんがいます。  
名前は、「久石 さゆり」  
香さんと血がつながってる、本当のお姉さんです。  
詳しくは、単行本版２０巻をお読みください。  
あたし、文章表現が乏しいので、  
文にすると、訳わかんなくなるかも…  
だから、原作読んでくださ～い。  
ごめんなさい…


### [826] ＳＡＹＡＫＡ 2001.07.14 SAT 01:01:03  
一日来ていなかった間にまた大量のログが・・・。相変わらず読むのに一苦労、時間がどんどん過ぎていく。  
  
香ちゃんの髪形ですが、私は長いほうが好きなんですよねー。子供ながら、美容師さんにどう説明してカットしてもらっているのかと真剣に考えていました。最近髪をショートにしたのですが、髪質が完全なストレートのため香ちゃんみたいにならない（泣）。香ちゃんの髪型とっても好きなのに。  
  
話はぜんぜん変わりますが、実は私明日から実家に帰るのですが実家はインターネットにつないでいないのでここに来れない！ヤバイ・・・、きっと帰ってきたら半端じゃない量のログがたまっていそう。でも、バンチはしっかり買いますよ。ただ、しばらくここに来れないかと思うとかなり悲しい（涙）。ここで皆さんの書き込み読むのが私の毎日の楽しみだったのに（泣）。

  

### [825] ゆなつ  2001.07.14 SAT 01:00:16  
どうやら一日二回は登場しないと気がすまないらしい（笑）  
  
＞月見様  
　レスありがとうです！「銀のロマンティックわはは」あれは名作です！とくに犬！って話をここでするべきじゃないかもしれませんね。今度メールででも、語りましょう(笑）  
  
＞りなんちょ様  
　早速レスありがとうございます！バンチ版今みたら足りないのは２・３冊ではなかった（涙）９巻までしか持ってなかった！  
私が働いている本屋は本屋として出版社から認めてもらってないらしく、いつも入荷が少ないし、勝手に入荷が切られたりする。だからバンチ版こなくなってショックだった。  
ＡＨがコミックになったとき、入ってくる数少なそう、、、絶対予約してやる！（自分の店なのに客より自分の分を優先する奴！）こればっかりは譲れないね！  
ネットでの購入って方法があるんですね。ちょっと調べてみます！情報ありがとうございます！！  
  
＞まみころ様  
　香のお姉さんって立木さゆりさんでしょう！香を一緒にニューヨークに連れて行こうとするんだけど、リョウには今の香の暮らしを見てから決めてくれって言われて、、、で最終的に彼女が出した結論が「今のままが香さんにとって幸せだと思ったから・・・（中略）わたしの知ってる真実より彼女が知ってる真実のほうがより真実だわ。」ってね。  
　最後にさおりさんがリョウに「責任とってくれるんでしょうね？！香さんのこと！！」って言って、リョウは笑ってごまかすんだよね。このときすでに、責任取るつもりだったんだろうな、、、  
  
だからあの二人は男と女の関係だったに違いない！さらりと初キスの話をするあたり。初ってことは次があったってことじゃないですか！！！いつ、どこでしたんだろう？二回目・・・（妄想中)(笑）「シンデレラ～」の初（？）デートの話をしながらリョウの部屋で・・・（うひゃ！）  
  
とろこで皆さんＣＥ．ＣＨ．ＡＨって訳しますよね。  
ＣＥは「シーイー」または「キャッツ」  
ＣＨは「シティーハンター」って心の中で読むんですけど、どうしても  
ＡＨは「ア～」って読んでしまうんですよ（笑）  
まだ作品に慣れてないからでしょうか？今後キャッツみたいに、エンジェルって訳すのだろうか、、、皆さんはどう思います？

＞まみころ様  
香的姐姐是立木小百合，对吗？ 她想带香一起去纽约，但獠告诉她要等看到香现在的生活状况再做决定，她最后的结论是 "我以为香会像现在这样幸福...（省略）她知道的真相比我知道的真相更真实"。 
　在最后，小百合对獠说："关于香，你会承担责任的，对吗？ ！”獠笑了笑，掩饰了一下。 那个时候，他已经打算承担责任了吧……


### [824] Goo Goo Baby （ＡＨに似た映画）2001.07.14 SAT 00:59:42   
＞ネットさ～ん！  
  
つい最近もそうゆう映画あったような・・・・  
  
確か  
デヴィッド・ドゥカブニー（Ｘ-ファイルのモルダー役の人）と  
ミニー・ドライヴァーが出てる映画だったと思うんですが、  
タイトル忘れました～～！！！（←ダメじゃんっ）  
  
確か  
妻の臓器を移植された女性と恋に落ちる物語だったと思います  
多分！！！！  
  
　勘違いしてたらごめんなさい  
あたし自信ないんで、俳優名で検索してみては？  

＞ネットさ～ん！  
最近好像也有这样的电影…
确实,
我想那是一部David Duchovny(x档案中扮演Mulder的人)和Minnie Driver的电影。
标题忘了~ ~ ! ! !(←不行!)
确实,
大概他爱上了一个移植了他妻子器官的女人！！！！
如果误会了，对不起，
我没有自信，试着搜索演员的名字怎么样?


### [823] kasumi 2001.07.14 SAT 00:35:04  
相変わらずログ多いですね～！！今日も１時間かかってしまいました。  
北条先生のメッセージ感動です！熱い思いが伝わってきます！先生が楽しんで書いておられるようなので本当にうれしいです！でもお体の健康には気をつけてくださいね！！  
  
＞リョウと香の関係  
きっと大人の関係ですよ～って思いますけどね（テレ：笑）っていうかそうあって欲しいっていうのが本心ですねー。  
私もリョウと香は一緒に寝てたんじゃーないかってなんとなーく思っているんですけどね！普通の夫婦みたいに。リョウに腕枕してほしーーーい！！って考えてしまいますよ・・・（キャ！恥ずかし）  
  
＞ありありさま  
たしかにアニメでリョウと香のキスシーンありますよー。香が催眠術で操られてて、それを助けに行ったとき、香がリョウに向かって銃をはなったんです。でもリョウはどんどん香に近づいていったんですね「おまえに殺されるんなら本望だ」（ちょっと違うかもしんないけど）ってなこと言いながら。そしたら香は催眠術に操られてるのに涙流し初めて。。。で、イヤリングのスピーカーから流れてくる操っている人の声を止めようとして、リョウがイヤリングを打ち抜いたんです。そしたら香が倒れて、リョウがそれをうけとめて・・・でも香は完全には意識が香に戻ってなくて・・・涙を流している香にリョウが「いつもお前を泣かせる俺が、今は・・・」って言ってキスするんですよーーー！！とーーっても感動！いいシーンですよ。（違ってるとこあったらゴメンナサイ。でもこんな感じです）ちなみに元に戻った香は「確かあの時・・・」って言って唇を触ってました。  
何か長くなってしまってごめんなさーい！なかなか文章力がないもので・・・

  

### [822] とっとこ茶々  2001.07.14 SAT 00:30:22  
みなさん、ありがとうございます。明日子供に伝えます。みなさん、もしかしたら若いのかな?私はTVシリーズの香と同い年です。名前も似ているので、香にはとても思い入れがあります。同性からみてもとてもかわいいヒトだと思います。みなさんそれぞれに好きなキャラがあると思いますが、これからもファンでいきましょう。おやすみなさい。

  

### [821] ネット  2001.07.14 SAT 00:27:43  
こんばんは。  
数年前に（５，６～１０年くらい前？）にアメリカの映画で  
最愛の妻を亡くした男性が、彼女の心臓が移植された女性と恋に落ちる、  
という映画があったと思います。  
（予告しか見ていないのですが。）  
確かその作品では、その女性に惹かれた後に、  
実は妻の心臓が移植されていた！という事実が判明したような・・・  
ああ、記憶があやふや。  
  
あまりメジャーな作品ではなかったと思うのですが、  
どなたか題名とかわかる方いらっしゃいませんか？  
いや、なんかふと思い出してしまって（笑）

晚上好。
几年前(5，6-10年前？)我想有一部电影，一个在美国电影中失去了心爱妻子的男人，爱上了她的心脏被移植的女人。
(我只看了预告。)。
的确在那部作品中，在被那个女人吸引之后，其实他妻子的心脏被移植了！这就是事实...。
是啊，记忆含糊其辞。

我觉得这不是什么主流作品，有谁知道题目的人吗？不，我突然想起来了(笑)