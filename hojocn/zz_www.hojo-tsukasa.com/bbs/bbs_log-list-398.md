https://web.archive.org/web/20050313021048/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=398

### [7960] chikka(Eastern Shizuoka)	2002.09.24 TUE 23:16:22　
	
今甲子園のジャイアンツ対タイガースの試合が終わりました。なんと、さよならワイルドピッチで２-３でタイガースが勝ちました。決まった瞬間「高笑い」をしてしまいました。「地獄から天国」に帰ってきた気分です。正に「最後の悪あがき」というところですね。本当に「神様、仏様、浜中様」です。浜中様ありがとう。あなたはタイガースだけでなく、スワローズ、フジテレビ、（この後「ナースのお仕事」の最終回）そしてテレビ朝日（明日「ジャイアンツ対タイガース」戦放映）にとって「救世主」と言ったところです。さて、我らがヤクルトスワローズもまだまだやるぞ～～～～。明日は勝つぞ～～～。ペタさん。ホームランたのんまっせ。

### [7959] TAKESHI	2002.09.24 TUE 21:59:59　
	
＞[7948]Ayu様
リョウたちが教師だったら、そりゃーもう学校が楽しくて楽しくてしょうがないでしょうね！！香や冴子や麗香はすごく美人だし、リョウはすごく男前だから生徒からモテモテでしょうね。リョウにはもちろん持ち前の野生パワー（笑）で体育の授業してほしいです。英語の先生はミックが似合うと思います。香や冴子や麗香たちの授業は、僕の場合あまりの美しさに見とれてしまい授業どころじゃないかも（笑）
個人的に海坊主には直伝のトラップを伝授してほしいです。

＞[7954]里奈様
よかったですね！！ところでレンタル料いくらぐらいですか？僕んとこのレンタルショップはレンタル料がとても高くて困ってます・・・。


### [7958] chikka(Eastern Shizuoka)	2002.09.24 TUE 21:45:07　
	
【７９５７】の書き込みをして、ＢＢＳへ送った直後に何とタイガースの浜中おさむ選手がセンターバックスクリーンに飛び込む同点アーチを放ち２－２になりました。しかも、アリアス選手もヒット、そして八木裕選手を敬遠のフォアボールで一塁へ送り２アウトランナー一塁二塁でサヨナラのチャンスをタイガースは迎えています。正に「ＭＡＫＥ ＭＩＲＡＣＬＥ ＴＩＧＥＲＳ ＶＥＲＳＩＯＮ」！！頑張れ、タイガース！！奇跡よ起これ！！と書いたら、三振に倒れ、延長戦になりました。こうなりゃ、応援するぞ！！最後の悪あがきだ！！

### [7957] chikka(Eastern Shizuoka)	2002.09.24 TUE 21:32:13　
	
ただいま２１時２６分です。いよいよ甲子園球場で行われるジャイアンツ対タイガースも残すところ９回の裏のタイガースの攻撃を残すのみになりました。２-１でジャイアンツがリードして、ジャイアンツは守護神河原純一（敬称略）を投入しました。一方、ナゴヤドームで行われたヤクルトスワローズ対中日ドラゴンズは４－９で我らがヤクルトスワローズは敗れてしまいました。これで、９９．９９９９９９・・・・・％ジャイアンツの優勝は決まりですね・・・・。さて、テレビでも見てよっと。

【７９４０】葵様
昨日の試合は「最後の悪あがき」というやつでしたので、別に笑ってはいませんでした。逆に今日の試合の方が笑ってしまいました。だって初回で４点取られたんだもん・・・・。思わずトシちゃん笑い（古）してしまいました。

### [7956] マイコ	2002.09.24 TUE 21:19:40　
	
＞「７９５４」里奈様　新しいレンタルショップがオープンするそうでよかったですね。でもその中にC・Hがあるかな？あるといいですね！（＾０＾）

### [7955] sweeper(お久でっす♪)	2002.09.24 TUE 21:16:40　
	
ううう･･･。もうすぐカキコが終わるところで取消を押してしまった･･･。(T_T)
気を取り直して、皆様こんばんは。それからお久しぶりです。

[レス]
＞おっさん様。
「31日はおまえの誕生日だったな･･･。プレゼントは何がいい？」
お久しぶりです！それから遅くなりましたがお誕生日おめでとうございます！ちなみにドラえもんの誕生日。私も存じてました。西暦からだと2112年9月3日だったと思います。

＞無言のパンダ様。
「私もできるだけ協力するから香さんを危険な目にはあわさないで！」
お久しぶりです！それからレスありがとうございます。「りょうちゃん大カンゲキー！」です。「ちびま＠こちゃん」は声優さんが他の番組で意外な役を演じている方が結構いますよね。お母さんと「クレ＠ンしんちゃん」のまさお君、たまちゃんのお父さんと「サ＠ボーグ009」のアルベルト･ハインリヒ、みぎわさんとみさえあたりでしょうか。(脱線失礼しました。)

＞OREO様。
こちらこそはじめまして。それからレスありがとうございます。「りょうちゃん大カンゲキー！」です。教えていただいたサイトさんへは早速行ってきました。CHのDVD化なってほしいですよね。

＞たま様。
こちらこそはじめまして。レスありがとうございます。「りょうちゃん大カンゲキー！」です。CHのDVD化は私も賛成です。うちの地元のレンタル店ではCHのビデオがあんまり置いてなく「さらばハードボイルドシティー」の回の巻はどこにも置いてないので、DVD化の時には是非この話を入れて欲しいなーと思う次第です。(1度も見たことがないので。)

＞「夜更かしのプリンス」いちの様。[7935]
「XYZ！おたくが伝言板にそう書いたんだろ？」

XYZ･BOX行ってきたのですか！依頼もしてきたなんて羨ましい！私はバイトがあったので、行けなかったです･･･。
それから過去ログを拝見したのですが、宇都宮の方にお住まいなのでしょうか？私は高校が宇都宮だったので電車で通ってました。違っていたらすいません。

[初カキコの皆様。]
初カキコの皆様はじめまして。sweeperといいます。以後、よろしくお願いします。

それではこれにて。

### [7954] 里奈	2002.09.24 TUE 20:40:35　
	
☆７９５１　ＴＡＫＥＳＨＩ様
今日もＣ･Ｈビデオを探しに行って来たのですが、どこも全部揃ってなくて悲しいです（+_+；）でも私の家からさほど遠くない所に近々新しいレンタルショップがオープンするという情報を入手！仕事ついでに京都で借りようと思ってましたが地元で借りれるかもぉ♪オープンは１０月だからもう少し待たなきゃ。

☆７９４８　Ａｙｕ様
再度カキコしちゃいます。
原作４巻を見なおしたら、リョウちゃんは『社会心理学』の教授してました。『社会心理学』ってのがリョウらしいですよね！

### [7953] マイコ	2002.09.24 TUE 20:33:38　
	
みなさんご存知でしょうか？
今日のニュースで見たのですが、新宿にタヌキがあらわれたんですってね！「しんちゃん」やら「カブキちゃん」ていう名前は、やはり「タマちゃん」「ウタちゃん」の影響ですかね～。タヌキいまどこにいるんだろ・・・。

★もうすっかり涼しくなりました。秋ですね～・・・。（きずくの遅いかな？）

### [7952] 葵	2002.09.24 TUE 20:21:32　
	
　私的に忙しいのでレスのみ…。（何でかって？いやぁ～わかるっしょ？！）

＞たまさま［７９４２］
　持ちつ持たれつ…素敵なコトバ。（笑）

＞千葉のマダムさま［７９４５］
　そうです。今晩は忙しいので早めに退散します。（笑）純真可憐なマダムも「勇者の会」に入会しませんか？(^_^;

＞ちょこさま
　すみません。Ｈｏｔｍａｉｌへのアクセスがナゼだか不可能になりましたので、スミマセンがこちらにレスいただけたら嬉しいです。(>_<;

### [7951] TAKESHI	2002.09.24 TUE 20:20:18　
	
先日、映画「サイン」見てきました。実におもしろかったです。ホラーというより家族愛をテーマにした感動ものだと思います。最後にすべてがつながりラストはなるほど！！！って感じでした。みなさん是非見てみて下さい！！！

今日ズームインで見たんですが、新宿駅東口にたぬきがあらわれたそうです。新宿ということで「しんちゃん」とか「カブキちゃん」という名前の候補がありました。タマちゃん、ウタちゃん、そしてこのたぬきといい、最近街に野生の動物が現れることが多いですね。

＞[7926]里奈様
それは残念でしたね。僕の町はど田舎なのになぜかCHの
ビデオ全部揃っています（いやみに聞こえたらごめんなさい！）。里奈さんもがんばって探してみてくださいね！

＞[7935]いちの様
普段も仲いいかな？

### [7950] 里奈	2002.09.24 TUE 20:09:37　
	
☆７９４８　Ａｙｕ様
はじめましてぇー♪
もしリョウ、香、冴子、麗香達が教師だったら…私絶対さぼらず行きますね！でも担当授業は…？冴子は男子校の英語教師か保健の先生が似合いそう。

そういえば、原作のほうでリョウが大学教師になったことありますよね。４巻で。メガネかけて授業してました。黒板は難しい図で埋め尽くされてて、いったいどんな授業したのかは不明なんですけど（笑）

### [7949] マイコ	2002.09.24 TUE 19:49:23　
	
＞「７９４８」　
Ayu様　はじめまして！マイコと申します。これからもよろしくお願いします！　
りょう達が女子校教師だったらですか？もっちろん毎日学校に行くのが、楽しみになりますよ～！（笑）

### [7948] Ａｙｕ	2002.09.24 TUE 19:04:48　
	
Ayuです。初めまして。初回からいきなり皆さんに質問なんですが、遼、香、冴子、麗香達が女子校教師だったら皆さんはどう思いますか？

### [7947] マイコ	2002.09.24 TUE 18:56:26　
	
＞「７９４６」里奈様　「ザ･シークレットサービス」借りれるといいですね！
★またなんか借りたら感想聞かせてくださいね！

### [7946] 里奈	2002.09.24 TUE 17:18:07　
	
☆７９４３　マイコ様
『愛と宿命のマグナム』のエンディングって他の映画のエンディングとはまた違うせつなさがありますよね。妖精の香がとてもキレイで、すり抜けようとする香をリョウが力強く引き止めるシーンが印象的でした！
さぁ～て…Ｃ･Ｈビデオをたくさん扱っているレンタル屋を探しに行ってこようかな。

### [7945] 千葉のOBA3	2002.09.24 TUE 09:48:27　
	
おはようございます！やーっと連休も終わって、ほっとしていますが、あいかわらず毎日いそがしくて余裕のない日々ですなー。

＞葵さま＞いちのさま　ありがとうございます。チビは１１歳になったんですが、昨日ヤツのサッカーの応援に行ったらクラスの女の子が２人、やはり応援に来てくれていました。しかし、その２人の大人びていること！今回のＡＨの茅野　夢ちゃんの大人っぽさが理解できました。うちのチビ（とサッカー部のメンバー）などまるでガキ。女の子は同じ年でも、中学生くらいに見えてしまうことを再認識しました。・・・でもって葵さま、巨人は今日あたり優勝ですか。・・・くやしいがセールには行きます。（ワハハ）

７９３４　ニャンスキさま　おひさーー！いそがしそうですね！ニャンスキさまバイオリン弾くんですか！？すごい！私、楽器ってまるでダメだなーーー。音楽は好きですけどねー。・・・このところ、カラオケすら行ってない・・・さびしいなー。

７９３５　いちのさま　ＸＹＺ．ＢＯＸ行ったんですかー！？そしてＸＹＺもしたの？えらい！！！しかし、そんなに勇気のいる所なんですか？えーーー、とにかく　行くだけ行ってみようと思うけど、純情可憐なワタシにはたしてＸＹＺができるでしょうか！？

７９４２　たまさま　しかし、ウタちゃんまで現れて、すっかりアザラシブーム。いったいどうしちゃったんでしょうね？もし日本の川すべてにアザラシがいたら・・・。そしてそれが、実はアザラシの皮を被った宇宙からの侵略者だったら・・・どーする！？

よくよく見てみたらこの次のバンチにＡＨが掲載されるか、どこにも書いてないですよね。これは、バン木探検隊出動せねば。





### [7944] もっこり	2002.09.24 TUE 04:15:48　
	
[7942]＞たま様
こちらこそよろしくお願いします。

[7938]＞マイコ
こちらこそよろしくです。ゲットワイルドのタイミングの良さは最高ですよね。

[7937]＞里奈
やはりDVDBOXでも発売してくれればいいのに～　と思ってしまう私です。

[7933]＞やすこ
ビデオには当時のCMまで入っていて時代を感じてしまいます。あぁ懐かしきバブルの時代よ～　っとね。

こんな時間の書き込みですいません。








### [7943] マイコ	2002.09.23 MON 23:55:07　
	
＞「７９３９」里奈様　「愛と宿命のマグナム」さっき見終えました！おもしろかったです。「なぜあなたは今まで香さんの気持ちに答えてあげなかったのですか」という言葉、本当にドキッとさせられました！　
♪エンディングを見ていたらとてもせつなくなっちゃいました！

### [7942] たま	2002.09.23 MON 23:09:31　
	
あらっ？今日、巨人優勝しなかったの？（せっかく勝ったのにね）
イチローは２００本安打（２年連続）ですね。

＞将人（元・魔神）様　【7919】
ＧＯＯＤです！ＶＥＲＹ　ＧＯＯＤ！最高っす！最ぃ高ぅーーっす！
想像できます♪キャッツでの会話が目に浮かびます♪
阿香も何かのキッカケがないと、海坊主って呼べないのかもしれないですね。
それにしても、「勝手な想像」だけにしとくのはもったいないです。
是非ＸＹＺ・ＢＯＸに投稿してくださいよぉ～。

＞千葉のOBA3様　【7920】
出来ることなら、投稿したいですよ。「カリスマ天使・たまちゃん」は・・・。
「ウタ」っていうそっくりさんをどうにかしてくれっー。
このままじゃ、タマの人気がどんどん取られていくわっ・・・ってね。（笑）

＞もっこり様　【7928】
はじめましてです。Ｃ・Ｈファン歴10年ですか？！
それは、お見事っですね！これからも宜しくお願いします。

＞OREO様　【7929】
リョウのアップ（２ページ目）！うん♪うん♪カッコイイーーーっ♪
切り抜いて持ち歩きたいくらいカッコイイーーっ♪

＞ニャンスキ様　【7934】
おっおっお久しぶりですーーーっ。
急にプッツリＢＢＳに来なくなったので「どうしたかなぁ」っと思ってたのですよ。
「ウタ」？！フンっ！そんな奴ぁ知らねぇーな！
日本の川に居たけりゃ、たまさんに上納金納めろっつぅ～の！

＞いちの様（漢）　【7935】
さすが！漢ですね。皆（葵さまを除く）が躊躇したＸＹＺ・ＢＯＸへの依頼を難なくクリアーですか。
いちの様のことだからきっと笑い１２０％の依頼をしたのでしょうね？！
よっ！副会長ぉ～！　（勇者の会のね）

＞いも様　【7936】
がんばれぇーっ！続けぇーっ！
「勇者の会」では、いも様の為に幹部のポストを空けて待っているそうですよ。

＞葵さま（もしかして、巨人ファン？）　【7940】
ＯＲＥＯ様とは、こちらのＢＢＳでＣＨのＣＤを頂いたのがキッカケで、仲良くなったのですよぉ～。
やっぱ、北条ファンは、『持ちつ持たれつ』つーことで・・・。

＞しゃぼん様　【7941】
そうです。テツ＆トモです。（さては、お笑い好きですなっ？！）
「ふけコン」すみません。解りません。（汗）
出来れば、教えて頂きたいーーーっです。
それから、「ＣＨ」でカキコが終わってるのは、狙い？ですか？
さすが、お笑い好き・・・（笑）

### [7941] しゃぼん	2002.09.23 MON 21:54:38　
	
[7869]＞たま様
お久しぶりです。覚えて頂けてたなんて感涙です。たま様になにか私しでかしましたっけ？（大汗）シャワーかかる以来かしら？（爆）レス読んで一瞬頭の中でテツ＆トモ（？）が浮かびました（違う？）最近ロムも十分に出来てないけどよろしくです。いんちきナースのしゃぼんでした。

ＣＨのスペで好きなのは’９７の「グッドバイマイスイートハート」ですねえ～。敵役のプロフェッサー役が山寺氏ってとこが往年のファンにはキましたね。話もおもしろかったし。私もそろそろ回しとかないとテープ腐るかも。

少し前の話になりますが、某ゲームのショウで田中真弓さんと伊倉一恵さん宛てに北条司氏から花輪がきていました。「ふけコン」以来の交流（笑）ながいね～。（年寄りしか解らないネタ）

ＣＨ

这是不久前的事了，在某游戏的表演中，北条司给田中真弓和伊仓一惠送来了花圈。自从“ふけコン”以来的交流(笑)很漫长啊~。(只有老人才知道的段子)
