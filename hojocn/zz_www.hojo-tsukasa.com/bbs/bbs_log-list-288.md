https://web.archive.org/web/20021101020336/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=288

### [5760] 無言のパンダ	2002.02.20 WED 13:17:50　
	
こんにちわ☆

＞Ｂｌｕｅ　Ｃａｔ様（５７２４）
レス、ありがとうございます(^^)
たしかに表紙に使われたイラストがイラスト集におさめられてないっていうのは寂しいですよね～！お気に入りのイラストは、きれいな形で手元にほしいですもんね(^○^)
私もジャ＠プの表紙を飾ったＣＨのイラスト、全部保存しとけばよかったなーって今さらながらに思います。
ただ「本家無言のパンダ」は雑誌の表紙をコミックスの表紙にもってくるのは「使いまわし」ってイメージがあるらしく彼女には不評なんですヨ（汗）「より数多くのイラストが見たい！」と言う意味でなんですけどね。私もそう言う意味では「書き下ろし」も見たいなぁとは思ってます。でもバンチの表紙になったもので、好きなイラストがコミックスの表紙をかざるってのも、もちろんうれしいですよー。

＞ＵＭＩ様（５７２７）
レス、ありがとうございます！
そうなんです～。娘が私のＨＮの名付け親なもんで・・・(^_^;)
それで使用権（？）についてモメた時に、まろ様の案で娘が「本家」私が「元祖」と区別することになったのです！（笑）
娘は友達関係に広めているだけなので、あんまり関係ないんですけどネ。これからもよろしくおねがいします♪

長くなりそうなので、とりあえずまた。(^_^)/~

### [5759] mei	2002.02.20 WED 10:52:45　
	
お久しぶりです(爆)←まだ３回目^^;
大阪から東京へ引っ越すので(しかも新宿近くをねらってる。)
ばたばたしててあんまりここにはこれないんですが^^;
さてさて、３月の先生の作品の発売予定載せときます。
<バンチワールド>
　06日ファミリー・コンポ[2]紫苑・援助交際!?編
　13日シティーハンター[30]祖父、現る!?編
　20日ファミリー・コンポ[3]思い出の同窓会編
　27日シティーハンター[31]冴子のお見合い!?編
<バンチコミックス>
　08日エンジェル・ハート[3]
見てて思ったんだけど・・・北条先生は「!?」よく使われてるような気がするんだけど・・・今回たまたまなのかな？^^;
タイトルまで気にして読んでなかったのでふと思ってしまいました(爆)

好久不见(爆)←才第三次^^;
因为要从大阪搬到东京(而且瞄准新宿附近。)
我太忙了，不太能来这里^^;
好了好了，现在登载老师3月作品的发售预定。
《Bunch World》  
06日Family Compo[2]紫苑·援交!?篇  
13日City Hunter [30]祖父，出现!?篇  
20日Family Compo[3]回忆中的同学会篇  
27日City Hunter [31]冴子的相亲!?篇  
《Bunch Comics》  
08日AngelHeart[3]  
看着看着就想…北条老师「!?」感觉好像经常被使用···这次是碰巧吗?^ ^;  
因为没有在意标题，所以突然想起来了(爆)

### [5758] 法水(Norimizu)	2002.02.20 WED 00:50:48　
	
＞Ms.(?) shan[5750]
Long time no see. “好久不見。”
It's so good you can read "A.H." every week.
It's translated into English or Chinese? If I have a chance to visit Hong Kong, I'll look for "JD Bunch."
BTW, what does "JD" stand for?

### [5757] *rose*	2002.02.20 WED 00:45:38　
	
　皆様、こんばんわ、*ｒｏｓｅ*です。まずはレスから！！

　[5730]おっさん様、こんばんわ。レスありがとうです。是非是非、新宿お薦めします。今年は既にもう１件、結婚式入ってるので、また行けそう！！けど、おっさん様、お仕事忙しいんですよね。（以前のカキコで・・・。）お体を大切に！！先週は火曜だったけど今週はいつになるかなー？雪降るとダメみたい・・・。では、また！！

　[5733]ちゃこ様、こんばんわ。レスありがとうです。
＞親戚に１人はいる面白い叔父さん（爆）確かに！！ス＠ス＠でラストの方にゲストにとってのス＠ップのポジションは？と聞く恒例のトークがあるのですが・・・。兄弟でもちと恥ずかしい。
結婚相手には、ってゆーか戸籍ない。恋人にするには甘くないのに過激すぎ！！友達になんてリョウの方がなってくれない、というか私ごときに本音は言わない（笑）となると残りはハトコ。
それでも昔はヒーロー性が強くて悪い面に気が向かなかった。
カオリ、本当に大変だと思うの。とか言っておきながら、めちゃくちゃリョウＬＯＶＥ～！！惚れた弱みです。上記はせめてもの強がりです。あんなに明るく昼間にナンパされたら一瞬（コンマ単位）でもマジ顔してくれたら、ついて行きますって！！けど、私にも作戦あるから初対面ではホテルに言っても×ですよ（笑）
けど、カオリの見方なの。私はとっても複雑です。麗香とかすみちゃんとリョウについて語りたいですぅ！！スミマセン、妄想炸裂の駄文でした。（逃げます！！）

　[5736]千葉のＯＢＡ3様、こんばんわ。そうです、そうです。
酒とタバコとコーヒー・・・。前々回のハンバーガーらしきものにちと安心です。阿香の料理・・・。どうでしょう？期待してます、が、カオリ出てくるのかな？進行形ってアリなのかなぁ？脳の記憶ではなく心臓だから瞬間に感じるカンジ？難しい・・・。
リョウは何でも出来るし・・・、というより誰かの為にならやっちゃうんだろうなー。けど、アノ１年はしてない！と思いたい。
私、根が暗いのかしら・・・？いや、いや、私の心の中では二人はラブラブです！！（またもや、逃げっっ）

　[5752]ひろ様、はじめまして、こんばんわ。*ｒｏｓｅ*と申します。ＤＶＤ情報ありがとうです。１０時間はスゴイ！！あっ、プレステ２ありました。そういえば！！「さらばハードボイルドシティ」が擦り切れないうちに欲しいです。では、また！！

### [5756] あお	2002.02.20 WED 00:05:24　
	
今晩は♪って、読んでいる方は･･･そうとは限りませんね･･･(汗)。
解禁が待ち遠しいです･･･。書きたいことけっこうあるから(笑)。

＞ちゃこ様
お久しぶりです！！！遅くなりましたが、お帰りなさいませ！私も最近出戻りしました(自爆)。何とか論文の口頭試問、クリアできたようです･･･結果は3月に出ますが、担当教官からのメールはどうやらそれらしいことほのめかされていたので。びば！！

言葉の使い方＞
不特定多数、全く見ず知らずの人間が読んでいる以上、不愉快になるかな？と思う表現は避けることが望ましいですよね。言論の自由は保障されていますし、感想は十人十色あって当然ですが、｢この場｣に書き込んでいい内容であるかそうでないかは考えるべきだと思います。
な～んてこと言っている私も、誰かを不愉快にさせているかもしれません･･･(大汗)。そういう時はどうか叱ったって下さいませ。それもまた愛(笑)です。どうでもいいと思っている人のことなんて、誰もコメントしたり叱ったりしませんものね♪

### [5755] ちゃこ	2002.02.19 TUE 23:52:49　
	
え～っと、度々すみません。レス第２弾です。

＞無言のパンダ様「５７１８」
こんにちは。
ニャンスキ様も書かれてますが、書き込み共感しました。（＞5743）
「時を越えて言い残した言葉を伝え合う」本当、そんな場面が見たいのです。
今まで書いたことなかったと思うけど、私が望む「優しい奇跡」は、
正にそんなイメージで「それよ、それ！」とかＰＣに話しかけてしまいました。

ところで。
旦那様への「来世で出会っても・・・」発言、大笑いでした。わかる！！
ただ、私は山梨に住んだからこそ、巡り会えた人達や物事も多いので、
とっても微妙(^^;;)　でも、友人に「もっと素晴らしい出来事が待ってたかもよ」
と言われ「それもそうね」なんて納得したりして。（←それで良いのか？）
だから、迷いつつも来世では「もっと素晴らしい出来事」を期待してる割合が
多いです（笑）
それ以前に人間に転生できるか、確かに疑問ですけどね。
私は、夢見たことありませんが、次は猫でも良いなぁ？


＞おっさん様「５７３０」
こんにちは。バレンタイン、はっきり気持ちは伝えてなかったのですね。
でも、微妙な恋のじゃれあい（？）楽しんでる感じで良いなぁ。
私もそんな頃がありました・・・（遠い目）
ちょっと話が逸れますが、この間「10年くらい前は・・・」って高校時代の
話をしようとしたら、遡れなくてショックだったのです。
（でも、年を重ねた自分は、あの頃の自分より好きだけどね。）

では、今日はこれにてドロン！　皆様、おやすみなさい。

### [5754] ひろ	2002.02.19 TUE 23:49:10　
	
こんばんは(^.^)

追記ですけど、

今日の事は今日だけにして、明日には引きずらないようにしましょうよ！

そしてみなさん、もっと楽しくやりましょうね！

それでは、再びおやすみなさ～イ(^^)/~~~

### [5753] いも	2002.02.19 TUE 23:38:35　
	
昨日は挨拶もそこそこに言い逃げみたいな形になってしまったので改めまして、皆様二度目まして、いもです！！こんばんばんばんばんわ～！！

‥‥っていう雰囲気ではないですね。
実は私はカキコするのこのＨＰが初めてで、昨日はものすごく緊張したんですが、でも言いたいこと言えて嬉しかったです。
カキコ初心者としては、こういう公式の場に意見を述べることがどれほど多くの人に影響を及ぼすのかまだよくわからないけど、
思ってた以上に大変なことなんだと感じました。
これから色んな人と仲良くなれて、ここにくるのが楽しみになるといいなあと思います。

それでは人生初レスです。
＞まろさま[5731]
はじめまして！記念すべき（？）人生初カキコにお返事いただきありがとうございました！！
わたしの歴史に今まろさまの名前が刻まれましたよ！（勝手に刻んですいません(^^;)でももう戻せませんよ～）
こちらこそよろしくお願いします。

＞ニャンスキさま[5743]
はじめまして。レスありがとうございます。
未熟者にはもったいないお言葉恐縮です。
私もリョウの気持を考えると心中複雑で、やり場のない辛い気持になったりします。でも登場人物と一緒に楽しんだり心痛めたりできるのって考えたらスゴイことですよ！これだけ心動かされているってことは、もうどっぷり頭までＡＨに浸かりきっている証拠だと思います。そんな作品に出会えてホントよかったです。

また長くなっちゃいました。簡潔な文章で自分の気持ちが伝えられるようになるにはまだ修業が足りないようです（苦笑）
それでは皆様おやすみなさい。いもでした。

### [5752] ひろ	2002.02.19 TUE 23:32:02　
	
こんばんは～(^o^)丿

新聞でチラッと見たのですが、このＢＢＳでも話題に上がっていたＤＶＤのことが載っていましたよ。
なんと、現在の６倍の記憶容量(約３０Ｇbyte)のＤＶＤが出るそうではないですか。デジタル放送だと２時間、一般の放送だと１０時間の録画が可能。場所も取らないので魅力的ですよね。
ただ、再生の規格はすべて統一されているのですが、録画の規格が３つに分かれているのが残念ですね。
最近では、子ども達がＤＶＤを借りてきて、プレ＠テ２で映画を見ているようですが、結構な迫力ですね。

このあとレスです。

kinshi[5734]さん、はじめまして。
液晶ともう一つの方って、デジタルテレビ、それともプラズマテレビのこと？　うちの誰かさんは、プラズマテレビが欲しいとか言っていますが、一式７０万、８０万(それ以上)の値段なので、とてもとても、薄くてよいのですが(･･;)

葵[5737]さん
ケーブルテレビ（または、スカパー)に契約していれば、日本全国でほとんど見れますよ。ひろは東京ですが、葵さんの神奈川もたぶん大丈夫です。
キッ＠ステーション(＠=ズ)を検索系サイトで検索してくだされば、情報が出てくるはずです。放送しているケーブルテレビも分かりますヨ(^○^)

少し眠くなりましたので、今日はこの辺でサイナラ(^^)/~~~

### [5751] ちゃこ	2002.02.19 TUE 23:12:33　
	
再びレス・・・のその前に。ちょっと書かせて下さい。

＞高田様「５７３８」「５７３９」「５７４２」
はじめまして。ちゃこと言います。
書き込みが今日の日付で「今日買ったバンチ」なら、どう考えても最新号。
というわけで、あなたの書き込みを読んで思ったこと、書かせていただきます。

何て言うか・・・ルールすれすれの書き込みに感じました。
別に「ＡＨ最新の話」の話そのものに触れているわけでは無い。確かに無い。
でも、確実にあなたの書き込みは「明日」バンチを、ＡＨを読む人の楽しみを
半減させてしまった気がするんです。
何故か？ネガティブな内容の書き込みだからです。（「5739」は違うけど）
皆、発売日前は楽しみにしてますよね？
次回予告や皆さんの書き込み読んで想像して・・・１週間楽しみに待ってる。
「楽しみ」に待ってるんですよ。あなたは客観的に自分の書き込みを読んで、
その「楽しみ」に水差された気分になりませんか？

過去の他の方々の書き込みでも「ちょっとフライング？！」的なものは
ありました。でも、それは「読む楽しみを誘う」ものでしたよ。
私は火曜日に読んでますので、そんな人達の書き込み見ながら
「そうだよねぇ。早く話題にしたいよねぇ。」と微笑ましく思ってました。
でも、あなたの書き込みは、そうは思えなかったです。

別に、ネガティブな書き込みをしないで！と言ってるわけではありません。
今までも散々書いてるけど、公式ＨＰだからって、先生の作品を褒めちぎる
必要は無いし、様々な人の思いを読めることが、BBSの存在理由だと思うから。
でも。「最新号を待ってる人達に水を差す」ような書き込みは、
たとえネタばれじゃなくても、解禁後にして欲しいです。
あなたの書き込みにレスを付けた他の方々も、そういう気持ちで
書いてるんじゃないかな？

気分悪くしたら、ごめんなさい。では、失礼します。

### [5750] shan	2002.02.19 TUE 22:31:37　
	
Hello !

I am so glad to share with everyone that now there is an authorized Bunch (called JD Bunch) being sold in Hong Kong every week. It's only lagging behind the original story by 2 episodes.

Thank you so much Mr Hojo for giving our Hong Kong readers such a great opportunity to enjoy the story as early as possible. ^_^

### [5749] さえむら	2002.02.19 TUE 22:19:37　
	
さえむらです。

[レス]
＞K.H様[5674]、Chikkaさま[5678]
＜冴子の指紋＞
何かで読んだのですが、警察官が現場に行った時、過って証拠物件等を触ってしまうと、指紋を採取するそうです。
冴子さんは、頻繁に現場に出てそうだし、新人の頃にはうっかり証拠物件等に触ったりしてそうだし。（という憶測に基づいています。すみません。）

＜新宿での戦争＞
Chikka様の御意見の「うやむやに出来る」のでしょうか？
あれだけの大規模な封鎖が行われ、SATも出動...。マスコミが気が付かないのでしょうか？それとも、箝口令が敷かれるとか？ネット時代の現代、圧力をかけようが、うやむやには出来ないのではないだろうかと思います。一時マスコミが報道しなくとも（出来なくとも）、ネットで広がり、マスコミが取り上げずにはいられなくなる程、大きくなるんじゃないのかと思います。

＞*rose*様[5670]ゆうちゃん様[5677]
「フロスキー」についての情報、ありがとうございました。
もし、過去ログをお読みになっていらっしゃるのであれば、さえむらの発言は、さら～っと流しておいて下さい。（というか、抹消したい...）

＞葵さま[5688]
Ｃ・Ｈでの墓石破壊は、気にしてませんでした...(^^;)
Ｃ・Ｈは、なんて言うか、「遊んでる」イメージが強くて。北条先生のお墓があったりとかして。

＞ちゃこ様[5746]
私も、ちゃこ様の感想文、大好きです。
これからも楽しみにしています。
「Ａ・Ｈが続く限り、感想文を書き続けられる」とか。
わーい！！楽しみにしています。(^o^)/~~

それでは。

### [5748] ｌｕｎａ	2002.02.19 TUE 22:15:08　
	
ちゃこさん、レスありがとうですぅ（＾＾）
そーなんです　花粉、もぉ来てますよぉ
薬で、予防してるけど、時々、朝、出勤する時、外出たら、
涙、ボロボロでますぅ
なんか、泣きながら、仕事行ってるみたいですよねぇ～
これから、ゆーつうな季節になるよぉ
花粉症になる前は、春が来るって、ウキウキしたもんやけど・・
今は、はぁーーって感じですぅ（＾＾）
ちゃこさん、明日の解禁後のカキコ待ってますよぉ
では、これからは、一緒に花粉症と闘う、戦友？
になりますねぇ

ＫＨさん、こんばんわ
せっかく、こらえはったのに、５７４７で、カキコしてしまったよぉ～
ごめんなさい～
あたしも、ルール違反ですねぇ
みなさん、ごめんなさい！
以後、気をつけます
高田さん、えらそーに言ってすみません

### [5747] ｌｕｎａ	2002.02.19 TUE 22:06:09　
	
こんばんわ
高田さん
はじめまして、
５０３８で、今日、買ったバンチはのＡＨは、
って、特定してますよね
今日、買ったっていうのは、解禁前のバンチだと、
おおかたの人達は、思いますよ
だって、バンチの先週号が、今日、あるなんて思えないから・・
だから、５７４２で、実際そうなのか、わからない
って事はないよ
愛があるＢＢＳって、
ほんとに思ってますか？
顔が、見えないＢＢＳでのカキコは、言葉には、充分
気を使ったほうがいいですよ
それでなくても、誤解されやすいから・・・
あなたが、言われるように、今まで、あたしが、ここに参加
するようになって、ルールを破ったカキコは、見た事が
ありませんでした
でした
過去形です
意味わかりますか？
不快に思ったら、人の振り見て、自分は、気をつけよう
って思ってくれたら、幸いです
みなさん、不快なカキコを読んでくれて、ありがとうです
そして、ごめんなさい

### [5746] ちゃこ	2002.02.19 TUE 21:59:14　
	
こんにちは。本日バンチ発売日♪　というわけで、解禁が楽しみ。
では、レスです。

＞ｌｕｎａ様「５６９７」
こんにちは。私も、ｌｕｎａ様に再会できて嬉しいです。
また、宜しくお願いしますね。
ところで、花粉症なんですか？じゃぁ、しばらく辛い日々ですねぇ・・・
私は出産直後の春＆翌年の春、花粉症の症状が出ず、冬はずっと辛かった
冷え性も治ってしまい、「ラッキ～♪」と思っていたのですが、
この冬は再び冷え性に逆戻り。カイロを貼りまくって生活してます。
鍼灸の先生曰く「出産後１年くらいは、ホルモンの関係で体が温かい」そうで
丸２年経った現在は、ホルモン切れらしい（苦笑）
この分だと花粉症も復活しそうです。（最近、鼻がムズムズする・・・）
でもメゲずに、お互いテッシュ片手にＰＣに向かいましょう（笑）

＞ゆうちゃん様「５７１３」
はじめまして・・・かな？
私なんぞの感想文、楽しみにして下さっていたようで、有り難うございます。
ファンだなんて・・・私を褒め殺ししようとしてるのかしらん？！とか
思っちゃいますよ～（照れまくりだわ、本当）
でも、感想文を書かなかった間（このBBSにアクセスしなかった期間）
ＡＨ読んでいても、何だか消化不良でした。
そう。ここで、皆さんの意見を読んだり、自分が書き込みすることで、
ＡＨの世界を更に広げて楽しんでたことに気が付いたんですよ。
なので、ＡＨの連載が続く限り、私も感想文書き続けますので(^^;;)
これからも宜しくお願いします。

それでは、この辺で。また来ます。


### [5745] K.H	2002.02.19 TUE 21:59:09　
	
･･･日付が2/19で、「今日買ったバンチ」なら、最新号だとボキは思うんですが･･･
　まあ、これ以上は言いますまい。個人攻撃になってしまうので。

＞ある漫画家の衝動
　今も某少年誌に連載を続けている、ある売れっ子漫画家は、その出版社のやり方に我慢がならず、当時人気絶好調だった連載を急遽打ち切りにしたことがありました。
　そのやり方とは、いわゆる「アンケート至上主義」というやつらしいです。人気があればネタが無くても連載を続けさせ、ネタがあっても連載を止めさせる、というものらしいです。
　これが原因で、どれほど多くの実力のある漫画家が苦渋の日々を送ったことか･･･
　ボキは少なくともコアミックスはそんな出版社ではないと思います。(変な誤植は多いですけどね)

＞sweeper様[番号？]

　「ちょ、ちょっと待って！！その先輩って・・・！」
　「・・・はい、野上冴子先輩です・・・（ポヨ～ン）」
　「あいつ～！！」

　ども、バレンタインデーは一人勉強していたK.Hです。なんか、お約束のセリフ引用ですが、かなり多くの方々がご覧になっていらっしゃるようですね。ちょっとボキ、照れちゃいました。
　唐突ですがsweeper様は、A.Hの主人公は、誰だと思います？ボキは香瑩だけではないように思えてならないんですよ、正直言いまして。むしろ香も？と思うのですが、sweeper様はいかがお考えですか？

　というわけでした。では皆様、解禁後にお会いしましょう。　

＞某位漫画家的冲动
有一位至今仍在某少年杂志上连载的当红漫画家，因无法忍受出版社的做法，紧急终止了当时人气正盛的连载。
这种做法就是所谓的“问卷至上主义”。据说只要有人气，即使没有素材也会继续连载，即使有素材也会停止连载。
因为这个原因，多少有实力的漫画家度过了苦涩的日子啊…
我认为博基至少Coamix不是那样的出版社。(奇怪的错字倒是很多。)


### [5744] おっさん（岐阜県：？？？？？）	2002.02.19 TUE 21:18:41　
	
毎度、なにかとお疲れ気味なおっさんっす。ということで今日もいつものご挨拶せ～のおっは～！！
レスでやんす。
高田様（５７３８）・（５７４２）、初めまして。ちょっと一言言ってイイですか？？？言い方によっては傷つける一言になってしまったら申し訳ございません。２つの書きこみ読んでるとなんか？？？？？って思ってしまう文章があったんですけど。どうなんでしょうか？何か読みにくい気がしたのは私だけかもしれませんが。申し訳ございません。
明日は解禁なんで来れるときに来ます。

### [5743] ニャンスキ	2002.02.19 TUE 21:09:14　
	
こんばんは。お邪魔しています。
私も、皆様ルールを大事にされている様に思いますが…
シリアスものはネタばれ致命傷ですしね。
ああ早く皆様と今週号の話したいです。

>[5728] いも様
初めまして。そうですよね…私も同感です。
悲しさとか、切ない気持ちばかりじゃなく、
依頼人なしの二人きりのこんな素敵な関係、やりとりを
断片的ですが見られるのはＡＨならではですね！
嗚呼、このひとときの幸せよ……と読みながら噛み締めて
います。香がいないからこそ際だつ、なかなか
本心を見せないリョウの彼女への想いとかもありますし…
ＣＨではなかなかそうはいかなかった私達の夢を
ＡＨで叶えられたという面もありますよね。
…何だかんだ言ってもまだＡＨのこの展開を
受け容れられてない部分もある私ですが、いも様の
カキコ読んでちよっと明るい気持ちになれました。

>[5718] 無言のパンダ様
こんばんは。書き込み読ませて頂きました。
「生まれ変わっても、きっと引き寄せられるように
出会ってしまう…」
そうですね（涙）すんごく感動しました。
二人の関係を私の思い通りに表現してあったので…
誰も…死さえも、あの二人を引き離すことは
出来ないですね…

…ちょっと遠い眼になってしまいましたが
この辺で失礼します。ニャンスキより

### [5742] 高田	2002.02.19 TUE 20:16:58　
	
>K.Hさま

んーと。他の方のフォローになるかな？
皆さんルールには反していませんよ。

なぜなら、
誰も、”今週””もしくは”最新号”とは
はっきりと、明確にかいていません。
主張もしていませんよ。

そうとれていても、実際そうなのか、わからない文章です。


皆さんルールを配慮しつつ、書き込みしています、
このBBSに・・・
愛あるとおもいますが、どうでしょうか？

早とちりは、ある意味混乱を招くので
気をつけたほうがいいと思います。

### [5741] K.H（From やまなし）	2002.02.19 TUE 19:57:24　
	
ほんのちょっとお邪魔

＞皆様へ

　上の事項でもお分かりでしょうが、今週のA.Hの話題の解禁は水曜18:00以降ですよ。納得しようがしまいが、最低限のルールは守りましょう。
