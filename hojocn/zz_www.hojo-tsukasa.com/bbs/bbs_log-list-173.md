https://web.archive.org/web/20021024203859/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=173

### [3460] ネット	2001.10.06 SAT 01:44:15　
	
こんばんは。ネットです。

今日、病院実習に行っていたのですが、そこで患者さんの死を見取る場面の写真をたくさん見せられたんです。
で、私の頭の中では香のことがもうぐるぐるぐるぐる回ってしまって、病院長の説明の間、涙が止まらずに号泣してしまいました。駄目だよ、あたし・・・（その場にいた先生方や同級生はその写真を見て泣いたと思っているらしいんですが、違うんです/汗）
ずっと想いを伝えられずに何年も過ごしてきて、やっと素直になれた後の香の事故・・・あまりにも切なくて、考えるだけで涙がとまらなくなってしまいました。彼女がリョウの傍にいて幸せだったことを、素晴らしい生を過ごしたことを願ってやみません。

＞ちゃこ様「3409」
レス、ありがとうございます。そう言っていただけると本当に嬉しいです。
＞辛口ながら・・・
やはりそう思われていましたか！（苦笑）
う～ん、確かに自分の書き込みを読み直してみるとそんなんばっか。もしどなたか、不快な思いをされていたら申し訳ありません。

けれど、辛口なのは作品に対して愛あればこそ！ですので、どうかご容赦くださいませ。愛がなければ「面白くない」と判断した時点で読むのをやめています。
私の中でＣＨのラストは本当に理想的なものだったので、それがこういう形で復活したことに関して複雑な思いを抱かざるを得ないのです。１０年前、子供心にも限りない未来と希望を与えてくれたあのラストを、私は一生忘れないでしょう。

さあっ、来週も朝一番でコンビニいってバンチを買うぞ！（笑）

### [3459] ちゃこ（レスです）	2001.10.06 SAT 01:02:42　
	
＞おっさん様「3454」
こんにちは。おっさん様の隠れファンでした（笑）　おっさん様の書き込みに
名前が登場して嬉しいです(^^)　そう私も、おっさん様同様、接客業してます。
といっても、育児と兼業の現在、週３～４日。それにインショップといっても
自営業なので、人間関係の大変さはありません。（何せ店長は義妹）
おっさん様は大変そうですものね・・・１日の大半をそこで過ごすだけに
精神的にやられると、辛い。どうにか乗り切って下さいね。
こちらのＳＣも今月特招会。来週くらいから準備が始まります。
お互い体調に気を付けて、忙しい時期を過ごしましょう。

＞Ｋ．Ｈ様「3448」
はじめまして。ここは、B'zやTMNのファンの方、多いですよ（＾＾）
私は、このBBSに来てから、ちゃんと（？）聴くようになりました。
（リョウ＆香のイメージソングとして、かなりの頻度で話題に登場してたので）
Ｋ．Ｈ様の書いた曲は、どれも好きです。特に熱心に聴いてないときでも
良いなぁ。と思っていた曲は「ZERO」とか「さまよえる蒼い弾丸」
「LOVE　PHANTOM」です。今はこれに「TIME」を始めとして、リョウ＆香を
イメージさせる切ない感じの曲が加わります。

そうそう。「リョウか海坊主サンか」選べませんよ、私には（＞3439）
どっちも大好きです♪違うようで、似てる２人。あの２人がコンビを組んだら
「無敵」って感じなのも良いです。海坊主サン「リョウの最後の仕事」の為に
カムバックするのでしょうかねぇ？

＞Ｂｌｕｅ　Ｃａｔ様「3442」「3444」
無事に節約出来るようになって良かったですね(^^)　でも機種の違いかなぁ？　
私の場合は、オフライン作業からログインは出来るんだけど、Webソフト上で
書き込み作業をすると、アップされたときの書き込みが変になってしまって。
だから、ワープロソフトで書き込み作ってコピー＆ペーストしてたんでした。
「画像保存」そんなに簡単なんですか？とすると、私の処理が悪かっただけで
プロテクト掛かってるわけでは無いのかなぁ？
「公式ＨＰ」なのに、それだとガードが甘いようにも思えるけど。謎だ・・・

ところで全然話は違いますが。ディズニーランド＆シー、楽しみですね。
従弟がバイトしていて、プレオープン声かけてもらったんだけど、
「真夏」「子連れ」「ダンナの仕事の都合で日帰り」の三重苦に、断念。
もう少し子供が大きくなったら行ってみたいと思ってます。

いい加減、長くなりましたので、失礼します。

### [3458] ちゃこ	2001.10.05 FRI 23:53:17　
	
こんばんは。再び登場です。

「イラストレーションズ」
優希様の書き込みを見て「ジュンク堂」ＨＰ行ってみました。（＞3435）
思いっきり「在庫ナシ取り寄せ」でしたが、一応、申し込んでおきました。
（「集＠社」に無いんだから無理だろうけど、気休めに（笑））
で、さちこ様（＞3447）オークション、教えて下さって有り難うございます。
早速アクセスしましたが、今日は出品されてませんでした。残念。
でも、まめにチェックしてみようと思います。
出品されてたら「オークション初体験」です。
「オークション」って「プレミア価格」とか付くイメージがあるから、
ちょっとドキドキしちゃいますね。
三毛猫様の「2,000円で発見」に、余計そう思ったりしました（笑）

「Ｐａｒｒｏｔ」
仕事帰りに、たまたま行った本屋にありました。
「イラストレーションズ」の代わりでもないけど、思わず購入。
というわけで（？）　Ｐａｒｒｏｔ様、お久しぶりです。（＞3441）
今度はちゃんと読みましたよ～（笑）面白かったです。
かなり「耳で読む（変な言葉だけど）」事を意識した話の作り方してるなぁ。
と思ったので、やっぱりＣＤ－ＲＯＭ聴いてみたいです。
でも、書き込み読んでると、これこそ「プレミアもの」のようですね。

では、また。

### [3457] 無言のパンダ	2001.10.05 FRI 23:46:34　
	
こんばんわ★
ＣＲＡＺＹ　ＣＡＴ様（３４５２）
お酒の量について、ご忠告ありがとうございます。
そうですね、私の父もお酒の飲みすぎで早くに亡くなってしまったので、気をつけようと思ってます(ｰｰ;)
でも！ＣＲＡＺＹ　ＣＡＴ様、たしか１５歳になったばかりですよねぇ？ダメですよ、お酒なんか飲んじゃ～！早すぎ！
早くから飲んでも、ろくな事ないですからね。
人間、一生に飲める量は決まっていて、早くから飲むとそれだけ
早く一生分の量に達してしまって命を縮めるんですよ！
根拠のない私説ですが・・・（笑）
それに大人になってから堂々と飲んだほうが美味しいですからね♪

おっさん様（３４５４）
お仕事ご苦労様です！おつかれさま☆
のんべえの私ですが、１年に数回こんな私でも、まったくお酒を受け付けなくなることがあるんですよ。
お酒やめたほうがいいかなぁって、友達に言ったら
「お酒やめるなんて人生半分捨てるようなものよ！」と力説され、妙に納得してしまいました（笑）
でも最近は、あとが、しんどいのでほどほどにしています(^_^;)
それから年のこと、私なんかと比べたら申し訳ないですよ（笑）

それに私のことは別にしても、年齢を重ねることは決して恥ずかしいことではないと思います。
それだけいろんな経験も重ねて、思慮も深くなってるはずなんですから・・・（←私は例外！）

関係ないことばかり書いてしまってごめんなさい！
ところで、ＣＨの再放送、大阪方面の友人も番組欄にないって言ってましたよ。
私も見られないのはわかってるけど気になります～！
どこでやるのか、おしえてぇ！

では、おやすみなさい★

### [3456] ワタル／Wataru	2001.10.05 FRI 23:25:22　
	
わーい，私のこと覚えててくれる人いて，嬉しいです。

【ＣＨのテレビ放送】
キャベツ（ケーブルテレビ）のキッズステーションでやって
ますよ。私は実家帰ったときしか見られないけど。(/_;)

【ＣＨ on TV】
You can watch CH series on Kid's Station on CABBAGE
(cable TV). I can watch those only when I come back
to my parents' house.(/_;)

### [3455] 琴梨	2001.10.05 FRI 23:12:23　
	
こんばんは。　琴梨です。　お仕事されてる方々、一週間お疲れ様でした。私は明日から３連休です。

＞アニメ再放送
何だか、読売テレビ（関西エリアで１０ｃｈです）だけのようですね…　少し残念。

＞年のこと
私も２０代なんです。ＣＨは小学校の頃からずっとリアルタイムでジャンプもアニメも見ていましたし、ＣＡの時なんて、教室で手作りのキャッツカード投げが大流行していたぐらいですから！（覚えのある方いませんか！？）

### [3454] おっさん(岐阜県：やれやれ・・）	2001.10.05 FRI 22:41:42　
	
毎度、今日何とか仕事の間にカレンダーの料金をぶちこみましたおっさんです。今日も疲れたじょ～。ということで皆様いつものご挨拶せ～のおっは～！！
さちこ様（３４４７）、初めまして。それ書いたの私です。やり方は結構簡単なんで・・・。一応参考までに、￥３，６５０＋手数料￥４２０なんで。
ちゃこ様（３４４６）、ショッピングセンターで仕事してるんっすか？私も一応ＳＣで働いてます。一応今特招会（略すとこうなります）の葉書を書きまくってます。明日か明後日には出さなくてはいけないんで。
無言のパンダちゃ～ん（３４４５）、おげんこ。酒かなりのんべっすか。すご～！私は６年前に蕁麻疹を起こしてから飲んでない＾－＾；。
kinshi様（３４５３）初めまして。年気にすることないっすよ。無言のパンダちゃ～んもそうなんで。（ごめんなさい＾～＾；）
ということで、おっさんは引っ込みます。

### [3453] kinshi	2001.10.05 FRI 22:01:14　
	
>３４３２さえむら様
どうもKinshiです　教えてくださって　ありがとうございます
まだ、このHP ５日目で、　Log Listがあることさえ　しりませんでした。以後　気をつけます。＜モッコリ＞でゆるして！

By the way　みなさまに　おたのみが・・歳のことはやめてーなー！！　　はずかしくて　私　参加できなくなりそう＜もっこり＞　今月末に、また　年をとるのです　いやでも・・＜T_T>
わたしぐらいになると　年を、かさねる　といいます。
・・・おなじかな？
では、また。

### [3452] ＣＲＡＺＹ ＣＡＴ	2001.10.05 FRI 21:55:38　
	
こんばんわ。ＣＲＡＺＹ ＣＡＴです。

＞聴コミの申し込み方
　あれは、10/9に出る、コミックス６冊+バンチ２２号
+２３･２４合併号、この８冊の内、どれか３冊を買わないといけないんですよね。１番安いのは、コミックス１冊に、バンチ２冊という組合わせですが、ＡＨの他に、蒼天の拳とか眠狂四郎とかリプレイＪとか買う人は、コミックス３冊、あるいは、コミックス２冊+バンチ１冊という組合せでもいいんだと思います。
あってますよね？いろいろな意見が飛び交っていたので不安になってしまいました。

＞無言のパンダ様
　飲めるだけいいですよ。しかも堂々と。ぼくなんか飲みたくても親に止められてあまり飲めません(未成年だからしょうがないけど)
健康のためだと思って、思い切ってお酒の量を減らしてみては。

＞手に入りにくい本について
　ぼくんちの近くの古本屋には、こもれ陽･･･が３巻ともだいたいの店に置いています。しかし規模が小さいために、貴重というか、本当に好きな人が買い、あまり売られないイラストレーションズのようなものは置いてません。ちなみに、ガイドブック、Parrot、天使の贈り物の愛蔵版、２０ｔｈイラスト集など、割と最近のものは、未だに新刊で、しかもデパートの中に入ってるような中途半端な大きさの本屋にもフツーに置いてあります。横浜の杉田ってトコなんだけど･･･

＞アニメ再放送
　いいですね～。しかしぼくの住んでる地域は放送されない。アニメの記憶が乏しいだけに、かなり見たいんだけど･･･。ＤＶＤになるのを祈るのみです。海外でもアニメ放送されたものなんかは、海外でＤＶＤになってたりするんだけど(音声は日本語で字幕は入りますが消せるものが主流)(きま＠れオ＠ンジロ＠ドなどがそうです、ま＠も＠泉氏の)、ネットで探してみても無いみたい(海外で放送されたかどうかは知らないけど)。キャッツ･アイのＤＶＤ版のｓｅｃｏｎｄ　ｓｅａｓｏｎも待ち遠しい。金はないけど。

さて、長くなってしまったので失礼します。
ＣＲＡＺＹ ＣＡＴでした。


### [3451] 葵	2001.10.05 FRI 21:44:58　
	
＞無言のパンダさま［３４４５］
　ミニクーパー、私も昨日見ました。天井（？）が白のヤツ♪
　お見舞いに行くためにタクシーに乗ってたんですけど、病院を通り越して、後を追っかけてもらおうかと思っちゃいました☆
　ＣＨを知らないで乗ってる人がいたら、熱い視線をなぜ浴びるのかと不思議に思うでしょうね。（笑）

　ＣＨの再放送。読売テレビでやるくらいなら、関東の日○テレビさん、一緒にやってください！ファンはあきらめませんぞ！！
　

### [3450] ｌｕｎａ	2001.10.05 FRI 21:12:23　
	
３４４１のｐａｒｒｏｔさん
２０才過ぎたらあかんって
何？
ちなみにあたしは、２１才ですが・・・

### [3449] 三毛猫	2001.10.05 FRI 20:42:21　
	
「イラストレーションズ」話題になっているんで、私もつい古書の本場・神田まで足を伸ばしてしまいました。
事前に検索で1000円で在庫切れの情報は知っていたのですが、500円で買ったとの話も出ていたんで、それぐらいで期待しつつ。
そしたら某古書店にありました！それも帯び付きで状態も良かったのですが･･･。
何と2000円のプレミア値が付いているじゃありませんか！！
定価が1000円だったんですよね？
いくらオトナでも、定価以上で買うのは･･･(TT)
しばらくの間は冷静に考えるので精一杯です･･･。
表紙、現物はやっぱりカッコよかったです！！
2000円のオーラが出ていました。（苦笑い）

### [3448] K.H	2001.10.05 FRI 19:53:25　
	
２度目のメッセージです！！！
＞ちゃこさまへ
　はじめまして。実は僕はB`ｚの大ファンでして、7月の西武ドームのコンサートにも行っちゃいました！！好きな曲は「いつかのメリークリスマス」・「恋心」・「もう一度キスしたかった」などです（全部「B`z The Best Treasure」の曲ですね～）。「コブシヲニギレ」をコンサートで歌ったときの稲葉は必見でした！！「憎たらしい笑い声が響く」の歌詞の時、映画のドラキュラなんかが出てくるときのライトに顔を照らされ、笑ってましたよ！！というわけで、今「ELEVEN」を聞きながら書き込みしています。もしB`zネタでお詳しかったら、ご一報ください。
＞「常連」の無言のパンダ様
　はじめまして！！海ちゃんいいですね～！だから、美樹ちゃんみたいに海ちゃん一筋に思う女性が出現するんですよね～。「シティーハンター」でもリョウが、「顔の形に左右されず、あいつの心がはっきりとみえた・・・男を見る目があるよ」と美樹ちゃんに言ったことを思い出しました。いずれにせよ、すごい二人ですね！！僕も見習わないと。
　ところで、アニメ「シティーハンター」の再放送って、どこでやるんですか？僕は山梨に住んでいるから諦めてます（「シティーハンター3」と「`91」しか放映しなかった！！あまりに悔しいから、「2」は全話ビデオをレンタルして見ちゃった！）が、なぜあのアニメは地域差が激しい(そう思っているのはひょっとして僕だけ？)のでしょうか？やっぱり「もっこり」がいけないのでしょうか・・・　

### [3447] さちこ	2001.10.05 FRI 18:51:30　
	
こんばんは。

＞３４３３・ちゃこ様、
イラストレーションズ在庫切れというのを読んで、とても残念でしたね。でも、私、ヤ＠－オークションで何冊かみかけました。
もし、オークションお嫌いでなければのぞいてみてはいかがでしょう？
イラストレーションズのリョウ、すごいかっこいいので是非手に入れて欲しいです。ちなみに私は表紙がよく見えるように立てかけて飾っています。変色が心配です。

ＣＨの再放送、ここで知ってＴＶ欄を探しましたが見つかりませんでした。関東ではやらないんですね。。。すごいショック。
読売テレビというのは、大阪方面でしょうか？

カレンダー、どなたかが書いておられましたが、お金を振り込まないと制作してくれないのですね。。。私もこれみおとしていてまだ振り込んでいません。それなのに、楽しみにわくわくしていたとは・・・。ショックです。３連休明けに銀行行かないと。
カレンダー、私表紙に入れる名前、キャッチコピーみたいなのを入れました。「永遠のTWO　SHOT」です。これ、どんな風に入るんでしょうかね？サンプルにも名前の部分が入っているのなかったし。たのしみに待てということでしょうか。。。

それでは、また。


### [3446] ちゃこ	2001.10.05 FRI 16:08:25　
	
こんにちは。仕事を早く終えたけど、夕飯作って、子供を迎えに行かなくては。

「音楽ネタ」
ＴＶを付けたら、偶然ポルノグラフィティが「電話相談」してるところでした。
ラッキ～♪（＞CRAZY　CAT様「3427」・Blue　Cat様「3442」）
「Voice」また聴けて嬉しかったです。そうですね、私の場合、
前半はリョウにもＧＨにも通じる内容に感じました。で、後半がリョウ。
「過去が＠＠＠＠＠Voice　明日＠導くVoice　君は確かに＠＠　届くよ」
私は勝手に、リョウには「鼓動（こえ）」が、香の「リョウ」と呼んでる声に
聞こえているのでは？と思っているもので(^^;;)
それでもって今週号で「ドクン」は描かれて無いけど、リョウは今も、
香の「鼓動」を感じてるんだろうと思ってます。

「JOY」は「土曜ワイド劇場」のエンディングだったとは。（＞chikka様）
何となく耳に残っていたけど、探すまではしていなかったので、
題名や、歌ってるのがTRFだということを、今回初めて知りました。
リョウと香のイメージだから。というだけでなく、良い曲だと思ってます。
ちなみに私「宇田多ヒカル」も「浜崎あゆみ」も買いました。話題だったのと
発売日に、働いてるショッピングセンターが、特別招待会で安く買えたので。
でも「outernet」が発売されてるのは、つい最近まで知りませんでした(^^;;)

あずみちゃん♪様、お久しぶりです(^^)　最近、女性ボーカル中心でしたが
Ｂ’ｚもお薦めしてくれたのは、全部おさえてＢＧＭにしております（笑）
切ないモードが続いたときは「さよならなんかは言わせない」が、慰めでした。
何かあれ聴いてると、前向きになれるのです。
「ELEVEN」好きな曲多いけど、１曲あげるなら「Raging　River」かな。

ではまた、夜に来ます。

### [3445] 無言のパンダ	2001.10.05 FRI 14:58:01　
	
こんにちわ☆
昨日もカキコしに来ようと思ってたのに、缶ビール２本飲んでいつのまにか寝てしまってました(-_-)zzz
もう少し若いときは、お酒にめっぽう強くて、弟がお店をやってる時にはビール、酎ハイから始まって日本酒、水割り、カクテル、にごり酒と次々と飲んで平常心を保てたという記録があったくらいなのに・・・
やっぱり、年には勝てませんね。トホホ(~_~;)
関係ないこと言ってすみません（汗）

Ｐａｒｒｏｔ様（３４４１）
はじめまして・・・なのでしょうか？
私も、ここに来る前から皆さんの書き込みは読みに来てたので、
そんな気がしませんよ♪
それから、私が「常連」ですかぁ？！めっそうもないです。
でも、ありがとうございます(^o^)
私も、Ｐａｒｒｏｔ様のような若い方たちとお近付きになれて、
うれしいですヨ(^^)
それからＣＨ、テレビスペシャルの北条先生（？）のこと、教えてくださってありがとうございます♪
そういえば、ほんの少しの声の出演だったけど、声優さんにしては素人っぽかったかな？（声優さんだったらどうしよう！汗）
もう一度見てみよう～☆

「海坊主さん人気」
すごいですね～！もちろん私も大好きです♪
リョウとどちらか？と言われてもこまっちゃうけど、母性本能をくすぐられるのは海坊主さんですね。
あの大きい体からは想像出来ないシャイなところが、めちゃめちゃカワイくて思わず抱きしめたくなります(^.^)
でも、さすがの私も腕が回らないかも・・・！

「ミニクーパー」
昨日、見たんですよ！まさにあの屋根が白くて赤いボディの。
都会では珍しくないかもしれないけど、こんな田舎町では、めったに見ることはないんですよ★ちょっと感激！
乗ってる人って、もしかしてＣＨファンなのかな？

「ＣＨ再放送」
うらやましいですね～。
うちにもビデオあるけど昔はビデオテープ高くて、気に入ったものしか残してないんですよね(･･;)
で、再放送がある地域ってどこなんでしょうか？

玉井真矢様（３４２８）
「聴コミ応募の件」コミックスとバンチの２２号と２３、２４合併号のいずれかから、３点と書いてあるので必然的に３つとも要るんじゃないのかなぁ？バンチの同じ号を２冊買うってことはないと思うので・・・。私もよくわからなくなってきたわ～。
本当にわかりにくいですよねぇ、あの説明書き。

タラタラと長くなってしまってごめんなさい(__)
では失礼します(^_^)/~


### [3444] Ｂｌｕｅ　Ｃａｔ	2001.10.05 FRI 13:23:48　
	
いったい何故オフライン作業の状態でもログインできるようになったのだろう・・・わからん^^;　でもこれで、節約できる♪　実はやっぱり貼り付けの方法が理解できませんでした・・・（汗）　せっかく教えていただいたのに、すみません^^;

＞玉井真矢様［３４２８］
　聴コミＣＤは単行本とバンチ（２２号と２３・２４合併号）のうちの、どれか３冊分の応募券（組み合わせ自由）で応募できるようなので、単行本を３冊買う気があるならバンチは買わなくてもいいんだと思いますよ。バンチだけではダメってことなんですね。だから単行本１冊とバンチ２冊とで応募する人が多いとは思いますけどね。

### [3443] AYUMU	2001.10.05 FRI 13:16:12　
	
はじめまして！AYUMUともうします。（ペコリ）AYUMUといっても女性ではありません！れっきとした男です。大学の空き時間にやってます。悲しいかな家にパソコンはあるんだけどインターネットに入ってないんですー！（泣）とゆうことであまり顔を出せないかもしれないけどよろしくお願いします！



### [3442] Ｂｌｕｅ　Ｃａｔ	2001.10.05 FRI 11:21:17　
	
＞［３４３３］ちゃこ様
　ＷｉｎｄｏｗｓとＭａｃだと違うんでしょうか・・？うちのパソコンはＷｉｎｄｏｗｓ　Ｍｅのなんですけど、ただ壁紙に使いたいイラストとかを右クリックして、名前をつけて保存しただけなんですけど・・・（って言ってよかったのか？）
　それから、教えていただいた方法で貼り付けてみようとしたらなぜか、オフライン作業のままでログインできてしまいました・・あれ？　前からできたのかなぁ？

　　昨日の『とくば＠』でも、もう一度ポルノグラフィティの「ヴォイス」をちゃんと聴いてみました。
　『僕の名前を呼ぶ＠＠誰？』『誰かをずっ＠探して＠る　そんな気＠するのだ＠れど』とか、やっぱりなんか、連載の最初のころのＧＨのイメージみたいだなぁ、って気がしました。あと、『きっと誰かに逢い＠くて　僕はここ＠いる＠＠』ってとこなんて、リョウに逢いたくて新宿にまで来た、ばかりの頃のＧＨみたいで・・・。
　

### [3441] Ｐａｒｒｏｔ（お久です）	2001.10.05 FRI 03:20:10　
	
こんばんちわー。お久し振りです。夏休みも終わり、バイトから本職（大学生）に戻ったＰａｒｒｏｔです。ここに来るのは２，３週間ぶりです。その為、相当の過去ログがっ！読むの大変でした。だから、頭が色々とごちゃまぜになってます。取り敢えず・・・レス下さった方有り難うございます。「リョウちゃんだいかんげきー」です。←言ってみたかった。

[神谷明さん]
なっ！なんとお見えになられてたんですね。驚きました。遅ばせながら、誕生日おめでとうございました。因みに私も９月生まれです。これからも、お仕事頑張って下さい。応援してます。

[もっぴー様]
お久し振りです。カキコ見て驚きました。一時お休みされるそうで・・・。淋しくなります。早目の復活を祈っています。お休み中、偶には、お顔を出して下さい。

レス
＞将人様[３１３６]
初めまして。Ｐａｒｒｏｔと申します。宜しく御願いします。遅くなりましたが、「機動戦士ガンダム　逆襲のシャア」での伊倉一恵さんが声優をしていたキャラは青いギラ・ドーガを駆るレズン＝シュナイダーです。これだけが言いたかった！

＞ｓｗｅｅｐｅｒ様[３１３１][３２１６]
お久し振りです。ご無沙汰してました。やはり、まだ「こもれ陽の下で・・・」最終巻来てないんですね。来ててもおかしくないんですが・・・。でも、私（Ｐａｒｒｏｔ）！？をお買い上げになったそうですね（笑）買って損は無かったでしょう？所で、過去ログを見て思ったんですが、失礼ですが女性の方なんですか？間違ってたら申し訳ありません。

＞Ｒｅｉｎｅ　ｄｅ　Ｓｈｉｎｊｕｋｕ様[３１７６]
初めまして。古本屋巡りしていた者です。私の住んでいるここ大分には「まんだらけ」が無いんです。見た事がありません。九州全域にあるかどうか・・・。

＞無言のパンダ様[３２５５]
初めまして。Ｐａｒｒｏｔと申します。宜しく御願いします。ここの常連の無言のパンダ様にやっとお近付きになれました。もう遅いかもしれませんが、「緊急生中継！？凶悪犯冴羽リョウの最期」に登場する北条先生らしき人はご本人（声もご本人）だったと思います。何かに書いてあったような・・・。確信は持てませんが・・・。すいません。役立たずで・・・。

＞玉井真矢様[３２９０]、三毛猫様[３２８５]、しゃる様[３３４０]
皆様初めまして。その「Ｐａｒｒｏｔ」からＨＮを戴いているＰａｒｒｏｔです。宜しく御願いします。私も、ＣＤ－ＲＯＭ版が欲しい！！でも、相当難しいようですね。手に入れるの。けど、見たい！どうすればいいんだ～。

他にも色々とカキコしたいんですが、この辺にしときます。それにしても、ここって女性の方が殆どを占めてません？私達、野郎共が少なく感じますが・・・。どうでしょう？
話変わりますが、いつの間にか新しい方が相当増えましたね。しかも１０代という若人達が・・・。若いファンが居るという事はなんか嬉しいもんですね。私も若いですが、２０歳を超えると駄目ですよ。ほんと。では新しい方々、どうぞ宜しく御願いします。又、お馴染みの方々やここを読んで下さってる方々も改めて宜しく御願いします。それでは、落ちます。
※乱文、御了承下さい。
