https://web.archive.org/web/20020625011445/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=92

### [1840] Goo Goo Baby	2001.07.30 MON 16:56:26　
	
法水さんって、海外の情報詳しいですよねぇ
もっと教えてくださいよ（←ずうずうしいぞオマエ！）

あたしも海外のヤ○ーで検索してみました
でも学校でしかできない！家でやるとネット代がぁ！（大汗）
おっと、これは私信だったかな

海外の個人サイトさん、結構凝った作りになってますよね
「コレ実は海外版の公式ＨＰだろ？」ってカンジのが結構あるある！

日本のファンが知らなそうな画像も結構ある！　それも「ドキッ」っとするようなのが多くてパラダイスよ～ん！！

あたしが［１８３６］で書いた冴子の名前、Ａｇｅｎｔｅってのは、
多分英語でＡｇｅｎｔかな　刑事だからエージェント？みたいな。
そしてＳｅｌｅｎｅかＳｅｌｅｎａとは・・・・
まさかギリシャ神話の月の女神セレナとかいうんじゃ・・・・

エージェント・セレナ？　でもなんかカッコイイじゃ～ん

へぇ～、英語版のリョウはｊｏｅなんですか・・・・
ジョーって読むんですね　

立て！立つんだジョー！！！（ちなみにココ笑うトコだから）

さっき英語版の香が書いてありませんでしたが香は何ていうんですか？　なんかずうずうしくてごめんなさい（汗）

我在[1836]中写的冴子的名字Agente，
英语大概是Agent，刑警所以是代理人?好像。
至于Selene或Selena……
难道是希腊神话中的月亮女神塞丽娜?

### [1839] kaori	2001.07.30 MON 16:12:49　
	
In case you were wondering: there are 2 versions of CH anime in Italy. The first one, censored for tv, has different names: Ryo is Hunter, Kaori is Kreta, Maki is Jeff, Saeko is Selene, Umi is just Falcon. In the VHS uncensored version, the names are absolutely the same as in the Japanese version.

如果你想知道：在意大利有2个版本的CH动画。第一个版本，经过电视审查，有不同的名字： Ryo是Hunter，Kaori是Kreta，Maki是Jeff，Saeko是Selene，Umi只是Falcon。在未经审查的VHS版本中，这些名字与日本版本完全相同。

### [1838] AIKI	2001.07.30 MON 15:08:41　
	
北条先生の美学というか、製作現場を披露しないというのは
ある意味、ファンサービスを拒否してるという部分も見えるかもしれません。

でも、僕としては
北条先生は 余裕をもって作品を提供している姿を嘘でも見せていてほしいす。
毎回ギリギリの線で保たれてるであろう週刊掲載も
先生の努力も苦労も、僕らは作品を見るだけでは分からない。
だから、僕らは知らないまま先生の作品を待つだけでいいのでは？
下手に『大変ですね～』的な言葉を掛けるのも
プロの先生に対して失礼だと思いますし。
作り出す事、それを維持する事は本当に苦労すると思います。
ましてや、これだけ多くの人間を満足させる事など神業だと思います。

だから、先生は読者側には苦労を一片も見せてはならないのだと。思います。
もちろん、笑いを取るとか ボケ的なネタとして取り上げられるのは
凄く楽しいものだとは思いますよ。

個人的には、バンチ廃刊まで掲載続けて欲しいくらいですが
某ジャンプ漫画のように ダラダラ続かず、でもいきなり終わらない
ちゃんと完結する作品であって欲しいと願います。

皆さんと同じように、
ＡＨが、過去の作品を超える傑作に仕上がることを期待しています。

### [1837] 法水(Norimizu)	2001.07.30 MON 14:31:29　
	
［世界に広げよう“もっこり”の輪！］
まみころさん、任命ありがとうございます(^^)。
さてもっこり研究会・香港支部のShaniceさん（勝手に任命してる。笑）の報告 [1829]によれば、「もっこり」はコミックスでは"來一次"(let have once)、アニメでは"開心一下"(happy time)となっているとのこと。コミックスの方は「もっこり」より「イッパツ」に近いような感じですね。でもアニメのhappy timeなんてなかなかイイじゃないですか。よく見ると「下心」が入ってる（笑）。ちなみに香港ではＣＨが成人向に指定されているようです。確か韓国でもそうだったような…。

個人的にも更に調べてみましたが、"mokkori"は確実に海外のＣＨファンのみならずアニメファンの間で浸透しています（好きな決め台詞の１位にしてあるところもあった）。ただアニメの英語吹替版では"nookie"となっているとのこと。"nookie"も「もっこり」よりは「イッパツ」に近い意味のようです。ちょっとニュアンス違うんですけどね。コミックスでどうなっているかは未だ不明ですm(_ _)m

そうそう、 [1836]でGoo Goo Babyさんがイタリア版について書かれてましたけど、英語吹替版ではリョウはJoeになっているそうです。更にドイツではリョウがNickyで香はLaura、韓国ではリョウがウ・スハンで香がサウリになってました。やはり日本人の名前って発音しにくいんでしょうね。

>Shanice san [1829]
Thank you for your interesting report. I write about it above in Japanese. I'm fond of "開心一下" (happy time) very much(^^).

長々と失礼致しました～。

[把 "mokkori "的圈子扩大到全世界！] 。  
谢谢你的任命，Mamikoro-san（^^）。  
现在，根据Mokkori研究小组香港分部的Shanice-san（我擅自任命她......笑）的报告[1829]，"Mokkori "在漫画中是"來一次"(let have once)，在动画中是"開心一下"(happy time)。 在漫画中，它更像是 "イッパツ"而不是 "mokkori"。 但是动画中的happy time非常好，不是吗？ 如果你仔细看，你可以看到其中的 "别有用心"（笑）。 顺便说一下，在香港，CH似乎是为成年人专用的。 我想在韩国也是如此...  
我个人做了进一步的研究，发现 "mokkori "不仅在国外的CH迷中绝对流行，而且在动漫迷中也很流行（在一些地方甚至被列为最喜欢的台词之首）。 然而，在动画的英文配音版中，它是 "nookie"。"nookie"的意思似乎也比 "mokkori "更接近于 "イッパツ"。 不过，细微的差别是有的。 目前还不清楚它在漫画中是如何使用的。 m(_ _)m  

对了对了，[1836]中Goo Goo Baby是关于意大利版的，不过，英语配音版的獠好像是Joe。而且在德国的香是Nicky，香Laura，在韩国的香料是Woo Suhan，香是Sauli。果然日本人的名字很难发音啊。

### [1836] Goo Goo Baby	2001.07.30 MON 12:49:06　
	
イタリア版の名前はこんなカンジらしい

香→　Ｋｒｅｔａ　Ｍａｎｃｉｎｅｌｌｉ

アニキ→　Ｊｅｆｆ

冴子→　Ｓｅｌｅｎｅ　か　Ｓｅｌｅｎａ，Ａｇｅｎｔｅ　Ｎｏｒａ

らしいのです　どうやら。　リョウハはＨｕｎｔｅｒっぽい

意大利版的名字好像是这样的  
香→Kreta Mancinelli  
哥哥→Jeff  
冴子→Selene或Selena, Agente Nora  
好像是。獠很像Hunter  

### [1835] まみころ	2001.07.30 MON 11:46:44　
	
みみごん様［1803］の心臓移植の本「記憶する心臓」ですか？
スッゴク興味持ちました｡今回心臓移植が元なので、見てみようかな｡物語の参考になるかも？

法水様［1813］自分で様と付けておいて何なんですが、私に様はやめて下さい｡（笑）何だかこそばい様な気恥かしい様な･･･｡
もっこりと叫んでるというコメント、本当に笑わせてもらいました｡そうですよね、本当にその通りですよね｡
でもそう考えると、恥ずかしくて言えなくなりそうですね｡（笑）
これでもう法水様も「もっこり研究会の一員ですね！（笑）」

ちなみに昨日選挙に行った時に、ふと考えた事が｡
リョウはもちろん戸籍がないのだからハガキが送られてくる事はないけど、海坊主はどうなんだろう？
一応「伊集院　隼人」という立派な名前があるからには、戸籍ぐらいはあるのかな？
という事は、あのでかい図体で選挙に行って小さな囲いにぎゅうぎゅうになりながらせっせと名前を書く･･･｡
イヤ、もちろん行くはずはないけど、行ったとしたら･･･と考えたらちょっと笑ってしまった｡
くだらないお話で、どうもすみません｡（笑）

顺便说一下，昨天去选举的时候，突然想到了一件事。
獠当然没有户籍，所以不会寄明信片来，但海坊主呢?
既然有“伊集院隼人”这个响亮的名字，至少应该有户籍吧?
也就是说，那个大块头去选举一边挤在小围栏里一边拼命写名字的···。
不，当然不可能去，如果去了……想到这里，我不禁笑了。
说了些无聊的话，实在对不起。(笑)

### [1834] タウンハンター(^^ゞ	2001.07.30 MON 10:22:51　
	
 [総集編の大きさについて]
なんとなく最新情報の写真だけを見ると
バンチワールドサイズっぽくないスか？
実際見てみなきゃ分からないけれども、
総集編と言ったら、講○社でよく出すような
あのA４サイズのを想像したのですが・・どうでしょうね？
私はコミックス派なので総集編は立ち読みで済ますつもり（いいのかな？m(__)m）

 [・・も、もっこりについて(^^ゞ]
しかしここの【もっこり議論】は何と表現したらよいのか
わからないくらいみなさん興味津々のようで（汗）
特に女性側からの視点で語られると我々男性陣にとっては
何ともしがたいかぎり・・・(^^ゞ
こりゃもうリョウの影響大！ですな・・・
みなさん１０年以上前から影響を受けていらっしゃるようで
もう恥ずかしくないくらい【もっこり】は共通語になったのねん
・・・

目指すは世界共通語ですな！
・・まったく久々にここにきたと思ったら
長々とくだらない事を（汗）・・・<m(__)m>

### [1833] 天使の心＠（Angel's　Heart）	2001.07.30 MON 03:05:35　
	
こんばんは。久しぶりに来たら英語がたくさん！

《掲示板問題》
なんか喧嘩売られても買ってもいませんし、もうこれ以上は
何を書いても・・・なので書きません。

《レス・ちょっと私信込み》
今回私個人宛にメールを送ってくれた、たくさんの方々！
どうもありがとうございました。私は元気ですので
ご心配なく！皆さんの心遣いには感謝しておりますので、
皆様もここに帰ってきてくださいね。私はここで
待ってます。

＞ありあり様
　憶えておりますよ！これからもよろしくです

《海外の皆様・・・あってるのかなこの英語・・・》
Hi! Overseas you I am the fan of Mr. Hojo who lives in Japan. The place in which my handle name is "Angel's Heart" and in which I live well is a place near Shinjuku which is also the stage of AH and CH. It goes also to Shinjuku occasionally. Although English is not its favorite, your writing is read enduringly! Since an English message is also written in occasionally, you also need to write. Moreover, let's meet you.

### [1832] James	2001.07.30 MON 02:42:43　
	
北条氏のコメントに対して。
ちょっと､勘違いしている部分があるような･･･
いいたいことは非常に良くわかるんですが。
メイキングでは苦労話も多いでしょうが､
創作現場への取材は北条司という漫画家がどのような環境で
漫画を書いているか､それを伝えるためにあるのでは？
ファンとしては見てみたいでしょう。
より漫画を好きになる為にも､より漫画家を好きになる為にも。
それに別に創作現場を見たからといって苦労が伝わるか
といったら､全然そうでもないでしょうし。

ファンサービスも、プロなら当然やるべきでしょうね。

### [1831] Beer	2001.07.30 MON 01:37:14　
	
わあ、ごめんなさいm(__)m　同じメッセージを二度送信して
しまいました。管理人の方、ご面倒ですが削除をお願いします。

Very sorry! I've mistaken to send a same message two times. Please delete the second.>manager

### [1830] Beer	2001.07.30 MON 01:33:40　
	
Hi, everyone.
It's very glad to read messages in English, as Mr.Norimizu said, don't worry. :)

By the way, can overseas-fan read BUNCH No.11 in last Tuesday or Wednesday? Was it translated already in English ?

To my surprise, in Japan special issue of A.H.(summary of No.1-9) will be released 2.August. It's so early, isn't it? As Coamix's comic A.H. will be released in Autumn too... :(

マンガ家やマンガのことは、ドイツ語でもMangakaやMangaと言いますよ。Comicとも言いますけれどね。Mangafanなる言葉もあります。

### [1829] shanice	2001.07.30 MON 00:55:13　
	
Hi all,
Thanks for the kind encouragment!
Actually I am very impressed for the help of the Japanese fans who are so kind enough to translate AH for foreign fans. Thks a lot !! T_T

For the meaning of "Mokkori", as the story of CH somehow involved a little bit sex and violence, so it is not allowed to be distributed to a person under 18 (age for adult) in Hong Kong. (But, of course, it's just a warning). Therefore, the meaning of "mokkori" is translated conservatively as " 來一次" (let have once) in comic books and " 開心一下" (happy time) in TV anime.--pls kindly correct if any!!
However, there may be some different interpretation in other Chinese edition.

Anyway, nice to see bbs becomes internationalised!

### [1828] Heather	2001.07.30 MON 00:44:47　
	
Hi,

I'm from Hong Kong. It's really nice to see English messages here cause my Japanese is really really poor. m( )m. Currently, I'm asking my friend in Japan to mail each issue of "Bunch" to me every week and I'll try my best to try to understand the dialogues like checking up dictionaries and asking friends who know Japanese well...

### [1827] メタル男	2001.07.30 MON 00:20:34　
	
はじめまして、メタル男です。男とは限らないけどね。
チョット気になったんですが今度出るA・Hの総集編のものとは別に単行本って出るんですかね？

### [1826] Beer	2001.07.29 SUN 23:45:59　
	
Hi, everyone.
It's very glad to read messages in English, as Mr.Norimizu said, don't worry. :)

By the way, can overseas-fan read BUNCH No.11 in last Tuesday or Wednesday? Was it translated already in English ?

To my surprise, in Japan special issue of A.H.(summary of No.1-9) will be released 2.August. It's so early, isn't it? As Coamix's comic A.H. will be released in Autumn too... :(

マンガ家やマンガのことは、ドイツ語でもMangakaやMangaと言いますよ。Comicとも言いますけれどね。Mangafanなる言葉もあります。

### [1825] 夏兎	2001.07.29 SUN 23:14:40　
	
北条先生のコメントを読んで…

メイキング…　確かに苦労話とかのオンパレードな気がします。
でも、それは当然の事で、私も職業柄わかるのですが、それを見せたくないという、プロの心情もわかる気がします。
でも、どんなに苦労していても、面白い物は面白いし、ダメなものはダメと…
それを決めるのって、原作者でもなく、スタッフでもなく、その作品を見たり、聞いたりして体験したリスナーの物だと思います。
リスナーって、ウラ話や苦労話を聞きたいのではなく、どれだけいい作品を手に出来るかだと思うのです。。。

私は、北条先生がどんなに苦労しているかとか、そういうの実際に見てるわけではないからわかりませんし、それを言われて同情に近い気持ちでＡＨを読んだりはしません。。。

ようは、面白ければ読み続けるし、つまらなければ読まなくなると…　結果はそういう事ですよね？

私はフリーのライターをしていますが、漫画家さんとは全く違う仕事かもしれないですが、仕事を終えるまでの苦労とか、大変さは同じような気がします。。。

今回の、先生のメッセージを読んでこの人はやっぱり本物なのかもしれないなぁと、呆然と感じました。。。
ただ、ＡＨが今後どう進んでいくのか？
私はそれを見たいと思います。はっきり言って今の展開を読んで
いて、少し物足りません…

もともとのリョウと香の位置が違ってしまってる事への違和感が捨てきれていないだけかもしれないのですが、少し変わった方向へ向かってしまいそうで不安にもなっています。

ここのＢＢＳは割合、好印象な方が多いのですが、他のサイトでは本当に賛否両論です。。。
今の状態ではどちらとも言い切れないですし、ただ、ひたすら月曜日の深夜を待ち続けるしかないのですが。。。

北条先生、良い作品書き続けていってくださいね。
今はそれを、心から待ち望んでいる次第です。。。

### [1824] 優希	2001.07.29 SUN 23:13:16　
	
ジッポーってガス入ってないんですね。考えればわかるのに焦りました。
amuさんお久しぶり。amuさんは台湾の方でLAに留学していらっしゃいます。（そうだったよね？）
前にテレビは日本語でやっていたと聞いた記憶が・・・違ったかな？本は・・・どうだったっけ？(^-^;)

### [1823] テムリン	2001.07.29 SUN 22:38:40　
	
### [1812] sweeper様、1792] Goo Goo Baby様。
覚えていてくれたんですか～！！
うれしい！！（感涙）
ありがとうございます。
これからも来ますので、よろしくお願いしますね～！
もちろん初めての方もよろしくね♪

＞先生のコメント
う～ん。確かに先生のプロ意識はすごいですね！！
かっこいいと素直に思ってしまいました。
ただ、一読者としては「裏側も知りたい」という気持ちが少しありますね。
自分が読んでておもしろい作品なんかは特に。
その作品の「ウリ」にするのはどうかと思うけども、
好きなものに関しては全部知りたくなるのがファンの心ではないでしょうかね？
実際、この前書いてた「時間がない」ってコメントも、読んでてなんかリアルでうれしかったです。

＞絵の変化について
今更だけど前にカキコがあったんで・・。
私は今のリアルリョウ（笑）好きです。
確かにＣＨ連載中の時と比べれば違った顔に見えますけど、
もともと私は先生の絵のリアルさが好きだったもので・・。
動揺しているリョウの顔、たまらなく切なかったなぁ。
ホントに今更ですみません(>_<)

では今日はこのへんで♪


### [1822] 法水(Norimizu)	2001.07.29 SUN 21:52:14　
	
>amu san### [1816]
Thanks for your reply. Please don't hesitate to write message in English and tell us your opinions. Anyway, "Mokkori" is translated by meaning? If you can, would you write in kanji (Chinese letter)?

「もっこり」は意味通り訳されているんですか？　できたら漢字で書いていただけませんか？
（注：amuさんは中国語圏の方のようです）

### [1821] よしき	2001.07.29 SUN 21:50:11　
	
＞Goo Goo Baby様
会員番号は自動的に出てもパスまでは出てこないのです。でも保存できちゃうのならこれから試みてみようと思います♪
教えてくださってありがとうございました！

＞先生のコメント
見ました。これは私も同感。言って良いのか危ういんですが、「千と千尋の神隠し」の公会前日に某局でドキュメンタリーやってたんですが、私はそれを知ってても見ませんでした。でもコンビニのパンフは手に取りました。やっぱ画がかわいいから。そしてまだ見れてない…。（忙）
苦労話っておもわず言いたくなるのが人のサガですけど、やっぱり仕事と私情の区別って大変だなぁと思いました。
