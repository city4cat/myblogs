https://web.archive.org/web/20020422035815/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=27

### [540] hiromi(北海道、港町）	2001.07.11 WED 01:03:44　
	
またまたおじゃましまーす！こちらもバンチ水曜発売！！
発売前日は落ち着かないんです。明日も仕事なのに眠れそうにないかな・・・　火曜、水曜の夜はドキドキしちゃいますね。
ああ、気になる！！

ミニクーパーの話題がでておりましたので、私も参加させてください！　先月車を変えました。残念ながらミニではありませんが・・・　本当は、赤のミニクーパーに乗りたかったんですが、諦めたんです。外車ですので故障の問題とか、北海道の冬道の不安とか・・・（乗ってる人は結構みかけますが）
一度でいいんで、運転してみたいですね♪

又来打扰啦!这里也是星期三发售Bunch! !
发售前一天心里不踏实。明天也要工作却好像睡不着···周二、周三的晚上很紧张呢。
啊，好在意! !

因为提到了MiniCooper的话题，请让我也参与!我上个月换了车。很遗憾不是Mini……其实我想开红色的MiniCooper，但是放弃了。因为它是外国车，我担心北海道的故障和冬季道路···(虽然我看到很多人都在开)
一次也好，我想开♪

### [539] black glasses	2001.07.11 WED 00:51:23　
	
こんばんは
FOOTSTEPの話が出てましたが僕もあの曲大好きですよ。それで僕は車を運転中にあの曲が流れると危険な運転になるので運転中は控えて他の曲を聞いています。

それと前に夢の話がでていたので、ふと思い出したんですがずいぶん前に僕はある夢をみたんです。その夢とはたくさんの女性（それもみんな美人）に追いかけられる夢だったんですが男としてはうれしい夢のはずなのになぜか僕はその人たちから一生懸命に逃げてるんですよ。その理由がその人たちはオカマなんです。あれはもう悪夢に近いものでした。なんでそんな夢をみたかというと当時まだＦＣが連載されててハマってたんです。そしてさらに昔にはＣＥやＣＨに関連した夢もみてるんです。そう考えるともしＡＨにハマってしまった場合、どんな夢をみるんだろう？とかんがえてしまいます。もしかしたら僕が心臓移植を受ける患者で香の心臓が移植される夢だったりして…。もしそうなったら絶対リョウに心臓エグりとられるんだろうな（笑）

晚上好  
提到了FOOTSTEP，我也很喜欢那首曲子。所以我开车的时候如果播放那首曲子，就会变成危险驾驶，所以开车的时候就会控制自己听其他的曲子。

因为之前提到了梦，所以突然想起来很久以前我做了一个梦。那个梦是被很多女性(而且都是美女)追逐的梦，作为男人应该是高兴的梦，但不知为何我拼命地逃避那些人。理由就是那些人都是人妖。那简直就像一场噩梦。为什么会做那样的梦呢?当时FC上还在连载，我很着迷。而且更早以前还做过CE和CH相关的梦。这样想的话，如果迷上AH的话，会做什么样的梦呢?这样想。说不定我是接受心脏移植的患者，梦见香的心脏被移植了……。如果是这样，一定会被獠挖走心脏里的。(笑)

### [538] 金田　朗(福岡）	2001.07.11 WED 00:44:04　
	
いよいよ明日BUNCHの発売（福岡は水曜日）待ち遠しいです。

＞のんちさま
一条先生の『有閑倶楽部』は全巻持っているのに今まで気が付きませんでした。確かに！すごいですね。皆さんの観察力。

＞紫苑♪さま、orphanwolfさま
ＣＭ情報有り難うございます。やっと、思い出しました。歳だから記憶が遠のくのも早いみたいです。

明日は通勤途中にBUNCHを買って、職場の更衣室で読む予定。
明日のBBSが楽しみです。

终于期待明天BUNCH的发售(福冈星期三)。

＞のんちさま
一条老师的《有闲俱乐部》全卷我都有，却一直没有发现。确实!好厉害啊。大家的观察力。

＞紫苑♪さま、orphanwolfさま
感谢您的CM信息。我终于想起来了。我已经老了，看来我的记忆力正在迅速消退。

明天打算在上班途中买BUNCH，在公司的更衣室里读。
期待明天的BBS。


### [537] ゆなつ	2001.07.11 WED 00:39:27　
	
しつこく、ゆなつ登場しましてすみません。

＞りなんちょ様
　情報ありがとうございます、、、終わってたのね。（ToT)　ショック！
　創刊号でアクセスしようとして、そのソフトが無かったから。
　っち！見逃してしまった！くやしい！聞きたかったな、、、

＞のりのり様
　うらやましい！マイカーだなんて！友達もＣＨの影響（だと勝手に私が思ってる）で緑のクーパー持っているんですが、これがまたハンドル操作が難しい。硬いんですよね？ステアリングが。なんかパワステになれてる私が買って果たして乗りこなせるかってのが疑問ですよ。

不好意思，ゆなつ执意登场。

＞りなんちょ様
谢谢你的信息，已经结束了。(ToT)震惊!
想用创刊号登陆，却没有那个软件。
嘁!错过了!不甘心!真想听啊、、、

＞のりのり様  
好羡慕!竟然是私家车!朋友也受CH的影响(我想是这样)有一辆绿色的Cooper，但它也很难转向。这很难，不是吗？ 方向盘很硬。我已经习惯了动力转向，但我不确定如果我买了，我是否能够驾驶它。

### [536] ゆなつ	2001.07.11 WED 00:31:35　
	
＞Orphan Wolf様
　きゃっ！さっきの書き込みでFootstepを違う曲と勘違いしてました！インストのこの間好きな曲としてここに書き込んだ曲を思い浮かべてしまった。

Footstep in the night
Echo in my mind
Footstep in the night
Baby said goodbye
　To　Me

CityHunter Original Animation Soundtrack Vol.2より抜粋

ですよね。。。
すみません、この曲で創作ダンスは無理だろ、、、って突っ込みがきそう、、、
間違えました。。。（－ｖ－）ｐ

＞Orphan Wolf様  
啊!在刚才的留言里把Footstep误以为是不同的曲子!我想到了在这里写的这首歌是我前几天最喜欢的器乐歌曲。

Footstep in the night
Echo in my mind
Footstep in the night
Baby said goodbye
to me

摘自《城市猎人Original Animation Soundtrack Vol.2》

是吧…
对不起，这首歌不能创作舞蹈吧，好像会有这样的吐槽，我搞错了。…(- v-) p

### [535] のりのり	  2001.07.11 WED 00:28:45　
	
みなさんこんばんわ
私の住んでいる高知はバンチの発売水曜です。だからネタバレいやだなと思っていたので皆さんのご協力すごくうれしいです。
＞ゆなつさん
うちの父もわたしがＣＨの影響で「ミニほしい」と言ったとき同じようなことを言って反対しました。しかし、今はなぜか家のマイカーです。（赤ではないけど）

大家晚上好
我住的高知是星期三发售bunch。所以我不想剧透，非常高兴能得到大家的协助。  
＞ゆなつさん  
当我在CH的影响下说"我想要一个Mini"时，我父亲也说了同样的话，并且反对。但是，不知道为什么现在是家里的私家车。(虽然不是红色的)


### [534] 天使の心@TOKYO（隠れ海ちゃんファン）2001.07.11 WED 00:26:28　
	
こんばんは。今日はバンチ持って新宿に行って来ました。
なんかね、突然行ってみたくなってしまったのです。
中央公園で、夕方バンチ読んでました。
新宿で読むAHも良かったですよ。今度はゆっくりと
初回からの分を担いで読んでこようかなって思いました。
その前に、毎週火曜は新宿でAH読んじゃいそう。
中央公園で6時ごろバンチ担いでる変なヤツがいたら
きっとアタシでしょう（爆）

＞black glasses様・UMI様
　私も「伊集院隼人氏の平穏な一日」は大好きです。
　隠れ海ちゃんファン（なぜ隠れる？）としては
　待ちに待った海ちゃん主役！！って話でしたもの。
　特にCM明けの「ファルコン！」って画面がサイコー！！
　マニアにはたまんない（爆）
　それと、海ちゃんがぶっぱなすバズーカは最高です！
　よっしゃー！！って思ってしまう・・・。

ミニクーパー・・・アタシも負けず劣らずやっぱり見つめて
しまいます。CHを読んでた学生の頃そんな高い車とは知らな
くて、免許取ったらミニクーパーがいい！って思ってました。
車の値段初めて知ったときにはびっくり！車の修理っていったい
どうしてるのリョウ？って思っちゃったくらい。いつも財政難
の冴羽商事だったから（爆）
香ちゃんのFIATだって高いけど・・・

晚上好。我今天带着礼物去了新宿。
我突然想去看看。
傍晚在中央公园看了bunch。
在新宿读的AH也很好。这次我想慢慢地把第一集开始的部分扛下来读。
在那之前，每周二在新宿看AH。
如果在中央公园6点左右有扛着Bunch的奇怪的家伙一定是我吧(爆)

＞black glasses様・UMI様  
我也很喜欢《伊集院隼人氏平静的一天》。
隐蔽的海迷(为什么隐藏?)我真的很期待看到他担任主角。
特别是广告结束后的“Falcon !”的画面太棒了! !
狂热者忍不住(爆)
还有，小海放出的火箭筒是最棒的!
太好了！！我会这么想···。

Mini Cooper···我也毫不逊色地盯着看。读CH的学生时代不知道是那么贵的车，我拿到驾照的时候就想要一辆Mini Cooper!  
第一次知道车的价格的时候吓了一跳!我甚至想知道他到底是怎么修车的。总是财政困难的冴羽商事(爆)
香的FIAT也很贵…

### [533] りなんちょ（再び！）	2001.07.11 WED 00:25:27　
	
☆ゆなつさま☆
神谷さんや高山さんのアテレコって、
「聴コミ」のことですか？
それでしたら、数週間ぐらい前に終了しましたよ？
またやってくれると良いな～
な～んて、思ってますけど…

☆ゆなつさま☆    
神谷先生和高山先生的电话，是指「聴漫」吗?
如果是的话，几周前就结束了吧?
还能再做就好了~
我是这么想的…

### [532] りなんちょ（窓から月が…）	2001.07.11 WED 00:20:12　
	
暑いです。
暑くて眠れないので、ここに来てカキコしてます…
あと１７時間半ほどで、ネタバレ解禁ですね。
午後６時は、カキコがすごそう～

冴羽さんの誕生日って、一応３月２６日ですよね?
５年前の事ですが、運転免許を取りに
試験場に行ったのですが、受験番号が３２６番だったんです。
「こりゃ、縁起がいいなぁ～」なんて思っていたら、
受かりました。
やっぱり、縁起が良い数字だったんだなぁ～

☆姫さま☆
あたしも、よく真似て絵を描いてました。
でも、さすがに北条先生の絵は真似できずに、
格好だけ、真似させていただきました。
今でも、何気に描いてます…
ヘタッピだけど…

很热。
因为热得睡不着，所以来这里留言…
还有17个半小时左右，剧透解禁了。
下午6点，发帖数很厉害~

冴羽的生日大概是3月26日吧?
那是5年前的事了，去考驾照的时候，考号是326号。
心想“这可真吉利啊”，结果就考上了。
果然是吉利的数字啊~

☆姫さま☆  
我也经常模仿画画。
但是，我还是无法模仿北条老师的画，只能模仿他的衣服。
即使现在，也不知道为什么在画…
虽然很差劲…


### [531] のんち(お疲れモード)	2001.07.11 WED 00:18:09　
	
こんばんわ。今日お客さんでバンチ持ってる人がいて感激しちゃいました！

＞Goo Goo Baby さま
私が行ったエアロのコンサートは「Nine Lives」が発売されたときだから、４年位前のだったと思います。
オヤジパワー炸裂で素敵でした！
スティーブン・タイラーとリブ･タイラー親子は口元がめっちゃそっくりですよ。

＞紫音♪さま
私もミニクーパー見ると振り返っちゃいます。
ところで一条ゆかり先生の「有閑倶楽部」で魅録が乗ってる車ってミニクーパーなんですけど、そのナンバーってリョウのと一緒じゃないですか？誰か知ってる人いません？
１１巻見るとよくわかるんですけど、当時からずーっと気になってるんですよねー。一条先生もファンなのかしら？

明日の皆さんの感想を楽しみにして寝ます。おやすみなさーい。

晚上好。今天客人中有拿着bunch的人很感动!

＞Goo Goo Baby さま  
我去的Airo演唱会是在“Nine Lives”发售的时候，应该是4年前的事情。
老爸力量炸裂很棒!
Steven Tyler和Liv Tyler父子的嘴长得非常像。

＞紫音♪さま  
我一看到Mini Cooper也会回头看。
对了，在一条由香里老师的“有闲俱乐部”里，魅录乘坐的车是MiniCooper，那个号码和獠的不是一样吗?有认识的人吗?
看了11卷就明白了，当时就一直很在意呢。一条老师也是粉丝吗?

期待着明天大家的感想睡觉。晚安。

### [530] 冬野（名古屋市のどこか）	2001.07.11 WED 00:09:14　
	
＞black glassesさん　紫音さん　おっさんさん 姫さん　ぺぎさん
私もミニには条件反射で振り返りますよ。
とりあえず、高校卒業したらミニをなんとしても買いたいな～と思ってますよ。（笑）
ミニを持ってる人ってうらやまし～。

＞black glassesさん　紫音さん　おっさんさん 姫さん　ぺぎさん  
我也会条件反射地回头看Mini哦。
总之，我想高中毕业后无论如何都要买一辆Mini Car车。(笑)
真羡慕有Mini的人。


### [529] ゆなつ	2001.07.11 WED 00:06:42　	

こんばんわ～！
今朝電車の中でＢＵＮＣＨを読んでる人がいて、おっと！この人もＡＨのファンか？って思わずじっと見てしまった。Ｏ横線を７時頃乗ってて怪しい視線を感じた人がいましたら、犯人は私です(笑）

＞orphan wolf様　
私もFootstep大好きです！！効果音のガラスが割れたりする感じが！しかも、一度高校の体育の授業でこの曲使って創作ダンスをしました。犯人と刑事をテーマに。思いっきり趣味に走ってしまったのね。ははは。。。

＞クーパー見たらＣＨと思え！
Black Glass様を初め、みなさんやはり振り返ってしまいますよね！特に赤だと。一時期欲しくて父上に変え返るならクーパーといったら、家族４人乗れない、おまえが旅行とかに行くとき残るなら別だけどって脅されて悲しかった。でもほしかったのよ。

＞Black　Glass様
　「伊集院隼人の～」私も大好きです！特にアニメのタイトルの登場の仕方が、なんかかわいくて好きだった。

＞優希様
　スクリーンセーバーいいですね！トンボがとんだり、からすが飛んだり、リョウをハンマーで追いかける香とか、冴子を見て飛び掛ろうとするリョウとか、赤くなる海坊主とか、海ちゃんのトラップ全集とか、ひたすら射撃の的を打ち続けるリョウとか、、、想像したら欲しくなった。

解禁日になったらここの書き込みすごい事になりそう、、、
でもみなさんと語るのを楽しみにしてます。

ところで、質問なんですが、BUNCH創刊の時に声優さんたちがアテレコしていたのってまだやってるんですか？神谷さんとか高山みなみさんとかがやってた奴。
クィックタイムを手に入れたのに、BUNCHのホームページ上ではなくなってたので。（TnT）

晚上好~ !
今天早上在电车里有人读BUNCH，啊!这个人也是AH的粉丝吗?不由自主地盯着看。7点左右乘坐O线，如果有人觉得视线可疑，那就是我。(笑)

＞orphan wolf様　
我也很喜欢Footstep ! !效果音的玻璃破碎的感觉!而且，有一次在高中的体育课上用这个曲子创作了舞蹈。以犯人和刑警为主题。完全变成了兴趣爱好。哈哈哈…

＞看了Cooper就想到CH !  
以Black Glass为首，大家还是会回头看的吧!特别是红色。有一段时间我很想买，父亲说如果要换的话就买Cooper，结果他威胁我说一家四口不能坐，你去旅行的时候留下来的话就另当别论了，这让我很伤心。但是我想要。

＞Black　Glass様  
“伊集院隼人的~”我也很喜欢!特别是动画名字的登场方式，让人觉得很可爱，很喜欢。

＞優希様  
屏幕保护程序好啊!蜻蜓飞来飞去、乌鸦飞来飞去、用锤子追着獠的香啦、看到冴子就想扑过去的獠啦、脸红的海怪啦、小海的陷阱全集啦、一个劲儿地不停射击的獠啦，。想象一下就想要了。

解禁日的话这里的留言会变成很厉害的事，，，
但是我很期待和大家交谈。

对了，我想问一个问题，BUNCH创刊的时候声优们做的配音现在还在做吗?神谷和高山南做过的那个。
虽然有了quick time（译注：音视频播放器），但是它在BUNCH的主页上却没有了。(TnT)


### [528] 姫 (４９年早生まれ）	2001.07.11 WED 00:03:53　
	
もう寝よう寝ようと思いつつ、まだこうやってHPを開いてる姫でございます。

＞ginteiさん  
こちらこそ申し訳ないです。削除されてしまったレスを明日また書き込んで下さいね！
誤り合戦になりそうですね（笑）
私ももっと書き込みの言葉を気を付けます。

明日は沢山書き込みがすごそうですねー。読むのが大変そう。明日も夜はパソコンから離れなれなそうですねー！

我是一边想着要睡了要睡了，一边还这样开着HP的姫。

＞ginteiさん  
我也很抱歉。 请你明天再写，并附上被删除的回复!
这是一场错误战斗(笑)
我也会更加注意留言的语言。

明天会有很多留言很厉害呢。读起来好像很费劲。听说明天晚上也离不开电脑呢!


### [527] あずみちゃん	2001.07.11 WED 00:01:22　
	
こんばんは。毎日暑いですね。大阪はほんとに暑い～！

*紫音♪さま*
1919チェックうけてくれて嬉しいです�それにしても原作でもアニメでも海ちゃんがミニのドアをひっぱがすシーンには笑いがとまらないですよ。あれ修理するのに結構お金かかるんですよね。だからいつもどうやって直してたんだろうかと
今でも悩んでます。

*Goo Goo Babyさま*
こんばんは。私もヴィトン大好きです。でも今はエピ派なんですけど...。でもVLのロゴをCHにしたりCAにしたりと言うのはおもしろいと思いますよ�　いっその事オーダーしてみます？ダメかなぁ。^^；半年またさるし、金額も破格だろうね～。でもやってみたいチャレンジ精神がみなぎるわ！

晚上好。每天都很热呢。大阪真的很热~ !

*紫音♪さま*  
很高兴能接受1919的检查�不过不管是原作还是动画，看到小海拽迷你车门的场景都让人笑到停不下来。修理那个要花很多钱吧。所以我现在也很烦恼，不知道平时是怎么修的。

*Goo Goo Babyさま*  
晚上好。我也很喜欢lv。但是现在是Epi派…。但是我觉得把VL的标志改成CH或者CA很有趣哦~干脆订个?不行吗?^ ^;我必须再等半年，而且价格会很贵。但我想尝试一下，我愿意接受挑战!

### [526] ぺぎ	2001.07.10 TUE 23:58:28　
	
>black glassesさん　紫音さん　おっさんさん 姫さま
確かに、ミニには条件反射的に振り返ってしまいます(^ ^;）。しかも、運転席なんぞも確認した挙句「やっぱりミニはリョウが一番よ!!」と呟いたりなんかしてしまいます。

>キャサリンさま
「りょうみたいな人！！」
学生の頃を思い出してしまいましたわ。私も似たような発言を友人に繰り返しておりました(笑)
結婚して思うことは、多かれ少なかれどんな男性にも（女性にもかな??）リョウ的な部分があるんじゃないかなぁということ。そういった部分でリョウに似た人と出会えたら幸せなのかも･･･
が、先は長いです頑張って見つけて下さいね。見つかったら教えて下さいませ、相方捨てて乗り換えます!!（爆）

AHが始って以来、すっかりCH+AH漬けになり、相方にあきれられていますが、このまま突っ走ります!!
明日の解禁が待ち遠しいです!!

ではおやすみなさい

>black glassesさん　紫音さん　おっさんさん 姫さま  
确实，对Mini会条件反射地回头看(^ ^;)。而且，确认驾驶席后嘟囔着“獠的才是最好的Mini!!”。（译注：待校对）

>キャサリンさま  
“像獠一样的人! !”
这让我想起了学生时代。我也对我的朋友说过类似的话。(笑)
现在我已经结婚了，我认为每个男人(女性也是吗??)或多或少都像獠。如果能遇到在这些方面和獠相似的人的话，也许会很幸福……
但是，未来还很长，请努力去发现。如果找到了请告诉我，我会离开我的配偶，换人!!(爆)

AH开始以来，完全沉溺于CH+AH，配偶很吃惊，但我要继续下去!!
期待明天的解禁!!

好了，晚安


### [525] そにあ（湘南の端から）2001.07.10 TUE 23:43:58　
	
こんばんは。２回目です
みなさん、すごいたくさんの方が掲示板に書き込んでいるんですね。
すごい！すごい！
それだけ、北条先生ファンがたくさんいるということですね。
今日も昼休みにコンビニに行くと、
バンチのおいてある前にたくさんの人がいて、
みんなバンチを手にしていました。
「コレコレ！立ち読みせず、バンチ買え！」
といいそうになりましたが、言えませんでした。

最新号の話題はまたあした、カキコします。
では、また。
明日も暑いかね？もう、暑さにぐったり疲れる。

晚上好。这是第二次。
大家，有很多人在留言板上留言呢。
好厉害!好厉害!
这说明北条老师有很多粉丝。
今天午休的时候去便利店的时候，看到放着bunch的前面有很多人，大家都拿着bunch。
“这个这个!别站着看了，买bunch吧!”我想说，但没说出口。

最新一期的话题明天再写。
那么，再见。
明天也很热吗?已经热得筋疲力尽了。


### [524] ｓａｋｉ	  2001.07.10 TUE 23:39:22　
	
こんばんは、初カキコです！！
「CITY　HUNTER」は幼稚園のときから大好きで、夢中で
全巻集めました（ちなみに私は16です・・）コミックバンチで
連載が始まってから、毎週火曜が楽しみでしょうがないです！！
北条先生、これからも頑張って下さい!!＆新参者ですが、
これからよろしくお願いします！！

晚上好，初次见面! !
《CITY HUNTER》从幼儿园开始就很喜欢，痴迷地收集了全卷(顺便说一下，我16岁了··)在ComicBunch开始连载之后，每周二都非常期待! !
北条老师，今后也请加油!!我是新人，今后还请多多关照! !


### [523] gintei  2001.07.10 TUE 23:38:45　
	
…す、すみません。ヤッタつもりは無いんですが
間違えて2度送ってしまったみたいです(滝汗)
解りづらくさせてしまい、本当に申し訳ありませんでした。

＞姫さま
こちらこそお気を遣わせてしまい、申し訳ありませんでした。
姫さまの発言にひっかっかった訳ではないのです。
…ただ、削除された発言が余りに正当なものだったので
アラシやネタバレ的書き込みだったと思われるのが悲しかっただけ･･…と言う理由からレスさせて頂きました。
こちらこそ、お気を悪くさせてしまい申し訳ありませんでした。

我......很抱歉。 我不是有意这样做的，但好像错发了两次。(瀑布汗)
让人费解，真的很抱歉。

＞姫さま  
让您费心了，真是不好意思。
并不是被姫的发言激怒。
……但被删除的留言是如此的正当，却被认为是Arashi或Spoilers（剧透）的帖子，我只是为此感到难过。
我才不好意思，让您不高兴了。（译注：待校对）

### [522] gintei	2001.07.10 TUE 23:24:47　
	
こんばんわ。始めまして…
いつもAH、楽しみに読ませて頂いております（^^）
本日は一言ご挨拶したく書き込ませて頂きました。
これからも北条先生の作品を楽しみにいたしております。

＞姫さま
初めまして。ginteiと申します。
…削除されている書き込みはルール守ってない訳じゃないですよ。（これも削除かな？）
ただタンに正直だっただけで…

晚上好。开始了…
我一直期待着阅读AH。(^^)
今天写了想说一句问候的话。
今后也期待着北条老师的作品。

＞姫さま  
初次见面。我叫gintei。
……被删除的帖子并不是不遵守规则。(这个也要删除吗?)
只是单纯的诚实而已…


### [521] 姫 (私も香と同い年）	2001.07.10 TUE 23:21:59　
	
どうもです。本日２度目のカキコです。

>ginteiさん  
初めまして！
なんか私の言葉が引っかかってしまったらごめんなさい。
私も今日バンチ読んでるんです。この気持ちをどうしようか・・・と考えたんですけど我慢して明日爆発させようと思ってます。マジでBBSの誰かに個人的にメールしようか！！とも考えたくらいです（笑）流石に迷惑だなーと思って思い留めました。

>black glassesさん　紫音さん　おっさんさん  
ミニはきっとみんな条件反射で振り返りますよ（笑）
パブロフの犬って奴ですね。
私もミニは乗りたかったです。今じゃ、商用車を自家用車にしてます。うちの会社の社長がミニもってるんですよー。「だから商用車にそのミニくれっ！！」って言ったら怒られました。けち！

谢谢。这是今天的第二次留言。

>ginteiさん     
初次见面!
如果我的话令你困扰了，对不起。（译注：待校对）
我今天也在看Bunch。这种心情该怎么办呢……虽然考虑了，但还是忍耐着明天爆发吧。真的要给BBS的某人发个人邮件吗! !我甚至想过(笑)实在是太麻烦了，所以就打消了这个念头。

>black glassesさん　紫音さん　おっさんさん    
遇到Mini一定会条件反射地回头看(笑)
是巴甫洛夫的狗吧?
我也想坐Mini Car。现在，我把商务车变成了私家车。我们公司的社长有一辆。我说“所以就给我那辆商务车! !”，结果被骂了。小气!（译注：待校对）
