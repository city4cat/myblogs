https://web.archive.org/web/20050313021048/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=449


### [8980] 千葉のOBA3	2002.12.09 MON 08:58:15　
	
おはようございますぅーーーうそ！なんで雪が降ってるのー！？しかも積もってるーーー！！関東の平野部で１２月に雪が積もるなんて・・・。めったにないことです！おかげで今朝は、ダンナと子供達送りだすまでバタバタでした。（汗）

ちょち、もう一度今週号の感想です。（っていうか、思ったコト）☆よくよく考えてみたら（考えなくても）成田空港って千葉県じゃーーん。りょうちゃんようこそ千葉県へ！・・・。しかしＣＨの頃から成田はよくでてたので、東関道通る時なんて（めったにないけど）「あー、ここをりょうちゃんや香ちゃんがミニクーパーで走ったのねー。」とか思ったものでした。考えただけでもウキウキします。りょうちゃんがアシャンに「よく一人で解決したな・・。」って言ったのは依頼よりも、心の問題なんでしょうね・・・。今回の事柄を乗り越えたことで、アシャンが大きく成長できるといいですね。

８９７５　いちのさま　グループ展？作品展やるの？まー、近ければ見に行けるのに。今日なんてそちらは、歩くのも大変な大雪では？忙しくて大変なんて、うらやましい忙しさだよね。がんばって！！

８９７２　将人さま　２０日までって長いですよねー。でも、北条先生にも、お正月くらいノンビリ休んでいただきたいですし、がまんしなくちゃ、と思うけど、禁断症状がー！！

８９７４　ＯＲＥＯさま　試験終了ですかー？良かったですねー。そちらのクリスマスってステキなんでしょうねー！見てみたいですよ！私もミニクーパーは前のが好きですー！

８９７６　無言のパンダさま　そう、あんなにカワイイ娘を持った優しいパパで、モッコリの威力は,半減したみたいだけど、いつも香ちゃんの事を想い続けているりょうちゃん。もーーー、超渋い、カッコイイ！ステキですよねーーー！！パンダさまは中学生の時お父様を亡くされたのですか？大変でしたね。私はハタチ過ぎに亡くしました。しょっちゅうケンカしてた私と父ですが、花嫁姿を見せてあげられなかったのは心残りでしたね・・。

８９７９　里奈さま　お仕事再開？疲れるよねー,慣れるまでは気もつかうしー。しかし、里奈さまんちのポメちゃん、カワイイー！そりゃ、癒されるでしょう。私ペットは飼ってないの。子供が喘息だったから・・・。治ったら飼おうね。って、もう治ったから飼ってもいいんだけど、なかなか・・・。イラスト描く余裕ができるのを。お待ちしてますわ！！

それでは、長くなりましたのでこれにて・・・。雪いつまで降るんだろう？

再来一次本周的感想。(应该说，我是这么想的)☆仔细想想(即使不考虑)成田机场是千叶县的吧。獠，欢迎来到千叶县!…。但是从CH的时候开始就经常出现在成田机场，所以经过东关道的时候(虽然很少)我会想：“啊，獠和香开着Minicooper在这里跑呢。”。光是想想就很兴奋。獠对香说:“你居然一个人解决了……。”与其说是委托，不如说是心理问题吧···。如果通过克服这次的事情，香能有很大的成长就好了。

### [8979] 里奈	2002.12.09 MON 01:11:08　
	
今日も仕事疲れたわん☆
服の販売って一見楽に見えるけど、そんなことないのよね。
わかってるつもりだったけど、プ～してた期間が長かったから　そのブランクがかなりキツイっす！足いてぇー…（泣）マヒってるよ　まじで。感覚無いじゃん。

☆８９７７　ＴＡＫＥＳＨＩ様
チワワ？
いやいや、里奈のペット･ポメラニアンのブブちゃんが一番可愛いに決まってるわ！！（親バカ）あの顔で首かしげて見つめられたら抱きしめたくなるのよねぇー！超フワフワですっごく気持ちイイのよぉ～♪ちょっと教育の仕方間違えたみたいで、『おて！』って言ったら手をあげるの！『ハイッ！』って感じで。しかも必至にピーン！って（笑）あぁぁー癒される…♪

☆８９７６　無言のパンダ様
はい！仕事頑張ってます！
今もまだ精神的にヘコんでるけど、やることはやってます。
販売ってやっぱり大変だわ…。期待されてる分、気が抜けない！
他の子には内緒なんだけど、里奈だけ時給高いのよね。一昨日に入ったばっかの子にいきなりそんな待遇でイイのか！？って焦ったわ。だから頑張って頑張って売りまくってます。ほんと自分でも関心するほど、ちゃんと仕事してるわ…。一日の売上の半分は里奈だもんね。この馬力がいちまでもつか…。（汗）

☆８９７５　いちの様
チッチッチ…！
そんな簡単に漢だと認めてたまるか！里奈の奴隷になってくれたら考えるわ（なんでまた奴隷…？笑）
いちの様、今　絵　描いてます？
里奈も早く新しいイラスト描きたいわぁー！ウズウズしてんのよね！でも今はまだ新しい仕事で手いっぱいで無理！いつになったら描けるの！？仕事の日数減らしてでもイイから描きたい！まじでソレ考えてるんだけど…。週休３日ぐらいにして欲しいわ。給料なんて生活費稼げたらイイから！絵描きの鏡やね（…？単に働きたくないだけ？）

☆８９７４　ＯＲＥＯ様
な…なんですとっ！？
バスの運転手が運行中にいきなり路駐してマック！！？
なんてステキなほどマイペースなんだ！！
そんなの日本じゃちょっとしたニュースだよ！
そんな　ノンビリした社会、いいなぁ～。やっぱり日本人は働きすぎだよ！皆もっと心に余裕を持って生きようよ！ってゆうか、そちらに逃亡してぇー！里奈の面倒みてくれるナイスカナディアン募集中っ♪（笑）

### [8978] chikka(Eastern Shizuoka)	2002.12.08 SUN 21:35:36　
	
皆さん。こんばんは。chikkaです。お久しぶりです。学校関係の仕事が忙しく、休みのたびに部活があってなかなか書き込む余裕がありません。そんな生活もあと２週間で終わります。私の学校の吹奏楽部の定期演奏会が再来週行われます。それまではかなり忙しい日が続きます。とにかく、ラストスパートです。頑張ります。

今週の感想ですが、ハッピーエンドに終わってよかったです。それぞれ道は分かれますが、またいつの日か再会できることを願わずにはいられません。

【あの人は今・・・・】
「ＣＩＴＹ ＨＵＮＴＥＲ」若手シナリオライタートリオと言えば「シリアスの遠藤」こと遠藤明範氏、「メルヘンの武上」こと武上純希氏、「コメディーの井上」こと井上敏樹氏の３人ですが、武上純希氏は最近では平成ウルトラマンシリーズ、昨年放映されたガオレンジャー、犬夜叉などで、井上敏樹氏は平成仮面ライダーシリーズでの脚本を手がけていることで知られています。しかし、遠藤明範氏の名前は最近はテレビよりもむしろ小説の方で見るようになりましたが、なんと久しぶりにテレビでその名前を見ました。それはあの「機動戦士ガンダムＳＥＥＤ」です。遠藤氏といえばガンダムシリーズでは「Ｚガンダム」や「ガンダムＺＺ」のメインライターでもおなじみですが、「ＳＥＥＤ」の第４話の「サイレントラン」と第６話の「消えるガンダム」で久しぶりの「遠藤ガンダムストーリー」を見ました。遠藤氏は今回もメインライターではありませんが、何回かは「ＳＥＥＤ」の脚本を書いてくれそうです。（ということは、「Ｃ・Ｈ」のアニメは当分お目にかかれそうもありませんが・・・。だって、「ガンダム」も「Ｃ・Ｈ」もサンライズですし・・・）遠藤氏が書き下ろした歴史小説「三遊亭円朝シリーズ」第２弾「真怪談累ヶ淵」が８月に発売されました。

【那个人现在……】  
“CITY说到年轻编剧三人组，“严肃的远藤”远藤明范、“童话的武上”武上纯希、“喜剧的井上”井上敏树三人。武上纯希最近在平成奥特曼系列、去年播放的《鬼连者》、《犬夜叉》等作品中，井上敏树还参与了《平成假面骑士》系列的剧本创作。但是，远藤明范的名字最近与其说是在电视上，不如说是在小说上看到的，竟然久违地在电视上看到了这个名字。那就是《机动战士高达SEED》。说到远藤，大家都很熟悉他是高达系列《Z高达》和《高达ZZ》的主要撰稿人。《SEED》的第4话《寂静兰》和第6话《消失的高达》中久违的“远藤高达罢工”——莉”。远藤这次虽然也不是主要编剧，但好像会写几次《SEED》的剧本。(也就是说，“C·H”的动画暂时看不到了…因为《高达》和《C·H》都是sunrise的…)远藤新写的历史小说《三游亭圆朝系列》第2弹《真怪谈累渊》8月发售了。


### [8977] TAKESHI	2002.12.08 SUN 18:58:52　
	
こんばんは！明日から石川は雪です。寒そう・・・。

＞里奈様
ペットは癒されますね！僕も犬が欲しい。今は「アイフル」のＣＭのチワワが注目されて、チワワが一番売れているそうです。

＞いちの様
友達がレンタルショップで働いてるんですか！？ただで借りられるのはいいですね。スパイダーマン是非見てください！
朋友といえば、昨日ＢＯＯＫＯＦＦで蒼天の拳の一巻を買いました！めっちゃおもろいです。やはりバンチはいいですねー。

＞たま様
「壁紙アニメ」といれて検索すればいいですよ。英語のところもありますが、大丈夫です。


### [8976] 無言のパンダ	2002.12.08 SUN 17:29:20　
	
こんにちわ☆

バースデイメッセージをくれた方々☆ありがとうございました～！またひとつ大人になって（！）成長した（？）無言のパンダでーす♪今後ともよろしくお願いいたします～m(__)m

＞里奈さま（８９６１）
お祝いの言葉ありがとうございます☆
お仕事がんばってるようですね(^^)時間ができたらイラスト絶対描いてくださいね！
私も空港でバイオリンを弾き始めた夢ちゃん、とても大人っぽくて「綺麗！」って思いましたよ☆夢ちゃんの心がこもったバイオリンの音色もきっとすごく美しくて聴く人の心を感動させたんでしょうね。

＞いちのさま（８９６４）
お友達との「グループ展」の準備、大変そうですね。成功を祈ってます！
誕生日のお祝いの言葉ありがとうございます。「幾千の星に願いを・・・」のフレーズにグッときました！(^^ゞクリスマス・イラスト楽しみにしていま～す♪

＞千葉のＯＢＡ３さま（８９６６）
年は取りたくないですが、お祝いの言葉をいただくのはいくつになっても嬉しいものですね。ありがとうございます！(^○^)
リョウちゃんはほんとにすてきな父親になりつつありますね♪私の父は私が中学の時に亡くなったのですが「星一徹」みたいな（ちゃぶ台をひっくり返すようなところが^_^;）人だったので、リョウのような父親は理想ですね☆

＞Ａｙｕさま（８９６８）
今日から修学旅行に出発ですね☆いい思い出をたくさんつくって来てください♪いってらっしゃ～い(^_^)/~

＞将人さま（８９７１）
ＣＤブックをカセットに録ることは可能なのですが・・・今どきカセットを使ってる人っていないのかな？ちなみに私はＭＤなどというものを使ったことがありません！(~_~;)オークションには、ちょくちょく参加しているのでまた出ていたら一応ご報告いたしますネ♪

＞ＯＲＥＯさま（８９７４）
私も絶対、ミニクーパーは古いほうがイイです！（新しいほうはまだ見てないんですが^_^;）あんなに人気の車種をチェンジしてしまうなんてもったいなーい！とりあえず「ミニカー」でいいからあのカタチを保存して欲しいです～！(>_<)
ＰＳ：楽しい冬休みを過ごしてくださいね☆

ではまた。(^_^)/~


### [8975] いちの	2002.12.08 SUN 13:46:45　
	
どうも、皆様こんにちは。
いよいよ切羽詰ってきました（汗）。昨日は、家に帰ったのが１０時過ぎでした・・・。準備とか、制作やらで。
展示の他にも提出期限が間近な課題もてんこ盛りなので、この冬を無事に越せるかどうかが心配です（泣）。
バンチも読んでないし・・・（涙）。

＞[8965]&[8974] OREO様
もう試験終わったのですか！？早い！！
しかも、今日から冬休みですって！！？う、羨ましい・・・（涙）。
私も、グッスリ眠りたい・・・。

＞[8969] 里奈様
あれ？ダメですか？あんなに男らいい台詞を言っても「いちの女疑惑」＆「いちのネカマ疑惑」は消えませんか？
おかしいな～・・・。予想によると、「まぁ、いちの様って男らしいわ～。漢やわ～。」という感じで疑惑がすっきり解消すると思っていたのですがね～（絶対に無いな）。
著作権の話しは、正直言って、分かりません（汗）。でも、１つだけはっきり言えることは、『バレなきゃＯＫ！』って世の中になっていることです。（冷笑）

＞[8970] マイコ様
リョウちゃんをモデルにすることにしたのですね。これは楽しみです（笑）。あせらずに自分のペースで描いていってくださいね☆
これでとうとう「マイコ画伯」の誕生ですな（笑）。

＞[8972] 将人（元・魔神）様
そうなんですよね。今の時代、パソコン無しでは何も出来ないところまで来てますよね（汗）。うちの大学でも、ビジュアルデザインとプロダクトデザインの人気が１番ですね。定員割れしたますから・・・。
パソコンは使いこなせれば便利なんだろうな～っていつも思います（汗）。パソコンの使い方を教えられるなんて、パソコン使いこなしているんですね（尊敬）。スゴイです。
私も使いこなせるようになりたいわ～。


### [8974] OREO	2002.12.08 SUN 11:03:07　
	
こんばんわ～☆
試験も終わって、今日から嬉しい冬休みで～っす！！
一日中ボケーっとヒマに過ごしちゃって、
今まで死にものぐるいで勉強してたのに、極端すぎで、なんか教科書とか開けちゃいます（笑）

＞８９６９里奈様
「旅行」で海外行くぶんにはサイコーに楽しいですよね☆
あ、カナディアンの場合ですが、かなり仕事テキトーですよ。バス乗ってて、イキナリ路駐して、なんだと思ったら、マックとかに入ってって、ご飯かって戻ってきたりします・・・でもだれも文句言わない・・いいのかなぁ・・・
お仕事、復帰するのに色々大変だったみたいですね。でも、頑張ってくださ～い！！

テレビを見てたら、新しいミニクーパーのＣＭしてました☆
でも、古いＣＨに出てたときのミニクーのほうがカワイイ！
そのＣＭにつかわれてたのは、あのリョウの赤ミニ。
でも、新しいのは全然イメージがチガ～ウ！！ってカンジで
「どうして変えちゃったのぉ～？？」っと疑問です☆

それでは、また♪

### [8973] 里奈	2002.12.08 SUN 08:01:18　
	
☆８９７２　将人様
ＨＰ見て来ました！
きゃっ★里奈のＣＨイラスト発見！！
嬉しいわぁ～ん♪
なんかアレ見たらもっと描かなきゃって気になってくるわね！
頑張ります♪できたら教師バージョンの絵、描きますね！

今から仕事で慌ててレスしたんで
続きは今夜また…ってことで。
では仕事行って来ます（汗）

### [8972] 将人（元・魔神）	2002.12.08 SUN 02:50:51　
	
＞[8961][8962] 里奈さま　2002.12.06 FRI 23:44:53　
お仕事、復帰おめでとうございます。
でも1月まで閉店って・・・なんという事
電車の事故で、8時半に終わったのが11時ですか？

新幹線なら、下手したら大阪から東京へ行けるかもしれない時間
ですね。お疲れ様です。

一応、試しに、里奈様のイラストを、僕の改装中のＨＰに
載せさせて頂きましたよ。ありがとうございます。

また、ある程度ミニコントを載せてが、出来上がったら、
言いますけど、
↑上の「URL」欄をクリックして貰ったら、いまの段階での
作りかけのＨＰが見れますよ。

もし、直すような部分があったら、ドシドシ言って下さい。

＞この掲示板に来られている皆様へ
里奈さまや、Ayu様以外の、みなさんのミニコントや、
ミニコントへの感想など、この掲示板に書かれていた物も
一緒に写して、僕のＨＰへ掲載しました。

「自分は、この公式ＨＰに書き込んだだけで、僕のＨＰに
　書き込まれては困る」という方がおられたら、すぐに削除
しますので、連絡して下さい。

＞[8964] いちの様　2002.12.07 SAT 00:41:05　
テストに、展示会って色々、忙しいですね。
パソコン、デザインの世界でも使うかもしれませんよ。

僕も、いままでペンキの腕で、看板を描く年配の職人さん
とかにもパソコンのそういう物を売っていた立場だったんで

プライスカード（価格がでかく書かれた物、ＰＯＰ広告）
を作る機械を売ったときは、スーパーのパートの方など、
パソコンを全く知らない方にもキーボードやマウスの
操作から教えてましたし。

＞[8965] OREO様　2002.12.07 SAT 06:38:26
試験、終わったんですね。ご苦労さまです。

＞[8966] 千葉のOBA3さま　2002.12.07 SAT 07:29:05　
ゲッ！次のバンチって、20日なんですか？
そんな～、また長く待たないと行けないですね。



＞[8954][8967] Ｂｌｕｅ　Ｃａｔ様　2002.12.07 SAT 16:09:14　
ジョーク画像と、＠パン3世のメール届きましたか？
良かった。ＣＨ版でもあれば、いいんですけどね。

＞[8968] Ａｙｕ様　2002.12.07 SAT 17:30:28　
すみません、金曜日の昼に、すでに送ってしまっていて
間に合いませんでした。
修学旅行、楽しんで来て下さいね

＞[8970] マイコ様　2002.12.07 SAT 22:19:58
キャッツアイってビデオレンタル店に喫茶店ＣＡＴＳですか？

お店の人って、まさか、デカくて、頭がハゲていて、
サンブラスされてませんよね？
あだ名が「海坊主」って方で・・・。

それか、美人3姉妹がやっているかも？（夜はドロボウで）

頭ですが、ほんまに悪いんですよ～。
要領、悪いし、人付き合い下手だし、今では、かけ算の九九を
言えって言っても、できないかもしれない。

「いち1　たす+　に2　は=」で、「サンバルカン」って答える方
なので・・・
（子供向き番組でのＥＤで、こんな歌あったんですよ）

### [8971] 将人（元・魔神）	2002.12.08 SUN 02:50:26　
	
こんばんは、将人（元・魔神）です。

＞[8953] 医龍さま　2002.12.06 FRI 00:48:25
「RASH!!」と「こもれ陽の下で・・・」のコミックスは、
とりあえず、置いておきますね。

今日、医龍さまの分に用意していた物を見たら、なぜか
「RASH!!」の第1巻、第2巻の組み合わせが、各2巻ありました。

自分の分はちゃんとあるし、Ayu様の分は送ったんですが、
(つまり、ぞの時点では、全2巻のセットが各2冊あった事に)

古本屋さんを、何軒も色々と回っているうちに、頭の中での
計算がずれていたみたいです。(汗)(^^；
古本屋さんで捜している時に、その前に買った本を確認すれば
良かったのですが、古本屋さんで見つけて時に、別の店で
買った本を取り出して、確認するなんで、あやしいと
思われると思ってたので、確認しなかったのが、
失敗でした。

→誰か「RASH!!」の第1巻、第2巻の全2巻セット欲しい人
　いませんか？

＞[8955] sophia様　2002.12.06 FRI 20:04:52
＞ネット荒らし（ストーカー）ですか！！
当時は、以前にいた会社にまで、怪文書を郵便、ＦＡＸ、電話など
ありとあらゆる手段で、いやがらせされました。
僕が、会社を辞めざるをえなくなるまで、ずっと。
ひょっとしたら辞めた後も、その会社に送ってたかもしれないけど

小学生、中学生の頃に、いじめられていた時に、助けてくれて
同じように、いじめられていたのですが、それ以来、20年以上
の学生時代から友達だったんですけどね、

もう1年たった、今では、もう落ち着いたのか、ほとんど
なくまりましたけどね。
たまに、思い出したかのように、掲示板に書き込むぐらいで・・・

＞[8956] 葵さま　2002.12.06 FRI 21:50:59　
海外版のジャン＠ですが、昨日、書き込みできなかったの
ですが、昨日の産経新聞の夕刊に海外版ジ＠ンプに対して、
好意的な事を書いていましたよ。

今の系列のテレビ局で、ジャ＠プ系のアニメを放送している
新聞だからなのかなって、思ったけど、どうやろ？

読＠新聞も、ＣＨやキン＠マン放送していたテレビ局の系列
ですけどね。

「＠ャンブ」も「雷神」も、海外でがんばって欲しいですね。
ただ、アメリカから帰ってきたら、まるで別物になって
帰ってくるケースもありましたが・・・。

おヒゲの生えた∀ガン＠ム、巨大トカゲになった＠ジラ
目の青いウル＠ラマン、スタート＠ック風の宇宙戦艦＠マト

・・・冴羽リョウが金髪になってミックと見分けが出来ない
ＣＨってのイヤかも

＞[8957] たま様　2002.12.06 FRI 22:03:38　
漫画文庫版ＣＨの18巻には、北条司先生の後書き
あるんですか？知らなかった。見てみたいな～。

＞「はぁぁあ・・・ふしゅるるる」って
あれは、北斗の拳のケンシロウのつもりなんですかね？
あの漫画のアニメで、そんな効果音あったような？

＞[8958] Ａｙｕ様　2002.12.06 FRI 22:10:21　
遅かったです。もう金曜日の昼に、郵便局から冊子小包で
送ってしまいました。もっと、早く気がついていれば・・・。

＞[8959] 無言のパンダ様　2002.12.06 FRI 22:47:39
ＣＤブック、つい最近もオークションに、出ていたんですね
また捜してみようかな？

＞[8961][8962] 里奈さま　2002.12.06 FRI 23:44:53　
お仕事、復帰おめでとうございます。
でも1月まで閉店って・・・なんという事
電車の事故で、8時半に終わったのが11時ですか？

新幹線なら、下手したら大阪から東京へ行けるかもしれない時間
ですね。お疲れ様です。

一応、試しに、里奈様のイラストを、僕の改装中のＨＰに
載せさせて頂きましたよ。ありがとうございます。

また、ある程度ミニコントを載せてが、出来上がったら、
言いますけど、
↑上の「URL」欄をクリックして貰ったら、いまの段階での
作りかけのＨＰが見れますよ。

もし、直すような部分があったら、ドシドシ言って下さい。

＞この掲示板に来られている皆様へ
里奈さまや、Ayu様以外の、みなさんのミニコントや、
ミニコントへの感想など、この掲示板に書かれていた物も
一緒に写して、僕のＨＰへ掲載しました。

「自分は、この公式ＨＰに書き込んだだけで、僕のＨＰに
　書き込まれては困る」という方がおられたら、すぐに削除
しますので、連絡して下さい。

### [8970] マイコ	2002.12.07 SAT 22:19:58　
	
みなさんこんばんわ～！！やっとテスト終わった～！！
もう自由の身ね。（笑）そうそう今日久しぶりに出かけたら、キャッツアイというビデオレンタル屋があって、いつの間に～！！とか思いました。しかも帰りにＣＡＴＳという喫茶店があってびっくりしましたぁ～！！いったい何なんだ今日は･･･。

レスでぇーす★

＞たま様＆いちの様
う～ん。一応絵のこと考えてみたんだけど、やっぱりリョウちゃんを書くことにしますわ～。ちょっと今週忙しくて、書くのに、時間がかかると思うので。ごめんなさいね～！！
できたら送りますんで・・・。（いらないと思ってるかも・・・。苦笑）じゃあ楽しみにしててねぇ～。（楽しみじゃないですよね・・・ハハハ）

＞里奈様
いろいろと大変でしたね。ペットに癒されましたか？そうですよね。里奈様が大好きな子犬ちゃんだもんね。よかったですね！！

＞医龍様
ご心配ありがとうございます～！！医龍様も風邪には気をつけてくださいね！！

＞将人（元・魔神）様
アホ？またまたうそおっしゃって～。ほんとは頭いいくせにィ～。（笑）テスト終わってもう気が楽です♪

### [8969] 里奈	2002.12.07 SAT 22:02:45　
	
今週号の感想パートⅡ
空港で阿香の為にいきなりバイオリンをひき始めた夢ちゃんの顔、すっごく大人っぽく見えました！大人の女っぽかった！そして、夢ちゃんに『いってらっしゃい！』って大きく手を振る阿香は、今までにないくらいの笑顔で、すごく普通の歳相応の女のコって感じがしました！二人とも成長したんだなぁ～って、リョウの側になったように嬉しかったです。それにしても、ほんとリョウちゃん可愛い（笑）

☆８９６８　Ａｙｕ様
明日から　待ちに待ったイギリス･フランス旅行ですねっ♪
いいなぁ～いいなぁぁ～！
里奈も連れてって欲しいにゃぁぁーん！（無理難題）
帰国後のカキコ、楽しみにしてます♪
イッパイイッパイ楽しんで来てくださいな☆

☆８９６５　ＯＲＥＯ様
試験お疲れ様でしたぁ～！
学生してるんだなぁ～。里奈も留学してぇ～。
でも勉強はしない！！（笑）

昨日、夫婦でグアムに旅行行ってた友達が帰国してきて、今朝通勤の電車の中で話聞いてたんですけど、海外の人達って日本みたいにセコセコと気張って働いたりとかしてなくてノンビリしてるんですよね？（いや、日本以外の全ての国がそうではないだろうけど…）一流ブランドＧ●ＣＣＩの店員さんが店内で座ってパン食べてたそうです（笑）そんなんでイイの！？

☆８９６４　いちの様
おぉぉぉーー！！
『男は女を守ってナンボだろうが！！（怒）』
いちの様からそんな男らしいセリフが出るとは！
さては『女』＆『ネカマ』疑惑から脱出しようと試みてるな！？
ダメダメ！いちの様にはレオタード着てもらわなきゃいけないんだから男らしくなってもらっちゃ困るのよ！（笑）
でもホント、いくら女も強く働く社会だからといって、体売っちゃイカンよ。それに群がるのもイカンぜよ！スナックとかまでにしときなさい！さて、著作権の話はどこに…？

いちの様、個人展示会開くの夢なの？
２～３年後には実現できるの？すごいすごい！
里奈も絵　描いてるといろいろ夢持っちゃうんだよね♪
里奈は個人展なんて大それたこと考えてないけど、自分だけのイラスト集みたいの作りたいんだよね。出版するとかじゃなくて、１冊作れたらイイの。自分が満足できたらイイ、究極の自己満足の世界だわね（笑）でもこれぐらいなら無理して頑張らなくても実現ぜきそうだけど。

☆８９６３　ヒロ様
『父親の気持ちって…愛情と思いやりがあればそれでイイと思いますよ、親じゃなくてもネ！』
なんか胸にジーン…と染み込んだわ！
父親のいない里奈でも理解できた気がします。

### [8968] Ａｙｕ	2002.12.07 SAT 17:30:28　
	
　　　　　　　　　ＢＢＳの皆様へ
ついに明日、私はイギリス・フランス修学旅行に行ってきます！！８日～１５日までＢＢＳに来れませんが帰国したらすぐにカキコします。旅行の持ち物としてＣＨドラマテックマスター＆ＲＡＳＨを持っていきます。皆さん元気でいて下さい。一週間後にまたお会いしましょう。

### [8967] Ｂｌｕｅ　Ｃａｔ	2002.12.07 SAT 16:09:14　
	
　こんにちは～。
　将人（元・魔神）さま、ジョーク画像＆ルパ○三世のやつ、届きましたよ～、ありがとうございました（ぺこり）。ル○ンのは、Ｃ・Ｈでもこんなのがあったらいいなぁ、とかちょっと思っちゃいました（にこにこ）。

### [8966] 千葉のOBA3	2002.12.07 SAT 07:29:05　
	
おはようございます！
ちょっと感想のみカキコさせていただきます。

＞私も「一人では,生きられない者同士・・」の所で、グッときてしまいました。ずーっと一人で生きてきたりょうちゃん。香ちゃんの存在が「一人では生きられない・・」と思わせてくれた大切なものだったんでしょうね・・・。あと、「何もない・・。」って言うアシャン。あなたにはこんなに素敵なパパがいるじゃないですか。子供の成長を手助けする・・・・そう、親の大切な役目です。しかし、こういったセリフをりょうちゃんに言われるとは・・・。感無量ですよ。・・・同じ年代なのよねー。と、しみじみ・・・。

今回ホントに色々な感想もったので、またあとでも今週号についてはカキコします。

で、次のバンチ２０日なのーーー！？まじでーーー！？そうだよね？げーーー、また生殺し状態に！？・・・このあとのアシャンの変化とりょうちゃんパパとの絆がさらに深くって書いてあるから楽しみにしてたのにー！あーーー、ながーい２週間の始まりじゃ。

＞無言のパンダさま　　遅ればせながら、お誕生日おめでとうございます！！この一年がパンダさまにとってすばらしい一年でありますように・・・。

それでは、取り急ぎ・・・。また！

### [8965] OREO(Done! :終了～♪）	2002.12.07 SAT 06:38:26　
	
こんにちわ～！！お久しぶりです☆
試験が全部終わるまではカキコできないでいました。
でも、つい先程、イグザムゼ～ンブ終了！
今日から復活です！！！（笑）
皆様またよろしくです☆

そんなときでも、ＡＨは読んじゃうのだなぁ♪♪
しかも試験当日の今朝に読みました。へへへ♪
あ～久々の成田空港・・・なつかしや～・・・
最後はちゃんと「行ってきます」「いってらっしゃい」で満面の笑顔でお別れできてよかったと思いました。
でも、夢ちゃん、笑顔で元気イッパイ、「行ってきます！」って言えてすごいと思います。私が同じ年だったら、絶対泣きそうな気がしますもの。偉いなぁ・・・☆
父娘の関係も、だんだんと縮まりつつありますよね！
次回予告がそんなかんじのこと書いてあるし。
どんな父娘になっていくのか、とても楽しみです。

昨日、試験があるってのに、ちゃっかりドラマなんぞを見てしまったので、一睡もしてません・・・というか、寝ようと思ったら、試験のことで頭イッパイで、寝付けなく、それなら試験勉強しなきゃ！！で、徹夜です。が、かなり元気で～っす☆
でも、も～終わった！終わった！
今はしばしの余韻に浸りたいと思います☆(試験反省はアト、アト！）　それでは、また(^-^)/~~~

### [8964] いちの	2002.12.07 SAT 00:41:05　
	
こんにちは、じゃない、こんばんは。
今日は今日とて、忙しけり。（出鱈目古語）
展示まで、あと５日。テストまであと３日・・・（汗）。
忙しいのは言うまでもありませんね・・・。

＞[8951] 将人（元・魔神）様
「いちの女性疑惑」の次は「いちのネカマ疑惑」ですか！？
勘弁してくださいよ～（汗）。
私は、男、否、漢（オトコ）ですたい！！！（前にもこんなこと言っていたような気が・・・）
確かに、自分の呼び方で年齢的な印象は変わりますよね。これからは「俺」って言おうかな・・・。
私はあまりパソコンとか機械系には詳しくないので、パソ使う仕事にはつかないと思いますが、なんの仕事にせよ、忙しかったり、大変なのは変わりませんよね。

＞[8955] sophia様
「ネカマ」じゃないって！！
クリスマスイラストですか？んんん・・・。
時間が出来次第、描いてみます。誰かさんが、ハードボイルド進化バージョンを希望なさっているのですが、この期間限定から考えると、クリスマスイラストのほうが先に描かないといけないようですね（汗）。

＞[8956] 葵様
自信なんてありませんよ（涙）。
ただ単に、鉛筆デッサンしかできないので、仕方なくデッサン重視で描いてみただけですから・・・。私的には、色を自由に使いこなせる人を尊敬しています。
今度は、クリスマスを題材にイラストを描く羽目になりそうです（いや、別にいいんですけどね。楽しいから）

＞[8957] たま様
グループ展は私が言いだしっぺではないのですが、友達がやりたいと言うので、自分の力を見るいい機会かも、と思い参加いたした次第で御座います。
個人展には作品が少なすぎるため、出来てもあと２，３年後くらいかな・・・先輩では何人も個人展を開いている人がいますけどね。
（応援しても何もでませんよ。残念ながら）

＞[8959] 無言のパンダ様
え！？ハッピーバースデイ！！？めでたい！！めでたいと言ったら、ネ（しめす辺）に兄（あに）＝祝☆
誕生日プレゼントはクリスマスイラストでよろしいでしょうか？もちろん、「渋い男」もあとで描きますが（汗）。
この１年がより良いものとなりますように。幾千の星に願いを架けます☆

＞[8961] 里奈様
本人にイラストを送るのにはカナリの勇気と根性と自信が必要ですよね。
私にもそんなことは出来ません（汗）。
著作権については私も詳しくは分からないのですが、よくお店の前にある「のぼり」とかに描かれていますよね。それも、パチンコとか風俗とかに使われていることが多いです。ホンとにファンにとっては憤慨ですよ。女性の身体を商売として扱っていること自体に嫌悪感を抱きますね。
なによりも、それに群がる男たちが嫌いです。情けないったらありゃしない（汗）。
男は女を守ってナンボだろうが！！（怒）。
金払ってまでヤルことかい？

[8961]里奈  
给本人发送插图需要相当大的勇气、毅力和自信。
我也做不到那样的事(汗)。
关于著作权我也不太清楚，不过经常在店前面的“旗”上画。而且，多用于柏青哥和色情场所。这对书迷来说是非常愤慨的。把女性的身体作为买卖来对待，这本身就令人厌恶。
最讨厌聚集在那里的男人们。真是太可怜了(汗)。
男人怎么能保护女人呢! !(怒)。
要花钱做吗?


### [8963] ヒロ	2002.12.07 SAT 00:38:26　
	
こんばんは～(^o^)丿

今週も･･･、やっぱりリョウちゃんが決めちゃいましたネェ～！
リョウちゃんが裏方にまわろうとすると、
逆に目立っちゃうんだから･･･、困ったもんだなぁ(笑)

阿香と夢ちゃん、やっとホントの友達になったんですね。
って言うか、ただただ、お互い切り出せなかっただけ！？
これからは、遠くてもずっと親友でいられそうな二人だね。
夢ちゃんが「助けて～！」て言ったら、どこへでも飛んでいきそうな阿香。

私なんか、先週からじれったくてじれったくて･･･。
せっかちな私にとっては、今週の「行って来ます」と「行ってらっしゃい」までは、と～っても長く感じちゃいました。でも、めでたしめでたし！
これで、阿香も少し大人になったんだろうな。無理して大人にならなくてもいいんだけど･･･。

レス、遅くなってすみませ～ん。

＞里奈[8873]さま
パパの気持ちでリョウちゃんを見ると･･･、うぅ～ん、うぅ～ん、頭が破裂しそ～！
私には高２の息子がいるんですが、ちょっと阿香とは程遠いしなあ。(喧嘩ばっかり)
娘が大きくなったら考えますね(笑)
親の気持ちって･･･、愛情と思いやりがあればそれでいいと思いますよ、親じゃなくてもネ！

＞たま[8874]さま
ＨＰ見て頂いてありがとうございます。
恥ずかしいんで、あんまりじっと見ないでくださいね。
あんまりみていると、恥ずかしくって画面が赤くなっちゃいますよ！ウソ(笑)
遅れましたけど、ご入籍おめでとうございま～す♪
お幸せに～！

それでは(^^)/~~~

### [8962] 里奈	2002.12.06 FRI 23:47:38　
	
☆８９５０　将人様
あれー？将人様のレスだけ消えてる！なんで？？
もういっかいカキコ！
里奈が今までに送ったイラストで使えるものがあれば載せていただいてけっこうですよ♪今までにＣＨ関連何枚送りましたっけ？もともとそんなに描いてないから少ないですけど…。

### [8961] 里奈	2002.12.06 FRI 23:44:53　
	
今日初出勤でした。久し振りに販売の仕事に復帰。
一日店頭に立ってたから足パンパンだわ！
疲れ果てて駅に行ったらホームがすごい人ゴミだったんで『なんじゃこりゃー！？』って引いてたら、人身事故で電車が何時間も遅れてた！やっと乗ったはイイが各駅で１０分づつ足止めくらって帰って来たのは１１時…。仕事終わったの８時半だったのに。
しかも、噂によると里奈が無理矢理入れられた店は１月半ばに閉店が決まってるらしい！えっ！？わざわざデパガ辞めさせられて来たのに１月までかよ！？もうイヤァァァー！！

そんなこんなで、クタクタになって地元に帰って来てコンビニでＡＨを読んだ。リョウパーパにすんごく癒されたわ☆なんてステキな父親してるの！？記憶をイイ想い出だけに録画しなおせたらどんなにイイか…。ほんとにそう！記憶…無くしたい記憶って誰にでもあるもんね。一人じゃ生きていけない者同士…って、リョウも香なしでは生きていけないぐらい落ち込んでいたんだと改めて感じさせられました…。今週のリョウパーパは、父親のいない里奈にとって心底泣きたいくらい、里奈にとってもパーパでした。産まれて初めて『パパが欲しい…』って思ったかもしれない。阿香の『私のパーパ…！！』ってセリフ、なんか里奈の心に深く刺さった気がする。

☆８９５９　無言のパンダ様
無言のパンダ様、誕生日だったんだ！
おめでとうございます♪
里奈は昨日に引き続き撃沈してます。
人間不信…？このまんまじゃ陥るかも…（汗）
でも里奈はそんなキャラじゃないんで立ち直ってみせるわ！！

☆８９４９　いちの様
ファンレターと一緒にイラスト送るファンってけっこういるんだぁ～？でも、もちろん　そのイラストは返っては来ないんでしょ？里奈いままで描いたイラスト友達とかにあげすぎて手元に残ってるのって最近描いたのだけだから、今後は全部とっておきたいのよね！里奈にとって時間かけて（かかってないか？）愛情込めて描きあげたイラストは自分の分身みたいなもんだから手離したくないわ！ってゆうか、北条先生に見てもらうだなんて、やっぱり恥ずかしくて無理だわ…（汗）
そうそう、著作権で思い出したんだけど、ＦＣの紫苑ちゃんのイラストを明らかにそのまんま看板に使ってる業者を見つけました！京都の細い裏道にあるいかがわしい店で、おもいっきり風俗関係でしたね。そんなとこに紫苑ちゃんが使われてるなんて許せなぁーっい！これって犯罪じゃないの！？

☆８９４９　いちの様   
和粉丝信一起送插画的粉丝相当多啊~ ?但是，那张插图当然不会还给你吧?里奈到现在为止画的插图给了朋友们太多了留在手边的都是最近画的，今后都想收藏起来!对里奈来说花了时间(没花吗?)饱含深情地画出来的插图就像自己的分身一样，不想放手!怎么说呢，让北条老师看，还是觉得不好意思，不行啊…(汗)  
对了对了，关于著作权我想起来了，我发现有商家把FC的紫苑的插图明显地原封不动地用在招牌上!那家店开在京都一条小路上，很可疑，跟风俗业有很大关系。紫苑竟然被用在这种地方，真是不可饶恕!这不是犯罪吗??  