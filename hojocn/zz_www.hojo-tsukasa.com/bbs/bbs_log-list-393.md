https://web.archive.org/web/20050313021048/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=393

### [7860] おっさん(岐阜県：来ちゃったのね～ん）	2002.09.19 THU 21:20:39　
	
毎度、おっさんでごじゃいます。新入り様が増えていますけど、ちょくちょくしか現われないんで、しかもおっさんって書いてても私は女（弐拾七歳）なんで、そこんとこよろしこ～です。

ちょっとレスを・・・
さすらいのアザラシ　たまちゃ～ん（７８２２）、おっは～！！うちの店のスタッフの子にもたまチャンって付けてるんですけど、引継ぎのお手紙に「さすらいの天使アザラシ・・・」って付けてます。（それ以上書くと放送禁止用語になるんで・・・。どんなんや！！）

Ｘ・Ｙ・Ｚボックスですか？私も書きたいんですけど、何せ岐阜も遠いし、行く暇がないし・・・（ＴＴ）仕事が・・・他にもおいてくれんかのう、といいつつおっさんは闇の中へ消えてゆく。

### [7859] TAKESHI	2002.09.19 THU 19:46:15　
	
＞[7848]里奈様
その企画いいですね！先月に東京行ってきたんで（もちろん新宿も）道案内ぐらいできますよ！自信ないですけど（笑）

＞[7853]たま様
そうですか、ゲームしないんですか。女性の方はあんまりゲームしませんもんね。ありがとうございました！

＞[7855]千葉のOBA３様
そんな、とんでもない！間違いなんて誰にでもあるんですから気にしないで下さいね。
OBA３－ズ入れていただいてありがとうございます！

### [7858] 西本　麻依子	2002.09.19 THU 19:07:47　
	
「もっこりネットワーク」をご覧になった方、いかがだったでしょうか？みなさん絶対笑ったと思います。　　　　　　

やったっ♪今日は、CH２の日だー！

### [7857] OREO	2002.09.19 THU 14:37:55　
	
ハッ！日本は今日１９日だった・・・コッチはまだ１８日なんで、今日が神谷さんの誕生日だと思っていた・・・
遅ればせながらおめでとうございま～っす！
よく考えてみたら、神谷さんはウチの父と同い年？・・・
全然見えません・・・かなりビックリ☆

＞７８５５千葉のＯＢＡ３様
よかったです！ＡＨの特大パネル見ることができて☆

＞７８３４いも様
そうなんです。只今カナダ在住です☆それでも頑張ってここに遊びに来てます☆

今日スーパーでアニメの雑誌を立ち読みしてきました。そしたら、いやぁ・・・載ってるわ、載ってるわ、日本のアニメ
ＲＡＩＪＩＮのコトも１ページの特集で載ってて、どうやら１１月に発売だそうで。ＣＨのこともちょっと紹介してて、「ロマンチックな一面も持った都会の犯罪ドラマ」って書いてありました・・・なんかこの表現ビミョーって思うのは私だけ？コッチってすごい本が高くって、そのアニメの雑誌もどっかのパンフレットの薄さなのに＄４．９９もして・・だからＲＡＩＪＩＮが２５０ページで確か＄３くらいはかなりスゴイって強調してました☆
それから、今週かな？金曜日には千と千尋～が映画公開されます。コッチの雑誌では、タイタニックよりも、すばらしい映画って紹介してて、メチャメチャ期待されちゃってます☆

ごめんなさい。長いっすね。
それでは、マタ♪

### [7856] 無言のパンダ	2002.09.19 THU 14:23:10　
	
こんにちわ★

いいお天気！明日はバン金♪

＞西本　麻依子さま（７８３１）
「もっこり検索」私もやってみました♪まさにもっこり新発見でしすね(^_^;)この「北条司公式ＨＰ」も載ってましたが、あと個人的にお気に入りなのは「もっこり居酒屋」！本当の居酒屋ではないと思うんだけど（お酒を扱ってるＨＰ？）もし本当にあったらぜひ行ってみたーい♪そしてやさぐれる・・・！（笑）

＞いもさま（７８３４）
遠くから見守る～？ダメダメ！一度見ちゃった人は必ず一緒にやさぐれなきゃ！バンチーズの首領「さすらいのあざらし・たま」さまにスパルタで鍛えてもらって出直してきなさ～い（笑）

＞葵さま（７８３５）（７８３６）
「ムチムチパニック」の私に救いの手をありがとうございます！
しかし私にはその時の記憶がまるでないのです～(T_T)
でもきっと私の「長～い指」のどれかが触っちゃったんでしょうね☆今度から気をつけます！(^^)
それから、まろてぃ勧誘の件（無意識に勧誘してた！）へのご指摘ありがとうございました～そうでした！そうでした！忘れてました(^_^;)管理人さまは約２ヶ月お留守でしたねー。
「やさぐれバンチーズ」入会希望の方（笑）しばらくお待ちを♪

＞将人さま（７８５１）
キーボードの誤動作の件へのアドバイスありがとうございます！
何がどうしたのかわからないのですが次回は「ムチムチパニック」に陥らないようにおぼえておきます！
保志さんのことは子パンダもおもしろがってますんで大丈夫ですよ☆
それから美樹さんと海ちゃん、そして信宏の会話！おもしろかったです♪ほんとにもしかしたら裏ではそんなことが？！って気になりました(^^)

＞たまさま（７８５３）
まさか次は「飼いあざらし」を目指しているのか～？！
ゴマちゃんのように可愛くなったら考えましょう

＞千葉のＯＢＡ３さま（７８５５）
交通費を出してあげて自分んちから信宏を通勤させる～？！それこそアブナイですよぉ！その上まさか心配になって新宿まで後をつけて行ったりして・・・！（もちろん変装して・笑）

昼間っから長々と失礼しました！m(__)m

＞西本 麻依子 (7831)  
我自己也尝试了 "Mokkori 搜索"....找到了这个"北条司公式ＨＰ"，但我个人最喜欢的是"Mokkori 居酒屋"！ 我认为这不是一家真正的居酒屋（处理酒精HP)？ (笑)


### [7855] 千葉のOBA3	2002.09.19 THU 08:40:58　
	
おはようございますです。
わーーん、１日遅れですが、
　　　☆神谷さん　お誕生日おめでとうございます！☆
　これからもステキな声を聞かせてください。

で、なんか、カキコの爆発で読むのが大変ですねー！すごい！自分のカキコもどっかいっちゃったし。

７８２８　無言のパンダさま　　いやー、危険だわ、子パンダさま。信宏を、小さくしてお家で育てる・・・。なんて。（私も交通費だしてあげるから、千葉から新宿まで電車通勤しないかな？なんて思いましたけどねー、ワハハ。）ここは、たまさんが変わりにアポドキシンを飲んで小さくなってくれるそうなので、玄関前にでもつないでおいて下さい。（子パンダさま暴れだしそう・・・。）

７８３０　ＴＡＫＥＳＨＩさま　いやーん、（コビたってかわいくないぞ）いちのさまのカキコ読んで気がつきました！私、ＴＡＫＥＳＨＩさまの、Ｈをずっとぬかしていました！わーん許してＴＡＫＥＳＨＩさま！どうぞどうぞ、ＯＢＡ３－ズにお入りください！（なにも特典はございませんが・・・。）

７８３１　西本　麻依子さま　「もっこり合唱隊」！？なんなんじゃそりゃー！ってカンジですね。すごいネーミング。私もあとで見に行ってみます☆

７８３５　葵さま　いやー、やすこさま（はじめまして。）、ちょこさま、いもさまのカキコを見て葵さまの偉大さが・・・えらい！あなたはエライ！！そーんなにはずかしいトコだったんですか！！（？？？）私来週あたり、行こうかなーと思ったけど・・・。おでこにマジックでＵＴって書いて・・・。（ワハハ）でもさー、もしみんな恥ずかしがって誰もＸＹＺしてなかったら・・・・。もしかして葵さまだけだったら？？？すごい！すごすぎるー！

７８４０　いちのさま　「スターウォーズ」の緑のちっこいのっつて？私が思い浮かべてしまったのは、なぜかヨーダ。（汗）

７８５１　将人さま　競馬やりましょ、競馬！（だから、誘うなって！）将人さまの　７８１５まるでさまへのカキコよんで思わず微笑・・・。（美樹さんを海ちゃんが信宏に紹介するところ。）いいですよねー、こういう想像って、皆さん色々考えているんでしょうねー。楽しいです。

７８５３　さすらいあざらし・たまさま　　しかし私ホントにここで「長老」かなー？誰か年ごまかしてない？（なーにを今さら・・・。）たまちゃんは横浜が気にいったようですよ！浜っ子たまちゃん！

７８５４　カオリンさま　はじめまして。（でしたよね？）皆さん色々複雑な心境でしょうね・・・。私も５、６月頃までモヤモヤしてたかなー？だんだんとふっきれてきたような気がしますです。

さー、木曜日だぜー！！がんばりまーす！長くてすいません。





### [7854] カオリン	2002.09.19 THU 01:49:14　
	
何か、マジで久々にここ来ました。ＣＨがほんとにほんとに好きで、ＡＨがパラレルワールドで香とかの設定とかが全然違う（出会いとか・・・。シュガーボーイは？？みたいな。）ことで遠ざかってたんですが、最近やっと受け入れられるようになってきました。今度からはちょくちょくきたいなと思います。
つーか明日は大学に成績をとりに行くんでめっちゃドキドキです。

### [7853] たま	2002.09.19 THU 01:28:18　
	
「神谷さん♪」お誕生日おめでとうございます。

（もう日付が変わり昨日になってしまいましたね。）
５＠歳ですかぁ～・・・。（結構なお年のようで・・・・・・・ﾊﾊﾊｯ）
私はいつも神谷さんの素敵な美声に心打たれています。
ラジ＠の声がリョウの声とダブって聞こえるのがたまらないのです。最高です！
これからもお身体には気をつけて、がんばって下さい。

あれれ？一日でカキコがいっぱい！！！

＞千葉のOBA3様
えぇ？ある意味「長老（メイヨール）」かなぁ～？なんて思ったりして・・・ＢＢＳのメイヨール　・・・・・・・・・えへへ（笑えない？）

＞sophia様
元気になって、いっそうパワーアップしてませんか？（ｓｏｐｈｉａ様のカキコが）
それとも、やさぐれメンバーだからかしら？！

＞ＴＡＫＥＳＨＩ様
わたし、ゲームはしないのでまったく分からないのです。
（いちお私もギャルなので・・・）
あっ！しまった！ギャルって書いてしまった！
長老（メイヨール）に烙印押されてしまうぅーーーっ！（逃げろ～汗）

＞無言のパンダ様
何々？「さすらいのあざらし・たま」をペットとして買いたいぃー？！
（注　：　どこにもその様な事は書いてござらん）
では、信宏の代わりにたまさんがアポトキシンを飲んで差し上げましょう！

＞いも様
あららっ。ＸＹＺ・ＢＯＸを前にして退散してしまった訳ですね。
やさぐれメンバーから「しょぼい」のレッテル貼られてしまいますよ。
こりゃ、葵さまに鍛え直してもらわないと・・・！

＞ちょこ様
まじですか？かっこいいっすか？
「さすらい」まではＧＯＯＤなのだけど、その後が「あざらし」じゃなぁ～。
「いよいよのご対面にワクワク」って誰に会うのですか？
もしかして椎名桔平さん？！？（このネタ引っ張るつもりかっ？！）

以上、前半戦のレスです。
まだまだ、レスはあるのですが、今日はこの辺で。
では、皆様おやすみなさい。ＺＺＺ

### [7852] 里奈	2002.09.19 THU 00:17:53　
	
☆７８５１　将人（元・魔人）様
東大阪市に住んでおられるんですね。
私は石川で産まれてあっちゃこっちゃ行って最終的に滋賀県に落ち着いてます。ほんま田舎ですよぉ～。仕事は京都なんで毎日滋賀から通ってます。帰って来ると静かで落ち着く。仕事で滋賀から大阪に毎日休み一切無しで半年間通った事もありました。死にました…σ（+_+；）ほんまに肉体疲労で大変だった！風呂場で倒れて意識不明＆呼吸停止！そういえば、それって香ちゃんの命日とされている２０００年５月の出来事！しかも私もしっかりドナーカード持ってるし。そんなこんなで大阪は二度と行ってません。だけどまた大阪で仕事させられそうなんで、大阪で息抜きできるイイとこ教えてくださいな（泣）

### [7851] 将人（元・魔神）	2002.09.18 WED 23:55:25　
	
＞[7811] Ｂｌｕｅ　Ｃａｔ様
原哲夫さんの奥さまのHPからAHと蒼天の拳の巨大パネルの
写真を見れました。情報ありがとうございました。

＞[7813] いちの様
＞字（あざな）が必要でござんしょう。
じゃあ以前に別のHPで付けて貰ったこういう名前ありますよ
「魔神我絶斗」「愚霊斗魔神我」「紅蓮対座」
「解津他炉墓」「頑駄無」・・・読めますか？（汗）(^^;
これは全部、昔のロボットアニメの名前で
マジンガーＺ、グレートマジンガー、グレンダイザー
ゲッターロボ、ガンダムってロボットの当て字です。
「魔神」が入っているのは2つだけですけどね。

＞[7815] まるで様
海坊主の奥さんの美樹さん、ミック、冴子の妹の麗香さん
は、今までの時点ではAHには出ていません。

CHとAHがパラレルとしての話なのでAHの世界にいないのか？
いてるけど香さんを亡くしたリョウの姿を見るのがツライ
ので遠慮して、リョウの近くを離れているとか？
リョウにも会ってるけど、漫画に出てないか思います。
（美樹さんは夫婦だけどリョウの為に、海ちゃんの別の家
とかに住んでいるとか）

北条先生がパラレルの世界とは言っても、絶対いないとか
出さないとかは、言われてないので今後出てこられるかも
しれないな～って思ってます。
冴子さんが香瑩に「おしゃれ」を教えたみたいに常識とか
女性らしい何とかとか教える話とかで。
もし美樹さんが出てきたら・・・
信宏「海坊主さん、この方は？」
海坊主「俺の・・・あの・・・その・・・」（赤くなる）
美樹「はじめましてファルコンの妻の美樹です」
信宏「えっ～海坊主さんにこんな美人の奥さんいたんですか？」
海坊主「いたら悪いか！うるさいんだよ」
（照れてボカッって信宏を殴る）
・・・ってシーンを勝手に想像してます。

＞[7819] 葵さま
ＸＹＺボックス行かれたんですか？
大阪の本屋さんでもやってくれないかな？
あっ、でも新宿東口のMyCITYだからいいんだよな～♪

＞[7824] 里奈さん、はじめまして。
関西の方ですか？私も大阪府の東大阪市に住んでいますよ。
これからもよろしくお願いします。

＞[7828] 無言のパンダ様
もしノートパソコンとか、テンキー（右の方にある計算機
と同じ並びの数字キー）が無いタイプだったら、
「Num Lock」キーという数字を入力する為の切り替えキー
を押したのかもしれませんね。

声優の保志さんって「イジられキャラ」のイメージなん
ですか？
子パンダさんは、信宏役の「予想」と「希望」あるんですね。
私が声優の保志さんの事を悪く言っちゃったかな？

神谷明さん、お誕生日おめでとうございます。

＞[7831] 西本　麻依子さま
「もっこりネットワーク」ってあるんですか？後で見てみよ！

＞[7832] やすこ様、はじめまして。
ＸＹＺボックス見てこられたんですね。
依頼書を持って帰った人って、そこで書きにくいから
書いて持ってくるつもりなんでしょうかね。
これからもよろしくお願い致します。

＞[7810] TAKESHI様[7812] 葵さま[7813] いちの様
＞[7814] 千葉のOBA3様[7818] sophia様
7777のキリ番のラッキーでロト6が当たればいいんですけどね。
＞菊花賞とか、天皇賞で勝負しましょう！！
競馬や競艇、パチンコ、麻雀とかカケ事できる年齢(31歳)
ですけど、どれもやった事ないんですよね。
ジャンボかロト6とかの宝クジだけですね。

### [7850] 里奈	2002.09.18 WED 23:48:06　
	
☆７８４９　西本　麻依子様
でしょでしょ！？（笑）あぁぁぁー新宿行きたぁーい！
ほんまに行きたぁーい！（￣□￣；）

### [7849] 西本　麻依子	2002.09.18 WED 23:40:10　
	
＞「７８４７」里奈様　「XYZ・BOXに投稿しようツアー」いいですね。ぜひ組みましょう★（笑）

### [7848] 里奈	2002.09.18 WED 23:32:37　
	
☆７８４７　ＴＡＫＥＳＨＩ様
きゃっきゃっ♪ヽ（≧ヮ≦）ノ
石川県民バンザイ！石川の人が何人もいてくれて嬉しいです♪石川出身メンバーで『ＸＹＺ・ＢＯＸに投稿しようツアー』組みます？（笑）そうでもしなきゃ方向音痴の私には東京の街は歩けません！田舎育ちなもんで都会は怖いですぅ～

### [7847] TAKESHI	2002.09.18 WED 23:23:07　
	
＞[7840]いちの様
そうですよね。バスケはほんとおもしろいですね。シュート（特にスリーポイント）がスパッと入ったときの快感は最高です！！バスケ部に入ったおかげで運動神経がかなり上がりました。バスケといえばスラムダンク、最高におもしろい！！！桜木花道はすごくすきです。[7841]いえいえ、気にしないで下さい。間違いは誰にでもあることですからね！

＞里奈様
僕も石川県加賀市出身ですよ！！いやー、石川県僕だけかと思ってたんでうれしいです。僕もXYZ・BOXに依頼したいんですけど、やっぱ石川から東京は遠いですよね。東京の方は羨ましい・・・。

### [7846] 里奈	2002.09.18 WED 22:59:38　
	
☆７８４２　西本　麻依子様
加賀市ですかぁ～。里奈は七尾市です！アホでおバカで貧乏だけど一応市長のひ孫だったりしちゃいます（笑）だけど石川を出た裏切り者です（てへへっ）

### [7845] 西本　麻依子	2002.09.18 WED 22:54:26　
	
＞「７８４２」里奈様　そうですよね。各県にXYZ・BOXおくべきですよ！　へぇ～里奈さんも石川県出身ですかー！！いや、びっくり！私は加賀市ですが、里奈さんは何市ですか？

### [7844] しゃぼん	2002.09.18 WED 22:51:00　
	
今日は神谷さんのおたんじょうび。
５＠歳めでたいめでたい！
また来てくれないかな～。（願望）

### [7843] 里奈	2002.09.18 WED 22:44:41　
	
☆７８４０　いちの様
なぁ～んだ！パソコン初心者は私だけじゃなかったんですね。私こうゆうのってパソコン使い慣らしてる方達ばかりが参加してるって思ってたんで安心しました。ってゆうか私たまに誤字しちゃいますけど触れないでやってくださいませ…。

### [7842] 里奈	2002.09.18 WED 22:38:28　
	
（　＾ヮ＾）_且~~~
みなさん今日も一日ご苦労様です。仕事終わってココに来るのが楽しみで一日中ソワソワしてました。

☆７８２７　ｓｏｐｈｉａ様
『ギャル』は禁句っ！？失礼しました（笑）でも私みなさんが思ってる程若くないです。ギャルは未成年とは限りませんから！四大だってしっかり出ちゃってる社会人です。あははは…。

☆７８３２　やすこ様
始めまして♪いいなぁ～、ＸＹＺ・ＢＯＸ見に行かれたんですね。ほんと羨ましい！関西にも欲しいよぉー！！依頼書は持って帰って後日出す事もできるんですね。そうしてる人多そう。でも私なら依頼内容考えて行って、しっかりそこで書いて出します！
ではこれからもヨロシク♪

☆７８３３　西本　麻依子様
石川県出身の方なんですね。実は私も石川出身！各県にＸＹＺ・ＢＯＸがあったらありがたみ無いですけど、でもなんとかしてこの目で見たいですよね！みんなはどんな事書くんだろう？

### [7841] いちの	2002.09.18 WED 22:29:49　
	
ＴＡＫＥＳＨＩ様ごめんなさい！！
↓のカキコで、１箇所ＴＡＫＥＳＨＩ様の“Ｈ”が抜けている箇所がございます（汗あせ）。
すいません（慌て）。
