https://web.archive.org/web/20031024205526/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=383

### [7660] あおい	2002.09.03 TUE 15:06:35　
	
こんにちは。

[7657] たま 様
お答え有り難うございます。でも実は言いたかったのは、ＡＨで看護婦をやっていてりょうを「野良犬さん」と呼んだのは、何歳を設定していると思いますでしょうかっだったのですよ。紛らわしい質問ですみません（Ｔ｡Ｔ）その辺はいかがでしょうか？

私はシティーハンターが好きで今のＡＨを読み始めて、香が死んでしまった時は恥ずかしながらも、涙してしまいました。私はどちらかというとシティーハンターの方が好きなもので（りょうと香の微妙な絡みが好き）なんだかＡＨでシティーハンターでの情報と違ってきているようで、それがちょっと寂しくって（；▲；）

[7658] 茶穂 様
私もシティーハンター時代の二人の関係はじれったかったですが、私的にはそれもそれでいいかなって思ってます。確かにＡＨではりょうの気持ちもよく表面に表れていて良いとも思います（＾０＾）

※なんか変な文章構成になっているような…（＊▲＊）

### [7659] 千葉のOBA3	2002.09.03 TUE 09:01:27　
	
おはようございます。今日もとっても暑いけど、子供達は学校だし,給食も始まったし、ルンルン♪

７６４６　ニャンスキさま　りょうちゃんが倒れてた場所って、そうかもしれませんねー。二人にとっては大切な場所かも・・・。で、ログの川に流れちゃってレスしそこねたんですけど、私もたまさま同様,３０巻はダメ組です。（汗）あきら（晶）様のおっしゃる事もわかるんですけど、やっぱりダメ。なんで、香ちゃんは、あんなに強くいられるんだろうと、不思議に思います。自分に自信があるわけじゃないじゃないですか、彼女の場合・・・。それでも、あのお話のラストでああいうセリフが言えるのは、なぜ？

なんにせよ、香ちゃんの愛情って相手に見返りを,求めてないんですよね。誰に対してもそうだけど、りょうちゃんに対しては、ほんとにそう・・・。何もいらないから、ただそばにいてくれってだけ。だからこそ、りょうちゃんにここまで愛されるんでしょうね。私は自分にはできないぜー！って思うから、よけいに香ちゃんに感情移入しちゃうんだろうなー。と自分の心理を分析してます。

７６５１　葵さま　今日でしたか新宿行きは。こちらこそ、早とちり失礼しました。暑かったですよー。今日も暑いでしょうね。
そういえば、例のＡＨの大パネル、ＪＲ信濃町駅にもあったと、先日神宮球場に野球を見にいった息子が言ってました。案外いろんな駅にあるのかも・・・。

７６５６　いちのさま　やっだー！いまごろわかっちゃったー！？＞７６３５で、ぴぃさまも言ってらっしゃるけど、ＯＢＡは「オバ」よ「オバ」！何回も言わせると、それこそハンマーよ。（ワハハ！）←不気味な高笑い。

それでは、学校はじまった皆さんがんばってくださいね。早く涼しくならないかなー！

### [7658] 茶穂	2002.09.03 TUE 08:41:39　
	
みなさん　おはよーございます。
にどめまして　です。

しょーじきいって　わたしは　Ｃｉｔｙ　Ｈｕｎｔｅｒよりも
Ａｎｇｅｌ　Ｈｅａｒｔのほうが　すきです。
いま　よむと　Ｃｉｔｙ　Ｈｕｎｔｅｒは
わかさと　いきおいが　はじけてる　かんじがするけど
Ａｎｇｅｌ　Ｈｅａｒｔは　おとな　なかんじがして　よいです。
Ｃｉｔｙ　Ｈｕｎｔｅｒの　香と　りょうちゃんは　
じれったすぎて　ときどき　　う゛～　　っていらいらとか
きはずかしくなったりも　したけど
Ａｎｇｅｌ　Ｈｅａｒｔのふたりは　わりと　しょーじきなんで
あんしんして　たっぷり　はいりこめて　よいです。
ふたりのふんいきが　Ａｎｇｅｌ　Ｈｅａｒｔのが　ここちよいです。
それに　なんか　Ｃｉｔｙ　Ｈｕｎｔｅｒは　
ぱたーんぽさが　あったけど
いまのところ　Ａｎｇｅｌ　Ｈｅａｒｔは　　
いろんなはなしがあって　しんせんで　よいです。
たしょーの　むじゅんが　なんぼのもんじゃいっ！
んなもん　Ａｎｇｅｌ　Ｈｅａｒｔだけじゃ　ないやろがっ！

・・っはぁっ
ためてたもん　はきだしたら　ちょっと　すっきりした。
Ｃｉｔｙ　Ｈｕｎｔｅｒは　わたしにとって
せーしゅんの　１ぺーじ。　わすれられない。
けど　いまは　Ａｎｇｅｌ　Ｈｅａｒｔのが
もっともっと　だいすきです。

これがほんねの　ちゃほ　でした。

### [7657] たま	2002.09.03 TUE 00:36:32　
	
みなさま、こんばんわ。
先ほどまで、まろ様のＨＰの方で一暴れ(？)していました「たま」でございます。

＞ニャンスキ様【7646】こんばんわ。
あきら（晶）様【7654】より貴重な“お言葉”を頂いちゃいましたね。(^^;
（香もリョウの心の動きを分かってくれると思います。最後、飛行機を見送るときの会話を見てください。）
　　↑
この事は１２０％分かっているのですよ。
解っちゃいるけど…ってやつですよ。ねぇ～ニャンスキ様っ。（同意を求めるたまさんであった。チャン・チャン）

＞実果様　【7641】こんばんわ。
実果様も相当のＣＨファンですね♪
ドラマ仕立てで始まり、途中から曲がフェイドインしてくるパターンも好きなんですよ。
実はＣＨのサントラはＯＲＥＯ様よりの頂きものなのです。うふふっ。
それから、私はシリアスが多いとは書いたけど、重いとは書いてないよぉ～。（ぐずん）
そして私はシリアス系よりギャグ系が好きなので、りょうちゃんのもっこり是非ＡＨでもふんだんに取り入れてほしいです。

＞あおい様　【7643】はじめましてです。
ＡＨでは香は１７歳でリョウに出会ってますよ。
ＡＨ「５５話槇村兄弟」の巻で槇兄が言ってましたもん。
「まだ１７なんだ・・・」って。
あれっ？質問って、香がリョウと出会った時の年齢じゃなかったですか？

＞TAKESHI様　【7647】はじめましてです。
「リターナー」見たいって思ってたんですよ。私も。
日本映画だから迷ってたんですけど、そうか、面白かったのかぁ～。
見ようかなぁ～。リターナ。

＞いちの様　【7656】ハンマーじゃぁーーーっ。
いや、手榴弾！トラップ！早朝バズーカー！じゃぁーっ。
「あ」使いすぎなんじゃぁーっ。
左小指大活躍なんじゃぁーっ。
はぁ。はぁ。少々叫びすぎました。（笑）


### [7656] いちの	2002.09.02 MON 23:47:20　
	
こんにちは。いちのです。
今日は特になんもなく、ＣＨ漬けの１日でした。しかし、３０巻まで読んだのですが３１巻がないためしばしおわづけの状態です（汗）。
早く続きが知りたいぃぃぃっぃい・・・・・・・。
つーか、少しは運動しないと、このままではグータラのまま夏休みが終わってしまいそうだ・・・。なんとかしなくては・・・青春の１ページを築かなくては・・・。

＞[7640] OREO様
元気そうでなによりです。いや～、おたくもソートー人情厚いお方ですなぁ（笑）。いいご兄弟をお持ちだこと。私も妹にたのんで買ってきてもらおうかなぁ・・・でも、妹は受験生だしなぁ（高校受験）。地道に努力しますか。

＞[7641] 実果様
全巻制覇おめでとうございます！！！！！いいなぁ～いいなぁ～、私も早く全巻ＧＥＴしたいなぁ～。
それに、ＣＨを買うために隣の県に行くなんざぁ基礎中の基礎です。全然おかしいことではございません。ご心配なく。ＣＨファンなら当然です。・・・たぶん。多分ね（苦笑）。

＞[7645] 千葉のOBA3様
そーですよね。あきらめた頃にはまた誰かが売りに行ってたりするかもしれませんね！前回（１４巻を探していたとき）もあきらめそーになったときに見つかりましたものね。まだまだ希望は捨てられません。
話しは変わるのですが、「千葉のＯＢＡ３」の「ＯＢＡ３」って何だろう？ってお風呂に入っているときにふと疑問に思ってしまって、私は普通に「オー・ビー・エー・スリー」って読んでて、「違うよなぁ・・・。」とか悩んでしまいました（お恥ずかしい）。
今はもう分かりましたよｖ「オバ」でしょ？｢オバ」。

＞[7647] TAKESHI様
文化祭ってなんだかワクワクしますよね。お祭り気分で騒げるし、達成感も気持ちいいですよね。私は１年生なので初めて経験する大学の文化祭がどんなものなのか、今からちょっとドキドキしています（笑）。楽しみだな～♪

＞[7649] りょうの愛人様
実写版ＡＨのリョウ役はこの俺“いちの”がいただいた！！！・・・・あっ！ごめんなさい！！もう言いませんっ痛ッ！！ハンマーはやめて！！ぅわっ！！四方八方からあらゆるハンマーが！！！！ぎゃゃゃゃややややぁぁぁぁあああああああああああああああああああああああああああああああああああああああああああああああああああ！！！！！！！！！！！！！！！！！！！！！！！！！！

＞[7652] おっさん様
ふうっ（額の汗を拭いながら）。危なかったぁ。危うく死ぬところだった・・・。あ、いや、こっちの話しです。
２７歳おめでとう。今日（昨日）１日はいかがでしたか？この１年がよい年でありますよう、遠い空の下よりお祈りいたしております。仕事がんばって！

### [7655] いも	2002.09.02 MON 23:17:49　
	
こんばんわ～。

一言だけ。
おっさんさま、お誕生日おめでとうございます！
いい年を過ごして下さいね。
おっさんさまの素敵な一年を祈って…。

レスはまた今度！では、いもでした。

### [7654] あきら（晶）	2002.09.02 MON 22:10:00　
	
これから忙しくなって、カキコの時間が少なくなるかも。でも、皆さんのカキコを必ず読んで続けています。
そして、初めての皆さん、あらためて挨拶します。私は台湾在住のあきら(晶)と申します。どうぞよろしくお願いします。

＞ＯＲＥＯさん
カナダに留学しに行くのですか。何年間かかりますか。大変なことは出るかもしれないが、がんばってください。また、何か面白い出来事とかculture shockのことをぜひ聞かせてくださいね。歯も早く治るように～～

＞たまさん、イニャンスキさん
リョウの本命はやはり香さんなんですけど、優希さんの気持ちをそんなふうに答えて、私はリョウの優しさと思いやりの人間性をいっそう感心しました。本当に有情な人だなと思います。香さんのフアンの皆さんもちょっと我慢して、見逃さないでください。（香もリョウの心の動きを分かってくれると思います。最後、飛行機を見送るときの会話を見てください。）

＞TAKESＨIさん、（金城　武と一緒ね&#10025;）

遊びに台湾に来たのですか、あるいは交換学生として？昨日、前、日本に行った時ホムステイしていただいたお父さんとお母さんが遊びに台北に来られました。西門町と淡水を案内しました。お母さんはこの二つの場所は日本のテレビも登場した気がするとおっしゃいました。特に「西門町」は有名だと。今、ちょっとした「台湾ブーム」があるとおっしゃいました。本当にそうなの？？？
臭豆腐の好きば外人さんは多くないですよね。（私も豆腐より付けた白菜の漬物が好き&#9825;　豆腐は揚げ物だから偶に食べました。＾－＾）
最近台湾で放送しているアニメはほとんど旧作で、台湾においても再放送のものです。たとえば、「め組大吾」、「彼氏彼女のこと」、「Ｈ2」、「steel angel-くるみ（？）」など。

私も「ＡＲＭＳ」を見ました。13巻のところまでかな。友達が借りてくれて、いま、学校が違うので、あまり会えないから……
また、8/9皆川亮二先生は台湾でサイン会を行いました。他にはいろんな先生が来られたが、私は怠け者で、まだ、時間の都合のため行かないが、クラスメートの従妹さんは、「前の日」から会場の外でキャンプした人を頼んで、代わりに列を並べてくれたとか、やっと皆川さんをはじめ、いろんな先生のサインを手に入れましたって。さすがサイン狙いの人！！自分はそんな根性、情熱と体力がありません。もう年を取ってしまいました。（Ｔ-Ｔ）

昔々、北条先生も台湾に来て、サイン会を開きました。弟に行かせて（時間が一番自由だから）、とうとうサインをゲットしました。今は兄弟全員の宝物。
北条先生、ぜひもう一度台湾に足を運んでくださいよ～～～～お願いいたします。(-人-)

很久很久以前，北条老师也来台湾开了签名会。让弟弟去(因为时间最自由)，终于拿到了签名。现在是兄弟全员的宝物。
北条老师，请您一定要再来一趟台湾哦～～～～。(-人-)


### [7653] 無言のパンダ	2002.09.02 MON 22:07:15　
	
こんばんわ★

私もちょっとひとこと。
おっさん様、２７歳のお誕生日おめでとうございます！
おっさんさまにとって、素晴らしい１年になりますように☆
ではまたのちほど、まろてぃにて(^_^)/~

### [7652] おっさん(岐阜県：有難うっしゅ！！）	2002.09.02 MON 21:44:56　
	
毎度、おっさんでごじゃいましゅ。皆様おげんこでごじゃいましゅか？

ちゅ～ことでレスレスでシュ！！
びぃ様（７６３５）・いちの様（７６３７）・たましゃま（７６３８）・ＯＲＥＯ様（７６４０）・実果様（７６４１）・千葉のＯＢＡ３様（７６４５）、有難うでしゅ！！今日弐拾七歳の誕生日を迎えました。ここでお祝いしてもらうのは２回目です。びぃ様・実果様、はじめまして！見た目は弐拾七歳には見えないらしいですが（マジです！若く見られるらしい）これからも、宜しくです！いちの様、おっちゅ～！！よかったっすか？ファルコン特製のケーキ。届いてるかな～ケーキ（＾＾）ＯＲＥＯ様、しやっす！ＯＲＥＯ１．２ｋｇっすか。いいっすねいいっすね。私が食べる前にオカンが食べちゃいそう（－－；）たまちゃま、おっつ～！！範囲内っすか？いいじゃないの！また、まろはんのＨＰでね。千葉のＯＢＡ３様、有難うでシュ！！弐拾七歳になりました。なんか、祝ってくれるのっていいっすよね。有難い。

ということで、これにて・・・

### [7651] 葵	2002.09.02 MON 21:28:38　
	
　せっかく書いた文章を、キーひとつ押し間違えて消しちゃった。（>_<;　仕方がないので簡略で…。

＞おっさんさま
　ＨＡＰＰＹ　ＢＩＲＴＨＤＡＹ♪おっさんさまにとって素敵な一年になりますように…♪

＞千葉のＯＢＡ３さま［７６４５］
　新宿行かれたんですか？私の定休は火曜なんですよ。ですから明日、行きます。（すみません、紛らわしい書き方で☆）私も御苑に行きたいのですが、明日も暑くなりそうで…。日焼けがコワイお年頃です。（^_^;

### [7650] 西本　麻依子	2002.09.02 MON 20:12:56　
	
こんばんは。最近AHの実写版の話が盛り上がっていますね。私は、冴羽りょうや槇村香などに、あう人はいないと思います。実写版だと声もちがうし。顔もちがうし。それはそれでいいかもしれないけど、やっぱりちょっと・・・。実写版よりも今は、アニメ化してほしいです。

### [7649] りょうの愛人	2002.09.02 MON 19:44:59　
	
皆々様　
私のたわいのない実写版の配役想定に
ＲＥＳくださいましてありがとうございます。
信宏は、小泉　孝太郎で皆様、御納得のご様子。決定かな？
誰か本人におしえてあげて。！彼は、ＡＨを読んでいるのかな？
結構　幼い時からお母上がいなかったから
ＡＨのファンにあるかも。
そしたらお父上（総理大臣）も読まれたりなんかしちゃって．．．．。
そうするとＡＨは、国民的漫画になります。（笑い）
ちなみに香が、財前さんと言うのも大賛成です。
あっ　いつまでまだこれに固執してるかって
ごめんなさい。

### [7648] りょうの愛人	2002.09.02 MON 19:38:49　
	
皆々様　
私のたわいのない実写版の配役の想定に
ＲＥＳくださいましてありがとうございます。
信宏は、小泉　孝太郎で皆様、御納得のご様子。
誰か本人におしえてあげて。彼は、読んでいるのかな？
結構　幼い時からお母上がいなかったから
ＡＨのファンになるかもね
そしたらお父上（総理大臣）読んだりしてＡＨは、
国民的漫画になります。（笑い）
ちなみに香が、財前さんと言うのも大賛成です。
あっ　いつまでまだこれに固執してるかって
ごめんなさい。

### [7647] TAKESHI	2002.09.02 MON 19:08:31　
	
今回のエンジェルハート驚きの展開でしたね！まさか槇村がCHだったとは思いもしませんでした。それにしても、リョウが香にちょっかいだしたり下着盗むところを見て思わず笑いました。早く続きが読みたいです！

＞[7637]いちの様
大学生なんですか、僕も来年は大学受験です。高校ももうすぐ文化祭ですよ。

＞[7646]ニャンスキ様
ジャンプで活動？もしかしてさいごのほうにあるネタを送るコーナーですか？

昨日リターナー見てきました。最高におもしろかったです！今までみた日本の映画で一番よかった。ぜひみなさんに見てもらいたいです。

### [7646] ニャンスキ	2002.09.02 MON 18:41:35　
	
こんにちは。９月ですね。

私は今日の４時〆切のレポートを、３：５０に学校で仕上げて提出に駆け込みました。今回ばかりはもう駄目かと思いました。腱鞘炎になりました。早くやらない私が悪いのです。
漫画家の人が〆切に慌てる時はこんな感じなのかな～。
時計の針と、じりじりと押し寄せる焦りとプレッシャー…。

今週のＡＨで思ったのですが、リョウが倒れていて香に助けてもらった場所と、ファ－ストキスの場所は同じみたいですね。香が「いつも酔うとここで寝てる」って…
あそこに行けば香が来てくれる、というリョウにとっての
想い出の場所なんでしょうか…！なんかゾクゾクします。

＞7640　OREOさま
私は以前は歯科助手だったので大変さはわかります…
抜歯は小さな外科手術ですもんね。まあでも皆やってることだし大丈夫。頑張ってください～。

＞7641　実果さま
初めまして。関係ないですが「実果」さんといえば好きな漫画「恋愛カタログ」の主人公を思い浮かべます♪
『香はりょうちゃんのすべて』って最高ですね！！
リョウの方からのアプローチ！期待できそうですよね。
アパートに押し掛けたのも香が忘れられないからでしょう！
私もＣＨと対照的なＡＨを楽しんでいます。今週の最後の台詞なんてＣＨじゃ絶対聞けない！それにＡＨならではの２人の絡みに釘付けです。また違った意味で２人の絆が深いんですよね。ＣＨではぶっちゃけ香の三枚目の多さが悲しく写ったりした時もありましたがＡＨでは大人で包容力があって
イイですよね～～（・∀・）イイ!!
浪人生でいらっしゃるんですか。暑くて勉強大変ですね。
がんばってください。

＞7638　たまさま
レスありがとうございます♪
実は私も「大使館前でのキスシーン」がダメなのでした…
私だけじゃなかったんですね（笑）嬉しいです。
まぁリョウは香ちゃんという大本命がいるから←（これが言いたかった）キス以上いけなかったでしょうね。ちょっとは心が動いてもね…（と言いきかす）

＞7625　TAKESHIさま
こんにちは。私は普段ジャンプの方で活動してるので←何の
同じくそう思います。ここ十年位で一番の踏ん張り所？
という気もします。こち亀はいいですね。私はジャガーさん
が好きです。でも読者のページと作者コメント読んで
先週はおわり…（……）ＷＪにはもっとがんばってほしいです。好きなので。

### [7645] 千葉のOBA3	2002.09.02 MON 17:03:45　
	
こんにちは！いやー、今日も暑いです！！蝉の声はだいぶ聞こえなくなってきたみたいですけど・・・。

７６３２　無言のパンダさま　大丈夫です。先週の疲労からぬけでました。問題は,木曜日、私の行ける時間内にバンチが届くか！？そこなんですよねー。パート出勤時間ギリギリの１２時くらいに行ってみようと思ってます。

７６３３　葵さま　え、今日新宿行きました？私も先ほどまで行ってました。あーでも私は今日西口のみでした。時間なくて。残念。東口から新宿御苑に行きたかったんですけどねー。

７６３４　おっさんさま　お誕生日おめでとうございます！！この１年がおっさんさまにとって,最良の１年でありますように☆

７６３７　いちのさま　何回かあきらめずに古本屋通うのもいいかもしれませんね。あきらめてたら、またあった！なんてラッキーなこともあるかもしれませんし・・・。幸運を祈る！

７６３８　たまさま　今回の台風すごかったみたいですね。ところで,タマちゃんはまた行方不明に・・・。いっそ江戸川にこないかなー。そしたらコナン君て呼べるのに・・・？（ダメ？）江戸川は今ハゼがいっぱいいてエサにはこまらないよーん。

７６４１　実果さま　２３巻は私もお気に入りでした。マリーさんの話も、翔子さんの話も、それぞれエピソードがすごくイイ！りょうちゃんの気持ちが伝わってくるお話ですよね。

＞ｓｏｐｈｉａさま　＞ＯＲＥＯさま　＞将人さま　ほか、歯の治療中の皆さん！治療がんばって！もー考えただけでも痛そう！

７６４２　ｙａｓｕさま　こんにちは。私も「こもれ陽・・・」大好きです。すっごくきれいなお話ですよね。もし映像にできたらステキだろうなーと思いました。

さー、晩御飯のしたくします。



### [7644] あおい	2002.09.02 MON 16:42:02　
	
すみません（；▲；）
漢字を間違えて変換してしまいました。
私だけ稼動かお聞きしたいのですが。ではなく→私だけかどうかお聞きしたいのですが。ですよね、あ～あ、パソコンを信じて送ってしまいました。すみません（＊▲＊；）

### [7643] あおい	2002.09.02 MON 16:40:06　
	
始めまして（＾o＾）／~~
シティーハンター大好きなあおいです。宜しくお願い致します。
突然なんですが、疑問を抱いているのは私だけ稼動かお聞きしたいのですが。確かシティーハンターでは香とりょうが初めて出会ったのって、香が高校生の時じゃないですか。ではＡＨでは一体何歳の設定なのでしょう？って考えてしまって。会社でもいつも見ている人がいるのですが､その人と一緒に考えています。皆さんはどう思ってらっしゃいますか？（長くなって申し訳ありません）（＞_＜）

### [7642] yasu	2002.09.02 MON 16:24:28　
	
みなさんこんにちは。９月に入ってもまだまだ暑い日が続きますねぇ。はぁ～…。

｢こもれ陽の下で…」を読み終えたんですけど、泣けます。いい話です。でも北条先生のおっしゃる通り少年誌にはむかない内容ですね。連載当時、小学生だった僕も金髪の宇宙人の闘う話や天才バスケットマンの話はよく読んでたけど｢こもれ陽の下で…」は記憶にないですし。いま思うともったいないことしてたって感じです。まだ読んでいない人はぜひ読んでみてください。
で、主人公の西九条沙羅ちゃんといえばＣＨにも依頼人として出てきましたよね。北条先生ってこの手のことよくやってますよね。Ｆ．ＣＯＭＰＯに香＆槇兄出てたりＲＮ探偵社の看板があったりとか。なのでＡＨに紫苑＆雅彦出演なんてことも期待しつつ毎週金曜日を楽しみにしてます。

最後に実果さんをはじめ全国の浪人生の皆さんがんばれ！大学はいいですよ。実は僕自身去年は浪人生だったのです。あっ、でもバンチによる息抜きを忘れずに♪

“在阳光下……”虽然看完了，但它让我泪流满面。是个好故事。不过正如北条老师所说，这些内容并不适合少年杂志。连载当时，还是小学生的我也经常读金发外星人的战斗故事和天才篮球人的故事，不过“在阳光下…”不记得了。现在想起来觉得太可惜了。还没有读过的人请一定要读一读。  
那么，主人公西九条沙罗在CH上也是作为委托人出现的吧?北条老师经常做这种事呢。F.COMPO上有香&槙兄，还有RN侦探社的招牌。所以，我很期待每周五在AH中看到紫苑和雅彦。


### [7641] 実果	2002.09.02 MON 08:40:32　
	
こんにちは。いやあすごいカキコの量で(汗)(嬉)
はじめましての方､実果と申します。ちなみに本名ではありません。浪人生なので､頻繁に書き込んだりは出来ませんが､
皆さんの投稿は全部読んでます。宜しくお願いしますね♪

今週と先週のAHを読んで。
香はりょうちゃんの『すべて』なんだと実感しました。
(この言葉以前千葉のOBA3様がおっしゃっていて、とても
気に入ったのでお借りしました♪）りょうちゃん素直ですね。さらにかっこよさ倍増。香ちゃん大人です。自分負けてる・・・。年上なのに。槇ちゃんは相変わらず優しいです。これからどうなっていくのでしょうね。愛することを求め始めた野良犬・・・って書いてあったのでりょうちゃんの方からアプローチ？(嬉しい)私も皆様と同じくこの出会い編がしばらく続いて欲しいですね。この二人の絡みがやっぱりとても好きなので。それが見たいが為にAHを読んでる気がしないでもないので。

　以前どなたか(すみません､忘れてしまいました。たま様？）が
AHはシリアスなシーンが多くて重いとおっしゃっていましたが､
私はCHと対照的なのでむしろ面白いと感じています。北条先生がお書きになりたいテーマがCHよりはっきりしていますしね。
それに、CHのりょうちゃんのもっこりには少ししつこさを感じて
いるので・・・。それがりょうちゃんのキャラなんでしょうが、確かにああいう部分も含めたりょうちゃんが好きなのですが、でもいくらなんでも好き放題やり過ぎでしょ！！香ちゃんもハンマー使い過ぎだし。TVのCHを見ていると､香ちゃんのハンマー使いが香ちゃんのメインになってる気がしてならないのです。だって香ちゃんは本当は優しい人なのに､その部分が見えない。それがすごく悲しくて。TVだから､一般うけしなきゃいけないから仕方のない事なのかもしれませんが。

　あと、遂にCH２３巻手に入れて全巻揃いました！わーい♪嬉しいな♪２３巻はマリーと翔子さんの回で､実は重要な巻だったんですね。私翔子さんの回お気に入りなんです☆
いちの様､諦めずに頑張って下さい！私は隣の県まで探しに出ましたよ！ただのバカ？

＞[７６３８]たま様
「A　LOVE　NO　ONE　CAN　CHANGE」良いですよね！！
私もサントラのドラマの中で一番好きです。「コルトパイソン３５７マグナム、こいつと相棒を組んでもう何年になるだろう・・・」って言う神谷さんの台詞から始まるんですよね～。
りょうが香に求めているもの、それは・・・続きは是非CDで！

最後におっさん様お誕生日おめでとうございまーす！私はあと２ヵ月後だ♪
