https://web.archive.org/web/20011121230819/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=8


##### [160] りなんちょ（実家は横浜）2001.07.05 THU 15:01:49  
うちの方は、バンチの発売が火曜日なんですが、  
月曜日の夜中（火曜日の午前２時頃）に、  
こそこそとコンビニまで、ダッシュで買いに行ってます。  
そして、読んで寝る…  
おかげで、次の日はいつも寝坊してます…  
かわいそーな、うちのダンナ…  
  
アニメ シティーハンター２のエンディングで、  
新宿の街並がでてきますよね。  
あたし、実際そこへ行って写真とったことあります…  
新宿御苑、高層ビル郡、  
はたまた、首都高まで乗って…  
もう、人目を気にせず、  
バリバリ、オタクってました…  
そんな、あたしの夢は、  
「新宿に住む」ことです…  
できれば、中心街に近いとこ…  
新宿大好き！！！  
新宿バンザーイ！！！  

我们家的Bunch发售时间是周二，周一半夜(周二凌晨2点左右)，我就偷偷摸摸跑到便利店买。  
然后，读了之后睡觉…  
托您的福，第二天总是睡懒觉…  
好可爱啊，我老公…  

在动画 城市猎人2的结尾，出现了新宿的街道。  
我实际上去那里拍过照片…  
新宿御苑、高层大厦郡、甚至是首都高速路…  
已经，不介意别人的目光，咔嚓咔嚓地，宅…（译注：待校对）    
那么，我的梦想是“住在新宿”…  
如果可以的话，去离市中心近的地方…  
最喜欢新宿了! ! !  
新宿万岁! ! !  

##### [159] chikka（Ｅａｓｔｅｒｎ Ｓｈｉｚｕｏｋａ）  2001.07.05 THU 14:40:49  
私も皆さんの真似をしてハンドルネームの横に「地名」を入れる事にしました。「Ｅａｓｔｅｒｎ Ｓｈｉｚｕｏｋａ」と打っていますが「静岡県東部地方」の事です。住んでいるところは「富士山」と「箱根山」に挟まったところです。天気がいい時は北西の方向に富士山が、東側には箱根山が、南側には天城山が、南西の方角には駿河湾が見えます。  
ほかのＢＢＳでも、こういう形です。でも、静岡県はあまり見当たりませんねえ。誰かいませんか。静岡県人は・・・。  
今回のエンジェルハートでは「グラス・ハート」の後ろ側に「アルタ」があったではありませんか。分かってはいましたが「アルタ」って、新宿駅東口を道路を隔てて反対側にあるのですね。ああいう風に描かれるとよく分かります。  

我也学着大家的样子，在昵称旁边加了“地名”。“Eastern Shizuoka”是指“静冈县东部地区”。我住的地方夹在“富士山”和“箱根山”之间。天气好的时候，西北方向可以看到富士山，东侧可以看到箱根山，南侧可以看到天城山，西南方向可以看到骏河湾。  
其他的BBS也是这种形式。但是，静冈县不怎么能找到。有人吗?静冈县人……。  
在这期的天使心中，“Angel Heart”的后侧不是有“Alta”吗?我知道“Alta”在新宿站东口对面，隔着一条马路。那样画就很清楚了。  

##### [158] 実葉（世田谷区）  2001.07.05 THU 14:28:35  
私は毎週、すぐ近くの小さなお店でバンチを買っているのですが、入荷は２冊ということがわかりました。  
これまでは入荷とほぼ同時に買いに行っているので、ばっちりですが、この先、いつ買いそびれるかわからないので、おじさんに「予約」を入れておこうか、と思っています。  

我每周都会去附近的小店买Bunch，但进货只有两本。
到目前为止，我几乎是一到货就去买，所以很稳妥，但不知道以后什么时候能买到，所以我想向大叔“预约”一下。

##### [157] おさる（愛媛のポンジュース）  2001.07.05 THU 13:16:12   
知らない間にラブラブになっちゃって。  
あ～ゆうのりじゃないと君は、チュウができないのか！(^o^)  
と、一人つっこみをやりつつ...。  
ふとリョウの顔を見れば頬がこけちゃって....  
なんか、回想シーンの香を見てたら  
「私は幸せでーす。」オーラがでてるし。  
あー、1週間がながいいいいいいいいいーっす。  

在不知不觉中变得很恩爱。
啊，你不是りじ，就不能做チュウ吗!(^ o ^)（译注：待校对）  
一个人一边吐槽…。（译注：待校对）
突然看了看獠的脸，脸颊都瘦了....
看了回想场景的香，
“我很幸福。”她有一种幸福的光环。
啊，一周好长啊好长好长啊。

##### [156] れもん（東京）2001.07.05 THU 13:11:33  
みなさんのマネをしてやってみちゃいました、出身地。  
でも「東京」というのはなんだかちょっと味気ないなあ。  
ま、２３区外の私はそんなに「東京」を意識しないのですが。。  
さて！ほんとにカキコがめちゃめちゃ多くてびっくりです！！  
こんなにハイペースな掲示板には初めて来ますよ～。  
目が回る～。（笑）  
で、確か「夏兎」さんだったかな？がカキコしてた内容に、  
思わず反応！  
西東京市にお住まいなんですねー。私はそこの市内にある  
某高校に通っているのですよ。いやあ、近い近い！！  
そして、なんとなくプライベートな話になっちゃうかも  
しれませんが、私の友人が北条先生の娘さんと友達らしい。。  
中学が一緒だったとかなんとか・・・。うらやましい～。（笑）  
うわ、思いっきり関係無い話をしてしまった。。悪い癖だ。

我模仿大家试着做了，写上了出生地。  
不过「東京」总觉得有点索然无味。  
好吧，我没有「東京」的意识，因为我住在23区之外。。。  
那么!帖子真的是多得不得了，吓了一跳! !  
第一次来这么快节奏的公告栏哦~。  
眼花缭乱~。(笑)  
那么，我记得是「夏兎」-san吗?对写的内容，不由自主地反应!  
您住在西东京市。我在市内某所高中上学。哎呀，好近好近! !  
然后，可能会聊到私人话题，我的朋友好像和北条老师的女儿是朋友。。  
初中在一起什么的…。好羡慕~。(笑)  
哇，完全说了些无关的话。。这是坏习惯。  

##### [155] ぽむりん（神奈川）  2001.07.05 THU 13:00:35  
初めて書き込みします。  
ＡＨ最初は悲しいだけで、読むのが辛かったのですが、最近は気持ちも落ち着き冷静に読めるようになりました。初キスのお話はじ～んとしました・・・・香は幸せだったんですね。うぅ、来週のお話が気になる！  
  
北条先生、お身体にはくれぐれも気を付けて連載がんばってくださいね。楽しみにしています。

第一次写。
AH一开始只是很悲伤，读起来很痛苦，但是最近心情平静下来，能够冷静地读了。初吻的故事让我很开心…香很幸福呢。嗯，很在意下周的故事!

北条老师，请一定要注意身体，努力连载哦。我很期待。

##### [154] 前川（東京に住みたい）2001.07.05 THU 12:54:02  
ＨＰリニューアルおめでとうございます！！！  
いや、いつＢＢＳが復活するか楽しみにしていたのです。  
書き込みが遅れたのが悔しい・・・。  
それにしてもＡＨ、はまっています。  
一話を読んだ時は悲しくて悲しくて仕方がなかったですけど・・・。  
毎週、毎週楽しみにしています。  
海坊主サンの今後の活躍かなり期待！  
それとミック・エンジェルさん出てきてほしい！  
更新後の初書き込みでした。  

恭喜HP更新! ! !  
不，我很期待BBS什么时候能复活。  
很抱歉我写得太晚了…。  
但是，AH，我还是被迷住了。  
看了第一话的时候悲伤得不得了…。  
每周，每周都期待着。  
非常期待海坊主今后的活跃表现!  
还有，希望Mick Angel出现!  
这是更新后的第一次留言。  

##### [153] よしき（狭山茶）2001.07.05 THU 12:23:40  
ＨＰリニューアルおめでとうございます。そしてお久しぶりです。＾＾  
昨今の僚と香のラブラブぶりにうちのめされまくってます（笑）  
嬉しいけどなんか悲しいというか…（これがＣＨじゃないと言われるのが）ともあれ暑い日が続きますがお体に気をつけて頑張ってくださいｖ  
ところでバンチの漫画ってアニメ化しないのかな。（かなり希望）

恭喜HP更新。还有好久不见。^ ^  
被最近的僚和香的甜蜜所吸引(笑)  
既高兴又难过…(有人说这不是CH)虽然炎热的日子还会持续，但是请注意身体加油v  
话说Bunch的漫画不动画化吗?(相当希望)  

##### [152] 来生（ΦΦ）  2001.07.05 THU 12:09:09  
ところで・・・  
次から次へと管理人さんに依頼して申し訳ないのですが・・・  
ｉ－ｍｏｄｅ用のページとか・・・  
作る予定はないでしょうか？  
できれば・・・待ち受け画面を、掲示板のとなりにある  
１００ｔハンマーにしたり、ＡＨの文字にしてみたり  
したいなぁ～・・・っと・・・。  
  
是非、ご検討願います！！！！  

话说回来……  
一个接一个地委托管理员，很抱歉……  
有制作i-mode网页之类的计划吗?  
如果可以的话···我想把待机屏幕变成BBS和AH字符旁边的100t锤子...。  

请一定要考虑！！！！

##### [151] 来生（ΦΦ）  2001.07.05 THU 12:04:29  
♪はむとらおさん＆りなんちょさん  
  
ハンドルネームの横の地名についての謎、解けました！！  
レス、ありがとうございました！！  
はむとらおさんは博多なんですネッ  
私も福岡出身者です☆  
確かに、ここのケイジバンは、ネットさんのおっしゃるとおり、九州人が多いですネッ  
どーでしょう？  
福岡弁でのかきこみっていうのは？？？  
福岡弁と言わず、それぞれの地方弁のかきこっていうのも  
おもしろそうですよねぇ～（？）  
  
Ｗｅｂ新潮、遅らばせながら読みました。  
北条先生のエンジェルハートに対する熱意など伝わり、  
私もこの先がとても楽しみになりました！！  

♪はむとらお-san＆りなんちょ-san  

关于网名旁边地名的谜团，解开了! !
谢谢回帖! !
はむとらお的故乡是博多。我也是福冈人。
确实，这里的公告栏如Net所说，九州人很多，对吧?
用福冈方言写的是什么? ? ?
不说福冈方言，把各个地方方言写下来也很有趣呢~ (?)

Web新潮，我读了，尽管迟了。  
北条老师传达了对Angel Heart的热情等，我也变得非常期待今后的作品! !

##### [150] りなんちょ(静岡に近い神奈川）  2001.07.05 THU 11:31:06  
何故、ハンドルネームの後に地名があるのかって？  
あたしは、北条先生がやっていたので、  
真似しただけです…  
みなさんもそーですよね！？  
もしかして、あたしだけ…？  
  
ネットさん情報だと、九州の方々が多いよーですが、  
神奈川県の西部地方の方いらっしゃいませんかぁ～？  
特に、静岡に近い！って方…  
  
エンジェルハートを読んでると、冴羽さんと香さんは、  
とって～も、幸せだったんだなぁ～  
と、ひしひし感じます。  
香さんに対する冴羽さんの接し方…  
香さんの冴羽さんへの想い…  
お互いの本音がはっきりでていて、  
あたしとしては、この現実に  
けっこう満足しています。  
たしかに、香さんがいないのは  
とても悲しいことだけど、  
冴羽さんと香さんお互いに対する愛が、  
永遠なものとゆーのには、変わりはないから…  
何か、２人のラブラブさに  
うれし涙がでてきちゃう…  

为什么网名后面会有地名?  
我只是模仿北条老师做的…  
大家也是这样吧!?  
也许，只有我…?  

从网上的信息来看，九州的人比较多，有没有神奈川县西部地区的人呢?  
特别靠近静冈!的人…  

读Angel Heart的时候，我深切地感受到冴羽和香小姐，都是幸福的啊。
冴羽先生对待香小姐的方式…  
香小姐对冴羽先生的感情…  
彼此的真实想法都很清楚，我对这个现实相当满意。  
确实，香小姐不在是很悲伤的事，不过，冴羽先生和香小姐对彼此的爱是永恒的事实。  
不知为何，2人的缠绵让我泪流满面…  

##### [149] ＴＡＫ（東京）2001.07.05 THU 11:06:43  
どーもはじめまして。TAKです。初めての書き込みっす。北条先生の作品はCITY HUNTERから知ったんですけど、その後CHのみならず、HOJO WORLDに引き込まれ、今では作品の全部を持ってるかと（まぁここではそんなの当たり前なんだろうなぁ）。  
で、さっき来生さんが書いたのよんでたらなんかハンドルネームに地名いれるのが流行ってるみたいなんで、いれてみました。これが更なる流行を呼ぶとうれしいいなぁ。  
さてさて毎週火曜日は一限のドイツ語の時間にコミックバンチを楽しく読んでますが、毎週ANGEL HEARTは特に楽しみ。この後先生がどういじるのかワクワクして、なんかイツ語の単位がくるかが微妙になってます（関係ない？）。

多多，初次见面。是TAK。第一次写。北条老师的作品是我从CITY HUNTER那里知道的，后来不仅是CH，还被HOJO WORLD吸引，现在已经拥有了全部作品(在这里这样是理所当然的吧)。  
刚才看到来生写的帖子，好像很流行把地名加在网名后面，我就加进去了。如果这个能引起更大的流行就太好了。  
每周二的德语课上，我都很开心地读Comic Bunch，每周的ANGEL HEART是我最期待的。我很想看看老师之后会如何调整，不知什么时候能拿到语言学分也很微妙(无关?)。

##### [148] はむとらお（博多）2001.07.05 THU 10:59:47  
おはようございます。今日も朝から福岡は３０度を越えて大変な暑さです。  
＞来生　様  
皆さんの名前の横に生息地（？）が記載されているのは、前のＨＰの時に、「そういう風に書き込んだら面白いね」となり、北条先生ご自身も、書き込んでらっしゃいました。そんなこんなで浸透したんですよ。  
全国各地に広がっている感じがしていいですよね。  
  
ところで、今週のバンチ、実はどこも売り切れで読んでない！！皆さんの、リョウと香のキスシーンに関する感想を見るたびに、【うっわ～～～みたい～～！！】とひとりもがいております。(^^ゞ　周りに聞き込みして、手に入れた人に貸してもらおうと思いつつ、仕事に戻ります・・・

早上好。今天从早上开始福冈就超过30度，非常热。  
＞来生　様    
大家的名字旁边被记载的是所在地(?)，在之前的HP的时候有写，“那样写的话很有趣”，北条老师自己也写了。这就是它的传播方式。 （译注：待校对） 
有遍布全国各地的感觉，真好。  

对了，我还没看这周的Bunch，因为它居然在各地都卖光了! !每次看到大家对亮和翔的吻戏的想法，我都会独自挣扎，【呜呜~~我想看~! ！】我一个人在挣扎。 (^^ゞ我去打听一下，找有这本书的人借给我，同时我回去工作...  

##### [147] ネット(熊本）  2001.07.05 THU 10:18:34  
気のせいでしょうか。  
吸収大陸（まちがえました。どんな大陸だ）もとい、九州大陸の方が  
多いように思います。なんだか嬉しい。  
  
それにしても、伝言板を見つけたグラスハートが  
コミックバンチを見つけた時の私にそっくりで（マジで）  
びっくりしちゃいました。  
  
どこに何があるのか判る・・・知ってる・・・  
足が・・・勝手に動く  
そこに引き寄せられるように・・・  
あっ・・・ああ・・・これが・・・・  
これで・・・やっとコミックバンチに・・・会える・・・！  
  
  
  
・・・・・・・・・・・すいません。  
毎週こんな感じです。  
まだ発売されてないのは判っていても  
火曜日にコンビニをチェックしちゃったり、  
火曜の深夜にコンビニで張り込みをして  
何時に雑誌が搬入されるのか調べたり（おお、リョウのようだ！）  
するのはたぶん私だけではないはず。  
なんだか学生の本分忘れてますね、私。  
  
＞管理人様  
ＢＢＳが復活したばかりなのに、迅速な対応ありがとうございます。  
これからのこちらのＨＰにお邪魔させていただきまので  
どうぞ宜しくお願いいたします。  
  
是心理作用吗?  
吸收大陆(错了。是什么样的大陆)不过，我觉得九州大陆比较多。总觉得很高兴。  

即便如此，发现留言板的玻璃心和发现Comic Bunch的我一模一样(真的)吓了我一跳。  

我知道它在哪里……我知道……  
我的脚……会不由自主地移动，就像被吸引到那里一样……  
啊……啊……这就是……  
这样···终于能见到Comic Bunch···!  

对不起。  
每周都是这样。  
虽然知道杂志还没有发售，但周二还是会去便利店检查，周二深夜还会在便利店蹲点，调查杂志几点运进来(哦，好像是獠!)  
这样做的应该不止我一个人。  
我好像忘了学生的本分。  

＞管理员  
BBS刚刚复活，感谢您迅速的应对。
今后我要去这里的HP，请多多关照。

##### [146] 来生（ΦΦ）  2001.07.05 THU 09:21:10  
みなさん・・・なぜハンドル名の横に地名が・・・？  
  
昨日バンチ読みました。  
あんな形でしたが、Ｃ．Ｈ最終話以降の香とリョウに会えて  
涙が出てきました・・・。（；＿；）  
初キッスって事は、それ以降沢山キッスしたのでしょうか？  
赤くなってる香を見れて、ハンマーもってる香に会えて、  
そんな香を思い出してるリョウがとても寂しそうで・・・、  
とっても切ない今週号でした。  
  
♪管理人さま  
  
ネタバレ防止策、ご検討頂けるみたいでありがとうございます！  

各位…为什么Handle旁边有地名…?  

昨天读了Bunch。  
是以这种方式，但当我在C.H.... 的最后一集后看到香和獠时，我哭了···。（；＿；）  
既然是第一次接吻，那以后是不是有过很多次接吻呢?  
看到脸红的香、见到拿着锤子的香，獠想起她的时候显得很伤心···，非常难过的本周号。  

♪管理员先生  

感谢您讨论防止剧透的对策!  

##### [145] Ｂｌｕｅ　Ｃａｔ　（　愛知　）2001.07.05 THU 07:05:58  
　あの～、Ｗｅｂ新潮にリンクしてＨＰに入ると、右上の方にクイックメニューってのがありませんか？そこをクリックして下にスクロールすると、波、ってのが出てくると思います。雑誌の名前です。それをクリックすると今度は目次みたいのが出てくるんで、北条さんのインタビューを選んでクリックすれば、見られると思いますよ～。

那个，链接到Web新潮进入HP的话，右上角有Quick Menu吗?点击那里向下滚动的话，会出现波，那是杂志的名字。点击那个的话这次会出现目录一样的东西，如果选择点击北条的采访的话，应该能看到哟~。

##### [144] スナフキン  2001.07.05 THU 06:04:17  
皆さん、北条先生はじめまして！  
掲示板、復活したんですねぇ。これからもたまにお邪魔させていただきます。  
今回はとりあえず顔みせということで。(^^

各位，北条老师，初次见面!  
公告栏复活了呢。今后也会偶尔来打扰您。  
这次姑且先见个面。(^ ^  

##### [143] 沙り  2001.07.05 THU 03:57:08  
初めまして！沙りといいます。初カキコにドキドキです。  
だって、大好きな北条司さんのＨＰなんですもの！！  
これからもよろしくお願いします♪  
創刊からもう、８号なんですね。早いですね。創刊日に駅のホームで泣いたことがウソみたいです。もう、ほんと火曜日になると胸がいっぱいで･･･でも、待ち遠しいんですが、笑  
私はちゃんとＴシャツはゲットしました♪ジッポもめちゃくちゃ欲しいけど･･･値段が、うぅ。でもきっと、運良く買えることができるなら買っちゃうでしょう(^^;  
北条司先生、これからも、エンジェルハートがんばってください！！

初次见面!我叫沙り。第一次写帖子很激动。  
因为，是我最喜欢的北条司先生的HP ! !  
今后也请多多关照  
创刊至今已经是第8期了。真快啊。创刊日在车站的月台上哭的事好像是假的。已经，真的一到星期二就很激动……但是，我已经迫不及待了，笑  
我好好地买了T恤♪Zippo也想要很多…不过价格，唔。但是，如果运气好能买到的话一定会买吧(^^;  
北条司老师，今后，Angel Heart也请加油! !  

##### [142] モネ  2001.07.05 THU 03:50:38  
こんばんは。新潮の北条先生の記事を見つける事が出来ません・・。ばかみたいです。がんばって探していますが、どこらへんにあるかどなたか教えてくださると嬉しいです。もう見れないのでしょうか？私もAHが始まった時は本当に寝こむかと思うぐらい落ちこみました。いろいろな友達にメールしてしまいました。香を失ったリョウを見るのがすっごくつらくて、今でも本当はまだ生きているのでは？と思ってしまう自分がいます。本当に幸せになってほしいので、北条先生宜しくお願いします。

晚上好。找不到新潮的北条老师的报道。像个傻瓜。正在努力寻找，如果有谁能告诉我在哪里的话我会很高兴的。已经看不见了吗?当AH开始时，我非常沮丧，我真的以为自己要睡着了。我给很多朋友发了邮件。看到失去香的獠，我非常难受，甚至现在我都怀疑他是否真的还活着。我有这样想。我真的希望他能幸福，所以就拜托北条老师了。

##### [141] 天使の心  2001.07.05 THU 03:19:21  
新潮の、北条先生の記事を読みました。記事を読んでいて  
少しは、この作品を理解でき、同時に、別物・・・と思う  
ようにしていこうと思いますが、やっぱりまだまだ時間が  
かかるかなぁ・・・。久振りに、CHを読み返してしまいま  
した。香ちゃんがいないんだって思ったら、やっぱりウルウル  
してきてしまった。過去でもいいから、もっと香ちゃんの  
事描いてほしいなぁって思います。

读了新潮的北条老师的报道。读了报道，稍微能理解这个作品，同时，我将努力把它当成不同的东西···，不过，.但我仍然认为这需要更多的时间···。好久没读CH了。一想到香不在了，我还是忍不住流下了眼泪。我希望你能多画一些香的故事，即使是在过去。
