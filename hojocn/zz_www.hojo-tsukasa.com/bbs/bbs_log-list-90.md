https://web.archive.org/web/20020625010637/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=90

### [1800] まみころ	2001.07.29 SUN 01:21:11　
	
お帰りなさいsweeper様。
もっこり研究会会員としては、以前法水様［1758］がおっしゃってた海外でのもっこりの事のコメントに興味がそそられました。
フランスのサイトでもっこりが｢coucou｣と書かれてたとか･･･。
しかも意味がカッコウとか鳩時計とか書いてありましたよね？
(違ったらごめんなさい･･･、汗)
という事は、教授が水飲み鳥をつけたのは意外に遠からずって事なんでしょうかね？(笑)

### [1799] みみごん	2001.07.29 SUN 01:10:11　
	
はじめまして
この掲示板はリニューアル前から見ていましたが、書き込む
のは初めてです。

今週（もう先週かな？）発売のバンチワールドＣＨ Vol.15で
リョウが吸ってたタバコを捨てるシーンがありますが、その吸
い口、かんであるの、気づかれました？
ＡＨの中で、香にのっとられた（？）ＧＨが言ってますよね。
「彼の癖...　吸い口をかむ癖...」って。本当だったんですね。

それと、リニューアル前、伊倉一恵さんの出演してた役の話が
出てたのおぼえてる方、いらっしゃいます？
変わったところで、「クレヨンしんちゃん」の "埼玉紅さそり隊
深爪のお竜" なんていうのがあるんですよ。知ってらっしゃいました？

最近、重い話が多かったので、あえてこんな事書いてみました。

先ほど、「週間ブックレビュー」見ました。
北条先生の顔、本当に自画像そっくりですね！

刚才看了「每周Book Review」。
北条老师的脸，真的和自画像一模一样啊!


### [1798] 優希	2001.07.29 SUN 00:18:06　
	
見ました。先生の指輪に何故か目がいってしまいました(^-^)
初めて先生の声聞きましたがやはり優しい感じですね。
もう少し長ければよかったのにと思いました。

### [1797] ワタル／Wataru（宮城県）	2001.07.28 SAT 23:52:37　
	
【今晩のＢＳとスクープ２１の内容教えて下さい】
家ではＢＳが見れないので…。スクープ２１はそういう内容や
るって知らなくて見逃してしまいました。どなたかレポートして
下さると嬉しいです。

【ウイルスSIRCAMについて】
どなたか書き込みをされていましたね。すみません名前忘れてしまいました。紹介されていたＨＰは「IPA情報処理振興事業協会」
でしょうか？ そこには英語版もあるのでそこのＨＰのアドレス
を下で紹介しておきますね。

【Will someone report today's BS program and SCOOP
21?】
I can't see BS programs at my house. I didn't see
SCOOP 21 because I didn't know it was about COA MIX.
Thanks in advance.

【Compuer virous SIRCAM】
A new virous SIRCAM is prevailing. It sends your
documents at random to many and unspecified people.
What is worse, it will destroy all the data on your
hard disc on October 16th. Make sure not to open
unfamiliar mails. If you want more information, access
to the following HP. It has an English version.
http://www.ipa.go.jp/

### [1796] 法水	2001.07.28 SAT 23:51:30　
	
［週刊ブックレビュー］
ほんとに“ミニ”ドキュメントでしたねー（笑）。内容的には先月の「スクープ２１」のダイジェスト版といったところでしょうか。今回気になった一言⇒「かつての人気キャラクターたちを前面に押し出した」。うーむ、やっぱりそうなのか？？　なんか北条先生個人としてはＡＨをＣＨの続篇とくくられたくなくて、コアミックスの取締役としてはＣＨのキャラクターをバンチ売上のために利用している（そのあたりkaoriさんも批判されていることですが）というジレンマに陥っているような…。

［「キャッツアイ」の謎］
５話を読み返していて「おや？」と思ったのが１４ページの１コマ目リョウが海坊主に言う「裏家業早々に引退することなかったんじゃないのかぁ？　もったいないぜこんな似合わない店やるよりは……な」という台詞。なんかこの台詞だと裏家業を引退してから「キャッツアイ」をはじめたように取れませんか？？　ということはやはりこの店は美樹と一緒にやっているのではなく、海ちゃんが一人で引退後の余生を過ごすためにはじめた店で、ＣＨでの「キャッツアイ」とは（もちろんＣＥの「キャッツアイ」とも）別物ってことでしょうか？

### [1795] EMI	2001.07.28 SAT 23:44:30　
	
ふにゃ～。ショックです～！ラスト10分前なんて～。ぎりぎりでパソに戻ってしまいました。SUN FLOWER様のレスを見てびっくりして固まってしまいました。しょぼん。皆様の感想レスでショックを和らげたいと思いますので、是非、教えて下さいね。（根性無しの私...。ファン失格です～。）

### [1794] SUN FLOWER	2001.07.28 SAT 23:37:16　
	
”週間レビュー”たった今観終えました。
　ふふ、北条先生、このHPのイラストにそっくりですね！！
番組のラスト１０分くらいでしたが、コアミックスの中で
皆様が働いている様子や先生のコメントが見れて感動でした。
　バンチ、０号がら拝見させていただいていますよお。
もう、それぞれに”泣き”が入ってます。純粋な気持ちになる物語りが多すぎて忘れていた気持ちが蘇ってきてます。
　皆様、お体には気をつけてください。
　北条先生、AH,毎回綺麗になっていきますね。
　私も”いい女！”をこの夏はめざそー！！


### [1793] EMI	2001.07.28 SAT 23:34:39　
	
こんばんは。あの...、先程までBS2で北条先生が出ると思って見ていたんですが何かあのいなかったんですけど....。私、チャンネルまちがえたのかしら...？「週刊ブックレビュー」...でしたよね？あれ？皆様、出演されたの見られました？

### [1792] Goo Goo Baby	2001.07.28 SAT 23:15:53　
	
今ＢＳで北条先生出演中？　皆さん今パソではなくＴＶに向かってるの？
い、いいのさ別に。ビデオ撮ってるからあとでゆ、ゆっくり見るさ

＞テムリンさん［１７８９］（←フ○ンス革命）、大丈夫です！覚えてますよあなたのこと！（＾＾）
一騒動あって、常連さんの何人かの方々が姿を見せなくなってしまわれたせいか、ログの回転も遅くなってしまい、寂しかったんです　あなたも姿を消したうちの１人かなと思っていましたが、安心しました　

＞よしきさん［１７８６］、ログイン時にパスワードをいちいち１コ１コ入力してるんですか？　余計なお世話かもしれませんが、パスワードはパソコンに保存しておくと、会員番号を入力した時にフツ～に出てくるんで、暗記しなくてすみますよ（＾＾）

### [1791] こいけいこ	2001.07.28 SAT 21:21:08　
	
こんばんわ！　2回目の登場です。私は出遅れてしまいやっと最近からバンチを読み始めたのですが、作者さんたちの一言を期待してたのでそれがなかったのがちょっと残念です…　しかしあまりこういう週刊誌を読んだ事がなかったもので他の雑誌もそうなのかな…唯一読んでた週間ジャン○では毎週載っていた先生の一言をけっこう楽しみにしていたもので。
なんか的外れな事言っていたらスイマセン

### [1790] Closter Zeckt	2001.07.28 SAT 21:01:41　
	
こんばんは。かなり久しぶりにきました！
まだ過去のログを拝見してないんですが、もうすぐ１００にいってしまいそうですよね！！すごく驚きました！！

AH毎週楽しみにしてますが、創刊号を見逃してしまったので総集編が発売されるのが待ち遠しいです。単行本の方も早く発売される事を祈ってます（ちょっとおおげさかも・・・？）

ではまた

### [1789] テムリン（覚えてる方・・いないよなぁ）	2001.07.28 SAT 20:52:44　
	
お久しぶりです～。
なにやら知らない間に一騒動あったみたい？ですが、気にせずに←おい！？　行きましょう。

（総集編について）
びっくりした！っていうのが一番最初の感想ですね。
今まではコミックスが出るまで、まだかまだか首をながーーーくして
待っていたのに、総集編！
私的にはすばらしいアイディアだと思いますよ。
私は・・並べられてたら買ってしまうでしょう。きっと。
だってバンチとかの週刊誌って、なんだか読みにくいし（厚い）、インクが手に着くのがなんともイヤなんですよね。
基本的に好きなマンガは何度も何度も読み返すタチなので、
読みやすそうな総集編には誘惑されますね～。

### [1788] 法水	2001.07.28 SAT 20:14:46　
	
［総集編］
総集編出すのに別に理屈はいらないと思いますけどね。コミックスだとカバーのデザイン、装丁などいろいろ時間がかかるし、それより一足先にという読者サービスだと思います。バンチに載っている漫画で最初にＡＨの総集編が出るということはそれだけ読者からの要望も多かったということで素直に喜べばいいのでは？

［美樹はなぜ出てこない？］
僕はＡＨをＣＨの完全なる続編とは考えてないので特に違和感はありません。多分以前の書き込みでもあったと思いますが、ＡＨの世界はＣＨの世界のパラレルワールドなのでしょう。大体、年齢の設定からして矛盾が出てきてしまうし、海坊主はガングロだし（笑）、つながっているようで完全にはつながっていない。海坊主の指には指輪もありませんから、今後も美樹が出てくる可能性は少ないかな…（まさか離婚したなんてことはないでしょうけど）。

### [1787] Goo Goo Baby	2001.07.28 SAT 19:54:08　
	
お久です　
最近ログの回転メッチャ遅くなりましたよね　何でしょうねぇ

タコ坊主さ～ん！
ＡＨ初登場の時の、手だけ出演の時は色が白かったのに、次に出演の時はナゼ黒くなってるの？
あの短期間にアナタに一体何があったの？？
つ～かリョウ達は、ンなコトまったく気になってないというか、それとも気付いてないとか、そんなカンジなんだけど。
いや、気付いてないなんてコトは無いよねぇ。

私が以前アナタにつけたあだ名「黒タコ」。ホントにタコ焼きになるとは思わなかった　リョウもそのことを予測して「タコ坊主」ってつけたのかなぁ

単行本が出る前に総集編かぁ・・・
ＣＯＡＭＩＸさんよ、実は何か作戦があるんですね？
なるほどね～、前代未聞のスタイルの漫画雑誌ですもんね♪（←勝手に納得して勝手に解釈してるし）
つ～ことは出版物の出し方も前代未聞のやり方にする作戦なんでしょうか？　んん～

### [1786] よしき（今日涼しいですね）	2001.07.28 SAT 19:20:58　
	
やったね！やっとパスのメモ見ずに入力できました♪（アホっぽい（笑））
＜総集編
表紙買いですかね…でもAHを遅く知った友人に勧めたいと思ってます。あとなんか先生のコメントも載ってれば、生の意見が聞きたい今の時期にはちょうどいいのではないかと…。（でもなかったらちょっとかなし～～）
関係ない話ですが、この間「ルパン三世Ｙ」の総集編買ったら、クレヨンしんちゃん風ルパンにキャッツアイもどきみたいな三姉妹がいました。（笑）パロディとはいえ、ああいうのって先生の許可とってるのかな？とる必要はないのかな。とりあえずアジトが喫茶店じゃなくてよかった。（笑）すごくおもしろかったし。

余談。李大人撃ったのは新キャラ？希望～♪
もうそろそろ新レギュラー出てきて欲しい感じです♪

### [1785] sweeper(帰って来たよー♪)	2001.07.28 SAT 18:05:43　
	
遅れましたが、ＡＨ１１話の感想です。

ＡＨ１１話読みました。
一言で言うと切ないです。
今回読んでいて思ったのですが、りょうは「一人の女が亡くなった今でも愛している男」として描かれていましたよね。私には、そう見えたのですが。「香はいない．．．戻ってきゃしないんだ！！」とか「香は香だッ！！香は一人だッッ！！」というりょうのセリフがすごくそれを物語っている気がしました。

＞初カキコの皆さん。
はじめまして。sweeperです。よろしくお願いします。

＞まみころ様。［１５４３]
もっこり研究会会員のsweeperです。
さて「動くもっこり」ですが、あれはりょう本人曰く「もっこり輪投げ」のことですね。確かにあれはイキイキと動いてました。
しかも器用に輪投げの輪をよけてましたよね。これには、笑ってしまいました。
「動く」については、イキイキとまではいきませんが、かずえ初登場の話で毒蜂の解毒薬の副作用で上下に動いていたのがありました。(水のみ鳥の状態。)私はもっこりに水のみ鳥の頭をつけたのは、教授だと思っています。でも、この話ってもっこりのパターンがいくつかありましたよね。

「波乗りジョニー」をバイト先の有線で聴いていたら、ＡＨのりょうを思い出しました。特にサビのところが．．．。(言葉にならない。)

カキコ長々と失礼しました。
それでは。

### [1784] ミラクルハート	2001.07.28 SAT 17:47:48　
	
＜総集編＞
総集編出るんですね。ＡＨにＣＨが絡んでいるということを後から知られた方が結構いらっしゃるようですし、創刊号から読んでる私でもすごくうれしいです。２９０円という値段なら、興味はあるけど、高いコミックスを買ってまでは読まないというような方も気軽に手にできるんではないでしょうか？そうしてＡＨやＣＨはもちろん北条先生のファンの輪が広がればいいなと思います。コミックスも出るのなら、希望としてはあまり大きくなくて、読みやすい素材のものがいいです。でももしコミックス化されなくても、最近ネットの普及を含めいろいろな理由で出版業界にも変化がでてきたということをちょっと耳にしたことがあるので、コアミックスさんの新しい試みということで納得できるような気がします。

### [1783] 優希	2001.07.28 SAT 16:22:16　
	
ジッポー来ました。可愛いです。使うの勿体無い感じ。次のグッズはお値段手ごろなものがいいなぁ。

### [1782] neko.mikan	2001.07.28 SAT 15:37:36　
	
初めての投稿です。よろしくお願いします。

シティハンターの大ファンで、ビデオに録画したものを飽きることなく何回も見ています。だって楽しいだもん。d(⌒o⌒)b

ところで最近は家庭にもビデオだけではなく、ＤＶＤも見られるようになってきたと思います。そこでいつも思うのですがシティハンターはなぜＤＶＤ販売されないのかと。

昔のアニメがＤＶＤ化されている中、人気のあるこの作品がＤＶＤにならないのはとっても残念です。ＢＯＸ発売されたら、絶対買いに行きます。（＾O^）/

みなさまはどう思いますか？

### [1781] EMI	2001.07.28 SAT 13:48:13　
	
＞zippo到着！
やっと、本日無事到着です。長かったなぁ。Tシャツの時は結構早かったんですけどね～。でもとにかく皆様、もうすぐですよ！

＞総集編...。
う～ん。私もどうして今更なんだろうって感じです。コミックスの方が惨然希望します。バンチ全部まだある私は多分購入はしない気がします。やっぱりコミックスの方が保存的にいいですし、安っぽい表紙よりそれなりの方が私個人は好きだから...。でも、本誌を見逃された方にとっては希望の一冊だと思います。

＞あっきーさん
私も美樹さんの存在に疑問を感じていました。何故一緒にいないんでしょうね。も、もしかして子供を産んだばかりとか...な事ないか。でも、すごく心配。早くあの2人も揃ってお店に出て欲しい。海ちゃんが寂しいはずだもんね！ 

