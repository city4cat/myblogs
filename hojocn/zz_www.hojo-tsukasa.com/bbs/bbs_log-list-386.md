https://web.archive.org/web/20031024205733/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=386

### [7720] おっさん(岐阜県：100ｔハンマー管理人殿）	2002.09.08 SUN 17:38:22　
	
毎度、雨がざんざん降りの中仕事から帰ってきました。今はやんでるのになんだったんでしょう（ＴＴ）

１００ｔハンマー管理人　殿
すいませんが、送信した時間が狂ってます。今は９月８日の午前０時１０分頃です。修正の方を宜しくお願いします。

ということで、本日はこれで・・・

### [7719] TAKESHI	2002.09.08 SUN 14:50:34　
	
＞[7718]たま様
そうですね！自分が住むところにあったらうれしいですもんね！そういえば、たまさんはお住まいはどこなんですか？

### [7718] たま	2002.09.08 SUN 13:34:09　
	
みなさま、こんばんわ。

感想はやはり皆さまと同じで
名づけ親が香だったとは・・・！っです。
名前を決めてもらったリョウは本当に嬉しそうですね。
―　大好きな香に…自分の名前決めてもらって　―ってアシャンが言ってたけど、この時すでに香のこと「大好き」になったのかなぁ？リョウは。
って事はＡＨではリョウが先に香に惚れたって事？（ＣＨでは香が先だったけど。）

香の涙をもう見たくないから、新宿に留まる！
「あの娘の涙見たくないの！俺」このセリフって、ググッと来ますね。
そして、リョウの顔に朝日が光る・・・
ここでようやく、リョウは、明日の朝日を一緒に見る仲間をこの時手に入れたのかな？

それにしても、槇ちゃん弱いですね・・・(^_^;
刑事なのに・・・。ＣＨっていう裏の仕事もしてるのに・・・。
それともリョウが強すぎるのかしら？！(アシャンもね☆)

＞TAKESHI様【7717】こんばんわ。
ＡＨの巨大広告・・・地方にも置いてほしいですよね。
東京では結構色々な所にあるのにね。

### [7717] TAKESHI	2002.09.08 SUN 10:03:04　
	
バンチ読みました。リョウの名前は香がつけたとは、ますますCHと話が違ってきましたね。でもこれはこれでいいと思います。一巻のメッセージのところにもAHはCHの続編ではないと書いてありましたし。それにしても香は本当に綺麗ですね！リョウと香はお似合いです。FCの若苗夫婦も（性別は逆転してますが）お似合いでしたね。理想の夫婦だと思います。来週も楽しみです！

どうやら書き込み日時おかしくなってますね。

＞[7690]いちの様
携帯使って車爆破するとは驚きでしたね。バイオ今度見てきますね！

＞[7693]たま様
地方にはないんじゃないんですか？僕もあんな看板あるとは思いませんでした。先月東京に旅行行ったとき、電車で秋葉原行く途中で見かけて驚きました。もう見れないかと思っていたら、別の駅の中にあったんですぐ写真撮りました。後、MYCITYとアルタ前も撮りましたよ。東京にしかないなんてずるいですね。でもわざわざ石川県から行った甲斐がありました。一度行ってみるといいですよ！！

＞[7696]千葉のOBA３
サンデー読んでるんですね！僕はコナンと犬夜叉が好きです。青山剛昌と高橋留美子の漫画は全部持ってますし。高橋先生のめぞん一刻はすごく好きです。

＞[7707]将人様
そうでしたか！それにしても北斗神拳の伝承者はみんなごついですね（笑）でもかっこいいです。

### [7716] 無言のパンダ	2002.09.08 SUN 08:33:41　
	
ほんとだ！書き込み日時おかしい！
私が送ったのもお昼すぎてたはず・・・！
土曜日はそんな早起きしませんもーん☆(^_^;)

### [7715] 喜楽	2002.09.08 SUN 07:48:08　
	
気になったのですが以前、海坊主さんのエプロンが猫ちゃん柄でしたね。彼って猫が苦手なのでは？AHでは大好き？なのかな（笑）
CHではシティ－ハンターの名前はたしか元々はリョウとミックのアメリカでのコンビ名でしたよね。
AHでは槙村兄貴のスイーパー名とは…。
素人同然？そりゃ「チンケなスイーパー」って言われるな…（笑）
アシャンが槙兄が「まさか…妹をねらってるんじゃ…」って言ってる時にクスッと笑うシーンなんかほのぼのしていいですね。

### [7714] ぽぽ（Hyogo）	2002.09.08 SUN 07:17:07　
	
ん？？？？
カキコミの日時､おかしいぞ？さっき送ったの､土曜日の13時半っす(*o*)

### [7713] ぽぽ（Hyogo）	2002.09.08 SUN 07:08:32　
	
[7707] 将人（元・魔神）様
先週は槇村兄貴の話の前編だったので今日は後編で､槇村兄貴も少し出てくるかもしれません｡

放映当時は録画しても保存していなかったので､十数年経た現在､いっしょけんめ録画しております｡時々すっとぼけて録画し損ねることもありますが･･･｡

まだバンチを見ていないので早く本屋に行かねばっ！

### [7712] 無言のパンダ	2002.09.08 SUN 07:06:23　
	
こんにちわ☆

今日はまだ誰も来てらっしゃらない・・・
嵐のあとの静けさでしょうか？！

今週のＡＨでのリョウの「あの娘の涙見たくないの！俺」ってセリフ。槙村のことを守るっていう意味もあるけど、自分自身も野良犬を廃業（？）するってことですよね。
だって自分に何かあっても香は自分のために涙を流すことになってしまうから・・・
昇る朝日と共にリョウ自身も生まれ変わるという決意をしたのでしょうね！このシーンのリョウ、イイ顔してます♪

＞将人さま（７７０７）
ライジンのプレゼント、間に合ったみたいでよかったですね！
手元に届いたらどんなものか感想を聞かせてくださいね(^^)
イラストは知っているものだけど、質感とかどんななんだろうなぁ♪

＞いもさま（７７０９）
そんなにお忙しいんですか～？！
体に無理のないように乗り越えられるよう、がんばってくださいね(^^)それにしても柱の「牧村兄妹」って・・・ねぇ？演歌歌手じゃないんだから。間違えないで欲しいですよね(>_<)

ではでは～。「元祖」無言のパンダでした。（笑）

### [7711] 無言のパンダ	2002.09.07 SAT 00:57:45　
	
こんばんわ★

今週のＡＨ、「ふたりの出会い編」は、これでおしまいなのでしょうか？リョウ・香・槙村３人の日常をもうちょっと見たかったなぁ！
ところで唐突に遊園地でのシーンになってましたが、いったいどちらが誘ったんでしょうか？私の予想ではリョウだと思うんだけど・・・香はかなりの節約家とお見受けしたので(~_~;)
リョウもそんな香に息抜きさせてあげたかった・・・とか？
そして、リョウの名前・・・香が決めたんだぁ！？まるでトランプでジョーカーを引くみたいに（いえいえこの場合はハートのエースかな♪）なぜ「冴羽リョウ」の名が印象に残っていたのか、もう少しフォローがあってもよかった気もするけど、きっと香ちゃんの「ひらめき」みたいなものだったのかもしれませんね。
そして香瑩が目覚める寸前のスリーショット、阿香のしあわせそうな笑顔・・・やっぱり「夢」なんだよね・・・(T_T)
でも心はこれからもずっと一緒だよ(^^)

ではまた。(^_^)/~

### [7710] 凛々	2002.09.07 SAT 00:04:47　
	
こんばんわ！私も漏れずに感想を・・・
槇村兄貴そんなに素人だったのね。ＣＨのかっこいい兄貴が焼きついててすごくチョック！意気投合してって方がやっぱりよかったな～。ところで冴子さんとのからみはＡＨではないのでしょうか？

☆千葉のＯＢＡ３様（のまんまでいいのかな？）
私もリョウちゃんの生い立ちが、ＣＨよりもしかしたらもっとつらかったのかな？って思いました（きっと飛行機恐怖症でもないんでしょうね）だから余計、香ちゃんのような人にめぐり逢えて、素直に飛び込んでいけたのかもしれないですね。でも常々思ってたのですが、つらい生い立ちであればあるほど、二人のほんとの子供をリョウちゃんに抱かせてあげたかったななんて思います（アシャンの話はまた別として）
裏の世界は・・・ってのはよくわかってますが、あんなに信頼して愛し合ってる二人の子供ってすごく幸せだと思うのです。（アシャンも幸せですが）
しかしなぜ遊園地！？ですよね。アシャンの夢だからアシャンの願望も入っているのかな？

☆7708　実葉さま
素敵な息子さんですね。親子ともどもなんてうらやましい。

☆7700　まろさま
な～るほどそうかも！刑事でＣＨってどんなヤツか槇村兄貴
を確かめたのかもしれないですね～ふんふんって納得しちゃいました！

### [7709] いも	2002.09.06 FRI 23:39:50　
	
こんばんわ～。

感想はまた後日ゆっくりってことで。（笑）
でも、どーしても一言いいたい！！
「牧村」じゃなくて「槇村」ですよ～！！
先週に続き今週も‥ちゃんとしてくださいね。(^_^;)

＞千葉のOBA3さま[7696]
忙しいんですぅ～。（泣）
今まで生きてきた中で一番忙しいです。まだ半年以上（波はありますが）こんな生活が続くと思うとそれだけで倒れそうです。(^_^;)
自分で決めたことなんで、頑張らないとなんですけどね。

ではでは～。いもでした。

### [7708] 実葉	2002.09.06 FRI 23:25:58　
	
こんばんは。
今週号・・・
リョウの心の奥が見えるような表情にぐぐっときてます。
私は単純なので、この「別物」のＣ・Ｈの世界に、もうなんの違和感もなく、とっぷり浸ってしまっています。
確かに、途中、少しだけ戸惑ったこともありましたが、今はもう、とっぷり、です。
そんなわけで、今週も、涙涙でした。

[7706] 千葉のOBA3 さま
＞アシャンが香ちゃんを、ママって呼んでくれた所では、またしても、もらい泣き。（オバサンは涙腺が弱い）　

ここ何週か、泣けますねぇ。
そばにいた高２の息子が、「また、泣けたの？」と言いながら、バンチを持っていきました。

### [7707] 将人（元・魔神）	2002.09.06 FRI 23:02:41　
	
こんばんは、将人（元・魔神）です。
今週号で、はじめてリョウが「シティーハンターの冴羽リョウ」に
なったんですね。
槇村兄さんと組んでCHになったのも、名前を「冴羽リョウ」に
したのも香さんがキッカケだったんですね。
最後にリョウと香の思い出だけでなく「いらっしゃい阿香」
って呼ばれたのが良かったです。
リョウと香と香瑩の親子の姿がいいな～って思えました。

AHの連載が始まった直後の時期だったら、香さんが
亡くなったという事で、もしこの話があったとしても
CHのファンだった読者として認められないという気持ちが
あったかもしれませんが（何を偉そうに・・・）、
今週号では自然に受け入れられたような気がします。

＞[7688] TAKESHI様
＞蒼天の拳、この作品は北斗の拳より前の話なのか
＞後の話なのかどっちなんですか？

蒼天の拳は、北斗の拳の前の話になりますよ。
蒼天の拳の第1話に（北斗の）ケンシロウが生まれた場面が
あり、霞羅門（北斗のケンシロウの師匠のリュウケン）が
前の北斗神拳伝承者だった兄（蒼天の）霞拳士郎の名前
からケンシロウと名付けたと描かれてます。

蒼天が1930年代の昔の日本が戦争をやっていた頃の中国で
北斗が199X年の（もう過ぎてるけど80年代の漫画だった
から）近未来の核戦争の後みたいです。

＞[7696] 千葉のOBA3様
＞読んでるのはバンチとサ＠デーだけなんですよ。

同じ組み合わせの漫画雑誌を読まれているんですね。
昔のジャ＠プ作品が、色々な雑誌に載ってますが、それを
まとめてクラシック・ジ＠ンプって出ないでしょうかね？
（大人のでアダルトってするとHな意味になりそうなんで）

＞[7697] いちの様
今日コアミックスから佐川急便で荷物が届いたので
「何やろ？」って見たら、ライジンが週刊から月刊になる
事と、北斗の拳のラオウの人形、そして
年間購読の申し込みで入金をした人には継続で差額
または変更による退会でお金が返ってくるという連絡でした

そして、その継続・退会の確認書にプレゼントの希望の記入
が欄がありました。
なんとかプレゼントに間に合ったみたいですよ。
僕はCHのジャンプコミック27巻のリョウと香の後ろに
ビルの背景にある絵を選びました。
みなさん、心配かけてすみませんでした。＼^o^／バンザーイ

＞[7698] ぽぽ（Hyogo)様
あれ関西でCHの再放送やってましたっけ・・・ゴソゴソ
読売テレビの土曜日とかの深夜にやってたんですね。
知りませんでした。以前にル＠ン3世をこの時間に
やっていたのは知っていたんですが・・・（汗）(^^;
今度見てみようかな？
槇村さんの登場する回見てみたかったな～♪

＞[7699] Ｂｌｕｅ　Ｃａｔ様
＞・・でも、確か昔謙徳を助けたときも冴羽リョウって
＞名のってたんだけど・・・

あっ確かに名乗ってましたね。CHでの話を合わせてゲリラ
の時に海原さんに付けられた「冴羽リョウ」という名前
だったので、裏の世界で生きるのに、使っていた偽名の
中でも特別な物で、謙徳に名乗ったのと、香さんが
選んだのは、その最初に海原さんに付けられた特別な名前
だったのでは？って考えてみました。

＞[7700] まろ様
もしケガをせず香さんに出会わずに、槇村兄さんのCHに
出会っていたら・・・やっぱり依頼通りに殺すのではなく
リョウとの実力の差を見せて（ケガさせるかもしれないけけど）
新宿の街から離れるように脅すとかで殺さない方法を
取るんじゃないかな？というかそうして欲しいと思いますね

＞[7701] sophia様
＞『シティーハンター　山田太郎』とかだったら

香さんじゃなかったら、どんな名前だったんでしょうね？
リョウの持っていたカードの中に「山田太郎」とか
「タナカ　タダシ」とかの名前ありましたね。

もし、あの中に「伊集院隼人（海ちゃんの本名）」の名前が
あったとしたら、リョウは名乗っていたんだろうか？

えっ初めっから、その名前は入れてないって？（汗）(^^;

＞[7688] TAKESHI  
苍天之拳，这个作品是北斗神拳之前的故事吗?
＞是后话还是哪一个呢?

苍天之拳是北斗神拳之前的故事。
苍天之拳的第1话中有(北斗神拳)健四郎诞生的场面。
有霞罗门(北斗的贤四郎的师傅龙研)
前北斗神拳传人哥哥(苍天的)霞拳士郎的名字，
上面画着取名为卡拉健四郎。

苍天是20世纪30年代，日本战争时期的中国，
北斗是199x年的，80年代的漫画。
好像在不久的将来核战争之后。

### [7706] 千葉のOBA3	2002.09.06 FRI 22:59:58　
	
こんばんは。感想書きにまた来ちゃいました。

☆香ちゃんの笑顔を守るために、りょうちゃんはＣＨになったんですね。ＡＨでのりょうちゃんって、ＣＨでのりょうちゃんより、孤独で仲間もなく、海原みたいな育ての親もなく、一人でほんとに野良犬みたいに、その日その日を生きてきたのかな？って思いました。そのりょうちゃんに香ちゃんが、初めて生きる意味を与えてくれた・・・。やっぱり、すべてを与えてくれた人なんだよね。

でも、いずれ、たぶん、｢守ってやる・・・。」って言った槇ちゃんを守りきれずに（この先は、わからないけど・・。）見たくなかった香ちゃんの涙を、見ることになってしまうんでしょうか・・・？わーん、りょうちゃんかわいそー！！

遊園地で（何ていって誘ったんだろ？＞７６９９　Ｂｌｕｅ　Ｃａｔさまのおっしゃる様に、デートじゃん、これって！）初めて香ちゃんの名前を呼ぶりょうちゃん。このシーンすごく感動しちゃって、自分の生涯をかけて、愛した女の名前を初めて口にする、この場面・・・｡　神谷さんの声で聞きたいよー！とムショウに思いましたよ。　アシャンが香ちゃんを、ママって呼んでくれた所では、またしても、もらい泣き。（オバサンは涙腺が弱い）　　香ちゃんが元気で、アシャンみたいな、かわいい女の子が二人の本当の娘だったらいいのにな。と以前から思っていたので、手をつなぐ３人の姿は・・・感動でした・・・。

なんか、今回もっと色々あるのですが、長くなってしまうので、今日はこれにて・・・。おやすみなさい。

### [7705] おっさん（岐阜県：解禁）	2002.09.06 FRI 22:35:50　
	
毎度、おっさんでごじゃいましゅ！！皆様おげんこでごじゃいますでしょうか？？？

ということでカキ～ン（解禁）の時間
香がりょうの名づけ親・・・なんかいいっすね。香が「一番印象に残ってたから」その人のイメージとかでその人の名前やその人自身の印象が残ることもあるかと思います。一番大切な人に名前を付けてもらえた。私の中ではいい感じかと思いましたが、そして二人の子供の香瑩（本当の子供ではないですが）に差し伸べた感じは本当の親子に見えました。来週はまた新たな話になるんっすかね？

ちょっちレスを・・・
Ｂｌｕｅ　Ｃａｔ様（７６９９）、おっは～っしゅ！！宇多田　ヒカル結婚したんっすか？ここのを見てて初めて知ったんで・・・（＾＾；）

ということで本日はこれにて・・・

### [7704] 葵	2002.09.06 FRI 21:29:27　
	
　某映画のタイトルを借りるならば…「恋に落ちた冴羽リョウ」って感じでしょうか？（^_^;

　刑事のクセにまるっきり拳銃の扱いが素人な槇ちゃん。ホントにＣＨをやってたの？（しかもチビ阿香にまで言われて☆）あれじゃ本気でリョウちゃんが殺しに来てたら…今頃お葬式だな☆槇ちゃん、これからしぶしぶとはいえ仲間と認めたリョウちゃんから、バッチリ拳銃の扱いを教えてもらってね♪

　「冴羽リョウ」誕生の謎が明らかに…。名付け親でもある香ちゃん、ホントの意味で「母親」だったんだね～。実は私もｓｏｐｈｉａさま［７７０１］と同じく「ＣＨ・山田太郎」だった可能性もあるな…と思った一人です。（^_^;　これじゃギャグ漫画にしかならないよぉ～☆（でも名前が決まったリョウの顔、すっごくいい表情ですね♪）

　それにしてもリョウが素直になれないのって、この頃から？「気に入ったのさ…」のあと、素直に「お前が」って言えばイイのに！！しかもどもりながら香に名前を聞くあたり妙に可愛くって…。思わず背中をバシバシと叩きたくなっちゃいましたよ。いやはやリョウちゃん若いっすね～♪

　最後に阿香。最高のパパとママに出会えてよかったね♪（でも次号予告の「色気グラグラ」って、ナニ？！あまりリョウパパに心配かけないようにね☆）

＞千葉のＯＢＡ３さま［７６９６］
　２０日の神宮？！いぃ～なぁ～…。でもその頃には決まってるかも？当日、テレビチェックしますから、見つけられるように派手なカッコしてって下さい。（^_^;

### [7703] いちの	2002.09.06 FRI 21:01:38　
	
こんばんは。解禁ですね。
今回初めてバンチを買って読んでしまいました。立ち読みと違ってゆっくり読めてよかったかも（笑）。

感想；リョウがＣＨを倒すために雇われた殺し屋だったなんて驚き！！でも、それがきっかけでリョウちゃんがＣＨになるのですねぇ～。やっぱりＣＩＴＹ　ＨＵＮＴＥＲとは違う世界なのですね・・・やはり少し複雑ですね（汗）。
「冴羽僚」（字は出ませんね）の名前も香がつけたものなんて、意外すぎですね。
でも、最終的に夢オチになっていたので本当のことかは定かではないですよね？余韻を残しつつ過去を語っちゃた感じです。
で、この後はどういう風に進んでいくのでしょうか？

### [7702] しゅんしん	2002.09.06 FRI 20:31:23　
	
こんばんわ～。初めての人は初めまして。
僚の名前って、香が付けたものだとは…(^^;
本家を知ってるものとしては、なんか複雑です。
私的には、ＣＨ時の海原が付けてくれたっていうのが好きなんですけどね。
ＣＨを始めたきっかけも違うし、やはり、パラレルとして考えるべきかな？

### [7701] sophia	2002.09.06 FRI 19:36:40　
	
こんばんわ。
解禁だ、解禁だ♪
さぁ、いってみよぉ～★

「AH」ではリョウは最初から香を『女』としてみてるんですね。
「CH」では香を『男』だと思って叩いてますからねぇ(シュガーボーイの時ではないよ。CH1巻参照)
それだけでもすごい違い。
それに「冴羽　僚(字がでない～）」の名づけ親が香とは・・・
他にどんな名前があったんだろう・・
『シティーハンター　山田太郎』とかだったら、めちゃカッコ悪いよね～＜＾＾；＞
香に感謝、かな？
最後に阿香が「香ママ」と、香に愛情を持ったのがうれしかったかも。
今まで『香』って呼び捨てだったもの。
これでホンとに阿香はリョウと香の子供になったんだなぁ～っておもっちゃいました＜＾＾＞
今回から「CH」が『槇村　兄』と『リョウ』のコンビになったし、次回からはどんな展開になるんでしょうね？
阿香が夢から覚めてしまったのでしばらくは(現在）の話にもどるのかなぁ・・・
あ～楽しみ♪
『バンチ』発売日に次の『バンチ』を読みたがるなんて私って欲張りか？？？

皆さんの感想が楽しみなのでまた明日こよ～っと。
それでは皆様良い週末を。
