https://web.archive.org/web/20020425133715/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=93

### [1860] tanoっち	2001.07.31 TUE 03:51:35　
	
バンチのＨＰ、もう更新されていました～。オドロキ！

強いて言えば表紙がネタバレ？？（笑）
読む前に見ても影響ないですヨ～大丈夫。。

総集編☆まとめてＡＨを読む初めての機会ですね。
続けて読むと、また違った感想を持つかもしれないです。
（地域によって発売日が異なるのかな？）

### [1859] こいけいこ	2001.07.31 TUE 01:10:55　
	
＜もっこりについて＞

わたしはかすみが初登場した巻で、リョウが100ｷﾛの重りをもっこりで支えてかすみが念力で浮かせたと見せかける場面を思い出しました！でも香が一生懸命挑発してりょうにもっこりさせようとしてもりょうはしなかったんだよね･･･｡（香ちゃんかわいそう）　でも100キロって…恐るべしもっこりパワー

あともっこりを車のギア（サイドブレーキだっけ？？）と間違えられちゃったりとかもありましたよね

### [1858] ワタル／Wataru（宮城県）	2001.07.31 TUE 00:26:39　
	
【宮城県南方町のもっこりネットワークについて】［１８４８］
法水さん，すいません，分かりません。そして答になってないと
思いますが，「ＶＯＷ」という投稿本の何巻目かに「もっこりに
ら」とか何とかいう写真が載っています。って，実は投稿したの
私なんですけど，はっきり覚えてなかったりして…。ちなみにそ
のときのペンネームは「りょうちゃん」でした。

【Mokkori Network in Minamikata-machi in Miyagi】 [1848]
Sorry, Norimizu. I don't know about the network, but I
know that leek named‘mokkori leek’or something is in
a book called‘VOW’. I don't remember which volume it
is in though I am the person who sent the　picture...
My pen name was Ryo-chan at that time.

### [1857] kaori	2001.07.31 TUE 00:23:28　
	
to Goo Goo Baby: Oui, La Rose de Versailles!! Je l'adore moi aussi!
Excuse the sudden French, I could not help do this!
A real masterpiece!!! This is a story where death is justified and complementary to the events...

### [1856] みみごん	2001.07.31 TUE 00:10:07　
	
 [心臓移植の本についてのレス]

前回「記憶する心臓」という本について書き込みしました。
 [１８３５]まみころ様が興味を持ってくださいましたので
ちょっと補足を。（個人的なレスですみません）

アンビ○バボーで、心臓移植された人の話をしていた時に番組中で紹介された本です。ただ、ドナーの記憶が...という部分は
少なく、自分がいかにして心臓移植にふみきったか、という闘病記のような感じが強かったです。もしも読まれるなら、古本で探された方がいいと思います。（お金がもったいないから）「記憶する心臓」というタイトルは本を売るための誇大広告じゃないか、とかんぐりたくなったように憶えてますから。紹介しといてなんですけど。ただ、確かに今までと全然違った自分が出てきた、という感じはうけたようです。

参考までに、
「記憶する心臓」クレア・シルヴィア、ウィリアム・ノヴァック
　　角川書店の単行本です。

番組やこの本で、ドナーの記憶がレシピエントに存在する、という話にすごく興味を持っていました。そうしたら、ＡＨでこういう話が出てきて、正直驚きました。ＣＨは連載中からの大ファンで、そのまま、北条先生の作品はすべて集めまくった私です。
北条先生が同じ事に興味を持たれたのか、と嬉しい反面、なんで
香ちゃんなんだ！という怒りとショックと...　複雑でした。

長々とごめんなさい。

### [1855] Goo Goo Baby	2001.07.31 TUE 00:05:32　
	
＞ｋａｏｒｉ［１８５３］

Versailles no Bara・・・・
“La Rose De Versailles”？？　Ｉ Love it！！

### [1854] sweeper(Resistance)	2001.07.30 MON 23:20:41　
	
こんばんは。
いつのまにかもっこりについてもりあがっているようですね。

＞テムリン様。もっこり研究会会員のsweeperです。
大分支部会長立候補ありがとうございます。
この場を借りて大分支部会長に任命します！これから、もっこり研究会会員を広めましょう!(笑)
ところで、りょうのもっこりって耐久力すごくないですか？山手線にはさまれて一周したり、「キャッツ･アイ」半壊してたりしてましたよね。

海外ではＣＨキャラっていろんな呼び方されてるんですね。
はじめて知りました。

明日は、バンチ発売日なので楽しみです。
それでは、失礼します。皆さん、おやすみなさい。

### [1853] kaori	2001.07.30 MON 23:19:54　
	
Moreover, the voice actors dubbing Ryo in Italy are 2 different ones depending on the version. The one I love best is VHS one, the same voice actor that dubbed Andr&eacute; Grandier of Versailles no Bara years ago.
If someone wants to receive a wav file of the 2 voices, maybe I can try and send it.
Both Kaori's voices are nice, by the way.

此外，在意大利为獠配音的配音演员是两个不同的，取决于版本。我最喜欢的是VHS的那个，也是多年前为《凡尔赛玫瑰》配音的那个配音演员。
如果有人想收到这两种声音的wav文件，也许我可以试着发送。
顺便说一句，Kaori的两种声音都很好听。

### [1852] Goo Goo Baby	2001.07.30 MON 23:19:14　
	
［１８５０］の自分のカキコ、
ズ、ズレてる！　ごめんなさい　この前もズレたよな～なんでだ？

おっさんサンや天使の心さんは今どうなさってるんだろう
最近お姿が見えないんで、心配なんですが。
もしや本当にここを去ったなんてコトは・・・
でもカキコしてなくても、せめてここに来て読んでくださってるだけでもいいんで、なにかお返事を～！！

＞ＯｖｅｒＳｅａｓ
海外の方々って日本在住じゃないのかな　
Ｋａｏｒｉさんはイタリア現地からナマ中継かしら？
ｊｉｊｉさんとかｓｈａｎｉｃｅさんとかも・・・

＞テムリンさん［１８４９］
笑って頂いてありがとう　ハズしたらどうしようかと・・・

### [1851] kaori	2001.07.30 MON 23:13:06　
	
Yes, there are 2 versions of CH anime in Italy. In Italy, anime is considered a just-for-children show, and
therefore every series is censored and adapted to be suitable for a children audience.
VHS and DVD are for anime-crazy guys of all age, so they are absolutely true to the original version.
So manga are. Therefore, CH manga keeps original names and has no censorship. The same is for
France and Spain, as far as I know.

### [1850] Goo Goo Baby	2001.07.30 MON 23:00:41　
	
法水さん［１８４８］
そうなんですか、香はそのまんまなんですか

ｋａｏｒｉ［１８３９］さんがイタリア版についてふれてましたね　英語だからって読み飛ばしてた（殴）

えーと、じゃ～私なりにまとめると、


|           |  USA   |  Italy  |  German  |  Korea  |
|-----------|--------|---------|----------|---------|
| Ryo →     | Joe    | Hunter  | Nicky    | Usuhan  |
| Kaori →　 | Kaori  | Kreta   | Laura    | Sauri   |
| Saeko →　 | Sandra | Selene  |          |         |
| Makimura→ |        | Jeff    |          |         |

で、イタリア版の名前はＴＶ版だけで使用であると。
なるほどね～（＾＾）

あたしも海外の方々とお話したいよ～！！
でも英語カラッキシ駄目なんです～（泣）

### [1849] テムリン	2001.07.30 MON 22:38:14　
	
[1848] 法水 様＞
「もっこりは国境を越える！」！！
おお！！すごいぞもっこり！！　←なんか恥ずかしい事言ってない？
「お前・・絶対お嫁に行けない・・」（リョウ談）
では、私は大分支部会長に立候補します！（なんのこっちゃ）

今まで海外ではキャラの名前が違う事すら知りませんでした・・。（汗）
香だけはそのままなんですね？発音しやすいのかな？？

[1840] Goo Goo Baby 様＞
笑わせていただきました！　ヽ(´▽｀)/
ジョォォォォォ！！（爆）

1842] mimirinn様、[1835] まみころ 様
海坊主が選挙！！　その発想自体がたまらなくおもしろかった！！
誰に投票するのかも気になるところですね。やっぱり小○サンかしら？

### [1848] 法水(Norimizu)	2001.07.30 MON 22:03:18　
	
>kaori### [1839&1844]
Thanks for information about CH in Italy! We didn't know that there are two versions in Italy... We want to hear Ryo speaks Italian(^^). Ha Ha. Anyway, "mokkori" is world-wide, isn't it?

もっこり研究会・イタリア支部（またもや勝手に任命。笑）のkaoriさんからの情報[1839&1844]によると、「もっこり」はアニメでもコミックスでも「もっこり」のままだそうです。ついにもっこりは国境を越えました(^^)。また、Goo Goo Babyさんがイタリア版の名前について書いて下さっていましたが### [1836]、それはテレビ版のアニメで、ビデオで売られているものは原作通りだそうです。
あ、あとGoo Goo Babyさんお尋ねの件[1840]ですが、香はなぜかそのままKaoriで、冴子はSandraになっているそうです。

>ワタルさん### [1843]。
「週刊ブックレビュー」は漫画家としてよりも経営者として出演されていたような気がします。やはり漫画家としての裏側は見せたくないということでしょう。
ところで宮城県の南方町に「もっこりネットワーク」なるものがあるらしいのですが、実態をご存知ではないですか？？

### [1847] jiji	2001.07.30 MON 22:01:54　
	
Hi,guys. How are you?
Thukasa Hojo's Mokkori Message was updated:
We sometimes see making storys of the movie, musics and dramas etc on TV before their releasing .
These making storys stuffed with creators toils more than needed are broadcasted as propagandas. To be sure they are interesting and impressive.
But I doubt it is good to use their toils for selling their works.
It is natural for creators to take pains to complete their works, and that is not related with their works themselves.
On the contrary they should not make their audience or readers realize their toils.
If audience or readers know creator's toils, they will feel sympathy and stand up for them whatever their works are.
It is shame of proffessional creators to showing off such toils.
I reluctant to accept the documentary TV program tracking for starting BUNCH of Comaix.
So I refused the interveiw at my workplace.
Then I would never write an inside or toil story here.
I will say in advance that those who are expecting these storys.
And because I was tired, last message was intense and like a toil story.
I regret. //

It is interesting there is a kind of censorship to publish or broadcast CH.
I'm looking forward to reading your country's sitiuatons of CH!
BTW I think Mr.Hojo implys Hayao Miyazaki's new movie...

### [1846] shanice	2001.07.30 MON 21:31:33　
	
Hello,

It's really interesting for our fans from different countries to share something about CH.It really becomes internationalized and proves that attractiveness of CH is worldwide. For your other kind of information, even in Chinese, there may be different interpretation. For example, the name of Saebe Ryo, it has different translation
In Hong Kong, it translated as 孟波
In Mainland China,it translated as 寒羽良
In Taiwan, it is directly translated same as Japanese kanji.
^_^

### [1845] みんみん	2001.07.30 MON 21:13:53　
	
《北条氏のメッセージ》
裏側を見せたっていいと思うんですけど。私は只単純に製作現場って興味あります。
作品が面白ければいいんです。苦労話聞かされたってつまらないものはつまらないし。
私個人としては北条氏が原稿を書かれているところを見たいです。プロの原稿ってホントきれいなんですもの。

《週間連載》
青年誌って、週刊誌でも休み休み連載しているのって多いですよね。ある程度まとめて掲載してから数週休んだり、隔週にしたり、月１にしたり。
気になったのですが、週間連載ってきつくないですか？
ジャンプの頃のように慢性的に〆切りに追われる状況が今後もっと厳しくなるような気がします。
それから、あの頃より年齢的体力も心配です（笑）
だから無理のない連載でもかまわないんだけどな・・・
その分素晴らしい作品を望みます、北条センセ。

### [1844] kaori	2001.07.30 MON 20:53:55　
	
Mokkori translation: in Italy Mokkori is untranslated.
It is just mokkori. In fact, this expression is sooo famous among Italian CH fans that in anime dubbing and manga translation it is still mokkori.

Mokkori翻译：在意大利，Mokkori是没有翻译的。
它只是mokkori。事实上，这种表达方式在意大利CH迷中非常有名，以至于在动漫配音和漫画翻译中，它仍然是mokkori。

### [1843] ワタル／Wataru（宮城県）	2001.07.30 MON 20:44:30　
	
［１８０９］で「週間ブックレビュー」の内容教えて下さって
ありがとうございます，法水さん。そうか，ＡＨについてのコメ
ントはなかったんですね。ちと残念。

Thank you for letting us know the content of the BS
program WEEKLY BOOK REVIEW in [1809], Norimizu.
It's a little disappointing that Mr. Hojo didn't make
any comments on AH.

### [1842] mimirinn	2001.07.30 MON 19:53:31　
	
まみころさん、海坊主が選挙に行く図想像させていただきました。腕が入るか、入らないかでしょうか（て、わたしはいったい海坊主をどのくらいの大きさだと思っているんだろう・・・）ちょっとかわいらしいですね。
前の話題に参加させて下さい。１７８８の法水さんの推測すごいなと思いました。（海坊主が指輪をしていないと気づく点からしても）手塚治虫がひげおやじやハムエッグなどのキャラを巧みに色々な作品に使ったようにＣＨのキャラを作品の枠を超えて使おうとしているのですね。ＣＥとＣＨで海原を使いましたが今度はミックや美樹が敵役として出てくるかもしれないと思うとそれも楽しみです。（使う という表現より 登場する の方が良かったかもしれませんね・・・）大分話しが後退しました、すみません。

### [1841] chikka(Eastern Shizuoka)	2001.07.30 MON 17:06:34　
	
お久しぶりです。北条先生のメッセージを読みました。あれですが、言っている事はよく分かります。｢プロ｣だから苦労するのは当然であって、苦労しなければいいものはできない。当たり前の事ですね。このコメントですが、現在テレビ朝日の野球解説をしている落合博満氏もよく現役時代同じような事を言っていたのを覚えています。落合氏も「人前では練習はしないよ。｣と言っていましたが、これは｢プロだから練習するのは当たり前。練習するところを人にわざわざ見せる事はない。」という考えを持っていました。要は｢結果がすべて。金がすべて。」と言う事ですが・・・。(これは実際に本人が言っていました。）よく考えてみれば、練習もせずに｢三冠王｣を三度も取れるわけないですもんねえ。｢人に苦労している姿を見せない｣。こういう｢美学｣を持っている人が逆によい結果を収めるということはよくあります。現役スポーツ選手では「イチロー｣、｢中田英寿」、「野茂英雄｣などはそのいい例でしょう。もっともこういう考え方を持つ人はかなり｢強い精神力｣が必要ですが・・・。 

好久不见。读了北条老师的留言。你说的我很明白。因为是“专业人士”，所以吃点苦是理所当然的，不吃点苦是做不出好东西的。这是理所当然的。关于这个评论，我还记得朝日电视台的棒球解说落合博满先生在现役时期也曾说过类似的话。落合氏也“不在人前练习哟。”，但这是“因为是职业选手，练习是理所当然的，没有必要特意让别人看到自己练习的样子。”抱着这样的想法。“结果就是一切，金钱就是一切。”是这样的事…。(这实际上是本人说的。)仔细想想，不练习是不可能拿到三次“三冠王”的。“不让别人看到自己辛苦的样子”。拥有这种“美学”的人往往能取得好结果。现役运动员中的“铃木一郎”、“中田英寿”、“野茂英雄”等就是很好的例子。当然，有这种想法的人需要相当“强大的精神力”……  