https://web.archive.org/web/20031027170314/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=484

### [9680] 将人（元・魔神）	2003.02.23 SUN 16:12:32　
	
こんにちは、将人（元・魔神）です。

＞[9671] Ｂｌｕｅ　Ｃａｔ様　2003.02.21 FRI 19:36:56　
冴子さんに、槇村さんからの指輪を、渡すのと渡さないのというのは
どっちが冴子さんに、いいのでしょうね。難しいですね。

その指輪を渡すはずだった槇村さんも、その後でその指輪を持っていて
渡せなかった香さんも、いなくなったＡＨの世界なんだなって思うと
悲しいですね。

＞[9673] 葵さま　2003.02.21 FRI 21:22:20　
葵様のお祖母様、大変でしたね。でも元気に回復されてよかったですね。

僕も以前に、母が具合が悪くなり、僕の車で父と一緒に母を休みの日に
やっている救急指定されている病院に連れて行った事がありましたが
その時は、すごく不安で悪い事ばかり考えてしまいましたよ。

しかも、その前の日に滋賀県の方にある母の先祖の墓参りに行って
母の兄弟の親戚一同がが集まった直後だったので、病院で待っている
間は、つい変な事を色々と考えてしまって・・・。

その後、何にもなく、ちゃんと直って、今も元気なんですけどね。

槇村さんの亡くなった理由も、ＣＨとは違うのでしょうかね？
でも冴羽さんとの仕事がらみとかで、冴羽さんが責任を感じてるのと
頼まれた妹の香さんも亡くなってしまった事とかで、冴羽さんが
アシャンに槇村さんの事を話すのは、辛いんでしょうね。

ＣＨでの香さんは、冴子さんが槇村さんの事を好きだったって知るのは
だいぶ後になってからですけど、ＡＨでは槇村さんが居たときから
冴子さんの事を知っていたのかな？
それとも、誰とは聞かずに、槇村さんの好きな女性がいるとだけ
知っていたのかな？

＞[9674] kinshi様　2003.02.22 SAT 16:34:58
ここの掲示板、遠ざかると入りずらいと思わずに、書き込みが出来る
ご都合の良い時には、遠慮なさらずに、ドンドン書き込んで下さいね

僕もカキコ出来る時は毎日のように入っているのに、都合が悪い時は
書き込みできませんですけどね。
出来るだけ掲示板は見てるんですが、返事が書けなかったり・・・。

インターネットにすら繋げられる精神的な余裕すらなくてメールの
チェックすら出来ずに後で、返事するのに膨大なメールの量で、
なかなか返事できずに迷惑をかけた事もありますし・・・。(汗)(^^；

＞[9675] 無言のパンダさま　2003.02.22 SAT 17:25:48　
冴子さんに、槇村さんの思いと指輪の事を伝えるかどうかというのは
難しいですね。
ただ冴子さんが「結婚する気で私と付き合ってたのか！？」って
本音を言えるようになった、今だったら教えてもいい時期なのかな
とも思えるんですけどね。
でも、それを伝える立場の香さんも亡くなっているというのも
悲しいですね。
香さんが、アシャンに見せた夢は、香さんとしても伝えるかどうか
毎年悩んでいて、答えが出せずに、ずっと心残りだったんでしょうかな

僕がメール友達から貰った、面白い画像のメール楽しんで貰って
良かったです。あの人があのキャラに似てるなんて、僕は
そのメール友達に貰うまで気が付きませんでしたよ（汗）(^^;
パソコンを前にして、思わず笑いそうになってしまったし

＞[9677] Rouark様　2003.02.23 SUN 11:39:03　
Rouark様はじめまして、こんにちは！
僕もバンチ、買うタイミングを逃すと、近くのコンビニとかで
売り切れになる事がありますよ。
お店によって仕入れる量とかの関係もあるんでしょうが売り切れに
なるんなら、もっと置いて欲しいですね。

ここは、ＣＨの話題をしてもＯＫですよ。
連載中のＡＨの話題だけが、「金曜日夕方18時の解禁」というルール
があるので、金曜日以降は、ＡＨの感想などの話題が、どうしても
集中してしまいけど、
それ以外は皆さん、好きな北条司先生の色々な作品の話題を
されていますよ。

いわゆる「常連さんだけＯＫ」のような個人的なサイトじゃなく、
北条司先生の公式なＨＰのサイトなんで、初めての人でも、
遠慮なく好きな話をして下さいね

最初は、常連の方同士の話題のレス（返事）の会話が多いので、
多少は戸惑われるか知れませんが、皆さんいい人ばかりですから
大丈夫ですよ。
噛みつきませんし・・・パソコンを通じて、そんな事できわけないって
映画リ＠グの＠子であるまいし（汗）(^^；

＞[9678] 圭之助（けーの）様　2003.02.23 SUN 11:49:51　
圭之助（けーの）様、初めまして。こんにちは！
僕も、ＣＨが復活したって事で、再び北条司先生の作品に
はまり込んでこのＨＰに来た31歳の男性ですよ。

僕と同じ東大阪市の方なんですか？しかも近鉄線の布施駅界隈ですか？
たぶん近所ですよ！
ホントに知らないだけで、出会っているかもしれませんね。
ひょっとしたら、どっかの本屋さんかコンビニなどで、バンチや
コミックスを取り合いの競争をしていたりしてたかも～（汗）(^^；

→イヤ、直接のケンカした事は、全く無いですけどね。

近いという事は、もしかして小学校とか中学校とか、同じ学校の
先輩後輩になっているのかも？

小学校から、中学校に上がる時、小学校1校から中学1校のそのままの
学年の生徒になりましたか？

それとも複数の小学校から、1つの中学校になって、その中学校の
建物は、自分の校区内を飛び出て、別の（上記の）中学校の
校区内に入っている建物の方の学校でしたか？

→書いていて、どっちの中学校も「変な部分がある」中学校って
　気付きました。(汗)(^^；
　合併する噂を聞いた事ありますけどね。

　ちなみに僕は、小学校1校から、学年そのままの生徒で中学校に
　行った方でした。

### [9679] Rouark	2003.02.23 SUN 11:55:16　
	
すみません、自己紹介が遅れました。Rouarkと申します。以後お見知りおきをm(_ _)m　　　順序が逆でしたね。

### [9678] 圭之助（けーの）	2003.02.23 SUN 11:49:51　
	
皆さま、はじめまして！

「圭之助：けいのすけ」と申します。メンドウな時は「けーの」で略す時もあります。

ちょうど１年ほど前、友人からＣＨが復活しているという噂を聞きまして…以来コミックスを買い集めるわ、この歳になってＣＨワールドにハマリ込んでしまいました☆
今年３度目の干支を迎えるとゆーのに… f(^_^;)　うきゃ～

圭之助は生まれてこのかた、ずぅ～っと東大阪に住んでます。
確かここの常連さんの 将人さま（はじめまして！）と、ものすんごい御近所にいるみたいです。
ローカルなお話で恐縮ですが、圭之助も近鉄布施駅界隈にいるので、ひょっとしたら何処かですれ違っているかも…！？

機械にはウトイんですが、ちょくちょく遊びにきますね～。
皆さま、どうぞヨロシクお願いします　m(__)m
　

### [9677] Rouark	2003.02.23 SUN 11:39:03　
	
こんにちわ。皆さんはちゃんと毎週「バンチ」買えてますか？どういうわけか、ウチの近くの書店（コンビニ）は「バンチ」がたまにしか売ってません…「何故にバンチが売ってない？？たまにしか。」と嘆いています。所で、ここはＣＨの話題をしてもＯＫですか？

### [9676] 無言のパンダ	2003.02.23 SUN 00:33:45　
	
今気づいたんですが
（９６７５）の、いちのさまへのレス内で「いとのさま」になってました！すみませんっ☆「いちのさま」の間違いです（当然）

### [9675] 無言のパンダ	2003.02.22 SAT 17:25:48　
	
こんにちわ☆

夕べ、ちょっとうたた寝してて目が覚めたら喉がヘン！
これは風邪か？！やったぁ！バカじゃなかったよ～ゴホッ！

今週のＡＨ☆
私もＢｌｕｅ　Ｃａｔさまと同じく、あの指輪は冴子さんに渡してあげたほうがいいと思うんですけどねー。だってこのままでいるほうが「結婚する気で私と付き合ってたのか！？」って冴子さんも言ってたようにずっとこの先も槙村（兄）のことを引きずっちゃうんじゃないかなぁ。槙村の気持ちを伝えてあげたほうが、気持ちの区切りができて楽になれるように思うけど、どうなのかな？
でも人の気持ちを思いやるのも難しいですよね。どうするのが「正解」なのか結果がでないとわからないんだから・・・
冴子さんには、槙村のことは大切な思い出として心にしまって、新しい恋を見つけてしあわせになって欲しい。そう香も願っていたのだろうけど・・・やっぱり無理かもしれないですね。冴子さんて、ああ見えて「一途」なんだもん。ホント、見ていて痛々しいくらいに・・・(T_T)

＞いちのさま（９６６７）
いろいろ書きたいことはありますが、とりあえずこの歌をいとのさまに贈ります☆
♪いちのママは料理上手～おいしいプリンも作るよ♪(^o^)丿（慎＠ママのお＠ロック風）
今後もどんどん腕を磨いてくださいね！「未知なる生命体」いちのさま☆（笑）

＞将人さま（９６７０）
面白画像メールありがとうございました！昨日の夜やっと気づきました(^_^;)あのネタ☆かなり前に私も思ってました！やっぱりみんなそう思ってたんだ～（笑）
ＰＳ：インターネットの速度が速くなって快適になったんですか？よかったですね！(^^)

＞葵さま（９６７３）
おばあさまが無事回復されて本当によかったですね。救急車を呼んで付き添って行く時は生きた心地しなかったでしょう？
それにしても葵さまも足を痛めているのに、おばあさまをおんぶされたって？！それって世に言う「火事場の馬鹿力」ってやつ～？

来週はＡＨお休みですね(~_~;)
でもプレゼント応募のためにバンチは買わなきゃ！

### [9674] kinshi	2003.02.22 SAT 16:34:58　
	
むちゃくちゃ　お久しぶりですが・・・覚えていただいてますか？
最後の書き込みは　去年だったと思いますが・・・。

一度　遠ざかると　なかなか入りづらいものですね・・・。
そこんところを　しゃぁしゃぁ　と入ってしまうのが、
おばはん・・・なんだな～。。。のkinshiです。

ところで、今週の感想・・・楽しみにしていたら、あんまり
書き込みが少ないので、　ちょっとびっくり！！
槇村兄　の話なのに・・・。冴子と槇村兄　が、ラブラブ・・・
ということは、ＪＢの槇村兄が、香に　思いをよせていた・・・と
いうのは、パラレルなのね～～私的には、その方が、面白いと
思っていたんだけど。。。

今日は、久しぶりなので、ご挨拶程度にこの辺で。

ＰＳ　葵様（９６７３）
お祖母さん　無事で、何よりです。
救急車初体験いかがでしたか？・・・て、
そんなこと言っている場合でもないよね、（反省）

では、また。

### [9673] 葵	2003.02.21 FRI 21:22:20　
	
　あ～解禁だというのになんて疲れた日だっ！昼前に祖母が急に体調を崩して、家に人数が少なかったので救急車、呼んじゃいました。で、付き添いで救急車初体験！（こう書くからには、当然、祖母は元気に回復いたしましたけど☆）おかげで夕方まで病院に缶詰状態！「バンチ」かったのは今さっきです。（病院には売ってなかったのだ☆）売りきれてたらどうすんのよっ！まったく…心配させおってからに～っ!!（>_<;

　で、感想です。（すでに脱力感☆）リョウと槙村…切っても切れない仲なんでしょうね。槇ちゃんのお墓、ＣＨと違うってのは、もしかして槇ちゃんの最期もＣＨと違うのかなぁ…なんて思いました。だからあんなにリョウが辛そうなのかな…と。
　でも槇ちゃん、冴子さんに指輪用意してたんだね。アレを渡してたら、また違ったストーリーが出来てたんだろうなぁ…と考えると、ちょっと切ないです。冴子さんの将来を考えて指輪を渡さなかった香。でも冴子さんは、ずっとずっと槇ちゃんの恋人で一生を終える人だと思うよ。あの芯の強さはカッコイイ半面、見ていて痛々しくなっちゃうなぁ…。
　リョウも知らなかった槇ちゃんの遺した指輪。もしかしたら、リョウも知らない香の残したプレゼントがあるのかな…なんて考えちゃったりしました。香の性格からしたら…結婚記念に何かちょっとした贈り物を用意してたろうな…ってね。

### [9672] 将人（元・魔神）	2003.02.21 FRI 21:12:24　
	
こんばんは、将人（元・魔神）です。
今週号のＡＨは、槇村さんと冴子さんの話でしたね。
ＣＨの時も冴子さんが槇村さん好きだったかもという風な事を
言っていましたが、こんな槇村さんと冴子さんを見るのは、
初めてでした。
好きだったけどケンカばかりしていたんですね。
ＣＨで香さんの誕生日に真実を言えなかった事以外に
冴子さんにも言えなかった事があったんですね。

冴羽さんの前では強気に見せている冴子さんが悲しそうですね。
ＡＨでは、ＣＨの時みたいに、色香で冴羽さんを使う冴子さんって
場面は、まだ無いんですよね。

冴羽さんも槇村さんの事は、心の傷になっている為か、香瑩には、
あまり話していないみたいなんですね。

ＡＨでは日本的なお墓になっていましたけど、やっぱりＣＨの世界とは
多少違うという事なんでしょうかね。

### [9671] Ｂｌｕｅ　Ｃａｔ	2003.02.21 FRI 19:36:56　
	
　こんばんは～。　今週の感想です。
　今回はもう、読んでる間中きゃあきゃあ言ってました☆　冴子さんと槇村、ちゃんと“おつきあい”してたんですね♪　冴子さん、もしかすると、らぶらぶ（笑）なリョウと香が羨ましかったのかなぁ。だってリョウと香は結婚写真を撮るってとこまでいってたのに、結局槇村はプロポーズできないまま逝ってしまって・・・。用意してた指輪を渡すのは冴子さんを縛ることになる（今のリョウのように？）から渡せない、てことみたいだけど、でもわたしは、やっぱり渡して、槇村の想いを冴子さんに伝えてあげた方が、と思うんですけどね。ストレートに感情を出す冴子さんもかわいかった☆けど、「私はかつて　槇村秀幸という男を愛した・・・・・・　その事実だけでいいの・・・・・・」って言える冴子さんも、ステキだな、って思いました。
　槇村のお墓、Ａ・Ｈの世界では日本的なごく普通のものなんですね、なんか生々しい・・・あの中に、香ちゃんのお骨もいるのかなぁ。槇兄の死因、リョウの心にも相当傷になってるみたいだし、気になるんですけど・・・いつか、描かれる日が来るのかな。ていうか、物語の中では今、いつ？服はまだ、冬っぽいけど。なんとなく３月だったらいいな、なんて思ってしまいました、できればＣ・Ｈと同じ３月31日が。でも服装を見ると、たぶん違うんですよね。
　そういえばリョウと阿香、家での食事にナイフとフォーク使うとは、なんてハイカラ（笑）なの！　あれは、朝？昼？　どっちが作ったのかなぁ、わたし、あんな豪華なの作ったことないや。カップがふたつづつ、片方はもしかしてスープ？

＞葵さま
　絵ハガキ、ありがとうございました☆彡

### [9670] 将人（元・魔神）	2003.02.21 FRI 00:02:10　
	
こんばんは、将人（元・魔神）です。
自分の入っているケーブルテレビのインターネット、
工事の人が来て、モデムの交換をしてもらって速度が速くなりました
月額利用料が500円高くなるけど、ADSLよりも早い(最大で)
下り20Ｍbps・上り２Ｍbpsになりました

→でもインターネットの速度を測るサイトで見ると実際の速度は
７Mbpsぐらいでしたけどね
宣伝のは、他に誰も使っていない状態での理想値の最高速度だから

インターネットのホームページを見るのは、そんなに急ぐわけでは
ないけど、ホームページが表示されるのが早くなって、なんか
うれしいです～♪
昔は、深夜11時のテレホーダイの時間を待ってやってたのに
それを思うと、ぜいたくになってしまったな～

＞[9659] 無言のパンダ様　2003.02.19 WED 02:30:08　
ＡＨで香瑩（グラスハート）とりょうが出会った直後に、りょうが
ピンチになって、香瑩が助ける時に、「２００１カオリ革命」という
香水の広告ありましたね。
あれって、まさか、この香水の事書かれていたんでしょうかね？

→裏では、既にその香水の話が進んでいたので北条司先生が描かれたとか？

僕はあの漫画の時点では、ＣＨのキャラから香さんが亡くなって
香瑩（グラスハート）のなるＡＨって物語の象徴の意味で
「カオリ革命」だったのかなって思っていました。

北条司先生のキャラクターって、美女というだけでなく、個性的
ですから、色々なキャラの香水って出来そうですね。
ＣＨの依頼人を含んだら、かなりのバリエーションが・・・。
そうでなくて北条司先生の漫画の主要キャラでも、色々ありそうですが

キャッツアイなんかだと「昼(喫茶店)の香り」「夜(泥棒)の香り」
ってセットで、お仕事の時用香水とアフター５用香水とかに
使い分けれるとか出来そうやな～

＞[9661] 里奈さま　2003.02.19 WED 10:02:3
これから、ハワイなんですね。楽しんで来て下さいね。
空港で銃刀法違反で捕まってませんよね。＞ムチにコンペイトウって

→これを読まれるのは、帰ってからでしょうけど（汗）(^^；
　できれば、ハワイからのカキコ期待していますよ

＞[9662] OREO様　2003.02.19 WED 15:08:02　
OREO様は、カナダで面接なんですね。
カナダでは日本よりも失業率高いんですか？でも、がんばって下さいね

＞[9663] たま様　2003.02.19 WED 23:12:41　
学校で、この掲示板を見られていたんですか？（笑）
笑ってしまうのなら、学校では掲示板を見るのガマンしないと(汗)(^^；

＞[9668] 野上唯香さま　2003.02.20 THU 20:35:10　
片思いの彼に、バレンタインのプレゼントとメッセージを渡したのに
返事が無いんですか？

全部の男性が、僕みたいな性格ではないでしょうけど、その年齢の頃は
僕は（男子校でしたが）女性よりも精神的に子供で、自分の事だけで
精一杯で、
女性の方に、好きになられていると思うと、まだ自分では
恋愛などの事の対象だとは、思っていずに頭になかったので、

戸惑うのと、テレるのとか、色々な感情が交じってしまい、
どう返事したらよいか悩んで、すぐに答えが出ずに、しばらく
ちゃんと会話さえも出来なくなる事がありますよ。

まだ悪い返事を貰っていないのなら、悪い風には考えずに、彼も
どう返事をしたらいいのか悩んでいるのかもしれないと思って
良い風に考えてみて、待ってみては、どうでしょうか？

＞[9669] 葵さま　2003.02.20 THU 21:27:59　
足は大丈夫ですか？まだ痛みがあるのなら、忙しくてもお医者さんに
見て貰う方がいいですよ。
昔、ちゃんと治療に行っていたら、すぐに直った物が、忙しくて
中途半端にしていたら、直すのに余計に時間がかかった事が
ありますよ。

1人９役のフラ＠ダースの犬は、すごかったですね。
ジャイ＠ンがアム＠ってのは笑えましたね。

僕は、好きなゲーム・スパロボでアム＠の声は、最近も聞いて
いたんですが、そのゲームでは後のガン＠ム作品で、成長した
大人になった設定の＠ムロ声が多かったので、少年の頃の
ちょっとイジケた時期のあの場面が懐かしかったです。

同じ場面なのに、演じる人が違うとイメージ変わりますね。
ジ＠イアン声では（笑）

神谷明さんも、あのような声優という仕事として見せて貰う番組に
出て貰いたいですね。

### [9669] 葵	2003.02.20 THU 21:27:59　
	
　痛めた足についてのお見舞いメール下さった方々、さんきゅぅ～です。でもまだ痛いです。右足小指から踵にかけての、甲から裏にかけての広い所なので歩きづらいのなんのって☆金曜に痛かったら来なさいとお医者は言ったけど…やはり行くべきか？！

＞将人さま［９６５７］
　はい！私もその番組見ました。一人９役とかやってて、すごかったですね～。カンドーものでした♪（ジャイ○ンの声のアム○には笑えたわ☆）神谷さんもレパートリー多いし…いつの日か神谷さんのフルキャスティングで一本見てみたいですね。（←スゴイ勝手なお願いかも☆）

＞無言のパンダさま［９６５９］
　足を痛めた私に対してそのセリフは何？！しかも経験者でナイのにいばってからに☆でも松葉杖って、あこがれない？（^^;

＞千葉のマダムさま［９６６０］
　マウスカバーなんてもん、ホント始めて見ましたよ。でもすっごく可愛いの♪今度見かけたらマダムのやさぐれマウスちゃんへのプレゼントとして、１０個くらいまとめて送ってあげるね。（笑）

＞里奈さま［９６６１＆６４］
　まだいたの…ってか、今ごろはハワイア～ンなのかしら。昨日手紙送ったから行き違いだわね。しかし里奈さまのキャラを考えると、マジで射撃場でバンバン打ちまくって、ブラックリストに載せられたりして…？！無事に帰ってこられるのだろうか…心配だわ☆（^^;

### [9668] 野上唯香	2003.02.20 THU 20:35:10　
	
里奈様
もーハワイへ行ってしまったんですね(/_;)昼携帯にメールしたので帰国後にセンター問い合わせして下さい。私の修学旅行出発前夜と朝は里奈様メールくれたのに私は出来なくてごめんなさい。出来れば見送りに行きたかった（無理）『２０年目の再会！冴羽さん妹をよろしく（後編）』みたいに・・・。ハワイでインターネットカフェに行ったら私のメッセージ読んで下さい。

今年のバレンタインに同じ塾の片思いの人に手作りお菓子プレゼントして中にメッセージカードを同封し、それに「言葉では伝えられないこの気持ち形にしてみました・・・好きです。」と書きました。しかし先日塾に行きましたが彼は私に対して無言でした。失敗しちゃったのかな？何かむなしいです。彼に何てメッセージの事聞けば良いでしょう？？悩んでいます。渡した日なんか成功することばっかり考えていましたが、今になっては否定されることばかりにおびえています。これって情緒不安定？トラウマ？皆さん力になって下さい。アドバイスを下さい。昨日はこの人の夢を見ました。

### [9667] いちの	2003.02.20 THU 11:33:00　
	
おはようございます。
今日も猫に起こされ「美味○んぼ」を観ていたところです。
しかし、今日も寒いっすねー！！いったいいつになったら春は訪れるのでしょうかね～。

＞[9654] TAKESHI様
俺も高校の時に部活で筋トレしましたが、マッチョにはならなかったですね（汗）。マッチョになるまでやらなかったのかもしれませんが。
やり方が違ったのかもしれません。筋力はついているのに筋肉が肥大してなかったからね。
あ～、ソフトマッチョに憧れる～。

＞[9659] 無言のパンダ様
プリン作りは簡単ですよ。（もう忘れかけてますが）
つーか、近頃の男性は料理の１つも出来なくてはなりませんよ。なにせ、料理のできない女性が増えていますから（おぉ、嘆かわしや）。得意料理は目玉焼きなんて言われた日には涙もでませんことよ（悲）。実際にこういう人がいるから恐い・・・。
子パンダへの伝言；私は男でも女でもないのだよ、ワトソン君。もしかすると、人間でも無いのかもしれない。私の姿を想像してみてくれたまえ。君の類まれなる想像力なら私の姿を見ることが出来るかもしれないよ。ほっほっほっほっほ。

＞[9660] 千葉のOBA3様
めちゃくちゃなこぎつけですね（汗）。
あのイノシシは「オトコヌシ」でもなければ「オコトヌシ」でもありませんよ。正しくは「オッコトヌシ」です。漢字で書くと「乙事主」。
犬の話ししてたのにどうしてイノシシが出てきたのか・・・。確かに、私はイノシシ年生まれですけどね（全く関係ない）

＞[9661] 里奈様
やっぱり、こういう事になると、詳しいのね。
なんか力説しちゃってるし、先生コント始めちゃうし（恥）。
ムチの先端に花火を付けるのは点数加算できますか～？
つーか、水着屋の店長やめて、ＳＭグッズ専門店でも開いたほうがいいんじゃない（笑）？

＞[9663] たま様
おーまーえーはーあーほーか。（のこぎりを片手でたたきながら）
あれは狼ではなく、犬だって。山犬だって。犬の神様でしょうが。
それはそうと、声優さんって大変ですよね。神谷さんもいろいろ工夫なされていましたし。声が仕事の人は風邪とか花粉症とかになっちゃうとどうしているんでしょうか？
そうなる前に、予防処置をして、気を付けていただきたいです。

### [9666] 千葉のOBA3	2003.02.20 THU 10:37:55　
	
おはようございますー！！
今日は、なーーーんて寒いの！！外でたくなーーーい！！

９６６４　　里奈さま　　今ごろは空の上かい？気をつけていってらっしゃーーーい！！いーーなーーー、ハワイだって・・・。あったかいだろうなーーーー。クスン。私など、身も心もサイフも凍るようだぞーーー！！

９６６３　たまさま　里奈さまいない間、日本は少し静かかなーー？その分たまさまが、がんばって騒いでおくれーーー！！魚もたらふく食って脂のりきってるでしよーー！？

９６６２　ＯＲＥＯさま　　あんまり緊張してないようで、いいね、いいね、いつものＯＲＥＯさまを、アピールできますように！！

さーーーて、雨はすこーーーし降ったりやんだり・・・。寒いほうが、きつそうだけど・・・。行くぞーーーバン木！！

### [9665] 無言のパンダ	2003.02.20 THU 09:10:03　
	
おはよう～☆

＞里奈さま（９６６４）
なんだ・・・まだいたんだ？（笑）
そうか、今日出発なんだぁ～！気をつけて（ある意味これはおまじないの言葉ですネ^_^;）いってらっしゃぁ～い♪
おみやげは里奈さまの「勇姿」と「マグナム射撃秘話」だけでＯＫよ～ん！(^o^)丿
むこうでカキコ出来るとイイネ♪

### [9664] 里奈	2003.02.20 THU 06:04:57　
	
じゃじゃん♪
（『なんだ…まだいたんだ？』なんて言わないで！）
今日のお昼に出発するのに 未だに荷造り完成してないノンビリな里奈ちん。
荷造りに励めば励むほど、部屋が散らかってたいへんなことに！
このまんま出かけてＯＲＥＯ様の二の舞踏みたくないしぃ～
やっぱ片付けるべきなんだろうなぁ～…（滝汗）

向こうでカキコしたいけど できるかなぁ～？ノート型とはいえＰＣ持って行くのは嫌なんで、日本語でインターネットできるカフェ探してみよっ☆あぁ…今週はカキコも読めない＆書き込めないし、バンチも読めない！深夜放送のＣＨも見れない！

☆９６６３　たま様
『気をつけてね』って皆に言われるんだけど、
『うん♪』と答えるたびに疑問に思う…。
もしテロが起きたりしたら、どう気を付けたらイイの…？？
まっ、里奈は余裕で生きてるけどね！ってゆうか、危険なんてこれっぽっちも思ってない お気軽な里奈ちん♪
Ｘ線？そんなもんピコンピコン鳴ったら電撃ムチで叩き潰せば問題解決！
わけのわからん英語で激情されたら催涙スプレーで関門通過！
ちょっとリアルなＲＰＧだと思えば何でもできるって！
レベルＵＰして帰って来るからねぇ～♪

☆９６６２　ＯＲＥＯ様
えぇぇーっ！日本よりもカナダのほうが就職率悪いの！？
日本人がそっちで就職するってことは かなり厳しいんだんね！
そんな難関に立ち向かうＯＲＥＯ様なんかステキ☆
あんた輝いてるよぉー！キラキラ輝いてるよぉぉおーっ！（涙）
ところで就職祝いはカナディアン一匹でいいですわよ♪
（なんでテメーがもらうんだよ！）

### [9663] たま	2003.02.19 WED 23:12:41　
	
やめてぇー。あまり笑わせないでぇーーー。
学校で読んで、思わずクスクス・・・。こっぱずかしかったぞ。

＞いちの様　【9653】
何で犬と走ると「もの○け姫」になるんだよぉ～？
そんなシーンあったか？
犬じゃなくて狼？
あの狼、美輪さんの声がめっちゃマッチしてたなぁ。

＞TAKESHI様　【9654】
ゲッツはダンディー坂○さんのネタよ♪
ダンディーを知りたければ、内Ｐを見るべし。
たまサンの中じゃ、かんなりマイブームですぅ～♪

＞将人（元・魔神）様　【9657】
へぇーーっ！「まんが日本昔ばなし」ってあの二人が全部を演じていたのですかぁーーっ？！？(゜o゜)ビックリです。

＞無言のパンダ様　【9659】
「子パンダの耳にウンバラバァ～」
あはは。わしゃ「ロウソク使いの達人」なのかっ？
あっ！　た・・たしかに・・・・。

＞千葉のOBA3様　【9660】
いちの様のレスで、何で犬と走ったら「もの＠け姫」なの？って思ってたんだけど・・・。
あはぁ～ん！なるほどねっ！「オットコヌシ」ねっ（笑）
漢字にしたら「男主」？まさに漢やなっ。

＞里奈さま　【9661】
おぉっ！ハワイ楽しんできてぇー。気をつけて行ってらっしゃい♪
んでも、コンペイトウとハンマーはＸ線審査ＯＫだけど、
里奈様がなぁ・・・。危険物だからなぁ・・・。
ピーピー鳴り止まないんじゃないっ（笑）

＞OREO様　【9662】
いよいよ明日面接ぅ～？！がんばってね。運転も気をつけて。

### [9662] OREO	2003.02.19 WED 15:08:02　
	
コンバンワ☆
私も里奈様に続いて、いってきま～っす！！です☆
明日はついに会社面接当日！！それよりも、終わったあとの観光の方が楽しみになっちゃって・・。緊張もどこへやら～（笑）

＞９６４９たま様
いえ・・・実は散らかしたのは自分でして・・（滝汗）その散々な状態の部屋を、大家さんが合鍵で開けちゃったらしいのですよねぇ～・・・（汗）今、この家、売りに出してて、家を見に来た人に部屋を見せてたみたいで・・・。きっと、そのとき、家を見に来た人は、私の部屋を見て、買う気も失せたに違いありません・・（笑）大家さんの営業妨害！？

＞９６５０里奈様
里奈様も明日からハワイなんですか～？？持ち物、ダメっすよ。そんなもんもってたら、ソッコーＸ線引っかかって、強制帰還命令でちゃいますって（笑）気をつけて、そして楽しんできてください！！
カナダは日本より失業率が高いので、どうしても日本人を雇わなくてはならない事情が無い限り、カナディアンを雇わなくてはならない国事情があるんです。だから、結構厳しいなぁ～・・。とにかく、頑張ってきます☆

＞９６５１千葉のＯＢＡ３様
花粉症の時期って、目がものすごい疲れて、こすって余計に疲れて、鼻も詰まってストレスたまって、イジョ～～に眠くなったりしてましたねぇ～☆でも、寝てると、痒くもなんとも感じないので（当然？）この時期は寝ているときが天国でした（笑）
コッチも、明日、雨・・というか、雪らしいです・・。明日運転気をつけないと・・・。本当は晴れて欲しいのですけどねぇ・・。

＞９６５５葵様
足大丈夫ですか～？？？でも、骨折とかしてなくってよかったです☆骨折なんて、しないほうが良いです！！メチャメチャ痛いだけですホント（経験者は語る☆）
う～ん。やっぱり、僚の３５７は人気ですね～☆どこ行っても「見本のみ」にあたっちゃってますよね～☆やっぱね～。だって、カッコイイですものねぇ～☆

＞９６５９無言のパンダ様
家を今、売りに出してるらしくて、家を見に来た人に、部屋を見せるのに、大家さんが合鍵で開けちゃったんです・・・☆も、ほんと、泊りに行くのに、荷物作るだけ作って、そのまま出てきちゃったんで、見るも無残な状態になってたんで、絶対見に来た人、購入やめたと思います（笑）
無言のパンダ様のように、親切心で開けられたのならいいのですが、ビジネスで勝手に開けられてっていうのが、ちょっとプライバシーの侵害！？

では、明日はとっても朝早いですし、丁度良いぐらいに眠気がきたので、オヤスミなさいませ・・・☆

### [9661] 里奈	2003.02.19 WED 10:02:39　
	
じゃっじゃぁ～ん☆　里奈ちん登場！ ヽ（≧ヮ≦）ノ ウホホォ～♪
（今更 珍しくも何ともねぇーよ…）

里奈ちんは明日からカキコしばらく お休みっ☆
そう！とうとう この時が来たのだよ！里奈は明日からワイハァ～へ
『たぁびぃ～だぁちぃぃ～ますぅぅう～♪』※狩人風
まだ出発まで１週間ぐらいあるってボケてた里奈ちん。今焦って荷造りに奮闘しております（汗）マジで実感ねぇー！何持っていきゃぁーイイんだ！？えぇーっと…機内にも持ち込める携帯用電撃ムチにぃ～、ハイパワー電気ショックでしょぉ～。あっ☆チェーン付き鉛のコンペイトウってＸ線通るかな？気合で何とかしよう！…って、全然ダメじゃん！こんなんじゃハワイ行けない！ハワイ上陸セカンド記念１００ｔハンマーも持ってかなきゃ♪
服？そんなのビキニ一着あれば十分よ。獲物捜すぞぉー!!
（注※ホントは とっっってもピュアなのよ★）
そうそう、偶然なんだけど、同じビルで働いてた店長仲間の友達とハワイ旅行かぶってんのよネ！向こうで会って遊べる♪

☆９６５９　無言のパンダ様
里奈はいつだってムチの構えできますわよ！その日の気分でいろいろ隠し持ってるもの♪さぁ～、共に いちのっちを『全身網タイツか!?』と思わせるくらい切り刻んじゃいましょっ♪
硝煙の香を缶詰に…？ え！えぇー!?どうやって!!?
お土産に売ってるかなぁ～？（絶対無いっ!!）

☆９６５８　将人様
そうそう、『ゴブリン』ってザコ的キャラ☆
でも 容姿さえ思い浮かべなきゃ、カワイイ響きじゃない？『リン』ってとこらへんが。男の人を『ちゃん』づけで呼ぶような感じでさ…（無理矢理…？）

☆９６５５　葵様
あぁーなんかわかるぅ～！里奈も１回も骨折したことないんだけど この頑丈なとこが逆に嫌で、昔 階段から落ちたぐらいで骨折してる子とか見ると可愛く見えた。（間違ってる…絶対）まっ、頑丈で何よりだけど！でも何度も事故ったりしてるからなぁー…体のアッチコッチの関節をバキボキ鳴らすことができます！（自慢のつもり）

☆９６５４　ＴＡＫＥＳＨＩ様
ちょっと！インターネットならまだ受けつけてるのかい!!?
でもどこ見たらイイの!?里奈がいくつか見たとこだと全部予約終了してたんだけど！ホント マジで欲しいのよぉー!!でも里奈明日からハワイだし…。金沢ならまだ店頭行ってもＧＥＴできそう？里奈のイトコが金沢だから頼んでみようかしら…。

☆９６５３　いちの様
うおぉっ!?質問責めオンリーやんけ！（汗）
なぁ～に？ＳとＭについて知りたいの？仕方ないわね坊や♪
その白い体にビシバシと身をもって教えてあげたいとこだけど…
今回はまず知識から叩き込んであげるわ！
Ｓ→『サド』　Ｍ→『マゾ』 のことネ。詳しく語ると…
サドは英語で『sadism』、マゾは英語で『masochism』
sadism→『相手を虐待して快感を味わう変態性欲』
masochism→『異性からの虐待を喜ぶ変態性欲』ってとこネ☆
コレ学年末テストに出ます。次回は『正しいムチの振り下ろし方』と『ムチを受けた時の正しいリアクション法』についての講義です。各自ムチを購買で買っておくように！オリジナリティー溢れる改造を施した者に対しては点数ＵＰよ♪一人で見せに来るように。

☆９６５２　野上唯香様
ハワイだよぉー！
忘れてたわけじゃないけど、まだ日に余裕あると思ってたよぉー!!
水着新店舗知りたい？『○ーパ』と『大○』。あと、今まで通りの『フィス○ー』です。里奈は主にフィス○ーにいまぁ～す♪
ポスター、没になったんだ？そりゃぁ～あの香ちゃん裸だもん！
節電ポスターに露出はダメだろうねぇ～（汗）基本的に、里奈のイラストは露出系が多いから、学校に飾れるようなものじゃないと思う…。じゃぁ～清純っぽいの描けってか？（笑）
