https://web.archive.org/web/20030105052620/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=317

### [6340] Aoi	2002.04.09 TUE 04:08:41　
	
みなさんこんばんわ･･･といってももうすぐ夜が明けますね。私も今日からこちらに参加させて頂きたく、皆さん宜しくお願いいたします。実は今日遅ればせながらCHの最終巻とAHの３巻までを読みました。CHの最終巻は何だかずっと怖くて読めなかったんです。（すごく、新宿に近いところに住んでいる事もあるのか、それに街の描写が写真のようにリアルですね。）自分の中で最終巻という事に整理がつきそうにありませんでした。つきなみですが、とても感動し共鳴しました。その位にしないと嫌がらせのような長い書き込みになりそうです。私は香と年が近いのですが、香のように、まっすぐ（私はそう感じました）生きていないと感じ、これから少しずつ、まっすぐに生きるようにしたいと思いました。少し長くなってしまいゴメンナサイ。

### [6339] kinshi	2002.04.09 TUE 00:40:55　
	
こんばんは、　皆さんの励ましのお言葉、ありがとうございます。　　　　　

６３３０＞無言のパンダ様
無言のパンダ様も買いましたか　あの本・・・・
あの本って、有名人のこととか、書いてあって、離婚や、自殺のことも
書いてあって、結構　怖いこと書いてありますよね、
ドキッとしてしまいます。
私も、無言のパンダ様見習って、封印しようかと思います。
本当にありがとうございました。


＞６３３２ひろ様
同じ境遇の人がたくさんいる。というのは、本当にそう思います。
全人類を数種類にしか分けられない運命なんて・・・って
冷静に考えれば、そう・・・ですよね。
気分転換が、もっとじょうずに出来れば、いいのに・・・と
思います。リョウちゃんに耳元で、イッパ～ッ！と言ってくれたら
元気出るかしら？　　　ナンテ（~_~;)
ひろ様の気分転換・・ってなにか　ありますか？


＞６３３３千葉のＯＢＡ３様
皆さんの暖かい励ましに徐々に回復しつつあります。
パソコン買ってよかったとつくづく思いました。そして、ＣＨやＡＨに
出会えた事も・・・。数年前までは、考えられなかったです。
もう１０年これが早かったら・・・と　年長組の私は、思うのですが。


＞６３３７葵様
葵様がおっしゃる通り努力に勝るものはない　と思います。
一人で、悶々と悩んでいると　何も頭に浮かばなくなって、
思考能力とともに回復力の仕方も忘れてしまいそうになります。

今日届いた、ペーパーウエイト・・・の１００ｔハンマーで、
香ちゃんに叩いてもらいたい気分です。　そしたら　すっきり
するかしら？　悪魔のささやき　も近づけないように。

長くなりまして・・・それでは、また。

### [6338] いも	2002.04.09 TUE 00:38:51　
	
こんばんわ。
新しい方がたくさんいらっしゃいますね。何だか嬉しいですね。
って、私もまだかなりの新人ですが‥。（汗）
‥‥よろしくお願いします、はい。

＞かおりさま[6324]
恐縮ですっ！！
ＡＨを読んでよかったと思って頂けて、良かったです。（私が描いてるんじゃないのに偉そうに・汗）
これからもＡＨについて語り合えるといいなあと思います。
しかし、高校生かあ。若いですねえ。（溜息）

では、明日のバンチを楽しみに今日はこの辺で。
いもでした。

### [6337] 葵	2002.04.08 MON 21:15:20　
	
　明日は「バンチ」あるんですよね？…というか「ＡＨ」、ありますよね？香瑩の余命ってどぉゆうことか、とっくりと読ませていただきます！！（笑）

＞ｋｉｎｓｈｉさま［６３２９］他・皆さま
　占いはイイコトばかり信じるようにしてます。まぁ、｢注意書き」みたいなのはチェックしますけどぉ…。でも、運て努力次第でしょ？ｋｉｎｓｈｉさまが努力なさった分、しっかりとイイコトが待っててくれると思いますよ。リョウも香も、香瑩も、みんな努力して「幸せ」を手にしてるんですもの。ｋｉｎｓｈｉさまもがんばって！そんな私は、ノー天気な双子座のＯ型人間です☆（私の運勢も、すごい事書いてあるのかな？！）

### [6336] ゆうちゃん	2002.04.08 MON 14:37:59　
	
無言のパンダ様[6315][6318]
いっつも亀レスですいません。キーホルダーの話題、そーいえばありましたね。でも、秋田に住んでいる私は、ディズ＠ーシーは、気軽にいけるところではないし・・。通販好きの私としては、やっぱりコアミックスさんに、お願い！！したいところです。
あと、アニメムックによると、リョウの車は「モーリスミニ・クーパー」とあります。

### [6335] トレゼゲット	2002.04.08 MON 12:10:56　
	
こんにちは。
今日、はじめて登録した新参者です。CITY HUNTERを
最近読んで、久し振りに漫画の楽しさを味わいました。
今はA･Hを読んでます。ということでよろしくお願いし
ます。

### [6334] 大阪ハナ	2002.04.08 MON 11:48:07　
	
はじめまして。
約四年振りにネットに繋げる環境に身をおけるようになり
早速おじゃまさせていただきました。

その間、F･COMPOの連載が終了し、A･Hが始まり、と
月日が流れるのは真に早いものと痛感せざるをえません。

またちょくちょく遊びにこさせてもらおうと思ってます。
よろしくお願いします。


### [6333] 千葉のOBA3	2002.04.08 MON 08:45:33　
	
おはようございます。
今日は、夏日になるとか言ってるけど,今はまだ肌寒い。
昨日は、家族で釣りにでかけて私の一人勝ち。超気分が良かったのでありました。で・・・。

６３２８ちぇりーさま　いいなぁー、３つとも買われたんですか。立体的なのは、やはりハンマーですか。私結局パソコン横の出窓にさりげなくおいたんですけどね。ホント、もうちょっと、値段もお手ごろならなー。と、思いました。

６３２４かおりさま　高校生ですか・・。いいですね、すべて、これからだもんね。絵の勉強されてるとのこと、若い？頃の自分を思い出します。（もっとも、それに関してはいまでも勉強中です。・・要するに、終わらないこと・・かな。）
北条先生は、ほんとに素晴らしいです。すごいの一言に尽きる。
これからも、勉強がんばってください、そして、今ドツボにはまって思いっきりめげてる、このＯＢＡ３に若いパワーを、時々わけてくださいませ。

６３２９ｋｉｎｓｈｉさま　　占いですか、私も気にするほうでした。昔はやった、天＠殺とかいうので、私が２０代前半で片親になるとかいうことまで当たってて、一時期本気で信じてました。でも、ぜーったい人生占いのとうりにはならないから・・。
どんなに、占いの結果がよくても、思いもかけなかった不幸にみまわれる事もあるし、その反対ももちろんある・・。
なんかさー、自分のポリシーとしては、絶対いつもポジティブでいようと、いつも思ってはいるんですけどね。
そうなれない時もありますよね。　でも、悪い事ばかり続かないって、絶対に。また、いい方向に行く時は必ずくるし・・・。

そうする為には、やはり気分を明るくもってなくちゃね。
・・・と、ｋｉｎｓｈｉさまに、いってるようで、実は自分をふるいたたせてるようなカキコになってしまいました、ごめんなさい。でも、ほんとうに、がんばりましょう。

・・・しかし、香ちゃんて、なんで、いつもあんなに前向きでいられるんだろう？彼女だって決しておいたちは幸福とはいえないのに・・・。あの、ポジティブな姿勢は、阿香の中の心臓になっても変わらない。いまでも、りょうちゃんや、阿香を支えてる。あの前向きさが・・・。　　そういうとこ、スゴイと思う。


### [6332] ひろ	2002.04.08 MON 03:32:03　
	
こんばんは(^o^)丿

ペーパーウェイト、私にも送られてきましたヨ。みなさんと同じハンマーなんですけど、ちぇり～[6328]さんのカキコを見て、少しホッとした感じ。総評ありがとうございました。

久々にレスで～す。
≫かおり[6324]さん　はじめまして!
高校入学おめでとうございます。義務教育から脱皮して、新たな学校生活スタートですね。高校は、社会人になるための準備段階です。失敗を恐れず、未知の分野に挑戦して、たくさんの情報･知識を吸収していってください。
ところで、初めて少年雑誌(バンチ含め)を買う時って、女性の方はドキドキですか。牛丼屋に初めて入る気持ちに似てるとか･･･。男性から見て、女性が少年雑誌を買うのは気にならないですけどネ。よっぽど、最近の少女性雑誌を買っているほうが気になりますよ。これからも、Ａ・Ｈ応援してくださいね(コアミックスの回し者って言われそう！)。

kinshi[6329]さん
私は占いは信じないほうなんですけど、占いが当たるとすると同じ境遇の人がたくさんいるとか･･･。
でも失敗って、決して良くないことではありませんよ。人生失敗と成功の繰り返しだし、失敗を恐れたら人間は成長しません。小さな失敗は「まあ、いいっか！」程度で済ませ、人に迷惑をかけるような失敗は、これからは、そんなこととが無いように心掛けて行けばよいのではないでしょうか。
大事なのは、その失敗引きずらないで、これからどう活かせるかという、先を見つめる目だと思います。
気にしだすと更に失敗を繰り返すことも良くあるので、早めに気分転換してくださいね。いまこそ、心の中にシティハンターが現れることを、お祈りいたします。

長くなりましたので、今夜はこの辺で！
おやすみなさ～い(^^)/~~~

### [6331] 趙雲	2002.04.08 MON 01:40:13　
	
こんばんわ。
今日はじめて登録さしていただきました。
北条さんのCITY　HOUNTERを読んで感動しました。
また、新しい北条さんの漫画が読みたいです。
　　　　　　追伸：あぁ、言いたい事は山ほど在るのにまとめれないので、初回はこれで、終わりにさしていただきます。

### [6330] 無言のパンダ	2002.04.08 MON 00:34:08　
	
こんばんわ★（何度目？）

＞ｋｉｎｓｈｉ様（６３２９）
細木＠子さんの例の本！私も数年前に初めて買って、それが今のｋｉｎｓｈｉ様のように最悪な運勢で、めっちゃくちゃ落ち込みましたよ～(T_T)読めば読むほど気になるので、もう途中で本を封印して無視して生活しました。
だいたい悪いことを回避するために書かれているらしいですが、気になる人はとりつかれたように気になりますからねぇ！
そんな時はやはり私のように封印してしまったほうが、精神衛生上良いのではないでしょうか？
お役に立てないコメントですいません(~_~;)
元気出してくださいね。

おやすみなさい(-_-)zzz

### [6329] kinshi	2002.04.07 SUN 23:54:17　
	
こんばんは、
４月・・・６３２７luna様がおっしゃっている様に、
新しい　何かが始まるような・・・新緑とともに新鮮な
気持ちになるのが、普通だと思います。でも　信じていいのか
わかりませんが、あの　占いで、有名な、細木＠子の
占いの本によると　私の４月は、大殺界　という一番悪い月
だそうです。３月から始まり５月まで、よくないらしいのですが、
・・・・そういわれると　なんだかへんてこなことばかり
やり始めて、失敗つづき・・・
みなさんは　こんな時、どう過ごしていますか？
信じなければ、それまでですが・・・・
何とか立ち直りたいkinshiでした。

### [6328] ちぇり～	2002.04.07 SUN 23:31:41　
	
こんばんわ♪

ペーパーウェイト、うちにも今日届きました☆
やっぱりハンマーを購入された方が多いですね。

＞千葉のOBA3さん
ちゃんとここにいますよ(笑)＜カラスとトンボ
最初はどれかひとつにしよう、と思っていたのですが、
結局３つとも買ってしまいました(^^;;)
限定という言葉にすっごく弱いんですよね、私。
カラスとトンボもなかなか良いですよ。
けど一番立体的で3Dらしさがあるのはハンマーですね。

### [6327] ｌｕｎａ	2002.04.07 SUN 23:15:48　
	
こんばんわ、みなさん、元気ですか？
４月ってなんか、新しいなにかが、待ってるよーで、
気持ち、初心に戻りますねぇ
コレクター無言のパンダさん、ハンマーだったんですね
そして、お気に入りになったよーで、よかったです（＾＾）
インテリアになりそーですね～
ミニカーも、実現したらいいのにな～
って、思ってますよ
火曜日、待ち遠しいですね
なんか、余命が・・・とかなってたけど・・・
シテイハンター、復活！
には、うれしい、驚きでしたよ
それと、冴子さん、かわいかったですね～
えーっと、女の人の年齢の事を言うのは、失礼になるかもやけど
冴子さん、とっても、素敵ですよね
カッコイイ女の人ですね
憧れますよ～
目指せ、カッコイイ女になる事！

### [6326] 無言のパンダ	2002.04.07 SUN 22:29:45　
	
こんばんわ★

「つねに明るく生きるのは、白い紙に白いペンで絵を書こうとするようなもの。絵にするには暗いところもないとね。」

これ、娘が春休み前の終業式にある若い先生からもらったメッセージなんです。（全員の生徒に手書きで贈ったそうです。ちなみに担任ではありません）
うまく言葉では表せないんですが、とても感動してしまったので関係ないことですが書かせてもらいました(^_^;)

香瑩も無理に笑顔を作ったりしなくていいよ。いつか自然に笑える時がきっと来るから・・・。
リョウも香を失った悲しみを無理に隠さなくてもいいよ。
みんな「自然体」が一番だもの。

・・・なんちゃって。
失礼しました！(^_^)/~

### [6325] 葵	2002.04.07 SUN 21:14:55　
	
　ペーパーウェイト、続々届いてるようですね。買っときゃ良かったかな？（でも高いし☆）コアミックスさま、もう少し安価でいいので、全プレの企画、お考えになりません？（笑）

＞かおりさま［６３２４］
　（少女コミックで育った私も）北条せんせのファンになり、一番初めに「オールマ○」を買ったときは恥かしかったですよ☆（ジャンプは立ち読みしてました。すんません☆）でも、これからは堂々と「バンチ」を買ってくださいね。北条せんせへの「愛」がアナタを応援してますよ♪私など火曜日に本屋へ行くと、おばちゃんに「はい、バンチだね？」と言われますもん。（笑）

### [6324] かおり	2002.04.07 SUN 19:55:16　
	
今晩和。かおりデス。
A.H1～3巻かいました★
とてつもなく切ない気持ちだったんですが
全く「悲しい」という感情はありませんでした・・。
おかしいでしょうか？
自分でもよくわかんないんですが・・・（汗）
「精一杯生きろ！」というﾘｮｳの言葉に心を打たれました。
香は生きてる。
なんて温かい女性なんだと想いました。
素敵すぎる。
全巻読んだ感想は全てひっくるめて「おもしろい！！」です。
実は少年漫画を買ったのは初めてです。
そしてこんなにﾊﾏったのも初めてです。
技術が凄い。と、想いました。
とりあえず「絵」の勉強をしているｱﾀｼにとって
北条先生（でいいんですか？？）の絵は凄く魅力的に見えました。
凄いっす！

明後日から高校生です。
A.Hを読むほんの前までとても鬱で行きたくありませんでした・・。
でも、頑張ろうって本気で思いました。

なんかもの凄く駄文ですね・・。でわここらへんで！

>>[6314] いもｻﾝ★
初ﾚｽでﾄﾞｷﾄﾞｷです！
A.Hを買おうか迷っていたのですが
いもｻﾝの言葉で買おうと決めました！
ありがとうございます（＾＾）
これからもよろしくお願いします！

### [6323] 千葉のOBA3	2002.04.07 SUN 19:32:53　
	
わーい！私もペーパーウェイト届きました。
うんうん、無言のパンダさまのいうとおり、なかなかキレイ。
私もハンマーなんですけど、これでカラスやトンボだったら、どんなんだろう？立体的なカラスとトンボって見てみたい・・・。どなたか、カラスか、トンボ購入した方います？（誰もいなかったりして。）・・・確かにカップラーメンの重しには、重すぎですね。やったらヒサンな結果になります。しまっておくのもなんだし、どこにおいておこうか、考え中です。

### [6322] (削除)	2002.04.07 SUN 19:26:45　
	
他サイトへのリンクはセキュリティの関係上ご遠慮いただいております。また商品等の販売に関しましてもお控えくださいますようお願いいたします。なお書き込みのありました品物は高品質の複製原画（プリマグラフィ）です。（管理人）

### [6321] 無言のパンダ	2002.04.07 SUN 15:40:59　
	
こんにちわ☆

ペーパーウエイト、たった今届きましたよ♪
思ったよりも出来が良く、ちょっと感動しました！
「文鎮」なんて言ってごめんねって感じです(^_^;)
ＣＨとコアミックスのロゴの入った、結構立派な箱に入っていて、クリスタルの中に繊細な描写で描かれた「ハンマー」が、立体的に浮き上がっていて、とても綺麗です♪
でも、とても重いのでカップラーメンの上に乗せたら、一瞬のうちに「ラーメンの具」と化して沈んでしまいそうなので、ご注意ですよ（笑）
