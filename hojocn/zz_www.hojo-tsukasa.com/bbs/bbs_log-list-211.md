https://web.archive.org/web/20020618121823/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=211

### [4220] 葵	2001.10.31 WED 21:49:37　
	
　久々の葵です。忘れられてないかな？

☆感想☆
　ＧＨの「心臓を返したい」という思いはわかるけど、でも、リョウとしてはやっぱり、香を二度も死なせたくないですよね。「たとえ明日までしか生きられなくても～仲間を作るには十分な時間だ」のセリフにはボロ泣きでした。ＧＨが真実、リョウを信頼した瞬間じゃないのかな。リョウもＧＨも、すっごくイイ顔してるのが嬉しい♪さてさて、蜂の巣になりかねないホテル街･･･。ＳＡＴと青龍部隊を相手に、リョウとＧＨ、どうするんだろ。冴子さんの決意した顔が痛々しいなぁ･･･。一週間は長いっ☆

☆「キャッツアイ」再放送☆
　某地方局の朝のアニメタイムで始まったＣＥ。帰宅してビデオを見て一言。「でたっ！空飛ぶオシリ!!」泪も瞳も愛ちゃんも、そして俊夫も、皆みんな懐かしすぎるっ☆気分は２０ン年前です。（恥）

☆《猫眼》重播☆  
CE始于某地方电视台早上的动画时间。回到家看了录像后说了一句话。“出来了!飞翔的屁股!!”泪、瞳、爱，还有俊夫，大家都太怀念了☆心情是20年前。(害羞)  


### [4219] Ｂｌｕｅ　Ｃａｔ	2001.10.31 WED 21:33:26　
	
　［４２０３］のまろ様のカキコミをＡ・Ｈ２２話を読んだあとに読んだので、なんてタイムリー！って思っちゃいました。ゲスト（？）で次原さんが出てるのを見て、本当にまろ様のカキコミにあったような会話がされてたりして、なんて思ったりして。次原さんの机の上にミニカーがのってるとことか、なんかお茶目♪本物の次原さんの机もあんなカンジなのかなぁ。
　一年以上Ｃ・Ｈとしての仕事をしてなかったはずのリョウがあそこを久しぶり（？）に通路として使うために入ってきたのに、次原さんが何事もなかったように普通に接してるのは、リョウが少しづつ“自分”を取り戻しつつあるのを感じてほっとした、ってのもあるのかなぁ、と思ったりもしました。あのため息にはそういう意味もあるのかな、と（あ、あれは単なるタバコの煙？）。Ａ・Ｈの世界ではこの二人も友人だったりするのかな、だったらこれからもたまにでいいから登場してくれたら面白いのに（笑）
　ところで、リョウとＧＨが天窓を破って落ちたあの部屋ってもしかしてサエバ・アパート！？って思っちゃったのはわたしだけでしょうか（笑）だから隣の次原さんの仕事場も通り道に使ってたりしたのかな、なんて思っちゃったんです。Ｃ・Ｈの頃とアパートが違ってるのは、ほら、パラレル・ワールドだからってことで（笑）
　今回のＡ・Ｈを読んで、わたしはやっぱりリョウのことが好きなんだなぁ、って思いました。ＧＨに「生きろ」って言った気持ち、香のことまだ完全に割り切ったわけでも吹っ切ったわけでもないんだろうけど、リョウ自身に言ってるようにも感じました。あの言葉どおり、リョウにも精一杯生きてってほしいです。
　それでは、今日はこの辺で。　

### [4218] 海里	2001.10.31 WED 19:21:21　
	
今晩の夕食はカレーです。
煮込んでいる間に、‘Human System’を聞きながらのカキコです。
‘Human System’を聞いてて、ふと思ったんですけど、
この曲って、ＧＨに合ってません？
あたしって、曲の意味とかわからない方なんですが、
今聞いてて、ちょっと思っちゃいました。
「え～っ、違うんじゃない？」って思った方、ごめんなさい… m(_ _)m

◇今週のＡＨ◇
最後のページを読んで、直感的に、
「冴羽さん、死んじゃうかも…」って、
不吉なことが脳裏に浮かんでしまいました…
でも、あたしの直感てはずれるんで、
そんなことは、ぜ～ったいにないと思うんで。

冴羽さんて、ＧＨを通して香さんを見てますよね？
特に、ＧＨを見る目がそう感じるんですが…
冴羽さんがＧＨを守るのは、
それと同時に、香さんを守る。
今度こそ、香さんを自分の手で守る。
そんな、冴羽さんを見て、
なんだか、涙が出てきました…
今週は、そんな悲しい話じゃないのにね。
急に、１話からの話が思い出されてしまって…

涙もろい、海里でした。
ではまた！

### [4217] ＣＲＡＺＹ ＣＡＴ	2001.10.31 WED 18:58:44　
	
こんばんわ。ＣＲＡＺＹ ＣＡＴです。
ついに解禁です。今のところ解禁第一号だけど、送信する頃には、だれか入ってるかな(タイピング遅いからなぁ～)。というわけで(どーゆうわけだよ!?)感想文。でも、その前に一個だけレス。

＞無言のパンダ様【４２１３】
　イーですねー。すぐ寝れるんですか～。ぼくは寝るのにかなり苦労するんですけど。まぁ最近は良く疲れるんですぐ寝れる、と言うより、飯食うまでが限界で、そのあと力尽きるって感じ。
　はい、すっきり目覚めるためのコツ、ですか。これは睡眠家のぼくにも難しいです。簡単に(というよりテキトーに)言うと、『気合い』です。まぁ～、よーするに目覚ましがなったらパッと起きようとする意志です。あとは、と言うよりこっちの方が大事かも。疲れを残さない。前日部活とかあるとやっぱり起きるのつらいです。そーゆうときは、(無言のパンダさんって主婦ですよね？だから、部活とかは無いと思うんですが、バーゲンとかやってて多く買いこんでしまったとか、気合い入れて掃除したときとか)できるだけ寝る前に疲れを取るんです。お風呂にゆっくり使ってみたり、入浴剤使ってみたり。他にも誰かにマッサージしてもらうとかね。まぁ、『適度な疲れ』に近づけるんです。そして、寝る。聞いた話によると、夢を見ている状態は熟睡ではないとか。なんか考え事とかあると筋肉や脳が緊張状態にあるらしく、逆に疲れてしまうので、全て忘れて。あと、二度寝はダメです。　どうしてもダメだったら、爽やかだと思い込んでください。
　なんかすっごいヘンな文だし、答えになってないと思います。これはあくまで『ぼくの場合』です。

　感想文まで書くとすっごい長くなりそうなので、予定を変更してこれで終わりに。明日は、基礎力テスト(中間テストの中間テストみたいなやつ)があるので勉強もしなきゃ。

ＣＲＡＺＹ ＣＡＴでした。

### [4216] ちゃこ	2001.10.31 WED 18:40:01　
	
解禁！ということで、今週号の感想です。（書いてるのは、昼間ですが(^^;;)）

「ＧＨのこと」～先週に続いて手紙風～
先週も書いたけど、やっぱりあなたは心臓をリョウに返したかったんだね。
だからリョウの「君の体から取り出せば、君も心臓も死ぬだけだ」という言葉は
辛い現実だったと思う。（同時に、リョウの想いの深さも知ったのかな。）
多分、本当に心臓を取り出してもらえたら、あなたは笑って死ねたんだろうね。
「初めて、誰かの為、役に立てた」と。でもそれは。やっぱり出来ない相談で。
あと・・・気付いてますか？あなた自身が変わってきてることに。
最初、あなたが死を選んだのは、ただただ「現実から逃げる為」だった。
でも今回は（「どうせ長く生きられない」という気持ちがあったにしても）
「存在する世界が違っても想い合う、リョウとカオリの為」だった。
そんな風に「誰かの為」を思うあなたの事、私、結構好きです。だから。
以前、カオリがあなたに言ったように「生きるために戦って」欲しいです。
それから、今回リョウも言ったように「君には２人分の命が宿っている」事
この先、何があっても、忘れずにいて欲しいです。
そして、どんなギリギリの状態になっても。
最後まで「生きる道を選び続ける事」を諦めないで下さい。
あなたがそんな風に生きられるなら、リョウの「仲間」になっていいから（笑）

「リョウのこと」
リョウの唯一の望みは「香を二度死なせないこと」なんだと改めて思いました。
あと、今回読みながら、ふと、再会したときの「香の涙」を思い出しました。
香は「自分という命綱」がいなくなった後、リョウが「死に急いで」しまうのを
心配していたと思うのです。
だから「生きていてくれて良かった！」という安堵も含まれていたのかなと。
そして、そんな香の想いが、リョウに届いていれば良いなぁ。
リョウもまた、ギリギリの状態になったとき「生きる道を選び続ける事」を
諦めないでくれればいいなぁ。と思ったのです。
そう。GHに言った言葉は、リョウ自身にも言い聞かせていて欲しいのです。

「冴子さんのこと」
どうやら「愛する者を想う女」らしいですね（笑）
でも、本当「愛」には色々な形があるから。もう、追求しないでいよう(^^;;)
ただ。この１年、冴子さんが辛かったのは確かだと思う。
リョウの心が「緩慢に自殺していく」様子を見続けてきたわけだから。
支えたい相手に、自分の言葉が届かない。こんなに切ないこと、他に無い。
自分の無力さが、悲しくて、情けなくて・・・腹立たしかったと思う。
今はただ、色々な想いを「署長の顔」の下に隠して、リョウの生還を・・・
奇跡を待っているのでしょうね。

一向に明けない新宿の夜。そんな「不夜城」新宿の夜が明ける時。それは奇跡が
起きる時なのかな？なんて思いました。　では、消えます。これにて、ドロン！

### [4215] まろ	2001.10.31 WED 18:37:52　
	
こんばんは。先ず、はじめにＫ．Ｈさま・ちゃこさま初め、山梨にお住まいの方、ゴメンナサイ！！
　学生のとき、少年ジャ○プが（神奈川県の様に）月曜でなく、火曜発売だったので、てっきり水曜発売とばかり思い込んでいました（そそっかし過ぎ！）。ホントにごめんなさい（汗）
　あ、もう解禁ですね。でもその前に、今週号の最後のアンケートの４番「今後バンチの増刊として読んでみたい云々」で北条先生の過去の作品の第１話を収録したものを選び、バンチに送付しましょう（笑）ひょっとしたら（と、いうより必然的に）実現するかもしれませんよ。
　今週号の感想ですか、あ、時間がない！！それでは、また明日きます。

### [4214] あお	2001.10.31 WED 18:32:59　
	
祝･解禁！！
ああ、今週号読んでいて、あの冴羽氏の台詞に涙しそうになりました･･･。｢2人分の命が･･･｣ですね。冴羽氏はGHの中に香さんの命を見ているということですもの。2度と香さんを死なせたくないから、GHを守る、という思いを強く感じました。
んで、ここで悟った。絶対GHと恋人的なものにはならないと。どんなに時がたっても、冴羽氏にとって一番は香さん。これは絶対変わらないし、GHの中にどうしても香さんの命を見てしまうこともかわらないだろうな、と思う。

れす
＞Parrot様(4157)
レスありがとうございます！遅くなってごめんなさいです。
CHでは美樹さんと海坊主さんをセットで考えてました(笑)。だってあの2人って切り離して考えること、もうできませんもの。でもGHでは･･･最初違和感ありましたけど、もう大丈夫になってます。別物、という意識はそこまで私の中で強いんですねえ･･･(人事)。もし美樹さんが出てくるとしたら･･･想像しにくくなっちゃってますね･･･。でも、やっぱり香さんと冴羽氏の良き理解者であり、海坊主さんの恋女房(死語？？)であってほしいなあ。

＞ちゃこ様
お風邪ですか？？無理なさらず、きちんと治してください。この時期体調崩しやすいですものね。皆様もどうかお気をつけて。

### [4213] 無言のパンダ	2001.10.31 WED 15:12:47　
	
こんにちわ。え～！？ちゃこ様風邪ですか～？
大丈夫でしょうか？ちゃ～んと養生してくださいねー。(^_^;)
私は以前、年末に肺炎になりかけてから、年が明けてから近所の医院に行ったら「ガマンしないで救急病院があるんだから行かなきゃダメですよ！」と怒られ、それからすっごく風邪というものが怖くて「風邪ひいたら死ぬ」くらいの緊張感をもって過ごしてるんですが、今、子供が風邪ひいてるので、イヤな感じです。
期限切れのビタミンＣでも飲んで阻止するぞ！（＞ありあり様）
今は睡眠たっぷり摂って、眩暈もおさまっているので、バンチ買いに行こうかなー。今週から、また立ち読み派に戻ろうかと思ってたけど、立ち読みするには、体力と、速やかに行動することが必要なので今週は無理か！？
ちゃんと購入して、ゆっくり読もうかな？
とにかく今から行ってきます！

＞ＣＲＡＺＹ　ＣＡＴ様、睡眠についてのアドバイスありがとうございます！私はいつでも、どれだけでも眠れる性質のようなので、今度は、すっきり目覚めるためのアドバイスをお願いします！それさえマスターできれば無敵です(~_~;)

＞Ｐａｒｒｏｔ様（４２０２？）
ご心配してくださって、ありがとうございます(^_^)
Ｐａｒｒｏｔ様も、睡眠不足にはご用心を！

やはり、どんな薬よりも睡眠ですよ。
そして、「やさしい言葉」です。言葉って「言霊」っていうくらいで、やっぱり力があるんですね。やさしい言葉には、ちゃんと
体温が感じられますもんね。不思議です。

### [4212] ちゃこ	2001.10.31 WED 14:36:13　
	
こんにちは。ついに、風邪のウィルスに「撃沈！」されてしまいました。
今は、薬の影響で、ボ～～ッとしてます。にもかかわらず。
いつもの癖で（？）感想文を書いてしまった自分が恐い。これも一種の病気？！
感想文書き終わったら、頭が働かなくなってきたので、
レスはまた明日以降、書き込みにきます。では、解禁後、お会いしましょう。

### [4211] 琴梨	2001.10.31 WED 01:26:13　
	
こんばんは。琴梨です。

　私の今の気分のテーマはズバリ「Ｒｕｎｎｉｎｇ　ｔｏ　Ｈｏｒｉｚｏｎ」です。（スペルに自身なし。）当分眠れそうにない…

　ＡＨとは関係無いけど…
　＞みやむーファンの方いらっしゃるんでしょうか？
　今ちょうど、学園祭シーズンですよね。１１月４日（日）に兵庫県にある神戸大学の学園祭にみやむー登場するみたいですよ。詳しい事は大学のＨＰを観られた方がいいと思いますが、確かな情報だと思います。

　みやむーといえばやっぱり「エ＠ァ」ですけど、なんとなく野上唯香ちゃんをやってほしいなぁって思ったことがありました。そんなこと考えてるとますます豪華になってゆく希望声優リスト…

＞Ｋ．Ｈ様［４２０９］
私も「父、来襲す！」の扉絵大好きです。まさに”来襲”なんですよね。「キン＠コング」や「ゴ＠ラ」のワンシーンみたい。野上シスターズ（野上ファミリーかな）ものはＣＨでも好きなシリーズです。一度お母様＆双子の妹達を見てみたい！　

### [4210] しゃる。	2001.10.30 TUE 23:25:29　
	
バンチ読んだ人いるんでしょうね。うらやましい。早く明日にな～れ。

〓kinshi様[4200]〓【セリフ引用ネタ】
「とにかく　乗れよ　いくぜ！」
「いいかげんに　車を　もう少し大きくしろ！！」
「おまえの態度を小さくすれば　ちょうどいいんだ！」
私もこのセリフ好きです。声だしね。車の中で聴きまくってます。
この後、この二人はどこに行くつもりなんだろうと聴くたびにいつも思います。しかも待ち合わせしてるし。（「待たせたな」って言ってるし）
二人で行かなきゃいけないような仕事って･･･。敵は何人いるんだろう･･･。すごい組織なんだろうなと。
敵？もしかしてナンパ？まさかね、海ちゃんも一緒だしね。




### [4209] K.H	2001.10.30 TUE 23:23:24　
	
バンチ、しっかりもっこり読んじゃいました！！
明日の18：00過ぎが待ち遠しい・・・

＞まろ様[4203]
山梨でも火曜日にバンチは売っているんですが・・・。

＞わが分身Parrot様[4202]

　なるほど、そいつはチェック不足でした。というか、いつのまにかみやむ～は、声優ではなく女優になっていたんですねえ。
　林原さんはいつのまにか結婚しちゃうし、「リョウちゃんたらぜぇ～んぜんツイてないのお！！」状態ですね。

＞sweeper様[４２０８]

「だからあなたを連れてきたんじゃない」
「またやってくれましたね・・・」

雨の日は必ず槇ちゃんのことを思い出してしまうsweeper様、カゼはたいしたことないそうで、よかったですね。

昨日のカキコ「もっこり虫の歌」、最初、なんのこと？って思っていたんですが、思い出しました！！最後「もっこりの権化じゃ」ってセリフが入る歌ですよね。僕はあの話、「父、来襲す！」のトビラが最高に面白かったです。ゴ○ラか何かの怪獣映画のワンシーンみたいで・・・

「あ、あんたたち～！！なに火に油を注ぐようなこと言ってんのよ！！」
「き・・・き・・・き・・・き・・・」（あ・・・）（またサル・・・・）
「き～っさまぁ～！！、ワシの娘に手当たり次第その毒牙にかけていたのかぁ！！！」←ちょっと違う気が・・・
（あ～あ）（もう知～らない）

＞みなさま　　ところで・・・
　今週のバンチを読んで、僕は少々違和感を感じました。詳しくは解禁後にカキコしますが、もし僕と同じような違和感を感じたら、どなたかメッセージをください。

ではでは、解禁後にお会いしましょう！

### [4208] sweeper(レスです。)	2001.10.30 TUE 22:39:02　
	
こんばんは。バンチ、買いました。
感想は、解禁になってからという事で我慢です。(火曜日はこればっかり・・・。)
今回はレスです。
[レス。]
＞luna様。[４１９５]
「わぁお♪奇跡の美人客ご到来！！」
レス、ありがとうございます。「りょうちゃん大カンゲキー！」です。はい！！ぜひ、お願いします。(笑)
この時期、風邪というわけではないのですがいつも咳と鼻水に悩まされます・・・。この前、鼻水がひどくて耳鼻科に行ったところ、ちくのう症になる寸前だったらしいです。(･･ii)
貢物は、冒頭のこのセリフということでどうですか？

＞K.H様。[４２０１]
「(ソニア、海坊主に銃口を向ける。)ダディやリョウのようにみんなが悲しむのはもういやっ！！」
「だめだよ・・・キスをする時は、目を閉じるのが礼儀ってもんだ・・・」

レス、ありがとうございます。「りょうちゃん大カンゲキー！」です。私は、槙村兄といえば「悪魔に魂は売らない！悪魔はドブネズミにも劣る！」このセリフが一番はじめに出てきます。
それから、風邪の対処法、ありがとうございます。熱は出ていませんが・・・。ご心配ありがとうございます。
TMN、いい歌多いですね。あげていくとキリないです。
槙村兄。学生劇で女装した写真が笑えました。ただ、りょうのレオタードはびっくりしましたが。バックにバラの花というのもツボでした。

「(りょう、涙目になりながら。)痛いー！槇ちゃんもっと優しく――――！！」
「(槙村兄、りょうの傷を手当てしながら。)そんなにわめくなら、手に風穴なんぞあけるなよ・・・。」

＞Parrot様。[４２０２]
咳、今は何とか大丈夫です。
レス、ありがとうございます。「りょうちゃん大カンゲキ―！」です。懸賞当選記念ということで、「海ちゃん猫抱いちゃうー！」から「軍曹、身体検査しちゃうー！」に変わりましたね。
これは、限定なのですか？それから、造語センスのうまさに脱帽です！！
それから、パートナーの推薦ありがとうございます。ただ、推薦された３人の方から承諾をいただけたらの話ですが・・・。パートナーが見つかるまで、地下射撃場で腕を磨きます。(笑)
雨、どうしても槙村兄を思い出します。息を引き取る間際まで香のことを心配していた槙村兄。もしかしたら、今までりょうが香に今一歩踏み込めなかったのは、槙村兄のことがあったからかなと思いました。

それでは、失礼します。
皆さん、おやすみなさい♪

### [4207] ＣＲＡＺＹ ＣＡＴ	2001.10.30 TUE 18:05:49　
	
こんばんわ。ＣＲＡＺＹ ＣＡＴです。

＞九郎様【４２０４】･【４２０５】
　カキコ見てて思ったんですが、九郎さんって学生ですか？ぼくも、同じような５～９歳ごろにＣＨが、５:００～６:００の間くらいでやっていた記憶があるのですが。そのころ、ＣＥも前後してやっていた記憶があり、『ハードボイルド』のわからなかった頃なので、ぼくはＣＥ派でした。

今日バンチ読んだのですが、ＡＨの内容よく憶えてなくて。明日の解禁までにちゃんと読み返しておこう。

今日、部活で、横須賀の不入斗競技場に言ったのですが、部活が終わった帰り、京急の横須＠中央駅の前にあるマックの前にあった自転車のかごの中に、バンチが入ったまま置いてあったので、ちょっとだけ勝手に拝借して読んでしまった。もちろん戻しました。『やっぱり読んでる人がいるんだー』と嬉しくなってしまった。

ＣＲＡＺＹ ＣＡＴでした。　
　　　

### [4206] めぐめぐ	2001.10.30 TUE 17:34:30　
	
うわーい。うわーい。こんばんは。
　日曜日にＣ・Ｈのカレンダーが届きました!(^^)!
やっぱり、北条先生の絵はすてきです。おかげで、来年はより素敵＆楽しい一年になりそうです。
　もうすぐ、ＡＤＳＬのつなぎ放題の手続きが完了します。そうしたら、ゆっくりここの過去ログを読ませていただこうと思います。

　Ｃ・Ｈのアニメ再放送されないかなぁ。私は、Ｃ・Ｈ３が一番好きでした。キャラクターの感じが好みでした。ずーっと、ＶＴＲ録画をして集めていましたが、引越の時に・・・なんて事をしてしまったのでしょう・・。だから、再放送して欲しいなぁ。Ａ・Ｈも、アニメ化して欲しいなぁ。

### [4205] 九郎	2001.10.30 TUE 11:57:07　
	
間違えて途中？で送ってしまったので続きを書かせてもらいます（恥）。すみません！！（汗）たまにしか書き込めないんで。
【ＣＨアニメの事つづき】
あの頃、あの年だったからなのか性別的なのかはわかりませんが友達は皆キャッツの方が好きでよく学校帰り「みーつめるキャッツアイ♪」とか歌ってました。その中一人私は「えー！？なんでー！？ＣＨはー？？」とか言って断固ＣＨ派を通していましたが。私の世代ってびみょ-なんですよね。ＣＨの事語れる人って
クラスに１人いるかいないかぐらいで…。今でも。
あの年ではビデオ録画なんて知恵もなくて。そのうちＣＨアニメもやらなくなって。しばらく忘れてました。で多分６年生の頃再放送で「さらば…」を見て最初わからなかったけど、あー！！この回の話好きだった！って思って、本屋でＣＨコミックス見てあの話あるのかなって思って親に買ってもらいました。ココらへんかなと２８巻ぐらいを（正確に覚えてない）。で、無いなーと思いつつちょっとずつ上へ行き、ミック初登場の回の香の涙に号泣
（漫画読んで泣いたこと無かった）でＣＨにどっぷりはまっちゃいました（中１か２の時）。でもあの話はない！なんで！？また見たい！！と思っても再放送ではやらないし、レンタルビデオ屋
に行ってもタイトルも分からないし、ＣＨ何なのかすらも覚えていない（その頃まで２とか３とかある事すら知らなかった）。
わかる友達もいない。でもそれっぽいものもない。
そして何年かたって、ついにここのどなたかの書き込みで「さらば…」ってことが分かり、やった！と思ってレンタルビデオ探し
が、なかなかない！！ＣＨ、ＣＨ３、’９１などは結構置いてる
けど２がまったく。捜すこと１４件目でついに発見！！もう泣きそうでした。
というわけで今日１００円の日なので借りに行ってきます！！
かなり長くなってしまいました。すいません！
失礼しました。


### [4204] 九郎	2001.10.30 TUE 10:44:39　
	
失礼します。ここは本当に書き込み多くて１週間に１回書き込みじゃあなかなか話題に乗れなかったりでう～ん。とりあえず書きたいこと書かせてもらいます。
今日はとうとうバンチ発売！！ＧＨのあのコトバにリョウはどう
答えたのかなぁ。
【ＣＨ当たり日】
って本当にあるものですねー。私はついに「さらばハードボイルドシティ」を発見できました！＆シティーハンター’９１ＣＤなども！！’９１のジャケットは確かによいですねー！ピンクのシーツに包まった香がかわいい♪結構前にここで話題？になってたので見たかったんです。でも中古のわりに千円近くて買うか悩んでるんですが、内容はどんな感じの物なんでしょう？
【ＣＨアニメの事】
「さらばハードボイルドシティー」はアニメＣＨで一番印象強く残っていてずっと見たかったんです。ＣＨアニメは私が物心つく前から大好きで、多分５～９歳。パーフェクトガイドで調べるとですが（自分でははっきりとは覚えてない）。あの頃５時半から毎日やっていて見ていた記憶があります。
（あれは今思うと再放送だったんでしょうか？？わかる方教えてください！！）

### [4203] まろ	2001.10.30 TUE 10:30:04　
	
おはようございます。バンチ、今日買いました・・・でも、読むのは明日にします（山梨県をはじめ、水曜発売の地域にお住まいの方と同じ気持ちで読んでみたいので・・・）。ホントはレスとかしたいのですが、時間が無いので独り言オンリーです。

[無意味な独り言・・・その４]
　さてさて、以前、さえむらさま[４１７４]が槙村の存在を指摘して頂いたとき、自らの不勉強を恥じ、バンチのＨＰのみならず北条先生のこのＨＰも改めて目を通させていただきました。
　私にとって、衝撃（！）だったのが、ギャラリー内の『三級刑事』（サードデカ）を閲覧していたときでした。あの「よろしくメカドック」で一世を風靡した、次原　隆二先生に手伝ってもらったとの件を読み、思わず、なーるぅ！！（←合点がいった事を意味するらしい）って心の中で叫んじゃいました。北条作品に登場する過去の名車達と、（クルマを愛する）次原先生が一つに繋がった（？）ような気がしたからです。ひょっとして（気のせいだ、とは思いますが）クルマを描くとき（北条センセと次原センセのお二人が）
註）以下、私のトリップモードです。
　北「ここで、クルマ描きたいんだけど、どんなの（車種）がいいかなぁ」
　次「だったら、これなんかいいんじゃない？」
　北「？」
　次「ホンダＣＲーＸ。ボク（メカドックで）これ描いてたし」
　北「よし、それに決めた！（でも、これ”蜂の巣”になるんだけどなぁ・冷や汗）」

・・・みたいなやりとりがあったのかなぁ、との妄想が勝手に頭をよぎってしまう私・・・って、かなりヤバい（タコだきゃあ）かしらん？（←相当重症だ）

[无意义的自言自语···4]   
话说回来，以前纱村[4174]指出槙村的存在时，我为自己的不用功感到羞愧，不仅是Bunch的主页，还重新浏览了北条老师的主页。
对我来说，冲击(!)当时我正在阅览画廊里的《三级刑警》。读到《请多关照修车郎中》风靡一时的次原隆二老师帮忙的事，不由得，呜! !(←好像是明白了的意思)我在心里叫着。北条作品中登场的过去的各种名车，和(热爱汽车的)次原老师连为一体(?)因为有这样的感觉。也许(我想是心理作用)画车的时候(北条老师和次原老师两位)  
注)以下是我的触发模式。  
北:“在这里，我想画车，画什么样的(车型)比较好呢?”  
“那么，这个不是挺好的吗?”  
北“?”  
“本田CR - X。我(在机械船坞)画过这个。”  
北:“好，就决定了!(不过，这是‘马蜂窝’啊·冷汗)”  

有过……这样的对话吗?这样的妄想在我的脑海中闪过……是不是很可怕呢?(←相当严重)


### [4202] Ｐａｒｒｏｔ	2001.10.30 TUE 02:47:59　
	
こにゃにゃちわ～。１時間しか寝ていない為、けっこうキツイです。最近、友達の影響でラジコンに填ってしまい、出費の多い日々を過ごしています。誰か貧乏学生に愛の手を！

＞我が分身Ｋ．Ｈ様[４１５８]
レス、有り難うございます。「軍曹、身体検査しちゃうー！」（←僕の懸賞当選記念）です。林原めぐみさんについて、本当に良くご存じで・・・。すいません。僕には勉強不足の為、クリス意外分かりません。お見それしました。
ついでですが、映画「バ○ルロワイ○ル」にみやむ～がちょい役で出演してると聞いて、見たら出演してました。ご存じでしたか？

＞無言のパンダ様[４１６２]
懸賞当選のお祝いの言葉、有り難うございます。「軍曹、身体検査しちゃうー！」（←僕の懸賞当選記念）です。懸賞の方にはイラストが描けるスペースが無い為、描いていません。描きたいのは山々なんですけど・・・。聴コミ、お互い当たるといいですね。祈りましょう。
所で、お身体の方はもう大丈夫ですか？

＞Ｌａｐｕｔａ様[４１６４]
レス、有り難うございます。「軍曹、身体検査しちゃうー！」（←僕の懸賞当選記念）です。当たってましたか。良かったー。実は僕も「Ｌａｐｕｔａ」ファンです。アニメの「金○一少年の事○簿」のオープニングで流れていた「ｍｅｅｔ　ａｇａｉｎ」から知り、填りました。今では、ほとんどのアルバムを持っています。しかし、１日数時間聴いてないと中毒症状って・・・。
「Ｌａｐｕｔａ」好き同士、これから宜しく御願いします。

＞ｓｗｅｅｐｅｒ様[４１６９]
レス、有り難うございます。それと懸賞当選のお祝いの言葉、有り難うございます。「軍曹、身体検査しちゃうー！」（←僕の懸賞当選記念）です。パートナー募集中ですか？では、ちゃこ様か無言のパンダ様、又はＫ．Ｈ様なんていかがでしょう？３人の方には悪いかもしれませんが、独断と偏見で勝手に推薦させて頂きました。すいません。個人的にはどなたでも、かなりいいコンビになると思っています。Ｋ，Ｈ様とはセリフバトル？でいい感じですし、女性スイーパーコンビなんてのも、悪くないですよね？
僕もｓｗｅｅｐｅｒ様の影響で雨の日は、槙村の死を思い出します。香もそうですが・・・。辛いですね。
では、地下射撃訓練場で腕を磨いて下さい（笑）・・・とその前に咳止めを飲んでからですよ。咳、大丈夫ですか？

＞ＣＲＡＺＹ　ＣＡＴ様[４１８９]
なっ！なんとＣＲＡＺＹ　ＣＡＴ様の学校、ｈｉｄｅの母校だったんですね！僕はＸ－ＪＡＰＡＮ（Ｘ）兼ｈｉｄｅファンでありながら、知りませんでした。ファン失格だあ～。勉強になりました。
「ｈｉｄｅ」と「ｈｙｄｅ」、確かに間違えやすいですね。前者を英語読みすると「ハイド」と読みますし。どうでもいいですが、「ｈｙｄｅ」の「ｅｖｅｒｇｒｅｅｎ」、僕のお気に入りの曲です。

＞おっさん様
お久し振りです。随分、遅くなりましたが、もう風邪は治りましたか？書き込みから察するにもう大丈夫なようですが・・・。

う～、もう限界です。睡魔が・・・。では、また来ます。おやすみなさい。



### [4201] K.H	2001.10.30 TUE 01:47:21　
	
は～い、こちら山梨のK.H、みなさん聞こえますか～？

まずは、レス

＞sweeper様[4193]
「悪魔に魂は売らない、悪魔はドブネズミにも劣る！」
　↑
すんませ～ん、この槇ちゃんのセリフ、どうしても引用したかったんで、しちゃいました。

　かぜ気味のご様子ですが、大丈夫でしょうか？無言のパンダ様も、こういう季節の変わり目は体調崩しやすいので、お大事に。
　ちなみに、僕のカゼ対処法は、ビタミンC入りの飴を常になめ、睡眠を多く摂る、これに尽きますね。お陰で、今まで微熱程度のカゼで済みました。まあ、なんとかはカゼ引かないっていいますんで・・・。
　ちなみにsweeper様、TMNの「RAINBOW RAINBOW」はお持ちですか？かなり名曲が入っている初期のアルバムですよ！

　「キザな槇村のことだ、おおかたこんなことを言ったんだろう、悪魔に魂は売らない・・・とな。」
　
　「フン、ミック・エンジェルなら知っている。凄腕のスィーパーだ。今ではアメリカ№１だろう。リョウと闘ってどちらが勝つかオレにもわからん。・・・ま、オレには劣るがな！（←これが言いたかった）」

＞luna様（番号忘れちった！すみません）

どもども、ありがとうごぜえます、お気に入りに登録していただきまして。微妙に違うところもありますが、温かい目で見てやって下さい。

　「500万より500円・・・タダの仕事のほうがアニキらしい　　　　や！」

　あのリョウの、「にぎやかにしてやるよ」のセリフですが、やはりユニオン・テオーペへの復讐のセリフと考えてよいと思います。
　「香をたのむ」、これがリョウに対する槇ちゃんの遺言でした。それを果たさないまま、死ぬ、ということはリョウの気持ちにはなかったと思います。かなり義理堅い性格だし、ましてや親友からの預かり物。ユニオンへの復讐宣言と捉えたほうが自然だとボキは思います。

あ～、待ちに待ったバンチの発売日！！さてはて、あの二人はどうなるんでしょう！？ 

