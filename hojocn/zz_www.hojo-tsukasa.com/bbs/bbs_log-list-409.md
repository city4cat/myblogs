https://web.archive.org/web/20050313021048/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=409


### [8180] 葵	2002.10.08 TUE 20:20:48　
	
　いえいっ！勇者の意地を見せました。２回目のＸＹＺだぜいっ！いや～ホント、キレイなカードになってるんで驚きでした。前回行った時はコピー用紙みたいなので色気も無く、事務的な依頼書でしたから。そして我らがいもさまと初対面♪初めてでドキドキしちゃいましたよ。すぐに見つけていただきました。その目印は…ナイショよ☆（まろてぃメンバーならわかるはず…？）

＞千葉のマダムさま［８１７２］
　マダム～！お仕事で残念☆いもさまとデートしてきちゃいました♪今度はマダムも参加で…？！（一度メール下さいよ。ご連絡したいです♪）それと「ア〇の結婚」見てきました。出征して行方不明のギルを追い、戦地を駆け巡るアンというストーリー。（「アンの娘・リラ」が原案らしいです。）なんかサスペンスっぽかったです。でもＰＥＩの美しさには涙が…。先の話ですが、新婚旅行はＰＥＩだわっ！(^_^;

＞ＯＲＥＯさま［８１７５］
　ややこしい書き方でゴメンネ☆「〇ンの結婚」見てきました。ＰＥＩは素晴らしい！！くそぉ～っ！いつかカナダに行ってやるっ！

### [8179] 里奈	2002.10.08 TUE 19:59:24　
	
☆８１７７･８１７８　ＴＡＫＥＳＨＩ様＆マイコ様
確かに、ポメラニアンは小型犬ですが、うちのポメは小型じゃないですぅ（汗）『可愛い×２』って餌与え過ぎて、ポメとは思えないくらい中型犬になってしまいました…。昔は豆みたいに小さかったのに！

### [8178] TAKESHI	2002.10.08 TUE 19:42:10　
	
＞[8170]里奈様
１０代目ですか！じゃあかなり前から犬飼ってらしたんですね。ポメラニアンかー、いいですね。やっぱり室内で飼うには小型犬がいいと思います。確かになかなか無い名前ですよね。でもほんとにかわいらしい感じがしますよ。僕も犬欲しいです。

### [8177] マイコ	2002.10.08 TUE 18:47:10　
	
＞「８１７０」里奈様
私もぶぶちゃん、くぁわいい～！！と思いましたよ。
なんとなく抱きしめたくなるような名前ですねぇ！！
扉から悲しげに見つめてるんですか！？なんてかわいいんだ～！！

＞「８１７５」OREO様
私も軟式テニスです！公式が多いので軟式の人はいないと思っていました！壁打ち楽しいですね。ストレス解消になります！

### [8176] 里奈	2002.10.08 TUE 18:05:59　
	
はぁあ～い　みなさま♪昨日も夜更かしで朝５時まで引きこもってたジャスミンでぇーっす☆
みなさん風邪にお困りのようですねぇ～。里奈は全然元気なんだけど、友達も仕事仲間もみんな風邪で苦しんでます。ストーブやヒーターなどという物を使わない里奈にしてみれば、まだまだ春のような暖かさなんですが…。

今日は仕事休みだったので昼まで寝てたんですが、仕事の合間に家に寄ったアニキに起こされて発した言葉は…
『伝言板…見に行かな…』
最近Ｃ･Ｈに溺れている里奈を知るアニキは
『……？お前は香か？
　　頼むし寝ぼけてハンマー出さんといてくれよ（笑）』
と去って行きました。は…恥ずかしい…（滝汗；）

☆８１７５　ＯＲＥＯ様
『壁打ち』懐かしいぃ～（＾ヮ＾）♪
テニスを始めたばかりの時、壁打ちが一番難しかったです。だって正確に打たないと自分のとこに球が返ってこないんだもん！力の入れ加減も考えないといけないし。慣れるとひたすら自分相手に打ってましたね。カナダでは軟式存在しないんですか？軟式ってどっから来たのかしらん？里奈は中学で軟式、高校＆大学で硬式してました。軟式のほうが体力使いません？おもいっきり力入れないと飛ばないし。今はもう昔のようなサーブ打つ自信無い！

☆８１７１　いも様
『九死に一生スペシャル』ですか！？
確かに…。ひとつひとつの事故（？）を取り上げれば危ないなぁって感じで終わりますけど、今になって全てのことを思い出すと｢よく生きてるな…｣って怖くなりますぅ。好奇心旺盛なのはイイんだけど…。いつか産まれてくるかもしれない自分の子供も同じことするんだろうなって…（汗）

### [8175] OREO	2002.10.08 TUE 16:19:09　
	
日本時間－１６時間の地からこんばんわです。ちなみに日本時間－４で昼夜逆にするとカナダ時間です♪てなわけで只今夜中です・・・

＞８１６２千葉のＯＢＡ３様
台風すごかったんですね。たまに茨城もヒットするんですよね。でも我が家は上～の方なんで無事・・・だったかな？

＞８１６３マイコ様＆８１７０里奈様
今日学校で壁打ちして来ました！あ～スカッとした！私は軟式なんですけど、コッチには軟式存在しないんでみんな興味津々な目でみられましたヨ。

＞８１６５無言のパンダ様
“あちら”のカキコ読みました。返信しようか迷ったんですけど、そこにカキコするの初めてだし、どうしよ～ぉって悩んで、結局こちらに戻ってきてしまいました(-_-;)説明ありがとうございます☆も～私の早とちりですねぇ☆オーロラ撮れたら、も～みんなにメールの嵐でっす♪ぜ～ぇたい撮る！

＞８１６７たま様
まんまと陰謀にかかった人がココにひと～り・・・（笑）でも、私の超短絡的思考による早とちりっすね。状況勝手にこっちで作りあげちゃいましたから。

＞８１６８葵様
私もごめんなさい☆どんな状況だったかもわからないのに勝手にこっちで想像作っちゃって、カキコしちゃったもので。常にどこにいても頭の中時差ぼけ状態なので、思考回路が回ってないのです（笑）オーロラ撮ったらメールの嵐っすよ♪

＞８１７１いも様
全然気にしないでください☆状況良くわかんないで、言葉そのまんま取って、まったく誤解な想像を作り上げてしまった私もごめんなさいです★オーロラは絶対生で一度見て欲しいです！空いた口をふさげず、空見上げちゃいます（ケッコウバカ面？？）

＞８１７３sophia様
ホント２，３日こないとタイヘンな事になりますよね。なんで頑張って毎日来てます！最近ナントカ時間の余裕が出てきました。好きなＴＶや、最新の映画も見れてサイコーです☆
まだ帰って来て１ヶ月なのに、日本食がコイシーです（笑）インターネット、チェックするだけなら、図書館とか利用するといいですよ♪メンドイですが、節約、節約♪

それでは、もう夜の１２時過ぎちゃいました。今日はテニスしたから、疲れてて眠気いつもより倍増！おやすみなさい☆

### [8174] Ｂｌｕｅ　Ｃａｔ	2002.10.08 TUE 11:35:48　
	
　こんにちは～。

＞［8167］たまさま
　わたしは地元のブックス・オオトリというとこと、あと夢屋書店というとこでも見かけましたよ。こっちにはまだセ○ンがないんで、コンビニでどうなってるのかはわからないんですが・・・本屋さんとかで見てみてはいかがでしょうか？　大きさや厚さはＣ・ＨＳＰのときと同じくらいで、別に袋とかには入ってなくて普通に（青年）マンガ雑誌が売ってるとこにありました。

### [8173] sophia	2002.10.08 TUE 11:15:15　
	
こんにちわ。
2,3日来ないだけでＬＯＧ　ＬＩＳＴ2回も開かなきゃならないなんて・・・・やっぱり毎日のチェックは欠かせないのかしら・・・ふぅ・・・。
一万円をきったネット代（電話料金）も先月また超してしまい、今月ももう、そうなりそうな勢いなので自粛しようと思っていたのだが・・・

＞（８１５９）いも様
喘息、つらそうですね。
誰かに背中をさすってもらうと楽になる、と聞いた事があるのですが、いも様に「暖かな手（愛情）のぬくもり」で喘息の辛さを癒してくれる方がいらっしゃることを願いつつ、くれぐれもお大事に、としか言えない自分がはがゆいです。
次回のチャット楽しみにしてますよぉ～

＞（８１６８）葵様
強風でガラスにヒビが・・・・！！
怪我もなく、大事に至らなかったようで安心しました。
ホンと天災はいつ、どこにやってくるかわからないので怖いいですね＜－－||＞
今頃はいも様と新宿デートの真っ最中でしょうか？
いいなぁ～いいなぁ～
大きなオフ会とかあれば地方から参戦するのになぁ～、と思いつつ、誰か開催してくれ～～～と叫んでいます＜＾＾；＞
あと私事ですが「葵さま住所はカタカナでファイナルアンサー」ということで、よろしくお願いしますｍ＜＿ ＿＞ｍ

＞（８１　　）ＯＲＥＯ様
カナダの生活はいかがですか～？
オーロラいいいですね♪
でも、無理して体調くずさないようにしてくださいね～
勉強、適当にがんばってください！！
早く元気なＯＲＥＯ様と日本でお逢いしたいです＜＾＾＞

（８１６２）千葉のＯＢＡ３様のレス中、いちの様に「彼女」が！？・・・と思ったら違うんですね？
そーだろーなー・・・・あっ！いえ、違うんですよ、いちの様。いちの様は面白いし、男らしいし、雑学王だし、いないのが不思議なくらいですが、できたらできたで、ちょっと寂しいな、と思っただけで・・・・（滝汗：決死のフォロー）
ははははは・・・・・・（もう笑って逃げます）

最近、風邪や身体の不調のカキコをみますが、皆様くれぐれも注意してくださいね。
風邪の方に「ル○」1ダース贈っときます。お大事に。
それでは皆様よい週の中日を。

### [8172] 千葉のOBA3	2002.10.08 TUE 08:47:28　
	
おはようございます！昨日とはうって変わって今日は静かな朝です。

今週号で思ったこと、第二弾。
アシャンが夢ちゃんに信宏のコト「ＢＦですか？」って聞かれるところ。「恋人って事？」って聞き返してるじゃないですかー。すると、「違う、友達、大事な仲間」って答える。男の友達なら、ＢＦでいいじゃんと、妙な突っ込みを考えてしまったワタシ。うーーーん。しかしアシャンのキモチはナゾ。そういう所は思い切り幼いのかなー？ＡＨでは信宏が香ちゃんの様にアシャンの心がはっきりわからずに、ヤキモキしていくんでしょうね。しかし、恋人っていうと、りょうちゃんパパと香ちゃんママを思い浮かべるんだろうかアシャンは・・・？しかし、パパとママなら夫婦だしーと、わけのわからない迷宮に入って行く私なのでした。（チャン、チャン）・・・だから、何？ってカンジですね。ハイ。（大汗）

８１６５　無言のパンダさま　私は喘息ではなくて、アレルギー性鼻炎だけなのですが、体質は、子供３人受け継いでしまって、みんな何かしらのアレルギーをもっています。次男が喘息。小さい時はアトピーでかわいそうでした。でも今はホントに元気になってくれて良かったです。私達、母親から息子達はアレルギーを受け継ぎ、アシャンは香ちゃんからハンマーを受け継いだのね！（なんのこっちゃ？？？）

８１６８　葵さま　８１７１　いもさま　ホントに新宿デート！？ホント！？

どーんよりとした曇り空のように私の心は暗いぜ・・・。って何！？

### [8171] いも	2002.10.08 TUE 01:13:47　
	
こんばんわ～。
早とちりの激しい・いもです。（笑）←笑い事ではないですね・汗
たまさまはじめ私の勘違いで迷惑を被った方々、ほんとにごめんなさい！！この場を借りてお詫びいたします。
笑って許して～。なんつって。・・・反省してます。（汗）

＞OREOさま[8161]
ピュアなOREOさま、私のカキコで勘違いを誘ってしまってごめんなさい。反省してるので、お許しを～☆
オーロラいいですね。私も死ぬまでに一度は見てみたいなあ。
鳥肌立つ・・・さぞかしナマで見たらすごいんでしょうね。

＞千葉のOBA3さま[8162]
千葉のOBA3さまが張り込みできないのに、私だけ張り込むなんて出来ないですよ。(^_^;)
だってマダムがリーダーだから。（何の！？）

＞無言のパンダさま[8165]
喘息の方は大分よくなりました。私はこの時期限定で、かかりつけの病院とか常備薬とかないので、一回でるとつらいです。(^_^;)
たまさまの一件（←なんかたまさまが悪いみたい・汗）私の大・大・大勘違いで迷惑をおかけしてほんとーにすいませんでした！！

＞葵さま[8168]
ご迷惑をおかけしてすいませんでした～。 (>_<);;;
新宿デートは・・・恥ずいっす～。ココでも何回かオフ会（？）の話でてますよね。でもちょっと見てみたいぞ、葵さま。（笑）
とりあえずあとでメールいたしますね。(^_^;)

＞里奈さま[8170]
無言のパンダさまへのレスを読んでてビックリしましたよ～。
なんですか！すごいですね、里奈さま兄妹は～！！！
このエピソードだけで二時間くらい番組つくれそうですよ。九死に一生スペシャルみたいの。それくらいすごいっす！！

毎度長々すいません。反省中のいもでした。

### [8170] 里奈	2002.10.07 MON 22:49:50　
	
☆８１６１　ＯＲＥＯ様
ＯＲＥＯ様もテニス部員でしたか！！
そかもしっかりカナダにラケット持って行ってるんですね♪使ってますか？運動はあんまり好きじゃないけどテニスならいつでもやりたい！でも一人じゃできん！あぁぁーテニスしたぁ～い…
私もオーロラというものを生で見てみたいです☆すっごい感動的なんだろうなぁ～。日本にいる限り絶対見れないし…。誰か私をカナダへ連れてって！（旅費も持って！）

☆８１６６　ＴＡＫＥＳＨＩ様
またまた心配して頂いてありがとうございます。
毎日、夜更かしが楽しみで仕事してるって感じです。なんか里奈ちん暗い奴みたい…？いやいや、里奈は２４時間元気っす♪
愛犬『ぶぶちゃん』、可愛い名前だと言ってくれたのはＴＡＫＥＳＨＩ様が初めてですぅ～（感涙）こんな名前はなかなかいないでしょ？うちは里奈が産まれる前からポメラニアンを飼ってきてて、今のポメはもう１０代目近いと思います。買って来て数日で死なれたことが何度かあったんで…。ちなみに皆、名前は『ぶぶ』でした！呼びやすいんで気に入ってます。怒る時とか呼ぶ時、『ぶぅぅぅーー！！』って叫ぶと気持ちイイです（笑）

☆８１６５　無言のパンダ様
ナイフが刺さった林檎を姉に投げたっ！？しかも頭に刺さったですとぉーー！！？そっちのほうが恐ろしいです！なんて姉妹なのかしら…（冷汗）
昔はしょっちゅうアニキと喧嘩してましたが、アニキが大学で北海道に行ってからは超仲良しです♪一緒にカラオケとか行きますよ♪私達兄妹は喧嘩もさることながら、幼い頃からバカなことしていつも親を困らせてました。まだ小学校にも上がってない頃…当時住んでたマンションの一階が喫茶店で、店の名前が書いてある屋根（ビニール）でトランポリンをしようと二人して２階から飛び降りました。当然ビニール突き破って落下…。たぶん自分はジャッキーだと勘違いしてたんでしょうね。（マジ）それから布団をクリーニングに出して返ってきた時のファスナー付きビニールにアニキを閉じ込めて遊びに行って窒息死しかけてたとか…。あと真冬に池に飛び込んで鯉と戯れて二人とも肺炎になりかけたとか…。乗れもしないのにチャリンコに二人乗りして、猛スピードで坂道を下って車輪に足を絡ませカカト落としたとか…。（これが一番痛かった！）アニキが燃え上がる炎の中にダイブしたのもこの頃です（笑）この平和な現代社会の中で、私達兄妹は飛びぬけてサバイバルしてましたね！

☆８１６７　たま様
あら、里奈ちん勘違いだったのね。
たまたま見つけたＣ･Ｈ小説のＨＰの主らしきお方が『結婚』って書いてたんでてっきり…。でもそのうち作って頂けるのかしら？

### [8169] マイコ	2002.10.07 MON 22:09:41　
	
＞「８１６７」たま様
たぶんそうですね・・・。私が一番若い？と思います。
テストがんばります・・・。数学がさっぱりわかりません。
やる気でないな～。

### [8168] 葵	2002.10.07 MON 22:01:45　
	
　すごい風でしたね。我が家のリビングの扉がすごい勢いで閉まり、そこにはまってた大きな一枚ガラスにヒビが入りました。一番近いのが私の席だったので、一歩間違ってたら大ケガするところでした。こわかったよぉ～っ！(>_<;

＞私のカキコ［８１５５］について
　ごめんなさい！私の書き方が悪かったです☆（無言のパンダさま［８１６５］、フォローありがとうございました☆）たまさまの新郎新婦参加のチャットというのは、たまさまが入力ミスかなんかで「たま」・「たま」とお二人の「たまさま」が出現されたからなんですよ。なので決してたまさまの「新郎ご本人さま」がお出でになったわけではございません。みなさまに多大なる勘違いをさせてしまった私の書き方が悪かったです。お詫び申しあげます。ごめんちゃい☆　m（_　_）m

＞無言のパンダさま［８１５８］
　催眠術…あぁ～あれっ！あったあった♪子パンダちゃんがマジで心配してたあのシーンね♪そっかぁ…のとき信宏が…？！

＞いもさま［８１５９］
　上記のとおりです。ゴメンネ☆で、新宿デート、マジなんですか？！いやはや、いもさまのご都合がよろしければ私はかまいませんが？(^_^;

＞ＯＲＥＯさま［８１６１］
　上記のとおりです。ゴメンネ☆オーロラ、お願いします♪（←遠慮の無いヤツ☆）

### [8167] たま	2002.10.07 MON 21:55:10　
	
みなさまこんばんわ。
ＣＨのアニメで好きなシーン私もあります。
「夢見るように恋したい。１２歳天使の作戦」の最初の３分！
リョウが足の不自由なこずえちゃんの部屋のベランダに来るシーンです。
スローモーションで空から降りてくるリョウは最高に素敵です。

＞いちの様 【8136】「愛はデータで図れない」（カッチョイイーーっ）
３９秒に１組結婚し、１分５０秒に１組が離婚・・・。
小室さんがかなり数字を稼いでいるような。はははっ

＞マイコ様【8139】 びっくり
私もビックリです。若いとは思ってましたけど、中学生とはっ！
もしかして、ＢＢＳの中で１番若い？のでは？（ＰＳテストがんばれ）

＞里奈さま　【8140】バーバラ婦人・・・思わず笑ってしまった。
そうですね。連載とかって書いたらプレッシャーになりますね。（ｺﾞﾒﾝﾈ）
それから、１０００％勘違いですよ。あたくし、ＨＰはまだ作っておりませんの。ホホホーーっ。
そして【8160】の歌『アスファ～ルトォ～・・・アタマを切りつけぇ～』
自分の過去をもギャグにしてしまう(?)その精神！さすがジャスミンだわ♪

＞葵さま　【8155】
こらぁ～！新郎新婦そろって参加なんて書くもんだから、どえらい勘違いをしてる人が約２名いるじゃないかあぁぁーーっ。
いもちゅぁ～ん！ＯＲＥＯちゅぁ～ん！これは葵ちゅぁ～んの陰謀よっ！＞無言のパンダちゅぁ～ん　【8165】
ご説明ありがとう。でも、↓
『そんなそんな面白いことがあったら私も・・・笑うヨ！』こりゃ無いよ。

＞Ｂｌｕｅ　Ｃａｔ様　【8164】
１０月５日ライジン売ってあったのですか？
私は過去２回、セブ○イレブ○で買ったのですが、今回見当たらないんです。ライジンが・・・。いったい何処に売ってあるのでしょうか

・・・。

### [8166] TAKESHI	2002.10.07 MON 18:04:47　
	
昨夜はすごい雨と風でなかなか寝付けませんでした。

＞[8160]里奈様
Ⅹですか。僕はⅦの雰囲気が一番好きです。夜更かしはかなり体力いるとおもうんですが大丈夫ですか？お母さんも里奈さんも犬のぶぶちゃん（すごくかわいい名前ですね！）も体調くずさないよう気をつけてくださいね！

＞[8161]OREO様
二年前に見られたんですか！すごく感動したことと思います。僕も一度でいいから見てみたい！！！
また見れるといいですね！！

### [8165] 無言のパンダ	2002.10.07 MON 14:47:30　
	
こんにちわ☆

こちらは昨日の雨も止んでとっても涼しいですー(^_^)
＞いもさま、ＯＲＥＯさま！たまさまがチャットに「新郎新婦」で参加したというのはちょっとした誤解です(^_^;)葵さまからご説明があると思いますが、私も「あちら」に事情を書いておきました♪そんな面白いことがあったら私も・・・笑うヨ！

＞いもさま（８１５９）
いもさまも喘息持ちだったんですか？ぜんそくは苦しいので本当につらいですね。ほんとに横になることも出来ないし・・・
私は点滴を受ける時もベットに座ってしてもらってます。
この季節はどうしてもしんどいですが、私は冬もかなりツライです。マスクが手放せません(^_^;)風邪をひくと最悪なので、いもさまも風邪には要注意です！水分も大切ですよ。
ＡＨ５巻の表紙は３８号のカラー、いいですね♪あと今週号の表紙も候補かな？

＞里奈さま（８１６０）
こわいよ～こわいよ～兄妹喧嘩も壮絶だよ～里奈ちん！
でも私の友人のバットの件は喧嘩じゃなくて事故なんですよ(^_^;)今も頭へこんでますけど・・・
そういえば高校時代のクラスメートで、姉とけんかして林檎に果物ナイフが刺さった状態にあったものが目の前にあったので姉に向かって投げたらパカッと林檎が割れてナイフが姉の頭に刺さった～と笑っていたっけ・・・実は私も弟に中華のレンゲを投げつけて流血騒ぎになったことが・・・！あー怖いよ～喧嘩しちゃだめだよ～。きょうだいは仲良くネ！(>_<)

＞ＯＲＥＯさま（８１６１）
オーロラはテレビでしか見たことないけど、実際に見ることができたなんてすごい貴重な体験ですね！デジカメにおさめることが出来たらぜひ私にもその感動を分けていただきたいです(^○^)

＞千葉のＯＢＡ３さま（８１６２）
息子さんが喘息持ちだったんですか？・・・うちもです！
うちもおかげさまで現在はほぼ完治してるんですが、それまでがホント大変でした。私は自分からの遺伝だと思うと自責の念に苛まれてすごくつらかったです。子供の苦しんでいる姿を見るのは一番つらいですもんね。

ではまた。(^_^)/~

### [8164] Ｂｌｕｅ　Ｃａｔ	2002.10.07 MON 10:24:33　
	
　こんにちは～。
　そういえばライジンて、毎回１話づつ連載してくカタチなら、もしかして単行本に収録されてない扉絵とかも連載どおりに掲載してくれるのかなぁ？　でも月刊だと、終わるのにいったい何年かかるんだ？

＞［8153］ｃｈｉｋｋａさま
　小室さんとＫＥＩＫＯの結婚、驚きましたよねぇ～。
　実はわたしの今の名字はＡＳＡＭＩのと同じで、旧姓はＫＥＩＫＯのと同じだったりするので、よけいにびっくり！だったりします。ごくありきたりで平凡な名字だけど、なんかちょっと嬉しいかも。

### [8163] マイコ	2002.10.07 MON 10:10:20　
	
おはようございます！このまえの大会で今日はおやすみです♪家では一人だからつまらないなー・・・。兄は体調をくずしていながらもがんばって学校に行っています。

＞「８１６１」OREO様
へぇー・・・、結構皆さんテニス部入ってたんだ～！OREO様は、カナダにもラケットもっていっちゃってるんですか！？す・すごい！
★オーロラか～、一度見てみたいっ！！

### [8162] 千葉のOBA3	2002.10.07 MON 08:37:49　
	
おはようございます！こちらは、早朝から、嵐です。このあいだの台風よりすごいかも・・・。週末いそがしくて、遅ればせながら感想を・・・。

＞☆なんか信宏は、自分の感じてきた「痛み」の分だけ人に優しくするには、まだまだ修行が足りないようで・・・。自分の想いだけで突っ走っちゃう・・・若いのねー。アシャンが夢ちゃんを慰めるシーンは、きっと香ちゃんの想いがはいっているんだろうな・・・と思うけど、夢ちゃんにああいう風に接することが出来る事で（後で余計につらいことになるかもしれないけど、）アシャンもまた、癒されてるんだろうなーと思いました。ラストの香ちゃんは、アシャンと夢ちゃんの二人を抱いているんでしょうんね・・・。なんか、海ちゃんとお酒を飲んでいる、りょうちゃんの辛そうな姿を見て、香ちゃん、りょうちゃんも抱いてあげて下さいと思ってしまいましたよ。

８１２７　無言のパンダさま　「堀ちえみ」さん懐かしいです！今じゃ、４人の男の子？５人だっけ？のお母さん。なんか、負けた！って思ったもん。（うちは３人さ・・・。）

８１５５　葵さま　そう、たまさまへは、最高のプレゼントを考えさせていただきました！で、新宿には明日行かれるんですか？えーん、明日私仕事で行けないよーーー！いもさまーーー！代わりに行ってーーー！もしカードが新しくされてたら教えてくださいね。私ももう一度行っちゃいまっせ！

８１５０　将人さま　私もパト＠イバー見たい！！大好きでした！アニメが特に。あの、とぼけた会話が最高でしたし、ドラマとしてもホントおもしろかったですよねー！ロボット物って苦手なほうなんですが、パト＠イバーだけは、夢中になって見てましたよ。

＞いもさまも無言のパンダさまも、ぜんそくなんですか？うちは次男がぜんそくです。でも､体が大きくなったのと、野球で鍛えだしたのがよかったのか、最近は夜中に病院に担ぎ込んだりすることはなくなりましたが・・・。季節の変わり目は一番いやですよね・・・。お大事にしてくださいね。

８１６１　ＯＲＥＯさま　ちわーっす！今何時ー？オーロラなんて、なーんてすごいんだ！！おばさんも見たいっす！このあいだの台風ねー、茨城大変だったよ、高圧電線の鉄塔が倒れたりして、でも、潮来のほうだけど・・・。

＞いちのさま　うん、そうだ。「彼女ができました。」でもじゅーぶんビックリしてあげる。ヘッヘッヘ。

それではこれにて・・・。失礼しますです。


### [8161] OREO	2002.10.07 MON 07:48:47　
	
こんにちわです☆今全部カキコ読み終わって、＞８１５３chikka様の小室哲哉結婚の話にすごいビックリ！！日本の芸能ニュースサイトに今からチェックだ～！！

＞８１３８ＴＡＫＥＳＨＩ様
こんにちは！本物のオーロラは寒気するほどすごいものですよ。２年前のココに来たての頃、一度だけ見れたんです！！

＞８１３９マイコ様
こんにちは！実は私も中学、高校とテニス部でした！！大会とかすごい懐かしい！！カナダにもラケット持ってきちゃってます☆

＞８１４５葵様
こんにちは！ホント、たま様の入籍にはビックリでした☆チャットでたま様のダンナ様とも話したんですか？？どんな人なのかなぁってちょっと興味津々デス。あっ、もちろんオーロラ見れたらデジカメに収めてみんなにメールしまくりますよ♪でも、本物を自分の目で見た感動を伝えられるかどうか・・・本物は鳥肌立つくらいスゴイです☆

＞８１５０将人様
こんにちは！転校生編かなりウケました！！最後にはも～みんな大騒ぎになってて、授業どこじゃないってかんじですよね♪オーロラは実は２年前にここに来たての頃に一度見たことがあるんです。かなり鳥肌もんですよ☆でも今度はデジカメに収めるぞ！！

それでは、今から芸能ニュースチェックしにいってきます☆
