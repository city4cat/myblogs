https://web.archive.org/web/20011112021315/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=159

### [3180] 踊るたい焼き♪ 	 2001.09.25 TUE 01:31:21　
	
どうも。二回目のカキコです。
掲示板に書き込まれたカキコを読んでいくと
皆さんの凄さが、まじまじとわかります・・・・・。
僕には入り込もうとしても入り込めない世界です・・・・・。
しかし、頑張って勉強していこうと思います。
皆様、マジでどうぞヨロシクッ！

### [3179] あお 	 2001.09.25 TUE 00:36:49　
	
さてさて、いよいよ今日が発売日！！楽しみですね。朝一でコンビニにダッシュする予定(笑)。

＞Blue Cat様(3164)
スケバン刑事＞
確かアニメビデオは3作出ていた気がします。渋谷だか新宿だか忘れましたが、TSUTAYAにおいてありました。といっても私が見たのは２作分なんですけどね(笑)。

＞CRAZY CAT様(3167)
おお、見付かりましたか！！おめでとうです。私はあのイラスト集、表紙の冴羽氏にほれ込んでおります。
お誕生日おめでとうございます！！しかし･･･お若い(笑)！！創か～私にもそんな頃があったんだよなあ(遠い目)。何はともあれ、学校生活に自分探し(？)、がんばってくださいませ。

＞ちゃこ様(3177)
妹さんですか(笑)？私お兄ちゃんかお姉ちゃんが欲しかったですねえ。って、話がずれている･･･。基本的に渋好みな私。小学生から大学時代、そして現在まで、流行の話にはついていけない･･･。ま、いいか、と達観している今日この頃です。とりあえず私はそろそろ論文書きに本腰を入れないと･･･。留年だけはしたくない(切実)。

最近BBSに入るのにえらく時間がかかってます･･･。重いのかしら･･･？？

### [3178] あお 	 2001.09.25 TUE 00:32:42　
	
さてさて、いよいよ今日が発売日！！楽しみですね。朝一でコンビニにダッシュする予定(笑)。

＞Blue Cat様(3164)
スケバン刑事＞
確かアニメビデオは3作出ていた気がします。渋谷だか新宿だか忘れましたが、TSUTAYAにおいてありました。といっても私が見たのは２作分なんですけどね(笑)。

＞CRAZY CAT様(3167)
おお、見付かりましたか！！おめでとうです。私はあのイラスト集、表紙の冴羽氏にほれ込んでおります。
お誕生日おめでとうございます！！しかし･･･お若い(笑)！！創か～私にもそんな頃があったんだよなあ(遠い目)。何はともあれ、学校生活に自分探し(？)、がんばってくださいませ。

＞ちゃこ様(3177)
妹さんですか(笑)？私お兄ちゃんかお姉ちゃんが欲しかったですねえ。って、話がずれている･･･。基本的に渋好みな私。小学生から大学時代、そして現在まで、流行の話にはついていけない･･･。ま、いいか、と達観している今日この頃です。とりあえず私はそろそろ論文書きに本腰を入れないと･･･。留年だけはしたくない(切実)。

### [3177] ちゃこ 	 2001.09.25 TUE 00:17:57　
	
今日は、マメに登場してスミマセン。再びレスです。

「あぁ勘違いの嵐」
＞あお様「3145」
こんにちは。テロの話も相変わらず感心しながら、読ませてもらってましたよ。
で「いのまたむつみさん」の話。「ク＠ーミーマ＠」やっぱり違ってましたか。
書くの迷ったんだけど、書いちゃたのです。恥かきは慣れっこさ（笑）
そうそう「パ＠レ＠バー」私も好きです。これは珍しく漫画もアニメも好き。
もう映画、やらないんですかね？
（アニメ観てない。と言ってる割に、結構観てる？！でも、嗜好は偏ってます）
それにしても、あお様の書き込み見ていると、歳の離れた妹を思い出します。
何て言うか・・・あお様の嗜好、妹に似ています。（音楽ネタとか）
妹がインターネットしてないの知ってるからそんなこと思わないけど、
そうじゃなかったら、電話して確認しちゃう所です（笑）

＞無言のパンダ様「3168」
永井一郎さんの話も反応して下さって有り難うございました（＾＾）
今回のは、ちょっと驚いてます。ずっと信じて疑ってなかったので。
どこで思いこんでしまったのでしょうねぇ、全く恥ずかしい（＾＾；；）　
洋画の吹き替えでは、意外に色っぽい女の人やってるとか言う話をＴＶで
見た気がして、そこから思いこんじゃったかな。
（でもこの話は、現在女優でもご活躍の「戸田恵子さん」の話な気もする）
周囲によく「ちゃこの思い込みは何処から来るの？」と、言われてます。
（ＨＮは、学生時代の呼び名そのまま）
これからも青くなる書き込みすると思いますが、突っ込んでやって下さい。

ついでに、勘違いネタをもう１つ。（自分からネタ振るなよ）
美樹さんの声、ずっと「泪」をしていた「藤田淑子さん」だと思ってました。
「小山芙美さん」の声、似てません？？似てないですね・・・（＾＾；；）
これは最初の頃、ＯＰとＥＤ、早送りして観てたのが原因。
本当、こんなヤツですが、今後とも宜しくお願いします。

＞将人様「3136」
ＣＥの最初のラスト後、私もそんな風に想像してました。
でも、ずっと瞳は俊夫のことを「騙してる」と悩んでいたわけだから・・・
やっぱり「記憶喪失」になるしか無かったような気がします。
ただ、これは「今だから」ですよね。ＡＨもENDマークが付いて、そして。
ちゃんと納得出来る日が来ればいいけど・・・一体、何時になるんだろう？

それにしても、あの頃のヒーロー役、軒並み神谷さんだったのですね。
ギャグの話、ぜひここでも披露して下さい。皆、ＯＫだと思うけどな。
そうそう。スーパーロボット物「ボルテス＠」なんてのもありましたね。

では、失礼します。

### [3176] Ｒｅｉｎｅ　ｄｅ　Ｓｈｉｎｊｕｋｕ 	 2001.09.24 MON 23:55:50　
	
オヒサでございます。２度目の登場です。
レーヌ・ド・シンジュク（フランス語で“新宿の女王”）でございます。
初めての方々、初めましてです。

ここんとこ、ロクにログをロムる暇もなかったので、大ざっぱにしか読んでません。しくしく
が、ちょっと余裕がある今日はちょっとだけレスです

【まんだらけ】
どなたかが古本屋めぐりをなさっていると言ってましたね？
なら「まんだらけ」がオススメでございますよ
関東には新宿店、渋谷店、中野店があります
どのお店も古本だけでなく古いオモチャもあるので、私が子供の頃持っていた物もあって懐かしかったりします
ただ、新宿店だけはコミックスは扱っていないみたいです（汗）
関西のほうにもチェーン店あるみたいですよ
それどころか、なんとアメリカや中国やイタリアにもあるとか！

http://www.mandarake.co.jp/ ←まんだらけの公式サイトです

＞ＣＲＡＺＹ　ＣＡＴ様［３１６７］
初めまして。お誕生日おめでとうございます。
私も皆さんと同じように女性の方かと思っておりました。
なんか女性っぽいＨＮだったもので(^_^；

【声優】
北条ネタに関係ないのにこんなこと書いていいのかなと思いましたが、「サザエさん」の波平役の声優さんとフネ役の声優さんって役柄だけでなくプライべート面でも本当のご夫婦ですよね

### [3175] ＣＲＡＺＹ ＣＡＴ 	 2001.09.24 MON 23:14:04　
	
こんばんわ。ＣＲＡＺＹ ＣＡＴです。

＞昔の女子高生？様【３１７３】
　ＣＨを好きになった経緯ですか･･･。ぼくは、まずＣＥのアニメです。再放送を見ていたんだと思いますが、内容をよく覚えていません。ただ、アクションがすごく印象に残ってました。それと同じか、ちょっと後くらいにＣＨのアニメを見てました。やはり内容はよく覚えていません。「Ｇｅｔ　Ｗｉｌｄ」との組み合わせで覚えてました。その後忘れかけていましたが、つい最近従兄の持っていたＣＥのコミックスをよんでカンドー。北条先生の世界にはまってしまいました。そしてＦＣを読んだ後、ＡＨの連載を知り、連載前の宣伝がＣＨの続編とのことだったので、やはり従兄の持っていたＣＨを読みました。アニメを見ていたのが小さいころだったので、つまらないというイメージがあって読んでなかったんです。
今では、ＣＥ、ＣＨ、こもれ陽･･･、ＲＵＳＨ、ＦＣ、ノベルス、イラスト集、など集めちゃいました。
ぼくがＣＨを好きになった経緯はこんなところです。

このまえ、ＣＥを読み直したら少し泣いちゃいました。

それでは、ＣＲＡＺＹ ＣＡＴでした。


### [3174] ちゃこ 	 2001.09.24 MON 22:58:44　
	
再び登場です。何だかんだで明日はバンチの発売日。
「冷静になるんだ」と思ってたんだけど、どうも駄目だったみたい・・・(^^;;)
16話のリョウの顔と、17話のリョウの顔。香の「ばんっ！」と言った顔。
事ある毎に思い出していました。さて、今回はレスです。（ちょっと遅め？）

「作品紹介のこと」
「続編」と紹介していたのは、某全国展開のレンタル＆販売チェーン店でした。
だからちょっと気になっています。それとも、たまたま私の住む町だけの話？
無言のパンダ様も書いておられますが（＞3138）ＡＨは純粋に「新作」と
扱って欲しい。で、ＣＨは「ＡＨにもちょっと登場しているリョウの話」
とかなんとか・・・無理があるか、やっぱり。
でも、売れてるんでしょうねぇ・・・ＣＨ
（↑私も古本込みで売上げ貢献してる）とっても複雑な気分であります。

＞Ｂｌｕｅ　Ｃａｔ様
「丹沢学」さんの観ました。あれ、前後編なのに作画監督違いますよね？
出来れば、前編も丹沢さんにして欲しかったと思うのは、私だけ？？

＞よしき様「3100」
「ＧＨ」本当、もっと表情が出てくると良いですよね。
仕事柄、たくさんの人の顔を見ますが「魅力的な人＝正当派美人顔」では
ありません。やっぱ、表情が豊かな人が「いい女」に感じます。
（怖ろしいことに、年齢を重ねれば重ねるほど顕著に現れる・・・！！）
ＧＨが表情豊かになった時、無言のパンダ様も書いてるように（＞3138）
ＡＨの世界が確立される気がします。

＞もっぴー様「3143」
ＡＨがインターネット全盛時代の作品で本当に良かったと思います。
じゃなかったら、気持ちのやり場に困ってましたよ。
アニメ化の最大の泣き所は、ＣＨを「もっこりとハンマーしかない漫画」
というイメージにしてしまったことだと思う、今日この頃・・・
ある種「スケバン刑事」と同じ悲劇がここにある？！（＞3164　BlueCat様）

それでは、また来ます～。

### [3173] 昔の女子高生？ 	 2001.09.24 MON 22:13:57　
	
お久しぶりです。皆さん、私のこと覚えてますか？ＨＰばっかり見てて、勉強を全然してなかったので、親に怒られました(笑）だから、２週間ぐらい見るのを控えていたら、
書き込みのたまりように、ビックリしました。これからは、ちゃんと勉強して、書き込みがたまらない程度に、見ることにします
す。
この間、ゲーセンに行ったら、北斗の拳のゲームがあって神谷さんのナレーションに思わずうっとりしてしまいました。すごくカッコよかったです。それと同時に、ＣＨのりょうのことを思い出して、やっぱり神谷さんの声はかっこいいなと思いました。声だけで人の心を動かし、感動を与えることが出来るなんて、声優とゆう職業は素晴らしいと思いました。私も将来は人に感動を与えたり、人の役に立てるような職業につきたいです。
＞ＣＲＡＺＹＣＡＴ様（３１４８）
はじめまして。遅れましたが、お誕生日おめでとうございます。１５歳になったんですよね？私は今１６歳で、来月１７歳だから、年が近いですね？年が近い人がいてうれしいです。
ここで、１０代の皆さんに質問なんですが、どうゆうきっかけでＣＨのファンになったか教えてくれませんか？おそらく１０代の方はリアルタイムでＣＨを見てないと思うんですよ。どこで知って、好きになったetcちょっと興味があるもので、教えてください。ちなみに私は小学生の頃にＣＨの再放送があっていて、それを見てあまりのかっこよさに好きになりました。一度は忘れかけていたけど、中学生になってまた復活して、今じゃ大ファンです。皆さんのレス(使い方あってるのかな?)待ってます。
それでは今日はこのへんで引っ込みます。

### [3172] おっさん(岐阜県：手が痛て～） 	 2001.09.24 MON 21:43:20　
	
度々、失礼します。葉書を１０月の初めまでに１００枚は書かなくてはならないもので・・・。
けいちゃ～ん（３１７１）、たま～に私もア○街ック天国見てるんすけど、いつもコーナーで街の物語をやってるんすよ。その街をスポットに当てて。それが結構面白かったりする・・・。
伊倉さんがその街に住んでるかは分かりませんが・・・。
また、葉書を書かなくてはいけないんでおっさんは引っ込みます。明日はバンチ発売日ダ～！！明日は仕事ダ～！！あ～あ。

### [3171] けいちゃん 	 2001.09.24 MON 21:06:42　
	
[3170]おっさんさま
情報さんきゅうです。伊倉一恵さん、南０住に住んでるのかしらー？それにしても、物語にするほどなのかしらー？振興住宅と下町が入り交じったとこですよねー？でも、便利でいいとこだ。

### [3170] おっさん（岐阜県：寝過ぎた） 	 2001.09.24 MON 20:44:38　
	
毎度、今日は休みだったんすが寝過ぎて頭がいたいおっさんです。皆様、いつものご挨拶せ～のおっは～！！
ＣＲＡＺＹ　ＣＡＴく～ん（３１６７）、思いっきり女の子かと思いました。男の子だったんすね。すんません＾～＾；。やっぱいいね～１５歳って。１１年前は部活を引退し、あほなことをしてました。今でも部活の顧問から年賀状きますからね。こんな話しちゃってすんません。
けいちゃ～ん（３１５８）、ア○街ック天国は南○住物語というところに出てましたよ。確か、貨物ターミナルの駅の所で出てましたね。
ということで、おっさんは引っ込みます。

### [3169] 玉井真矢 	 2001.09.24 MON 19:33:42　
	
＞[３１５４]sweeper様、[３１６５]無言のパンダ様
少し暴走気味の話にレスして頂きありがとうございます。
あといちよう付け足すと「サ＠ラ大戦」はゲームが先です。
アニメでやったのはゲームがアニメ化されたやつでこのアニメには伊倉さんがやっているレニ（キャラクター名）は出ていなかったし僕の住んでいるところでもやっていなかったので見ていません。
無言のパンダ様
声優が好きならばぜひ「歌謡ショウ」を見てみてください。
これは「サ＠ラ大戦」をもとにしたもので前半がストーリ的なもので後半が舞台です。後半の舞台は宝塚みたいなものです。
ちなみにレニ（伊倉さん）は男役です。
他に男役をやっているのは高乃麗さん（皆さん知っているかな）と田中真弓さん（田中さんは皆さん知っていますよね）です。
後半の舞台は「サ＠ラ大戦」を知らなくても十分楽しめます。
ビデオも出ていますし、レンタルして一度ご覧ください。
特に「アラビアのバラ」は伊倉さんの主役の舞台なのでおすすめです。
最後にゲーム「サ＠ラ大戦２」のラスボスの声は神谷明さんでした。


### [3168] 無言のパンダ 	 2001.09.24 MON 19:03:41　
	
ＣＲＡＺＹ　ＣＡＴ様（クン？）
遅ればせながら、お誕生日おめでとうございます♪
これからまだ、１０代２０代・・・と若い青春の日々が続くと思うと、「オバサン」は羨ましくてたまりませんヨ！

ちゃこ様（３１６６）
「峰不＠子」は、小原乃梨子さんじゃなくて、増山江威子さんです。いらんツッコミしてごめんなさい！
ちなみに小原乃梨子さんは「アルプスの少女ハ＠ジ」の「ペー＠－」なんですよ☆私ハ＠ジ大好きなんで(^_^)

ちゃこ様のコメントからは、いつも本当に、リョウと香のことを
心から愛してるんだなぁって気持ちが伝わってきます。
私も香の死が、単なるストーリーや作品の軌道を変えるための道具として使われたわけじゃなかったんだと納得できる今後の展開を願っています。

### [3167] ＣＲＡＺＹ ＣＡＴ 	 2001.09.24 MON 17:06:09　
	
こんにちわ。１５歳になった、ＣＲＡＺＹ ＣＡＴです。

＞あお様【３１４５】
　見つかりました！！北条先生のイラスト集。あお様のおかげです。ありがとうございました。

＞皆様へ
　皆様、お祝いの言葉ありがとうございます。とてもうれしいです。それから、おっさん様【３１５６】　「ちゃ～ん」って、女の子だと思われてしまったのかな？すみません、「く～ん」なんです。つまり、男です、一応。

＞ちゃこ様【３１６６】
　「受け入れられる死・られない死」、分かるような気がします。それにしても、ちゃこ様の表現力に驚かされます。はっきり言ってカンドーです。

それでは、ＣＲＡＺＹ ＣＡＴでした。


### [3166] ちゃこ 	 2001.09.24 MON 16:36:55　
	
こんにちは。「富士山に初冠雪」と思ったら、今日は夏の暑さ。
皆様も体調に気を付けてお過ごし下さいませ。

そうそう、CRAZY　CATさんお誕生日おめでとう（＾＾）/
まだ私の半分しか人生生きてないなんて、羨ましい限りです（笑）
楽しい１年でありますように。

「声優ネタ」
皆さん、本当に詳しいですね～！何か、感心してしまいます。
永井一郎さんは、色々な役をされてるんですね。（しかも観てる物多し）
「波＠さん」だけだと思っていて、かなり失礼だわ、私（＾＾；；）
役柄のギャップといえば、確か「ドラ＠もん」の「の＠太」と「ル＠ン＠世」の
「峰＠仁＠」は、小原乃梨子さんがやってらっしゃるんですよね。
・・・ごめんなさい、更にＣＨから離した話題にしちゃってますね（＾＾；；）

「受け入れられる死・られない死」
やはり「スケバン刑事」ファン多し！ですね。
すごく切ないラストでしたが、それでもサキ＆神は結婚したのね。
と思うと「幸せにね」と、言ってあげたいと思いました。
色々思いつかないけど「C@WBOY　B@P@P」も受け入れられたラストです。
基本的に嫌だけど、「死」がその人に「平穏」をもたらしたのであれば
そこに救いがあるのなら、それはそれで良いのかな、と。
上手く表現できませんが・・・

私が「心が生きてるから香は生きてる」って、言い続けてるのは、
ゆうやん様も書いてますが（＞3142）あまりにリョウが可哀相過ぎて。
どんなに辛くても「孤独なまま」でいられたら。その孤独が、それ以上深く
ならない気がするのです。でも「癒される心地よさ」を知ってしまったら。
それを無くしたときの「孤独」は、以前よりも深くなってしまうと思うのです。
ＡＨ読み始めてからずっと、リョウに微笑って欲しいなぁ・・・
優しい目をして欲しいなぁ・・・と思ってます。
（16話で笑顔見せてるけど・・・私の中では、ちょっと違うから。）
香といる時に見せていた「愛しくてたまらない」って感じの優しい目して
微笑む日はもう来ないのかなぁ・・・
今後の展開で香には、本当にリョウ＆ＧＨを救って欲しいと思います。

長い上に湿っぽくなってしまいました。スミマセン。
また、来ます。

### [3165] 無言のパンダ 	 2001.09.24 MON 13:04:44　
	
玉井真矢様（３１５３）sweeper様（３１５４）
「三つ目がとおる」へのレス、ありがとうございます。
「保介」でしたか～！思い出させてもらって、うれしいです♪

「サ＠ラ大戦」は残念ながら見たことがないんですよ～。
こちらでは放映されてなくて・・・。
でも「サ＠ラ大戦」の存在自体は知ってますよ。
ゲームにもなっているし、たしかこれも実写でＣＭされてましたよねー。横山＠佐さん！

私は常々、声優さんの仕事ってすごいなぁって思っているので、声優さんに詳しいんではなくて、好きなだけなんですが、
特に大人になってからは、アニメを見る時どうしても声優さんから入っていく傾向にあるんですよね～(^_^;)
でも最近、声優さんの中にも同じ番組のスタッフの人と付き合ってて、別れたので一緒に仕事できないからって、役を降りたりする人もいるそうで・・・。
それは、ちょっとプロ意識に欠けるかなぁって思いますけど・・・
だけど、一般の人に置き換えてみると、ちょっとわかる気も・・・声優さんも人間だから、複雑ですよね。

それにしても、Ｂｌｕｅ　Ｃａｔ様（３１４６）
野村＠子さんと内海＠二さんがご夫婦だったなんて、初めて知りました！びっくりしました。
私は内海さんといえば、「ドクター＠ランプ」の海苔巻せん＠いを思い出してしまいますが（古！？）

ドクター＠ランプといえば、初代ア＠レちゃん役の小山＠美さん（ＣＨでは美樹さん）と「巨人の＠」や「聖闘士＠矢」初代ガンダムの古谷＠さんと、以前結婚されてましたよね～。（現在は×）
あと漫画家では「セーラー＠ーン」の武内＠子さんと「幽＠白＠」の富樫＠博さん、最近結婚されて、お子さんも生まれたそうで・・・。
声優さんや漫画家の先生も、職場結婚て多いんでしょうか？

将人様（３１６２）
神谷さんのＣＭ、よく憶えていらっしゃいますね～♪
私は、そのＣＭがはじまると、目が釘づけになって、あっけにとられてるあいだに終わっちゃうので、セリフまでは記憶してませんでした（笑）

私も「宇宙戦艦ヤ＠ト」、劇場で３作くらい観たんですけど
（テレビでは、あんまり見てなかったかも）
永井＠郎さんの事は、憶えてなかったです★
それにしても「安室波平」は寒いですヨ！（笑）

長々と失礼しました☆

### [3164] Ｂｌｕｅ　Ｃａｔ 	 2001.09.24 MON 10:54:38　
	
★サザ＠さんの声優さん
　　永井一郎さん（波平さん）と麻生美代子さん（フネさん）って、「らんま」で八宝菜＆コロン（シャンプーのひいばあちゃん）の声をしてたんですよねぇ、あまりにも「＠ザエさん」のときとギャップがあって、声優さんってやっぱすごい！って思いました。永井さんといえば「うる星やつら」のチェリーもそうなんですよ。
　　あと、野村道子さん（ワカメちゃん）と、内海賢二さん（「Ｃ・Ｅ」の課長さんや「北斗の拳」のラオウ役ｅｔｃ）が夫婦だって知ったときは本当にびっくりしました。今は内海さんが賢プロの社長で野村さんが副社長をしているんだそうで。
　　それから増岡弘さん（マスオさん）は、アンパンマンのジャムおじさんの声もしているんですよ。
　　でも・・・ここでこんな話してていいのかな？(笑）

★スケバン刑事
　　３１６２将人（まじん）様、実写ドラマ、３代目は浅香唯さんですよ。わたしは原作への思い入れがありすぎて、実写版は耐えられませんでした・・・原作まで笑いものにされてしまいそうで。
　　ちなみにアニメはＯＶＡで、「誕生編」と「復讐編」の２本が出てたと思います。伊倉さんのサキがすごくカッコいいんですよ！ちょっとドスをきかせたカンジの声で。

　　いよいよ明日は２週間ぶりにＡ・Ｈに逢える♪　楽しみだなっ♪　
　　

### [3163] 踊るたい焼き♪ 	 2001.09.24 MON 03:54:48　
	
う～む、探せばあるもんだ。
ずっと前から「キャッツアイ」や「シティーハンター」
などは全巻そろえて何度も読み返していたんですが、
何となく「北条 司」さんのＨＰってあんのかな？
と思い調べるとあったんですな、これが！
今日のトコロはこのへんで。
これからも皆様、どうぞヨロシクッ！！

### [3162] 将人（まじん） 	 2001.09.24 MON 01:57:09　
	
こんばんは、将人（まじん）です。

＞[3138] 無言のパンダさま
「スーパーロボット大戦シリーズ」
神谷明さんは、1998年頃に発売されたプレイステーションの
ゲーム「スーパーロボット大戦Ｆ」と「Ｆ完結編」のＴＶＣＭ
に出演されていましたよ。
ロボットのパイロットの教官役で、女性の主人公のパイロットの
服装をされた方に、「神谷雄叫び」を教える設定でした。
「ハイでは無い、了解またはラジャーだ！」ってセリフとか
あったと思います。

＞[3151] 無言のパンダさま
「安室波平・・・もとい磯野＠平さん」
永井一＠さんは、宇宙戦艦ヤマ＠では、酒飲みのお医者さん役
と機関長の２役で出演されていましたよ。

後、有名なところでは初代のガンダムでナレータ役をされて
いました。ガンダムでは他に「兵士Ａ」とか「その他」とか
で声を出されていたので、地球連邦軍とジオン軍の２重スパイ
って言われていました。
（同じ声が敵味方の両方で聞けるので）

＞スケバン刑事
アニメ版ってあったんですね。
私は大阪では木曜日の７時に神谷明さんの「北斗の拳」を見て
その後７時半に放送されていた実写版を見ていました。
たしか１代目が斉藤由貴さんで、２代目が南野陽子さんだった
んですが、３代目が誰か忘れてしまいました。
よくモーニング娘・・・違った「おニャン子クラブ」が
ゲスト出演されていましたね。
特撮ヒーロー物に出演されていた方も多く出ていて、
たしか南野陽子さん演じる２代目麻宮サキの父親役に
仮面ライダーＶ３・アオレンジャーの宮内洋さんが
でてこられたのが印象に残っています。

### [3161] さえむら 	 2001.09.24 MON 00:43:53　
	
こんばんわ。さえむらです。
最近めっきり寒くなってしまいましたね。

[ウィルス]
多分、プロバイダのホームページに今回のウィルスに関する情報が詳しく載っていると思いますよ。

＞CRAZY CAT様[3148]
お誕生日、おめでとうございます！
いいなぁ、十五歳かぁ....。
若くて羨ましいです。
今年が素敵な一年でありますように....。

連休明け（火曜日♪）が待ち遠しい�（今年はもう、さえむらには仕事で三連休がない(T_T)）

