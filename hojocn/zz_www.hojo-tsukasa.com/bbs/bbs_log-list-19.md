https://web.archive.org/web/20011121230819/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=19


[380] おっさん（岐阜県：裏Ｎｏ．1販売員）2001.07.08 SUN 23:32:55  

こんばんわー。やっと仕事が終わって、ご飯食べてくつろいでるところです。今日も昨日と比べてかなり忙しかった。Ｔ－Ｔ明日は仕事お休みです。やっと・・・。  
姫様・柴音♪様有難うございます。何とか今日の仕事を終えました。  
仕事先で、ＣＨ知ってるって聞くと知ってる人かなり多い。しかも、ＡＨの連載のことを聞くとまだ知らない人もいるし・・。やっぱ、私と近い年齢の人（上は３１歳下は２０歳、ついでに私は２５歳＾－＾）知ってますね。もっと仕事先の人に色々おしえなきゃね・・・。  
また、明日も書きまーす。  
以上、おっさんでした。ついでに、私も柴音♪様と同じ昭和５０年９月生まれです。ドラエモンの前日の日です。

晚上好。终于工作结束了，正吃着饭放松呢。我今天也比昨天忙了很多。Ｔ－Ｔ明天休息。终于…。
姫様・柴音♪様，谢谢。总算完成了今天的工作。
在工作的地方，听说知道CH的人相当多。而且，听说AH的连载还有不知道的人··。果然，认识和我年龄相近的人(大的31岁，小的20岁，顺便说我是25岁^ - ^)。必须更多地告诉工作处的人各种各样的事···。
另外，明天也写。
以上是おっさん。顺便说一下，我也和柴音♪一样是昭和50年9月出生的。是哆啦a梦的前一天。


[379] Goo Goo Baby （さっき超ウケた！）2001.07.08 SUN 23:19:36  
さっきコンビニ行ったらビビった！！！  
心臓止まるかと思った！！　いや、止まった！！  
  
だって  
海坊主みたいな人がイキナシ入ってきたんだもんっ！！（怯）  
  
すっげ～デカかったし、ある程度筋肉あったし  
スキンヘッドだったから、店内の人間すべてが彼に大注目！！  
  
でもみんな目ぇ合わせようとしなかった。私も。  
だってガラ悪かったからコワかったもん（怯）  
  
しかも  
どーせ酒でも買うんだろうと思ってたら、  
コレが全然ハズレ！！  
大ハズレ！！（爆）  
  
彼がレジに持ってった商品を見て、私は笑いそうになった（堪）  
  
彼がコンビニから出てったあとの店員の含み笑いを  
私は一生忘れないよ！！（核爆）  
  
海坊主のようなその彼はイッタイ何を買ったと思う？？  
  
　それは・・・・  
  
　　　「なっちゃん」・・・・・・・・（爆笑！！！）  
  
あの海坊主が  
「なっちゃん」を買うシーンを想像してごらんよ！！！  
笑っちゃうよ！！！　あ～おっかしい！！！　  
おなかイタイ！！（疲）  
  
彼は「なっちゃんトランク」が欲しかったのだろうか・・・  
  
話は変わって、  
私の家の近くにある某本屋サンは  
単行本や週刊マンガ誌を発売日前日から売っているんで、  
明日そこ行けばバンチ読めるんだワ！！！  
あの本屋ショボイ（←失礼）から存在忘れてたワ・・・（汗）

刚才去便利店吓了一跳! ! !
我以为心脏要停止跳动了! !不，停了! !
因为有像海怪一样的人毫无生气地进来了! !(怯)
厉害~因为很大，有一定程度的肌肉又有光头，所以店内所有的人都非常关注他! !
但大家都不看。我也是。
因为是我不好，所以才害怕的(怯)
我还以为他会买些酒呢。
情况完全不是这样的! !大错误! !(爆)
看到他拿到收银台的商品，我差点笑出来。
当他离开便利店时，店员的微笑
我一辈子都不会忘记! !(核爆)
你猜像海怪一样的他到底买了什么? ?
那就是……“なっちゃん”……(爆笑! ! !)
想象一下海坊主买“なっちゃん”的场景吧! ! !
我要笑了! ! !啊~好可怕! ! !
肚子好疼! !(累)
他想要“なっちゃん皮箱”吗?
换个话题，我家附近的某家书店从发售日的前一天开始卖单行本和周刊漫画。
明天去那里就能读到Bunch! ! !
那家书店太小了(←失礼)我都忘了它的存在…(汗)


[378] 金田　朗（福岡）2001.07.08 SUN 22:53:17  
週末出張だったので今まで週末分読んでました。つくづく回転早いですねぇ。  
  
「ファーストキス」の時期ですが、私もＣＨ時代だと思ってました。同意見の人がいてうれしいです。  
  
それとT.M.ファンがいるってことは…年代的には私と近い？T.M.のファンしてた私としては感激です。小室さんのソロコンサートも元旦に行ったし。  
  
脳死の話は悲しいですねぇ。若干医療系を知っているのでそれがどういう状況で行われるのか知っていますし。提供者側としても他の臓器と比べて思い入れが強いようです。リョウちゃんのすごい覚悟の上での提供と思いますが、新宿で探し回っている姿はその事実をどこかで受け入れきれていないのか…とかも思ってしまいます。

因为周末出差，所以到现在为止读了周末的帖子。刷新得真快啊。  
「First kiss」时间，我也认为是在CH时侯。我很高兴有相同意见的人。  
还有T.M.有粉丝的话…年龄上和我近吗?作为T.M.的粉丝的我很感激。小室的个人演唱会元旦也去了。  
脑死亡的故事很悲伤。因为我了解一些医疗方面的知识，所以知道那个是在什么情况下进行的。作为捐赠者，与其他器官相比，似乎也更有感情。我想是獠酱在很大的觉悟上的提供，不过，在新宿四处寻找的身姿，我想他是否还没有完全接受这个事实...  


[377] ワタル／Wataru（宮城県） 2001.07.08 SUN 22:32:43  
おばんです！（＝こんばんは。方言です）  
  
みなさん，「シュリ」っていう映画見たことありますか？  
ちょっとAHを連想させられました。北条先生は見たこと  
あるのかなあ。  
  
ところで私のHNはアニメの「ワタル」とは関係ないっす。  
  
Obandesu!(=a dialect for good evening)  
Have you ever seen the movie‘Shuri’?  
It reminds me of AH. I wonder if Mr. Hojo has seen it.  
  
By the way, my handle name has nothing to do with  
the cartoon ‘Wataru’.

おばんです！(表示晚上好的方言)。
你看过电影《Shuri》吗？
让我稍微联想到了AH。我想知道北条老师有没有看过。

顺便说一下，我的HN和动画片《Wataru》没有关系。(译注：HN是"handle name"的缩写,指用户名。)


[376] おさる  2001.07.08 SUN 22:31:48
こんばんは、おさるです。  
伊倉さんは、日テレの深夜番組の「あんたにぐらっちぇ」  
という番組でもナレーションなさってますよ。  
伊倉さんの低めの声がなんともクールでよいです。  
某国営テレビで、ER（外国のドラマ）でもたまに  
患者の役で出演なさっているみたいです。  
  
私は、「今週のキス」のほうが好きです。  
なんかとっても２人らしいし....。  
コミック７巻でも、ゲストの拓也くん（？）に  
せかされていろいろ画策していますし。  
今回は、大成功ってところでしょうか。  

晚上好，我是おさるです。  
伊仓先在日本电视台的深夜节目《あんたにぐらっちぇ》中担任旁白呢。
伊仓低沉的声音真是太酷了。
在某国营电视台，在ER(外国电视剧)中偶尔也出演患者的角色。

我更喜欢“本周之吻”。
好像是两个人的样子....。
在漫画第7卷中，客串的拓也君(?)被催促着做了各种策划。
这次应该是大获成功吧。


[375] ｍａｍａ  2001.07.08 SUN 21:58:03  
＞はやねさま  
アニメの方は分かりませんが、ナレーションの方は、伊倉一恵さんになっています。日テレに多いかなナレーションは。  
  
みなさんは、「セイラ」と「今週のキス」どちらが好きですか？  
私は、今週号が好きです。なぜなら、香の記憶がはっきりしているときのキスだからです。リョウは、いつも通り知らない振りしていたけど、本当は覚えていたし。リョウのことだから色々考えてあのキスになったんですよね。本命には弱いのかもリョウは・・・  
香にキスをするタイミングとか計画したのかな？  

＞はやねさま  
动画方面我不知道，旁白是伊仓一惠。日本电视台的旁白比较多。

大家喜欢「セイラ」还是「本周之吻」呢?
我喜欢本周这一期。因为这是在香的记忆清晰的时候接吻。獠虽然像往常一样装作不知道，但其实记得。因为是獠的事，所以考虑了很多才有了那个吻。也许獠对他的真爱是软弱的···
是不是计划好了吻香的时机？


[374] 優希  2001.07.08 SUN 21:44:56  
早い人は明日バンチが読めるんですねー。いいなぁ。見たくて仕方が無いです。最近シティを読み返ししまくってます。テレビでも見たいです。またスペシャルしてくれないかなあ  

早起的的人明天就能读到Bunch了。真好啊。因为想看，所以没办法。最近一直在重读City。也想在电视上看。能再给我点特别节目吗


[373] リンダ  2001.07.08 SUN 21:44:22  
＞由紀恵さま  
ホントだ、桜の花咲くころの帯に「Ｍｓ．ＫＩ」入ってますっ！！まだ知らない事が多いですね。こちらに来るとすごく参考になります。  
しかも、伊倉さんのラジオに北条先生が出演されていたなんてっ…（かなりショックかも）去年かな？佐藤藍子さんのラジオに出演された時も聞き逃したんですが、どなたか聴いた方いらっしゃいますか？ぜひ感想を教えて下さい！！  
  
聴コミのＣＤ化、私もして欲しいです。うちのＰＣでは聴けなかったんです～っ！！でも、きっとリョウの声で「香は生きている」なんて言われたら、私泣いてしまうんだろうなあ。（苦笑）  

＞由紀恵さま  
真的，樱花盛开时的书带上有「Ｍｓ．ＫＩ」! !还有很多不知道的事情呢。来这里的话很有参考价值。
而且，北条老师也出演了伊仓的广播…(可能相当震惊)是去年吗?佐藤蓝子参加广播节目的时候我也没听，有谁听了吗?请一定告诉我感想! !

听漫的CD化，我也想要。我家的PC不能听~ ~ ! !但是，如果听到獠说“香还活着”的话，我一定会哭的吧。(苦笑)


[372] はやね（大阪秘密基地）  2001.07.08 SUN 20:30:15  
ああ、同じ発言が二回ある時ってこういう状態なんですね(笑)。うかつに更新や送信を押すのはまずいみたい。反省。

啊，同样的发言有两次的时候是这样的状态(笑)。轻率地按下更新和发送好像不太好。反省。  


[371] はやね（大阪秘密基地）  2001.07.08 SUN 20:28:45  
＞由紀恵さん  
　ずっと「Ms.K.I」が気になってたのですが謎が解けて嬉しいです。といっても心当たりでこのイニシャルの人って伊倉さんしかいなかったのでそうかな～とは思っていたのですが、なぜ伊倉さんなのか確信がなくて。  
　ところでmamaさんが書いてらしたように伊倉さんは一恵から一寿へ改名されてましたよね。（確か姓名判断がすごく悪かったとかいう理由だったような？）今はまた一恵に戻ってらっしゃるんでしょうか？  
Ａ・Ｈについて神谷さんの感想はきいたことがあるんですが伊倉さんがどう思ってらっしゃるのかも聞いてみたいな。  

（译注：日文与下条相同。译文见下条译文。）


[370] はやね  2001.07.08 SUN 20:28:44  
＞由紀恵さん  
　ずっと「Ms.K.I」が気になってたのですが謎が解けて嬉しいです。といっても心当たりでこのイニシャルの人って伊倉さんしかいなかったのでそうかな～とは思っていたのですが、なぜ伊倉さんなのか確信がなくて。  
　ところでmamaさんが書いてらしたように伊倉さんは一恵から一寿へ改名されてましたよね。（確か姓名判断がすごく悪かったとかいう理由だったような？）今はまた一恵に戻ってらっしゃるんでしょうか？  
Ａ・Ｈについて神谷さんの感想はきいたことがあるんですが伊倉さんがどう思ってらっしゃるのかも聞いてみたいな。  

＞由紀恵さん  
虽然一直很在意「Ms.K.I」，但是解开谜题很高兴。话虽如此，但我能想到的名字首字母的人只有伊仓，所以我想是吧，但我不确定为什么是伊仓。  
对了，就像mamaさん写的那样，伊仓从一惠改名为一寿。(好像是因为姓名判断得很差之类的理由?)现在又变回一惠了吗?    
关于A·H，我听过神谷先生的感想，也想听听伊仓是怎么想的。    
（译注："伊倉一惠"艺名"伊倉一壽（1991年－1995年）,平文式罗马字:Ikura Kazu"，参见[伊倉一惠 | Wikipedia](https://zh.wikipedia.org/wiki/伊倉一惠)）

[369] MIRACLE  2001.07.08 SUN 20:26:23  
うむむ。  
ＹＡＨＯＯとかだと否定的な意見が多い！！  
でも、北条先生がそんなんで終るはずはないし、俺は信じてる！！  
がんばれ！！北条先生！！！  
  
※ページ数が少ないのは非常に悲しい・・・・  

嗯嗯。  
在YAHOO和其他地方，有很多负面的意见! !    
但是，北条老师不会就这样结束的，我相信! !  
加油! !北条老师!  

※很遗憾，页面太少了……  


[368] 葉月  2001.07.08 SUN 20:20:24    
★　ＵＭＩさま  
初めまして。海坊主さんが好きなんですね！ＣＨの中では私は  
冴子サンが一番のお気に♪ですヨ！ＡＨでは署長さんにまで登りつめてる彼女がカッコよくてＣＨ連載当時から憧れでした。  
海坊主さんって、思わず寄りかかってしまいたくなるくらい、頼りがいがありそうですよね？  
そばに居たら、キャーって叫んじゃいそうなくらいカッコいいです。。。  
ＣＨから何年も経ったのに未だにミーハーな私って一体・・・(^^;)  
文庫本の最終巻に北条先生が「続きは考えてる。また書きたい」というような事を書かれているのを、ずっと信じてまたリョウや  
海坊主サンたちに会えるのを楽しみにしてました。  
その願いが10年ぶりに叶って、毎週バンチが楽しみです。  

★　ＵＭＩさま  
初次见面。你喜欢海坊主啊!CH中我最喜欢冴子小姐♪哟!在AH攀登到署长的她很帅，从CH连载当时开始憧憬。  
海坊主看起来很可靠，让人忍不住想要依靠他吧?
如果我靠近他，我就会尖叫，他太酷了。 …
从CH到现在都过了好几年了，我还是个Mīhā(^^;)  
（译注："ミーハー/mīhā"表示“只有当某个事件成为公共领域的话题时（或者在媒体中被提及）才开始对这件事感兴趣的人”。参见：[ミーハー|Zhihu](https://zhuanlan.zhihu.com/p/41023676)、[ミーハー|Spicomi](https://spicomi.net/media/articles/1628)。）  
在文库本的最后一卷中，北条老师说:“我还想继续写下去，我还想再写。”我一直相信这句话。   
我很期待见到海坊主们。  
那个愿望时隔10年实现了，每周都很期待Bunch。  


[367] キャサリン  2001.07.08 SUN 20:18:48  
～由紀恵さん～  
初めて知りました。  
ずっと謎だったんですよね～・・・  
有難うございます  
  
ところで、皆さんは「CITY」の中のキャラ誰が好きですか？  
私はなんといっても「りょう」ですね。  
あのきめた所がカッコイイんです～！！  
よければお返事ください。  

～由紀恵さん～  
我第一次知道了。  
一直是个谜呢~···  
谢谢您。  

对了，大家喜欢《CITY》中的谁呢?  
不管怎么说我都是“獠”。  
他坚定的态度很帅~ ! !  
如果可以的话请回信。  


[366] UMI  2001.07.08 SUN 20:10:20  
はじめまして。UMIです。  
私は、HNをUMIにした理由は、海坊主の大ファンだからなんです。  
チョッと変わっていますかねー？  
こんな私ですがよろしくおねがいします。  

初次见面。我是UMI。  
我把HN变成UMI的原因是因为我是海坊主的忠实粉丝。  
有没有变了呢？  
就是这样的我，请多关照。  


[365] 紫音♪  2001.07.08 SUN 20:10:14  
＊由紀恵さま＊  
「桜の花の咲くころ」にはそんなエピソードがあったなんて知りませんでした・・・確かに「Ms.K.I」って入ってます！いい情報ありがとうございます！また違った感じでおもしろく読めそうです。(^\_^)  
  
＊茶々さま＊  
私も神村幸子さんの絵好きですよ～。クリエ－ターさんでしたっけ？？←うる覚えでこれがまた。。。(^^;すごく特徴的な感じで綺麗だったんで気になってましたよ。  
  
＊りなんちょさま・姫さま＊  
そうですね、北条先生の作品好きなことに年齢は関係ございませんっ☆ありがたきお言葉ありがとうございます☆  
これからもよろしくです～！！  
  
＊Goo Goo Babyさま＊  
お知り合いの男性の方が「女」でカキコしてらっしゃるとは何だかおもしろいです。(^^)  
でも、わたくしは女ですよ～！（笑）  

＊由紀恵さま＊  
我不知道《樱花盛开时》有过这样的故事···确实有「Ms.K.I」!谢谢你的好消息!又会有不同的感觉，读起来也很有趣。(^ _ ^)

＊茶々さま＊    
我也很喜欢神村幸子的画。是Creator-san吗? ?←太啰唆了，这又是…(^ ^;非常有特征的感觉很漂亮，所以很在意。  

＊りなんちょさま・姫さま＊    
是啊，喜欢北条老师的作品和年龄是没有关系的。
今后也请多多关照~ ! !    

＊Goo Goo Babyさま＊    
认识的男性用“女性”来写，总觉得很有趣。(^ ^)  （译注：待校对）  
但是，我是女人啊~ !(笑)


[364] ゆなつ  2001.07.08 SUN 19:51:37  
でおくれてしまった。  
最近忙しくてホームページを開かなかったら、あらら復活しているではありませんか。。。しかも相変わらず書き込みが多い。  
とりあえず、前のは後で読むことにして、今更ながら一人で復活をよろこぶとします。（＾ｗ＾）  
  
そこで、ＣＨで好きな曲の話が少し書かれているようなので乱入させてください。  
ＣＨで好きな曲っというか、インストになるんですが、  
ミッドナイト・ライトニングが好きなんです。  
イメージは今まさに照準を敵に合わせているときのリョウ。  
曲ではやっぱり「Get Wild」でしょう。「Still Love Her」も好きですけど。  
  
ＧＨの話しは控えます。まだ整理ついてないので。でも、毎回読むごとに少しずつ私の中で考えが変わってきています。っというか救われてきてます。  
創刊号読んで次の日に仕事場でみんなに心配かけるほど落ち込んでいた私だったので、、、（笑）  
  
発売日まで後二日！楽しみでしょうがない！久しぶりに週刊誌を買って燃えてます！  
北条先生頑張ってください。  
と今なら今なら言えます（っておいおい）（笑）  

我迟到了。
最近忙得没打开网页，不是又复活了吗……而且留言依然很多。
总之，前面的帖子以后再读，现在一个人为复活而高兴。(^ w ^)

我想打断一下，CH上好像写了一点喜欢的曲子的事，请允许我乱入。
CH上最喜欢的曲子，或者说是前奏，我喜欢Midnight Lightning。
Image是正在瞄准敌人时的獠。
曲子方面还是《Get Wild》吧。我也喜欢《Still Love Her》。

不要说GH的话。因为还没有整理。但是，每次读的时候，我的想法都会一点点改变。或者说被拯救了。
看完创刊号，次日在工作的地方，我的心情非常低落，让大家都很担心。(笑)

距离发售日还有两天!很期待吧!好久没买周刊杂志了!
北条老师，请加油。
现在我就可以这么说了(喂)(笑)


[363] Goo Goo Baby　（神奈川）2001.07.08 SUN 19:26:57  
＞由紀恵さま  
　  
　すいません  
　私の説明不足でした（謝）  
　詳しくは過去ログ１７を見てください  
　（私のＨＮの右のカッコの中は  
　　確か「好きな曲♪」だった気が・・・）  
  
私が好きだと言った曲は  
「ＣＨの曲の中で好きな曲」ではなく  
「聴きながらＡＨ読んだら個人的に泣けた曲」ですんで  
ＣＨとは関係ない曲です  
  
　でもイイ曲なんスよ！！　セツナイ！！  
　  
　胸キュンモノ！！　　どっきんｍｙハート！！！（死語？）  

＞由紀恵さま  
对不起，是我解释得不够充分。(谢)  
详细请看过去log 17(我的HN右边的括号里好像是“喜欢的曲子♪”…)  

我说我喜欢的歌不是“CH的歌中我最喜欢的那首”，而是“我一边听，一边读，让我哭了”，和CH无关。  
不过这是首好曲子哦! !难过! !  
这是一首让人心潮澎湃的歌! !どっきんmy heart! ! !(死语?)（译注：待校对）  


[362] 玉井真矢  2001.07.08 SUN 18:23:38  
>由紀恵さん  
そうなんですか？  
僕も伊倉さんのファンですけど全然知りませんでした。  
そう言えば「こもれ陽の下で・・・」で伊倉さんの出身地である長野県の上田市（２巻Ｐ２０３）がでてくるのもなんか関係があるのでしょうか？  
ちなみの僕も上田に住んでいます。  

>由紀恵さん  
是这样吗?  
我也是伊仓的粉丝，但完全不知道。  
这么说来，「在阳光下...」中提到伊仓-san的出生地长野县的上田市(第2卷P203)也有什么关系吗?  
（译注：作品「在阳光下...」的中译名为《阳光少女》或《艳阳少女》。伊倉一惠出身於長野县上田市，参见[伊倉一惠 | Wikipedia](https://zh.wikipedia.org/wiki/伊倉一惠)）   
ちなみ的我也住在上田。（译注：待校对）  


[361] 由紀恵(三重）  2001.07.08 SUN 18:06:59  
回転早いっすねここ…当然かもしれないけど…  
伊倉さんファンとしましては伊倉さんの話が出ているのに黙っておれません…北条先生の「桜の花の咲くころ…」（読みきりですよね？）は伊倉さんの実際のお話が元になっております。証拠に、帯にK.Iへと入ってたはずです。（今手元に無いんで記憶だけですが…）お手元におもちの片チェーックしてみてください。  
ラジオで、北条先生がゲストに出たときに話してました。  
「今度の作品は伊倉さんの話しがもとなんだ…」と…  
ワタルも知ってる方は知ってらっしゃいますよね？（＾＾）/~~

刷新得真快啊这里…这也许是理所当然的…  
作为伊仓的粉丝对于伊仓的话题不能保持沉默…北条老师的「樱花盛开时…」(读完了吧?)是根据伊仓的真实故事改编的。证据就是腰封里应该有K.I。(现在手边没有，只有记忆…)请试着在手边挂链。  （译注：待校对，故事原型究竟是伊藏本人还是她说的话？"伊倉一恵"的平文式罗马字为"Ikura Kazue"，参见[伊倉一惠 | Wikipedia](https://zh.wikipedia.org/wiki/伊倉一惠)）  
在广播里，北条老师作为嘉宾出现的时候说过。  
「这次的作品是以伊仓的话为原型的……」和…（译注：待校对，故事原型究竟是伊藏本人还是她说的话？）    
知道ワタル的人都知道吧?(^ ^) /~~  （译注：“ワタル”可能指“魔神英雄伝ワタル”）  
