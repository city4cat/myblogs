https://web.archive.org/web/20031024195543/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=435

### [8700] Ａｙｕ	2002.11.14 THU 16:20:48　
	
里奈様
「ストレートパーマ＆縮毛矯正禁止」の校則についてですが、縮毛矯正に関しては最近専用許可書が出来てそれを学校に提出して許可が出たら出来るという様なのですが・・・。でも私はそれを出さないで高校入学前からずっと黙ってやってます（爆）遼と香が生活指導だったら校則違反をどんな感じに取り締まると思いますか？

将人様
ＣＤ－Ｒありがとうございます。メールで送った合同コントの一部はどうでしたか？今日キンキキッズのＣＤを聞いていて閃いたんですが、槇ちゃんと遼が卒業生を送る会とかでキンキの曲歌ったらどう思いますか！？

### [8699] 里奈	2002.11.14 THU 04:00:10　
	
あっ…
こんな時間（深夜４時）なのに将人さんまだ起きてる…

### [8698] 里奈	2002.11.14 THU 03:56:06　
	
　あぁぁぁーー！
　　　もぉぉぉ　いやぁぁぁーー！！
　　　　　　　　　ガァァーー（￣□￣；）
どうにかしてデジカメの画像を取り込もうと頑張ってたけど、ぜんっぜんダメ！もう腹たってきたわっ！ムッキィィ！！５時間もパソコンとニラメッコしたけど原因がわからんくって何もかも投げ飛ばしたくなる勢いで爆発中！しかも全然関係なさそうなとこクリックしてたらなんか余計わけわからんことなって　ますます激怒中！機械オンチなんじゃぁー！こらパソコォーン！なにがどうなってんのか自分で説明しやがれぇー！シカトすんなぁぁー！
…って切れまくってたら今夜も家の前で車が事故って、なぜかその激突音でスッキリしてしまいました。（他人の不幸はチョコの味ぃ～♪←超チョコ好き）
ってなわけで、理解不能なことはサッサとあきらめて気晴らしにカキコに訪れたのさぁー♪
あぁ～　この時間…癒されるわぁ～（￣ヮ￣）ホッ

☆８６９５　将人（元･魔人）様
トップページ、ＡＨ５巻に変わってますね♪
まさか…いやいや　もしかして…と思って『もっこりメッセージ』を確認して案の定ガッカリしてしまいました（笑）
共同コントを連載に！？うおぉ～、なんかそうなったら凄い！で、誰が作るの…？（人事…笑）

リョウの｢もっこりパワー充電！｣ってやつ、すごく使いたい！
でも友達や親や彼氏がいる時にパソコンにいきなりそんな事言われたら恥ずかしくなっちゃうわ！でもパソコンにリョウが住んでるみたいに思えて愛着沸くかも♪（ついさっきまでコイツ割ったろか！？って思ってた里奈…単純）

ケンシロウＶＳガンダム、ガンダム好きの人が見たら怒るんじゃない？だってあんな巨体なのに攻撃はいっさい通じず、素手でやられちゃうんだもん（笑）あぁダメ…また笑いが…！

☆８６９４　無言のパンダ様
カキピーの｢パンダとアザラシが合体｣のＣＭ、意外に誰も見てないんだぁ～。ってことはホントに地域限定？そういえば関西弁だったようなぁー…
内容は、体がアザラシで顔がパンダの謎の生命体の親子が普通の民家でくつろいでカキピー食べてテレビ見ながら世間話してるシーンです。
父とみられる親のほうが（超おっさん声で…）
『柿の種とピーナッツはこんなに合うんに
　アザラシとパンダがこんなに合わんとは思わんかったなぁ～』
で、ソファーでくつろいでる子のほうが
（憎たらしそうな子供の声で…）
『うん…そうやね…』
で、もくもくとカキピー食ってます。
他にも会話の内容だけが違うバージョンもありましたよん。

☆８６９３　ＴＡＫＥＳＨＩ様
見た見た！？
ケンシロウＶＳガンダム、見たぁー！？
　　　　　　　　　　　　　　　　　　　　（以上！）

### [8697] 将人（元・魔神）	2002.11.14 THU 03:47:38　
	
トップのＨＰ5巻の事を書いてるのいいのだけど、
その下の更新情報・・・
「AngelHeart 第５巻発売中！(2002.3.8)」となっている

ＡＨ4巻よりも早くて、ＡＨ3巻と同時発売だったのか～(汗)(^^;
たぶんＡＨ3巻の文章をコピーして付け足したんだろうな

### [8696] 将人（元・魔神）	2002.11.14 THU 03:07:00　
	
いつもは、このＨＰの掲示板に直接リンクで「お気に入り」
に入れて来ていたので、気付かなかったですが、
トップページ、やっとＡＨのコミック5巻になっていましたね。

### [8695] 将人（元・魔神）	2002.11.14 THU 02:04:20　
	
こんばんは、将人（元・魔神）です。

＞[8683] いちの様
画材とか言ってたら分かりにくですよね。これ出してもいいかな？
もう8年以上前に辞めているんですが、高校卒業から5年半程
勤めていた会社は、サ＠ラクレ＠スという文房具、画材のメーカー
でした。
パソコンで看板や、スーパーで使われるPOP広告(プライスカード)
を作るシステムの電話ユーザーサポートの担当でした。
広告に使われるPOP広告文字って、丸みを帯びていて独特ですね。

＠ラえもんが出来る時代って、どうなるんでしょうかね？
アト＠の頃に出てた携帯電話とか、ＴＶ電話（カメラ付き電話）
とか出来てますよね。
まだ人間の形をして、自分で考えて、人間のように動くロボット
は、技術は近づいてきてますが、一般家庭などには、まだ出来
て来ていないですからね。

＞[8684] OREO様
せっかく出来たカキコ、消えてしまったんですか？残念ですね。
共同コントのストーリ、何回かに分けて出してもいいですね。
「明日に続く」とか、「来週に続く」って、雑誌の連載みたいに

＞[8685] 千葉のOBA3様
ガスコントのつまみ、無理矢理押し込んだら直ったんですか？
しかし、修理の部品、送料の方が高いんじゃなんですか？
メーカーさんじゃなく、配達業者さんに渡す価格でしょうけど

＞[8686] Ａｙｕ様
合同コント、がんばって作りましょうね。
僕は、ＣＳ放送ではなく、東大阪市のケーブルテレビに入っていて
ＣＳ放送と同じような番組みれますよ。

（番組はケーブルテレビの契約した固定の番組で、1年に1度、
変わるんですが、自分で好きな選べないんですけどね。）

里奈さまや、TAKESHI様、マイコ様に送ったのと同じような
ＡＨのドラマＣＤなどを、作って送りますね。
楽しみに、待って下さい。

＞[8687] たま様
ＡＨ5巻、買われたんですね。「阿香」の事を翻訳する人は、
どうするんですかね？

＞[8688] 里奈さま
ＣＤ-Ｒ、そちらに無事に届きましたか。良かったです。
喜んで頂いて、こちらもうれしいです。
楽しんで下さいね。

「もっこりパワー充電！」ってのは、僕も好きですね。

＞[8689] TAKESHI様、[8691] マイコ様
聴コミのＡＨの第一話のドラマの香瑩（当時はグラスハート）の
声は、コ＠ンの高山みなみさんです。

本当は、ラジ＠で、神谷明リターンズで、神谷さんと雪野五月さん
が演じられていたＡＨの1場面も、パソコンで録音してたのですが
バックアップを取る前に、パソコンのハードディスクの故障で
パーになってしまたんですよ。　あれは、おしかったな～。

ＣＤ-Ｒの事、喜んで頂いて、僕もうれしいです。

＞[8690] しゃぼん様
XYZ BOXへ依頼に行かれるんですか？いいですね。
大阪じゃなかったら、僕も行きたいところなんですけどね。

＞[8692] 里奈さま
あの「ケンシロウ対Ｇガンダム」の映像に出ていたロボットは、
一応ガンダムです。

ですが、それまでの戦争物ではなく、当時に流行っていた
ス＠リートファ＠ターなどの格闘ゲームの影響をうけて、
ロボットで殴る蹴るなどの格闘をする話でした。

中に入っている人のレオタードか、全身タイツみたな物の
動きを、そのままロボットがマネするという形だったんですよ。

あの映像は、インターネットで知り合った人に、このＨＰに
こんな映像あるよって聞いて、ダウンロードした物です。
今は、そのＨＰのデータは、消されたみたいですが・・・。

上手い事、両方のアニメの画像を繋げているなってビックリ
しまいたよ。

＞[8693] TAKESHIさま
ぜひ、「北斗の拳のケンシロウvsＧガンダム」見て下さい。
笑えますよ。面白いです。

＞[8694] 無言のパンダ様
嘉＠達夫さんの「♪あ～あ～小市民♪」って歌ありましたね。
あれ聞いていて、僕もあれやってるな～って思いましたよ。

### [8694] 無言のパンダ	2002.11.14 THU 00:36:03　
	
こんばんわ★

ここのところ、寒かったり暖かかったり雨が降ったりで体調を崩し気味・・・あ～でももうすぐバン金♪なんだか早～い！早すぎていつも懸賞に応募しそこねてしまう(~_~;)次こそは・・・！

＞将人さま（８６７４）
嘉門達＠さんの歌に♪あ～あ～小市民♪っていうのがあったと思うんですが、その中の歌詞に「本は上から３冊目を取る」っていうのがあって、まさに私はソレです(^_^;)でもどうしても１冊しかない場合はかなり躊躇してしまいますよね～私の場合、他をいろいろ探し回って、最後の手段は「ネットショッピング」です(^^ゞやっぱり好きな本は出来るだけいい状態のものが欲しいですからね～♪
☆海坊主さんのケイタイ、あの大きさから言ってグ＠コはグ＠コでも「手提げグ＠コ」のおまけかと思われます（笑）

＞千葉のＯＢＡ３さま（８６７５）
コンロのつまみが取れちゃったんですか？！不便ですよね～(~_~;)私のとこはタンスの取っ手がよくとれてしまいます。これもタンスが開かなくて不便です(ｰｰ;)おまけに私の頭のネジも取れそうです（汗）
ケンシロウのバラバラ（？）に対抗してＡＨではポストカードの案に賛成です♪あと時期的に組み立て式のスタンプ（年賀状用）っていうのは・・・？でも使いたくないから（もったいなくて）ダメかなぁ(^^ゞ

＞ｓｏｐｈｉａさま（８６７６）
愛車にボールをぶつけてへコんだ～？！そりゃあ大変だ(ﾟoﾟ)
うちも自分でではないけど、よく新車の時にきずつけられましたよ（社宅に住んでたせいか？）修理代のことを考えるとますますへコみますよね～おだいじにしてください！(~_~;)

＞たまさま（８６７８）
毎日の水仕事で手荒れが・・・なんて、んもう～！若奥さん♪
車に彫られたっていうのは文字だったのですね！チン○なんて、もう乗れないじゃない！？ひどーい☆
余談ですが高校時代の美術の先生で「鎌田」っていう結構年配の先生がいたんですが、よく表札に「お」を付け足されて困っているって言ってたのを思い出しました（笑）
なにやら「パンダ＋あざらし」のＣＭがあるようですが、私も見たことないです！ちなみに子Ｐ～いわく、これは「パンダとあざらしのジョグレス進化だ！」と言ってます(*_*)

＞里奈さま（８６８１）
しゃぼんさまが書いてらっしゃるようにチンチンポテトのＣＭはＣＨの放送中に流れてたんですよ。リアルタイムでだったかどうかまでは思い出せませんが(~_~;)あと、このポテトわりと最近も見かけましたよ♪もしかしてまだ売ってるかもデス☆
「パンダ＋あざらし」のＣＭのこと、以前チャットで聞いたことがあるんですが残念ながらまだ見たことありません(^^ゞ地域限定なのかなぁ～！でもそんな組み合わせを思いつく人っていったいどんな人なんでしょうか！？なぜ柿の種とピーナッツがパンダとあざらしに？どっちが柿の種でどっちがピーナッツなのぉ～？！(>_<)

＞いちのさま（８６８３）
いちのさまのイラスト、ぜひ見せていただきたいです♪絵を書くのも見るのも大好きな子パンダも「欲しい☆」と言っていますので送っていただけるとうれしいなぁ♪

＞ＯＲＥＯさま（８６８４）
せっかく書いたカキコが消えてしまうとは・・・！私も何度か泣きました(T_T)ホント、もう二度と同じことは書けないよ～！って力尽きちゃいますよ。
ブラックベリーの紅茶？！それはやはりフルーティな香りがするのかな？私はコーヒーよりも紅茶党なので興味あるなぁ～♪でもいつも買うのはお買い得の品で、ぜんぜんこだわりはないんですけどネ☆この「紅茶」のティーバックを取っておいて目が疲れた時にまぶたにのせるといいって友人が言ってましたが、たくさん残ったハーブティの使い道も聞いときゃよかったよ～(~_~;)

＞しゃぼんさま（８６９０）
ＸＹＺ・ＢＯＸに依頼されに行かれるんですか～？！
無事依頼することができたら、しゃぼんさまも「勇者の会」にめでたく入会ですね（笑）依頼カードがちゃんと設置されてるように私もお祈りしています！がんばってきてくださいね(^o^)丿

それでは、おやすみなさい(-_-)zzz

### [8693] TAKESHI	2002.11.13 WED 23:16:01　
	
＞[8692]里奈様
ええ、すごく感激しました！！ケンシロウ対ガンダムはまだ見てません。それは早く見ねば！人間とロボット、いったいどうやって戦うのだろう（笑）

### [8692] 里奈	2002.11.13 WED 22:25:58　
	
☆８６８９･８６９１　ＴＡＫＥＳＨＩ＆マイコ様
将人様からのプレゼント、最高に感激させられましたよね！
ケンシロウＶＳガンダム（あれガンダム？ロボット系うといからわかんない…）見ました？里奈なんか笑っちゃいました（笑）ヒーローＶＳヒーロー、どっちが勝つの！？ってハラハラしちゃったわ！

☆８６９０　しゃぼん様
あぁっ！そうか！ＣＨがリアルタイムで放送されてた頃に｢チンチンポテト｣のＣＭやってたんですよね！だから印象強いんだわ！

### [8691] マイコ	2002.11.13 WED 20:15:28　
	
＞「８６８２」将人（元・魔神）様
うっわぁ～！！うれしいです！！ほんとにありがとうございます！！あぁ～ほんとなんと言えばいいか・・・。

### [8690] しゃぼん	2002.11.13 WED 20:03:10　
	
こんばんにゃ～。「僕のポテトはちんチンチーン♪」のなつかCM（殴）私もリアルタイム放送時のCHのビデオテープに入ってたりします。えへへ。
いよいよ今週XYZ BOXへ依頼しにいきます。依頼内容も決めました。ふふ。ここ雪降ってるんで、飛行機飛ぶか心配～。さむっ！シャワーかかっただけじゃ暖まりません。（使ってみました／笑）

＞千葉のOBA3さま
　葵さま　
お久しぶりです。レスありがとうございます。お二人に言われるとぜったーい依頼用紙ある！って強気になれましたVV無かったら通い続けてやるう～（ストーカー？）

### [8689] TAKESHI	2002.11.13 WED 18:09:49　
	
＞[8682]将人様
Ａ・Ｈのドラマ聴きました！いい！すごくいい！阿香の声って高山みなみさんですよね？なかなかいい感じでした。皆さん、全然声が変わってなかったです。さすが声優だなーって思いました。将人さんには本当に感謝してます！ありがとうございます！僕も里奈さんと同じく「もっこりパワー充電！」のやつ気に入ってます。神谷さんて本当にいい声してますね！

＞[8683]いちの様
かなり奇抜なアイデアですね！僕も見てみたいです。今年の冬でも東京行けないかなー。いちのさんが書いたイラストぜひ見てみたいです！あ、そういえば、いちのさんはゲームとかするんでしたっけ？

### [8688] 里奈	2002.11.13 WED 17:28:32　
	
☆８６８７　たま様
あれぇ～？関西限定ＣＭなのなかぁ～？
かなり面白い映像なんで是非見てもらいたいのに！！
去年までの年賀状、見せてあげれればイイんですけど、里奈スキャナーとか持ってないのよね…（汗）どうすれば…？
デジカメって手があるんだけど、里奈のパソコンが古過ぎて最新のデジカメについて行けてないの…（悲）

☆８６８６　Ａｙｕ様
えぇぇっ！？
ストレートパーマ禁止！？ストレートがダメだなんて理解できないわ！まっすぐなら問題ないじゃん！なんで？そもそもパーマじたいがダメってこと！？厳しいってゆうか…ホント理解できん

☆８６８５　千葉のＯＢＡ３様
そうそう、そうなのよ！英語は世界の共通語らしいから、むこうの方は平気でベラベラと横文字並べてくるし、日本人も一応それに必死に対応しようとするんですよね。でも日本人ほど英語の上達しない国はないんじゃないかってホント思う。だって、本屋行ったらあんなに英語の教材積み重なってブワァーって並んでるのに…買ってるのってせいぜい受験生ぐらいのもんじゃないの？里奈が世間知らずなだけかしらん？

☆８６８４　ＯＲＥＯ様
そうか！年賀状って海外じゃないんですよね！
でも似たような物とか無いんですか？せめて電話かけあうとか、家に集まるとか…それぐらい？
もうクリスマスの準備していらっしゃるようで…
いいなぁ～、里奈も海外でクリスマス過ごしてぇー！！

☆８６８３　いちの様
なぁ～んだ☆ア○ムの間違いだったんですね！
どこでもドアはまだまだ先か…ケッ…（笑）

☆８６８２　将人（元･魔人）様
きゃぁぁぁーー！！
将人さん！届きましたよ！ＣＤ-Ｒ！
なんて素晴らしいの！？ビックリしちゃいました！！
まさかＣ･Ｈ関連ばっか７枚組みだなんて…！
しぃ～かぁ～もぉ～！！Ａ･Ｈの音声ドラマ入ってるじゃん！！
　　（◎_◎；）
原作と同じ流れで同じセリフだから、本見ながら聞かせて頂きました♪もうホント感謝感激ですぅー♪♪今日は一日中コレ聞いて過ごしてます（ﾉ^ヮ^)ﾉ
神谷明さんのパソコン用音声ＷＡＶデータも感激☆
リョウの声で｢もっこりパワー充電！｣みたいなこと言うやつ、あれお気に入り（笑）
ってゆうか…ＣＤ-Ｒを７枚も使ってくれて、しかもＣＤケースまで付けて送ってくださるなんて…どこまで親切なの！？いくら安かったとはいえ、これは里奈がしたことの｢お礼｣だなんて、｢お礼｣を遥かに超えてますわよ！里奈ママも嬉しそうに聞いてます。なにからなにまで…あぁ…ありごとうぅぅぅ…（涙）

### [8687] たま	2002.11.13 WED 16:37:16　
	
やっと買えました。ＡＨ５巻♪
やっぱ、リョウと香が見詰め合うシーンは最高涙です♪
あぁっー！
「阿香の夢の中の話」が途中で終わってる・・・。うそ～ん。
この話はすべて一緒に読みたかった・・・。６巻まで待ち遠しやぁ～。
この後がイイのにーーっ(>_<)

＞里奈さま　【8681】
わははっ♪わははっ♪
顔がパンダ＆体がアザラシ＆声がおっさん？！？
いやぁ～ん。見たぁ～い♪関西限定ＣＭなのでしょうか？
カキＰーめぇ。うちらにマージンよこしやがれぃ（笑）
年賀状・・・。もうそんな時期？
エアーブラシ使って・・・ってスゴォ～イ！これまた見たぁ～い♪
メールで添付とかって出来ないの？見たいよ！見たいよ！見たいよぉーっ！

＞将人（元・魔神）様【8682】
「阿の意味」ＡＨ５巻読む前のまだ意味を知らないうちに書いたものでした。ＡＨ５巻の「読書後の行動のご注意」に書いてありましたね。（汗）
中国語読みだと二人とも阿香・・・。（どうするんだぁーーーっ？）

＞いちの様　【8683】
車の落書きは正確にはチン○（完全に下なのであえて書かなかったのよ）
いちの様のカラー絵見たいなぁ。
やっぱ、あれだけ絵を上手に書くには子供の時から書かなきゃダメねっ♪

＞OREO様　【8684】
たまの日はたま意外の者は平日扱いです。働け！稼げ！シモベども！
たま独裁政権？！（シャレになってる？汗）

＞千葉のOBA3様　【8685】
ペンタブじゃなかったかなぁ～（焦り）
ちちの様。あ！間違えた「いちの様」が最近買ったんじゃなかったけ？
ＰＣで絵を描くとかで・・・（？）
よく分からないのに書きました。ペンタブの事。んま適当っという事で。
人生適当！高田純○くらい適当な人生を歩んでおります。

### [8686] Ａｙｕ	2002.11.13 WED 16:10:12　
	
将人様、里奈様
こんにちは。合同コントはだいぶ（？）まとまってきましたね。私の案もまた思いついたらメールをします。将人様はＣＳ放送に加入していませんか？「さらばハードボイルドシティ」の放送はいつ頃でしょうね。学校の校則で「ストレートパーマ＆縮毛矯正禁止」というのは皆さんどう思いますか？またＣＨキャラ先生達だったらこの校則をどう捕らえるでしょうね？？

### [8685] 千葉のOBA3	2002.11.13 WED 12:58:26　
	
こんにちはー！今日は水曜日、明日はもうバン木なのね。いそがしいせいか、一週間たつのが早い！！

８６７６　ｓｏｐｈｉａさま　そう・・・焚き火にしようか、レンジでチンにしようか迷ったんですけど。取れたスイッチを無理矢理押し込んで（？）夕べは使うことができました。メーカーにたのんでスイッチとりよせることにしたんですが、「これなら新しいのいらないじゃん！」と、少し後悔しました。

８６７８　たまさま　ペンタブ？なにそれはーーーー！！えーい、パソコン関連（かどうかもわからないが、）で私に何かこむずかしいことを言うんじゃなーーーい！！ペンダコならあるぞーーー！！・・・・いけない・・・。ますます「たま化」してきている。自分がコワイ。

８６８１　里奈さま　恐怖の怪力女は、とれたスイッチを、また力でねじこみましたわ。なにー！？年賀状書くのにエアブラシまで使うの？すごーい！見た－い！里奈さま凝り性なのね。しかし、確かにお仕事京都あたりだと、外人さんと、接する機会も多くて英語は必要になってくるでしょうね。外国の人は平気で英語で話しかけてくるけど、日本人は外国人に平気で日本語で話しかけるってしないじゃないですか。やっぱ、お国柄の違いなんですかねー？？？

８６８２　将人さま　うーーー、私が改造人間ということがバレてしまったわね。・・・しかしメーカーに頼んだスイッチの値段が４００円。送料が９００円。なんなのよー！！

８６８３　いちのさま　広告に書かれてある字はうまくても、私はうまくないのよーん！いちのさまの成長を、心から楽しみにしてますわ。（このオバハンに、これ以降のノビは期待できないのよ。）

８６８４　ＯＲＥＯさま　そうでしょー！いちのさま上手でしょー！うちのダンナがイタめしダメなのは、年齢的なものもあるかも。このおばさんの私より、ずっと年上だから。レンジでチン生活には、ならなくて済みそうです。しかし、昨日はレンジより、なぜかホットプレートを、真っ先に思い浮かべました。結構便利ですよね。

それでは、もうひと仕事してきまーす！！


### [8684] OREO	2002.11.13 WED 05:07:57　
	
こんにちは！実は昨日カキコ書いたのですが、ほぼ終わりそうなときに全部消してしまって・・・その時間が深夜３時。力尽きました☆

＞８６６８里奈様
まずいっすよ！暴言英語は・・・ってシチュエーション的にあってればいいですけどね☆でも「殺人系」そのまま「日常」に使ったら確かにコワイかも・・・簡単な英語が無意識で出てくるのはスゴイですよ！頑張って勉強続けてください！年賀状懐かしいなぁ・・かれこれ２年ご無沙汰です☆私は今、クリスマスカードで忙しいです～☆

＞８６６９いちの様
イラストゲットしました～！！正直言って、スッゴクキレ～～～！やっぱ、「アシ」応募すべきでしょ（笑）・・・あ、ほんとだ、フッと見たら、「チン」旋風が・・・

＞８６７２無言のパンダ様
菊茶は、タダ甘いだけの紅茶でした。インスタントの粉状のやつで、全然菊の香はどこへやら～ま、変にマズイよりか全然いいですが・・・ハーブティーかどうか良く分かりませんがこちらでブラックベリーとかのスッゴイおいしい紅茶を発見して、母にあげたら大喜びでした♪
ビーフジャーキーはありますね。でも、冷蔵して売ってるスモークサーモンを「干物」と呼んでいいのだろうか・・・堅い、干物っぽいのも「日本人向けお土産や」に行ったら売ってるようですが、ハムみたいな生のが主流なんですよね。

＞８６７３将人様
ストーリー長くなりそうなカンジですね！じゃ、「続きは明日までのお楽しみ！」みたいに、２回、３回に分けたらいかがでしょう？？

＞８６７５千葉のＯＢＡ３様
コンロのつまみ、壊しちゃったんですか！？大変だぁ！これはレンジで「チン」生活行くしかないでしょう（笑）旦那様イタメシダメなんですか？もったいない・・・おいしいものイッパイあるのに・・・あ、ウチの父もダメだったりしますでもスパゲティとか彼を無視して作ります！（笑）

＞８６７６sophia様
車ヘコませちゃったんですか！？それはイタイ！！(*x*)一度直してるとこ見たことあるのですが、あのトイレ詰まったときにに使う吸盤みたいなのの、でっかいので、ひたすら外に戻すようにしてました。何回かやってると突然「ボンッ」って言ってほぼもとに戻っていたのに、感動しました☆

＞８６７８たま様　
１１月１１日は「たま」の日！？（笑）これって祝日ですか？（これ重要です）
「マトリックス」の時点で何の疑問も抱かず、納得してました☆そうかぁ～あれは「アトリックス」だったのねぇ～☆ちょっと感動ものでした☆

では、学校いってきま～っす！(^-^)/~~

### [8683] いちの	2002.11.13 WED 00:34:34　
	
どうも、こんばんは。最近無駄に夜型の人間になりつつある“いちの”です。早く寝ろよ（汗）。明日も早いんだぞ・・・。

＞[8674]＆[8682] 将人（元・魔神）様
私など水彩と油絵くらいしか触れたことがないので、他の画材を言われてもピンっときません（汗）。
小さい時から絵を描くのが好きでして、いつも落書きばかりしていました。そのお陰か、今、美術系の大学に籍をおいております。好きこそものの上手なれってやつですかね（笑）。
ドラ○もんの件は、おっしゃるとおり、アト●の間違いでした・・・。２００２年は科学技術も発展し、ア○ボや先●者などの二足歩行型ロボット開発され、本当にア●ムが誕生してもおかしくない世の中になりました。ドラえ○んが完成する頃には、どんな世界になっているのでしょうかね？

＞[8675] 千葉のOBA3様
広告に書かれている数字ってとても上手くて、そして個性的で好きですよ。普通に書く数字とはちょっと違いますよね。丸みを帯びた感じと申しましょうか。やわらかい感じが好きです。
私はこれからＰＣでの色塗りを会得して、１日でも早く自分の絵を見つけたいです。

＞[8676] sophia様
了解～。これから送りますね～。
他にいませんか？私のイラスト欲しい方～？今のうちですよ～。今ならなんと、２００円（税込み）！！←金取るんかい！？　そんなにたいそうなものでは御座いません（汗）。

＞[8678] たま様　アザラシ型ロボット？
何故に「チン」と彫られていたのやら（汗）。彫った奴の気持ちが知りたい（笑）。そいつは相当病んでるね☆「チン」伝染病ではなく心が病んでるよ。
または、陳って名前の人が書いたのかもね（笑）。

＞[8680] TAKESHI様
私も以前行った時は、まだ改装中だったので完成後の店内は見てないんです。ブラウン管を壁に使用した奇抜なアイディアをこの目で見てみたい！！デザイナー曰く、「このフロアーに新宿を表現してみた」だそうです。
ゴミゴミした感じをだしたかったとか。見てぇ～！！！

＞[8681] 里奈様
ごめんなさい。２００２年は鉄腕ア●ムでした（汗）。将人（元・魔神）様のご忠告通りです。ドラえ●んはまだまだ先です。よってどこでもドアも先でしょう・・・。ジャパ○ット危うしです（笑）。

### [8682] 将人（元・魔神）	2002.11.12 TUE 23:44:44　
	
こんばんは、将人（元・魔神）です。

＞[8675] 千葉のOBA3様
ガスコンロのスィッチが、取れてしまったんですか？
まさか改造人間？
いや、すみません。仮＠ラ＠ダーで改造された直後に、力が分から
なくて水道の蛇口を壊すシーンを思い出してしまったので(汗)(^^;

僕の家のガスコンロも壊れています。2つある内の1つが火が
つかないようになってます。ガス屋さんに来て貰ったら、
「古い機種なので部品交換したら1万円以上かかるんですが、
新しいの3万円ぐらいからであるんで、どうしますか？」
って言われて、母はどうするか悩んでいたみたいですが・・・。

出会い編1つだった方が良かったですよね。
5巻は、香瑩の新米CH編だけでも、まとまっていても良かったような
気もしてましたけど。

＞[8676] sophia様
愛車に凹みですか？大切にされていた車だったら、どうしても気に
なってしまうんですよね。
僕の車は、家の駐車場に入れる時に、ブロック塀にぶつかって
出来たスリ傷だらけです。

AHは、今までシリアスだったのが、裏表紙かわいいギャグの絵に
なってましたね。5巻の話も今まで比べると明るい方向になって
きてますしね。

＞[8678] たま様
＞手荒れにはマトリクス・・・
「マ」と「ア」で全然違う物になってしまうんですね(笑)

「阿香瑩」と「阿瑩」って、ただ名前を並べようとしたんですが
頭に「バカ殿＠ま」の霊が降りてきて（まだ生きてるって）
つい書いてしまったんです。
「阿香」の「阿」は、「シャンちゃん」という意味で使われている
みたいですね。

＞[8679] Ａｙｕ様
パソコンからコントのメールも、携帯からのメールも、ちゃんと
届きましたよ。
オリジナルキャラの問題生徒ですか？いっぱいいますね。
ちゃんと覚えて、使いこなせるだろうか？
　→　そろそろ記憶力に自信が無くなってきた年なんで(汗)(^^;

＞[8680] TAKESHIさま
「北条司イラストレーションズ(1)」は、無事にそちらに
届きましたか。安心しました。良かった。
エンジェルハートとか蒼天の拳の第1話のドラマ楽しんで下さいね。

＞[8681] 里奈さま
もう年賀状の時期なんですね。
里奈さんは、全部手書きで、作られるんですか？すごいですね。

今までは「プントごっご」で、去年は妹がプリンターを買ったので
それを予備のインクを買ったので、使わせて貰っていました。
今年は、どうしようかな？

＞「体はアザラシなんだけど顔がパンダ！で声はオッサン」
そんなCMがあるんですか？　一度そのCMを見てみたいですね。

あの仮病をして並ぶシーンって「RASH!」でしたっけ？
香のソックリさんだから、混乱してました。

＞[8681] 里奈さま、いちの様
2002年生まれは、たぶん「鉄＠アト＠」だったと思いますよ。
TVで、ホンダのア＠モとかソニーとかの出る、展示会の案内を
やっている時に、言ってました。
で、ド＠えもんは、2112年の22世紀生まれだったはずです。

「どこ＠もドア」売ってるのなら僕も欲しいな。
どうせなら四次元ポケットごと全部の秘密兵器付きで・・・。

### [8681] 里奈	2002.11.12 TUE 21:22:44　
	
もうコンビニには年賀状が並んでますねぇ～。
毎年早くから見てるのに、書き出すのはクリスマスで友達とさんざん遊びあるいて｢はぁ～！今年も遊び収めは完璧だわ♪｣っと落ち着いてから…。焦って描くんだけど、コピーとか出来上がったものが嫌いなもんで全部手書きで凝ったの描くから毎年７～８枚しか出せないんだよね。でも本気で徹夜してエアーブラシとか使って描いてるんだよ、こっちは！今年は早めに描こう…。って、毎年思ってるんだけどね。なかなか舞い降りてこないのよ。
※ちなみに里奈の年賀状のテーマは毎年、美女の裸体です（笑）
　北条先生の絵も何度かマネさせていただいたわ♪

☆８６７９　Ａｙｕ様
オリジナルキャラたくさんいますね！
どう使うか悩むわぁ～＠しかもろくなヤツいないし…（笑）
ストーリーとかはココじゃまだ言えないけど、少しづつ進んで来ましたねぇ。里奈はまだ口はさむ程度しか参加してないけど（汗）でも面白いのができそうで楽しみだわ♪

☆８６７８　たま様
そうそう、手荒れにはマトリックス…って、おぉ～い！
里奈、たま様の乗り突っ込みを読み切る前にいろいろ想像してパニクりましたよ！（笑）たま様宅ではどんな激しい手荒れ予防が行われているのか真剣に考えてしまいました。
無言のパンダ様とたま様の合併を目撃！（ドキュン！）
今｢カキピー｣のＣＭでパンダとアザラシが混ざった生き物の映像が里奈の目に飛び込んで来ました！体はアザラシなんだけど顔がパンダ！黙ってれば可愛いかもしんないけど、オッサン声なんだなコレが…。

☆８６７７　将人（元･魔人）様
お礼だなんてそんな！
里奈のほうがいろいろしてもらってるからお礼したのに、まさかお礼返しされるとは！！しかもなにやらＣＤ-Ｒが付いて来るらしいですね！？いったい中は何が…！？楽しみです♪
ほんといっつもありがとうございまする☆
男子生徒が仮病を使って保健室にズラ～っと並ぶシーン…
｢ＲＡＳＨ！！｣ですね！？
確かに、勇希って香そっくりだからゴチャマゼになるのわかりますわ♪今読むとＡＨの看護婦だった頃の香とダブるわぁ～

☆８６７５　千葉のＯＢＡ３様
でた！里奈に負けずと台所で物を破壊する怪力女が！（笑）
かなり分厚い皿をいとも簡単にまっぷたつに割った里奈より、寝起きのサワヤカモーニングにガスコンロのスイッチを握り潰す（あ、違う…笑）あなた様のほうが怪力かも！
いやいや、今の女は力あって当たり前！恥ずかしいことじゃございませんわよ♪香ちゃんだって１００t振り回してるんだから！それくらいできないと男を尻にひくなんてできないわ！
英語、里奈も基本的に好きではなかったですね。高校の時、テストがあるから勉強してただけで、学校出たら関係ないって思ってましたから。ところがどっこい！大学では｢基礎ができてて当たり前｣な扱いを受けた！で、中国語＆フランス語に逃げた！でも働き出したら思い知らされました…。里奈京都駅近くで働いてたから、外人さん毎日見るのよね。しかも話しかけられる！しかも｢これでもか！｣ってぐらいの英語オンリーで質問責め！ナンパまでしてきやがる！英語なんて必要ないって思ってたけど、お客さんとして来られる方も多かったんで強制的に接客用英語マニュアルとか渡されました（泣）でも自分流を探せば面白いかも…って気付いて、とりあえず香取慎吾の番組にハマりましたね（笑）今でもまだ話せません！（なんだそりゃ…）でもいつかは…ね。

☆８６７２　無言のパンダ様
おぉぉーー！
｢チンチンポテト｣についてそこまで覚えておられるとは！
しかもチンするシェイクまで！（笑）
でも、録画したビデオに…って、その頃テレビで何やってましたっけ？

☆８６６９　いちの様
ジャパ○ットタカタでもまだ扱えないのね…
ドラ○もんって２００２年製なの！？だったら｢どこでもドア｣ももうすぐ発売開始！？整理券持って並ばなきゃ！どこ！？どこに並ぶの！？先着何名様！？
