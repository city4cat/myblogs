https://web.archive.org/web/20031021102914/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=359

### [7180] 将人（元・魔神）	2002.07.25 THU 01:52:47　
	
「午後のこ～だ」のライン入力を使う方法
その2　
（すみません。長すぎて1つには入りませんでした）

8.パソコンの画面（デスクトップ）に「午後のこーだ」の
　アイコン（絵文字）がでますので、ダブルクリックして
　ソフトを起動させます。

9.「現在の設定を維持する」で、「設定する」をクリックします。

10.「ライン入力　ＭＰ3録音」をクリックします。

11.試しに、ラジ＠の再放送を流して下さい。
　　もし、画面の左上の白い部分に声に反応して
　　波形状の「波」のゆれがあれば、録音できる状態です

12.もし出来なければ、以下の設定をして下さい。

12-8.これで、Ｗａｖｅを選択できるようになりますよ。

12-9.　先程の「午後のこ～だ」の、画面の左上の白い
　　　部分に声に反応して波形状の「波」のゆれが
　　　あれば、録音できる状態です。

12-10.これでパソコンが自分で鳴らした音声を、録音できる
　　　体制になりました。

13.これでパソコン内の音声を取り込む準備ができました。

13-01.「出力先」の項目の「参照」をクリックして、録音した
　　　MP3音楽データの保存先を指定します。

13-02.再度、ラジ＠の再放送が聞けるようにして、試しに
　　　ほんのちょっと数十秒ほど録音してみます。
　　　「録音開始」を押して下さい。

13-05.できあがったMP3の音楽ファイルをダブルクリックして
　　　録音できているか確認して下さい。

13-06.この後、ラジ＠の放送時間分（神谷明リターンズなら30分）
　　　録音してみます。
　　　とりあえすエラーとかで止まらなければ、これは、聞き直さ
　　　なくていいですよ。
　　　（テストのＭＰ3形式の音楽ファイルを消して下さい）

14.これで、一通り、「午後のこ～だ」で、ＭＰ3の音楽ファイルで
　　の録音できる準備が出来ましたので、後は、本番の放送を
　　聴くときに、録音をして下さい。

15.最近のほとんどのＣＤ-Ｒを作る（焼く）為のライティングソフト
　なら、ＭＰ3形式の音楽ファイルを、音楽ＣＤ用に変換できる
　機能がありますので、ＣＤ-Ｒにして、保管する事も出来ますよ。

※みなさん、文章が長くなってしまってすみません。m(..)m

### [7179] 将人（元・魔神）	2002.07.25 THU 01:51:08　
	
「午後のこ～だ」のライン入力を使う方法
その1　
（すみません。長すぎて1つには入りませんでした）

＞[7168] kinshiさま

＞こんにちは、今、将人様の書かれた通り、ＣＤの書き
＞込みＴＹＲしたのですが、サウンドレコーダーの
＞録音が、６０秒までしか出来ず、その理由をメーカー
＞にＴＥＬした所、著作権の関係上らしいです。
＞細かく分けながらもいいけど
＞やはり、いっきにテープのほうが、いいみたい・・・・。
＞それ用のソフトも売っているようですが・・・・。
＞ちなみに私のＰＣは、Ｗｉｎ－ＭＥです。

kinshiさま、こんばんは将人(元・魔神)です。
Windows Meのサウンドレコーダは、60秒までなんですね。
調べてないのですが、Windows 9x系の1つのデータ
ファイルの容量の限界が2GBまでってのが影響してるかも（汗）(^^;


掲示板のこの書き込みを見てから、何か良い方法がないか
1日中、考えてみました。

それで、思いついた方法がありました。
Windows版に限りますが、インターネット上で公開されて
いるオンラインソフトでお金が掛からないフリーソフトで

「午後のこ～だ」というMP3音楽ファイルを作るのに
有名なソフトがあるのですが、そのソフトに

「ライン入力からMP3に録音」という機能があるので
それで試したところ、録音できそうなので、

たぶんWindows 9x系や、Meでも使えるはずですので
とりあえず、その方法を書きますので試して下さい。

※みなさん、長くなってしまって、すみません。


1.「午後のこ～だ」または「午後のこーだ」でYaHo@とかの
　ホームページ検索サイトで、探してみてください。

2.「MARINCAT マリンキャット」というタイトルのHPに
　行く事ができると思います。

3.「更新履歴」の2002/04/11のところに（2002/07/24時点）
　「午後のこ～だ for Windows Ver.3.10β5」という文章が
　　ありますので、これをクリックします。

4.「午後のこ～だ」のダウンロードというところに
　　「β3」「β4」「β5」とあるので、最新版の「β5」を
　　ダウンロードします。
　　「午後のこ～だ for Windows Ver3.10β5 のダウン
　　　ロード (3.49MB)」の項目の「サイト1～3」の
　　どれかをクリックして下さい。

5.「次のファイルをダウンロードします」と表示されると
　「保存」を押して、このデータを保存する場所を指定
　します。
　（※私の場合は、とりあえず「ダウンロード」という
　　フォルダーを作って保存しています。）

6.「wing310beta5.lzh」というLHA形式で圧縮された
　データが保存できたと思います。

7.　LHA形式で圧縮されたファイルを元に戻す（解凍する）
　ソフトを使用します。

7-1.「Vecter」や「窓の社」というようなオンラインソフトを
　扱っているＨＰの「ユーティリティ」の項目で解凍ソフト
　が色々とありますし、まだパソコンの入門雑誌などの
　おまけのＣＤ-ＲＯＭなどにも、付いている事があります。

7-2.私が使用しているのは「LHAユーティリティ」というソフト
　　ですが、ちょっと「DLL」という物を準備しないといけない
　　分かりにくい物ですので、今「Vecter」というところの
　　ランキングで人気のある「Lhasa32 （らさ）」とか
　　「Lhaca」とか「+Lhaca」とかのソフトが使いやすいかと思います。

7-3.他のソフト圧縮・解凍ソフトの使い方は、私が知らないので
　　自分が使っている「LHAユーティリティ」の場合で説明します。

7-4.「LHAユーティリティ」の場合は、圧縮されたLHA形式の
　　ファイルを、ダブルクリック（マウスボタンを2回押す）
　　するか、

　　「LHAユーティリティ」の操作画面に、そのファイルを
　　ドラックアンドドロップ
　　（マウスでアイコンの絵文字のところをマウスボタンで
　　　押しながら、操作画面に重なるように移動して
　　マウスのボタンを話します。）

　　どちらかの方法で「LHAユーティリティ」の画面に
　　先程の「午後のこ～だ」の「wing310beta5.lzh」の
　　ファイルに圧縮されているデータの一覧が表示されます

7-5.「編集」→「全てを選択」をクリックします。

7-6.「編集」→「解凍」をクリックします。
　　そうすると、解凍したデータをどこに置くか
　　確認してきます。

　　私の場合は、オンラインソフトなどは「ツール」という
　　フォルダーにいれて、用途別にフォルダーを作って
　　分類しています。

　「午後のこ～だ」の場合は、
　「ツール」→「MP3作成soft」→「午後のこーだ」に
　　先程の解凍したファイルの置き場所に指定してます。

7-7.できあがったファィルの中に「Setup.exe」という
　　ファイルがありますので、これをダブルクリック
　　（マウスボタンを2回押す）して、インストール
　　します。

7-8.「次へ」を二回押して、「デスクトップ」に
　　ショートカットを作るにチェックを入れて
　　また「次へ」を繰り返すると「午後のこ～だ」が
　　パソコンにインストールされました。

※みなさん、文章が長くなってしまってすみません。m(..)m
　（まだ、続きがあります）

### [7178] ジェイド	2002.07.25 THU 00:02:01　
	
初めてなのにこんなにすぐレスをいただいて嬉しいです！
＜いちのさま(7173）
筋肉○ンにジェイドっていうのが出てくるんですか？
そうなんだ、全然知りませんでした！
ハンドルネームは、エアロスミスというバンドの「ジェイド」という曲からとりました！（エアロファンなもので…）
これからもヨロシクお願いしまーす♪

＜おっさんさま(7176）
私の、一人で勝手にアツクなっている書き込みにお返事くださって、ありがとうございました！
そうですよね。シャンインちゃんとの出会いでどうなるなか、
ですよね。ああ、目が離せない！！

それにしても「香はじつは生きていた！」というふうに
ならないものですかね～～。
かおりーーーーーー！！！！！　ううっ(涙）


### [7177] たま	2002.07.24 WED 23:50:33　
	
みなさま、こんばんわ。　たまです。

あぁぁぁ～～～！！本当だ！！！
吉祥寺ブックスルーエで、北条センセーの原画展開催ぃ？！
しかも、携帯ストラップが『先着２５０名ご来店いただいたお客様のみのサービス』って書いてありますぅ！
その他のプレゼント商品は、本購入者のみ抽選ですかぁ。(あっ、ネット販売もしてるぅ)
でも、ＡＨ全部持ってるしなぁ。コミックバンチ１周年号って、何で今頃販売してるんだろぉ？

行きたい！行きたい！原画展！……でも行けないっ！遠すぎる！
うらやましいぞぉ～！東京都！

＞kinshi様［７１６８］こんばんわ。そして、実は初めまして★なんです。
私も、録音が６０秒しか出来なくって、メーカーに問い合わせしようかなぁっと思ってた所だったんです。そっかぁ、６０秒以上は出来ないのかぁ。
では、ラジ＠は３０分番組だから、３０回録音ボタンを押さないといけないのですね。ハハハッこりゃしんどい(^_^;)
それと、コンビニは前日の夜中に「本」到着するので、朝８時はばっちりバンチ置いてありますよ。
それから、kinshi様も「かかる同盟」加入ですねっ！よしよし！

＞無言のパンダ様
＞ＯＲＥＯ様
＞葵様
こんばんわ。
２６日１１時３０分まで、まだ時間があります。それまでに、何とかＣＤ－Ｒ書き込み出来るとイイですね。
『まろ様』と『将人様』の説明文のとおり進めていったらいつも間にか出来てますよ！(きゃはっ☆人まかせぇ～)

＞おっさん様[７１７６]はじめまして。
草刈様も渋いけど、「おっさん」様もかなり渋い・・・。
いや、渋いというより、謎？！

では、わたくしも皆さまのように、最後の締めはコレ！
　　　　↓↓↓
「今からシャワーかかってこよっと！」

### [7176] おっさん（岐阜県：岐阜も・・・）	2002.07.24 WED 23:23:20　
	
毎度～。岐阜も今日は暑かったじょ～最高気温３６度（＾＾；）へたばってます。つ～ことでおっは～っす。
ちゅ～ことでレスです。
しゃぼん様（７１６７）、初めまして。北海道っすか？夏は涼しいいでしょうか？岐阜は夏は地獄です。真夏日連続してますから・・・（ＴＴ）最低気温も熱帯夜（今夜は２６度・・・）私もシャワーにかかります。
草刈正雄様（７１６３）、初めまして。渋すぎる・・・私もそうおもいました。１巻～３巻いや４巻までしっかり読んで見て下さいね。
ジェイド様（７１７０）、初めまして。ジェイド様が書いて見えること何か共感できるものがある気がします。りょうの心の成長あるべきことだとおもいます。私では役に立てない文章になるかもしれませんが、りょうと香は互いにかけがえのない二人だし失いどれだけ深い傷を得たか、そして傷を癒すことができるのか香瑩と出会いでどうなるか？そんな気がします。すみません変なこと書いてしまって・・・。

ということで本日はこの辺で・・・？？？？？

### [7175] 葵	2002.07.24 WED 21:34:12　
	
　暑いよぉ～･･･。少しは涼しくなっても、夏は夏！みなさん、シャワーにかかりましょうね。（^_^;

＞たまさま［７１６４］
　ＣＤ－Ｒ書き込みできたんですね。おめでとうございます。私もと思い、接続コードまで買ったのに…ソケット（というのか？デッキのコードを受けるところ）があわない～っ！（泣）確か家のどこかにあったような…。明日までですもんね。がんばって探します！！あ、私は関東人ですが、きちんとした標準語を話してるかどうかは…かなり怪しいです☆

＞しゃぼんさま［７１６７］
　北海道ですか？いやぁ～…「シャワーかかる」、すごい広範囲に広まりそうですね♪

### [7174] ハル	2002.07.24 WED 21:18:30　
	
今日も関西はメチャメチャ暑かった！！
ってなわけで久しぶりにＰＣのある所に来てカキコします（＾＾）
＞７１５５sophia様！
レス遅れてすいません！！ビデオありましたか！！
画像が乱れてようと、関係ありません！！ホンマあつかましいですがダビングお願いします！！直接メール送ろうとしたんですが、エラーが発生して出来なかったので書き込みしました（；０；）どないしたらいいんでしょうか？これに登録しているメールアドレスは携帯のやつなんですけど・・。どないしたらいいのでしょうか？

### [7173] いちの	2002.07.24 WED 21:14:14　
	
っみなさま、こんばんは。
やっとこさ今日でテストが終了いたしました・・・。出来の方は・・・おいおい、そりゃ言いっこなしですぜ、ダンナ。
てな感じで、夏休みまであと集中講義を残すのみとなりました！これまた２９、３０、３１日と３日連続の上１日６時間という無謀かつ理不尽な設定となっております（汗）。
・・・今年の夏は長くなりそうです・・・。

＞[7161] OREO様
＞[7164] たま様
“ｔ（トン）ハンマー”気に入っていただけたようで、なんかちょっと恥かしいですが、静かなブームを起こしてくれれば幸いです。

＞[7163] 草刈正雄様
これまた渋いハンドルネームですねえ（汗）。個人的に嫌いじゃありませんが（笑）。
ＡＨ１～３巻是非読んでみてくださいね。

＞[7170] ジェイド様
ん！？どこかで聞いたことある名前ですね・・・あ！！「筋肉●ン２世」！！！そーいや今日やってた！！そうだ。
関係なかったらごめんなさいｍ（＿ ＿;）ｍ


今日は前日に比べると暑くなかった気がしますが、それでも汗はとまりませんね・・・。（！）こりゃーシャワーか？「かかる」か？（笑）。

### [7172] 玉井真矢	2002.07.24 WED 20:56:56　
	
今見てきたら応募券は１冊でもらえるみたいですね！
でもエンジェル・ハートの単行本は全部持っているしどうしよう・・・

### [7171] 玉井真矢	2002.07.24 WED 20:53:21　
	
皆さんお久しぶりです。
久しぶりに来て見たらＴＯＰに要チェックな情報が！
吉祥寺ブックスルーエで原画展をやっているみたいでちょうど８月に東京に行く予定があったのでついでに観て来ようと思います。
あとプレゼントって単行本全部買わなくては応募できないんでしょうか？
まあ全部買わなくちゃいけなくても応募すると思いますが・・・
だって先生のサイン色紙もプレゼントに入っているのですもの！！

### [7170] ジェイド	2002.07.24 WED 18:06:03　
	
はじめまして。ジェイドと申します。
先日、偶然ＡＨの存在を知り、あわてて1巻から4巻までを買って
とりあえず読み終えました。
読み終えてからすぐだと感情的になるかな…と思いつつも、
どうしても感想を書きたいので、どうか参加させてください。

まず思ったのは、
今回のこのＡＨのテーマって一体何なんだろう…ということです。
先生は「少女の成長」を描きたいと言われていたんですよね？
でも、どう見ても、アシャンの心の傷より、リョウの心の傷のほうが大きいと思うんですけど。。
アシャンは何もないところ（ゼロ）からの出発。
それに比べて、リョウはアシャンと同じ苦しみを背負いながら生き、そして長い年月をかけて、やっとかけがいのないもの（香）と一緒に生きる喜びをを得た。それなのに…
リョウのような人間が味わう喪失感って、私たちとは比べ物にならないくらい、重くて深いものなんじゃないかな。。

独断的かもしれませんが、ＡＨで「少女の成長」だけを描くのは
無理なんじゃないでしょうか。
リョウの「こころ」の変化（＝成長？）を描いていくことは絶対に必要ですよね？

先生は、それも承知の上で描いてらっしゃるのか、それとも少女の成長だけを描きたいのであって、リョウと香は導入部分でしかなく、ふたりは、ただ、主人公を温かく見守る存在でしかないのでしょうか。

アシャンの成長も見たいし、ＣＨの頃のような、思わず「ぷっ」と吹き出してしまうようなコメディタッチのお話も見たい。

でも、私が一番気になってしまうのは、やっぱり、
リョウの「これから」です。
アシャンがリョウと香、そして海坊主、冴子、信宏たちに見守られながら幸せになっていくのは、なんとなく想像することができるのですが、リョウに関しては、なにも予想できません。
誰もが願っているように幸せになっていってくれるのか…？？？

だから、先生にお願いします。
脇役とはいえ、リョウの心理描写も今までと同じように
きっちり丁寧に描いて欲しいです。
これから、リョウが「香」という過去と、どうやって
付き合っていくのか。どう捉えていくのか。
私が一番知りたいもの、見たいものはこれです。

やっぱり感情的になってしまったような。。。スミマセン。
なんか独りよがりで、全然作品を理解していないヤツなのかなぁ
わたしって。
　　
わっ、初めてなのに、すごく長くなってしまいました。
失礼いたしました。　

### [7169] Ｒｏｃｏ	2002.07.24 WED 16:50:00　
	
皆さん、最近Ａ．Ｈはどんな感じですか？あと、以前もっこりメッセージで先生が言っていたあゆの「ＦｒｅｅａｎｄＥａｓｙ」がアシャンの世界だとありましたね。
では皆さん、観月ありさの「Ｄａｙｓ」という曲知ってますか？これの歌詞は遼と香の二人の生活を表しています。(^_^)一度聴いてみて下さい。

大家，最近AH是什么感觉呢?还有，以前老师在留言里说过的“FreeandEasy”就是阿香的世界。
那么大家知道观月亚里沙的《Days》这首歌吗?这句歌词表现的是辽和香两个人的生活。(^_^)请听一听。

### [7168] kinshi	2002.07.24 WED 12:05:52　
	
こんにちは、今、将人様の書かれた通り、ＣＤの書き込み
ＴＹＲしたのですが、サウンドレコーダーの録音が、６０秒
までしか出来ず、その理由をメーカーにＴＥＬした所、
著作権の関係上らしいです。細かく分けながらもいいけど
やはり、いっきにテープのほうが、いいみたい・・・・。
それ用のソフトも売っているようですが・・・・。
ちなみに私のＰＣは、Ｗｉｎ－ＭＥです。

２６日の雷神。その日は、朝から、地方移動なので、
コンビニに寄る時間が・・・で、誰か教えていただきたい！
コンビニって、だいたい何時ごろ、店頭に並ぶのでしょう？
朝８時なら、置いてあるでしょうか？雷神が置いてある
コンビニは、駅とは、反対方向だし・・・
電車に乗っちゃえば、外には出ないし、・・・

それと雷神は、９月から、店頭販売するわけ？今週号に
書いてなかった？
んでは、今日もムシ暑いけど　がんばりましょう。
一日に２度、シャワー　「かかる」　kinshiでした。

### [7167] しゃぼん	2002.07.24 WED 01:02:03　
	
いつのまにやら「かかる」同盟ができていたのですね。（大汗）今日の予想最高気温は２３℃です。縦長日本ね。

＞たま様[7150]
「かかる」九州の言葉なのですね。御教授ありがとうございました。北海道でも言わないものですから、おんやあ？と思いましたんです。地味に（笑）広めていきますね。

＞千葉のOBA3様[7154]
会長（笑）派遣員ぐらいにしておいてください。

香ちゃんの享年。。。近いです私も。
じゃあシャワーかかって、枕元のバンチ32号みて寝ます。
RAIJINプレ創刊号楽しみ～。


### [7166] OREO	2002.07.24 WED 00:37:12　
	
こんばんわ。アッツイしバテマシタ。少しも涼しくなって欲しいです。肌は日焼けでマッカッカ・・・唇まで焼けちゃったようでキュウリパックでもしようかな？っと考え中のＯＲＥＯです。
＞７１６４たま様
こんばんわ。高校生のバイト生中心の中では、私はおばちゃんで、体力も気力も彼らにはついていけません。まだコーチ始まって２日目だと言うのにもう限界です。ハハッ・・・

＞７１６２将人様
こんばんわ。早速の回答ありがとうございました。
プロパティを開いてみましたが、ＷＡＶＥが見つかりません
これって同じＸＰでも違うということなのでしょうか？
重ね重ね、スミマセン。

今日もまたＴＳＵＴ＠Ｙ＠のバイトだったのですが、誰かが借りたＣＨのビデオが帰ってきてて、でもそのうち一本を何か書類と一緒に処理されてるのを目撃。どうやら破損やらなんかしちゃったみたいで・・オイタワシヤ～ビデオ様（笑）

それではお疲れモード全快なので、おやすみなさい

### [7165] 無言のパンダ	2002.07.23 TUE 23:54:10　
	
こんばんわ★
あっついですね～明日は最高気温３６度と聞いて気が遠くなりました。

＞まろ様（７１５９他）
＞将人様（７１５３他）
ラジ＠などの録音に関するアドバイス及びご指導、ありがとうございました！私もぜひやってみたいのですが、相当する機器がない上（と、思う。）日々老化していく脳細胞がなかなか理解してくれないので、いつになるかわかりません。トホホ・・・(ｰｰ;)
でも一家に一人、お二人のような方がいたらなぁってつくづく思いましたよ～。

＞草刈正雄さま（７１６３）
私も、たま様同様そのＨＮにビックリです！
もしかして本人？！とか思っちゃいました。（んなワケないか！）それにしてもＣＨにハマって、いきなりＡＨ４巻ですか？！かなり衝撃的な読み方ですネ！１～３巻は、もっと心の準備をして読まれたほうが良いですよ～(^_^;)
読まれた感想をまたカキコしてくださいね♪

＞たま様（７１６４）
録音成功おめでとうございます！すっごーい！尊敬しちゃう～！
これで、ますますラジ＠を聞くのも楽しみになりますね♪
あ～若い脳細胞がうらやましい～です(^^)

では、おやすみなさい　(-_-)zzz

### [7164] たま	2002.07.23 TUE 22:51:59　
	
みなさん、こんばんわ。(今回レスのみです)

＞草刈正雄…様　[７１６３]　初めまして。そして、
何故その名前を引用したのでしょうか？(不思議だ！)
しかも、出だしが、ビビル大木のギャグ…(マニアックだ！)
ＡＨ１～３巻ぜひ買って読んでくださいね。

＞いのち様　[７１５７]　こんばんわ。
本気(ホンキと書いてマジと読む)で、ｔハンマー「ブフォフッ」気に入りました！これからチョクチョク使っちゃおうかなぁ～。

＞ＯＲＥＯ様　[７１６１]　こんばんわ。
プールのコーチ♪　←ワイルドですね。(働け！稼げ！若人よ！)
そして、ＴＳＵＴ＠Ｙ＠ランキング…やったね♪「ＡＨ」♪
それにしても、冴子さん・・・う～ん何歳なんだ。(気になるぅ)

＞千葉のＯＢＡ３様　[７１５４]こんばんちわ～す。←変になった（＾＾；）
冴子さん、本当は若いままでいてほしいんですけどね。
それに、冴子さんには机上ではなく現場に出てほしい…(リョウも現場が合うって言ってたしぃ)
おっと、ＯＢＡ３様もついに「かかる同盟」加入しましたかぁ！よしよし！

＞葵様　[７１５８]　こんばんわ。
「かかる」人口増やしましょうよぉ～。
がんばれ！関東部門！標準語うらやましいぞぉ～！

＞将人（元・魔神）様　[７１６２]　こんばんわ。
ＣＤ－Ｒ書き込みきちんと出来ましたよ♪ありがとうございました。
へぇ～、神谷さんゲームの声とかもしてるんですね。(勉強になります♪)

＞ sophia様　[７１５５]　こんばんわ。
香の享年と同じ年・・・まだまだ若いですよぉ！！！
そして、わたしとも「タメ」なんですぅ！うれしかったので、書きました♪

以上、レスずくしでした。

### [7163] 草刈正雄	2002.07.23 TUE 01:58:59　
	
初めまして、こんばんみ。草刈正雄です。
最近シティーハンターにはまってて、友達にそれの続編(正確には続編じゃないけど）があると聞いて、何故かいきなり４巻を買って読んだら、話の内容はよく分からなかったけどリョウちゃんとか海坊主とか出てて、かなり楽しく読むことが出来ました。
これから、１巻から３巻までを一気に買いに行こうと思ってます。それでは。

### [7162] 将人（元・魔神）	2002.07.23 TUE 01:58:38　
	
＞[7161] OREOさま
＞＞７１５３将人様
＞こんばんわ。貴重な情報ありがとうございます。
＞私もＸＰなのですが、なぜかＷＡＶＥの選択のＢＯＸが
＞ないのです。ＣＤ、マイクロフォン、ライン入力、
＞そして補助とか言うのがありますが・・・
＞どうなのでしょう？

「コントロールパネル」→「サウンドとオーディオ～」→
→「オーディオ」→「録音」→「音量」の設定画面ですね。

その画面の上にあるメニューの「オプション(P)」→
→「プロパティ」を選択して下さい。

「音声の調整」の「○録音」が「◎」になって選択されて
いる状態にして下さい。

その下の「表示されるコントロール」というのがあり、
「□ＣＤ」とかはチェックが入っていると思います。
それに加えて「□Ｗａｖｅ（ウェーブ）」にもチェックを
追加して下さい。

これで、Ｗａｖｅを選択できるようになりますよ。

＞[7159] まろ様
ＷＭＰかＲｅａｌ　Ｐｌａｙｅｒのプレイヤーソフト側の
音量を最大にしておくという説明、私も書くのを
忘れてました。ありがとうございます。

あとＣＤ-ＲやＣＤ-ＲＷの説明のフォローもありがとう
ございます。
だいたいの音楽ＣＤとして録音したＣＤ-Ｒは、最近の
ほとんどのＣＤプレイヤー（ラジカセやコンポ、ヘッド
フォンステレオ）で、動くみたいですが、動かない物も
ありますので注意が必要でした。
すみません。フォローの説明ありがとうございます。

＞[7160] たま様
ラジ＠の録音できましたか？良かったですね。
ＣＤ-Ｒに、ちゃんと書き込みできたでしょうか？

私のＨＰに来て頂いたのですね。ありがとうございます。

あのＨＰは、神谷明さんも声を入れられているゲーム
「スーパーロボット大戦」シリーズを扱っていたＨＰです。
（といっても最近、更新していねいのですけど）

ＨＰに掲載していたロボットの絵は、私が子供の頃に見た
ロボットアニメで、その「スパロボ」にもでている
神谷明さんが主人公の流竜馬役をされていた
「ゲッターロボ」シリーズ（赤いロボットの方の絵）
ジャッキーチェン役で有名な石丸博也さんの演じられた
「マジンガーＺ」シリーズのロボットアニメの絵です。

### [7161] OREO	2002.07.23 TUE 00:50:23　
	
こんばんわ～今日は働きづめだわ・・・プールのコーチのバイトだからついに焼けてアイスノンノ全身あてまくりのＯＲＥＯです・・・ヒリヒリいたいよぉ・・・
＞７１５０たま様＆７１５４千葉のＯＢＡ３
こんばんわ。ほんと「ナゾ」ですよね。でも冴子さんの歳はナゾのままのがなんかいいって感じがします。冴子さんはいつでも若い気がするし、あのなにげな優しさいいですよねぇ

＞７１５２いも様
こんばんわ。私の周りの人々は「種を食べてしまえ！」と言ってきます。でもなんか盲腸になりそうでコワイ・・・それだけ種に注意して食べてるので、遅いですよ。食べるの・・しかもと～っても真剣、無表情で食べてる（コワッ）

＞７１５３将人様
こんばんわ。貴重な情報ありがとうございます。私もＸＰなのですが、なぜかＷＡＶＥの選択のＢＯＸがないのです。ＣＤ、マイクロフォン、ライン入力、そして補助とか言うのがありますが・・・どうなのでしょう？

＞７１５７いちの様
こんばんわ。お～これはショッピングに行って覗いてこなくては！ですね！！やっぱりそうですよね。実が一緒に出ちゃいますよね。ただでさえ種を口のなかで分けるのができないってのに、そこまでやっちゃってかなりのちらけよう＆きったなぁ～い！と非難苦情の嵐でした・・・（笑）当の本人はそこまで頑張った苦労が水の泡～っとブルーでした。
それからハンマーの「ブフォフッ！」かなりグーです（笑）

今日はＴＳＵＴ＠Ｙ＠でバイトもあり、何気なし、店内ＢＧＭに耳をかたむけました。あれってランキングしたり、オススメビデオ紹介してるんですけど、本の分野別一位の発表があって、ぬぁんと！ＡＨは見事に一位で放送かかってました
うっれしかったぁ！！！皆様も聞いてみて！
それでは、明日も働きずめの一日です。それではまた。
