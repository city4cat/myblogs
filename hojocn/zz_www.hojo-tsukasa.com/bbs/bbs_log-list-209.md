https://web.archive.org/web/20021225190003/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=209

### [4180] kinshi	2001.10.29 MON 00:58:17　
	
＞K.H様（４１７７）
私も　このCHの台詞大好きです。このあと　TVか、原作か忘れましたけど　俺は、プロの世界一だ！と　ぬけぬけと言ってしまうリョウちゃん　す・て・き。セリフ引用バトル？楽しんでいます。

＞CRAZY　CAT様
実は　私の学校も　ペンと剣　でした。もち私は女です。なのになぜ？男子校の校章が・・ハハハ;;　はずかしくて言えません。
その辺は謎ということで・・お風邪めしませぬよう。

では、また。

### [4179] ｌｕｎａ	2001.10.28 SUN 23:23:21　
	
ＫＨさんの
ＣＨのセリフの引用
かなり、いいです
お気に入りに入ってますよ
ｓｗｅｅｐｅｒさん
女の人？だったんですね
もう少しで惚れてしまうところでしたよ（笑）

### [4178] ｌｕｎａ	2001.10.28 SUN 23:17:27　
	
こんばんわｌｕｎａです
誰かのカキコで、Ｊｂｏｏｋの事を話されてましたね
ストーリは、別の人が書いてるから、いいやって思ってたけど
どうも北条先生も関与しているようで
それこそパラレルなｂｏｏｋなんですよね
でっ読みたーいって事で、即　注文しました
でっ、欲がでてきて、ジャンプが特別編集した物が
何冊かでてますよね
これも、欲しいって事で、これは、ネットの古本屋さんに
依頼しました。
前回コミックス、在庫があるってあったので、注文したけど
某古本屋さんからは、連絡なし
けどね、他の古本屋さんもあるから・・・
信じて依頼しました。
全部ｇｅｔできたら、幸せやわぁ
オークション話３
オークションの場には、欲しいと思うのはあります
がっ参加しない事にしました。
結構、悲惨な目にあってる人達がいて、
色々、聞くうちに・・・あたしってラッキーだったけど
次は・・・って事で引いてしまったし
みんなが悪い人達やない事わかってるけど
なるべくトラブルには、巻き込まれたくないよぉ
詐欺ってヤツが多々あるそうです
みなさんも気をつけて下さいね
今週、火曜日、明後日ＡＨ見れますね
長かったよ
どう展開していくか・・・楽しみだぁ

### [4177] K.H	2001.10.28 SUN 23:11:40　
	
こんばんは、K.Hです。寒いっすねえ。

＞ｓweepwer様[４１６９]

「どうせ弟の間違いだろ、って言うんだろ！」
「ん～、弟～？そんなことは言わないよ。今のキミのカッコみればね！」

はいは～い、リョウちゃんに負けず劣らずのもっこり野郎、K.Hで～す。
ははあ、結構有名ですよね、そのラジオ番組。sweeper様のカキコ読んで、思い出しました。本にも書いてありましたねえ。一番笑えたのが、稲葉は納豆を食べるときにまぜてから醤油をかけるけど、松本は醤油をかけてからまぜる（あれ、逆だったかな？）
んで、どっちが旨いか真剣に（番組で）議論したとかしないとか、読んだ覚えがありますね。
TMNのEXPOのなかで好きなのは、「We Love The Earth」なんです、ボキは。「LOVE TRAIN」もいいんですが、なんかハマっちゃいました。
　ちなみに、「あの夏をわすれない」の歌詞「キミのいない一秒は～」を、つい最近まで「キミのいない日常は～」と覚えていたのは、何を隠そう、このボキです。「CLASSIX」もいいですよ！特に「TELEPHONE LINE」「CONFESSION」「GIRL FRIEND」は絶品！！

　「フッ・・・なにがオリンピックだ、奴が世界一なら俺は・・・」
　「俺は日本一軽い男だっ！！」（ガタガタッ）
　「あ、あれ・・？ってことは世界一より下なんだよな・・・」
（香・麗香言葉を失う・・・）

＞槇ちゃん・冴子さんカップル

　皆さんのカキコ見ていると、冴子さんはかなり槇ちゃんに積極的にアプローチしていたんですねえ。もったいない・・・
　
　でも、槇ちゃんは香を妹としてではなく、一人の女として愛情を注いでいたんですね。ちなみに香ちゃん、あの北尾の話から察するに、自分への好意には結構鈍感だった気がします。（僕も人のことは言えませんが・・・）

　槇ちゃんの、香への想い、伝わっていなかったのかも・・・
　今思えば、槇ちゃんが最後に「香を・・・頼む・・・」とリョウに言った理由がわかるような気がします。その結果、リョウはかなり悩みましたが、香を仕事上だけでなく、人生のパートナーとしましたが、槇ちゃんもそれを望んだのではないでしょうか？

　「地獄はさびしいところだがすぐに賑やかにしてやるよ・・・槇村・・・」

### [4176] 葵（母デビューの被害･･･）	2001.10.28 SUN 22:15:53　
	
　こんばんは。母がネットデビューしてからというもの、ここ数日かかりっきりでコーチしてました。もう、ゲッソリです☆
　
　留守してる間が（火・水）でなくてよかった。そしたら、ログのたまりがすごいでしょうね。一週間ぶりの「バンチ」、早く来いっ！

＞冴子＆槙村のこと
　あっというまに死んでしまった槙村。冴子の心にも、皆の心にも印象深いキャラとして残っていますよね。あっけなく殺してしまった作者である北条せんせ、しまった！･･･とか、思いませんでした？（笑）

### [4175] ＣＲＡＺＹ ＣＡＴ	2001.10.28 SUN 21:47:11　
	
こんばんわ。ＣＲＡＺＹ ＣＡＴです。

＞kinshi様【４１６１】
　良くご存知ですね。知ってる人はいないと思ったのですが。が、しかし微妙に違いまして、有名な東京開成ではなく、校章に桜がつく方です。開成祭、今日も楽しかったです。疲れたけど。

＞まろ様【４１６５】
　はい、そのとおり。書くときかなり悩んで結局書いちゃったんだけどちょっと後悔してます。

＞『月』
　きれいでした。帰ってくるときフッと見上げると、雨雲の一部が妙に明るい。雨が降ってたので月が出るわけ無いと思ってたので気付かなかった。雲が切れて月が現れたとき『神秘的だなぁ～』としみじみ思った。またすぐ隠れてしまって、『リョウの心もチラッと見えるとすぐに戦いやトラブルという雲で隠れてしまうんだよなぁ～』なぁ～んて考えてたら階段でこけそうになった。

＞Ｃ.Ｃの[マァ～ジ最悪なんだけどぉ～]のコーナー
　今日の開成祭。ファイナルで裸で御輿やる予定だったんだけど、『雨降ってるしやんないだろ』と思って油断しているところに、『今日の御輿はあります』の知らせ。雨が体を冷やし、海に近いんで、強い海風でさらに冷え。しかも雨で御輿の会場が変わり、うまく動かないでほとんどとまってる状態。風邪の体にはきっついよぉ～。声は出なくなるし、全身の皮膚感覚が麻痺するし。『マァ～ジ最悪なんだけどぉ～』。
明日風邪こじらしてるかも。まだ片付け残ってるけど明日いけるかなぁ～？

ＣＲＡＺＹ ＣＡＴでした。
　　

### [4174] さえむら　(追加)	2001.10.28 SUN 21:30:52　
	
さらに追加します。
（ごめんなさい、長々と。）

＞ありあり様、ちゃこ様
Ａ・Ｈで、槙村は存在しますよ。
バンチのホームページで、冴子の紹介欄に「故槙村秀幸の恋人」とありますよ。

それでは皆様、お風邪に気を付けて。
既にひかれた方は、早く治るよう、お祈りします。

＞ありあり様、ちゃこ様  
在A·H，槙村是存在的。
在bunch的主页上，冴子的介绍栏里写着“已故槙村秀幸的恋人”。


### [4173] さえむら　(2)	2001.10.28 SUN 21:23:39　
	
あまりに長いので、二回に分けました。

[冴子さんとリョウ、槙村兄について]
冴子がリョウと出逢う前に、既に槙村と恋人同士で。冴子は、なかなか態度をはっきりさせない槙村との「穏やかな」けれども「ぬるま湯」な関係が、少し焦りと言うか、いら立ちを感じていて。
で、リョウと出逢って、リョウの影のある部分とかに惹かれてしまう。
なによりリョウの「激情」が、槙村とは違う『愛され方』が、冴子の恋愛感情を刺激させた。
冴子は、リョウには「恋」を、槙村には「愛」を感じていたのだろう。
冴子とリョウは、「恋人」には向いていても、それ以上の関係には向いていない。
そういう事を冴子が考えている内に、槙村が殺されてしまった。
槙村が亡くなる頃、時が経つにつれ、冴子はリョウとの「恋愛」に疲れてしまったのかもしれない。そして、槙村こそが自分のパートナーである事を再認識する。
そしてリョウは、香と出逢って、香から「穏やかな心地よさ」を手に入れた。

[槙村の香に対する感情]
槙村は、香に父親的感情を抱いていたのだろう。「誰かに奪われたくない」という感情。
もしも槙村があの時殺されなければ、槙村は冴子との結婚を決意していただろう。

[レス]
＞Black glasses様[4025]
江○市か小○市でしょうか？どちらにしても、わりに近くですね☆

＞sweeper様
遅くなりましたが、お誕生日おめでとうございます。
今年がsweeper様にとって、素晴らしい一年でありますように...。

＞Chikka様およびヤクルトファンの皆様
ヤクルトの日本一、おめでとうございます。
古田さんのケガが早くよくなりますように。

なんだか凄い長文になってしまいました。駄文、および不快に感じられる方がいらっしゃいましたら、すみません。

### [4172] ありあり	2001.10.28 SUN 21:23:22　
	
ちょっとおひさーです。
【冴子ねた・その２】
なんと！J-BOOKでそんなことが！
あんまり知識がないままカキコしたのが恥ずかしい・・・。
あくまでCHだけをベースにするとああいう妄想ができるかな、と思った事を並べていました（汗）
しかもちゃこ様[4128,4153]２度もレス、カキコしていただいて、とても感激です。いえいえ、とても怪文で（汗）思った事を順番どうりに文章にできないらしい・・・。
AHでは槇村は存在しない、という仮説には衝撃と共に納得です。それであの涙。確かにCHを土台にしていたらあり得ないシチュエーションではあるような気がします。

【無言のパンダ様】[4162]
だ、大丈夫ですか？あまり無理為さらないように・・・。

バンチの日は棚卸しだった。今週は憂鬱だ。では失礼。

### [4171] さえむら　(1)	2001.10.28 SUN 21:21:52　
	
おひさしぶりです。（約１週間振り！）さえむらです。
（体調が悪かったわけではなくて、なんだか間が悪くて、来られなかった。）
初めての方、よろしくお願いします。

[月曜ドラマ]
このドラマ、移植問題総決算！という感じのドラマでしたね...。
（ラストの近くで、『事故死した死体から、遺族に無断で心臓を取り出した』というのが出てきましたが、最初、Ａ・Ｈでそういう設定なのかと思っていました。（最初は６号くらいから読み始めたので）
ところで、グラスハート、今もちゃんと免疫抑制剤を飲んでいるんだろうか？（たしかドクの所を出る時に、身１つだったよね？時間の経過が今いちよく分からないんだけどﾉ？）
香ちゃんの心臓をもらうはずだった人は、やっぱり亡くなったんだろうなぁ...。（すぐにではないにしろ）
ということは、グラスハートは、香の命と移植されるはずの人の命と、二人分の命がかかってるんだから、きちんと『生きる』努力をして欲しい。

### [4170] 玉井真矢	2001.10.28 SUN 16:46:38　
	
皆さんお久しぶりです。
少し関係ない事ばかり書きすぎたと反省し書き込みを自粛していました。

さて11月10日僕の住んでいる地元で伊倉一恵さんのトークショウがあります。今日早速チケットを取ってきました。
もし質問できるときがありましたらＡ・Ｈの感想や槙村香のことを聞いてみたいと思います！！

### [4169] sweeper(雨ですね。)	2001.10.28 SUN 16:01:30　
	
こんにちは。栃木は雨です。

[レス。]
＞K.H様。[４１５８]
「やはり、男がブーケを取ると花嫁がふってくるのだ！！」
「んなアホな話聞いたことがない！！」

はい！こちらは「もっこり守備範囲内」のsweeperです。(笑)
レス、ありがとうございます。「りょうちゃん大カンゲキー！」です。セリフの引用頭が上がりません！
B'zのラジオ番組は私が高校の時に放送してました。番組名はつかさ様[４１０７]がカキコして下さいましたが「BEAT　ZONE」だったと思います。当時、「ミ＠オンナ＠ツ」が月曜日から木曜日の夜１０：００～１１：３０まで放送していて、その木曜日の１１：００～１１：３０の３０分枠だったと思います。「ミ＠オンナ＠ツ」のDJは「THE夜もヒ＠パ＠」でおなじみの赤坂泰彦さんでした。もっこりネタから音楽ネタまでコーナーがはばひろかったです。ゲストも豪華だったと思います。「ト＠セン」の井ノ原快彦さんや「M＠.Chi＠dr＠n」の桜井和寿さんなんかも出ていました。
それから、だんだんりょうに似てきましたね♪

「ほう・・・！！ついに本音が出たな！！」
「香は・・・友に託された・・・大事なあずかりものだ！おまえのような男にわたすわけにはいかないんでね！！」

＞Parrot様。[４１５７]
レス、ありがとうございます。「りょうちゃん大カンゲキー！」です。はい、日々精進中です。(笑)
パートナーですかー。うーん。いませんがいたらいいなと思ってます。りょうは香のパートナーですから。この２人には頭が上がりません。(笑)りょうみたいな人。パートナーだったらいいですね。理想です。それから、バンチの懸賞当たったそうなのでおめでとうございます。
雨。私はＣＨでの槙村兄が亡くなった話を思い出します。ひん死の槙村兄が「香を頼む・・・。」と言い残してりょうの腕の中で息を引き取ったコマと雷が忘れられないです。
読み返してまた辛いです・・・。

[音楽のこと。]
globeとTMNが無性に聴きたくなってしまって、この間、globeの「outernet」とTMNの「EXPO」をレンタルしてきました。
globeの「angels'song」聴いていて「りょうと香だ！」と思いました。TMN「EXPO」もいい歌多いですね。「LOVE　TRAIN」や「月はピアノに誘われて」好きです。まだまだありますが・・・。

[Dearest]
着メロで見つけたのでダウンロードしました。サビの部分ですが、すごくキレイで気に入っています。

[木根さん]
音楽ではないのですが、小説の「ユンカース・カム・ヒア」以前文庫版で買った記憶があったので早速家の中を探してみたら、見つけました。嬉しかったです♪今は、「CAROL」を探してます。

それでは、失礼します。

### [4168] kinshi	2001.10.28 SUN 15:50:22　
	
＞（４１６２）無言のパンダ様
お体だいじょうぶですか？主婦ってこうゆう時、けっこう孤独だったりしませんか？親がいれば、何かと気遣ってくれて、だんなが、やさしい人だといいんですが、　子供に気をつかわれるのもなんだかよけい　心苦しかったり、とにかく　主婦は、体力が、一番、ぐあいがわるけりゃ　自分でとっとと病院へ・・ってなもんだから・・・季節の変わり目　どうぞご自愛のほど。

＞原哲夫の黄金伝説５作品、ちょっとのぞきました　が　あまりにもの迫力と　その濃さに　私は引いてしまいました。
ファンには、たまらなくうれしいでしょうが、画的にも強すぎて
BUNCHで、週１が、いいみたい　そういえば、　発売日まで後２日！！じゃぁ　また。

### [4167] Ｂｌｕｅ　Ｃａｔ	2001.10.28 SUN 15:45:32　
	
わたしも、Ａ・Ｈには槇村がいたのかどうかまだわからない・・・と思ったりもしましたが、やっぱり槇村がいてくれないと、香がＣ・Ｈのパートナーになった理由も納得いかなくなりそうなんで・・・・今は、Ａ・Ｈの世界にも槇村は存在してたんじゃないかなぁ、って思っています。だからあの涙は、槇村だけじゃなくリョウまでいなくなってしまったら・・・・・・ってよぎってしまったための涙なのかな、とも思いました。

### [4166] ちゃこ	2001.10.28 SUN 14:51:18　
	
こんにちは。雨で寒いです。
見苦しいと分かってるけど、「4153」の自分の書き込みへの言い訳・・・
（書けば書くほど、墓穴を掘る気がするけれど、書きます）

「AHの冴子さん＆リョウ」について。
基本的に私は、AHが「パラレル」でも、過去はCHでの過去がベース。
（100％同じでなくても、80～90％は同じ）と思って読んでますので、当然、
「槇村兄は存在」し「冴子さんの愛する人は槇村兄」と思っております。
だから、リョウとの間は「戦友」であり「同じ悲しみを解り合える仲間」
過去には惹かれた部分もあったけれど、現在は「恋愛対象」では無い。
そう思ってます。
リョウにしても、やっぱり香への想いとは違う。
「異性で信頼を寄せられる、数少ない人間」て思ってると思います。
ただ、余りに冴子さんの涙が衝撃だったので、妄想入ってしまったのでした。

あぁ、4153の書き込み、後半部分削除したいです(^^;;)　
とんでもない内容で、本当にゴメンナサイ。
では、買い物に行ってきます（今日は寝坊したので、昼カキコが出来ない・・・）

### [4165] まろ	2001.10.28 SUN 07:33:55　
	
なんか、最近朝型になりつつある私・・・未だ、どなたもいらしていないＢＢＳに独りカキコをしていると、香を失ったリョウの”心の渇き”が解る（？）気がしました。

それでは、レスしませう。

[４１５５]ＣＲＡＺＹ　ＣＡＴさま
！！！ち、近いですねェ！でも、ネット上ではあまり個人情報を公にするのは危険ですから、ホドホドにした方がいいと思いますよ（”若い”って、いい事だ・笑）
　ちなみに、わたしは京○線の○田から、５つ南下した駅が最寄りです。あと、私が先週、駆けずり回ったＧＳは○倉市にあるものですから、多分お判りにならないのでは？（地図で言うと、湘○モノレールの湘○深沢駅の近くです。時間があったら確かめてください・笑）

まろの（期待度ゼロの）新企画！「あゝ、とらふぃっく」・・・略して『ＡＴ』のはぢまり×２～
記念すべき第１回はトヨタスポーツ８００です。このクルマはＣＨ本編「撮影所パニック！」にて、海ちゃんを買収した後、リョウが女優・佐藤　由美子を連れてドライブに行くシーンで登場しています。（時間のある方は、コミックスなどを開かれて確認しながら読むと、ちょっち雰囲気が出ます）リョウが運転していることから、多分彼の所有でしょう。

－－－現在でこそ、水平対向エンジンを積むクルマは、スバルとポルシェのみであるが、その昔、トヨタにも水平対向エンジンを積む、２ドアスペシャリティーが存在した。トヨタスポーツ８００、通称ヨタハチである。
　全長３５８０ｍｍ、全幅１４６５ｍｍ、全高１１７５ｍｍというコンパクトなボディの総重量は何と５８０ｋｇ、驚異的な軽さである。
　これに搭載される７９０ｃｃの、空冷水平対向２気筒エンジンは、最高出力４５馬力（※グロス値）、最大トルク６．８ｋｇを発生。先代のＶＷビートルのような「バタバタ」というサウンドを奏でる。
　流麗なフォルムを持つこのクルマが製造された期間は、１９６５年から６９年にかけて、わずか４年である・・・

※グロス値・・・エンジン単体での出力をさす。現在ではエンジンに車体重量と同等の負荷をかけた値「ネット値」が用いられているので、現在の馬力に換算すると若干数値が落ちる。

(零期待度的)新企划!“啊，流量”……简称“AT”的流量×2 ~
值得纪念的第一次是丰田体育800。这辆车是CH正编“照相馆惊魂!”中，在收买小海之后，獠带着女演员佐藤由美子去兜风的场景中登场。(有时间的人，打开漫画等一边确认一边读的话，会有一点点的气氛)从亮开车来看，大概是他的所有吧。

- - -现在，装水平对置发动机的车，只有斯巴鲁和保时捷，不过，在那以前，丰田也有装水平对置发动机的2门特别车型。丰田sport 800，俗称丰田八。
长3580mm、宽1465mm、高1175mm的小巧车身的总重量为580kg，轻得惊人。
搭载的790cc的气冷水平对置2缸引擎，最大功率45马力(※gross值)，最大扭矩6.8公斤。演奏出与前代VW甲壳虫一样的“啪嗒啪嗒”的声音。
拥有流丽外形的这辆车制造的时间，从1965年到69年，仅仅4年…

※gross值···引擎单体的输出。现在因为使用了对引擎施加了与车身重量同等的负荷的值“net值”，换算成现在的马力的话数值会略微下降。

### [4164] Ｌａｐｕｔａ	2001.10.28 SUN 03:07:48　
	
こんばんわ。親子ともども「Ｃ・Ｈ」で涙する、
Ｌａｐｕｔａです。

Ｐａｒｒｏｔさん♪
　　　まさに全くそのとおりでゴザイマス。
　　　まさしく僕は廃人と化しております。
　　　１日数時間は聴いていないと、中毒症状が・・・。
　　　夜も眠れませんし・・・・・。
　　　「Ｌａｐｕｔａ」最高「Ｃ・Ｈ」も最高ですね。

### [4163] sweeper(月がキレイです☆)	2001.10.28 SUN 01:37:32　
	
こんばんは。
バイトから帰って一段落ついたところです。

[遅くなりましたが冴子さんのこと～CITY　HUNTER～]
皆さんのカキコを読んでいるとウンウンとうなずいてしまう部分が多いです。
私は、冴子さんが愛していたのは槙村兄だと思っています。
りょうと槙村兄に愛されて、また自分も二人を愛してしまったけれど、どちらかを選ばなければならなかった。でも、答えを出す前に槙村兄は亡くなってしまった・・・。槙村兄がもし生きていたら、結論がまた違っていたかもしれません。原作を読んでいると、りょうが「槙村兄を忘れられない事だって知っている」というようなことを言ってますよね。もしかしたら、冴子さんはりょうのことは気持ちの整理がついたのかなと思います。
この３人の関係、すごく微妙ですよね。
ノベライズでは、槙村兄は冴子さんが唯一結婚を迫った人であり、元恋人となってました。ということは、仕事以外でのプライベートな時間を槙村兄と冴子さんは過ごしていたのかなと思います。もし、そうだとしたら後に残された冴子さんが切ないです。ふと思ったのですが、槙村兄の命日。りょうにとっても香にとっても辛いですが、一番辛いのは冴子さんなのではないでしょうか。もし、槙村兄の死をりょうから聞いたとしたら冴子さんにとってはもっと酷ですよね。

それでは、失礼します。
皆さん、おやすみなさいませ♪

### [4162] 無言のパンダ	2001.10.28 SUN 01:24:40　
	
こんばんわ★
わたくし、昨日の夜から原因不明の体調不良で、頭はクラクラ、
目はグルグル、寝てたら寝てたで足はツルし・・・！(>_<)
風邪の症状もないし、変なものも食べてないはずなのに～。
まともに意識を保っているのも、しんどいので今日はもう寝ます。（ていうか、今日一日寝てたけど）こんなことだけ書いて終わるのも気がひけるので、やっぱ、ちょっとだけレス。

＞ちゃこ様（４１５３）
お仕事お疲れさまでした。お子さんの具合はいかがですか？
ちゃこ様は、私よりお若いけど、かなりの恋愛上級者と見ました！でないと、冴子＆槙村兄の大人の恋愛をそれほどに分析できませんもん(^^ゞ

＞Ｐａｒｒｏｔ様（４１５７）
懸賞に当たったんですか？おめでとうございます♪
以前、最初の書き込みにも書いてあったけど、それにもイラスト描いたんですか？聴コミも、この勢いで当たるといいですね！

なにぶん、頭がもうろうとしているので、失礼なことを書いていたら、先にお詫びしときます。
では、おやすみなさい　(-_-)zzz

### [4161] kinshi	2001.10.28 SUN 00:36:01　
	
＞CRAZY CAT様
学園祭　楽しそうですね、いい思い出をたくさん作ってください
ハプニングも　過ぎれば、いい思い出になりますよ。
ところで　開成祭って　　もしかして　校章は、ペンと剣ですか？ 
