https://web.archive.org/web/20011112014413/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=154

### [3080] chikka(Eastern Shizuoka) 	 2001.09.20 THU 00:55:07　
	
【３０７０】まみころ様、【３０７２】ｓｗｅｅｐｅｒ様
私の住んでいる静岡にも「東照宮」があります。静岡の久能山に「久能山東照宮」と言って、晩年に駿府（今の静岡市）に住んでいた徳川家康公が亡くなった時に遺言で「一年間」だけ遺骨を納めたところです。日光東照宮が完成した後はそちらに移されました。昨年は「大河ドラマ」で「葵三代」が放映されたのでかなり静岡が取り上げられていました。また、久能山東照宮は山の頂上にあるので長い階段があって、行ったことがありますがかなりきつかったです。現在ではイチゴの名産地としても有名です。

「ミニクーパー」について
先週の火曜日は台風の影響で学校に生徒が半分しか来なかったために、授業ができませんでしたので「ＭＲ．ビーン」のビデオを来ていた生徒達に見せました。「ＭＲ．ビーン」の愛車もよく考えてみれば「ミニクーパー」だということをビデオを見て思い出しました。（色は黄色でしたが・・・）

「脚本家」のことについて
【３０６４】のちゃこ様と【３０６９】のＢｌｕｅ Ｃａｔ様が「作画監督」について触れていましたので、私は「ＣＩＴＹ ＨＵＮＴＥＲ」の脚本を書いていた脚本家について書きたいと思います。【３０６５】でちゃこ様が触れていましたが「グッパイ槇村、雨の夜に涙のバースデー」の脚本を書いていた「井上敏樹氏」は以前書きましたが今は「仮面ライダーアギト」の脚本を書いています。この前、映画「ＰＲＯＪＥＣＴ Ｇ４」の情報を見るために東映のＨＰを覗いたらプロフィールのところに主な作品として「ＣＩＴＹ ＨＵＮＴＥＲ」と書いてありましたが、デビュー作がなんと「Ｄｒ．スランプアラレちゃん」と言うことを知りました。「仮面ライダーアギト」でコミカルなシーンや食べるシーンが多いのはもしかして「ＣＩＴＹ ＨＵＮＴＥＲ」の影響と思っているのは私だけでしょうか。実際に、書いた作品を見てみると「コミカル」物が多かったし・・・。また、「北条司」先生の名前を拝借したとしか思えない人物が出ているし・・・。（北条透刑事と司龍二刑事。司刑事は後に逮捕されました。）
実はこの前本屋で遠藤明範氏の書いた歴史小説を買いました。遠藤氏も「ＣＩＴＹ ＨＵＮＴＥＲ」ではお馴染みの脚本家で、映画版の「愛と宿命のマグナム」の脚本を書いた人です。経歴を見てみると「機動戦士Ｚガンダム」、「金田一少年の事件簿」など結構シリアス物が多かったです。この前たまたま「犬夜叉」を見た時に脚本のところに遠藤氏の名前を見ました。ところで、遠藤氏の書いた歴史小説の題名は「真牡丹燈篭」といって、１８５５年（安政２年）の江戸の上野が舞台となっています。主人公は三遊亭円朝という当時１７歳の落語家（この若さで「真打ち」です）で、彼の身の周りで起こった奇怪な事件がこの本に書かれています。ちなみに三遊亭円朝は実在した落語家（１８３９～１９００）で主に「怪談噺」を得意としていました。「牡丹燈篭」は彼の代表作で、この話のもとになった出来事が実は彼が１７歳の時のこの本の中に書かれている事件だと言われています。また、彼は今日「笑点」で出ている三遊亭円楽、小遊三、楽太郎などの「三遊亭」一門の「祖先」のようなもので、当時没落していた「三遊亭」一門を盛り上がらせ、今日の活躍の原点を作った落語家です。ストーリーは時代劇ものでかなり面白かったです。それにしても、悪役の金貸しの名前が「黒川」というのはこれも「Ｃ・Ｈ」から取ったのかなあと思ってしまいました。麗香さんが出てきた話で友村・深町の両刑事を殺し、暴力団に麻薬を横流ししていた「黒川警視正」のことを思い出しました。ちなみにこの話の脚本を担当したのは遠藤明範氏です。
第２５話の「愛の国際親善？！ライバルはブロンド美人」で出てきた金髪のスイーパーが「エンジェルハート」という名前でしたが、この話を書いたのは武上純希さんです。彼女は今は平成「ウルトラマン」シリーズでよく名前を見掛けます。ＯＶＡの平成「ウルトラマンセブン」と「ウルトラマンネオス」は彼女が主に話の構成を行なっていました。彼女の「ウルトラマン」の話はかなりメルヘンチックでホロリとさせるものが多いです。ちなみに香さんと冴羽さんが初めて会う第４話「美女蒸発！！ブティックは闇への誘い」を書いたのは武上純希さんです。
ところで平野靖士氏や外池省二氏などの「Ｃ・Ｈ」でよく見かけた脚本家はその後はどんな作品を書いているのでしょうか？知りたいです。（最近テレビをあまり見ないので・・・）

关于“mini cooper”  
上周二因为台风的影响，学校里只有一半的学生来上课，所以我把《憨豆先生》的录像带给来上课的学生看了。仔细想想，“憨豆先生”的爱车也是“mini cooper”。(颜色是黄色的……)

关于“编剧”  
【3064】的chakko先生和【3069】的Blue Cat先生提到了“作画监督”，所以我想写一下写《CITY HUNTER》剧本的编剧。在【3065】中chakko先生提到过，写《晚安槙村，雨夜流泪的生日》剧本的“井上敏树先生”以前写过，现在正在写《假面骑士aguit》的剧本。前几天，为了看电影“PROJECT G4”的情报，在东映的主页上看到了简介中主要作品“CITY”写着《全职猎人》，后来才知道出道作竟然是《Dr .Slump Arale-chan》。在《假面骑士》中有很多搞笑的场面和吃的场面，难道只有我认为这是受《CITY HUNTER》的影响吗?实际上，看他写的作品，“滑稽”的东西很多……另外，还出现了只能认为是借用了“北条司”老师名字的人物…。(北条透刑警和司龙二刑警。司警官后来被逮捕了。)

其实之前在书店买了远藤明范写的历史小说。远藤也是《CITY HUNTER》的编剧，也是电影版《爱与宿命的连发枪》的编剧。从他的经历来看，有《机动战士Z高达》、《金田一少年事件簿》等相当严肃的作品。之前偶然看《犬夜叉》的时候在剧本那里看到了远藤的名字。对了，远藤氏写的历史小说的题目叫《真牡丹灯笼》，以1855年(安政2年)的江户上野为舞台。主人公是当时17岁的落语家三游亭圆朝(这么年轻就叫「真打ち」)，这本书里记录了发生在他身边的离奇事件。顺便一提，三游亭圆朝是实际存在的落语家(1839—1900)，主要擅长“怪谈”。《牡丹灯笼》是他的代表作，据说这个故事的原型就是他17岁时写在这本书里的事件。另外，他是今天在「笑点」中出现的三游亭圆乐、小游三、乐太郎等「三遊亭」一门的“祖先”，是让当时没落的「三遊亭」一门重新繁荣起来，成为今天活跃的原点的落语家。故事是时代剧，相当有趣。即便如此，我想反派的借贷人的名字叫「黒川」也是从“C·H”中取来的吧。在丽香小姐出现的故事里，我想起了杀害友村和深町两名刑警，向黑社会贩卖毒品的「黒川警視正」。顺带一提，负责这个故事的编剧是远藤明范。

在第25话的《爱的国际亲善? !竞争对手是金发美人》中出现的金发甜点名叫“AngelHeart”，这个故事的作者是武上纯希。现在在平成《奥特曼》系列中经常能看到她的名字。平成OVA《赛文奥特曼》和《奥特曼 尼奥斯奥特曼》的故事主要由她编剧。她的“奥特曼”的故事相当有童话色彩，很多都让人觉得很失落。顺便一提，香和冴羽初次见面的第4话“美女蒸发! !时装店向黑暗的邀请”的作者是武上纯希。

对了，平野靖士氏和外池省二氏等在“C·H”中经常看到的编剧，后来都写了什么样的作品呢?我想知道。(最近不怎么看电视……)



### [3079] おっさん（岐阜県：こんばんにゃ～） 	 2001.09.20 THU 00:37:01　
	
毎度、今日はすぐ引っ込む予定のおっさんです。皆様せ～のおっは～！！
神谷さんの誕生日、すっかり忘れてました。おもいっきり、お馬鹿や～。でも無言のパンダちゃ～んが（３０５９）が言ってたス○ス○、私も見てました。キムタクぞ～ちゃん（仕事先の偉いさんがキムタクのことをこんな風に言ってました。もしキムタクファンがいたらほんまにすんません）のですよね。
今日は、もうえらいんで引っ込みます。

### [3078] あお 	 2001.09.20 THU 00:35:25　
	
あれ…ちょっと関係ない内容かも…(汗)。

＞みみごん様(3066)
四聖獣の話
そうです。四季も意味しています。青竜は青を象徴し、春。白虎は白を象徴し、秋。朱雀は赤(朱色)を象徴し、夏。玄武は黒を象徴し、冬です。五行思想に基づいています。

ファンは…
同感です。何を書いても｢これの続きを書いてくれ｣と言われつづけるのはとても辛いことだと思います。だってその間書いている作品を無視されてしまうような気分になるかもしれませんもの。でもまあ、これは避けては通れない道なんじゃないかな、とも思うのです。誰にだって｢1番のお気に入り｣というものはあるわけですから。夫婦な2人も見たかったけど…あまり想像できなかった、というのも事実ですし(笑)。とりあえず、AHの展開を楽しみにしようかな、と思ってます。

＞まみころ様(3070)
レスありがとうございます。
そうですよね～、日本が攻撃されたところで、アメリカが動いてくれるかって言うと、かなり疑問があります。いてやっている、守ってやっている、と口で言いながら、その行動は迷惑極まりないものばかりですし。沖縄での暴行事件や、基地周辺での騒音問題などなど。｢出て行け｣と言いたいようなことばっかりやっている彼らが、いざというときに日本を守ろうとするでしょうかね？？
日本政府もまた、アメリカの報復に賛同するようなことを言っていました。でもこれって間違っていると思います。アメリカは日本に｢湾岸戦争時以上の協力を｣と要請しています。これって内政干渉だと思います。日本政府もまた、なぜ武力での報復に賛同するのでしょうか。戦争放棄を謳っておきながら、実質的に戦争行為でしかない報復攻撃に協力するのは矛盾していると感じます。テロ組織壊滅に必要なのは、徹底的な武力構想ではないはず。人が１人1人、飢えに苦しむことなく、安定した収入を持ち、寒さや暑さをしのぐ住居を持っていれば、まずこんなこと起こらないと思うのです。日本政府がすべき｢協力｣は軍事面でのサポートではなく、その余波を受けて苦しむ人への救済措置を優先すべきだと感じました。

＞sweeper様(3072)
初めまして。
そうです。いのまたむつみさんはサイバーフォーミュ○のキャラデザを担当していました。何冊が出ている彼女のイラスト集にサイバーのイラストもかなり掲載されています。そうなんですよねえ･･･この方もまたCHの作画監督されてたんですよねえ･･･。長いなあ…。

日光東照宮＞
小学校の修学旅行以来行ったことないですね…落葉の頃にぜひ訪れたいものです。
日本の宮号を冠する神社で、天皇系列ではないところって、ここと北野天満宮だけなんですよね～。普通宮号を冠する神社は天皇を祀ったとこなんですよ。東照宮と天満宮は天皇並に扱われているってことですね。

＞みみごん(3066)  
四圣兽的故事  
是的。也有四季的意思。青龙象征蓝色，春天。白虎象征白色，秋天。朱雀象征红色(朱红色)，夏天。玄武象征黑色，是冬天。基于五行思想。


### [3077] totti 	 2001.09.19 WED 23:33:20　
	
みなさまはじめまして、tottiと申します。よろしくお願いいたします。さて私は先週号で初めてAHを読みましたので何ともいいがたいのですが、リョウが何となく「暗かった」ですね。パートナーだった香が死んでしまったからでしょうか。香の死についてはみなさん色々あるようですが、スッキリしてない方が多いようですね。自分もその一人ではありますが、もし香が健在でリョウと夫婦生活を送っていたとしても、あまり物語にはならないと思います。だからこそ北条先生はあえてこのような選択をされたのではないでしょうか。これからどのような展開になるかは想像もつきませんが、毎週読ませてもらおうと思います。それでは今日はこのへんで失礼します。

### [3076] 無言のパンダ 	 2001.09.19 WED 23:30:29　
	
こんばんわ☆

もっぴー様（３０７３）
「キム＠クと神谷さんの共演」
レスありがとうございます♪
私、あの時はケンシロウの「ギクッ！？」にハマッてしまいました。（笑）
そういえば、キム＠クの奥さんのドラ＠ンボール好き、有名でしたよね。あの「徹子の＠屋」でも言ってたもん。むかし。
彼女の事あんまり好きじゃなかったんだけど、そういうの聞いてから、なんだか好感持つようになったよ。

「リョウと香のもっこりシーン？！」
そういう事実は事実として、目の当たりにするとヤケちゃうかも（笑）

### [3075] totti 	 2001.09.19 WED 23:00:34　
	
みなさまはじめまして、tottiと申します。よろしくお願いいたします。さて私は先週号で初めてAHを読みましたので何ともいいがたいのですが、リョウが何となく「暗かった」ですね。パートナーだった香が死んでしまったからでしょうか。香の死についてはみなさん色々あるようですが、スッキリしてない方が多いようですね。自分もその一人ではありますが、もし香が健在でリョウと夫婦生活を送っていたとしても、あまり物語にはならないと思います。だからこそ北条先生はあえてこのような選択をされたのではないでしょうか。これからどのような展開になるかは想像もつきませんが、毎週読ませてもらおうと思います。それでは今日はこのへんで失礼します。

### [3074] 玉井真矢 	 2001.09.19 WED 21:48:58　
	
皆さんのお役に立てたようで嬉しいです。

＞｢３０７３｣もっぴー様
そうです。始めていらしたとき｢こんにちは！神谷明です。｣と自己紹介されていたのでわかりました。
あの頃はちょうど年末で忘年会の話で盛り上がっていたことを思い出します。



### [3073] もっぴー（台風気をつけて下さいね） 	 2001.09.19 WED 20:56:09　
	
こんばんわ（＾＾）ｖ　CRAZY CATさん！ヨロシクデース！
【神谷さんのハンドルネーム】＞[３０５６]玉井真矢さま
レスアリガトウございます！
なぜあっくん？と一時考えましたが明のあっくんなんですね！
神谷さんて、『神谷』ていうお名前の印象が強いので、以外だった！可愛いっすねー！でもなんで本人て分かったんだろう？
自分で言われてたんですか？

【ミニクーパー】＞[３０６２]sweeperサマ
レスアリガトウございまーす！
あー！私ってばかー！リョウちゃんAHでも、ミニクーパー乗ってました！言われて思い出しました！総集編にばっちり載ってる
し！あー失礼いたしました！
あと、今日、旅の情報誌見ましたが、栃木ってたくさんいい所があるんですねー！

【スマスマに出演された神谷さん】＞[３０５９]無言のパンダ様
私も見ました。あと、前、話題になってましたが、ビストロに、叶し＠いが、出た時、キム＠クが、二人に挟まれて、『冴羽リョウになった気分！』ていってましたよねー！彼の奥さんは確か、ドラゴン＠－ルの、ゴ＠ウの大ファンですよね！あの夫婦、結構マンガ好きかもしれないですね！

【ラブラブのリョウちゃんと香ちゃん】＞[３０６５]ちゃこ様
レスアリガトウございます！
濃厚なのみたいですよねー！裸で抱き合ってるシーンがあるけど、相手がGHなんだもん。もっこりシーン見たいなー！
でも、AHになった時胸がちゃんと描かれてた事に驚きました。
CHの頃は初めのほう意外は無かった気がするので・・。

【ファンというものは作家にとって、最大の味方であると同時に～過去に縛り付けようとする最大の敵】←[３０６６]みみごん様
私も北条先生もそうゆう心境なのかなー？って思いました。
シティハンターをいつまでも愛するファンを有りがたいと思っている反面、いつまでも引きずらないでよ！って思われているかも。現に、エンジェルハートを読んで、１０年間引きずってきた私は間違っていたのかな？と思ったりもしましたからね。
でも、Angel Heartが終わるまで見守ります。そして、終了した時、パラレルと考えるか、続編と考えるか決めて、心の決着を付けたいと思います。

### [3072] sweeper（レスです。） 	 2001.09.19 WED 19:32:22　
	
こんばんは。
今週のバンチ、ＡＨが休載らしいのでちょっと寂しいです。

＞ちゃこ様。［３０６４］
レス、ありがとうございます。
私も、「RUNNING　TO　HORIZON」のオープニング好きです。
映画のワンシーンみたいですよね。ラスト、りょうが敵のヘリを撃ち抜くシーンもいいですが、香もかっこよかったです。
あと、ドラマティックマスター。りょうと冴子がもっこりのチャラを賭けてしりとりをするのも好きです。
［薔薇のこと。］
そういえば、りょうの白いスーツに胸に薔薇の服装。ありました。はじめのイラストレーションズにのってましたよ。
やっぱり「CHANCE」のとおり、薔薇が似合うんでしょうねー。ナレーションの「血と硝煙」のところなら似合うのが何となくわかるのですが。

＞みみごん様。［３０６６］
レス、ありがとうございます。
お住まいは京都という事ですが、私は修学旅行で京都に行きました。京都といえば八つ橋ですよね。あと、「京のお抹茶プリン」というのもありますよね。弟が高校生のときこれを食べていたのを見たのですが。
それから日光東照宮のことですが、よく小説に出てくるようです。「眠り猫」と「見ざる、言わざる、聞かざる」あたりが有名なようです。あとは「鳴き龍」ですね。（これは微妙・・・。）

＞まみころ様。［３０７０］
レス、ありがとうございます。「りょうちゃん大カンゲキー！」です。はい、栃木県在住です。でも、日光在住ではありませんが。日光東照宮。「眠り猫」確かにありますよ。私は小学生のとき、いとこが日光に住んでいるということもあったのでときどき親と一緒に行ってました。ついでという感じで日光東照宮へは行っていたので、そのたびに「外人さんも来てるなー。」といつも思ってました。

＞Blue　Cat様。［３０６９］
いのまたむつみさん。アニメＣＨの作画監督やっていたんですか。初めて知りました。確か、「サイバーフォーミュラ」も作画かキャラクターデザインのどっちかをやっていませんでした？

それではまた来ます。

### [3071] ＣＲＡＺＹ ＣＡＴ 	 2001.09.19 WED 15:47:27　
	
どうもはじめまして。

＜自己紹介＞
　ＣＲＡＺＹ ＣＡＴです(キャッツアイの｢離南島からの招待状｣で瞳が乗ってたポルシェからとってます)。14歳の中三です。これからもよろしくおねがいします。

　今日は、ヨットやって(うちの学校はヨットやるんです)疲れているので、これで終わります。たまったログを読み終わったらまたきます。　ＣＲＡＺＹ ＣＡＴでした。

### [3070] まみころ 	 2001.09.19 WED 12:00:32　
	
お久しぶり（？）です。レスです。

＞あお様［３０４４］
　報復措置等のレスどうもありがとうございました。
公式サイトに、関係ない事を書いてしまったので、少々気がひけ
てたんですけど、レスをいただき嬉しく思いました。
フランスは冷静な判断を、と言ってたようですが、今日のニュー
すでは、どうやら参加表明等をしたとかしないとか･･･。
あくまでイスラムとの戦いではなく、テロリストとの戦いだ、と
言う事で一致したのでしょうか･･･。
　以前ここが＠だよ日＠人のアメリカ人バージョンで、誰かが日
本はアメリカに守られてる。守ってやってるんだ。という言い方
をしていました。でも本当に日本が戦争に巻き込まれた時、はた
してアメリカは日本の為に戦うでしょうか？多分絶対戦わないと
私は思ってます。日本の米軍基地を攻撃されたとかならその事の
報復はすると思いますが、日本本土への攻撃に対しては多分そう
いうかたちの戦いはしないでしょうね。
力こそ全て、力こそ正義、私が書いたテロの意味がそうならば、
アメリカも自分の政策を力で押し返そうとしてるのだから、
相手にしたらアメリカもテロリストだと感じてるでしょうね。

またまた関係ないことばかり書いてスミマセン。

［神谷明さんの誕生日］
ちなみに私もここで初めて神谷明さんの誕生日を知りました。
ホント、ファン失格ですね。スミマセン。（笑）
又無知な私に、いろいろな情報を教えて下さい｡（笑）

［栃木と京都］
sweeper様、栃木って事ですが日光東照宮近いのですか？
小学校の頃移動教室で行ったのですが、眠り猫や、くぐる柱か
何かがあったような記憶があります。
ちなみにみみごん様の京都も修学旅行で行きましたね。
私からすると、良いな～、と思います。
そういう観光地がそばにあるってどういう感じですかね？

### [3069] Ｂｌｕｅ　Ｃａｔ 	 2001.09.19 WED 10:48:53　
	
★アニメＣ・Ｈの作画監督さん
　　３０６４ちゃこ様、そうそう、いのまたむつみさんも作画監督をされてたんですよねぇ、たしか子供のでてくるような話が多かった気がします。
　　ちなみにわたしは、丹澤学さんが作画監督をされた回がけっこう好きだったりします。Ｃ・Ｈ’９１の浅香姉妹の話の後編、朝露に濡れたリョウの顔とか、大好きです♪

★声優さん
　　神谷明さんと伊倉一恵さんのコンビといえば、「スケバン刑事」のＯＶＡとか、「万能文化猫娘」なんかもそうですよね。
　　「万猫」ではこの二人が親子だったりするんですが、神谷さんの演じる久作ぱぱさんはなんかすごくあったかくって、わたしはリョウと同じくらい大好きです♪　伊倉さんの演じる龍之介くんもすっごい可愛いし、おすすめですよ♪　個人的にはＣＤドラマ版の「ＳＯＵＮＤ　ＰＨＡＳＥ」シリーズが一番好きです。
　　「スケバン刑事」のＯＶＡって最近レンタルに置いてあるのを見かけないんですが・・・出たのがＣ・Ｈ’９１の放送と同時期だったんで最初はこのキャストにびっくりしましたが、いまではわたしの中で麻宮サキの声は伊倉さん以外考えられなくなってます。神谷さんの神恭一郎の声もよかったです。もっといっぱい作ってほしかったな・・・・原作のラストのあのプロポーズのシーン、二人の声で聴きたかった・・・でも聴いたら切なすぎて涙が止まらないかも。

★声优  
说到神谷明和伊仓一惠的组合，《好色刑警》OVA、《万能文化猫女》等都是这样的吧。   
在《万猫》中，这两个人是父子关系。神谷饰演的久作爸爸非常可爱，和獠一样非常令人喜欢。伊仓饰演的龙之介也非常可爱，推荐我个人最喜欢CD电视剧版的“SOUND PHASE”系列。  
“skiban刑警”的OVA最近没有看到出租放着的···出的和C·H’91的广播是同一时期最初被这个角色吓了一跳，不过，现在在我心中麻宫崎的声音除了伊仓先生以外，我想不出别的了。神谷的神恭一郎的声音也很好。原作最后的那个求婚的场景，想用两个人的声音听的···但是听了的话太悲伤了眼泪止不住。


### [3068] 無言のパンダ 	 2001.09.19 WED 09:38:20　
	
おはようございます♪

将人様（３０６７）
はじめまして、よろしくお願いします♪
「冴羽商事のこと」
アニメ版ＣＨから名づけたのかどうかは、わからないけど、たしかに２０００年版の声優データファイルには、神谷さんの所属プロダクションの名前が「冴羽商事」になってますね～！
２００１年版は、もちろん「コアミックス」になってますけど。
今まで、あまり気にしたことなかったので、びっくりです！
これって、どう考えてもＣＨからとったとしか思えないですが、
たしかなことをご存知の方がいらしたら、教えていただきたいですね♪

麻上洋子さんが「一龍斎春水」っていうのは、一龍斎貞友さん（元、鈴木みえさん）と同じ流派ってことなんですかね？
アニメ雑誌で、投稿者の学校の行事で「講釈師」として来られたというのを読んだことがあります。（貞友さんが、です）

ちゃこ様（３０６５）
書店で、ＡＨがＣＨの続編と紹介されていたんですか～？
それって、書店側が勝手に、そう解釈してのことなんでしょうかね？なんだか複雑ですね。
ＣＨを知ってる人は、そう言われても「そうなのかなぁ？」と
自分なりに考えられると思うんですが、ＣＨを知らない人は、
ストレートに信じちゃうと思うから、ちょっとヤダな・・・。
でも、ちゃこ様の言うように、これをきっかけにＣＨのビデオやＣＤが復活＆充実してくれたら、うれしいですね♪
特にビデオを、みんなの手の届くところに置いてもらいたいですね（笑）
ところで、ぜんぜん関係ないことですが、私の娘が、ちゃこ様の
ＨＮを見て一言。「おかあさんだったら、（ちゃんこ）だね」だって！失礼しちゃうでしょ（笑）
でも、あんまりうまいこと言うので、言い返せなかった（泣）

### [3067] 将人（まじん） 	 2001.09.19 WED 01:48:08　
	
こんばんは、将人（まじん）です。
声優の神谷明さんって、今日誕生日だったのですか？
知りませんでした。おめでとうございます。

神谷明さんの事で質問なんですけど、どなたか知りませんかね？
声優さんの本で、神谷明さんの所属プロダクションの名前が
「冴羽商事」と書かれていたのですが、これは神谷明さんが
演じられたアニメ版のＣＨから名付けられたものなの
でしょうか？
誰か知っている方がおられたら、教えて下さい。
（できればＣＨから取られたという方がファンとしては
うれしいんですけどね。）

＞冴羽 馨りさま[3063]
冴子役の麻上洋子さんは「女流講釈師」で
「一龍斎春水（はるみ）」を名乗られているそうです。
http://www02.so-net.ne.jp/~c-ookubo/
↑上記のＨＰで、麻上洋子さんがヒロイン森雪役で
出演されていた宇宙戦艦ヤマトの「講談」版を
聞く事ができますよ。
講談のＣＤも出されているみたいです。

ＣＨやＡＨまたはＣＥなどを「講談」版でやったら
どうなるんでしょうね？（汗）（＾＾；

＞冴羽 馨りさま[3063]  
冴子的扮演者是麻上洋子，她在《女讲解师》中据说取名为“一龙斋春水”。  
http://www02.so-net.ne.jp/~c-ookubo/  
↑以上HP中，麻上洋子饰演女主角森雪。  
出演的宇宙战舰大和号的“讲谈”版
可以听哦。
好像还出了讲谈的CD。


### [3066] みみごん 	 2001.09.19 WED 01:47:34　
	
[四神獣]
＞[３０４４]あお様
解説、どうもありがとうございました。ストラップの台紙にいろいろ書いてあったのですが、もう捨ててしまってよく憶えてないんです。なんか、それぞれが四季を表わしてる、とかも書いてあったような...

＞[３０３２]葵様
ストラップ、マスコットがかわいいし、紐の部分もちょっと組紐っぽくていいですよ。８００円くらいだったと思います。私も買おうかと思ったのですが、携帯にはホームズの格好をしたスヌーピー、ポケットステーションにはサンダーバード２号（これが１番のお気に入り！）のストラップがついていて、もうつける物がないんです。

＞[３０４２]sweeper様
私は京都在住です。平安神宮からは歩いても１０～１５分くらいです。sweeper様は栃木県ですか？栃木と言えば日光東照宮。少し前に東照宮を題材にしたミステリー小説を読んだので、行ってみたいな、と思うんですが、関西からはちょっと遠い...

さっき読んでいた雑誌に、「ファンというものは作家にとって最大の味方であると同時に、"昔のあの作品のようなものを" といって過去に縛りつけようとする最大の敵でもあるのです」という文章がありました。なんか身につまされます。ＣＨ以降、北条先生の画かれた物はオールマンの単発の作品も全部読んできましたが、(もちろんＣＨ以前のものも)「ＣＨの続編が読みたい」という思いが頭から離れませんでしたから。

### [3065] ちゃこ 	 2001.09.19 WED 01:06:50　
	
レス第２弾です。

＞無言のパンダ様「３０４３」
こんばんは。こちらのアニメビデオのレンタル半額日は、毎週日曜です。
最近行き始めたレンタル屋さんで、ＣＨが全部あったのが驚きでした。
でも、やっぱり目に付かない隅っこの方に並べてありましたよ。
今回は初期シリーズVol.４「グッバイ槇村」も借りました。
話ズレますが、この間本屋に行ったら、漫画文庫の所にＣＨが北斗の拳と一緒に
平積みされていて「現在連載中のＡＨは続編です」と紹介されていました。
良いのだろうか？！　でも、こういうのをキッカケにＣＨのビデオやＣＤが
レンタル屋に復活してくれると、嬉しいかも。

＞葵様「３０１２」
ちょっと遅くなっちゃったネタですが。
「野上ママ」きっとかなりの美人に違いありません。
なんといっても「野上シスターズ」の生みの母ですから（＾＾）
そして、野上パパを操縦してるんだから、相当スゴイ人だと思われます。
もしこの先、ＣＨを描くことがあったら、ぜひ「野上ファミリー」には
総出演してもらいたいところです。（スゴイ話になりそうだけど）

＞もっぴー様「３０５６」
私も「ラブラブ全開モード」の２人は見てみたいです（＾＾）
でも以前にも話題になりましたが「初キス」があんな感じでしたからね。
始まりはやっぱり「じゃれあい」なんでしょうね。
・・・これ以上は「妄想モード」に私が突入しそうなので、失礼します(^^;;)

では、おやすみなさい。

### [3064] ちゃこ 	 2001.09.19 WED 01:04:37　
	
こんばんは。何となく習慣で、バンチ手に取ってしまいました。
やっぱり休載で、残念。でも、どこかでホッとしてたりして・・・
先々週・先週とヘビーな気分を引きずってしまったので、
ここで気分転換して（？）来週からの展開に備えたいと思ってます。

「作画監督の話」
どなたも触れてないと思うのですが、初期の頃に「いのまたむつみ」さんが
作画監督されていたのに驚きました。凄く意外だった・・・
（「ドラマティックマスター２」のテレビジョンデータで知った）

＞sweeper様「３０４２」「３０４９」
「サントラの話」
「Battle」の曲は、確かに「RUNNING　TO　HORIZON」です。
（そういえば、あの時のオープニングアニメ好きです。)
何だかんだ言って、メッセージ剥がさないでおく辺りが良いですよね。
アニメやノベライズのリョウは、本編（漫画）に比べると可愛い（笑）
最初それが違和感あったんだけど、今は楽しんでます。

「薔薇の話」
そういえばミックは、香に呼び出された時、薔薇の花束持ってました（大笑）
あの話の時、コートは飛んで行くし、彼はどこまでも「笑い担当」だったなぁ。
リョウがプレゼントするなら、sweeper様の書かれた様な状況でしょうね。
それで香が感激して泣きそうになったら、身の置き所が無くて、
とんでもない発言をするのでしょう・・・（笑）
リョウ＆薔薇は、単行本のカバーイラストで描かれていました。
（白いスーツで、胸に薔薇とか）
ということは、先生も「薔薇が似合う」と思っていたのかな！？

では、また。

### [3063] 冴羽 馨り 	 2001.09.18 TUE 23:51:48　
	
こんばんは。

★北条センセのサイン色紙★
＞玉井真矢様［３０５１］
初めまして。かなり前のログを読んだ時の話なのですが色紙の当選羨ましいです！どうされてます？

そういえば今日は神谷さんの誕生日ですね　おめでとうございます！
パフパフドンドン！！（ふるっ）

★ＣＨの声優陣★
＞玉井真矢様［３０５１］
もしかしたらご存知の情報かもしれませんが一応書きますね
　神谷さんは確か１９４７年生まれのＡ型で横浜出身だったと思いますが。
　伊倉さんは１９５９年３月２３日生まれのＯ型で長野県出身です。
　冴子役の麻上洋子さんは生まれ年は知りませんが７月１０日生まれのＡ型で、出身地は神奈川県の藤沢市、出生地は北海道の小樽市です。
彼女は声優のほかにもう１つ職業を掛け持ちしているらしく、そこでは「一龍斎春水」と名乗っているらしいです。
お茶の先生みたいな名前だな（笑）

★スマスマに出演の神谷さん★
＞無言のパンダ様［３０５９］
実は見たことは無いのですが、その話はその週のスマスマを見た友達から聞きました。
その時の神谷さんは冴羽リョウ以外のキャラクターの声もやっていたとかで。

＞ありあり様［？］　
ごめんなさいログのナンバー忘れてしまいました。。。
前回はレスありがとうございます。やっぱり今週は休載らしいですね　さみしー・・・　

★CH的声优阵容★  
＞玉井真矢先生[3051]  
也许这是你知道的信息，我先写下来。  
  神谷先生是1947年出生的A型血，应该是横滨人。  
  伊仓先生是1959年3月23日出生的O型血，长野县人。  
  饰演冴子的麻上洋子是7月10日出生的A型血，出生地是神奈川县的藤泽市，出生地是北海道的小樽市。
她除了声优之外，好像还有另外一个职业，在那里自称“一龙斋春水”。
这个名字就像一个茶先生(笑)


### [3062] sweeper(・・・むむう。) 	 2001.09.18 TUE 23:08:13　
	
Good　evening！！
＞玉井真矢様。［３０５８］
神谷さん。リニューアル前はよくカキコしていらっしゃいましたよね。ＨＮ、［神谷　明］でも来ていたような気がしましたが。

＞もっぴー様。［３０５６］［３０５７］
レス、いつもありがとうございます。「りょうちゃん大カンゲキー！」です。最近、「りょうちゃん大カンゲキー！」が好きと言う方が少しずつですが、いるようでうれしいです♪
ミニクーパー。私もはじめて車を買う時はこれにしようと思ってました。でも、なかなか見つからなかったので、やむなく軽にしました・・・。
あと、ＡＨのりょうはミニクーパーに乗ってます。ナンバープレートのナンバーもＣＨと一緒です。
それから、ビンゴです！私のすんでいるところはその女優さんの実家があります。旅館というよりホテルです。
あ、やっぱり栃木県は日光猿軍団でしたか。

［ＣＨを読んでいて思ったこと。］
原作だと行事ネタ。出てこないですよね。アニメだと、クリスマスがあるのですが・・・。誕生日だけでしたね。バレンタインとか、ホワイトデーの話もあったら読んでみたいなと思いました。

それでは、失礼します。

### [3061] けいちゃん 	 2001.09.18 TUE 22:53:59　
	
お久しぶり～のけいちゃんです。
今週、やっぱりお休みでしたねー。でも今週は仕事心身共に疲れたので、あってほしかった・・。
マンガは、仕事がんばってる「ごほうび」と自分自身にいいきかせてます、ハイ。
あ～なに書いてるんだろ、わたしってば。
神谷明さん、誕生日っていったい何歳になったんでしょう・・
ま、自分も年とったんだんだから、何ともいえないが・・。
ＡＨについて、１０年前ＣＨを夢中になって読んでたときより、展開とか矛盾を感じてしまうこともあります。大人の視点でさめた視線ってやつ。ただ、読者の中にはほとんどのひとが １０年前のおもしろさを知ってる人が多いわけで・・読者の求めてる物が大きくなってしまうのは当然かな・・ホントにこれからの展開に期待したいとおもいます。
私はかなり北条先生びいき？だと思ってますが、「Ｒｕｓｈ！」という作品だけはいまいちでした・・・だから、２巻で終わってしまったのかもしれませんが。こんなこと書いちゃって北条先生、ごめんね～

