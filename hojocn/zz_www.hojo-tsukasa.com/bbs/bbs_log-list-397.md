https://web.archive.org/web/20030829181711/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=397

### [7940] 葵（世の中思うようには…）	2002.09.23 MON 21:44:46　
	
　ｃｈｉｋｋａさまと千葉のマダムの微笑む顔が目に浮かびます。世の中思うようには行きませんよね。ま、明日ってことで…？！（でも私は火曜しか休めないんだぁ～っ☆せめて一週間セールにしてくれぇ～っ！！）

＞ＯＲＥＯさま［７９２９］
　だからぁ～…たまさまには謝ったのにぃ～。(^_^;でもホント、うらやましい仲良しさんですね。これぞ北条ＦＡＮの鏡！！（…ってことで、指詰めはチャラね？！）

＞いちのさま［７９３５］
　おおっ！同士よ！！（笑）これでいちのさまも「勇者」の仲間入りですね♪でもホント、もちっと場所を考えて欲しいですよ。（せめて囲いがあったらねぇ…☆）さぁさぁ「勇者の会」に入会希望者は、今すぐＭＹ･ＣＩＴＹへレッツゴー♪(^_^;

＞いもさま［７９３６］
　「勇者の会」会長の葵です。（笑）さぁ、いちのさまも勇者の会に入られましたよ？いもさまも続け～っ！！

### [7939] 里奈	2002.09.23 MON 21:39:00　
	
☆７９３８　マイコ様
『愛と宿命のマグナム』を見てから、また『緊急生中継～』と『グッド･バイ～』をそれぞれ見たんですけど、やっぱり新しいほうが絵がキレイで違うわ（＾ヮ＾）
『愛と宿命～』の中でリョウがヒロインに『なぜあなたは今まで香さんの気持ちに答えてあげなかったのですか』と聞かれるシーンがあります。リョウのその答えは少しあいまいでしたが、でもいつもと違ってちゃんと真面目に答えてるんですよね。ちょっとドキッとさせられました。早く見てくださいねっ♪

### [7938] マイコ	2002.09.23 MON 21:23:56　
	
＞「７９３０」里奈様　「愛と宿命のマグナム」借りたんですか！私も借りたけどまだ見ていません・・・。（ひまがないので。）あぁ～！早くみたい！
★今夜は落ちついてねむれそうでよかったですね♪（笑）

＞「７９３５」いちの様　ええっ！XYZ・BOXに投票してきたんですか！うらやまし～。私は石川県なので投票なんてできないんです。（泣）いいな～私も投票したいな～。　　　　★いちのさんすみません！今日バンチ発見して読みました。いちのさんもあきらめず、がんばって見つけてください。（なんか悪いな～。）マクガイヤー大統領候補なつかしいですね。（笑）

＞「７９２８」もっこり様　はじめまして！マイコと申します。これからよろしくお願いします！そうですね。ゲットワイルドのタイミングいいですね。私も好きです。

### [7937] 里奈	2002.09.23 MON 20:55:41　
	
☆７９３１　Ｋａｔｈｉｎｅ様
きゃぁー！香港の方ですか！？海外の方と接することの少ない私には、とても刺激的です。阿香ちゃんみたいで嬉しいなぁ～♪これからよろしくお願いします♪♪

☆７９３２　もっこり様
天に召されたテープのご冥福をお祈り致します…。やっぱりＤＶＤで見たいなぁー。私のテープももうお迎えが来そうなんで、そろそろレンタルしなおしてダビングしなおそうと思ってます。最近のビデオはダビング防止のせいで残しておくことができませんが、Ｃ･Ｈは平気ですもんね♪

☆７９３３　やすこ様
今日も一日Ｃ･Ｈの歌ばっか歌ってました♪店内の音楽がバカでかい服屋さんで働いてるんで、多少大きい声で歌っててもバレないから気持ちイイです（笑）『最近里奈ちゃん機嫌イイね』って言わるからバレてないと思ってるのは本人だけかもしれないですけど…。

☆７９３５　いちの様
あらやだっ！冴子様に誘惑されたいのっ！？でも利用されて踏み倒されて終わりかもよ？しかも冴子に関わったらあとの２人も黙っちゃいないでしょ！恐るべし３姉妹ですからね（笑）私なら、ミックがイイかな♪白人大好き（＞ヮ＜）きゃはっ
って、私達ったらなんてフシダラなのっ！？私の思考回路を淫らにしちゃ嫌っ！いちの様のバカン☆

### [7936] いも	2002.09.23 MON 20:15:12　
	
こんばんわ～。
カキコの多さに少々面くらい気味です。
はじめましての方々よろしくお願いします。
巨人優勝なるか！！？（野球はあんまり見ませんが・汗）

感想はこれだけ多くの方が書いてらっしゃるので、私は省略ってことにします。(^_^;)

レスです。
＞いちのさま[7935]
いんや～、行ったんですね。新宿に！ＸＹＺの為に！！
でもあれはちょっと書きにくいですよね～。元祖ピュアボーイのいちのさまでさえ勇気がいるんですもん。シャイな私にはそうとう難しいことです。はあ。

＞葵“勇者”さま
ね！？いちのさまでさえ「あれは…」って仰ってるんですよ。
ちょっと難しいですよ。新宿には学校帰りにいつでも寄れるので、まだまだチャンスはたくさんあるんですどもね…。(^_^;)
最近は忙しいのでどうかなあ。い、言い訳じゃないですよ！
なるべく行けるように…ど、ど、努力はしてみますが。はい。
あんまり期待せずに報告（出来るかなあ・汗）を待ってて下さい。

ではでは～。いもでした。

### [7935] いちの	2002.09.23 MON 17:54:46　
	
こんちは！いちのです。
昨日今日と友達の家に泊まりに行って来たのですが、遊び疲れて、まぶたが重い・・・もちろん夜更かし。寝たのが朝の４時で起きたのは７時でした（汗）。若いって素晴らしいね（爽）。（←壊れています）
そんなことより！！遊びに行くついでにと言っては失礼ですが、ＭＹ　ＣＩＴＹＸＹＺ・ＢＯＸに依頼を投票してまいりました！！！！！むしろ、ＸＹＺ・ＢＯＸ行くついでに友達の家に行った感じ（笑）。２２日のお昼頃、ＢＯＸの前で人の肩かりて用紙に記入していたノッポは、私です。
しかし、ホントにあれじゃあ、とてもじゃないが依頼しずらかったです（汗）。せめてテーブルくらい出してくださいよ（驚）。しかも、すごく人目に触れる場所でしたし・・・。
一人だったら書けなかったな・・・。勇気いりますよ。
特にたいした依頼はしてないんですけどね（汗）。

＞[7915] マイコ様
え！？マイコ様も見逃しましたか！？
たぶん、私の場合はもう見れないでしょう（泣）。いいんだ～、コミック読むから。マイコ様はあきらめないで見つけてください。頑張れ～！！同士よ～！！
同士と言えば、マクガイヤー大統領候補を思い出しますね（ザ・シークレットサービスより）

＞[7916] 里奈様
どうも、“夜更かしプリンスいちの”です☆
実際に家にりょうちゃん＆香のコンビがいたら相当にぎやかでしょうね。うるさいくらいですかね（笑）。１度は一つ屋根の下に暮らしたいものです。冴子に誘惑されてみたいものです。

＞[7919] 将人（元・魔神）様
ＨＰにお邪魔させていただきました。
マジンガーシリーズっていっぱいいるんですね！！私はゲームとかあまりやらないので、スーパーロボット対戦（？）は分かりませんが（汗）、前、深夜にやっていたアニメは見ていました。（ゲッターロボだったかな？）
阿香と海坊主の会話はほんとにありそうで面白いですね。いつかそんなシーンが現れるかもしれませんよ。

＞[7920] 千葉のOBA3様
昨日はまた、映画「サイン」を観てきました。ネル・ギブソンです。・・・だからどうしたと申されますと、それまでですが・・・。
リベンジ待ってますよ（余裕）。逃げも隠れもいたしません。英国紳士ですから（笑）。
それと、息子さん、お誕生日おめでとうございます☆★☆

＞[7921] TAKESHI様
兄妹でＣＨファンなのは、うちも同じですよ。私が読んでいるのを見て読みたくなったらしいのですが。私だったら兄妹で書き込みはできませんね（汗）。やっぱり、仲良いんですよ。それとも、仲良いのはＣＨ＆ＡＨの時だけなのかな？


### [7934] ニャンスキ	2002.09.23 MON 17:48:07　
	
こんにちは。御久しぶりです。
学校が始まりとても慌ただしい日々です。

ＡＨの方はちゃんと読んでます。
北条先生はバイオリン好きなのでしょうか。私もバイオリン弾くので出てくると嬉しくて細かく見ちゃいます。
以前ＣＨで出てきた時は弓の持ち方の図が間違っていた気がしますが夢ちゃんは正しいもちかたになってて良かった！

＞千葉のOBA3様
マ～マ様ごぶさたです！お元気ですか??
レスしようしようと想いながらなかなか来られませんでしたもう随分前のことですがエールを有難うございました！
大変なことの方が多くて今も弱音が出そうですが、
人生をより良いものにするための勉強ですので…がんばります。通学も勉強も友達も全てに学ぶ事はありますよ。

＞たま様
千葉のOBA3様のカキコ読みました?
やさぐれあざらし…！！笑！
たま様は不良あざらしだったのですか??だから海なんて捨てて川へ?
最近新しいあざらしが出てきましたね～ウタちゃん。
可愛かったですよ。ライバル出現ですね??

我读 AH 有一段时间了。
不知道北条先生是否喜欢小提琴。 我也会拉小提琴，所以当小提琴出现在我面前时，我很高兴，并细细地看了起来。
我觉得之前在 CH 中出现的拿弓方法图是错误的，但我很高兴夢-chan 掌握了正确的拿弓方法！


### [7933] やすこ	2002.09.23 MON 12:37:45　
	
＞［7902］将人さま
ドラマティックマスター２の情報ありがとうございます。
とっても興味をそそる内容なので買おうと思います
これで誰かが先に買っていたら泣きますよ！

＞［7928］もっこりさま
初めまして。私もまだ数回しか来てない新入りです。
「セイラ」の回は私も大好きですよ
あのキスシーンは見てるこっちがドキドキしちゃいました。
昔撮ったビデオは何度も見ているので
トラッキングがすごくて画像が乱れまくってます。

ビデオは何本あるのか数えたら、8本ありました
スペシャルも合わせてこの数なので、
きっと消しちゃった物やとり忘れちゃった物も
あるかも・・・

＞［7930］里奈さま
最近Angel Nightを口ずさんでます
一度頭のなかに入るとなかなか消えませんよね


### [7932] もっこり	2002.09.23 MON 07:33:46　
	
☆7930　里奈様
挨拶ありがとうございます。ビデオ状態の表現の上手さに口元が緩んでしまいました。確かに何回も見すぎて画像が凄いことになっています。でも見てしまうんですよね。でもやはり見すぎてＣ・Ｈ1の最期の方（レッドスコルピオとの死闘だったかな）はテープが天に召されました。残り10本いつまで持つかしら…　

### [7931] Kathine	2002.09.23 MON 04:36:29　
	
＊Hello＊
私はKathineてす,私は香港に住んでいる .日本語を少し話すことができる.このwebsiteを愛する.私は常に再度来る。^_^

### [7930] 里奈	2002.09.23 MON 00:59:54　
	
☆７９２７　マイコ様
あはあははぁ～♪
３時間の捜査の末見つからなかったビデオは仕方ないので今日は諦めて…里奈ちんたらさっき書き込みした後またレンタル屋行って『愛と宿命のマグナム』借りてきちゃいましたぁ♪これは昔見たけどねっ。とりあえず今夜は落ち着けそう（笑）

☆７９２８　もっこり様
はじめまして（＾０＾）／
夜更かしクイーンの里奈ちんでっす！以後よろしくです♪
最近一日中一人でゲットワイルド口ずさんでます。もちろんリョウと香のキスシーンを思いだしながら…。あのタイミングはほんと鳥肌たっちゃうくらい最高ですよね！何回の見過ぎてテープが擦り切れそうです（＞_＜）男の部屋に隠されているアダルトビデオ並に画像が乱れまくっちゃってます（笑）

### [7929] OREO	2002.09.23 MON 00:19:35　
	
もしや日本ってまた３連休？？いいな、いいなぁ～
ホント、カキコ読むのタイヘン・・・Ｌｏｇ　Ｌｉｓｔ良く気力が今日はありません・・・ホント大量です☆

ＡＨ、「すばらしき友人たま様」よりスキャナで送ってもらって読みました♪
感想は皆様と大体一緒ってことで（手抜き）
でも、今回、僚パーパの顔半分アップがちょっとカッコヨカッタ～ドキドキしちゃいました・・・
あと、僚の髪の分け目が一コマだけ変わってたと思ったのは私だけですかね？？

＞７９２５葵様
涙がでるくらい「すばらしき友人」っすよたま様は♪やっぱ、指詰めは、覚悟すべきでしょ？（悪友どうし？：笑）

＞７９１０たま様
や～っぱ指詰め覚悟っすよねぇ。いつか執行ってことで・・イヒヒ（私が一番デビルだったりして・・・笑）

ＣＨのビデオかぁ・・・いいなぁ、いいなぁ・・・
私もお家帰って見たいよぉ。こっちじゃ、まだＣＨやＣＥはやってないと思います。でもそれは普通のローカル版のＴＶの話・・・ケーブルテレビをつけたら３００チャンネルくらいになるので、そっちだったら、いろいろやってるかもしれません。うちは予算がないのでフツーの６０チャンネルくらいまでのしかないんです。アニメ専用のチャンネルはそのうち２つ。一つは幼児むけであんまいいのやってません。もう一つのほうで色々やってます。ケッコウ最近のもやっててビックリしました☆

それではまた☆

### [7928] もっこり	2002.09.23 MON 00:06:15　
	
　みなさん、はじめまして。私はＣ・Ｈファン歴10年ほどの者です。ここの掲示板で一緒にみなさんと盛り上がりたいと思います。度々訪問させていただきますのでよろしくお願いします。ちなみに現在、ビデオに撮っておいたＣ・Ｈのビデオ見まくっています。もうじきＣ・Ｈ２の「セイラ」の回。私が一番好きな話です。ゲットワイルドのタイミングが最高です。あれほど唄と映像がマッチするアニメは自分の中ではなかなか無いです。っと初めての投稿で長い文章になってしまい申し訳ありません。

### [7927] マイコ	2002.09.22 SUN 23:58:24　
	
＞「７９２６」里奈様　ビデオもなければ、CDもなかったんですね・・・。この嫌な気持ち、伝わってきます。本当にお気の毒です。がんばれ！里奈さん！

### [7926] 里奈	2002.09.22 SUN 23:41:16　
	
☆７９２４　マイコ様
☆７９２１　ＴＡＫＥＳＨＩ様
Ｃ・Ｈのビデオを借りに行くのが待ち遠しくて仕事中ずっとソワソワしてました。んでもって猛スピードで地元に帰って来たんだけど…。無い！無い無い！なぁぁぁーっい！！『シークレットサービス』がレンタル中どころか置いてなかった！悔しくて他店にも行って隅々まで探したけど扱ってない！
　なんで！？どぉぉーしてぇぇぇーーっ！！？
　　うがぁぁぁーーー（￣□￣；）
地元の店は３時間かけて全部まわりました…。
中古販売の店もダメでした…。仕方なくＣＤを借りようと思ったのですが、これまた全然扱ってない…。すごい集中力で探したら全然ジャンルの違うところに『ドラマティックマスター２』を発見！ところがどっこいレンタル中！もう嫌…（泣）少し前まではもっとたくさん扱ってたのに、新装開店前のセールで売れてしまったらしです。これから先Ｃ・Ｈを借りる為には車すっ飛ばして遠出しなきゃいけないのね…。
負けないわよぉぉー！次の休みの日はＣ・Ｈ探しの旅に出てやる！あぁ悔しくて眠れない…

### [7925] 葵（カウントダウン！）	2002.09.22 SUN 21:43:09　
	
　最近みなさまのカキコがおもしろくって、すごいログですね。前日の自分のカキコを探すのが大変です。(^_^;

＞ｃｈｉｋｋａさま［７９０７］
　本日も屈辱で、あいすみません。（笑）敵として登場の美樹さん…イイ考えだと思います。腕利きの傭兵時代でしたからね。敵に回したらコワイ女性ですよね。（めずらしく下ネタの大先生に、ちょっとビックリです☆）

＞たまさま［７９１０］　
　指詰め？！そ、そーゆーこと言うから「悪友」だってば…。(^_^;でもＯＲＥＯさまは感激でしょうね。うん、えらいえらい。誉めてつかわそう♪

＞将人さま［７９１９］
　ドラマティックマスター、イイんですよね♪あんなに生活感あふれるＣＤ、他にないでしょう。(^_^;

＞千葉のマダ～ム［７９２０］
　チビちゃんお誕生日おめでとうございました♪３タテですみません。いよいよカウントダウン、入らせていただきます！

### [7924] マイコ	2002.09.22 SUN 13:59:25　
	
石川県風がとっても強いです！雨がふりそう・・・。あれ？時間ちがうぞ？

### [7923] マイコ	2002.09.22 SUN 08:36:40　
	
＞「７９２２」りえ様　そうですね。私も最後はギバちゃんとくっついてほしかった～！！　　　　　　　　　　　　　　
＞「７９１６」里奈様　「ザ・シークレットサービス」借りました？

### [7922] りえ	2002.09.22 SUN 08:06:47　
	
>7908 マイコさま
私も女だと思います。
そして、あんな微妙な終り方じゃなくて、ギバちゃんとくっついてほしかったです・・・・。

### [7921] TAKESHI	2002.09.22 SUN 07:56:45　
	
おはようございます。昨日、ロードオブザリングとCHの愛と宿命のマグナムレンタルしてきました。

＞[7903]葵様
お二人で書き込みできるといいですね！ではお仕事がんばってください！

＞里奈様
「ザ・シークレットサービス」借りれましたか？

＞[7910]たま様
兄妹揃ってCHファンなもので（スミマセン）結果的に二人ともここに書き込みするようになってしまいました。

＞[7913]いちの様
仲良しに見えるようですが結構ケンカしてますよ（笑）気が合わないことも多いですし。でもCH見るときは一緒に見てます。

＞[7920]千葉のOBA３様
息子さんたちも好きなんですか！やっぱおもしろいですもんね。
