https://web.archive.org/web/20020521233620/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=218

### [4360] ちゃこ	2001.11.07 WED 18:46:52　
	
さて、解禁！ということで、感想文です。（ごめんなさい「また」長いです。）
全体的な感想は、辛口もあるので、また次回（←それ以前にヒンシュクだって）
ラスト３ページに集中して感想文です。

「今週号の感想」
差し出す手。思わず出てしまった言葉。「リョウっ！！私に銃をかしてっ！！」
本当、思わず出てしまったんだと思う。出来るなら静かに見守っていたかった。
でも、見てられなくて。口に出さずにいられなかった。そんな気がしました。
自分がいなくなってからの、リョウの生活を目の当たりにした感じかな。
「私のいない間、あんたは仕事もせず、何してたんだぁ～！！」
ハンマー振り上げたい位の苛立ちと歯痒さ。泣きたいくらいの切ない思い。
「いなくなっちゃって、ごめんね。死んじゃって、ごめんね。」
存在する世界の違いを感じながら、でも、改めて強く思った気がする。
「リョウを絶対死なせない」と。「もう、独りになんかさせない」と。

ところで。あの言葉は、GHも言おうと思っていた言葉だった気がしました。
あの瞬間、２人はシンクロしてたんじゃないかな？
（香の想いが強かったから「リョウっ！！」という呼びかけになった気が）
ガラス越しに映る香。アルカイック・スマイル。本当「菩薩様の微笑」みたい。
それを見つめるGHの表情も、何だか可愛くて。「お母さん」見つめる感じ？
リョウとGHが１つの関係を作れたように。香とGHもやっと思いが通じ合えた。
そんな瞬間だったように思います。
だからきっと。２人分の思いを込めて言ってくれたと思う。
「あなた・・・守るため・・・！」と。

そして、リョウ。不意を突かれて、崩れるポーカーフェイス。
心の隅に閉じこめた想いが溢れ出す。
「記憶なんて曖昧な存在じゃなく、香は今も、GHの中で生きている・・・」
もしリョウが、もっと言葉を発していたら。もし香が、返事をしていたら。
リョウの理性は、吹っ飛んでいた。GHを「香として」抱きしめていた。
香の理性も消え失せて、抱きしめられていた。そんな気がしたギリギリの表情。
ポーカーフェイスを取り戻したリョウは、今、何を思うのだろう。
「やっぱり、ただの偶然だよな・・・」そんな風に思いたいのかな？
あるいは。「香もそんなこと言ったっけ・・・」思い出して、懐かしんでる？
それとも「生きているなら、確かめたい」のかな？
「銃」の事を考える。「確かめたい」なら「香の銃」を渡すのかな？なんて。

改めて思う。「リョウの心を揺らすのは香だけ」と。そして、いつも思うこと。
リョウにとって「香が生きている」事は、良いことなのだろうか？
（読む私は「その事」に支えられていて、リョウにもそうであって欲しいけど）
結論は、今日も持ち越し・・・では、失礼します。

### [4359] あゆゆ（fromFUKUOKA)	2001.11.07 WED 18:30:49　
	
　初めまして、あゆゆと申します。ＡＨは１４話から読み始めて今までずっとＲＯＭ専でしたが２３話を読み、ついに顔を出してしまいました。
　解禁になったので早速ネタバレ話を。
今週のリョウ、銃の腕は鈍ってなくても肉体が鈍ってましたね・・・。あんなリョウを見るのはツライけどこういう展開でもなきゃＧＨが動かないんだと考えるとこれもアリかなって思えちゃいました。（ギャグっぽい感じだし）
　それと、突然現れた香に驚くリョウ。驚いたのは香の姿が見えたことに対してかな？「銃をかして」と言う香は、リョウにとって珍しいことではないのかも知れないなと思いました。過去にさんざん銃火器を占領してたし、香ちゃん^-^;
リョウがなぜ、と訊いたのはＧＨに対してなんだろうな。
　香の、リョウを守りたいという意思にはすごく強いものがあるんだなと今更ながら思いました。香の意思にＧＨが引きずられているだけなのか、今後の展開が大変気になるところです。ＧＨに笑みかける香も意味深・・・。
　それにしても差し出したＧＨの手、助け起こすためじゃなく銃を受け取るための手かい、と思わずつっこんでしまう私・・(^^;
　お初で長々と書き込んでしまいました。乱文お許しを。
たまにしか出没しませんが今後ともよろしくお願いします！

### [4358] まろ（誰だチミはってか？）	2001.11.07 WED 18:24:13　
	
みなさん、こんばんは。
まず、はじめに訂正です。先のカキコ[４３５３]にてミニの生みの親である『アレック・イシゴニス』氏を、誤ってアレック・イゴニシスと表記しておりました。失礼致しました。

さて、今週号の感想です。
文章表現力の乏しいボキでは、ちょっと不自然な箇所も出てきますが、そこはご勘弁を。
　
　凄い！凄過ぎる！！（ショボ過ぎやなァ・冷や汗）
ＧＨのナイフでの降下アクション！
（ボキは、てっきり外壁の広告を一部裂いて、『０○７（お～っと伏字になってない！）』ばりに降下するのかな？って思いましたが、ちょっち違いましたネ）
　そして、極めつけは「銃を貸して」というＧＨ（香）のセリフ（号泣）
リョウの『動き』の不甲斐なさ（空白の１年間のブランク）を叱咤する、優しくも強い姿に、ああ、香もすっかり「大人の女」になったんだナって染々思っちゃいました。
（そういえば、私、もうすぐＡＨの香と同い年になるんだよナ。ボキはじぇんじぇんお子様なのに・汗）

### [4357] あお	2001.11.07 WED 16:00:09　
	
解禁までカウントダウンになってまいりました。残りあと２時間30分！！解禁後が楽しみです。

＞トスク様(4325)
レスありがとうございました。とても嬉しかったです。ちゃこ様(4322)や、無言のパンダ様(4336)、おっさん様(4347)がすでにおっしゃっていることですが、私も｢売りに行く｣発言やそのままドロンさんの書き逃げ状態に腹を立てておりましたから。一言の謝罪もなしとは･･･恐れ入りますよ、ほんと(怒)。だから、トスク様が｢それは違う｣というようなカキコを見て、それに答えを返してくださったこと、とても嬉しく感じています。AHを読むにせよ、読まないにせよ、ここではCHの話題、大盛り上がりを見せていますから(笑)、いっしょに楽しみましょうね♪
メッセージ＞
たぶんHPに載せることも考えられたと思います。ただ･･･すべての読者がHPを見ているとは限りません。見ていない人にも、同じように｢知る機会｣を設けるとなると、やはり紙面、特にコミックスが一番であると思うのです。バンチじゃあ･･･書く場所ないし･･･。編集側、というか販売部？の思惑もあるでしょうし(笑)。最初からパラレルと思っていれば、確かにこんなに辛い思いはしなかったと思います。ただ･･･最近思うのです。最初からパラレルと分からなかったからこそ、今こんなにAHの世界に引き込まれているのだな、と。最初のころずいぶん感情を移入して読んでいました。そのまま引っ張られちゃったんですよねえ･･･。パラレルと分かっていたら、多分これはこれ、状態で、あまり感情移入しなかったかもしれません。
CH続編＞
読みたいな、という気もあるんですが、あくまでも、終了直後の絵のままで続けてほしいのが本音。あれから10年、北条先生の絵も変わって当然です。今から｢あの頃の絵で描いてほしい｣なんて無理ですから、あのまま終了でいいと思っています。結構きれいな終わりかただと思っているし。もし続編が出るとしたら･･･ノベルズ希望ですね(笑)。

＞ちゃこ様(4342)
レスありがとうございます。最近、たれパンダな日常にこげぱんの気分です(救いようがないな･･･(泣))。論文の中間発表は来週。それまで･･･とにかくがんばります、はい(泣)。
Enyaは大好きですよ！！中学校の頃からのファンです。Coccoも好きですが、リラックスできるような曲調ではないんで(笑)。ささくれ立っている気分のときや落ち込んでいるときはEnyaをよく聞きます。彼女を知っているんでしたら、Adiemusってご存知ですか？？ジャケットはイルカです。きれいな青色のジャケットなんです。機会があったらぜひ聞いてみてください。あれもかなりのリラクゼーション･ミュージックです。

＞Parrot様(4344)
レスありがとうございます。おお、今回は早く返せた(嬉)。
確かに！！ミックも登場してほしい！！でも･･･香さんが死んでしまっているからな･･･かつての初恋(間違ってないよね(笑))の人でしょう。その幸せをひょっとしたら一番に望んでいた人だから。出てきても･･･読んでいるこっちがさらに辛い･･･。難しいですねえ･･･。
あ、オフ会、時期が合えば参加希望です！！とりあえず･･･私論文あげない限り遊べないんで(号泣)。

### [4356] 無言のパンダ	2001.11.07 WED 15:04:29　
	
こんにちわ。寒いです(*_*)夏の太陽は嫌いなんだけど
今は、その太陽がちょっぴり恋しいです。

今週号のバンチ、三毛猫様が「絶対買い！」とご推薦なので
またまた購入してしまいました。
これまた、ちゃこ様ご推薦（？）のファ＠リーマートで、
ダブルシューと共に！
バンチとシュークリームどちらも、おいしかったです)^o^(

昨日初めてオークションというものを覗いてみました。
今まで「オークション」と言えば「ぼったくりが横行している世界」という勝手な偏見を持っていたのですが、目からうろこの、見てるだけでも楽しい世界でした。
まだ手を出す勇気はないのですが、私が宝物として大切に保管している少年ジャ＠プ全プレのＣＨテレカ（北条先生書下ろし）が、多数出品されていたのにはショックでしたねー(~_~)
え～なんで売っちゃうの～？！って感じ。

＞Ｐａｒｒｏｔ様（４３４３）
＞Ｋ.Ｈ様（・・・・？）
ありがとうございます(__)
お二人の寛大なお気持ちに甘えて、これからも私の頭の中で混同してしまうかもしれませんが、どうかお許しください(^_^;)

[個人レス]
＞Ｐａｒｒｏｔ様（４３４３）
送迎バス！？それはありがたいです（笑）
うちの娘も私と出かけると「遭難する」と言ってビビるくらいなので・・・(~_~;)

＞Ｋ.Ｈ様（４３５２）
ハイ。正確には「お見合い」ではないんですけどね。
（個人的な事は置いといて）「幸福」の価値観は人それぞれだと思うんですが、私が「恋愛結婚」にこだわるのは、ある程度年月を経た時に「あの頃は～だったね♪」的な会話が出来るとこなんです。他にも色々ありますが、それが主な理由かな？
どうでもいいように思えて結構大切なポイントですよ、コレ。

たった今電話で、娘が学校で捻挫をして、太い足が腫れて、いっそう太くなっている（娘よゴメン！）という連絡がありましたので、これにて失礼します。

### [4355] ちゃこ	2001.11.07 WED 14:40:59　
	
こんにちは。解禁までは、まだまだまだ・・・（笑）
でも、皆さん「気分はフライング」そんな感じですね。さて、レス等。

「カレンダー」
本日到着しました。う～ん、やっぱり「縦位置」のイラスト小さいよぉ（涙）
「２ショット」に拘らず「横位置」で統一すれば良かったかな。ブツブツ・・・
（あ。「沙羅ちゃん」のイラストは入れたんだった。あれは可愛くて好きです）
思うところはありますが、パソコン周りに２人のイラストが増えるのは嬉しい。

＞K.H様「4349」　sweeper様「4354」
こんにちは。もう、昨日からB'z聴きっぱなし（笑）のちゃこです。
「もう一度キスしたかった」「夢見が丘」両方今の気分です、はい。
が。現在、聴いているのは、買ったばかりの「FRIENDS」
「いつかのメリークリスマス」「どうしても君を失いたくない」
稲葉さんの声、好きです。バラードもロックも聴かせる。その魅力に「降参！」

＞Ｐａｒｒｏｔ様「4343」
こんにちは。最近、皆さんの間で「造語」流行ってて、何だか楽しいです。
貢献者のあなたを、当BBSの「企画・開発課長」に任命したいと思います（笑）
で、色々レス。先ずは「懸賞当選おめでとう！」（←いつの話題だ（大汗））
「絵が描けて羨ましい！本当、送った絵、見たかったです」（←更に前？！）
「軍曹、身体検査しちゃうー！」ナイスでした（←これは、最近・・・かな？）

あと「オフ会」良いですね！やっぱり「関東地区」「関西地区」に分けるのかな？
どなたか書いてましたが「CH＆AH現場見学（？）会」＆「コアミックス訪問」
とかあると面白そう。何より、皆さんと直接お話出来る機会。というのが
楽しそうですよね。
今から徐々に企画して「祝☆AH連載終了記念」とかに実現出来たらスゴイけど。
（まだまだ、考える時間、たくさんありそうですものね）
大抵の場合「見かけと中身が違う」「普段と撮ってる写真の雰囲気が違う」と
散々な評価になってしまう私は、ベールの向こうにいるのが、
世のため＆BBSのため・・・って気もしますが(^^;;)　想像すると楽しいです。

さて、昼の部、終了（笑）家事に戻ります。解禁後、お会いしましょう。

### [4354] sweeper	2001.11.07 WED 12:29:29　
	
こんにちは。寒くなってきましたね。

[カレンダー]
やっと届きました！ついさっきですが。
まだ中身をよく見てないので、じっくり見ようと思います。
今はうれしいです！

[レス。]
＞K.H様。[４３５２]
「いいから背筋を伸ばせーーー！！」
「い、いーやーだーあーーー！！」

レス、ありがとうございます。「冴子ちゃん借りを返しちゃうー！」です。「都会のシンデレラ」ソニアの話の伏線にもなっていてすごかったです。そしてその話が遙ちゃんの話に少しだけつながっていたのも驚きです。そういえば、美樹さんから見た２人のあのデートも意外だったのかもしれませんね。
「もう一度キスしたかった」今の季節にピッタリですね。稲葉さんの歌声が切ないです。「夢見が丘」も捨てがたいです・・・。

「りょう、あたしのせいで死んじゃうかもしれないものぉ！！」

＞Parrot様。[４３４３]
はい！完全復活しました！！
レス、ありがとうございます。「冴子ちゃん借りを返しちゃうー！」です。地下射撃場。探してみたところ、「RN探偵社」との「愛のトンネル」らしき入り口を見つけました。(笑)時々麗香さんがこの通路を通って射撃練習に来ているようです。(笑)
私も時々ここの地下射撃場を使わせてもらってます。冴羽アパートに住んではいませんが。(笑)
冴子さん。ＡＨでもＣＨでも強さがありますよね。でも、ＡＨの冴子さんはＣＨとまた違った強さだと思います。ＣＨでの強さが「正義や悪に屈しない強さ」なら、ＡＨでの強さは「人間的な強さ」が出ている気がします。りょうと冴子さんは、「愛する者を失った」という点は同じなんですよね。だから、冴子さんはりょうの気持ちがわかるから反対に「これ以上大事な人を失いたくない」というのが冴子さんの気持ちかもしれません。槙村兄と香。りょうと冴子さんにとって大事だった２人。２人の存在はそれぞれに大きかったんですね。
まとまりなくなりました。すいません。

それでは、これにて。

### [4353] まろ（じゃあ、おまーは誰だ？）	2001.11.07 WED 12:02:37　
	
おはようございます。今日は一段と冷えているみたいです（み・・皆さん、さ・・・寒くてエラいズラ？←甲州弁）

[ミニのお話し]Ｋ．Ｈさま、ちゃこさまには蛇足っって感じですが（苦笑）
　う～ん、私なんか最近まで（とはいっても、３年ほど前ですが）ミニとミニ・クーパーの違い、判りませんでした（苦笑）
　じゃあ、ドコが違うのか、といいますとその『生い立ち』が違うんですね。ミニは１９５９年、英国人のアレック・イゴニシス氏の手により、送り出された４人乗りのＦＦ（前置きエンジン・前輪駆動）車をいいます。これに対し、２年後の１９６１年、彼の友人であるジョン･クーパー氏等が、ミニの高性能バージョンとして送り出したクルマ・・・それがミニ・クーパーです。
　そして、先代ミニ（２０００年９月を以って生産終了。現行型はＢＭＷ製を予定）の最終生産型の見分け方は、といいますと・・・
　フロントグリル（左右のヘッドライトの間にある開口部）両端に『丸型フォグランプ（霧灯）』があり、かつ、ボンネット両端に『白い帯』（の塗装）が入っているのが、ミニ・クーパー。
　そうでないのがミニ、となっています。
（それ等を考慮すると、リョウの乗っている車、恐らく（？）ミニではないか、と思います←年式が判らないので断言しない方がいいかな）

[４３４８]ｋｉｎｓｈｉさま
　こんにちは。ゴ、ゴキちゃんですか（なんともはや、ハハ）
なんだか牧原　友佳さんの黄色い悲鳴（正確にはその妹・こずえちゃんが『にせゴキ』ちゃんのイタズラをした時の）が聞こえてきそうで・・・そんな時は、たくましいリョウ（註：「ＮＧ」ではない方の）に助けてもらってください（微笑）

[４３４５]ちゃこさま
　レス、ありがとうございますレス、ハイ。
高校生時代、漫研だったんスか。当時、私もなりたかったクチなんですが、何せ『美少女アニメ系命！』みたいな空気があったので（苦笑）ちょっと近づけませんでした（こ・・・怖かったっス）でも、独学ですが好きで描いてましたネ（中学ん時、美術は５段階で”２”を記録。それにヘコんだ私は、高校では選択科目で美術をやめ、音楽を履修）学生のとき、岡○（石○寄り）の脇の道を少し北上したとこの画材屋さんにＧペンとか買いに行きました（でも、結局使いこなせかった・涙）今は、Ｍａｃで細々と（？）下手な絵（のレベルなのかなァ）を描いてます。

[４３５２]Ｋ．Ｈさま
　レスありがとうございます。
実は私、（一時期ですが）ヒデの出身校のある、韮○市にも住んでたことありますヨ（笑）駅前のイトー○ーカ堂、良く使ってました。
あ、そうそう。あの近くの舟○橋（国道２０号線と５２号線がぶつかるトコ）、ヒドい渋滞に泣かされました（笑）まぁ、甲○市街、平○通り周辺も最悪でしたが。

それでは、哀愁の意を込めて、

「・・・マチルダさん・・・・マ・チルダさぁぁぁぁあ！？」
　　　・・・ズゥゥゥン！！
「どうです？９０年代最初のハンマーの味は！？」
「く・・・苦汁（９０）の味がします・・・」

### [4352] K.H	2001.11.07 WED 01:31:12　
	
　お待たせしました！！多分、ひじょ～に長いレスになります！
　正直、今のボキの気持ちは「（野上）父の喜び全快、もとい全開！！」です。

＞琴梨様[4324]
　「ほら、股間の銃！よく見ると右に曲がってるでしょ！（内容　が内容のため、自粛）」
　「さ・・・冴羽さ・・・ん・・・」
　「いいかげんにしろ！何考えてるんだ、おまえは！！」（煩悩退散！）

　は～い、メッセージ有難うございっ！でもボキ、予備校に2年も通ってました！おそらく2歳年上です、ボキのほうが。
　大学での学年は同学年で、卒業した年も一緒ですよ、琴梨様とは。

＞無言のパンダ様[4327]

　「あ・・・あたしはこう見えてもまだ40前なんだよ！！」
　（リョウちゃん、反省！）

　はいはい、30歳までマジック③のK.Hです！ひょっとして、お見合いされたんですか？でも、幸せな家庭・家族を築き上げていらっしゃるんなら、問題ないとボキは思いますよ！
　「母親」や「女房」がいないと、家族として成り立たない、という世帯が多いのが日本の家族社会の特徴ですが、その存在意義、やはり必要不可欠なんですし・・・
　なんか、独身者のボキが偉そうなこと抜かしてすみません。

＞まろ様[4328]

　「真紀の目をあざむくための変装だ、よく見破ったな・・・」
　「見破らないでか！どこの国にサングラスかけたカーネルおじ　　さんがいるんだ！」

　そ・・・そうでした！赤頭巾ちゃんバ－ジョンと、アヒルだか鴨だかを頭にのせて潜水しているバージョンもありました！
　ちなみに、ボキはあのころから海ちゃんファンになりました。

＞まみころ様[4331]

　はじめまして、BBSに参加してまだ（本当に）一ヶ月とちょっとのK.Hです。「SWEET PAIN」、やっぱいいっすよね！！
　「自分以外のあの人・・・」、ちょっと昔を思い出してしまいます。ちなみに、その思いに憧れていたボキ、学生時代には彼女は作れんかった・・・。まあ、いい思い出ですけどね。って、今もそうですが・・・

＞sweeper様[4334]

　「お前・・・らしいな・・・でも、凄いよお前・・・こんな決　　断をするなんてな・・・」
　「リョウ？」

　ども。メッセージありがとうございます。「海ちゃん、ボン　ッ！」です！
　そうです、あの締めくくり、非常に意味深かつ切なさを感じさせるものなんです。しかも、その直後の海ちゃんとの決闘への複線にもなっているから、さらにすごい！
　あの話の連載当時、「C.H」終了説が流れたのは、なんだかうなずけますよ。

　「ホンマに終わらせたろか・・・」

＞我が分身Parot様[4343]

　「初号機を起動させる」
　「問題ない、予備のパイロットが届いた・・・」

　はいは～い、他の作品からも引用している常習犯、K.Hですぅ！
　そういえば、このBBSのパーティ－、ボキももちろん賛成ですぅ！場所はもちろん、「C.H」にちなんで新宿ですよ、新宿！
　歌舞伎町も候補地ですが、いかんせん治安が・・・というわけで、北尾ちゃん歓迎会と送別会くらいの日程で行いたいくらいだ、と思っちゃいます！（無謀ですな・・・）
　
　「ザクとは違うのだよ、ザクとは！」

＞ちゃこ様[4345]

　「デビッド・クライブ・・・地獄のテロリスト・・・」
　「リョウ、知っているの？」
　「・・・知らん。」

　現在もニアミス中のK.Hですぅ！今日も見ました、ミニクーパー！しかも赤！
　日曜、友達と西の方（N田英寿の出身校があるほう）に遠出したんですが、ある雑貨ショップの駐車場を見たら、ミニクーパーが20台くらい注射、もとい駐車されてました！圧巻でしたね。
　聞くところによると、あのタイプのクーパー、もう生産中止になったとか・・・ホンマかいな！？と思いました。

### [4351] 琴梨	2001.11.07 WED 00:55:21　
	
こんばんは。琴梨です。

今日は寒かったですね～。まさに「六甲おろし」（私はヤクルトファン）とでもいうような北風が冷たい一日でした。

＞ｓｗｅｅｐｅｒ様　［４３３４］　ちゃこ様［４３４２］
　レス、ありがとうございます。ラストの屋上のりょうちゃんのセリフと何とも形容しにくい顔と鈍感な香ちゃん、ふっとこっちが笑ってしまいます。ガラスの靴のかわりの片方のイヤリングがなくても、王子様はシンデレラを見つけられるんですね。ほんとにりょうちゃんってば、不器用なんだか、シャイなのか、キザなんだか、こっちが恥ずかしくなっちゃいます。
　タイトルのお話、気づいてくれている人がいて嬉しかったです♪　音楽とか映画が割りと好きなほうなんで気になるんです。ＧＨのタイトルは誰がつけているんでしょう…？

　あと、扉絵なんですが、ＣＨのコミックス１５巻の「ラブラブ大作戦」の”うちのりょうちゃんしりませんか”ハマりました。”うちのタマしりませんか”みたいで最後の”新宿の香まで”が好きです。

### [4350] クロック	2001.11.07 WED 00:33:36　
	
　こんばんは、クロックです。
　今日は昨日に比べて暖かかったのですが、明日はまた寒くなるそうです。皆様暖かい格好でお出かけを…。

＞Parrot様　[4239]
　いいですね。パーティー…。私は賛成派です。文面では語りきれないことなどもあると思うので、企画はとてもよいことだと思います。私一個人としての意見ですが…。

＞ワタル様　[4242]　sweeper様　[4288]
　ありがとうございます。
　ワタル様には英文までつくっていただいて…とてもありがたいです。得意ならよろしいのですが、苦手だったらごめんなさい。
　お二方の意見を読ませていただいて、「知っていたんだなぁ」と納得することができました。ずっと堂々巡りしていたんです。
「あそこでああ言っているから…でも知っていたらこんなこと言えないしなぁ…」のような感じで。そして霧笛（？）が鳴り終えて走り去っていく香さんの表情と、その後ろ姿を見つめる冴羽さんの表情が限りなく切ないです…。読んでいる時、一緒に切ない顔をしたのを覚えています。

＞Rikayo様　[4272]
　神谷さんと共演されたことがあるんですか？それはとても羨ましいです！私もその場にいたとしたら、きっと同じ気持ちになっていたと思います。ややや…本当に羨ましいです。

＞おっさん様　[4297]
　お疲れ様でした。
　「式」と名のつくものは、大変です。必ずそこに人の目が集まるので…。体力的にも精神的にも疲労がたまります。でもその忙しさの中で、立ち止まってふと「式」の中心人物のことを考えたりするのでしょうか…。月並みな言葉を出してしまいますが、お許しください。「時間が解決してくれる」

　最近富に「初めまして」の方々が多いですね。先生の作品について色々話し合える人が多いのはとても嬉しいです。
　初めまして、クロックです。こちらこそよろしくお願いいたします。
　光子ロケットの話ですが、「みつこ」読みしていました。
「こうし」なんですね。すみません…。先生には遠く及ばずながらも、世間の時間と合わないときがあります。でも私はひねくれているので、「最新の情報を知ることは大切だけれど、それにこだわるのはどうだろう？」と思ってしまいます。ひねくれですね。

　それでは皆様よい夢を…。

### [4349] K.H	2001.11.07 WED 00:31:32　
	
わ～い、レスがいっぱいありすぎるんで、あとでまとめてお送りしま～す！！
バンチ、しっかりもっこり読みました。内容は解禁後にて・・・
でも、これだけなら宜しいでしょう！わくわくしてきた・・・今後の展開、（前からそうですが）じつに楽しみ！！
　
　木枯らし、吹きましたね～！この季節になるとB`zの「もう一度キスしたかった」の歌詞を口ずさんでしまうボキです。
　というわけで恒例の、アレでいったんきります。

　「10年後の再会を楽しみにします！！」（キッパリ！）
　「・・・その・・・八歳の女の子って君のこと・・・？」

### [4348] kinshi	2001.11.07 WED 00:25:36　
	
こんばんは、解禁前になると　やたらはりきっているkinshiです
初めての方こんばんは、よろしくおねがいします
ところで　話し変わるますが、＜～猫だいちゃう～＞　をおもいっきりパクッテ、自分の友達のメールにうれしい表現として、
この世で一番きらいな　ゴキブリをパロッて（ゴキ飼っちゃう～）って使ってみました　もう一年前になろうとしますが、
今年の初夢　壁いっぱいにゴキちゃんはいずっていた夢でした。
特に今年の夏は、５年分の神経と寿命を使ったような気がします
おかげで、一匹もみませんでしたので、なんとか現在に至っています。友達からの顰蹙（ひんしゅく・・おーむずかしい字だ！）
をかうか、うまく　ウケテくれるか、どうでしょう？

寒さも増してきましたね　そろそろ石油ストーブの準備だ！
では、また。

### [4347] おっさん(岐阜県：お疲れモード）	2001.11.07 WED 00:16:22　
	
毎度、久々にいつもの勤務先に戻ったはいいけど（１週間ぶりに）おもいっきりお疲れモードになってしまったおっさんっす。
ということで、今日も皆様いつものご挨拶せ～のおっは～！！今日はレスだけで勘弁して・・・＾－＾；。
トスク様（４３２５）、おばんでやんす。ちゃこ様のレスと重なりますが、私も今もＡＨを読んでくれてるのはありがたいっす。「本を売りに行く」って言った人には正直ショック＋怒りいっぱいだったので、なんかありがたいと思いました。（すんません私って怒ると正直男声になってしまうんっす）
無言のパンダちゃ～ん（番号が・・・）ちょい～す。まじめっていうよりは人からは個性的な人ってよく言われます。もとが暗かったせいかこういう仕事をしたら自分が変われるのではないかと思って始めた仕事が販売の仕事です。いろいろありましたが、今の自分を大切にしたいとおもってる私がいるんです。変なことかいてしまったけどね・・・。
ということで、明日ももちばち仕事なんで今日はこの辺で。そいじゃまた・・・・・。

### [4346] 三毛猫	2001.11.06 TUE 23:52:22　
	
[4326]訂正です。m(*_ _*)
二番目の画材屋の名前は地球堂ではなくて、世界堂だったような気がします。モナリザの看板で有名でした。無くなるときに、新聞の記事に載るくらい、何か遭ったような気がします。今週の「リプレイＪ」の映画館の元ネタ・文芸坐とともに街角の女神でした。（※今は両方ともありません。もしかしたら、新文芸坐で復活しているかな？）

今週の「バンチ」はＡＨネタを抜きにして（解禁前ですから･･･）
全体的に画力がグッと上がったと思いません！？
以前カキコしたのですが、画が固くて読みづらいところがあったのですが、どの作品もキャラクターのバランスも整ってますし、画に動きが出て読みやすくなりました。
もしかして、休みがあったせいですか？
いつもの目次の文句は理にかなっているんですかね。^^;
本当に気が早いのですが、どうしても早く編集部の人に伝えたかったので一言･･･

「感動した！！」（←小泉首相風に）

最初に買ったNo.19を見たとき、220円とはいえ･･･と厳しく見ていたのですが、こんなに全体的に一気に読み切ったのは初めてです。ＡＨと限らず、今週は涙率高い作品が揃っています。

＞luna様[4340]
今週号から他の先生方の作品も見てください。(^_^)
もしかして、バラバラにしなくなったのは、私と同じ事感じるほど、今週のバンチの良さに気付いています？

最近、バンチは買わなくなったとか、ＡＨだけ立ち読みとか言う人多いですけど、
今週号は絶対に買いですよ！！　買い！！
お客さん、損はしませんよ～！！　m^^=

＞ちゃこ様［4345]
嬉しいですね！！“亀”の人ですか！
もしかして、ニアミスしているとすれば、
エル＠ードで売り子をしていた時、見ているかもしれませんね。
あそこでたまに今は無き鯛焼き屋でバイトしていましたから。
さらに船＠屋に次いで文＠堂や＠村屋を押さえて２位の売上を出すほど、店頭販売で目立ってましたし･･･(^^;

### [4345] ちゃこ	2001.11.06 TUE 23:04:27　
	
もう一度、来れました。レスなど。

「先生のメッセージ」
メッセージを読んでふと思った事。「連載は、少なくともあと半年は続くらしい」
８月、９月。「年内くらいで、リョウ＆香の事には＜第一部完！＞といった形で
決着付けて欲しい」と思っていたことを、懐かしく（笑）思い出しました。
時は確実に流れて、私の気持ちも変化しているなぁ・・・（しみじみ）

＞sweeper様
「祝☆復活」ということで。私の方も完調になりつつあるので
アシスタントとして採用していただけるか、お知らせ下さい（笑）

＞まろ様「4337」
レス後半部「教育的指導」で「座布団１枚取って！」って感じですか（苦笑）
そうですねぇ、私も「典型的Ｂ型」と言われる口ですが(^^;;)　
無言のパンダ様みたいな方もいるわけだし、まぁ色々ですよね。
確か、日本人はＡ型が多い記憶が。K.H様の「アジア全体ではＢ型が多い」
という書き込みは、だから、意外だった記憶があります。

＞三毛猫様「4308」
前にも「もしかして？」と思ったことがあったんですが、やっぱり私の実家が
ある辺りにお住まいのようですね(^^;;)　我が家は父の趣味で「流浪の民」の
生活をしてまして、私が高校生の時からそこに住んでおります。（亀のある区）
でもって、地元の公立高校時代は「漫研」でした・・・
（絵なんか全然描けなくて、読んでただけという、お粗末部員でしたが。）
今は「絵の描ける人が羨ましい！」と思いつつ、写真撮ってます。
繋がったら凄いことだ！とビックリ箱開けるような気持ちで、書いてみました。
（何せ、K.H様とは、ニアミスしてるみたいだから(^^;;)）

＞luna様「4280」「4340」
またまたパワー有り難う(^^)/　大分、復活してきましたよ！
そんな優しい人ですから、luna様の懺悔、きっと神様にも届くでしょう（笑）
私も、他の作品、２～３しか読んでない事を告白いたします(>_<)
他の先生方、ごめんなさい！！

＞K.H様（書き込みNo分からなくなってしまった）
私も市内でクーパー見かけるけど、皆、緑色です（もしかして同じ車？）
「ベ＠クラ」は、赤、見かけますか？
そうそう。K.H様の住んでる所からのお客様もいますよ。
（市内でも１番東側にあるせいかな？後は、仕事の関係もあるのかな？）
K.H様も一度くらい、このSCには来たことあるかもしれませんね。

では、いい加減、失礼します。これにて、ドロン！

### [4344] Ｐａｒｒｏｔ（続きです）	2001.11.06 TUE 22:26:14　
	
又、長くなりましたので、僕の[４３４３]のカキコの続きです。

＞あお様[４３１４]
レス、有り難うございます。「海ちゃん、猫抱いちゃうー！」です。レス、遅れても全然構いませんよ。
あお様のお気持ち察します。女性の傷んだ心は、男性と違い深いですからね。かといって、男性が浅いわけではありませんよ。なんていうか傷が違うというか、傷付き方が違うというか・・・。
それにしても、かすみが登場するなら、ミックもかずえも登場させてほしいです・・・。こうなってくると、キリが無いですね。
ほんとに。

＞Ｌａｐｕｔａ様[４３４１]
レス、有り難うございます。「海ちゃん、猫抱いちゃうー！」です。あっ！そうなんですか。Ｌａｐｕｔａのビデオ、出るんですかー。知らなかったです。ごめんなさい。１９曲も入ってるんですかー！多いですねえ！それから、Ｌａｐｕｔａの曲。他に「硝子の肖像」や「揺れながら・・・」が好きですね。
それと、Ｌａｐｕｔａ様のお父さん話、いい味出してます。ほのぼの～です。

皆様、僕のパーティー企画に対し、色々なご反響有り難うございます。「サンチョス、香に一目惚れー！」です。ほんとに嬉しいです。でも、実現してほしいですね。実際に。色々と問題もあるし、難しいと思いますが実現を祈りましょう！！
それから、ここで他のアニメのセリフを書いた事、申し訳ありません。
明日は解禁日ですね。僕の所はバンチ、明日発売なので、楽しみにしてます。それでは、この辺で失礼します。又、現れます。

### [4343] Ｐａｒｒｏｔ	2001.11.06 TUE 22:23:54　
	
どうも、こんばんは。最近、何故か頭の中で「名○偵コ○ン」のエンディングの上原あずみの「青い青いこの地球に」の曲が良く流れているＰａｒｒｏｔです。今でも、流れてる・・・。

[レス]
＞ＣＲＡＺＹ　ＣＡＴ様[４２９６]
レス、有り難うございます。「海ちゃん、猫抱いちゃうー！」です。野球少年だったんですか！僕も少年野球はやってました。それにしても、バッティングセンターって左打ちか左右打ちができるゾーンが少なくありません？僕は、左打ちなんで・・・。
それから、「ＢＢＳの皆様でパーティー？！」企画に賛同して頂き、有り難うございます。やっぱ、皆様に会いたいですねえ。実現、難しいでしょうねえ。

＞無言のパンダ様[４２９８]
レス、有り難うございます。「海ちゃん、猫抱いちゃうー！」です。そんなに胸を痛めないで下さい。僕もＫ．Ｈ様同様ですから。逆にもっこり？！（仲）が深まりましたよ（爆）。だから、気になさらないで下さい。間違っても、全然ＯＫです。
それから、僕のパーティー企画案、夢のようであって現実的という評価。いやあ、嬉しいです。僕なりに一生懸命考えたんですよ。でも、もし実現したら不参加と言わず、必ず参加して下さいよー。あの超方向音痴の良牙君だって、遅いですがきちんと目的地に着いてるんですから。（笑）
う～ん。送迎バスか何か用意が必要と・・・。（書き書き）
それと、無言のパンダ様の漫画ネタが元の「ＢＢＳの皆様、ＡＨに出演！」企画。こちらも考えるだけで楽しかったです。

＞ｓｗｅｅｐｅｒ様[４２９９]
レス、有り難うございます。「軍曹、身体検査しちゃうー！」です。←これ、懸賞当選記念限定なんですよ。だから、元に戻したんです。ｓｗｅｅｐｅｒ様の要望？！にお応えして、又々登場しました。
冴子は不思議な魅力と美貌、色気を持つ素敵な女性。世の中、こんな女性ばかりいれば・・・と思います。女性から見ると、冴子は格好いい、憧れのお姉さん的存在でしょうね。そういえば、ＣＨで登場した冴子の後輩、橘葉月が冴子を慕ってましたね。冴子にとって、槙村兄の存在も大きいですが、リョウの存在も大きく影響していると思います。
ＡＨの冴子は、ＣＨの冴子とは違う強さを持っていると思います。又、良く考えてみると、ＡＨのリョウの気持ちを１番良く理解しているのが冴子なんですね。リョウは香を、冴子は槙村兄を。そして、リョウも槙村兄を、冴子も香を。同じ愛する者同士を亡くしたんですから。だから、香と槙村兄の死、どちらが重いかなんて比べられませんね。何か分け分からん文章になりました。すいません。
風邪治ったんですね。良かったです。それと、面白そうという事は、ｓｗｅｅｐｅｒ様も「ＢＢＳの皆様でパーティー？！」企画に賛成ですね？有り難うございます。
香の銃、使ってないんですかー。なんか、残念のような、そうじゃないような・・・（笑）。それと、「ＲＮ探偵社」に通ずる「愛のトンネル」無ければ、今度掘りに行きます（笑）。
「冴子ちゃん借りを返しちゃうー！」ですか。来ましたねー。造語ブーム到来か！？

＞赤木リツコさんファン兼我が分身、Ｋ．Ｈ様[４３０１]

　「男と女は分からないわ。ロジックじゃないもの。」

レス、有り難うございます。Ｋ．Ｈ様の造語の「美樹ちゃん、感涙！」に対して、「海ちゃん、あたふたしちゃうー！」です。金髪に弱いんですかー。「お主も悪よのう。」（←すいません。）僕もリツコさん、けっこう好きですよ。あの泣きぼくろがいいですね。
綾波レイファンじゃなく、林原めぐみさんファンなんですかー。分かりました。確かに偉大な声優さんですね。歌もオリコン、トップ１０に入るぐらいですからね。凄いですよ。
やっぱり、「リョウのような男になりたい！」ですね。かといって、リョウのような行動はできませんからねえ（残念）。そうですねえ。僕もリョウはロキシア王国のハーレム以来、もっこりしてないような気がします。香に隠れてしてるかもしれませんけど。リョウって、良く考えると、男の本能そのままですね。
僕は、Ｋ．Ｈ様の言う「女に手を出さない勇気」より「女に手を出せない勇気？！」（なんてったって、香がいるから。）では？と思っています。どちらも難しい・・・。

　「カスパーが裏切った・・・・・・母さんは娘より、自分の男
を選ぶのね。」

＞ｋｉｎｓｈｉ様[４３０４]
初めまして。「ＢＢＳの皆様でパーティー」企画に賛同（←ですね？）して頂き、有り難うございます。実現するといいのですが・・・。






### [4342] ちゃこ	2001.11.06 TUE 21:55:25　
	
こんばんは。
本日はお終い。と思っていたから「ドロン！」と書いたんだけど、来られました。
というわけで（？）レス第三弾です。

「都会のシンデレラ」
いつから香と見抜いていたか？琴梨様の書き込みに１票！（＞4324）です。
台詞前半は「香」に言ってる気がするし、sweeper様も書かれてるように
軟派野郎からのフォローも絶妙でしたものね。（＞4334）
でも「１度位、香と、恋人気分してみたい」リョウのそんな気持ちと照れが
ああいう風な流れに持っていったのかな？なんて。
ラストの「イヤリング」の意味に気が付かない、香が「らしくて」良いです。
リョウは香の「鈍感さ（笑）」も愛おしかったんでしょうね。

＞無言のパンダ様「4327」「4336」
こんにちは。「逆続きナンバー」ゲット！のちゃこです（笑）
トスク様への書き込み、補足していただけた感じで嬉しいです。
そう。先生が公言されていたのも、チャンスが無ければ知らないわけで。
その辺のフォローをせずアップしてしまった事、後から気付く私（大汗）

ところで、今までにも何度となく思いましたが「世の母親ってのはエライ！」
私は結婚後丸５年、自分勝手に生活してきましたので、本当に実感してます。
「我が家に「主婦」が欲しい！！」と切実に思った、この１週間。でした(^^;;)
特に土日は、切実に思いましたよ。本当、トホホ・・・な週末だったので。
そうそう。バンチ買いに行ったら「ダブルシュー」という名称だと判明しました。
（↑何だかしつこくて、ゴメンナサイ）

＞あお様「4323」
こげぱんの心境。思わず、納得！大笑い！でした。（ごめんね、大変なのに）
私は大学生をしなかったので、論文の大変さはよくわかりませんが、
「３足の草鞋」の履き替え方が分からなくなると、そういう気分になるかも（笑）
Coccoだとヘビーになると思うので、「エンヤ」を聴いて癒されて下さい（？）
（確かCoccoのネタは、あお様でしたよね？ラストアルバム買ってしまいました）
私の思う「奇跡」が起きるとき、AHでのテーマ曲は「エンヤ」か
hitomiの「IS　IT　YOU？」なのでした・・・
あ。話がズレてしまった(^^;;)　とにかく適当に息抜きして、書き上げて下さい。

では、失礼します。

### [4341] Ｌａｐｕｔａ	2001.11.06 TUE 21:41:53　
	
みなさんお久し振りです。
今朝、「Ｃ・Ｈ」を全巻読み終わった父に
「Ａ・Ｈ」を渡しました・・・・。
毎日、会社に２冊ずつ持っていった父。
食事中、ビデオの「Ｃ・Ｈ」の
『もっこりなシーン』に過敏に反応した父。
どうなったでしょうか？
ピンポーン・・・・。あ、父が帰ってきました。
「おもろいわ。前にＣ・Ｈ見てても、見てなくても
　はまるわ。どう考えたって、おもろい。」
これが５６歳になる父の言葉でした。
続きだとか全く違う世界だとか考えずに、見れたそうです。
そんな見方ができる父は本当に僕の父なのでしょうか？
ＡＢ型だからでしょうか？（僕、Ｂ型）
とにかく、父を見習って、話を正面から
見ようと思いました。

Ｐａｒｒｏｔさん♪
「ミートアゲイン」って好きか嫌いかでかんっペきに
　分かれる曲ですよね♪そう言う所がまたグゥ～ですよね♪
そういえば２１日Ｌａｐｕｔａのビデオがでますね。
ななななんと、１９曲も入ってます！！お買い得♪
