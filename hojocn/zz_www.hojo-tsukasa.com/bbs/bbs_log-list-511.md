https://web.archive.org/web/20031027175553/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=511

### [10220] 無言のパンダ	2003.04.04 FRI 16:39:15　
	
こんにちわ☆

こっちはすでに下っております。雨～～～！

＞たまさま（１０２１４）
ほぉ～！たまさまも就活ですか？！エライぞ～！ＩＴアザラシ！
どんな「荒波」もアザラシなんだからきっと平気さ☆泳ぎきれすよ！がんばれ～(^o^)丿

＞九郎さま（１０２１６）
バンチ０号☆そうそう！私はソレを北条先生の「もっこりメッセージ」に書いてあって知ったんですよ。ＡＨ連載前はこまめに更新されてましたもんね～☆それでさっそく本屋さんに行ってゲットしたんですがＡＨの内容について詳しくはわからないまま、当分バンチも見てなかったんです(~_~;)その後、ＢＢＳで内容を知ってギョギョォ～？！って感じになり、コンビニでバンチと出会い、今に至るわけですが結局ＢＢＳのロム組を３ヶ月くらいしてました(^^ゞそれくらいココに入ってくるのに勇気が要ったってことですよ！まるで縄跳びを飛ぶのになかなか入れないみたいみたいに・・・。九郎さまもそうでしょ？（笑）

＞ｋｉｎｓｈｉさま（１０２１９）
「愛と宿命のライバル」ブームが去っても（ブームかよっ！）きっとまた葵さまのことだから新しいネタを提供してくれることでしょう♪まだまだ当分笑えそう(^○^)

ではまた～(^_^)/~

### [10219] kinshi	2003.04.04 FRI 14:44:31　
	
こんにちは、天気は下り坂？

＞１９９　　千葉のＯＢＡ３さま　・　葵様
何？習＠野ナンバー・・・と！お！恐ろしい～。。。
そんな方に「マダム」の命名とは・・・葵様！違う名前に変えましょう♪
それで　「ライバル」の件　忘れてあげる。（笑）

＞２０３　たまさま
ＣＨとＣＥのフィギア　見つけたんですか？いいな～～～。
どうですか？かっこいいですか？もち、ＧＥＴしたんでしょ？

＞２１３無言のパンダ様
「愛と宿命のライバル」の「呪縛」まだまだ　解かれておりません。
・・・が、↑に描いて書いてある通り　葵様の「ネタ？」しだい・・・と
なっております。（一人爆笑）

では、また。

### [10218] あひる	2003.04.04 FRI 12:16:42　
	
ものすんんんんんごく久しぶりにカキコしてしまいました。ずっと来てはいたんですが、ついつい読みだけになってして・・・。ずっと職にも就かずフリーターしてましたが、去年の10月に就職しました！(^O^)
某書店で働いています。入社してから10キロ痩せました！意外と厳しい仕事なんです。でも楽しくって、楽しくって。入社してから、2回Ａ・Ｈの発売日があったんですが、売れること売れること。買っていく人買っていく人に｢私と仲間ね｣と心の中で握手を求めています。
コミック担当者もＣ・Ｈのファンで語り出したら止まらなくなってます。
どこに行ってもＣ・Ｈファンがいてうれしいなぁとしみじみ思ってしまった。Ｃ・Ｈってすごい(^○^)

### [10217] TOTTI・XYZ	2003.04.04 FRI 02:20:23　
	
北条司先生に質問です。シティーハンターのDVDを出す予定はありますか？

### [10216] 九郎	2003.04.04 FRI 01:20:20　
	
こんばんは！また来てしまいました；

無言のパンダ様
こんな奴のこと、覚えていてくださったのですね；；
ありがとうございます～；；感激です；；

皆様のカキコ見てちょっと思ったこと。
なにげに実は中々の古株なのかもです、私。
ちょっと回想（うろ覚え）…
専門学校通い始めて、すぐの頃、駅のホームで、ＡＨ（バンチ）の看板を発見して、わあい！！北条先生、新連載やるんだ！しかもアクションもの！？？超読みたい～ｖｖｖ
で、本屋にいったが、バンチ？？？って聞いたことないな…；と、うろうろ探していた所、小冊子でバンチ０号というのが置いてあって、おおタダだ！と家に持ち帰り見て、ココの存在をはじめて知りました。
しかし、当時お休み中で、ああ
もっと早くココの存在知ってれば良かった～；；とか思いつつ、毎日見に来てました；
そのうち、ＡＨが始まり、リョウが出てきた感激と香の死を知り、まわりにＣＨ語れる人もいなかったので、ああ゛～この苦しみを…誰かと話したい～！！！とか思ってました。再開したら絶対すぐ書き込みするぞ！と決意のもと、待っていたんですが…うちのパソコさん再起不能に；；
で、１ヶ月ほどたって、新しいパソコさんでココに来たら、すでに復活してまして…ログもグルグルまわってて、思いっきりタイミングをはずし、結局ほとんど見てるだけ～；；な奴に…；たまに書いてもしばらく来れなかったりすると、ちょっとなに書いて良いのかわからなくなったりで、また見てるだけな奴に；

なので（？）私のように見てるだけだけど、昔からいるって人も結構いるんじゃないんですかね（笑）

…ってだからなんだてかんじっすね；
失礼しました！また来ます；

### [10215] chikka(Eastern Shizuoka)	2003.04.03 THU 23:59:04　
	
お久しぶりです。chikkaです。Ｂｌｕｅ Ｃａｔ様、私のことを心配していましたが大丈夫です。このところ少し忙しかったことと、ここのＢＢＳに書き込むためのネタ探しをしていたので書かなかっただけです。これからは毎日とはいきませんができるだけ多く訪れますので皆さんよろしくお願いします。ＢＢＳの方は毎日見ていましたので、誰が何を書いたかは大体分かります。
ところで里奈様がとんだ災難に巻き込まれたそうですが、大事に至らなくて安心しています。実は私事ですが、今春休みを利用して大型自動車免許第一種取得のために自動車教習所に通っています。今は路上の方に出ていますが、大型トラックを運転して分かったのですが、曲がる時の巻き込みの確認やミラーでの注意、それからトラック側からは普通に運転しているつもりでも、車側から見ると煽っている風に見られるなどいろいろな事を発見しました。また、改めて普通乗用車を運転する時に気を付けなければならないことを知りました。昔、運転免許を取る時に習ったことをもう一回復習している感じです。部活関係で２９人乗りのマイクロバスや楽器運搬のためにトラックを運転するので取得しようと思ったのですが、一年後の転職先でもおそらく使うかもしれませんので通っています。今月中には取得できます。

【あれが帰ってくる。しかもパワーアップして】
昨年の一月から三月までＴＢＳ系で放送されていて、私たち「Ｃ・Ｈ」ファンの間で話題沸騰していたあの伝説のドラマ「木更津キャッツアイ」が何とこの秋に映画化されることが決定しました。このドラマのストーリーはもうお馴染みの通り、Ｖ６の岡田准一さん演じる主人公のぶっさんこと田渕公平が余命半年と宣告され、残りの人生を草野球仲間と怪盗団を結成し活躍した話でした。今回はぶっさんがその後生き延びた一年間の夏を中心に話が進められるそうです。そう言えば、「木更津キャッツアイ」がドラマ化されるのを真っ先に知らせたのも私でした・・・。日刊スポーツのＨＰを見たら載っていました。おそらく、明日の芸能欄にまたどでっかく載っていると思います。詳しくは明日の日刊スポーツを見てください。

【那个回来了，而且还增强了力量】  
去年一月到三月在TBS电视台播放，在我们“CH”的粉丝之间引起话题沸腾的传说中的电视剧《木更津猫眼》终于决定在今年秋天拍电影了。这部电视剧的故事大家已经很熟悉了，讲述的是V6成员冈田准一饰演的主人公田渕公平被宣布只剩下半年的寿命，在剩下的人生里，他和棒球伙伴组成怪盗团活跃在舞台上。这次的故事以物产在那之后存活下来的一年的夏天为中心进行。这么说来，最先知道《木更津猫眼》被拍成电视剧的也是我…看了日刊体育的主页刊登了。我想，明天的文艺栏目里应该还会刊登很多吧。详细内容请看明天的日刊体育。

### [10214] たま	2003.04.03 THU 23:31:50　
	
あっ。そういえば、ＡＨの感想って書いてない。明日またバンチ発売なのに。遅々汗々
っという訳で今更ながら感想ぉ～♪

アシャン、会ったばかりの遠山になんでそこまで全てを話すの？
（心臓から何かを感じとったのねっ？やっぱ）
喫茶ＣＡＴ’Ｓでの海ちゃんと僚ちゃんの会話で、僚ちゃんの「そう思っただけなんだよ・・」って言ったときの笑顔・・・。
その笑顔は心からの笑顔？（なんか無理して笑ってるように見えるよぉ～涙）
あの爽やか過ぎる笑顔が逆に切ないんですけど、、、。
香ちゃんはやはり「癒し」。遠山に話しかける香ちゃんの表情に憎しみや怒りはなかったものね。
肝硬変って、肝臓の病気？しかも不治の病なの？（すんまそん。無知で。）
では、また明日、解禁後に。

さてさて、わたくし事なのですが、今日「面接」に行ってきました。（学校が終わったので働くのだ）
でも、今は仕事が無い！本当に無い！無い！ない！ナァぁぁイぃぃぃーーーーっ！（荒波や）
よぉ～～し！世の就職難にも負けず頑張るぞっっっと！

＞のっつぃ様　【10204】
えぇ～っと、フィギアのサイズは携帯ＴＥＬぐらいですかねぇ。両手にすっぽり入りますよぉ～。
残念ながら、僚ちゃんフィギア・・・似てるかっ？と言われると、、、うぅ～ん。悩。
やっぱり平面のものを立体に、しかも、似せて作るのって、難しいのかなぁ～？

＞将人（元・魔神）様　【10212】
すみません。表現が悪かったですね。
香水付きなのはＣ・Ｅだけです。Ｃ・Ｈの香水付きはありませんでした。
（もしかしたらあるのかもしれないけど、その店には無かったです。）

### [10213] 無言のパンダ	2003.04.03 THU 23:29:12　
	
こんばんわ★

春のセンバツ☆広島県代表「広陵高校」優勝おめでとうございます！

＞ｋｉｎｓｈｉさま（１０１９１）
プ○ンプ○ン物語、知ってます♪でも私も神谷さんが出演されていたことまで記憶にないんです～(^^ゞ
・・・そろそろ「愛と宿命のライバル」の「呪縛」からは解き放たれましたかぁ？（笑）

＞ｙｏｓｉｋｏさま（１０１９８）
風邪だそうですが大丈夫ですか？季節の変わり目ですから要注意ですよ☆
おっ！私もびっくり！英語だよ～(ﾟoﾟ)私はスーパーで外人さんに声をかけられただけでビビっちゃってるよぉ！相手が何を聞いてるのかはなんとなくわかるんだけど答えられなくて、おかしなジェスチャーしちゃったよ。あはは・・・(~o~)

＞千葉のＯＢＡ３さま（１０１９９）
ゴ＠－ちゃんが薬中の役を？！それは知らなかった！どちらかといえば「影のある役」が多いですよね。
「愛と宿命のライバル」・・このネタを後世に残す会を共に発足させようではないかっ！とにかくこれからもひっぱるゾ～☆（笑）

＞将人さま（１０２０２）
そういえば、あの「依頼」の当選者ってどうなったんでしょうか？私は最後まで思いつかず応募しなかったんですけど。。。(^^ゞ応募といえば、バンチのカラー複製原画☆まだ出してナイ！(>_<)シールは集めたけど抽選ということでちょっと気落ちしてしまって・・・でもあきらめずに応募しなきゃね！

＞たまさま（１０２０３）
そのフィギュア、オークションで見たことあるよ♪画像が微妙で鮮明ではないんですが、いつも人気で高値になっちゃって買えません(>_<)・・・で、たまさま買ったの？見つけただけ～？

私も今日、少しだけ「過去ログ」見てみました☆なんだか歴史を感じますね～。思えばこのＢＢＳを見てＡＨの存在を知り、香ちゃんの死に愕然としたものです☆
私はここにきて1年半くらいなんですが、過去の自分のキャラがかなりまともだったことに驚いてしまった！・・・初心に戻らなきゃっ？！

明日はもうバン金？！なんだか早いっ！でも待ち遠しい♪

ではまた。(^_^)/~

### [10212] 将人（元・魔神）	2003.04.03 THU 23:18:48　
	
こんにちは、将人（元・魔神）です。

＞[10203] たま様　2003.04.02 WED 23:07:06　
ＣＨとＣＥの香水付きのフィギアですか？そんなのがあったんですね。
もしかしてラジ＠の神谷明リターンズで、神谷明さんが言われていた
香水って、それだったんでしょうかね？

僕はどこで売っていたのか？（もしかしてゲーセンかな？）知らないの
ですけど、オークションで売っていたＣＨとＣＥのフィギア持ってます
けど香水付きでは無かったです。

ＸＹＺボックスの依頼は、どうなったんでしょうんね？

＞[10204] のっつぃ様　2003.04.03 THU 00:57:04　
たま様の見られたフィギアと同じか分かりませんが、僕がオークション
で手に入れたのは約12cm、去年バンチの1周年号だったかに付いてきた
北＠の拳フィギアよりもちょっと大きめでしたよ。
もしバンチが次に付けるなら、ＣＨやＡＨフィギアにして欲しいですね

＞[10205] 野上唯香さま　2003.04.03 THU 11:17:21　
クラス替えで、仲良しだったクラスメートや先生が替わるのは
寂しいですね。新しいクラスに早く慣れて下さいね。

合同コントの話は、北条司先生とかアニメ版の制作スタッフの方とか
なら、どう思われるんでしょうかね？(→教師ネタだし)
もし本当にそのような募集する機会があったら、野上唯香さんが
応募して下さって結構ですよ。

ここで他の漫画家の先生の作品を、悪く批判するのはマズイかも？
その作品のファンの方もこの掲示板を見ているかもしれませんよ。
→この前、僕が注意されたんで・・・（汗）(^^；

漫研もこの掲示板に来られている方が所属されているかも知れませんし
悪く思われている部分の方だけでなく、本当にプロの漫画家を
目指しての勉強でクラブ活動・サークル活動されている方も
おられると思いますし・・・。

とりあえず、その作品は読んだ事無いですが「北条司先生の
漫画が好き」というのを、その方は「すべての漫画が好き」
という風に思われているのかな？

＞[10209] 阿樹さま　2003.04.03 THU 20:29:04　
そういえば文庫版の古本って、ちょっと高いんですよね。
新しいお友達が出来るようになればいいですね。

### [10211] forny	2003.04.03 THU 21:02:41　
	
The cartoon in Peking is too little now! Can buy to a cartoon, true get very difficult!

### [10210] forny	2003.04.03 THU 20:56:39　
	
yosiko:
Nice to meet you too! I`m sorry ! I don`t know the Mr.Hojo's ! Connected the 《ＡｎｇｅｌＨｅａｒｔ》 to also can not see in Peking.(bei jing)

### [10209] 阿樹	2003.04.03 THU 20:29:04　
	
もうすぐ？かな。春休みも終わり。うれすい～～。
短期のバイトも見つからず、家にいるか、遊ぶか・・・
暇でたまらんだ～～～。
新しい友達ができる４月にわくわくです�

＞１０１９８　yosiko様
　はじめまして！阿樹（あじゅ）といいます。こんにちわです～。yosiko様は現在進行中の、一人暮らしですか。
もうすぐ、うちも仲間入りだ～！
いいですねぇぇ、北条先生ファンがまわりにいて！
話に花が～～♪るらら～
うちも、高校の時にそんな友達いて楽しかった！リョウのイラストくれたりしたし。
今度もいるといいな～と思っています。

＞将人（元・魔神）様
　そそそうなんですyo～。
集英社文庫の古本だといっても、１００円とかにはならないわけだから、貧乏さんには、痛かった！！（ちゃんと見て買えばよかったんだけど）
今度は、コンビニに売ってるやつで、足りない分を買うことにしようと思っています！

目が痛い最近。しょぼしょぼ阿樹でした。

### [10208] Rouark	2003.04.03 THU 13:09:23　
	
＞[10207] 野上唯香様
＞「香瑩みたいにして下さい」
アリなんじゃないかと思います。まぁ、思い通りの髪型に
なるかどうかは美容師さんにかかってますが。

＞[10206] Ｂｌｕｅ　Ｃａｔ様
「僕のHNの読み方の件」
あれ普通じゃ読めないので構いませんぜw「ルーク」でも…
「チャットの件」
そんな過去があったのですか…確かに北条先生の言う通り
かもしれません；；；ん？ということは北条先生も
ここ覗いている事もあるということでしょうか？

＞[10204] のっつぃ様
＞[10206] Ｂｌｕｅ　Ｃａｔ様
「十六夜」（じゅうろくや）でいいんですよね？
戸惑いました。

返信ばかりだとアレなので、最近驚いたことをお話します。
北条先生の経歴を調べていたところ、現在AHを連載している
コミックバンチの会社COAMIXって、会社設立に北条先生も
関わってらっしゃったんですね…知りませんでした。
（会社名の由来ってなんだろう…）知っとけって話ですが。
またまた長くなってしまいました。失礼します。

以上、最近ログインが面倒くさくなってきた（でも、ログインシステムは好き）Rouarkでした。

### [10207] 野上唯香	2003.04.03 THU 11:47:49　
	
質問
美容室に行くときヘアカタログ代わりにＡＨを持っていって「香瑩みたいにして下さい」ってオーダーするのはどう思いますか？

### [10206] Ｂｌｕｅ　Ｃａｔ	2003.04.03 THU 11:23:53　
	
　こんにちは～。

＞[10201] Rouarkさま
　はい、わたしはここがお休みしてる間も早くリニューアルしないかなぁとほとんど毎日のように覗きにきてたんで、復活したその日に登録いたしました。でも、昔の自分のカキコミを読まれるのはかなり恥ずかしいです、ほとんど勢いで書いてたからなぁ。過去Ｌｏｇっていったいいつまで全部残しておくのだろう^^;　あの頃のみなさんたち、今どうしてるかな、おねがい、ｃｈｉｋｋａさままでカキコミやめないでねっ。
　ところで、チャットといえば、リニューアル前の「もっこりメッセージ」でここの掲示板について北条さんが『チャットつくってくれという声もあるけど、あれ、チャットだよ、会話してるものみんな、あれで！』っておっしゃってたんですよね～。たしかに、完全に会話になってますよね（笑）それが楽しいんですけど。
　あと、わたしずっと、 Rouarkを「ルーク」って読んでました（汗）

＞[10204] のっつぃさま
　「十六夜」は「いざよい」って読むのが本当なのかもしれないですけど、これはタイトルなんで「じゅうろくや」でいいんですよ、だってサントラの曲名にはちゃんと「十六夜（じゅうろくや）」って書いてありますから。

### [10205] 野上唯香	2003.04.03 THU 11:17:21　
	
今日新しいクラスが発表されました。ＣＨがきっかけで仲良くなり、１～２年生まで同じクラスだった親友とクラスが離れて２年連続一緒だった先生とも別れて凄い寂しくて泣きたい気分です(;_;)(/_;)(T_T)('_')さっきＴＶでＳＰＥＥＤ復活ライブの話題が出てきて、ＢＧＭで流れてきた「Ｍｙ　Ｇｒａｄｕａｔｉｏｎ」を聴いて更に切なくなりました。そして私のお姉さん的な存在の里奈様も早くＢＢＳに帰ってきてみんなで楽しい話をしたいです。

将人様
読者から原案を募集する話で私、里奈様、将人様で作った合同コントの内容とかを応募したらどうでしょう？ハードボイルド系もありながらコメディ（？）系の明るく楽しい話で・・・。将人様は「稲中卓球部」という漫画を読んだことはありますか？私のクラスの変な子が人を漫研扱いしてうざくて私にこれを強制するんです。私は北条先生の美しい絵の漫画が大好きで、あんな下品で下手くそな気持ち悪い漫画なんか嫌です。あんな奴は北条先生とは月とスッポン（笑）
私は北条先生が大好きです。

### [10204] のっつぃ	2003.04.03 THU 00:57:04　
	
こんばんは、近頃ネット上を深夜徘徊するのが癖になりつつあるのっつぃです。

>[10181] Ｂｌｕｅ　Ｃａｔ様
のぉぉ～(No)!ものっすごい勘違いをのっつぃったらしてましたね…。｢十六夜｣の作詞、作曲はASUKAさんの方で…(-_-;)ご指摘ありがとうございました。恥さらしついでにいうと、実は｢十六夜｣を、高校ぐらいまで｢じゅうろくや｣って読んでました…(^_^;
なんだか、好きな曲だけど、トラウマが増えて行くわぁ…(T_T)

>[10203] たま様
CH、CEのフィギュアってあったんですね…。じゃぁ、AHも出るかしら…？アニメ化とかしたら出るとか、そういう事なんでしょうかね…。だったら、尚更是非是非アニメ化して欲しいですよ!!
所で、そのフィギュアは、どれくらいの大きさだったんでしょうか？

レスのみで申し訳ないですが、これにて失礼します。

### [10203] たま	2003.04.02 WED 23:07:06　
	
土日曜日に何気に入った雑貨屋（中古）で、なんとＣ・ＨとＣ・Ｅのフィギアを発見！
へぇ～。こんなのがあったのかぁ～。っと感心×２
ついでに言えば、Ｃ・Ｅのは香水付きのもあったりして。
なにぶん中古屋なので、全種類って訳ではないけど、泪姉さんのフィギア付き香水のタイトルは、なんと「バイオレット」！
（なんかイメージどおり・・。大人の香りムンムンなのかぁっ？　　よだれぇ～♪）

＞無言のパンダ様　【10187】
＞葵さま　【10195】
だしょぉ～？！香ちゃんのＢＤ＝槇兄ぃの命日（Ｃ・Ｈ）
よかった。たまの勘違いじゃなくって・・。「愛と・・・ライバル（笑）」の二の舞は踏みたくないものぉ～。ウッヒャッヒャッ。

＞yosiko様　【10189】
チャレンジャー！いや、恐いもの知らずや。死に急ぐでない！
だって、「ＯＢＡあ３サマ」って・・・。汗々。
んでも最高。さっそくメモらなきゃっ♪（使う気かいっ！）

＞Rouark様　【10201】
「ルアーク」様っとお読みするのですね。（今まで何って読んでたんだっ？自分・・汗。）

＞将人（元・魔神）様【10202】
ＸＹＺボックスに投稿した貴女の依頼が実現するかも？！って話。本当ですね。どうなったのでしょうね？
もしかして冗談だったとか？「実現するかも？」みたいな言い回しでしたものねぇ。確か。

### [10202] 将人（元・魔神）	2003.04.02 WED 21:53:32　
	
こんにちは、将人（元・魔神）です。

＞[10190] 阿樹さま　2003.04.01 TUE 10:43:42　
見ていないと言われていたＣＨの話を読まれたんですね。
ただ、ミック登場の話が欠けているんですね。残念ですね。
またいつか手に入ると思いますよ。
これから一人暮らしが始まるんですね。
大変でしょうが、がんばって下さいね。

＞[10199] 千葉のOBA3様　2003.04.02 WED 08:01:16　
「宿命のライバル」は、ＣＨの冴羽さんと海ちゃんもライバルなんでしょうが、どっちかっていうと冴羽さんと海ちゃんってのは悪口は言い合っても、結構仲の良い味方の時の方が多いですからね。
やっぱりライバルは、スポ根物が多いのかな？

そうえいば歴史上のライバル？武田＠玄と上杉＠信の苗字がＣＨに出た老人のじいさん、ばあさんの苗字で出ていましたね。

＞[10200] 野上唯香さま　2003.04.02 WED 12:34:57　
もしＣＨのスペシャルをやるなら読者からの募集っての面白そうですねそれか、北条司先生の作った漫画版のストーリのどれかをアニメ化して欲しいかという選挙みたいな投票方式とかね。
既にＴＶアニメ化していても1本30分や、善後編60分だと、かなりエピソード削られていますからね。

→そういえば新宿のマイシティにあったＸＹＺボックスに行かれた方の
　依頼を、実現されるという話があったけど、どうなったのかな？
　11月末までだったから、まだ北条司先生が選んでストーリを
　考えられているとか、まだ雑誌には載っていないけど、描かれている
　のだろうか？

历史上的竞争对手?武田@玄和上杉@信的姓氏出现在CH上了, 是老人的爷爷、奶奶的姓吧?


### [10201] Rouark	2003.04.02 WED 13:24:35　
	
どもっ！ルアークです。
こないだ、少し遠い所にある黄色い本屋に行って来ました。
北条先生のマンガの辺りを見ていたら、な、なんと！
「こもれ陽の下で…」を見つけました。しかも３冊
そろって！でも財布にはお金が無く、買えませんでした！
くぅやぁしぃいぃ～！！銀行口座にはお金あったのに！！
CDが近くに無い！！
皆様、このような状況にあったこと、あります？

---返信---
＞[10198] yosiko様
すご～い！英語できるんですか？僕は全然ダメです。

＞[10181] Ｂｌｕｅ　Ｃａｔ様
Blue Cat様って、かなり昔からここに
いらっしゃったんですね～！Log List見てたら

 [17] Blue Catって書いてあったので驚きました。

うわぁ、長くなった。とっとと失礼。

以上、「ここにチャットは無いのかなぁ、まぁ、公式
だから無理も無いか…」と思っているRouarkでした。
