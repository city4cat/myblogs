https://web.archive.org/web/20031024202143/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=447

### [8940] たま	2002.12.04 WED 19:06:46　
	
流行語大賞取っちゃった？まじで？
これはこれは恐縮ですぅ～♪（お前が照れてどうすんじゃい）

＞里奈さま　【8934】
そう♪私は「素直じゃないリョウちゃん」みたいなの♪♪♪（終始笑顔）
（一足遅かったわね・・・もう喜びまくってます♪）

＞いちの様　【8935】オフレコだぞ！ヽ(`Д´)ノ　ばぶぅー
あはは。女キャラ・・・。遠い昔の話だわぁ～。（ウッケッケッ）
（でも、今でも気づかず読むと女かと思うぞ。）
少林サッ○ーは私も好き。この監督さんの作品はおもろいらしい。

＞マイコ様　【8937】
絵！描けるのですかぁ～？！？見たい♪見たい♪
いちの様からの課題、もし出来上がったら是非たまへもメールしてぇ～。
ところで課題は何？リョウちゃんとか？（うふふ）

＞いも様　【8938】
ノミネートは何なのでしょう？ウタも選考に入っていたのかなぁ？
良きライバルであり良き仲間だった。ウタよ永遠に…(生きてるってば　汗)

＞千葉のOBA3様　【8939】
復活してもマーマ節は健在ですね。
そうね。たまＶＳいちの！紅白で決着を付けなければぁーーっ。
噂によると奴ぁ、美川○一のものまねをするらしい。さすが女キャラ！
（おいおい、ものまねの紅白かよっ）

### [8939] 千葉のOBA3	2002.12.04 WED 08:33:41　
	
おはようございます。今日は雨なんですねー。しかし流行つつある風邪のウィルス対策には雨はいいかも・・・。

８９２９　無言のパンダさま　え、元「１９」ですか？なんか、そっちのほうがいいな・・・。（ごめんね、チビノリ。）しかしパンダさまがやさぐれたのは、「た」のつく人のせいなの！？うーーーーん。あなどりがたし。今年の流行語大賞め。

８９３１　たまさま　祝・本年度流行語大賞。・・・あざらしの着ぐるみに加えてレオタードも持ってるの？（いや、もっているのは人間の着ぐるみか。）で、女キャラのいちのさまとラインダンスを踊る？もりあがりそうですなー。今年の紅白は・・・。

８９３２　葵さま　イラスト送ってくれるのは嬉しいが風邪のウィルスはいらないよーーー！私も「ジャ＠プ」アメリカ版の記事読みました。するってえと「ライジン」もアメリカではどうなの？「ジャ＠プ」なんてどうでもいい！（失礼）それが心配だー！

８９３３　里奈さま　また、たまさま経由でイラスト送ってもらってもいいですか？楽しみですーーー！

８９３５　いちのさま　あれ・・・？いちくんは男なの？女なの？またしても「いちの女疑惑」復活！しかし、ほんとにイラストうまいよねー。描くたびうまくなるもんね。また描いたらオバサンにも見せてくれー！！

さ、さ。今日はノンビリノビノビするぞー。明日は木曜だしねー。ではまた！

### [8938] いも	2002.12.04 WED 01:03:23　
	
こんばんわ～。
１２月に入って急に忙しくなってきました。さすが師走！

ってなわけで、レス。
＞ｓｏｐｈｉａさま[8913]（←たまにはちゃんと書いてみる）
腰痛に加え、胃炎まで！！？それは大変でしたね。最近お見かけしないので心配してたんですよ。ほんとに。
でもよくなりつつあるようでよかったです。 (^^)

＞将人さま[8918]
ほんと次に７や８が並ぶのはいつなんでしょうね？ (^_^;)
考えただけでも気が遠くなりそうですが・・・。
とりあえず１００００っていう近場の目標（？）もすごいと思いますよね。

＞無言のパンダさま[8929]
紅芋味・・・知らなかった・・・。チョコちんすこうは友人に頼まれたので買ってきたんです。もっとよく見ればよかった・・・。「チョコちん」発見に喜びすぎたのがいけなかったのね。（泣）

＞たまさま
今年の「新語・流行語大賞」が発表されましたね。
なんと大賞は「たまちゃん」！！！
今年の話題を総なめのご気分はいかがですか？

では、また。いもでした。

### [8937] マイコ	2002.12.03 TUE 22:57:02　
	
またまたレス～♪

＞「８９３５」いちの様
うっ･･･。いちの様からの宿題ね。よ～し！！がんばって描くぞ！！絵の課題はなんですか？特にありません？

### [8936] マイコ	2002.12.03 TUE 22:49:25　
	
こんばんわ～★明後日テスト･･･。でもくじけずがんばるぜっ！！

レスです♪

＞「８９１８」将人（元・魔神）様
ええっ！？超ーー優秀そうな将人様が、勉強できない学生だったんですかぁっ！？し･･･信じられないわっ！！
ウソは言わないでっ！！（笑）でもほんと勉強って嫌ですね。

＞「８９１９」医龍様
はじめましてぇー！！テストが終わって、冬休み。うれしいけど、うれしいけどっ部活があるの！！私に冬休みなぞ存在しないのよぉ～！！（泣）ご心配ありがとうございます！！

＞「８９２１」里奈様
イラストまたまた見せてもらいましたっ。はぁ～、なんて上手いの！？うっらやましー！！なぜあんなふうに描けるのっ！？う～ん･･･時間を忘れるような趣味ですか？絵を描いてるときとかかな･･･？

＞「８９２７」千葉のOBA3様
ありがとうございます！！がんばりますっ♪




### [8935] いちの	2002.12.03 TUE 22:32:51　
	
どうも、こんばんは。
そう言えば、今日はサッカーやってたんですね（汗）。すっかり忘れてたわ・・・友達から借りた「少林サ●カー」のほうを見てたし（サッカーつながり）アホやね。あの映画は（笑）。（いい意味でね）

＞[8916] マイコ様
息抜きに描くくらいが１番絵を描くことを楽しめるかもしれませんね。仕事にしちゃうと忙しくなっちゃうし、自分の好きなことばかり描くわけにもいかなくなりますからね。
是非、テスト終わった後にでも絵を描いてみてください☆そして、見せてくださいね♪

＞[8917] 葵様
「ほんわか」葵様、風邪のほうは大丈夫ですか？
風邪には生姜がいいですよ（常識？）体が温まりますし。気分的にもキキそうです（笑）。昔から「病は気から」って言いますしね☆

＞[8918] 将人（元・魔神）様
あんな鉛筆デッサンの（しかも中途半端に色づけされた）イラストをカッコイイと言っていただけて、ワタクシは幸せものです（感涙）。ホントに、皆様いい人たちです。
また今度、ちゃんと時間のあるときに、もっとみっちりと描いてみたいです。

＞[8921] 里奈様
は～、なるほど！！懐に銃ですか。描いてるときは考えもしなかったのですが、言われてみると持ってそうな気がしますね。
里奈様の新作も流石北条先生ファンだけあって、最高に上手いです！！本当に、北条先生に見せてあげたいくらいです。今度、北条先生に送ってみてはいかがですかな？郵便とかで。

＞[8928] sweeper様　適当な台詞が思いつかん・・・（汗）
ありがとうございます。
褒めてくださる方がいるだけで、「あ～、明日も生けそうかな・・・。」と刹那を感じている私にとっての原動力となっています（笑）。最近は忙しくて自分の時間を持てないでいるのですが、春休みあたりに、また描いてみようかな～と思っています。だって、大学生って春休み２ヶ月以上あるんだも～ん♪

＞[8929] 無言のパンダ様
映画の１シーンですか！？
・・・・・・・・・・・・・・・・・・（感涙）。
伝わった・・・私が思い描いていたことが１枚のイラストを通して伝わってしまいました・・・。まさに、映画の１シーンのような情景を思い浮かべながらこのイラストを描いていました！！絵には人の思いを運ぶ役割ももっているわけですね。

＞[8930] Ｂｌｕｅ　Ｃａｔ様
いえいえ、こちらこそ見たいを言っていただけて、光栄です（ペコリ）。
私が今までに描いたイラストですが、よく考えてみればたま様宛てに描いた「結婚お祝いイラスト」と、TAKESHI様がご依頼になった「ハードボイルド」しか描いてませんでした（汗）。あとは、落書き程度のものしかございませんｍ（＿　＿：）ｍまた今度、描いたら送らさせていただきます。
今週のラン●王国は必見ですよ！！特に、ＡＨが１位になって、少し内容のダイジェストみたいなことをやっているときのＢＧＭがカッコイイです！！！

＞[8931] たま様
だから、「木目」じゃないって（笑）。
鉛筆１本と言っても、鉛筆の数は多いと思いますよ。私の持っている鉛筆は２Ｈ～４Ｂ（硬さ）の間で、本数は約２０本ほどあります。一応、画家ですから。
たま様も絵の才能が欲しいですって？持ってるじゃないですか、画才なら（ぷっぷっぷ）オフレコ、オフレコ（ぷっぷっぷっぷっぷ＝３＝）
あと、昔、私が女キャだったとか言うと、初めてのかたが誤解するでしょうが！！私は一言も女です、とは言ってないんですからね（男ですよも言ってませんが）。そこんとこヨロシク！！

### [8934] 里奈	2002.12.03 TUE 21:07:06　
	
☆８９３２　葵様
またカラーじゃない原作本の中から選んでカラーバージョンで描きますねぇ～♪｢一日一枚｣どっかで聞いた？どこどこ？

☆８９３１　たま様
ぬわぁぁにぃーー！？
お世辞かどうかは本人じゃなきゃ　わかんないじゃん！
ガルルルルゥーー（｀□´ ）
最初はたま様だって里奈のこと可愛いって褒めてたくせに！
たま様ってば、素直じゃないリョウみたい！
（いかんいかん…喜ぶだけだわ！）

おいおい　タンスあさってるよ！（笑）
新妻レオタード姿…リョウちゃん大興奮だわね！
いちの様のレオタード姿？
……ス…ステキよ！うん！ステキステキ！（ﾟヮﾟ;）（汗）
（この３人はレオタードは着れても動き鈍そう…）

### [8933] 里奈	2002.12.03 TUE 20:54:31　
	
☆８９２９　無言のパンダ様
イラストの｢天使ちゃん｣は毎晩続く残業に疲れ果てております。
しばらくは引きこもるそうです…（笑）いやいや、ホントは天使ちゃん何匹も（虫扱いかよ！？）舞い降りてきてるんだけどね。里奈の体がもたないから捕まえて閉じ込めてます（笑）そのうち爆発して　また描きまくると思いますよん♪

☆８９２８　ｓｗｅｅｐｅｒ様
『いい女だ!!今回ばかりはおれが…本気でほれた!!』
ミック様みたいなステキな外人にこんなこと言われてみたぁ～い♪
里奈が可愛いだなんて　もぉ～☆
そうゆうことは　もっといっぱいカキコしてイイのよ！（笑）
ほれほれ　遠慮なさらずに！（強要…）
あっ、リョウのセリフ、
『だからこそ　俺のような男がいるのさ…』
これ、今ちょうどＲＡＩＪＩＮで勉強したフレーズだわ♪
英語のリョウもステキなのよねぇ～！

☆８９２７　千葉のＯＢＡ３様
イラスト、いっぱい描きすぎて、皆さんがどのイラスト褒めてくださってるのかハッキリ言ってわかんなくなってきた…（汗）
しかも簡単に描いたやつなんて　そのへん置いといたらクシャクシャになってるし…（滝汗）
　　　　　　トホホ…σ（+_+；）
デッサン力…？里奈あるのか？
あんまりデッサンばっかりしたことないから解かんないけど、デッサン力ならきっと　いちの様のほうがあるでしょう！

### [8932] 葵	2002.12.03 TUE 20:52:17　
	
　風邪ひき葵です。昨夜（今朝？！）1時半に寝て、今日は昼の1時におきました。健康的な生活やなぁ…。え？ちがう？！（^^;

　今朝の新聞（読〇）見て驚きましたよ。「ジャ〇プ」米国発売についての記事だったんですが「アメリカでマンガを読むのは子供と変人だけ」とか「通勤時間が長い日本で栄えた文化」とか、なんかむかついちゃいました。日本のマンガは世界一のレベルだーっ！アニメにしたって、ディズ〇ーにも負けてないぞっ！…と、朝から熱血風邪ひきの葵でした。（あ、また熱が…☆）

＞Kinshiさま［８９２０］
　ＡＨパネルとの２ショット（というのか？）写真、出来たら見せてくださいね～♪あ、手紙さんきゅ～です♪

＞里奈さま［８９２１］
　次から次へとイラストの嵐に感謝です♪マリー、キレイでしたよ♪一日一枚ですか？あんまムリしないようにね。（プレッシャーにもなるし☆）しかし「一日一枚」どこかで聞いた言葉だ…。（^^;

＞Ｐａｒｒｏｔさま［８９２３］
　はっはっはっ！偶然ですかね？今度からお見かけしなくなったら「Ｐａｒｒｏｔ」読み返しますね｡（^^;　頑張れ大学生♪

＞千葉のマダムさま［８９２７］
　そうなんっすよ、風邪ひいちゃった☆それでも深夜まで描いてしまうこの根性！北条せんせへの愛ゆえに…ですわ♪完成した暁には、風邪のウィルスつきで送りますね。（^^;

＞無言のパンダさま［８９２９］
　だっからぁ～完全なるゲコ下戸蛙なんですってばぁ～。甘酒でも匂いでダウンしちゃいますよ☆（>_<;

　当我看到今天早上的报纸（读卖新闻）时，我感到非常惊讶。 那是一篇关于《Jump》在美国发售的文章，但却说 "在美国只有孩子和怪人看漫画"，"这种文化在通勤时间很长的日本兴盛"，我真的很反感。 日本漫画在世界上处于最高水平！ 即使是动画，也不亚于迪斯尼！ ......于是，从今天早上开始，葵就患上了热血感冒。 (哦，我又发烧了......）。

### [8931] たま	2002.12.03 TUE 20:34:48　
	
今日７時！サッカー見ようと思ってＴＶのチャンネルをいじくってたら、他のチャンネルで懐かしのＴＶ番組？みたいなのがやってて、そこでキャッツアイが出てましたよ。こういうのって同じシーンが多いけど、この番組は見かけないシーンで結構長めに放送してくれてました。

＞いちの様　【8908】木目かよっ？　木目だよっ！
超ぉーーーっハードボイルド！渋いよ。渋すぎるよ。オッサン・・・。
（あのオヤジからは、オヤジ臭なんてしないんだろぉ～なぁ。笑）
里奈さまの意見に賛同！確かに銃隠し持ってそう。しかも遠距離から狙うめっちゃ長い銃。
鉛筆一本で出来るなんて凄いよ！うらやましいよ！欲しいよ！その才能！

＞Ｐａｒｒｏｔ様　【8909】初めまして。これからもヨロシクです。
お祝いレスありがとうございます。
[いきなり！ＣＨ４択クイズ]←これ最高！！
今回のは本を見なくても分かりましたわ。（自分の実力が試されているぅ）
かなり好評ですよ。続行希望者続出です。次が楽しみです。

＞葵さま　【8917】
何ぃー？風邪引いてるのにデッサンしてたぁーっ？
絵描きのカガミやな。その絵描き根性で風邪も逃げて行くだろうさ。
さて、いったい何を描いたのでしょう？見たいなぁ。ダメかにゃぁ～？

＞里奈さま　【8925】
お世辞に決まってるじゃないのよぉーーーっ♪（『僕より若いのでは？』）
え？レオタード？（ゴソゴソ。タンスから取り出す）←持ってるのかよっ
いちの様は昔は女キャラだったから大丈夫よね。レオタード。でも見たい？
それから、
ＭＥの目が肥えたのはＹＯＵ達のせいざますのよ。（イヤミ風）

＞sweeper様　【8928】
「死なせやしないよ・・・」あぁ～。リョウちゃんカッコイイぃーーっ♪
葵さまへのセリフレス「いい加減にしねェと酢ダコにして猫に食わすぞ！」
あはは。あはは。あははは～。
葵さまぁ～、何かしたのかっ？酢ダコにされちゃうよ。（笑）

＞無言のパンダ様　【8929】
「やさぐれ」なんて言い出したのは、妖怪子パンダじゃないのさぁ～っ。
何？ＣＤブック？そのＣＤブックなる物は原作をＣＤにしてるの？それともアニメ？もしかしてオリジナル？
しかもその題名「パートナーに口づけを」って何？誰が誰に口づけするの？

＞Ｂｌｕｅ　Ｃａｔ様　【8930】
いちの様が描いた私へのお祝いイラストも目茶苦茶上手なのですよ。
ハードボイルドとは全然違うタッチで。

### [8930] Ｂｌｕｅ　Ｃａｔ	2002.12.03 TUE 15:27:26　
	
　こんにちは～。
　いちのさまにメールしたら、ハードボイルドなイラストを送っていただけました、届きましたよ～、ありがとうございました（ぺこり）。渋いイラストですねぇ、鉛筆描きのせいか、見た瞬間ワイルド！って思ってしまいました。他のイラストもとっても見てみたいので、もしよかったら、送っていただけたら嬉しいです。
　あ、そういえば「ラン○王国」でＡ・Ｈが１位だったそうですが、地元のＣＢＣは木曜深夜の放送なので、これからなんですよぉ。見るのが楽しみです、ちゃんとタイマーかけとかなくちゃ♪

### [8929] 無言のパンダ	2002.12.03 TUE 15:06:32　
	
こんにちわ☆

ハイ～夜は寒いんですよねぇ～なので昨日はついアルコール消毒しすぎました(~_~;)

＞Ｐａｒｒｏｔさま（８９０７）
おひさしぶりです！お元気でしたか～？
私はいろんな方の影響を受け（特に例のあざらしと同じ名の方！？）すっかり「やさぐれ」てしまいました！！（笑）が、よろしくお願いしまーす☆ＣＨのＣＤブック持ってますよ～♪ちなみに第２弾の「パートナーに口づけを」もポストカードついてました(^_^)v

＞いちのさま（８９０８）
イラストありがとうございました！(^o^)丿
すんごいシブイです！映画の１シーンみたい～♪これからも、いろんな絵に挑戦してくださいね！（そして見せてぇ☆）

＞里奈さま（８９１０）
イラスト届いてます！ありがとうございました！偽原作バージョン（？）よかったです♪アニメより断然イイ！！（いいのかな？こんなこと言って！？）マリィーさんもきれい！まだコミックスの絵を見てないけど、あとで見てみま～す♪どうアレンジされたのかなぁ～☆(^.^)
「これで書きおさめ」なんてサミシイこと言わずにイラストの「天使が舞い降りた時」はまた描いてくださいネ！楽しみにしてまーす☆

＞千葉のＯＢＡ３さま（８９１１）
「ちびノ＠ダー」が息子さんの高校の先輩でご近所？！
さすが千葉！（特に意味はないです^_^;）息子さんの高校では有名だったのかな☆うちの近所の男子校には元「１９」の背の高いほうの彼が通ってたそうです。学校内では当時から有名だったどうですがぜーんぜん知らなかったー！

＞いもさま（８９１２）
いえいえ私は「泡盛」にはまだ手をだしてませんから、いもさまにはかないませんわ～！(>_<)
ちんすこうと言えば「紅芋味」のを食べたことがあるけど美味しかったですよ♪いもさまなんだからコレ絶対選びましたよね？！

＞ｓｏｐｈｉａさま（８９１３）
腰だけでなく「胃」までやられてたんですか？！ほんとに大変でしたね(>_<)お子様共々、健康体にもどりつつあるようで良かったです☆やっぱり腰は「安静」が効いたのではないかな？(^_^;)

＞葵さま（８９１７）
そんなぁ～！やさぐれてないで「甘酒」でも飲んであったまってくださいよ～！生姜も入れてネ☆

＞将人さま（８９１８）
いえいえ説明不足なんかじゃないですよ(^^ゞ私の理解力のなさのせいなんで・・・(>_<)
時の経つのはほんとに早いですよね～！自分自身に対して「大人になったなぁ」と年月を実感するよりも、他のいろいろな変化によって（ちびノ＠ダーが大きくなってたり）それを実感してしまう自分が少々なさけなかったりしますが・・・(^_^;)
私が実家に住んでた頃に、その裏手に住んでいた小さな男の子が現在漫画家になっていると聞いた時も同じ気持ちになりましたよ☆

＞ｋｉｎｓｈｉさま（８９２０）
ＡＨのパネルの前で写真撮影ですか～？！
・・・私もその場にいたらやってたかも・・・(>_<)本屋さん専用のポスターやタペストリーなど、オークションで見かけることありますがああいう大物はどうするのかなぁ～？(^_^;)

ではまた。(^_^)/~

### [8928] sweeper(レスオンリー)	2002.12.03 TUE 12:28:12　
	
こんにちは。昼間は暖かいけれど夜になると寒いですね。
### [レス]
＞将人様。[8903]
「そしてりょうのスケベの先生よ。」

レスありがとうございます。「りょうちゃん大カンゲキー！」です。「例のアレ」はい！存じております。[8901]のセリフでバレバレですね。そういえばこの話。「PTA特選」なんていうのもありましたよね。(笑)それから信宏くんの部屋の話は夢ちゃんを「CAT`SEYE」に泊めた話であってます。もっこり本。やっぱりりょうさんからもらったのかもしれません。買うときなんかためらってそうで。(笑)それから「ユ@クロ」と「自販機」の画像と「ル@ンのサブタイトル」ありがとうございます。無事届きました。ル@ンの方はサブタイトルを見ながら「これは新ル@ンだー」とか「TVSPのやつだー」などと当ててます。(笑)イラストは何でもOKです。(＾-＾)

＞たま様。[8904]
「死なせやしないよ・・・」

わーい！香ちゃんのセリフだー！(*＾-＾*)では私はその続きのセリフを送りまっす。レス、ありがとうございます。「りょうちゃん大カンゲキー！」です。それからメールありがとうございます。いちの様のイラスト無事届きました。(＾-＾)りょうさんもカッコイイし！たま様も素敵！

＞いちの様。[8906]
「マ･･･マグナム弾をうけながら･･･それにこの驚異的な力は！PCPエンジェルダストッ！！悪魔の薬かっ！！」

レスありがとうございます。「りょうちゃん大カンゲキー！」です。それからイラストありがとうございます。たま様から無事頂きました。もう！素敵です！自分だけで見るのはもったいない！！

＞Parrot様。[8907][8909]
「やっほーもっこり後ろ姿のおねーさーん！」

お久しぶりです！レスありがとうございます。「りょうちゃん大カンゲキー！」です。造語、相変わらずすごいですねー。あと誕生日レスありがとうございます。それから4択の答えは「どうです？90年代最初のハンマーの味は？」ということで3)の絵梨子さんですね？ちなみに冴子さんはセスナの話で「警視庁100tハンマー」を出してます。(笑)

＞里奈様。[8910]
「だからこそおれのような男がいるのさ･･･」

はじめまして。レスありがとうございます。「りょうちゃん大カンゲキー！」です。それからメールありがとうございます。イラスト無事届きました。素敵ですよー！マリィーさんも、りょうさんも香ちゃんも！「大人になった夢ちゃん」ちゃんと届きました。里奈様ご本人も可愛いじゃないですかー。(＾-＾)

＞いも様。[8912]
「ごめんなさい♪これでも私刑事なの♪」

レスありがとうございます。「りょうちゃん大カンゲキー！」です。もっこり本。陳さんが持ってそうな気もしますね。密かにあげてたり。(笑)りょうさんなら絶対譲らなそう。(笑)「コレは絶対ダメだー！」とか言って。(笑)

＞葵様。[8917]
「いい加減にしねェと酢ダコにして猫に食わすぞ！」

お久しぶりです！レスありがとうございます。「りょうちゃん大カンゲキー！」です。はい！もっこり本です。(笑)海ちゃんの秘蔵品！うーん。意外ですねー。信宏君が自分でアノ本を買ったとは思えないです。(笑)

それではこれにて。「もっこり」連呼失礼しましたー。(笑)

### [8927] 千葉のOBA3	2002.12.03 TUE 08:46:12　
	
おはようございます。ホントに今年もあと一月ないんですねー。一年あっという間です。

８９２５　里奈さま　見ましたわよーん！！たまさまからイラストおくっていただきました！も、すごーーーい！！ビックリ！！ホント上手です。里奈さまは基本的にデッサン力のある人なのね。「目」があるのよねー。カラーもキレイです。また描いたら見せてねー！！

８９２３　Ｐａｒｒｏｔさま　こんにちは！（はじめまして、かな？それともお久しぶり？）Ｐａｒｒｏｔさまも釣りなさるんですね。大分かーーー。いいなー。なんか、すごい釣れそうな所ですよねー。うらやましいですー！

８９２０　ｋｉｎｓｈｉさま　ファルコンはクリスマスケーキの準備で忙しいですか？私のうちにも超特大の送ってくださいませ。しかし前から思ってたんですが本屋さんて店頭を飾ったパネルとか、どうするんでしょうね？やはり捨ててしまうんでしょうか？それともどなたか希望された方にあげるんでしょうか？

８９１８　将人さま　本当にキリ番二回もゲットできるなんて、そうあることじゃないですよ。宝くじもだけど、ちょっとした懸賞にだしてみるのもいいかも？手始めはバン懸から？

８９１９　医龍さま　医龍さまも受験生ですか？うちの長男と同じですね。これから追い込みで大変でしょうががんばって下さいね。

８９１７　葵さま　風邪ひきの体で,何？なに描いたの？また見せてねー！？

８９１６　マイコさま　大変そうですねー！試験も部活もがんばってね！

８９１３　ｓｏｐｈｉａさま　点滴したんですか？大丈夫ですか？病み上がりの体でムリはなさらない様に・・・。といっても主婦はそうもいかないんですよねー。

さー、今日はお仕事の日です。ガンバロー。

### [8926] 里奈	2002.12.03 TUE 05:44:33　
	
あ…あれっ…？
なんで２回も…？？　　（滝汗）

### [8925] 里奈	2002.12.03 TUE 05:43:38　
	
普段、里奈のイラストを見てくださっている方々、
また新作イラスト描き上げたんで勝手に送りつけときました☆
もう描かないと何度も言っておきながら一日一枚描いてる気がする。
ほんとに　もう　今回で描き収めだと思います。

☆８９２３　Ｐａｒｒｏｔ様
きゃぁぁぁーー！！
初めてもらった！遼のセリフ付きレス！
超感激だわぁぁぁーー！！　　ヽ（≧ヮ≦）ノ
しかも『リョウちゃん、ズボン落とししちゃう～！』だって☆
どうぞどうぞ、落としてください（コラコラ…笑）

ポストカード！？
んなもん持ってないわよ！？
そんなイイもの付いてたのっ！？羨ましいぃ～！

イラストレーターって言ってもバレない！？
でもココじゃもう通じないなぁ～。残念。
たま様なんて目が肥えてきちゃって文句タラタラよ！（笑）

皆様！読みました！！？
２２歳のＰａｒｒｏｔ様に『僕より若いのでは？』だって！
ちゃんと読んだ！？ねぇー読んだ！？
戻って　もう１回読んで来て！（しつこい！！）
里奈はあなたより年上です。でももしかしたら同い年かも…？
里奈この前２３になったとこだから。
いやぁ～、もしかしたら里奈まだ１８で通用するのかもなぁ～♪
（誰！？あつかましいなんて　ほざいてるのはっ！！）

あはははっ！
『グッド･バイ･マイ･スイート･ハート』に
『こ…心が寒い…』サイコー　（≧ヮ≦）
んじゃ、里奈ちんレオタードで『またよろしくねぇ～♪』
あ。２人足りない。えっと、たま様といちのっちで！
（いちの！お前は女だ！女になるのだ！！）

### [8924] 里奈	2002.12.03 TUE 05:43:25　
	
普段、里奈のイラストを見てくださっている方々、
また新作イラスト描き上げたんで勝手に送りつけときました☆
もう描かないと何度も言っておきながら一日一枚描いてる気がする。
ほんとに　もう　今回で描き収めだと思います。

☆８９２３　Ｐａｒｒｏｔ様
きゃぁぁぁーー！！
初めてもらった！遼のセリフ付きレス！
超感激だわぁぁぁーー！！　　ヽ（≧ヮ≦）ノ
しかも『リョウちゃん、ズボン落とししちゃう～！』だって☆
どうぞどうぞ、落としてください（コラコラ…笑）

ポストカード！？
んなもん持ってないわよ！？
そんなイイもの付いてたのっ！？羨ましいぃ～！

イラストレーターって言ってもバレない！？
でもココじゃもう通じないなぁ～。残念。
たま様なんて目が肥えてきちゃって文句タラタラよ！（笑）

皆様！読みました！！？
２２歳のＰａｒｒｏｔ様に『僕より若いのでは？』だって！
ちゃんと読んだ！？ねぇー読んだ！？
戻って　もう１回読んで来て！（しつこい！！）
里奈はあなたより年上です。でももしかしたら同い年かも…？
里奈この前２３になったとこだから。

### [8923] Ｐａｒｒｏｔ	2002.12.03 TUE 03:43:59　
	
こんばんわ～。もう今年も終わりですが、今年に入り趣味が増えました。ゴルフやら釣りやら・・・。
今日、昼に連れと釣りに行ったんですが、当たり無し。代わりにアメフラシを釣ってしまいました（汗）。まあ、当たりあってもふぐしか・・・。他の所は良く分かりませんが、ここ（大分）では小型のふぐが良く釣れます。
それから、かなり前ですがＣＨのＣＤブックを中古で見付け、思わずＧＥＴしました。題は「哀しい天使」。西九条紗羅ちゃんの話です。中古ながら、封入されてるポストカードが３枚きっちり入ってました。この中で誰か持ってる人はいるんだろうか？

### [レス]
＞里奈様[８９１０]
初めまして。そうです。「苦汁（９０）の味」です。（笑）
イラスト有り難く頂戴致しました。「リョウちゃん、ズボン落とししちゃう～！」です。しかし、本当に上手過ぎです。口では言い表せないとはこの事ですよ。イラストレーターって言っても疑われないのでは？そんな里奈様の実力に脱帽です。
一番下にあった写真は里奈様ですか？綺麗ですねえ。いえ、ほんとに。もしかして、僕より若いのでは？因みに僕は２２歳の卒業赤信号の大学生です。
イラスト、次回を期待してます！その時は又、宜しく御願いします。

＞葵様[８９１７]
お久しぶりです。レス、有り難うございます。「リョウちゃん、ズボン落とししちゃう～！」です。風邪、大丈夫ですか？
はい、葵様が「Ｐａｒｒｏｔ」を先日読み返していたのを知ってて登場しました（笑）。しかし、偶然ってあるんですねえ。驚きです。
クイズの答え、解りましたか。やっぱり、簡単すぎたかな。

＞将人様[８９１８]
初めまして（かな？）。レスと画像とソフト、有り難うございます。「リョウちゃん、ズボン落とししちゃう～！」です。
まだメールの確認しかしてませんが、ソフトの方、ＰＣが相当古いのと容量が一杯一杯な為、難しいですが何とかやってみます。
ＨＰの方もまだ見ていませんが、北＠の拳もＧ＠ンも好きですよ。Ｇ＠ン好きはメアドで分かって頂けると思います。（笑）
ＴＷＯ－ＭＩＸ、実は好きですよ～。
クイズ、やっぱり解っちゃいましたか。う～ん・・・。

＞医龍様[８９１９]
初めまして。こちらこそ宜しく御願いします。
受験ですかー。受験勉強、頑張って下さい！

クイズの解答はもうちょっと待ってみたいと思います。皆様の事だから、答えは知ってるとは思いますが・・・。「いきなり！ＣＨクイズ」、けっこう好評？なのでシリーズ続行で考えていきたいと思います。皆様、御協力を御願いします。
それでは、グッド・バイ・マイ・スイート・ハート（なんちゃって）
「こ・・・心が寒い・・・！」ｂｙリョウ



### [8922] 里奈	2002.12.02 MON 23:36:46　
	
☆８９２０　ｋｉｎｓｈｉ様
ハハハ…（≧ヮ≦）
ファルコン、今月はクリスマスケーキ予約いっぱいで大忙しですか！？想像すると　ほんと　おもしろい…（笑）デコレーションが気になるわ！

あっ。やっぱり！最終日だったらパネル持って走ってた！？
そういえば遼もそんなことしてたな昔…。
原作３巻、佐藤由美子のファンで、わざわざ街中でバルーンを銃で打ち割って、人々がバルーンに注目してる間にパネル持って逃げたでしょ？（笑）パネル持って逃げる前までは　すっごい真剣な顔だったから『ほんとに欲しかったのね…』って笑ってしまった…。その後香に破られてパニクってたっけ☆
ｋｉｎｓｈｉ様もそこまで真剣に強奪してきて！！

### [8921] 里奈	2002.12.02 MON 23:26:51　
	
☆８９１８　将人様
『ＣＨキャラが教師だったらバージョン』のイラストかぁ～。
描けないことも無いですよ。だって、ハンマーで叩き潰されるには変わりないし（笑）、教師の服装とかだっていつも通りでイイんだし、要するに背景だけ学校にすればイイんでしょ？
…って、そんな簡単じゃないですけどね☆どんなイメージで描くか考えておきます♪今また違うの描いてて、今回は時間かかるんでＣＨ描くの遅くなると思うけど…（今描いてるのも一応ＣＨだけど、遼や香じゃないんです。外人大好きマリーちゃん♪）

ゾロメ効果は絶対年末ジャンボでドカン！ときますよ！
（根拠無し…）
是非買ってみて！もしかしたら　かなりの幸運が訪れる予定で、その為に今不幸なことがまとめて来てるのかもよ！？
（更に根拠無し…汗）

☆８９１７　葵様
葵様もイラスト描く喜びに目覚めて、里奈のように自分の体にムチ打って描いてるようですね…！風邪悪化しちゃったって？大丈夫ですか？里奈も人のこと心配してる場合じゃないわぁ～。ホント、高圧ガスってちょっと吸っただけなのに一日中息苦しい…中毒になっちまうよ★でも今は死期が早まってもイイから描いてたいの。何かに取りつかれてるのかしら…（汗）
イラスト見せてくださいねぇ～♪

☆８９１６　マイコ様
やる気ないダメな私…とかなんとか言っといて、マイコちゃんしっかり頑張ってるじゃん！息抜きに絵ってイイもんよ♪絵が描くのが大好きなもんからしたら幸せな時間だわね！時の流れを感じないし。マイコちゃんも絵に限定しないで、何か時間を忘れられる趣味って持ってますか？

☆いちの様
おおおぉぉぉーー！
とってもハードボイルドじゃん！！ウマイよ　いちのっち！
なんか『私は殺し屋です。今から仕事します。』って言われてる気がしたわ！
大人の色香がプンプンだわよ！コイツ、絶対わき腹あたりに銃隠し持ってるわ！うん　間違いない！いやいや、ゴ○ゴ１３とお揃いのスコープ付きの銃かしらっ！？絶対　目　合いたくないぐらいリアルに怖いわ！ハードすぎるわ　いちのっち！今回は里奈のほうが励まされてしまいましたわ★里奈はオヤジ描くの苦手なんで、あぁゆうの描けないんだけど、大人の女の色気で責め続けるわよっ（笑）さぁ～今夜も頑張って描くぞぉ～！
