https://web.archive.org/web/20020819074030/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=301

### [6020] Ｂｌｕｅ　Ｃａｔ	2002.03.09 SAT 17:10:58　
	
　ごめんなさい、「神谷明リターンズ」について、さっきのだとまだ説明が足りなかった気がして・・・。えっと、ラジ＠にアクセスできたら、下にある「再放送」の「３月０８日　金」ていうのをクリックして、そしたら「神谷明リターンズ」という番組名の右にある「Ｗｍｐ」か「Ｒｅａｌ」をクリックすれば、聴けると思います。それぞれのパソコンで、どちらが使えるか試してくだされば・・・・ごめんなさい、こんな説明しかできなくて。

　それから、［６０１９］で最後に書いたのは、Ａ・Ｈ３巻の作者コメントを読んで書いたものです、わたし、できるだけカキコミをシンプルにしようとするあまり、ひとりごとのようなどうしても舌ったらずな表現になってしまうことが多くて・・・気をつけます、ごめんなさい。

### [6019] Ｂｌｕｅ　Ｃａｔ	2002.03.09 SAT 16:08:14　
	
＞［６０１５］まろさま
　あ、えっと、わたしはバンチが創刊した頃からほぼ毎週「神谷明リターンズ」を聴いていて（存在を知ったのは、リニューアル前のここの掲示板か、もっこりメッセージだったか・・・すでに１年近く前なので記憶が・・・ごめんなさい）、最初は検索サイトから「ラジ＠」（ちなみに伏せ字じゃなくこのままです）で探しました。アクセス方法とか説明不足だったかなと後で思ったのですが・・・すみませんでした、そして丁寧な説明、ありがとうございました。昨夜は嬉しさで興奮状態のまま、早く伝えたいって気持ちしかなかったもので・・・。

　ところで、Ａ・Ｈは香港では１８禁、ですか^^;　Ｃ・Ｈに比べて、殺人のシーンがいっぱい出てくるせい、なのかなぁ・・・？

### [6018] kinshi	2002.03.09 SAT 13:28:54　
	
こんにちは　
今日は、風、強いですね、

ＡＨ１，読書前、
ＡＨ２，読書時、
ＡＨ３，読書後、

ネタ切レって、　マジッスか？

### [6017] ｌｕｎａ	2002.03.09 SAT 10:52:17　
	
おはようっす！
６０１６無言のパンダさーーーーーーん
に又もや、「やっやられーーーた！」です
あたしは、あたしのためによるあたしのための
リョウさんとの恋人宣言でっ
無言のパンダさんをいっちぃおっとぉ心の恋人同志なんで、
つけ加えた？のっにぃ～
どーして、リョウさんと無言のパンダさんが、あたしの父と母
になるんだぁぁぁぁぁぁぁぁぁぁ
って事で、おかあさま（無言のパンダさん）
かわいい娘のｌｕｎａをよろしくするだぁ
おまけ
無言のパンダさんのＨＮに対する素朴なギモン？
無言やないしぃ
強気でバシバシ言うパンダさんてのは、いかがでしょうかあ？
ひよぇ～
要、必要、我、逃亡、来、危険
（これ、シャンインになったつもりで、書いてみたけど、
　めちゃくちゃ過ぎますねぇ、我、要、反省ですかねぇ）
ｓｗｅｅｐｅｒさん、おめでとうメッセージありがとうです！
けど、あたしは、リョウさんのこ・い・び・とでいたいっす！
さぁて、あたし、どこへ逃亡したらいいんやろぉ？
（ポンっと手を打つｌｕｎａ）
そして、不適な笑み
リョウさんの所へ行けばいいやん！（＾＾）
いっちばん安全だよねぇ～餅山しゃん～
でも、餅山と一緒にされたくない正義の味方のｌｕｎａであった
あたしって、凛々しいかも・・・
このカキコを見て、不快な思いをされた方
これは、香港ではＲ指定になってるんで、マジで、マジっすかぁ
って事で、よろしくぅ！

### [6016] 無言のパンダ	2002.03.09 SAT 09:55:26　
	
おはようございます☆
ＡＨ３巻まだ手に入れてませーん(T_T)
今日は入ってるかしら？

＞Ｋ.Ｈ様（５９９８）
レスありがとうございます！お体の具合はいかかですか？
ハイ、絵は見るのも書くのも大好きなんです～。（本家）
北条先生のイラスト集もお気に入りです♪
ところでＡＨ観とＣＨ観ですが・・・。
やはり私は別物派。というかＡＨはＣＨの「ａｎｏｔｈｅｒ　ｓｔｏｒｙ」だと思っています(^_^)
以前「ｉｆ（もしも）」というドラマがあって、ひとつのストーリーで結末を迎えるんですが、「もしも、この時点で違う選択をしていたら・・・」という設定で、また違う結末が用意されているんです。
なんていうか・・・そんな感じです。ワケわかんないですね（笑）だからＡＨはＣＨのリョウと香が、その後事実上の結婚をするという選択をしたお話。あーでもそれじゃあ二人がラブラブになるのがいけないことみたいですよねぇ（結果的に香ちゃんが亡くなってしまったから）でも、それはあくまでも「運命のいたずら」ってことで・・・(T_T)
すみません！混乱してます～(~_~;)だって、リョウと香の二人には、やはり同じ世界に存在していて欲しいから。
だからＡＨはＣＨの「もうひとつの物語」だと考えてます！（結論）

＞ｌｕｎａ様（６００８）
リョウとの「恋人宣言」してくれてありがとう！私のために。（笑）大丈夫！ｌｕｎａ様は香瑩のように、私とリョウの「娘」にしてあ・げ・る！平和的解決でしょ？！
冗談はさておき(^_^;)私は長女で昔から「兄」という存在に憧れていたのでリョウにはたよりになる「アニキ」になって欲しい・・・かな？

ではまた。今から本屋さんに行ってみようかなー？
（「最新情報」もう「３巻発売」に変えてくださいよ～）

### [6015] まろ（神奈川県：梅の蕾が・・・）	2002.03.09 SAT 09:35:28　
	
押覇っス！
ラジ＠の『神谷明リターンズ！』で『バンチのリョウちゃんネタ希望』のメール投稿者の１人の”まろ”でやんす。

いや～、いがったなぁ（微笑）
でも・やっぱ、クジ運が良いのか（苦笑）名前の紹介は無く、『多くの皆様』の中にに埋没してましたね（汗）。
（ですが、ボキの密かな野望としては、
　神谷さんをこちらにお招きする事っス！メールするぞォ！！・笑）
と、ゆー訳で、そう遠くない内に、バンチの最新号とシンクロしたコーナーでＡＨとかをやってくださる事になりました！

★ラジ＠へのアクセスは――
ボキＨＮ脇のＵＲＬを押して、サイト内のリンク集からジャンプして頂くか、若しくは、検索エンジン（→Ｙａｈ○ｏ！）などで”ラジ＠”で検索して、ジャンプして頂ければＯＫです。

あ、あと、今週放送分のラジ＠の事を、初めにカキコした方が居ましたが、意味が分からない方もいらっしゃるので、どういった経緯で、アクセスできたのか位は説明しましょうよ。
（しかも、紹介した人へのお礼も無しに、というのはちょっと・・・ね）

んぢゃ、あでおすぢゃ！

### [6014] 千葉のOBA3	2002.03.09 SAT 07:40:27　
	
おはようございます。なんか、みなさんのカキコを読んで，笑ったり感心したり・・・。んー、やっぱりこうでなきゃ。ルンルン。　　　　　３巻見ましたー！読書後のご注意、わらっちゃいました．３巻でネタぎれ・・．今から４巻が楽しみです。あはは・・。

＞５９９２ちゃこさま　　おくればせながら、お誕生日おめでとうございます。私は年末生まれですが，なんか年度末生まれって同級生の中では，得？・・・えーと，許すということ・・についてですが、すごくむずかしい・・・りょうちゃんて、そういう感情を，自分の中で，昇華させるの、普通の人より、はるかに早い。・・・だって、香ちゃんの、心臓がG.Hに移植されたと知ってすごくショック受けたあと，李兄に会うじゃないですか、あの時の態度・・・。私はなんで、そんなふうにしていられるのー？と、ほんとにあのシーンでは、そう思ってたのですが、そう・・ちゃこさまの言うとおり、許すしかない・・・。のでしょうね。
・・・・私にはできないなー、と、自分の周りの色々なこと、思い浮かべて思いました。きっとりょうちゃんは、今まで，ものすごーくつらいこと、のりこえてきた分、そういう感情を自分の中で整理していくことも、私などのおよびもつかない早さ・・なのかなー。　　　でも香ちゃんの心臓を強奪されたから、そのあとの一年を、りょうちゃん、生きてこれた？必死に探す事で・・・
その一年間のこと考えると・・・すごい、ダークになる・・。いかん今は朝、しかもこれから仕事、えーと、そういうことで、今朝はこれにて，失礼します。

夕べ，調子わるくて、「木更津キャッツアイ」見てないの，あとでビデオでみます。

### [6013] ひろ	2002.03.09 SAT 01:42:25　
	
こんばんは～(^o^)

ホントにもう春ですネ！
今年は暖冬だったせいか桜の開花も早いそうで、東京では３／２０頃(聞いた話だと、本当かな)と言うことですが、東京近辺で行われる桜祭りの時期（３／末～４／初）には、もう花が終わって葉っぱだけになっているんじゃないの！？
私の花見は、今年も近くの公園かな。近所の人たちと。

＞Blue Cat[6011]さん
ラジ＠の「神谷明リターンズ」おかげさまで聞くことができました、ありがとうございます。結構真に迫ってすごく良かったですネ。まだ聴いていない人は、一度聴いてみてください。
そして最後に神谷さんが、りょうちゃんは自分の分身だと言っていましたが、ホントにそうなんでしょうね。

明日は休み、予定はないけど忙しいゾ～！
少し疲れてきたので、今夜はこの辺でサイナラ(^.^)/~~~




### [6012] sweeper(レスオンリー)	2002.03.09 SAT 01:13:01　
	
こんばんは。単行本の話題で盛り上がっているみたいですね。
### [レス]
＞ちゃんちゃん様。[５９８５]
お久しぶりです！いつも笑えるカキコ、楽しみに待ってます！それから、クイズの「さよならＸ３」は映画の解説でおなじみだったあの人ですね？(もう故人ですが・・・。)

＞ちゃこ様。[５９９１]
お久しぶりです！おくればせながらお誕生日おめでとうございます！今年１年がちゃこ様にとっていい１年でありますように。

＞営業部長のK.H様。[５９９８]
「どうですボクは"人間三脚"になれます！！三脚を忘れたときに便利ですよ！！さらに！！このもっこりを使って―――」
「えーいやめんかいっ！」「わたしのカメラに何てことするのよっ！！」(バキッ！)

レス、いつもありがとうございます。「信宏、李大人と香瑩を見て腰を抜かしちゃう！(キャッツアイ前にて。)」です。風邪で熱が出ていたということですが、具合は大丈夫ですか？むむう。意外な解釈でしたか。でもはじめから別の１つの話として読んでいたわけではないんですよ。わりきるまでかなり時間がかかりました。AHは皆様がカキコしているようにCHの設定がないとわかりづらいところがあるかもしれませんね。１つの作品に対して受け止め方は人それぞれですから。
「サイボーグ009」と「HOTEL」そういえばピンとこないですね。後者は青年誌で連載されていたからでしょうか？「サイボーグ009」サイボーグに改造されてしまったがゆえの宿命に泣けて来ます。ちなみに00メンバーでいちばん辛い経験をしているのは004ではないかと思います。

＞luna様。[６０００]
キリ番ゲット、おめでとうございます！
りょうの身内になれるなら。娘もいいし妹も、いいですねー。

それではこれにて失礼します。

### [6011] Ｂｌｕｅ　Ｃａｔ	2002.03.08 FRI 23:27:12　
	
　きゃ～～～～っ、今、ラジ＠で「神谷明リターンズ」を聴き終わったところなんですけど、その番組内で、神谷さんと雪乃五月さんが、Ａ・Ｈ３４話でのリョウが“香瑩”という名前の理由を告げるシーンを、生で演じてくれたんですっ！！　すっごい嬉しかったですっ、みなさんも是非聴いてみてください、来週まで、再放送してるんで。　あ～ごめんなさい、今わたしかなり興奮してます(汗)びっくりした～。

我刚刚在广播@上听完《神谷明归来》，在那个节目里，神谷和雪乃五月现场表演了A·H34话里亮说出“香莹”这个名字的理由的场面。是莴苣! !非常高兴，请大家一定要听一下，到下周为止，会重播的。啊~对不起，现在我很兴奋(汗)吓了一跳~。

### [6010] おっさん（岐阜県：体力ねえ～）	2002.03.08 FRI 22:42:46　
	
毎度、今仕事先から帰ってきました。電車の乗換えがあるんですが（２回ほど）改めて思ったこと。「体力ねえ～」って思ってしまったおっさんっす。階段がしんどいのＴ＿Ｔ結構長い階段なんで・・・（電車通勤の方、改めてご苦労さんでやんす）ということで、今日もいつものご挨拶せ～のおっは～！！
言っちゃっていいのかな～。え～い３巻かったじょ～！！
やっぱり来たネ来たね「ご注意」
もっこり指定・・・ネタの一部だったんっすね。でも「香港版」じゃＲ－１８指定じゃないすか。先生の「マジっすか」は受けましたね～。う～んいいよいいよマジで・・・かなり来てるね。（アカン、今日マジでラジア○リミテッド風になってる）ところで、これを見たＫ．Ｈ営業部長殿どうおもいました？？？う～ん聞いて見たいっすね。皆もどうだったんでしょう？？？？？
レスしたいんですが、モ～～～うダメ今日は消えます。疲れた（ふう～）・・・・・・？？？？？

### [6009] hironosu	2002.03.08 FRI 21:29:15　
	
北条司先生ありがとうございました。
エンジェルハート第3巻購入させていただきました。
まだエンジェルハートが始まる前に一度CITYHUNTERの続編をと待望にまってます。友達も沢山いると送ってしまいましたが、、これは偶然かわかりませんが、なんと僕の名前が、、宏信が信宏となって出ている同じ字だ！と思うと涙が出てきそうなほど、感激でした。凄くうれしいです。当然全巻発売日に買い、友達にお薦めしまくり、やはりみんな素晴らしい作品だ。おもしろいと同意見です。これから僕は、ホンシンと名乗ります(笑)ずっと応援してます頑張ってください。CITYHUNTERのDVDも早く出して欲しいです！

### [6008] ｌｕｎａ	2002.03.08 FRI 20:23:21　
	
こんばんわ
ＡＨ、モチロン、手元にありますよぉ
パチパチパチ、６０００番、あたしが、獲ってたのね
けど、あたしも、一言、無言のパンダさんへ
６００２のコメントを見て、
「げっやられたぁぁぁぁぁぁ」
って、無念ですぅ
あたしは、もぉ～１００万歩ぐらい譲って、
リョウさんの妹ってカキコしたのにぃ
無言のパンダさんの・・・・
ここには、悔しくって、カキコできないよぉぉぉぉぉぉ
あたしも、堂々と宣言させて頂きます！
リョウさんは、ｌｕｎａと無言のパンダさんの心の恋人です！
まぁ本当は、改めて言うほどやないけど・・・
公認されてるしぃ（いつのまに？）
絶対的、恋人宣言でした！
どうだぁ！（＾＾）

### [6007] yaeko	2002.03.08 FRI 19:47:49　
	
こんにちは。かべちょろにこだわるyaekoです。
３巻買いました。内容書いちゃっていいのかな。２巻発売日のＬog見たらその日に感想カキコあったので、今回もいいんですよね？誰も書いてないから不安だなあ。

「読書後のご注意の巻」
「ジャッキー・チェン」「マジッスか」「ネタ切れ」もうどれも大ウケです。センセったら４３歳になってもオチャメなんだから♪４巻のコメントどうするんですか～？また楽しみが増えちゃったじゃないですか♪
「もしや３００点ゲットかな？？？の巻」
「最後の決着(ケリ）」の１ページ目。阿香の足の包帯が正しい位置に描き直されてましたね。私は所さんに勝てたのでしょうか？←ワケワカメな方は１月３０日のカキコを見てね♪
それでは。晩御飯食べよっと。

### [6006] kinshi	2002.03.08 FRI 11:25:28　
	
＞luna様　
６０００キリ番ＧＥＴ　おめでとうございます。
早いものですね。それだけたくさんの話題が、あったんですよね、
これからも楽しく　北条ワールドを語っていきましょう。

### [6005] こむぎ	2002.03.08 FRI 01:50:41　
	
はじめまして、こむぎです。
今まで、見てるだけだったのですが参加したくなりました。よろしくおねがいします。
　
　北条先生お誕生日おめでとうございます。
　　　（今さらすいません）

　え～と、いろいろ書きたいんですが今日はこれでしつれいします。

### [6004] サボテン	2002.03.08 FRI 01:14:34　
	
こんばんわ。

突然ですが[5982]の三毛猫様のおかげで、私の中で謎だった、バンチの中吊り広告の秘密（？）がやっと分かりました。最近、広告が二重になっているなぁ、と不思議に思ってはいたんですけど、そういう仕組だったんですね！これでスッキリしました。ありがとうございました！！

今日は、AH３巻の発売日、楽しみです！

### [6003] いも	2002.03.08 FRI 00:33:11　
	
こんばんわー。

レスです。
＞おっさんさま[5980]
はじめまして！半年くらい前からここをのぞいていて、
最近カキコし始めたのですが、おっさんさまからレスを頂ける日が来るとは‥‥嬉しいです！
これからもよろしくお願いします。

＞Ｋ.Ｈさま
私はＡＨはＣＨの「その後」（未来っていうとちょっと違う）のうちの一つだと思っています。
ただ、「ＣＨの続編」と言い切れないだけかもしれませんが‥。
ＣＨが無ければ、ＡＨという物語は生まれないと思うんです。
同じ様な設定の話は書けても、ＡＨを描くにはＣＨの存在が前提として必要だと思います。
だからといってＣＨの未来そのものではないような気がするし、
ＣＨはＣＨで揺るぎない一つの世界だし‥。
つまり私としては、「ＣＨの過去をもった別のお話」というような非常に曖昧な（汗）定義の上でＡＨを楽しんでいます。
「皆様が考えるＡＨ」を読んでいて、いろいろ気付かされたりして面白いです。ＣＨにせよ、ＡＨにせよ、ホント奥深い作品ですね～。いろいろ考えてしまいます。
北条先生も罪なお人だ（笑）
って、結局全体的によくわからない文になってしまいました。すいませんです。（汗）
こんなんですが、今後ともよろしくお願いします。

明日‥‥日付変わって今日！３巻発売ですね。
内容知っていても、通して読むとまた違う雰囲気だったりするんですよね。
それではまた！！
いもでした。

### [6002] 無言のパンダ	2002.03.08 FRI 00:06:01　
	
こんばんわ★
一言だけ。
＞ｌｕｎａ様（６０００）
元気ですかぁ～？
６０００キリ番ゲットおめでとう！！
リョウと私は一心同体。身内である必要ないのです！ウフッ♪

おやすみなさい！(-_-)zzz

### [6001] まっつん	2002.03.08 FRI 00:00:33　
	
遅くなりましたが、あお様、げら様、レスありがとうございました。
あの香が催眠術をかけられるのは、アニメだけのお話だったんですね。そりゃ、マンガじゃ見つかりませんよね。
これから頑張ってビデオを見付けます。でも、私の周りのビデオ屋さんにはＣ.Ｈのビデオあんまり置いてないんですよ。（泪）
でも負けません。頑張って、私がここ数年探し求めていたお話に巡り会いたいと思います。

