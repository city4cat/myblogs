https://web.archive.org/web/20050313021048/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=483

### [9660] 千葉のOBA3	2003.02.19 WED 07:50:12　
	
おはようございます。
今日はまた、暖かくなりそうでうれしいです。

９６５９　　無言のパンダさま　　あーーー、言うまいと思っても、やっぱり眠いって言っちゃうーー！パンダの星って白と黒なのかなー？葵さまが、足を痛めたのは、歩きすぎたせいだと思いますよ。「疲労性捻挫」？？？

９６５５　葵さま　歩きすぎて、足痛めちゃったんですか？（違うか！？）でも、折れてなくてよかったですよーーー。ギブスとかすると大変だってー。私はやったことないけど、子供は何回かやってるから・・・・。東＠ハンズ、マグナム型のライターがあったんですか？でも、見本なんだーーー。ほしいですよね！（煙草は吸わなくても）あと、ぬいぐるみ型のマウスカバーなんてぇのもあるの！？うちの言う事きかないマウスに着せたら似合うだろうか？？？あーーー、でもまた、近いうちにユザ＠ヤに行って、マグナムさがししたいですーーー！

９６５３　いちのさま　　なに？「もの＠け姫」の「オトコヌシ」？いや、あれは「オコトヌシ」？？？やっぱり漢なのねーーー！？（自分でも意味が判りません。）

明日はもう、木曜日だと言うのに天気予報では雨・・・。うーーーん。バン木はしたいし、雨はいやだし・・・。考えちゃいますね。

### [9659] 無言のパンダ	2003.02.19 WED 02:30:08　
	
こんばんわ★

深夜です。人気海外ドラマ「フ○ンズ」を見ながら書いています♪

＞ＯＲＥＯさま（９６４８）
部屋を勝手に開けられたって？それはお友達ですか？私も以前、社宅に住んでた頃、出かけている時に雨が降ってきて、運悪く布団を干していたので近所の奥さんたちが管理人さんから鍵まで借りてきて部屋に入って親切にも布団を入れてくださったのです～(~_~;)もちろん「親切」だとはわかっているけど、部屋は急いで出かけた時だったのですんごい散らかってて、とても人様に見せられる状態じゃなかったので大ショックだったわ～！・・・そんな感じかな？(^_^;)

＞たまさま（９６４９）
コント～？！あの会話内容はノンフィクションよ！
ところで私が帰るのは星なの？！国じゃなくて？！いったいどこなの？パンダの星って・・・見つけてくれたらお望み通り帰ってやるさ♪フンッ！
ＰＳ：あざらしは潮の香りか？昆布だしか？！

＞里奈さま（９６５０）
だから、あれは「コント」ではありません☆(>_<)
そう、真実なんだからしょうがないのです♪ほぉ～ら、いちのさまもご自分を「ミスターＸ」と認めてるではないですか！その上、この期に及んで「男の中の漢」などとうそぶいてる～！さあ！里奈さまムチの用意は良くて？
ＰＳ：この夏（春から？）は大忙しになりそう～？！☆その前にワイハ～（汗）で充電ってとこかな？楽しんできてネ♪できればおみやげには硝煙の匂いを「缶詰」にして持って帰って！（笑）

＞千葉のＯＢＡ３さま（９６５１）
「なんか、」はマウスのせい！？それはまた摩訶不思議なっ！
マダムは魔法使いか～？！それとも責任転嫁か？(^_^;)でもやっぱり眠いんでしょ？（笑）

＞いちのさま（９６５３）
そーんなこと言ってオドシても子パンダには「パンダの耳に念仏」よ♪(^○^)それにしても、やはりいちのさまは「ミスターＸ」か！？ではこれから、「ムチ使いの達人」里奈さまと、実は「ロウソク使いの達人」であるたまさまのところで修行開始よ！
でもプリンを作れるなんてやっぱり「ミセス」だわね。

＞ＴＡＫＥＳＨＩさま（９６５４）
香水は、さりげなく通り過ぎた頃にフワッと香るのがベストですよね♪
タ○リのグッジョブ見ましたよ！残念ながら神谷さんは出られなかったけど、かなりイイ内容でした☆

＞葵さま（９６５５）
なにぃ～？！足を痛めた～？足のどこよ！？
それにしてものん気に一度骨を折ってみたかっただとーっ？そりゃあ折ったところは元の骨より頑丈になるっていうけど、アンタ！松葉杖がどんなに不自由かわかってんの～？（ちなみに私も骨折経験なしでございます）何はともあれ、おだいじにネ！

＞将人さま（９６５８）
そういえばＡＨでまだ香瑩がグラスハートだったころ、ビルからナイフを使って滑り降りるシーンで「２００１カオリ革命」という香水の広告（？）が出てきましたよね☆ああいうイメージなのかな？香の香水ができたら♪北条先生の漫画のキャラクターはみんな個性的だから想像しただけでいろんなイメージが膨らみますよね(^^)

もうこんな時間！ダンナは飲み会から帰ってこないし、どうしようかなぁ～カギをしっかり閉めて締め出してやろうかな。ウケケ・・・（千葉のＯＢＡ３風！）

### [9658] 将人（元・魔神）	2003.02.18 TUE 23:56:50　
	
こんばんは、将人（元・魔神）です。

＞[9639] 無言のパンダ様　2003.02.16 SUN 01:07:04　
ラジ＠の神谷明リターンズ聞きました。ＣＨのキャラをイメージした
香水ですか？いいですね。シティーハンターやキャッツアイなど
北条司先生の漫画は、美女が多いですからね。
FCは、男女が逆になりそうですが・・・。

香さん、冴子さん、美樹さんなど、漫画での性格のイメージの
香水なんだろうな～♪

＞[9641] 里奈さま　2003.02.16 SUN 03:39:44　
「海坊主」って「シーゴブリン」なんですか？
ゴブリンって響き、カワイイですか？
僕は、頭にファンタジー小説や、ファンタジーRPGゲームのイメージが
あるんで悪役（しかもザコ敵）のイメージに思ってしまいました。

冴羽さんの香水は、硝煙の香りですか？
確かに色々と銃を撃っているけど・・・。
冴羽さんはナンパするから、なんかクールでさわやかなカッコいい
香水を付けていそうですね。
海坊主さんは、なんか、実質本意な感じなんで、そういう余計な物を
付けていないようなイメージなんで、硝煙のニオイかも知れないな～

＞[9647] 無言のパンダさま　2003.02.17 MON 02:00:51　
子パンダ様の案では、海ちゃんはコーヒーですか？
喫茶店のマスター、ＣＨでの初のエプロン姿はビックリしたけど
今は、喫茶店のマスター姿が板に付いてきてますからね。
喫茶店のキャッツアイって、もう泥棒の３姉妹よりも、海ちゃんの
店のイメージで定着してるような

### [9657] 将人（元・魔神）	2003.02.18 TUE 23:20:45　
	
こんばんは、将人（元・魔神）です。

今日、タモリの番組で声優さんが出るというのを、この掲示板で
知って見ましたよ。
シティーハンターの神谷明さんは出られませんでしたが、
ドラゴン＠ールの悟＠や、ドラえ＠んのジャイ＠ン、フラン＠ース
の犬のネ＠、ガ＠ダムのアム＠とかの役の方が出られてましたよ。
１人で何人もの役を同時に演じ分けるのを、生で見られるなんて思って
なかったので、ビックリしました。
やっぱり声優の方って、すごいですね。
昔あった「まんが日本昔ばなし」なんて、全部の役を２人で
演じられたそうですが・・・。

その後で見たニュースで、韓国の地下鉄で、大変な火災があった事を
知りました。
警察では40人以上、消防の方では100人以上、亡くなられたかも
しれないという話で、まだ全体を把握していないとの事ですね。
亡くなられた方のご冥福をお祈りします。

そのニュースでは、日本の地下鉄では大丈夫とは言われていましたが、
その直後のニュースで、新宿歌舞伎町のビル火災での管理者の逮捕の
ニュースがあったので、類似した事件のように思えて、ちょっと
不安を感じました。マネをする模倣犯とか出てきて欲しくないな。
亡くなられた方のご冥福をお祈りします。

### [9656] TAKESHI	2003.02.18 TUE 22:47:25　
	
おやすみ前にレスです。

＞[9655]葵様
僕もアレルギーです（泣）たまに一日中鼻水が止まらないときがありますが、死にそうなほどつらいです。

大丈夫、妄想ってのは冗談ですよ（笑）リョウみたいにはなりませんからご安心ください。イラストの方、妹に伝えておきますね。

よっしゃー！！明日もがんばるぞー！！

### [9655] 葵（おーまいがっ☆）	2003.02.18 TUE 22:05:07　
	
　おーまいがっ☆昨夜ＰＣ終わって立とうとしたら、しびれてて右足をグキッと。折れてないにしろヒビが入ったような気がするくらいの痛さでした。（なんたって歩けなくって、立膝で移動したわよっ☆）でもレントゲン撮ったら大丈夫とのこと。ただいま伸縮性の包帯でグルグル巻きです。骨太（肉太？！）でよかった～と思いつつ、一度骨折ってやってみたかったかな…と思うフトドキモノでした☆（^^;

　きのう新宿東急ハ○ズで３５７マグナムのライターを発見しました♪１５００円で大きさといいイイ感じだったのに、現在見本のみということ。残念☆ルパ○やロバート愛用のワルサーＰ３８は同額でキチンと売られてました。リョウちゃん人気ってコトで、今回はあきらめますわ☆

＞ＴＡＫＥＳＨＩさま［９６４６］
　花粉症じゃないんだけどアレルギー持ちなので、いつかはなるだろうと覚悟してます。あんまり香水に妄想しすぎると、リョウみたいに危険人物（？）になっちゃうわよっ☆（^^; あ、お手数なのですがマイコさまのイラスト拝見したいんで（マダムが「キレイだよ～」とおっしゃてたの♪）、ご連絡していただけますか？遅くなっちゃって申しくわけないのですが、お取次ぎよろしくお願いします♪

＞たまさま［９４６９］
　パンダは酒の香…ナイスッ!!!（一体どんなパンダやねん☆）あ、昨日例のパンダアザラシのＣＭを見ましたよ。とうとう関東進出か？！

＞千葉のマダムさま［９６５１］
　「なんか」…やはり笑えます。きのう新宿のハ○ズで、お掃除もできる動物ぬいぐるみ型のマウスカバーというのを発見しました。マダムにプレゼントしようかと思ったのですが、他の買い物が重くって（何買ったんだって？！）やめました。今度見つけたら送りますね～。ちなみにアザラシ型はありませんでした。（笑）

### [9654] TAKESHI	2003.02.18 TUE 19:06:20　
	
こんばんは。今日はタモリのグッジョブに声優が出るとのことで楽しみです。神谷さん出ないかな？（ワクワク）

＞[9647]無言のパンダ様
そうそう、つい振り返ってしまいますよ（笑）ただ、香水をつけるのはやはり大人の女性がいいです。女子高生でつけてる人もいますが、香水くさくてたまったもんじゃありません（汗）

＞[9649]たま様
すいません、なんか調子に乗って妙なこといってしまいました（笑）でも香とか冴子とか（北条先生の描く女性キャラ）めっちゃ美人じゃないですか！！！香みたいな女性は僕の理想です。

＞[9650]里奈様
すいませんっ！！！発売日ＣＭで見たら２月２２日でした。えっ、店頭に並ばない！？！そんなバカな！ならネットのショッピングで買ったらどうですか？

＞[9653]いちの様
愛犬と散歩（爆走）ですか！いいな～僕も犬とランニングしてみたい。最近は運動してないので体がなまっちゃってます。マッチョになりたくて筋トレしてますが、やはりなかなかつきませんね。

それはそうと、ゲッツとはいったいなんですか？一回２０円！？！

### [9653] いちの	2003.02.18 TUE 17:34:33　
	
こんにちは。
今日は、犬の散歩で走りまくり！！寒かったけど、走っている間はそんなこと感じもせずに頭の中には「もの○け姫」のワンシーンを思い描きながら爽快に疾走してきました（笑）。

＞[9644] 野上唯香様
手作りのものって気持ちが込めてあるぶん、美味しく感じますよね。（きっと）
自分はプリンしか作った事がないので、クッキーはことさら難しく感じましたよ。練ったり、捏ねたり、寝かしたり・・・意外と力仕事ですし。大変ですよ。
でも、貰ったほうとすると、うれしいですよね。

＞[9645] 葵様
私はあまり食べなかったんですけどね（汗）。
今度は卒業式に作って持っていくそうです。徹夜にならないように、前もって準備させるようにしたいと思います。
失敗作を食べさせられるのはイヤですし（笑）。

＞[9647] 無言のパンダ様
こら！子パンダ！！せめて男（ミスター）にしておけよ！！
男の中の漢（おとこ）だと、口をすっぱくして言っておろうが！！おぬしには届かぬのかこの熱い想い（魂）が！！
しかし、まーミスターＸは的を得た表現かもしれませんなー。
子パンダもなかなか鋭いなー（は、は、は）

＞[9649] たま様
やはり、分かる人には解るんですねー（笑）。
ゲッツ！！（１回２０円）
つーか、ウインターバージョンがわからない・・・（汗）。
ターン＆リバースぐらいしかわからない・・・まだまだ勉強不足のようですたい。

＞[9650] 里奈様
妄想大好きの飲んべー店長は大丈夫なんですか？
ちゃんと店開けるんですか？
酒に酔いつぶれているときではないのでは～？
こんなところで「趣味；妄想」を披露している場合ではないのでは？

＞[9651] 千葉のOBA3様
最後の（ウケケ・・・。）は何ですか？
意図的に話しをごちゃごちゃにしたんですか？
ＳとＭの謎は里奈様、もしくはたま様に尋ねてみてください。彼女たちはその道のプロらしいので。
きっと、知りたくない事も教えてくれるかもしれませんよ（笑）。

### [9652] 野上唯香	2003.02.18 TUE 16:49:31　
	
里奈様
ハワイ旅行はもうすぐなんですか？早いですね～冬休みにその話を聞いてからもう２ヶ月位になるんですか・・・。水着店店長会議店長会議お疲れさまでした。京都で３店舗ですか！？じゃあ相当忙しくなるんじゃないですか！？どことどこに出来るんですか？帰省したらまた訪問します☆
そうそう、「節電・節水にご協力を」のＡＨ香のポスターはバカ厳しい校則のせいで却下になりました(T_T)別に女子高なんだから良いじゃんて思いませんか？

### [9651] 千葉のOBA3	2003.02.18 TUE 09:44:48　
	
おはようございます。
だんだんと、春らしく変わりやすい天気になってきましたね。

９６３６　たまさま　　「なんか、」って・・・。なんなんじゃーーー！？って思ったら、たぶんマウスのせい？？？（自分のせいにはしない。）取替えたら、大丈夫。（と、思いたい？？？）

９６３７　無言のパンダさま　そう・・・。「なんか、」の後には、お決まりの「眠い・・・。」じゃなくってーー、最初の頃カキコした文字が知らない間に、下に行っていたって・・・信じられますーーー？マウスがいけないんだ！このネズミめ！！あーーー、しかし、ＣＨの香水っていいですね♪「香」ちゃんの時は、カジュアルな服装で、「冴子」さんの時はおしゃれしてって、皆さん使いわけそう・・・。

９６４１　里奈さま　もう、水着の店の店長会議？やっぱりファッションの事だけに早いのかな？そういや、ＣＨでも,水着のファッションショーはクリスマスだったもんね。・・・しかし、ハワイで、銃撃ちまくって、その銃の先を嗅げば,確かに硝煙の匂いはするだろうけど、間違って自分の鼻吹き飛ばさないでよーーー！
あーーー、これもニュースででそうだ・・・。（大汗）「日本人観光客、銃で自分の鼻を吹き飛ばす。」「ワタシは、硝煙の匂いを嗅ぎたかった・・。」笑えないってーーーー！！あーー心配だ！！

９６４３　いちのさま　フン！バズーカなど、「こんぺいとう2号ディスカバリータイプ」で迎撃してくれるわ！！ところでさーーー、このあいだからでてる、いちのさまのＳって、なんの略？ｓｉｓｔｅｒ？それでＭはｍａｎなの？だから、ＭじゃなくってＳだって？・・・って、なんのハナシかわからなくなったわ・・・。（ウケケ・・・。）

９６４８　ＯＲＥＯさま　　花粉症の薬のんで、ただでさえいつも眠い人が、ますます眠くなっているようです。しばらくは、つらい季節が続きますよーーー！

それでは、今週も忙しくなりそうだ！がんばります！！

### [9650] 里奈	2003.02.18 TUE 00:38:43　
	
昨日、５月から始まる水着の店の店長会がありました。でも、いざ参加したら、ほとんど飲んだくれて里奈のキャラをアピールしただけの集まりに…（笑）今年は京都だけで３店舗に拡大が決定して、里奈ちんアッチコッチ行かなきゃなんない☆飲んだくれてて大丈夫なのか…！？

☆９６４９　たま様
ゴブリンってモンスターな雰囲気存分にかもし出してるよね！
ＲＰＧでザコ的としてよくでてくる。
遼ちゃんの香水で硝煙ってのイイでしょ♪
でも里奈もどんな匂いなのかイマイチわかんないけど…。
ワイハ～で これでもかってくらい撃ちまくって体匂ってみる！
ってゆーか、撃ち放った後の銃の先を匂げば早いんじゃ…（汗）

☆９６４８　ＯＲＥＯ様
就職活動かぁ～。日本じゃ就職に失敗する（就職できない）人も多いし リストラ大流行だけど、海外ではどうなの？カナダは？里奈も海外で仕事したいなぁ～♪昨日の店長会で、『今シンガポールに店出てますけど、もし他にも海外（ワイハ～希望）でオープンすることがあったら、あたし 行きたい！』って熱はいっちゃった★もし そんな話しがあったら里奈本気で迷わず行く！
おぉぉぉー！なんか英語の勉強に力入りそう！（単純）
面接リラックスで超頑張ってネ！（なんのこっちゃ★）ＯＲＥＯ様って心配しながら 難なく成功しそうな気がする！それに、まだ たま様から『ウンバラバァ～♪』うけてないでしょ？アレ呪いだから、それがまだのうちは安心よ♪

☆９６４７　無言のパンダ様
海ちゃんをイメージした香水『コーヒー』ぴったり★
裏家業引退して すっかり喫茶店マスターしてる海ちゃんは ひきたてのコーヒーの香りが染み付いてそうだもんね！ネーミングは是非『CAT'S EYE』で♪
信宏は『洗剤』？ うん、確かに（笑）
でも意外に消毒液かもよ！だって海ちゃんの｢体で覚えさせる まわりくどくない教育法｣だと生傷絶えなさそうじゃん…。消毒液の香りの香水なんて欲しくないけど（苦笑）

『いちのネタ コント』の後始末をたま様＆里奈に押し付けたな！？
苦情なんて受け付けないわよ。だって真実なんだから苦情言われたって仕方ないやん！文句あるヤツは『○ャロ』に電話しなさい。

☆９６４６　ＴＡＫＥＳＨＩ様
えぇぇーっ！石川では ３月末発売なの！？こっちは２月２０よ！
しかも どこも仕入れ数が少ないらしくて、もう予約終わってるとこばっか！いろんなとこにＴＥＬしたんやけど、どこも店頭には並ばないって…（泣）マジで欲しいのに！

大人の香りに妄想中！？ＴＡＫＥＳＨＩ様の頭の中は今どんな世界が広がっているんだろう…。なんなら里奈の素肌の香り匂ってみる？（とってもゴージャスな 居酒屋の油の香り）

☆９６４５　葵様
同じメ～ルが３通も！？ちょっとアピりすぎた…？（笑）
だって里奈のＰＣ、その時だけ『届けましたよん』って報告してくんなかったんだもん！送信できてないと思って何度もチャレンジしちゃった…。

チョコでコーティングした たまねぎ！？
その たまねぎは生かい！？甘いと思ったら辛くて、そのギャップがウケるかもよ！（絶対有り得ない…）

☆９６４３　いちの様
ちょっとアンタ！！　（￣□￣;）
里奈から『妄想』を取り上げたら何が残るっていうのさ！？
長年かかって築き上げた里奈ワールドが崩壊しちゃうじゃないのよ！
もしや…！あんさん、里奈ワールドに憧れて乗っ取るおつもりどすえ？
許しまへん！許しまへんえぇぇー！
心配せんかて、あんさんも うちのイリュージョンで魅惑の世界に引き込んだるさけぇ～、おとなしゅ～待っときなはれ♪

### [9649] たま	2003.02.17 MON 22:38:03　
	
＞里奈さま　【9641】
ゴブリン
よぉ～分かりませんが、海ちゃんの頭の感じがゴブリンっぽいのかっ？
ってかゴブリンって何だっ？
う～ん！リョウちゃんの硝煙のかほり賛成ぃ～♪
んで、硝煙ってどんなにおいなの？
ワイハ～で銃撃った後、自分の身体におってみて♪（硝煙の臭いを）

＞いちの様　【9643】
うわぁ～おっ！たまんねぇ～！
最高だよっ！あんた最高だよっ！文頭に「ゲッツ」もってくるなんて、マジたまさんツボっす！
現在「ゲッツ☆ウィンターバージョン」を習得中のたまさんはウホウホです。（やっぱ時代はダンディーやなぁ～）

＞葵さま　【9645】
うんうん。香ちゃんのイメージだとフルーティーな柑橘系だぁ～ね。
あはは。海ちゃんはワイルドな男っぽいかほり？
ミックは確かにブランド系！葵さま見事だわっ！
ついでにパンダは酒のかほりやなっ（笑）

＞TAKESHI様　【9646】
ＴＡっ・・ＴＡＫＥＳＨＩ様ぁ　・　・　・　。
香ちゃんのかほりを妄想？しかも⇒（クラクラ）付きで。
え？クラクラじゃなくてヘロヘロだってぇ～？
でも意外な文章だったから、なんか笑っちまったよ。
でもダメよ！今はテスト勉強に集中しなきゃぁーっ！
テストがうまくいきますように。ウンバラバァ～♪

＞無言のパンダ様　【9647】
香ちゃんの香水！
とても素敵なお話しですわぁ～♪
１人のレディーとして香水は必需品ですものっ。オーーーホホホッ♪
パンダ親子よ。見事なコント楽しませてもらったよ。
日常会話とは思えない。あんたら親子こそ未知なる親子や。
ところでいつ自分の星に帰るんだいっ？

＞OREO様　【9648】
どうやらトホホな一日だったみたいだねぇ。
部屋を勝手に開けられ・・・ってまさか散々汚されてたとかっ？
だってそうとう物が動いてないと気づかないんじゃ・・・？
まぁ、そんなトホホにめげないで、面接がんばってねっ♪

### [9648] OREO	2003.02.17 MON 09:55:17　
	
こんにちわ☆
人から急遽留守を預かることを頼まれ、いきなりお泊り決定で１０分で用意して部屋散々散らかしたまま出て行ったら、私の留守中に、部屋を勝手に開けられ、ショック・・。そして泊り先では１０分用意で用意したので、忘れ物一杯して散々な日々を過ごしてし帰宅しました・・・☆☆

今週ＡＨ
そっかぁ～海外脱出して、仲良くなってるみたいでよかったぁ～。いつか二人で立派なお店が開けるといいなぁって思います。いままで仲たがいしてたぶん、取り戻してほしいですね☆
久々、香ちゃん登場で、スッゴイ嬉しかったです☆香は、そこにいるけど、触れることはできなくて・・・。だから、実際に、自分の傍にいつも居て、やり取りしてる、親子見て、阿香は、うらやましかったのかなぁ・・・って思いました。あ～あ・・親子３人で持てる時間が作れたらいいのに・・ってすごい思いました・・。

＊レス＊
＞９６２９野上唯香様
えっ！？がまがえるのお話・・・！？きっとまだ読んでません（汗）是非、読まさせていただきます♪

＞９６３２里奈様
ＰＣフリーズ大丈夫でしたかぁ～？？
やったぁ～☆里奈様から、お許しでたんで、私もキリ番ゲットに参加しちゃいます～☆

＞９６９３千葉のＯＢＡ３様
花粉症・・。ついに始まりましたか・・・。想像しただけで目が痒くなりそう＆くしゃみ出そうです（笑）ほんと辛いんですよねぇ～・・。
就職面接、頑張ってきます！！

＞９６３６たま様
う゛ぅ～～（感涙）アドバイスありがとうゴザイマス！！たま様に、そんな風に言って頂けて、ホント嬉しいです！！とっても励みになりました！！自分らしい自己ＰＲ作って（でっち上げ抜き☆笑）面接でうまくアピールできるよう頑張ります！！

＞９６３７無言のパンダ様
「かす汁」～！！なんて素晴らしい響き！！（笑）寒いときはあったまりますよねぇ～☆☆ラジ＠の香ちゃん香水の話、聞きましたよ～！！それから、無言のパンダ様のメールも☆香水の企画、ホント商品化して欲しいです！！
就職面接まで、もう直前で、何か緊張してきちゃって・・。でも、なんとか、自分をアピールできるよう、頑張ります！

あ～・・・そういえば、バレンタインなんてものが・・忘れていました・・・☆だ～から、バラの花もった男の人が街であちこちにいたわけだぁ～。なんてロマンチック☆花をもらう女の子がうらやましいです！！

### [9647] 無言のパンダ	2003.02.17 MON 02:00:51　
	
こんばんわ★

シーゴブリン・・・一瞬マンゴープリンとか、そういう「プリン」系デザートの新メニューかと思ってしまった。←バカ

＞里奈さま（９６４１）
リョウの香水は硝煙の匂い？！いいかも！・・・でもそれって香水で表現可能なのかな？(^_^;)
子パンダの案では、海ちゃんは「コーヒー」信宏はもう1年以上皿洗いしてるから「洗剤」の匂いだそうです（汗）でももはや香水の必要はなさそう・・・☆
ＰＳ：「偽りのスマイル」はやめて～コワすぎる！(>_<)

＞いちのさま（９６４３）
Ｍのほうがよかったというよりも、Ｍのほうが似合うなぁって・・・(^^ゞ
というよりも本日のパンダと子パンダの会話を再録（？）いたしましたのでお聞きください。
子「いちのって人はＳでもＭでもないＸ！未知なる男だよ」
パ「えっ？Ｘ！？じゃあミスターＸじゃん！（笑）」
子「ミスターＸ？・・・ミセスＸかもよ☆」
パ「爆っ！」
我が家でのいちのさまの評価は現在このようになっております。
尚、この件への苦情は「里奈さま」または「たまさま」までお願いいたしますm(__)m

＞葵さま（９６４５）
そうね～たまねぎチョコより「小たまねぎのチョコフォンディュ」のほうがオサレかな♪
うん♪うん♪洋酒も忘れずに入れてね～♪
ウッキ－ッ！酒量は年々減る一方なんだよ！根拠のない言動は慎みましょう(~_~ﾒ)☆

＞ＴＡＫＥＳＨＩさま（９６４６）
香水にクラクラ～？妄想でへろへろ～？！（言ってない！）
でも香水を上手に使ってる人ってステキですよね。すごいいい香りだったりすると女の私でも、つい振り返ってしまいますよ(^^ゞ

おやすみなさ～い！(-_-)zzz

### [9646] TAKESHI	2003.02.16 SUN 22:58:41　
	
こんばんは、明日からは期末テスト一週間前です。緊張するな～(汗）

＞[9645]葵様
花粉症ですか！？そういえばそんな季節がやってきましたね。僕も鼻炎なので季節の変わり目はつらいです（笑）

＞[9641]里奈様
シーゴブリン！？！ゴブリンってのは海坊主（ＣＨ）のイメージに合ってないですね（笑）ゴブリンって聞くとＦＦを思い出します。雑魚キャラの定番ですもん（笑）

ＰＳ２の「ＳＡＫＵＲＡ」は今、予約受け付けてますよ。発売は三月末くらいですかね。

＞[9639]無言のパンダ様
香の香水ですか！あぁー、いいにおいだろうな～～（クラクラ）香は香水つけなくてもいい匂いするんだろうな～・・・あ、すいません、妄想してしまった（笑）僕も、葵さんの言うように、フルーティな香水が似合うと思います。

それでは、おやすみなさい。


### [9645] 葵	2003.02.16 SUN 21:40:12　
	
　急に冷たい雨になりましたね。なんだってこんなに冷え込むんだか…。春はまだかーっ!!!（とかいいつつ、目がショボショボしてきた今日この頃。これはもしかして恐怖の「花粉症」ってやつか？！ひえぇ～っ☆）

＞里奈さま［９６３２］
　どもども。キチンと届いてホッとしたのはイイけど…メール、同じのが合計で３通も来てるわよ。里奈さまのＰＣ、早くもハワイモードでイカレちゃったの？！（^^;

＞無言のパンダさま［９３６７＆９３６９］
　香をイメージした香水…ナイスですね～。香にはフローラル系よりも柑橘系の方が似合いそう♪（単に私好みなだけ☆）リョウはともかく、海ちゃんにはワイルドな感じの香がイイよね。ミックは絶対ブランドモノだろう…☆（^^; で、何なのよ！アノ、たまさまへのレスは!!!チョコでコーティングしたたまねぎだ？そんなメンドクサイこと誰がするかっ☆大体食えるのか？（いくらたまねぎ大好きの私でも、それはゴメンこうむる☆）そっちこそ酒量が増えてるんじゃない？アル中には気をつけてね～♪

＞いちのさま［９６４３］
　完食に３日ですか？！ご苦労様☆でもそこまで頑張ったチョコなら、市販のものより、どんな失敗作だろうと渡した方がよかったんじゃ…？兄貴に食べさせるために徹夜したワケじゃないんだろうしねぇ…。（^^;

### [9644] 野上唯香	2003.02.16 SUN 18:40:18　
	
もうすぐＣＳ放送Ｃｈ．２７６でＣＨ２が始まるので急ぎです(^_^;)

いちの様
塾の男の子達は私のクッキー美味しいって言ってくれました。作って良かった☆

里奈様
海坊主の英訳の意味は私の電子辞書で調べたところ、「ａ　ｓｅａ　ｍｏｎｓｔｅｒ」で国語辞典で調べたら意味は「海上に現れるという頭の丸い化け物。船がこれに会うと凶事が起こるという」でした（笑）

关于“海坊主”的英译意思，我查了一下电子词典，是“a sea monster”，在国语词典里查了一下，意思是“在海上出现的头圆圆的怪物。船遇到它会发生凶兆”(笑)



### [9643] いちの	2003.02.16 SUN 16:25:32　
	
どうも、ゴブサターン！！（ゲッツ！！）

＞[9629] 野上唯香様
やはり、料理の得意な女性は素敵ですよね。
私はお菓子作りはプリンしか作ったことないんですけど、分量とか本見ながらやっても難しいです・・・。
クッキーとか生地をこねたり、かき混ぜたりと意外と力仕事を必要としますし、大変ですよね。
バレンタインに手作りクッキーなんてもらったら男なんてコロッといきますよ。単純ですからね、男なんて★

＞[9631] 葵様
別に、食えなくはなかったですよ（笑）。
３日寝かしたらまずまずの味になりましたし・・・。
ただ、彼女自身が失敗作だと申しておりますので、バレンタインには市販のチョコをあげたそうです。
そのため、後片付け（完食）には３日かかりましたけどね。
来年はどうなることやら・・・。

＞[9632] 里奈様
変な妄想もそこまでにしてくださいね（汗）。
それと、ゴブリンは決してカワイイ者では無いと思うぞ。
（どうかたま様のところから帰ってきませんことを頑なに祈りながら）

＞[9633] 千葉のOBA3様
そんな～、遠慮なさらずに～（満笑）。
私からの心ばかりのお返しです故、ホワイトデー特製バズーカ受け取ってくださいよ～（冷笑）。
１ヵ月後を楽しみに待っていてくださいね～♪
さあ～、これから海ちゃんにどんなのがいいか相談しなくっちゃ☆

＞[9636] たま様
バレンタインに男がチョコあげたっていいじゃない！！（私はあげて無いけど）
日本だけだよ、バレンタインに女性限定なのは。しかも、チョコを贈るって仕来りみたくなっているのは。菓子業者の陰謀にまんまとはまってしまっていますよ～。
目を覚ませー！！
いろんな意味で目を覚ませー（笑）！！
里奈様をこっちに寄こすなー！！お宅のでしょ。飼い主ならなんとかしなさいよ。野放しにし過ぎよ、アンタ。
気を付けて～。

＞[9637] 無言のパンダ様
まー、里奈様色に染まってきていると言うよりは、だんだんと素が出てきたのかな～、と思うときもありますが。
全くそんなことは無く、このスタイルが自分らしいと申しますか（汗）。（タジタジ）
これだけは言っておきますが、Ｓだろうと、Ｍだろうと、あんまり興味ないですね（笑）。
ただ、痛いのは嫌いなので（注射とか）、Ｍでは無いことは確かです（笑）。
Ｍのほうが良かったですか？


### [9642] 謝麟	2003.02.16 SUN 14:35:38　
	
to dear 里奈
End of this week.To me this time.the happyness things
is go to play with my friends.some times ,we go to KTV
to sing song through night.Or drink some beer side the river.I'm easy to satisfy.I feel very happy with my friends! Person is an animal in community.(^Q^)
some times,when I'm down in spirits.I'll smoke,sleep all day and all night.or talk with my close friend.but my spirits change so quick.
when I wake up.I feel everything it's ok.(I'm a affirmative man).And how about you?Same to me?(I guess you are.)
I'm concerned about the develop between 阿香&遼,
I don't think that they best appropriate nexus is
a daughter and father.I wish they are lovers,like
香&遼.but,I think to 遼,It's so difficult?I'm expect
that mr.hojo can give us a best answer!!!(^^;)

### [9641] 里奈	2003.02.16 SUN 03:39:44　
	
｢海坊主｣って…英語で｢シー ゴブリン｣だって（笑）
ゴブリンって響き、なんかカワイイと思うのは里奈だけ…？

☆９６３９　無言のパンダ様
香の香水！？欲しい欲しい！実現しないかなぁ～♪♪
遼の香水ってどんな香りがするのかなぁ～？……硝煙？（単純）

あら やだ！子パンダちゃんが里奈に怯えてる！？
子パンダちゃぁーん 出ておいでぇー（汗）里奈は電撃ムチ振り回してるけど、マグナム ズキュンズキュン撃ち放ってるけど、でも とってもエンジェルなのよ♪（偽りのスマイル）

☆９６３６　たま様
｢トバッチリ｣！？
た…たま様まで里奈の愛を床に叩きつけるおつもり…！？
あなただけは…あなただけは私のことを心底愛してくれているものと信じていたのに！ひ…ひどい！ひどいわっ！タマオさんのバカァァァーーッ！！ザッパーン…（夕日が映える海辺にて。）

☆９６３３　千葉のＯＢＡ３様
え！いいじゃん いいじゃん！一生ハワイ暮らし♪でも帰って来れないのは嫌だなぁー。ＡＨ読めなくなるし…。（他に理由ないのかよ！？）
『なんか、』サイコー（笑）そのあとの長い空白がナイスです！
