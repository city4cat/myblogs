https://web.archive.org/web/20031101135114/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=600

### [12000] 圭之助（けーの）	2003.10.09 THU 22:22:21　
	
今週、バン木だったんですね。予告で１日早いのを見た気がして、本屋へ寄って正解でした(o^-')b

★今週の感想★
公園を駆け回ってる子供たち、あれは香の小さい頃の幻影？（左側のコ）かわいい！元気よく走ってるのが香だぁ…って思わず目が細くなってしまったわ☆
早百合さんに香の、阿香の新宿を離れたくないって想いは伝わったと思うんだけど…。リョウと香の絆の深さはわかってもらえるのかなぁ。どーしても、リョウの素行が気にくわないようなので…わかってあげてよぉ！って私が叫びたくなっちゃうわ(;^_^A
…けど、出会った頃はリョウのほうが香に一目ぼれみたいな感じだったから、いつから香がリョウに対してあんな深い愛情を感じ始めるのか、すごく興味がわきますぅ!!
そこんとこ、見たいなぁー！o(´^｀)o

＞[11999] 葵さま
おぉぉ～、生きておじゃったか～！！入れ違いで生存確認＆気まぐれハガキが届くことでしょう☆あ、あとひとつで「12000」キリ番だったのにね(゜o゜)

＞[11995] 無言のパンダさま
咳って、腹筋にこたえるんだよねー(^_^;)
みろりさまに見せてもらった踊るパンダは結構ムキムキだったけど、このパンダは意外と体力弱い!?

＞[11994] 腐十貯（ふじっちょ）さま
どんどん、みんな名前が進化してくんだから～(^^ゞ
携帯に「1919」やら入れたせいか？やたら最近、多いのよ。出会い系迷惑メールが(-_-メ)抹消してるけどさ！

＞[11993] リョウ357さま
４２歳でリョウ派な小父様、ステキですねー☆今度写真送って下さい♪あら、ミニクーパーのですよ(〃∇〃) てれっ☆
新しいミニは何回か街中で見かけたけど、旧バージョンのは未だかつて現物を見たことナイ…(T_T)中古でも見つかるといいですねー。購入された暁には、ゼヒ写真を！！（しつっこい？汗）

＞[11991] ぽん☆さま
インコは可愛いよね。でもさすがに人間語で「帰りたいの」とは鳴きません（汗）気が向いたら、直メールしてきてね☆インコ＆小鳥談議しましょうや♪♪（*￣▽￣*）ゞ

### [11999] 葵	2003.10.09 THU 21:29:55　
	
　すっかり忘れてました。今日は「バンチ」でしたね、いやはや…。イキナリ発売日早めるのはやめてくれーっ!!!(;^_^A

　感想です。阿香の目を通して香の過去を見ることが出来て、とても嬉しかったです。亡き父を思う香、槇ちゃんを慕う香。香の思いでの全てをココ（新宿）が吸収してる、同化してる…これでさゆりさんにも、わかってもらえるよね？阿香を、香をＮＹに連れてくことは出来ないって。「新宿＝香」…改めて感じてしまいました。今回の香ちゃんはＣＨ（作品時代）よりも、ずっとずっとキレイでした。こんな表情（かお）をするのも、香が新宿を離れられないのも、「リョウ＝新宿」、そして「槇ちゃん＝新宿」だからこそなんだろうなぁ。新宿って暖かい街だったんだね。(^-^*)

＞はじめましてのみなさま
葵と申します。最近イロイロとあってご無沙汰ですが、食いついたりしませんので安心して下さいね。（＾・￥

＞常連のみなさま
どこかで死亡説（?）が流れたようですが、生きてますよー!!!（｀＿´　）ゞ

　センターカラーを見て、携帯が欲しくなった…。イマドキ持ってないヤツが珍しいんだろうけど、でも持ってないんだよーっ!!（だって必要ない状況なんだもん☆）でも待ちうけ画面見たいよー欲しいよー。誰か画像送って?!（T_T)

感想。能通过阿香的眼睛看到香的过去，我非常高兴。思念亡父的香，仰慕槙子的香。这里(新宿)吸收着香的想法的全部，同化着…这样小百合小姐也能理解了吧?不能把阿香、阿香带到纽约来。“新宿=香”…我再次感受到了。这次的阿香比CH(作品时代)漂亮多了。做出这样的表情也好，香不能离开新宿也好，正因为“獠=新宿”，而且“槙子=新宿”吧。新宿真是温暖的街道啊。(^-^*)  
（译注："阿香"指香莹，"香"指槙村香。）

### [11998] Ｂｌｕｅ Ｃａｔ	2003.10.09 THU 19:49:55　
	
　こんばんは～。　木更津キャッツアイ（というドラマ）のシナリオ集が文庫になってたんでつい買ってしまった腐゛・・・じゃなくって（あーあぶない）Ｂｌｕｅ Ｃａｔです♪　巻末の語句解説を読んでるだけでも面白いです（にこにこ）。

　では今週の感想です。
　「だから私はこの街から離れない……　何があっても！」、えらいぞ、よく言った香！　これで、少しは早百合さんもわかってくれたかなぁ。香の新宿への想いが、知ることができたのが嬉しかったです。この想いが香瑩を新宿に導いた、ってのもあるのかな。最後の頁の香、すっごくキレイだった。　あ、若き日の槇村の制服姿、なんかかわいかったです☆
　窓から阿香たちを見送るリョウ・・・。　わかってくれ俺たちの想いを、そう言ってるように見えました・・。
　予告にあった「香の切ない過去」というのがとっても気になります、香のリョウへの想いが、描かれるんだったら嬉しいんだけどなぁ（どきどき）。

＞[11995] 無言のパンダ さま
　ん～、なんか違うんだな。どっちかっていうと、「腐女子への道は足が腐んごんで歩きにくい」ってカンジかな（笑）　蟻地獄や底なし沼にはまったら腐んごむ程度じゃすまないのです。

晚上好~。木更津猫眼(这部电视剧)的剧本集是文库版，所以不小心买了腐゛・・・じゃなくって（あーあぶない）Blue Cat♪光是读卷末的语句解说就很有趣(微笑)。

那么这周的感想。  
“所以我不会离开这个城市……无论发生什么!”好厉害啊，说得好香!这么一来，早百合应该多少明白了吧。我很高兴能了解到香对新宿的感情。也许正是这种想法把香莹引向了新宿。最后一页的香，非常漂亮。啊，年轻时槙村穿制服的样子，真可爱啊~
从窗户目送阿香等人的獠···。看起来是那样说着理解我们的想法的··。
预告中提到的“香悲伤的过去”非常令人在意，如果能描写香对獠的感情的话就很开心了(心跳加速)。


### [11997] 呼腐ﾞた	2003.10.09 THU 11:59:02　
	
呼腐ﾞたです！
午後から息子の七五三の衣装を探しに行きます。もち！レ・ン・タ・ル(^_-) －☆デジカメ持ってLet's　Go！！

＞[11994] 腐十貯（ふじっちょ）さま
アワワワァ～（⌒◇⌒；） すごい影響だぁ～
腐ァラオみろりさまの手にかかってしまいましたね。
みろりさまの”気”を分けてもらって早く隊長治してね(^-^)

＞[11992] ミラノさま
大変ですよね、主婦も。休む暇ないし･･･
こぶたは仕事もしているので部屋の掃除をする時間がない！
旦那様に手伝ってもらえたらいいんですが、旦那は「まだ汚れていないから掃除する事ないっ！」て言って手伝ってくれません(σ_-) グスン
休日はほうき・ちりとり・雑巾を持って家の中をはってます･･･ゴキブリのように。それを横になってタンスの上から眺めているネコが羨ましい･･･。

＞[11995] 無言のパンダさま
お大事に！早く回復して下さい。

では･･･あっバンチ発売日！読めるかな？
じゃぁ！またεεε＝(p^-^)p

### [11996] 千葉のOBA3	2003.10.09 THU 10:33:21　
	
おはようございます。

１１９９５　　無言のパンダさま　　腹筋痛むほどセキでるんですか？だいじょうぶ？？？やはり、ほれ、それにはアルコール消毒が一番では！？

１１９９４　　ふじっちょさま　「腐」を十も貯めてどうすんの？？？私は今、飼うならネコを飼いたい・・・。Ｂ型に合っていると思う。ネコ族のほうが・・・・。犬なら柴ワンコ。「わー太」に出てくるみたいなのがいいなぁ・・・。

１１９９３　　リョウ３５７さま　　はじめまして、ですよね？いいなぁ、ミニに乗せてもらえるなんて、うらやましいですーーー！私もミニクーパーのオーナーになりたいと、ずっと思ってましたが・・・。将来買ってくれと、ダンナではなく息子にねだってます。

１１９９１　　ぽん☆さま　　うちにいたインコは拾った？時から病気でした。お腹に大きな腫瘍ができていて・・・。でも、そのあと一年近く、私達を和ませてくれましたよ・・・。うちダンナが小鳥好きなんですよ。

１１９８９　　Ｂｌｕｅ　Ｃａｔさま　　来シーズンの中日の活躍を期待してます！！プロ野球が面白い！！と言われる為にも！ヤクルトもがんばるぞーー！！

１１９８８　　ゆぅさま　　今試験中ですか？うちの次男もそうですよ！がんばってくださいね！！

１１９８６　　ちゃんちゃんさま　　おひさしぶりですーー！お元気でしたか？ＨＰさっそく遊びにいかせていただきます。

１１９８１　　みろりさま　　新宿御苑の薪能って、秋にやります？ポスターが張ってあったような・・・・。

１１９７９　　英輔さま　　フムフム・・・。ビールや焼酎は､度数が変わっている事があるんだなー。私はグラス半分も飲めば腕まで真っ赤になれる、超安上がりな人間です。

今日はお仕事お休みなので、ＡＨを読んで「しみじみ」します。

### [11995] 無言のパンダ	2003.10.09 THU 09:35:25　
	
おはようございます～☆
咳すると腹筋痛～い（あるのか？腹筋。。。）

＞ｙｏｓｉｋｏちゃぁ～ん（９６５）
「寝て宝」。。。なるほど～!(^^)!睡眠は宝だっ！寝る子は育つぞ☆果報は寝て待て～！
それにしても入院中に「北条作品ファン」の友人をゲットするとは、転んでもタダでは起きないところがサスガだ！ｖ（▼∇▼）

＞Ｂｌｕｅ　Ｃａｔさま　ｙｏｓｉｋｏさま
「腐んごむ」の解説ありがとう～♪
つまり「腐女子の道」は「アリ地獄」に腐んごむようなものってこと？！(￣▼￣)ニヤッ

＞腐十兆円さま（９７７・９４４）
例のアルバイトの存在よりも、なぜ「先生」ばかりが経験者？というほうが気になるなー(?_?)
イケニエゲット！⊂(▼(工)▼ﾒ)⊃==C----＿＿＿＿≡￤-）┼＜　ｽﾞﾙｽﾞﾙ

＞英輔さま（９７９）
ＤＶＤの件はウソかマコトかわかんないよ～（汗）でも出たら私も欲しい！

＞みろりさま（９８１）
Ｒ３５７氏はともかくｙ嬢にまで手を出すとは！（￣□￣；）！！さすが腐女子の鏡！"ｏ（▼∇▼）ｏ"　
それにしても王子を手放すとは・・・とんでもない見返りを要求してくるんじゃあ。。。Σ(▼□▼ﾒ)Ｆ氏は時々貸し出すからカンベンしてくれ～！
早百合さんも一人でつっぱって生きるのに疲れたのかも。。。？

＞ちゃんちゃんさま（９８６）
超×１００くらい☆お懐かしいやぁ～～～！！ちゃんちゃんさま～！再会できてウレＰよぉ～♪
また不思議な歌を聞かせてくれぇ～～～っ☆ｖ(≧∇≦)ｖ
『ＨＰ開設おめでとう！』さっそく遊びに行ったよん(o^∇^o)ノ

今日はバン木☆うれしいな♪
でも買いに行くのしんどいな。。。げほっげほっ！(ｰｰ;)　

ではまた～(^_^)/~

### [11994] 腐十貯（ふじっちょ）	2003.10.08 WED 23:55:08　
	
こんばんわ☆どうにか復調してきた腐十貯（ふじっちょ）です(^o^)丿

＞[11979] 英輔さま
マニュアル＝ミッションでは無いんですね(゜o゜)勉強になります( ..)φ書き書き…
＞今回のシリーズ、『ＭＳ』って単語がでてこなかったような気がするんですが
『ＭＳ・ＭＡ』って言ってたと思いますよ(^o^)全部見た訳ではないですが、結構聞いた気がします。

＞[11980・11992] ミラノさま
さらに穴を深くするようで恐縮ですが…僕のＨＮもぶ（Ｂｕ）じっちょじゃなく“ふ（Ｆｕ）”じっちょなんですよ (^^ゞ初めてだから間違えるのは仕方ないですよ(^_^)
でも『ぶじっちょ』って久々に聞いたな…確か誰かも間違えてた気がする…見分けにくいのかな？

＞[11981] みろりさま
＞そんなコトで自重するようなリョウなら、香も苦労しないと思うゾ。
それもそうですね(-_-;)それじゃあ、紫苑が美人にもかかわらずリョウのモッコリレーダーが反応しなかったからかな？
（紫苑が年齢対象外と見抜いたor紫苑が＠＠＠だから？？）(笑)
＞熨斗つけて、じゃない、おリボンつけて進呈しちゃう。
こうして僕は生贄…もといお婿に出されるのネン
強制(▼▼メ)(Ｔ△Ｔ｜｜)(▼▼メ)連行中

＞[11982] 圭之助（けーの）さま
僕の携帯のアドレスも「1919」と「XYZ」入ってます(^_^)ただ後で気付いたんですが友達ともろ被りだったと言う…(笑)
なおＰＣのアドレスにも「1919」や「3298」・「XYZ」入れようとしたら親に却下されてしまった(-_-;)

＞[11975] リョウ357さま＆[11982] 圭之助（けーの）さま
僕も乗るんだったら絶対ミニクーパーですね(^_^)もち赤で最近ではミニクーパーを見つけるとすかさずチェック入れてしまいます(笑)

＞[11983] こぶたさま
伝染っちゃいました(笑)かなり酷く…だって名前全部変わったし…(爆)
みろり女王（あれ王子だったんじゃ？）のムチさばきから逃れられない…(T△T)

＞[11986] ちゃんちゃんさま
はじめまして(^o^)丿５月ごろから書きこまさせていただいてます“ふじっちょ”と申します(^_^)これからも宜しくお願いしますm(__)m

＞[11988] ゆぅさま
ここの回転は恐ろしいほど速いですからね、あ…………っと言う間に書き込みが過去ログに移ってしまいます(笑)ゆぅさまが書き込んだのはちょうど日曜～火曜までの加速期でしたからね仕方ないですよ(^_^)早くここの速さに慣れて下さいね(^o^)丿

それではおやすみなさい(-_-)zzz

### [11993] リョウ357	2003.10.08 WED 23:21:36　
	
 [11981] みろり さん★
バンチ買いだしたの最近なんですよ・・・ってことは結構長いことバンチを買って、溜め続けなきゃいけませんね★
でもやりますよ^^!!　単行本が僕の買い始めたバンチに追いついたら、またお知らせしますね^^★
 [11985] ぽぽ（Hyogo）さん★
 [11982] 圭之助（けーの）さん★
そうなんですよ・・・旧型のミニはもう作られてないんです★だから中古で程度のいいのを見つけるしかないんです★
叔父はCH ファンだからミニに乗ってるんじゃなくって、ただミニが好きで、レースに出るために乗ってるんです★
もう１台７(セブン)って言う車にも乗ってるんです、オープンのF１みたいな感じの車です★
もう４２なんですけど、驚く事に見た目が凄く若いんです★この前、叔父と一緒にモデルガン(パイソン３５７)を買いに行く約束をしてて、叔父が学校までミニに乗って迎えに来たんです★そんで、次の日学校に行ったら叔父を見た友達達が『お前兄貴いたのか?』って言うんです・・・年齢差２３歳ですよ・・・でも見栄えは僕とたいして変わらないんです★
格好も短パンにTシャツって感じで★
でも、僕の自慢の叔父ですね^^あんなにカッコイイ４２歳はいないと思います^^★
１回、叔父の出るレースを見に行ったんですけど、超かっこよかったです^^★漫画とアニメの中でリョウがモノ凄い運転をして街中を走り回るけど、それが目の前で実際にそう言う走りをしてるんですから(街中じゃないですけどね)★
実際に乗せてもらったこともあるんですけど、いいですよアレは^^★ラリー使用だけあって、最高のエンジン音です★
もうミニクーパーにメロメロです^^★
叔父がのってるミニもリョウと同じ赤色なので、これからもチョクチョク乗せてもらうつもりです^^★☆★

### [11992] ミラノ	2003.10.08 WED 22:08:36　
	
あー明日バンチ楽しみのミラノです。

>ぽぽさま
ひゃあ～あたしったら間違えちゃて。恥ずかしい。
穴があったら入りたいわ。(汗、汗、汗)
ぽぽさま、ごめんなさい。これからはちゃんと気をつけますね。

>こぶたさま
子供がいらっしゃたんですか!!わぁー緒だわ。
なんかますます親しみがもてます。
そうなんですよね、主婦っていろいろと大変。
よーくわかります。こぶたさまの気持ち。
世間では、主婦って案外暇だと思っている人いるけど
そうじゃない。ものすごく忙しいんですよね。特に
子供がいると余計に。だからうちでは、だんなさんに
家事、子守もてつだってもらってます。そうしないと
ストレスがたまちゃう。

>みろりさま
わたしもりょうに恋してますぅ。(だんながいるけど)
だって、だって、だぁってかっこいいんですものぉ。

>ゆぅさま
はじめまして。ミラノといいます。あたしもまだつい最近
みなさんの仲間に入れてもらったんです。
よろしくお願いしますね。

早くあしたになぁーれ。パンチの日だ。

ふぅ。寝るか。みなさま、おやすみなさい。

### [11991] ぽん☆	2003.10.08 WED 21:51:26　
	
けーのさま☆＞うちもセキセイですが、私の友達は全然
インコが好きな人がいないに等しいので嬉しいです♪
顔を指にすりすりしてきた子はやはり小さい時から育てた
子はそうなりましたが、ポンはもう8ヶ月位たっていて
毛並みがすっかり揃っていて、しかも全然鳴かず、困って
いたらゴミ袋の「クシャクシャ」って音に反応しだして
鳴いてくれました（TT)本当良かった・・（今やうその
よう・・ちなみにお隣さんからわざわざ頂きました）
カゴの前で「帰りたいの～」って人間語喋ったんですか？？
それは凄いかしこい～☆うちも「ぎゃぎゃ！」とは
合図しますよ♪（寝る時に布かけろとか、音楽うるさいとか）

みろりさま＞これも腐女子のヴァリエーション!?
ではないですが色々かしこいインコちゃんは喋ってくれる
ようですよ♪人間と会話する子もいますよ☆
（本では生後3ヶ月の間に凄く言葉を覚える
んだそうです～）


### [11990] ぽん☆(ぽ、です～ぼではないですよ☆）	2003.10.08 WED 21:39:19　
	
今話題のCHのパチンコのHPを主人が見つけたので
みなさま、良ければ見てみて下さいね♪私もしてみたい～

http://www.pandora.nu/pachitan/cityhunter.html

です！（＾O＾）♪

### [11989] Ｂｌｕｅ Ｃａｔ	2003.10.08 WED 19:37:55　
	
　こんばんは～。　明日はバン木（わーい）お忘れなく♪

＞[11979] 英輔 さま
　“女の子”、子？　や～ん嬉しい（笑）　もう子って年でもないんだけどな～（精神年齢は置いといて）。うん、わたしは三河っ子だでね～、忘れんでね♪　三河弁の代名詞は「じゃんだらりん」なのに、地元以外では知られとらんだね。嫌味っぽくあげた言い方じゃなくて、三河弁のはもっとなちゅらるなんだけどなぁ。

＞[11985] ぽぽ さま、ぽん☆ さま
　あああ、ごめんんさいっ、わたしもずっと濁音のぼぼさまだとばっかり思ってました、地名つきなのでてっきり“ボボ・ブラジル”（名前しか知らないけど・・^^;）から取ったんだと（汗）　で、ひょっとして、と思って字の大きさを最大にして見てみたら・・・ぽん☆さまも半濁音だったのですね（汗あせ）　どうもすみませんでした～（ぺこり）。

＞[11972] 千葉のOBA3 さま
　少なくとも、Ｓッチーと違って裁判沙汰になるような騒ぎは起こさないと思うし（笑）

　というわけで、落合監督正式決定です。わたしは会見での言葉を信じたいと思うし、楽しみだなぁ♪

### [11988] ゆぅ	2003.10.08 WED 19:13:32　
	
千葉のOBA３さま、chikkaさま、ふじっちょさま、英輔さま、ぽぽさま
お返事ありがとうございます。
えっと、二重投稿してしまって…ごめんなさい（>_<）
６日にかきこみしたんですけど、なぜか見当たらなくてミスしちゃったと勘違いして、また同じ内容を送ってしまいました…。
あぁ～はずかしいです！！携帯ばっかいじってるのでパソコンはまだ未熟…ですが、さっそくのお返事とても嬉しかったです☆
明日はバンチの日ですね。現在中間テストの真っ只中だけど楽しみです♪（勉強しなきゃ…）

### [11987] ayako	2003.10.08 WED 16:59:37　
	
ちょっと思ったんですけどＣＨのパチンコ出るじゃないですか～広告に「ＣＲ　ＣＩＴＹ　ＨＵＮＴＥＲシリーズ」と載っていたので続編出るのかな？よくＰＳ２でパチンコゲーム出るけどＣＨも発売して欲しいと思いました。出るかな？

### [11986] ちゃんちゃん	2003.10.08 WED 16:22:11　
	
おおおおお～～～！！
みなみなさまお久しぶりでございますぅ～！
だーいぶ前にこちらにお邪魔させてもらってた
ちゃんちゃん　です　はじめましての方よろしくお願いいたします！
無言のパンダさま（娘さんも）、BｌｕｅＣａｔさま、
千葉のＯＢＡ３さま、ｃｈｉｋｋａさま。
おなつかしゅうございます～
みなさんお元気そうでなによりです
久々にお邪魔してちょこっとだけスレ見ても懐かしいお名前があるので嬉しかったです。

今日はちょっと最近作ったわたしのＨＰの宣伝にやってまいりました``r(^^;)ポリポリ
はじめましての方もぜひ寄ってみてくださいね！
超初心者用の本を片手に作ったばかりですのでこれからどんどん内容を濃くしていくつもりです。
特に「痛い痛いの物語」という心でなく体に痛い笑える怪我自慢のコーナー作ってますので投稿もお待ちしてまーす！
とりいそぎ本日は宣伝まで
またお邪魔しまーす

### [11985] ぽぽ（Hyogo）	2003.10.08 WED 15:54:03　
	
はっ、バンチ明日発売！！連休前の発売日は早い、やたぁ！ （⌒｡⌒）

＞[11980] ミラノさま
はい、ぽぽ（POPO）もとい、ぼぼ（BOBO）で～す。
笑けました〝w（´ワ｀）w〝 濁点と半濁点、見分けにくいですものねι
ぼぼ（BOBO）、おもしろいです。ボノボじゃない。
あ、ぽぽ（POPO）です♪
リョウと香にまた誌面で会えるって、嬉しいですね☆ （＾∇＾）

＞[11975] リョウ357さま
なにをおっしゃるうさぎさん♪
謝る理由なんてないない♪大好きな思い、語り合いましょ♪
ミニクーパー買うんですか☆熱いですね！ミニクーパーに乗ってませんが
街で見かけると、車が通り過ぎるまで「りょうちゃんの車！！」…と目が
離せないです。赤に白天井のアニメ版のミニクーパーはよく見かけます
が、黒天井のミニクーパーは見かけないですね。渋くてカッコいいと思う
けれど、ないのかな？黒天井のミニクーパー、もしあったら
お買いになります？ （＾－＾）

＞[11974] ゆぅさま
はじめまして。よろしゅう～～ ｗ（＾ワ＾）ｗ

### [11984] こぶた	2003.10.08 WED 12:43:57　
	
＞[11982] 圭之助（けーの）さま
少しは元気になりましたか？
セキセイインコ、こぶたも飼っていました。言葉をしゃべりますよ！ＴＶから聞こえた鶏の鳴き声「コケコッコー」を言ってみたり、人の笑い声を真似したり･･･「カア～カア～」も言ったな？
一言で覚える賢いインコでした(^-^)

### [11983] こぶた	2003.10.08 WED 12:38:15　
	
腐女子の仲間入りした「こ腐ﾞた」です。
＞[11981] 腐ァラオみろりさま
腐女子の道！行くぞぉ～εεε＝┌（￣▽￣）┘
お手柔らかにね(^o_o^)
こぶたンちの猫達の柄は、アメショウとアビシニアン（レッド）です。２匹とも良いネコ同士の掛け合わせで雑種ですが･･･
♂だから大きくってね(^.^;

＞[11980] ミラノさま
こぶたンちと一緒！うちも子供２匹･･･いやいや２人（それとネコ２匹）にぎやかを通り越して毎日台風とやりとみろりさまのムチが飛び交うくらい大騒ぎです。
忙しいですよね。仕事に主婦に母･･･
ここに来てみなさんの元気を分けてもらっています。
私も主人に教えてもらったんです。エンジェルハートのこと。本屋に行ったら「あの冴羽りょうが父親に！！」。それを見つけて早速本を買ってきてもらいました。

＞[11977] ふじっちょさま
伝染していますよぉ～！「腐じっちょさま」にしなきゃ！
いやいやもうなっていたりして･･･？
腐ァラオみろりさまがムチを片手に･･･あっやばいε=ε=(ﾉ>_<)
ＣＨＰＧ早く見つかるといいですね。

### [11982] 圭之助（けーの）	2003.10.08 WED 12:26:41　
	
今日もやや睡眠不足な圭之助どぅえす(σ_-)
でもメゲズにレスしたいと思いまふ…。

＞[11975] リョウ357さま
ミニクーパー、いいですよね♪色はやっぱり赤ですよ！
でも今回リニューアルしちゃったでしょ？旧型はもう生産してないんじゃないのかな？在庫である分だけとかなのかしら。詳しく知らないんですけど…(;^_^A
ペーパーＤなけーのは、きっとこの先も車を運転する事はナイと思いますが、買うならミニがいいです！！現実は、もっぱら１００均のミニカーや、プラモのミニクーパー集めに精を出しておりますが☆

＞[11979] 英輔さま
励ましのお言葉、有難うございます（笑）もう少しで気弱になるところでした…(^^ゞ

＞[11977] ふじっちょさま
下駄はダメだなぁ。末端冷え性だし、鼻緒ですぐ靴ズレ（鼻緒ズレ？）するし（笑）圭之助の携帯アドレスには「1919」も「3298」も入ってます☆

＞[11976] ぶよ☆さま　＆　[11981] みろりさま
インコの固体にもよるけど、結構言葉覚えるみたいですよ。じぃーっと、口元見てますね。何か食べてる時も口元見て、おねだりしてきます。（ホントはやっちゃいけない）過去のうちのコ達は、鉛筆で書き物してるとかじりにきて邪魔したり、首筋掻いて～ってたまにスリスリ指にじゃれてました。あと、おうち（鳥かご）に帰りたくなると、玄関マットの上まで行って振り返って「入りたいの～」って鳴いてましたよ。

ＣＨのパチンコがようやく出るんですね。圭之助は賭け事、まるっきしダメ人間だけど、見たいなぁ♪日○スポーツに広告が載るって？それって大阪でも見れるの？全国一斉刷りなのかしら。見たいよー☆

### [11981] みろり	2003.10.08 WED 02:02:05　
	
こんばんわ～
やっとウチに帰ってきた午前0時49分
BBSは今日も元気でウレピー！！

＞[11963] 里奈さま
うぉぉぉ～！相変わらずエネルギッシュでいらっしゃる。もうすぐ誕生日、っつーことはてんびん座なのねー。でも、ハピバメール出しても読んでもらえるのは帰って来てから!?それともハワイであけてくれるのかな～

＞[11964] 無言のパンダさま
学生の頃人気の的のバイトだったよ。ソレ。一日一万というウワサで。誰もがやりたがってたけど、不思議とやった人がいなかった…。在庫はーこないだ後宮入れしたF氏しかいないから、熨斗つけて、じゃない、おリボンつけて進呈しちゃう。みろりはｙ嬢とかR357氏を狙うことにするわ。うふふ
早百合さんなんかあせってる気がするの。急に熱した恋は冷めるときも急よ～。結局阿香は血ぃ繋がってないんだし…なんであんなに結論出したがるんだぁ

＞ [11967] こぶたさま
んまぁ、こ腐ﾞたさまも腐女子の仲間入りね。腐女子の道はつらくキビシィわよ～。挫けずついてくるのよー！
ウチのコは二匹とも♀さ♪真っ白コと真っ黒コ。ちょっと前までなら、ペンギンカラーって言ってたけど、最近はパンダカラーって言う様になっちゃったゼ

＞[11969] Ｂｌｕｅ Ｃａｔさま
おお、無論、無論、我慢はいけませんゼ。そっちの寒さが想像できなくってっさ。健康第一ヨン。失礼しました。
ＡＨはファンタジー。そ－だね。北条先生の絵って精美だし、リアルだけど、ストーリィだけ考えるとファンタジィだわ…。だけど、いきなり出てきた、おっさんに言われるより、レギュラーの誰かに言って欲しかったよ。その辺がとってつけたような感じがするんです。クスン

＞[11970] ayakoさま
みろりは実は新潟出身なのさ♪だからayakoさまの仰る
＞ 敷地はすっごい広い…ってよく分かるよ。パチンコ屋の建物より、べらぼうに広い駐車場、そこにラブホ真っ青など派手な電装されてんのよね。
ダイジョ－ブ！ガラガラなら、見てる人も少ないから…

＞[11972] 千葉のOBA3さま
え！？あれ、新宿御苑でしたか★でも、不思議じゃないもんね、新宿にすんでる二人なんだから…
みろりは薪能でしか新宿御苑入ったことないのです。暗かったからさっぱり分らんかったです～

＞[11973] ぽぽ（Hyogo）さま
ノラちゃんたちたくましいよね。そこいくと、飼い猫たちは伊達毛皮だわ。毛羽立つなんて、宅急便がきた時ぐらいだもんね…。

＞[11975] リョウ357さま
やーだ、謝らんといてくださいな♪しかし、いつからバンチ買ってるの？初恋編？だとすると12月まで9巻出ないよ～～
それと、叔父さまもひょっとしてCHファン？

＞[11976] ぶよ☆（ぽん）さま
これも腐女子のヴァリエーション!?
インコってひょっとして、教えると言葉喋るの？

＞ [11977] ふじっちょさま
＞やっぱ彼女がいる家にホイホイ連れこんじゃったら…
そんなコトで自重するようなリョウなら、香も苦労しないと思うゾ。
プリンス戦争はそろそろ終結ね。みろりも腐ァラオに出世したし。で、ものは相談だけど、やっぱ、パンダ姫のお求めの王子さまになってやってちょ。あちらはプリンス＆プリンセス、こちらは王子＆姫。はい、一本締め！お手を拝借！よ～っつ！

＞[11980] ミラノさま
＞ これもまさしく運命ですよね。
わかるわ！恋って運命なのよぉお。リョウに恋するみろりです…

ふぅ、カキコに１時間…いいかげん、寝なきゃ…
