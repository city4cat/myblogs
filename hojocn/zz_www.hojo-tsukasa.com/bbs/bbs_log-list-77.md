https://web.archive.org/web/20020821055223/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=77

### [1540] 法水	2001.07.23 MON 11:38:20　
	
＞Goo Goo Babyさま。

ああ「パロディ小説」ってあれのことだったんですね。ログNo72でも書いたように、別に目くじら立てるようなものでもないと思うんですが…。正直言って僕は流してました（笑）。僕はパッと見て自分が興味ある内容かどうかで読んだり読まなかったりするので、ひとつひとつちゃんと読む方にしたらしんどかったのかも知れませんね。

### [1539] 来生（ΦΦ）	2001.07.23 MON 11:31:42　
	
おひさしぶりです☆

この度、北条先生に関するＨＰを開設しました。
が・・・ほとんどのページで、みなさんからの投稿やご意見
などが必要だと思い、お願いにあがりました。
まだまだ未完成のページなのですが、北条作品ファンのみなさま
からのご意見やご投稿をお待ちしております！！
宜しくお願い致します！！

### [1538] みゅう	2001.07.23 MON 11:29:26　
	
ごめんなさい。↓の書き込み改行失敗して見づらい上に
柚季さんを呼び捨てにしてしまいました。<(_ _)>

### [1537] みゅう	2001.07.23 MON 11:25:49　
	
おはようございます。
昨日の夜1晩の間に様々な意見が交わされたようですね。
私はバンチ火曜組ですので、今日の深夜コンビニへ走ります。

AHについては、はっきり言ってしまうと、私は楽しく読む事が出来ません。(^^;
香の死の上に成り立っている割には、GHのキャラが薄すぎでは？
香というキャラに押しつぶされているのが現状だと思います。
もっともまだ10話ですし、北条先生が「少女の成長物語」とおっしゃっていた事から、
これから人格が形成されていくのかな？とも思いますが･･･。
CHファン、北条先生ファンは多少ストーリーが動かなくとも読んでいけるだろうけど、
前にも書いた一般のバンチ読者が、GHのキャラ立ちが済むまでついてくるのかどうか。
今の現状では個人的にかなり不安です。
GHの好き嫌いについては、どちらとも言えません。
今の時点では判断材料が乏しいからです。

＞姫さん
私は普段からあけっぴろげというか思った事を口にする性格なので、
ここでの私と普段の私はあまり差はないと思います。
きつめの言い方をしてしまう癖がある事を自覚していますので、かなり考えて書いていますが、自分の過去レスを読んでいて、自分でも
「これは感情的だったかな」「ちょっときつい書き方だったな」と思う事があります。
冷静なつもりで書き込んでいても、後で見ると、
書いてる時には気づかなかった事に気づいたりして。
そう思うと、ひろさんがおっしゃってたように、
送信する前に文を読み直して考える事が大切ですよね。

＞柚季
タイトルのお話、とても納得しました。
今回は話題をどう分けたらいいのか分からなかったので、
次から見やすいようにつけようと思います。

### [1536] あお	2001.07.23 MON 10:21:49　
	
ちょこっと気になることがあったので。

ネット上の情報開示の件＞
ここにカキコしていらっしゃる皆様、けっこう自分の家のこと、誕生日のことなど、かなり個人情報をカキコしていらっしゃるような気がします。悪いとはいいませんが、不特定多数の方が見ていることを考えると少し危険かな、という気がします。カキコしている皆様は北条先生の作品が大好きな方で、たとえ情報が開示されても悪用しない方であることは、文面を見れば分かります。しかし、読み手全てがそうだとは限りません。情報が金になる時代、日本はこの手の問題に対する法整備が遅れています。もう少し神経質なくらいでいいと思うのですが、いかがでしょう？？自衛手段はそのくらいしかないですから。

掲示板問題＞
＞201様
毎日過去ログを見ているわけでもないでしょうし、このHPに来ている訳でもないでしょうから、あれ以来カキコなさっていないのも分かるのですが、法水様の意見同様、もし見ていたら、何らかの答えを返すべきだと思われます。問題を提起した張本人が｢終了｣させるようなコメントを発しない限り、しりきれとんぼ状態になってしまいます。せっかくの問題提起なのですから、発言をお待ちしています。

＞みなさま
過去ログを見ていて少し危険だと思ったのは、数人の方のコメントが非常に攻撃的で、きつい表現を用いていたことです。読み手側として、かなりドキッとしました。201様のコメントを見る限り、そこまで感情的になる必要はないと思います。むしろ、問題提起は重要だと思います。そういう人もいて、初めて社会が成り立っているんですから。今のBBSの雰囲気は、201様がコメントしづらい状況を作り出していると思います。読んでいてまるで異なる意見を排除しようとしているかのような印象を受けました。｢みんなで楽しむ掲示板｣を目指すのならば、異なる意見の人も受け入れるくらいの気持ちでいなければ無理ですよ？感情的になりすぎない、攻撃的･直接的できつい表現は慎む、というのも、ネチケットに当たると思われます。

皆様はいかがお考えですか？
私自身の表現にも問題があると思われましたら、ぜひご指摘ください。直していきたいと思います。

### [1535] ひろ	2001.07.23 MON 07:52:19　
	
皆さんいろいろな意見をお持ちですね。
当たり前ですけどね＾＾
みんながみんな同じ意見だったらつまらないですし・・
お互いの意見を尊重してみんなでより良いものを作っていけばいいんですよね♪

柚季さんのカキコ・・納得してしまいました♪
建設的な意見ですね。今までとまったく切り口が違う。
でも解決方向に向かってるし。頭いい人と思ってしまう。

皆さん良識ある人だと自分も常々思ってますよ。
柚季さんが言う様に他の掲示板ではこうはいかないですもん。
暴言吐いて書き逃げが横行してますもんね。

ネットの世界は文字だけの世界・・
良くも悪くもそれは事実です。
体に負う傷も痛いですが文字での攻撃は心に深く突き刺さります。
しかもなかなか治るもんじゃありません。
あ、ここの人たちは攻撃というほどの事を書いてる人はいませんでしたね(汗)

でも自分で気づかないうちに相手を不快にしてしまうこともあります。
「送信」を押す前にもう一度良く読みなおし、
他人にこれを言われたらどうかな？？と考えてから送信することも大切ですね。
リアルタイムで話してるわけじゃないので良く文を考えて送信すると良いと思います。

って今読みなおしたけど俺偉そうなことかいてるなぁ・・(苦笑)
でも良く考えた上で送信♪(笑)

不快な気分にさせた方がいらしたらすいませんm(_ _)m



### [1534] 天使の心＠寝不足	2001.07.23 MON 06:37:50　
	
オハヨウございます♪
お断り：：私の発言に際し気分を悪くされた方が
いらっしゃいましたらすみません

【レスの嵐】
＞柚季様
　なるほど・・・って思いました。タイトル確かに参考に
　なりさっそく使わせていただきました。でも忘れたら
　ごめんねって事で。使いにくい掲示板をどう使いこなすか
　っていうのも私たちの工夫ですね。

＞りなんちょ様
　祝！復活！おおまかにわけて・・・私は後者です。
　今のところ。今後はわかりません。もち！北条先生の
　作品が好きだからいろんな感想がでるんですよね。

＞GooGooBaby様
　＞タコ？・・・（爆）やばい。笑いすぎてお腹痛いし。
　

### [1533] ツバサ（ふう・・・）	2001.07.23 MON 06:29:05　
	
追伸
私はバンチ月曜組です

### [1532] ツバサ（ふう・・・）	2001.07.23 MON 06:27:53　
	
おひさです。
仕事の都合上、ジッポの申し込みを夕方したら「売り切れ」とでました。仕事をやっている人間には買うなということでしょうか？
幻滅です。受注してから生産すればいいのに・・・・。
ただただ悲しいです。

### [1531] 柚季	2001.07.23 MON 04:51:37　
	
【掲示板問題について】

先ほど最近の過去ログをざっと読み終えたのですが、自分なりに思うところがあり、新参者ですが書き込ませて頂きます。
私個人の見解としては、こちらの掲示板は、北条作品を好きな人たちが自由にコメントを残す場所であり、その内容については、ＡＨのみならず過去の作品についてであろうがアニメについてであろうが、又ファン同士の交流目的であろうが、良識に則ったものである限り、制限されるべきものではないと思います。
ただ、アクセスする側にはそれぞれここに求めるものが違い、正直この問題について語ること自体にうんざりされている方もいらっしゃるでしょう。

一番問題なのは、膨大な量の過去ログの中から、自分の求める内容の書き込みを見つけることが難しいことにあると思います。
こちらの掲示板には、なぜかタイトルを書き込むスペースがありません。故に、掲示板問題についての書き込みなのか、私的な書き込みなのか、ＡＨについての書き込みなのか、それが一度ざっと内容に目を通してからでないと判別出来ないという不便さがあります。
また返信機能もないですし、スレッド表示も出来ません。
敢えて注文をつけさせて頂くならば、これだけの書き込み量なのですから、スレッドを個々に立ち上げられるような形の掲示板機能にして欲しいと思います。
しかし管理サイドへそれらの注文を付けたとして（一応メールしました）、即刻の対応は有り得ないと思えます。
ならば利用する側が、少しでも使い易いよう見易いように配慮をするしか、この件に関しての前向きな対処法はないと思えます。
私はこの書き込みの最初に、敢えてタイトルを付けました。
これにより、この書き込みの内容がおおよそ予測出来、興味のない方は読まずに済むと考えたからです。
こちらの掲示板に書き込まれる方たちは、皆さん極めて良識的な方たちばかりだと思いました。大抵の掲示板では、議論が白熱すると見苦しい暴言も飛び出すものですが、意見の相違はあれど、この掲示板を大切に思っていらっしゃる気持ちは皆さん変わりないと思います。

二回目の書き込みで、早々に出過ぎた真似をしてしまいました。
けれどここが、北条ファンにとっての、楽しく、和やかに、活気ある、そんな集いの場所になれば素敵だなと。
そんな思いから書かせて頂きました。
差し出がましい真似をして申し訳ありませんでした。

### [1530] あっきー	2001.07.23 MON 03:37:35　
	
＞なちさん

私は、AHは「CHの正当な続編」としか考えていません。
これだけキャラと世界観を引っ張っていて、CHの内容をある程度知っていることが前提となっている点を考えると、全く別作品だとはとても言えないと思います。

北条さん自身の口から、「AHはCHの設定を用いてはいるが、本来のCHとは別の世界の話である」と語られれば良いとも思えません。
私にとっては、逃げ口上にしか聞こえません。（厳しいでしょうか？）
CHで築き上げられてきたキャラの魅力、人物関係など全てを破壊しつつ、AHは描かれています。
なぜわざわざ壊すのか？それは、壊すことによってより良いものを生み出す可能性を模索するため。
そうでなければ、わざわざCHの世界を傷つける理由がありません。
ですが、今のところ壊れただけで、再建が全く進んでない印象を受けます。
爆破は華々しかったけど、今はその後の解体作業だけでいっぱいいっぱいという感じです。
何せ、屋台骨であるべきリョウが折れたままなのですから・・・

少女の成長物語にリョウを絡める、という設定自体は非常に面白くなる可能性があると思います。
しかし、その話をCHの世界でやる必要は無かったのではないか？
極端な話、冴羽リョウというキャラクターだけを借りてきて世界は全く別とした方が、これほどの混乱は無かったんじゃないかなぁ・・・
CHを一からやり直すことになりますけど。

えーと、あれこれ書きましたけど、AHには面白くなって欲しいんですよ、本当に。
とりあえず、11話（結構、一般の評価の正念場になりそう）がどういう展開になるのかを楽しみにしてます。

在CH构筑起来的角色魅力、人物关系等全部被破坏的同时，AH被描写。
为什么要特意破坏?那是为了探索通过破坏而创造出更好的东西的可能性。
否则，没有理由特意伤害CH的世界。
但是，现在给人的印象只是坏了，重建完全没有进行。
虽然爆破很华丽，但是现在光是之后的解体工作就已经很紧张了。
不管怎么说，应该是顶梁柱的獠还是断了……


### [1529] Goo Goo Baby	2001.07.23 MON 03:37:07　
	
只今オベンキョは休憩中・・・全然眠くないです（爆）

=========================================================

＞グラス・ハートさん

みんなに感情移入してもらえない可哀相なアナタ。
私は結構アナタに同情してます

だってさぁ、昏睡状態中の時も、起きた時も、新宿に来て歩ってる時も、ＭＹＣＩＴＹの前でも、幻影に付きまとわれてるんだよ？
キミはこわい話ニガテな人じゃないみたいだからイイけど、そうじゃない人だったら絶対気がヘンになっちゃうって。

しかもその幻影、香だけならともかく伝言版まで化けて出てんだもん
可哀相だっての。

そして、リョウという生きる希望を見つけたから新宿まで来たのに、その気持ちが実はアカの他人のモノだって知ったらさぁ、アンタじゃなくても「なんなのっ!?」って思うわな。
やっぱ可哀相だよアンタ。

そんな失望状態のキミを見ても顔を赤くしてテレてる香を見たら、やっぱ鏡に怒りぶつけたくもなるわな。

　・・・・でもね、心臓をブッ刺すコトないじゃないのさ
　
=========================================================

＞海坊主さん

「よせ、その名は捨てた」
でわ、私はアナタをこれからどう呼べばよろしいの？
ファルコン？　伊集院さん？　タコ？　
　

### [1528] 柚季	2001.07.23 MON 03:09:27　
	
初めまして。
ＣＨを知ってから北条先生のファンになり１×年・・・（笑）　ちなみに我が家の車はミニですv（私のリクエスト！）
ＦＣが終わって次の作品を心待ちにしていた自分としては、ＡＨの連載開始はとても嬉しかったです。勿論、香が亡くなっているという設定に対し、複雑な気持ちは他のファンの方たちと変わりませんが。
先生の、人間に対する厳しくも温かい眼差し。
それをこの作品でも感じることが出来たらいいなと思っています。

### [1527] りなんちょ（汗がダラダラ…）	2001.07.23 MON 03:04:56　
	
よしっ！！！
みなさんから、励ましのメールやカキコをいただいて、
元気が出ました！！！
ありがとぉ～う！！！

え～っと、Ａ・Ｈのことですが、
みなさんの気持ちが良くわかりました、
おおまかに言ってしまえば、
戸惑いもなく読める人もいれば、
まだ、素直に読めない人もいる
ってことでしょうか？
あたしは、戸惑いもなく素直に読んでる方ですが、
それはそれで、良いってことで…
みんな、それだけ北条先生の作品が好きなんだよね？

### [1526] ＣＨＡＲＡ☆チャラ	2001.07.23 MON 02:50:19　
	
すいません、名前のところ、☆だけになってましたね・・・。

### [1525] ☆	2001.07.23 MON 02:49:21　
	
残念ながら、あたしもＡＨにはあんまりいい意見はいえない感じです。でも、それはＦＣを読んでないせいもあるのかなーって思いました。
最近、少しＦＣを読むようになったのですが、絵の感じとか、人物の描き方とか、ＡＨを描いている北条司はここからきているのかなーと思われる部分がけっこうありました。
ＣＨが大好きで、ＡＨにもあのおもしろさを期待してしまうのですが、北条先生自身が変わってしまったのだろうなーとか思います。ＡＨが良い悪いじゃなくて・・・。
好きか嫌いかでいうと残念ながら、まだ好きになれる要素がみつからないです。回想シーンはすっごく嬉しい反面、過去の絵と比べてしまうので、微妙な感じです。
素直に、いま自分の感想を述べて見ました。
ＡＨについて、いろんな意見があるとおもうけど、まあこんな意見もありますよ～ってな感じでうけとめてくださいな☆☆

### [1524] なち	2001.07.23 MON 02:12:17　
	
　＞ＡＬＬ

>Ｃ・Ｈの続きとして考えてるからじゃないの？
>違うものとして考えればいいと思うんだけど…
>（一部分引用しました m(__)m ＞りなんちょさん）

この話は結局どちらに落ち着いたのでしたっけ。
確か北条先生はＣＨの続編じゃない、とおっしゃってました。
でも、ＴＶＣＭからはＣＨの続編のような印象を受けました。

半パラレル、知っている読者がにんまりするような仕掛けは読んでいて楽しいです。例を挙げれば『こもれ陽の下で…』の西九条紗羅・隼人や『TAXI DRIVER』の松村渚、『TAXI DRIVER』にちょっと顔を出した香・リョウなどです。
でもこれらの仕掛けは、物語の導入部や物語の大筋に関わってくることはなかったはずです。逆に言えば、そのネタ元を知らなかったとしても内容は変わらない話でした。それなのに今回のＡＨでは、香が死んで幽霊になるのが話の肝になってます。
キャラ設定が若干違うといえど、リョウ・香・冴子・ファルコン・喫茶CAT'S EYEと、これだけＣＨの人物・設定が出てきました。となると、ＡＨの導入部で、「単に香という女が死んだ」のか「ＣＨにおける香が死んだ」のかで話が全然変わってくると思うんです。
前者の場合、ファルコンは直感に優れた体のゴツい喫茶店のマスター、冴子はなぜか警察内に力を持っている便利な人…安易すぎる設定になってしまっています。そして、それぞれのキャラの背景がＡＨを読んだ限りではあまりわからないので、薄っぺらな人物描写にしか感じられません。
後者の場合、登場キャラクターが不憫です。パートナーを失って壊れたリョウ。事件揉み消し役兼語り部の冴子。霊能者となったファルコン。そして、幽霊になりはてた香。これらはＣＨにのめり込んでいた中高生時代の全否定なんですよね…そのうえ、香の死と引き替えに得たはずの重たい設定も、中途半端な軽いノリによってほとんど生かされていないように思います。

北条先生が
　　ある少女の成長物語を描きたい→『シティーハンター』の
　　世界観で、冴羽リョウも絡めたら、面白くできるのかな。
と、「波」に書いていることから考えて、

リョウと香が主人公じゃないからＣＨ２ではないけれど、時間軸的にはＡＨはＣＨの続きであって、１話で死亡したのは「ＣＨの香」と考えるのが、結局の所一番妥当なのでしょうか？

### [1523] みんみん	2001.07.23 MON 01:41:02　
	
＞さえむらさん
打ち切り・・・私が飲み込んでいた言葉を・・・
世間の評価ってシビアです。
私がなんでそんな事を言うかというと、実際の周りの評価なんです。
２０代から４０代の男性がほとんどです（まさしくバンチのターゲットですね）。
「ＡＨってどう？」っていろいろな人に聞いてみました。
内容はここでは控えますが、なんとなく分かる人には分かるでしょう。
だから心配なんです。
＞りなんちょさん
泣きたい気持ち（ＡＨへの否定的意見に対してって事でよろしいですか？）も分かりますが、
私も泣きたいです、今のＡＨに。
現実の評価って厳しいんです。
それが、さえむらさんの言うような処置を取られてしまったら、それこそ読者が置いてけぼりをくらってしまいます。

だから、なんとかしてください。

### [1522] sariel(川崎市だけど新宿の方が近い)	2001.07.23 MON 01:23:31　
	
>グラスハートへの感情移入
まだ無理です！でも、俺はAHはCHの続編だと考えています。CHがあって、AHがある。CHのリョウと香、そして二人の生活があって、今のリョウの悲しみ、香の嘆きがある。ですが、AHはグラスハートの話。主人公はグラスハートです。リョウと香は言い方悪いですが「サブ」だと思ってます。リョウと香の二人の上にグラスハートがいて、二人の物語をグラスハートがグラスハートを中心に作っていく。そんなのがAHのイメージです。まだまだグラスハートは登場したばかりじゃないですか。あれだけ高い話の上に作るんですから、１０話やそこらじゃ登りきれないと思います。
これからのグラスハートに期待しましょう。そして、これから読んでいって、まだリョウと香のことが中心にあるのなら、AHはCHを超えられなかった作品ってことになるんでしょうね。でも、北条先生ならば、きっとグラスハートを動かしてくれると信じてます！

>ちみちみ様
上のような感じで俺は考えてます。CHを読んでいらっしゃるのならば、AHを読んでいて、いずれ良かったと思わせてもらえると思います。立ち読みとかでもかまわないんで、どうか、一緒に見守っていきましょうよ。

>掲示板について
俺なりに、あれから考えて、皆さんの意見を読んで、やり始めたことがあります。
文章で書く事って、自分の意見をまとめられるし、思っていることをより深く自分自身が知ることが出来ると思います。そこで、自分なりの感想・意見を考えて、とにかく書いてみるんです。そうすると、表現が変だったり、「これはまずいな」とかでてくると思います。それを直していって送信するんです。確かに、面倒くさいですし、文章が硬くなってしまいがちですが、会話じゃなくて掲示板なので、一回の書き込みは大変な意味を持つと思います(会話表現も入れたほうがいいところはもちろんありますけど)。だから、こんな感じでやってみてるのですが。まだまだ表現が変だったり、失礼なことを多く言ってると思うので、それは、俺個人への非難なので、出来るだけメールにて、皆さんにも伝えたいのならばここで言っていただければ、直します。
・・・書いてて変ですね(＞＜)。でも、今のところはこんな感じです。俺の参考にもなるので、ご意見をお願いします。

### [1521] Goo Goo Baby	2001.07.23 MON 01:09:22　
	
＞法水さん

ごめんなさい　パロディー小説を書いた張本人は私でございます
ログの４５から５０の間に全７回に渡って書いてしまいました
その頃は、リョウや香に子供ができたとしたら、という話題でココのＢＢＳが盛り上がっていた時期でしたよね
その話に混ざった私が悪ノリして爆走してしまった、というわけです（汗）　すいません
　 

