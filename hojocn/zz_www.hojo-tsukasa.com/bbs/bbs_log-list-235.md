https://web.archive.org/web/20020614090140/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=235

### [4700] Ｂｌｕｅ　Ｃａｔ	2001.11.25 SUN 13:47:03　
	
　もっこりメッセージ、わたしも読んで思わずふきだしてしまいました（笑）　ネタがないと言いつつもこうしてちゃんと更新してくれるのって、やっぱり嬉しいですよね。［4692］露崎なごみさまのカキコミを読んで、そういえばそんな髪型だった気がする、って思いました（笑）「ＬＯＶＥ　ＬＯＶＥ　ＬＯＶＥ」は大好きな曲なんですけどねぇ、リョウのこと考えながらこの曲を思い出すと、とっても切なくなってしまうし・・・。

　ところで、わたし、Ｃ・Ｈ’91のサントラに入ってる「Ｙｏｕ　ｎｅｖｅｒ　ｋｎｏｗ　ｍｙ　ｈｅａｒｔ」という曲に出てくる「アンテウス」という言葉の意味がずっと疑問だったのですが（なんとなく流れ的に香水なのかな、とは思ってたんですけど）、そうか、せっかくインターネットをやってるんだから検索サイトで調べればいいんだ、と今ごろになって気付き^^;　さっそく調べてみました。それによると、「アンテウス」というのはシャネルの男性用の香水で、「優しさと強さを合わせ持つ、男性的なウッディ・レザーの香り」（ちょっと違ったかも^^;）だそうです。歌詞の内容的にてっきり女性用の香水だとおもってたんですけど、違ったんだ^^;　この曲に出てくる男が愛用しているのがアンテウスで、主人公はそれをマネしてつけてる、ってこと？それとも移り香？なのかなぁ。

　わたし、今、Ｆ．ＣＯＭＰＯをまた読んでるんですけど、改めて、やっぱり紫苑って女の子だったんだな、って思いました。雅彦があまりにもいつまでも疑うから、からかってただけ、って気も（笑）　なので、前にわたしが書いた「両性具有説」は忘れてください（ってとっくに忘れてるか）。この作品って、よけいなこと考えずにシンプルに読んだ方が面白いんだな、って今更ながらに思ってます。

### [4699] ｌｕｎａ	2001.11.25 SUN 13:33:15　
	
ちわっｌｕｎａでーすぅ
無言のパンダさん
あたしのドタバタぶり、笑けましたぁ？
目覚まし使って起きたんで、ヤツも目覚ましていて、
隣の部屋から、あたしが、あーーーとか、ちょっと待ってぇとか
やばいよぉとか、色々口走ってた事、聞こえてて、
なにやってんだぁ？って不審に思ってたけど、眠いし、
起きた時に聞こうって思ってたそーです
でっ、実はこーゆう事があってとか、こっちは、必死になって
説明したら、「寝る時間には、寝たほうがいいんちゃう」
って、あっさり言われてしまい、あたしの苦労は、水の泡
となりましたぁ
でっそれもそうやなあと　あたしも思い
運を天にまかせて、ビデオ予約してぐーーっすり寝ました
と　きちんと録画されてたんで、ヤツにも報告して、
「そらぁよかったなあ」と言ってもらい、今朝は、昨日の朝
とは、うって変わっていい気分を味わいましたよ
めでたし、めでたしですぅ

先生のもっこりメッセージ
ドリカムのｌｏｖｅｌｏｖｅｌｏｖｅですね
その妙なクリップをちらっと見た事があります
歌の大辞典やったと思うけど・・・
確か、再放送で、愛くれを見て、オープニングとか
ドラマの中で流れていた時は、すっごくしっくりしていて
いい歌やなぁって思い、ＣＤ買って、それだけを聞いたら
あんまり、・・・と思いました
今でも、愛くれを見たら、そのｌｏｖｅｘ３は、いい歌って
思うけど・・・
ドリカムのファンのみなさん、ごめんなさい
ドリカムのみなさんにも、ごめんなさい
けど、吉田美和さんの声、なんかあたたかくって好きですよっ

### [4698] sweeper(相手も私に銃口を向けている。)	2001.11.25 SUN 11:45:53　
	
こんにちは。ここのＢＢＳはいつもにぎやかですね。

[もっこりメッセージ]
更新されてますね。ドリカムと言えば、「LOVE　LOVE　LOVE」もそうですけど、「晴れたらいいね」も記憶に残ってます。「LOVE　LOVE　LOVE」は手話でよく歌う歌に入ってますよね。(脱線失礼しました。)

[レス]
＞K.H様。[４６９６]
「君の声＠泣い＠いる８月＠長＠夜～」

レス、ありがとうございます。「冴子ちゃん借りを返しちゃう！」です。歌詞は、一部伏字にすれば大丈夫だと思いますよ。TMNほとんど持っているんですかー。どうりですぐにレスが返って来るなーと思いました。セリフの引用に続き、頭が上がりません！(笑)B'zの「ねがい」確かにサビはしっくり来るなと思います。

「・・・すまんミック。銃が暴発した・・・。」
「リョオ～～てめぇー！」「だからあやまってるだろ・・・。」

＞まろ様。[４６９７]
レスありがとうございます。「冴子ちゃん借りを返しちゃう！」です。スズキのCMで確かにき＠ろうがやってましたね。「♪スズキにき～＠ろう～」と歌ってましたしね。スズキには織田裕二さんも出てましたしね。ショートショート楽しみにしてます。

それでは、これにて。

### [4697] まろ（謎の男：えっ？！ボキしゃんれすかぁ？）	2001.11.25 SUN 06:53:19　
	
おっは～です！
もっこりメッセージ見ました！
――ネタ切れかぁ。かくいう私も、とうの昔（←これこれ！）に陥ってたりして（汗）
ちょびっとレスです。

［４６８４］無言のパンダさま
　『百恵の赤い靴』のＣＭのクルマ、判りましたよ～！
あれは、トヨタの初代ターセルという『おやぢグルマ』（※１）だそうです。
ちなみに、初代ターセルは１９７８年（昭和５３年登場）でＣＭは、兄弟車（※２）のコルサと共に紹介されていたみたいです。
百恵さんが出てもクルマの名前がパッと思い出せないのは、トヨタマジック（※３）なのかも。

※１・・・クルマ好きの若者が見向きもせず、保守的な「おとーたま」達しか買わんので、こう言う（苦笑）

※２・・・トヨタには、販売店（メーカーとは別の会社）は、たーくさん在るので、各系列店（カローラ店・ビスタ店など）などに、同じクルマでも、名前や、ヘッドライト（テールランプも含む）の形を変えて販売する習慣がある。それを「兄弟車」と呼ぶ。以前、ハチロクの話をしたが、中身がいっしょのこの二台、カローラレビンはカローラ店、スプリンタートレノはスプリンター店で販売していた（そのまんまやんけ・・・）。

※３・・・私の勝手な造語。大した事のない・ヘッポコなクルマ（失礼）のＣＭに有名人を起用するも、結局、時代に無視されていく・・・（→無言のパンダさまが思い出せないのも無理ないっす・笑）ある種のマジックです、ハイ（苦笑）

［４６９３］ｓｗｅｅｐｅｒさま
　レスありがとうございます。
「りょうちゃん、シカト（古っ！）でナンパしちゃうー！」です（笑）
な、なんかスズキの軽、多いっスね（笑）ボキもジムニーとか乗らないとまずいのかしらん？（某宇宙刑事のよーに・汗）
そーいや、スズキのＣＭでキタローさん、出てましたね。
「♪軽、軽、ス○キの軽～♪」って曲（まんまキタローやん・汗）で（爆笑）。
　話しは変わって、沙羅ちゃんが出てくる話で（後半のあたり）買い物の後、彼女が公園でバッタ（？）を捕まえてるとき、襲ってくる悪漢が乗ってるクルマ、たしかスズキの軽でした。
セ○ボだったかな（→ちゃこさま、すみません！）

［４６９３］ｓｗｅｅｐｅｒ  
谢谢回帖。
“獠，我要用旧的!搭讪你!”(笑)
喂，铃木的轻型车，多得很呢(笑)我也要开吉姆尼之类的车吗?(某宇宙刑警尤尼·汗)
对了，铃木的广告中出现的罗先生。
“♪轻、轻、〇树的轻~♪”这首歌(完全北野·汗)(爆笑)。
话题变了，沙罗出场的故事(后半部分)买完东西，她在公园抓蚂蚱(?)的时候，歹徒开的车好像是铃木的轻型车。
(→茶子小姐，对不起!)

### [4696] K.H	2001.11.25 SUN 00:39:27　
	
もっこりメッセージ、しっかりもっこり更新されていましたが・・・
センセ、ネタないんならそんなに無理して更新しなくてもいいんではないのでは・・・？
　ドリ○ムファンを敵に回しているような･･･

＞sweeper様[4693]

　「アスファルト、タイヤをきりつけながら～」

まいど、K.Hです。セリフネタがちょっと思いつかなかったんで、局地的な歌詞を入れちゃいました。著作権上、まずかったっすかねえ・・
今日、友達と一緒に浜名湖に行って来ました。B`z、ミスチル、globeをガンガンかけて暴走していました。
いまだに「ねがい」の歌詞が頭から離れません。
でも、サビの部分は確かにビンゴです。出だしはちょっとちがいますけどね。
TMNでっか？はい、CDが普及した頃のアルバムは全部持ってますよ。んで、いまだに頭のなかでは歌詞が離れていません、はい。
一番最初に聞いたのが1985年、当時はカセットテープしかなかったんですが、「Be Together」の最初のバージョンの奴を聞きました。以来14年、いまだに持ってます。あ、でも「CAROL」持ってないんですよ、ボキったら。

### [4695] 琴梨	2001.11.25 SUN 00:38:49　
	
こんばんは。琴梨です。

ねぇどうして…
　先生のメッセージ、更新されていましたね。　思わず笑っちゃいました。　「ＬＯＶＥ　ＬＯＶＥ　ＬＯＶＥ」といえば「愛してると言ってくれ」の主題歌でしたね。毎週欠かさず観てて、最終回は泣けちゃった。　まだビデオ置いてあるし。

### [4694] 三毛猫	2001.11.25 SUN 00:33:18　
	
もっこりメッセージ見ました。(^_^;
プロモの髪型はよく覚えてないけど、「サイババ」化ですか？
（火に油を注ぐ発言･･･^^;）
サイババも神様に取り憑かれる前はあんなに頭バクハツしていなかったし～♪
ドリカムのプロモといえば、昔、高校生の時ソ＠ーからビデオを借りて、プロモの上映会をしました。その中でデビュー曲を引っさげたドリカムがいて、一番光ってました。

＞ちゃこ様[4682]
山羊、そりゃ覚えてますよ！！一応、草取り係としてあそこの職員だったんですよね。だけど、あんなに電車と至近距離で仕事をしているものだから、ストレスが溜まって、マザー牧場に引き取られて行きました。(T T)
クリスマスツリーは見たことあります？なぜかホーム向かいの線路土手にもみの木が一本だけ立っていて、今時期になると電飾が入るんですよね。最初ころは感激しましたけど、数年前に台風で木の先端が折れてしまったり、電飾に負けてしまったり、最近元気がないんです･･･。サラちゃん出動だ！！

### [4693] sweeper(「誰っ！」声の主に銃口を向ける。)	2001.11.25 SUN 00:02:48　
	
こんばんは。HN脇のショートショート、展開が気になるという方々がいて嬉しいです。

[レス。]
＞ちゃこ様。[４６７０]
レス、ありがとうございます。「唯香ちゃん突撃取材に行っちゃう！」です。ショートショートはまろ様の話とは連動してません。舞台は、地下射撃場なのですが話になってないです。(滝汗)全文載せるとしたら、かなりの部分を手直ししないと・・・。
それから、B'zレスありがとうございます。運転中に聴くと、指がリズムをとってます。それから、口ずさんでたりもします。(笑)「Crazy　rendezvous」私も好きですよ。あの強引さはすごいです。でも「The　wild　wind」元気がない時に聴くと稲葉さんの歌声が優しくしみこみます。

＞kinshi様。[４６７１]
レス、ありがとうございます。「唯香ちゃん突撃取材に行っちゃう！」です。でも、その前にりょうと海ちゃんが許しません。(笑)確かにあの３人と行くと、私は引き立て役です・・・。ホームパーティーならにぎやかそうです。

＞K.H様。[４６７８]
「わあお、美樹ちゃん。りょうちゃんさびしかったぁん。」
「あ･･･あら冴羽さん・・・きてたのぉ・・・」
お久しぶりです！お帰りなさいませ♪
ちょっと思ったのですが、ＡＨの今後をB'zの歌にたとえるなら「ねがい」なんかもしっくり来るかなと思います。ところで、TMNの曲は全部持っているのですか？すごく詳しいので・・・。

＞無言のパンダ様。[４６７９]
「ゴー！ワゲノー！」ご使用いただきありがとうございます。(笑)元は、レオナルド･ディカプリオが出ていた「スズキ･ワゴンR」のCMが元ネタです。車、ワゴンRなんですかー。私はスズキアルト･エポです。新車ですがはじめて買った車なので愛着あります。まだローンは終わっていませんが・・・。

＞まろ様。[４６８０]
車、詳しいですね！冴子さんはノベライズで「ポルシェ･９１１ターボ」に乗っています。そのせいか「冴子さん＝スポーツカー」と言うイメージがあります。香ちゃんはスバルのプレオが似合いそうです。冴子さん。カブリオレも似合うかなと思います。

それでは、これにて失礼します。

＞まろ様。[４６８０]  
你对车很了解啊!冴子在小说中驾驶的是“保时捷911turbo”。也许是因为这个原因，我有“冴子=跑车”的印象。阿香很适合昴的preo。冴子。我想我也很适合。


### [4692] 露崎なごみ	2001.11.24 SAT 23:46:28　
	
こんばんは。もっこりメッセージ更新されてて嬉しいです♪笑っちゃいました。確かあの髪型、私の記憶違いでなければ‘？’みたいな形してたと思うんで、ホント「どうして？」って感じが良く表現されてるなぁ…と改めて感心してしまいました。

＞[4682]ちゃこさま
はじめまして、こちらこそ宜しくお願いします。データ集、50音順、年度別で毎年更新してるに違いないですね！！
　あの資料棚、難しい本が並んでてリョウの博学さに感心した事を思い出しました。（でも難しそうな題名のラベルが張ってあって実は女体ハダカ本の可能性もあったりして…？）

### [4691] 無言のパンダ	2001.11.24 SAT 23:30:30　
	
こんばんわ★
「もっこりメッセージ」見ました♪
内容はともかく、お忙しいにもかかわらず、ネタにも困ってらっしゃってても、ちゃんと更新してくださる先生のサービス精神に感激しました！内容にも笑えましたけど・・・(^_^;)

「出会い」の法則？！
槙村兄と冴子さんの出会いが話題になっていたので、それにちなんでひとつ、いつも思ってたことなんですが。
将来恋愛に発展するふたりの出会いって、決して第一印象から
「いい印象」ではないことが多いですよね。
どちらかと言えば、「最悪な出会い」「最悪な印象」から、いつのまにか恋に発展していくパターン、漫画やドラマだけでなく実際にもありますよね。「この人、一番嫌いなタイプ！」という第一印象だったのに、「一日顔を見られないだけで、寂しくてしかたない」人になってしまう・・・。
これって何かの法則なんでしょうか？
もしかしたら、槙村兄と冴子さんの出会いのきっかけも「最悪」な何かがあったのかも・・・なんて思ってしまい、ちょっと書いてしまいました（笑）槙村兄が冴子さんにひと目惚れってことはあっても、冴子さんが槙村兄にひと目惚れっていうのは考えにくいですし・・・（すみません！）
ただこの法則（？）は恋愛だけに当てはまるものであって「結婚」となると、それはまた別の話・・・なんですよね。（溜息）

わけのわからないこと書いてすみません。これにて失礼いたします。

### [4690] おっさん(岐阜県：くっくっく・・・）	2001.11.24 SAT 23:25:32　
	
毎度、今週は店が暇で暇で・・・＾～＾；皆様おげんこ～ということで今日もいつものご挨拶せ～のおっは～！！
もっこりメッセージ更新されてますね。みて一言「くっくっく・・」でしたね。先生もネタ切れしてるみたいで・・・＾～＾。でもプロモはあんまり見ませんが、この曲はどんなプロモかなって思ってしまいますね。仕事先で有線聴くので、特に店長が休みのときは邦楽オンリィになりますけど、普段は洋楽です。
まろ様・black glasses様・無言のパンダちゃ～ん、有難うっす。正社員になってもがんばってまっせ～！！まろ様、まだ風邪は治ってなかったりするんだな～これが＾～＾；。店の空調管理がしっかりしてなくて、寒かったり暑かったりの繰り返しだったりする・・・。black glasses様、お久っす。おげんこでしたか？Gooちゃん来てたのに、また来なくなったよ～。いつでも毎日きてちょ！無言のパンダちゃ～ん、おっつ～。おもろい間違いでしたね。まだそこまで進展してないっすよ～。奴（彼）もそうなんですがもう一人今の仕事先で問題児がいるのですが。ハッキリ言えば例の逆切れ店長です。ここ最近、人の帰りのタイミングと合わせて帰ろうとするんです。とくに私一人で帰るときは特に・・・ちょっと怖かったりもする。
明日は、高野山（和歌山県）へ行ってきます。日帰りですが、父親の御骨が３分の２行ってるのでちょっとお参りに行ってきます。ということで、今日はこの辺で・・・・。

### [4689] 玉井真矢	2001.11.24 SAT 22:10:20　
	
今のプロモはどれもこったものばかりで面白いですね！
僕の最近のプロモで気に入っているのは桑田佳祐さんの「白い恋人達」のプロモです。桑田さんが雪の中ピアノを弾きながら歌っていていい感じです。それにウッチャンやユースケさんも出ていてこれも面白い所ですね！

### [4688] 葵（もっこリメッセージ）	2001.11.24 SAT 21:47:02　
	
　もっこりメッセージ更新されてますよん♪
しかも１時間ほど前に！うっうっ…更新される瞬間が見たかった☆

　せんせ、そのカキコは…吉田美和ファンが怒りまっせ。（かくいう私も、同じ感想を持った一人です☆）

### [4687] まろ（俊夫：だ、だ～れだァ？！貴様ァ！）	2001.11.24 SAT 19:22:41　
	
こんばんはっす。

レスです。

［４６８４］ｂｌａｃｋ　ｇｌａｓｓｅｓさま
　はじめまして。おっしゃる通り、冒頭は某「あ○ない刑事」をモチーフにしてます（苦笑）
　ｂｌａｃｋ　ｇｋａｓｓｅｓさまは、お幾つなんですかね？
このネタ解るヒト、当時だと、最低でも私と同じ年齢じゃないとキツイと思ったものですから（それとも、再放送組ですかね？）。ただ、話の展開が、私の当初の思惑とはかなり違う気が（冷や汗）

［４６８６］無言のパンダさま
レス、有り難うございます。
実は２０００年モデルのトコからアクセスしたら、アソコにジャンプしてしまって（汗）たぶん２０００年モデル、削除されてるんでしょう（←外見的な変更がない為。ワゴンモデルの追加と色の追加（銀、白）しかない）。
　あそこの左のメニューでＧａｌｌａｒｙをクリックして、しばらく待つと、Ｂ４の写真で３箇所、ポインタが当たると写真が切り替わる所があるので（ただし、シルバー）、フォルムはつかめると思いますヨ。
　あと、さんこ・ぱいふぇ（←山○　百恵さんの中国読み。ちなみに安○　奈美恵は「あふ・なみふぇ」だそーです・笑）さんのＣＭ、遺憾ながらしらないんすよネ（涙）スミマッセン！

・・・『しょーもなストーリー』の結末やいかに？！（←それほどのモンか？）

### [4686] 無言のパンダ	2001.11.24 SAT 18:48:07　
	
こんばんわ☆・・・かな？
最近は陽が落ちるのが早くって、どこからが夜なのかわからないです～。

＞ｌｕｎａ様（４６８１）
ＣＨへの情熱が感じられるカキコ、おもしろかったです。
おもしろがって、ごめんなさい(^_^;)
でも本当に、深夜の番組のＧコード、いいかげんなこと多いですよね。

＞まろ様（４６８３）
教えてくださったアドレスに、さっそくアクセスしてみました。
くわしくは見てないんですが、トップページにあったのは２００１年モデルですよね？「プレミアムレッド」あのイメージでしょうか？冴子さん。結構スポーティーな感じで、行動的な人に似合いそう。たしかに女の人が乗ってると、かっこいいですよね～！
他のページには、まろ様がおっしゃってた２０００年モデルも紹介されてるんでしょうか？私に区別がつくかどうか、わかりませんけど。ところで、その昔、山口百恵さんが「百恵の赤い靴」というキャッチコピーでＣＭしてた車の名前は、ご存知ですか？なぜか昔実家にポスターがあって、そのキャッチコピーだけが印象に残って、車の名前は覚えてないんですよ～。
どうでもよいのですが、思い出そうとして思い出せないこと最近多くて（！）気持ち悪いんですよね（笑）
あー、でもこの車のＣＭの頃は、まろ様小学生ですよね（笑）
すんません！

失礼しました。

### [4685] kinshi	2001.11.24 SAT 18:12:43　
	
こんばんは、
ネタギレで、何を書いていいのかわからないけど・・とりあえず、ラクガキ・・・ということで、

最近　金曜夜中の３時、正確には、土曜AM３：１０からやっている、「新バビル２世」を　録画して、見ています。
かなり懐かしく　何となく見ているのですが、旧バビル２世　もほとんど内容は、　忘れているのですが、　確か・・「ヨミ」との対決が、主だったようなきがします。（ちがかったらごめんなさい）　　・・・・　ところが、まだ始まって、１ヶ月ぐらいだと思うのですが、　早々と、ヨミが、死んでしまいました。
ならば、　バビル２世・・でなくて、３世・・とか　　ルパン　とまちがえないように、いっそうの事　７世　とか　してもいいのでは？・・・っておもってしまいました。早まった感想かもしれませんが、（本当は、ヨミは、まだ　死んでいなかったとか・・）　そんなこと　思っていました。
ツタナイ内容で、すみません。

ところで、まだ、BBS２ヶ月目の私には、よくわからないのですが、あの、嵐のようにやってきては、去って行ったような？あの方は？思わず、天から　笑いが　降りてきたようでした。
Goo　Goo　Baby様　もう一度、現れていただけることを
ねがっております。あの笑いは、癖になりそうです。

では、また。　

### [4684] black glasses	2001.11.24 SAT 17:36:59　
	
どうもお久しぶりです。
最近は書き込むことがなくてＲＯＭに徹していました。

[誰と飲むか？について]
遅い話題で申し訳ないのですが僕としましてはリョウと海ちゃんでしたらまちがいなく海ちゃんでしょう！確かに海ちゃんとだと沈黙の状態が続きほとんどしゃべることなく飲んでいるだけでしょうけど（僕もかなり無口なものですから）僕の中では男二人が黙って飲んでいる姿ってもうシブくってかっこよくて憧れのような感じがあるんですよね。リョウだと絶対そんな状態にはなりませんからねぇ。

＞Goo Goo Baby様
あなた様の名前を見たときびっくりして思わず「目がテン」状態でしたよ。ほんとうにお久しぶりです。

＞まろ様
ＨＮの隣の「ショート･ショート」の今回の内容で初めの「港署」というのを見た時点で僕は横浜を舞台にした某刑事ドラマ関係ではと思ってました。さすがに最初からツッコむのも悪いでしょうからもう終盤に近いと思われるので今回言わせてもらいました。

＞おっさん様
遅くなりましたが正社員昇格おめでとうございます！正社員ということでこれからもより一層お仕事がんばってください。

＞ちゃこ様[4682]
「海坊主、復活のバズーカ攻撃！」これはもう最高です！！意味はよくわからないのですが何か凄さが伝わってきます。まるで「キ○肉マン」の「屁のつっぱりはいらんですよ」のように意味はわからないけどすごいことなのがわかるのと似たような感じの言葉で久々に大ウケでした。


たぶんしばらくはまたＲＯＭに徹すると思うので次はいつ来るかわかりませんが、それではみなさんその時までGOOD LUCK!

### [4683] まろ（・・・・・・÷←極楽トンボのつもり）	2001.11.24 SAT 17:02:13　
	
ちょい～っす！

無言のパンダさま他、もしレガシィＢ４（セダンの名前。ビーフォーと読む）ブリッツェンが気になった方へ

ここでの直接のアドレス公開は、ちとナニですので、一部伏せた形にてご案内します。

ｗｗｗ．＠＠＠（会社名を６文字で）－ｓｔｉ．ｃｏ．ｊｐ／
ｂｌｉｔｚｅｎ／ｔｏｐ／ｔｏｐ＿（←ＳＨＩＦＴ＋”ろ”キーで入力）ｆ．ｈｔｍ

――冴子さん、ボキの頭の中ではこんなクルマに乗ってます（笑）
北条先生、書いてくれないかなァ・・・

### [4682] ちゃこ	2001.11.24 SAT 16:20:43　
	
こんにちは。
昨日は実家から妹が遊びに来ていました。（今日の昼間、帰ったけど）
世間様は３連休。天気が良いので、皆様も遊びに行かれているのかな？
私は平常生活です。さて、レス。

＞露崎なごみ様「4613」
はじめまして。ちゃこといいます。以後、宜しくお願いします。
「リョウ＆冴子さん＆槇村兄の出会い」ナイスですねぇ。
リョウの「資料棚（？）」には「各業界の美人データ集」が
50音順で並んでいそうです。しかも年度別。
（新入社員＆退社社員が出るから、毎年更新してるに違いない）

この３人の出会いについては、CH番外編（外伝？）として、ぜひ読みたいです。
実は続編より、そういう話の方が興味があるのでした。
先生描いてくれないかな？その辺は「自由に想像して下さい」って感じなのかな？

＞三毛猫様「4528」
今頃レスになってしまって、ごめんなさい(^^::)
“旧・玉”が分からず、確認してから・・・なんて思ったもんで。
でも、やっぱり分からなかったです（苦笑）
そういえば、この間ふと思い出したのですが、ＪＲ駅に生息していた山羊、
「シンコちゃん＆イワオ君」覚えてますか？
２両編成の車両同様、衝撃でした。不思議ワールド、第６学区・・・（笑）

＞“造語大臣（あ、変えてしまった）”Parrot様
遅ればせながら、レス下さって有り難うございます(^^;;)　
「海坊主、復活のバズーカ攻撃！」です。（←攻撃してどうするんだっ！！）
あお様同様「多忙な学生生活」過ごされているようですね。
イラスト、HPが直ったらURL載せるので、良かったらそちらへ送って下さい。
WEB上で、公開させていただきまする（笑）
・・・しっかし、HPが直るのはいつになるのやら。
年内「再開店！」目指しておりますが、ちっともいじれないでいるのでした。

では、また。もう一回、来られたら来ます。

### [4681] ｌｕｎａ	2001.11.24 SAT 11:07:39　
	
おっはー
眠いすっ
夜中、ＣＨを見るために、仮眠をして、ＡＭ３時に起きたんです
と言うのは、またもやＧコードが、２時間分になってたんで
前に録画したテープに録画するため
前に余計な別の番組が一時間分入ってるんで
けど、そのままＧコードで録画したら前のが消えてしまうから
だいたいの時間を予測して、時間で予約して、仮眠をとって
念の為に、３時に起きました
セットしてた時間は、３時１０分から４１分
４１分っていうのは、いつもエンディングがもうちょっとって
ところで、切れるから
でっぼけーっと起きて見てたら、なんと、３時７分に始まって
録画しなあかんって焦って、予約取り消しして、入力入れ替え
をしている間にオープニングが終わってしまって、ぎりぎり
ストーリから録画できたよぉ
でっ終わりにちらっと、次回は、っていうのが写たんで、
巻き戻して、見ようとしても、なかなか見れない
ぼーとしてたからやけど、やっと見れて、なんと、今日の深夜も
放映されるって、ＡＭ３時３０分から
はぁーーー疲れた
ＣＨのＤＶＤ化を願いながら、寝なおしましたよぉ
今夜は、どうしようかなあ？
なにかで、延長したりしたら、予約しててもダメになるしぃ
やっぱ、起きたほうがいいかなあ
夜が弱いんで、苦やわっ
ＣＨを録画して見るためにドタバタしているｌｕｎａでしたっ
しょーもないカキコを長々としてすんませーん

