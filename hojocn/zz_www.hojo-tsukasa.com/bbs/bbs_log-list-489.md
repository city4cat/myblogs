https://web.archive.org/web/20031027171245/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=489

### [9780] OREO	2003.03.04 TUE 03:13:56　
	
こんにちわ！！
すごい！すごい！！！・・・ＢＢＳが賑やかになっている！
カキコ大量に増えて、コレは過去ログに行かないと追いつきませんね（笑）っということで、レスはマタ今度・・・☆
お初の皆様、ＯＲＥＯと申します、どうぞ宜しくです☆

日本、ひな祭りですよね・・ってもう終わっちゃいましたよね？コッチは今日が３日なのですが・・う～ん。今部屋に雛飾りがチョコットあるのですが、日本時間で片付けたほうがいいのかなぁ・・・？？お嫁にいけなくなちゃったら嫌ですよぉ～～（笑）

＞ＣＨ最終回秘話☆
えぇぇぇっ！！知らなかった！そんな秘話があっただなんて！昨日、それを読んでから、ソッコーで、最終回のマリーの話、見ましたよ～・・・画面とにらめっこして、巻き戻し＆再生何度もして、見つけようとしたんですけど・・これかなぁ、これかなぁって思うの２，３個あって、正確にわからないですぅ～(+_+;)
ちなみに、判断材料は、僚の「眉」と登場してくる人の「目」で判断してみました☆
本当に、一体、どこなんでしょう・・・気になります～！！

それでは、学校へいってきま～っす！明日はマタマタ試験です～・・・頑張らなくては！！

### [9779] 里奈	2003.03.04 TUE 02:22:56　
	
雛人形…ちゃんと飾ってやらないと呪われる！？
里奈は人形の『呪われる系』に弱いです…（ビクビク）寝かせてやると目をつぶるフランス人形みたいなのあるじゃないですか。アレとかとくにダメ！昔 幼稚園ぐらいの時に その人形が怖くて勝手に焼却炉に捨てたことあったんだけど、幼稚園から家に帰って来たら人形が今まで通り小さな椅子に座ってた！『あたし絶対この人形に殺される！』って青くなって泣いた…。大人になって母から『管理人さんがカワイイのにもったいないって拾って来たのを見つけて返してもらった』って真相を聞いたんだけど、未だに絶好調なくらいトラウマです。今でもリアルな人形はダメ！！雛人形！？問題外！！うちには ありません！

☆９７７７　yoshiko様
おほほほほっ（高笑）やっぱり！たま様のお仲間っ！少しイジメると対抗意識が芽生えてくるようですわね♪そんなアナタが好きよ…うふっ。
えっ！なになに！？半年前にカキコ参加に失敗！？（早速食いつく）
そういえば、半年くらい前に『カキコに参加したいけど文字化けしちゃってできないんです』ってメ～ルくれた子がいたなぁ～。まさかアナタじゃ…？（違うか…笑）

☆９７７６　将人様
あっ。将人様の先を越してＣＨ２最終回の話ししたのはイイけど、アレって３話連続だったってこと忘れてた☆やられたり！
え！？『ＳＡＫＵＲＡ』オークションで出てるの！？でも５千円以上値段上がってんじゃん…。いくら欲しくても、はやまって後悔したくないしなぁ～。悩む…。大阪にはもしかしたらまだ手に入る店が存在してるかも？見るだけでイイんでお願い致します！もし置いてるとこ見つけたらメ～ルくださいネ♪
チャカ（笑）の射撃練習は確か１６歳からだったような…里奈が勝手にそう思ってるだけかも…（笑）でも、里奈が１６歳ぐらいの頃から『うち もう銃撃ってもええ歳なんやから、次海外行ったら絶対射撃すんねん！』って言ってたような気がする。もっと女の子らしい夢は無かったんかい！？（苦笑）

### [9778] Ｒｉｋａｙｏ	2003.03.04 TUE 02:09:05　
	
私も知りたいーーー
どこなのよーどこなのよー

録画していた昔のビデオを引っ張り出して見たんだけど、わかんなーい。

北条先生のハッキリとは言わない、あのビミョーな書き方が、余計に気になって、探してやれという気分になる。


う～ん。みんなが指摘している所なのかなぁ。

古いアニメ雑誌もひっくり返してみたら、
『今月の名シーン』というものがあった。(今週ではなかった)
それは、さらばハードボイルド・シティー(後編)の例のシーン。
「おまえを泣かす男が、今はおまえを…」

でも、これ最終回じゃないもんね。
内容は最終回っぽいけど。

嗚呼、誰か正確な、じょ、情報を…ガクッ

### [9777] yosiko	2003.03.04 TUE 01:43:16　
	
チョッと暇＆眠れぬので、またたびカキコを・・これを読んでくれてる方、yosikoはさぞかし暇なのだろう、とお思いになったことでしょう・・(-_-;) That's その通り。明日は用事ナシ。サムシイ限りでごわすます。ここで１つ、レスを・・・

＞里奈サマ
たしかに！！ワタクシのカキコ、どんなに早くスクロールしても白さが目立ちます！！ぬぅお～(-_-メ)っっ。これでもかっっというくらい黒くなれるようフンヌッしてみましょう・・いつか。
負けませんわヨ←謎キャラ。　目指せ余白なっすぅいんぐ！！

＞将人(元・魔神)サマ
お世話になりますm(__)m。いえいえ。正確に言うと、半年前にカキコに加わろうとして、失敗した者ですねぇ・・はっっ。里奈サマに手の内を明かしてしまうトコでした(^^;事実、今回が初参加でごわすのです。よろしく願います！！

### [9776] 将人（元・魔神）	2003.03.04 TUE 01:34:06　
	
こんにちは、将人（元・魔神）です。

＞[9774] 里奈さま　2003.03.04 TUE 01:12:27　
レスの書き込みしている前に、里奈様に、ＣＨ２の最終回の説明を、先に越された～！

香さんのあの絵は「ヌード」ではなくて「セクシー絵」なんですね。
やば、あれの説明の部分に僕が「ヌード」か何か書いてたかも？(汗)(^^；

ＰＳ２の限定版、この前までは、大阪の日本橋電気街のパソコンの
店にもあったけど、サクラいろって、もう売り切れてるのかな？
それとも、あれはデモで並んでいただけで、無かったのかも？
また、置いてあるか見ときますね。
さすがに貧乏なんで、２万円～３万円の物は、買えるだけのお金は
ないで、あるかどうか見ておくだけね(汗)(^^；

・・・書き込んでいる途中で、ふと気になって、オークションを
「P S 2 限定」確認して見たら、34,800 円とかで売ってるな～
あれって限定品でも定価でも「２万いくら」じゃなかったけ？

### [9775] 将人（元・魔神）	2003.03.04 TUE 01:15:32　
	
こんにちは、将人（元・魔神）です。

前に書き込んでいたＣＨのＣＤブックのオークションをやっていて
無事に\2,000円で落札してから、この掲示板に戻ってきましたが・・・
ひぇ～、なんだこの書き込みの量は～？！(汗)(^^；

→僕も1つに収まりきらない書き込みを一杯してるせいもあるけど

やっぱり、北条司先生の「もっこりメッセージ」の更新とか
ＡＨのコミックス６巻が、３月８日(土)に発売だとかが
あるからか書き込みの量が多くなっていますね。すごいな～。

＞[9766] 里奈さま　2003.03.03 MON 21:02:37　
１日４０時間くらいあったらいいでしょうね。仕事とか普段の生活は
２４時間で、自分だけ特別に４０時間で、残りの時間は、自由自在に
出来るとかあればいいだろな～
しかし、それなら毎週のバンチで、ＡＨの次の話が出るのを、
待つ時間も倍になるのかな？それは困るかも？(汗)(^^；

銃の射撃訓練は１６歳以上で出来るんですか？初めて知りました。

＞[9767][9768] 知沙さま　2003.03.03 MON 21:19:34
僕のサイトを見て頂いたんですね。ありがとうございます。m(..)m
野上唯香さまや里奈さまのミニコントとか、里奈さまのイラストを
見て下さったのですね。
今日の夕方頃に、いちの様やマイコ様のイラストも、僕のＨＰに
追加しましたよ～。
合同コントの後、新しいコントの案が浮かばずスランプになっていて
更新できていないんですけどね。
まだ、この掲示板で書き込みしていたＣＨクイズとかも溜まって
いるので、また僕のＨＰに載せようと思ってます。

＞[9769] TAKESHI様　2003.03.03 MON 21:26:1
修学旅行は、九州に行かれるんですか？
九州と言うと、やっぱり受験生なんで太宰府天満宮は回るんでしょうかね？
楽しんできて下さいね。

＞[9770] 葵さま　2003.03.03 MON 22:08:04　
もっこりメッセージのＣＨ２の原画、北条司先生が公開してくれない
でしょうかね？
あのアニメのフィルムとかに撮影した後は、その原画って誰が
持たれているんでしょうね？

僕も、ここのＨＰでシティーハンターＴシャツを売っていた時にも
来てたんですよ。その事はバンチの創刊直後かリニューアルされる
とかで、掲示板は閉鎖されていたのかな？

とりあえず、その時は、僕もそのＴシャツ値段が高いと思って、
買うのを諦めたてですよ。

オークションで見た時は、それが我慢出来なかったというか何というか
衝動買いしてしまいました。

オークションに出された人に払うのは良いとして、毎回オークション
するたんびに、銀行や郵便局、宅配便の業者さんなどを、振り込み
手数料や送料で、儲けさせているではないかと疑問に思う事も
あるんですけどね。(汗)(^^；

＞[9771] たま様　2003.03.03 MON 22:46:08　
アニメ版のシティーハンター２のセイラの回は、最終回になる予定
だったという話らしいですよ。製作中に番組の延長が決まったとか？
ＣＨ２の最終回は、ブラッディマリーの話の前中後編の３話みたいです

題名は「グットラックスイーパー二人のシティーストリート」
第112話(CH2の61話)～第114話(CH2の63話)見たいですよ。
でも僕も資料を見て、この事を書いてるだけで、ビデオなどに
録画してないんで見れないんですけどね。

＞[9772] yosiko様　2003.03.03 MON 23:45:17　
たま様とは、お知り合いなんですか？
たま様の書き込みに「半年の沈黙」って書かれていましたけど、
以前にも、この掲示板に来られていた方なんでしょうか？
僕は、ちょうど半年ぐらい前から、来てたんで、すれ違いで
初めてなんでしょうか？
これから、よろしくお願い致しますね。

＞[9773] いちの様　2003.03.04 TUE 00:12:00　
僕の家も、妹の為にひな人形があって、妹が幼稚園の頃には飾っていた
記憶があるんですが、20数年ぐらい飾ったことないですよ。
たぶん捨てやんと、どっかにしまい込んでいるのだと思うのですが

同様に、僕の為のだったと思う五月人形も、小学生低学年を
過ぎてから一度も見た事有りません。
人形たちに呪われるかもな？

いちの様からメールで送って頂いた画像、僕のＨＰに飾らして
頂きましたよ。
もう一度、作るちゃったから、あのＨＰの場所、ＨＰスペースが
パンクしない限り、いちの様や里奈様、マイコ様などのイラストの
個展の展示会場代わりにして頂いていいですので、もし新作の
イラストが出来たら、メールでドンドン送ってみて下さいね。
出来る限り、載せるて見るようにしますよ。

### [9774] 里奈	2003.03.04 TUE 01:12:27　
	
はぁ～。里奈が住んでる滋賀県のＪＲ湖西線が３時間も止まってたよ。理由？風が吹いたから。弱っ!!でもマジ（汗）雪が降る冬はしょっちゅう止まってます…。

☆９７７３　いちの様
あらら？年明けに描いた香のイラスト、いちの様には送ってなかったのかしら？それともＰＣの調子が悪くて見れなかったのかな？
ってゆぅーかぁ～、『香のヌード』って言われると なんかイヤラシイわね（汗）せめて『香のセクシー画』と言って頂戴！里奈はエロイ イラストばっかを描く子だと勘違いされちゃうでしょ！
ビシっッ！バシッ！！（結局ムチ打ち☆）

☆９７７２　yoshiko様
よぉーすぅいーこぉぉぉーっ！（ドサクサに紛れて呼び捨て…）
ササッ！（警戒して構えてみる…だって たま様の仲間でしょ？）
あらぁ～ん♪いつも里奈のこと見てたのねぇ～☆（勘違い）
たま様とは『ちょっとワケありの関係』！！？（￣□￣;）
な…なに！？そんなに深い関係なのっ！？きいぃぃー！悔しいしぃー！
たま様は里奈だけのダァ～リンにしておきたかったのに！たま様が浮気してただなんて…（そして今、たま争奪戦が始まる！！）
※おまけ※
空白多過ぎるんでないかい？ププッ♪（新人いびり）

☆９７７１　たま様
っぶっぶぅぅーーっ！あぁー残念！（笑）
ＣＨ２最終回はセイラじゃなくてマリーのストーリーですぜ あねさん！
第１１３＆１１４話目『グッド ラック 二人のシティーストリート』
さぁーもう一度見るのです！録画してなきゃレンタル屋でゲッツ！
コラ！里奈の弱点を公表しないで（汗）自分だって素直に褒められるとオロオロしちゃうクセにぃ～！しかも いつ里奈が『チャカ』って言ったよ！？（でもチャカって響き好きかも…）
あぁ…春の陽気に誘われてＢＢＳに迷い込んで来た子羊さん達が たま様の餌食になりませんように…

☆９７６９　ＴＡＫＥＳＨＩ様
ダメなのぉぉーっ！まだ手に入ってないのぉぉぉーっ！！
ねぇー！なんとか手に入る方法ないわけ！？金沢ではいかが？やっぱ さすがにもう無いかな…。どうしてもＦＦⅩ-ⅡはＳＡＫＵＲＡでプレイしたいんだけどなぁ～。だって、里奈ママがＰＳ使ってて里奈使わせてもらえないんだもん…（泣）

☆９７６８　知沙様
うぅぅっ…。里奈 褒められてる！なんか調子狂うわ（汗）
そうなのさ！里奈は美人でスタイル良くて頭も良くて性格まで良くて しかもセクシーイラスト描かせたらプロ並み！（開き直り）どうだっ！？ここまで自分のこと褒められるヤツいないぞ！（あぁ 隠れたい！もう出てこれない！）

### [9773] いちの	2003.03.04 TUE 00:12:00　
	
どうも、こんばんは。
朝起きて、新聞の４コマを見て「あ、今日はひな祭りか」と悟った１９歳の“いちの”です。
うちの妹は雛人形が恐いと言ってカレコレ７年くらい飾っていません。呪われるとホザイテイマス（汗）。
確かに、親近感の湧く表情ではないですよね。
まー、ぶっちゃけ、男のは関係のない行事ですよ。
気づいたらもう３日じゃないしね（汗）。

＞[9750] 里奈様
イラストを描こう（マジで）・・・。
最近マシな生活してないことに気づいてしまい、独り暮らししていたときの方が作製意欲はあったような・・・（汗）。
でも、将人（元・魔神）様のＨＰを拝見させていただきまして、初めて里奈様の最新作、香のヌードを見ました！！
あんた、ホントに上手いね。素人にしとくのはもったいないよ。冗談抜きで。
あ！照れ隠しにムチ打つのはやめて！！イッ痛ッ！！

＞[9752] 千葉のOBA3様
さーて、こんぺいとうのお返しバズーカを作らないとね☆
本当は今日、ひな祭りイラストでも描こうかと思っていたけど、ねぇ～、ははは～（乾笑）。
でも、ホワイトデーには必ず仕上げるので舞っててください（もとい、待っててください）ね～♪

＞[9754] TAKESHI様
修学旅行ですか～。いいですね～。
私の通っていた学校は男子校だったので、修学旅行と言ってもあんまり盛り上がりに欠けましたね（汗）。盛り上がったことは盛り上がったのですが、男特有の盛り上がり方と申しますか・・・。
楽しんできてくださいね～。
実は、今、大学の友達と一緒に漫画を描こうかと話している最中なんですけれども、オリジナリティーに欠けてしまい、なんとも上手くいきませんね（汗）。でも、自分の描いた漫画がＴＶアニメとかになったりしたら感激ですよね！！
頑張ってみようかな！

＞[9764] 将人（元・魔神）様
ＨＰ拝見させていただきました！！
なんて言うか、もう、感動です！！自分の描いたイラストがＷＥＢデビューするなんて（涙）。でも、改めてみると、やっぱりヘッタクソですね（汗）。第３者の視点から見れる分、アラが目立ちます・・・。
もっと、勉強が必要のようです。
最近はバンチ以外に雑誌を読んでいないので、よくわかりませんが、編集者も変わってきているんですかね？


いや～、ホントにカキコの量が増えてますね～。
始めましての方、お久しぶりの方、どうも、よろしくお願いします。

### [9772] yosiko	2003.03.03 MON 23:45:17　
	
何だかんだで三度目の登場。
こんばんみ←たまサマ。（ビビ○大木サマ？）いただき。じゅる。
なんてキャラ・コスギ、テンションたっけぇ皆様なんでしょか。
ワタクシも気合をいれて・・・ど～ん！！！！・・・
・・さて・・・

＞里奈サマ
　はじめまして！ワタクシはいつも見てましたが←何だか怪しい。たまサマとはですね、ちょっとワケありのカンケイなんどすえ。ぐふふふ。ステキなお母様ですね、ちなみにアノ電話の翌日の学年集会の場で全員の前で発表されたのです。ヤイヤイ。（お手柔らかに）よろしくお願いしま～す(^0^)v

＞知沙サマ
　はじめまして。こちらこそお願いします(^_-)
「ＣＨ」＆「ＡＨ」、読むたびに号泣してます(^^;
ワタクシもまだ知識はかなり浅いので、色々教えてくださいね！

＞葵サマ
　はじめまして！既にアッパーを受けたと同様の衝撃を皆様からうけとります。カキコに参加するのを楽しみにしてましたので、これからどんどんカキコしてゆきます(^^)v お願いしま～す

＞たまサマ
　うふふふ。ついに来ました(^ .^)y~~~　よろしくお願いします。モチロン呼び捨てOKでごわす。しか～し！半年前よりも、たまサマのキャラ濃い度upしているのは、やはりここの影響でしょうか？？　んにょ。←文字化けではありません。　皆さんがアタタカク迎えてくれて、とっても嬉しいです(^0^)vv　楽しませていただきたいと思います。フロム。よ・すぅい・こ☆グフ

＞りえサマ
　はじめまして！よろしくお長いします(^^)
ひな祭りだったんどすねぇ。女の子の日なのに、スッカリ忘れてました。バレンタインも・・・さみし～っっ。　








### [9771] たま	2003.03.03 MON 22:46:08　
	
ＢＢＳがにわかに活気付いてるぅ～～～！
やっぱり北条センセのもっこり更新の威力は絶大なのかっ？
それ以外にも３月はビックイベント目白押しだから、ますますＢＢＳが賑わいを見せるかも・・。
春の陽気に誘われて、新しい子羊たちがＢＢＳに迷い込んで来る季節なのね。ﾒｪ～ﾒｪ～

「CITY HUNTER 2」の最終回ってセイラの話？ですよね？違ったっけ？
もっこりメッセが更新された時に放送されてたのってセイラの話ですか？
誰かぁぁーーー、、、リアルタイムで放送見ていた方ぁー。
心優しき我が友朋ぉ～～。その日にケーブルＴＶで放送されてたＣＨの題名とか覚えてたら教えてチョンマゲ♪

先生Ｃ・Ｈの放送見て、昔を思い出しただろうな。
偶然つけたＴＶの放送で当時を思い出し、妙に気恥ずかくなって、照れ笑いをする・・。そんな先生を想像し、「先生かわいい」っと思ってしまいました。
そんなことを考えるたまさんもまた「いとかわい♪」（←苦情却下）

＞知沙さま　【9751】
はじめまして。こんばんわ。
早々里奈さまへの褒め殺しの刑・・見事です。見事に里奈様の弱点を見抜いてます。これからも手のひらできゃつを転がしまくってください。（笑）

＞yosiko様　【9753】
半年の沈黙を破って遂に現れたなぁーーっ。ヨォ～スィ～コォ～(既に呼び捨て・・・笑)
でも無事に来れて良かった×２。
中学生の時「拳銃」（おもちゃ）が欲しいって？！でも「ジュウ」って言ってるうちは正常よ。里奈様なんて「チャカ」って言ってるんだから。

＞りえ様　【9760】
はじめまして。こんばんみ。
そうでした。今日は桃の節句。ひな壇より雛あられが好き。
これからもヨロシクです。

＞TAKESHI様　【9769】
おぉ～！修学旅行が九州ですかっ？それはすんばらしい！
やさぐれ支店のある博多へ来た際にはぜひやさぐれラーメン、いや、博多ラーメンを・・・。
ちなみに県外の方に人気があるのは「一蘭」です。

### [9770] 葵	2003.03.03 MON 22:08:04　
	
　もっこりメッセージが更新されたとたん、昨日の私のカキコはすでに過去ログの中へ…。（みなさん、早っ☆）もこりメッセージに書かれてたＣＨ２の原画のシーン、やはり見たいですよね～。北条せんせ、ココで公開いたしませんか？！（^^;

＞将人さま［９７４３］
　Ｔシャツですか？欲しかったけどお値段的に少々だったので、泣く泣くあきらめた記憶が蘇えりましたよ☆（>_<;

＞無言のパンダさま［９７４７］
　魔女の呪文？！そんなことないわよ、美声を響かせてますってば！今日も張り切って歌うぞぉーっ!!!（←ホントはクチパクだってば☆）

＞Ｒｏｕａｒｋさま［９７４８］
　「ＲＡＳＨ！！」読まれましたか？北条せんせの作品はどれも甲乙つけられないけど、コレは私も大好きな作品です。全２巻がもったいないくらいですよね～♪

＞千葉のマダムさま［９７５２］
　ゴメ～ン、一日違いだったね。でも今もラブラブなら、結婚記念日の意味もナイってか？！息子さん名義でＴシャツが当たったとか。おめでとうございます。今は平気みたいだけど、マダムもそろそろブラックリスト入りか？！（^^;

＞Ｂｌｕｅ　Ｃａｔさま［９７５７］
　え？他にも「愛称パンダちゃんだぁーっ!!!」なんて名ゼリフもアルじゃないっ！（笑）

＞ＴＡＫＥＳＨＩさま［９７５４＆６９］
　修学旅行だそうですね。しかも九州？まさしく「やさぐれ本舗・たまさま」の本拠地じゃないですか！生きて帰ってこいよ～っ!!!（笑）

＞初参加の方々
　はじめまして、葵と申します。よろしく♪ココのみんなはかなり濃ゆ～いキャラですが、噛み付いたり、取って食ったりはしませんので（一部、例外アリかも？！）安心してカキコして下さいね♪（^^;

### [9769] TAKESHI	2003.03.03 MON 21:26:13　
	
こんばんは。

＞[9765]将人様
九州方面ですよ。精一杯楽しんで参ります！！

＞[9766]里奈様
１６歳以上ならできるんですか!?!すごいな～！！是非一度経験したいものです。そういえばＰＳ２のピンクは手に入りました？

＞[9767]知沙様
やはり、同じく受験生でありましたか。おたがいがんばりましょう！！

### [9768] 知沙	2003.03.03 MON 21:19:34　
	
里奈さま
初めまして、お返事ありがとうございます！！将人さまのサイトのイラスト見させてもらいました。すごくお上手なんですね！しかも、美人だなんてすごく羨ましいです☆これから仲良くしてくださいね！よろしくお願いします！！！

将人さま
初めまして、お返事ありがとうございます。アドバイスありがとうございます。遠慮なくとのことですので、出来るだけ遠慮なく参加できたらと思います。将人さまのサイト行かさせてもらいました！！楽しかったです！！

### [9767] 知沙	2003.03.03 MON 21:06:37　
	
千葉のＯＢＡ３さま
初めまして☆早速のお返事ありがとうございます。これから宜しくお願いしますね！！

ＴＡＫＥＳＨＩさま
初めまして、お返事ありがとうございます。同い年なんですか？！お若いんですねぇ～！！びっくりです。これからも宜しくお願いします☆

野上唯香さま
初めましてお返事ありがとうございます。はい、高２です。４月から高３ですよ！！高校教師ですかぁ、残念ながら見れてないんですよ(泣)将人さまの合同コント朝イチで読みました！
里奈さまのイラストとても綺麗だったのですが、野上唯香さまご説明ですと、とてもお綺麗な方のようで。美人でイラストもお上手だなんて羨ましい限りです。

りえさま
初めまして☆これから宜しくお願いします。仲良くしてくださいね！！そういえば今日はひな祭りでしたね。すっかり忘れていました。宜しくお願いしますね！！

ｙｏｓｉｋｏさま
初めまして！宜しくお願いします。仲良くしてくださいね☆私も『ＣＨ』『ＡＨ』大好きです！！これからも宜しくお願いします。

皆様、お若いんですね！かなり驚きました。
これからもよろしくお願いします！！仲良くして下さい！！！

### [9766] 里奈	2003.03.03 MON 21:02:37　
	
☆９７６３　野上唯香様
いやん★そんなに里奈のことを誉めたたえても何にも出ないわよ！照れ隠しにムチくらうかもよ（笑）

☆９７６０　りえ様
里奈ちんです♪始めまして☆そうだよねぇ～！１日４０時間くらいあったら里奈ももっと北条センセの作品に浸れるわ！自分だけ時の流れが遅かったらってホントに願いたい…。

☆９７５９　ぺぺ様
こらこら！大事な彼女を｢モン｣って言っちゃイケナイわ（笑）
女の扱いを間違えると恐ろしいことになりますわよ。それとも里奈のムチが欲しいのかしら？里奈以外からも手榴弾やコンペイトウをくらうわよ（笑）

☆９７５４　ＴＡＫＥＳＨＩ様
やっぱり？そうだよね！北条センセが手がけたと見られる原画部分、あのへんだよね？（ちょっと安心…笑）
銃は原作３２巻で美樹が言っているように、『銃なんて誰だって練習すれば ある程度当たるようになるんだから 才能なんて次のレベルの話し!!』ですわよ！思ってるより当たる！ＴＡＫＥＳＨＩ様も海外行くことあったら挑戦してね！確か１６歳以上なら扱えるはずだし。

☆９７５３　ｙｏｓｈｉｋｏ様
はじめまして♪ん？たま様とお知り合い…？ってことは、たま様の悪友！？…ってことは！！また里奈をイジメる御主人様と呼べる人が増えたのか！？里奈はたま様を｢師匠｣としたっております。ＣＥの美人三姉妹のように深い絆で結ばれております。今までは末っ子は｢いちの様｣だったけどメンバー交代のようですわね♪
里奈もＣＨで銃の魅力にハマって、子供の頃アニキとエアガンで遊んでましたよ。女の子なのに…でも母は笑顔で的のコピーをくれていた。

☆９７５２　千葉のＯＢＡ３様
えっ！？懸賞当たったんだ！スゴイじゃないですか！でもＡＨ物じゃなかったんですね…残念☆里奈もＡＨ時計欲しくて懸賞出したけど落選したようですわ。里奈嬢を見落とすなんてバンチ編集部の目は節穴か！？（笑）
巨大ゴキちゃん…南国で生活する為には この生物と仲良くならなければ生きていけないのね！（オオゲサ）でも里奈ならきっと華麗なムチさばきで手なずけてみせるわ！（潰れるって！）
そっか☆結婚記念日は４日だったのね！あんまり差は無いって？ダメダメ！大事な記念日でしょ！

☆９７５１　知沙様
どうもどうも里奈です！唯香様が里奈の事紹介してくれてるけど、なんかベタ褒めで恥ずかしいわ☆事実だけどねぇー！（笑）…って、里奈はココではＳ嬢で通ってるから 普通に褒められるとコメントに困る。てへへ（汗）

### [9765] 将人（元・魔神）	2003.03.03 MON 20:57:34　
	
＞[9752] 千葉のOBA3様　2003.03.03 MON 08:11:48　
バンチの懸賞当たったんですか？良かったですね。
しかし本命のＡＨじゃなかったんですね。
僕も前に、最初にバンチの漫画のコミックスが発売された時に
ＡＨのドラマＣＤに応募したんですが、当たったのは本命では
なかった蒼天＠拳の方だったんですよ。→キライでは無いんですが
もし当たるのを分かっていたら、２本ともＡＨで応募してたのに
って思っていた事ありましたよ。

ＣＨクイズ、全部分かりましたか？やさしすぎたかな？
でも前は、難しすぎるって言われていたし～今度はどうしようかな？

＞[9754] TAKESHI様　2003.03.03 MON 10:46:29　
明日から修学旅行なんですか？試験が終わった直後なのに大変ですね。
どこへ行かれるんですか？楽しんできて下さいね。

面白い画像のメール、ちょっと今回のは笑うに笑えないネタでしたね。
メール友達は東京方面の方で、実際には会った事は無いんですが、
メールで色々とやり取りしています。

昔、あるゲームのメールマガジンをやっていた時は、この掲示板で
やっていたＣＨのミニコントのように、そのゲームでのコントみたい
なのを色々と、投稿して貰っていた方なんですよ。

→過去形じゃない今もメルマガ続けていると言いたいですが、
　最近ずっと、さぼっていますんで。

＞[9756] 野上唯香さま　2003.03.03 MON 11:26:09　
海坊主さんが、ヒゲ剃りですか？
いや～海ちゃん頭はキレイですか、ヒゲは生やしていますよね？
宣伝できるのかな？

→昔、阪神にいたバースというヒゲが特徴だった選手のヒゲ剃りの
　ＣＭを思い出しました。海ちゃんでも出来るかな？

同じバンチに書かれている原哲夫先生の北斗＠拳のキャラのヒゲ剃り
は最近出ていますね。

＞[9757] Ｂｌｕｅ　Ｃａｔ様　2003.03.03 MON 11:33:54　
そいうえばチャットで「もうそろそろAH6巻のコミックス出るのに
更新されてませんね」などと話してましたね。
まさか北条司先生、冴羽さんかキャッツアイにでも頼んでチャット
ルームに盗聴器を仕掛けて、聞いていたのか～？(汗)(^^；

＞[9759] ぺぺ様　2003.03.03 MON 12:21:16　
北条司先生の作品、ＣＨの頃よりも、ＡＨは落着いたような感じに
なっていますね。
ＡＨの連載が始まった時は、意外な展開での始まりでしたが・・・。
→それを受け付けられない友人もいましたし。

＞[9763] 野上唯香さま　2003.03.03 MON 18:37:19　
「高校教師」って今やっているドラマなんですね。
昔に、よく似た名前のドラマがあって、それの再放送やっているのかと
ずっと思っていました(汗)(^^；　→気付くのかなり遅いかも

野上唯香様や里奈様の「もしもＣＨキャラが教師だったら・・・」の
ミニコントは、イラストと一緒に、僕のＨＰに載せていますので
興味のある方は、見て下さいね。

### [9764] 将人（元・魔神）	2003.03.03 MON 20:57:00　
	
こんにちは、将人（元・魔神）です。
＞[9746] いちの様、マイコ様
いちの様、マイコ様のお二人のメールで頂いたイラストを僕のＨＰに
載せるのを、許可して頂いてありがとうございます。

今日、さっそく僕のＨＰに載せるように致しました。
この書き込みしている掲示板の名前の横の「URL」欄をクリックすると
里奈さま、いちの様、マイコ様のイラストのページに直接行けるように
していますよ。
３人の方のイラストを見たい方は、良かったら見て下さいね。

→普段は、僕のＨＰのトップページにしています。今回だけです。

＞[9746] いちの様　2003.03.03 MON 00:08:17　
更新された「もっこりメッセージ」編集部の方にも内緒だったんですね
今のジャ＠プは、読んでないんで分からないんですが、サ＠デーとか
だと原作者の方がが描いたシーンとかだと、その雑誌で大きく
取り上げて見て下さいと、宣伝しているんですけどね。

→劇場版だからかもしれませんが・・・。

＞[9747] 無言のパンダ様　2003.03.03 MON 00:31:41　
北条司先生の描かれた部分って、どこなんでしょうね？
３０分の番組の一瞬のシーンだけど、どこか知りたいですね。

「カイカ～ン」は、やっぱり「セー＠服と＠関銃」でしたか？

僕はバンチを買っているのに漫画本編だけみてプレゼントとか
見逃していました。
この掲示板ＢＢＳに来てなかったら、今回の複製原画集も
たぶん気が付かなかったと思いますよ(汗)(^^；

ＣＤブック、今度こそはって思ってるんですが・・・前も
狙っていたら、直前で取られた事ありましたよ。
最近オークションは見てるけど、入札しなかったんで知らなかったけど
今は時間キッチリなのかな？
前は、直前の数分で入札されたら５分ずつ延長されていたので油断してました。

お送りした面白い画像のメールは、面白いけど、ちょっと不謹慎な
ブラックユーモアというようなネタでしたね。
笑って良い物かどうか微妙ですね。

＞[9748] Rouark様　2003.03.03 MON 00:36:22　
「RASH!!」は、無言のパンダ様の言われるように全２巻ですよ。
１巻２巻を買われたそうですが、たしかにあの２巻の最後の終わり方
なら、まだ続きがありそうな終わり方でしたね。

＞[9750] 里奈さま　2003.03.03 MON 05:45:38　
アニメのシティータンター２の最後の方のシーンって、冴羽さんんが
３０歳の中年はイヤダ！ってオチになる部分でしょうか？
あちゃ、ビデオに録画しておくべきだったな～(汗)(^^；
ケーブルテレビで放送されていたけど、まさか、そんなシーンあるとは
思わずに、前に見てたからいいやって思って、録画せずに、見ていた
だけでした。
北条司先生と同じ時間に見てたのだろうか？それは、うれしいような。

→CS放送のCHは、同じ内容を時間帯を変えて放送しています。

僕は、眼鏡をかけていても、そう遠くは、はっきり見えないので
的は当たらないかもしれませんね。

＞[9751] 知沙さま　2003.03.03 MON 05:47:22　
＞[9753] yosiko様　2003.03.03 MON 10:23:49
＞[9760] りえ様　2003.03.03 MON 16:41:25　
知沙さま、yosiko様、りえ様、はじめまして。こんばんは！
ここはファン歴が短い、長いとか関係なく、北条司先生の作品の
好きな方が集まってますし、みんな良い人ですから、遠慮なく
書き込んで下さいね。
常連さん同時のレス（返事）が多いので、戸惑われるかもしれませんが
いろんな話題に（分かる物でいいですから）参加して見て下さいね。

### [9763] 野上唯香	2003.03.03 MON 18:37:19　
	
知沙様
先程は手短な挨拶だけでしたが、知沙さんは１７なら私と同じ高２（４月から高３）ですか？今私のクラスでは藤木直人＆上戸彩の金曜ドラマ・『高校教師』が超流行っています。今町田雛や工藤紅子と同じ学年の私は藤木直人のような先生に憧れています(^^)ちなみに私も高校教師に出てくるように女子高です。私がＢＢＳ内でよく話すのは里奈様と将人様です。以前この方達と一緒に「もしもＣＨキャラが教師だったら・・・」というタイトルで合同コントを作ったんですよ！！私はコント作りが好きで私が作ったコントはいつも好評で今までの作品は全て将人様のサイトに掲載されています。里奈様はギャル系ショップ店員でＣＨ関連のイラストがとても上手で優しくて美人で仲間由紀恵の様なサラサラストレートロングヘアの綺麗なお姉さんです(*^_^*)将人様も優しくて良い人で自ら管理人を勤めるサイトはとても面白い人です。これから宜しく☆★　

### [9762] 将人（元・魔神）	2003.03.03 MON 18:23:12　
	
ＣＨクイズの答えです。
その1・映画女優・佐藤由美子が自分を殺すように、海坊主に依頼した
　　　　事件「亡霊を撃て」で、冴羽さんは、何で海坊主さんを
　　　　買収した？

１.昨日の朝メシ

由美子「ふ・・・ふたりともグルぅ！！」
由美子「そんな！依頼は必ず守る海坊主がどうして・・・！？」
冴羽　「俺が買収しちゃった！！」
由美子「いったい、いくらでっ！！」
海坊主「きのうの朝メシ・・・かな」
由美子「・・・ずいぶん安いのね、あたしの命って・・・」


その2・「とんだティチャー」で片岡優子さんの執事の四谷さんが
　　　　冴羽さんを調べていたのは？

２.優子さんを助けた人に顔が似てる為

→1や3は、冴羽さんが、隠し撮りに気付いて、写してしまった事ですね
　4の"声"は、冴羽さんは一言もしゃべっていないようです。

優子「あのとき・・・なぜ一言もしゃべらなかったの？」
優子「助けてくれたとき・・・わたし、震えながら言ったのよ
　　　「ありがとう」・・・て、でも、あなたは一言も・・・」
冴羽「日本語がよく、しゃべれなかったのさ」
冴羽「日本についたばかりでね・・・言葉をほとんど忘れていた・・・」

その3・片岡優子さんの「白馬の騎士(ナイト)」で冴羽さんにあったのは？

３.胸にキズ跡

→冴羽さんに関係あるのは、567ですね。
　5、海坊主さんと対決した時出来た傷です。、冴羽さんを行方不明の
　孫かもというお爺さんの回と、最後の方の海原さん（ユニオン
　テオーペ）の船に向かうシーンとかで出ています。

　7、手術の跡は、あるか無いか分かりませんが、目の手術をする
　浦上まゆ子ちゃんの回で、冴羽さんは盲腸になりました。
　たぶん手術もしたんだと思いますが～あるんだろうか？(汗)(^^；

　どちらも、４巻での話の時は、まだ出来ていない傷跡です。
　6は、ＡＨの香瑩にある自殺した時の傷と、手術跡ですね。

→他はジャ＠プ出身のヒーローです。なぜかＣＨがＡＨとしてパラレル
　ですが続編が出来たように、色々と形は違いますが、続編として
　復活している漫画ですね。
　ブラック以外は冴羽さん役の声優・神谷明さんが演じられました。

　1、北斗＠拳ケンシ＠ウ　2.ブラック＠ンジェルス雪籐ほか
　4.キン肉＠ン

他のヒーローの傷やその設定が、冴羽あったとしてもカッコ良いと
思いますが、さすがに冴羽さんの額に"肉"の文字はやだな(汗)(^^；

その4・片岡優子を狙った黒幕赤川グループの会長から手を引かせた
　　　　冴羽さんが狙った物は？

4.コーヒーカップ

→「でも冴羽さん、まだ黒幕がどう動くか分からない今、あなたに
　　去られるのは・・・」
　「フフ・・・やつの野望は・・・コーヒーカップと、ともに
　　砕け散った・・・よ！！」

### [9761] TAKESHI	2003.03.03 MON 17:11:11　
	
あ～、今日ものんびりと一日を過ごしてしまった。休みってなんだか気が抜けてしまいます。

＞yosiko様
僕も確か再放送で見たんだと思います。小さい頃なのでよく覚えてません。銃に興味を持つのは全然おかしくないですよ。僕もＣＨにはまって以来、ちょっとずつ銃のことに興味を持つようになりました。コルトパイソン３５７マグナムなんて欲しくてたまりませんよ（笑）

＞りえ様
初めまして。そうですね、今日は雛祭りなのに雨とは、なんとなく落ち込んでしまいます。男の僕がこんなこと言ってもしょうがないですけど（笑）今日はのんびりとしております。
