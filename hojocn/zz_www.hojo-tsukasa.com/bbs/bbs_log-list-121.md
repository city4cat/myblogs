https://web.archive.org/web/20011102115916/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=121

### [2420] Tata（静岡は午後から晴れました）	2001.08.22 WED 18:52:48　
	
・ねたばれ

　やっぱりそうきましたか、李大人・・・ＣＨでも何度かありましたしそうかなっと。でもそうなると今まで出ていた人はどっちなんだろう？海ちゃんと歓談している人物が３話（総集編８７ページ）で嫌な笑いしている人と同一人物とは思いたくないんですが、みなさんどうです？

・もっこりネタ（＞もっぴーさん[2408]、 法水さん[2415]

　もっこりの相手は依頼人と言うことなら無いと思いますが、それ以外ならあからさまなのが一つありますよ。たしか冴子が初登場の話、ダヤン王子（でしたっけ？自信ない）の親衛隊５０人の足腰立たなくして（うわっ、露骨！）冴子にもっこりを迫るシーンがあります。

### [2419] ちゃこ	2001.08.22 WED 18:05:04　
	
さて、解禁ですね。
とりあえず自分の感想を書いちゃいます。

もう、読み終わった後、切なすぎて頭が真っ白になってしまいました。
通勤途中に読まれた方、大丈夫でしたか？
私は、給油している間に読んだのですが、運転中ボ～ッとしてしまいました。
無事、家に帰り着いて良かった（＾＾；；）

私は、香が「ドナー」になったのは
「もし、私がリョウの前から消えることがあったとしても、
誰かの体の中で生きてるから。だから、リョウも生き続けてね」
という理由からかなぁ・・・と思っていました。
そんなもんじゃ無かった。もっと、衝撃的だった。

香にとって、何より恐怖だったのは「リョウの死」で・・・
本当にリョウは、香の全てだったんだ。
どうして、これほどまでに「リョウのため」に生きられるんだろう？
（リョウだって「香のため」に生きてきたのだと思う）
２人は「お互いのため」に生きて、生きようとして。

この２人の「リョウの（香の）ため」っていうのは、
いわゆる「自己犠牲」の精神から発しているものでは無いと思う。
だって、２人の気持ちが「相手を生かして」いるから。
自分に置き換えて考える。私は「誰かのため」に生きられるだろうか？
そして、誰かが「生かしてくれている」から「生きている」と、
これから先、感謝して謙虚に生きられるだろうか？

どうかＧＨ、生きて生きて、生き延びて、
その鼓動をリョウに届けてあげて下さい。
リョウに、香はまだ生きていることを教えてあげて下さい。
そう、香は死んでいません。
肉体は無くなってしまったかもしれないけど、香の心は生きてる。
そしてリョウが生き続ける事が、香の心を生き続けさせる唯一の方法だと思う。

・・・こんなに、作品に感情移入しちゃって、子供みたいかな？
でも、涙も出ないくらい、衝撃を受けたのです。
長文の独白、失礼いたしました。

### [2418] sweeper(槙村兄と香を思い出す・・・。)	2001.08.22 WED 17:50:16　
	
こんにちは。解禁前のカキコです。

＞月見様。［２３８５］
レス、ありがとうございます。
そうですよね！ラブラブというわけではないけれど、ベタベタしてない。いいですよね。こんな２人の関係。
私も何見て笑ってるんだろうって思いました。もし、壁紙か待受画面があったらダウンロードしておきたいです！

＞Mami様。［２３８７］、ちゃこ様。［２３９３］
レス、ありがとうございます。
そうですよね！りょうと香、グラスハートには救われて欲しいですよね。今のりょうは、時折見せる表情が辛そうです。危うい状態なのでは・・・。と思ってしまいます。
チャゲアスの「蛍」、ＡＨのりょうと香を思い出します。サビのところとかが切ないです。

＞さえむら様。［２３９１］
レス、ありがとうございます。
余談ですが、かすみ２度目の登場の話。もっこりを催眠術で封じられたりょうが気性の荒いスッポンを崇司の股間に入れるという仕返しが笑えました。(撃沈・・・。)

＞葉月様。[２３９４]
りょう、くわえ煙草すごく似合いますよね。
私は、マリィー初登場の話での屋上でのくわえ煙草が好きです。
目のアップがかっこいいです。
あとはちょっとした仕草ですね。香の頭をクシャッとなでたりとか。パイソンをかまえたときのガンアクションも捨てがたいです。こういうところに何気に「男の色気」というやつを感じます。(暴走してる！すいません。滝汗)

＞hiromi様。[２３９６]
はじめまして。
レス、ありがとうございます。「りょうちゃん大カンゲキー！」気に入って頂いてありがとうございます。「りょうちゃん大カンゲキー！」です。
そうですよね。香が裏の世界に入ったのは自分の意思ですよね。
絶対りょうのせいにはしないと思います。

＞法水もっこり研究会名誉会長様。[２４１５]
お久しぶりです。覚えてますよー。
そういえば、銀狐の話でもりょうは香にもっこりしてましたしね。香も香でりょうが自分にもっこりしないことをだんだん気にしてないような気がしませんか？

[伝言板のこと]
けっこう、遊び心はいってますよね。アニメのスタッフの名前とか入っていて見ていて面白いですよね。
ところで西九条沙羅ちゃんのお話。昨日読み返していたら伝言板の待ち合わせ場所が「AM１０:００天安門広場にて待つ」ってあったんですが、これもパーフェクトガイドブックで北条先生がおっしゃっていた「イタズラ」なんでしょうか。

[りょうと香のこと２]
ミック初登場の話で香がりょうに言っていたセリフ「生きるも死ぬも～どこの誰だよバカぁ！」とそのあとのりょうの表情。複雑というかあ然というか・・・。何て言えばいいかわからない表情してましたよね。もしかしたらこれは、香のりょうに対する気持ちではと思ってしまいます。りょうは辛いこととか何でも一人で抱え込んでしまうようなところがあるから、槙村兄と香がもしいなかったら、いつも死に急いでいたと思うのですが。香がいたからりょうは救われていたんですね。でも、りょうには香の手を絶対離して欲しくなかった。どんなことがあってもつないでいて欲しかったです。(悲しくなってきた・・・。)(T○T)

[ＡＨのこと]
これほど、感情移入できた漫画は初めてです。
ＣＨとは別物だと思っていてもどうしてもＣＨとだぶってしまいます・・・。

長々失礼しました。
それでは。

[留言板]  
玩心还是挺大的。加入了动画工作人员的名字，看着很有趣呢。
对了，西九条沙罗的故事。昨天重读时发现留言板上的约定地点是“am10: 00在天安门广场等候”，难道这也是Perfect Guide Book上北条老师所说的“恶作剧”吗?  


### [2417] ちゃこ	2001.08.22 WED 16:42:47　
	
台風は、行ってしまったのでしょうか？
山梨県は、カンカン夏の日差しです。
それにしても台風の影響か、今日は昼間のアクセスが多いですね。


＞冴羽馨り様「２４０１」
はじめまして。最近、出没始めたちゃこです。宜しくお願いします。
私も運び屋のみゆきさん好きです。
気の強いところも良いけど、何と言っても、歌っている姿が可愛らしい（＾＾）
この話のラストでみゆきが香のことスカウトしてるけど、
本当に２人がコンビになったら「ダーティペア」みたいで、面白そう！
・・・「ダーティーペア」知らないかな（＾＾；；）


＞Eve様「２４０２」
本当、素晴らしい洞察力の弟さんですね。
実際、北条先生自身が、どんなに「ＡＨとＣＨは、違うお話です。」って言っても
ＣＨの登場人物が出てくる以上、読者は「ＣＨの続編」という捉え方をしますから
先生は、スゴイ決断をしたと思います。
（生憎「波」のインタビュー記事は、読むことが出来なかったのですが、
何かのインタビューを読んで、私は「どうしてこんなに、違う話だと
力説するのだろう？」と思ったくらいです。）
香を死なせてしまったことについて、私も納得なんかできるわけありませんが、
先生が「香を死なせてまでも」伝えたかったメッセージ（話）を
ちょっと頑張って読み続けてみようと思ってます。


＞まみころ様「２４１１」sweeper様「２３８６」
本当、海坊主サン＆美樹は、リョウ＆香の兄貴（姉貴）分みたいなところが
ありましたよね。
対策用にシェルター作るくらいなら、バズーカ一発！追い出せばいいと思うんだけど
そのままやらせてしまうし。（ていうか、誰も香を止められないのか！？）
香の一途さや無垢さは、裏の世界で生きていた人達には、眩しく映ったと思う。
そんな香の輝きを、皆、大切にしていたんでしょうね。

「こんぺいとう話」
貫通してましたか・・・（絶句）
やはりヒーローは、不死身の肉体が基本なのですね（笑）

もうじき解禁。またお会いしましょう。

### [2416] プリ	2001.08.22 WED 15:53:00　
	
＞Ｅｖｅさん［2414］
それはネタバレです。
注意書きにも書いてありますが、最新号の話題の解禁は水曜日の１８時からです。
以後気を付けてください。

### [2415] 法水  2001.08.22 WED 15:26:39　
	
昨日の夜から今日の昼にかけてこのサイトにだけアクセスできんかった…なぜ？

［久々もっこりネタ］
どうもどうも。もっこり研究会名誉会長の法水です（誰も覚えてないって？）。

＞もっぴーさん[2408]。
私といたしましてはずばり！リョウは誰とももっこりしてないです。だって冴子とチャンスがあったときですら逃亡（？）してるんですから（少年誌というのもあるだろうけど）。

＞さえむらさん[2391]＆のんちさん[2413]。
渚ちゃんのエピソード以降でリョウが香にもっこりしたのはまだあります（なんか前にもこのネタやったような気もしますが…ま、いいか）。「思い出の渚」でカツラをかぶってオーディション出場者に化けた香にもっこりしてますね（コミックス９巻123ページ）。しかもその前のページで「もろ好み」と言ってます。ここらへんにもリョウの本音が見え隠れしてますね。

台風接近の中コミックス読んでてひとつ発見しました。かすみが初登場する話の中で学ランに身を包んだリョウが「秘奥義上下つっぱり」と称して下半身を「つっぱり」させてます（コミックス８巻47ページ）。「もっこり」以外にもワザがあったのか…。

あとこの話に出てくる不律乱寓博士って今考えたらドイツの映画監督フリッツ・ラングのもじりじゃないですか！　chikkaさんご指摘の通り、北条先生は映画好きですよね。「ＲＡＳＨ!!」のサブタイトルも映画のタイトルから取られてましたし。

还有这个故事里出现的不律乱寓博士，现在想想不就是模仿德国电影导演Fritz Lang吗?正如chikka指出的那样，北条老师很喜欢电影。“RASH !”的副标题也是取自电影的标题。

### [2414] Eve	2001.08.22 WED 15:03:03　
	
>つぐみさん
はじめまして。
お話・・・できるかなぁ。かなりショックだったものですから、暫く遠ざかろうと思っているんです。

>もっぴーさん
はじめまして。レスありがとうございます。
香ちゃんが死んだのは今週号のＡＨで否定できなくなってしまって・・私の中で。だから来週から買うのをやめようかと。ＡＨの記憶としては存在しないはずの、リョウちゃんと香ちゃんの思い出とかドナー登録したのはリョウちゃんの生命保険だったこととか、それを告げたときのリョウちゃんの香ちゃんの表情とかがＡＨの中でFlash Backするんですが・・・もう読んでいて涙でした。ふたりの絆の深さに驚くあまり自殺を思いとどまるＡＨ。リョウちゃんとＡＨが結ばれるハッピーエンドなんて、あまりに現実っぽすぎてヤ！o(><;)(;><)o　あぁ・・・でもこのまま読んでいたらそれを許してしまいそうで・・・でもきっと許したらあの３５冊のコミックスは二度と読みたくなくなってしまうだろうし・・・。

>まみころさん
はじめまして。レスありがとうございます。
でも皆さんのカキコを読んでいて、落ち込んでいるのは私だけじゃないと知って安心しました。ひどく子供っぽい感情だと思って、最初のカキコはかなり勇気がいったのです。少し気分が楽になりました。

北条先生、罪な漫画家さんですね。ペンひとつでこんなにも多くの人の心を揺り動かしてしまうのですもの。まるでHarriet P. Stoweの漫画家版みたい・・・。(T_T)

ところで台風はどこにいったんだろう？何だか全然大したこと無かったような・・。

### [2413] のんち　(主にレスです)	2001.08.22 WED 13:19:06　
	
初めての方が結構増えてきましたね。これからよろしくお願いします♪
台風でバンチ買えなかったっていう方、結構いらっしゃるみたいですね。雨がやんだら是非ゲットしてくださいね！早く解禁になれ～！

＞さえむらさま[2335]　しゃるさま[2360]　ちゃこさま[2376]
レスありがとうございます。理想としてはやっぱり槇村と香ちゃんとリョウの３人の写真ってのがいいですよね。
槇村兄妹はもういない・・・。うー、寂しすぎる・・・。

＞sweeper　さま[2338][2379]
ミックのキスした場所って唇ギリギリだったんですよね。ミックは優しいから香ちゃんのファーストキス奪わないでしょ。あとでリョウに必死に弁解してる香ちゃんがかわいい♪

それと、確かにリョウの手帳に声優さんの名前かいてありました！あれだけじゃなくて掲示板の落書きとかにもたまに声優さんへのメッセージみたいの書いてありますよ♪
翔子さんの話の最初の回の最初のシーンの掲示板がよくわかると思います。

＞再びさえむらさま[2391]
リョウのもっこりについて。香にもっこりするシーンまだありますよ♪
絵梨子さんが香に水着でモデルの特訓させるシーンです。あれは確実に香とわかってるのにしちゃってますよね。そのあとリョウはかなり焦ってるけど（笑）。
リョウが「香ってあんなにカッコイイ女だったっけ？」って言ってるのもいいですよね♪

### [2412] まさやん　（大阪県？限りなく和歌山拠り）	2001.08.22 WED 12:58:22　
	
初めまして、A.H.がきっかけになって、再び北条先生の作品を
読み漁ってます。それで、北条先生のホームページってないか
なぁ、と思い検索してみたら、ビンゴ！！活気あふれる掲示板
なので、思わずカキコしたくなりました。

A.H.読んで、まず感じた事は、皆さんと同様、”香の死”とい
う巨大な喪失感なのですが、これって、りょうの気持ちと読者
の気持ちが見事にリンクしてますよね？
その分、他の作品よりも感情移入しやすいですね。（でも、香
は生きていてほしかったなぁ。）

気が早いかもしれないですが、A.H.のラストってどうなるんだ
ろう？　私はG.H.が記憶喪失になって、りょうの娘となり（香
の影響でハンマー振り回したりして。）、ハッピーエンドって
感じになってほしいと思ってます。

A.H.と似た設定のまんがで、高橋ツトムの”地雷震”の最終話
でLIFEという話がありますが、こっちは主人公が相手の心臓を
打ち抜くという、バッドエンディングなんですよ。
A.H.では、りょうとG.H.には幸せになって欲しいです。今まで
どんな事件も解決してきたりょうが、最後の仕事と言うのだか
ら安心でしょうが。

長くなってしまいましたので、この辺でやめときますね。

### [2411] まみころ	2001.08.22 WED 12:42:11　
	
［２３８５］月見ちゃま。
香の不二子化コメントには笑っちゃいました。
確かに香がお色気で男に･･･だったら、不二子ですね。
でもその役は、冴子が引き受けてくれてますね。（笑）

［２３８６］のsweeper様。
リョウと香のケンカで、海坊主がシェルターを作った話。
確かあの時海坊主が｢香はスイーパーとしては腕を上げた。｣
というような事を言ってませんでしたか？
私はあの一言で、海坊主と香の師弟関係は確かだと感じました。
だって、明日の敵になるかもしれない相手のパートナーに、
自分のトラップ技術を教えるところからして可愛がってる感じ
で良いです。
香も｢海坊主直伝｣と行ってますし。（笑）
それに美樹も香の事をとても可愛がってますよね。
射撃の練習に付き合ったり、後半に入ってからはずっと
良き相談相手でしたものね。
誰からも好かれる香がもういない･･･。あ、悲しくなってきた。

［２３９５］ちゃこ様。
コンペイトウの話といえば、初めの方に出てきたコンペイトウのとげが、リョウの頭に刺さってたのを憶えてます。
貫通してるので、あれは死んでて普通です。（笑）

［２４０２］Ｅｖｅ様。
初めまして、まみころと申します。
私もＡＨを初めて読んでショックを受けたひとりです。
これからも一緒に香の心臓を持ったＧＨとリョウの行方を
見守っていきましょうね。

［２４０８］もっぴー様。
もっこり話なんですが、私は一応しちゃってると思ってます。
やっぱりＡＨの最初の方で｢私と一晩過ごした｣と言ってる
女の人がいるので、多分してるのでは？
プラトニックな関係は、まずあのリョウからして無理のような。
ただ、女としての意見で言えば、してない方が良いですけどね。

最後にとても長くなってしまいましたが、
皆さん北条先生の作品がとても好きなんだと感じます。
私もここに来て色々な事を学んで吸収してます。
新しい方もそうでない方も、どうぞヨロシクです。

### [2410] つぐみ	2001.08.22 WED 11:28:01　
	
[2385]月見さま
お返事ありがとうございました。ああいう時、北条先生は
リョウの感情をハッキリと描きませんよね。
そこがまたいいのだけど、胸をグッ衝かれて痛みます。

皆さん、台風の被害は大丈夫でしょうか？

### [2409] もっぴー	2001.08.22 WED 11:20:54　
	
＞Ｅｖｅ様

私は、シティハンターは、心のオアシスでなので、
ショックでしたよ。
他の漫画は漫画で終わったけど、
シティハンターは漫画という事で終われないというか。

これに関しては、何度も過去に書きこみしましたが、
今だ、受け止められません。
でも、まだ香ちゃんの死に関しては曖昧な点が沢山あるので、
私は、香がまだ生きているんでは無いかと、信じていたりも
しています。
全ては、連載が終わった時に私の中で決着がつくのだろうと
思います。
やっぱり、リョウと香の未来を決めれるのは北条先生ただ１人
だから、幸せにしてあげて欲しいですよね。

【アシスタント】
皆さんアシスタントが誰だとか詳しいですよね。
すっごい！

### [2408] もっぴー	2001.08.22 WED 11:07:29　
	
こんにちわ！（＾＾）/
こちらでは台風避けられました。

【レス】

＞タウンハンター様
〈男性からみたシティーハンターについて、〉

ご意見有難うございます。
今後とも、男性からの視点の貴重なご意見お聞かせ下さい。
ちなみに、私は理想のタイプは、リョウちゃんですが、
やっぱり、香ちゃんだったりするんですか？

他の男性の方の理想のタイプも聞きたいです。

＞ｓｗｅｅｐｅｒ様
(ＣＤ，ドラマチックマスターについて）

さんきゅーでーす！

ＧＥＴ　ＷＩＬＤの、前の
香『また、足手まといになっちゃったね。』
リョウ『香、おつかれさん』
も、良いですよね！

【もっこり】

えっと、私は、これに関しては、もっこりするだけで、
実行は１度もしていないと信じているんですが、
（本当の所は愛する女しか抱けない。）
ある一般的なファンの男友達が言うには、
やりまくりとか言うんですよ！（下品でごめんなさい。）
でも軽く読んでいる人はそう思うんでしょうかね？
男性視点だし。
でも、リョウはいろいろと、屈折してるから、
普通の人とも違うと思うし。

皆さんは本当の所はどう想いますか？
でも、もししてなかったら、香ちゃんと結ばれた日までずっと、
ご無沙汰って事ですよね。
わたしは、そうだと信じているんですけどね。
みなさんは、どう解釈しているのかと想って・・・。

ずっと、聞きたかったけどちょっと、迷っちゃって。
でも聞いてみました。


### [2407] つぐみ	2001.08.22 WED 11:00:31　
	
[2396]hiromiさま
ありがとうございます。由加里ママの件、なるほど！と思いました。
ホっとした気持ちです。

[2406]EVEさま
私も怖いほど今、ＣＨとＡＨにハマっています。なので気持ちは
とてもよくわかります。私は書くことが好きなので、やりきれない
気持ちはいろいろと書くことで解消しています。
香ちゃんのしあわせだった時の気持ちとか、いろんな気持ちを。
でもEVEさん、これからもここでいろいろとお話してくれるんですよね？

### [2406] Eve	2001.08.22 WED 09:41:29　
	
今日は台風で休暇を取りました。

私は来週からは暫くＡＨを読むのを（バンチを買うのを）お休みすることにしました。読むたび、香ちゃんの死が現実（いや、フィクションなんだけど）になっていくのが悲しいので。私には、ＡＨを楽しむにはもう少し時間が必要なようです。でも映画のReturn to Meみたく、心臓移植された女性とドナーの夫が恋をする、って展開になるのだけはどーしても受け入れられない私。

私がＣＨを好きになったのは、ＣＨが持つ非日常性と現実性という相反する要素。現実の世界では人間は１人では生きられないので死んだ人を引きずるのは絶対良くないともう。だから香ちゃんには絶対死んで欲しくなかった。でも香ちゃんが死んでしまった以上、非現実的な路線で是非ひきずってほしい。(^^;)　リョウちゃんが香ちゃん以外の女性と人生のパートナーになるなんて絶対いや！（遊びかでならいい。また、仕事のパートナーとしてアクションものに絞るならそれもいい。）でももしそうなったら、現実逃避してＣＨのFanFicだけに逃げちゃおうかなぁ。

Mamiさん

初めまして、Eveです。レスありがとうございました。
この落ち込み、子供っぽいと自分でもわかっているのです。悲劇さえも「新しい展開」として楽しむようになれたら・・と思うのですが、そうはいかなくて。

サイト管理者様へのリクエスト２
北条先生の作品を、ここから直接ネット注文できるようにしてほしいです。

### [2405] 幸	2001.08.22 WED 09:37:03　
	
m(__)mはじめまして♪幸です。
北条先生の作品は大好きで読みあさってます。(笑)
F.C.が終わって新連載♪♪♪
ヾ（＞▽＜）ゞ ウレシイ♪いえぇ～いっ！
ドキドキしながら少しだけ読んでみました。

しかし、C.H.への思い入れが強すぎる私にはナカナカ痛かったので、A.H.はコミックが出て一気に読もうと思って、雑誌の方は買わないことに決めました。(ファン失格？滝汗)
(T△T)　アウアウ～
リョウと香が結ばれなかったのはとっても悲しいケド･･･
新しい刺激と、リョウがhappyになれるような内容になることを期待してまふ。
リョウ大好きファンのマガッタ願望ですが、叶うことを期待してまふ。
神様！司様！ ｍ(゜゜）おねがひ･･･。(願)

### [2404] Ｍａｍｉ	2001.08.22 WED 07:50:57　
	
＞ちゃこさん［2393］
　再びレスありがとうございます。しかも立ち読み勘弁してくださって（笑）　でも、たしかにコンビニで泣いちゃうかもしれませんね。「総集編①」もチラっと目を通して買ったけど帰りには悲壮な顔でしたから。うー、今日こそ２週間ぶりの［ＡＨ］読みたいですぅ。

＞ｃｈｉｋｋａさん［2398］
　初めまして。歴史の勉強もできるなんてよかったです。しかし漫画家もいろいろ博学でないといけないものですね。
和歌には当時も切なくて泣いたものだけど、今回もどう言う形でもいいから、漫画の世界。なんでもありで香を生きかえらせて欲しいものです。（ムリかなぁ・・・）

＞みみごんさん［2400］
　ココいつも言うけど早すぎです！私も分けわかんなくてレス出し忘れも多くて皆様ごめんなさいね。それだけ北条ファン多しの証拠かな？

＞Ｅｖｅさん［2402］
　初めまして。私もほんの２週間程前に「総集編①」を初めて手にとり未だに落ちこんでいるＭａｍｉと申します。過去ログにも書いたけど［ＣＨ］の大ファンだったし未だに読み返せません。
ショック大きくて・・・。でも弟さんのいうこと、もっともですよね。北条先生すごい勇気というか発想というか。でも哀しいの～！

長々と失礼しました。

### [2403] Takenori	2001.08.22 WED 07:42:31　
	
私は、いま外国に住んでいるため簡単なストーリーしか分かりません｡
ですが、北条さんにひとつだけ聞きたいことがあります｡
どういった気持ちで冴羽リョウを登場させた、というかCityHunterの続編のような形にしたのでしょうか？
というのも、作家の方には2つのタイプがいると思います｡
1つ1つの作品は、すでに終わったものだからもう二度と使ったりはしない、と言う方｡
そして、もうひとつのタイプはまた使ったりする方｡
今回、これを拝見して複雑な感情を抱きました｡
また彼らが登場するという、うれしいような感情｡
その一方で以前のイメージとは違ったり、蛇足になりはしないかというものです｡
いずれにせよ、北条さんのような方であれば質の高い作品を送り出してくれるという信頼感を持って応援したいと思います｡
これからも、体調には気をつけて楽しさとシリアスな展開をうまく融合させるマンガを期待しています｡

我现在住在国外，所以只知道简单的故事。
但是，我有一件事想问北条。
是以怎样的心情让冴羽獠登场，或者说是像CityHunter的续篇一样的形式呢?
因为我认为作家有两种类型。
有人说，每一个作品都是已经结束的东西，不会再使用第二次。
另外一种是还会使用的人。
这次，我看了这个，产生了复杂的感情。
他们又要登场了，那种高兴的感情。
另一方面，会不会和以前的印象不一样，会不会画蛇添足。
不管怎样，只要是像北条这样的人，就会送出高质量的作品，抱着这样的信赖感来支持他。
今后也要注意身体，期待着将快乐和严肃的展开很好地融合在一起的漫画。

### [2402] Eve	2001.08.22 WED 06:31:35　
	
【驚きと嘆き】
ＣＨが開始したと聞いてさっそくキオスクへ。そして不幸のどん底なわたし。
どーしてぇ？どーして香ちゃんが死ななきゃ行けないのぉ？北条先生、編集さん、ひどいよぉ～！ o(><;)(;><)o　それじゃなに？ＡＨって、Return to Me(邦題：この胸のときめき)みたいに、二人は恋愛関係になっちゃうのー？　香ちゃんかわいそう過ぎるよ～。だってきれい事言ったって、結局、人間は死んだら終わりじゃないの～！せめて二人の忘れ形見でもリョウの手元に残っていれば・・・。あるいは香ちゃんの生まれ変わりの女の子と出会うとか・・・。香ちゃんがいないなんて、やだーやだー！

ＣＨ大好きだったけど・・・書棚に入った３５巻ものコミックスも（一時的だろうけど）今はつらくて読めない。(T_T)　私としては是非番外編で本物のＣＨ（リョウとカオリの、の意味）を書いて欲しいですぅ。（リクエスト）

でも弟が、「長年あれほど人気のあったＣＨをこういう形にしたっていうのは書いてる方にとっては挑戦だと思うよ。惰性のない、いい漫画家だと思うけど、ＣＨファンにはつらいよね。」などと冷静な事を言うので、またまた今週もバンチを買ってしまった。でもでも・・・やっぱり私は悲しい。

こんな気持ちは私だけなのでしょうかぁ・・・（かなり落ち込み）

【サイト管理者さま】
このＢＢＳ、投稿にタイトルが入れられないのですね。
カキコにタイトル入っていると、読むときちょこっと便利なのになー。(^^)

但是弟弟说:“把这么受欢迎多年的CH变成这样，对创作者来说是一种挑战。我觉得他是一个没有惰性的好漫画家，但是对CH的粉丝来说很痛苦。”因为说了这样冷静的话，所以这周又买了bunch。但是……我还是很悲伤。

### [2401] 冴羽 馨り	2001.08.22 WED 02:03:15　
	
こんばんは！　今日は台風でバンチ読めませんでした

皆さまは依頼人の中で誰が好きですか？
私は１７巻に出てきたキミ子ちゃんと野上唯香ちゃんと運び屋の小林みゆきさんですね
常に気の強いところが好きです。友達になりたいタイプです

そして、気は強くないけど好きなのは２２巻の愛子さん。酔うと人格変わるところがギャップ激しくて好きです。

逆にちょーっと苦手なタイプの依頼人は（ってこの場で言ってもよろしいのでしょうか）、アルマ王女や神宮寺遥さんなどの世間知らずなお嬢サマ系たち。 

