https://web.archive.org/web/20020625010743/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=91

### [1820] 葵（またもや…☆）	2001.07.29 SUN 21:39:47　
	
　ぐ～っ☆あろうことか、「週間ブックレビュー」見逃したっ！
なぜか一時間間違えてて、１１時１６分にチャンネルあわせたら、小堺さんが映画について語ってた…。うﾞぁ～っ☆オバカ☆

　でも、更新されてた北条先生のメッセージを読んで、あ…見なくてもよかったのかな…と思いました。製作段階の苦しんでるトコなんて見てほしくないよなぁ…。（←実体験♪）でも、一度でいいから、「動く北条先生｣を見たかった…。（前回のテレビも見逃した、バカなヤツ×××）じ…次々回こそっ？！

### [1819] OLIVE	2001.07.29 SUN 21:07:51　
	
Hello!
Many all over the world of hojo-sensei fan!!
I am Japanese.
I want to ask to you.
Can you read [Angel Heart] [comic BUNCH]?
Please tell me how to obtain information about the story of A･H?

### [1818] なっちゃん	2001.07.29 SUN 19:44:04　
	
北条先生のコメント、ちょっと感動しちゃいました・・・￥
プロだからこそ、苦労話や裏話などで関心をひくような真似をせず、作品一本で勝負したいとおっしゃるのですね・・うーん、やっぱり生みの親なのだなぁ、冴羽リョウ的ですよね、すごく！
ていうか、そのもの。他のプロの方達のことを非難するつもりはさらさらありませんが、（だって人間の考え方は1００人１００色ですもん）北条先生のそういう考え方、とても素敵だと思いました。私は心がふるえました。

### [1817] ありあり（５日ぶり）	2001.07.29 SUN 18:53:28　
	
こんばんは。誰か覚えてます？
新しい人がたくさんいらしゃって、新しい雰囲気になりつつあるのでしょうか。

【１１話感想】
いよいよ始まったって感じ。昔、まだ１０話かもう１０話かって話があった時に１０話が区切りじゃないかと発言した事があるけど、まさに１１話からスピードが上がりそうですね。
【総集編発売！！】
多分買っちゃうと思う（笑）
初めの方読めなかったし。この総集編を何気なく手に取って、読んで、バンチに新たにはまる人がいればなお良し（＾＾）
【ウイルス】
ここで聞く事ではないかも知れませんが、最近賑わせているウイルス、Windowsに感染してどうのこうのとありますが、Macはどうなんだろう・・？すみません、Macなもので、心配で・・。
【掲示板】
いろんな話が飛び交ってますが、初めの頃「これって私の事かな？」と思うような、使用法についての御指摘が何点かありました。確かに誰かにレスしてほしいという甘えた考えがあったのは事実です。これからはあくまで１参加者という立ち場を守って行けるよう努力しようかな、と考えております。
そしてその頃わたしの怪文につき合ってくれた方々のHNを見かけないのはとても残念。ある意味原因の一端である私がこんな事を言うのは変ですが、これからもおつき合いくださいまし。
【法水様】
「もっこり」「もっこり」と叫びながらかくれんぼ、ですか！！さいこーです！！このイメージを抱えたままフランスには行けませ～ん（大爆笑！）

### [1816] amu	2001.07.29 SUN 18:52:05　
	
法水 sun
hanks for help(tanslated the bbs)
"mokori" it translated by meaning(i think so,any other oppnion from chinese fans?)
thanks for beer's encouragement.it was really nice,because i am not sure is ok to leave english message here.
for AH 11...ryo's reaction..."kaori wa..." . we cannot see this reaction on CH.so...AH, yabali, CH2ja nai.
oh~waiting for NO12...


### [1815] ミラクルハート	2001.07.29 SUN 18:31:46　
	
＜もっこりの国際化＞
法水さん、海外での「もっこり」の表現、いろいろ調べてくださってありがとうございます。フランスで「coucou」ですか、なるほど。私としてはどこかの国で「mokkori」と表現していてくれるのを期待していたのですが、さすがのもっこりパワーでも「もっこり」という言葉は海をこえられなかったか・・・。
他にも表現し辛そうな言葉が結構あるような気がするんですが、海外でもやっぱり少年誌なんでしょうか？いろいろ気になる～。

### [1814] Goo Goo Baby	2001.07.29 SUN 17:59:37　
	
＞法水さん［１８１３］
そ、そうだったんすか　　へぇ～・・・・
ｍａｎｇａ－ｋａで通じるのね（汗）
「ジャパニメーション」てのは知ってましたけどねぇ

あ、イタリアってスタジオジブリ作品より、ＣＨやめぞん一刻やうる星とかのほうが人気らしいですよ

### [1813] 法水	2001.07.29 SUN 17:32:26　
	
＞Goo Goo Babyさま。### [1811]
mangaやmanga-kaはすでに国際語です。アニメも正しくはanimationですがanimeで通じますし、日本のものは特に
Japanime［ジャパニメ］と呼ばれ高く評価されています。試しに海外のYahoo!に行ってmangaやmanga-kaで検索してみたら面白いと思いますよ。ちなみにcartoonというのは元々はよく新聞に載っているような１コマの風刺的な漫画のことを言い、普通のストーリーマンガならcomicと言います。

＞まみころさま［1800］。
「coucou」は「かっこう」や「鳩時計」でいいんですよ。あと、「鬼さんこちら（隠れんぼをしている子供の叫び声）」なんていう意味もありました。フランスの子供達は隠れんぼしながら（日本の隠れんぼとはちょっと違うようですが）「もっこり」「もっこり」と叫んでいるわけですね（笑）。ちなみに「coucou」は「クク」と発音します。

### [1812] sweeper（研究会会員募集中。）	2001.07.29 SUN 15:08:49　
	
こんにちは。
北条先生のメッセージ読みました。
そう言われればそうだなと妙に納得してしまいました。

＞### [１８００]まみころ様。
もっこり研究会会員の意見としましては、意外と教授はこのことを知っていたのだと思います。
だから水のみ鳥の頭をもっこりにつけたのだと思いますよ。
でも、もっこりを辞典にのせるとしたらなんて書くんでしょうね。ちょっと気になります。
あとは、もっこりレーダーも動いてますよね。でも、あれは感度よすぎでした。（１００tハンマーとこんぺいとうにて撃沈・・・。）

＞週間ブックレビュー
うちにはＢＳないので、見られなかったです。(T０T)
動いている北条先生見たかったです。

＞テムリン様。はじめまして。
そして、お久しぶりです。しばらく見なかったので寂しかったです。私もあなたのことは覚えています！！大丈夫です！！

それでは、失礼します。


### [1811] Goo Goo Baby	2001.07.29 SUN 14:44:46　
	
［１８０９］の法水さんへ

多分だと思いますが、漫画やアニメってのは英語でｃａｒｔｏｏｎっていうんだと思います　
そして漫画家はｃａｒｔｏｏｎｉｓｔ。　あ、多分ですからね

あ、いえその～、ｍａｎｇａ－ｋａって書いても海外の方々は意味わかってらっしゃるのかな～って思っちゃいまして。

### [1810] 法水	2001.07.29 SUN 13:36:40　
	
Sorry, the message ### [1809] was written by Norimizu...

### [1809] 法水	2001.07.29 SUN 13:30:28　
	
 [Weekly Book Review]
In the latest "Mokkori" message, Mr. Hojo writes that he was reluctant to be taken the documentary in SCOOP 21 because he thinks professionals shouldn't let us know their efforts. So, I don't know if I should write about it or not, but I do for those who couldn't watch the program...
What he said in the last night BS program titled Weekly Book Review was like this... Now, books don't sell well in Japan except manga. But, the benefit publishers got from selling manga doesn't return to manga-ka or magazine. It is strange. So, Mr. Hojo co-founded COAMIX in purpose to use the benefit for creating much better manga. COAMIX prepares the studio and hires assisstants for manga-ka in order to get rid of labors from manga-ka and make manga-ka concentrate on their works. Mr. Hojo didn't say about
AH.
P.S. To those who are in foreign countries: Would you tell us how the word "mokkori" is translated? Some wants to know that.(^^)

［週刊ブックレビュー］
最新の“もっこり”メッセージで北条先生がスクープ２１でのドキュメンタリーは不承不承だったと書かれておりました。プロなら苦労を読み手に知られてはならないからだというのがその理由のようです。なのでそのことについて書くべきかどうか分かりませんが、番組を見られなかった方々のために書きたいと思います。
昨夜のＢＳ「週刊ブックレビュー」でのコメントは以下のようんなものでした。日本の出版界は不況で、その中でマンガだけは売れている。しかし、その利益がマンガ雑誌やマンガ家に還元されずに他への補填に回っている。それはちょっとおかしいのでは。そこで北条先生はコアミックスを共同設立し、マンガで得た利益をすべてよりよいマンガ作りのために使えるようにした。コアミックスはマンガ家のためにスタジオを用意し、アシスタントを雇い、マンガ家の負担を減らしてマンガに専念できるようにした、というようなものでした。ＡＨについてのコメントはありませんでした。
P.S.海外にいらっしゃる皆さんへ。“もっこり”はどのように訳されているのか教えていただけませんか？　知りたがっている人たちがいますので(^^)

[週刊Book Review]  
在最新的 "Mokkori "Message中，胡州先生写道，他不愿意在《SCOOP 21》中被拍摄纪录片，因为他认为专业人士不应该让人知道他们的努力。所以，我不知道我是否应该写这个，但我为那些不能看节目的人写了...  
他在昨晚名为《Weekly Book Review》的BS节目中所说的内容是这样的...... 现在，除了漫画，书在日本卖得并不好。但是，出版商从销售漫画中获得的利益并没有回到漫画家或杂志上。这很奇怪。因此，北条先生共同创立了COAMIX，目的是利用这些利益来创作更好的漫画。COAMIX准备了工作室，并为漫画家聘请了助手，以摆脱漫画家的劳动，使漫画家专注于他们的作品。北条先生并没有说到AH。  
P.S. 给那些在国外的人：你能告诉我们"mokkori"这个词是怎么翻译的吗？有些人想知道这个。(^^)

### [1808] Goo Goo Baby　（凹んでます）	2001.07.29 SUN 12:58:13　
	
今朝飛び起きた・・・　すっごいショックだったもん（大泣）
リョウに首しめられる夢を見てしまった（核爆）
夢にしては苦しかった・・・・
なんであんな夢見たの！！　創刊号読んで香が死んだと知った時よりもショックだった（凹）
心理学者さ～ん！私の心理状態は今どうなってるの？
てゆうかこんなこと書いてよかったのだろうか（大汗）

やっと昨日の放送見れた（汗）
先生ってば似顔絵より若く見えますねぇ　３０代後半ぽい（＾＾）
そして先ほど更新された先生のメッセ！んん～納得！
このメッセこそ右向け左の意味なんですね！

### [1807] James	2001.07.29 SUN 10:23:03　
	
２､３日前に､ここでメルアドを公開してからきたHybrisに続いて､SirCamもどこかから来てしまいました。皆さん､知らない人からきたメール､ましてや.exeや.vbxの拡張子をもつ添付ファイルは絶対にあけないようにしてすぐ削除しましょう。

### [1806] SUN FLOWER	2001.07.29 SUN 10:12:08　
	
玉井様のコメントに私も同感！！
北条先生の作品の数々から大ファンになりましたが、
先生を知るにつれてポリシーや内面もそれ以上に大好きになっていきました。右向け右！で左を向くとこがいいです（笑）。
　Mr。冴羽を生み出した時、”無口なヒーローではなく
ペラペラ話す英雄を誕生させよう！”とおしゃっていたのが
先生の素顔に触れた原点でした。

### [1805] 玉井真矢	2001.07.29 SUN 09:44:39　
	
北条先生のメッセージを読んで北条先生の考えが良く分かりました。
僕は「スクープ21」も「週間ブックレビュー」も観ていませんが逆にその方がいいのかな。と先生のメッセージを読んで思いました。

### [1804] kaori	2001.07.29 SUN 05:57:11　
	
Can someone please write in English what Mr. Hojo said in the tv interview?
Thank you!

### [1803] みみごん	2001.07.29 SUN 01:39:49　
	
### [心臓移植]

ア○ビリバボーの話の人と同じかどうかは忘れましたが、
心臓移植された人の自伝みたいなので、「記憶する心臓」と
いう本がありました。やっぱり、移植後、自分のものでない
好みや記憶がでてきてとまどった、という内容だったと思います。

でも、記憶だけで意志までは持たないですよねぇ...


### [1802] みた	2001.07.29 SUN 01:25:36　
	
はじめまして！！
北条先生のメッセージ更新されてますよぉ♪

### [1801] まみころ(ちょっと連続･･･)	2001.07.29 SUN 01:24:24　
	
そういえば以前アンビＯバボーで、海外で移植手術した患者が、急に味覚が変わったり以前興味のなかった物が好きになったりとかっていう話がありましたよね？
その話では、その移植された側が提供者の家へ行き家族しか知らない事を言い当てたりして、今では家族ぐるみで仲良くしてるとか･･･。
番組的な意見としては、脳が記憶をと思っていたが細胞(心臓)もやはり何らかの記憶が残っているのか？と言う話でした。
という事は、やはり香の心臓を持ったと言う事で、ＧＨも何らかの変化を遂げると言う事になりそうですね。
でも、どうして心臓なのかな？肝臓とかではそういう話聞かないけど、もし細胞的なレベルの話になるのなら心臓以外でもそういう事が起こりそうなのに。
まあ、脳移植は世界的にみても行われてないはずだけど、もしされるような事になったら本当にちょっと怖い･･･。 

