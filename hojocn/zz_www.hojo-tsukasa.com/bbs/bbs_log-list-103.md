https://web.archive.org/web/20011006095537/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=103

### [2060] 金田　朗	2001.08.07 TUE 01:39:29　
	
こんばんわ。
初めての方々、昔からの方々、徐々に増えてきて賑やかでいいですねぇ。私もちょくちょくよらせてもらってます。よろしく。

＜[2058] Goo Goo Babyさま＞
そうですよ、香は何て名前つけるんでしょう？以前誰かのカキコで「１１話の話：香がでてこないのは名前を考えてるから」っての見て笑った！腕君で、あぐらかいて『う～ん』と悩んでいる姿を想像させられた。
でも、どんな名前だろうなぁ。やっぱ、Angel Heartかな？呼びにくい気もするけど、いろんな名前はＣＨで使い切っちゃった気がする、、、。う～ん、思いつかない。


### [2059] ゆなつ	2001.08.07 TUE 01:28:23　
	
こんばんわ。
Sweeper様 Gooちゃん＞ありがとうです！！
あずみちゃん♪＞お帰りなさい♪
初めての皆様＞はじめまして！

明日（今日だ・・・）いよいよ初の合併号ですね！
って事は、に、二週間も読めない・・・。
これはやばい！絶えられるのだろうか？！
ま、そのための総集編かな？
間違い探し（ちょっと違う）（笑）をしていよっと。
まだ、ちゃんと本誌と総集編見比べてないのでね。

### [2058] Goo Goo Baby	2001.08.07 TUE 01:02:51　
	
突然失礼します♪

香がＧＨにつける名前。
それは多分「エンジェル・ハート」（爆）

でないとタイトルの意味が無い！
ＧＨさんよ、コードネームの変更じゃ！

あ、ＧＨさん、香は呼び出せましたか？
前回も言ったけど、あなたが手鏡持ってりゃショーウィンドウの前で叫ぶ必要なくなるよ
テクマクマヤコンをレッツトライ！

ＡＨはパラレルワールドってことにしとこうって人は結構多いんですね

なるほどねぇ、ウチら読者はドラえもんの道具の取り扱いを間違えたからパラレルワールドに迷いこんじまったってわけなのね？（はい？）
うんうんそうだろうなぁ。リョウと香があまりにも素直なトコとかがもうパラレル入ってる！
ＣＨの時は素直じゃなかったのにね

髪だけじゃなく心までストパーをおかけになりましたのね

「香が死んだバージョン（ＡＨ）」に迷い込んだ人がほとんどみたいですね（は？）
じゃ「リョウが死んだバージョン」に迷いこんだ人は結構少ないんだろうなぁ（はい？）

香バージョンはＡＨだけどリョウバージョンは何ていうタイトルなんだろ

ふ、私利私欲のためにドラえもんの道具を使うから誤作動起こしてパラレルワールドなんぞに迷い込むんだよ（なんの私利私欲だよ）

元の世界の５月１５日の創刊日の新聞の一面には「突然消えてしまった北条ファン」とか書いてあるんだろうなぁ（なんだそりゃ）
今頃捜索してんのかしら

だからね「創刊日にそんな記事は載ってなかった」というあなた達や私は今パラレルワールドにいるんだってば

元の世界に戻りたくてもドラえもんのチカラじゃどうにもならんのよ
元に戻るには連載が終わるしかない

私は香バージョンにいるから、連載が終了したらリョウバージョンにいた人の話を聞いてみよっと♪

なんてね♪

### [2057] Ｍａｍｉ	2001.08.07 TUE 00:28:06　
	
皆様はじめまして。
北条先生の作品に出会ってから１８年近くになります。
なかでも小学生の頃から大ファンとなるきっかけの「ＣＨ」。
リョウさんにベタ惚れでしたが、香さんも大好きで、ふたりが幸せになる日をずっと夢見てました。

新連載が始まりリョウさんもでてくるとのこと。「ＣＨ」に決着をつけるとおっしゃる北条先生のお言葉でとても楽しみにしていました。
主婦となり子持ちとなった今なかなか漫画も読めず、本日「総集編①」にて内容を初めて知りました。
感想。ただただショック。これはあくまで「ＡＨ」という漫画だけどリョウや香が出てくるのに、この展開は哀しすぎました。
古くからの友人を亡くした気持ちです。
今後、ＧＨのなかで生き続けるであろう香りはリョウとどうなるのか。どうしてもきょうは「ＣＨ」びいきです。ボロボロ泣いてしまいました。この先を知るうち「ＡＨ」の物語として読める日は来るのかな？皆さんは初めどうでしたか？
私は今日は落ちこみました。

それにしても北条先生はやはり天才ですね。
本当に心に響いてくる作品ばかりです。
これからもますますのご活躍を期待しています。

### [2056] あずみちゃん♪	2001.08.07 TUE 00:26:23　
	
みなさま、こんばんは～。お久しぶりです！何人の方が覚えていてくれるかは分かりませんが、またひょっこりカキコさせて頂きたいと思っていますので宜しくお願いします。（Gooちゃん、ありがとね。）

＞とりあえず、明日のバンチ楽しみです。AH総集編まだ買え　　　　ずじまいで困ってます。

### [2055] hiromi	2001.08.07 TUE 00:04:18　
	
★グラスハート
彼女は何故子供の頃から組織にいたんだろう・・・？？？
どこで生まれたんでしょうね？　李大人の娘ってことはありませんよね！？
★まだ月曜
バンチ水曜組の私は落ち着きません。ＡＨ、どうしてもＧＨよりリョウの方が気になってしまって・・・
★レス
けいちゃん、ありがとうございます♪

### [2054] まどか	  2001.08.06 MON 23:27:56　
	
[２０５０]さえむらさんのコメントを読んで思ったこと。
『ＡＨ』のリョウと香は現在読者の目に映っている通りですが、本来（？）の『ＣＨ』の方のリョウと香たちは今でもハンマーで追いつ追われつの、相変わらずなふたりなのでしょうかね？
うーん。その場合香ちゃんは生きてるけど、恋人ドオシになっている『ＡＨ』の二人の方が、ある意味幸せなような気がする。
私はどうしても香びいきだからな。
リョウファンの人だと『ＣＨ』の二人の方が幸せだと思うのでしょうか。

### [2053] おっさん(岐阜県）	2001.08.06 MON 22:46:47　
	
度々、失礼します。
先ほど変なこと書いちゃいました。「明日の・・」じゃなくて「水曜日・・」です。本当にスミマセンＴ－Ｔ。

### [2052] おっさん(岐阜県）	2001.08.06 MON 22:42:55　
	
毎度！こんばんわ～！
もっぴー様、注文できました？あー良かった。
ありあり様、私もよ～く伸びる海パンだなって思いました。前に海パンはいてて、破れたじゃないですか（優希の話のとき）。今回は破れないようにストレッチのかなりきいた海パンを買ったのじゃないのかな？特にもっこり部分がストレッチきくのを・・・。（ま～た書いちゃったよこのおっさんは＾～＾；）
明日はバンチ発売日です（明日の皆様スミマセン＾～＾；）。
皆様、書店へＧＯです。

### [2051] 美樹	2001.08.06 MON 22:26:26　
	
こんばんは～☆美樹です。
2045のよしきさん！私も91年のアニメムック持ってます！(^○^)
めちゃめちゃいいですよね～!!!!!!あの綴じ込みポスターの冴子がまたヨダレモノで･･･。(＃^｡^＃)
私は当時小学生だったんですが、偶然発売を知ってリアルタイムで買いました！でも、後でなぜ2冊買わなかったのかと後悔したもんです。そうですか、古本屋でも探せばあるんですか♪あれは当時割とすぐに無くなってしまったようだったので･･･。
りょうや香のプロフィールをそらで覚えるほど読み込んでましたよ！

### [2050] さえむら	2001.08.06 MON 22:17:41　
	
パーフェクトガイド、買いました！
ああ、本屋にあってよかったぁ～！！

［Ａ・ＨとＣ・Ｈの時間の流れについて、私的考察］
Ｃ・Ｈの時間だと、香ちゃんは３６才！（槙村兄貴没年が1985年だから）
Ａ・Ｈ時間では、約半分の年月しか経過していないことになりますよね？
おそらくは、香ちゃんが25才くらいで、それなりの結論を出したんだろう、と思っています。
三年くらいは、幸せだったんだろうな、と.....。
Ｃ・Ｈ原作後一年しか幸せじゃなかったなんて、悲しすぎるし。

ところで。
私としては二度目のキスも気になるなぁ。
二度目のキスも、あんな感じなのか？

テレずに、近付けるようになるには、時間がかかったろうなぁ.....。

### [2049] もっぴー	2001.08.06 MON 22:12:19　
	
[パーフェクトガイドブック]
おっさん様、法水様、アリガトウございます。

早速、注文します。詳しく書いて頂いたから、注文しやすいです。

[2話の抱き合ってるシーン]
hiromi様、葵様、sweeper様、ニーナ様、お返事アリガトウございます。

えーっと、私がなぜ二人がキスしてるのかと思ったかというとですね、エンジェルハートでの二人は昔と比べて凄いラブ・ラブ・ラブで、
リョウちゃんも自分の気持ちを包み隠さずに、香ちゃんに接しているような気がして、チューも、自然とできる二人になったのかな？と思って。（昔だったら絶対考えられないけど。）

私の、希望も含まれてますけどね（＾＾）
私もニーナさんみたいに、考えすぎかなー？
でも、香ちゃんが泣いて、あんなに照れも無く自然に抱きしめちゃうリョウちゃんて、変わった。スッゴク幸せだったんだわ！
あのときのリョウちゃんて、辛い過去を全部忘れたように見えますよね！香の深い深い愛が、リョウちゃんの心の傷を癒したんでしょうな！

＞sweeper様
こんばんわ。
8話のキスって、初キスのですかね？
あのキスは初めてのキスだから一回目では、ないんですかねー？

＞初めての皆様
よろしくお願いいたします。個々にレスはしていないけど、
ちゃんと、読んでおりますので、また、皆さんで書き込みましょう！

### [2048] 葵（古本屋と私…）	2001.08.06 MON 21:53:54　
	
単行本にオマケ…。考えただけでうっとりです♪
なんたって古本屋に出まわっている北条作品を見ると、悲しくって…。くそ～っ！キミを捨てたのは誰だぁ～っ☆と、コミックに語りかけてしまう私…（恥）

　でも、先日２０周年短編集を買っちゃいました。（古本屋で☆）コミックで持ってるしぃ～と思ったんだけど、表紙の美しさと装丁のキレイさに、レジに持ってっちゃいました♪やっぱり何冊あってもイイもんですねぇ。そう考えると、古本屋もイイもんかなぁ…なんて思ったり？（笑）

　明日は「バンチ｣だっ！朝イチで、コンビ二に出動！！

### [2047] ありあり（夏日がこない・・）	2001.08.06 MON 20:53:15　
	
単行本におまけをつける、か。いいですねー。
でもそしたらステッカー使う用と永久保存用と２冊かわなきゃ～だめになっちゃう。いいけど♪
【茶々さま】
多分即死ではないでしょう。
っていうかもっと辛い状況だと勝手に妄想しております。
「脳死確定後心臓摘出」とあるので、間違いなく生きていた、と。きっとGHの夢のように始めは意識もあって、りょうの腕の中で意識を失って、最終的に脳死に至った、という事ではないでしょうか。と、ゆー事は脳死後ドナーとなることを最終的にOKしたのはりょう、ということに・・・。これはもう絶対に辛い。未だに「愛する者を守れなかった・・」と自分を責めているのも納得できます。私はこの事を想像するとホント泣けてきます（ToT)（っていうか恐ろしい想像力だな～）
【フィンキール】
あれを見て最初に思った事は「よーくのびる海パンだこと・・（笑）」はっはずかしい・・。
あと、行間はなるべく詰めていただいた方が良いかと思いますが・・。どうなんでしょう。

### [2046] ニーナ	2001.08.06 MON 20:43:51　
	
＞もっぴー様

はじめまして。　
回想シーンのシルエット、私もＫｉｓｓしてると思いました。
友達にそれを言ったら、考えすぎ～って言われましたが・・・（笑）
やっぱり、そうですよね～！！　同意見の方がいて、うれしいです。　何度見ても、切ないシーンですね。カオリには幸せになって欲しかったけど、それで、充分幸せだったのかもしれませんね。


＞パーフェクトガイドブック
　
　持ってます。先週、本屋で、見つけました。初め、なにかわからず、購入しました。読んでみたら・・・　去年、出でいたなんて、知らなかった・・・　すごく懐かしくて、うれしかったです。

### [2045] よしき（彩の国の狭山市）	2001.08.06 MON 20:24:05　
	
「総集編」で思ったこと。
ただ連載したものをそのまま載っけたものではなく、感想やイラストの投稿のコーナーや、ＣＨを詳しく知らない方のためのＣＨの説明（過去のあらすじなんか）のコーナーなどを設けてはどうでしょうか？ここらへんはパーフェクトガイドブックとダブっちゃうかもしれないですが。クイズとかもいいですね♪
……などと某社の総集編を読んで思った。（爆）

＞パーフェクトガイド
皆さんの言ってるのって原作版のですよね？
私も持ってますが、先日古本屋でアニメ版のパーフェクトガイドを見つけてソッコーで買いました♪
A4版位の１９９１年（かな？）の集英社から出たムックですが、かなり鼻血もので狂喜しました。古本屋に売ってくれた方に超感謝です。（笑）機会があったら探してみることをお薦めしますよー！北条先生のセル画が素敵すぎです！（涙）

明日はバンチの発売日ですね！あはは、楽しみ～♪

### [2044] メガロック	2001.08.06 MON 19:43:01　
	
１０月にＡＨの単行本が発売されるようですが
何か他社の単行本とは違う工夫がほしいですね。
そこで初回限定版を出してははいかがでしょうか？
未発表のイラストとか、ＡＨステッカー、ＡＨタトゥ
シール付きなどなど・・・
古本屋にはなかなか出回らないようにしてほしい。
先生が汗水たらして一生懸命描いた漫画が古本屋に
出回るのはファンとして何かやるせないですね。
まあ古本屋がまったくダメとはいいませんが、今の
制度だと出版社側に利益が入らないみたいなのでそこ
をなんとかしてほしいものです。
　
　明日のバンチ、楽しみですね！

### [2043] sweeper（３５７）	2001.08.06 MON 16:30:22　
	
こんにちは。
ログが１００をこえたので驚いてます。

＞chikka様。### [２０３１]
はじめまして。
レス、ありがとうございます。私は「北条時宗」を見たことがないのでわからないのですが、１２話の「昨日の・・・車炎上事件のことか？」の下のコマのりょうの表情が原作ＣＨ文庫版１７巻でミックがりょうに言っていた「ダーク･アイ」に見えたのですが。

＞もっぴー様。### [２０２６]
２話でりょうが香を抱きしめているシーン。
キス説ですかー。あの２人だったらあるかも。
もし、そこでキスをしていたとしたら８話のキスは２度目ということですかね？
キスの味は煙草の味とかすかな硝煙の匂いがしたんでしょうね。

特に、２話を読んでいて回想シーンのりょうが香を抱きしめているシーンの次のページがなかなか開けません・・・。
りょうがタキシードの前で頭を抱え込むシーンと香が交通事故にあうシーンがショックすぎて・・・。

展開、すごーく気になります。
李大人を狙撃したのは誰だー!
私は、張さんがあやしいと思ってます。

おそるべし、フィンキール！おそるべし、もっこり！

それでは。失礼します。

### [2042] X.Y.Z	2001.08.06 MON 15:24:49　
	

はじめまして！
ＲＯＭ専門で皆さんのカキコを楽しく拝見させて
もらってましたが、ついに我慢しきれなくなり私もBBSﾃﾞﾋﾞｭｰ!!
ﾊﾟｰﾌｪｸﾄｶﾞｲﾄﾞ私も持ってますぅ｡｡｡^^;

ＡＨ今後の展開がほんと楽しみです。
明日が待ち遠しい！！
仕事の合間にこんなことやってて、いけない私！
これからもよろしくお願いしまーす。(^^)y
　


### [2041] タウンハンター（仙台七夕真っ最中）	2001.08.06 MON 12:39:25　
	
【パーフェクトガイドを見て】  
なんかこの話で盛り上がってるみたいですが、
先日この本読んで今まで気が付かなかった事が
あってすごい笑ってしまった！！
リョウの読んでいた新聞紙の名前が「横田間珍報」
だったのだ！わはははは！（っていいのか？これ？）
気づいていた方います？北条先生のセンスにはいつも
頭が下がりますよ本当に！・・・失礼しました～
他には「大東亜新報」なんてのもありました。他にも
面白いものがあれば教えてくださいねm(__)m

【看完美指南】  
好像因为这个话题很热烈，不过，前几天读了这本书到现在为止没有注意到的事厉害地笑了! !
獠看的报纸的名字是《横田间珍报》!哇哈哈哈!(这样真的好吗?这个?)
有人注意到了吗?北条老师的品味总是让人佩服真的!…失礼了~
还有《大东亚新报》。如果还有其他有趣的东西，请告诉我哦。m(__)m