https://web.archive.org/web/20031027173306/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=570

### [11400] まろ	2003.08.15 FRI 13:22:01　
	
度々スミマセン（汗）

先の投稿で、chikkaさまのスペルをミスしておりました。
大変失礼しました。

では、また何時かお会いできるその日まで、あでおーす！

### [11399] まろ	2003.08.15 FRI 13:14:35　
	
皆さま、こんにちは！
いやぁ、一夜開けてこっちは大雨でひどいですが、
（一部で下水が溢れ、道路が冠水してます）
チキューの裏側のNYなどでは、原因不明の大規模停電だそうです。
８時間経った今も、漆黒の闇だそうです（ママきょわいっ・汗）

さらに追い打ちなのですが、公式にいらっしゃる、
Windows2000およびXPのをユーザーを震撼させる、
脅威の新型ウィルス（※１）が出現、猛威を振るい始めている事です。

※１…このウィルス、ネットに接続しているだけで侵入してきます。
　感染後の症例としては、
　①Office製品は起動不能になり、
　②慢性的に処理速度が悪化、
　③しまいには強制終了する
　という、深刻な事態に至るそうです。
しかも、現状ではこのウィルスを防御・駆除する術がありません。
くれぐれも、お気をつけ下さい。

さて、ちょこっとレスを（笑）

＞『元祖』やさぐれ媽媽さま（笑）
あのねぇ…。やさぐれ番長はないざましょ？

＞千葉のOBA3さま
わたしもシロートでヘタっぴですが、北条センセの作品への愛は
誰にも負けない自信があります！
（なーんて事言って、皆さまを敵に回すよーなつもりはありませんが・汗）
愛する作品ゆえに、安易な考えや、妥協で、
「これが総天然色版」として欲しくはないだけなんです。
遠近感や、スピード感、キャラの心理に至るまでを、
きっちり表現して貰いたいだけなんですね。
CHカラー化については、千葉のOBA3さまの仰る通りです。

＞chikaさま
ええ、覚えておりますとも！（笑）
ここ５か月間はロムっていただけの事です（微笑）
（ホントは、もう少し沈黙するはずでしたが・苦笑）
今年度の教師としてのお仕事、頑張って下さいね。

＞いもさま
でも、これをもって、また数カ月間・ロムの旅（笑）に出る予定です。

＞sophiaさま
お久しぶりです。私も、つい先日より、一瞬だけカムバックしました（笑）

バンチのない金曜は、どーにも落ち着かない、まろでした。
それでは失礼します。

### [11398] 千葉のOBA3	2003.08.15 FRI 07:54:24　
	
おはようございます。
久しぶりのカキコです。でも、昨日も今日も大雨・・・・。
子供と遊ぶ予定だったのに・・・・。

１１３９６　　無言のパンダさま　　そうでーす！私はふじっちょさまにパンダ状日焼けになることを、お勧めしたいのです！！そうすれば、ふじっちょさまも四川省の住人に！！

１１３９５　ｓｏｐｈｉａさま　　おひさしぶりです！私ｓｏｐｈｉａさまは知ってるけど、｢癒しの乙女」は知らないぞーーー！！

１１３９４　ｙｏｓｉｋｏさま　　こんにちわーーー！台風で流されず元気してましたかぁーーー！？母上様にふりかけを、鼻の穴に押し込まれるなんて・・・夢とはいえ、すごいねーーー！ｙｏｓｉｋｏワールドを、感じました☆

１１３９２　　ふじっちょさま　　この夏、夜も寝られないほどの日焼けしたのって、貴重な経験かも・・・・。今日はホントに寒いんですけど！！

１１３９３　　将人さま　　元気になられて良かったです！またバンバンとカキコしてくださいね！

１１３９１　　圭之助さま　　そういえば、コロッと忘れてたけど、カード届きましたぁーーー？

１１３８１　　ぽぽさま　　アシャンはこれからもドンドン、綺麗になって行くんでしょうねーーー！楽しみですね☆

１１３８０　ＣＯＯさま　　そうですね。ＡＨでは、りょうちゃんと香ちゃんは、早いうちからラブラブだったかも・・・。りょうちゃんの方が先に惚れてたみたいだし・・・。ＡＨでの香ちゃんのＣＨぶりも、見てみたいですよねー。

１１３７８　いもさま　　祝☆復活！！

今日ってホントならバン金なのにねーーー！しかし、幸い忙しさに紛れて、あっという間に一週間過ぎそうです・・・。　


### [11397] ふじっちょ	2003.08.14 THU 23:13:40　
	
こんばんわ～ん（￣ー￣ ）
パンダさまが言うように今日は涼しすぎ…ていうか寒い（￣□￣；）！？明日の朝はありえない事になるそうな…夏だよね今って…ねえ？

＞[11393] 将人（元・魔神）さま
お帰りなさい将人さま(^_^)自分のペースで書きこしてくださいね(^o^)丿皆さんあなたの復活を見守っていますから。

＞[11394] FCハンターyosikoさま
＞「タコ公園」情報、ありがとうm(＿＿)m
え？知ってどうするつもりなんですかー(゜o゜)！？
タコ公園だけど海坊主はいないんだよ～(-_-;)
お金を貯めて歌舞伎町に歩きに行こうとしてたんですか（￣□￣；）！！チャレンジャーですな～(笑)まあリョウの住む町の空気を感じたい気持ちは分かりますけど…

＞[11395] sophiaさま
はじめまして☆ふじっちょと申します(^_^)３ヶ月前から書き込みに来て色々な人にイヂられてます（特に白黒の人…はう(＃▼□▼)＝○＜(T_T)＞…また怒られる？笑）これからも宜しくお願いしますm(__)m

＞[11396] 無言のパンダさま（グラサン標準装備）
誰がそんな早く寝る良い子ですか？(笑)朝の７：３０は睡眠時間てのは万国共通よ！！＼(-□-;)（爆）
怪しいビデオなんてないよ(゜o゜)！！ディ＠ニーとかド＠えもんの夢とかあるのばっかしだから（￣□￣；）！！（直していた所が悪くて全部ダメになったけど…）

この間から見てる録り溜めしてたCHのビデオ自分で言うのもなんやけど、最悪…(-_-;)途中切れ当たり前、最悪なのはオチが切れてるの…あ～DVDが早く欲しい…(T_T)

### [11396] 無言のパンダ	2003.08.14 THU 21:41:53　
	
こんばんわ★

今日の涼しさは何？！ウレシイようなコワイような・・・Σ(▼□▼ﾒ)

＞ｙｏｓｉｋｏさま（３８８・３９４）
オヒサ～♪ｙｏｓｉｋｏ菌入りのヨーグルトをとるか、集中力のないアホパンダのままいるか・・・考え中・・・え？３万円？！Σ(▼□▼メ）
ＦＣにｙｏｓｉｋｏさま、ハマること必至よ♪覚悟してごらんあそばせ！（ｙｏｓｉｋｏ菌の補充も必至！？）

＞Ｂｌｕｅ　Ｃａｔさま（３９０）
おおっ！指は痛かったけど、人のやさしさに触れることができてよかったですね(σ_-)
私の場合、親指の付け根を切っちゃったんだけど、いったいどうやったらそんなとこ切るのかフ・シ・ギ♪(o^-')b

＞圭之助さま（３９１）
夕べは短時間だけど、ようやく夜のうちに眠ることができました☆でもヘンな時間に目が覚めて・・・~(▼▼#)~~
「うたた寝」してる時は、とっても安らかな顔で寝ているようなので、そのへんが原因か？！（*￣▽￣*）ゞ
圭之助さまもイヤホンなしで安眠できますように。。。
※ｋｉｎｓｈｉさま経由で「暑中見舞い」の別バージョンを見せてもらったよ♪ｖ（▼∇▼）ニャッ

＞もしや今ごろオネム？のふじっちょさま（３９２）
あれぇ～？なんだぁ～！ムチで打たせてもくれないし、皮の一枚も引っぺがさしてくれないってわけ～？ケチッ！！(#▽皿▽）＝3 ではビデオはアヤシイものまで根こそぎいただきますっ！（・。－☆
※千葉のＯＢＡ３さまへのレスを見て初めてわかったことが！あれってパンダ状の日焼けってことだったの？！Σ(▼□▼ﾒ)マダムったらタダモノじゃないね。その発想。。。！

＞将人さま（３９３）
とりあえず帰って来られて嬉しいです！
これからも自分のペースでカキコしてくださいね♪(^^)v

＞ｓｏｐｈｉａさま（３９５）
ひさしぶりのソッピ―ふっか～っつ！！（だから雨だったのか？）
すっごくウレシイけど「癒しの乙女」はかなりのヒンシュクモノだぞ！おまけにパンダ＆アザラシへの毒舌はどうーよ！？(#▽皿▽）＝3 アザラシはともかく＼（▼▼ﾒ）パンダさんは本当に優しくてステキだろ？だろ？！だろーっ！！

ではまた。(^_^)/~

### [11395] sophia	2003.08.14 THU 11:22:08　
	
ややっ！！
こんにちわ～♪
はじめまして。癒しの乙女「sophia」です（汗）
いや～めっちゃ久しぶりだぁぁぁぁぁ。。。

将人様からメールをいただきましたが、しばらくＰＣにも触ってなかった私は「？？？」だったのでここにやってきました。
・・・・・でもログ多すぎで頭がパンクしそう（＠＠）
全部はとてもじゃないけど読めません（ＴＴ）

＞将人様
メール見ました。突然だったのでびっくり！
皆様もいわれていますがオークションに気兼ねなんていりませんよ～。欲しいものは欲しい！！それでいいんです。
相変わらず将人様はやさしいですね（＾＾）
どこかのパンダ様やアザラシ様にも見習って欲しいもんだ・・・・なんていったら殺されるな（ーー；）
（本当は優しくてステキなオネーサマ方ですよ：笑）
誰も将人様に迷惑なんてかけられてないし、逆に物知りな将人様に教えられてるばっかりで感謝してますよ♪
大阪は今、１８年ぶりの優勝（？）に沸いているはず！！
将人様もパァ～～～っとはじけましょう！！
これからもよろしくです　＼（＾＾）／

あぁ・・・外は雨　∥∥∥
洗濯物がぁぁぁぁ・・・・・（ＴＴ）
、ということで久しぶりのsophiaでした♪
皆様よい１日を。

### [11394] yosiko	2003.08.14 THU 02:51:47　
	
こんばんわ♪
朝に引続き、再登場のyosikoです（ですぎ？？）

＞将人サマ
メール送らせていただきました♪

＞はじめましての方々♪
あいさつ遅くなりました・・・yosikoです♪よろしくおねがいします(^-^)b

＞Blue Catサマ
Blue Catサマは西ですな・・・yosikoは東なんですよ～～Blue Catサマのメアドさえわかれば住所をドーン！！と送りつける（？）つもりでしたが・・え？いらないって？(ToT)

＞Ｆっちょクンくん
「タコ公園」情報、ありがとうm(＿＿)m
リョウちゃんが来て遊んでくれるかしら・・・なんちて。

そういえば、高校生の頃、大学に入ったらアルバイトをめちゃくちゃして、リョウが辿ったコースを歩いて見たい！・・って考えた記憶があります。・・・想像すると、いろんな意味でキケンなコースのようですね・・・(^^;　だからこそリョウがいるんでしょうが、あそこに。

「ＦＣ」、手に入りそう！！ある本屋に電話したら、在庫ありとのこと！明日買いに行ってきます♪楽しみ～～(^-^)

### [11393] 将人（元・魔神）	2003.08.14 THU 01:37:01　
	
こんばんは、将人（元・魔神）です。
皆様、ごめんなさい。今まで鬱病(うつ病)だったのに黙っていて
だましてばかりでした。すみません。
なんか頭の中で被害妄想ばかりで皆さんの事を悪い風に考えられなくて
申し訳ありません。
メールや掲示板で、皆さんのあたたかい、はげましの言葉、ありがとう
ございます。嬉しかったです。
その割には、返事をするのが遅かったですけどね。すみません。

また後で、皆様へ個別にレスしますが、とりあえず皆様に
謝っておきたかったです。
ご迷惑をお掛けしました。申し訳ございません。
すみません。

### [11392] ふじっちょ	2003.08.13 WED 19:03:47　
	
皆様こんばんわ☆ふじっちょです(^o^)丿
お盆ですね～これといってする事も無いので暇してます（爆）
前回[11382]のカキコではショックのあまり皆さまへのレスが出来ませんでした(笑)ウソです(^_^)ちょっと用事があったもので…(^_^;)＞それでは、たまったレスです。

＞[11377] 千葉のOBA3さま
いやだ～！！＜(T△T)＞それだけはカンベンを～～～（￣□￣；）！！あっ…四川からお迎えが…（爆）

＞[11378] いもさま
はじめましてm(__)mふじっちょと申します。３か月前より頻繁にここに来ては色々な人にイヂラレテます(笑)ふつつかものですが（？）宜しくお願いします(^o^)丿

＞[11380] COOさま
ぼくも２・３日上半身は完全に発汗してませんでした。今はおびただしく向ける皮に驚いたり…(笑)

＞[11383] 無言のハードラック☆パンダさま
じゃあリベンジ成功させるために、その日やさしく知らせて下さいよ(笑)CHのビデオラベルがいい加減だからどれだか分かるかな～？（ウソ）その前にキャッツカードをお忘れなく（爆）

＞[11385] chikkaさま
７：３０なんて眠りはじめる時間じゃあないですか(゜o゜)（爆）そのCMの為だけにビデオを録るのもおかしいし…(笑)
でも情報は感謝です(^o^)丿

＞[11388] 大人のふりかけyosiko味さま
♪お久しぶり～ね♪もう十日も経つのねん＼(-o-)／
なかなか壮絶な夢を見られたようで…^m^プッ
F菌はやめれ！！<(｀^´)>！！
＞「イカ公園」に「タコ公園」・・・興味深い（？）
「タコ公園」は大阪ですよ奈良には無いはず…(-_-;)「みはらし」公園を「みたらし」公園って言う奴はいるけど…（その一人　爆 ）

＞[11390] Ｂｌｕｅ　Ｃａｔさま
せっかく情報頂いたのになんたる不始末…／(T_T)＼あ～許しておくんなまし～（誰？）次ぎは僕も報告出来る様になります！！

### [11391] 圭之助（けーの）	2003.08.13 WED 13:08:14　
	
お久しぶりです。前のカキコはどこだっけ？何書いたっけ？
ついていけん…(;^_^Ａ

＞11390 Ｂｌｕｅ　Ｃａｔさま
私もビデオに撮ってみました♪番組内では、ほんの１コント分・数分モノなんですけどね。ケンシロウの「あたたた～」「うぉちゃぁ！」「西野社員、お前はもう食べている」といセリフが聴けましたヨ。あと番組最後のテロップにトップで「神谷明」とお名前が流れたので何か嬉しかったデス(^-^*)
あれだけじゃ、チョット淋しいなぁーと思ってたんですけど…また顔出し出演あるんですね☆楽しみです！

＞kinshiさま
お手紙着いたよぉ、ありがとね♪

＞無言のパンダさま
不眠は少しはよくなりましたか？圭之助はここ数日、またもやイヤホン生活なので耳が不安です…。

さぁ、昼飯でも食おう…食欲ないんだけどハラは減る…σ(+_+;)

### [11390] Ｂｌｕｅ　Ｃａｔ	2003.08.13 WED 11:47:14　
	
　こんにちは～です。

＞[11381] ぽぽ さま
　いえいえ、わたしのカキコミが、放送に間に合ってよかったです。コント、面白そうですね～、いいなぁ。まだ、声だけじゃなく顔出しでの出演もあるらしいですので、また見られたら感想報告お願いしま～す☆

＞[11388] yosiko さま
　三河国・・・のいったいどこなんでしょ（わたしは西尾ですけど）、聞いちゃいけないのかな。まさかyosikoさまがこんなに近くの出身だったとは・・・これって喜んでいいのかな？（笑）
　あと、タイピングタッチでなくタッチタイピングだと思うです・・。

＞[11383] 無言のパンダ さま
　わたしも本屋さんで指、切ったことあります・・Ｆ．ＣＯＭＰＯを読むためのオールマンを取ったとき、表紙で切っちゃったんですよぅ。とりあえず目の前のデパートのトイレで手を洗ってトイレットペーパーで血を止めようとしてたら、たまたま入ってきた見知らぬおねーさんがばんそうこうをくれたので、それを貼ってなんとか止めたんですけど・・・。あのときのおねーさん（読んでないだろうけど）本当にありがとうございました（ぺこり）。

### [11389] ゆき	2003.08.13 WED 09:57:43　
	
こんにちは、ゆきです。だいぶ間隔があいてしまいましたが、
＞［１１３１７］ぽぽ様
　本当に事実は小説より奇なり、ですね。正直な所、実際にそういう事があるとは信じていなくて、西洋では長らく感情は心臓に宿ると考えられていたそうなので、その影響かと思っていました。
　そういえば先にあげた小説でも、恋に落ちた相手とは別に、やはりドナーの可能性のある女性の恋人が、心臓だけでも生きていたのでは天国に行けない－つまり日本式に言えば成仏できない－従って後を追っても一緒にはなれないからと（特定できなかったので）移植された可能性のある人間を次々と殺していくのですが、一理あるかもしれない。（だからといって勿論、その行動を肯定する気はありませんが）
　ただ一巻目を読み直してみると、移植手術を受けたのは一年前、発作が起きてからは数ヶ月という事で、少なくとも半年はあいてるんですよね（辞書で調べたら、例として数行は二、三行、もしくは五、六行とありました）。案外、グラス・ハートが無意識にあげていた心の悲鳴が香を呼び覚ましたのかも、などと考えたりもしています。
　融合するかどうかは別として、最後はやはり消えてしまうのかな。正直、心のどこかでしつこく生き返って欲しいと望んだりもしていますが、難しいでしょうし。
　なんだか妙に長くなってしまいましたが、ではまた。

### [11388] yosiko	2003.08.13 WED 07:16:20　
	
おはようございます♪母親に、鼻の穴へフリカケを押し込まれた夢を見て、目覚めてから一生懸命に鼻をかんだyosikoです☆←久々なのにキタナイ（汗）ラストカキコから１０日ほどしか経ってないのに、ワタシのカキコはいずこっ・・て感じでした。盛んですね、ココは♪

【遅ればせながら今週の感想】
なんか、カラーで見ると、リョウちゃんの水着、紫だったんだ・・てとこに、衝撃を受けました。もっこりが登場した瞬間、サメ・・？と思いましたし（汗）・・本編の感想は、皆様のカキコとほぼ同様です。殺人マシンのように生きてきたアシャンの自己は未だ不安定で、香の自己との融合によってアシャン＝香ではなく、アシャンそのものになる、っていうのがなんか素敵♪（何かいてるか分からないかもm(＿＿)m

＞Blue Catサマ
ワタクシも三河国なのだす♪yosiko菌、蔓延地帯でしょう（笑）

＞Rouarkサマ
タイピングタッチですか(*.*)情報、ありがとうございます♪

＞無言のパンダ姫
・・・ＰＣ崩壊してましたよ・・実家の。実家に帰ったら、電気屋さんが奮闘してました。集中力の増す、yosiko菌入りヨーグルト（３万円）いかがですか～～（自分で書いてて気持悪くなってしまった）

＞みろりサマ
あんよの方は順調ですか？？過去ログ見ていて気がついたんですが、ワタシ、どうやら、足が象さん化、とうのを見て、象の鼻化をイメージしていた模様。

＞Ｆ菌クン
「イカ公園」に「タコ公園」・・・興味深い（？）

＞千葉のＯＢＡ３サンマ
ココで「去年までと比較して涼しいねえ」と話題に上ったばかりなのに、あれからスグ暑くなりましたね・・・

夏休みになったら「ＦＣ」購入計画・・実行☆☆しかあし！！実家の近くの本屋１５件まわったのに、１巻がどうしても見つからぬ！！一巻がないと読まない主義なので、内容いまだ分からず。
ひぐっひぐっ・・・(ToT)
ではまた(^^)v

### [11387] 無言のパンダ	2003.08.13 WED 01:34:49　
	
こんばんわ★

さすがにお盆だから出かけられてる方が多いんでしょうか～？何の予定もない私にとってはここに来るのが楽しみなのにサミシイなぁ～(σ_-)

子パンダが毎月買っている某アニメ雑誌の今月号に載ってた「コミックス売上ベスト１０」にＡＨの７巻が初めて９位に入っていた！ｖ(≧∇≦)ｖ このアニメ雑誌の中でのベスト１０に登場したのは初めてだよ♪

＞ｃｈｉｋｋａさま（３８５）
初恋かぁ～いいですね～☆（遠い目・・）
不器用ゆえの純愛。。輝いた二人の季節かぁ。。やさぐれパンダにはそんな美しい思い出はないけどサ・・・(ﾒ▼▼)y-.｡o○ ﾌｰｰ

＞キャロラインｚさま（３８６）
夏目さんのこと、少しは見直す気になってくれました？(^_^;)
避暑地に行かれるのですか～？！なんとうらやましいっ！
スペキュタクラービ～ム！！(/★_★)/…━━━━━☆)ﾟoﾟ)

ではまた。(^_^)/~

小熊猫每个月都买的某动漫杂志的本月号刊登了“漫画销量前10名”，AH的第7卷第一次进入了第9名!v(≧∇≤)v这是第一次出现在动画杂志的前10名哟♪

### [11386] キャロラインｚ	2003.08.12 TUE 12:42:03　
	
＞[11379] COOさんの感想の
"アシャンの涙"についてですが、これは最後に夏目さんに会うことができ再会の約束ができたうれしさと別れの寂しさが入り混じった感情からくる涙かと思います。
あと香の感情も入っているかとも思います。

と一緒です。香さん重視ではないからかな？純粋にそう感じました。まぁ、遠距離恋愛経験者だったり、
約束は守ろうと努力しているから失恋とは思わなかったな。
阿香モテモテやの～夏目よく頑張った！よく言った！
不器用だが、いい人に思われてよかったね！！でちた。

過去ログよみました！なかなか笑えます。嬉しい～
詳しいことはまた書き込みさせていただきますね。
では、いざ！避暑地にいってきます。



### [11385] chikka(Eastern Shizuoka)	2003.08.11 MON 23:45:51　
	
どうも今晩は。chikkaです。お盆の夜いかがお過ごしですか？暑いのも昨日までで、今日からはまた天気が崩れるそうです。８月なのに台風がきたり、また天気が悪くなったりまるで秋の空模様です。東北地方は結局梅雨明け宣言がなかったそうです。何か１０年前のあの冷夏を思い出します。甲子園のほうは静岡県代表の静岡高校が登場します。

【今週の感想】
「初恋は破れるから初恋だ。」と私はよく生徒達に言っています。初恋は淡く切ないもの。だから破れて当然のものだと言うことです。香瑩ちゃんにとって夏目さんとの出会いは大人になるためのステップで、きっと後で振り返ったときいい思い出になると思います。お互いに口では再会を約束していましたが、たぶん、夏目さんと彼女は２度と会わないと思います。冴羽さんと皆川由貴ちゃんの時みたいに・・・。
今回のテーマである「初恋」と言えば、今は亡き村下孝蔵さんを思い出しますが、むしろここではＴＵＢＥのアルバム「Ｓｏｕｌ Ｓｕｒｆｉｎ’ Ｃｒｅｗ」（２００１年）に収録されている「初恋」の方がぴったしかもしれません。歌詞を全部書く事は著作権の関係上できませんので、一部を紹介します。（削除覚悟です！！）

願い事がもしも叶うとしたら、あの時に言えなかった・・・。
初恋よ 短くも激しく 不器用ゆえの純愛
誰よりも輝いた二人の季節
青き春の日に Ｓａｙ ｇｏｏｄ－ｂｙｅ

香さん、夏目さん、香瑩ちゃんの３人にとってぴったしのフレーズのように感じたのは私だけでしょうか。

【１１３７３】まろ様、【１１３７８】いも様、お久しぶりです。chikkaです。覚えていますか？私自身変わったことと言えば来年の３月いっぱいで教師を辞めて、福祉関係の職業に転職することです。

【１１３８１】ぽぽ様、【１１３８２】ふじっちょ様
神谷明さんでしたら、ここ数年は毎週日曜日の朝７時３０分からやっている「戦隊ヒーローシリーズ（今年はアバレンジャー）」で声を聞くことができます。（後楽園遊園地「ラクーア」のＣＭですが・・・。）それから、先週でしたが朝のワイドショーで「海坊主」の声でおなじみの玄田哲章さんが登場しました。玄田さんが声を担当しているアーノルド・シュワルツェネッカー氏の例の話題の時です。

【１１３８１】ぽぽ、【１１３８２】ふじっちょ  
神谷明先生的话，最近几年每周日早上7点30分开始的“英雄战队系列(今年是avanranger)”可以听到他的声音。(这是后乐园游乐园“rakua”的广告…。)另外，上周在早间新闻节目中，因“海坊主”的配音而为人熟悉的玄田哲章登场了。玄田担任配音的阿诺·施瓦辛格内克的那个话题。


### [11384] 無言のパンダ	2003.08.11 MON 22:59:22　
	
あれ？！また時間が大幅に狂ってる！(~_~;)

### [11383] 無言のパンダ	2003.08.11 MON 22:56:46　
	
こんばんわ★

昨日、本屋さんにいる時に「あれ？なんか手が痛いな？」と思ったら、なんと血が！？（少量）本をさわった時に紙で切れたのか？！今日は今日とて買い物に行って外へ出たら雨＆雷がっ！（おかげでずぶ濡れさ♪）というわけで、ちょっぴりアンラッキーなパンダの今日この頃~(▼▼#)~~みなさまはいかがお過ごしですか～？
つまらない挨拶を長々とゴメンナサイ（*￣▽￣*）ゞ

＞美白の女王ふじっちょさま┗（▽皿▽#）┛！！（３７６・３８２）
リベンジ不可能な四川省在住パンダはどうすればいいのさ？！(#▽皿▽）＝３　なんちゃって。次回、見事リベンジ成功の暁には、おおまかでよいので内容の提供ヨロシク～♪ えっ？ダメ？！だったらそのＣＨビデオをいただきに参上するぞ～！もしくは背中の皮を一枚剥ぎに♪それともタコ殴り？
（（○＝（▼▼メ）オリャ！ｏ（▼皿▼メ）○オリャ！ｏ（メ▼▼）＝○）） （ﾟｏ★（☆○（▼ﾍ▼ﾒ）ｏ
どれがいい？（注：冗談ですよ。たぶん・・）

＞千葉のＯＢＡ３さま（３７７）
香瑩はまだまだ成長過程にあるので先のことはわからないとして(^^ゞ別に好きな人が出来た場合は夏目さんのことは「初恋の思い出」として心にしまっておいて将来大切な友人として再会できればいいなと思うんだけど・・・ダメ～？そんなんじゃ(^_^;)

＞いもさま（３７８）
ひさしぶりの公式復活おめでとう♪パンダさんは一日たりとも「いもさま」のことを忘れたことはなかったぞ！（感涙）

＞ＣＯＯさま（３８０）
私は金額的に無理なものほど結末が気になって、つい見届けたくなってしまいます～(^^ゞ

＞ぽぽさま（３８１）
そうそう♪（うなずき返し^_^;）
神谷さんの出演（声）されたという、その番組☆楽しそうですね～(^○^)私も一緒に笑いた～い！(>_<)

※葵さま情報　ポコロンピン・・・♪
解禁に参加できなくて、とっても残念がっておられる様子(~_~;)
「１週間～１０日で復活できるかも」予報が出ております♪

### [11382] ふじっちょ	2003.08.11 MON 15:53:34　
	
＞[11381] ぽぽさま
ぬぅあぁ～！？シマッタ～！！(゜o゜)！！見るの忘れてた～！！！（￣□￣；）！！その時間普通にCHのビデオ見てたし…(-_-;)せっかく情報もらったて言うのに…散々調べて皆さんに時間とかお知らせしたのに…(σ_-) Ｂｌｕｅ　Ｃａｔさませっかく教えていただいたのにスイマセンm(__)m　ぽぽさま大まかな内容教えてくれてありがとうございます<m(__)m>
でも、聞く所によると数回分録り溜めしているって聞いたので次こそリベンジ！！┗（▽皿▽#）┛！！

### [11381] ぽぽ（Hyogo）	2003.08.11 MON 15:06:46　
	
今週のＡＨをまた読んで、夏目さんと阿香の涙、切ないです～。

ふた晩前の深夜に放送された『チョップリン凸劇場』見ました。神谷さんの声の出演、ありましたぁ。全編コントの番組で、起死回生をかけて新商品の開発に取り組む町工場が題材のコントに声の出演をされていました。「電子レンジ」ならぬ「北斗のケンシレンジ」を開発した町工場。詳細は書いていいものか分からないので書きませんが、笑えました。 o(´ワ｀)o
Ｂｌｕｅ　Ｃａｔさま、神谷さんの出演情報ありがとうございました♪ m(_ _)m

シティーハンターVersionはないかなぁ～。
ＣＨだと「ハンターレンジ」、「もっこレンジ」？
『がんばれ日本ソウル五輪　もっこれ日本ソウレもっこり！』
（「もっこレ」つながり ι）

＞[11377] 千葉のOBA3さま
ほんと阿香、きれいになってますよねー。もーかわい～～って！！

＞[11374] 無言のパンダさま
うんうん♪（うなづき）

＞[11366] Ｂｌｕｅ　Ｃａｔさま
スミマセ～ン。私、北条先生の作品に関する新しい情報にはもー、一々めっちゃ一喜一憂するのでＢｌｕｅ　Ｃａｔさまがカキコしてくださった内容に一喜二喜（そんな言葉はない！ ι）したまま[11359]のカキコしてます。嬉しくて気持ちが盛り上がったゆえの言葉足らずもありますので～ f(´v｀;)

＞[11362] 将人（元・魔神）さま
将人さま、気楽にいきましょう。自分の心、大事にしてください。自分をいじめないでください。ダメな人なんていませんよ。自分をダメだなんて思わないで。
