https://web.archive.org/web/20031021105220/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=373

### [7460] 千葉のOBA3	2002.08.20 TUE 11:45:11　
	
こんにちは。心配していた台風も何てことなく過ぎてしまいました。

７４５２　ＳＭＴＰさま　私も今回の過去のお話がおわったら、また最初からＡＨを読み直してみるつもりでいます。きっと今までと違う感想、感動があるんじゃないかと思うんです。たぶん完全にＣＨと分けて考えられるんじゃないかと・・・、たぶん、ですけど。

７４５５　葵さま　「赤毛のア＠」の映画,私も見ましたよ。しかし、いまいち、ギルバート役の俳優さんが・・・。（もうちょっとスリムなほうが・・・。）でも、次の作品も楽しみですね。あの、まわりの風景が素晴らしいですしね。

７４５６　たまさま　タマちゃん荒川で発見という情報もあったようですが、まだ行方不明みたいです。でも野生だから心配いらないんでしょうけどねー。

７４５８　いもさま　そうです。私ポンチョ希望で懸賞だしました。当たるかなーーー？ドキドキ☆なんか、エイッってカンジでポンチョにしたんですが、迷いますよねー。

７４５９　ｙａｅｋｏさま　私もこのお話って何月頃なんだろうって思いました。ひょとしてＡＨでは、りょうちゃんの誕生日は３月２６日じゃないのかな？って・・・。アシャンの夢の中だから、まわりは季節感ないかもしれませんけど、香ちゃんの服装では、そんなに寒くない季節？？？

うーーー、今日は火曜日。バンチ発売までもう少しの辛抱です！

### [7459] yaeko	2002.08.20 TUE 02:12:14　
	
「お盆だぜ！なぜこんなに店が混むんだ～辛いぜサービス業週間」が終わりやっといつもの生活が戻ってきたyaekoです。こんばんは。初めての方よろしくお願いします。

「槇ちゃんは？？？の巻」
ボサボサ頭で眼鏡君でスーツ姿でネクタイは緩めていてコートを着ている。それがCHでの槇ちゃんの特徴。CHの槇ちゃんと眼鏡の形が同じだったりするAHの槇ちゃん。すごくCHの槇ちゃんを壊さないように描かれているみたい。
さて、槇ちゃんといったら確かにコートってイメージはあるけれど、阿香の夢の中のりょうちゃんと香ちゃんの出会いの話ではおかしいような。。。一人だけ厚着すぎないかい？？５歳の阿香は半袖、香ちゃんはブラウス１枚、街の人は半袖の人もいる。槇ちゃんだけスーツ＋コート！？いったい季節はいつなんだ～。春か秋？？？…あ、もしかして槇ちゃんって寒がりなのかな？？？だから厚着？…謎じゃ。

あははは。かなり遅い感想でした。だってさ何回も読んでたら気になっちゃって。皆さまは季節はいつだと思って読んでました？？？
それでは。

### [7458] いも	2002.08.20 TUE 00:18:55　
	
こんばんわ。
わあ～、はじめましての方々のカキコ多いですね。(^^)
いもといいます。よろしくお願いします！

＞千葉のOBA3さま[番号‥？]（汗）
合併号のスペシャルプレゼント、ポンチョにするっておっしゃってたの、確か千葉のOBA3さまでしたよね？？？
私まだ決めてないんですよ～。でもあれだけシリアスな絵のポンチョはかなり貴重ですよね。（笑）　　迷うなあ。

＞kinshiさま[7416]
お誕生日ありがとうございます。（？）
毎年毎年タモさんと一緒に年を重ねられる‥‥幸せです。（ホントか？）
海ちゃんの太い指から作りだされる、繊細なデコレーション期待してますよ！！予約なしでいいんですか？（笑）

＞無言のパンダさま[7445]
お祝いありがとうございます！
「夏の太陽のように」‥‥。素敵なお言葉恐縮です。
でも実際言われるのは「あんた夏生まれだからキャラが暑苦しいのね」です。（泣）
さわやかな女性目指して、精進いたします。

祝ってもらうばっかで、他の方々を祝ってない自分‥。（汗）
全国の８月生まれの皆様、いっぺんにおめでとうございます！（なんじゃそりゃ）
それでは～☆いもでした。

### [7457] OREO	2002.08.19 MON 23:33:29　
	
こんばんわ。実はさっきカキコをしたのに、ＥＳＣを押したとたんに全部消えてしまいました。うわぁ・・・悲しい。

今日はついに誕生日。弟が今日、免許とったので、免許証には私の誕生日が載った面白い免許証が出来上がりました。
で、祝い事がかさなったし、私が前から寿司寿司とうるさかったので、（帰ってきてから寿司食べてないんだもん）港にある、回転すしで食べました。うまかったぁ。

＞７４５６たま様、５５葵様、５２ＳＭＴＰ様、５１千葉のＯＢＡ３様、５０sophia様、４５無言のパンダ様等々・・・お祝いの言葉をくれた皆様（ごめんなさい書ききれません）
ほんとうにありがとうございます。私なんぞの誕生日に、こんなにたくさんの方からお祝いの言葉をかけてもらえるとは思いませんでした。ほんと、嬉しいです♪感涙（Ｔ0Ｔ）

あっ、でもケーキ食べるの忘れた！！カナダじゃ食べれないモンブラン食べる予定だったのに！！絶対あとで食べてやる！！
そろそろ、私の誕生日も終わりです。
次は２３日、いちの様の番ですね♪今日はこれまで See ya!

### [7456] たま	2002.08.19 MON 22:35:27　
	
みなさま、こんばんわ。たまです。
はじめましての方々、わたくし「たま」と申します。どうぞヨロシクです。

＞千葉のOBA3様　【7451】
うん！うん！見た★見た☆ニュースで見ましたよぉ～。
アゴヒゲアザラシの「タマちゃん」(奴ぁ、どうやらカタカナらしい…）
無事、東京湾に逃げたかなぁ～？ちょっと心配だなぁ～（だって動物大好きなんだもんっ）←む○ごろうサンには負けるけど・・・
多摩川のタマ・・・なんだか、他人事とは思えんなっ（笑）

＞sophia様　【7450】
そうでぇ～す♪
わたしと、ｓｏｐｈｉａ様はタメなので～す！

＞凛々様　【7448】はじめましてですね☆よろしくです。
ＣＨサントラＣＤ情報ありがとうございます。（残念ながら私は関西圏ではないのですが・・・）
でも、こうやって、情報を教えてくださった事がうれしいのです。(ﾆｺﾆｺ）
それから、神村幸子さん・・・の事はまったく分かりません。(ごめんね。役立たずで・・・）

ん～。こうカキコが多いと、レスするのも難しいなぁ～。（汗）
なるべく皆様と会話がしたいのですが・・・。(たまさんガンバレっ）

### [7455] 葵	2002.08.19 MON 21:56:05　
	
　ひじについてお見舞いいただいた方々、ありがとうございました。きのうは調子よかったのに、今日は最悪…。やはりよくなったからといってムリしちゃいけませんね。はぁ…しんど☆

＞ＯＲＥＯさま
　お誕生日おめでとうございます♪カナダ特派員、よろしくお願いしますね。（笑）

＞千葉のＯＢＡ３さま［７４５１］
　ミーガン・フォローズ主演の映画「赤毛のア〇」をご存知ですか？その続編の「アンの結婚」が公開されるそうですよ。いまから楽しみです♪ひじに電機はかけてませんが、エアマッサージとジェットバス、それと普通のマッサージをセットししたリハビリをしてもらってます。効果は…五分五分ですね。（^_^;

### [7454] けいちゃん	2002.08.19 MON 18:49:21　
	
ども、こんにちは。何週間ぶりだろうか・・書き込みは。
今、エンジェルハートの紹介ページをみてきたら、少しだけかわってましたねえ。登場人物紹介に信宏くんもくわわってるし。香ちゃんの次にかかれていたので、少し出世したかな？なんて思いましたよ。
お盆にアメリカ方面に行ってました。買い物で入ったＣＤショップには日本のアニメのＤＶＤが並んでいてびっくりしました。あ、でもＣＨあったかは見なかったわ～～～
ライジーンは本屋さんにならぶのかなあ？日本の文化ってすごいと感じたけいちゃんでした。

### [7453] りえ	2002.08.19 MON 16:34:38　
	
>Ｂｌｕｅ　Ｃａｔさま
こんにちわ。
ほんとですか？？？いいな～。
近くのビデオ屋さんはＣ・Ｈのビデオもすくないし・・・。
前あったＣ・Ｈみたいに夜中に再放送があればいいのになぁ。
Ｂｌｕｅ　Ｃａｔさんはどこに住んでるんですか？

### [7452] ＳＭＴＰ	2002.08.19 MON 16:28:20　
	
にゃは☆（壊？…壊！）どうもこんにちはＳＭＴＰです。
はやくＡＨを１話から読みたいよ～と思っている今日この頃であります。

今日はＯＲＥＯ様の誕生日ですね。おめでとうございます！

＞kinshi様[7416]
どうも、こんにちは。ファルコンから誕生ケーキ受け取りましたぁ。ありがとうございます。帰るときのファルコンの後姿かっこよかった！今頃はＯＲＥＯ様のおうちへお届けしている頃でしょうかね？しかし、うちまでどうやってきたのかな…？

＞千葉のOBA3様[7438]
はじめまして。動揺はしていないです。まだ第５５話しか読んでいないのでなんともいえませんが、今のところＡＨとＣＨをうまく分けて考えることができそうです。もしかすると１話から読んでいったら動揺してしまうかもしれませんけど。

＞葵様[7442]
こんにちは。たしか７年ほど前のＣＨ２の再放送（関東）のときはＯＰ曲が長かったように記憶しています。ただ、途中で最悪の事件があって中断された後、再開されたときには短いヴァージョンになってしまいましたが…。
ひじ、お大事に。

すべての方にレスできなくてごめんなさいm(_ _)m

### [7451] 千葉のOBA3	2002.08.19 MON 13:17:08　
	
嵐の前の静けさ・・・。不気味なくらい風もないし、雨もやんでる・・・。

７４４６　ＯＲＥＯさま　ホントにお誕生日おめでとう！！カナダに行かれても、どんどんカキコしてくださいね！

７４４１　いちのさま　１０代最後になにかやらかしたいって・・・。栃木の少年Ａにはならないでくださいよ。

７４４２　葵さま　テニス肘ですか？私もやったんですが、なんか、電気かけて治したんですよ。なんていうのだか、忘れてしまった。（年のせいか・・・、物忘れがひどい。大汗。）でもそれで治ったんですけど・・・。（わー、役にたちませんね、これでは・・・。）

７４４７　たまさま　新宿御苑はいい所ですよ。まさに大都会のオアシスってカンジです。ところで、今台風接近中のこちらでは、多摩川に迷い込んだアザラシの「たまちゃん」がどこかに行ってしまい,行方をマスコミが探しています。そちらでもニュースになってますか？東京湾に逃げないと多摩川は増水して危ないんです。たまちゃんやーい！

７４５０　ｓｏｐｈｉａさま　ありがとうございます。まさに今年の千葉県は台風大当たり。きっとこれから雨風ともにひどくなるんでしょうが、学校が休み中なので、ちょっと気が楽です。（子供の通学を心配しなくていいから・・・。）

それでは、もう今からバンチ発売を指折り数えて待ってます！！

### [7450] sophia	2002.08.19 MON 12:40:26　
	
こんにちわ。
な・・なにっ！
久しぶりに来たらこのカキコの量！！
読めないよ＜＾＾；＞
休みボケの頭ではもう無理ですわ・・・

＞（７４０４）他　いちの様
女性に年齢を聞くとわ・・
そんなことでは今年は（も？）彼女はでき・・・・
じょ・冗談です。冗談ですってば！！
ｔハンマーしまってくださいっ＜＾＾；＞
歳は「たま様」と同じ（でしたよね？）で、『香の享年』です。
「ＡＨ」１巻をもう１度見直しましょう＜＾＾＞
それより（歳の話はやめよ～)いちの様！
とうとう「ＣＨ」￥７０００お買い上げですか？
近ければ誕生日プレゼントとして贈れるのに・・
残念、あ～残念《本当か？》

＞（７４２９）ＯＲＥＯ様
今日がホンとのＨａｐｐｙＢｉｒｔｈｄａｙ！！
とりあえず、これだけです。はい。

最近、新しい方も増えましたようで、
皆様　はじめまして♪＆よろしくお願いします　ｍ＜＿　＿＞ｍ
台風が移動中です。
千葉のＯＢＡ３様他、影響のありそうな処の方々はご注意くださいませ。
それでは。

### [7449] Degel	2002.08.19 MON 01:23:29　
	
皆様、初めまして。
私はDegel（デジェル）と言います。
ずっと拝見しているだけだったのですが、皆様がとても楽しそうに会話をしてらっしゃるので、仲間に入れたらと思い書き込みをすることにしました。

私は今のＡＨはコミックスのみで読み続けている身なので、お話についていけない部分もあるかとは思いますが、宜しくお願いします。

### [7448] 凛々	2002.08.19 MON 00:40:08　
	
関西圏にお住まいでＣＨのＣＤをお探しの方がいたら、今日ヨドバシ梅田で1の一枚目と2の２枚目とドラマティックマスター1の３枚があったのでご報告です。
　ＯＰ／ＥＤのお話が出てましたが、ＣＨのＯＰ／ＥＤと言えば、私は劇場版を思い浮かべます。あの美しい絵を超えるものは今だかつてないです～
　ところで、神村幸子さんの最新情報をお持ちの方いらっしゃいませんか？ＨＰが閉じてあるもので。くすん
　
☆７４４７　たま様
私もス○パーしたいんです～　あぁ夢のス○パー

☆７４４３　おっさん様
お返事ありがとうございます！２２時まで働いて片道１時間３０分！！わたしも以前同じくらいかけて通勤していたのでそのつらさよくわかります。夜道と体にはきをつけて。　

### [7447] たま	2002.08.18 SUN 23:58:04　
	
ＣＨの再放送・・・やっぱス○パーですよね。
毎日ＢＢＳでＣＨの話していると、どうしてもＣＨが見たくなりますよね。
（あぁ～、ス○パーかぁ～・・・悩むなぁ。金ないしなぁ。ス○パーのＣＨ、全部終わったら、また１から放送してくれるのかな～。）

＞千葉のOBA3様　【7438】
見て来てくれたんですね～♪伝言板！（なかったけど）←ありがとう。
そして、うらやましい・・・。本気でうらやましい・・・。
ＡＨでリョウと香がデートしてたのが「新宿御苑」っとかって、やっぱ地方人には分かりえない事ですからね。（行ってみたいよぉ～）

＞いの・・・・・・ハッ！いちの様　【7441】ふぅ～、危ない！危ない！
また、５斤ハンマーが飛んでくる所でした。（レス、これだけ…ﾊﾊﾊｯ）

＞マミマミ様　【7439】
ＣＨのケース見つかりましたか？そして、ネット販売の説明とっても分かりやすい説明でしたよ。ありがとう☆ございます。
ＣＨのサントラは・・・・・・・
うふふっ。じ・実は、ＯＲＥＯ様が・・・・
ねぇ～☆ＯＲＥＯさまぁ～(お願いの眼差し）

＞ＯＲＥＯ様
ハッピ～バァ～スデェ～（ドンドンドン　パフッ　パフッ）
私はすぐ文章が長くなるので、公式では、これだけです。

今日からまた月曜日・・・新しい１週間の始まりですね。そして、今週は待ちに待ったバンチ発売・・・。（楽しみですっ）

### [7446] OREO	2002.08.18 SUN 23:26:48　
	
こんばんわ。ついに誕生日までカウントダウンに入りました・・・嬉しいのやら、悲しいのやら。弟が免許の卒検に合格して明日免許センターで学科です。もし受かったら、弟の免許に二つの誕生日が載ることになります。弟の誕生日は勿論、免許の発行された日として、私の誕生日が・・・面白い免許証になりそうです。（受かればね☆）

＞７４４３おっさん様
こんばんわ。来月誕生日なんですね。おめでとうございます
私も将来サービス業につこうと思っているので今のうち遊んどこってかんじです。

＞７４４１いちの様
こんばんわ。２３日がお誕生日なんですね？すぐ私の後ですね。なんか嬉しいです同じ誕生月で♪ほんとＣＨ読破してください！

＞７４３７たま様
こんばんわ。ほんと明日ですよ。別に何するでもなくバイトしながらすぎて行きそうです・・・あっ、メール届いてましたよ。ありがとうございます。成功っすね！！

明日台風こないかなって思ってます。そしたらプールのバイトきっと屋内プールになる。そしたら日焼けもせずに、キレーなプールで泳げる♪あと少しで水泳教室終わりだけど、結構体力限界。コーチ陣に救いの手をぉ・・・ではまた☆

### [7445] 無言のパンダ	2002.08.18 SUN 23:21:06　
	
こんばんわ★

「ハッピーバースデイ♪」
＞ＯＲＥＯ様（１９日）いも様（２２日）いちの様（２３日）おっさん様（来月）その他（省略してゴメンナサイ）近々お誕生日を迎えられるみなさま☆おめでとうございます！！
みなさんきっと夏の太陽のようにキラキラ輝いてて、熱っいハートの持ち主なんでしょうね♪どうかこれからも輝き続けてくださいネ☆

＞いちの様（７４３１）
お友達をまんまとＡＨファンにされたとか(^^)
その調子でどんどんＡＨの輪を広めてくださいね♪
でも私はどちらかというと、いちの様にハメられた（？）お友達同様、とっても人から感化されやすいタイプでハメられたことはありますがハメたことはありません(^_^;)信頼性に欠けるのかしら？コツがあったら教えてくださーい（笑）

＞葵さま（７４４２）
ひじの具合が良くなってきたそうで良かったですね(^^)
まだ完治するまでは時間がかかるのかもしれませんが、お大事にしてくださいね。台風が葵さま行く手を阻みませんように！

ではまた。(^_^)/~

### [7444] OREO	2002.08.18 SUN 22:18:15　
	
こんばんわ。ついに誕生日までカウントダウンに入りました・・・嬉しいのやら、悲しいのやら。弟が免許の卒検に合格して明日免許センターで学科です。もし受かったら、弟の免許に二つの誕生日が載ることになります。弟の誕生日は勿論、免許の発行された日として、私の誕生日が・・・面白い免許証になりそうです。（受かればね☆）

＞７４４３おっさん様
こんばんわ。来月誕生日なんですね。おめでとうございます
私も将来サービス業につこうと思っているので今のうち遊んどこってかんじです。

＞７４４１いちの様
こんばんわ。２３日がお誕生日なんですね？すぐ私の後ですね。なんか嬉しいです同じ誕生月で♪ほんとＣＨ読破してください！

＞７４３７たま様
こんばんわ。ほんと明日ですよ。別に何するでもなくバイトしながらすぎて行きそうです・・・あっ、メール届いてましたよ。ありがとうございます。成功っすね！！

明日台風こないかなって思ってます。そしたらプールのバイトきっと屋内プールになる。そしたら日焼けもせずに、キレーなプールで泳げる♪あと少しで水泳教室終わりだけど、結構体力限界。コーチ陣に救いの手をぉ・・・ではまた☆

### [7443] おっさん（岐阜県：何せサービス業・・・）	2002.08.18 SUN 21:17:14　
	
毎度、おっは～！！皆様おげんこで～ごじゃいますか？おっさんで～ごじゃいます。初めての方もいますが私は女なんでお間違えなく。つ～ことで宜しくお願いします。

レスレスレスレスレス・・・・・（長げ～よ！）
凛々様（７２４７）初めまして・まろは～ん（７２４６）おっつ～！！、二人の気持ちはよ～～～～～く分かりましゅ。何せ私も仕事はサービス業、しかも家から仕事先まで片道１時間３０分。帰りは酷いと２３時３０分に家に着く。店閉店は２２時、勘弁して・・・な状態です。電車通勤なんで帰りがマジ怖い（ＴＴ）アパートは借りたくないし（訳アリで）今正直どうしようって状態です。すんません、変な話してしまいました。

今月誕生日の人多いですね。私も来月誕生日じゃ～！！つ～ことで本日はこの辺で・・・

### [7442] 葵	2002.08.18 SUN 21:11:55　
	
　ひじの具合がよくなってきた。明日湿布薬をもらいに行きたいのに台風が…。どうしよう？！

＞ＳＭＴＰさま［７４３５］
　再放送されると、やはりＯＰ（オープニング）ＥＤ（エンディング）共々多少はカットされてますよ。ノリに乗って歌ってると尻切れトンボみたいでイヤですよね～。私も録画を始めたのが再放送からなんで、とっても残念です。フルバージョンで再放送してくれないかしら…。（>_<;

### [7441] いちの	2002.08.18 SUN 17:04:50　
	
。はい！！とゆーわけなんでけどもね。えー、みなさ元気ですか？いちのですぅ。本日はレスです。

＞[7416] kinshi様
ファルコンの誕生日ケーキ楽しみにしています！！ちなみに２３日が私の誕生日です☆居留守をつかうなんて滅相もございません。家さえ壊さなければ大歓迎です（笑）。

＞[7423] たま様
ＣＨの件なのですが、安値で売っているお店が近くにありません（汗）。ＣＨを置いているところは全巻７０００円以上します。もう、７０００円を出して買おうかなぁと思いはじめました。

＞[7429] OREO様
いえいえ、十分かっこいいですよ！！留学なんてしてみたい！ホームステイならしたことあるのですが・・・いいですよね、海外は。
ＣＨはなんとしてでも買います。そして読みます！読破します！！

＞[7434] 葵様
さすが！！葵様！！！上には上がいましたねぇ～♪
私の方はまだ妹と、友達２，３人ぐらいしか洗脳できていません。しかも、洗脳のレベルも全巻買わせるほど高くありませんし・・・。御見それいたしました。

＞[7438] 千葉のOBA3様
これからもちゃくちゃくとＣＨファンを増徴させていきたいと思います（ほくそ笑み）。
息子さんはさぞかしすばらしい教育を受けてきたのでしょう・・・（想像）。ぞくっ（寒気）。
１０代最後にでかい事をやらかしたいのですが、具体的に何をしたらよいのやら、皆目見当もつきません（汗）。かたはらいたし。

最後に、はじめましての方々、私は“いちの”と申します。“いのち”ではございません。念のため言っておきますが（笑）。これからも宜しくお願いします。
