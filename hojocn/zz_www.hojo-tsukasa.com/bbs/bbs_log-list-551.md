https://web.archive.org/web/20031027165844/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=551

### [11020] みろり	2003.07.05 SAT 04:19:25　
	
こんばんわ
なんだ!この時間？ちょっと横になろうと思ってウトウトしたらもうこんな時間！？
あ～もう1回寝ようか、起きよか…
今週はバンチどこにもないよ～。既にゲットしても、なんかコンビニとか行くとふらふら雑誌コーナーに行って手にとたり立ち読みしたりしてたけど、1日目にして、どこにもないなんて！スンゴイですねぇ。ＡＨ効果ですねぇ。アンケートもナイスな北条クエスチョンだし。
今週の感想。
ここで、総スカンン食らうの覚悟で言っちゃうけど、カラーが気に食わない。
絵は素敵だと思いますよ。でもさ、なんか、違う。いつもグヮバーって感じるオーラが感じられない…。北条先生疲れてる？どう説明したらいいかわかんないけど、タイトルバックの第一印象が特に。
しかしぃ～、玄武！アンタ等何しに日本に残っとるの？ストーカーしに？こら！陳爺さん、玄部は実はストーカー部隊かい！？
でも、以外なヒトって、実は美樹ちゃんだったり？とかみろりは予想してたりして。海ちゃんと別れた訳とか、愛についてとか、阿香に講義するのかな？なぁ～んて美味しい想像をしてたんだけど、実のおとさまだったのね、無難に。ところでＰ217のはみ出し予告に「夏目がキャッツアイ来訪」ってありますね！来週こそ、修羅場！？愁嘆場！？正念場！？

### [11019] 将人（元・魔神）	2003.07.05 SAT 01:56:45　
	
こんばんは、将人（元・魔神）です。

今週号のバンチ、懸賞のアンケート北条司先生の関係の内容でしたね。
アンケートに答えて意見が多かったら本当に北斗の拳のように
シティーハンター・完全フルカラー版とか出来るんでしょうかね？

今週のＡＨは、冴羽さんの涙を見たアシャンがどうなるのだろ～
と思っていたんですが、意外な人物が出てきてビックリしましたよ。

先週の信宏と冴羽さんのアシャンの尾行のさらに後に陳さんと
玄武部隊まで尾行していたんですね。

李大人、陳さんに香瑩様が初恋をされたようですとでも聞いて
いてもたってもいれなくて、日本・新宿に来たんでしょうね。

何度かのアシャンのＣＨの事件で陳さんを通じて名前が出たりして
いましたけど、「娘が初恋」とは状況が違いましたからね。
「親バカコンビ」いいですね～！
暗殺部隊から抜け出した直後のＧＨと比べて、今の香瑩は普通の
女の子に近くなっているのを見て感激して、すごく派手な行動して
いるような気も・・・。今回は李大人、裏の組織のボスではなく
完全に「親バカな父親」でしたね。

＞[10994] 無言のパンダさま　2003.07.03 THU 09:14:15　
僕のやっていたゲームは、あやしいＨな目的の美少女系じゃないですよ～！
そのゲームが健全かどうかは、わからないですけど・・・。
アニメCH冴羽りょう役の声優、神谷明さんの雄叫びも色々と
聞こえる某スーパーロボットアニメ競演作品のゲームです。

→僕のＨＰで北条司先生のシティーハンター関連以外にやっている
　ファンサイトの項目ですよ。

最近、あまりにも早いペースで毎年のように出るので、
他の種類のゲームはここ数年でドラ＠エかファル＠ムのYsイースの
シリーズしかやっていないな～！

＞[10993] みろり様　2003.07.02 WED 22:19:35　
｢赤い衝撃波｣って名前、分らないですね。
某格闘ゲームを思い出したけど・・・山口百恵さん主演のドラマで
「赤い」シリーズってあったようなか？なかったような？

＞[11013] ふじっちょ様　2003.07.04 FRI 20:34:27　
「常識の時間」の神谷明さん見ましたけど、キン＠マンの旧作は
場所によって違うかもしれませんが僕の住む大阪では日テレ系でしたよ。

一度終了してから、始まった「～王位争奪編」とか「～２世」は
どこの放送局だったか知らないんですけどね。

あの問題の時に出てきたアニメキャラの絵パネルで一番最初に
CH冴羽りょうの絵が出たのは良かったけど・・・。

僕は、ふじっちょ様のメールの配達員なんすか？
yosikoさまからメールが来たら、ふじっちょ様へメールを転送したり
ふじっちょ様メールアドレスをyosikoさまに連絡したりするのを
中継してもいいですけどね。(笑)

### [11018] 里奈	2003.07.05 SAT 01:05:10　
	
いやぁぁーん！仕事帰りにバンチ探しまわったんだけどドコも売り切れで読めなかった！（￣□￣;）
まだ読んでないのに皆の感想カキコをチラチラ見てしまって余計はがゆい！

この前カキコしてから一度も家に帰らず友達の家 泊まり歩いて遊んでたら、またレス仕切れないような状態になってる…(汗)過去ログ見なきゃ付いていけねぇー！友達の家泊まりに行く日はＰＣも持って行かなきゃ！
脱べッカム後、また何かやらかそうとスパイラルパーマあてたら、なんかカッコイイってゆーより野生っぽいのはナゼ…？

極力 感想カキコ読まないようにレス…
☆１１０１７　Ｍａｍｉ様
へっ…？Ｍａｍｉ様って結婚してたの！？しかも７年目！
知らなかったの里奈だけ…？里奈ちんＭａｍｉ様のこと１０代ぐらいだと勝手に想像してたわ！
里奈ちんもＢ型だっちゃ♪『ｂｅａｕｔｉｆｕｌ』の『Ｂ』ね！

☆１１０１１　ふじっちょ様
『殺し屋』ってストレート過ぎた？んじゃ『街の清掃人』ぐらいにしとくわ(笑)殺し屋ってどうやってなるのかなぁ～？面接とかあるのかなぁ～。外見重視だったら自信あるんだけどなぁ～。バイト雑誌見てみよ。(…笑)

☆１０９９４　無言のパンダ様
くんくん…ほんとだ。この手紙タマネギ臭ぁ～い！
封印しても部屋中匂いそうだから、どっかのパンダに送りつけちゃおっかな。
なになに？今度は『Ｓ』？そりゃ明らかに『サド』だよ『サ･ド』！

☆１０９９３　みろり様
あれれ？『はじめまして』だった？里奈ちん人見知りしないから気付かなかったぁ～！(人見知りで片付けた！)
あなたはもう私の電流ムチのターゲットにされてるくらい、里奈の心の友ですのよ♪さぁ～！遠慮なさらずにドンドン体当たりしてきてちょうだい！容赦なく叩き潰してさしあげてよ☆
ちなみに里奈は いつもハイテンション(笑)年下のバイトの子達にも関心されてる。『里奈店長スゴイ元気ですよねぇ～。もうイイ歳なのに…(ぷぷぷっ)』って。はうぅぅっ(￣□￣;)

### [11017] Ｍａｍｉ	2003.07.04 FRI 22:08:12　
	
こんばんは、某Ｍａｍｉです。かなりお久しぶりです。
覚えていてくださった方ありがとう。忘れた方と初めての方、はじめまして。
感想といきたいところですが、まだ手に入っていません（涙）

私事ですが、今日は７回目の結婚記念日です。んが、まだ旦那様が帰ってきません・・・。りょうや香みたいに、月日とともに深まる愛っていうのを経験してみたいです。

ちなみに私もＢ型です。多いですね～。
またパソコン直ったらきます。おやすみなさーい。


### [11016] 葵	2003.07.04 FRI 21:16:59　
	
うわぁーっ、なんなんだ下の「＞」印と余白はっ！恥かしい～っ!!!

### [11015] 葵	2003.07.04 FRI 21:15:25　
	
　感想です。カ、カラーって…ス・テ・キ♪無防備に涙するリョウちゃんに、優しく寄り添う香ちゃん…。こんなステキな扉絵、ＣＨでもなかなかなかったですよね？この香の姿がリョウには見えないんだ…ってのが、また悲しいです。しっかし９Ｐ（ページ）の李パパ（おおっと！この呼び方も懐かしいっ☆）アップ、なんだかすごく穏やかぁ～な、渋めのイイ人に見えますね。これが裏の世界のスゴイ方だなんて、誰が思います？！でもやっぱり阿香には弱いらしい…。(笑)屋上での二人の「パパ」の会話はナイスでした。「親ばか」…って、二人のためにある言葉かと思ったし☆阿香の初恋の行方…リョウにとっても李パパにとっても、そして何より阿香にとっても、しこりの残らないものになって欲しいですね。ところで信宏はっ？！(笑)

＞ａｙａｋｏさま［１０００］
気づきませんでした。キリ番(だよね、コレ）おめでとうございます♪

＞みろりさま［１００７］
ウソつかない人間なんて、いやしないわっ！（開き直り☆）

＞ｙｏｓｉｋｏさま［１００９］
'８１年生まれ？！私といくつ違うんだ？ひぃふぅみぃ…。(>_<;

＞無言のパンダさま［１０１０］
コレも妙なキリ番か？「泰然自若」…辞書引きましょう。脳が退化しますからね☆バン懸女王、そうなのか…な？(^_^;

＞ふじっちょさま［１０１３］
そうですね、単行本掲載の時もカラーにして欲しいです♪･･･で、「診察拳」ってぇ～のはナニ？！ぎゃはは _(_ _)ノ彡☆


＞

### [11014] ayako	2003.07.04 FRI 20:37:00　
	
こんばんわ。
今週の巻頭カラー素敵でしたね！私は立ち読み派（ごめんなさい）ですが、先週に引き続き、買いそうになりましたが今週は我慢しました。ところでアンケートに北条作品で欲しいものとかいってあったけど全部欲しい…フルカラー本は出るって正式には決まってないんですかねえ。高いけど欲しいです。

＜昨日の神谷さん＞
見ました。しかし！なぜ冴羽リョウを飛ばすんですか？いつもいつも…。それにしても神谷さんってほんとウマイですよね。

### [11010] 無言のパンダ
キリ番ゲット！？き…気づかなかったぁ。これって喜ぶべきですか？

### [11013] ふじっちょ	2003.07.04 FRI 20:34:27　
	
こんばんわ～☆

今日学校帰りにコンタクトレンズを買うつもりで金を下ろし診察時間まで古本屋で時間つぶしてたんだけど（猫目読んでました）、いざ時間になり行きつけの？(笑)眼科に行こうと思ったら…診察拳忘れちゃったのよね～＼(T▽T)／ハハハハ…

☆今週の感想☆
巻頭カラー良いっすね～先週話題を読んだあのシーンがカラーで拝めるなんて…(σ_-) 是非単行本掲載の時にはカラーで掲載して欲しいですね(^o^)丿
本編に入って玄武が怪しい動き…お久しぶりに李大人登場！
破壊された猫目の修理代（と信宏の治療費）まで立て替えてくれるなんてCHの頃には無かった新たな展開ですね(笑)
他人として実の娘の恋愛の相談に乗り、娘の確かな成長を実感する、そして流した涙は今の父親（リョウ）と同じ涙…いいですね。
娘の一大事にいても立ってもいられない父親二人の会話、お互い、いい親バカですよね(^_^)
しかし…夏目氏に暗殺の危機が迫っていたなんて(笑)親バカも程々に(^o^)

＞[11007] みろりさま
きゃ～セクハラ～＼(゜ロ＼)(／ロ゜)／って、別にこれぐらいでセクハラにはならんでしょう（まあ男の俺がしてたらあかんやろうけど…）別に不快感も感じてないしこれぐらいなら許そう(-_-)/~~~

＞[11009] yosikoさま
メールってラブメールっスか？(これこそセクハラ？笑)メールの所にカーソル合わせると一番したの所にアドレスが出ますよ。それか、将人さまは僕の所にメールされるので将人さまに聞いて見ましょう。

＞[11010] 無言のS.パンダ女王さま(▼▼)/~~~オーホホッホ
僕もｙｏｓｉｋｏさまに一票！！(笑)
「シンデレラ」になりたかった頃のパンダ女王さまに合えば同じ台詞言ってたに決まってます！！(ー_ー)!!

昨日の「常識の時間」神谷さん出てましたね(^_^)でも、放送局の事考えたら普通は「リョウ」を演るのが妥当のはずだろうに…「うる＠」はフジだったし、「キン＠マン（旧）」はたしか朝日系列だったはず。日テレだから期待したのに…(σ_-)

今宵はこの辺で失礼！(-_-)/

### [11012] Ｂｌｕｅ　Ｃａｔ	2003.07.04 FRI 20:02:25　
	
　こんばんは～。　今週の感想です。
　今回は、あったかい話でしたね。なんだかほのぼのしてしまいました。“意外な人物”って、李大人のことだったんですね。あいかわらずおおげさなとこが、面白かったです（笑）　屋上にたたずむ父親二人・・・いいなぁ。
　リョウぱーぱ、「アシャンの見る目は確かさ」なんて言ってるけど、彼が香にとっても初恋だって知っても、そんなこと言ってられるかな？　やきもち焼くとこ見てみたいのに、どうもそうなりそうにないね・・・あはは。
　阿香と夏目さん、しあわせになってほしいなぁ、と思うんですけど、どうなるのかなぁ。次回は柱の予告によると、夏目さんがＣ・Ｅにやってくるようで・・・あーん、楽しみ♪
　カラー扉は、やっぱりキレイだわぁ。静かに涙をこぼすリョウと、彼を抱きしめる香の魂（？）が、切ないです・・・もう、胸をぎゅっとわしづかみにされるかんじで。
　そういえば今回のバン懸アンケート、「北条作品で欲しい単行本」てのがありましたけど・・・わたしは、「自選エピソード集」を「オールカラー」で、がいいなぁ。だって、全部オールカラーになったら、揃えるのにいくらかかるんだか^^;　でも、「完全版」て、カラーだったとこはすべてカラーで、扉も全部収録、てこと？　だったら、ちょっと欲しいかも・・。それよりも、おねがいだからセ○ンイレ○ン限定ってのだけはやめて（うるうる）、ない地域に住む人間の気持ちも考えてくれぇっ。

＞[11006] たま さま
　リョウは、拳銃を構えてシリアスなカンジでしたよ～。

### [11011] ぽん☆	2003.07.04 FRI 18:59:21　
	
仕事を辞めて家でゴロゴロと寝てばかりの私ですが、
先生の漫画を読む為にまたもコンビニに行って来たポン
です♪今週の感想ですが、

思いっきりハズレちゃいましたネ＊けど一遍に色々な事
が判ってすっきりです。アシャンどんどんかわいくなって
ますね☆カラーの香が遼にそっと寄り添ってるイラストは
感動しました！・・が、遼はそれに気づいてないんです
よね？それがすっごく不満です＊遼に何とか気づいて
もらえないのでしょうか？（アシャンが見ていたから、
アシャンが伝えてくれるのかな・・？）なんだかとても
せつないです。いつか、遼パパの秘密の一日、とかで
香そっくりな女性と何らかのきっかけで一緒に過ごして
まるで香が生きていたらこんな感じという風な話が
見てみたいです・・香ちゃんの、遼の元気な顔が見たい
です☆来週も楽しみにしています～★

### [11010] 無言のパンダ	2003.07.04 FRI 18:49:43　
	
こんにちわ☆→こんばんわ★

今日もすっきりしない一日だった・・・降るのか降らないのかハッキリしてくれぇ～～～っ！

＞ｙｏｓｉｋｏさま（９９６）
いつのまにか「Ｕ」から「Ｓ」になってる！今度は何？！「さわやかパンダさん」かな～♪も、もしや○ド？！（￣□￣；）！！

＞千葉のＯＢＡ３さま（９９９）
今週のＡＨの巻頭カラーは予想通り・・・だよね～？(^_^)v

＞ａｙａｋｏさま（１０００）
キリ番ゲットおめでとー♪ヽ(^｡^)ノ
私もコミックス派ですよ。表紙のイラストもきれいだしネ♪

＞ふじっちょさま（１００１）
そーだよ！愛するリョウちゃんが言ったんだよ！・・・でもまてよ。＞お金に執着するからそんな事言われるんだ。。。って、麗香さんが言われたんだぞー！私じゃないじゃん。オバカさん♪あやうく、だまされるとこだった～~(▼▼#)~~
「打倒ふいっちょ」も撤回だーっ！だって、すぐ叶っちゃうも～ん♪

＞葵さま（１００２）
「泰然自若」なんて初めて聞いたよ。もう～☆物知りなタマネギだこと♪・・・で、どういう意味～？(･o･)
ＰＳ：バン懸女王復活ぅ～？！(o^∇^o)ノ　

＞Ｂｌｕｅ　Ｃａｔさま（１００４）
高坂真琴さんといえば、私はどうしてもエースをね○え！の「岡ひろみ」の印象のほうが強くて。。。(^^ゞ

＞暴走女王たまさま（１００６）
たまぁ～早く帰ってきておくれ～＞幸薄いパンダより。

＞みろりさま（１００７）
＞パンダ様の七夕のお願いにはぜひ私たちがシアワセになれるよう、お願いしてもらいましょう。
よっしゃ！引き受けた！ただし依頼料はたっぷりいただきますわよ♪（*￣▽￣*）でもいいのか？！そんなお手軽なお願いで。みろりさまったらホント欲がないのね～。
※Ｂ型で正直モノのパンダを信じて～！(σ_-)

あらら？いつの間にか解禁時間過ぎてる～！ではまたあらためて来襲するぞー！（今週だけど）


### [11009] yosiko	2003.07.04 FRI 17:35:13　
	
こんにちわ♪
遅ればせながら・・常識の時間、見ましたよ♪生神谷サン♪皆様も言われるとおり、リョウちゃんの声もやってほしかったなぁ～でもテレビでリョウチャンの絵を見られて満足♪

＞ふじっちょサマ
いやいや（汗）違う違う！直メールにて言い訳を・・と思ってたのに、アナタに遅れないのはナゼ？？

＞千葉のＯＢＡ３サマ
槙村アニキ＝「メガネ君」に爆笑☆　でも昔、メガネかけてた子って博士、なんて呼ばれてませんでした？？

＞Ｃｈｉｋｋａサマ
にょいにょいにょい。先日ワタクシの誕生日の際に舞ってくださったじゃないですか♪年齢詐称疑惑が消えたところへ・・yosikoは正真正銘の1981年生まれですわよ♪・・・槙村アニキに惚れ、彼が亡くなった後、リョウチャンにfallin' loveだったんですが・・当時、友人に「おじん趣味？」と聞かれ・・「おう！」と半ばヤケクソで答えた記憶あり。年がいくつ差でもステキな人はステキなのだ！！←yosiko論。

では、また・・・・

### [11008] 千葉のOBA3	2003.07.04 FRI 16:56:46　
	
こんにちは☆今日は仕事は休みで、友人達が我が家に来て昼前からドンチャン騒ぎしてました。（汗）あーーー、面白かった！ストレス解消になったわ♪

ところで、見ました！夕べ神谷さんの出られた番組！「りょうちゃんモッコリー！」を聞きたかったです！！ちょっと残念。でも嬉しかったな☆

１００７　　みろりさま　　ひえー！御部屋様に御息所になってるーーー！そー、解禁前のこの時間って何か緊張感が漂ってますよねー。解禁後が楽しみです！！

１００６　たまさま　小さくだけど、りょうちゃんが見れました。銃かまえている所だったみたい。さむいオヤジギャグを連発してないで、もっとカキコしに来なさい！ダーリンと私とどっちが大事なの！？（もちオバサンよね。）

アホなコト言ってないで、本日はこれにて失礼。解禁後・・・いつこれるかなぁ・・・？

### [11007] みろり	2003.07.04 FRI 12:46:48　
	
こんにちわ～
蒸暑～。早く、梅雨終わってほしいですね。
さぁ♪待ちに待った金曜日♪でも、6時まで、みんな息を潜めてるんですもの。このBBSきキンチョーがもっとも高まる時ですね。無神経のみろりはこんなとき、騒ぎたくなってしまいます。

＞[10991] Blue-Sileghty さま
私も「焼肉定食」だった・・・。それに続く言葉が出なくって、でも脳裏には「野菜炒定食」の文字が浮かんでしまいました・・・。

＞ 無言のパンダさま
＞葵さま
ウソつきがウソをついたらやっぱりウソ？
正直モノが「ウソつかない」って信じられる？

＞[10994] 無言のパンダさま
ええ～！！！やっぱり知らない？「レッド・インパルス」（ミズノのスキーウェアではない）
どｰせ、どｰせ、ここじゃあたしが一番オバハンなんだわ～(泣)
あ、あたしもB型だよん～
リョウはＯ型か、ＡＢ型の気がします。

＞[10999] 千葉の御部屋さま
う～ん、御息所と申し上げるべきか・・・？
いやん～、恥ずかし～（ぽ・赤面）あんな好き勝手なコト書いたのにしっかりお見通しだなんて～
でも、分かってもらえて、超うれし～

＞[11000] ayakoさま
えらい！5日間耐えしのいだのですね！(拍手)
やっぱ、写真集とかならまだしも、コミックスは切り取れませんよね。ははは
だけど、私も単行本はジャンプコミックをゲットしました。仰るとおり、文庫は小さいのが我慢できなくって。収納とか携帯を考えると文庫がいいんだけど、北条先生の絵は小さいの耐えれれないんですよね～。他の漫画家さんならあまり気にならないんだけどなあ

＞[11001] マイダｰリン
パンダ様の七夕のお願いにはぜひ私たちがシアワセになれるよう、お願いしてもらいましょう。
（う～ん、あたしって、ネットセクハラしてる？）

＞[11002] 葵さま
悲惨な夢でしたね。もう、実物はゲットしましたかぁ？
みろりは今朝4時に目が覚めてしまいました。朝ごはんも作らずにコンビニに走ってバンチ買っちまったぁ！葵さまもＷＩＬＤにＴＯＵＧＨにＧＥＴして下さい。

＞[11006] たまさま
みろりだけレス頂いて恐縮です。
で、で、でも、「文才」ってなんスかぁ～。そんなコト言われたの、生まれて初めて。あたしこそ「ほっぺがポッ♪」ですがな。そんなコト言われると、き、き、キンチョーしますがな。早く、お時間取れるようになってバシバシ、レスつけてくださいませ。

では、皆々様、解禁後にまた～（←まだカキコするつもりでいる☆）

### [11006] たま	2003.07.04 FRI 00:09:54　
	
うそぉ～ん。見たかった(T_T)
ＴＶに僚ちゃんの画像が出たの？常識の時間で？
いやぁーー。知らなかった・・・。がなじぃーっす。
僚ちゃんはどんな顔で写ってたのだろう？
にへら顔？それともシリアス顔？

さてさて、
皆様から、かんなりレス頂いちゃって恐縮ですぅ。
（とっくに過去ログへ消えてしまっているけど・・・）
皆様からのレスをまとめると、どうやら、アザラシに暴走特急になって欲しいようだわね。
ケッケッケッニヤリ！っとあくどい笑いを残しつつ、やっぱ忙しくて今は無理・・・。なかなか暴走出来ない模様のたまサンなのです。

そんな中、始めましてなので、みろり様へ一言
始めましてこんばんみ。
「みろり様」からもレス頂いちゃって照れ屋のアザラシはほっぺがポッ♪文才があるなぁ～っと尊敬の眼差しでカキコ読んでますですよ。


では時間が出来たらまた来襲します。来週にでも・・。
（最近おやじギャグ化してきた「たま」でした）

### [11005] 無言のパンダ	2003.07.03 THU 23:32:02　
	
私も見ました！生神谷さん♪
ほ～んと！一番最初がリョウだったので「今度こそ・・・今度こそリョウの声が聞ける！」と期待マンマンで見てたのに、チッ！松○めぇ～！・・・あ、キン肉マンも好きですけどね（ナイスフォロー♪）それともオンエアされなかっただけでスタジオではリョウの声もやられたのかな？(^_^;)

### [11004] Ｂｌｕｅ　Ｃａｔ	2003.07.03 THU 22:45:33　
	
　こんばんは～、「常識の時間」、見ましたよ♪
　６人のアニメキャラ（リョウ、バ○ル２世、キ○肉マン、毛利小○郎、面堂終○郎、ケ○シロウ、だったかな）が画面に順番に出て、共通点は？てクイズだったんですけど、一番最初にリョウが出たときはもう、どうしようかと思っちゃいましたよ（どきどき）。でもね、動く映像じゃなかったんだよね。画面に出たキャラの声全部やってくれるのかと期待したのに、聞けたのはキン肉○ンとケンシ○ウと面○終太郎だけだった・・・しくしく。それでも、あの美しいお声が聴けたのは、とっても嬉しかったです（はあと）。
　あと、○次郎（伏せ字になってないし^^;）の声優さんを当てるクイズで、高坂真琴さんが出てきたとき、つい「マリィーちゃんだ☆」とか思っちゃいました（笑）

＞[11002] 葵　さま
　ごめんねぇ、わたしも昨日書こうかな～とも思ったんですけど、本当に今日（３日）でいいのかな、と自信がなかったので・・・。

＞[11000] ayako　さま
　わたしがよく行く本屋さん（ダンナに車で送ってもらわなきゃいけないから、週末くらいしか行けないけど、小説はたいていここで買ってます）では、今でもＣ・Ｈの単行本、見かけますよ～。本屋に行くとついつい確認しちゃうんですよね。あ、わたしも読むのは文庫より単行本のが好きです。やっぱ馴れ親しんでるせいかなぁ。文庫は小説のイメージが強いし。

＞[11003] chikka　さま
　おねがいだから、そんな思わせぶりなこと言わないで・・・。

### [11003] chikka(Eastern Shizuoka)	2003.07.03 THU 22:26:50　
	
皆さんこんばんは。最近昭和の曲がやけに新鮮に感じているへたれ高校教師のchikkaです。さっそくコミックバンチを手に入れました。今週の「Ａｎｇｅｌ Ｈｅａｒｔ」は久しぶりにあの人が登場します。そして・・・・。おっとこれ以上は言えません。詳しくは明日発売のバンチを楽しみに。それから、私個人的にすごく好きな４コマ漫画家がいよいよコミックバンチでの連載を始めます。予告無しで始まったので驚きと同時に嬉しさも湧いてきました。

【１０９９１】Ｂｌｕｅ－Ｓｉｌｅｇｈｔｙ様
私は一番目の方は諸行無常、二番目は大器晩成でした。人生観が平家物語の最初の部分の有名なフレーズと言うのは、やはりかなり現実的になったということですか。夢が無いですねえ・・・。３０過ぎればこんなもんですかねえ。大器晩成と言うのはまだ独身のせいですね。きっといつかいい人が見つかることを願いましょうか。流れ星に。そして、短冊にも書きましょうか。

【生神谷さん登場】
うれしかったですねえ。キン肉マンと「うる星やつら」の面堂終太郎をやってくれました。かなり興奮してしまいました。

【１０９９６】yosiko様
初恋が槇村さんですか？泣かせますねえ。ほんのわずかしか登場していないのに人気度が高いですねえ。でも、あの頃の冴羽さんの方が本来のキャラのように感じるのは私だけでしょうか？ところでyosiko様。そうですか。その頃中学生ですか？私や将人（元・魔神）様と同年代ですねえ。もしかして、み・そ・じ？

### [11002] 葵（ＣＥ２見逃したっ☆）	2003.07.03 THU 21:27:38　
	
　夢を見ました。バン金で｢バンチ」を買ったのに、ＡＨが休載だったの。「アレだけ盛り上げといて、お休みかいっ!!!」と書店からの帰り道でビリビリに引き裂いてました。明日のバン金…巻頭カラーでＯＫですよね？！（疑い深くなった葵☆）

＞里奈さま［９８２］
どもでした。詳しくはメールにて♪(o^-')b

＞ｃｈｉｋｋａさま［９８５］
確かにＡＨのリョウは人間くさくなりましたよね。でもリョウの涙ひとつのネタでココまで書けるｃｈｉｋｋａさまって…やはり「大先生」？！(^^;

＞Ｂｌｕｅ－Ｓｉｌｅｇｈｔｙさま［９９１］
はじめまして葵です、よろしゅうお願いしまっす♪私は「泰然自若」と｢一日一歩」（これは４字熟語か？！）でした。一日に一歩も進まない時はどうすりゃイイんでしょうね～。(^^;

＞みろりさま［９９３］
どこぞのカタカナ語で書いてるパンダ以外…ってことで…。（￣ー￣）

＞Ｂｌｕｅ　Ｃａｔさま［９９８］
うっわぁーっ!!!今晩の神谷さん、すっかり忘れてたよ☆先週はしっかりとチェック入れておいたのに…最近とみに記憶力の低下した私めに、ぜひそーゆーおいしいネタは「前日に」改めて教えて下さいっ☆（T_T

### [11001] ふじっちょ	2003.07.03 THU 20:33:59　
	
皆さんこんばんわ！
今日は本当は昼間に来ようとしていたのですが、うちのPCめが謀反を働き、書いていた文を消去するという暴挙に出たので頭にきて今まで不貞寝してました(-_-メ)（今日に限って以前将人さま達に言われたやりかたをしていなかった愚か者…-_-;）

＞[982] 里奈さま
将来なろうと思ってるのが殺し屋って(^^ゞそれはストレートすぎですよそりゃ店員もビビリますよ、ここは「何でも手広く受けるスイーパー」って言葉を濁さないと(笑)

＞[991] Blue-Sileghtyさま
僕は一つ目は「一期一会」、二つ目は「四捨五入」…？
人生観に関しては文句なし！まるで「フォレスト・ガ＠プ」のような人生！しかし…恋愛観「四捨五入」って…？？？

＞[993]マイハニー(笑) みろりさま＆[994] MKPさま
Mって虐げられる方の事を指すのではなかったっけ？そうだったらMKPさまはMよりSの方が…
(☆□◎；)シュパ！！≡≡(#▽皿▽）_/))"

＞[994] 無言のパンダさま
ちっが～う！俺が言ったんじゃな～い<(｀^´)>あなたの愛しのリョウが言ったんでしょう！！！お金に執着するからそんな事言われるんだ～い(-_-；)

＞[983]＆[996] yosikoさま
メゲタのは僕のせい？僕のせい？だって「なまこ」が気になったんだもん(T_T)

＞ [11000]ayakoさま
＞ＣＨの単行本って古本屋でしか手に入りませんよね
そうですよね。「北斗＠拳」や「DB」・「聖闘士＠矢」なんかは良く見かけるけどCHは新品では見かけないな～。でも、僕は昔CHの30巻を古本屋で新品を買った事あります(笑)
＞私は文庫版は好きじゃないな
僕も最終5巻分の文庫（16～18巻）をもっていますが。やっぱりJCの方を買いましたね。やっぱり統一して揃えたいし。でも文庫の方は旅行の時持って行くのに便利なんですよね（旅先でもCHが読みたくなる愚か者F）
