https://web.archive.org/web/20011006053643/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=122

### [2440] みみごん	2001.08.23 THU 01:19:25　
	
今週号の話を読んで、リョウが１年経っても香ちゃんの心臓を捜し続けてた訳も、「えぐりだす」の意味もわかりました。

でも、もし強奪されてなかったとしても、予定通りの人に移植されていたとしても、リョウには許せなかったんじゃないでしょうか。香ちゃんのドナー登録はすべてリョウのためであって、赤の他人に心臓を渡してしまうなんて...　リョウには我慢できなかったと思います。リョウ自身が強奪しててもおかしくなかったんじゃないでしょうか。ただ、そうすれば香ちゃんの心臓を殺してしまう事になるからしなかっただけ、のように思えます。

このドナー登録の話は香ちゃんに何もなかったら、感動的なシーンですむけど、香ちゃんが亡くなってしまったら、あまりにも哀しいです。

[２４３０]えるぴく様
はじめまして。
私もここに参加したのは連載が始まってかなりたってからだったので、最初に読んだ時のこととか書けなかったのですが、私は香ちゃんの死のショックで体調を崩しました。もともとストレスが身体に出やすいのですが（神経性胃炎や自律神経失調症、不眠症）、漫画を読んで体調を崩したのはこれで２度目です。（１度目はミックの乗った飛行機が爆破された回を読んだ時...）
他にも電車やコンビニで泣かれた方、いらっしゃいますし、皆同じだったんですよ。ほとんどの方がまだ、香ちゃんの死を受け入れられてないと思いますが、でも、リョウの今後が気になって、早く立ち直って欲しくてＡＨを読みつづけているんだと思います。
ここで、辛い気持ちをぶちまけて、少しでも楽になりましょうよ。他にも同じ思いの人がいる、というだけで私は随分楽になれました。がんばって！

最後に、今週号のリョウに一言。
香ちゃんにあそこまで言わせといて、「約束する」だけ～？　もっと、ストレートな愛の言葉は言えんのか！

長々とすみませんでした。


### [2439] Phantom	2001.08.23 THU 01:02:37　
	
Wataru（宮城県）

Could you please help me? Thanks a lot!!!!!!!!!!!

### [2438] ちゃこ	2001.08.23 THU 01:02:11　
	
もうじき日付が変わりそうです。

今週号のコメントをアップしようとアクセスしたら、解禁後一番乗りになって
大汗かいてしまいました。
（あえて書き直しませんでしたが、暴走したまま書いてるから
我ながら、文章が暑苦しいんだもん・・・）

でも、Ｂｌｕｅ　Ｃａｔ様（＞２４２２）ベリー様（＞２４２３）まみころ様（＞２４２４）
さえむら様（＞２４２５）レス付けて下さって有り難うございました。
実は、あのコメント、昨日の時点で気持ちを落ち着ける為に、
書いて保存しておいたのでした。
書いて吐き出さないと、頭真っ白のまま１日過ごさなければならなさそうだったから。
その辺、ＳＡＫＵＲＡ様と同じ気持ちです。（＞２４２１）
Ｍａｍｉ様（＞２４２８）お互い子供のいる身、事故には気を付けましょう（＾＾；；）
ちなみに私は、給油中読んでいたため、
作業してる店員さんの視線をチラチラ感じました。
泣いてなかったけど、すごく恐い顔をしていたんだと思う。


「お守り」
今週号の２人のシーンを見て思い出したのが、「ブラッディ・マリィ－の話」です。
マリィーが１人で敵地に乗り込もうとするとき、車に隠れて付いてきた香が、彼女に
「あたしがいれば、あなた死ねないでしょ！？あたしは、あなたのお守りよ！！」
という場面があるのです。
サラッと言ってるけど、これって凄いことだと思う。
ずっと思っていたんだけど、ＣＨのキャラの中で、一番強いのは香なんじゃないかな。
「守るモノがある強さを知ってる」というか・・・上手く書けないけど。

守るモノがあると、それを庇おうとするから弱点が生まれたり、もし失った場合、
その喪失感や後悔は、もう、どうしようもないものですよね。
まさに、今のリョウの様がそうなわけで。
海坊主サンも心配してるけど、もう、死に急ぐしかない。
（もしかしたら今のリョウは、死んでしまった香の事を思うと、
出会ってしまったことさえ後悔しているかもしれない。そうでないといいけど）
リョウを始めとして裏社会にいる人達の、そんな「脆さ」を吹き飛ばす力を
香は持っている気がします。

でも、sweeper様も書いてますが（＞２４２６）
今週号最後のリョウは、ちょっと力強かった。頑張れ～っリョウ！！


### [2437] Phantom	2001.08.23 THU 01:01:14　
	
Hi, I am a Chinese from Hong Kong. I am a big City Hunter fan and I just realized the new series AH. Could someone please help explaining the story to me in English?

Please email me at PhantomOG@hotmail.com

### [2436] 金田　朗	2001.08.23 THU 00:50:16　
	
＜解禁！＞
はぁ、ドナーカードの新たな意味を知った気がしました。そういう気持ちを表すこともできたんだぁ～と思い、先生の思考能力をいまさらながら凄いと思います（少なからず私は思いつきませんでした）。
GHの「何て…ふたり…」って思いを聞いて（読んで）、あぁ、この子は自分の抱いていた思慕を香のものだと整理できたんだろうな、と思いました。
私もあずみちゃん♪と一緒です。今の彼女には香の感情って辛いと思いますよ。でも、他人の感情にそれこそ『喜怒哀楽』しながらちょっとづつ変わってきたと思うんですよ、彼女。成長…ってものより『人間になっていく』って感じを受けます。まだまだ、みんな辛い状態だけど、リョウも香もGHも幸せになってほしいです。
そうそう、sweeperさん、同感！最後のリョウはスイーパーのリョウになってましたよね。格好良かった。

＜香の死について＞
私も職場で取り乱すほど悲しかったです。友人はいまだにAHを読めないと言います。妹君も「私は…だめだ～！」と叫んでいます。でも、私はここで救われたなぁと思います。
今ではAHを見ずにはいられない体になってしまいました。ははは。持つべきモノは共感できる人たち！ありがたいです。

長々すいませんでした。

### [2435] yaeko	2001.08.23 THU 00:28:51　
	
「今週のＡＨ］
りょうと香のページ何回も何回も読み返しました。読んでて気がついたこと。２３巻、香が屋上でりょうの誕生日を決めるときと同じような話の運び方ですね。最初は明るく言ってから本心を言うみたいなところ。この時と同じように、言う前は悩んだのかなあ？
「予告について」
前の号に二行だけの予告が書いてありますが、もっと的を獲て欲しいものです。「ＧＨが厳戒態勢の新宿で完全武装だ！！」って、どこがだよ！！（三村風）

### [2434] あずみちゃん♪	2001.08.22 WED 23:50:49　
	
[今週のAH]
久しぶりのバンチですね。今週号を読んで思った事は2つ。1つは皆さんと同じように香ちゃんのドナー登録。リョウを想う香ちゃんの気持ちがひしひしと伝わりました。本当に香ちゃんはリョウだけを見ているんですね...涙。たった数ページであれだけの想いが伝わる事に北条先生の2人への想いを改めて知り得ました。
それから、もう1つはグラスハートの切なさ。最初はあまり好感を持ちきれなかった私ですが、今週号を見て彼女も正直苦しんでると思いました。実際のところ心臓移植によってあれほどに故人の記憶が蘇るのかどうかはわからないけど、何もない彼女にあれほどの2人の想いは重荷なんじゃないかなと感じました。理由はどうであれまだ感情の薄い彼女にはすごくつらいのかな...って。きっと2人の想いによって救われるのではないかと思いながらも今の状態では重いのかもしれない。反感をかってしまったらごめんなさい。でも、今の私の読んで感じた事を書きました。

最近ここへカキコに来なかったらすごく進んでいて追い付くのが大変！また、ゆっくり読んでレスなんかもしたいと思います～。それでは失礼しました。

### [2433] もっぴー	2001.08.22 WED 23:45:24　
	
皆様、こんばんわ（＾＾)/
初めての方、よろしくです。

【今週号の感想】
香ちゃん可愛すぎるよ！もう!（ToT)(TOT)(;_;)
あなたは、まさにANGEL！

【レス】
＞えるぴく様、よろぴく。←シャレ（x＾-＾x)

あなたの気持ちはよくわかります。
私も超ショックでしたが、この掲示板で少し救われました。
私も寝れないぐらい、すごく落ち込んで
読むのをやめようかと悩んだときもありましたが、やっぱり、
読まずにはいられません。

ここの掲示板に書き込む事で、すこしは心が落ち着くような気がします。
実際私はこの掲示板に参加したくてパソコンを購入しましたので。
この掲示板に参加されている方は皆さんご親切です。
きっと、先生の作品を読んで、心が暖かいんだわ！

【『リョウちゃん、もっこりー』の実行。】

レスありがとうゴゼーマス。

＞法水様
男性でしたよね。たしか・・。男性から、もっこりは
実行していないという意見が聞けて嬉しいです。（ToT)

＞まみころ様。
りょうちゃん、ヤッテルヤッテル！
ですか（XoX)

＞Tata様。
冴子の初登場の件は、まだ香ちゃんに再会して間もないから
許します。（←そんな権利ないっしょ！）
その後の事は、どう思いますか？

私は、香ちゃん以外は実行はしてないと思うけど（してたらかなりゲンメツ！）

他の方も意見あったら是非お聞かせください！
もっこり研究員の方も沢山いらっしゃいましたよね。

### [2432] ワタル／Wataru（宮城県）	2001.08.22 WED 23:41:27　
	
【今週のＡＨ】
りょうの「約束する」っていうセリフを読んで，ガラス越しのキ
スのシーンを思い出しました。ああ，りょうの目…。何か私には
「俺がこんな世界に引き込んでしまったんだな」と言っているよ
うに思えました。香の意思だとは分かっていてもりょうの後悔は
消えないんだろうな…。

【This week's ＡＨ】
I recalled the episode of the kiss over the grass when
I saw Ryo say‘I promise.’ Oh, what sad eyes he has…
To me, he seems to think that it IS he that led Kaori
to his world. Maybe Ryo knows it was Kaori's own will,
but he never stops regretting…

### [2431] sweeper(栃木は台風、かすりました。)	2001.08.22 WED 22:34:02　
	
こんばんは。また来ました。

＞まみころもっこり研究会特派員様。［２４２４］(笑)
レス、ありがとうございます。「りょうちゃん大カンゲキー！」です。気に入って頂けたようなのでありがとうございます。
沙羅ちゃんのお話のことですが、りょうは香と沙羅ちゃんの会話の内容を知っていたと思います。ただ、知らないフリをしていたのではないかと思ってます。香と自分との関係がへんに気まずくなるのをおそれたというのも考えられますね。
もっこり・・・奥が深いですね。(ぐげっ！・・・撃沈。)
あと、香は海坊主と美樹の後ろをとれるようになりましたしね♪
葉月さんの話です。

[りょうと香のこと]
ＣＨを読んでいていつも思うのですが、どうして香の前では素直になれなかったのかなと思ってしまいます。
一度でいいから、「俺のもんだ。」と言ってあげればよかったのに。と何度も思ってしまったことがあります。でも、香を手放すかずっとそばにおくか迷ったときりょうはすごく悩んだし、香もりょうの気持ちがわからなくて悩んでいたときがありましたしね。でも、りょうの過去を受け入れた香は強いですね。悩んで、すれ違って何度もそれを繰り返したからお互いを心の底から信頼できるようになったし、今の２人がいるんですよね。

長々駄文、失礼しました。
それでは。

### [2430] えるぴく	2001.08.22 WED 22:03:45　
	
みなさま、はじめまして。ここの掲示板はのぞくだけでおもしろく、それで満足していましたが、もうむずむずしてきたので（笑）参加させてください。

＞Eve様〔2402〕
あなたのカキコを読んで自分も初参加することにしました。あまりにも私と同じ感じ方をしていらっしゃるようなので。ＡＨを受け入れられないそのお気持ちは、とてもよくわかります。でも、たぶん、ここの掲示板をときどきのぞいていれば、少しは苦痛がやわらぐと思いますよ。

初めてバンチを読んだときの衝撃はいまだに忘れられません。３号からだったんですが、あまりのショックでわけがわからず、１週間たって４号を読んだとき、はじめてどういうことか飲み込めて泣きました。もう１０年ちかくＣＨからはなれていたのに、まさか泣くとは（しかも電車の中）。恐るべき吸引力です。私はなにしろ香ちゃんファンでしたので、ＡＨがどうしても受け入れられず、北条先生を恨んだり（すみません）、作品を否定したり（ごめんなさい）、けっこう苦しみました。ＡＨ、読むのをやめようと思ったこともあります。今もＡＨを自分の中でどうとらえたらいいのかわからず、ちゃんと受け入れられてはいませんが、とにかく読まずにはいられないので読んでいます。
そして、今週号！　また通勤電車の中で泣いてしまったじゃないですか、どうしてくれるんですか。私の中では、香ちゃんの魂は生きているんだという思いと、肉体はないのだからもうおしまいだという思いがいつも葛藤していて、やっぱり悲しすぎます。それに、香ちゃんのいなくなった後の砂漠のような世界を、リョウがどんな気持ちで生きているのかと思うと……（号泣）。ＣＨ、コミックスの後半は悲しすぎて読めません。香ちゃんの笑顔とか、ふたりのじゃれじゃれとか、何気ない場面にまで涙が……。
いまごろ、こんなことを、それもこんなに長々とカキコしてしまって、ここだけ浮いちゃいましたね。まだ悲しみモードから抜け出せないもので。
ここの掲示板にはずいぶん慰められました。ひとりでは立ち直れなかったと思います。このあふれてくるやり場のない思いをどこへやったらいいのー、という感じで。ときどき、来させてもらいますので、どうぞよろしく。

### [2429] 葵（ＡＨ中毒真っ最中☆）	2001.08.22 WED 21:53:09　
	
　２週間ぶりの解禁～♪
　李大人、影武者の弟ときましたか。じゃ、ＧＨはアニキさんの（本物の李大人の）娘なの？胸に銃をあてて「永別（さよなら）…カオリ」というからには母国語、日本語じゃないよなぁ。それにしたって、狙撃された影武者弟さんて、生きてんのかなぁ。

　ドナー登録してたんだ、香。らしいといえばらしいよね。でも香が一番恐れた「リョウの死」を避けるためには、絶対必要な事だったんだろうな。香の心のケジメって感じで。
　
　そんな香の気持ちがわかってるからこそ、リョウの「えぐりだす」発言があったんだと改めて思った。リョウにとって、香の心臓（ハート）はどんなことがおきても「リョウのもの」なんだって。香とリョウの約束なんだもの。誰にも渡せない…そんなリョウの気持ちが伝わってきて、つらかった。苦しかったなぁ…。

　う゛ー…読めば読むほどつらくなる。でも、やめられない。ＡＨ中毒の葵でした。一週間は長い…☆

### [2428] Ｍａｍｉ	2001.08.22 WED 21:50:42　
	
解禁ですね♪やっぱりココ回転早いよぉ～（汗）
結局コンビニで立ち読みしました（苦笑）ヤバかった。思わず涙こぼしそうで。帰りのチャリもどこを通ったか覚えてないぐらい衝撃でした。「ドナーカード」は私も子供が出来てから持つようになったけど香ちゃんのような切ない考えではなくもっと単純。自分の死後使える物は使ってって感じでしたから。
ＧＨもりょうも自分のために生きていける日が来ること切に願います。
［もっこり談義］
　女の立場としては絶対して欲しくないし、してないと信じたいです。でも、りょう本能で生きてるからなぁ・・・。

### [2427] やえ	2001.08.22 WED 21:44:30　
	
皆さま、はじめまして!
最近、ＣＨの文庫版を読んでハマったやえと申します。
そのときすでにＡＨの連載が始まっていて、連載のラストからそのまんま香の死になだれ込んでしまった（総集編ですね^^;)ようなもので凄いショックでした。初回からりょうが凄く痛いですし。半身千切られたようなものなんだろうなぁ（泣）

あの、ＧＨに何て二人と言わしめた香の記憶は彼女にどう影響してくるのでしょう？その後の刺客を倒した彼女の表情に、わたしはちょっと希望が持てたんですが。（でも、命を大事にする人間兵器なんてあり?^^;）

### [2426] sweeper(Good　evening！)	2001.08.22 WED 21:44:24　
	
こんばんは♪
解禁したので来ました。２週間長かったです！
最新号、読みました。

展開、切ないです。
回想シーンの幸せそうな２人にいつも救われてます。
うーん、しっくりいってるなあ。この２人。どうしてこうなったのか過程を知りたいですね。

[李大人の狙撃のこと。］
弟いたんですね。前回の解禁のとき、「影武者説」をカキコしてしまいましたが、まさか影武者が弟だったとは！でもまだこれには裏がありそうです。

香のドナー登録はりょうが生きのびるためにかけた保険だったんですね。香はりょうにドナー登録したことを明るく報告していたけれどきりだすとき、すごくすごく悩んで悩んで悩んだすえにこういう報告のしかたをとったのではと思います。多分香は話した後のりょうの反応を知っていた気がします。
りょうが死んでしまうことを望んでいなかったのは香だったのですから。そして香の死はりょうが一番恐れていたことだったのではと思います。でもそれは現実におこってしまったから、未だりょうの心の傷は消えないんですね。どんなにおちゃらけていても、普段どおりにふるまっていても香を失った心の傷はとても深くて、完全に癒えないんですよね。りょうはどんなことがあっても香を忘れてはいないし、未だ思い出にすることはできないんですね。回想シーンのりょうの表情と目が切なすぎました。

今回のグラスハート。
モヤモヤみたいなのがふっきれた感じがします。
２人の回想シーンをフラッシュバックで見て、２人の関係を知ってしまったときの表情が驚きと驚愕が入り混じっていた感じでした。

最後のページの最後のコマのりょう。
「プロのスイーパー」としてのりょうの顔が戻ってきた感じでほっとしました。「そうそう、その表情を待っていた！」と一人読みながら思ってました。あと、立ち位置とか・・・。

［餅山さん］
今回、登場したところで笑えました。
これからも、出番待ってます！！
がんばれ！餅山さん！話し方とか仕草とか笑えます。

長々といつも稚拙な駄文、失礼しました。
それでは。

### [2425] さえむら	2001.08.22 WED 21:28:31　
	
[今号のＡ・Ｈ]
そうか、そういうイミで香ちゃん、ドナー登録したんだ..。
リョウの「えぐりだす...」発言も、なんとなく納得。
これで、グラスハート、とりあえずは自殺しない事に決めたみたいね。
よかったわ。
全くもう、気を揉ませるガキだな。
これから、またグラスハートはリョウを探すんだろうな。
心臓（香）とリョウを会わせる為に。
リョウは、李大人、殺すんだろうか？
何と言っても、香ちゃんの心臓を盗ませた、張本人、だからなー。
李大人は、リョウに会いに来たんだよね？
でも、彼の狙いがイマイチよく分からない気がする。
やっぱり、自分の部下であるグラスハートと、心臓のパートナーであったCity Hunterの二人を使って、新宿および日本の裏社会を支配しよう、って魂胆なのかな。
でも、リョウは組織の軍門に下るようなヤワな男じゃなーい！
そんな魂胆に嵌る位なら、対決を決意するんじゃないのかなぁ。
第一、 組織の実態を知ったなら、自ら進んで潰しに行きそう。
来週の展開も、楽しみだ�

[レス]
皆様、レスありがとうございます！
短歌、間違えていて、すみませ～ん。今日の昼間になってから、気付いて、赤面してしまいました。
あと、もっこりネタも、全部確認してから、書き込めばよかったな.....。まだ、もれがたくさんありそう。

＞ちゃこ様[2417]
ちゃこ様の書き込みを読んでいて、今号のショックが蘇ってきました。
リョウは、香の為に。
香は、リョウの為に。
本当に、『なんて二人....』です！

＞Eve様
私は、Ｃ・ＨとＡ・Ｈは、別物語、パラレルワールドだと思っています。
Ｃ・Ｈの二人は、今も元気で仲良くしている、と思っています。
けれども、未だに辛い気持ちが少しあります。
パラレルワールド、とは思っていても、あのリョウと香であることに変わらないから。


### [2424] まみころ	2001.08.22 WED 20:51:04　
	
今週は訳あってまだバンチをＧｅｔしてないので、皆さんの
お話についていけないのがとっても悲しい･･･。（涙）
でも、カキコを見て大体の内容がわかりますね。

［２４１５］法水様。
会長！！法水名誉会長！！忘れるわけ無いじゃないですか！！
もっこりの創始者sweeper様もおっしゃってる通り、会員は
皆会長の帰りを待ってました！！（大げさ？笑）

［２４１８］sweeper創始者様。（笑）
伝言版のことで、西九条紗羅ちゃんの事が書いてあったので
読み返して気付いたんですが、紗羅を追って香が屋上へ行った時
亜紀子さんが「なに話してるのかしらね」って言った時リョウは
さあねと言ってましたが、確かリョウは読唇術ができるとはず。
という事は、あのやりとりはやっぱり分ってたんですかね。

それから｢リョウちゃん大感激～｣は私も大ファンです。
どっかで香かなんかが｢たまや～｣って言ってませんでした？

［２４１９］ちゃこ様。
私はまだ今回のバンチを読んでないんですが、
ちゃこ様の感想だけで胸が苦しくなるほど何かを感じました。
これって結構すごい事ですよね？
ちなみに私も感情移入してしまうので、子供っぽい？？（笑）

それと海坊主の話にレス付けてもらいましたが、無垢な輝き。
この言葉に涙が出るほど納得しました。
そうですね、確かにそうです。

ではここらへんで。

### [2423] ベリー	2001.08.22 WED 20:27:50　
	
やっと、解禁♪

＞Tata様[２４２０]
李大人･･そうきたかって感じです。影武者って･･?弟って？ますます気になってしまう。二週間待って待って待ちわびたら、もっと気になって仕方が無い。確かに、あの笑い方をしている人とは同一人物とは思えないけど･･。どうなのかしら？
＞ちゃこ様[２４１９]
ちゃこ様の感想を読んで私もいろいろ考えてしまいました。リョウは、香がどんな気持ちで「ドナーカード」を自分の前に差し出したかを良く分かっていたからこそ、そして香と約束したからこそ、香の心臓を探し続けていたんですね。でも、香の心臓がリョウを生かし、支えているとはいえ、やっぱり、リョウはつらく悲しいでしょうね。ちゃこ様の言うようにGHには生き続けて欲しい。｢リョウのため｣だけではなく、GH自身の為にも。今はそれを願うばかり。私も香やリョウのように｢誰かのため｣に生きられるかな？自分勝手に生きてきたような、何だかそんな気がしてなりません。

ちゃこ様、全然子供なんかじゃないですよ。(でも車の運転だけは気をつけて下さいね)私もそうですよ。こんなに衝撃を受けるなんて思ってもいませんでしたから。

### [2422] Ｂｌｕｅ　Ｃａｔ	2001.08.22 WED 19:58:55　
	
　　香のドナーカードはリョウが死に急がないための“お守り”がわりだったのかなぁ・・・それがこんなことになってしまって、・・・・・・うる。２４１９のちゃこさんの意見、わたしもそう思います、読んでてますます切なくなってきた・・・。
　　「ばんっ！」って言った時の香ちゃん、なんかすごいかわいい♪って思いました。Ａ・Ｈになって香ちゃん性格変わった？とか最初は思ったりもしたんですが、よく考えるとＣ・Ｈでもしおりちゃんのエピソードのときとかその兆候があった気もするし、もしかするとこれが本当の香だったのかもしれませんね、ずっと香はこうしたかったのかも。リョウに愛されてる自信が持てるようになって、自然にこういう態度がとれるようになったのかな。あぁ幸せだったんだなぁ（過去形が切ない・・）ってつくづく思います。
　　ところで前回は合併号だったんだから今回は１４話のはずなのに、なんで１５話ってなってるのだろう。

### [2421] ｓａｋｕｒａ	2001.08.22 WED 19:46:47　
	
みなさん、はじめまして。
私はキャッツアイからのファンで、北条さんの作品は私の青春でした。結婚もした今、しばらく漫画からも離れていたのですが、思いがけないところでAHの総集編を読み、ものすごい衝撃というか、感動というか、とにかく一人ではかかえきれません。
私みたいな人、きっといると思います。
年齢的に、立ち読みとか漫画を買うのが少々恥ずかしいのですが、絶対AHを読みます。（といいつつ、まだ今週は見ていない・・。）
長くなりましたが、香の死はやはりショックでした。
今後もよろしくお願いします。
（私的には、香は大大々好きなのですが、他の女性に比べてあまりかわいさがないような気が・・。（怒らないで）　ｍ（＿　．＿）ｍ　目がちっさいのかな？）

