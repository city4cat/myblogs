https://web.archive.org/web/20020425132801/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=85

### [1700] なち	2001.07.26 THU 01:51:34　
	
ＡＨに不満を持つ者の１人の意見を述べさせてください。

 [手段と目的と]

現実世界では、ほんの些細なことで人は死にます。
風邪をこじらせて。川に飛び込んで頭を打って。それから、交通事故で。
しかし虚構の世界では現実と少し異なることも許されています。
特に主人公側ならなおさらですし、ＣＨのようなハードボイルドモノは言うまでもありません。いわゆるお約束というやつです。
このような物語の場合、主人公側の人物の死は最大の演出となります。
そして、香の死がストーリー上必須だったならば、感情としては納得いきませんが、理屈としては理解できます。
北条義政さんが書いているように、香の死をストーリーの手段として用いることは“設定だった”。ただそれだけのことです。
ですが、ＡＨのなかで香の死は十分に活用されているでしょうか？

　「最愛のパートナーを亡くした男が、そのことを受け入れ思い出とする」
どんなときも平静を保っていた男が、パートナーの死後必死に平静を装うものの、装いきれていない。世界最強の男に見え隠れする壊れた心。
あるいは
　「感情を失った男がもう一度感情を取り戻すまで」
パートナーのおかげで人間味を得たスナイパー、生きる欲求が生まれたが、そのパートナーの死によって再び自暴自棄の殺戮マシーンに戻る。
例えばこんな話があったとして、これにＣＨを絡めるならば、香の死はストーリーの手段として欠かすことが出来ません。

しかし、北条先生が描きたかったのは少女の成長物語でした。
香を殺す必然性がありましたか？ＣＨキャラを入れる理由がありましたか？香が生きているとマンネリだから、これしか香を殺す理由が見あたりません。
これに追い打ちをかけるのが

コミックバンチ創刊０号に掲載された北条先生のコメント
「予想外の展開に驚かれるかもしれません（笑）」
スクープ２１で放映された、北条先生が新連載の会議をしているシーンの資料には
「グレードアップした香」

これらの軽いノリからは「グレードアップした香」にヒロインをすげ替えるだけの目的で香は死んだ、香は「香を殺す目的」だけのために殺された、以外に私には受け止められません。
漫画家生活２０周年を超えた北条司が渾身の力で世に問う、作品にちゃんと持っていってくださいな。

### [1699] 法水	2001.07.26 THU 01:43:39　
	
＞もっぴーさん。
まず普通に接続してこのページに来ますよね。で、ログリストの中から未読のもの（青色で表示してある）を開くだけ開いてから回線を切断します。そうしたら過去に行ったことがあるページはオフライン（要するに回線がつながっていない状態）で見ることができますこれはあくまでもExplorerの場合ですが、「接続」の画面が現れたら「オフライン作業」をクリックすればＯＫです（ただし入力フォームのようにオンラインでなければ開けないページもありますが）。
書き込むときも入力フォームの画面でいったん切断してから書き込み、また接続して送信する、というようにすれば、ちょっと面倒かもしれませんが通話料の節約にはなります。

＞Goo Goo Babyさん。
そうなんですか、知らなかった。僕も最初kaoriさんがイタリアの方ということでびっくりしました。しかもバンチも手に入れていらっしゃるようだし（まさか木曜より早いなんてことはないでしょうね!?笑）。

＞みんみんさん。
まだ翻訳ソフトは完全とは言えませんからねー。ただ、僕の英語ももちろん完璧ってワケじゃないし（さっきも自分の書き込みでミスを見つけた）、kaoriさんもShaniceさんも英語が母語ではないのでところどころミスもありますが…。でも、日本語が読めず、自分達が浮いていることを分かっていながらも書き込んでくれる彼女（？）たちの発言は無視すべきではないし、みんみんさんのように何とか理解しようと努めてくださる方がいらっしゃることは非常によいことだと思います。

### [1698] ゆなつ	2001.07.26 THU 01:35:27　
	
久々登場です。

＜１１号感想＞
「香は一人だッ！」って叫ぶ、ある意味ＣＨより人間らしいリョウ。ＣＨではひたすら、かっこいいイメージ（例えもっ＠りばかり言ってても）が先だったけど、ＡＨでは今後もこういったＣＨキャラのもっと普通の人間的感情や、男、女な部分がいろいろ出てくるんだろうな。。。
　それと、まどかさんも書いてらっしゃったように、最後の方でようやく、二人が会うの会わないのパターンから脱出して新たな展開がスタートしていたので、ちょっと楽しみ！

今後レスをするのも難しくなりそうな予感。
なので、ゆなつはひとまず、読む側に回ろうかと思います。
ここも、ただ自分の意見を書き込むだけの掲示板になってしまったら折角同じ北条先生のファン同士なのに交流がなくなってしまうのではないでしょうか？　それはかなり寂しい気がします。

### [1697] トウヤ	2001.07.26 THU 01:05:35　
	
ははっ、２回目がコンナに離れていては、ファンとしてやばい

かな～なんて。前読んでくれた人なんているのかな？因みに自分

も何時か分からなくって、過去ログ探しまくった。(笑）

コミックバンチ読んで無い事多いので、僕はコミックスを待つ事

にする。香の死は未だ僕を苦しめるけど、それはＡＨだから…

と割り切っていけたらどんなにいいか。だからって北条先生を

恨んでばかりも居られないしね。先生の絵はとても好きだし。

まだ書ける気はしないけど、何時か先生に僕が考えたＯＧ

(オーギット）を送りたい。香とリョウは結婚していないけど、

ＯＧの行く末を優しく見守るという設定だが。…素人の戯言。

聞き流して。

　

### [1696] なな【レスの時間♪】	2001.07.26 THU 00:45:22　
	
文が長くなりすぎそうだったので２つに分けさしていただきました。

＜月見さん＞
はじめまして。今回の感想はけっこう月見さんと同じ意見なんです～。今回も切なさのオンパレードでしたねー。目頭がじーんと熱くなりながら読んでました。私も李大人を狙撃した人気になってます。だって絶対ＧＨじゃないと思うし・・・。

＜優希さん＞
はじめまして～。そうかあの冴子の表情は槙村兄を思い出してたのか・・・。細かい所をよく見てますね♪今回冴子のセリフ少ししかなかったですね。海坊主に至ってはセリフ無し・・・。場面にはでてるのに～。海坊主って黒すぎ！はさることながらあんなに前から無表情でしたっけ？

＜金田　朗さん＞
以前いらした方がもしいなくなったら悲しいですよね・・・。
ただたまたまいらしてないだけだと信じてますけど。

＜木曜組のみなさん＞
まだ読んでらっしゃらないかたネタバレしてすみません。
明日はここにもしかしたらこれないかもしれないので今日のうちに思いっきり書いちゃおうと思って。

### [1695] みんみん	2001.07.26 THU 00:45:08　
	
<<今週のＡＨ>>
皆さんもおっしゃってますが、取り乱したリョウは確かに人間らしかったですね。
１号での「香は生きている」（だっけ？）発言とかなり矛盾していますが。
それは現実を受け入れられなくて混乱しているという事にしましょう。
あと、凍り付いているリョウの目のアップに私は凍り付いてしまいました。
恐い、恐すぎる・・・
今週は動きがあったという事で評価したいと思います。
<<レス>>
＞ななさん
否定的意見が多い私にとって、あなたの考えは私の考えと同じです。前書き込みでも指摘してます。（読んでないのかな？）
ななさんはいつも前向きな意見が多いと関心しております。
＞法水さん
英語が苦手な私にはkaoriさん（その他英語の方）とのやり取りが少し羨ましく思います。
翻訳サイトで翻訳してみるのですが、なんか変なんですよ。なんとなくは分かるんですが。
＞なちさん
北条氏が本当に壊したかったものについてなんですが、あまりにも的を射てたので驚きました。
北条氏はかなりの覚悟と決心があったと思います。
それが最後にはどう評価されるのか気になるところです。
正直、今のところ悲観的ですが。

最後に私の地域ではパンチいっぱい置いてあるんですが・・・
最初は出遅れると売り切れてたのに。
発行部数を増やしたのでしょうか。それとも・・・

### [1694] 銀花	2001.07.26 THU 00:33:02　
	
### [個人レスをつける方々へ]
自分の発言がここを訪れる一日何百人かのその人々に自分に関係の無いレスの内容を読ませる必要があるのか、一度考えられたらいかがでしょうか？

### [1693] なな【今週号の感想♪】	2001.07.26 THU 00:31:24　
	
今週号で徐々にストーリーが動き出した気がします。

＜リョウ＞
「いい加減にしてくれないかな。」と言った時のリョウの背中・・・。切なかったー！！はじめは「香は必ず戻ってくる。」と言っていたリョウですが(７号より以前の号は読んでいないので、皆さんのカキコを参考。)やはり時間がたつにつれて香がいないという事が孤独と共に襲ってきて、もうまわりにかき回されたくない、ほっといてくれという気持ちなんだと思います。
ドクが「香の心臓と記憶」の事を話した時、リョウは怒りをあらわにしましたねー。それは今まで香の死から逃げていたリョウにとって、とても残酷です。香の死を受け入れざるを得ない状況にたたされ、さらにアカの他人に記憶まで奪われたからあんなに感情が乱れたのだと思います。海坊主の言葉でいないはずの香の姿を捜し求めていたぐらいなのでやはり今までは香の死を受け入れてない、まだどこかにいるという気持ちを捨て切れなかったのかな？と思ってしまう・・・。

＜ＧＨ＞
香に対して感情をぶつけるという事はなんらかしらＧＨにとって変化がでてきたという事なのかな？多分今は自分の中に知らない誰かがいてかなり混乱していると思うけど、そのうち冷静になって香と向き合えるようになるはずだよね。

### [1692] まどか	2001.07.26 THU 00:12:34　
	
どんな発言も誰かにとっては波紋を呼ぶものだと思いました。
でも、ここで誰もが1番言いたいことを言える場所であればいいなと思います。でもどうせ討論するなら、北条先生の作品の話がしたいですね。
と、いうわけで今週の感想！！
ある程度ＧＨに嫉妬するリョウが見たいと思っていたけど、まさかあんなみっともないカオしたリョウを見れるとは思いもよらなかった。
私は「ＣＨ」の頃よりただの恋する男に成り下がった今のリョウの方が好きかもしれません。
もう一つ。
「おおお！！やっと事件！？」（笑）
香のことでアタマいっぱいのＣＨとＧＨの仕業でないことは確かだと思うけど、だとしたら新しいレギュラーの誕生でしょうか。
それともただの使い捨てキャラだろうか。（笑）
後者の可能性の方が強い気がするが。

### [1691] ☆サディ☆	2001.07.26 THU 00:05:14　
	
も～ぉぉぉぉう！！
一週間長すぎます！！
こんなにまっても今日のは読んじゃって続きが気になりますよ！！
毎日発売してほしいです！！（月刊じゃなくて良かったとも思ってます(^_^;)）
とうとうリョウは知ってしまいましたね。
どうなるんだろう・・・。
李さんやられちゃってビックリもしたのですが、だれが殺したのでしょうかね？？
あの人前回意味深な笑いをしていたのに・・・。
まったく先の読めないラビリンスですよ！！
それにしても、バンチ買って読むたびにシティハンター気になってまた一巻からよむんですけど、香のことでいっつも涙ですよ。
辛いっす(T_T)
もちろんほかの作品も読んでるけどどれも続きが気になりすぎて創刊号から読み返すんですよね。
一日中部屋から出て行かないから引きこもりになりそうです(-_-;)
早く一週間すぎるようにしてください。

### [1690] Goo Goo Baby　（レス特集）	2001.07.25 WED 23:46:25　
	
夜のカミナリこわいよ～（怯）　

＞皆さま
　私を「サマ」なんて呼ばなくていいっすよ～　首筋あたりがコソバユイ（←古っ）

＞掲示板のことで夜も寝れなくて悩んだという方は、どなたでしたっけ？
　そのお言葉にあたしゃ感動よ！そんなにココを大切に思ってくれていたなんて！　ま、他の皆さんもそうだということは、皆さんの文面見ればわかりますよね（＾＾）　

＞法水さん
　ＣＨって、イタリアとかのラテン系の方々に人気あるらしいですよ　
知らなかった・・・
アジア圏ぐらいかな～と思っていたもので。

イタリアには日本の漫画専門店が結構あるとか。
その漫画ショップの店員になりたいイタリア人は多いらしいっす
日本の漫画に影響受けて日本語を勉強し始めた人って多いみたいだし。うんうん、イイことじゃ！

あ、時計ね～、私も進んでるなって思ってました。（以前も書いたけど。）

＞Ｊａｍｅｓさん、しゃららさん
　初めまして！　問題児（？）の私をよろしく！
そ～っすね、読者は逃げないからもっとラクにお仕事してほしいですよね
でないとお体が・・・・（汗）

＞ちはるさん
　え、ＨＮ変えたんですか　初カキコの人だと思ってました
じゃぁ、他にもＨＮ変えて再デビューした人いるのかな

＞前川さん
　ちはるさんにも言いましたが、私は掲示板問題に紛れてＡＨのこと結構書いてま～す　過去ログにバッチリ記録されてます♪
てゆうか、別の角度から見て書いてる気もするけど・・・・

＞ＴＡＫさん
　私明日からですテスト。でも１日１回ココ来なきゃ気が済まなくて・・・・（大汗）

＞グラスハートさん（１５）
　キミねぇ、年上の人に向かって「お前」って何さ。香が出てきてくんない理由はソレでしょうお嬢さん（笑）
　
＞りなんちょさん、姫さん
　うぅっ、ドコに行ってしまわれたの？？　
カキコする気が無くなってしまわれたのは仕方ありません。
でも、せめて読むだけ読んで～！でないと寂しい・・・
１人でもココを去るのはイヤだ～！！！

長くなりました　すいません（←いつもだろっ）

意大利有很多日本漫画专卖店。
很多意大利人都想当漫画店的店员
很多人都是受日本漫画的影响才开始学习日语的。嗯嗯，这是好事!


### [1689] shanice (shan)	2001.07.25 WED 23:32:01　
	
To kaori,
Nice to meet you. Haha, actually I also have the same feeling as you and think that it is very strange to write English here at the beginning. The worst thing is that I can't read Japanese and I don't know what did other fans said. But now, I am very happy that there becomes more messages here in English and so I can respone to them.

For me, I am not sure who actually draw the whole pictures. I did note that the drawings have really changed from CH, F compo to AH. I just hope that we can enjoy the best drawings from Mr Hojo.

致kaori、  
很高兴见到你。哈哈，其实我也有和你一样的感觉，觉得一开始就在这里写英文很奇怪。最糟糕的是，我不懂日语，我不知道其他粉丝说了什么。但是现在，我很高兴这里有更多的英语留言，所以我可以对它们作出回应。

对我来说，我不确定整个图片到底是谁画的。我注意到，这些画真的从CH、F compo到AH发生了变化。我只希望我们能欣赏到北条先生最好的画。

### [1688] 藤谷真尋	2001.07.25 WED 23:19:47　
	
突然お話に割り込んで申し訳ありません。
先日から回答を募っておりました、意識調査アンケートの件です。今夜は、集計およびコメントの一部を掲載いたしましたので、ご案内に参りました。コメントの数が多いので、数日にわたって更新作業をすることになりますが、よろしければ後日も時々覗いてやってくださいませ。

この度はご協力をありがとうございました。

### [1687] kaori	2001.07.25 WED 23:14:29　
	
to Norimizu: I am sorry, but I am a straight and frank person, and I always say what I have to say. Drawing myself, I have a little experience on how an artist develop his style, and I feel that from the latest volumes of FCompo on something wrong happened.  
I mean, the style is always the same, but quality of it is worse and worse!!!! This thing can only be explained in two ways: 1) Mr. Hojo has completely lost love and passion for his work, therefore he draws without caring of the real results, knowing that at this point of his carreer he could also retire and live of Cat's Eye and especially CH royalties... so if his latest works are not soooo loved it would not be a problem... or 2) He is just drawing the story boards of this manga, and his assistants do his job.
I don't think I am not polite here, because I know - and I am sure of what I am saying - that lots of famous artists around the world make their assistants do their job and then they just sign it!!! So he won't be the first or the last. Of course, this would deceive me.  
But I could be even more deceived if I were sure he is drawing AH *that* way!!!! With no passion at all!!!!! And the same I can say for the latest volumes of FCompo. No passion, no love, no care in there. This is not a change of style: either ways it's a change of mind.  
I feel like fighting here because I want to give a shake to him!!!! I loved him soooo much, what on earth did happen to him???

致Norimizu： 我很抱歉，但我是一个直率的人，我总是说我要表达的东西。我自己画画，我对艺术家如何发展他的风格有一点经验，我觉得从最近几卷的FCompo开始，有点不太对劲儿。  
我的意思是，风格总是一样的，但质量却越来越差，!!!!！这件事只能用两种方式来解释： 1）北条先生已经完全失去了对他的工作的热爱和激情，因此他不关心真正的结果，知道在他职业生涯的这一点上，他也可以退休，靠猫眼和特别是CH版税生活...所以，如果他的最新作品不是那么受欢迎，这将不是一个问题......或者2）他只是在画这部漫画的story boards，他的助手们做他的工作。  
我不认为我在这里没有礼貌，因为我知道--我确信我在说什么--世界上很多著名的艺术家都让他们的助手做他们的工作，然后他们就签了字！！！所以他不会是第一个在这里签名的人！所以他不会是第一个或最后一个。当然，这对我将是欺骗。  
但如果我确定他是以*这种*方式画AH，我可能会更受骗!!!!！完全没有激情!!!!! 对于FCompo的最新几卷，我也可以这么说。那里没有激情，没有爱，没有关怀。这不是风格的改变：无论如何，这是思想的改变。  
我觉得我是在这里打架，因为我想给他一个震动，!!!!！我太爱他了，他到底怎么了？

### [1686] 法水(Norimizu)	2001.07.25 WED 23:01:06　
	
to kaori;
Why do you doubt Mr.Hojo doesn't draw AH at all? It is not polite to say such a thing. He is professional and is pround of his works, I believe. Maybe his drawings has changed, but is that natural because ten years has passed since CH's ending? I can understand you don't want to believe the present drawings are his, but...
And, it is not true that "almost all the rest of the people here still adore AH and Mr. Hojo..." Some write
critical opinions and most of people in this site cannot accept AH or GH or Ryo yet.

to Shanice;
Nice to meet you. Ryo becomes bald? (^^;) Hmmmmmm...
maybe he gets old and depressed...

致kaori；  
为什么你怀疑北条先生根本不画AH？说这种话是不礼貌的。我相信他是专业的，对自己的作品感到自豪。也许他的画已经改变了，但这是自然的，因为从CH的结束到现在已经过去十年了？我可以理解你不愿意相信现在的画是他的，但是......
而且，"这里几乎所有其他的人都仍然崇拜AH和北条先生...... "这不是真的。有些人写了
批评的意见，这个网站上的大多数人还不能接受AH或GH或Ryo。

致Shanice；  
很高兴见到你。獠变成了秃头？ (^^;) 嗯嗯...。
也许他变老了，变得沮丧了......

### [1685] もっぴー	2001.07.25 WED 22:58:54　
	
>法水様
オフライン作業の仕方、よろしければ、詳しく教えていただけませんか？多分、他の方も、知りたいと思います。

### [1684] James	2001.07.25 WED 22:49:30　
	
＞法水さん
教えてくれてありがとう｡
なるほどねー。わかりました。
国際的ですねぇ｡イタリアからとは。

### [1683] 金田　朗	2001.07.25 WED 22:30:45　
	
＜AH １１話について＞
もう、いっぱい書いてありますね。私もリョウちゃんが『普通の人間』って感じで、香ちゃんを失ったことのストレートな感情表現が切なくなります。
GHが好きになってきた私としてはウィンドーに崩れるGHも切ないです。そして前みたいに服を盗むのをGHにお勧めしたい。病衣のままじゃ迷彩服より目立つよ。
＜レスです＞
もっぴーさん、TAKさん、私も同じくです。以前いらした方々も来てほしいです。潜伏期間なのだろうか？私もここは続けるつもりです。だって、いち早くいろんな人の意見が聞けるし、楽しいですよ。いろんな情報もほしいなぁ。




### [1682] 法水	2001.07.25 WED 22:27:08　
	
＞Jamesさん。
kaoriさんはイタリアの方です。他にも海外の方が書き込みしてらっしゃいます。見辛いなんておっしゃらずにじっくり読んでみてはいかがですか？　結構辛らつなことを書いてますよ。jijiさんはよく知りませんが多分日本の方だと思いますが（違ったらごめんなさい）、kaoriさんはじめ海外のファンのために内容の要約等をしてくださっているのです。

＞ななさん。
そうなんです。「のりみず」と読みます。もちろん本名ではありません。こちらこそよろしくです。

通話料の話が出てましたけど、ログを必要な分表示しておいてから、いったん切断してオフライン作業にしてそれから書き込みを読めばそんなに通話料かさむことないと思うんですけど…。言わずもがなのことかも知れませんが、初心者の方結構多いようなんで。

ところでこの掲示板の右上に出る時計表示、実際の時間より１０分ほどはやくありません？（なんか今回まとまりなくてすいません）。

### [1681] 優希	2001.07.25 WED 22:26:10　
	
今週号の感想です。リョウの目が自分を取り戻しつつあるような気がしました。香を失ってどこかに心が行っていた様な気がしてました。香は一人だと言ったリョウ。これからもその心だけ忘れないでいて欲しいです。その言葉を言った時の冴子の表情はどこか悲しげに見えました。亡くなった槙村の事思い出していたのかな？来週も気になります。

あと色々言われていた掲示板問題。本当はレスする気なかったのですが、お話したい事あったので。
｢元茶々さん」私はあなたが実写版でやるなら誰がいいと書かれていたあなたに対しての返事しました。
気がついていただけなかったのは残念です。
私もあまり皆さんからレスいただく方ではありません。
最初は寂しいと思いましたが、今は違います。
私がここに来るのは自分なりの感想を書きたいから。
書き込みをする事で自分の意見を、ここを見ているだけの人にも見て貰えると思ったからです。

だから私はこれからもここに来て感想かいていきます。
個人的にお名前出して傷つけたらごめんなさい。
私が言いたかったのはレスくれなくても見ている人はいるって事を言いたかったのでした
