https://web.archive.org/web/20031024210015/http://www.hojo-tsukasa.com/cgi-bin/bbs/bbs.cgi?function=log&logno=470

### [9400] ＳＭＴＰ	2003.01.17 FRI 17:30:01　
	
今日はセンター試験前日で、うちの大学はお休みです。受験生がんばれ。割とシビアです。センターは結構重いです…。私はこれの結果が悪かったので、実家からかなり遠くの大学に来ることになりました…（ＴＴ）

＞いちのさま[9386]
暇なんですか？いいな～。暇なら私の課題手伝ってくださいよぉ～（いや、冗談ですけど）。ぐすん…（；；）

＞将人（元・魔神）さま[9392]
うあー。高いですよね。私の場合、１つずつ騙し騙し（？）買わないと絶対に買えないですね。まとめて買う勇気ありません。

### [9399] KIM	2003.01.17 FRI 16:57:59　
	
「C・H」が別バージョンで「A・H」と言う漫画になってる事を知った時は、「なんて蛇足的なことを・・・」と思っていたけれど、よい感じに話の内容は裏切ってくれてて、新しい物語の始まりを楽しんでます。３月にはカナダに行くことになるけれど、帰ってきたら不在中に発売された本を買います♪
この年になって、漫画をかうことになるとは思ってなかった・・・

### [9398] 野上唯香	2003.01.17 FRI 16:39:23　
	
里奈様
お仕事お疲れさまでした(^-^)次はどんな仕事をするんですか？ハワイ旅行楽しんできて下さい。そしたら今度は逆に春休み、私がお土産話聞くようになりますね。もう、お土産は何を買うとか決めているんですか？フランスではアナスイの「ＳＵＩ　ＬＯＶＥ」が日本で定価￥６５００が￥４５００でした。ディオールの香水も￥３９００でした。射撃の話を聞いていたら、私も旅行について行きたくなりました。誰かイギリス・フランスで射撃場があるかないかわかりませんか？

将人様
ＣＤ－Ｒの雑音問題はデッキに入れたとき、雑音がして音が飛んだりとかでしたけどもう解消しました。

### [9397] 野上唯香	2003.01.17 FRI 16:33:40　
	
里奈様
お仕事お疲れさまでした(^-^)次はどんな仕事をするんですか？ハワイ旅行楽しんできて下さい。そしたら今度は逆に春休み、私がお土産話聞くようになりますね。もう、お土産は何を買うとか決めているんですか？フランスではアナスイの「ＳＵＩ　ＬＯＶＥ」が日本で定価￥６５００が￥４５００でした。ディオールの香水も￥３９００でした。

将人様
ＣＤ－Ｒの雑音問題はデッキに入れたとき、雑音がして音が飛んだりとかでしたけどもう解消しました。

### [9396] 謝麟	2003.01.17 FRI 10:38:13　
	
to 里奈
.....wait,
that you said:"I think that you are a dear woman."woman?did you think I'm a woman?god!!fiant!!(T_T)I'm a man!
sigh I have mail to you,to see what I look
like a woman.....(smile).
and return me soon.I want see your photo.you must be a dear woman.(smile again).

### [9395] 医龍	2003.01.17 FRI 08:33:51　
	
おはようございます＾＾、皆さん
最初に訂正＾＾；ガンに関することで訂正します。
まず、エアーガンは最長距離が３０～５０ｍで、ガスガンが３０～４０ｍで、電動ガンが６０～７０ｍです＾＾；
デリンジャーはよく分かりませんが、銃口までの距離からしても、２０～３０もしくは４０ｍだと思います。（詳しい事を知ってる方は教えてください）
じゃあ、何で、１００ｍと書いたかと言うと、安全面を考慮したからです。普通ガンで遊ぶ時はゴーグルをはめるんです。（これは、ガンを扱う時のマナーと言えばいいでしょう＾＾）で、これをはめていれば、目に玉が当たろうと別に大丈夫なのですが、何も知らない方が範囲内にいると、当たる確率も考慮しないといけないと＾＾；。まあ、この様な考えも踏まえ１００ｍと書きました＾＾；ご理解の程宜しく御願いします。
そそ、パイソンで思ったんですが、リョウが持ってるコルトパイソン３５７マグナム４インチで、モデルなんでしたっけ＾＾；確か、ＣＨで、一度モデルまで言った場面があったんですが、う～～～ん覚えてない；０；
はぁ！そだそだ、まっちゃん様へ＞初めまして医龍（いりゅう）と言います。ここに来て、たぶん三ヶ月くらいになるのかな？？まぁ、まだまだここにいる方々からすれば新米ですがね＾＾；メッセを読んでも分かるように、話題があっちいったり、こっちいったり、と＾＾；ご理解しづらいところもありますが、そのときはご指摘を願します。（これは皆様にも御願いします。僕も気づいたら訂正をしたり、また教えたりもしますので＾＾；）まぁこんな僕ですが宜しく御願いします＾＾。
沢山書きたい事があるんですが、頭の整理をしてから又来ます。
では、失礼します＾＾

### [9394] 将人（元・魔神）	2003.01.17 FRI 03:09:26　
	
こんばんは、将人（元・魔神）です。

＞[9390] 里奈さま　2003.01.17 FRI 01:39:06　
今日、里奈様のお店の撤収作業だったんですね。
仕事が無くなって大変ですね。
次のバイトは、決まっているんですね。

ハワイに行かれたら、向こうでもシティーハンターを知っている
でしょうかね？
僕は行った事ないけど、ハワイは日本語とか通じるところが
あるらしいから、シティーハンターの漫画も見ている人も
いるかもしれませんね。

もしハワイでシティーハンター関連の物（英語のコミックとか
CHやAHやCEの絵を使った看板の店など）があったら、
デジカメとかで撮影してくれませんか？

親切とかマナーとか、小さな事かもしれませんけど、
それを里奈さまエライな～。
僕は、そういう事、鈍感で気付かない事ありますよ。
分かっていてイジワルするとかじゃなくて、マナーとか
気を付けているつもりでも、席を譲るとかの親切とか
忘れてしまう事が多いですね。

僕も、これから里奈様のように気を使って親切になりたいですね。

### [9393] 将人（元・魔神）	2003.01.17 FRI 02:45:05　
	
＞[9385] 医龍さま　2003.01.16 THU 13:02:56　
CHのカラー版、ぜひみたいですね。
ライジンコレクションの名前でコミックスは出てますが、
日本語ですね。本当の英語版コミックスって出て欲しいですね。

ドラ＠もんとか小学館のキャラの英語のバイリンガルコミックって
のは本屋さんで売っているの見た事ありますから、売れると
思うけどな～

エアガンのコルトパイソンって100メートルも飛ぶんですか？
すごいですね。僕の持っているのは、BB弾は使うんですが
昔の銀玉鉄砲のような仕組みで空気で押す物です。
でも、それも撃った事ないですね。
買う時にに、リョウの持っている銃の銃身の長さが分からなくて
一番デカイ奴だろうと思って、８インチのコルトパイソンを
買ったのですが、長すぎる。
ライフルちゃうかって感じで！
それで、この前、りょうの持っているサイズの４インチのコルト
パイソンのライターを買いましたよ。
→ルパン３世の「次元」って書かれていますが・・・。

地震は、８年前を思い出しますね。
大阪なので、家は壁にヒビか入るなどで、大丈夫だったんですが
地震のあった時に目を覚ますと、自分の頭、まくらのあったとこの
横ぐらいに向かって14インチのテレビが、落ちかかっていました。

その日のちょっと前の11月に、前に勤めていた会社で、東京から
大阪に転勤になり、実家に戻ってきたのですが、テレビを置いた
時に、偶然なんとなく思いついて、テレビ台とヒモで括っておいた
んですが、それで引っ掛かっていなかったら、それか寝相が
悪くて頭の位置がずれていたら、どうなっていたか分かりません
でしたよ。

しかも、あの壊れていた高速道路をその日に上司と一緒に通る
予定でした。

地震があった後も、電車の一部が止まっていたので歩いていって
行くつもりで、上司が来るのを待っていて、朝のニュースでは
神戸は分からず、京都が震度が高いみたいに思って、京都の
お客様の所に電話で確認とかしていたのですが、

昼休みに近くの食堂にいった時に、通る予定の高速道が
横倒しになっていた映像を見て初めて神戸の地震の
大きさを知りました。

仕事での取引先やお客様もプライベートでも友人がいた為に、
何度も神戸を往復して色々とやっていました。

再建が難しいと思われたお客様からは、銀行の貸しはがしの
ように、納めた機械とか、リース会社を通じて貸していた機械を、
取り返すとかなどの被災された弱い方に対して、内心イヤな事を
色々として・・・。

それ以前も、そのパソコンソフトの僕のいた会社ブランドで
開発していた会社が倒産して、不具合が直す事が出来ないのを
分かっていながらの「クレーム処理」とかの担当だったので
ストレスの影響で、胃潰瘍、十二指潰瘍、自律神経失調症などを
起こして体調を崩していたので大阪に戻ったのですが、

地震の直後は、異常に緊張していて、ずっと眠らずに休まずに
作業をしていても大丈夫だったのですが

半年ぐらいして、少し安定してきて、安心した時に、上司が
調子を崩し目の手術などもしないといけないので、しばらく
元の東京の上司に連絡を取りながら、一人で仕事をして
上司が仕事に戻ったとたんに、今度は僕がダメになりました。

鬱（うつ）病という精神的な病気になってしまって、仕事とかに
集中する事が出来なくなり、やる気・元気とかの気合いが出ない
調子になってしまって、最終的に、その年の11月に会社を
辞めました。

まだ、その鬱（うつ）病が続いて、通院していて、神戸の方向へ
出かけるのを、つい、さけてしまいます。

学生時代は、いじめられていたので、それまでも人と話すのが
苦手だったんですが、仕事をするようになり、普通の会話は下手
ですが、クレーム処理などの仕事での話をする時は、
だいぶ慣れてきていて自信を持っていたのですが、対人恐怖症
のように、人と話すとか一緒に何かをするのが、さらに怖くなって
しまいました。

東大阪市に住んでいるので、神戸や淡路の方のように、
直接、自分の家が壊れた、会社などの建物が壊れた、家族が
亡くなったという直接的な被害は、無かったのですが、
あの阪神淡路大震災は、僕にとっても、何かを大きく変わらせた
ような物ですね。

＞[9386] いちの様　2003.01.16 THU 13:39:16　
CHカラー版みたいですね。自己満足ではなく、ファンとしても
見てみたい物ですね。

＞[9387] Ｂｌｕｅ　Ｃａｔ様　2003.01.16 THU 15:57:30　
CHのカラー版、値段も高いですが、置き場所の問題もありますね。
いつも母親に「いらないの整理整頓して～」って言われているん
ですが、どれも捨てるのがもったい無いように思えるんですよね。
学生時代の少なかった小遣いの中をやりくりして買った思い出も
色々とあるし・・・。

CHのカラー版、エピソード別に出るのもいいですね。
それだと好きなのだけ選べるし。前にバンチワールドで
通常の連載順のCHとは別に「がんばれ香ちゃん編」や
「海坊主のぞっこん編」のように、そのキャラが目立つ回を
抜き出したコミックスがありましたが、あれみたいな形式で
エピソード別にも出ませんかね？

＞[9388] 無言のパンダ様　2003.01.16 THU 20:58:00　
無言のパンダ様もミニクーパー買われたんですか？
僕も100円ショップで、ミニクーパー買いましたよ。

＞[9389] 葵さま　2003.01.16 THU 21:42:34　
CHフルカラー版、北条司先生だけでは無く、ファンも絶対に
見たいですよね。
シティーハンターのDVDも出て欲しいですね。

### [9392] 将人（元・魔神）	2003.01.17 FRI 02:44:42　
	
＞[9357] 医龍さま　2003.01.13 MON 20:59:46　
受験勉強がんばって下さいね。いまは漫画禁止でも、いつか読める
ようになったら話題に参加できますから、がんばって下さい。

＞[9366] Ｐａｒｒｏｔ様　2003.01.14 TUE 15:06:15　
 [ショート×２劇場]面白かったです。
絵が無いのに、海ちゃんのその場面が想像できますね。

＞[9368] 野上唯香さま　2003.01.14 TUE 16:55:51　
すみません。ちょっと風邪で体調が悪かったのと、パソコンを
別の事で使っていてインターネットに繋げなかったので、
メールを確認していませんでした。
そちらに、お送りしたCD-Rの雑音の件、すみません。
問題が解決したそうですが、どこか悪かったのでしょうか？

＞[9376] ＳＭＴＰ様　2003.01.15 WED 17:48:29　
CHのフルカラー版、ぜひ実現して欲しいですね。
ただフルカラー北斗の拳の値段「\1,429円（税別）」が
1巻分だとして、シティーハンターも、同じ値段だとしたら
、一番巻数が多いジャンプ版の数として、全35巻だから、
\1,429×35巻＝\50,015円（税別）！
ゲッ！５万円以上かかるんですね！
全巻を買うの大変ですね。

でも、もし出るとしたらエピソード別に区切って欲しいですね。
CHは依頼があって1つ事件を解決してと話題の区切りがあるから、
その1つの事件の途中で「次の巻に続く」という形で終わらせずに
1つの事件を完結して欲しいですね。

長い話はしかたないとしても、普段の話なら今までのコミックス
での区切りではなく、本のページ数や厚みは変わるとしても、
何個かのエピーソードの話のうち途中で別れているのを
前後どっちかの本に移動して、まとまられないかな？

＞[9378] マイコ様　2003.01.15 WED 22:42:30　
北条司先生のもっこりメッセージ変わってましたね。
マウスで「うる星やつらのラムちゃん」を描かれたのですか？
見てみたいですね。

僕は、描いた事ないけど、学生時代にクラブで「うる星やつら」を描いた人いますよ。
今みたいにグラフィックスソフトの無い８ビットパソコンFM-77AVのBASICといプログラム言語のライン命令を組み合わせてでした。

→当時、小学館の出していたパソコン雑誌に、そういう絵を描くプログラム例で、「うる星やつら」とか良く載っていました。

※８ビットパソコン→初代ファミコンみたいな物で、使える文字はアルファベットと、昔の電報のようにカタカナが中心で絵もそんなに細かくなかった時代のパソコンです。

＞[9379] しゃぼん様　2003.01.16 THU 00:15:28　
ホントだ！北条司先生のもっこりメッセージ「シティ」ハンターですね。北条司先生か、このHPの管理人さんの打ち間違い？
一応、コミックスは「シティー」ですね。

あ、でも俳優の「藤岡弘」さんが「藤岡弘、」って「、」を付けて微妙に改名されていたからな～！
「シティー」から「シティ」に変わったかも～！(汗)(^^；

→自分が見た記事では「藤岡弘　変身！」って書いていましたよ。
　旧１号から新１号の仮面＠イダーになったのだろうか？

＞[9380] 里奈さま　2003.01.16 THU 00:19:09　
販売の仕事、終わりなんですね。お疲れ様でした。
お兄さん、韓国で射撃してこられたんですか？
しかもシティーハンターの357マグナム・・・いいな～♪
→韓国でも、シティーハンターの銃として知られているんですね。

＞[9381] 無言のパンダ様　2003.01.16 THU 01:30:27　
シティーハンターのフルカラー版、北条司先生の自己満足だけ
ではなくファンとしても見たいですよね。

＞[9382] 千葉のOBA3様　
シティーハンターのフルカラー版、見たいですよね。
１００円ショップのダ＠ソーでミニクーパー見つけましたよ。

＞[9383] まっちゃん様　2003.01.16 THU 09:21:05　
はじめまして、こんばんは！
ここの掲示板の人は、いい人が多いですから、ドンドンと
書き込んで下さいね。
男性の方ですか？僕は、東大阪市に住む31歳のおっちゃんです。

＞[9378]舞小姐2003.01.15 wed22:42:30  
北条司老师的留言很奇怪呢。
用mouse画了“福星小子的小羊”吗?
真想看看呢。

我虽然没画过，但学生时代在社团里有人画过《福星小子》。
像现在这样没有图形软件的8位个人电脑FM-77AV的基本ic和类似的程序语言的行指令组合。

当时，小学馆出版的电脑杂志上，经常刊登“福星那帮家伙”之类的程序例子。

※8位电脑→像初代红白机一样的东西，能使用的文字是字母和，像以前的电报那样片假名中心画也不那么细致的时代的电脑。


＞[9379]肥皂泡2003.01.16 THU 00:15:28  
是真的!这就是北条司老师的“城市猎人”。是北条司老师，还是这个HP的管理员打错了?
姑且，漫画是“城市”吧。

啊，但是演员“藤冈弘”把“藤冈弘、”加上了“、”，微妙地改名了~ !
也许从“city”变成了“cityー”!(汗)(^^;

→自己看到的报道是“藤冈弘变身!”这样写着。
从旧1号变成了新1号的假面@イダー吗?


### [9391] 将人（元・魔神）	2003.01.17 FRI 02:44:02　
	
こんばんは、将人（元・魔神）です。
風邪をひいて体調が悪かったのと、別の事でパソコンにずっと作業
させていてインターネットに繋げなかったので、来れませんでした
しばらく、ここの掲示板に来れなかったら、すごい書き込みの量
ですね。

北条司先生のもっこりメッセージ更新されていますね。
CHもフルカラー版を出す話が進んでいるんですか？
「売れなくてもいいから」って書かれていますが、
買って読んでみたいですよ。

この掲示板で話題になっていた100円ショップの＠イソーの
ミニクーパー見つけましたよ。即、買いました。
それと、うちの近所のセブンイレブンに無かった北条司短編集も、
その店の近くのセブンイレブンで見つけて買いましたよ。

＞[9336] 野上唯香さま　2003.01.11 SAT 12:50:32　
「CHアニメムック」届きましたか？無事に届いて良かったです。

＞[9338] TAKESHIさま　2003.01.11 SAT 17:55:00　
トトロの「龍の猫」のネコってのは分かるんですが
龍はなんでなんですかね？神秘的な生き物とかいう事かな？

＞[9339] マイコさま　2003.01.11 SAT 21:57:06　
図書館の管理をしている人、シティーハンター好きなんですか？
コミックスではなく小説版だから図書館に入れれたのかな？
僕が学生だった頃は、そういうなの無かったな～
あった漫画の本は、三国志とか、日本経済入門とかですね。

＞[9340] 千葉のOBA3様　2003.01.11 SAT 22:41:28　
たしかに1万円札だったら置いてるのに、細かくなると、
使ってしまいますね。
CHの時の冴羽さんって、毎晩飲み歩いていたけど、
お金あったのかな？→一億円を1週間でってのは1回だけですが
いつも香さんに、お金が無いのに飲み見歩いてってシーン
多かったからツケを貯めていたんだろうか？

＞[9341] 里奈様　2003.01.12 SUN 01:33:29　
AHでも、冴羽さんはターニァのお母さんの店でツケが百万円を
超えていたけど、色々な店にツケを溜めているんでしょうかね？
そういえば、AHを思い出してみると、缶ビールらしき物を
飲んでいるシーンが何回かあるのを思い出したけど、店に
行くのが減って、自宅で飲むのが増えたのかな？

僕もAHの漫画は好きですけど、香さんが亡くなったというのは
思いたくない部分ですね。
（最近、書いてないけど）CHのミニコントなんかでも
僕の中ではAHのリョウじゃなくて、CHのリョウと香のままで
AHの香瑩が別に加わっただけという勝手な解釈ですね。
ミニコント、また書くとしても、今の一年後の性格が変わった
香瑩の性格がまだ分からんので、ミニコントでは、まだ一年前の
香瑩にさせて下さい。

＞[9342] OREOさま　2003.01.12 SUN 11:20:50　
シティーハンターの音声ドラマのCDブック「悲しい天使」編
をCD-Rに写すのを、ちょっと時間がかかりましたが、やっと
作る事が出来ましたので、明日にでも郵便局に持っていって、
送るように致しますね。
→カナダに届くのって、何日ぐらいかかるのだろうか？

一教科１００Ｐ以上で、それのテストもあるんですか？
頭いたいな～。大変ですね。

＞[9345] 阿樹さま　2003.01.12 SUN 19:59:25　
4月から一人暮らしですか？
でも自分の部屋が、12畳もあるんですか？
うちの家、家の全部の部屋を合わしても、そんなに無いかも
4人家族で住んでいます。
母がたのおじいさんが住んでいた頃は、母を含んで８人ぐらいも
いてたらしいですし・・・。
→持ち家ではなく、三軒長屋の借家です。
　父が養子に来たのでもありません。
　母のおじいさんが別の所に引っ越した時に、妹が生まれる
　直前だった僕の家族が、上手く借りれたのでアパートから
　引っ越したという事らしいですけど

僕も一時期、東京で一人暮らししていた事ありますけど
（前に勤めていた会社の寮ですが）最初は不安ですね。
でも行く前は、不安でも、しばらくしたら慣れますよ。

＞[9347] Ｐａｒｒｏｔ様　2003.01.12 SUN 23:48:32　
クイズの問題を考える方も楽しいですね。
それで答える方ですが、今回のクイズ分かりません！
人造人間？サイボーグなら009を連想するのだけど・・・。
キカイダー？キャシャーン？（→新造）仮面ライダー？（→改造）

＞[9350] 無言のパンダ様　2003.01.13 MON 02:10:09　
同じ年頃だと、女性の方が大人ぽいですね。
僕は、5つ下の妹よりも子供ですね。妹の方が大人です。

＞[9351] ひろっさん様　2003.01.13 MON 02:46:24　
「Ｆ．ＣＯＭＰＯ」は、今のところは、コミックスに載っている話で終わっています。

ただ北条司先生は、「キャッツアイ」はジャ＠プ連載の終了の後で
（→空港で指輪を手錠だと言って投げる話）
「CE完結編」を描かれていますし（→コミックスの時点で入って
います。瞳を追いかけて外国に行く俊夫の話）
CE小説版や、カラーCGの短編集Parrot幸福の人にCEが、ちょっと
出てきたり（→実写のCE映画版の時期だったのかな？）してますし

「シティーハンター」は、現在バンチで北条司先生はパラレル
とは言われていますが続編の位置づけになる話の
「エンジェルハート」として描かれているので、
いつかは、続編を描かれる事があるかもしれませんよ。

コンビニなどで売っていたバンチワールドのシリーズで
長期連載の作品が出終わったかなって思ったら、まさか
フルカラー版シティーハンターなんて話題も出てきましたし。

### [9390] 里奈	2003.01.17 FRI 01:39:06　
	
なぜか今日はＰＣちゃんの機嫌がすこぶる悪い！
カキコ終わって送信しようと思ったらフリーズ…しかも２回も！

今日、里奈が働いてた店の撤退作業をしました。
ほんとに終わっちゃったよぉ～。また職無しだよぉ～！
でも繋ぎで他のバイト決まってるんだけどね☆

☆９３８９　葵様
里奈も兄も海外でＣＨが有名なことが とっても嬉しいです！
ハワイでは どんな説明をしてくれるのか楽しみだわっ♪

☆９３８８　無言のパンダ様
なっ…なに！？
バ…ババ…バレンタイン！！？　（￣□￣;）
そんなもんスッカリ忘れてたよ！
『もうすぐ節分！マメ食いてぇー！』って考えてた…。

海外でオヤジにまみれてストリップ観覧…。
リョウみたいに『ピューピュー♪いいぞぉーっ！それ！脱いじまえぇー！』
…って、オヤジと同化して最前列で楽しんでる自身あり！（笑）
以前ハワイに行った時、日本みたいに道端でストリップのパンフ渡されたり誘われたりしたんだよね☆ってゆうか、女性も誘うもんなの？って笑ってたんだけど…。意外に女性客多いのかしら？

☆９３８７　Ｂｌｕｅ　Ｃａｔ様
里奈も見たい！ラストのリョウ＆香が抱き合ってるシーンとか、
海原の船でガラス越しのキスを交わす二人のカラー！！
絶対泣く！ほんとに実現してくれないかしらぁーっ！

☆９３８６　いちの様
てへへっ♪見下すどころか、ふんづけてましたっ☆
えっ！マジでか！？いちの様より背の高い友達が１０人以上もいるの！？
す…すごい！里奈囲まれたい！（笑）

コラッ！里奈をＮＡＳＡに売るのか！？
捕まってたまるか！こうなったらＴＶから這い出るのは中止！
炊飯器の中とか引き出しの中とか冷蔵庫の中とか、予想外のとこから
軽快に登場して もぐら叩き状態にしてやる！（貞子じゃないじゃん！）
朝、あなたが眠るベッドの中から赤い顔して出てくるかもよ♪
（夜這いじゃん！）

☆９３８５　医龍様
スゴイ！銃好きなんですね♪里奈も欲しいかも！
でもオモチャじゃなくて本物！（平和ボケの無い物ねだり）

医龍様、関心したわ！地球のこと真面目に考えてる！
なかなか そうやって皆に向かって言える人って少なくなってきてますもんね。里奈も、ふと考えることはあるけど、実際の生活の中では全然逆のことしてること多いし…。
里奈は地球環境とかについては語れませんが、ちょっとした人同士のことについては ちゃんと考えてます。小さいことだけど、電車に乗ってて自分が疲れてても席を譲ってあげる親切とか、タバコのマナーとか（吸うことじたい良くありませんが…）、そんな小さな心の余裕とかって、自分自信も癒される気がします。皆が気分良くなれるしね！
なんか里奈のキャラとは違うかも…。
（里奈の株ＵＰ☆笑）

☆９３８４　Ｔｏ 言身寸 麟
Thank you for teaching me your thing.　（^ヮ^)ﾉ
I was born in October,1979.I like eating!
And it is also favorite to buy the dress of fashion!
Therefore,the fashion adviser is continued!

I want to see your photograph!!
I think that you are a dear woman.
With me,since age is near,it is very intersted!
My E-mail address is 『Rina_1011@hotmail.com』.
I am wating for the mail from you♪
I also want to send mail of a photograph to a return!
It is very pleasure!! ヽ（≧ヮ≦）ﾉ

☆９３８２　千葉のＯＢＡ様
えへへ☆アザラシ、可愛かった？
でも本人は未だに出てこないですねぇ～。
アザラシが風邪ひいてちゃシャレにならんよ まったく☆

マイコ様にアドレス教えておきますね！
早くラムちゃん見たぁ～い♪

いちのっちったら、ＮＡＳＡに売るとかなんとか言ってるけど、
ほんとは里奈があまりにも可愛過ぎて、捕まえて離したくないのよ！
彼、照れ屋さんだから♪（しったかぶり…）

☆マイコ様
『画像届きました？』ってメールは来てたけど、
肝心のイラストは届いてないです…（泣）
再度チャレンジしてみてぇー！
千葉のＯＢＡ３様のアドレス、送っときますね♪

### [9389] 葵	2003.01.16 THU 21:42:34　
	
　あﾞ～っっっ！！！なんてこったいっ！きのうカキコした時、すでに北条せんせのメッセージが更新されてたなんて！！気づかなかったなんて…不覚っ☆(>_<;

　で、ＣＨフルカラーですか？もちろん買いますよ。北条せんせお一人用だなんて、そんなの許せなぁ～いっ！リョウと香と、海ちゃん美樹ちゃん、その他すべてのＣＨキャラを愛するＦＡＮのためにも、ぜひぜひ実現しますように♪あ、その前にＤＶＤ化を考えていただきたいです。家のビデオ、やっぱりぐれてきたので（使いすぎ？！）今度買い替えるならＤＶＤにしようと考えてるんです。そのためにもぜひ、ＣＨの一日も早い（ビデオが完璧に壊れる前に☆）ＤＶＤ化を望みます！

＞里奈さま［９３８０］
　御兄妹そろってＣＨファンですか？いいなぁ～うらやましい♪しかし３５７マグナムの説明の仕方、すごいですね。韓国での日本のＣＯＭＩＣブームのすごさを実感しました。

＞千葉のマダムさま［９３８２］
　そう思うでしょ？あんなちゃん、かわいいですよね～♪「アン」と名づけるなら「安」「杏」のどちらかかな。今からでも遅くはありませんよ、がんばって下さい！(^_^;

＞いちのさま［９３８６］
　いやはや…すごい勘違いをさせてどーもでした☆(^_^;　でも家のばば、らぶり～でしょ？

＞無言のパンダさま［９３８８］
　バレンタインのコーナーでミニクーパー？ＣＨ好きの彼へのプレゼントってコト？でも自分用に買いたいと思う女のコが殺到だね♪葵という名前は古風かな。（私のＨＮの由来は古典だけど☆）私も「子」の字が欲しかったです。友達みんな持ってる（？）のに、私だけないんだもん。母いわく「モダンにしたかった」らしいけど、その実ホントのところは両親の名前から一字ずつ取っただけなんだよ～。(^_^;

### [9388] 無言のパンダ	2003.01.16 THU 20:58:00　
	
こんばんわ★

昨日はやたら眠たくて、フルカラーコミックスの話題にふれるのが精一杯でしたー(^_^;)
さあ！今日はまだ夜は長いぞー♪

。。。というわけで千葉のＯＢＡ３さまに続き、
「今週のミニクーパー情報！」
今日、買い物に行ったら「バレンタインデー」コーナーで設置されていて、そこにぬぁんと！「ミニクーパー」がありました～♪
結構かわいくて２００円だったので思わず買ってしまいました(^^)

＞いちのさま（９３７４）
私も身長の高い男性は好みです！だがしかし、なぜか自分が好きになるのはその逆で案外華奢な人だったりしてましたね～(^_^;)
やはり無意識に自分にないものを求めてしまうのかなぁ～？
そういえば、いちのさまがココに来られた当初も「ヒマだー」と連発されてましたよねぇ？（笑）さあ、今のうちに風邪を完治させよう！

＞葵さま（９３７７）
その「葵ちゃん1位」の話題☆葵さまが不在のあいだに千葉のＯＢＡ３さまがカキコしてらっしゃいましたよ～♪
でもそれって意外かも☆葵ちゃんって結構古風な名前じゃない？
私も大好きだけど(^^)（葵さまの本名も好きっす♪）
私の時代はやたら「子」がつく名前が多くて、自分の名前に「子」がついてないのがかなり不満だったりして自分の名前が好きになれなかったんだけど、今は結構好きですよ。あ、言っときますが「パンダ」じゃないですヨ(^^ゞ

＞マイコさま（９３７８）
私もラムちゃん見てみたいな～♪(^^)私にも見せてくれるかな？

＞里奈さま（９３８０）
「ストリップでおやじまみれ！？」なんとオヤジチックな発言！(ﾟoﾟ)でも里奈さまってオヤジをノセるのうまそうだから、かなり盛り上がって楽しいかも！？毎日「おやじ」を見慣れている私には興味薄ですけどね（笑）
お兄さんに「パイソンで射撃」先を越されたの～？それにしても韓国でも３５７マグナムを「冴羽リョウ愛用の銃」として紹介されるなんてー♪ハワイでもしも、そのように紹介されてなかったら里奈さまがバッチリ「宣伝」してきてくださいね～(^o^)丿
ＰＳ：あざらしの顔文字最高！(^^)v

＞まっちゃんさま（９３８３）
はじめまして！私もＣＨはアニメから入ったクチです(^^ゞこれからもよろしくお願いします♪

＞医龍さま（９３８５）
いろんなガンをたくさん持ってらっしゃるんですね！うらやましいです♪
ところで「核問題」より「地球環境・・」たしかにそうかも(^_^;)地球が「健全」であってこそ他のいろいろな問題に取り組めるってもんだし・・・。う～でもどちらにしても難しい問題ですね。

ではまた。(^_^)/~

＞葵(9377)  
那个“小葵第一名”的话题☆葵大人不在的时候千叶的OBA3大人写了哟~♪
不过这也很让人意外~葵这个名字不是很老派吗?
我也很喜欢(^^)(葵大人的本名我也喜欢♪)
在我那个时代，很多名字都带“子”字，所以我对自己的名字里没有“子”字感到非常不满，所以不太喜欢自己的名字，现在倒是很喜欢了。啊，我要说的不是“熊猫”哦(^^ `灬。°


### [9387] Ｂｌｕｅ　Ｃａｔ	2003.01.16 THU 15:57:30　
	
　こんにちは～。　風邪は、徐々によくなってきてる・・・かな、てかんじです。
　フル・カラーのＣ・Ｈ、わたしも見てみたい！　けど実際問題として、全巻カラーで出したら、買うのにいったいいくらかかるんだろう・・・置き場所だって、どうしよう、だし。だから、ブラッディ・マリィーの話とかソニアの話とか、ミック登場以降とか、重要な話だけフル・カラーで出していただけたら嬉しいかな、なんてわがままなこと思っちゃったりして^^；　ラストの、リョウと香が抱き合ってるシーンとか、カラーで見たいなぁ。
　あ、例のミニカー、迷ってるうちに売り切れてしまいました（汗）今日行ったら、薄紫のやつ一個しか残ってませんでした。

＞[9351]ひろっさん さま
　もっと早くレスろうと思ったんですが・・・えっと、Ｆ．ＣＯＭＰＯの続編は、今のところないですよ。あの続きは、それぞれの想像におまかせ、ってかんじかな。

＞[9385]医龍さま
　Ｃ・Ｈの英語版コミックス、興味はあるけど、出たとしてもやっぱり全巻となると・・・自分の好きな話の巻だけ買ってしまう、のかもしれませんね。
　あと、医龍さまも愛知県民だったんですね！　わたしもですよ～。地震対策、うちも運まかせかもしれない（汗）　やらなきゃいけないのはわかってるんですけどねぇ。

### [9386] いちの	2003.01.16 THU 13:39:16　
	
どうも、こんにちは。大学生はホントに暇です（汗）。

北条先生へ
私もＣＨカーラー版見たいです！！是非！！！
自己満足なんかで終わらせないで世に発表してくださいね☆
発売した暁には全巻買います！！お年玉あるし（笑）。
是非、実現してくださいね。話し合いとは細かいところのチェックとか大変かと思われますが、頑張ってください。応援しています。

＞[9377] 葵様
ＰＣ退院おめでとうございます。
メールで「退院」って題なのにおばあちゃまの写真が出てきたので「え！？おばあちゃま入院してたの！！？」って大変な勘違いをしていました（汗）。よかったよ、ＰＣで（あまり良くない？）
設定とか大変でしょうが頑張ってね。

＞[9378] マイコ様
なんですと！！？？？マイコ画伯の第１作品が完成したのですか！！これは是が非でもお目にかかりたいものですな☆
もしよろしければ私にも送ってくださいまし～。
私もそろそろイラストでも描こうかな。暇だし。（大学生は１年の半分が休みですからね）
・・・テストまだ終わってないけど（現実逃避）。

＞[9380] 里奈様
あんたは今まで私のことを見下してたんかい！！！
自分は１８０㎝ありますが、自分よりデカイ人が身近にざっと１０人以上はいますね（汗）。一人は１９０を軽く超えていますし・・・。その中にいると、自分はそんなに背が高くないんだと思います。
フランス語は英語以上に日本語にない発音がありますから難しいですよね（汗）。私が覚えている言葉は「この本は面白い」です。どう考えても役にたちそうにありませんね・・・。
そんなことよりも、早くＴＶから這い出てきてくださいよ。ＮＡＳＡに売りに行かなくてはいけませんから（笑）。
さあ～、カモ～ン（ノヴ○うさぎちゃ～ん）♪

＞[9382] 千葉のOBA3様
はい、なんか勘違いしているマダムがここにいらっしゃいますね（汗）。人の話は最後まで聞きましょうね～（いい子いい子）。いいですか～？お兄さんは悪の頭（ドン）里奈様を捕獲（生け捕り）にしてＮＡＳＡっていうメガネトンチンカンヤローのところに売ろうろしているのよ～。解ったかな～？（笑）。
（人を小バカにするのもいい加減にしろって感じですね）

### [9385] 医龍	2003.01.16 THU 13:02:56　
	
こんにちは、皆さん＾＾
『もっこりメッセージ』更新されてました＠０＠！！→→
（＾＾）￥嬉しい～～！！
ふむふむ・・・おお！ＣＨをカラーにして販売したい（希望らしい）！！是非!販売実現してください＜（。。）＞御願致します！！
あと、これは僕の希望ですが、ＲＡＩＪＩＮ　ＣＯＭＩＣＳ（月刊の方でが＾＾；）毎月購入しています。それで思ったんですが、英語版のＣＨも販売企画に入れてもらえたらなぁ～＾＾；（僕のわがままかもしれませんが）これはこれで、日本語とは違った面白さ？を受けるんです。他の方はどうなのでしょうか？もし、このメッセを読んで自分もライジンコミック（月刊若しくは週間でも良いです。）かたは、どう考えてるか、教えて欲しいです＾＾。・・・・・かなり自己中な文ですみません＾＾；
話は変わります（ＣＨに関してですが・・・・変わってないジャン＾＾;）
　里奈様のメッセ読んで、「羨ましい～～＠０＠」とまず思いました。コルトパイソン３５７マグナム撃てたんですか！
ＣＨ，コルトパイソンでですが、僕もコルトパイソン３５７マグナム持ってます＾＾（ガス銃ですが＾＾；）これもＣＨ読み終えて撃ちたいな～～→良いな～～→欲しい!と、まあこんな感じいたんです。それで、その頃親と話をしていたら父が、「昔「ＧＡＮ」と言う月刊雑誌読んでいて・・・・」其れを聞いてさりげなく「漫画でコルトパイソンっていう銃があったんだけど、どんなの？」と聞いたら、父がその話にのって・・・・ガンショップへ＾＾￥色々見てると父に「これがそうなんだ～」とあれこれ言っているうちに、ついに手に入れました＾＾￥（これを読んで悪いやっちゃ＾＾；と思われるかもしれませんが、それでもいいです。＾＾；本当の事ですから）それから、まああれこれガンの話で盛り上がって、今では、ガス銃三丁（そのうち１つがデリンジャーと言うガンで大きさは手のひらサイズで、ポッケや、女性が持ってるハンドバックにも入るくらい小さい銃です）、エアーガン二丁、電動ガン一丁（すべて１８歳以上推奨です）。あ、そそこれら全て親の許可を得て買って貰った物です＾＾。
これらを持ってて、困る事は撃てる場所が無い；；事です。
どこか広くて人との距離感が最低でも１００ｍは保てる場所はないもんですかね～＾＾；（そうでないと、玉が当たる恐れもありますから＠０＠；）
は！いかんいかん＾＾；
あ～～早く漫画禁止令解禁にならないかな～～；；ＣＨが～ＡＨが～それから・・・・・・＠０＠おおおおおお！読みたい！！
は！いかん＾＾；暴走してた。
あ！話題変えますが、皆さんは地震対策とかは、しているんでしょうか？僕は愛知県に住んでるので＾＾；地震が起きたら、運任せです＾＾；。
う～ん＞－＜新聞読んでて一言「これからどうなるんだろ～～？？拉致、イラク、北朝鮮問題、景気、地震、地球環境、それから・・・・・う～～ん沢山有り過ぎる＠０＠；。」
タダ言える事は、核問題より、地球環境に関する問題を優先すべきだと思います。（だって、戦争で何が解決されるんでしょう？ただ、生まれるのは、悲しみ、憎しみ、苦しみ、形を変えれば、テロリズムなど・・・・まぁ、一時的に景気が国によっては回復、好況にはなりますが・・・）しかし、人間をはじめとして、動物植物皆、地球で生きてるんですよ。その地球が今、危ない（人で言えば癌ステージⅢぐらいかも？）まぁ、「きれい事ぬかすな！」と言われればそうかもしれませんが、上で言ったことも事実です。まぁ、これらの問題は、この地球で生きてる人間一人一人の意識でしか解決できないくらい、大きくかつ難しいと思います。
　綺麗事言うな！とか、現実を見てものを言え！とか、言われるかもしれませんが、まぁ、ここに独り馬鹿が言ってると思えばいいことですからね。ただ、・・・現実に囚われ過ぎて、日本が暗い、世界が疑わしい、なのも事実かもしれませんがね・・・・・。
＠０＠；！偉そうな事書いてすみませんでした。でもこれが今の僕の一瞬で思った心境です。＾＾；
　長々かいてすみませんでした。ではこの辺で失礼させてもらいます＜（。。）＞。
　またね～～＾＾

### [9384] 謝麟	2003.01.16 THU 10:37:32　
	
to SMTP
Hi.Yes,I can repair my computer by myself,because I feeling interesting,and I can do it.somtimes,I help my friend to repair computer,assemblage computer.
and thank you to give me the word"謝"
(smile).


to 里奈
first,I wish all of your dream become true.and,I want an overseas close friend
too.so.what do you want to know about me?well!I'm a man,born in 1978,with long hair(faint),(^Q^)！like comic,sport,movie,music.....
and,like giggle to the mirror.
en,do you want see my photo?tell me your
E-mail.
And what's my appearance in your mind?

### [9383] まっちゃん	2003.01.16 THU 09:21:05　
	
はじめまして！！
つい最近、シティーハンターを古本屋で見かけ
懐かしくて思わず購入したのですが、それ以来どっぷりはまってます。（勿論A・Hも買ったよ！！）
アニメでしか観たこと無かったけど、改めて漫画で読むと
色々と真実（僚の出生・海坊主の本名etc・・・）が発見でき、多々驚きと感動の連続です。それになんといっても「もっこり！！」。アニメで観てた頃は俺がガキだったせいか、いまいち訳わかりませんでしたが、今ではバッチリ朝モッコです。←わけわかんねー！！
まぁーともかく、この掲示板にこれからお世話になると思うので皆様、北条先生、宜しくお願いします。

### [9382] 千葉のOBA3	2003.01.16 THU 08:20:32　
	
おはようございます！きゃー！「もっこりメッセージ」が更新されてる！きゃー！ＣＨフルカラーですって！？きゃー！！
ぜひぜひ、実現してほしいです！北条先生！！

さー、めでたいなー、今日はバン木だしーーー。

９３６６　Ｐａｒｒｏｔさま　　あーーー、Ｐａｒｒｏｔさまのミニ、私の「もどき」より大きいー！いいなー！私のは９センチの３．５センチってとこよ。うちの近所の中古車売ってる店では、赤にルーフ白のミニが１１５万で売ってます。最初１３５万だったのが、値下げしたの。「あれが、１１５円になったら私が買う。」と､家族に宣言してます。（なるか！）わーーーん、クイズ、カンで答えても、わかんないーー！じゃ、２！！　

９３６７　たろももさま　　ホント！手数料もったいない！土日だったのかしら？（細かいなーーー！）だって今うちなんて、お金に火つけて燃やしてる様なカンジだもん・・・。子供大きくなると、お金かかりますよーーー！（脅してどうする！？）

９３７２　無言のパンダさま　　そうですねー。なんだかんだ言ってても、海ちゃん信宏には、甘いのかも・・・。パーパになってしまったのねーーー。ＣＨのフルカラー化楽しみですねー！

９３７４　いちのさま　　なにーー！里奈さまを捕獲！？きゃー！やっぱり捕まえたいのね！！どうする里奈さま！？しかし、フランス語ってそんなにムズイの？息子にも言っておこう・・・。（英語だってわからんのにねー。）

９３７７　葵さま　　見ましたよ！葵ちゃんがいっぱい生まれたニュース（？）「あんな」って名前もカワイイよねー。私と姉は、二人とも「赤毛にアン」のファンだから、子供が女の子だったら、「アン」って呼べる名前を考えてました。・・・二人とも叶わぬ夢でした・・・。（ワハハ・・・。）

９３７８　マイコさま　　マウスでラムちゃん！？すごいなー！私前のパソコンの時、年賀状にしようと、マウスでネズミ（マウスでマウス！？ネズミ年だったのね。何年前？）描いたけど、却下されました。今のパソでなんて、どうやるんだろーーー？？？（おいーー！汗）マイコちゃんの描いたラムちゃん見たいなー！私のアドレス、里奈さまが知っている・・・ハズ。

９３７９　しゃぼんさま　　おひさしぶりです。ＣＨのフルカラー楽しみですねー。しかし、「シティ」「シティー」私どっちやってたろ？わからないーーー？？？

９３８０　里奈さま　　絵文字のアザラシ、超カワイイー！！しかし、いちのさま、里奈さま捕獲計画を、考えているようですぜ！どうします！？やはり、電気ムチで戦う？それともあっさり捕まる？？？お兄様も海外射撃組かい！？しかも３５７マグナム？しかも「冴羽りょう愛用」って説明受けたの？まー、りょうちゃんったら国際的☆

さー、今日もがんばりまーす！！（ちょっとアタマボケてるけど・・・。）

### [9381] 無言のパンダ	2003.01.16 THU 01:30:27　
	
こんばんわ★

ひさびさに長い先生の「もっこりメッセージ」が♪
それも「ＣＩＴＹ　ＨＵＮＴＥＲ」のフルカラー版が実現するかも～？！
先生の自己満足どころじゃないですよ！ぜひぜひファンのためにも実現させてください☆
