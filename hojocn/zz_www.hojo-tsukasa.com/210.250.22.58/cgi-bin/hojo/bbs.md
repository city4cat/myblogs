http://210.250.22.58/cgi-bin/hojo/bbs.cgi  
http://210.250.22.58/cgi-bin/hojo/bbs.cgi?function=loglist  

https://web.archive.org/web/20010206230715/http://210.250.22.58/cgi-bin/hojo/message.cgi

注：[http://210.250.22.58/ | web.archive.org](https://web.archive.org/web/*/http://210.250.22.58/*)页面上显示，该IP地址下还有原哲夫的[BBS页面](../hara/bbs.md)、[Diary页面](../hara/diary.md)和[editor页面](../hara/editor.md)。

注：web.archive.org上只记录了2001.02.06～2001.02.07的数据，相信该时间段内还有很多其他数据没有被记录。  

![](../../../img/hojoheader_01.gif)  

### 響	2001.02.07 WED 05:45:03　
	
あと、北条先生のもっこりメッセージのＬＯＧをみせて頂きまして１言。。。

北条先生＞＞＞
コミックバンチの今コンビ二などでうられてるもの。。
あれで私はまたまた火がつくきっかけになったからうれしいの
ですが、どうせ出るならＣ・Ｈ２が読みたい！！！
ってこれ私のわがままだね・・・
でも、アニメ版の下ろしてコミック化されてない話とか
見てみたいなぁ～、でも、書かなきゃならないから、
そんな暇、お忙しい先生に無いですよね・・・
でも、どうせ出すなら、ちょっと変わったカンジのほうが
今よりもっと引き付けられてしまう気がします。。。
Ｃ・Ｈは、多少変わってもりょうちゃんは絶対変わらない
ちょっとやそっとで変わるような男じゃない（笑）から
そのまんまだすよりおもしろいかも・・・
なんて、生意気に思ってみたりしました。。。
なんも知らんファンがごめんなさい・・・でも、思った事
書けって言ってたから言ってみました(≧∇≦)

これ私から先生へのＸＹＺです（笑）
次の連載楽しみじゃ～、Ｃ・Ｈだってらいいなぁ～なんて
考えてニヘラニヘラしてま～す！！！

我也看了一下北条老师的Mokkori Message Log，有几句话要说。。。

北条老师> > >  
在Comic Bunch现在已经在便利店和其他地方出售了。。
因为那成为了我又燃起热情的契机所以很高兴，不过，反正要出的话想读C·H2 ! ! !
这是我的任性啊…
但是，很想看看动画版里有，但漫画里没有的故事啊~，但是，因为必须要画，所以百忙之中的老师没有这样的时间吧···
但是，如果要出版，如果它有一点不同，我想我会更喜欢它…
即使C.H有一点变化，獠永远不会变，他不是那种稍微改变一下就会变的男人（笑）…
我真的很狂妄地想着…
什么都不知道的粉丝真对不起……但我说了我所想的，因为你让我畅所欲言(≧∇≦)  

这是我给老师的XYZ(笑)
期待下次的连载~，如果是C·H就好了~我想了想，开始减少~ ! ! !（译注：待校对）


### 響	2001.02.07 WED 05:03:01　
	
おはよ～ございますぅ～！！！
やっおぱりかきこの量多いですね。。。昨日長々とかいちゃって
すいませんでした。でも、とっても嬉しかったのぉ～(≧∇≦)
やっぱりここにかきこしてよかったです。
いろいろ情報頂けるし、なにより初日に神谷氏のかきこに
出会えたっていうのが１番！！！
ちょくちょく顔だすのでよろしくです！！！

すぐるさん＞＞＞  
お返事どうもありがとうございました。小説だったのか～！
気がつかんかった・・・ダメね、私・・・（泣）
探してみます！あとレーザーディスクですが
ビデオの方って、やっぱり中古屋さんかオークションしかないですよね？私の家はレトロ（良い言い方をして）なので
ＤＶＤもレーザーも見れません・・・
もう、どうにもならんなぁ～わたし。（笑）
どこか良いお店ご存知でしたら教えてください！
（って、しょっぱなから頼み事ばっかですいません）
これからも見かけたら声かけてください！
それでは、おやすみなさ～い！！

早上好~大家好~ ! ! !
这么多的发帖啊…昨天写了那么多真是不好意思。但是，我非常高兴~(≧∇≦)
还是在这里发贴好。
能收到各种各样的信息，最重要的是第一天就能见到神谷氏的帖子! ! !
我会经常来，请多关照! ! !

すぐるさん＞＞＞  
感谢您的回信。原来是小说啊~ !
没注意到…不行啊，我…(泣)
我找找看!还有Laser Disc、录像带只有在二手商店或拍卖会上才能买到吧？ 我的家是retro（因为想用一个更好的词），所以我不能看DVD，也不能看laser……
我已经无能为力了。(笑)
如果您知道哪里有好的店的话请告诉我!
(一开始就全是拜托你的事，真对不起)
今后如果看到也请打声招呼!
那么，晚安! !


### マット	2001.02.07 WED 03:08:33　
	
＞暁子さんへ　ゴールデン　アイと　トゥモローネバーダイ
　です　残念ながら　新作では　別の方がふきかえてらっしゃいます。　
　　本日二回目　書き込み３回目　やり過ぎました　すいません
　でも　昨日入れんかったぶん　今日来てるだけだもん　って　もう次の日か。。。　寝なきゃ。　　

＞暁子さんへ  
Golden　Eye 和 Tomorrow Never Die  
很遗憾新作换了别的人。
今天我第二次发文，第三次写太多了，对不起。
但我今天在这里只是因为我昨天没能进来…我得睡了。


### 暁子（茨城・古河）	2001.02.07 WED 01:47:39　
	
書き込みすぎ？akikoです。
ふと、私が小さい頃なんであんなにロングのストレートにこだわっていたのか・・・瞳みたいにかっこよくありたかったからかも・・・（現実は厳しいですけど^^;）

＞マットさん
どの007で神谷さんがふきかえているのですか？
教えてください！

J-books私は一冊だけまだ手に入れていないのです。
う゛う゛う゛(・_・、)しくしく

发文太多?我是akiko。
突然，我小时候为什么那么执着于长直发呢···也许是因为想要像瞳一样漂亮···(虽然现实很残酷^^;)

＞マットさん  
哪部007是由神谷配音的呢?
请告诉我!

J-books我只有一册还没有入手。
唔唔唔(·_·、)唔唔唔


### けんじ	2001.02.07 WED 00:12:27　
	
今、車を買おうか悩んでいます
その車はミニなんです色は赤か青かで悩んでいます
皆さんどうおもいますか？
ちなみに今は１８０ｓｘにのっています
ミニはもう生産が終わってしまったので今しか新車で買う
チャンスがないんです　
同じ悩みを持っている人はいますか？

我现在正在考虑买辆车。
这辆车是一辆Mini，我想在红色和蓝色之间做出决定。 你们有什么看法？
顺便说一下，我现在开180sx。
Mini已经生产完了，没有机会买，现在只能买新车。
有同样烦恼的人吗?


### すぐる	2001.02.07 WED 00:08:15　
	
＞響さん
JーＢＯＯＫＳとは、集英社から出している
ジャンプの作品などの小説です。
シティーハンターは「CITY HUNTER」
「CITY HUNTER SPECIAL」「CITY HUNTER II」
「CITY HUNTER SPECIAL II」の
４冊が出ていたはず。
スペシャルはそれぞれＴＶスペシャルの
１作目と３作目の小説版。
そのほかの２つはオリジナルストーリーの小説です。
もちろん北条先生の挿絵満載です(笑)
ですから本屋で探す時は小説のコーナーを探してみては？
J-BOOKSは赤いのですぐわかるはずです。
ちなみに「CAT'S EYE」も出てますね。
ちゃんと持ってます(笑)

レーザーディスクですが、
ちょっと前に書き込みしたんですが
俺はヤフーオークションで手に入れました(笑)

＞響さん  
J-BOOKS是集英社出版的Jump作品等小说。
「CITY HUNTER」
「CITY HUNTER SPECIAL」「CITY HUNTER II」
「CITY HUNTER SPECIAL II」这4册应该是City Hunter的作品。
Special分别是TV Special的第1部和第3部的小说版。
另外两个是原创故事的小说。
当然是北条老师的插图。(笑)
所以，在书店找小说的时候，试着找找看怎么样?
J-BOOKS是红色的，应该马上就能看出来。
顺便一提，还有「CAT'S EYE」。
我有这本书(笑)

这是一张Laser Disc，我在Yahoo Auction上买的。(笑)


### 芙蓉（東京都町田市）	2001.02.06 TUE 23:09:48　
	
昨日、アクセスできなかった理由が分かって安心しました。
私も自分の家のPCが壊れたのかなって思ってたんで。

神谷さんのカキコうれしい�
私は中学１年の時と２年の時に、
SMAPの握手会に行った事があるんです。
で、その時は吾郎くんが好きで行ったんですけど、
メンバーと握手している時に、
なかでも木村君のパワーがあるというか、
綺麗な瞳をすごく覚えています。
今考えると、かっこよさのなかにも、そういう、
人を引きつけるものを持っている人なんだなって
思ってます。

知道了昨天不能访问的理由之后安心了。
我也以为是自己家的电脑坏了。

神谷先生的帖子？
我初中1年级和2年级的时候，去过SMAP的握手会。
那时候因为喜欢吾郎而去的，和成员们握手的时候，特别记得木村那充满力量的漂亮的眼睛。
现在回想起来，我觉得他在帅气中也有那种吸引人的东西。


### さお（神奈川／江ノ島が見え～てきた♪）	2001.02.06 TUE 23:01:43　
	
あで？オイラは丸一日このサイトに入れんかったが？
カキコの時間見てると、ちゃんと入れていた人もいるよーですねぇ･･･。
ずーっとつながらなかった、っちゅー訳でもないんかなぁ？
ま。いっか。。こーしてまた来れるようになったんだし♪

久々の神谷さんのお出ましですなー！うれしっ。
でも、今回は”あっくん”ではないんですね。なぜでしょお？
なんて。そんなことどーでもいいですよね。（汗）
そっか。キムタク、かっこよかったですか。いーなぁ。私も見たぁい。
本？北条先生と原先生のイラスト付き？？
う～ん。早く本屋で見てみたいです。

是吗？我一整天都上不了这个网站?（译注：待校对）
看了下发帖的时间，我发现有些人能够进入这个网站···。
我想，是不是我一直都上不了？
嗯。那就好。我现在可以再来了♪

久违的神谷先生出场了啊!太高兴了。
但是，这次不是“あっくん”。为什么呢?（译注：神谷在BBS的用户名曾经是“あっくん”。参见[Log List No.153](../../../bbs/bbs_log-list-153.md) ）
怎么可能。那种事都可以吧。(汗)（译注：待校对）
这样啊。キムタク很帅吗?好啊。我也想看。
书?附带北条老师和原老师的插图? ?
嗯。想早点去书店看看。


### たれたれつー（ふくしまからです。）	2001.02.06 TUE 22:29:13　
	
こんばんは。
今日久しぶりに､「ベイシティーウォーズ」を見ました。「百万ドルの陰謀」同様この作品だけ美樹の声が小山さんじゃないのが､ちょっと残念ですけど､CHのアニメシリーズの中で､好きな作品の一つです。（みんな好きなんですけど。)あーあDVDとかでないかなぁ・・・北条先生！コア・ミックスの方々どうでしょうか！！  
晚上好。
今天久违地看了「Bay City Wars」和「百万美元的阴谋」一样，只有这部作品美树的声音不是小山，有点遗憾，在CH的动画系列中，这是我喜欢的作品之一。(虽然大家都喜欢。)啊，不是DVD什么的吗?北条老师!Coamix的各位怎么样! !


### 華燐（福岡県北九州市）	2001.02.06 TUE 22:18:45　
	
はじめまして。
小学生の頃からシティーハンターが大好きで、北条先生のファンです。
今は友達にF.Cを勧めて、ファンを増やしています。
先生のＨＰが有るとは知らなかったので見つけられてうれしいです。
これからもちょくちょく覗かせていただきます。

初次见面。
我从小学开始就很喜欢City Hunter，是北条老师的粉丝。
现在向朋友推荐F.C，增加了粉丝。
因为不知道有老师的HP，所以很高兴能找到。
今后也请允许我经常看一下。


### マット2	2001.02.06 TUE 22:16:21　
	
　　きのう　鬼のように何度もここに　しかもいろんな方面からアクセスしては　　なんで～～～！！！　と叫んでいましたが　そうゆうことだったんですね。　よかった　わたしのpcが　壊れたんじゃなくって。　ほっ。  
昨天像鬼一样好几次从各个方面访问这里为什么~ ~ ~ ! ! !，原来是这么回事。幸好不是我的电脑坏了。好吧。（译注：待校对）


### マット	2001.02.06 TUE 22:14:19　
	
　うわー　早速本の予約しなきゃ。　神谷さんだいすき�　今も　007吹き替え版で見てたんです。ああ　かっこいい。
　シリアスなときのりょうとかぶってる気がしてよけいにめろめろです。　なんだか　ピアース　ブロスナンが　りょうにみえてきたぞ。　やっぱ　飲み過ぎたかなァ。。。

哇，得赶紧预约书了。最喜欢神谷先生�现在也在看007的配音版。啊，好帅。
感觉严肃时象獠一样。皮尔斯布鲁斯南开始像獠了。　也许我喝得太多了…


### 風（山口県）	2001.02.06 TUE 21:53:35　
	
神谷さんの出された本、見つけました。北条先生と原先生のイラストが付いてました。  
我找到了神谷先生出版的书。附有北条老师和原老师的插图。（译注：待校对）  


### 猫の目ちゃん♪	2001.02.06 TUE 21:53:14　
	
北条司先生こんにちは！！！
私は「キャッツアイ」大好きっ子です！
やっぱり瞳が大好きです！
小学校の時マンガを全巻読みました｡

日本画を習っていたママは「この人の漫画の色のつけ方参考になるわ～」ってよく言ってました｡
あと、表紙の裏に先生の言葉で「いまどきのアイドルは痩せっぽっちの貧相な人が多くてダメだ」とか書いててママも「この人いい事書いてるわねえ～」ってよく言ってたました！

「キャッツアイ」の最後は今読んでも涙で漫画が読めなくなります｡
やっぱり海っていいよね～♪
「Ｆ・ＣＯＭＰＯ」の最後のシーンも海でよかった！！！

ところで私の夢は瞳のように屋根と屋根を飛び回ることです！
さようなら。

北条司老师你好! ! !
我最喜欢「Cat'sEye」!
还是最喜欢瞳了!
我小学的时候读了全卷漫画。

学习日本画的妈妈经常说:“可以参考这个人的漫画上色方法哦～”
还有，封面背面用老师的话写着“现在的偶像很多都是又瘦又穷，这可不行”，妈妈也经常说“这个人画得真好啊~”!（译注：待校对）

「Cat'sEye」的最后现在看也会因为眼泪而看不下去漫画。
大海果然很好啊~♪
「F·COMPO」的最后一幕也是在大海，太好了! ! !

对了，我的梦想是像瞳一样在屋顶和屋顶之间飞来飞去!
再见。


### レノン！（１９）	2001.02.06 TUE 21:41:55　
	
こんばんは～！2回目で～す！常連になろうなどと企んでおりま～す！！（何故かハイテンション！）

さてさて、最近、ついにまた、今年も”あの言葉”を見かける季節がやってまいりました。雑誌を見たり、百貨店や繁華街を歩いたり、新聞の折り込みチラシを見て「今夜のおかずは何にしようかしら？」と思ったりした人はもう気付いてきているはずです。あの日が刻一刻と迫っていることを。

そう・・・・・・”ヴァレンタイン”だ～～～～～！！！（何故かハイテンション！？）

ヤな事を考えるのはよそうじゃないか。特に男性諸君（俺だけか？）よそうよそう。
そういえば僕の知っている北条作品（Ｃ・ＨとＦ・ＣＯＭＰＯ）ではバレンタインについての話が無かったような・・・。（あったらゴメンナサイ）
と思っていたら、「今年のリョウと香、雅彦と紫苑はどんなバレンタインを過ごすのだろうか・・・？」「う～ん、やっぱり紫苑はチョコを渡すのか。でも、男かも・・・。」「雅彦は映研の先輩に女装させられて、宣伝のためにチョコ配ってるのかな？」などと一人で空想に耽ってしまいました。
あぁ、どんどんふくらんでいく～～～ッ！

収拾が付かないのでまた今度！

晚上好~ !这是第2次~ !试图成为常客! !(不知为何情绪高涨!)

话说回来，最近，今年又到了能看到“那句话”的季节。看杂志，逛百货公司和闹市区，看到报纸上的宣传单，“今晚吃什么菜呢?”这样想的人应该已经注意到了。那一天一分一秒地逼近。

对······是“Valentine（情人节）”～～～～～! ! !(不知为何情绪高涨!?)

别想些不雅的事。特别是各位男性(只有我吗?)好了好了。
这么说来，我所知道的北条作品(C·H和F·COMPO)好像没有关于情人节的故事……(有的话对不起。)想着「今年的獠和香、雅彦和紫苑会度过怎样的情人节呢…?」「嗯，紫苑果然还是要送巧克力啊。不过，说不定是男的…。」「雅彦是被映研的前辈逼着穿女装、为了宣传才送巧克力的吗?」我一个人沉浸在空想中。
啊，越来越多~ ~ ~ !

无法收场，下次见!


### テツ	2001.02.06 TUE 21:10:40　
	
うぉっマジで神谷さん？（遅い？）ファンです～子供の頃のバビル２世、キン肉マン、当然リョウちゃんも大好きでした～。ホントにカキコしてたんや～すごい～
ところでうちも持ってたな～ハンマーキーホルダー・・・どうしたかなぁ？う～ん

真的是神谷先生吗?(太晚了?)我是粉丝~小时候的巴维尔2世，筋肉人，当然獠也很喜欢~。真的在发帖啊~好厉害~
话说我家也有啊~锤子钥匙圈···怎么了呢?唔~


### みなみ（東京）	2001.02.06 TUE 19:46:32　
	
みなみです。

いくつか質問のあったアニメのメイキングスペシャルは、
神谷さんや、伊倉さんが司会進行しながら
製作現場の裏側を紹介するって形でした。
劇場版「愛と～」の時は、話の紹介や、シリーズの名曲集、
アフレコ風景がメインでしたね。
関西限定バージョンのは、確か、神谷さん達が、
たくさんのセル画を前に話を進めてて、
神谷さんが、リョウのセルをひっくり返して、
「普段カッコいいんですけどね、こうすると・・・・
　あらん、丸坊主ぅ～！」
なんて言って遊んでらした記憶があります。
・・・・でも、１０年近く昔の記憶ですけど。
あれから今まで、まさか毎日リョウさんに頬ずりして
生きてきた訳ではないので、
ブランクがあった分、やっぱり記憶が飛んでるトコもあります。
なんで手元にアニメの台本があるのかとか、
キャラの名前が「これ、ご家族のだぁ」って知ってたのか、
なんて、すっかり忘れちゃって、未だに謎のまま・・・・。
たぶん、誰かに教えてもらったりしたんだろうなぁ。
（なにせ、九州在住の小学→中学生だったから･･･）

今はこうして情報交換できて、大変よいですねっ（＾－＾）


我是みなみ。

有几个问题，关于动画制作的特别节目，是由神谷和伊仓一边主持一边介绍制作现场的背面的形式。
剧场版「爱与~」的时候，故事的介绍，系列的名曲集，后期录音风景是主要的。
关西限定版本的时候，神谷他们把很多Cell画放在前面，神谷翻开獠的Cell，说「平时很帅的，这样的话……哎哟，光头啊~ !」我记得他说了什么，然后就玩了起来。
····但那是近10年前的记忆。
从那以后到现在，我并不是每天都和獠在一起，所以有空白期，也有记忆遗漏的地方。
为什么手边会有动画的脚本？角色的名字是「这是家人的」，我完全忘记了，至今仍是个谜…。
大概是有人能告诉我吧。
(不管怎么说，我从小学到初中是住在九州的……)

现在能这样交换信息，真是太好了(^ - ^)



### ひま子（西日本の某所）	2001.02.06 TUE 16:03:00　
	
神谷さんのカキコは、あのものすごい量の過去ログの中にいくつもあります。
北条先生のもありましたね。
まだ知らない人は、がんばって探してみては・・・？

神谷先生写的帖子，在那大量的历史log中就有好几个。
还有北条老师的。
还不知道的人，努力找找看吧……?


### 天城　小次郎（愛知県名古屋市）	2001.02.06 TUE 13:42:24　
	
新作って何かな？暇な時は、そればっか考えてます。
シティーハンターが復活するのかな？
それとも全く別のお話になるのかな？
前者は北条先生が否定しているので無理かな？
とにかく！
五月が待ちどうしくてしょうがないです！！
うーん、あと約３ヶ月か、長いです！！

新作是什么呢?我一有空就想。
City Hunter会复活吗?
还是完全不同的故事呢?
因为北条老师否定了前者，所以不可能吧?
总之!
五月已经等得不得了! !
嗯，还有3个月左右，好漫长啊! !


### MAKOTO（静岡県にて発見！）	2001.02.06 TUE 12:37:44　
	
昨日、私は絶対にスマスマを見るぞ！と思っていたのですが
残念ながら、パソから顔を上げたとき、すでに１２時を回って
ました…。
でも話題に上らない所を見ると、別になにもなかったってことですね。

しかし、神谷さんがホントにカキコしてるなんて…。
らじ＠？神谷さんのらじおですか？この間、よっしゃ聞くぜ！
と思ったのですが、いつどこでやってるって知らなかった…。

ハンマーのキーホルダーはあったんだ。
でもやっぱりこんぺいとうはないんだね。そっか。
だれか作ってー！
鈴になってるこんぺいとうとかあったらめっちゃほしい！
リョウちゃんは退治できないけどね(笑)ほし～い！ 

昨天，我下定决心要看Sumasma!很遗憾，当我从电脑上抬起头来时，已经12点多了…。
不过从没有成为话题来看，也就是说没有什么。

但是，神谷竟然真的发帖了…。
Raji@?是神谷先生的Raji吗?这段时间，好好听着!
但我不知道它在什么时候和什么地方播放…。

锤子的钥匙圈是有的。
不过果然没有竞标。这样啊。
谁来做!
要是有变成铃铛的参赛者就好了!（译注：待校对）
虽然不能消灭獠(笑)星~好!（译注：待校对）