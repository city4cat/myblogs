http://www.hojo-tsukasa.com/project/project.html  


![](../img/headder.jpg)  
![](./img/title.gif)  

## https://web.archive.org/web/20020810204618/http://www.hojo-tsukasa.com/project/project.html
![](/web/20020810204618im_/http://www.hojo-tsukasa.com/project/ch_pw/bg_ue.gif)

![](./ch_pw/pw_title.gif)  
![](./ch_pw/pw_all.jpg)  

　北条司作品において決して欠かすことのできない名脇役達─。  
あるときは登場人物の怒髪の怒りを具現し、またあるときは言葉にならないやるせなさを代弁し、さらには"愛”という心の交流まで描きだすあのキャラクター達の登場です。

　原画よりおこしたデータに基づき、クリスタルの表面から内部に向けて照射されるレーザーでひとつひとつの点を描画し、その集合体で美しい三次元アート"３Dレーザーアートクリスタル”を実現しました。各キャラクターと"CITY HUNTER"のロゴが立体的に浮かび上がります。

　飽きの来ないデザインに仕上がっておりますので、ペーパーウェイトとしての用途はもちろん、お部屋のインテリアからカップラーメンの重しにまで皆様の工夫次第で様々な使い方が─。

  

[![](./ch_pw/pw_hammer_s.gif)](./ch_pw/pw_hammer.md)  
**ハンマー**  
CH0001PH  

[![](./ch_pw/pw_karasu_s.gif)](./ch_pw/pw_karasu.md)  
**カラス  
**CH0001PK

[![](./ch_pw/pw_tombo_s.gif)](ch_pw/pw_tombo.md)  
**トンボ  
**CH0001PT

[各商品をクリックすると拡大されます]  
  

**縦×横×高さ**

50mm×50mm×50mm

**重量**

約310g

**材質**

オプティカルクリスタル  
（鉛含有量100%カット）

**価格**

各4,500円（税別）＋送料500円（税別）

**限定数**

**各220個（売り切れ次第終了）**

　

　

　**お申し込みは4月1日正午より、[COAMIXウェブサイト](https://web.archive.org/web/20020810204618/http://www.coamix.co.jp/)で開始いたします。**　お申し込みよりおよそ10日でお届けにあがります。料金の支払いは、佐川コレクト（代引き）でお願いしております。クレジットカードでのご購入は現在のところできませんのでご了承ください。

[![COAMIXページへ](../img/button_gocoamix.gif)](https://web.archive.org/web/20020810204618/http://www.coamix.co.jp/)  
ご購入はこちらにて

## https://web.archive.org/web/20010707023515/http://www.hojo-tsukasa.com/project/project.html  

この商品開発企画室では、北条司オリジナル商品の企画開発やライセンス商品の紹介を随時行います。ご希望の商品やご意見などがありましたら、[Contact（お問い合わせ）](../contact/contact.md)から商品開発企画室までメッセージをお送りください。

![](./zippo2001/img/zippo1.jpg)

## 「CITY HUNTER」ZIPPO 7月17日販売開始決定!  
北条司作品に登場する愛すべきサブキャラクターをモチーフにしたオリジナルZIPPOライターが完成しました。  
表面にはカラス、裏面にはトンボと北条司のサインというお馴染みのキャラクターのオシャレなライターです。  
  
　７月１７日正午販売開始！  
　販売価格　￥８，５００（消費税別）  
　送料全国一律　￥５００（消費税別）

　

* * *

![](../img/icon_coamix.gif)
このホームページに掲載のイラスト・写真・商標の無断転載を禁じます。  
Copyright(C)2001,2000,Coamix Co.,Ltd. All Rights Reserved.