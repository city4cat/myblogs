http://www.hojo-tsukasa.com/portfolio/portfolio.html  
https://web.archive.org/web/20020802195049/http://www.hojo-tsukasa.com/portfolio/portfolio.html  

<a name="top"></a>    

![](../img/headder.jpg)  
![](./img/title.gif)  

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/80.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/otoko.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_otoko.gif)  

週刊少年ジャンプ  
'80年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('otoko.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/81.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/3rd.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_3rd.gif)

原作：渡海風彦  
週刊少年ジャンプ  
'81年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('third.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

　

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/cats.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_cats.gif)

週刊少年ジャンプ  
'81年～'85年  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('cats.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/82.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/space.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_space.gif)

週刊少年ジャンプ  
'82年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('space.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/83.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/ch_xyz.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_ch_xyz.gif)

週刊少年ジャンプ  
'83年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('ch_xyz.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/84.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/ch_de.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_ch_de.gif)

フレッシュジャンプ  
'84年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('ch_de.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/85.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/ch.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_ch.gif)

週刊少年ジャンプ  
'85年～'91年  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('ch.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/86.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/nekomanma.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_neko.gif)

週刊少年ジャンプ  
'86年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('neko.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/88.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/tensi.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_tensi.gif)

週刊少年ジャンプ  
'88年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('tensi.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/90.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/taxi.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_taxi.gif)

週刊少年ジャンプ  
'90年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('taxi.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/92.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/family.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_family.gif)

週刊少年ジャンプ  
'92年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('family.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

　

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/shoujo.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_shoujo.gif)

週刊少年ジャンプ  
'92年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('shoujo.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/93.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/sakura.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_sakura.gif)

週刊少年ジャンプ  
'93年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('sakura.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

　

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/komorebi.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_komorebi.gif)

週刊少年ジャンプ  
'93年～'94年

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/94.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/rash.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_rash.gif)

週刊少年ジャンプ  
'94年～'95年

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/95.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/aozora.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_aozora.gif)

原作：二橋進吾  
週刊少年ジャンプ  
'95年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('aozora.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

　

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/shounen.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_shounen.gif)

週刊少年ジャンプ  
'95年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('shounen.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

　

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/american.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_american.gif)

原作：二橋進吾  
週刊少年ジャンプ  
'95年（短編）  
[![詳細を見る](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/look_data.gif)](javascript:OpenWin('american.html','works','width=540,height=370'))

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/96.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/fcompo.jpg)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_fcompo.gif)

マンガオールマン  
'96年～'00年

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/shim.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/line.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/97.gif)

![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/parrot.jpg)

北条司短編集  
![](/web/20020802195049im_/http://www.hojo-tsukasa.com/portfolio/img/t_parrot.gif)  
'97年6月号～  
コミックとＣＧの素敵なコラボレーションで作り上げた一冊。  
集英社『メンズノンノ』と『ＢＡＲＴ』 で連載された北条司がデジタル時代におくる新感覚ラブストーリー。北条司が、すべての「恋愛中毒者」に捧げる大人の恋愛テキストブック！

　

　

[![PAGE TOP](/web/20020802195049im_/http://www.hojo-tsukasa.com/img/go_top.gif)](portfolio.html#top)