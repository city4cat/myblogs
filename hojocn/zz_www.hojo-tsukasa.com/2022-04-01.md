source: 
http://www.hojo-tsukasa.com/cat-c/1131.html

以下是中译文。[hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96937)    

#「Cat's?Eye」为庆祝其成立40周年，首次举办了原画展!


2022年5月13日（周五）至5月23日（周一），「Cat's?Eye40周年纪念原画展 ～再到城市猎人～」将在东京（3331艺术千代田1楼主画廊）举行！届时将有更多的人参加。

北条司的杰作Cat's?Eye40周年纪念，它继续以怪盗3姐妹--瞳、泪和爱--以及刑警俊夫和瞳的爱情故事吸引着所有人。
展览将展出约350幅原画，包括北条司为本次展览绘制的三姐妹的插图，以及罕见的彩色原稿、漫画原稿和最終話的整页画，还有「City Hunter」的彩色原稿。 这个展览将使参观者充分领略北条司的谱系和足迹，从「Cat's?Eye」开始，一直到「City Hunter」。

欲了解最新的详细信息，请点击这里→[EDITION88](https://edition-88.com/pages/catseye/)  

![](http://www.hojo-tsukasa.com/wp/wp-content/uploads/2022/04/catseye_mv.jpg) 

![](http://www.hojo-tsukasa.com/wp/wp-content/uploads/2022/04/catseye_01.jpg)  

**令人印象深刻的力度和质感！ 该展览展出了约350幅原画，主要是连载时的漫画原稿。**

完整地覆盖了「Cat's?Eye」的世界观! 该系列的情感又回来了!
展览将展出大量原画，涵盖Cat's?Eye世界的方方面面，包括三姐妹--瞳、泪和爱、俊夫和猫咪特别搜查队的侦探们以及小偷绅士老鼠的吸引力和令人振奋的行动。 北条司的原稿的魅力，与数字稿不同! 这是一个独特而宝贵的机会。

![](http://www.hojo-tsukasa.com/wp/wp-content/uploads/2022/04/catseye_02.jpg)  


**北条司的另一部杰作「City Hunter」！**  
最初的「City Hunter」的灵感来自于「Cat's?Eye」中出现的神谷真人（又名 "老鼠"），他是「City Hunter」的主角--冴羽獠的灵感来源。 通过展览黑白和彩色的城市猎人原稿，探讨了两个世界之间的联系。

![](http://www.hojo-tsukasa.com/wp/wp-content/uploads/2022/04/catseye_03.jpg)  


展厅内的其他宝贵文件和拍照的地点!
除了原画之外，还将展出当时的杂志和书籍以及用于绘画的材料。还可以看看会场上设置的表现「Cat's?Eye」世界的作品。还会有非常适合为SNS拍照的地点。

总共有100多件新的物品将被出售，以庆祝原来的展览!
总共有100多件新物品将被推出，包括由北条司为原创展览新绘制的插图的醒目物品。 有关产品的更多信息将于4月下旬在EDITION88网站上公布!

**活动概要**
活动名称：Cat's?Eye40周年記念原画展 ～再到City Hunter～。    
时间：2022年5月13日[星期五] - 5月23日[星期一]  
开放时间：11:00 - 20:00 ※展览的最后入场时间为19:00。  
地点：3331 Arts Chiyoda / アーツ千代田 3331　1层 Main Gallary  
场地网站：[https://www.3331.jp/access/](https://www.3331.jp/access/）  

「入場券」从4月9日（星期六）起在Lawson Ticket出售。  
入场券只在劳森票务公司（Loppi in Lawson and Ministop shops）以时区预约方式出售。  
开始销售：4月9日[星期六]，12:00-（预定）。  
门票：成人和大学生1,500日元（含税），高中生及以下1,000日元（含税） ※学前儿童免费入场。  
　　　　　　带目录的门票：3,500日元（含税）。  

①入場特典  
4张原创40周年Cat's Eye通告卡（4种类型）中的一张将被随机包括在内! 双面彩色印刷，正面是Cat's Eye通告卡的设计，背面是北条司的新插图，以纪念原始展览。  
※入场奖金将在会场发放。 不向参观者提供免费入场券。  

![](http://www.hojo-tsukasa.com/wp/wp-content/uploads/2022/04/catseye_04.jpg)  

②目录  
A4大小，80页，全彩（计划）。  
该目录也可以在展览现场购买，含税价格为2200日元。  
图册也可以在展览现场购买，含税2200日元（但是，图册有可能被卖光，所以如果你想确定购买，请购买图册的入场券）。  
(然而，目录可能已经售罄，所以如果你想确定，请随目录购买入场券。  

![](http://www.hojo-tsukasa.com/wp/wp-content/uploads/2022/04/catseye_05.jpg)  

【关于入场】  
※时间段为（11:00-/12:00-/13:00-/14:00-/15:00-/16:00-/17:00-/18:00-/19:00-），将按时间划分。  
※没有替换系统。  
※入场券仍然可以在活动当天购买。  
※进入销售区也需要入场券。  
※活动的内容可能会因某些原因而改变或取消。  
目前正在采取措施防止新的冠状病毒感染的传播。 我们请您合作，在您来参加活动时戴上口罩。  

【关于门票的查询：[劳森票务](https://l-tike.com/contact/)】  
有关其他规定和细节，请参考[EDITION88](https://edition-88.com/pages/catseye/)。  




P.S. 一些大图，源自 https://edition-88.com/pages/catseye/
![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/tokuten_360dpi_800x.jpg?v=1648712346)  

![](https://cdn.shopify.com/s/files/1/0587/8009/0526/files/zuroku_360dpi_800x.jpg?v=1648712371)  






























