
# CE的一些信息   ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96941))  

## 卷-话-标题：  
[CAT'S EYE 貓之眼 （完全版）  ](./ce_subtitle_jp.md)(JP)  

[CAT'S EYE 貓之眼 （共18卷）  ](./ce_subtitle_18.md)  
[CAT'S EYE 貓之眼 完全版（共15卷）  ](./ce_subtitle_15.md)  

[CAT'S EYE 貓之眼 （完全版）  ](./ce_subtitle_en.md)(EN)  

注：  

- 在HTKJ中，每话标题被称为"Subtitle"，意思是"子标题"，而不是"字幕"。  



