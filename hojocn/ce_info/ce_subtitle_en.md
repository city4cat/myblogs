## Cat's Eye Subtitles 
source: 
[URL1](https://mangahot.jp/site/works/e_R0027), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=e_R0027) 
(2024/12, it's updated to "32.Lost Memories")  

1.Sexy Dynamite Gals  
2.Skyscraper Tightrope  
3.A Tricky Love Affair  
4.Do You Like Blondes?  
5.Vol de Nuit is a Dangerous Fragrance  
6.Kittens to Spare  
7.Enter the Gentleman Thief  
8.The Pesky Rat  
9.Memories of Dad  
10.The Cat's Helping Hand!  
11.I Love Hitomi  
12.Phantom of the Cat  
13.Dad's Self-Portrait  
14.Get the Goddess of Mars  
15.The Swooping Hawk!  
16.Dog Crisis!!  
17.Armor Dog vs. Ai  
18.Cat's Eye Is a Woman  
19.I Wish We Could Stay Like This  
20.Ten Minutes, On the Dot  
21.Stumped and Stubborn  
22.Hands Off My Diary!  
23.Two Asatanis  
24.Visitor in the Snow  
25.Little Hurricane  
26.Burglary 101  
27.Back to Back  
28.Lost Memories  

