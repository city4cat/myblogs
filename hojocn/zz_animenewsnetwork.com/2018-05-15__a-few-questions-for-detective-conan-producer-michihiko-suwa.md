https://www.animenewsnetwork.com/interview/2018-05-15/a-few-questions-for-detective-conan-producer-michihiko-suwa/.131580


# A Few Questions for Detective Conan Producer Michihiko Suwa  
by Jordan Ramee & Zac Bertschy, May 15th 2018  

Michihiko Suwa has been a part of the anime industry for over thirty-five years. He's probably best known to western audiences for Inuyasha and Detective Conan, but has planned and produced an enormous and impressive roster of beloved classic series, including Lupin III, Magic Knight Rayearth, Black Jack, Angel Heart, Kekkaishi, File of Young Kindaichi, YAWARA!, and City Hunter. We caught up with Mr. Suwa at this year's Anime Boston and had the opportunity to ask him just a few questions about his storied career.

![](https://cdn.animenewsnetwork.com/thumbnails/max600x600/cms/interview/131580/city01.jpg)  
ANN: How closely did you work with Tsukasa Hōjō on the City Hunter anime? Did you collaborate often or was he mostly hands-off?

MICHIHIKO SUWA: Of course, I try to do what I can to get the information, but for that particular title most of the communication was between me and the editor at Shonen Jump. I seem to meet him at least once a year in a certain place these days. I feel like I've become a lot closer to him, and I have a good working relationship with him.

As for the upcoming City Hunter film, we've already released that we'll have a theatrical release in February next year. For this one, I have communicated directly with Mr. Hojo.

![](https://cdn.animenewsnetwork.com/thumbnails/max600x600/cms/interview/131580/city03.jpg)  
You worked on many, many seasons of City Hunter. What was the biggest challenge for you during that period?

So there've been several rounds of City Hunter: City Hunter, City Hunter 2, City Hunter 3, and City Hunter '91. They all were broadcasted during different time slots. At first, it was Monday at 7:00pm, and then it was Friday evening, and then it became Saturday at 6:00pm. Those all call for different target audiences and I remember how I had to change how I approached creating the anime to match the target audience.

To give you an example, I clearly remember that, at once point, City Hunter was a Sunday night program and, in order to match the target audience, we used a more fashionable song for both the opening and ending.

![](https://cdn.animenewsnetwork.com/thumbnails/max600x600/cms/interview/131580/conan01.jpg)  
What's the production cycle on an average Detective Conan movie? Do you feel like you're given enough time to make what you want?

As far as Conan is concerned, we have two separate teams. One works on the TV series and the other works on the movies. For the TV series, we have about 40 new episodes every year. For the movies, we have 1 new one every year. But those are two completely separate teams. We've done this long enough that I think we're in a good rhythm.