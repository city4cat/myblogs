# 考究一下FC漫画名的意思  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96729))  

FC的漫画名有这几个:

- 日文  

    - ファミリーコ ンポ(日文原版)


- 英文 

    - F.COMPO(日文原版[3], 法语版[6]，西班牙语版[6]，意大利语版[6]) 
    
    - Family Compo(英文版)

    - Family Component [4]


- 中文

    - 非常家庭(大陆版)
      
    - 搞怪家庭(港版(玉皇朝))[5]
      
    - 變奏家族(台版(大然文化))[5]
    
    - 混亂家庭(台版(林立出版(長鴻出版社)盜版)) [5]
    
    - 反串家族(新加坡版) [5]


- 泰语

    - F.Compo อลวนรักสลับขั้ว (泰语) [4] (直译为: F.Compo 颠倒的爱)

从原版日文名来看:  
”ファミリーコ ンポ”的google翻译为: Family component (日译英), 家庭组成(日译中)  
(我不懂日文，急需懂日文的朋友帮忙鉴定一下)

从原版英文名来看:  
F指的是family, 即”家庭”。
COMPO如果指的是component的话，则整体意思是”家庭(组成)成员”。
COMPO如果指的是compose/composed的话，则整体意思是”组合家庭”——柳叶家和若苗家的组合。


### 总结:  

1. 原版标题的名字的意思应该是”家庭(组成)成员”或”组合家庭”(柳叶家和若苗家的组合)。  

2. 这两个意思究竟取哪一个，还是需要懂日文的朋友帮忙辨析一下日文标题。  

3. 中文大陆版译为"非常家庭"，可能是考虑到"非常"的拼音"Fei Chang"的首字母为"FC"。同时，"非常家庭"的"非常"二字也表示若苗家的"与众不同"。  


### 参考链接

1. https://en.m.wikipedia.org/wiki/Tsukasa_Hojo

2. https://tieba.baidu.com/p/4606797743

3. https://www.amazon.co.jp/s?k=fcompo

4. https://h.mangairo.com/story-db251535

5. https://zh.wikipedia.org/zh/非常家庭

6. https://www.animenewsnetwork.com/encyclopedia/manga.php?id=4339

--------------------------------------------------

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
