# F.COMPO Subtitles 
source: 
[URL1](https://mangahot.jp/site/works/j_R0002), 
[URL2](https://web.mangahot.jp/works/detail.php?work_code=j_R0002)  
  
1.「家族」初体験  
2.裸天(ラテン)な夜に  
3.温かいご飯  
4.入学式の条件  
5.キャッチボールの思い出  
6.遠くで見ていた初恋  
7.一肌　脱ぎます  
8.母の思いで  
9.家族旅行！！  
10.おしかけアシスタント  
11.憧れの人  
12.紫苑、援助交際！？  
13.スクリーンに映るもの  
14.思い出の同窓会  
15.女優誕生！？  
16.女装テスト  
17.追い討ち  
18.意外な協力者  
19.辰巳　純愛す・・・！？  
20.18年ぶりの妹  
21.父と娘！  
22.順子の結納  
23.回り道  
24.父親　倒れる！？  
25.どっちが幽霊！？  
26.二人の観客  
27.雅彦を奪還せよ！  
28.19歳の誕生日  
29.女装コンテスト  
30.死のドライブ  
31.婦人科はお好き！？  
32.それぞれのイブ  
33.紫苑の初恋  
34.葉子の求愛！？  
35.母の墓参り  
36.雅彦の独立戦争  
37.危ないバイト  
38.葵の純情  
39.ファミリー・若苗  
40.憧れのウェディングドレス  
41.コレズ・茜  
42.秘密の上京  
43.養子に行きます  
44.結婚式の秘策  
45.鍾乳洞は危険な香り  
46.悪夢の露天風呂  
47.二人のマサヒコ  
48.マサヒコの正体  
49.父と子  
50.若苗家の新居候  
51.薫の策略  
52.男へのステップ  
53.偽装家族  
54.働くパパ  
55.葉子のロストバージン作戦  
56.葉子の変身  
57.二人のバースデー  
58.メールラバー  
59.ライバルに投げ勝て  
60.辰巳の願い  
61.辰巳の告白  
62.母のイメージ  
63.早紀の誘惑  
64.薫の告白  
65.親子の値段  
66.紫苑の第一志望  
67.二人の取材旅行  
68.雅美の値段  
69.恐怖のストーカー  
70.紫苑誕生  
71.身代わりデート  
72.それぞれのＸマス  
73.すれ違いのイブ  
74.似た者同士  
75.紫苑の入試  
76.紫苑の卒業旅行  
77.紫苑と雅彦一夜を共に!!  
78.旅は道連れ…!!  
79.雅彦 孤軍奮闘!!  
80.成人式はミステリアス  
81.男か女か？  
82.クラブ入部攻防戦  
83.足は口ほどにモノを言う…  
84.薫の進路  
85.失楽園（ロスト・パラダイス）  
86.親心  
87.親離れ子離れ  
88.近くて遠い東京  
89.母と娘  
90.衝突  
91.一日だけの女将  
92.恐怖の新歓コンパ  
93.再会  
94.浅葱来訪  
95.浅葱陽動作戦  
96.どっちがいいの!?  
97.一日だけの恋人  
98.雅彦のシナリオ  
99.流れを変える  
100.浅葱の反撃  
101.奥多摩の別れ  
102.家族（ファミリー）(完結)  

