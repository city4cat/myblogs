## 卷-章节-起始页码-标题

```
---卷----章节-----起始页码------章节英文标题  /  章节中文标题
Vol01	CH001	page003		"家庭"的初体验  	First "family" Life Experience  		  
Vol01	CH002	page043		天体之夜  	A Torrid Night  		  
Vol01	CH003	page073		温暖的饭  	A Hot Meal  		
Vol01	CH004	page099		开学礼的条件  	Entry Ceremony  		  
Vol01	CH005	page125		棒球的回忆  	The Glove Of Memories  		
Vol01	CH006	page153		在远处遥望的初恋  	A First Discrete Love  		
Vol01	CH007	page179		助一臂之力  	Just A Little Service  		
Vol02	CH008	page003		母亲的回忆  	Mother's Gift  		
Vol02	CH009	page031		家庭旅行!！  	Family Trip!  		
Vol02	CH010	page059		不请自来的助手  	The Stubborn Assistant  		  
Vol02	CH011	page087		仰慕的人  	The One I Admire  		  
Vol02	CH012	page115		紫苑的不道德交际!？  	Shion Courtesan!?  		
Vol02	CH013	page141		银幕上的紫苑  	The Girl On The Screen  		
Vol02	CH014	page169		充满回忆的同学会  	A Day To Remember Another Life  		
Vol03	CH015	page003		女星的诞生!？  	A Star Is Born!?  		
Vol03	CH016	page031		扮女人的测试  	Transvestite Apprentice  		
Vol03	CH017	page059		追击  	Repetition  		
Vol03	CH018	page087		意想不到的助手  	An Unexpected Collaborator  		
Vol03	CH019	page113		辰已，纯情的爱…!?  	Tatsumi's Unwavering Love  		  
Vol03	CH020	page141		阔别了18年的妹妹  	A Little Sister Not Seen For Over 18 Years  
Vol03	CH021	page169		父与"女"!  	The Father And The "daughter"!  		
Vol04	CH022	page003		顺子的过文定日子  	An Engagement Gift For Yoriko  		  
Vol04	CH023	page029		冤枉路  	Turnabout  		  
Vol04	CH024	page057		爸爸倒下了？  	Father Has Fainted?  		  
Vol04	CH025	page085		谁是鬼魂？  	Where's The Ghost?  		  
Vol04	CH026	page113		两位观众  	Two Spectators  		  
Vol04	CH027	page141		把雅彦还给我！  	Rescue Masahiko!!  		  
Vol04	CH028	page169		19岁的生辰  	His Nineteenth Birthday  		  
Vol05	CH029	page003		扮女人比赛  	Crossdressing Competition  		  
Vol05	CH030	page031		亡命驾驶  	Maniac At The Wheel  		  
Vol05	CH031	page059		你喜欢妇科吗？  	Gynaecology? You're Kidding Me!  		  
Vol05	CH032	page086		各人的平安夜  	A Xmas Like The Others  		  
Vol05	CH033	page113		紫苑的初恋  	Shion's First Love  		  
Vol05	CH034	page141		叶子的求爱!！  	Yoko Makes Her Move!!  		  
Vol05	CH035	page169		拜祭母亲  	At Mother's Graveside  		  
Vol06	CH036	page003		雅彦的独立作战  	Masahiko's Plan For Independence  		  
Vol06	CH037	page031		危险的兼职  	A Dangerous Job  		  
Vol06	CH038	page057		纯情葵  	The Feelings Of Aoï  		  
Vol06	CH039	page085		Family-若苗  	My Family The Wakanaes  		  
Vol06	CH040	page111		憧憬的婚纱  	Wanted  		  
Vol06	CH041	page137		同性恋师妹-茜  	Akane's Idol  		  
Vol06	CH042	page165		秘密赴东京  	The Secret Trip  		  
Vol07	CH043	page003		愿当养子  	Become My Son!  		  
Vol07	CH044	page031		婚礼的秘密对策  	A Super Secret Plan  		  
Vol07	CH045	page061		钟乳洞内的危险幽香  	The Smell Of Danger From Limestone Cave  
Vol07	CH046	page089		恶梦般的露天温泉  	The Nightmare Otherdoor Bath  		  
Vol07	CH047	page115		两个雅彦  	The Two Masahikos  		  
Vol07	CH048	page143		"雅彦"的真正身份  	The Identity Of Masahiko  		  
Vol07	CH049	page171		两父女  	Father And Child  		  
Vol08	CH050	page003		若苗家的新住客  	A New Member Of The Wakanae Family  		  
Vol08	CH051	page031		薰的诡计  	Kaoru's Stratagem  		  
Vol08	CH052	page059		迈出男人的第一步  	The First Step As A Man  		  
Vol08	CH053	page087		伪装家庭  	Camouflaging As A Family  		  
Vol08	CH054	page115		工作中的爸爸  	Papa's Work  		  
Vol08	CH055	page143		叶子的献身大作战计划  	Yoko's "first Time" Plan  		  
Vol08	CH056	page171		叶子大变身  	Yoko's Transformation  		  
Vol09	CH057	page003		二人世界的生日  	Their Birthday  		  
Vol09	CH058	page031		网上情人  	Mail Lover  		  
Vol09	CH059	page059		战胜劲敌  	Victory Pitch  		  
Vol09	CH060	page085		辰已的心愿  	Tatsumi's Wish  		  
Vol09	CH061	page111		辰已的表白  	Tatsumi's Confession  		  
Vol09	CH062	page139		母亲的印象  	Mother's Image  		  
Vol09	CH063	page167		早纪的诱惑  	Saki's Temptation  		  
Vol10	CH064	page003		薰的表白  	Kaoru's Confession  		  
Vol10	CH065	page031		亲情的价值  	The Price Of Kinship  		  
Vol10	CH066	page059		紫苑的第一志愿  	Shion's First Choice  		  
Vol10	CH067	page087		二人的取材之旅  	Their Material Gathering Journey  		  
Vol10	CH068	page115		雅美的身价  	Masami's Values  		  
Vol10	CH069	page143		恐怖的跟踪者  	The Frightening Stalker  		  
Vol10	CH070	page171		紫苑的诞生  	Shion's Birth  		  
Vol11	CH071	page003		约会的替身  	Substitute Date  		  
Vol11	CH072	page031		各人的圣诞节  	Separate Christmas  		  
Vol11	CH073	page061		阴错阳差的平安夜  	An Eve Of Fated Encounters  		  
Vol11	CH074	page089		物以类聚  	It's All The Same  		  
Vol11	CH075	page117		紫苑的入学试  	Shion's Admission Exams  		  
Vol11	CH076	page145		紫苑的毕业旅行  	Shion's Graduation Trip  		  
Vol11	CH077	page173		紫苑和雅彦共渡一夜  	Shion And Masahiko's Night Together  		  
Vol12	CH078	page003		出外靠旅伴…！  	Dragged Along On The Trip!  		  
Vol12	CH079	page031		雅彦孤军作战！  	Masahiko Fighting Alone!!  		  
Vol12	CH080	page059		充满神秘的成人节！  	The Mysterious Coming-Of-Age Day  		  
Vol12	CH081	page087		是男是女？  	Man Or Woman!?  		  
Vol12	CH082	page115		加入学会攻防战  	Club Recruiting Battle  		  
Vol12	CH083	page143		脚如嘴巴般道出真相  	The Foot Speaks As Well As The Mouth...  
Vol12	CH084	page171		薰的前途  	Kaoru's Path  		  
Vol13	CH085	page003		失乐园  	Lost Paradise  		  
Vol13	CH086	page031		父母心  	Parental Feelings  		  
Vol13	CH087	page059		孩子要独立  	Cutting The Umbilical Cord  		  
Vol13	CH088	page087		似近亦远的东京  	Tokyo, So Near And Yet So Far  		  
Vol13	CH089	page115		母与女  	Mother And Daughter  		  
Vol13	CH090	page141		冲突  	Clash  		  
Vol13	CH091	page169		只做一天老板娘  	Okami For A Day  		  
Vol14	CH092	page003		恐怖的迎新联欢会  	Scary Meeting  		  
Vol14	CH093	page031		再会  	Reunion  		  
Vol14	CH094	page057		浅葱到访  	Asagi's Visit  		  
Vol14	CH095	page083		浅葱突击行动  	Asagi's Plan  		  
Vol14	CH096	page109		哪方面较好呢？  	It Doesn't Matter?!  		  
Vol14	CH097	page137		一天的情人  	Couple For A Night  		  
Vol14	CH098	page163		雅彦的剧本  	Masahiko's Scenario  		  
Vol14	CH099	page189		改变剧情  	Change Of Course  		  
Vol14	CH100	page215		浅葱的反击  	Asagi's Retaliation  		  
Vol14	CH101	page243		奥多摩的分手  	Split At Okutama  		  
Vol14	CH102	page269		家庭(最终话)  	Last Day - End  		  

```