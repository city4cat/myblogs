
说明:   

- 「つなぐ」是[pound_poundy](https://twitter.com/pound_poundy)的CH同人漫画。分为前篇、中篇、后篇。  
- 作者禁止使用其作品图片(“Reproduction is prohibited. Don't use the pictures.”)。所以这里只使用图片链接(网页将链接显示为图片)；如果这种方式也不被允许，请告知我。  
- 因为我不懂日文，所以译文为机翻，所以无法保证翻译质量。**欢迎指出错误，谢谢！**  
- 无法访问twitter的用户，将无法看到本页面的图片。很抱歉无法上传图片到本论坛以方便浏览，因为作者禁止使用其作品图片。  

--------------------------  

# つなぐ  (连接)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96925))    
By [pound_poundy](https://twitter.com/pound_poundy)  

--------------------------  

## 「つなぐ」前編 ([Link](https://twitter.com/pound_poundy/status/1434144797929865217))  

--------------------------  

![](https://pbs.twimg.com/media/E-cbF0MVcAEU6y-.jpg)  

獠: いや...これはその...  
不，它是... 这只是...  

獠：深い事情が あつて...  
另有隐情...  

(ゴロ ゴロ)  
(砰 砰 )  

(ポツ)  
(扑通)  

(ダツ)  
(Dats)  

獠：香っ  
香  

(ダツ) (ダシ)  <--- 不确定这一行的手写体是否识别正确。  
(Datu) (Dashi)


--------------------------  

![](https://pbs.twimg.com/media/E-cbF0MUcAI8Hh7.jpg)  

*：ちつ  (译注：不确定这个该如何翻译比较合适)  
阴部  

獠：だあーめ  
就这样吧!  

路人：お店の時間まで 付き合ってくれる 約束でしよお?  
你答应过要陪我到商店的，对吗？  

獠：それなんだけど...  
是的，但是...	 

獠：それ今度にして、先に 情報教えてくれないかな なーんて  
我告诉你吧，我只是想知道你是否可以给我一些线索  

路人：やですう それならあの人が 狙われてる情報、おしえなーい  
不，那我就不给你他被盯上的消息  

路人：ほら与雨降ってきたし  
来吧，开始下雨了  

路人：いこいこ❤  
来吧❤  

獠：たははは...   
哈哈哈...   

*：ど--せ (ど一せ)  
什--么？  (什么？)  

香：はあ  
嗯  

香：はあ  
嗯  


--------------------------  

![](https://pbs.twimg.com/media/E-cbF0LUUAMEIaG.jpg)  

香：どーせあたしは手を 出されない女だもの  
我是一个不能被触碰的女人  

香：そもそも女として 認識されてないかも...  
我不知道，也许他们一开始就没把我当成一个女人......  

香：美樹さん達の結婚式から 何もかもが宙ぶらりん  
自从美樹的婚礼之后，一切都处于停滞状态

香：バカね...もう考えるのは よそうって決めたのに  
我真蠢... 我已经决定不考虑这个问题了  

香：泣いちゃだめ  
不要哭  

香：またいつもの二人を 始めるために----  
要像往常一样重新开始我们两个人的生活----  

(ザアアアア)  
(沙沙沙沙)  

--------------------------  

## 「つなぐ」中編その1 ([Link](https://twitter.com/pound_poundy/status/1438854186167197705))  

![](https://pbs.twimg.com/media/E_fWQSYUUAQHqz9.jpg)  

(チチチ)   
(Chichi Chichi)  

(ソロ... )  (ソロ...)  

(カチヤツ)  
(Tick-tock)  

獠: ...おはよ  
... 上午好   

香：はら おはよう  
早上好 哈拉  

香：珍しく 早いのね  
你异常地早  

香：タベ遅かったみたいだから  まだ寝てるかと思ったのに	 
托夫，你迟到了。 我以为你可能还在睡觉。  

獠：いやその...  
不，我是说...  

香：ああ、別に  気こしてないから   
不，我不是那个意思  

獠：昨日の彼女  なんだけど...  
这只是我昨晚的女人...  

香：へ?  
嗯？  

--------------------------  
![](https://pbs.twimg.com/media/E_fWQSaUYAAdwc-.jpg)  

香：外で何しようと  あんたの自由だしね  
你在外面做什么是你的事，你知道  

獠：え...  
哦...  

香：ほら、そんな事より パン燒いてよ  
来吧，别这样，让我们把面包放在桌子上

獠：そんな事って	
你是什么意思？	 

獠：ーーだってお前昨日、泣いてなかったか?		
----我是说，你昨天没有哭吗？

香：ーーないわよ  
不，我没有

獠：なに？  
什么？

香：だから泣いてなんか ないってば  
我没有哭。我没有!  

*：ちがう  
没有

香：大体、なんであたしが  泣かなきゃいけないのよ  
我为什么要哭呢？ 要哭吗？

*：こんな風にしたいんじゃない  
这不是我想要的方式

獠：香...  

香：さわらないでっ  
别碰我! 


--------------------------  
![](https://pbs.twimg.com/media/E_fWQSWUYAIX_R7.jpg)  

香：あ...  
哦...  

香：伝言板見てくるわ  ご飯、食べちゃって  
我去看看留言板，你已经吃过了  

(パタソ..)  
(啪哒...)  

獠：どうにもここ最近 特に上手くいかない----  
最近我的事情不是特别顺利----  



獠：まるで絡まり合った 糸にがんじがらめで、 お互い身動きがとれない ような感じだ...  
仿佛我们都被困在一个纠缠不清的线里.....  

獠：あの時の淚は  
那时候呢？  

獠：自分でも気づいて いなかつたのか----  
我甚至不知道我是否意识到----  

獠：それでも	
然而  

獠：嬉しかった、なんて 言ったら、こっぴどく 殴られるだろうな...  
如果我说我很高兴，我就会被狠狠地揍一顿......  



--------------------------  
![](https://pbs.twimg.com/media/E_fWQS3VkAUCl08.jpg)  

香：今日も依頼は なし...か  
今天没有要求... 或  

香：あたしったら 何やってんだろう...  
我不知道我在做什么......  

香：いつも通りにつて 決めてたのに  
我打算做我一直做的事情

香：まったくう   
哦，天啊

*：ほーんと後が ないって顔ね  
你看起来好像没有时间了  

香：あなた...は  
你看起来... 你好!  

路人：どーも❤  
你好❤  

路人：ねえ  
嘿!  

路人：昨日なにがあったか 知りたくなあい?  
你难道不想知道昨天发生了什么吗？  


--------------------------  

## 「つなぐ」中編その2  ([Link](https://twitter.com/pound_poundy/status/1443202548551208965))  

![](https://pbs.twimg.com/media/FAdJEPDUcAEW4Rs.jpg)  
路人：ここのお店、お砂糖も こだわってるのよ❤  
这家店对他们的糖也很讲究❤  

香：はあ  
哦  

香：あの...  
所以...

香：伝言板に書いたって 事は依頼という事で いいですか?	 
如果你把这个放在留言板上，是不是意味着你想让我去做？  

香：昨日の事は獠が どうしょうと 興味もないんで  
我对獠昨天所做的事情不感兴趣  

路人：......  

路人：そうなのお?獠ちゃん べッドでとっても 素敵だったのにい❤  
是吗？ 獠在床上的表现很好❤  

香：ああああああ  
哦哦哦哦哦哦  

(せか°くの 高コヒが)  
(不可能！)  (译注：香的内心所想)

路人：嘘よウソ! あなたってばホント 分かりやすいわね  
不可能! 你是如此容易让人看透  （译注：不确定该怎么翻译这句）

(香惊讶)  

路人：まあ、あなたほどじゃ ないけど、昨日の 獠ちゃんは 分かりやすかったわ  
嗯，獠没有你那么容易让人看透，但昨天的他很容易让人看透  

路人：普段なら絶対見せない ような顔してーー  
他的脸上有一种你通常不会看到的表情----


--------------------------  
![](https://pbs.twimg.com/media/FAdJEPEUYAUcVnD.jpg)  

路人：ごまかしてたけどお  
他是假装的  

路人：多分あなたの事 考えてたんだと思うわ  
我想他是在想你  

路人：お店があくまで 一緒にいたのも  
这店是唯一的我们在一起的地方

路人：うちの店にくるどっかの 組の若頭が、あなたを 狙ってるって獠ちゃんに 教えたからで----  
我告诉过獠，有些黑帮头目在追杀你。他们来过我店里。  

*：二度とそんな事	 考えさせないくらいには、痛い目にあわせてたわよ  
我已经给了你足够的痛苦，以至于你再也不想提它了   

獠：サンキュ、麗奈ちゃん また何かあったら頼むな  
谢谢你，麗奈，如果你以后需要我帮忙，尽管告诉我  

麗奈：そう言って帰ってく 獠ちゃん見てたらあ  
当我看到獠离开时，我也是这么想的...  


--------------------------  
![](https://pbs.twimg.com/media/FAdJEPMVUAcf6U-.jpg)  

麗奈：あれ?昨日の私の態度、結構マズくない?って思ったりしてえ  
什么？ 我在想，我昨天的态度是不是太糟糕了？  

麗奈：だって獠ちゃん 詳しい事言わなそうだしい  
我的意思是，獠不会对此说什么  (译注：不确定这句话该怎样翻译)

麗奈：て、心配して駅に来てみたら あなたがめっちゃ暗い顏で いるんだものお  
来到车站时，我很担心，发现你的脸色如此阴沉  

麗奈：あなた達って素直に 愛情を表現すると 爆発する病気なのお?  
你们这些人是不是有病，当你们诚实地表达你们的爱时就会爆发？  

香：........  

香：あたしは  
我是  

香：そうなのかもしれない...  
我想我可能是...  

麗奈：え?  
什么？  

(爆發すんの?)  
(你要爆發了吗？)

香：獠から離れたら----  
如果我离开獠----  

香：きっとどうやって息を してたのか分からなくなる	 
我不知道我将如何能够呼吸	 


--------------------------  
![](https://pbs.twimg.com/media/FAdJEPVVEAAjUN3.jpg)  


香：だから...  
所以...  

香：獠からの愛情を期待して、一緒にいられなくなるのが  
我期待着獠的爱，而我却不能和他在一起。  

香：怖い...  
我恐怕...  

麗奈：なあんか こんがらがってんのねえ  
你有点糊涂了吧？  

麗奈：いっそのこと一回 ほどいてみたら?  
你为什么不试试自己解开呢？  

香：ほどく...?  
解开...？  

麗奈：どーせ相手が 何考えてるかなんて 分かりやしないんだし  
反正你也不知道男人们在想什么  

(ましてや	あの獠*んたし) 	
(更不用说是獠了)  

麗奈：ほどいた先がどこに 繋がってるか見てみたらあ?  
你为什么不给他松绑，看看会有什么结果？  

麗奈：でもひとっ 確かなことがある  
但有一件事我是肯定的  


麗奈：獠ちゃんはこれからもきっと あなたを守り続ける  
我相信獠会继续保护你。  

麗奈：それでもあなたは 怖いからってずっと 動かないでいるの?  
而你依旧这样担心，还不行动？   (译注：不确定这句话该怎样翻译)


--------------------------  

## 「つなぐ」中編その3  ([Link](https://twitter.com/pound_poundy/status/1446825392363376645))  

![](https://pbs.twimg.com/media/FBQoBhWVcAQHKpW.jpg)  

麗奈：ま、  
好吧  

麗奈：それは ともかく  
无所谓了  

(ずずい)  
(哦!)  

麗奈：私の依頼はあなたに 昨日の話を聞いて もらうこと！  
我让你听听你昨天说了些什么!   

麗奈：依頼料はここの コーヒーをおごる ってことで! 以上、解散!  
我请你喝杯咖啡！ 就是这些!  

(もう飲めな だろうけど)  
(我知道你不能再喝了）  

香：え...これが依頼?  
嗯... 这是一个请求吗？  


香：まさか...私のために...  
不可能... 这是给我的（对我来说）...  

麗奈：あーんもう、私も獠ちゃん好き だったのに、なんで 恋愛相談されてんのお?  
哦，天啊。我也喜欢獠。但为什么，你为什么要向我征求爱情建议？  

(゛タツ)  
(笑)  

麗奈：でも、まあ  
但是...嗯  

麗奈：----って  

麗奈：ちょっとお 何やってんのあなた!  
嘿 你在做什么！  



--------------------------  
![](https://pbs.twimg.com/media/FBQoBhWVUAMBk0V.jpg)  

香：何ってコーヒー 飲んでるだけデスガ  
我只是在喝咖啡  

(ぶあリ ぶおリ)  

(バカなの?)  
(你疯了吗？)  

麗奈：待って コーヒーってそんな 音が出る飲み物だった?  
等等，咖啡应该发出这样的声音吗？  

(ハアかリやすリ上*)
(太吵了) <---这行手写体识别不出来  

香：ぼリ  
哦

麗奈：なあんか 分かったわ  
我现在知道它是什么了

(バカて° 言ゆ*た)  
(你是个白痴)  <---这行手写体识别不出来

麗奈：獠ちゃんがなんで そこまでして あなたと一緒に いようとするのか  
我不知道为什么獠这么想和你在一起

麗奈：だって私もあなたが好きに なっちゃったから  
因为我也已经爱上了你

麗奈：香ちゃん❤  
香妹❤  

香：ありがとう  
谢谢你  

香：嬉しい とっても  
我很高兴了 很开心  



--------------------------  
![](https://pbs.twimg.com/media/FBQoBhXVUAE2AvX.jpg)  

獠：......  

麗奈：ありがとう ございました  
谢谢，非常感谢你  

香：どうも ごちそうさまあ  
谢谢，谢谢你请客  

獠：何しやべってたんだか 分からんが、大丈夫 だったみたいだな...  
不知道她在做啥，但她似乎没事......  

麗奈：じゃーね香ちゃん またお茶しょーね  
再见了，香，以后有机会再来喝茶吧  

香：うん、是非  
好，一言为定  

(ス...)  
(是的...)  

麗奈：あっそうだ 香ちゃん  
哦，是的，香  

麗奈：例の病気の事なんだけどお  
说到这个病...  

麗奈：なんか色んな事情が あるんだろうけど  
我知道你有很多理由...  

麗奈：そんな熱い想い抱えて いっか暴発しちゃうより、出来る方法で伝えてみたら?  
你为什么不以一种可行的方式告诉他？而不是怀着这样的热情最终爆发  

(ぼ)  
(哦......)  

香：あ...ちょっと  
啊... 嘿...  

麗奈：そんじゃ まったねー  
咱们回头见--  

（今度は  恋バナレよーゆ）  <---这行手写体识别不出来  
（这一次我将告诉你我的爱情生活)  

香：あたしに出来る  伝え方...かあ  
我可以做到这一点。怎么能告诉你... 好的  


--------------------------  

## 「つなぐ」後編その1  ([Link](https://twitter.com/pound_poundy/status/1451550036735660032))  

![](https://pbs.twimg.com/media/FCTxD9nVcAI1Gkb.jpg)  

香：こうやって...と  
像这样... 然后  

香：よし、出来た!  
好了，搞定了!  

獠：香のやっ、帰ってきてから 自分の部屋にこもりきりで  
香回家后一直坐在我的房间里

獠：何やってんだか...  
我不知道他在做什么...  

香：獠、ただいま  今大丈夫？  
獠，我来了。你现在还好吗？

(コン コン) (コソ コソ)   <---这行手写体识别不出来  
(敲门) (偷偷摸摸)  

獠：おー、おか、えり  どうした?  
哦，嘿，怎么了？  

香：えっと、ちょっと お願いがあって...  
我在想是否可以请你帮个忙.....

獠：お願い?  
帮忙？ 

香：あのね、これなんだけど----  
你知道，这是...   




--------------------------  

![](https://pbs.twimg.com/media/FCTxD9mUcAI4mzI.jpg)  

獠：紙コップ?  
纸杯？  

獠：いや----  
编号----  

獠：糸電話...か?  
有线电话... 还是？  

香：も...もしもし りようお?  
你好... 喂？  

香：びつくりさせちゃった かもしれないけど  
如果我吓到你，我很抱歉。  

香：少しだけ、これで 話してもいい?  
我可以和你谈一下这个问题吗？  

香(内心)：これなら素直に 話せるかと思ったけど  
我想这将是一个让你开口的好办法  

香(内心)：子どもっぽくて 呆れられちゃうかな----	 
这真是太幼稚了，我担心你会认为我很傻----	

獠： ......  



--------------------------  

![](https://pbs.twimg.com/media/FCTxD9mUUAERhAU.jpg)  

獠：...ふーん、いいぜ そういや依賴は あつたのか？  
...哦，那么，Yorai，你得到它了吗？  (译注：不确定这个翻译是否正确)

香：う、うん!それで もう終わったわ！  
嗯哼! 这么说忙帮完了！  

獠：随分早いな どんな依頼だったんだ?  
太快了，你想要什么？  

香：ええと... あったかくって  
它...嗯, 它很温暖  

香：あと甘かった かな、へへ  
我想，也很甜蜜  

獠：なんだそりや ホットケーキの 食べ放題の依頼でも  あったのか?  
那辆雪橇和任你吃的煎饼是怎么回事？  (译注：不确定这个翻译是否正确)  

香：なっ  
没有  

香：ちっ違うわよ バカっ？  
不，不是的，你这个白痴。 

獠：ははつ  
哈-哈-哈。 

獠：----香？  

香：今朝は  
今天早上...

香：嫌な態度とって ごめん  
我很抱歉  

獠：いやそれは 俺が----  
不，那是因为我是----  

香：ううん  
没有  


--------------------------  

![](https://pbs.twimg.com/media/FCTxD9mUcAAq4kL.jpg)  

香：美樹さん達の結婚式から ずっと...不自然な態度 だったと思う  
自从美樹的婚礼之后... 我想我一直表现得很不自然。  

香：必要以上に いつも通りにこだわってた...  
我一直在坚持我的事务，有些是我不必做的...  

香：だってあたしの願いは ひとつだけで----  
因为我只有一个愿望：----  

香：それさえ叶えば  それでいいと  思ってたから  
我想如果我得到这些就足够了。

香：だけど...  
但是...  

香：いつの間にか それ以上を願ってる 自分がいて----  
我发现自己的愿望不止于此, 后来我才意识到这一点----

香：それじやダメだって 分かってるのに  
我知道这还不够好  

香：でもやつぱり 抑えきれなくて こんがらがって...  
但我没办法，我很困惑......  

香：だからもう逃げないで、この先の願いを今の自分に つなごうと思うの  
所以我不会再逃避了，我要把我未来的愿望和现在的愿望联系起来  

(すう...)  
((香)叹气...)  

香：聞いて獠 あたしは----  
獠，听我说，我----  


--------------------------  

## 「つなぐ」後編その2  ([Link](https://twitter.com/pound_poundy/status/1455530790192914450))  

![](https://pbs.twimg.com/media/FDMViImXEAAnaKQ.jpg)  

*：あなたこ----  
Anatako----  (译注：不确定这个如何翻译)

香：え...?りよ...  
什么......？ 獠...

(カララソ)  
(Carrara So)  <----這一行手寫體不確定  

獠：決して多くを望まない お前のたった一つの願いを----  
我不求多，只要一个、唯一一个愿望----  

獠：俺は何があろうと 守ろうと思っている  
无论如何，我都会保护你。 



--------------------------  

![](https://pbs.twimg.com/media/FDMViIqWYAY1YUb.jpg)  

獠：なぜならそれは  
因为这就是  

獠：俺の願いでも あるからだ  
因为这也是我所希望的 

獠：来年も...再来年も ずっと----  
而明年... 后年也是如此----

獠：おれもおまえと 同じもので----  
我和你一样----

獠：だから...一緒にいる為に いつも通りが必要なら  
所以... 如果你需要想往常一样在一起的话  

獠：これからもずっと---- そのままでと思っていた  
我以为我们总是----是一样的  

獠：それがお互いを  
这使我们想要的

獠：身動きが取れないくらい がんじがらめにしてしまったのに  
就像我们被困在屋里无法动弹  (译注：不确定这个如何翻译)



--------------------------  

## 「つなぐ」後編その3  ([Link](https://twitter.com/pound_poundy/status/1460234989987979267))  


![](https://pbs.twimg.com/media/FEPL-s0aUAcGDc6.jpg)  

獠：だが----  
但----  

獠：お前はまた新しい糸で 俺達をつなごうと してくれたんだな  
你试图用一个新的方式再次连接我们  

獠：不安にさせて...すまなかつた  
我很抱歉我让你感到不舒服...我很抱歉  

香：ううん  
没有  

香：同じ願いを  持つてくれてたなんて  
我不知道你对我有同样的愿望  

香：嬉しい----  
我很高兴----  

獠：----香  

獠：お前の この先の願いつて なんなんだ？  
你对未来有什么愿望？  

獠：え...  
嗯...  



--------------------------  

![](https://pbs.twimg.com/media/FEPL-s1aIAAvmPd.jpg)  

香：え...えれはえの...  
呃...Erre ha e no...  
 
獠：改めて言うのは 恥ずかしいつていうか...  
我很不好意思再说一遍，但是......  

獠：...言つとくが俺だつて 相当だからな？  
我... 我告诉你，我很擅长这个，你知道吗？  

香(汗)：うつ  
呃  

獠：ええと その  
嗯，嗯...  

(ハ一   音)  <--- 该行手写体看不懂  

香： 獠と  

香：ちやんとした  
我想有一个小  (译注：不确定这个如何翻译)  

香：キスがしたい...  
我想吻你...  

香：そつそこから 始めたら  
从这里开始  

香：何か変わるかと 思って...でも  
我想这可能会带来一些变化...... 但是  

(魂あか*中)  
(灵魂燃烧)  <--- 该行手写体看不懂  

香：今涙で顔が...あとハナとか  
我现在泪流满面...和汉娜	(译注：不确定这个如何翻译)  

(そ----)  
(所以----)  

香：だから顔洗って 出直してきたい と思ってて----  
所以我想，我要洗把脸，然后回来 ----  

(こんな時にNOと言える女)  
(一个在这种时候能说不的女人)  



--------------------------  

![](https://pbs.twimg.com/media/FEPL-s0aAAI-pqc.jpg)  

香：じゃっ そういう事で！  
好吧，那就这样吧!  

獠：香っ！？  

(ほんと* 行く気か？)  
(你确定你想去吗？)  

香：おいっ 待てって！  
嘿，等着我！

獠：待たないっ！！  
我不等了!  

香：あつ  
啊!  

*:んう......  
嗯......  




--------------------------  

## 「つなぐ」後編その4  ([Link](https://twitter.com/pound_poundy/status/1470020632687841291))  


![](https://pbs.twimg.com/media/FGaP-LkVcAYIw_J.jpg)  

*：はあっ	
哈!	

*：ふつん	
嗯...	

(ググ...)  
(Gugu...)  

*：ん
嗯...

*：んん...
嗯嗯...


(ほう)  
(嗯...)  

獠：香----  

獠：香っ！？  


(しゆゆゆん)  <--- 不確定這一行的手寫體  
(叹气)  

香：ごめ...なんか...獠とキスなんて 夢みたいで...  
对不起...只是... 我一直做梦都想吻你...  



--------------------------  

![](https://pbs.twimg.com/media/FGaP-LlVcAcF4-K.jpg)  

獠：きや?  
你什么意思？

獠：だああああ~   
哒哒哒～

(がば)  
(Gaba)

(ひよリフ)  
(Hiyorif) <--- 不確定這一行的手寫體

香：え?	
嗯？

獠：じゃあ俺も この先の願いつつーのを 叶えさせてもらうわ  
那我就来实现我的愿望吧  

香：え?獠にも あったの？ていうかこの 状況なに?  
什么？ 你有一个这样的人吗？ 这是什么情况？  

獠：もっちろん！  
我不知道  

獠：お前との もっこり❤  
我真为你感到高兴❤  

獠：ほんで今俺の寝室に 向かって移動中で はい到着～ 
现在我们正在去我卧室的路上。我们到了～

香：え?  
什么？

(パタン) (パタソ)  
(啪嗒) (啪嗒) <--- 不確定這一行的手寫體  



--------------------------  

![](https://pbs.twimg.com/media/FGaP-LmVcAICbTA.jpg)  

麗奈：香ちゃーん!	
香--嘿!  

香：え?  
嗯？  

香：!  

麗奈: ひっさしぶりー❤ 兀気してたあ?  
好久不见❤你的精神状态很好吗？		

香：麗奈さん!	 
麗奈!  

麗奈：やったね こんなとこで偶然 会えるなんて  
你做到了！我真不敢相信我们在这里见面了！  

香：ホントに！  
真的!  

麗奈：あれからどうしてるかなー って思ってたんだあ  
我一直在想，从那时起你过得怎么样。  



--------------------------  

**Links:**  

- https://ocr.space/  
- https://o-oo.net.cn/japaneseocr/  
- https://o-oo.net.cn/keyboard/  
- http://www.deepl.com/  
- https://translate.google.cn/  
