- Twitter上的相关tag（已转载至[Twitter上与北条司及其作品相关的信息 - hojocn](http://www.hojocn.com/bbs/viewthread.php?tid=96791)）:  
    - [キャッツアイ40周年記念原画展 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年記念原画展)  
    - [キャッツアイ40周年原画展 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年原画展)  
    - [キャッツアイ40周年 - Twitter](https://twitter.com/hashtag/キャッツアイ40周年)  
    - [キャッツアイ - Twitter](https://twitter.com/hashtag/キャッツアイ)  
    - [CatsEye - Twitter](https://twitter.com/hashtag/CatsEye)  


## キャッツアイ40周年記念原画展（2023年03~04月，名古屋展）

https://twitter.com/cahxybz/status/1640755744797757441  
絵が素敵💕見入ってしまった  
\#シティーハンター が４０周年の時にも原画展を！お願いします  
キャッツアイの原画見ながら遠くからGet Wild流れていたのがなんとも不思議空間、でも嬉しかった   
画帅气💕看过了  
\#《城市猎人》40周年之际也要举办原画展!拜托了  
猫眼的原画一边看一边从远处Get Wild流淌着的真是不可思议的空间，不过很高兴  
![](./img/FsUipehaEAANnRz.jpg) 
![](./img/FsUipejaMAE6lvB.jpg) 
![](./img/FsUipelaYAALcFl.jpg) 
![](./img/FsUipemaEAAOWZQ.jpg) 


https://twitter.com/tamagawaya_uc/status/1640687995115413509  
「 #キャッツアイ40周年記念原画展 〜そしてシティーハンター へ〜」 @ #松坂屋名古屋店 、北条司の出世作『 キャッツ・アイ 』40周年。  
門外漢の福岡の青年が、終生の盟友となる編集・堀江信彦と二人三脚で作り上げた青春の連載（キャプションは全て連名）。  
最終回の原稿ラスト、お疲れ様でした…  
「#猫眼40周年纪念原画展~然后向着城市猎人~」@ #松坂屋名古屋店，北条司的成名作《猫眼》40周年。  
门外汉的福冈青年，和成为终生盟友的编辑堀江信彦以二人三脚合作完成的青春连载(Caption全部联名)。  
最终回的原稿Last，大家辛苦了…  
![](./img/FsTlBIOakAIOfoj.jpg) 
![](./img/FsTlBrDaIAA3TJs.jpg) 





https://twitter.com/JeJeCH4/status/1640574605533536256
神谷さんの生サイン〜っ！(≧∇≦*)    
神谷先生的现场签名~  
![](./img/FsR953IaUAAbjmR.jpg) 



https://twitter.com/JeJeCH4/status/1640569092750077955  
俊夫さん、何故にピンクww  
っていつも思ってしまうww  
俊夫，为什么是粉红ww。
总是这么想的ww

https://twitter.com/tamagawaya_uc/status/1640346146961862660  
「 #キャッツアイ40周年記念原画展 〜そしてシティーハンターへ〜」  
限定漫画『スペースエンジェル』  
ジャンプ愛読者賞読み切り（1982）のセリフリメイク。  
北条司がプライベートで描き発表する気は無かったのを、盟友・堀江信彦が口説き落とし、会場限定で全編展示。  
星野之宣テイスト…  
※撮影禁止  
「#猫眼40周年纪念原画展~然后向着城市猎人~」  
限定漫画『Space Angel』  
Jump爱读者奖短篇(1982)的台词Remake。  
北条司本来不想私下公开，但盟友堀江信彦说服他在会场限定全篇展示。(译注：应该是北条司本来只想私下公开)  
星野之宣Taste…  
※禁止摄影  





https://twitter.com/Polaris1725/status/1639970083165569024  
春は出会いの季節。  
沢山の人の笑顔が好きな獠ちゃんだから香ちゃんが自分と初めて出会った日を誕生日にするのも頷けるよ。  
本当に素敵な誕生日。  
春天是邂逅的季节。  
喜欢很多人笑容的獠酱，所以把阿香和自己初次相遇的日子定为生日也可以点头哦。  
真是很棒的生日。  
![](./img/FsJYGHyaYAANWhY.jpg) 



https://twitter.com/Polaris1725/status/1639938131117092865  
今日は獠ちゃんの誕生日だからずっと眺めていたい。  
おチャラけコミカルな獠ちゃんも面白くて大好きだから。  
(((*≧艸≦)ﾌﾟﾌﾟｯ笑笑笑  
今天是獠酱的生日，我想一直看着。  
调皮搞笑的獠酱也很有趣很喜欢。  
(*≥草≦)噗噗笑笑笑  
![](./img/FsI7CD6aQAUgAn_.jpg) 
![](./img/FsI7CD4aUAAFOMQ.jpg) 



https://twitter.com/Polaris1725/status/1639926909999120385  
獠ちゃんの腕の💪が…。  
すんげぇーー！  
獠酱手腕的💪…。  
承启——!  
![](./img/FsIw1KHaEAArMp3.jpg) 



https://twitter.com/Polaris1725/status/1639841612296757250  
獠ちゃん  
ℍ𝕒𝕡𝕡𝕪 𝔹𝕚𝕣𝕥𝕙𝕕𝕒𝕪  
今原画展にいるよ！  
❀.(*´▽`*)❀  
獠酱  
ℍ𝕒𝕡𝕡𝕪𝔹𝕚𝕣𝕥𝕙𝕕𝕒𝕪  
现在在原画展!  
![](./img/FsHjPnZagAMXx2V.jpg) 
![](./img/FsHjPnaaUAEmL9n.jpg) 
![](./img/FsHjPnXaMAEzTZ8.jpg) 
![](./img/FsHjPnWakAExeB0.jpg) 


https://twitter.com/ch_4001_Yturn/status/1639826610500288513  
朝遅かったんだなぁ（ボソ）  
早上好晚啊。  
![](./img/FsHVm2gakAAQPU8.jpg) 




https://twitter.com/Sheryl_KITG/status/1639552640639201281  
【#キャッツアイ40周年記念原画展 〜 そして #シティーハンター へ〜】  
図録付き入場券を購入し、いざ！   
人も少なくて、回りやすく撮影📸もし易い  
所々に、エピソード等も交えた美しい原画を拝見  
意外と広くてボリュームあり  
グッズも思わず色々購入してしまう  
これだから、ファンはやめられない  
【#猫眼40周年纪念原画展～然后向着城市猎人～】  
购买附有图录的入场券，开始!  
人少,周围很容易拍摄📸如果容易  
到处都能看到夹杂着小故事的美丽原画。  
意外的宽敞和丰富  
也不由自主地购买了各种商品  
就是因为这样，粉丝们才欲罢不能  
![](./img/FsDcbi8aMAAosjh.jpg) 
![](./img/FsDcbA0acAEDcvY.jpg) 
![](./img/FsDcaapacAA492J.jpg) 
![](./img/FsDcZyQagAANb7-.jpg) 

https://twitter.com/Sheryl_KITG/status/1639563161358733312  
来場者特典のキャッツカードは泪姉でした  
到场的猫眼卡是泪姐。  
![](./img/FsDmAW5aUAA6mRP.jpg) 
![](./img/FsDmAH9akAE2G65.jpg) 



https://twitter.com/sumikojo/status/1639537057239531520     
連載当時の懐かしい原画等をじっくり堪能しました。  
ジャンプがF1参戦した時の記念イラストも有  
これほぼ撮影可なんですよ  
ありがたやー😭勉強になるわー  
コーヒーハウスCATS EYEの電光看板が販売していましたが見事に完売  
慢慢地享受了连载当时令人怀念的原画等。  
Jump参加F1时的纪念插图也有  
这几乎是可以拍摄的。  
谢谢你😭能学到很多。  
在CATS EYE咖啡店的电子广告牌上销售。  
![](./img/FsDOQpHaYAAPMhC.jpg) 
![](./img/FsDOQpCaEAABhIz.jpg) 
![](./img/FsDOQpAacAA72Kc.jpg) 
![](./img/FsDOQo_aUAIOr7A.jpg) 



https://twitter.com/MsJunction/status/1639528981539225600  
原画"読んで"泣きそうに  
原画“读吧”，快哭了  
![](./img/FsDGkKnaUAA8JG0.jpg) 
![](./img/FsDGkKgaUAEXCfW.jpg)  
https://twitter.com/MsJunction/status/1639528433398210560  
原画、ならでは笑  
原画，笑死人了  
![](./img/FsDGZCQaYAQkeRA.jpg) 



https://twitter.com/MsJunction/status/1639527647675035648  
そして。。行ってきた。  
胸いっぱい。  
ヤバい。  
然后。。我去了。  
满腔热血。  
糟糕!  
![](./img/FsDFH4VaMAAPnjo.jpg) 
![](./img/FsDFH4TakAI6rqW.jpg) 
![](./img/FsDFH4SaAAUY4lk.jpg) 
![](./img/FsDFH4YaEAE3Y3r.jpg) 



https://twitter.com/takebetter1/status/1639501419534503937  
見～つめる  
magic play is dancing  
緑色に光～るぅ～♪  
看~填满  
Magic play is dancing  
绿色的光芒~♪  
![](./img/FsCt2iSaQAAJfWd.jpg) 



https://twitter.com/Polaris1725/status/1639499131940143104  
これも完売かぁ。😳….ᐟ.ᐟ.ᐟ  
ｽｺﾞ───(〃'艸'〃)───ｨ✨✨✨✨✨👏✨👏🏻✨👏✨👏🏻✨👏✨👏🏻✨👏✨👏🏻✨初日の完売は看板の他にマグカップだったなぁ。明日26日獠ちゃんの誕生日🎂🎉にまた行きます！  
❀.(*´▽`*)❀    
这个也卖完了啊。😳…….ᐟᐟ。ᐟ  
ｽｺﾞ───(〃'艸'〃)───ｨ✨✨✨✨✨👏✨👏🏻✨👏✨👏🏻✨👏✨👏🏻✨👏✨👏🏻✨在第一天销售一空的招牌的其他马克杯的啊。明天26日獠酱的生日🎂🎉又去!    
❀。(*´* ` *)❀  
https://twitter.com/TheEdition88/status/1639462058646458369  
【#キャッツアイ40周年記念原画展 〜そして #シティーハンター へ〜】  
シティーハンター　「トートバッグ Bang！」が完売となりました。入荷は未定となっております。  
【#猫眼40周年纪念原画展~然后向着城市猎人~】  
城市猎人“手提包Bang !”卖完了。还没有决定进货。  
![](./img/FsCKC7dagAAQoGu.jpg) 



https://twitter.com/cosmo2202/status/1639266500686012416  
今日は #キャッツアイ40周年記念原画展  に行ってきたのですが、二つの最終回の生原稿が観れたのは実に眼福ものでありましたよ  
今天去了猫眼40周年纪念原画展，能看到两个大结局的原稿真是一饱眼福。  
![](./img/Fr_YLBNX0AI9nSn.jpg) 
![](./img/Fr_YKNWXgAAstBR.jpg) 
![](./img/Fr_YJLJWIAMdtIV.jpg) 
![](./img/Fr_YH8SXoAEZojr.jpg) 


https://twitter.com/TheEdition88/status/1639135339288875008  
【#キャッツアイ40周年記念原画展 〜そして #シティーハンター へ〜】  
アートトレーディングカードが完売となりました。入荷は未定となっております。  
【#猫眼40周年纪念原画展~然后向着城市猎人~】  
艺术交易卡卖光了。进货还没有决定。  
![](./img/Fr9g5fDacAIbK48.jpg) 


https://twitter.com/TheEdition88/status/1639095811366871041  
【#キャッツアイ40周年記念原画展 〜そして #シティーハンター へ〜】  
実物大「喫茶キャッツアイ」看板はご好評につき完売いたしました。  
入荷は未定となっております。  
实物大小的「Cat'sEye咖啡」招牌由于好评已售完。  
还没有决定进货。  
![](./img/Fr8880laYAAaQAe.jpg) 



https://twitter.com/ryosaeba357xyz1/status/1638669117040525312  
\#シティーハンター in #墓場の画廊 #都会のシンデレラ 編 POP UP STORE
\#XYZ ・・・  
3月24金曜日  
\#中野ブロードウェイ 3階  
墓場の画廊にて待つ  
明日から❗❗❗❗❗  
写真は  
\#キャッツアイ40周年記念原画展 〜そしてシティーハンター へ〜より  
\#槙村秀幸  
のエピソード  
\#城市猎人in#墓地的画廊#都市灰姑娘编POP UP STORE。  
\#XYZ...  
3月24星期五  
中野百老汇3楼  
在坟场的画廊等  
从明天开始…  
这张照片是  
\#猫眼40周年纪念原画展~然后去城市猎人~。  
槙村秀幸的情节  
![](./img/Fr244NBaUAAmqra.jpg) 


https://twitter.com/Sheryl_KITG/status/1638353825156444160  
ジョジョアニメ展の次はこれに行くっきゃない！！  
昨日はジョジョアニメ展出たのが、18時前で無理でした…😭  
昔、浸ってた北条司先生公式サイトの掲示板に神谷明さんご本人がお見えになって、掲示板上で繋がれたのは良い思い出…  


https://twitter.com/Polaris1725/status/1638174814367289344  
原画展行って来た！  
沢山展示してあり大満足！  
版画も美しい…。  
喫茶キャッツアイの看板がリアルで販売してるのも驚いた！😳….ᐟ.ᐟ.ᐟ  
❀.(*´▽`*)❀.  
jojo动漫展之后一定要去这个!   
昨天jojo动画展出来了，18点前不行…😭  
以前，沉浸在北条司老师的官方网站的公告栏上，神谷明先生本人也出现了，在公告栏上被联系起来是美好的回忆…  


https://twitter.com/saiohharuka/status/1638146498226712576  
懐かしいっと友達と連呼  
各話が混ざってるし、最終話って2つ？！とか新たな発見があったのですぐ読み直す！！  
\#シティーハンター もあって贅沢な原画展でした  
怀念地和朋友连呼  
每个故事都混在一起，最終話是两个？！因为有了新的发现，所以马上重新读！！  
还有城市猎人，是一个奢华的原画展。  
![](./img/FrvdjlNaAAAmjVD.jpg) 
![](./img/FrvdjlIaQAAutHp.jpg)  
https://twitter.com/saiohharuka/status/1638146902440161282  
誤字と投稿エラーで再掲。。  
もともと海原さんはキャッツが先だったよな〜とか冴羽さんのモデルこの人だったなぁ〜とかシティーハンターがらみもたくさん思い出し、大満足な私  
转载错字和投稿错误。  
海原先生本来就先是在Cat's的吧~还记得了很多城市猎人，比如还是冴羽的原型啊~~想起了很多关于城市猎人的事，我非常满足  


https://twitter.com/Lol_un_ily/status/1638147872616255488  
大大大満足そして大大大好きだと再び自覚した、  
\#キャッツアイ と #シティーハンター の原画展でした😍  
見事に #あなたのハート再び盗みにまいります に盗まれました  
今から読み直しますっ  
非常非常满足，并且再次意识到非常非常喜欢。  
\ #猫眼和#城市猎人的原画展😍  
你的心再次被偷走了。  
我现在开始重读。  
![](./img/FrvezmNaQAAF2xQ.jpg) 


https://twitter.com/TheEdition88/status/1638084558527819777  
本日より開催‼️  
初日から多数のご来場ありがとうございます☺️  
松坂屋名古屋店にて、瞳・愛・泪が皆さまをお待ちしております❣️  
関連商品はECでも販売中！（一部除く）  
即日起举办!️  
感谢从第一天开始众人的到场☺️  
在松坂屋名古屋店，瞳·爱·泪等着大家❣️  
相关商品也在电商销售中!(部分除外)  
https://edition-88.com/collections/hojotsukasa  
アクリルスタンド、再販お願いいたします！  
丙烯支架，请转售!  
![](./img/FrulOMlaMAA7v-b.jpg) 
![](./img/FrulOMWakAUyXxg.jpg) 
![](./img/FrulOLsaEAAQw1c.jpg) 



https://twitter.com/hy7709/status/1638080858379935746  
キャッツアイ40周年記念原画展 in名古屋に行ってきた。  
　東京展いけなったので、まさか名古屋に来てくれるとは！感謝  
「キャッツ」との出会いは、再放送アニメでその時ジャンプではシティーが連載中でキャッツは終わってたけどコミック買い揃えるくらいハマりました。  
我去了猫眼40周年纪念原画展in名古屋。    
去了东京展，没想到又来名古屋了!感谢  
和「Cat's」的相遇，在重播动画中那个时候在JUMP上有城市连载中，虽然Cat's结束了，但是迷上了买漫画的程度。  
![](./img/Fruh2rYaQAEG8zL.jpg) 
![](./img/Fruh2rWagAAbdXw.jpg)  
キャッツとシティーはアニメ化されてどちらも好きだけど、自分は「キャッツ」派かな。  
先生の今の写実的な現代風タッチも良いけど硬い感じがして、昔の柔らかく暖かみのあるタッチのが好き。懐かしい気持ちにさせてくれた1日でした。ありがとうございます。  
《猫》和《城市》都被动画化了，我自己是「Cat's」派吧。  
老师现在的写实的现代风笔触也很好，但是感觉生硬，我喜欢以前的柔和的温暖的笔触。让我怀念的心情的一天。谢谢。  
![](./img/FrwrBQjacAAjZBy.jpg) 
![](./img/FrwrBQjakAA8WXh.jpg) 
![](./img/FrwrBQnaMAABdRl.jpg) 



https://twitter.com/shuna_mu/status/1638063528098824193  
きゃー"(ﾉ*>∀<)ﾉ"(ﾉ*>∀<)ﾉ"(ﾉ*>∀<)ﾉ"(ﾉ*>∀<)ﾉ  
哇!    
![](./img/FruSF4eaEAIA-5V.jpg) 


https://twitter.com/kahoru1010/status/1638059850554040320  
東京でも見ましたが、やっぱり原画は何度見てもいいですねヽ(=´▽`=)ﾉ  
今日は誕生日だったのですが、いい思い出になりました。  
ありがとうございました！  
今度は獠ちゃんのお誕生日に来ます！  
在东京也看了，果然原画看多少遍都好啊(=▽`=)  
今天是我的生日，留下了美好的回忆。  
谢谢大家!  
这次獠酱的生日要来了!  

https://twitter.com/ryosaeba357xyz1/status/1638046662815010816  
とっても素敵だと思いませんか❗  
\#キャッツアイ40周年記念原画展 〜そして #シティーハンター へ〜  
で  
\#墓場の画廊  
から出てるグッズなんです  
你不觉得很棒吗！  
\#猫眼40周年纪念原画展～然后向着#致城市猎人#～  
在  
\#坟墓画廊  
的商品。  
![](./img/FruCwAUaIAAkxJ7.jpg)  




https://twitter.com/JHII8xmjxcpawsk/status/1638036853663342594  
行ってきました❗️間近でみる北条先生の原画、素敵でした。 #キャッツアイ40周年記念原画展  
去了！近距离看北条老师的原画，很棒。猫眼40周年纪念原画展  



https://twitter.com/ryosaeba357xyz1/status/1637948497491288065  
僕にとって  
\#CATSEYE  
宝なんです   
今日行きますね  
\#キャッツ❤️アイのfan  
对我来说  
\#catseye  
都是宝贝  
今天去吧  
\#Cat's❤Eye的fan  
![](./img/FrspejwaMAECDqb.jpg) 


 
https://twitter.com/TheEdition88/status/1637692516689645568  
【#キャッツアイ40周年記念原画展 〜そして #シティーハンター へ〜】  
いよいよ明日より開催‼️  
只今絶賛設営中です  
名古屋会場から発売される新商品も、会場とEDITION88のECサイトにて同時発売されます  
【#猫眼40周年纪念原画展 ～然后向着#致城市猎人 ～】  
终于明天开始召开！！  
现在正在筹备中  
名古屋会场发售的新商品也将在会场和EDITION88的电子商务网站同步发售。  
![](./img/FrpApfoaYAAjQcM.jpg) 
![](./img/FrpApglaMAAbMlT.jpg) 
![](./img/FrpApflaEAManoW.jpg) 





https://twitter.com/TheEdition88/status/1631588330508005377  
【#キャッツアイ40周年記念原画展】読切作品「#シティーハンター -ダブルエッジ-」の額装準備中✨  
美しい生原稿のオーラにドキドキです😍  
松坂屋名古屋店で3/21より開催の原画展で展示いたします  
【#猫眼40周年纪念原画展】短篇作品《#城市猎人-Double Edge-》裱框准备中✨  
美丽的原稿的气场令人心动😍  
将在3/21松坂屋名古屋店举办的原画展上展示。   
![](./img/FqSQ700aEAYDXcf.jpg) 



https://twitter.com/ryosaeba357xyz1/status/1606751951341129728  
@Canal_official
\#キャナルシティ博多  
の皆様  
\#キャッツアイ40周年記念原画展  
の時は親切な感じのよいご案内、記念撮影  
ありがとうございました❗❗❗❗❗  
東京  
\#吉祥寺 
の   
\#カフェゼノンサカバ  
より  
@canal_official  
博多广播剧  
各位同仁  
猫眼40周年纪念原画展  
的时候亲切的感觉好的向导，纪念摄影。  
谢谢大家❗❗❗❗❗  
东京  
吉祥寺  
的  
\#芝诺酒卡瓦  
比  
![](./img/abc.jpg)  





https://twitter.com/pwfregonese/status/1638447096130080769  
Allez à Nagoya exprès pour visiter la très cool exposition #キャッツアイ40周年原画展 de @TheEdition88  
 ! Si ça intéresse du monde, je ferai un thread dessus, vous me dites  
特意去名古屋参观@TheEdition88举办的非常酷的#Cat'sEye 40周年原画展  
 ! 如果有人感兴趣，请告诉我，我会做一个主题。  
![](./img/FrzudZiaAAAgtCa.jpg) 

https://twitter.com/pwfregonese/status/1638542474682728450  
En route pour l'exposition  #キャッツアイ40周年原画展  consacrée principalement à Cat's Eye, mais pas seulement ! 350 pièces en tout avec du City Hunter XYZ, City Hunter: Double Edge - première mondiale - et du Space Angel (1/357)  
去参加#Cat'sEye 40周年原画展，主要是猫眼的展览，但不仅仅是! 城市猎人XYZ、城市猎人：双刃剑--全球首发--和太空天使（1/357）共350件作品。  
![](./img/Fr1A5h1aIAAT9HD.jpg) 
![](./img/Fr1BabWaUAI9E3A.jpg) 

https://twitter.com/pwfregonese/status/1638542481355833345  
L'exposition a lieu au Matsuzakaya, une sorte de Galerie Lafayette. D'où l'éclairage particulièrement agressif qui flingue bon nombre de photographies, vous m'en excuserez.  
展览在松坂屋举行，这是一种Lafayette画廊。因此，特别猛烈的灯光会毁掉很多照片，请原谅。  
![](./img/Fr1Be-TaYAIEGsg.jpg) 

L'accueil se fait avec des .357 Magnum, bien évidemment, et les collections des tankōbon et les principaux Jump.  
欢迎的是用.357Magnum手枪，当然，还有tankōbon和主要Jump系列的收藏。  
![](./img/Fr1B8rwakAAEaSr.jpg) 
![](./img/Fr1CG3yaMAAiftH.jpg) 

Ensuite viennent les fiches personnages de Cat's Eye.  
然后是Cat's Eye角色表。  
![](./img/Fr1CXMaaMAApggP.jpg) 

Puis une folle quantité de manuscrits en noir et blanc, ou en couleur, accompagnés de quelques commentaires. On revient sur les altercations entre Hōjō et le Jump au sujet des corpulences (trop minces) et des visages (les mêmes pour les Cat's Eye, seules les coiffures changent)  
然后是数量惊人的黑白手稿，或者说彩色手稿，还有一些评论。我们又回到北条和Jump之间的争吵，关于身体（太瘦）和面孔（猫眼也一样，只是发型变了）  

Le mangaka voulait commencer #CityHunter directement avec Kaori, puis revenir sur son passé par flashbacks. Là encore Horie et le Jump lui ont demandé de changer la structure. De même, Kaori a une coupe courte - très mal vu au Jump - car elle vient d'un one-shot à l'origine.  
漫画家想让香直接从《城市猎人》最初登场，然后转而回忆她的过去。在这里，堀江和Jump再次要求他改变叙事结构。另外，香是短发--这在Jump中非常不受欢迎--因为她最初来自一个短篇故事。    

Particulièrement intéressant, toutes les planches de Space Angel, le Cat's Eye dans l'espace sauce Cobra, sont visibles ! De même que celles de City Hunter: Double Edge avec les annotations.  
特别有趣的是，Space Angel、Cobra空间Cat's Eye的所有板块都清晰可见！City Hunter: Double Edge的板块都有注释！（译注：待校对）  
![](./img/Fr1ETOfakAAGddB.jpg) 
![](./img/Fr1Ef-daUAcHZ21.jpg) 

Enfin, les goodies sont fous et déjà en rupture de stock pour la plupart. Des inédits, des reproductions, etc. J'ai même vu un mec devant moi remplir trois valises complètes. La balayette n'était pas loin.  
最后，好东西都是疯狂的，大部分都已经断货了。未发行的，复制品，等等。我甚至看到在我前面的一个人装满了三个完整的手提箱。刷子就在不远处。  

Enfin, le plus important - et j'y suis allé pour ça : il était possible d'obtenir des reproductions dédicacés à la main par Tsukasa Hōjō en nombre ultra limité et pas franchement données. J'ai ainsi pu obtenir ma préférée. Maintenant, je cherche des piges pour la rembourser.  
最后，最重要的是--我是为了这个去的：可以买到北条司的手工签名复制品，数量极其有限，而且价格也不便宜。所以我得到了我最喜欢的。现在我在找东西来偿还她。  





https://twitter.com/pio_pi03/status/1638400845246246913  
念願だったキャッツアイ原画展…  
最高だった…  
1ヶ月くらい前に通販で版画買ってて、まだ届いてないのが本当に惜しい😭  
早くきて…  
期間中もう一回くらいは行きたい…  
期待已久的 "猫眼 "原创艺术展...  
真是太好了...  
我大约一个月前通过邮购购买了版画，真的很遗憾还没有到货😭。  
我等不及了...  
我想在展览期间至少再去一次...  
![](./img/FrzE4ghaQAASm-I.jpg) 
![](./img/FrzE4gjakAAcoCE.jpg) 
![](./img/FrzE4hAaEAAA1-i.jpg) 


https://twitter.com/ruumii22/status/1638170769439199235  
\#キャッツアイ40周年原画展 名古屋にて鑑賞してきました。  
正直キャッツアイのストーリーはほぼ覚えてなかったので、そんないい話で終わってたなんて！  
北条司先生の貴重な原画を見れて良かったです😊  
そして #シティーハンター 新作映画が！  
\#あなたのハート再び盗みにまいります  
\#ˈ我刚刚在名古屋观看了#猫眼40周年原创艺术展。  
说实话，我几乎不记得《猫眼》的故事了，我不知道它最后会成为一个这么好的故事!  
很高兴能看到北条刚的珍贵原画😊。  
还有#城市猎人新电影！  
\#我会再来偷你的心  

https://twitter.com/Polaris1725/status/1639916826401570816  
我が家にも原稿やってきた！泪姉のタカラジェンヌな男装は昔から好きだった～毎日見れるのは凄く嬉しい！俊夫のワンマンプロポーズはアニメで見ても面白いし！ブラインドグッズは全て瞳ちゃん！😳….ᐟ.ᐟ.ᐟでも素敵！獠ちゃんと香ちゃんのこのシーン面白くて大好き！  
原稿来到了我们家! 我一直很喜欢泪姐的塔卡拉丁男装--每天都能看到它，真好! 而俊夫的单人求婚在动漫中也很有趣! 所有的盲品都是瞳! 😳.... ᐟ. ᐟ. ᐟ但很可爱! 我喜欢獠和香之间的这一幕，很有趣，我喜欢!  
（译注：タカラジェンヌ系宝冢歌剧团和宝冢音乐学校的女演员的昵称。[宝ジェンヌ](https://kotobank.jp/word/宝ジェンヌ-559541)）  
![](./img/FsInqL7akAAEFyr.jpg) 


https://twitter.com/tomo_niko_niko/status/1638888335409315840  
今日、原画展を見に行ってきました😊  
今見てもキャッツアイもシティーハンターも素敵な作品です  
スペースエンジェルを拝見できて幸せでした!   
我今天去看了原画展😊。  
现在看《猫眼》和《城市猎人》都很精彩!  
我很高兴看到《太空天使》!  
![](./img/Fr6APyAakAEjxGq.jpg) 



https://twitter.com/mory8686/status/1637850262219161601  
遂にヤツらがベールを脱いだ。  
2枚目→ヤラセph  
3枚目→ホントのドロ  
他们终于揭开了面纱。  
第2张→雅拉塞ph。  
第3张→真正的ドロ  
![](./img/FrrQH_NacAEXhU7.jpg) 
![](./img/FrrQH_QaMAALbUB.jpg) 


https://twitter.com/andGALLERYpopup/status/1638378231631052801  
【営業中】  
2日目も朝からお客様がいらしてくださっています✨✨  
ありがとうございます😊  
コラボクリアファイルが500円で一番お買い求めして頂きやすく人気です！  
ぜひお早めのご来店、お待ちいたしております！  
【营业中】  
我们从早上开始就有顾客来了，这是第二天✨✨。  
谢谢你😊。  
合作的ClearFile是最受欢迎的，500日元就能买到！！  
我们期待着您能尽快到店里来!  
![](./img/FrywT-5acAAXMFX.jpg) 
![](./img/FrywT-0aEAAb4Nz.jpg) 


https://twitter.com/ihcnak19/status/1638753160247513088  
キャッツアイって確か三姉妹が亡くなったお父さんの作品を集めてたんだっけ
杏里さんのこの曲で保育園のお遊戯会で踊ったのを思い出した  
多分サンディーみたいにダイナミックには踊れてないと思うけど  
Cat'sEye，我想，是三姐妹已故父亲的作品集。  
我记得在幼儿园的操场上，我跟着杏里的这首歌跳舞。  
也许我没有像サンディー那样跳得那么有活力。  

https://twitter.com/TokudaiRainbow/status/1638216306209210369  
【ビバ！ 1970年代アイドル】「#キャッツアイ」さん。ノン（大谷親江）さんとナナ（山中奈奈）さんで構成され、正式表記は「キャッツ★アイ」。1977年「アバンチュール」でデビュー、1978年に解散。おそらく「ピンク・レディー路線」を模索したと推察。  
[Viva! 1970年代的偶像] 「#Cat'sEye」。 由ノン（大谷千惠）和ナナ（山中奈奈）组成，正式称为「Cat's★Eye」，于1977年以「Aventure」出道，1978年解散。 据推测，他们试图走「Pink lady路線」。  
![](./img/FrwdCDGaEAMdqk_.jpg) 
![](./img/FrwdBGAaEAIJNh8.jpg) 
![](./img/Frwc_6XakAIFkOD.jpg) 
![](./img/Frwc-S8aYAExWXT.jpg) 

https://twitter.com/ajtmdmjj/status/1640729804595949568  
今更ながら新台で入った  
6.5号機キャッツアイを打ってみた  
最初の1時間は  
液晶、出目、リールロック、楽しい  
700枚ほどでたので  
機種の解析を見る  
キャッツ目？ボナ無抽選区間？？  
3時間後、アラジン打ってた😅  
到现在才用新台进来  
打了6.5号猫眼  
最初的一个小时  
液晶，凸眼，旋转锁，快乐  
大概有700张，  
看机种的分析  
猫目?波纳无抽签区间? ?  
3小时后，阿拉丁敲响了😅  
https://twitter.com/19_evangelion/status/1639572659519504386  
ビタ練習に来た…ええもんみれた🤤  
来练习了…看到了好东西

https://twitter.com/j6131/status/1639945853376004098  
昨日は北条司先生の #キャッツアイ #シティーハンター の原画展行ってきた
恥ずかしながらキャッツアイは勉強不足やけど、シティーハンターは大好きでな
昨天去了北条司老师的#猫眼#城市猎人的原画展。  
说来惭愧，我对猫眼的学习还不够，但很喜欢城市猎人。  
![](./img/FsJCD0faEAM7ck3.jpg) 



https://twitter.com/shin99shin/status/1639963927093329920  
![](./img/FsJSflJaUAEXkGK.jpg) 

https://twitter.com/AoiTakarabako/status/1637937678632255489  
おパヨ〜ございます  
欢迎光临!  
![](./img/Frsfoq8agAEnLe5.jpg) 



https://twitter.com/iroiroiro_ha5/status/1639362532879261696  
どなたかへ  
敬请关注  
　#キャッツアイ  
　#北条司  
　#松坂屋  
　#中日新聞　3/24（金）  
![](./img/FsAviDlaIAAVjTo.jpg) 


https://twitter.com/okameinko1bunch/status/1639599310584320002  
\#キャッツテール　という植物を見かけました。猫のしっぽのようなモフモフのお花🐈‍⬛可愛いですね　#1日1レモンちゃん　#カナリア　#文鳥　#オカメインコ　#NORISAN #illustration #キャッツアイ  
我看到了一种叫猫尾巴的植物。猫的尾巴一样的モフモフ🐈‍⬛花可爱啊# 1日1柠檬酱#金丝雀文鸟オカメインコnorisan illustration # # # # #猫眼  
![](./img/FsEG4ZqaMAEGsol.jpg) 
![](./img/FsEG4ZlaEAE3WUr.jpg) 


https://me-houdai.com/cats-eye-anime/?utm_source=ReviveOldPost&utm_medium=social&utm_campaign=ReviveOldPost  
アニメ「キャッツ・アイ」シリーズを視聴できる動画配信サービス(VOD)比較  
可以观看动画片“猫眼”系列的视频发送服务(VOD)比较  
投稿日：2019年1月28日 更新日：2020年1月28日  











https://twitter.com/_chigaya05_/status/1643598921388924930   
最終日に行ってきました。名古屋での開催ありがとうございました。劇場の予告を聴きながら生原を見る最香な時間を過ごせました✨幸せ☺️  
最后一天去了。感谢在名古屋的召开。度过了一边听剧场预告一边看生原的最香的时间✨幸福☺️  
![](./img/Fs88gWaaIAA8UOZ.jpg) 
![](./img/Fs88gWfaAAE52be.jpg) 
![](./img/Fs88gWcagAAXaPg.jpg) 



https://twitter.com/Polaris1725/status/1643590884188131328  
最終日の今日も行ってきました。穴が空くほど見てきました。
開催中は本当に楽しかったです。💗💕名古屋に来て頂きありがとうございました。✨✨✨✨✨🌈🍀
(❁ᴗ͈ˬᴗ͈)  
最后一天的今天也去了。看得几乎要破了个洞。
举办的时候真的很开心。💗💕感谢您来到名古屋。✨✨✨✨✨🌈🍀
(❁ᴗ͈ˬᴗ͈)  
![](./img/Fs81MLEaQAEehpg.jpg) 
![](./img/Fs81MLEaIAAAEHi.jpg) 


https://twitter.com/QaTbun/status/1643512004660039680  
最終日に漸く行けました。
往復🚲で80分つ…疲れた…😅
スペースエンジェルの名前が色々ツッコミどころ満載で楽しかったです。セイラやらアキラやら…！  
最后一天终于去了。
往返🚲80分钟…累了…😅
space angel的名字充满了各种槽点，很开心。塞拉和阿基拉什么的…!  
![](./img/Fs7tc_baQAAU-eK.jpg) 
![](./img/Fs7tc_faEAE79vm.jpg) 
![](./img/Fs7tc_eaUAALRYH.jpg) 
![](./img/Fs7tc_caQAASj72.jpg) 


https://twitter.com/TheEdition88/status/1643442829308596225  
本日最終日 最終入場は17:30まで❗️
\#北条司 先生がセルフリメイクした「スペース・エンジェル」は会場限定で展示しています👀さらに劇場版シティーハンターのPVも公開中🎞
松坂屋名古屋店にて皆様のお越しをお待ちしています  
今天最后一天 最后入场到17:30❗️
\#北条司老师重绘的“SpaceAngel”在会场上限定展示👀进一步剧场版城市猎人的pv也公开中  
松坂屋名古屋店恭候您的光临。  
![](./img/Fs6uibPaQAA_gx-.jpg) 
![](./img/Fs6uiaBaEAEkfKn.jpg) 
![](./img/Fs6uiZvaIAAoQKA.jpg) 

https://twitter.com/TheEdition88/status/1642473408259829760  
4月5日（水）まで開催📣残りあとわずか！！
シティーハンター ダブル・エッジの生原稿は名古屋会場から初展示✨
\#北条司 先生の筆跡を間近で見れる貴重な機会です✍️  
在4月5日(周三)之前举行，剩下的时间只有一点！！
城市猎人DoubleEdge的原始手稿从名古屋会场首次展出。
\#这是一个难得的机会，可以近距离看到北条司老师的笔迹。  
![](./img/Fss828xaEAEWp1E.jpg) 
![](./img/Fss828vaEAECcbI.jpg) 




https://twitter.com/retrofuture_08/status/1642118278318530561  
ぎりぎりまで行くか迷ってたけど、やっぱり行ってよかった😭😭
東京で何度も何時間も見たはずなのに、新たな気づきもあったりでまた写真パシャパシャ取っちゃった…やはり生は最高だ……   
直到最后去犹豫了,不过,还是去好了😭😭
明明在东京看了好多次好几个小时，又有了新的发现，于是又拍了照片…果然生是最棒的……  
![](./img/Fsn524caUAArVxb.jpg) 
![](./img/Fsn52MpacAAqn9o.jpg) 
![](./img/Fsn51nIagAAJD0Q.jpg) 


https://twitter.com/Polaris1725/status/1639486451950964736  
ﾟ+｡:.ﾟおぉ(*ﾟOﾟ *)ぉぉﾟ.:｡+ﾟ完売したんだぁ～。凄い！どんな人が買ったんだろう。私なら版画だなぁやっぱり…💕21日に行った時も申し込み書がある所には結構人がいた。スーパーロボット&ヒーローも同時開催だったから。     
啊~ ~ +゚销售一空了。好厉害!什么人买的吧。如果是我的话还是版画啊…去💕21日时也有申请书所相当的人。超级机器人&英雄也是同时举办的。  
https://twitter.com/Polaris1725/status/1639492882569789440  
初日に行った時には既に申し込み書に赤線引いてあったなぁ。でも今日にお知らせって…❓❓❓でもこれ外に置くものなのに雨ざらしは嫌やなぁ～。と言う逆心理。読書の行灯代わり？気が散って本が読め～～～ん！  
(￣▽￣;) (*´艸`)ﾌﾟﾌﾟ  
あれこれ想像したら面白くなった！笑笑笑  
第一天去的时候已经在申请书上画了红线了。但是今天要通知…❓❓❓不无论是素食还是放在外面的东西，都不想下雨啊~的逆反心理。代替读书的灯笼?分心看书~ ~ ~嗯!  
(￣▽￣;)(*´艸`)pup  
想象了这么多，觉得很有趣!笑笑笑  

https://twitter.com/Polaris1725/status/1644184999313408001  
いつ見てもこのポスター素敵ですよね。フランスの方々が北条司先生の作品を好きでいてくれるのが凄く嬉しい。
日本でも放送してくれないかなぁ。  
不管什么时候看这个海报都很漂亮呢。我很高兴法国的人们喜欢北条司老师的作品。
在日本也能播放吗?  
https://twitter.com/Polaris1725/status/1642522704787161088  
ひろゆきさんがパリにいる事はYouTubeで知ってたけどアカウントでキャッツアイに出会えるとは！嬉しい！
「シティーハンター」も実写映画凄く面白かったのでこれも観たい！日本でも円盤出してくれないかなぁ。
ポスターも美しい！
このポスター、フレームに入れて飾ったら素敵だろうなぁ。   
但没想到能在账号上遇到猫眼！我很高兴！
“城市猎人”真人电影非常有趣,所以也想看这一部!日本也能推出光碟吗?
海报也很美!
把这张海报放在镜框里装饰应该很漂亮吧。  

https://twitter.com/hirox246/status/1642170052391714816  
木更津じゃないほうの『キャッツアイ』がフランスで実写テレビドラマになる様子。
2018年に『シティーハンター』もフランスで実写映画化されてるけど、フランス人は北条司先生が好きなんかね。  
木更津的《猫眼》在法国拍真人电视剧的样子。
2018年《城市猎人》也在法国拍成真人电影，法国人喜欢北条司老师吗?  

https://twitter.com/vf_cqs/status/1645920157234249728  
キャッツアイ 来生三姉妹の血液型 (イメージ)  
瞳:A型  
泪:AB型  
愛:O型  
こんな感じです。  
猫眼来生三姐妹的血型(形象)是这样的感觉:  
瞳:A型  
泪:AB型  
爱:O型  



https://twitter.com/kuzuo555/status/1645713186442870786  
AT初期ゲーム数300Gを引いて完走を夢見たヒキ強の末路です。
お納めください。
\#パチスロ #キャッツアイ
\#ムズ過ぎて草しか生えんわ  
AT初期游戏数抽300g梦想跑完全程的抽强的末路。
请收好。
\#柏青哥#猫眼
太过了，只长草了。  
![](./img/Fta_ameaIAA3vw1.jpg) 


https://twitter.com/shinoro_cats/status/1645632648562237440  
こんにちわ🎵
本日も少し曇り空☁で合間に太陽が☀見えますね😺
最高気温も高めだけど風があるので肌寒いですねー  
你好🎵
今天也有点阴天☁间隙太阳☀看到😺
最高气温也很高，但是因为有风，所以有点冷。  

https://twitter.com/Analox7/status/1645042646946320384  
![](./img/FtRdkamaEAEelxR.jpg)  


https://twitter.com/Sakesukinohito/status/1645011056404885504
「君はセクシーな泥棒」  
1983年なら東京でコーヒーが一杯300円で飲めた時代。  
物価が高くなったと改めて実感します。  
“你是性感的小偷。”
1983年，在东京一杯咖啡300日元就能喝到。
我再次切身感受到物价上涨了。  
![](./img/FtQ_fataEAAZiPY.jpg) 


https://twitter.com/4egH8mW5mnX71qk/status/1644453454461673478
そう言えば放送当時の制作会社は東京ムービー新社だったと思うんだけど…  
再放送に合わせて字幕替えたのかな？  
这么说来广播当时的制作公司应该是东京电影新社…
是配合重播换了字幕吗?  
![](./img/FtJFsJFaQAAdCsw.jpg) 

https://twitter.com/toyojewelry/status/1644253381672996870  
おとなしそうなカボションストーンだけど、光を当てると…✨
トルマリンキャッツアイ😍
coming soon👻
プレゼント企画応募〆切まであと3️⃣日です  
看起来很老实的南瓜石，但是用光的话…✨
Tourmaline cat's-eye😍
coming soon👻
礼物企划〆切还有3️⃣日应征。  
![](./img/FtGPu--aUAAqfWT.jpg) 
![](./img/FtGPu_DaAAAipDz.jpg) 
![](./img/FtGPu--aEAAWoDX.jpg) 


https://twitter.com/Analox7/status/1644205506242945030  
![](./img/FtFkMXVakAECosB.jpg) 
![](./img/FtFkL2pacAA4bti.jpg) 
![](./img/FtFkLSWaEAEHd5h.jpg) 

https://twitter.com/Analox7/status/1613579887792771072  
![](./img/FmSWWA-aEAECWjD.jpg) 
![](./img/FmSWVlQaMAAxpjo.jpg) 
![](./img/FmSWVGIaMAAekdO.jpg) 

https://twitter.com/Analox7/status/1606311449097555969  
![](./img/FkrDv6mVEAEFwk5.jpg)   
https://twitter.com/Analox7/status/1592471337364492289  
![](./img/FhmYPckUYAANAuC.jpg) 


https://twitter.com/marty_00/status/1644163554650763264  
サンテレビで始まりました。
特にエンディングが見たかった。しかも日本語EDは13話までなので貴重w  
sun电视台开始了。
特别想看结尾。而且日语ED到13话很珍贵。  
![](./img/FtE-Cb_aMAAlVfo.jpg) 
![](./img/FtE-Cb2aQAAKhbU.jpg) 
![](./img/FtE-CcBaYAEGOsN.jpg) 

https://twitter.com/kz_brother/status/1644105861403643904  
嘘みたいだろ・・・
これ、令和の地上波なんだぜ・・・
新作じゃなくて再放送・・・  
好像是骗人的吧……
这是令和的地面波。
不是新作而是重播···  
![](./img/FtEJj2CaMAEsL-y.jpg) 


https://twitter.com/raging_storm121/status/1644105348389945345  
予告のラストカットとエンドカードが繋がっててビビったww  
「おわり」の書体の可愛らしさよ。   
预告的最后剪辑和结局卡连在一起吓了一跳的ww
“尾”字的字体很可爱。  
![](./img/FtEJGiLacAIvgWN.jpg) 
![](./img/FtEJGa_aUAAUbIm.jpg) 

https://twitter.com/raging_storm121/status/1644104482115170304  
シャフ度エンド草。
トシが三姉妹から内心「プークスクス」されながら生きていくアニメだということだけは理解できた。(適当感)  
弯度草。
我只能理解，这是一部讲述阿敏在三姐妹的内心“扑哧扑哧”中生活的动画片。(适当感)  
![](./img/FtEIUGDaIAInTB8.jpg) 

https://twitter.com/raging_storm121/status/1644103778621329408  
昔めちゃイケで光浦さん雛形さん紗理奈さんのコントで見ただけやったから、本物をやっと観れて感動してる  
以前在美浓池光浦先生雏形先生纱理奈先生的小品只看过，终于看到真材实料很感动。  
![](./img/FtEHrHNaEAAxIBI.jpg) 

https://twitter.com/raging_storm121/status/1644102284778045440  
近畿圏民の男の子達が朝っぱらから目覚めちゃう  
近畿地区的男孩们一大早就醒了。  
![](./img/FtEGULVagAAN_T2.jpg) 

https://twitter.com/raging_storm121/status/1644101343353913344  
この紙製の買い物袋(？)も80～90年代辺りのノスタルジーを感じて良いっすねぇww  
这个纸制购物袋(?)也能感受到80 ~ 90年代的怀旧感觉真好。  
![](./img/FtEFdV2aYAAkaw5.jpg) 

https://twitter.com/raging_storm121/status/1644100051843846148  
なんだろ、キャラデザやら雰囲気やら喫茶店要素やら、まるでシティーハンターを観ているような気分になるなぁ～👀  
什么呢，造型、气氛、咖啡厅要素，简直就像看《城市猎人》一样呢~👀  
![](./img/FtEESLcaYAQAid1.jpg) 
![](./img/FtEESDDaMAMR5k_.jpg) 
![](./img/FtEER7AaUAMMmKg.jpg) 

https://twitter.com/raging_storm121/status/1644099079428014080  
初手からこち亀みたいな大惨事になっててワロタ  
从一开始就酿成了这样的惨剧。  
![](./img/FtEDZoqakAAvacd.jpg) 

https://twitter.com/raging_storm121/status/1644098351527505920  
5時前からアップしてました。
今踊ってます。
緑川光？  
5点前就开始上传了。
我现在在跳舞。
绿川光?  
![](./img/FtECvNvaMAASgAZ.jpg) 

https://twitter.com/koutetusanbo/status/1643935986681020416  
明日からサンテレビの朝が、「#キャッツアイ」「#うる星やつら」と並んでて、ビバ80年代であることよ  
从明天开始sun电视台的早晨，“#猫眼”和“#福星小子”并列，就是viba 80年代了。  

https://twitter.com/acecrown904/status/1643901901166219264  
【画像】北条司の「キャッツ・アイ」、フランスで実写化！！ひろゆき氏が地元情報を伝える！！  : 最強ジャンプ放送局  
【图像】北条司的猫眼电影，法国真人化! !弘之先生传达本地信息! !最强jump放送局  
[【画像】北条司的猫眼电影将在法国真人化! !](./saikyo-jump.com_2023-04-06.md)  
 



https://twitter.com/touhokusnk/status/1643820919306203137  
上出来！時差が弱いので即やめです。  
做得好!因为时差差，所以马上停止。  
![](./img/FtAGZ_yaMAAlFwQ.jpg) 


https://twitter.com/ayahietc_dqx/status/1643391930418741255  
4/5は、声優の故･藤田淑子さんの誕生日です。  
4月5日是已故声优藤田淑子的生日。  
![](./img/Fs6AQAdagAE7G5I.jpg) 

https://twitter.com/mma9135/status/1643250629488541697  
禁パチ90日目無事終了。
今夜の画像はキャッツアイ。
この台とは関係ないけど、めちゃイケでやってたキャッツアイのコント好きだったなぁ。
禁Pa第90天顺利结束。
今晚的画面是猫眼。
虽然和这个台没有关系，但我很喜欢在猫眼里演的小品。  
![](./img/Fs3_ZiKaMAECd_y.jpg) 
![](./img/Fs3_ZgSaIAAOidO.jpg) 


https://twitter.com/onasunokotono/status/1643190289069375488  
![](./img/Fs3I263aQAAbLCR.jpg) 


https://twitter.com/Yoshiro35913325/status/1643048253880164357  
先週末、３日ぶりの名古屋訪問！
今度は違うメンバーで、しかもキャッツ展のためだけに行きました😀
感動しました。連載デビューで、元々漫画家とか目指していなかったはずなのにあのクオリティなんて  
上周末，时隔3天的名古屋之行!
这次不同成员,而且去了只猫展😀
我很感动。连载出道，本来就不是以当漫画家为目标的，怎么会有那样的品质?  
![](./img/Fs1HquAaYAUsXEk.jpg) 

https://twitter.com/sea_na0315/status/1642294800610390016  
昨日、ディスクみたいなやつやろ？程度でほぼ無知識でキャッツアイ打った。
何かあれよあれよと続くし、終わったと思ったら引き戻すしで2000枚出た。
途中休憩終わりでそれまで100%だったビタを４連續でミスって頭？になったけどやや遅め意識で修整。
ベル菱形、平行四辺形は美しい  
昨天，不是光盘那样的东西吗?在程度上几乎没有知识的情况下打了猫眼。
有些东西还在继续，以为结束了又拉回来，出了2000张。
中途休息结束之前100%的bita 4连续的错误?，不过稍微晚了修正意识。
贝尔菱形、平行四边形很美  
![](./img/FsqaZ18aEAA1Rmo.jpg) 
![](./img/FsqaYp_acAA9rb7.jpg) 
![](./img/FsqaW_4akAEmRmg.jpg) 


https://twitter.com/hidyk623/status/1641978254935597056  
さて観ますかね(≧∀≦)  
那么就看了吧  
![](./img/Fsl6g-caUAEFiUZ.jpg) 
![](./img/Fsl6g-baAAET8Ds.jpg) 
![](./img/Fsl6g-UaYAUuW2b.jpg) 


https://twitter.com/AlohaPete/status/1645027742822572032  
I went to a rad #CityHunter pop-up show with my buddy @Muminazuki  
They had the most interesting City Hunter and #CatsEye items including original manga pages and honey from the Cat’s Eye cafe featuring Umibozu!    
我和我的朋友@Muminazuki去了一场很棒的#CityHunter快闪秀  
他们有最有趣的城市猎人和#猫眼物品，包括原创漫画页面和猫眼咖啡馆的蜂蜜，以海坊主为特色!  
![](./img/FtRP77OacAISeu1.jpg) 
![](./img/FtRP77PaUAEkn5l.jpg) 
![](./img/FtRP77TaEAAQHZO.jpg) 
![](./img/FtRP77WagAI0-G-.jpg) 

https://twitter.com/LeZit_/status/1643204451300196353  
Tsukasa Hôjô, invité d'honneur à #JapanExpo Paris 2023 🇫🇷 ! 
De #CityHunter à #CatsEye, l’œuvre de Tsukasa HÔJÔ a marqué l’histoire du manga et il est l’un des mangaka les plus emblématiques.
L’année 2020 a marqué le 40ème anniversaire de sa carrière et c’est à Japan Expo qu’il…  
Tsukasa hojo， #JapanExpo Paris 2023🇫🇷的主要嘉宾!
从#CityHunter到#CatsEye, Tsukasa hojo的作品标志着漫画的历史，是最具代表性的漫画之一。
2020年是他职业生涯40周年，在日本世博会上，他……  

https://twitter.com/yukihiro10101/status/1643187914942320641  
@TheEdition88
 さんのA4アートプリントが到着！ポスターやグッズも持ってるけどいつも印刷が綺麗でマーメイド紙も高品質😊　冴子のB2ポスターがあれば買っちゃう笑  
@theedition88  
先生的A4艺术打印到达!海报和周边也有，不过印刷总是很漂亮，美人鱼纸也有高品质😊冴子的B2海报的话会买的笑  
![](./img/Fs3GsDgaQAEUYw5.jpg) 


https://twitter.com/Nakayomi/status/1642962738375954433
Ne cherchez plus le casting parfait pour l'adaptation live de #CatsEyes, je l'ai déjà rencontré ! Et avec le costume s'il vous plaît ! Et Frédéric Gorny (qui jouait le conservateur du musée) en Quentin Chapuis et hop (ou en commissaire Bruno, ça pourrait le faire aussi)  
不要再为#CatsEyes的现场改编寻找完美的演员了，我已经见过他了!请穿上戏服!还有frederic Gorny(他是博物馆的策展人)、Quentin Chapuis和hop(或者策展人Bruno也可以)  

https://twitter.com/Nakayomi/status/1170378749839314945  
Bon, ben c'était bien sympa ce petit escape game sur le thème de #CatsEye au Louvre ^^ Rencontre avec Quentin Chapuis et sa fameuse coupe légendaire sortie du lit 😂😂 (petite private joke de notre groupe qui s'est un peu moqué de sa perruque)  
很好，这是一个有趣的小逃脱游戏，主题是#猫眼在卢浮宫^^遇见Quentin Chapuis和他著名的传奇剪发😂😂(我们小组的小私人笑话，有点嘲笑他的假发)  
![](./img/ED4FQ8DWkAAcJyv.jpg) 
![](./img/ED4FQiYWkAAO3Yo.jpg) 
![](./img/ED4FP_xXYAEM3y3.jpg)   
Les comédiens/animateurs étaient à fond dedans ! Petite photo avec les Cat's et on est reparti avec un sac avec quelques goodies (magnet, cahier, livret avec extraits de mangas). Cadre impressionnant même si compliqué d'en profiter pleinement avec la mission. Groupe très agréable  
Et puis même si avec  mon retard de train on a pas pu vraiment discuter (pas autant qu'on aurait pu), j'ai été très content de rencontrer XXX  
演员/动画师都在里面!和猫拍了一张小照片，我们带着一个袋子离开了，里面有一些好东西(磁铁，笔记本，有漫画摘录的小册子)。令人印象深刻的设置，即使复杂的充分享受与任务。非常愉快的团队  
即使我的火车晚点了，我们并没有真正有机会交谈(没有尽我们所能)，我还是很高兴见到XXX  


# « Cat’s eyes » : TF1 va prochainement adapter le dessin animé culte en série  
《Cat’s eyes》:TF1将很快将经典动画片改编成电视剧  

source: 
https://www.ouest-france.fr/medias/tf1/cats-eyes-tf1-va-prochainement-adapter-le-dessin-anime-culte-en-serie-26637bcc-d21c-11ed-a4d7-24d4124528e0  

Le manga « Cat’s eyes » diffusé dans les années 1980 devrait bientôt revenir sur les écrans, mais cette fois-ci en prise de vue réelle. Vendredi 31 mars 2023, la chaîne TF1 a annoncé la nouvelle pour un tournage prévu cet automne.  
20世纪80年代播出的漫画《Cat’s eyes》很快就会回归荧幕，但这次是实景拍摄。2023年3月31日星期五，TF1频道宣布了计划在今年秋天拍摄的消息。  
La série animée « Cat’s Eyes ».    
动画片《Cat’s eyes》  
La série animée « Cat’s Eyes ». | CAPTURE D’ÉCRAN TF1  
动画片《Cat’s eyes》。TF1 |截图。  
Ouest-France avec agence Publié le 03/04/2023 à 15h08  
法国西部与agence于2023年4月3日15:08发布  
Diffusée pour la première fois en 1986, la série Cat’s eyes pourrait faire son retour sur TF1. Le réalisateur Alexandre Laurent, connu notamment pour les séries Le Bazar de la Charité et Les Combattantes, devrait piloter le retour du manga, mais en live action (prise de vue réelle), rapporte Puremédias .  
1986年首次播出的《Cat’s eyes》可能会在TF1上回归。据puremedias报道，以《Le Bazar de la Charité》和《Les Combattantes》系列而闻名的导演Alexandre Laurent应该会推动这部漫画的回归，但要实拍。  
La chaîne TF1, dans un communiqué publié vendredi 31 mars 2023, a expliqué que la série serait produite par Big Band Story et inspirée de l’œuvre originale de Tsukasa Hôjô pour un tournage prévu à l’automne. Elle a également indiqué que « des discussions » sont « engagées avec plusieurs plateformes SVOD pour son co-financement ».  
TF1在2023年3月31日周五发布的一份声明中解释说，该系列将由Big Band Story制作，灵感来自北条司的原作，拍摄计划在秋季进行。她还表示，“正在与几个SVOD平台进行讨论”，以共同资助该项目。  
Une diffusion au plus tôt pour l’automne 2024  
最早将于2024年秋季播出  
La série reviendra sur les origines de trois sœurs Chamades qui devront faire preuve d’audace et d’imagination pour cambrioler les plus beaux monuments de Paris, dont la Tour Eiffel. Elles espéreront ainsi dérober « une œuvre ayant appartenu à leur père disparu 10 ans plus tôt dans le mystérieux incendie de sa galerie d’art » afin de « comprendre enfin ce qui lui est arrivé ».  
该系列将回到三个Chamades姐妹的起源，她们必须展示胆量和想象力，闯入巴黎最美丽的纪念碑，包括埃菲尔铁塔。通过这种方式，她们希望偷到“一件属于他们父亲的作品，他在10年前的画廊神秘火灾中失踪了”，以便“最终了解他的遭遇”。  
La série sera développée en 8 épisodes de 52 minutes chacun. La diffusion, peut-être en prime time sur plusieurs soirées, est prévue, au plus tôt, à l’automne 2024.  
该系列将分为8集，每集52分钟。该节目最早将于2024年秋季播出，可能在几个晚上的黄金时段播出。  

https://twitter.com/Ajtbe11/status/1642112749395230723   
Cat's Eye fan cast : Bon je sais que ce n'est pas certain que TF1 et la production prendront des actrices peu connues ou assez jeunes mais j'ai déjà deux actrices pour Sylia et Tam.  
Ophélie Bau pour Sylia et Marylin Lima pour Tam.  
Cat's Eye粉丝阵容:嗯，我知道TF1和制作是否会有不太知名或相当年轻的女演员，但我已经为Sylia和Tam准备了两位女演员了。
Ophelie Bau饰演Sylia（译注：泪）, Marylin Lima饰演Tam（译注：瞳）。  
![](./img/Fsn01FfXgAA4qLa.jpg) 
![](./img/Fsn0p_3WAAAreKB.jpg) 


--------------------------------------------------  
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

