source:  
http://www.saikyo-jump.com/archives/60360771.html 


【画像】北条司の「キャッツ・アイ」、フランスで実写化！！ひろゆき氏が地元情報を伝える！！  
2023/04/06 少年ジャンプ  
北条司的猫眼电影，法国真人化! !弘之先生传达本地信息! !  

1: 2023/04/02(日) 17:46:49.97  

実業家・ひろゆき氏が１日、自身のツイッターを更新し、北条司氏の人気コミック「キャッツ・アイ」の実写リメイク企画がフランスで進行していることを伝えた。  
1日，实业家·弘之更新了自己的推特，传达了北条司的人气漫画《猫眼》正在法国进行真人翻拍的计划。  
  
ひろゆき氏は地元紙「ル・パリジャン」の「ＴＦ１がリメイク版『Ｓｉｇｎｅｄ　Ｃａｔ’ｓ　Ｅｙｅｓ』のポスターを公開した。」という記事を引用。「木更津じゃないほうの『キャッツアイ』がフランスで実写テレビドラマになる様子。２０１８年に『シティーハンター』もフランスで実写映画化されてるけど、フランス人は北条司先生が好きなんかね。」とコメントした。  
弘之在当地报纸《巴黎人报》的“TF1公开了重制版《Signed Cat’s Eyes》的海报。”的报道。“不是木更津的《猫眼》在法国拍成真人电视剧的样子。2018年《城市猎人》也在法国拍成真人电影，法国人喜欢北条司老师吗?”的评论。  

（続きはソースをご覧下さい）  
（接下来请看原报道）  
https://www.daily.co.jp/gossip/2023/04/02/0016205330.shtml

78: 2023/04/02(日) 18:54:31.14
>>1
良作の予感しかせんな  
我只有一种良作的预感  

123: 2023/04/02(日) 19:40:10.70
>>1
ひろゆきも出演しちゃえよ  
Hiroyuki也来出演吧  

6: 2023/04/02(日) 17:50:40.69
びっくりどっきりメカとか不要だから実写化し易いっちゃし易いわな  
吓了一跳因为不需要机械所以很容易拍成真人版  
 
7: 2023/04/02(日) 17:51:03.99
パリジェンヌがやるなら観たいわ  
如果巴黎女郎演的话我想看  

2: 2023/04/02(日) 17:48:05.81
スーパーモデル体型の人がやるのかね  
是超级模特身材的人演的吗?  

25: 2023/04/02(日) 18:10:13.28
シティーハンターがそうだったけどやっぱ西洋人スタイル良くて  
北条司の画風とバッチリ合うんだよね  
比如《城市猎人》，但它还是像西方人一样  
和北条司的画风很搭呢  


4: 2023/04/02(日) 17:49:22.13
日本版は藤原紀香だっけ  
日本版是藤原纪香吧  

11: 2023/04/02(日) 17:52:01.93
>>4
泪：藤原紀香
瞳：稲森いずみ
愛：内田有紀


102: 2023/04/02(日) 19:17:31.53
>>11
良いキャスティングじゃん 見てないけど  
这是个好角色，我没看过  

136: 2023/04/02(日) 20:07:10.39
>>11
へー豪華なメンツ  
ビジュアルとスタイルは悪くないね  
ちょっと前に実写化なら愛役で剛力がゴリ押しされそう  
瞳が武井咲で泪が忽那とか？  
なんか一気にグレード下がるな…  
很豪华的样子  
视觉效果和风格都不错  
如果是之前真人化的话，我相信XXX会被推举为小爱的角色  
瞳是武井咲 泪是忽那?  
好像一下子就降低了等级…  

12: 2023/04/02(日) 17:52:51.16
これか  
是这个吗  
![](./img/130be88f-s.jpg)  


30: 2023/04/02(日) 18:19:18.93
>>12
タケモトピアノか？
竹本钢琴吗?  

41: 2023/04/02(日) 18:25:42.54
>>30
ふざけんな、もうそれにしか見えんだろうがw
开什么玩笑，看来就只有这个了  

138: 2023/04/02(日) 20:14:20.62
>>12
キャットウーマンのコスプレみたい
我想演Cat Woman Cosplay

23: 2023/04/02(日) 18:09:15.69
立花理佐 早見優 MIE  
![](./img/9b9b0249.jpg)  


29: 2023/04/02(日) 18:17:03.24
>>23
このバージョンは知らんかった  
这个版本我不知道  

31: 2023/04/02(日) 18:19:56.65
>>23
結構イメージ的にはよいね  
非常形象  

149: 2023/04/02(日) 21:21:41.37
>>23
なお、内海警部は西城秀樹  
内海警部是西城秀树  

192: 2023/04/03(月) 01:12:00.16
>>149
舘ひろしじゃないのかよw
不是馆广吗?  

55: 2023/04/02(日) 18:42:15.88
とうとうこれの実写化かよ  
终于要拍真人版了  
![](./img/f3507d4d-s.jpg)  


186: 2023/04/02(日) 23:14:20.52
>>55
衣装再現度は藤原紀香や早見優のバージョンより上だな  
服装再现度比藤原纪香和早见优的版本还要高  

84: 2023/04/02(日) 18:58:47.45
いま日本で実写化するとなるとキャストはどうなるんや
愛ちゃんは浜辺美波辺りかのう・・・
泪姐さんは生前の竹内結子がええと思うが、今の女優さんではちょっと思いつかんな
现在在日本真人化的话，演员会怎么样呢?
小爱可以去海边美波附近…
泪姐觉得生前的竹内结子比较好，现在的女演员就有点想不出来了


87: 2023/04/02(日) 19:00:38.57
>>84
シン・キャッツアイ？  
New Cat'sEye?  

93: 2023/04/02(日) 19:03:47.75
>>87
俊夫が柄本とか池松とかはあかん
竹内涼真あたりにしてもらわんと
俊夫不能是柄本或池松  
一定要像竹内凉真那样  


100: 2023/04/02(日) 19:16:16.17
昔のソフィー・マルソーやエマニュエル・ベアール  
ジャン・レノが出るなら観てみたい  
以前的苏菲·玛索，伊曼纽尔·贝尔  
如果让·雷诺出场，我想看  


98: 2023/04/02(日) 19:13:05.04
藤原紀香の唯一の代表作が奪われてしまうー  
藤原纪香唯一的代表作被夺走了  

58: 2023/04/02(日) 18:43:23.80
ジャッキーがなかまになりたそうにこちらをみている！  
成龙好像成了伙伴似的看着这边!  

199: 2023/04/03(月) 04:22:13.98
杏里がアップを始めました  
杏里开始了特写   

5: 2023/04/02(日) 17:50:18.46
ルパンとのコラボでアニメ映画になったのは見た。
なんか攻殻機動隊とキャッツアイのコラボかと途中から錯覚した…キャッツの声優の2人は当時と同じ事にもびっくりした。  
我看过他和鲁邦合作的动画电影。
中途我还误以为是攻壳机动队和猫眼的合作呢…的两位声优也被当时一样的事情吓了一跳。


53: 2023/04/02(日) 18:41:44.66
レオタードは死守してくれ  
请死守紧身衣  

35: 2023/04/02(日) 18:20:51.55
あのレオタードと  
現実的な泥棒描写に説得力もたすのムズすぎ  
和那个紧身衣  
现实的小偷描写也太有说服力了  

8: 2023/04/02(日) 17:51:18.77
実際、レオタードって動き回るのに最適なのかな  
事实上，紧身衣是最适合活动的吗？  

45: 2023/04/02(日) 18:32:27.08
>>8
元々、エアロビクスをする時の衣装でした。  
原来是做有氧运动时的服装。  

64: 2023/04/02(日) 18:48:32.49
キャッツアイにストーリーらしいストーリーってあったっけ  
猫眼里有类似故事的故事来着  

85: 2023/04/02(日) 18:59:02.31
>>64
三姉妹の父親で芸術家だったハインツの非合法に奪われた作品を取り戻す&行方不明になったハインツの手がかりを探すってのが基本ストーリー  
找回三姐妹的父亲、艺术家海因茨被非法剥夺的作品&寻找失踪的海因茨的线索是基本故事。  

94: 2023/04/02(日) 19:04:32.50
最後記憶喪失するのは覚えてるけどそこまで再現したらすごいな  
我记得最后会丧失记忆，但如果能重现到这种程度，那就太了不起了  

132: 2023/04/02(日) 20:01:45.54
キャッツアイに化けた瞳が俊夫を誘惑するようになってからが面白い
变成Cat'sEye的瞳开始诱惑俊夫，很有趣。

140: 2023/04/02(日) 20:21:32.98
トランプ投げてキャッツアイごっこやったなあ  
扔扑克牌玩猫眼游戏  

43: 2023/04/02(日) 18:27:39.30
フランスはアニメの実写が好きなイメージ  
なんなの国民性？  
法国喜欢动画真人的形象。  
这是什么国民性？  

91: 2023/04/02(日) 19:02:50.74
世界観は好きそうだな
你好像很喜欢世界观。

95: 2023/04/02(日) 19:04:46.95
劇画チックな絵が好きなのかもしれない
也许喜欢连环画式的画

118: 2023/04/02(日) 19:37:41.87
美術品窃盗はフランスが本場だもんな  
設定上日本よりリアリティがあるわ  
白人三姉妹でいいから、ぜひやってもらいたい  
法国可是盗窃艺术品的发源地啊  
在设定上比日本更真实  
白人三姐妹也行，一定要试试  

36: 2023/04/02(日) 18:23:01.04
ルパン三世の実写映画あたりもフランスでウケそうだな   
鲁邦三世的真人电影也会在法国上映  

39: 2023/04/02(日) 18:25:29.12
>>36
小栗旬のアレか・・・  
小栗旬的那个吗?  

28: 2023/04/02(日) 18:11:59.57
これがハリウッドとかなら姉妹だっつってんのにムキムキの白人黒人アジアン並べてキャッツアイですってやりそうで怖いが  
完璧なシティハンター制作したフランスなら期待できるな  
如果是好莱坞，恐怕他们会说是姐妹，但他们只会摆出一堆肌肉发达的白人、黑人和亚洲人，然后说她们是猫眼。
但如果是法国，他们拍了完美的城市猎人，我对他们抱有有很大的希望。


155: 2023/04/02(日) 21:39:09.14
>>28
この間観たシャザムが正にそんな感じだったな  
ヘレン・ミレン、ルーシー・リュー、レイチェル・ゼグラーが三姉妹設定  
まあ、神様の娘だから何でもありなんだろうけど  
前几天看到的夏瑟姆就是这样的感觉  
海伦·米伦、露西·利厄、雷切尔·泽格勒为三姐妹设定  
不过，因为是神的女儿所以什么都有吧  

103: 2023/04/02(日) 19:18:40.65
アメリカだとチャーリーズエンジェルが有るからな  
香港でシティハンター制作したよな  
在美国，有《霹雳娇娃》  
在香港制作了《城市猎人》   

16: 2023/04/02(日) 17:57:46.16
シティハンターのフランス版実写映画がよかったから
これも期待してしまう
NICKY-LARSON-affiche-2-1
因为《城市猎人》的法国真人电影很不错  
这也令人期待  


22: 2023/04/02(日) 18:08:31.65
シティーハンターは結構いい感じだったんだよな  
《城市猎人》给人的感觉很不错   

24: 2023/04/02(日) 18:09:22.18
>>22
日本上映版権のエンディングはGet Wildが流れた  
日本上映版权的结尾播放了Get Wild  

72: 2023/04/02(日) 18:51:45.48
シティハンターの監督なら期待できる
《城市猎人》的导演是值得期待的  

40: 2023/04/02(日) 18:25:39.50
シティーハンターは好きすぎて監督主演までつとめたおっちゃんがいてくれたけど、
今回は、だれが作るねん？
因为我太喜欢《城市猎人》了，有位大叔还担任了导演和主演。  
这次是谁做的?    

151: 2023/04/02(日) 21:28:50.63
シティハンターはめちゃくちゃ良かったから期待感あるな  
《城市猎人》很棒，我很期待  
引用元：https://hayabusa9.5ch.net/test/read.cgi/mnewsplus/1680425209/

https://www.daily.co.jp/gossip/2023/04/02/0016205330.shtml  
「キャッツ・アイ」フランスで実写リメイク企画進行　ひろゆき氏が地元情報伝える  
猫眼电影在法国进行真人翻拍企划弘之传达当地信息  
　北条司氏  
　ル・パリジャンの公式ツイッター＠ｌｅ＿Ｐａｒｉｓｉｅｎより  
来自巴黎人的官方推特@ le_parisien  

2枚

　実業家・ひろゆき氏が１日、自身のツイッターを更新し、北条司氏の人気コミック「キャッツ・アイ」の実写リメイク企画がフランスで進行していることを伝えた。  
1日，实业家·弘之更新了自己的推特，传达了北条司的人气漫画《猫眼》正在法国进行真人翻拍的计划。  

　ひろゆき氏は地元紙「ル・パリジャン」の「ＴＦ１がリメイク版『Ｓｉｇｎｅｄ　Ｃａｔ’ｓ　Ｅｙｅｓ』のポスターを公開した。」という記事を引用。「木更津じゃないほうの『キャッツアイ』がフランスで実写テレビドラマになる様子。２０１８年に『シティーハンター』もフランスで実写映画化されてるけど、フランス人は北条司先生が好きなんかね。」とコメントした。  
弘之在当地报纸《巴黎人报》的“TF1公开了重制版《Signed Cat’s Eyes》的海报。”的报道。“不是木更津的《猫眼》在法国拍成真人电视剧的样子。2018年《城市猎人》也在法国拍成真人电影，法国人喜欢北条司老师吗?”的评论。  

　「ＴＦ１」はフランスのテレビ局。「ル・パリジャン」はアレクサンドル・ロラン監督の名前も挙げて、企画が進行していることを伝えた。公開されたポスターは主人公の３姉妹とみられる人物が屋根の上でエッフェル塔を見つめるデザインとなっている。  
“TF1”是法国的电视台。《巴黎人》还提到了亚历山大•罗兰导演的名字，并表示正在进行企划。在公开的海报中，主人公的三姐妹站在屋顶上凝视着埃菲尔铁塔。  

　北条氏の作品はフランスでも人気が高く、過去にはアニメ版が放送されていた。「シティーハンター」はフィリップ・ラショーの主演兼監督で２０１８年に制作され、日本では「シティーハンター　ＴＨＥ　ＭＯＶＩＥ　史上最香のミッション」のタイトルで１９年に公開された。原作愛に満ちた、再現度の高い作風は北条氏自身や日本のファンにも好評だった。  
北条的作品在法国也有很高的人气，过去曾播放过动画版。《城市猎人》由菲利普·拉肖主演兼导演，于2018年制作，19年在日本上映，片名为《城市猎人THE MOVIE 史上最香的任务》。充满了对原作的爱，高再现度的作品风格也受到了北条本人和日本粉丝的好评。  

![](./img/16205331.jpg)  
