
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


说明：  
  
- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
- 人物右脸对应图片左侧，人物左脸对应图片右侧。  
- 如果图片或格式有问题，可以访问[这个链接](./readme.md)  
- 温馨提示：**本文图片里的点较多，可能会产生密集恐惧**。  
- 后来用mark点作了更详细的对比，详见[FC的画风-面部2(脸颊)](./readme2.md)。  


# FC的画风-面部  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96864))  

## 面部素描参考图  
![](./img3/DrawingTheHeadHands.AndrewLoomis.female.jpg) 
![](./img3/DrawingTheHeadHands.AndrewLoomis.male.jpg)  
![](./img3/DrawingTheHeadHands.AndrewLoomis.teenage.girl.jpg) 
![](./img3/DrawingTheHeadHands.AndrewLoomis.teenage.boy.jpg)  


准确地说，本文说的面部只关注(从正面看)下颌骨外轮廓的形状。  

## 方法  

- 首先，筛选正面图片。对于某个角色，若该角色的鼻子位于左右外眼角的中线，则认为该分格里该角色是正面。例如，01_042_0，11_043_7, 03_094_2。  
- 然后，分组。对于以上筛选出的正面图片，把脸颊相似的图片分为一组。例如，01_042_0、02_001被分类到紫苑Group0； 03_094_2、 04_015_5被分类到紫苑Group2。  


## 部分角色的面部

### 紫苑(Shion)-正面  
搜集了FC里紫苑的正面图片。整理了其中一部分后，发现可以分为几类：  

- Group0:  
    - 图片：01_042_0，02_001，01_047_0，04_064_6。  
    - 特点：相对其他组图片，这组图片脸稍短(或稍宽)、显得角色年龄小。 
    - 这些图片叠加后的效果：  
    ![](img/face_front_shion_group0-young.jpg)  

- Group1:  
    - 图片：01_150_7, 02_079_1, 03_003_0, 03_145_4, 03_195_0, 07_029_4.  
    - 特点：虽然是正面图，但左脸比右脸稍宽（角色稍微向右转头的效果）。
    - 这些图片叠加后的效果：  
    ![](img/face_front_shion_group1-left.jpg)  

- Group2(front):  
    - 图片：03_094_2, 04_015_5, 06_065_0, 07_097_0, 07_188_0, 08_096_2, 08_165_4, 08_176_6, 09_083_2.  
    - 特点：正面图。
    - 这些图片叠加后的效果：  
    ![](img/face_front_shion_group2-front.jpg)  

- Group3:  
    - 图片：11_043_7, 11_133_2, 14_122_0, 14_280_6, 14_282_1.  
    - 特点：虽然是正面图，但左脸比右脸稍宽一点点（介于Group1和Group2之间）。
    - 这些图片叠加后的效果：  
    ![](img/face_front_shion_group3-slight-left.jpg)  

- 对比：Group0 --> Group1 --> Group2 --> Group3   
![](img/face_front_shion_group0-young.jpg "Group0") -- 
![](img/face_front_shion_group0_to_group1.gif) --> 
![](img/face_front_shion_group1-left.jpg "Group1") -- 
![](img/face_front_shion_group1_to_group2.gif) --> 
![](img/face_front_shion_group2-front.jpg "Group2") -- 
![](img/face_front_shion_group2_to_group3.gif) --> 
![](img/face_front_shion_group3-slight-left.jpg "Group3")  

- 对比：Group1 --> Group3  
![](img/face_front_shion_group1-left.jpg "Group1") -- 
![](img/face_front_shion_group1_to_group3.gif) --> 
![](img/face_front_shion_group3-slight-left.jpg "Group3")  


### 盐谷(Shionoya)-正面  
紫苑男装时自称"塩谷"（'塩'通'盐'）。搜集了FC里其正面图片。整理了其中一部分后，发现可以分为几类：  

- psudo-front：  
    - 图片：13_167_1，13_174_0，14_010_5。  
    - 特点：虽然是正面图，但左脸比右脸稍宽一点点。（与紫苑Group3符合）  
    - 这些图片叠加后的效果：  
    ![](img/face_front_shya_psudo-front.jpg)  

- right-top：  
    - 图片：10_125_0，12_150_4，12_151_4，12_158_6，14_067_4。  
    - 特点：虽然是正面图，但右脸比左脸稍宽（角色稍微向左转头的效果）、略微低头、左嘴角上扬。 
    - 这些图片叠加后的效果：  
    ![](img/face_front_shya_right-top.jpg)  

- 紫苑Group3 --> 盐谷psudo-front：（紫苑女装变男装，左脸一致，后者右脸下颌骨末端凸）  
![](img/face_front_shion_group3-slight-left.jpg) -- 
![](img/face_front_shion_group3_to_shya_psudo-front.gif) --> 
![](img/face_front_shya_psudo-front.jpg)  


### 雅彦(Masahiko)-正面  
搜集了FC里雅彦的正面图片。整理了其中一部分后，发现可以分为几类：  

- Group0:  
    - 图片：01_023_4，01_081_5，02_063_4。  
    - 特点：相对其他组图片，这组图片脸稍短、显得角色年龄小。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_masahiko_group0_young.jpg)  

- Group1:  
    - 图片：06_123_4，13_129_5。  
    - 特点：相对其他组图片，这组图片脸稍宽。两侧腮帮子稍凸(婴儿肥?)。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_masahiko_group1_wide_left.jpg)  

- Group2:  
    - 图片：03_131_4，08_109_3，11_129_3，13_116_3，13_116_4，13_118_6，13_139_1，13_157_0，14_149_5，14_283_4。  
    - 特点：正面图。两侧腮帮子稍凸(婴儿肥?)。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_masahiko_group2_front.jpg)  

- 对比：Group0 --> Group2(Group0变化至Group2，脸部变长)  
    ![](img/face_front_masahiko_group0_young.jpg) -- 
    ![](img/face_front_masahiko_group0_to_group2.gif) --> 
    ![](img/face_front_masahiko_group2_front.jpg)  

- 对比：Group1 --> Group2(Group1变化至Group2，脸部变窄)  
    ![](img/face_front_masahiko_group1_wide_left.jpg) -- 
    ![](img/face_front_masahiko_group1_to_group2.gif) --> 
    ![](img/face_front_masahiko_group2_front.jpg)  


### 雅美(Masami)-正面  
搜集了FC里雅美的正面图片。整理了其中一部分后，发现可以分为几类：  

- Group0:  
    - 图片：03_027_1，03_027_3(镜中)，03_060_1。  
    - 特点：相对其他组图片，这组图片脸稍上扬、脸稍短。雅彦第一次异装。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_masami_group0_vol03.jpg)  

- Group1:  
    - 图片：03_114_0，07_048_0。  
    - 特点：正面。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_masami_group1_vol03_vol07.jpg)  

- Group0 --> Group1:(脸型相似。Group0的鼻子稍上移(上扬))  
    ![](img/face_front_masami_group0_vol03.jpg) -- 
    ![](img/face_front_masami_group0_to_group1.gif) --> 
    ![](img/face_front_masami_group1_vol03_vol07.jpg)  

- 雅彦Group0 --> 雅美Group0：(后者脸颊宽、下巴短)  
    ![](img/face_front_masahiko_group0_young.jpg) -- 
    ![](img/face_front_masahiko_group0_to_masami_group0.gif) --> 
    ![](img/face_front_masami_group0_vol03.jpg) 

- 雅彦Group2 --> 雅美Group1：(这说明后续雅彦的脸型偏瘦长、后续雅美的脸型偏短宽)  
    ![](img/face_front_masahiko_group2_front.jpg) --
    ![](img/face_front_masahiko_group2_to_masami_group1.gif) -->
    ![](img/face_front_masami_group1_vol03_vol07.jpg)  


### 若苗空(Sora)-正面  
搜集了FC里其正面图片。整理了其中一部分后，发现可以分为几类：  

- front:  
    - 图片：01_00a_0，02_000a，04_015_0，04_039_0，05_059_0，06_135_2。  
    - 特点：正面。 
    - 这些图片叠加后的效果：  
    ![](img/face_front_sora_front.jpg)  

- down:  
    - 图片：01_152_0(脸部稍宽)，08_063_3，12_108_5。  
    - 特点：正面，鼻子稍微上移(仰头)。 
    - 这些图片叠加后的效果：  
    ![](img/face_front_sora_down.jpg)  

- left:  
    - 图片：02_191_3，06_111_0。  
    - 特点：正面，鼻子稍微向右脸偏移(向右转头)。 
    - 这些图片叠加后的效果：  
    ![](img/face_front_sora_left.jpg)  

- 空front --> 紫苑Group2（比较两人的相貌）：  
    ![](img/face_front_sora_front.jpg) -- 
    ![](img/face_front_sora_front_to_shion_front.gif) --> 
    ![](img/face_front_shion_group2-front.jpg) 

- 空front --> 紫苑Group3（比较两人的相貌）：  
    ![](img/face_front_sora_front.jpg) -- 
    ![](img/face_front_sora_front_to_shion_front-slight-left.gif) --> 
    ![](img/face_front_shion_group3-slight-left.jpg) 


### 若苗紫(Yukari)-正面  
搜集了FC里其（包括雅彦妈妈）正面图片。整理了其中一部分后，发现可以分为几类：  

- front:  
    - 图片：01_005_0，01_025_4，01_043_0，01_200_4，02_004_4，02_146_4，03_087_0，04_000a_0，04_180_1，05_042_1，05_092_2，05_183_6，06_092_0，06_092_6，06_135_2，07_059_3，08_017_5，09_099_2，09_155_5，09_155_6，10_020_4，10_035_3，10_055_4，10_057_0。
    - 特点：正面。 
    - 这些图片叠加后的效果（因彩图不易比较，故不包含彩图）：  
    ![](img/face_front_yukari_front.jpg)  

- open-mouth:  
    - 图片：05_050_6, 06_013_5, 09_091_3。
    - 特点：正面, 张嘴，下巴下移。 
    - 这些图片叠加后的效果：  
    ![](img/face_front_yukari_open-mouth.jpg)  
    - 该图变化到front（闭嘴的效果）：  
    ![](img/face_front_yukari_open-mouth.jpg) -- 
    ![](img/face_front_yukari_open-mouth_to_front.gif) --> 
    ![](img/face_front_yukari_front.jpg)  

- down:  
    - 图片：01_124_1，02_000a，02_189_0，04_022_2，04_062_4，04_170_5，05_064_2，05_174_0。
    - 特点：正面, 鼻子略微上移(略微抬头)。  
    - 这些图片叠加后的效果（因彩图不易比较，故不包含彩图）：  
    ![](img/face_front_yukari_down.jpg)  
    - 该图变化到front（略微低头的效果）：  
    ![](img/face_front_yukari_down.jpg) -- 
    ![](img/face_front_yukari_down_to_front.gif) --> 
    ![](img/face_front_yukari_front.jpg)  

- wide:  
    - 图片：01_006_1，01_012_3，02_018_6，08_066_3。  
    - 特点：正面, 脸略宽。  
    - 这些图片叠加后的效果（因彩图不易比较，故不包含彩图）：  
    ![](img/face_front_yukari_wide.jpg) 
    - 该图变化到front：  
    ![](img/face_front_yukari_wide.jpg) -- 
    ![](img/face_front_yukari_wide_to_front.gif) --> 
    ![](img/face_front_yukari_front.jpg)  

- Mama:  
    - 图片：01_025_4，02_027_2。  
    - 特点：雅彦的妈妈。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_yukari_mama.jpg) 
    - 该图变化到front（雅彦妈妈和若苗紫的相貌比较，几乎没有差别）：  
    ![](img/face_front_yukari_mama.jpg) -- 
    ![](img/face_front_yukari_mama_to_front.gif) --> 
    ![](img/face_front_yukari_front.jpg)  

- 紫front --> 紫苑Group2（比较两人的相貌）：  
    ![](img/face_front_yukari_front.jpg) -- 
    ![](img/face_front_yukari_front_to_shion_front.gif) --> 
    ![](img/face_front_shion_group2-front.jpg) 

- 紫front --> 紫苑Group3（比较两人的相貌）：  
    ![](img/face_front_yukari_front.jpg) -- 
    ![](img/face_front_yukari_front_to_shion_front-slight-left.gif) --> 
    ![](img/face_front_shion_group3-slight-left.jpg) 

- 紫front --> 雅彦Group0（比较两人的相貌）：  
    ![](img/face_front_yukari_front.jpg) -- 
    ![](img/face_front_yukari_front_to_masahiko_group0.gif) --> 
    ![](img/face_front_masahiko_group0_young.jpg) 

- 紫front --> 雅彦Group2（比较两人的相貌）：  
    ![](img/face_front_yukari_front.jpg) -- 
    ![](img/face_front_yukari_front_to_masahiko_group2.gif) --> 
    ![](img/face_front_masahiko_group2_front.jpg) 

- 紫front --> 空front（比较两人的相貌）：  
    ![](img/face_front_yukari_front.jpg) -- 
    ![](img/face_front_yukari_front_to_sora_front.gif) --> 
    ![](img/face_front_sora_front.jpg) 

- 紫front和雅美group1的脸型有些相似，前者脸颊处线条的(与水平线的)倾角小于后者。  



### 浅冈叶子(Yoko)-正面  
分为几类：  

- front(正面。叶子的腮帮子稍凸(婴儿肥?))：
    - 图片：13_178_5，13_172_0，13_154_2，13_142_1，08_160_3，08_153_3，08_152_6，06_007_3_，05_160_2，05_142_1，05_097_5，04_178_0，04_136_3，03_041_2  
    - 特点：正面。叶子的腮帮子稍凸(婴儿肥?)。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_yoko_front.jpg)  

- short:  
    - 图片：13_108_2，06_060_3，04_161_0。  
    - 特点：相比front组，本组脸稍短。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_yoko_front_short.jpg)  

- long:  
    - 图片：11_056_1，11_009_5，04_139_5，03_072_3，02_092_5，02_065_0。  
    - 特点：相比front组，本组脸稍长(嘴微张)、左腮帮子稍凸。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_yoko_front_long_left-cheek-bump.jpg)  

- open-mouth(嘴张大):  
    - 图片：13_125_3，13_116_3，03_056_0，03_008_0，02_088_2。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_yoko_front_open-mouth.jpg)  

- open-mouth2(嘴张得更大):  
    - 图片：13_152_0，13_101_3，02_072_6。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_yoko_front_open-mouth2.jpg)  

- 雅彦Group2 --> 叶子front:  
    ![](img/face_front_masahiko_group2_front.jpg)  -- 
    ![](img/face_front_masahiko_group2_front_to_yoko_front.gif) --> 
    ![](img/face_front_yoko_front.jpg)  

- 紫苑Group2 --> 叶子front：  
    ![](img/face_front_shion_group2-front.jpg)  -- 
    ![](img/face_front_shion_group2-front_to_yoko_front.gif) --> 
    ![](img/face_front_yoko_front.jpg)  

- 雅美Group0 --> 叶子front(两者脸型很相似，后者下半脸(尤其是左脸)微瘦、下巴稍尖)：  
    ![](img/face_front_masami_group0_vol03.jpg)  -- 
    ![](img/face_front_masami_group0_vol03_to_yoko_front.gif) --> 
    ![](img/face_front_yoko_front.jpg)  

- 盐谷psudo-front --> 叶子front：  
    ![](img/face_front_shya_psudo-front.jpg)  -- 
    ![](img/face_front_shya_psudo-front_to_yoko_front.gif) --> 
    ![](img/face_front_yoko_front.jpg)  

- 若苗空front --> 叶子front：  
    ![](img/face_front_sora_front.jpg)  -- 
    ![](img/face_front_sora_front_to_yoko_front.gif) --> 
    ![](img/face_front_yoko_front.jpg)  

- 若苗紫front --> 叶子front：  
    ![](img/face_front_yukari_front.jpg)  -- 
    ![](img/face_front_yukari_front_to_yoko_front.gif) --> 
    ![](img/face_front_yoko_front.jpg)  


### 浩美(Hiromi)-正面  
- front  
    - 图片：09_042_4，05_075_1，03_008_3，01_191_5，01_187_4。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_hiromi_front.jpg)  

- right  
    - 图片：09_047_4，07_106_1，05_130_2。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_hiromi_right.jpg)  

- 浩美right和叶子front的脸型很类似，前者腮帮子没有叶子凸:  
    ![](img/face_front_yoko_front.jpg)  -- 
    ![](img/face_front_yoko_front_vs_hiromi_right.gif) --> 
    ![](img/face_front_hiromi_right.jpg)  


### 熏(Kaoru)-正面  
- front
    - 图片：08_013_4，07_193_5。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_kaoru_front.jpg)  

- 熏front和浩美right的脸型类似，前者脸更有棱角、右下颌骨末端偏下:  
    ![](img/face_front_hiromi_right.jpg)  -- 
    ![](img/face_front_hiromi_front_vs_kaoru_front.gif) --> 
    ![](img/face_front_kaoru_front.jpg)  


### 江岛(Ejima)-正面  
- front
    - 图片：10_123_1，04_139_5，04_096_6，04_103_2，03_004_4。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_ejima_front.jpg)  

- wide(脸庞稍宽)
    - 图片：14_025_1，05_012_0。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_ejima_wide.jpg)  

- 江岛wide和熏front的脸型有些类似，前者下颌骨末端稍高。  

- 与以上人物相比，江岛的下颌稍窄（下颌骨末端稍高）。  


### 辰巳(Tatsumi)-正面  
- front：  
    - 图片：09_134_6，09_110_1，04_139_4，03_125_5。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_tatsumi_front.jpg)  

- left（脸向右转，左脸稍微偏大，右脸稍微偏小）：  
    - 图片：13_074_4，09_093_7，06_026_5。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_tatsumi_left.jpg)  

- left_wide（相比left组而言，脸偏宽）：  
    - 图片：06_041_0，04_135_6。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_tatsumi_left_wide.jpg)  

- long（脸偏长(下巴下移?)）：  
    - 图片：09_135_0, 09_095_3。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_tatsumi_long.jpg)  

- 辰巳front和空front的脸型几乎一致:  
    ![](img/face_front_sora_front_vs_tatsumi_front.gif)  
        

### 导演-正面   
- front：  
    - 图片：03_061_2, 03_099_1。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_director_front.jpg)  

- 导演戴眼镜，所以无法定位眼睛的位置。只能大概估计外眼角水平位置约为眼镜腿的位置。  
- 导演胖，五官比例不符合标准人像比例，但符合胖人的人像比例。以下显示了胖人的五官比例[1]：  
    ![](img3/DrawingTheHeadHands.AndrewLoomis.page014.jpg)  
- 导演front和雅彦group2的下巴尖重合，前者脸胖于后者。  


### 早纪(Saki)-正面  
- front:  
    - 图片：10_042_5, 09_176_2, 09_175_7, 09_175_1, 09_174_4, 09_152_3。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_saki_front.jpg)  

- top_short(稍微低头、下巴稍短):  
    - 图片：11_113_2，10_043_1。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_saki_top_short.jpg)  

- 早纪front类似紫苑group3，前者下颌骨线条更直。左脸很类似，紫苑group3的右脸占比例小(右转头)，而早纪front的右脸是正面：  
    ![](img/face_front_saki_front.jpg)  -- 
    ![](img/face_front_shion_group3_vs_saki_front.gif) --> 
    ![](img/face_front_shion_group3-slight-left.jpg)  

- 早纪front和熏front的脸形很类似:  
    ![](img/face_front_saki_front.jpg)  -- 
    ![](img/face_front_kaoru_front_vs_saki_front.gif) --> 
    ![](img/face_front_kaoru_front.jpg)   
          
- 早纪front类似雅彦group2，没有雅彦group2的腮帮子凸。  

- 早纪front v.s. 紫front。早纪的脸窄于紫的脸。  
- 早纪front类似雅美group0和group1。早纪下巴比雅美下巴长一些(或许是因为雅美闭嘴，早纪稍张嘴)，早纪眼眶处的脸颊比雅美窄。  

- 相比浩美right，早纪front的脸更有棱角。  （腮帮子棱角：早纪 > 熏 > 浩美）  
- 早纪top_short的左脸类似盐谷right-top的左脸。  


### 真琴(Makoto)-正面  
几乎都是长大嘴，多数是笑（[疑问]这是否能反映她的性格？）。  

- front(正面):  
    - 图片：03_021_2, 04_176_1, 06_005_6, 08_110_0, 07_075_5, 07_072_0。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_makoto_front.jpg)  

- down(稍抬头):  
    - 图片：01_063_0, 07_051_1, 07_112_5。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_makoto_down.jpg)    

- 真琴front的脸型和雅美group1的脸型相似：  
    ![](img/face_front_masami_group1_vs_makoto_front.gif)    


### 摄像师-正面  
该角色的正面几乎都是张大嘴。

- open(正面、张嘴):   
    - 图片：14_171_4, 12_157_5, 12_148_0, 10_067_3, 03_070_4。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_cameraman_open.jpg)  

- down(稍微抬头):   
    - 图片：14_194_6，03_098_4。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_cameraman_down.jpg)  

- open_down_left(张嘴、稍微抬头、稍微右转):   
    - 图片：14_171_1，10_063_1。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_cameraman_open_down_left.jpg)  

- top_left(稍微低头、稍微右转):   
    - 图片：10_127_1，07_104_1。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_cameraman_top_left.jpg)  

- 摄像师和辰巳left的脸型有一点类似，前者棱角更明显。  
    

### 浅葱(Asagi)-正面  
- front(正面):   
    - 图片：14_250_1，14_197_0，14_196_1，14_193_1，14_185_2，14_169_5，14_090_1，14_057_0，14_039_0。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_assagi_front.jpg)  

- 浅葱front和紫front的脸型几乎一致:   
    ![](img/face_front_yukari_front_vs_assagi_front.gif)  


### 和子(Kazuko)-正面  
和子的面部比例大多不符合标准：    
![](img/01_052_2__mod.jpg) 
![](img/08_092_3__mod.jpg)  
和子是大块头，所以头应该大；所以让其脸宽符合参考图，使得头偏大。  

- front_open(正面、稍张嘴)：  
    - 图片：08_120_3，08_091_5。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_kazuko_front_open.jpg)  

- top(稍低头)：  
    - 图片：08_109_3，07_075_0，05_061_3。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_kazuko_top.jpg)  

- down(稍抬头)：  
    - 图片：08_105_0，08_092_3，04_189_1，03_021_2，01_069_0，01_066_0，01_052_2。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_kazuko_down.jpg)  

- down2(抬头)：  
    - 图片：07_051_1，05_163_0。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_kazuko_down2.jpg)  

- 和子(和人)的脸型和以上人物都不相似。  


### 爷爷-正面  
待完成  


### 横田进(Susumu)-正面  
- 03_021_2，横田进这个镜头不符合标准比例：  
    ![](img/03_021_2__mod.jpg)  

- 03_012_5(第一次出场时的正面特写)：  
    ![](img/face_front_susumu_03_012_5.jpg)  

- open(正面，微张嘴)：  
    - 图片：03_094_1，03_012_5。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_susumu_open.jpg)  

- 横田进03_012_5和雅彦group2的脸形很类似，前者更有棱角、且腮帮子不凸。  
- 横田进open和空front的脸形相似，前者脸颊窄于后者。   


### 宪司(Kenji)-正面  
宪司是大块头，所以头应该大；所以让其脸宽符合参考图，使得头偏大。  

- front:  
    - 图片：04_039_5, 04_035_1, 04_014_1。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_kenji_front.jpg)   

- top(稍低头):  
    - 图片：04_051_4, 04_014_3, 03_187_2。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_kenji_top.jpg)   

- 宪司front与和子(和人)front_open的脸型有些类似。宪司的下巴比和人的下巴宽大。  


### 顺子(Yoriko)-正面  
- front:  
    - 图片：10_173_3，04_039_0，04_016_3，06_187_0，03_172_0。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_yoriko_front.jpg)   

- 顺子front和雅美group1的脸型很类似：   
    ![](img/face_front_masami_group1_vs_yoriko_front.gif)  
          
- 顺子front和熏front的右脸类似。  
- 顺子front和早纪front脸型类似，顺子的脸比早纪大一圈。  


### 森(Mori)-正面  
- front:  
    - 图片：10_102_3，10_102_1。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_mori_front.jpg)   

- 头发遮住了右侧脸颊，所以不易和其他人作比较。左脸腮帮子略微凸。
- 森front和雅彦group2的左脸类似。  
- 森front和空front的脸很类似，前者左下颌骨末端稍窄于后者。  



### 叶子母-正面  
叶子的母亲。  

- 13_103_3(第一次出场的正面)：  
    ![](img/face_front_ykma_13_103_3.jpg)   

- open(正面，稍张嘴):  
    - 图片：13_180_4，13_161_3。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_ykma_open.jpg)   

- 叶子母13_103_3和叶子front脸型类似，前者腮帮子没有后者凸。  
- 叶子母13_103_3和浩美right的脸型很类似：    
    ![](img/face_front_hiromi_right_vs_ykma_13_103_3.gif)  
    
- 叶子母13_103_3和熏front的脸型很类似：    
    ![](img/face_front_yoko_front_vs_ykma_13_103_3.gif)  

- 叶子母13_103_3和早纪top_short的脸型有些类似，前者下颌骨末端偏高。  


### 理沙(Risa)-正面  
- 12_025_0：  
    ![](img/face_front_risa_12_025_0.jpg)  

- 理沙12_025_0和紫苑group0(young)的脸型有些类似。前者脸大2圈。  


### 美菜(Mika)-正面  
- front(正面):  
    - 图片：12_008_6，11_167_4。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_mika_front.jpg)   

- open(正面，稍张嘴):  
    - 图片：12_008_6。  
    ![](img/face_front_mika_open.jpg)   

- 美菜front和雅美group1的脸型类似，前者(上半部分)脸宽。  
- 美菜front和紫front的脸型类似，前者(上半部分)脸略宽。  


### 齐藤茜(Akane)-正面  
- front(正面):  
    - 图片：06_145_7，06_144_5。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_akane_front.jpg)   

- wide(和front组几乎一致，但鬓角处稍宽):  
    - 图片：07_099_3，06_157_1。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_akane_wide.jpg)   

- 齐藤茜front和美菜front脸型类似，前者鬓角处窄。  


### 仁科(Nishina)-正面  
- 14_014_5(只显示了右脸):  
    ![](img/face_front_nishina_14_014_5.jpg)   
- 仁科14_014_5右腮帮子(下颌骨末端？)比大多数人(雅彦、紫苑、叶子、浩美、熏、江岛、早纪、横田进、)窄。  
- 仁科14_014_5和浩美front右脸类似，仁科的腮帮子(下颌骨末端？)窄。  
- 仁科14_014_5和熏front右脸类似，仁科的腮帮子(下颌骨末端？)窄。  
- 仁科14_014_5和横田进03_012_5右脸类似，仁科的腮帮子(下颌骨末端？)窄。  


### 葵(Aoi)-正面  
- front(正面):  
    - 图片：06_073_4，06_042_1。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_aoi_front.jpg)   

- 葵front和空front的脸形很类似，前者脸颊的线条没有后者直。   
- 葵front和辰巳front的脸形类似，前者右下巴比后者窄。  
- 葵front和熏front的右脸类似，前者脸颊稍宽。  
- 葵front和江岛wide的右脸类似，前者左脸比后者宽。  
- 葵front和真琴front的上半脸类似，前者下半脸比后者长。  
- 葵front和横田进13_012_5的下半脸类似，前者上半脸比后者宽。  

- 该角色的造型类似CH里獠的异装：  
    ![](../fc_dress/img/06_039_0__.jpg) 
    ![](../ch_make-up/img/ch_34_093__mod.jpg)  

 
### 奶奶-正面  
待完成  


### 文哉(Fumiya)-正面  
- front(正面):  
    - 图片：12_012_3，12_010_3。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_fumiya_front.jpg)   

- fumiya front和雅彦group2的脸型类似，前者腮帮子偏上、且没有后者凸。    
- fumiya front和叶子front的脸型类似，前者比后者脸长。  
- fumiya front和浩美right的脸型类似。  
- fumiya front和江岛wide的下半脸型很类似，前者上半脸比后者窄。  
- fumiya front和辰巳left_wide的脸型类似，前者下巴比后者尖、左鳃没有后者凸。  
- fumiya front和横田进03_012_5的脸型很类似：  
    ![](img/face_front_susumu_03_012_5_vs_fumiya_front.gif)   
        
- fumiya front和葵front的下半脸类似，前者上半脸比后者窄。  


### 齐藤玲子(Reiko)-正面  
- 01_166_0(左脸、正面):  
    ![](img/face_front_reiko_01_166_0.jpg)   

- reiko 01_166_0和紫苑group0的脸型类似，前者下颌骨末端比后者略凸：  
    ![](img/face_front_shion_group0_vs_reiko_01_166_0.gif)   

- reiko 01_166_0和茜wide的脸型类似，前者比后者小两圈。  


### 松下敏史(Toshifumi)-正面  
没有完全的正面镜头。09_077_5稍低头，09_076_5稍右转。  

- 09_077_5:  
    ![](img/face_front_matsu_09_077_5.jpg)   

- 松下敏史09_077_5和雅彦group2的脸型类似，前者没有后者腮帮子凸。  

- 松下敏史09_077_5和空front左脸类似，前者右脸比后者小一圈。  
- 松下敏史09_077_5和叶子front脸类似，前者下巴比后者稍长。  
- 松下敏史09_077_5和浩美front脸类似。和浩美right脸类似，前者右脸比后者小一圈。  
- 松下敏史09_077_5和辰巳front左脸类似，前者右脸比比后者小一圈。  
- 松下敏史09_077_5和横田进03_012_5脸形几乎一致：  
    ![](img/face_front_susumu_03_012_5_vs_matu_09_077_5.gif)      


### 章子(Shoko)-正面  
待完成


### 阿透(Tooru)-正面  
没有符合参考图的镜头。  

- open(正面、张嘴):  
    - 图片：12_046_1(张大嘴)，12_044_0(张大嘴)，12_038_3(张大嘴)，12_015_1(张嘴)。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_tooru_open.jpg)   

- toor open和空front的下巴类似，前者脸颊窄于后者。  
- toor open和辰巳long的脸型类似，但前者下部尖于后者。  
- toor open和仁科14_014_5的脸型类似，但前者腮帮子凸于后者。  



### 京子(Kyoko)-正面  
- 02_177_2(正面):    
    ![](img/face_front_kyoko_02_177_2.jpg)   

- top(稍低头):  
    - 图片：02_186_2，02_186_0。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_kyoko_top.jpg)   

- 京子02_177_2和齐藤茜front的脸型很类似：  
    ![](img/face_front_akane_front_vs_kyoko_02_177_2.gif)  
        

### 一马(Kazuma)-正面  
待完成  


### 一树(Kazuki)-正面  
- 12_012_3(正面):    
    ![](img/face_front_kazuki_12_012_3.jpg)   

- 一树12_012_3和雅彦group2的脸型很类似：  
    ![](img/face_front_masahiko_group2_vs_kazuki_12_012_3.gif)  

- 一树12_012_3和雅彦group2叠加，可以看看雅彦戴眼镜的样子：  
    ![](img/face_front_kazuki_masahiko_glasses.jpg)   


### 千夏(Chinatsu)-正面  
- 12_141_0:  
    ![](img/face_front_chinatsu_12_141_0.jpg)   

- wide(脸颊宽):  
    - 图片：14_016_2，12_159_3。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_chinatsu_wide.jpg)  

- chinatsu 12_141_0和紫苑group3的脸型很类似：  
    ![](img/face_front_shion_group3_vs_chinatsu_12_141_0.gif)    
       
- chinatsu 12_141_0和早纪front/早纪top_short的的脸型类似。   
- chinatsu 12_141_0和横田进03_012_5的脸型有些类似，前者鬓角处稍宽、下巴尖处稍窄、左下颌骨末端圆滑。  
- chinatsu 12_141_0和顺子front的脸型有些类似，前者比后者小一圈。  
- chinatsu 12_141_0和fumiya front的脸型有些类似，前者下巴尖于后者。  
- chinatsu wide和葵front的右脸类似。 前者左脸窄于后者。   
- chinatsu wide和江岛wide左脸类似。前者右脸宽于后者。  


### 绫子(Aya)-正面  
- front(正面):  
    - 图片：02_189_4, 02_180_5。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_aya_front.jpg)   

- aya front和导演front的脸型相似（胖），前者腮帮子稍瘦于后者。  


### 典子(Tenko)-正面  
- front(正面):  
    - 图片：02_188_4，02_180_6，02_178_7。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_tenko_front.jpg)   

- tenko front和辰巳left_wide的右脸相似，前者的棱角小于后者：   
    ![](img/face_front_tenko_front.jpg)  --
    ![](img/face_front_tatsumi_left-wide_vs_tenko_front.gif) --> 
    ![](img/face_front_tatsumi_left_wide.jpg)  

- tenko front和横田进03_012_5类似，前者脸小一圈、下巴圆于后者。  


### 麻衣(Mai)-正面  
- front(正面):  
    - 图片：12_159_3，12_141_0。  
    - 这些图片叠加后的效果：  
    ![](img/face_front_mai_front.jpg)  

- mai front和紫苑group3的脸型几乎一致：  
    ![](img/face_front_shion_group3_vs_mai_front.gif)   
      
- mai front和熏front的右脸几乎一致，前者左腮帮子稍大于后者、鬓角处稍窄于后者。  
- mai front和chinatsu 12_141_0的脸型很类似。  


### 古屋公弘(Kimihiro)-正面  
- 09_051_6(稍低头):  
    ![](img/face_front_kimihiro_09_051_6.jpg)  

- kimihiro 09_051_6和熏front脸型很类似，前者右下颌骨末端稍靠上、左下颌骨末端稍凸：  
    ![](img/face_front_kaoru_front_vs_kimihiro_09_051_6.gif)  

- kimihiro 09_051_6和早纪front脸型很类似，前者下颌骨末端稍靠上：  
    ![](img/face_front_kimihiro_09_051_6_vs_saki_front.gif)    

- kimihiro 09_051_6和雅彦group2脸型有一点类似，前者下半脸瘦、脸颊稍宽。  
- kimihiro 09_051_6和雅美group1上半脸类似，前者下半脸长于后者。  
- kimihiro 09_051_6和浩美right脸型类似，前者更显棱角。  
- kimihiro 09_051_6和江岛wide脸型类似，  
- kimihiro 09_051_6和顺子front脸型类似。  
- kimihiro 09_051_6和chinatsu 12_141_0脸型类似，前者右脸颊稍宽。  


### 古屋朋美(Tomomi)-正面  
- 09_047_1(正面):  
    ![](img/face_front_tomomi_09_047_1.jpg)  

- tomomi 09_047_1和雅彦group2脸型(和眼睛)有点类似，前者脸稍胖。  
- tomomi 09_047_1和紫苑group2脸型(和眼睛)有点类似，前者左鬓角处稍宽。  
- tomomi 09_047_1和雅美group0脸型(和眼睛)有点类似:  
    ![](img/face_front_masami_group1_vs_tomomi_09_047_1.gif)  
       
- tomomi 09_047_1和叶子front脸型有点类似，  
- tomomi 09_047_1和真琴front脸型(和眼睛)有点类似  
- tomomi 09_047_1和顺子front脸型有点类似  
- tomomi 09_047_1和kimihiro 09_051_6脸型类似，前者右脸颊大于后者。  


### 八不(Hanzu)-正面  
- 02_183_7(正面):  
    ![](img/face_front_hanzu_02_183_7.jpg)  

- 八不02_183_7和辰巳left脸型有点类似，前者右脸下颌骨末端偏上：  
    ![](img/face_front_hanzu_02_183_7.jpg)  -- 
    ![](img/face_front_hanzu_02_183_7_vs_tatsumi_left.gif) --> 
    ![](img/face_front_tatsumi_left.jpg)  


### 哲(Te)-正面  
没有正面图。   


## 总结：  
把上述人物按脸形类似分为以下几类(每一行是一类，每一类里的人物脸形很类似)：  

- 紫苑(Shion)group0(年幼) ～ 齐藤玲子(Reiko) 01_166_0  
- 紫苑Group3 ～ 盐谷(Shionoya)psudo-front ～ 早纪(Saki)front ～ 千夏(Chinatsu) 12_141_0 ～ 麻衣(Mai)front  
- 雅彦(Masahiko)group2 ～ 一树(Kazuki)12_012_3  
- 雅美(Masami)Group1 ～ 真琴(Makoto)front ～ 顺子(Yoriko)front ～ 朋美(Tomomi) 09_047_1  
- 叶子(Yoko)front ～ 浩美(Hiromi)right ～ 熏(Kaoru)front ～ 叶子母13_103_3 ～ 古屋公弘(Kimihiro) 09_051_6  
- 空(Sora)front ～ 辰巳(Tatsumi)front  
- 紫(Yukari)front ～ 浅葱(Assagi)front  
- 横田进(Susumu)03_012_5 ～ 文哉(Fumiya)front ～ 松下敏史(Toshifumi)09_077_5  
- 齐藤茜(Akane)front ～ 京子(Kyoko)02_177_2  
但有一个问题，例如，真琴front和顺子front/雅美Group1是同一类，但真琴front明显异于顺子front/雅美Group1(前者下巴短于后者)。这个问题的原因可能是：同一类里角色脸形虽然相似，但还是有微小差异；这些微小差异积累后，两个角色间的差异就变得明显。  


## 其他  
- 对于FC里很多肖像，当嘴张开不大时，下巴的位置和闭嘴时一样。但空的这三张图，张嘴由小到大，下巴相应下移：  
    ![](img/08_050_2.jpg) 
    ![](img/02_011_1.jpg) 
    ![](img/01_134_0.jpg) 

    三张图的渐变：  
    ![](img/sora_open-mouth.gif)  

- 14_191_1, 叶子接到漫画获奖的电话，误以为是妈妈的电话。这里，叶子虽然是闭嘴，但面部比例显示下巴下移。  
    ![](img/14_191_1__open-mouth.jpg)  
    08_141_0, 10_191_4, 也是如此：  
    ![](img/08_141_0__hiromi.jpg)  
    ![](img/10_191_4__hiromi.jpg)  
    类似的还有08_141_0里的横田进、14_028_6里的仁科。  




## 参考资料**：  

[1]. Drawing The Head & Hands, Andrew Loomis.  


--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



