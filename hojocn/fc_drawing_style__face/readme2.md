温馨提示：**本文图片里人脸上有很多点，可能会令人不适**。  


# FC的画风-面部2(脸颊)（[原链接](./readme2.md), [hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96865)）  

## 提要    
- 借助美容整形外科领域所使用的[人脸的mark点](#介绍面部mark点)，[对比、分析FC主要角色的正面脸颊](#FC主要角色的正面脸颊)。从对比结果来看，这些mark点有效地显示出FC主要角色面部的差异。  
- 归纳出这些角色面部特征的一些特点或规律，汇总在[“总结”](#总结)一节。  
- 基于这些总结，有[一些猜测](#一些猜测)。其中包括：对于正面美貌的理解，作者北条司和美容整形外科领域可能有很多共识。  


## 动机    
在[^0]里比较了FC里某些角色的面部。其中比较角色面部特征时，经常出现类似下列的描述："前者左下颌骨末端稍窄于后者", "腮帮子略微凸", "(脸部线条)更有棱角", "前者脸颊窄于后者"。本文尝试借助美容整形外科(Aesthetic Plastic Surgery，以下简称APS)领域的一些方法和术语来描述这些差异，试图让这些描述更精确。  


## 结构，词义辨析  

jaw: n. 下颌、下巴[^cd]  
mandible： n. 下颚骨[^cd]  
nasal: adj. 鼻的[^cd]  
nuchal: adj. 项的，颈背的[^cd]  
occipital: adj. 枕部的[^cd]  
orbital: adj. 眼眶的、轨道的[^cd]  
protuberance: n. 隆起部分;凸出物[^cd]  
superciliary: adj. of, relating to, or adjoining the eyebrow : supraorbital[^mw]. 眉的(?)  
supraorbital: adj. 眶上的[^cd]  
temporal: adj. 太阳穴的;颞骨的[^cd]  
temple： 额角[^imaios]  
zygomatic: adj. 颧骨的，颧弓的[^cd]  

External occipital protuberance： 枕外隆凸[^imaios]  
Jawline (base of the mandible)： 下颌线(下颌骨底部)[^yd] [^qq] [^dl] [^bi]   
Mental protuberance： 颏隆凸[^imaios]  
Nasal bone： 鼻骨[^imaios]  
Orbital margin： 眶缘[^imaios]  
Superciliary arch (brow ridge): 眉弓[^imaios]  
Superior nuchal line： 上项线[^imaios]  
Temporal line: 颞线[^imaios]  
Zygomatic arch: 颧弓[^imaios]  
Zygomatic bone： 颧骨[^imaios]  

注:(?)表示该词条的翻译为本文作者的猜测、没有明确的出处。以下同。  

[^cd]: [Cambridge词典](https://dictionary.cambridge.org/zhs/词典/英语-汉语-简体/)  
[^li]: [Linguee词典](https://cn.linguee.com/中文-英语/)  
[^qq]: [腾讯翻译](https://fanyi.qq.com/)  
[^yd]: [有道翻译](https://fanyi.youdao.com/index.html)  
[^dl]: [DeepL翻译](https://www.deepl.com/)  
[^bi]: [Being翻译](https://cn.bing.com/translator)  
[^mw]: [Merriam-Webster词典](https://www.merriam-webster.com/dictionary/)  
[^tf]: [thefreedictionary](https://www.thefreedictionary.com/)  
[^imaios]: [英文IMAIOS搜索](https://www.imaios.com/en/imaios-search/(search_text)/)或[中文IMAIOS搜索](https://www.imaios.com/cn/imaios-search/(search_text)/)  
[^scid]: [SCIdict](http://www.scidict.org/items/lip%20vermilion.html)  


## 如何描述面部(正面)  
### 资料1[^1]中的信息  
#### 介绍面部mark点  {#介绍面部mark点}  
参考资料[^1]里在人物面部正面和侧面定义了一些mark点，借助点、线之间的关系(距离、角度)来研究"美女脸的特征"。虽然本文只整理FC的角色的正面，但这里罗列了[^1]里关于正面和侧面的mark点和术语，目的是方便查找和比较这些mark点。  

#### 面部mark点-正面  {#面部mark点-正面}  
![](img2/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig3.png)    
图1(FIGURE 3 in [^1])  

图1中的mark点的注解[^1]和中文翻译:  

- 1 and 2: pupil (p), （瞳孔）  
- 3: trichion (tr), （发际中点）  
- 4: glabella (g), （眉间点）  
- 5: sellion (se), （鼻梁点）  
- 6 and 7: alare (al), （鼻翼点）  
- 8: subnasale (sn), （鼻下点）  
- 9: labiale superius (ls), （上唇中点）  
- 10: stomion (sto), （口裂点）  
- 11: labiale inferius (li), （下唇中点）  
- 12 and 13: cheilion (ch), （口角点）  
- 14: menton (m), （颏下点/颔下点） (gnathion (gn)[^3])  
- 15 and 16: endocanthion (en), （眼内角点）  
- 17 and 18: palpebrale superius (ps),  （译注：上眼睑点？）
- 19 and 20: palpebrale inferius (pi),  （译注：下眼睑点？）
- 21 and 22: exocanthion (ex), （眼外角点？）   
- 23 and 24: highest point of eyebrow (Hb), （眉高点？）  
- 25 and 26: most medial point of eyebrow (Mb), （眉内侧点？）  
- 27 and 28: zygion (zy), （颧点）    
- 29 and 30: points of mandible angle (ang)（下颌骨角点）, that is, points of the outline of the mandible that meet with the horizontally extending line of the two cheilions（口角）.("口角点的水平连线"与脸颊的交点)  
- 31 and 32: lateral gonion (latgo)（下颌骨后下点） tangential points with the outline of the mandible; the tangential line should be parallel to the ipsilateral（同侧的） line of gn-­zy,  (详见[^3] Figure 1 )(注：该点描述脸颊(腮帮子)的凸起程度，详见[标记31/32点](#标记3132点)一节。)  
- 33 and 34: points of the outline of both cheeks (chk)（脸颊）, which meet the transverse extending line of the subnasale, ("鼻下点的水平线"与脸颊的交点)  
- 35 and 36: the points of the chin（下巴/颏） contour line at the anterior-­lateral chin, which meet the vertical extending line from each pupil. (al-­chin);  ("瞳孔铅垂线"与脸颊的交点)  
- Except for 27 and 28, the facial points designated on facial contour lines 23 to 36 were not usually defined as verified neoclassical canon; only I defined the landmarks. The arbitrary points help us to understand the actual differences in face shape between Caucasians and Asians  

#### 面部mark点-侧面  {#面部mark点-侧面}  
![](img2/DifferencesBetweenCaucasianAndAsianAttractiveFaces.Fig4.png)    
图2(FIGURE4 in [^1])   

图2中的mark点的注解[^1]和中文翻译:  

- Tragion (t) （耳屏点）: the most anterior portion of the supratragal notch),  
- glabella (g) （眉间点）: the most prominent or anterior point of the forehead between the eyebrows,  
- sellion (se) （鼻梁点）: the most concave point in the tissue overlying the area of the frontonasal suture,  
- pronasale (prn) （鼻尖）: the most prominent or anterior projection point of the nose,  
- columella breakpoint (c) （(鼻)小柱断点） the highest point of the columella or breakpoint of Daniel,  
- subnasale (sn) （鼻下点）: the junctional point of the columella and the upper cutaneous lip, alar curvature point or alar  
crest point (ac) （波峰点）: the most lateral point in the curved base line of each ala, indicating the facial insertion of the nasal wing base, 鼻翼根与面部连接处的曲线的最外侧点  
- labiale superius (ls) （上唇中点）: the mucocutaneous junction and vermilion border of the upper lip,  
- labiale inferius (li) （下唇中点）: the mucocutaneous junction and vermilion border of the lower lip,  
- cheilion (ch), （口角点）  
- pogonion (pg) （颏前点）: the most anterior point of the soft-­tissue chin,  
- distant chin (dc): the farthest point from the fiducial t. 下巴上距离t点最远的点。  
- menton (m): the most inferior portion of the anterior chin. Points m1, m2, m3, and dc were not usually defined in articles; only I defined the landmarks.  （颏下点/颔下点） (gnathion (gn)[^3])。  
    The arbitrary points help us to understand the actual differences in the profile faces of Caucasians and Asians.  
    - m1: the point where the mandibular（下颌骨） contour line meets the horizontal line that extends from the fiducial ls,  
    - m2: the intersecting line of the mandibular contour with the horizontally extended line from the fiducial li;  
    - m3: the intersecting line of the mandibular contour with the horizontally extended line from the fiducial pg;  


### 资料13[^13]中的信息  

![](img2/Form-of-the-Head-and-Neck_face-breadth0.jpg)  
![](img2/Form-of-the-Head-and-Neck_face-breadth1.jpg)  
"The widest part of the face is between the two cheekbones.  
The breadth of the face, measured across the most lateral projections of the **cheek bones** (**zygomatic arches**). In anatomy, the **zygomatic arch** (**Za**), is a part of the skull formed by the zygomatic process of the **temporal bone** (**Tb**) (a bone extending forward from the side of the skull, over the opening of the ear) and the temporal process of the **zygomatic bone** (**Zb**)."  
(脸最宽的部分在两颧骨之间。  
脸的宽度，通过**颧骨**(**颧骨弓**)最外侧的突出部分来测量。在解剖学上，**颧骨弓**(**Za**)是颅骨的一部分，由**颞骨**(**Tb**)(从颅骨侧面向前延伸，在耳口上方)和**颧骨** (**Zb**)的颞骨突起组成。)  

Bony landmarks of the head:  
![](img2/Form-of-the-Head-and-Neck_bony-landmarks.jpg)  
**Tl**  Temporal line  
**Sa**  Superciliary arch (brow ridge)  
**Nb**  Nasal bone  
**Za**  Zygomatic arch  
**Om**  Orbital margin  
**Eop** External occipital protuberance (位于头背面, 此处省略图示)  
**Sn**  Superior nuchal line (位于头背面, 此处省略图示)   
**Zb**  Zygomatic bone  
**Mp**  Mental protuberance  
**Jl**  Jawline (base of the mandible)    


## FC里角色正面的特点  
### 使用的方法和遇到的困难  
- 本文使用[^0]里的角色正面图片及其分类。其方法参见[^0]里"使用的方法"一节。    
- 有多种方法对比不同角色的面部差异：  
    - 方法1. 动态渐变。使用gif动画，把一个角色的面部图片逐渐过渡到另一个角色的面部图片。在[^0]里曾使用过这种方法;  
    - 方法2. 一个角色的面部图片做成红色半透明，另一个角色的面部图片做成蓝色半透明；两个图片叠在两个图层作对比。在[^5]里曾使用过这种方法;  
    - 方法3. 在某些面部特征的位置定义一些mark点。比较不同面部的mark点的差异。  
    - 方法4. 将某角色的左脸和另一个角色的右脸放在一张图里比较。  
- 本文主要使用上述方法3，并对极少数主要角色使用上述方法4。发现方法3优缺点如下：  
    - 优点：  
        - 能清晰、明显地展现出面部的差异;  
        - 因为定义了很多mark点，所以它们联合起来能反映出面部特征。  
    - 缺点：  
        - 由于需要标记mark点，所以，如果标记的位置不准确，则直接影响后续对比结果和结论。所以，欢迎指出文中的错误。  
        - 下巴颏处只有一个mark点(14), 未能体现出下巴颏尖/圆的程度。比如:  
            - 1)叶子front对比若苗空front，参见["浅冈叶子(Yoko)-正面"](#浅冈叶子(Yoko)-正面)一节；  
            - 2)早纪front对比若苗空front，参见["早纪(Saki)-正面"](#早纪(Saki)-正面)一节;  
            - 3)千夏对比横田进、千夏对比文哉，参见["千夏(Chinatsu)-正面"](#千夏(Chinatsu)-正面)一节。  

- 我认为脸颊的形状是指：从正面看，下颌骨外轮廓的形状和颧骨的形状。依照使用的mark点，正面脸颊的形状指"27/28, 29/30, 31/32, 33/34, 35/36, 14"这些点的位置。  
- FC里的角色面部很少画鼻翼，所以，本文图里鼻翼处的mark点(6/7)可忽略。  
- 遇到的困难：  
    - 标记mark点时，难免有误差。尤其是"31/32点（右脸latgo/左脸latgo）"。  
    - 对比角色A和角色B的面部特征时，要把A、B的头部放缩至同样大小，然后叠放在不同图层，最后对齐A和B的五官。难点在于：  
        - 1.角色A、B的头部放缩后，大小可能不一致；  
        - 2.对齐五官时，最先对齐哪里（即，以哪个位置为基准）？比如，首先对齐“左右眼的瞳孔”、或首先对齐“两侧脸颊最大宽度(颧点zy)”、或首先对齐“左右外眼角”，等等)。很常见的困难是，对齐某个五官后，发现另外几个五官对不齐，比如：  
            - 1)水平方向对齐鼻子后，浩美front组的脸(相对于雅彦group2/紫苑group2)向图片右侧偏(详见["浩美"](#浩美(Hiromi)-正面)一节);  
            - 2)对齐瞳孔后，发现两侧脸颊对不齐；  
            - 3)对齐两侧脸颊后，发现鼻子对不齐(左右脸不对称?)，等等;   

### 标记31/32点  {#标记3132点}  
31/32点（右脸latgo/左脸latgo）的定义是"latgo点处的切线与gn-­zy平行"。因为脸颊向外凸，所以该定义等同于"latgo点距离线段gn-­zy最远"，即用latgo点描述脸颊(腮帮子)的凸起程度。之所以用平行线的方式来定义latgo点，我猜，可能是因为用平行线的方法更容易找到latgo点。本文标记latgo点的方法如下(如下图动画所示)：  
    - 以右脸颊为例。初始时，取一直线(蓝色)经过gn点和zy点；  
    - 向脸颊外侧水平移动该直线，则该直线与脸颊的两个交点(绿色)之间的距离会越来越近；  
    - 当这两个绿色交点重合为一个点时，该点即为"(右脸颊)距离线段gn-­zy最远的点"，即为所求的latgo点。  
    ![](img2/face_front_cmp_aps_mark31-32-latgo.gif)  


## 说明  
- "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  
- 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
- 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  
- 人物右脸对应图片左侧，人物左脸对应图片右侧。  


## FC主要角色的正面脸颊  {#FC主要角色的正面脸颊}  

### 紫苑(Shion)-正面  {#紫苑(Shion)-正面}  
搜集了FC里紫苑的正面图片。整理了其中一部分后，发现可以分为以下几组。这些组和[^0]里的组一致，但依据重要性重新排列了顺序。这对于以下其他角色也是一样。  

- Group2:  
    - 图片：03_094_2, 04_015_5, 06_065_0, 07_097_0, 07_188_0, 08_096_2, 08_165_4, 08_176_6, 09_083_2.  
    - 特点：
        - 正面图。左右脸对称。  
        - **这些图片的编号显示出，它们集中于作品的前期、中期(在作品中期，紫苑的正面脸颊画法稳定在本组)**(FC共有14卷)。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**雅彦Group2**(红色mark点)的对比（紫苑嘴角窄(FC里女性的嘴角窄于男性); 31/32点比雅彦低，反映出紫苑腮帮子处的线条更圆滑(更不棱角分明)）；  
      （下图左三）左一叠加左二；  
      （下图左四）雅彦Group2；  
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2_vs_masahiko-group2.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2-p_vs_masahiko-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(蓝色mark点)、**若苗紫front**(红色mark点)的对比(**血缘关系**)（右脸颊一致，左脸颊和若苗空一致(若苗紫的左脸偏宽)；紫苑嘴小（紫的嘴是按男性比例）；紫苑31/32点相对靠下，说明其腮帮子处的线条相对更圆滑（即，更不棱角分明，其父母(成年人)腮帮子处的线条更有棱角））；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front；  
      （下图左五）若苗紫front；  
      （下图左六）若苗空front渐变对比；  
      （下图左七）若苗紫front渐变对比；  
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group2-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_sora_front_to_shion_front.gif) 
        ![](img/face_front_yukari_front_to_shion_front.gif) 

    - （下图左一）用方法4比较（左：紫苑group2；右：雅彦group2）；      
      （下图左二）用方法4比较（左：紫苑group2；右：雅美group0）；   
      （下图左三）用方法4比较（左：紫苑group2；右：雅美group1）；                
        ![](img2/face_front_cmp__shion-group2_vs_masahiko-group2_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shion-group2_vs_masami-group0_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shion-group2_vs_masami-group1_rl-cmp-p.jpg) 

- Group0:  
    - 图片：01_042_0，02_001，01_047_0，04_064_6。  
    - 特点：  
        - 相对其他组图片，这组图片下巴稍短(或脸颊稍宽)、显得角色年龄小。
        - **这些图片的编号显示出，它们集中于作品的前期(在作品前期，紫苑的正面脸颊画法稳定在本组)**。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_shion_group0-young.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**Group2**(红色mark点)的对比（本组颏下点/m/gn高，说明脸短，显得年龄小）；  
      （下图左三）左一叠加左二;  
      （下图左四）Group2;  
        ![](img/face_front_shion_group0-young.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0-p_vs_shion-group2.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(蓝色mark点)、**若苗紫front**(红色mark点)的对比(**血缘关系**)；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front；  
      （下图左五）若苗紫front；  
        ![](img/face_front_shion_group0-young.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group0-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

- Group3:  
    - 图片：11_043_7, 11_133_2, 14_122_0, 14_280_6, 14_282_1.  
    - 特点：
        - 虽然是正面图，但左脸稍宽（介于Group1和Group2之间）。下图对比左右脸mark点，可以反映出这一点。  
        - **这些图片的编号显示出，它们集中于作品的后期(这说明随年龄增长，该角色的相貌在变化；在作品后期，紫苑的正面脸颊画法稳定在本组)**。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。**不是严格正面**，所以本组对比说服力不大；  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻、嘴稍微偏离中线(偏图片左侧)，但人物的左脸稍宽；所以，似乎是鼻子偏离方向与脸颊偏离方向相反，这很奇怪[疑问])；   
        ![](img/face_front_shion_group3-slight-left.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3_rl-cmp.jpg) 
        ![](img/face_asymmetry_shion-group3_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(红色mark点)和**Group2**(红色mark点)的对比；  
      （下图左三）左一叠加左二;  
      （下图左四）Group2;  
        ![](img/face_front_shion_group3-slight-left.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3-p_vs_shion-group2.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(蓝色mark点)、**若苗紫front**(红色mark点)的对比(**血缘关系**)；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front；  
      （下图左五）若苗紫front；  
      （下图左六）若苗空front渐变对比；  
      （下图左七）若苗紫front渐变对比；  
        ![](img/face_front_shion_group3-slight-left.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shion-group3-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_sora_front_to_shion_front-slight-left.gif)
        ![](img/face_front_yukari_front_to_shion_front-slight-left.gif)

    - （下图左一）用方法4比较（左：紫苑group3；右：雅彦group2）；      
      （下图左二）用方法4比较（左：紫苑group3；右：雅美group0）；   
      （下图左三）用方法4比较（左：紫苑group3；右：雅美group1）；       
        ![](img2/face_front_cmp__shion-group3_vs_masahiko-group2_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shion-group3_vs_masami-group0_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shion-group3_vs_masami-group1_rl-cmp-p.jpg) 


### 雅彦(Masahiko)-正面 {#雅彦(Masahiko)-正面}  
搜集了FC里雅彦的正面图片。整理了其中一部分后，发现可以分为几组。这些组和[^0]里的组一致，但依据重要性重新排列了顺序。这对于以下其他角色也是一样。  

- Group2:  
    - 图片：03_131_4，08_109_3，11_129_3，13_116_3，13_116_4，13_118_6，13_139_1，13_157_0，14_149_5，14_283_4。  
    - 特点：
        - 正面图。两侧腮帮子稍凸(婴儿肥?)。  
        - 相对于该角色的其他组，本组图片在数量上占绝对优势，说明**雅彦的正面脸颊画法稳定**。   
        - **这些图片的编号显示出，它们集中于作品后期(在作品后期，该角色的正面脸颊画法稳定在本组)**。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**紫苑Group2**(红色mark点)的对比（**雅彦嘴更宽**(FC里女性的嘴角窄于男性)）；  
      （下图左三）左一叠加左二：  
      （下图左四）紫苑Group2;  
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2-p_vs_shion-group2.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**若苗紫front**(红色mark点)的对比（**血缘关系**）（两人嘴横向、竖向长度一致(这说明**若苗紫的嘴宽符合青少年男性比例**);两人右脸一致；若苗紫左脸宽；若苗紫眼睛更修长(成年人的特征)。两人脸颊有不少相似之处，符合血缘关系的设定。）；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗紫front；  
      （下图左五）若苗紫front渐变对比；  
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group2-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_yukari_front_to_masahiko_group2.gif) 

    - （下图左一）用方法4比较（左：紫苑group2；右：雅彦group2）；              
      （下图左二）用方法4比较（左：紫苑group3；右：雅彦group2）；      
      （下图左三）用方法4比较（左：盐谷psudo-front；右：雅彦group2）；      
      （下图左四）用方法4比较（左：盐谷right-top；右：雅彦group2）；      
        ![](img2/face_front_cmp__shion-group2_vs_masahiko-group2_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shion-group3_vs_masahiko-group2_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-psudo-front_vs_masahiko-group2_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-right-top_vs_masahiko-group2_rl-cmp-p.jpg) 

    - 导演front对比雅彦group2，参见[“导演front”](#导演-正面)一节。  
    - 早纪front对比雅彦group2，参见[“早纪front”](#早纪(Saki)-正面)一节。  
    - 横田进03_012_5和雅彦group2，参见[“横田进03_012_5”](#横田进(Susumu)-正面)一节。  
    - 仁科对比雅彦group2，参见[“仁科”](#仁科(Nishina)-正面)一节。  
    - 文哉front和雅彦group2，参见[“文哉”](#文哉(Fumiya)-正面)一节。  
    - 松下敏史09_077_5和雅彦group2，参见[“松下敏史”](#松下敏史(Toshifumi)-正面)一节。  
    - 古屋公弘09_051_6和雅彦group2，参见[“古屋公弘”](#古屋公弘(Kimihiro)-正面)一节。  

- Group0:  
    - 图片：01_023_4，01_081_5，02_063_4。  
    - 特点：  
        - 相对其他组图片，这组图片脸稍短、显得角色年龄小。  
        - **这些图片的编号显示出，它们集中于作品前期(在作品前期，该角色的正面脸颊画法稳定在本组)**  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。因耳朵被半遮挡，故以颧骨为准。鼻子应该位于中线，所以应该是严格正面。  
        ![](img/face_front_masahiko_group0_young.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0_rl-cmp.jpg) 
        ![](img/face_asymmetry_masahiko-group0_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**雅彦Group2**(红色mark点)的对比（本组颏下点/m/gn高，说明脸短(下巴短)，显得年龄小；嘴角宽度窄，符合年龄小的设定）；  
      （下图左三）左一叠加左二：  
      （下图左四）动态渐变对比；  
        ![](img/face_front_masahiko_group0_young.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0_vs_masahiko-group2.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0-p_vs_masahiko-group2.jpg) 
        ![](img/face_front_masahiko_group0_to_group2.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**若苗紫front**(红色mark点)的对比（**血缘关系**）；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗紫front；  
      （下图左五）若苗紫front渐变对比；  
        ![](img/face_front_masahiko_group0_young.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__masahiko-group0-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_yukari_front_to_masahiko_group0.gif) 

- Group1:  
    - 图片：06_123_4，13_129_5。  
    - 特点：相对其他组图片，这组图片脸稍宽。两侧腮帮子稍凸(婴儿肥?)。 
    - 本组图片叠加后的效果：  
        ![](img/face_front_masahiko_group1_wide_left.jpg)  


### 盐谷(Shionoya)-正面  {#盐谷(Shionoya)-正面}  
紫苑男装时自称"塩谷"（'塩'通'盐'）。搜集了FC里其正面图片。整理了其中一部分后，发现可以分为几组：  

- psudo-front：  
    - 图片：13_167_1，13_174_0，14_010_5。  
    - 特点：虽然是正面图，但左脸稍宽（与紫苑Group3吻合）。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)；  
      （下图左三）示意图。以耳朵为准。鼻嘴位于中线，所以是严格正面。  
        ![](img/face_front_shya_psudo-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_shya-psudo-front_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组脸颊、鼻子、嘴和紫苑一致，说明本组是女性的画法）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_shya_psudo-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(蓝色mark点)、**若苗紫front**(红色mark点)的对比(**血缘关系**)；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front；  
      （下图左五）若苗紫front；  
        ![](img/face_front_shya_psudo-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-psudo-front-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

    - （下图左一）用方法4比较（左：盐谷psudo-front；右：雅彦group2）；      
      （下图左二）用方法4比较（左：盐谷psudo-front；右：雅美group0）；   
      （下图左三）用方法4比较（左：盐谷psudo-front；右：雅美group1）；       
        ![](img2/face_front_cmp__shya-psudo-front_vs_masahiko-group2_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-psudo-front_vs_masami-group0_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-psudo-front_vs_masami-group1_rl-cmp-p.jpg) 

- right-top：  
    - 图片：10_125_0，12_150_4，12_151_4，12_158_6，14_067_4。  
    - 特点：虽然是正面图，但右脸比左脸稍宽（角色稍微向左转头的效果）、略微低头、左嘴角有笑容。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。以耳朵为准(虽然耳朵外轮廓模糊)。鼻子偏离中线，所以**不是严格正面**，所以本组对比说服力不大。  
        ![](img/face_front_shya_right-top.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top_rl-cmp.jpg) 
        ![](img/face_asymmetry_shion-group3_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(本组嘴角有笑容，所以嘴角宽度变大)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_shya_right-top.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(蓝色mark点)、**若苗紫front**(红色mark点)的对比(**血缘关系**)；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front；  
      （下图左五）若苗紫front；  
        ![](img/face_front_shya_right-top.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top_vs_yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__shya-right-top-p_vs_yukari-front_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

    - （下图左一）用方法4比较（左：盐谷right-top；右：雅彦group2）；      
      （下图左二）用方法4比较（左：盐谷right-top；右：雅美group0）；   
      （下图左三）用方法4比较（左：盐谷right-top；右：雅美group1）；     
        ![](img2/face_front_cmp__shya-right-top_vs_masahiko-group2_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-right-top_vs_masami-group0_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-right-top_vs_masami-group1_rl-cmp-p.jpg) 


### 雅美(Masami)-正面  {#雅美(Masami)-正面}  
雅彦女装时名为雅美。搜集了FC里雅美的正面图片。整理了其中一部分后，发现可以分为几类：  

- Group0:  
    - 图片：03_027_1，03_027_3(镜中)，03_060_1。  
    - 特点：  
        - 相对其他组图片，这组图片脸稍上扬、脸稍短。雅彦第一次异装。  
        - **这些图片的编号显示出，它们集中于作品前期(在作品前期，该角色的正面脸颊画法稳定在本组)**  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img2/face_front_cmp_aps__masami-group0_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴和雅彦一致，所以**雅美的嘴是按男性比例**。[疑问]脸颊处的黑、蓝、红点间隔出现，这能说明什么问题？）；  
      （下图左三）左一叠加左二；  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**若苗紫front**(红色mark点)的对比（**血缘关系**）；  
      （下图左三）左一叠加左二；  
      （下图左四）若苗紫front；  
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**group1**(红色mark点)的对比(嘴大小一致)；  
      （下图左三）左一叠加左二；  
      （下图左四）Group1；  
      （下图左五）渐变对比Group1(脸型相似，Group0的鼻子稍上移(上扬))；  
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img/face_front_masami_group0_to_group1.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**雅彦group0**(红色mark点)的对比(两组的共同点是相貌年龄小)；  
      （下图左三）左一叠加左二；  
      （下图左四）雅彦group0的渐变对比；  
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0_vs_masahiko-group0.jpg) 
        ![](img2/face_front_cmp_aps__masami-group0-p_vs_masahiko-group0.jpg) 
        ![](img/face_front_masahiko_group0_to_masami_group0.gif) 

    - （下图左一）用方法4比较（左：紫苑group2；右：雅美group0）；   
      （下图左二）用方法4比较（左：紫苑group3；右：雅美group0）；   
      （下图左三）用方法4比较（左：盐谷psudo-front；右：雅美group0）；   
      （下图左四）用方法4比较（左：盐谷right-top；右：雅美group0）；   
        ![](img2/face_front_cmp__shion-group2_vs_masami-group0_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shion-group3_vs_masami-group0_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-psudo-front_vs_masami-group0_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-right-top_vs_masami-group0_rl-cmp-p.jpg) 

- Group1:  
    - 图片：03_114_0，07_048_0。  
    - 特点：正面。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。**左右脸颊有几处差异**。  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子位于中线，所以是严格正面。  
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1_rl-cmp.jpg) 
        ![](img/face_asymmetry_masami-group1_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2的渐变对比(这说明后续雅彦的脸型偏瘦长、后续雅美的脸型偏短宽)；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_to_masami_group1.gif) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗紫front**(红色mark点)的对比（**血缘关系**）（**脸颊有一些类似**；本组脸颊稍窄；本组31/32点（右脸latgo/左脸latgo）相对靠下，说明本组腮帮子处的线条相对更圆滑(更不棱角分明)）；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗紫front；  
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__masami-group1-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 

    - （下图左一）用方法4比较（左：紫苑group2；右：雅美group1）；   
      （下图左二）用方法4比较（左：紫苑group3；右：雅美group1）；   
      （下图左三）用方法4比较（左：盐谷psudo-front；右：雅美group1）；   
      （下图左四）用方法4比较（左：盐谷right-top；右：雅美group1）；   
        ![](img2/face_front_cmp__shion-group2_vs_masami-group1_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shion-group3_vs_masami-group1_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-psudo-front_vs_masami-group1_rl-cmp-p.jpg) 
        ![](img2/face_front_cmp__shya-right-top_vs_masami-group1_rl-cmp-p.jpg) 


### 若苗空(Sora)-正面  {#若苗空(Sora)-正面}  
搜集了FC里其正面图片。整理了其中一部分后，发现可以分为几类：  

- front:  
    - 图片：01_00a_0，02_000a，04_015_0，04_039_0，05_059_0，06_135_2。  
    - 特点：正面。虽然左右脸不对称，但人中、嘴唇中点位于"两耳外侧间距的中线"上(如下图所示)，说明这是严格的正面。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。  
        ![](img/face_asymmetry_sora-front_with_markline.jpg) 
    - 左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)，脸颊处的mark点只显示了31/32点（右脸latgo/左脸latgo）有差异。所以，mark点没有显示出本组左右脸的差异。故改用曲线作对比。下图显示了(左二)本组脸颊曲线(红)；(左三)水平反转后的脸颊曲线(绿)；(左四)这两条曲线的对比(左右脸颊对比)，明显反映出了左右脸颊(腮帮子处)的差异:**左脸(图片右侧)腮帮子较凸出**。本组"左右脸颊差异大"的画法还出现在辰巳front、葵front，应该是作者的一种固定画法，详见本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节。  
        ![](img2/face_front_cmp_aps__sora-front_rl-cmp.jpg) 
        ![](./img/face_asymmetry_sora-front_original.jpg) 
        ![](./img/face_asymmetry_sora-front_flipped.jpg) 
        ![](./img/face_asymmetry_sora-front_compare.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）紫苑Group2渐变对比；  
        ![](img/face_front_sora_front.jpg) 
        ![](img2/face_front_cmp_aps__sora-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__sora-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img/face_front_sora_front_to_shion_front.gif) 

    - 紫苑对比若苗空front（血缘关系），参见[“紫苑”](#紫苑(Shion)-正面)节。  
    - 盐谷对比若苗空front（血缘关系），参见[“盐谷”](#盐谷(Shionoya)-正面)节。  
    - 若苗紫front对比若苗空front，参见[“若苗紫”](#若苗紫(Yukari)-正面)一节。  
    - 森front对比空front，参见[“森”](#森(Mori)-正面)一节。  
    - 葵front对比若苗空front，参见[“葵”](#葵(Aoi)-正面)一节。  
 
- down:  
    - 图片：01_152_0(脸部稍宽)，08_063_3，12_108_5。  
    - 特点：正面，鼻子稍微上移(仰头)。 
    - 本组图片叠加后的效果：  
        ![](img/face_front_sora_down.jpg)  

- left:  
    - 图片：02_191_3，06_111_0。  
    - 特点：正面，鼻子稍微向右脸偏移(向右转头)。 
    - 本组图片叠加后的效果：  
        ![](img/face_front_sora_left.jpg)  


### 若苗紫(Yukari)-正面  {#若苗紫(Yukari)-正面}  
搜集了FC里其（包括雅彦妈妈）正面图片。整理了其中一部分后，发现可以分为几类：  

- front:  
    - 图片：01_005_0(彩)，01_025_4，01_043_0，01_200_4(彩)，02_004_4，02_146_4，03_087_0，04_000a_0(彩)，04_180_1，05_042_1，05_092_2，05_183_6，06_092_0，06_092_6，06_135_2，07_059_3，08_017_5，09_099_2，09_155_5，09_155_6，10_020_4，10_035_3，10_055_4，10_057_0。
    - 特点：正面。相对于该角色的其他组，本组图片在数量上占绝对优势，说明**若苗紫的正面脸颊画法很稳定**。  
    - （下图左一）本组图片叠加后的效果（因彩图不易比较，故不包含彩图）；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。左右颧骨、脸颊有差异。  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子位于中线，所以是严格正面。（这与左二图(左右脸对比)逻辑矛盾，[疑问]如何解释该矛盾？一个可能的解释：因为面部转动幅度小，所以左右脸差异小；故以外眼角为准测不出差异。另一个解释：以鼻子为中线，该角色的左右外眼角对称，而左右颧骨不对称。）  
      （下图左四）示意图。以颧骨为准。鼻子位于中线，所以是严格正面。（这与左二图(左右脸对比)逻辑矛盾，[疑问]如何解释该矛盾？这是否是测量误差导致的？）  
        ![](img/face_front_yukari_front.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_yukari-front_with_markline_0.jpg) 
        ![](img/face_asymmetry_yukari-front_with_markline_1.jpg) 
    - （下图左一）本组图片叠加后的效果（因彩图不易比较，故不包含彩图）；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）雅彦Group2渐变对比；  
      （下图左六）紫苑Group2；  
      （下图左七）紫苑Group2渐变对比；  
        ![](img/face_front_yukari_front.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_yukari_front_to_masahiko_group2.gif) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img/face_front_yukari_front_to_shion_front.gif) 

    - （下图左一）本组图片叠加后的效果（因彩图不易比较，故不包含彩图）；  
      （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比(**脸颊几乎一致（夫妻相?）**。35/36点不一致，说明若苗空瞳距稍小；27/28点不一致，说明若苗紫脸颊宽)；   
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front；  
      （下图左五）若苗空front渐变对比；  
        ![](img/face_front_yukari_front.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_yukari_front_to_sora_front.gif) 

    - 紫苑对比若苗紫front（血缘关系），参见[“紫苑”](#紫苑(Shion)-正面)节。  
    - 盐谷对比若苗紫front（血缘关系），参见[“盐谷”](#盐谷(Shionoya)-正面)节。  
    - 雅彦对比若苗紫front（血缘关系），参见[“雅彦”](#雅彦(Masahiko)-正面)节。  
    - 雅美对比若苗紫front（血缘关系），参见[“雅美”](#雅美(Masami)-正面)节。  
    - 早纪front对比紫front，参见[“早纪front”](#早纪(Saki)-正面)一节）  

- open-mouth:  
    - 图片：05_050_6, 06_013_5, 09_091_3。  
    - 特点：正面, 张嘴，下巴下移。  
    - 本组图片叠加后的效果：  
        ![](img/face_front_yukari_open-mouth.jpg)  
    - 该图变化到front（闭嘴的效果）：  
        ![](img/face_front_yukari_open-mouth.jpg) -- 
        ![](img/face_front_yukari_open-mouth_to_front.gif) --> 
        ![](img/face_front_yukari_front.jpg)  

- down:  
    - 图片：01_124_1，02_000a，02_189_0，04_022_2，04_062_4，04_170_5，05_064_2，05_174_0。
    - 特点：正面, 鼻子略微上移(略微抬头)。  
    - 本组图片叠加后的效果（因彩图不易比较，故不包含彩图）：  
        ![](img/face_front_yukari_down.jpg)  
    - 该图变化到front（略微低头的效果）：  
        ![](img/face_front_yukari_down.jpg) -- 
        ![](img/face_front_yukari_down_to_front.gif) --> 
        ![](img/face_front_yukari_front.jpg)  

- wide:  
    - 图片：01_006_1，01_012_3，02_018_6，08_066_3。  
    - 特点：正面, 脸略宽。  
    - 本组图片叠加后的效果（因彩图不易比较，故不包含彩图）：  
        ![](img/face_front_yukari_wide.jpg) 
    - 该图变化到front：  
        ![](img/face_front_yukari_wide.jpg) -- 
        ![](img/face_front_yukari_wide_to_front.gif) --> 
        ![](img/face_front_yukari_front.jpg)  

- mama:  
    - 图片：01_025_4，02_027_2。  
    - 特点：雅彦的妈妈。  
    - 本组图片叠加后的效果：  
        ![](img/face_front_yukari_mama.jpg) 
    - 该图变化到front（雅彦妈妈和若苗紫的相貌比较，几乎没有差别）：  
        ![](img/face_front_yukari_mama.jpg) -- 
        ![](img/face_front_yukari_mama_to_front.gif) --> 
        ![](img/face_front_yukari_front.jpg)  


### 浅冈叶子(Yoko)-正面  {#浅冈叶子(Yoko)-正面}  
分为几类：  

- front：  
    - 图片：13_178_5，13_172_0，13_154_2，13_142_1，08_160_3，08_153_3，08_152_6，06_007_3_，05_160_2，05_142_1，05_097_5，04_178_0，04_136_3，03_041_2  
    - 特点：  
        - 正面。叶子的腮帮子稍凸(婴儿肥?)。
        - 相对于该角色的其他组，本组图片在数量上占绝对优势，说明**(作品前中后期)浅冈叶子的正面脸颊画法稳定**。
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(本组脸颊类似雅彦、紫苑；本组嘴和雅彦一致，所以**叶子的嘴是按男性比例**)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）雅彦Group2渐变对比；  
      （下图左7）紫苑Group2渐变对比；  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img/face_front_masahiko_group2_front_to_yoko_front.gif) 
        ![](img/face_front_shion_group2-front_to_yoko_front.gif) 

    - 叶子和叶子母13_103_3的对比（血缘关系），参见[“叶子母”](#叶子母-正面)一节。  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**雅美Group0**(红色mark点)的对比(嘴宽一致。本组11偏下(下嘴唇更厚);本组8/鼻下点低（人中短）；本组31/32点(右左脸latgo)高(腮帮子平(更有棱角)))；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美Group0;  
      （下图左五）雅美Group0渐变对比;  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_masami-group0.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_masami-group0.jpg) 
        ![](img/face_front_masami_group0_vol03.jpg) 
        ![](img/face_front_masami_group0_vol03_to_yoko_front.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**盐谷psudo-front**(蓝色mark点)的对比(本组下巴颏更尖(mark点反映不出该特点)；本组31/32点（右脸latgo/左脸latgo）偏上，说明本组腮帮子更平(更有棱角))；  
      （下图左三）左一叠加左二：  
      （下图左四）盐谷psudo-front;  
      （下图左五）盐谷psudo-front渐变对比;  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_shya-psudo-front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_shya-psudo-front.jpg) 
        ![](img/face_front_shya_psudo-front.jpg) 
        ![](img/face_front_shya_psudo-front_to_yoko_front.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**若苗空front**(蓝色mark点)的对比（**需仔细分辨**：本组31/32点（右脸latgo/左脸latgo）更靠外侧，表示叶子腮帮子更凸。29/30点、31/32点一起说明若苗空腮帮子线条更有棱角）；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front;  
      （下图左五）若苗空front渐变对比（叶子下巴颏更尖）;  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_sora_front_to_yoko_front.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**若苗紫front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗紫front;  
      （下图左五）若苗紫front渐变对比;  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_yukari_front_to_yoko_front.gif) 

- short:  
    - 图片：04_161_0，06_060_3，13_108_2，  
    - 特点：相比front组，本组脸稍短。  
    - 本组图片叠加后的效果：  
        ![](img/face_front_yoko_front_short.jpg)  

- long:  
    - 图片：02_065_0，02_092_5，03_072_3，04_139_5，11_009_5，11_056_1。  
    - 特点：相比front组，本组脸稍长(嘴微张)、左腮帮子稍凸。  
    - 本组图片叠加后的效果：  
        ![](img/face_front_yoko_front_long_left-cheek-bump.jpg)  


### 浩美(Hiromi)-正面  {#浩美(Hiromi)-正面}  

- front:  
    - 图片：09_042_4，05_075_1，03_008_3，01_191_5，01_187_4。  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。右脸相对小、左脸相对大(也可能是脸向右侧转)；  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子位于中线，是严格正面。  
        ![](img/face_front_hiromi_front.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_hiromi-front_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_hiromi_front.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

- right:  
    - 图片：09_047_4，07_106_1，05_130_2。  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子位于中线，是严格正面。  
        ![](img/face_front_hiromi_right.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right_rl-cmp.jpg) 
        ![](img/face_asymmetry_hiromi-front_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_hiromi_right.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**叶子front**(红色mark点)的对比(本组31点(右脸latgo)偏上，说明右脸腮帮子不比叶子凸)；  
      （下图左三）左一叠加左二：  
      （下图左四）叶子front；  
      （下图左五）叶子front的渐变对比；  
        ![](img/face_front_hiromi_right.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right_vs_yoko-front.jpg) 
        ![](img2/face_front_cmp_aps__hiromi-right-p_vs_yoko-front.jpg) 
        ![](img/face_front_yoko_front.jpg) 
        ![](img/face_front_yoko_front_vs_hiromi_right.gif) 

    - 早纪front对比浩美right，参见[“早纪front”](#早纪(Saki)-正面)一节。  


### 熏(Kaoru)-正面   {#熏(Kaoru)-正面}  

- front:  
    - 图片：08_013_4，07_193_5。  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)；  
        ![](img/face_front_kaoru_front.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴宽接近紫苑；脸颊似乎更象雅彦）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_kaoru_front.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**浩美right**(红色mark点)的对比(**31/右脸latgo位置类似，说明两人腮帮子类似；又因为31/右脸latgo位置高，说明两人腮帮子都不凸。熏脸颊(右腮帮子)更有棱角，体现在29、31这两点离得更近**)；  
      （下图左三）左一叠加左二：  
      （下图左四）浩美right;  
      （下图左四）浩美right渐变对比；  
        ![](img/face_front_kaoru_front.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 
        ![](img/face_front_hiromi_front_vs_kaoru_front.gif) 

    - 熏front和早纪front、早纪top-short的对比（血缘关系），参见[“早纪front”](#早纪(Saki)-正面)一节。  
    - 熏front和辰巳front、辰巳left的对比（血缘关系），参见[“辰巳”](#辰巳(Tatsumi)-正面)一节。  
    - 熏front和江岛wide的对比，参见["江岛wide"](#江岛(Ejima)-正面)一节。    
    - 熏front对比麻衣front，参见[“麻衣front”](#麻衣(Mai)-正面)一节。  


### 江岛(Ejima)-正面  {#江岛(Ejima)-正面}  

- wide:
    - 图片：14_025_1，05_012_0。  
    - 特点： 脸颊稍宽。  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_ejima_wide.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴宽，**脸颊窄**）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_ejima_wide.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**空front**(红色mark点)的对比（本组**脸颊窄**）；  
      （下图左三）左一叠加左二：  
      （下图左四）空front;  
        ![](img/face_front_ejima_wide.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)和**熏front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）熏front;  
      （下图左五）放大对比。江岛(蓝色)的29/ang点比熏(红色)的点高;  
        ![](img/face_front_ejima_wide.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__ejima-wide-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg)  
        ![](img2/face_front_ejima_wide_vs_kaoru_front.jpg)  

- front
    - 图片：10_123_1，04_139_5，04_096_6，04_103_2，03_004_4。  
    - 本组图片叠加后的效果：  
        ![](img/face_front_ejima_front.jpg)  


### 辰巳(Tatsumi)-正面  {#辰巳(Tatsumi)-正面}  

- front：  
    - 图片：09_134_6，09_110_1，04_139_4，03_125_5。  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）示意图。虽然左右脸不对称，但人中、嘴唇中点位于"两耳外侧间距的中线"上(如下图所示)，说明这是严格的正面。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。  
      （下图左三）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_tatsumi_front.jpg) 
        ![](img/face_asymmetry_tatsumi-front_with_markline.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front_rl-cmp.jpg) 

    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_tatsumi_front.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、和**若苗空front**(红色mark点)的对比(**脸颊几乎一致**；本组嘴偏上，(可能是为了让)下巴显得更长)；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front；  
      （下图左五）若苗空front渐变对比；  
        ![](img/face_front_tatsumi_front.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
        ![](img/face_front_sora_front_vs_tatsumi_front.gif)  
      辰巳front和空front脸颊几乎一致，说明辰巳是按照若苗空的相貌设计的。我猜，之所以为辰巳加上胡子和伤疤，可能是为了避免两人相貌太相似。为辰巳去掉胡子和伤疤，对比一下：    
    ![](img/face_front_tatsumi_front.jpg) 
    ![](img2/face_front_tatsumi_front_no_bear_scar.jpg) 
    ![](img/face_front_sora_front.jpg) 

    - 辰巳front对比葵front，参见[“葵”](#葵(Aoi)-正面)一节。  

- left：  
    - 图片：13_074_4，09_093_7，06_026_5。  
    - 特点：脸向右转，左脸稍微偏大，右脸稍微偏小。  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。以耳朵为准。鼻子偏中线左，故**不是严格正面**，所以本组对比说服力不大。  
        ![](img/face_front_tatsumi_left.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left_rl-cmp.jpg) 
        ![](img/face_asymmetry_tatsumi-left_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴宽）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_tatsumi_left.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**空front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front；  
        ![](img/face_front_tatsumi_left.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__tatsumi-left-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

    - 辰巳left对比摄像师，参见[“摄像师open”](#摄像师-正面)一节。  
    - 辰巳left对比八不02_183_7，参见[“八不”](#八不(Hanzu)-正面)一节。  

- （下图左一）熏front图片叠加后的效果；  
  （下图左二）熏front(黑色mark点)、**辰巳front**(蓝色mark点)、**辰巳left**(绿色mark点)的对比（疑似[^6]**血缘关系**）（和辰巳front相似度高）；  
  （下图左三）左一叠加左二：  
  （下图左四）辰巳front；  
  （下图左五）辰巳left；  
    ![](img/face_front_kaoru_front.jpg) 
    ![](img2/face_front_cmp_aps__kaoru-front_vs_tatsumi-left_vs_tatsumi-front.jpg) 
    ![](img2/face_front_cmp_aps__kaoru-front-p_vs_tatsumi-left_vs_tatsumi-front.jpg) 
    ![](img/face_front_tatsumi_front.jpg) 
    ![](img/face_front_tatsumi_left.jpg) 

- left_wide:  
    - 图片：06_041_0，04_135_6。  
    - 特点：相比left组而言，脸偏宽；  
    - 本组图片叠加后的效果：  
        ![](img/face_front_tatsumi_left_wide.jpg)  

- long：  
    - 图片：09_135_0, 09_095_3。  
    - 特点：脸偏长(下巴下移?)  
    - 本组图片叠加后的效果：  
        ![](img/face_front_tatsumi_long.jpg)  


### 导演-正面  {#导演-正面}   
首先，导演戴眼镜，所以无法定位眼睛的位置。只能大概估计外眼角水平位置约为眼镜腿的位置。其次，导演胖，五官比例不符合标准人像比例，但符合胖人的人像比例。以下显示了胖人的五官比例[^1]：  
![](img3/DrawingTheHeadHands.AndrewLoomis.page014.jpg)  

- front：  
    - 图片：03_061_2, 03_099_1。  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_director_front.jpg) 
        ![](img2/face_front_cmp_aps__director-front_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(本组脸颊凸出很多，胖；下巴颏位置一致，所以，脸大是因为胖)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_director_front.jpg) 
        ![](img2/face_front_cmp_aps__director-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__director-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - 导演front对比雅彦group2。前者脸胖于后者。对比导演(蓝色)、雅彦(红色)脸颊上的点(27/zy，29/ang，31/latgo，33/chk，35/al-chin):   
        ![](img2/face_front_masahiko_group2_vs_director_front.jpg)  

    - 导演front对比绫子front（两人都胖），参见[“绫子front”](#绫子(Aya)-正面)一节。  


### 早纪(Saki)-正面  {#早纪(Saki)-正面}  

- front:  
    - 图片：10_042_5, 09_176_2, 09_175_7, 09_175_1, 09_174_4, 09_152_3。  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__yukari-front_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴宽和紫苑一致；脸颊更类似雅彦）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）放大对比早纪front(蓝色)、雅彦Group2(红色)脸颊上的点, 29/ang，31/latgo，35/al-chin几乎重合，说明两人脸形相似。27/zy，33/chk差异较大，说明雅彦腮帮子更凸；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img2/face_front_masahiko_group2_vs_saki_front.jpg)  

    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比（**血缘关系**）(脸形很类似)；  
      （下图左三）左一叠加左二：  
      （下图左四）熏front；   
      （下图左五）熏front渐变对比；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 
        ![](img/face_front_kaoru_front_vs_saki_front.gif) 
    
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**若苗紫front**(红色mark点)的对比(若苗紫左脸宽、嘴宽、眼间距宽)；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗紫front；  
      （下图左五）放大对比早纪front(蓝)、若苗紫front(红)；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg)  
        ![](img2/face_front_yukari_front_vs_saki_front.jpg) 
    
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**雅美group0**(红色mark点)的对比(本组下巴更长(或许是因为雅美闭嘴，早纪稍张嘴)；本组27/28/颧点窄；本组31/32点（右脸latgo/左脸latgo）偏上，说明腮帮子不凸；雅美嘴宽、8/鼻下点高；两人瞳孔间距一致)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美group0；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_masami-group0.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_masami-group0.jpg) 
        ![](img/face_front_masami_group0_vol03.jpg) 
    
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比(雅美嘴宽、瞳孔间距宽、8/鼻下点低)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美group1；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
    
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比(本组嘴窄、脸颊稍窄；本组脸颊(腮帮子)更有棱角([疑问]mark点如何体现出这个特点？))；  
      （下图左三）左一叠加左二；  
      （下图左四）浩美right；  
      （下图左五）放大对比早纪(蓝)、浩美(红)。  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg)  
        ![](img2/face_front_hiromi_right_vs_saki_front.jpg) 
    
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比(**脸颊几乎一致**。早纪嘴窄。早纪下巴尖(未能从mark点上反映出来))；  
      （下图左三）左一叠加左二；  
      （下图左四）若苗空front；  
        ![](img/face_front_saki_front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__saki-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 
    
    - 顺子front对比早纪front，参见[“顺子front”](#顺子(Yoriko)-正面)一节。  

- top_short:  
    - 图片：11_113_2，10_043_1。  
    - 特点：稍微低头、下巴稍短；  
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_saki_top_short.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴宽(因为右嘴角有微笑)、8/鼻下点低(因为稍低头)、腮帮子窄(不凸)）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_saki_top_short.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
    
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**紫front**(红色mark点)的对比(本组脸窄)；  
      （下图左三）左一叠加左二：  
      （下图左四）紫front；  
        ![](img/face_front_saki_top_short.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
    
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**盐谷right-top**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）盐谷right-top；  
        ![](img/face_front_saki_top_short.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short_vs_shya-right-top.jpg) 
        ![](img2/face_front_cmp_aps__saki-top-short-p_vs_shya-right-top.jpg) 
        ![](img/face_front_shya_right-top.jpg) 
    
    - （下图左一）本组图片叠加后的效果；  
      （下图左二）熏front(黑色mark点)、**早纪front**(红色mark点)、**早纪top-short**(紫色mark点)的对比（**血缘关系**）（相似度高；比起辰巳，熏的脸颊更象早纪）；  
      （下图左三）左一叠加左二：  
      （下图左四）早纪front；  
      （下图左五）早纪top-short；  
        ![](img/face_front_kaoru_front.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front_vs_saki-front_vs_saki-top-short.jpg) 
        ![](img2/face_front_cmp_aps__kaoru-front-p_vs_saki-front_vs_saki-top-short.jpg) 
        ![](img/face_front_saki_front.jpg) 
        ![](img/face_front_saki_top_short.jpg) 
    
    - 叶子母13_103_3和早纪top_short，参见[“叶子母”](#叶子母-正面)一节。  


### 真琴(Makoto)-正面  {#真琴(Makoto)-正面}  
几乎都是长大嘴，多数是笑（这是否能反映她的性格？）。  

- front:  
    - 图片：03_021_2, 04_176_1, 06_005_6, 08_110_0, 07_075_5, 07_072_0。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。左右脸差异大：  
      （下图左三）示意图。因耳朵被遮挡，故以颧骨为准。鼻子、下巴偏离中线，**不是严格正面**，所以本组对比说服力不大。   
      （下图左四）示意图。以外眼角为准。鼻子位于中线，是严格正面。（[疑问]如何解释左三和左四的矛盾？一个可能的解释：因为面部转动幅度小，所以左右脸差异小；故以外眼角为准测不出差异，而以颧骨为准能测出来差异。另一个解释：以鼻子为中线，该角色的左右外眼角对称，而左右颧骨不对称。）   
        ![](img/face_front_makoto_front.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_makoto-front_with_markline_0.jpg) 
        ![](img/face_asymmetry_makoto-front_with_markline_1.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二；  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_makoto_front.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比；  
      （下图左三）左一叠加左二；  
      （下图左四）雅美group1；  
      （下图左五）雅美group1渐变对比；  
        ![](img/face_front_makoto_front.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img/face_front_masami_group1_vs_makoto_front.gif) 

    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**雅美group0**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美group0；  
        ![](img/face_front_makoto_front.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front_vs_masami-group0.jpg) 
        ![](img2/face_front_cmp_aps__makoto-front-p_vs_masami-group0.jpg) 
        ![](img/face_front_masami_group0_vol03.jpg) 

- down:  
    - 图片：01_063_0, 07_051_1, 07_112_5。  
    - 特点： 稍抬头(镜头俯视)。
    - 本组图片叠加后的效果：  
    ![](img/face_front_makoto_down.jpg)    


### 摄像师-正面  {#摄像师-正面}  
几乎都是长大嘴。  

- open:   
    - 图片：14_171_4, 12_157_5, 12_148_0, 10_067_3, 03_070_4。  
    - 特点：正面、张嘴；**因线条修饰，颧骨很明显**。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_cameraman_open.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组脸颊稍窄;本组嘴高、14/下颏点稍低(显得下巴长)）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_cameraman_open.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**辰巳left**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）辰巳left；  
        ![](img/face_front_cameraman_open.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open_vs_tatsumi-left.jpg) 
        ![](img2/face_front_cmp_aps__cameraman-open-p_vs_tatsumi-left.jpg) 
        ![](img/face_front_tatsumi_left.jpg) 


### 浅葱(Asagi)-正面  {#浅葱(Asagi)-正面}  
  
- front:   
    - 图片：14_250_1，14_197_0，14_196_1，14_193_1，14_185_2，14_169_5，14_090_1，14_057_0，14_039_0。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_assagi_front.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴宽同雅彦；本组嘴角略高(有微笑?下嘴唇厚?)）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_assagi_front.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果；  
      （下图左二）本组(黑色mark点)、**若苗紫front**(红色mark点)的对比(**脸颊几乎一致**；浅葱眉略高；浅葱嘴角略高(有微笑?下嘴唇厚?)；若苗紫的发际线略高(因为年龄大？))；  
      （下图左三）左一叠加左二：  
      （下图左四）紫front；  
      （下图左五）紫front渐变对比；  
        ![](img/face_front_assagi_front.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__assagi-front-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg) 
        ![](img/face_front_yukari_front_vs_assagi_front.gif) 


### 和子(Kazuko)-正面  {#和子(Kazuko)-正面}   
首先，和子是大块头，所以头应该大；所以让其脸宽符合参考图，使得头偏大。其次，和子的面部比例大多不符合标准：    
![](img/01_052_2__mod.jpg) 
![](img/08_092_3__mod.jpg)  

- front_open：  
    - 图片：08_120_3，08_091_5。  
    - 特点：正面、稍张嘴；  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_kazuko_front_open.jpg) 
        ![](img2/face_front_cmp_aps__kazuko-front-open_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组下颌骨明显大一圈）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_kazuko_front_open.jpg) 
        ![](img2/face_front_cmp_aps__kazuko-front-open_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kazuko-front-open-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

- 和子(和人)的脸型和以上人物都不相似。  
- 和子与宪司这两个大块头的对比，见[“宪司front”](#宪司(Kenji)-正面)一节。  


### 爷爷-正面  {#爷爷-正面}  
待完成  


### 横田进(Susumu)-正面  {#横田进(Susumu)-正面}  
  
- 03_021_2，横田进这个镜头不符合标准比例：  
    ![](img/03_021_2__mod.jpg)  

- 03_012_5(第一次出场时的正面特写)：  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴比雅彦宽、脸颊稍窄）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）放大对横田进(蓝)、雅彦group2(红)。横田进更有棱角、腮帮子不凸。29/ang, 31/latgo, 33/chk, 35/al-chin这些点的差异说明本组的腮帮子更平(即，不凸)。  
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg)  
        ![](img2/face_front_susumu_03_012_5_vs_masahiko_group2.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**空front**(红色mark点)的对比（横田进脸颊稍窄；嘴宽一致）；  
      （下图左三）左一叠加左二：  
      （下图左四）空front；  
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__susumu-03_012_5-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

    - 千夏12_141_0对比横田进03_012_5，参见[“千夏”](#千夏(Chinatsu)-正面)一节。  
    - 葵front对比横田进，参见["葵"](#葵(Aoi)-正面)一节。  

- open：  
    - 图片：03_094_1，03_012_5。  
    - 特点：微张嘴。  
    - 本组图片叠加后的效果：  
        ![](img/face_front_susumu_open.jpg)  
    - 横田进open和空front的脸形相似，前者脸颊窄于后者。   


### 宪司(Kenji)-正面  {#宪司(Kenji)-正面}  
宪司是大块头，所以头应该大；所以让其脸宽符合参考图，使得头偏大。  

- front:  
    - 图片：04_039_5, 04_035_1, 04_014_1。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_kenji_front.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front_rl-cmp.jpg)  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组下颌骨大两圈）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_kenji_front.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg)  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**和子(和人)front_open**(红色mark点)的对比（**宪司下巴更大**，这符合设定--宪司开店需要经常装货卸货，比起日常工作是画漫画的和子，宪司有更多体力活儿）；  
      （下图左三）左一叠加左二;  
      （下图左四）和子(和人)front_open;  
      （下图左五）放大对比宪司(蓝色)、和子(红色)；  
        ![](img/face_front_kenji_front.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front_vs_kazuko-front-open.jpg) 
        ![](img2/face_front_cmp_aps__kenji-front-p_vs_kazuko-front-open.jpg) 
        ![](img/face_front_kazuko_front_open.jpg)  
        ![](img2/face_front_kazuko_front_vs_kenji_front.jpg)  


### 顺子(Yoriko)-正面  {#顺子(Yoriko)-正面}  
  
- front:  
    - 图片：10_173_3，04_039_0，04_016_3，06_187_0，03_172_0。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比（**血缘关系**）（**左脸颊一致**。本组嘴窄。左嘴角一致，因为本组有微笑）；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front;  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比（嘴宽一致）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美group1;  
      （下图左五）雅美group1渐变对比;  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img/face_front_masami_group1_vs_yoriko_front.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比；  
      （下图左三）左一叠加左二;  
      （下图左四）熏front;  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**早纪front**(红色mark点)的对比(**左脸几乎一致**)；  
      （下图左三）左一叠加左二;  
      （下图左四）早纪front;  
      （下图左五）放大对比顺子(蓝)、早纪(红)。顺子的脸比早纪大一圈。1/latgo, 35/al-chin点的差异，说明那里顺子腮帮子比早纪的凸。  
        ![](img/face_front_yoriko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front_vs_saki-front.jpg) 
        ![](img2/face_front_cmp_aps__yoriko-front-p_vs_saki-front.jpg) 
        ![](img/face_front_saki_front.jpg)  
        ![](img2/face_front_yoriko_front_vs_saki_front.jpg)  


### 森(Mori)-正面  {#森(Mori)-正面}  
  
- front:  
    - 图片：10_102_3，10_102_1。  
    - 特点：右脸颊被头发遮挡；  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_mori_front.jpg) 
        ![](img2/face_front_cmp_aps__mori-front_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(因为张嘴，本组嘴角、下嘴唇偏下；本组嘴宽介于雅彦和紫苑之间；本组左脸窄)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_mori_front.jpg) 
        ![](img2/face_front_cmp_aps__mori-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__mori-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比(本组左脸窄)；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗空front;  
      （下图左五）放大对比对比森(蓝色)、空(红色)；  
        ![](img/face_front_mori_front.jpg) 
        ![](img2/face_front_cmp_aps__mori-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__mori-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg)  
        ![](img2/face_front_sora_front_vs_mori_front.jpg)  


### 叶子母-正面  {#叶子母-正面}  
浅冈叶子的母亲。  

- 13_103_3(第一次出场的正面)：  
    - （下图左一）本组图片叠加后的效果;      
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(本组嘴宽介于雅彦和紫苑之间；本组脸颊稍窄；)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
    
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**叶子front**(红色mark点)的对比（**血缘关系**）（脸颊类似；两人嘴宽一致；叶子11点偏下(下嘴唇更厚);右脸29/ang、31/latgo的差异说明叶子腮帮子更凸(婴儿肥?)）；  
      （下图左三）左一叠加左二：  
      （下图左四）叶子母13_103_3；  
      （下图左五）放大对比本组(蓝色)、叶子(红色)；  
        ![](img/face_front_yoko_front.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front_vs_ykma-13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__yoko-front-p_vs_ykma-13_103_3.jpg) 
        ![](img/face_front_ykma_13_103_3.jpg)  
        ![](img2/face_front_yoko_front_vs_ykma_13_103_3.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比()；  
      （下图左三）左一叠加左二：  
      （下图左四）浩美right;  
      （下图左五）浩美right渐变对比;  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 
        ![](img/face_front_hiromi_right_vs_ykma_13_103_3.gif)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）熏front;  
      （下图左五）熏front渐变对比;  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 
        ![](img/face_front_yoko_front_vs_ykma_13_103_3.gif)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**早纪front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）早纪front;  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_saki-front.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_saki-front.jpg) 
        ![](img/face_front_saki_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**早纪top_short**(红色mark点)的对比（脸颊类似，叶子母下颌骨末端偏高）；  
      （下图左三）左一叠加左二：  
      （下图左四）早纪top_short;  
      （下图左五）放大对比本组(蓝色)、早纪(红色)。右脸29/ang、31/latgo的变化方向相反，说明(这部位)早纪棱角更明显。  
        ![](img/face_front_ykma_13_103_3.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3_vs_saki-top-short.jpg) 
        ![](img2/face_front_cmp_aps__ykma-13_103_3-p_vs_saki-top-short.jpg) 
        ![](img/face_front_saki_top_short.jpg)  
        ![](img2/face_front_saki_top-short_vs_ykma_13_103_3.jpg)  

- open:  
    - 图片：13_180_4，13_161_3。  
    - 特点：稍张嘴；
    - 本组图片叠加后的效果：  
        ![](img/face_front_ykma_open.jpg)   


### 理沙(Risa)-正面  {#理沙(Risa)-正面}  
  
- 12_025_0：  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。左右脸颊差异明显：  
      （下图左三）示意图。因耳朵、颧骨被遮挡，故以外眼角为准。鼻、嘴、下巴稍微偏离中线，**不是严格正面**，所以本组对比说服力不大。(但该角色没有更好的正面图可用)   
        ![](img/face_front_risa_12_025_0.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0_rl-cmp.jpg) 
        ![](img/face_asymmetry_risa-12_025_0_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(**本组脸宽、下巴圆**；本组嘴宽和雅彦一致；)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_risa_12_025_0.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**紫苑group0**(红色mark点)的对比（本组脸大一圈）；  
      （下图左三）左一叠加左二：  
      （下图左四）紫苑group0;  
        ![](img/face_front_risa_12_025_0.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0_vs_shion-group0.jpg) 
        ![](img2/face_front_cmp_aps__risa-12_025_0-p_vs_shion-group0.jpg) 
        ![](img/face_front_risa_12_025_0.jpg) 
        ![](img/face_front_shion_group0-young.jpg) 


### 美菜(Mika)-正面  {#美菜(Mika)-正面}  
美菜的脸最宽。  

- front:  
    - 图片：12_008_6，11_167_4。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。左右脸颊差异明显；  
      （下图左三）示意图。因耳朵、颧骨被遮挡，故以外眼角为准。鼻子位于中线，是严格正面。  
        ![](img/face_front_mika_front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_mika-front_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(**本组脸宽**)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_mika_front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__mika-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比(**本组脸宽**)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美group1；  
      （下图左五）放大对比美菜(蓝色)、雅美(红色)；  
        ![](img/face_front_mika_front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__mika-front-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg)  
        ![](img2/face_front_mika_front_vs_masami_group1.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗紫front**(红色mark点)的对比(**本组脸略宽**)；  
      （下图左三）左一叠加左二：  
      （下图左四）若苗紫front；  
      （下图左五）放大对比美菜(蓝色)、若苗紫(红色)；  
        ![](img/face_front_mika_front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front_vs_yukari-front.jpg) 
        ![](img2/face_front_cmp_aps__mika-front-p_vs_yukari-front.jpg) 
        ![](img/face_front_yukari_front.jpg)  
        ![](img2/face_front_mika_front_vs_yukari_front.jpg) 

    - 齐藤茜front和美菜front，参见[“齐藤茜front”](#齐藤茜(Akane)-正面)一节。  

- open:  
    - 图片：12_008_6。  
    - 特点：张嘴；  
    ![](img/face_front_mika_open.jpg)   


### 齐藤茜(Akane)-正面  {#齐藤茜(Akane)-正面}  
  
- front:  
    - 图片：06_145_7，06_144_5。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。以耳朵为准(右耳轮廓模糊)。鼻子位于中线，是严格正面。   
      （下图左四）示意图。以外眼角为准。鼻子偏离中线，不是严格正面。（[疑问]如何解释左三和左四的矛盾？）   
        ![](img/face_front_akane_front.jpg) 
        ![](img2/face_front_cmp_aps__akane-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_akane-front_with_markline_0.jpg) 
        ![](img/face_asymmetry_akane-front_with_markline_1.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴宽(可能因为有笑容)）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_akane_front.jpg) 
        ![](img2/face_front_cmp_aps__akane-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__akane-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**美菜front**(红色mark点)的对比(脸型类似；**美菜脸宽**)；  
      （下图左三）左一叠加左二；  
      （下图左四）美菜front；  
      （下图左五）放大对比齐藤茜(蓝色)、美菜(红色)；  
        ![](img/face_front_akane_front.jpg) 
        ![](img2/face_front_cmp_aps__akane-front_vs_mika-front.jpg) 
        ![](img2/face_front_cmp_aps__akane-front-p_vs_mika-front.jpg) 
        ![](img/face_front_mika_front.jpg)  
        ![](img2/face_front_akane_front_vs_mika_front.jpg) 

- wide:  
    - 图片：07_099_3，06_157_1。  
    - 特点：和front组几乎一致，但鬓角处稍宽；  
    - 本组图片叠加后的效果：  
        ![](img/face_front_akane_wide.jpg)   


### 仁科(Nishina)-正面  {#仁科(Nishina)-正面}  
  
- 14_014_5:  
    - 特点：只显示了右脸颊；  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_nishina_14_014_5.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比(**本组脸颊窄**)；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）放大对比仁科(蓝色)、雅彦group2(红色)。29/ang，31/latgo的差异说明本组腮帮子不凸；  
        ![](img/face_front_nishina_14_014_5.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg)  
        ![](img2/face_front_nishina_14_014_5_vs_masahiko_group2.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比(本组腮帮子稍窄；**本组31/右脸latgo偏下，说明本组腮帮子处更凸（横田进更有棱角、更男性化）**)；  
      （下图左三）左一叠加左二：  
      （下图左四）横田进03_012_5；  
        ![](img/face_front_nishina_14_014_5.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__nishina-14_014_5-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 


### 葵(Aoi)-正面  {#葵(Aoi)-正面}  
该角色的造型(下图左一)类似CH里獠的异装(下图左二)：  
![](../fc_dress/img/06_039_0__.jpg) 
![](../ch_make-up/img/ch_34_093__mod.jpg)  

- front:  
    - 图片：06_073_4，06_042_1。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_rl-cmp.jpg) 
    - 特点：正面。虽然左右脸不对称，但人中、嘴唇中点位于"两外眼角间距的中线"上(如下图所示)，说明这是严格的正面。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。    
        ![](img/face_asymmetry_aoi-front_with_markline.jpg)  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比。**葵的左右脸不对称，若苗空的左右脸也不对称；但两人左脸类似，右脸也类似**。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。  
      （下图左三）左一叠加左二；  
      （下图左四）若苗空front;  
      （下图左五）放大对比葵(蓝色)、空(红色)；29/ang，31/latgo，35/al-chin点的差异表明葵的脸颊线条比空的更圆滑(更不棱角分明、更女性化)；  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg)  
        ![](img2/face_front_aoi_front_vs_sora_front.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**辰巳front**(红色mark点)的对比(脸形类似，葵右下巴比辰巳窄)；**葵的左右脸不对称，辰巳的左右脸也不对称；但两人左脸类似，右脸也类似**。在本文[正面左右脸颊不对称](#正面左右脸颊不对称)一节里归纳出了该画法的规律。  
      （下图左三）左一叠加左二；  
      （下图左四）辰巳front;  
      （下图左五）放大对比葵(蓝色)、辰巳(红色)；35/al-chin点的差异表明葵的右下巴窄；  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_tatsumi-front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_tatsumi-front.jpg) 
        ![](img/face_front_tatsumi_front.jpg)      
        ![](img2/face_front_aoi_front_vs_tatsumi_front.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**横田进13_012_5**(红色mark点)的对比(本组脸稍宽、腮帮子更圆滑。横田进脸稍窄、腮帮子更平、更有棱角)；  
      （下图左三）左一叠加左二；  
      （下图左四）横田进13_012_5：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比(右脸类似；本组脸颊稍宽)；  
      （下图左三）左一叠加左二；  
      （下图左四）熏front：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**江岛wide**(红色mark点)的对比(本组脸颊宽)；  
      （下图左三）左一叠加左二；  
      （下图左四）江岛wide：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_ejima-wide.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_ejima-wide.jpg) 
        ![](img/face_front_ejima_wide.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**真琴front**(红色mark点)的对比(**脸颊处的红黑mark点几乎等距离地间隔排列，这说明什么？**)；  
      （下图左三）左一叠加左二；  
      （下图左四）真琴front：  
        ![](img/face_front_aoi_front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front_vs_makoto-front.jpg) 
        ![](img2/face_front_cmp_aps__aoi-front-p_vs_makoto-front.jpg) 
        ![](img/face_front_makoto_front.jpg)  

 
### 奶奶-正面  {#奶奶-正面}  
待完成  


### 文哉(Fumiya)-正面  {#文哉(Fumiya)-正面}  
  
- front:  
    - 图片：12_012_3，12_010_3。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子位于中线，是严格正面。   
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_fumiya-front_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（因为有笑容，本组嘴角高；本组腮帮子平(即，不凸)）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）放大对比文哉(蓝色)、雅彦(红色)；29/ang点的差异表明本组嘴角高(因为有笑容)；31/latgo，33/chk, 35/al-chin点表明本组右腮帮子线条更平滑（更不棱角分明）。  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg)  
        ![](img2/face_front_fumiya_front_vs_masahiko_group2.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**叶子front**(红色mark点)的对比(叶子腮帮子凸（婴儿肥？）)；  
      （下图左三）左一叠加左二；  
      （下图左四）叶子front：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_yoko-front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_yoko-front.jpg) 
        ![](img/face_front_yoko_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比（本组右腮帮子线条更平滑（更不棱角分明））；  
      （下图左三）左一叠加左二；  
      （下图左四）浩美right：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**江岛wide**(红色mark点)的对比；  
      （下图左三）左一叠加左二；  
      （下图左四）江岛wide：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_ejima-wide.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_ejima-wide.jpg) 
        ![](img/face_front_ejima_wide.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比(本组右腮帮子线条更平滑（更不棱角分明）)；  
      （下图左三）左一叠加左二；  
      （下图左四）横田进03_012_5；  
      （下图左五）横田进03_012_5渐变对比：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5_vs_fumiya_front.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**葵front**(红色mark点)的对比(本组脸颊稍窄)；  
      （下图左三）左一叠加左二；  
      （下图左四）葵front：  
        ![](img/face_front_fumiya_front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front_vs_aoi-front.jpg) 
        ![](img2/face_front_cmp_aps__fumiya-front-p_vs_aoi-front.jpg) 
        ![](img/face_front_aoi_front.jpg) 


### 齐藤玲子(Reiko)-正面  {#齐藤玲子(Reiko)-正面}  
  
- 01_166_0:  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_reiko_01_166_0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_reiko_01_166_0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**紫苑group0**(红色mark点)的对比(两组的相貌看起来年龄小；本组脸颊稍宽)；  
      （下图左三）左一叠加左二；  
      （下图左四）紫苑group0；  
      （下图左五）紫苑group0渐变对比;  
        ![](img/face_front_reiko_01_166_0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0_vs_shion-group0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0-p_vs_shion-group0.jpg) 
        ![](img/face_front_shion_group0-young.jpg) 
        ![](img/face_front_shion_group0_vs_reiko_01_166_0.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**齐藤茜front**(红色mark点)的对比(本组脸颊比齐藤茜小(也可能是本组放缩不当?))；  
      （下图左三）左一叠加左二；  
      （下图左四）齐藤茜front；  
        ![](img/face_front_reiko_01_166_0.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0_vs_akane-front.jpg) 
        ![](img2/face_front_cmp_aps__reiko-01_166_0-p_vs_akane-front.jpg) 
        ![](img/face_front_akane_front.jpg) 


### 松下敏史(Toshifumi)-正面  {#松下敏史(Toshifumi)-正面}  
没有严格的正面镜头。  

- 09_077_5:  
    - 特点：稍低头；  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组嘴角宽、偏下(抿嘴?);雅彦腮帮子凸(婴儿肥?)）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）放大对比本组(蓝色)、雅彦(红色)；29/ang, 31/latgo，35/al-chin点表明本组腮帮子没有雅彦凸；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg)  
        ![](img2/face_front_matsu_09_077_5_vs_masahiko_group2.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比(本组脸窄)；  
      （下图左三）左一叠加左二；  
      （下图左四）若苗空front；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**叶子front**(红色mark点)的对比(本组脸窄)；  
      （下图左三）左一叠加左二；  
      （下图左四）叶子front；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_yoko-front.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_yoko-front.jpg) 
        ![](img/face_front_yoko_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**浩美front**(红色mark点)的对比；  
      （下图左三）左一叠加左二；  
      （下图左四）浩美front；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_hiromi-front.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_hiromi-front.jpg) 
        ![](img/face_front_hiromi_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比；  
      （下图左三）左一叠加左二；  
      （下图左四）浩美right；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**辰巳front**(红色mark点)的对比；  
      （下图左三）左一叠加左二；  
      （下图左四）辰巳front；  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_tatsumi-front.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_tatsumi-front.jpg) 
        ![](img/face_front_tatsumi_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比（横田进脸颊稍窄）；  
      （下图左三）左一叠加左二；  
      （下图左四）横田进03_012_5；  
      （下图左五）动态渐变对比：  
        ![](img/face_front_matsu_09_077_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__matsu-09_077_5-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5_vs_matu_09_077_5.gif) 


### 章子(Shoko)-正面  {#章子(Shoko)-正面}  
待完成


### 阿透(Tooru)-正面  {#阿透(Tooru)-正面}  
没有符合参考图的镜头。  

- open:  
    - 图片：12_046_1(张大嘴)，12_044_0(张大嘴)，12_038_3(张大嘴)，12_015_1(张嘴)。  
    - 特点：张嘴；  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子模糊。下巴稍微偏离中线(可能因为咧嘴)，**不是严格正面**，所以本组对比说服力不大。   
        ![](img/face_front_tooru_open.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open_rl-cmp.jpg) 
        ![](img/face_asymmetry_tooru-open_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_tooru_open.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**若苗空front**(红色mark点)的对比；  
      （下图左三）左一叠加左二；  
      （下图左四）若苗空front；  
        ![](img/face_front_tooru_open.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open_vs_sora-front.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open-p_vs_sora-front.jpg) 
        ![](img/face_front_sora_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**仁科14_014_5**(红色mark点)的对比(本组腮帮子凸)；  
      （下图左三）左一叠加左二；  
      （下图左四）仁科14_014_5；  
        ![](img/face_front_tooru_open.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open_vs_nishina-14_014_5.jpg) 
        ![](img2/face_front_cmp_aps__tooru-open-p_vs_nishina-14_014_5.jpg) 
        ![](img/face_front_sora_front.jpg) 


### 京子(Kyoko)-正面  {#京子(Kyoko)-正面}  
  
- 02_177_2:    
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子偏离中线，**不是严格正面**，所以本组对比说服力不大。   
      （下图左三）示意图。以颧骨为准。鼻子偏离中线，**不是严格正面**，所以本组对比说服力不大。   
        ![](img/face_front_kyoko_02_177_2.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2_rl-cmp.jpg) 
        ![](img/face_asymmetry_kyoko-02_177_2_with_markline_0.jpg) 
        ![](img/face_asymmetry_kyoko-02_177_2_with_markline_1.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_kyoko_02_177_2.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**齐藤茜front**(红色mark点)的对比；  
      （下图左三）左一叠加左二；  
      （下图左四）齐藤茜front；  
      （下图左五）齐藤茜front渐变对比：  
        ![](img/face_front_kyoko_02_177_2.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2_vs_akane-front.jpg) 
        ![](img2/face_front_cmp_aps__kyoko-02_177_2-p_vs_akane-front.jpg) 
        ![](img/face_front_akane_front.jpg) 
        ![](img/face_front_akane_front_vs_kyoko_02_177_2.gif) 
        
- top:  
    - 图片：02_186_2，02_186_0。  
    - 特点：稍低头(镜头俯视)；  
    - 本组图片叠加后的效果：  
        ![](img/face_front_kyoko_top.jpg)   


### 一马(Kazuma)-正面  {#一马(Kazuma)-正面}  
待完成  


### 一树(Kazuki)-正面  {#一树(Kazuki)-正面}  

- 12_012_3:  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_kazuki_12_012_3.jpg) 
        ![](img2/face_front_cmp_aps__kazuki-12_012_3_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（本组脸颊类似雅彦；嘴角宽，因为有微笑；）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）雅彦Group2动态渐变对比：  
        ![](img/face_front_kazuki_12_012_3.jpg) 
        ![](img2/face_front_cmp_aps__kazuki-12_012_3_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kazuki-12_012_3-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
        ![](img/face_front_masahiko_group2_vs_kazuki_12_012_3.gif)  

- 一树12_012_3和雅彦group2叠加，可以看看雅彦戴眼镜的样子：  
    ![](img/face_front_kazuki_masahiko_glasses.jpg)   


### 千夏(Chinatsu)-正面  {#千夏(Chinatsu)-正面}  
  
- 12_141_0:  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子位于中线，是严格正面。   
      （下图左三）示意图。以颧骨为准。鼻子位于中线，是严格正面。   
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_rl-cmp.jpg) 
        ![](img/face_asymmetry_chinatsu-12_141_0_with_markline_0.jpg) 
        ![](img/face_asymmetry_chinatsu-12_141_0_with_markline_1.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group3(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**早纪front**(红色mark点)、**早纪top_short**(紫色mark点)的对比（早纪右腮帮子更有棱角）；  
      （下图左三）左一叠加左二：  
      （下图左四）早纪front；  
      （下图左五）早纪top_short；  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_saki-front_vs_saki-top-short.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0-p_vs_saki-front_vs_saki-top-short.jpg) 
        ![](img/face_front_saki_front.jpg) 
        ![](img/face_front_saki_top_short.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比（本组下巴颏更尖(mark点反映不出该特点)；本组31/32点（右脸latgo/左脸latgo）偏下，说明本组腮帮子更圆滑、横田进更平直）；  
      （下图左三）左一叠加左二：  
      （下图左四）横田进03_012_5;  
      （下图左五）放大对比千夏(蓝色)、横田进(红色)。千夏左脸27/zy处稍宽、鬓角处稍宽；   
      （下图左六）放大渐变对比。mark点无法反映出"千夏下巴尖左侧更尖、左下颌骨末端圆滑"这两个特点；  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg)  
        ![](img2/face_front_chinatsu_12_141_0_vs_susumu_03_012_5.jpg) 
        ![](img2/face_front_chinatsu_12_141_0_vs_susumu_03_012_5.gif)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**顺子front**(红色mark点)的对比（千夏右脸稍窄）；  
      （下图左三）左一叠加左二;  
      （下图左四）顺子front;  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_yoriko-front.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141-p_0_vs_yoriko-front.jpg) 
        ![](img/face_front_yoriko_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**文哉front**(红色mark点)的对比（本组下巴颏更尖(mark点反映不出该特点)）；  
      （下图左三）左一叠加左二;  
      （下图左四）文哉front;  
        ![](img/face_front_chinatsu_12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0_vs_fumiya-front.jpg) 
        ![](img2/face_front_cmp_aps__chinatsu-12_141_0-p_vs_fumiya-front.jpg) 
        ![](img/face_front_fumiya_front.jpg) 

- wide:  
    - 图片：14_016_2，12_159_3。  
    - 特点：脸颊宽。  
    - 本组图片叠加后的效果：  
    ![](img/face_front_chinatsu_wide.jpg) 

- 千夏wide和葵front的右脸类似。 前者左脸窄于后者。  
- 千夏wide和江岛wide左脸类似。前者右脸宽于后者。  


### 绫子(Aya)-正面  {#绫子(Aya)-正面}  

- front:  
    - 图片：02_189_4, 02_180_5。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
      （下图左三）示意图。因耳朵被遮挡，故以颧骨为准。鼻子位于中线，是严格正面。   
        ![](img/face_front_aya_front.jpg) 
        ![](img2/face_front_cmp_aps__aya-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_aya-front_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_aya_front.jpg) 
        ![](img2/face_front_cmp_aps__aya-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__aya-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**导演front**(红色mark点)的对比（导演更胖）；  
      （下图左三）左一叠加左二：  
      （下图左四）导演front;  
      （下图左五）放大对比绫子(蓝色)、导演(红色)；  
        ![](img/face_front_aya_front.jpg) 
        ![](img2/face_front_cmp_aps__aya-front_vs_director-front.jpg) 
        ![](img2/face_front_cmp_aps__aya-front-p_vs_director-front.jpg) 
        ![](img/face_front_director_front.jpg)  
        ![](img2/face_front_director_front_vs_aya_front.jpg)  


### 典子(Tenko)-正面  {#典子(Tenko)-正面}  
  
- front:  
    - 图片：02_188_4，02_180_6，02_178_7。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_tenko_front.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_tenko_front.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**横田进03_012_5**(红色mark点)的对比（横田进脸颊的棱角更分明）；  
      （下图左三）左一叠加左二：  
      （下图左四）横田进03_012_5;  
        ![](img/face_front_tenko_front.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front_vs_susumu-03_012_5.jpg) 
        ![](img2/face_front_cmp_aps__tenko-front-p_vs_susumu-03_012_5.jpg) 
        ![](img/face_front_susumu_03_012_5.jpg) 

    - 本组和辰巳left_wide的右脸相似，本组棱角小：   
        ![](img/face_front_tenko_front.jpg)  --
        ![](img/face_front_tatsumi_left-wide_vs_tenko_front.gif) --> 
        ![](img/face_front_tatsumi_left_wide.jpg)  


### 麻衣(Mai)-正面  {#麻衣(Mai)-正面}  

- front:  
    - 图片：12_159_3，12_141_0。  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)。左右脸不对称，左脸大：  
      （下图左三）示意图。因耳朵、颧骨被遮挡，故以外眼角为准。鼻子位于中线，是严格正面。([疑问]如何解释左二左三的矛盾？可能是角色稍向右转头；也可能左脸本来就大)。   
        ![](img/face_front_mai_front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front_rl-cmp.jpg) 
        ![](img/face_asymmetry_mai-front_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（对齐颧点，鼻子未对齐；**脸颊和紫苑几乎一致**）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_mai_front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__mai-front-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 
      
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比（对齐颧点，鼻子未对齐；本组左腮帮子稍大、鬓角处稍窄）；  
      （下图左三）左一叠加左二：  
      （下图左四）熏front;  
      （下图左四）放大对比麻衣(蓝色)、熏(红色)；29/ang, 31/latgo的差异表明本组腮帮子更平滑(更不棱角分明)；  
        ![](img/face_front_mai_front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg)  
        ![](img2/face_front_mai_front_vs_kaoru_front.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**千夏12_141_0**(红色mark点)的对比（对齐颧点，鼻子未对齐；**脸颊类似**；本组31/32点（右脸latgo/左脸latgo）偏上，说明腮帮子更不凸、更有棱角）；  
      （下图左三）左一叠加左二；  
      （下图左四）千夏12_141_0；  
        ![](img/face_front_mai_front.jpg) 
        ![](img2/face_front_cmp_aps__mai-front_vs_chinatsu-12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__mai-front-p_vs_chinatsu-12_141_0.jpg) 
        ![](img/face_front_chinatsu_12_141_0.jpg) 


### 古屋公弘(Kimihiro)-正面  {#古屋公弘(Kimihiro)-正面}  
  
- 09_051_6:  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点), 左右脸不对称：  
      （下图左三）示意图。因耳朵被遮挡，故以外眼角为准。鼻子稍微偏离中线，**不是严格正面**（下面的对比发现，其8/鼻下点、嘴偏下，说明其低头），所以本组对比说服力不大。   
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_rl-cmp.jpg) 
        ![](img/face_asymmetry_kimihiro-09_051_6_with_markline.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（对齐颧点，鼻子未对齐；**本组脸颊和雅彦类似**）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
      （下图左六）放大对比公弘(蓝色)、雅彦(红色)；右脸27/zy表明本组脸颊稍宽；右脸31/latgo表明本组瘦；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg)  
        ![](img2/face_front_kimihiro_09_051_6_vs_masahiko_group2.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**熏front**(红色mark点)的对比（本组右腮帮子平）；  
      （下图左三）左一叠加左二：  
      （下图左四）熏front；  
      （下图左五）熏front渐变对比；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_kaoru-front.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_kaoru-front.jpg) 
        ![](img/face_front_kaoru_front.jpg) 
        ![](img/face_front_kaoru_front_vs_kimihiro_09_051_6.gif)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**早纪front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）早纪front;  
      （下图左五）早纪front渐变对比;  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_saki-front.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_saki-front.jpg) 
        ![](img/face_front_saki_front.jpg) 
        ![](img/face_front_kimihiro_09_051_6_vs_saki_front.gif)    

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美group1；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**浩美right**(红色mark点)的对比（本组更不显棱角）；  
      （下图左三）左一叠加左二：  
      （下图左四）浩美right；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_hiromi-right.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_hiromi-right.jpg) 
        ![](img/face_front_hiromi_right.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**江岛wide**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）江岛wide；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_ejima-wide.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_ejima-wide.jpg) 
        ![](img/face_front_ejima_wide.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**顺子front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）顺子front；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_yoriko-front.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_yoriko-front.jpg) 
        ![](img/face_front_yoriko_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**千夏12_141_0**(红色mark点)的对比（本组右脸颊稍宽）；  
      （下图左三）左一叠加左二：  
      （下图左四）千夏12_141_0；  
        ![](img/face_front_kimihiro_09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6_vs_chinatsu-12_141_0.jpg) 
        ![](img2/face_front_cmp_aps__kimihiro-09_051_6-p_vs_chinatsu-12_141_0.jpg) 
        ![](img/face_front_chinatsu_12_141_0.jpg) 

    - 古屋朋美09_047_1对比古屋公弘09_051_6脸型类似，参见“古屋朋美”一节。  


### 古屋朋美(Tomomi)-正面  {#古屋朋美(Tomomi)-正面}  
  
- 09_047_1:  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比（嘴宽同紫苑）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**古屋公弘09_051_6**(红色mark点)的对比（**血缘关系**）（古屋公弘象雅彦、古屋朋美象雅美，所以两人必然相貌相似；所以把二人设定为兄妹是符合逻辑的）；  
      （下图左三）左一叠加左二：  
      （下图左四）古屋公弘09_051_6;  
      （下图左四）放大对比朋美(蓝色)、公弘(红色)；29/ang，31/latgo，33/chk，35/al-chin的差异表明公弘右腮帮子相对平；  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_kimihiro-09_051_6.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_kimihiro-09_051_6.jpg) 
        ![](img/face_front_kimihiro_09_051_6.jpg)  
        ![](img2/face_front_tomomi_09_047_1_vs_kimihiro_09_051_6.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**雅美group0**(红色mark点)的对比（本组嘴窄）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美group0;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_masami-group0.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_masami-group0.jpg) 
        ![](img/face_front_masami_group0_vol03.jpg)  

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**雅美group1**(红色mark点)的对比（本组嘴窄）；  
      （下图左三）左一叠加左二：  
      （下图左四）雅美group1;  
      （下图左五）雅美group1渐变对比;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_masami-group1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_masami-group1.jpg) 
        ![](img/face_front_masami_group1_vol03_vol07.jpg) 
        ![](img/face_front_masami_group1_vs_tomomi_09_047_1.gif) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**叶子front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）叶子front；  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_yoko-front.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_yoko-front.jpg) 
        ![](img/face_front_yoko_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**真琴front**(红色mark点)的对比（脸颊处的红色mark点和黑色mark点间隔出现，说明什么？）；  
      （下图左三）左一叠加左二：  
      （下图左四）真琴front;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_makoto-front.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_makoto-front.jpg) 
        ![](img/face_front_makoto_front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**顺子front**(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）顺子front;  
        ![](img/face_front_tomomi_09_047_1.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1_vs_yoriko-front.jpg) 
        ![](img2/face_front_cmp_aps__tomomi-09_047_1-p_vs_yoriko-front.jpg) 
        ![](img/face_front_yoriko_front.jpg) 


### 八不(Hanzu)-正面  {#八不(Hanzu)-正面}  
  
- 02_183_7:  
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）左右脸对比(红色表示原mark点，绿色表示水平翻转mark点)：  
        ![](img/face_front_hanzu_02_183_7.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7_rl-cmp.jpg) 
    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、雅彦Group2(蓝色mark点)、紫苑Group2(红色mark点)的对比；  
      （下图左三）左一叠加左二：  
      （下图左四）雅彦Group2；  
      （下图左五）紫苑Group2；  
        ![](img/face_front_hanzu_02_183_7.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7-p_vs_masahiko-group2_vs_shion-group2.jpg) 
        ![](img/face_front_masahiko_group2_front.jpg) 
        ![](img/face_front_shion_group2-front.jpg) 

    - （下图左一）本组图片叠加后的效果;  
      （下图左二）本组(黑色mark点)、**辰巳left**(红色mark点)的对比（本组脸颊窄）；  
      （下图左三）左一叠加左二：  
      （下图左四）辰巳left;  
      （下图左五）辰巳left渐变对比;  
      （下图左六）放大对比八不(蓝色)、辰巳(红色)；本组29/ang、31/latgo重叠；右脸29/ang、31/latgo表明本组脸颊窄；  
        ![](img/face_front_hanzu_02_183_7.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7_vs_tatsumi-left.jpg) 
        ![](img2/face_front_cmp_aps__hanzu-02_183_7-p_vs_tatsumi-left.jpg) 
        ![](img/face_front_tatsumi_left.jpg) 
        ![](img/face_front_hanzu_02_183_7_vs_tatsumi_left.gif)  
        ![](img2/face_front_hanzu_02_183_7_vs_tatsumi_left.jpg)  


### 哲(Te)-正面  {#哲(Te)-正面}  
没有正面图。   


### 如何对比更多角色  
下载[工程文件](./project2/face_front_cmp_aps.xcf.7z)，解压缩后，用软件Gimp(版本2.10)打开。  
 

-----------------
## 总结  {#总结}  

这些特点或规律应该可算作FC的绘画风格的一部分。  

### 正面左右脸颊不对称  {#正面左右脸颊不对称}  
FC里角色左右脸常常不对称，有“左右脸差异小”的画法，也有“左右脸差异大”的画法。  

- 左右脸差异小。如：雅彦group2。以雅彦group2的脸颊为例, 下图依次是: 雅彦group2，脸颊曲线(红)，水平反转后的脸颊曲线(绿)，两条曲线的对比(左右脸颊对比)。  
    ![](./img/face_front_masahiko_group2_front.jpg) 
    ![](./img/face_asymmetry_masahiko-group2-front_original.jpg) 
    ![](./img/face_asymmetry_masahiko-group2-front_flip.jpg) 
    ![](./img/face_asymmetry_masahiko-group2_compare.jpg) 
- 左右脸差异大。如：空front、辰巳front、葵front。首先，在以上这些角色的组里已经说明它们均是严格的正面像。其次，这些角色的组的左右脸颊很类似，呈现出一定规律。对比发现该规律是:**左脸(图片右侧)腮帮子较凸出**。以若苗空front的脸颊为例, 下图依次是: 若苗空front，脸颊曲线(红)，水平反转后的脸颊曲线(绿)，两条曲线的对比(左右脸颊对比)，左右脸mark点对比(红色表示原mark点，绿色表示水平翻转mark点)。  
    ![](./img/face_front_sora_front.jpg) 
    ![](./img/face_asymmetry_sora-front_original.jpg) 
    ![](./img/face_asymmetry_sora-front_flipped.jpg) 
    ![](./img/face_asymmetry_sora-front_compare.jpg) 
    ![](img2/face_front_cmp_aps__sora-front_rl-cmp.jpg) 
  此外，下图叠加空front(红)、辰巳front(绿)、葵front(蓝)的mark点。可以看到：相应的mark点聚集在一起，说明他们的脸的特征相似：  
    ![](./img2/face_asymmetry_mark_sora_tatsumi_aoi.jpg) 

### 其他归纳结果  
- 年龄小的角色，其下巴稍短(或脸颊稍宽)。这符合面部素描规律。例如，[紫苑group0](#紫苑(Shion)-正面)；[雅彦group0](#雅彦(Masahiko)-正面)。  
- 青少年的相貌随年龄而变化。例如：  
    - 紫苑group0、group2、group3依次是她在作品前中后期的正面脸颊的画法。  
    - 雅彦group0、group2依次是她在作品前后期的正面脸颊的画法。  
    - (作品前中后期)浅冈叶子的正面脸颊画法稳定。  
- 成年人比青少年的脸颊(腮帮子)处棱角更分明。  例如，紫苑group2里与若苗空front、若苗紫front的比较；雅美group1 和若苗紫front的比较；  
- 盐谷psudo-front多处五官是女性画法。  
- 若苗空,若苗紫脸颊相似。（夫妻相?）  
- 即便是大块头，也不一样。相比和子，宪司下巴更大。这符合设定：宪司开店需要经常装货卸货，比起日常工作是画漫画的和子，宪司有更多体力活儿。  
- 31/32点（右脸latgo/左脸latgo）越偏上，越显得脸颊(腮帮子)棱角分明、男性化。  
- 成年男性嘴宽 > 青少年男性嘴宽 > 女性嘴宽。例如，部分角色嘴宽的程度：  
    - 葵 > 辰巳 > 若苗空 = 横田进；   
    - 松下(抿嘴?) > 空；  
    - 若苗紫 = 雅美 = 雅彦group2 = 叶子 = 叶子母 = 顺子；  
    - 紫苑 = 盐谷 = 熏 = 早纪front = 古屋朋美；  
    - 千夏 > 麻衣；  

- 不同角色的脸颊/腮帮子有明显差异；
    - 腮帮子更有棱角的程度（31/32位置高）：
        - 若苗紫front > 雅美Group1
        - 若苗空front/若苗紫front > 叶子 > 盐谷psudo-front
        - 熏 > 浩美right > 叶子母
        - 早纪 > 浩美right > 文哉
        - 盐谷right-top > 早纪top-short
        - 横田进 > 仁科
        - 横田进 > 葵
        - 横田进 > 文哉
        - 横田进 > 千夏
        - 早纪 > 千夏

    - 腮帮子凸的程度（31/32偏外侧/凸）：
        - 若苗空front/若苗紫front > 紫苑Group2 > 雅彦Group2 > 浩美front/right 
        - 叶子 > 浩美right 
        - 叶子 > 叶子母 
        - 叶子 > 文哉
        - 雅彦 > 早纪front
        - 古屋朋美 > 雅美 > 早纪
        - 古屋朋美 > 顺子 >
        - 宪司 > 和子 >
        - 顺子 > 熏
        - 顺子 > 早纪
        - 雅彦/紫苑 > 文哉
        - 雅彦/紫苑 > 千夏
        - 导演 > 绫子

- FC部分角色的脸宽的程度：  
    - 美菜 > 里沙 > 紫苑/雅彦 > 江岛wide
    - 紫苑/雅彦 > 摄像师
    - 葵 > 横田进
    - 葵 > 熏
    - 葵 > 江岛wide
    - 空 > 江岛wide
    - 空 > 横田进
    - 紫 > 早纪
    - 松下 > 浩美 > 早纪
    - 松下 > 横田进
    - 顺子 > 熏
    - 顺子 > 千夏
    - 紫苑/雅彦 > 典子 > 横田进 > 仁科
    - 文载 > 江岛
    - 葵 > 文哉

- 多数男性下巴颏圆、多数女性下巴颏尖。但mark点反映不出该特点。FC部分角色下巴颏圆的程度：
    - 岩谷psudo-front > 叶子 >  
    - 辰巳～若苗空front > 若苗紫front > 叶子  
    - 雅美group1 > 叶子  
    - 浩美～熏  
    - 里沙 > 紫苑/雅彦  
    - 横田进 > 千夏  

### 一些猜测  {#一些猜测}  
- 因为辰巳front的脸颊类似若苗空front的脸颊、浅葱的脸颊类似若苗紫的脸颊、麻衣的脸颊类似紫苑的脸颊。我猜测，作者可能根据若苗空的相貌去设计了辰巳的相貌、根据若苗紫的相貌去设计了浅葱的相貌、根据紫苑的相貌去设计了麻衣的相貌。  

- 两个角色有血缘关系，ta们面部有相似之处。例如：    
    - 紫苑group2和父母的右脸颊一致，左脸颊和若苗空一致；
    - 雅彦group2和若苗紫front右脸一致，脸颊有不少相似之处；
    - 雅美group1和若苗紫front脸颊类似；  
    - 熏front和早纪front脸颊类似；  
由此，我猜测，作者在设计这些角色相貌时有考虑"血缘关系来带的相貌相似"。  

- 作品前中后期，浅冈叶子的正面脸颊画法稳定，这说明其相貌变化不大。或许可以推测其身体发育早。  
  在[^6]里有如下推测： 03_053, 03_054, 03_057, 这几张图，怎么看都觉得叶子的身高比雅美(雅彦)高一点点。两人都是平底鞋。后续11_085_0进一步证实了这一点。    
  ![](../fc_details/03_053_054_057__.jpg) 
  ![](../fc_details/11_085_0__.jpg)  
  "叶子身体高"、"叶子身体发育早"这两个推测不矛盾。  

- 一方面，因为这些mark点很有效。以右脸为例，下颌骨上"29, 31, 33, 35"这4个点刻画了脸颊的外轮廓；不同FC人物的面部特征在大致这些点处有明显的不同。所以我推测：**对于正面美貌的理解，作者北条司和美容整形外科(Aesthetic Plastic Surgery)领域可能有很多共识**。另一方面，面部整形外科(Facial Plastic Surgery)于1960年代开始研究美貌[^4]，心理学于1970年代开始研究美貌[^4]。论文[^1]发表于2017年，本文图1([^1]的图3)里的"23/24，25/26，29/30，31/32，33/34，35/36"点是其作者添加的，前人没有定义过这些点。综合这两方面的信息，我推测：**如果论文[^1]定义的正面mark点代表当时美容整形外科领域最新的研究成果，那么北条司对于正面美貌的理解可能领先该领域约20年**。  


## 后续  

- FC里的角色面部很少画鼻翼，所以，鼻翼处的mark点(6/7)可忽略。后续，或者可以把鼻翼处的mark点(6/7)移至鼻孔处，从而比较鼻孔的大小、远近。  
- 下巴颏处可以添加mark，描述下巴的尖/圆程度。  
- [^12]（图46-11）里将正面下巴颏的形状(Frontal chin shape)分为8类。或许有助于分析FC角色的脸颊。  
- 分析对比老人、小孩几个角色：爷爷，奶奶，章子。  
- 文中标记的疑问有待回答。  
- 以上列出的“部分角色嘴宽的程度比较“、”不同角色的脸颊/腮帮子有明显差异“、”部分角色的脸宽的程度比较“、”部分角色下巴颏圆的程度比较”中分别有多条序列，这说明比较的案例既有冗余、也有不足。可以比较更多的角色，把多条序列合并为一条序列。  
- 软件STASM[^7]在下巴颏处有3个mark点([^8])。可以尝试把STASM应用到FC的角色正面图上，看看会有什么结果。  
- 近几十年来，与人脸漂亮(facial attractiveness)相关的话题被多个学科研究，包括: 发展心理学、进化生物学、社会学、认知科学和神经科学[^11]。以下是一些术语、观点，或许有助于FC角色相貌分析？  
    - 测量面部特征的位置关系被称为"面部分析（Facial Analysis）"[^9].  
    - ... shape averageness, dimorphism, and skin texture symmetry are useful features capable of relatively accurate predictions, while shape symmetry is uninformative.... Increased femininity is not necessarily more attractive in female faces, but increased masculinity seems to reliably lead to lower ratings. Conversely, the effect of shape symmetry seems almost entirely unimportant, and its dominance in the literature can possibly be attributed to the methodologies used to assess it. However, texture symmetry is somewhat important, and this is borne out by dermatological studies that show smooth, even skin texture is attractive[^10]. （译：...脸形的平均、二形性和皮肤纹理的对称性是有用的特征，能够进行相对准确的预测，而脸形的对称性是无信息的...在女性面孔中，增加女性气质不一定更有吸引力，但增加男性气质似乎可靠地导致较低的评分(译注：指"不漂亮")。相反，脸形对称的影响似乎几乎完全不重要，它在文献中的主导地位可能要归因于用来评估它的方法论。然而，纹理对称性在某种程度上是很重要的，这一点在皮肤学研究中得到了证实，这些研究表明光滑、均匀的皮肤纹理是有吸引力的）  
    - Much work in this field has also been devoted to assessing the covariation of the perceived beauty of a face with facial traits that are believed to signal good phenotypic condition, mainly: facial symmetry, averageness and secondary sexual traits. After decades of intense research, the role played by these traits is known to be limited: facial beauty seems to be more complex than symmetry, averageness and secondary sexual traits[^11].（译：这一领域的许多工作都致力于评估脸部(能被感知)的美与(被认为是良好表型条件信号的)脸部特征的关系，主要是：脸部对称性、平均性和第二性征。经过几十年的深入研究，人们知道这些特征所起的作用是有限的：脸部的美似乎比对称性、平均性和第二性征更复杂）  


## 参考资料  
[0]. [FC的画风-面部](./readme.md)  
[1]. [Differences between Caucasian and Asian attractive faces, Seung Chul Rhee, 2017.](https://www.researchgate.net/publication/318548459)  
[2]. [国家标准--人体测量术语(GB 3975-83)]()  
[3]. [Photogrammetric Analysis of Attractiveness in Indian Faces, Shveta Duggal and etc, 2016.](https://www.researchgate.net/publication/299371197)  
[4]. [History and Current Concepts in the Analysis of Facial Attractiveness, Mounir Bashour, 2006. (DOI: 10.1097/01.prs.0000233051.61512.65)](10.1097/01.prs.0000233051.61512.65)  
[5]. [FC的画风-面部(侧面)](./fc_face_side.md)  
[6]. [FC的一些细节](../fc_details/readme.md)  
[7]. [STASM](http://www.milbo.users.sonic.net/stasm/)  
[8]. [STASM User Manual - Figure 1](http://www.milbo.org/stasm-files/stasm4.pdf)  
[9]. [Facial Analysis, Dimitrije E. Panfilov. (in Aesthetic Surgery of the Facial Mosaic, 2007. ISBN:3540331603)](https://link.springer.com/book/10.1007/978-3-540-33162-9)  
[10]. [Biological Bases of Beauty Revisited: The Effect of Symmetry, Averageness, and Sexual Dimorphism on Female Facial Attractiveness. Alex L. Jones, Bastian Jaeger, 2019.](https://doi.org/10.3390/sym11020279)  
[11]. [Subjectivity and complexity of facial attractiveness. Miguel Ibáñez-Berganza, Ambra Amico & Vittorio Loreto, 2019.](https://doi.org/10.1038/s41598-019-44655-9)  
[12]. [Aesthetic Plastic Surgery in Asians Principles and Techniques. Lee L.Q. Pu, 2015](https://doi.org/10.1093/asj/sjv249)  
[13]. Form of the Head and Neck, 2021, Uldis Zarins.  

[^0]: <./readme.md> "FC的画风-面部"  
[^1]: <https://www.researchgate.net/publication/318548459> "Differences between Caucasian and Asian attractive faces, Seung Chul Rhee, 2017."  
[^2]: <https://> "国家标准--人体测量术语(GB 3975-83)"  
[^3]: <https://www.researchgate.net/publication/299371197> "Photogrammetric Analysis of Attractiveness in Indian Faces, Shveta Duggal and etc, 2016."  
[^4]: <10.1097/01.prs.0000233051.61512.65> "History and Current Concepts in the Analysis of Facial Attractiveness, Mounir Bashour, 2006. (DOI: 10.1097/01.prs.0000233051.61512.65)"  
[^5]: <./fc_face_side.md> "FC的画风-面部(侧面)"  
[^6]: <../fc_details/readme.md> "FC的一些细节"  
[^7]: <http://www.milbo.users.sonic.net/stasm/> "STASM"  
[^8]: <http://www.milbo.org/stasm-files/stasm4.pdf> "STASM User Manual - Figure 1"   
[^9]: <https://link.springer.com/book/10.1007/978-3-540-33162-9> "Facial Analysis, Dimitrije E. Panfilov. (in Aesthetic Surgery of the Facial Mosaic, 2007. ISBN:3540331603)"  
[^10]: <https://doi.org/10.3390/sym11020279> "Biological Bases of Beauty Revisited: The Effect of Symmetry, Averageness, and Sexual Dimorphism on Female Facial Attractiveness. Alex L. Jones, Bastian Jaeger, 2019."  
[^11]: <https://doi.org/10.1038/s41598-019-44655-9> "Subjectivity and complexity of facial attractiveness. Miguel Ibáñez-Berganza, Ambra Amico & Vittorio Loreto, 2019."  
[^12]: <https://doi.org/10.1093/asj/sjv249> "Aesthetic Plastic Surgery in Asians Principles and Techniques. Lee L.Q. Pu, 2015"  
[^13]: Form of the Head and Neck, 2021, Uldis Zarins.  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处



