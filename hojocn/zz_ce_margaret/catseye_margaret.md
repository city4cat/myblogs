# Margaret Atwood的小说《Cat's Eye》 ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96872))   

《使女的故事》(The Handmaid's Tale)的作者Margaret Atwood曾在1988年9月[2]出版过小说《Cat's Eye》。  

- Amazon.com上的内容简介[1]：  
Cat’s Eye is the story of Elaine Risley, a controversial painter who returns to Toronto, the city of her youth, for a retrospective of her art. Engulfed by vivid images of the past, she reminisces about a trio of girls who initiated her into the the fierce politics of childhood and its secret world of friendship, longing, and betrayal. Elaine must come to terms with her own identity as a daughter, a lover, an artist, and a woman—but above all she must seek release form her haunting memories. Disturbing, humorous, and compassionate—and a finalist for the Booker Prize—Cat’s Eye is a breathtaking novel of a woman grappling with the tangled knot of her life.  
(《Cat's Eye》讲述的是有争议的画家Elaine Risley的故事。因自己艺术作品的回顾展，她回到了年轻时曾居住的城市多伦多。 过去的情景历历在目，令她回想起三个女孩。是她们将她带入了童年的勾心斗角，也为她开启了充满友谊、渴望和背叛的秘密世界。Elaine必须兼顾女儿、情人、艺术家和女人的多重身份；但最重要的是，她必须从挥之不去的记忆中寻求解脱。本书入围Booker奖的决赛名单，是一部令人叹为观止的小说，它讲述了一个女人面对人生羁绊的痛苦和挣扎，作品充满幽默且让人心生怜悯，读完让人久久不能平静。)  

- 该小说曾多次出版。不同版本的封面[3]：  
![](img/covers0.jpg) ![](img/covers1.jpg)  ![](img/covers2.jpg) 

- 针对该小说的其他(研究)著作：  
    1. [A Study Guide for Margaret Atwood's "Cat's Eye", Cengage Learning Gale](https://www.amazon.com/Study-Guide-Margaret-Atwoods-Cats/dp/1375377906/)  
    2. [Seeing in the Dark: Margaret Atwood's Cat's Eye](https://www.amazon.com/Seeing-Dark-Margaret-Atwoods-Canadian/dp/1550223127/)  
    3. [Cat's Eye by Margaret Atwood (Book Analysis): Detailed Summary, Analysis and Reading Guide](https://www.amazon.com/Cats-Margaret-Atwood-Book-Analysis/dp/2808019866)  
    4. [Gothic glimpses in Margaret Atwood's "Cat's Eye" or representations of art and media and mysterious twin ship ](https://www.amazon.com/glimpses-Margaret-Atwoods-representations-mysterious/dp/3638665550)  
    5. [Margaret Atwood: Works and Impact](https://www.amazon.com/Margaret-Atwood-European-American-Literature/dp/1571132694)  
    6. [Summary & Study Guide Cat’s Eye by Margaret Atwood](https://www.amazon.com/Summary-Study-Guide-Margaret-Atwood-ebook/dp/B007HCKX2Y/)  
    7. [Margaret Atwood's Cat's Eye: Insight Text Guide](https://www.amazon.com/Margaret-Atwoods-Cats-Eye-Insight/dp/145966227X/)  


- 从书名、首版时间(1988年)[2]、内容简介里的某些元素(油画/绘画)[1]，我猜，该小说可能和北条司的《Cat's Eye》有某种联系。  


- 此外，关于Cat's Eye:  
    - 首先，有一种宝石名为"猫眼(Cat's Eye)"[4]:  
    ![](img/cat-cymophane.jpg)  
    - 其次，一些电影、小说的名字是"Cat's Eye"，或者类似"Cat's Eye"；这些作品的情节或与猫眼宝石有关[5][6][7]、或有悬疑/神秘/侦探、惊悚的特点或情节[8][9][10][11][12][13][14]。北条司的作品《Cat's Eye》里三姐妹为自己取名Cat's Eye，可能是取"身手敏捷、来去无踪、给人留下神秘感"之意。  


**参考链接**:  
1. [Cat's Eye, Margaret Atwood - Amazon.com](https://www.amazon.com/Cats-Eye-Margaret-Atwood/dp/0385491026)  
2. [Cat's Eye, Margaret Atwood - Goodreads](https://www.goodreads.com/book/show/51019.Cat_s_Eye)    
3. [Cat's Eye, Margaret Atwood - Moly.hu](https://moly.hu/konyvek/margaret-atwood-cats-eye)  
4. [Cat's-eye gemstone - Britannica](https://www.britannica.com/topic/cats-eye-gemstone)  
5. [The Cat's Eye - Amazon.com](https://www.amazon.com/Cats-Eye-R-Austin-Freeman/dp/B08T6BTJ7J/)  
6. [Krait's Redemption (The Cat's Eye Chronicles Book 5) - Amazon.com](https://www.amazon.com/Kraits-Redemption-Cats-Chronicles-Book-ebook/dp/B073NQCDGY)  
7. [The Cheshire Cat's Eye (A Sharon McCone Mystery Book 3) - Amazon.com](https://www.amazon.com/Cheshire-Cats-Sharon-McCone-Mystery-ebook/dp/B006JE394U)  
8. [Cat's Eye - Amazon.com](https://www.amazon.com/Cats-Eye-William-W-Johnstone-ebook/dp/B01BAZICTY/)  
9. [Seven Deaths In The Cat's Eye - Amazon.com](https://www.amazon.com/Seven-Deaths-Cats-Jane-Birkin/dp/B077ZH8WSY)  
10. [Stephen King's Cat's Eye - Amazon.com](https://www.amazon.com/Stephen-Kings-Cats-Drew-Barrymore/dp/B0199I8MJE)  
11. [The Supernatural Adventures of Ryan Bumble: The Cat's Eye - Amazon.com](https://www.amazon.com/Supernatural-Adventures-Ryan-Bumble-Cats/dp/B08JDXBTC3)  
12. [Isle of the Dead, Eye of Cat - Amazon.com](https://www.amazon.com/Isle-Dead-Eye-Roger-Zelazny-ebook/dp/B00K7S0J24/)  
13. [The Eyes of the Cat: The Yellow Edition - Amazon.com](https://www.amazon.com/Eyes-Cat-Yellow-Alejandro-Jodorowsky/dp/159465042X)  
14. [Cat's Eyes, 21st Century Vampire Book 2 - Amazon.com](https://www.amazon.com/Cats-Eyes-Catherine-Alexander-Blair-novel-ebook/dp/B07PGW8RWJ)  







--------------------------------------------------

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处

