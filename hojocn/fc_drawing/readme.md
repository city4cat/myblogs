
"FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭 / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡) 


- **说明：**  
    - 类似"03_143"的字符串表示该漫画里的"卷-页"编号。xx_yyy表示第xx卷、第yyy页。例如：03_143表示第3卷143页。  
    - 类似"03_143_7"的字符串表示该漫画里的"卷-页-分镜/分格(panel)"编号。xx_yyy_z表示第xx卷、第yyy页、第z分镜。z从0开始计数。例如：03_143_7表示第3卷143页镜头7;如果第1卷第3页有两个镜头，那么这来个分格/分镜编号分别为：01_003_0, 01_003_1。  



**FC的绘画特点（汇总）**  
---

以下若干文章给出了FC在绘画方面的一些特点。这些特点或多或少地也存在于北条司的其他作品中，所以可将其认为是北条司的绘画特点。    

- [FC的绘画-对话框](../fc_dialogue/readme.md)  
- [FC的画风-面部-年龄变化](../fc_drawing_style__aging/readme.md)  
- [FC的画风-人物身材比例](../fc_drawing_style__body_proportion/readme.md)  
- [FC的画风-衣服/布料/褶皱](../fc_drawing_style__cloth/readme.md)  
- [FC的绘画-面部](../fc_drawing_style__face/readme.md)  
- [FC的绘画-面部2](../fc_drawing_style__face/readme2.md)  
- [FC的画风-面部(侧面)](../fc_drawing_style__face/fc_face_side.md)  
- [FC的画风-面部-眉](../fc_drawing_style__face_brow/readme.md)  
- [FC的画风-面部-眉2](../fc_drawing_style__face_brow2/readme.md)  
- [FC的画风-面部-五官组合(正面)](../fc_drawing_style__face_composition/readme.md)  
- [FC的画风-面部-耳(侧面)](../fc_drawing_style__face_ear_side/readme.md)  
- [FC的画风-面部-眼部](../fc_drawing_style__face_eye/readme.md)  
- [FC的画风-面部-眼睛2](../fc_drawing_style__face_eye2/readme.md)  
- [FC的画风-面部-眼睛(侧面)](../fc_drawing_style__face_eye_side/readme.md)  
- [FC的画风-面部-嘴](../fc_drawing_style__face_mouth/readme.md)  
- [FC的画风-面部-嘴(正面)](../fc_drawing_style__face_mouth2/readme.md)  
- [FC的画风-面部-鼻](../fc_drawing_style__face_nose/readme.md)  
- [FC的画风-面部-鼻(正面)](../fc_drawing_style__face_nose2/readme.md)  
- [FC的画风-面部比例](../fc_drawing_style__face_proportion/readme.md)  
- [FC的画风-面部2(侧面)](../fc_drawing_style__face_side2/readme.md)  
- [FC的画风-特效](../fc_drawing_style__fx/readme.md)  
- [FC的画风-动作/姿态](../fc_drawing_style__gesture/readme.md)  
- [FC的画风-头发](../fc_drawing_style__hair/readme.md)  
- [FC的画风-面部-头发2(正面)](../fc_drawing_style__hair2/readme.md)  
- [FC的画风-手](../fc_drawing_style__hand/readme.md)  
- [FC的画风-腿](../fc_drawing_style__leg/readme.md)  
- [FC的画风-化妆](../fc_drawing_style__make-up/readme.md)  
- [FC的画风-颈部](../fc_drawing_style__neck/readme.md)  
- [FC的绘画-透视](../fc_drawing_style__perspective/readme.md) (03_180_1 v.s. 03_155_5  in eye_front_yoriko.xcf)    
- [FC的绘画-写实风格](../fc_drawing_style__realistic/readme.md)  
- [FC的画风-场景](../fc_drawing_style__scene/readme.md)  
- [FC的画风-着色](../fc_drawing_style__shading/readme.md)  
- [FC的服饰](../fc_dress/readme.md)  

## 分类  

笔者认为可将这些绘画特点分为几类：  

- 形状(Geometry)。物体的形状由线条勾勒出。比如，画面里人物面部五官的比例符合素描比例；画面里人物身体的比例符合素描比例；画面里角色、物体的结构准确；画面里物体(比如，车、衣服褶皱)的细节丰富。  
    - 透视(perspective)。例如，1点透视、2点透视、3点透视，这些绘画方法会使画面更逼真。    
- 着色(Shading)。对于黑白漫画，着色指物体表面的明暗度[1]。比如，面部不同部位因凹凸不平而有不同的明暗度，这些明暗度符合实物。  
    - 光照(Lighting)。包括：高光、阴影，等。  
    - 纹理(Texture)。纹理指物体表面的花纹。比如，服饰上有不同的花纹。  
- 运动(Motion)。经常用多种方式表现物体或角色的动感，如稀疏的线条特效、密集的排线、密集的交叉排线。  

例如下图的眼仁和头发，只有形状，没有着色:  
![](img/07_082_6__crop0.jpg)   
  




**参考资料**：  
1. [Shading - Wikipedia](https://en.m.wikipedia.org/wiki/Shading)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)

![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)

转载请注明出处
