'''
sudo apt install python3-pip
python3 -m pip install --user pipx
pipx install --prefix=/media/z/Traveller/mypippyprefix matplotlib

python3 ./test_raps.py

'''

DIR_mypippyprefix='/media/z/Traveller/mypippyprefix'
import sys
sys.path.insert(0, '%s/lib/python3.8/site-packages'%(DIR_mypippyprefix))
print('PATH=', sys.path)

import os
import time

import matplotlib.pyplot as plt
import numpy as np
import collections
import skimage
from skimage import io

import util_logging
import util_common
import raps



def add_list(old, new):
    short_list = []
    long_list = []
    
    if len(old) > len(new):  
        long_list = old
        short_list = new
    else:
        long_list = new
        short_list = old
        
    for i in range(len(short_list)):
        long_list[i] += short_list[i]
        
    return long_list
        
def compute_average(sum_list, count_list):  
    assert len(sum_list) == len(count_list)
    
    avg_list = [0.0]*len(count_list)
    for i in range(len(count_list)):
        if count_list[i] == 0:
            avg_list[i] = 0.0
        else:
            avg_list[i] = sum_list[i] / count_list[i]
            
    return avg_list
    
    
def max_leg(image_filepath_list):
    leg_list = []
    
    for image_filepath in image_filepath_list:
        image = io.imread(image_filepath)   
        leg_length = np.hypot(image.shape[0], image.shape[1]) #sqrt(x1**2 + x2**2)
        leg_list.append(leg_length)
        
    return max(leg_list)
        

def test4(image_filepath_list, image_output_dir, use_center, matplot_x_max):
    print('test4()...')  
    #print('image=', image)  
    
    
    sum_pixel_sum_val = []    
    sum_pixel_count = []

    image_count = len(image_filepath_list)

    for i in range(image_count):
        idx_str = '%d/%d'%(i, image_count-1)    
        
        image_filepath_i = image_filepath_list[i]
        image_base_name_i = os.path.basename(image_filepath_i) 
        image_filepath_o = image_output_dir+'/'+image_base_name_i+'_matplot.jpg'
        
        print('image(%s): %s'%(idx_str, image_base_name_i) )
        pixel_sum_val, pixel_count = raps.algorithm4(image_filepath_i, use_center)
        avg_one_image = compute_average(pixel_sum_val, pixel_count)

        sum_pixel_sum_val = add_list(sum_pixel_sum_val, pixel_sum_val)
        sum_pixel_count = add_list(sum_pixel_count, pixel_count)
                
        avg_all_images = compute_average(sum_pixel_sum_val, sum_pixel_count)
    
        X = [i for i in range(0, len(pixel_count))]
        Y = avg_one_image
        fig, axes = plt.subplots(1, 2, sharex=True, sharey=True)
        axes[0].plot(X, Y)
        axes[0].set_xlim(0, matplot_x_max)
        axes[0].set_ylim(0, 260)
        axes[0].set_ylabel('Averaged pixel value of (%s) image: "%s"'%(idx_str, image_base_name_i))
        X = [i for i in range(0, len(sum_pixel_count))]
        Y = avg_all_images
        axes[1].plot(X, Y)
        axes[1].set_xlim(0, matplot_x_max)
        axes[1].set_ylim(0, 260)
        axes[1].set_ylabel('Averaged pixel value of (0 ~ %s) images'%(i))
        plt.savefig(image_filepath_o)
        
    
    #print('sum_pixel_count=', sum_pixel_count)
    #print('sum_pixel_sum_val=', sum_pixel_sum_val)
    
    avg_all_images = compute_average(sum_pixel_sum_val, sum_pixel_count)
    #print('avg_all_images=', avg_all_images)

    
    X = [i for i in range(0, len(sum_pixel_count))]
    Y = avg_all_images
    #print('X=', X)
    #print('Y=', Y) 
    '''
    plt.plot(X,Y)
    plt.xlabel('Radius from (0,0)')
    plt.ylabel('Averaged pixel value')
    plt.savefig('./test.jpg')
    plt.show(block=True)
    '''
    '''
    fig, axes = plt.subplots(1, 2, sharex=True, sharey=True)
    axes[0].plot(X, Y)
    plt.savefig('./all.jpg')
    plt.show(block=True)
    '''
    '''
    plt.ion()
    
    plt.figure(1)
    plt.plot(X,Y)
    plt.draw()
    #time.sleep(5)
    #plt.close()
    
    #plt.figure(2)
    plt.plot(X,np.cos(X))
    plt.draw()
    time.sleep(5)
    '''  
    
    

if '__main__' == __name__:
    import util_dir_file_filter
    
    print('Start >>>')

    os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'
    util_logging.setup()

    username = os.path.expanduser('~').split('/')[-1]

    root_dir='/media/%s/Traveller/usb/fc_statistic'%(username)
    #root_dir='/media/%s/home/tmp'%(username),
    assert os.path.isdir(root_dir), root_dir
    
    # ------------------------------------
    '''  
    image_input_dir = '/img2'
    exclude_prefix_list = [
    '01_047_0',
    '01_161_2',
    ]
    '''
    
    image_input_dir = '/img'
    exclude_prefix_list = [
    'image0',
    'image1',
    'image2',
    'image3',
    'image4',
    'image5',
    'image6',
    'ref',
    'simple2'
    ]

    image_output_dir = '/img_output'
    # ------------------------------------

    util_logging.info('1 ----------------')
    filepath_list = util_dir_file_filter.test_gather_files(
    root_dir=root_dir+image_input_dir,
    include_filters=['*.jpg', '*.png'],
    exclude_filters=[]
    )
    filepath_list.sort()
    util_dir_file_filter.print_list(filepath_list, 'filepath_list')

    util_logging.info('2 ----------------')


    filepath_list_after_exclude_prefix = util_dir_file_filter.exclude_files_by_prefix(filepath_list, exclude_prefix_list)
    util_dir_file_filter.print_list(filepath_list_after_exclude_prefix, 'exclude prefix')

    print('%d found, %d processed'%(len(filepath_list), len(filepath_list_after_exclude_prefix)))
    print('<<< End')

    #test0(image)
    #test1(image, use_center=False)
    #radial_profile(image, use_center=False)
    #test3(image, use_center=False)
    matplot_x_max = int(max_leg(filepath_list_after_exclude_prefix)) + 10
    print('matplot_x_max=', matplot_x_max)
    test4(filepath_list_after_exclude_prefix, root_dir+image_output_dir, use_center=False, matplot_x_max = matplot_x_max)
    
    
    
