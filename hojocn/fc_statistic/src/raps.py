''' 
Algorithms for RAPS (Radially Averaged Power Spectrum)

radially averaged periodograms (or r-spectrum)
Radially Averaged Energy (RAS / RAPS)
rotational average of the power spectrum 
'''


DIR_mypippyprefix='/media/z/Traveller/mypippyprefix'
import sys
sys.path.insert(0, '%s/lib/python3.8/site-packages'%(DIR_mypippyprefix))
print('PATH=', sys.path)


import time
import matplotlib.pyplot as plt
import numpy as np
import collections
import skimage
from skimage import io




def algorithm0(image):
    print('algorithm0()...')  
    
    projection = {}
    total_count = {}

    for x_i,x in enumerate(image):
        for y_i,y in enumerate(x):
            if round(np.sqrt(x_i**2+y_i**2),1) not in projection:
                projection[round(np.sqrt(x_i**2+y_i**2),1)] = y
                total_count[round(np.sqrt(x_i**2+y_i**2),1)] = 1
            elif np.sqrt(round(np.sqrt(x_i**2+y_i**2),1)) in projection:
                projection[round(np.sqrt(x_i**2+y_i**2),1)] += y
                total_count[round(np.sqrt(x_i ** 2 + y_i ** 2), 1)] += 1

    od = collections.OrderedDict(sorted(projection.items()))
    x, y = [],[]

    for k, v in od.items():
        x.append(k)
        y.append(v/total_count[k])

    plt.plot(x,y)
    plt.xlabel('Radius from (0,0)')
    plt.ylabel('Averaged pixel value')
    plt.show()


def algorithm1(image, use_center):
    print('algorithm1()...')  
    
    center = [0.0, 0.0]
    if use_center is True:
        center = [image.shape[1] / 2.0, image.shape[0] / 2.0]
        
    # create array of radii
    x,y = np.meshgrid(np.arange(image.shape[1]),np.arange(image.shape[0]))
    R = np.sqrt( (x-center[0])**2 + (y-center[1])**2 )

    # calculate the mean
    f = lambda r : image[(R >= r-.5) & (R < r+.5)].mean()
    r  = np.linspace(0, image.shape[1], num=image.shape[1])
    mean = np.vectorize(f)(r)

    # plot it
    fig,ax=plt.subplots()
    ax.plot(r,mean)
    plt.show()


def algorithm2(image, use_center):
    print('algorithm2()...')  
    
    center = [0.0, 0.0]
    if use_center is True:
        center = [image.shape[1] / 2.0, image.shape[0] / 2.0]
    print('center=', center)

    y, x = np.indices((image.shape[0], image.shape[1]))
    print('x=', x)
    print('y=', y)   
    r = np.sqrt((x - center[0])**2 + (y - center[1])**2)
    r = r.astype(np.int)

    print('r.ravel()=', r.ravel())
    print('image.ravel()=', image.ravel())
    tbin = np.bincount(r.ravel(), image.ravel())
    print('tbin=', tbin)
    nr = np.bincount(r.ravel())
    print('nr=', nr)
    radialprofile = tbin / nr
    print('radialprofile=', radialprofile)
    
    plt.plot(radialprofile)
    plt.show()


def algorithm3(image, use_center):
    import radialprofile
    print('algorithm3()...')  
    
    leg = np.hypot(image.shape[0], image.shape[1]) #sqrt(x1**2 + x2**2)
    leg = int(leg)
    print('leg=', leg)
    #radbins  = np.ndarray([i for i in range(0, leg)])
    #print('radbins=', radbins) 
    radbins, az, radavlist = radialprofile.radialAverageBins(image, radbins=leg, corners=True)
    print('radbins=', radbins)
    print('az=', az)
    print('radavlist=', radavlist)
    
    plt.plot(az)
    plt.show()
    
    
def algorithm4(image_filepath, use_center):  
    print('algorithm4()...')  
    image = io.imread(image_filepath)
    print(image.shape) # height, width, channels
        
    leg_length = np.hypot(image.shape[0], image.shape[1]) #sqrt(x1**2 + x2**2)
    print('leg_length=', leg_length)    
    leg_length = int(leg_length+1)

    
    #radius_axis = [i for i in range(0, leg)]
    pixel_count = [0.0]*leg_length
    pixel_sum_val = [0.0]*leg_length
    
    
    #print('image[0]=', image[0])
    #print('image[1]=', image[1])
    for xi in range(image.shape[0]):
        for yi in range(image.shape[1]):
            #color = image[xi][yi]
            color = np.average(image[xi][yi])
            #print('color=',color)
            
            radius = np.hypot(xi, yi)            
                        
            if radius == int(radius):  
                pixel_sum_val[int(radius)] += color
                pixel_count[int(radius)] += 1.0
            else:
                i0 = int(radius)
                i1 = i0 + 1 
                t0 = radius - int(radius)
                t1 = 1.0 - t0
            
                pixel_sum_val[i0] += color * t0
                pixel_sum_val[i1] += color * t1
                pixel_count[i0] += t0
                pixel_count[i1] += t1

    
    #print('pixel_count=', pixel_count)
    #print('pixel_sum_val=', pixel_sum_val)
    
    return pixel_sum_val, pixel_count
    

if '__main__' == __name__:

    image_filepath_list = [ 
    "../img/simple_3x2x3.jpg",
    "../img/simple3_4x2x3.jpg",
    "../img/image4.jpg",
    "../img/simple2_3x2x3.jpg",
    ]
    
    image_filepath_list2 = [ 
    "../img2/01_047_0__mod_500.jpg",
    "../img2/01_150_7__.jpg",    
    "../img2/01_161_2__.jpg",
    "../img2/02_009_5__.jpg",    
    "../img2/02_119_6__.jpg",
    "../img2/02_135_2__.jpg",    
    "../img2/02_167_1__.jpg",
    ]

    #test0(image)
    #test1(image, use_center=False)
    #radial_profile(image, use_center=False)
    #test3(image, use_center=False)
    test4(image_filepath_list2, use_center=False)
    
    
    
