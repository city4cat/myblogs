source: https://note.com/komachi0319cat/n/n463a7cf94b0a  


# 北条司展   
こまち  2024年6月12日 09:06

吉祥寺のゼノンギャラリーで開催中の北条司展に行ってきました。  
去了吉祥寺的GalleryZenon举办的北条司展。  

[**北条司展 – GALLERY ZENON｜ギャラリーゼノン**   
「カフェゼノン&ゼノンサカバ」がリニューアルし、新たに「GALLERY ZENON｜ギャラリーゼノン」が誕生します  
gallery-zenon.jp](https://gallery-zenon.jp/exhibitions/hojotsukasaten/)[](https://gallery-zenon.jp/exhibitions/hojotsukasaten/)  
**北条司展—GALLERY ZENON|画廊ZENON**  
“咖啡厅ZENON & ZENON酒馆”更新，全新“GALLERY ZENON|画廊ZENON”诞生  
gallery-zenon.jp  
  
私はシティーハンターの原作が大好きなので、エンジェルハートのコーナーは薄目でスルーしてじっくり拝見してきました。  
我很喜欢城市猎人的原作，所以对天使之心一角稍微看了一眼，仔细看了一下。  

これまでも墓場の画廊でソニア回の墓場の決闘やミック回の原画は見ていますが、今回は私の大好きなさゆりさん回の原画も展示されていて、舐めるように見つめてしまいました。  
之前我在墓地画廊看过索尼娅那集《墓地对决》和米克那集的原画，这次我最喜欢的小百合那集的原画也展出了，我舔屏似的盯着它们。  

ネトフリ効果なのか外国人も結構いました(私は実写苦手なので観ていませんが、評判が良くて嬉しいです)。  
也有很多外国人（我不喜欢真人电影所以没看，不过评价很好很高兴）。

前期はななちゃんの入院でうっかり忘れていたので、後期だけでも見られてよかった！  
前期因为娜娜住院不小心忘记了，后期能看也很好！  

原画は全体を載せるのは控えたいのでこんな感じでしたというイメージで。  
原画因为想控制刊登全体所以是这样的印象。

興味がある方で吉祥寺に行ける方はぜひ実物をご覧ください。本当に素晴らしいです✨    
有兴趣去吉祥寺的人请一定要看看实物。真的很棒✨  

劇場版の続編も首を長くして待っています！  
剧场版的续篇也翘首以待！  

[![画像](https://assets.st-note.com/production/uploads/images/143817636/picture_pc_f49ec988dfefd90e28a5c77220434fdb.png?width=100)](https://assets.st-note.com/production/uploads/images/143817636/picture_pc_f49ec988dfefd90e28a5c77220434fdb.png?width=2000&height=2000&fit=bounds&quality=85)

[![画像](https://assets.st-note.com/production/uploads/images/143817635/picture_pc_85750b6919417746ee067b4f97bb5a5d.png?width=100)](https://assets.st-note.com/production/uploads/images/143817635/picture_pc_85750b6919417746ee067b4f97bb5a5d.png?width=2000&height=2000&fit=bounds&quality=85)

[![画像](https://assets.st-note.com/production/uploads/images/143817633/picture_pc_eb7af63fb491e7777f77c7ce702c319c.png?width=100)](https://assets.st-note.com/production/uploads/images/143817633/picture_pc_eb7af63fb491e7777f77c7ce702c319c.png?width=2000&height=2000&fit=bounds&quality=85)

[![画像](https://assets.st-note.com/production/uploads/images/143817634/picture_pc_2014aaf33eea33e6a2403c4b5eea8e5e.png?width=100)](https://assets.st-note.com/production/uploads/images/143817634/picture_pc_2014aaf33eea33e6a2403c4b5eea8e5e.png?width=2000&height=2000&fit=bounds&quality=85)

[![画像](https://assets.st-note.com/production/uploads/images/143817637/picture_pc_1fe91c4766d48716dfc1cc119a73ce71.png?width=100)](https://assets.st-note.com/production/uploads/images/143817637/picture_pc_1fe91c4766d48716dfc1cc119a73ce71.png?width=2000&height=2000&fit=bounds&quality=85)

[![画像](https://assets.st-note.com/production/uploads/images/143817630/picture_pc_2a2ebf9335da355465c9ca21c12daa72.png?width=300)](https://assets.st-note.com/production/uploads/images/143817630/picture_pc_2a2ebf9335da355465c9ca21c12daa72.png?width=2000&height=2000&fit=bounds&quality=85)

[![画像](https://assets.st-note.com/production/uploads/images/143817639/picture_pc_e966f181265c78edbb7bccf00ab1359b.png?width=100)](https://assets.st-note.com/production/uploads/images/143817639/picture_pc_e966f181265c78edbb7bccf00ab1359b.png?width=2000&height=2000&fit=bounds&quality=85)

[![画像](https://assets.st-note.com/production/uploads/images/143817644/picture_pc_52ee300ff0688be5c6123bdf49f71d81.png?width=100)](https://assets.st-note.com/production/uploads/images/143817644/picture_pc_52ee300ff0688be5c6123bdf49f71d81.png?width=2000&height=2000&fit=bounds&quality=85)


[#北条司展](https://note.com/hashtag/北条司展)