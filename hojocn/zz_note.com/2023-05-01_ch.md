https://note.com/369chocolate/n/n5f1f142b4bc7  

# 憧れていた聖地（シティーハンター）  
きむら 2023年5月1日 04:11  


《はじめに》  
この記事は私のいわゆるヲタ活的な内容です(^_^;)  
シティーハンター好きではない方が読まれると  
きっと引くと思いますので、ここでそっと閉じて下さい…  

そしてもし、何らかの形でここに辿り着いて下さった  
古くからのシティーハンターファンの方がいらっしゃったら…  
内容が薄過ぎてイラっとさせてしまうかもしれません:(；ﾞﾟ'ωﾟ'):  
私、まだファンになって1年弱の新参者ですm(_ _)m  
内容が浅すぎる！という点についてはお許し下さい…。  

上記注意事項を見ていただいて大丈夫な方は  
ぜひ私のうはうはヲタ活の様子を覗いて行って下さい！笑  
よろしくお願い致しますm(_ _)m  

## 関西から憧れていたカフェゼノンさん

  

[![画像](https://assets.st-note.com/img/1682913858077-hkV8s0IL0y.jpg?width=1200)](https://assets.st-note.com/img/1682913858077-hkV8s0IL0y.jpg?width=2000&height=2000&fit=bounds&quality=85)

お誕生日の飾り付け♡

シティーハンターファンになってから  
色んな情報をネット検索で収集していました。  
もう随分昔の漫画なので  
情報もあまりないんだろうな…なんて思っていたのが大間違い！  
何が初見かはすっかり忘れてしまいましたが  
東京に獠ちゃんの愛車や北条先生のサインが飾られているカフェがあるという情報を得ました((((；ﾟДﾟ)))))))！！

でも発見当時、私はまだ関西在住。  
そもそも人混みが大の苦手で、東京に行くなんて考えられませんでした( ；´Д｀)  
ただただネット見て、いいなぁぁぁ〜と羨望の眼差しで見つめるのみ。

そしたらまぁ何という偶然か、  
夫がまさかの東京転勤(´⊙ω⊙\`)  
都会に対してビビりたおしましたが  
シティーハンター関連の色んな場所に行けるぞ！！  
がモチベーションになりました。笑

そしてついに憧れていたゼノンさんにお邪魔することが出来ました✨✨

> 🎂シティーハンター獠＆香の誕生祭2023開催決定🎂  
> 期間は3月13日（月）〜4月2日（日）🥂✨  
>   
> 限定ノベルティ付のコラボメニューに  
> 特別グッズ、フォトスポットも登場します😍🌟  
> みんなで獠と香をお祝いしよう😘🌈[https://t.co/FdLMbXv9wi](https://t.co/FdLMbXv9wi)[#シティーハンター誕生祭2023](https://twitter.com/hashtag/%E3%82%B7%E3%83%86%E3%82%A3%E3%83%BC%E3%83%8F%E3%83%B3%E3%82%BF%E3%83%BC%E8%AA%95%E7%94%9F%E7%A5%AD2023?src=hash&ref_src=twsrc%5Etfw)[#北条司](https://twitter.com/hashtag/%E5%8C%97%E6%9D%A1%E5%8F%B8?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/PnjefeRvqI](https://t.co/PnjefeRvqI)
> 
> — CAFE ZENON&ZENON SAKABA (@cafezenonsakaba) [March 3, 2023](https://twitter.com/cafezenonsakaba/status/1631513381105717248?ref_src=twsrc%5Etfw)

3月は獠ちゃんと香ちゃんのお誕生月なので  
お誕生日特別メニューでお祝いすることが出来  
店内もお誕生日の飾り付けをして下さっていたので大興奮でした(● ˃̶͈̀ロ˂̶͈́)੭ꠥ⁾⁾

そして北条先生のサインが拝見出来たのもめちゃくちゃ感動！！！  
神谷さんや伊倉さんのサインも拝見致しました✨  
私からしてみれば宝物だらけの店内でした(´༎ຶོρ༎ຶོ\`)✨

東京に住んでられるのは期間限定なので  
憧れのお店に行けて本当に良かった！！！(´༎ຶོρ༎ຶོ\`)✨  
文句も言わず付き合ってくれた夫には大変感謝しておりますm(\_ \_)m✨

  

## 墓場の画廊さん

ゼノンさんと同じく  
検索していて存在を知った墓場の画廊さん。  
こちらではシティーハンターの企画展示やグッズ販売をして下さっていました✨

中野ブロードウェイに店舗を構えていらっしゃり  
様々な企画展示を順次開催されているそうですが  
3月はシティーハンターの企画展示をして下さっていました！

どなたかのツイートかブログかで  
シティーハンターの展示やグッズ販売は  
そんなにずっと前からあったものではない、  
という風に仰ってたように記憶しておりますが  
こちらにもお邪魔出来て感無量でした(´༎ຶོρ༎ຶོ\`)✨

店内にはたくさんお客さんがいらっしゃり  
こんなにシティーハンターファンの方がいらっしゃるのね(\*ﾟ∀ﾟ\*)✨  
と、とぉーっても嬉しく思いました！！！  
チキンなので、思ってただけで声かけたりは出来ませんけど。笑

店内展示スペースには原作原稿のコピーが展示されており  
舐め回すようにじっくり拝見して来ました！笑  
いやもう本当に幸せ…(((o(\*ﾟ▽ﾟ\*)o)))♡

グッズもたくさん販売して下さっていて  
オンラインストアでも実店舗でも購入致しました( ˊ̱˂˃ˋ̱ )♡  
グッズは買わないと決めていたのに  
いつの間にやら身の回りは大量のシティーハンターグッズで囲われています。笑

中野店さんだけでなく  
順次全国の色々な場所を回って行かれるようで、  
前回の企画展示（昨年）の際  
関西にも数日間開催されていたもようで、行くチャンスはあったのです。  
ですがまだハマって間もなかったので  
熱量がそこまでなかった…！  
勿体無いことしたなぁ｡ﾟ(ﾟ´Д｀ﾟ)ﾟ｡  
関西帰ってから企画展示あったら絶対行きます！

  

## 2019劇場版で見た景色に興奮

今まで劇場版プライベートアイズ観てても  
新宿の景色なんて実際見たこともないし  
映画だから架空の街なんだろうと思ってました。

こっちに越して来てから  
獠ちゃんがゴジラに火を吹かせた場所や  
香ちゃんが華麗なハンドル捌きでミニを運転中に角を曲がったドンキ、  
あいちゃんと獠ちゃんがジョギングしてた神社…  
たくさんの場所が実在することを知りました∑(ﾟДﾟ)

んもー…  
すげぇぇぇぇぇ！！！！！！！！(● ˃̶͈̀ロ˂̶͈́)੭ꠥ⁾⁾  
と心の中で大興奮でした！笑

ゴールデン街や新宿駅なども見に行って  
すごいリアルに映画に登場してたんだなぁと感動でした(((o(\*ﾟ▽ﾟ\*)o)))  
なんか本当に新宿駅東口に居たら獠ちゃん香ちゃんに会えそう♡  
そんな妄想を掻き立てる新宿駅近辺でした✨

  

## 原作やアニメを感じる都庁付近

原作で背景として描かれていた新宿の風景も見に行って来ました！  
どなたかのブログを拝見したりして  
都庁近辺にわりとシティーハンタースポットがあると知り都庁へ。  

[![画像](https://assets.st-note.com/img/1682914022508-cXuDkp3f3J.jpg?width=1200)](https://assets.st-note.com/img/1682914022508-cXuDkp3f3J.jpg?width=2000&height=2000&fit=bounds&quality=85)

そびえ立つ都庁

都庁ってそもそもそんな古い建物って知らなくて  
まずその大きさにびっくり！  
え！？これが30年以上も前に建てられたん！？  
と驚きました(　ﾟдﾟ)  
原作の終盤で背景として登場してたんですよね、たしか。

田舎者ですから  
都庁の前に立ってまずはその大きさに呆然と立ち尽くしましたよ。笑  
せっかくなので展望室へ行きましたが  
とってもキレイでした✨  
  
展望室から中央公園を発見！！  
こ…これって…！！  
アニメのエンディングで見た景色と似てない！？  
似てる気がする…！！  
似てるよなぁぁ！！！  
と、また大興奮。笑

都庁出てから中央公園へ行き  
公園とビルという都会っぽい景色に  
シティーハンターを感じてました(\*ﾟ∀ﾟ\*)

  

[![画像](https://assets.st-note.com/img/1682914088801-29UvYnCLMW.jpg?width=1200)](https://assets.st-note.com/img/1682914088801-29UvYnCLMW.jpg?width=2000&height=2000&fit=bounds&quality=85)

セイラの曲が聴こえて来そう！笑

新宿駅から都庁までの間にある高層ビル街は  
アニメの主題歌セイラを思い出しました(((o(\*ﾟ▽ﾟ\*)o)))  
うわー！  
獠ちゃんの足元思い出すわぁー！って興奮しました。笑

もっとしっかり情報を得てから行ったら良かったのかもですが  
十分にシティーハンター感を味わえて大満足でした♡  
またこっちに居る間にしっかり調べて行ってみたいと思います(\*´ω\`\*)

  

## まとめ

本当に私の興奮をぎゃーぎゃーと書き殴っただけになり失礼致しましたm(\_ \_)m  
30年（もうすぐ40年か！）経つのに  
原作に登場した景色を感じることができるなんてすごいなぁと感動しました。

もっともっと聖地と言われるスポットがあるはずなので  
ぜひ東京に居るうちに巡りたいです♪  
奥多摩も遠そうですが行ってみたいなぁ( ´ ▽ \` )♡  
ブログやTwitter等で情報発信して下さっている方々  
ありがとうございます！！！m(\_ \_)m✨

今年公開の映画でも  
また新しい新宿が出てくるのでしょうか…

映画楽しみで待ちきれません(((o(\*ﾟ▽ﾟ\*)o)))✨  
内容的に一回じゃ足りなさそうなので  
ぜひ2回、3回とシリーズものにしていただけないでしょうか！？(● ˃̶͈̀ロ˂̶͈́)੭ꠥ⁾⁾

現段階ではシリーズものだとは発表されてませんが  
期待と願望を込めて  
40周年まで一年に一作ずつ公開するシリーズものとしていただけたら…  
嬉し過ぎて飛び上がります！！！  
そうなったらいいなぁ〜(๑˃̵ᴗ˂̵)♡

ここまでお読み下さりありがとうございますm(\_ \_)m✨  
Twitterはイラスト総合アカウントとしているので  
あまりシティーハンターのことは呟かず  
こうしてたまにnoteで熱を放出させていただいております。笑

管理しきれないからTwitterアカウント1つしか持ってませんが  
シティーハンター専用垢作りたいなぁ。。  
もっとシティーハンター盛り上がって欲しい♡

ではでは、ありがとうございました！m(\_ \_)m✨

copy

[#ヲタ活](https://note.com/hashtag/ヲタ活) 
[#シティーハンター](https://note.com/hashtag/シティーハンター) 