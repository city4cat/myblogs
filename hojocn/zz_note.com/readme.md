source:  
https://note.com/hashtag/シティーハンター?f=new&paid_only=false  
https://note.com/hashtag/冴羽獠?f=new&paid_only=false  
https://note.com/hashtag/ファミリーコンポ?f=new&paid_only=false  
https://note.com/hashtag/北条司展?f=new&paid_only=false  
https://note.com/hashtag/北条司?f=new&paid_only=false  


# note.com上的相关信息  

## 2024  
- 2024-11-19 [笑消除压力！社畜的“城市猎人考察”](./2024-11-19_ch-novel.md)  
- 2024-11-07 [受人敬仰的老师的原创画展](./2024-11-07_hojo-exhibition.md)  

- 2024-09-18 [漫画「RASH!!」感想（完結作品を語る! #438） ](./2024-09-18_rash.md)  
- 2024-07-06 [日本の冴羽獠がそこにいた『シティハンター』](./2024-07-06.md)  
- 2024-06-21 [展期仅剩几天！ 北条司展](./2024-06-21_hojo-exhibition.md)  
- 2024-06-20 [#23『シティーハンター』(2024)](./2024-06-20.md)  
- 2024-06-12 [北条司展](./2024-06-12_hojo-exhibition.md)  
- 2024-05-26 [和不知道原作的年轻人一起看的真人版《城市猎人》](./2024-05-26.md)  
- 2024-05-11 [现在看了“城市猎人新宿PrivateEyes”！](https://note.com/buri_lien/n/n2053ab3ffd8a)  
- 2024-05-06 [映画『シティハンター』 鈴木亮平 2024 2024.04.25 日本 20240506](https://note.com/fictitiousness/n/naaf9a87bfeac)  
- 2024-05-03 [喜欢logo的设计师想说的《城市猎人》标题logo的进化和魅力](./2024-05-03_ch_logo.md)  
- 2024-05-02 [【影视作品鉴赏记录】剧场版城市猎人<新宿PrivateEyes>](https://note.com/choyumekitan/n/ned5f927d86d1)  
- 2024-04-28 [CityHunter冴羽獠的「Mokkori」是如何翻译成海外14种语言的调查](./2024-04-28_mokkori.md)  
- 2024-04-26 [【再录·接龙随笔】请让我用香去，释◯美子广告也请让我说](./2024-04-26_kaori.md)(关于‘香’的解释)    
- 2024-04-26 [守护XYZ的香](./2024-04-26_hojo-exhibition.md)  
- 2024-04-24 [Nikon Plaza和令和的城市猎人之城新宿的语言奇迹令人思索](./2024-04-24_nikon-plaza.md)  
- 2024-04-24 [万众期待的真人版Netflix电影《城市猎人》](./2024-04-24_netflix-ch.md)  
- 2024-04-01 [果然北条司先生不能缺席](./2024-04-01.md) (回忆当年)   
- 2024-03-09 [千穐楽东京马拉松清洲桥路很热！"Meiji the Cat's Eye"㊗️34场公演结束](./2024-03-09.md)   
- 2024-03-04 [29万点赞的热烈反响！三只猫试图爬上门并进入主题“怪盗喵喵眼”与漫画相似](./2024-03-04.md)   

## 2023  
- 2023-12-13 [漫画“天使心 第二季”感想(完结作品说！# 355)](./2023-12-13.md)  
- 2023-12-12 [漫画“天使心”感想(完结作品谈！# 354)](./2023-12-12.md)  
- 2023-09-08 [最终章终于开始了！北条司《剧场版城市猎人天使之泪》虽然对宇宙感到兴奋，但也对太多的噱头感到厌倦](./2023-09-08.md)  
- 2023-05-01 [一个渴望已久的避难所（城市猎人）](./2023-05-01_ch.md)  
- 2023-02-23 [漫画「F.COMPO」感想（完結作品を語る! #226） ](./2023-02-23_fc_review.md)   

## 2022  
- 2022-12-02 [被劇場版城市猎人的KeyVisual而震撼](./2022-12-02_ch-movie.md)  
- 2022-10-30 [「刈谷アニメCollection2022」神谷明さんトークショーレポ ](./2022-10-30.md)  

### 2021  

- 2021-03-23 [214.2021.03.23「紫苑」](./2021-03-23_fc-shion.md)  



### [ シティーハンター天使の涙舞台挨拶レポ ](https://note.com/ch_0326m/m/m0b848538fc29)   
- 2024-10-13 [《剧场版城市猎人 天使泪》神谷明脱口秀&特典赠送会活动第二部](./2024-10-13.md)  
- [2023/10/7剧场版城市猎人“天使之泪”上映后小室哲哉登场舞台问候（TOHO新宿）11:55](./2023-10-07.md)  
- [2023/10/1劇場版シティーハンター「天使の涙」上映後スペシャルトークイベント(TOHO新宿) 16:00の回](./2023-10-01.md)   
- [2023/9/18劇場版シティーハンター「天使の涙」上映後舞台挨拶(バルト９) 13:50の回](./2023-09-18.md)  
- [2023/9/9劇場版シティーハンター「天使の涙」聖地新宿上映後舞台挨拶(TOHOシネマズ新宿13:10～回) ](./2023-09-13.md)  
- [2023/9/9劇場版シティーハンター「天使の涙」聖地新宿上映後舞台挨拶(バルト９) ](./2023-09-09.md)

[Jump Comics 单行本 CityHunter Covers by 小説家 金賀こう（ｶﾅｶﾞｺｳ）@カクヨム](./jump-comics-ch-covers.md)


---  

**Links:**  

- https://ocr.space/  
- [html-to-markdown](https://codebeautify.org/html-to-markdown), 
  [HTML to Markdown Converter](https://htmlmarkdown.com/)  
- [打日语 日语在线输入](https://www.qiuziti.com/tool_dariyu.html)  
- [DeepL](https://www.deepl.com)  
- [腾讯翻译君](https://fanyi.qq.com/)  
- [有道翻译](https://fanyi.youdao.com)  
- [Bing翻译](https://cn.bing.com/translator)  

--------------------------------------------------
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)  
![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)  
转载请注明出处  
