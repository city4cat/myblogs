https://note.com/naofumitasaki/n/nc1660f2cbfcb  

![](https://assets.st-note.com/production/uploads/images/138582539/rectangle_large_type_2_9a7e65b4bf80184a70dd9cd4b05ce15e.jpg?width=800)

# ガード下はXYZの香り  
守护XYZ的香  

田﨑 尚文  2024年4月26日 14:23  

シティーハンター・冴羽獠とキャッツ♡アイの三姉妹が共にいるとの目撃情報を得て、我々捜査班は吉祥寺へと急行した。  
得到城市猎人冴羽獠和Cat's♡Eye三姐妹在一起的目击情报，我们搜查班迅速赶往吉祥寺。  

[![画像](https://assets.st-note.com/production/uploads/images/138582804/picture_pc_778ad254ba538e3d51933953c082e1d8.png?width=800)](https://assets.st-note.com/production/uploads/images/138582804/picture_pc_778ad254ba538e3d51933953c082e1d8.png?width=2000&height=2000&fit=bounds&quality=85)

ギャラリーゼノン…  
Gallery Zenon...  

この高架下のギャラリーカフェが怪しい。  
这家高架下的Gallery Cafe很可疑。  

[![画像](https://assets.st-note.com/production/uploads/images/138582882/picture_pc_4f602fd75b3532110e48f72224c2b6a7.png?width=800)](https://assets.st-note.com/production/uploads/images/138582882/picture_pc_4f602fd75b3532110e48f72224c2b6a7.png?width=2000&height=2000&fit=bounds&quality=85)

むむ！伝言板！  
かつてどの駅にもあったにもかかわらず、今では見る影もなくなった伝言板が、どうしてカフェの外壁に…？  
嗯 留言板！  
以前每个车站都有的留言板，现在已经不见踪影，为什么会出现在咖啡馆的外墙上？  

しかも複数書かれた「XYZ」の文字…  
上面为什么写着多个 "XYZ "字母  

こ、これは、まさか、あのシティーハンターの…  
这，这个，难道，那个城市猎人的…...  

- - -

なんていう下手な小芝居はさておき。  
抛开这出蹩脚的噱头不谈。  

吉祥寺高架下のギャラリーゼノンで開催中のこちらへ行ってきました。  
我去吉祥寺高架下的 Zenon Gallery看了这个展览。  

[![画像](https://assets.st-note.com/production/uploads/images/138583380/picture_pc_8b0625abe3f4e258f5a3e00a43d782f0.png?width=800)](https://assets.st-note.com/production/uploads/images/138583380/picture_pc_8b0625abe3f4e258f5a3e00a43d782f0.png?width=2000&height=2000&fit=bounds&quality=85)

キャッツ♡アイ、シティーハンターなどの原作者・北条司先生の世界に浸れる北条司展です。  
这里是北条司展，在这里你可以沉浸在《Cat's♡Eye》和《城市猎人》的原作者北条司的世界里。  

伝言板は外にあって、自由に獠への依頼（じゃなくてもいいんだけど）が書けます。
展厅外有一块留言板，您可以自由地在上面写下对獠的请求（不是也可以)）。  

[![画像](https://assets.st-note.com/production/uploads/images/138583821/picture_pc_8e9612c865e25abd88267a94f2acefe0.png?width=800)](https://assets.st-note.com/production/uploads/images/138583821/picture_pc_8e9612c865e25abd88267a94f2acefe0.png?width=2000&height=2000&fit=bounds&quality=85)

C♡EやCH以外の短編を含む作品の原画やラフが展示されています。  
展出了他的作品原画和草稿，包括《C♡E》和《CH》以外的短篇作品。  

とはいえ、まずはキャッツ♡アイ。  
但，首先是《Cat's Eye》。  

[![画像](https://assets.st-note.com/production/uploads/images/138584048/picture_pc_7c6ae6446f6dc6fb2a3d7377b03a5a15.png?width=800)](https://assets.st-note.com/production/uploads/images/138584048/picture_pc_7c6ae6446f6dc6fb2a3d7377b03a5a15.png?width=2000&height=2000&fit=bounds&quality=85)  
[![画像](https://assets.st-note.com/production/uploads/images/138584047/picture_pc_fe46c66476788944386e9924a8c88c04.png?width=800)](https://assets.st-note.com/production/uploads/images/138584047/picture_pc_fe46c66476788944386e9924a8c88c04.png?width=2000&height=2000&fit=bounds&quality=85)

北条司先生の描く女性は自然でかつ美しい。  
北条司描绘的女性自然而美丽。   

続くはもちろんシティーハンター。  
其次当然是《城市猎人》。  

[![画像](https://assets.st-note.com/production/uploads/images/138584257/picture_pc_4dfd51a9e8e42ca42e459fbd12f1dc4d.png?width=800)](https://assets.st-note.com/production/uploads/images/138584257/picture_pc_4dfd51a9e8e42ca42e459fbd12f1dc4d.png?width=2000&height=2000&fit=bounds&quality=85)  
[![画像](https://assets.st-note.com/production/uploads/images/138584259/picture_pc_8fc32ca775a40b1d4984106ce2da3a72.png?width=800)](https://assets.st-note.com/production/uploads/images/138584259/picture_pc_8fc32ca775a40b1d4984106ce2da3a72.png?width=2000&height=2000&fit=bounds&quality=85)  
[![画像](https://assets.st-note.com/production/uploads/images/138584260/picture_pc_c631dd0d0bfb9464fba7c7b11c0ee3d1.png?width=800)](https://assets.st-note.com/production/uploads/images/138584260/picture_pc_c631dd0d0bfb9464fba7c7b11c0ee3d1.png?width=2000&height=2000&fit=bounds&quality=85)

香や依頼主美女の美しさ、可愛らしさが、獠のカッコよさを引き立ててるっていうところあるな。うん。  
香和委托人美女的美丽、可爱，反而衬托出獠的帅气。嗯。  

原画には余分な線を消すホワイトや掲載にあたっての指示書きなども見え、先生が手を動かして描いた息遣いさえ感じられそうです。  
在原画中，可以看到去掉多余线条的白线和出版说明，甚至可以感受到老师画画时手上的气息。  

[![画像](https://assets.st-note.com/production/uploads/images/138584564/picture_pc_7660d694676ff0fadabf593432a1c65c.png?width=800)](https://assets.st-note.com/production/uploads/images/138584564/picture_pc_7660d694676ff0fadabf593432a1c65c.png?width=2000&height=2000&fit=bounds&quality=85) 
[![画像](https://assets.st-note.com/production/uploads/images/138584569/picture_pc_a3c7fcedf9c7b4aad8ed640cb0778f31.png?width=800)](https://assets.st-note.com/production/uploads/images/138584569/picture_pc_a3c7fcedf9c7b4aad8ed640cb0778f31.png?width=2000&height=2000&fit=bounds&quality=85) 

注意書きも粋です。  
警示标志也很时尚。   

トイレの案内の近くには…  
在厕所信息附近有一个...  

[![画像](https://assets.st-note.com/production/uploads/images/138584631/picture_pc_5863fa213ef2d7ed5669200b9ce3e1f3.png?width=800)](https://assets.st-note.com/production/uploads/images/138584631/picture_pc_5863fa213ef2d7ed5669200b9ce3e1f3.png?width=2000&height=2000&fit=bounds&quality=85)

最近の雑誌や愛蔵版などは追えていないので、当初の連載より後の絵は新鮮です。 
我一直没有关注过最近的杂志和愛蔵版，所以看到比最初连载更晚的图片让我耳目一新。  

[![画像](https://assets.st-note.com/production/uploads/images/138584769/picture_pc_5bfd6475da13a3d71108b1c5d8050cf9.png?width=800)](https://assets.st-note.com/production/uploads/images/138584769/picture_pc_5bfd6475da13a3d71108b1c5d8050cf9.png?width=2000&height=2000&fit=bounds&quality=85)

２階の展示室に上がる階段の入り口には  
在通往二楼展览室的楼梯入口处   

[![画像](https://assets.st-note.com/production/uploads/images/138584815/picture_pc_0b32145a66b60682c01e93c4b86eacd6.png?width=800)](https://assets.st-note.com/production/uploads/images/138584815/picture_pc_0b32145a66b60682c01e93c4b86eacd6.png?width=2000&height=2000&fit=bounds&quality=85)

海坊主と美樹ちゃん（推し）に出迎えられたい…  
想被海坊主和美树(我猜)迎接……   

[![画像](https://assets.st-note.com/production/uploads/images/138584865/picture_pc_601a5a6f40b2b711db4395453b1bea4f.png?width=800)](https://assets.st-note.com/production/uploads/images/138584865/picture_pc_601a5a6f40b2b711db4395453b1bea4f.png?width=2000&height=2000&fit=bounds&quality=85)

瞳姉さん  
瞳姐  

[![画像](https://assets.st-note.com/production/uploads/images/138584866/picture_pc_d0e8837e3bf822ef845cb821136d1fab.png?width=800)](https://assets.st-note.com/production/uploads/images/138584866/picture_pc_d0e8837e3bf822ef845cb821136d1fab.png?width=2000&height=2000&fit=bounds&quality=85) 
[![画像](https://assets.st-note.com/production/uploads/images/138584864/picture_pc_652257da2a26bdfd1bb35a361a372155.png?width=800)](https://assets.st-note.com/production/uploads/images/138584864/picture_pc_652257da2a26bdfd1bb35a361a372155.png?width=2000&height=2000&fit=bounds&quality=85) 

憧れのコルトパイソン.357マグナム  
令人向往的柯尔派森.357玛格南  

[![画像](https://assets.st-note.com/production/uploads/images/138584867/picture_pc_f35f1cf2818af7515e9f2d5b40869edc.png?width=800)](https://assets.st-note.com/production/uploads/images/138584867/picture_pc_f35f1cf2818af7515e9f2d5b40869edc.png?width=2000&height=2000&fit=bounds&quality=85)

上手くね？  
这很好，不是吗？  

[![画像](https://assets.st-note.com/production/uploads/images/138584868/picture_pc_24dd56756948f9c0650c5f52a672e175.png?width=800)](https://assets.st-note.com/production/uploads/images/138584868/picture_pc_24dd56756948f9c0650c5f52a672e175.png?width=2000&height=2000&fit=bounds&quality=85) 

これも観たい  
我也想看到这个  

[![画像](https://assets.st-note.com/production/uploads/images/138584959/picture_pc_85de3b5370f12b55d5fda1fe7620db7e.png?width=800)](https://assets.st-note.com/production/uploads/images/138584959/picture_pc_85de3b5370f12b55d5fda1fe7620db7e.png?width=2000&height=2000&fit=bounds&quality=85) 
[![画像](https://assets.st-note.com/production/uploads/images/138584962/picture_pc_5a059536a36b3785c7f7e62629fc36fb.png?width=800)](https://assets.st-note.com/production/uploads/images/138584962/picture_pc_5a059536a36b3785c7f7e62629fc36fb.png?width=2000&height=2000&fit=bounds&quality=85) 

１階に戻ると…  
回到1楼…  

[![画像](https://assets.st-note.com/production/uploads/images/138585187/picture_pc_78a8b8a0b5163b5be6e5c98bf0d012b6.png?width=800)](https://assets.st-note.com/production/uploads/images/138585187/picture_pc_78a8b8a0b5163b5be6e5c98bf0d012b6.png?width=2000&height=2000&fit=bounds&quality=85)

こちらは記念撮影用  
这是纪念摄影用的  

[![画像](https://assets.st-note.com/production/uploads/images/138585214/picture_pc_0c69488ac58fd1fbf422a653d9ce0d85.png?width=800)](https://assets.st-note.com/production/uploads/images/138585214/picture_pc_0c69488ac58fd1fbf422a653d9ce0d85.png?width=2000&height=2000&fit=bounds&quality=85)

そして今回の展示で一番カッコいい獠ちゃんは…  
而这次展示中最帅的獠酱是…  

[![画像](https://assets.st-note.com/production/uploads/images/138585122/picture_pc_72bdd6f8b32eed8c0bd0f40891befe77.png?width=800)](https://assets.st-note.com/production/uploads/images/138585122/picture_pc_72bdd6f8b32eed8c0bd0f40891befe77.png?width=2000&height=2000&fit=bounds&quality=85)

むむ、これは…  
嗯，这是…  

[![画像](https://assets.st-note.com/production/uploads/images/138585247/picture_pc_cd5dd5e43433471e994bf636c16320c6.png?width=800)](https://assets.st-note.com/production/uploads/images/138585247/picture_pc_cd5dd5e43433471e994bf636c16320c6.png?width=2000&height=2000&fit=bounds&quality=85)

そう、NETFLIX限定配信の実写版シティーハンター！  
没错，就是NETFLIX限定版的真人版《城市猎人》!  

鈴木亮平演じる冴羽獠を北条司先生が描く！  
北条司老师描绘铃木亮平饰演的冴羽獠!  

[![画像](https://assets.st-note.com/production/uploads/images/138588515/picture_pc_422bec1febd49fe0d32dfee0f1bfa317.png?width=800)](https://assets.st-note.com/production/uploads/images/138588515/picture_pc_422bec1febd49fe0d32dfee0f1bfa317.png?width=2000&height=2000&fit=bounds&quality=85)

NETFLIXシティーハンターは25日から配信開始！  
これは観なければ。  
NETFLIX城市猎人从25日开始放送!  
这个必须看。  

観たら必ずやレビューあげます。  
如果我看了它，我一定会给它一个评论。  

待ってろよ！獠！  
等等我!獠!  

というわけで、北条司展は吉祥寺高架下のギャラリーゼノンにて。  
因此，北条司的展览就在吉祥寺高架桥下的芝诺画廊举行。  

後半展示が変わるっていうし、今日はバタバタしてグッズ見るだけにしちゃったから、またこなくちゃな。  
后半段展览会发生变化，今天就忙着看周边了，下次还得再来。    



