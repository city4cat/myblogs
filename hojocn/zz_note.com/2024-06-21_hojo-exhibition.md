https://note.com/naofumitasaki/n/nc4c8852e3788  

# 会期残りわずか！北条司展
展期仅剩几天！ 北条司展  

田﨑 尚文  2024年6月21日 10:44  

また来ました。  
又来了。  

[守护XYZ的香 @Note  
![](https://assets.st-note.com/production/uploads/images/138582539/rectangle_large_type_2_9a7e65b4bf80184a70dd9cd4b05ce15e.jpg?fit=bounds&format=jpeg&quality=85&height=100) ](./2024-04-26_hojo-exhibition.md)  

吉祥寺ガード下のギャラリーゼノン。  
吉祥寺护卫下的Gallery Zenon。  

その新装オープン記念でもある「北条司展」の会期が残りわずかとなりました。  
作为新装开业纪念的“北条司展”的会期所剩无几了。  

[**北条司展 – GALLERY ZENON｜ギャラリーゼノン**   
「カフェゼノン&ゼノンサカバ」がリニューアルし、新たに「GALLERY ZENON｜ギャラリーゼノン」が誕生します  
「Cafe Zenon & Zenon Sakaba」经翻新后成为新的「GALLERY ZENON｜ギャラリーゼノン」  
gallery-zenon.jp](https://gallery-zenon.jp/exhibitions/hojotsukasaten/)[](https://gallery-zenon.jp/exhibitions/hojotsukasaten/)  

展示替えがあったとのことで２度目の来訪。  
因为展览更换的事第二次来访。  

[![画像](https://assets.st-note.com/production/uploads/images/144787296/picture_pc_8ba84d567a7acea1b842efa8d35f5c94.png?height=100)](https://assets.st-note.com/production/uploads/images/144787296/picture_pc_8ba84d567a7acea1b842efa8d35f5c94.png?width=2000&height=2000&fit=bounds&quality=85)  

あれ？  
咦?  

[![画像](https://assets.st-note.com/production/uploads/images/144787578/picture_pc_f8df37dcec7419b324ed35bba5afa619.png?height=100)](https://assets.st-note.com/production/uploads/images/144787578/picture_pc_f8df37dcec7419b324ed35bba5afa619.png?width=2000&height=2000&fit=bounds&quality=85)  

パネルが増えてる…  
面板在增加…

あれ？  
咦?  

[![画像](https://assets.st-note.com/production/uploads/images/144787589/picture_pc_d517a68ace5adc1975effa4bfd554a25.png?height=100)](https://assets.st-note.com/production/uploads/images/144787589/picture_pc_d517a68ace5adc1975effa4bfd554a25.png?width=2000&height=2000&fit=bounds&quality=85)  

[![画像](https://assets.st-note.com/production/uploads/images/144787588/picture_pc_39b3700a87797b09b9ba071a9b9481cc.png?height=100)](https://assets.st-note.com/production/uploads/images/144787588/picture_pc_39b3700a87797b09b9ba071a9b9481cc.png?width=2000&height=2000&fit=bounds&quality=85)  

[![画像](https://assets.st-note.com/production/uploads/images/144787587/picture_pc_c45ce79599ee98d6879a6e9e1da000df.png?height=100)](https://assets.st-note.com/production/uploads/images/144787587/picture_pc_c45ce79599ee98d6879a6e9e1da000df.png?width=2000&height=2000&fit=bounds&quality=85)  

サインがある…！  
有签名…!


[![画像](https://assets.st-note.com/production/uploads/images/144787595/picture_pc_cc74f29cfacdef13f7b1faa13d999ddd.png?height=100)](https://assets.st-note.com/production/uploads/images/144787595/picture_pc_cc74f29cfacdef13f7b1faa13d999ddd.png?width=2000&height=2000&fit=bounds&quality=85)

増えたパネルは、劇場版**「シティーハンター 〜天使の涙<sup>エンジェル・ダスト</sup>〜」**向けの書き下ろしだそうな。  
增加的面板是面向剧场版**「城市猎人~天使之泪<sup>Angel Dust</sup> ~」**的新画的。  


改めて拝見すると、北条司先生の筆遣いが美しく、繊細です。  
再次拜读，北条司先生的笔触优美而细腻。

[![画像](https://assets.st-note.com/production/uploads/images/144787864/picture_pc_f0768b4216fdac81bea9cf8e0d3e6777.png?height=100)](https://assets.st-note.com/production/uploads/images/144787864/picture_pc_f0768b4216fdac81bea9cf8e0d3e6777.png?width=2000&height=2000&fit=bounds&quality=85)  
「キャッツ♡アイ」の扉絵  
雪の一粒一粒がしっかり“乗って”いる  
“Cat's♡Eye”的扉页  
雪一片一片地“压”着  

[![画像](https://assets.st-note.com/production/uploads/images/144788807/picture_pc_387a3ea8f11f23646b6c8119764d4588.png?height=100)](https://assets.st-note.com/production/uploads/images/144788807/picture_pc_387a3ea8f11f23646b6c8119764d4588.png?width=2000&height=2000&fit=bounds&quality=85)   
「シティーハンター」の扉絵  
革の質感と銃身のキラリの存在感  
「城市猎人」的扉页  
皮革的质感和枪身闪闪发光的存在感  

[![画像](https://assets.st-note.com/production/uploads/images/144788809/picture_pc_041332f73fbd831dfc8aa9d658528df0.png?height=100)](https://assets.st-note.com/production/uploads/images/144788809/picture_pc_041332f73fbd831dfc8aa9d658528df0.png?width=2000&height=2000&fit=bounds&quality=85)  
「シティーハンター」の扉絵  
鬼気迫る海原の圧力  
「城市猎人」的扉页  
阴气逼人的海原的压力  

[![画像](https://assets.st-note.com/production/uploads/images/144788960/picture_pc_1481c55d3f3270a54d49b74192ae1c05.png?height=100)](https://assets.st-note.com/production/uploads/images/144788960/picture_pc_1481c55d3f3270a54d49b74192ae1c05.png?width=2000&height=2000&fit=bounds&quality=85)  
そして獠の頬には本当に血が流れているかのよう  
而且獠的脸上似乎真的流着血  

[![画像](https://assets.st-note.com/production/uploads/images/144788980/picture_pc_b0234469693ddaeae0c4e9acd0793c95.png?height=100)](https://assets.st-note.com/production/uploads/images/144788980/picture_pc_b0234469693ddaeae0c4e9acd0793c95.png?width=2000&height=2000&fit=bounds&quality=85)  
「シティーハンター」の扉絵  
「城市猎人」的扉页

[![画像](https://assets.st-note.com/production/uploads/images/144788985/picture_pc_4dc7df3513996bd1de2ce11792b9ca97.png?height=100)](https://assets.st-note.com/production/uploads/images/144788985/picture_pc_4dc7df3513996bd1de2ce11792b9ca97.png?width=2000&height=2000&fit=bounds&quality=85)  
「キャッツ♡アイ」扉絵の雪と比べて繊細な細雪  
与「Cat's♡Eye」扉页的雪相比，纤细的细雪  

平面な上に下手っぴな写真なので伝わらないかもしれません。ぜひその目で見ていただきたい。  
因为是平面的，再加上照片质量差，可能传达不了。请一定要亲眼看看。  

２階の展示。  
二楼的展示。

[![画像](https://assets.st-note.com/production/uploads/images/144789654/picture_pc_11740dc8ddb01e68d776bb8502f7f132.png?height=100)](https://assets.st-note.com/production/uploads/images/144789654/picture_pc_11740dc8ddb01e68d776bb8502f7f132.png?width=2000&height=2000&fit=bounds&quality=85)  
瞳姉さんは跳んでます  
瞳姐在跳。  

[![画像](https://assets.st-note.com/production/uploads/images/144789696/picture_pc_8347d81656d01ce3bbfe8ebd557406c4.png?height=100)](https://assets.st-note.com/production/uploads/images/144789696/picture_pc_8347d81656d01ce3bbfe8ebd557406c4.png?width=2000&height=2000&fit=bounds&quality=85)  
さりげないにもほどがある！  
（字がかわいい）  
不会太随意！  
(人物很可爱)  

[![画像](https://assets.st-note.com/production/uploads/images/144790322/picture_pc_1aaa25ac20f193cfc8eddcd40f2ec30b.png?height=100)](https://assets.st-note.com/production/uploads/images/144790322/picture_pc_1aaa25ac20f193cfc8eddcd40f2ec30b.png?width=2000&height=2000&fit=bounds&quality=85)  
前回スルーしてしまったけど、このクロッキーはとても貴重  
虽然上次没看过，但是这个草图非常珍贵  

１階に降りて…  
下到1楼…  

[![画像](https://assets.st-note.com/production/uploads/images/144790474/picture_pc_c9be7a48c2e6514096855b12adfe9c44.png?height=100)](https://assets.st-note.com/production/uploads/images/144790474/picture_pc_c9be7a48c2e6514096855b12adfe9c44.png?width=2000&height=2000&fit=bounds&quality=85)  
前回と違うアングルで  
用与上次不同的角度  

物販コーナーぐるぐる…  
在卖东西的角落转来转去…  

[![画像](https://assets.st-note.com/production/uploads/images/144790521/picture_pc_f96a9dc98a93a0d479387667cb6506a0.png?height=100)](https://assets.st-note.com/production/uploads/images/144790521/picture_pc_f96a9dc98a93a0d479387667cb6506a0.png?width=2000&height=2000&fit=bounds&quality=85)  
うわぁ…  
哇…  

コルトパイソン.357マグナム…  
これはほしい…  
額としては妥当だけど…  
Colt Python.357Magnum…  
这个想要…  
作为价格是合理的…

…モデルガンの方がいいな…（贅沢）  
……模型枪比较好啊…(奢侈)  

最終的に原画集を購入して２度目の来訪を終えました。  
最终购买了原画集，结束了第二次来访。  

会期は残すところこの週末２日間。  
会期只剩下本周末两天。  

北条司先生にご興味のある方はぜひ！  
对北条司老师感兴趣的人请务必!  
