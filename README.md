
## 说明  

- "CE"是北条司作品"Cat's Eye"的缩写，该作品的其他名称有: キャッツアイ / 貓眼三姐妹(大陸) / 貓之眼(港) / 貓眼(台)  
- "CH"是北条司作品"City Hunter"的缩写，该作品的其他名称有: シティーハンター / 城市猎人(大陸) / 城市獵人(港/台) / 侠探寒羽良(大陆 盗版)  
- "FC"是北条司作品"Family Compo"的缩写，该作品的其他名称有: ファミリーコ ンポ / F.COMPO / FamilyCompo / 非常家庭(大陆) / 搞怪家庭(港) / 变奏家族(台) / 反串家族(新加坡)  
- 《こもれ陽の下で･･･》是北条司的作品，该作品的其他名称有: Komorebi No Moto De... / Under the Dapple Shade / Beneath the Dappled Shade / 阳光少女(大陆) / 艷陽少女(港)  
- "AH"是北条司作品"Angel Heart"的缩写，该作品的其他名称有: エンジェルハート / 天使之心(港) / 天使心(台)  
- 以下这些文章发布在[北条司中文网论坛](http://www.hojocn.com/bbs)，欢迎您来该论坛讨论。  
- 本项目数据同步至以下两个代码库：  
    - [myblogs - Gitlab](https://gitlab.com/city4cat/myblogs)  
    - [myblogs - Gitee](https://gitee.com/city4cat/myblogs)  


## 目录  

## CE:  
- [关于「Cat's Eye 40周年纪念原画展」的一些信息](./hojocn/ce_40th/readme.md)  
- [猫眼咖啡屋的建筑风格](./hojocn/ce_catseye_cafe/readme.md)  
- [CE的一些细节](./hojocn/ce_details/readme.md)  
- [Margaret Atwood的小说《Cat's Eye》 ](./hojocn/zz_ce_margaret/catseye_margaret.md)  
- [CE的一些信息](./hojocn/ce_info/readme.md)  
- [Margaret Atwood的小说《Cat's Eye》 ](./hojocn/zz_ce_margaret/catseye_margaret.md)  


## CH:  
- [CH结尾处的致谢](./hojocn/ch_acknowledgement/readme.md)  
- [CH的绘画 - 关于"《城市猎人》里角色相貌相似](./hojocn/ch_drawing/similar_faces.md)  
- [CH的绘画 - 衣服/布料/褶皱](./hojocn/ch_drawing__cloth/readme.md)  
- [CH的绘画 - 动作/姿态](./hojocn/ch_drawing__gesture)  
- [CH的绘画 - 化妆](./hojocn/ch_make-up/img)  
- [CH的一些信息](./hojocn/ch_info/readme.md)  
- [CH的一些细节](./hojocn/ch_details/readme.md)  


## 短篇:  
- [解读《白猫少女》](./hojocn/ss_cat_lady/readme.md)  
- [《Parrot-幸福的人》 ~ 神谷明](./hojocn/ss_parrot/airman_parrot_kamiya_akira.md)  
- [解读《蔚蓝长空》](./hojocn/ss_blue-sky/readme.md)  
- [解读《少年们的夏天--珍妮的乐曲》](./hojocn/ss_melody-of-jenny/readme.md)  
- [解读《American Dream》](./hojocn/ss_american-dream/readme.md)  
- [《Rash!!》的相关信息](./hojocn/rash_info/readme.md)  
- [《阳光少女》的相关信息](./hojocn/komorebi_info/readme.md)  


## FC:  
- [Family Compo Asset Library](https://gitlab.com/family-compo-asset), ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96741))    
- [FC同人-无题](https://gitlab.com/family-compo-asset/family-compo-mods/-/tree/master/02_memory) ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96877))  
- [FC同人-你是我的女王](https://gitlab.com/family-compo-asset/family-compo-mods/-/tree/master/04_you_are_my_queen)  ([hojocn上的链接](http://www.hojocn.com/bbs/viewthread.php?tid=96759))   
- [FC动图](./hojocn/fc_animation/readme.md)  
- [可能影响FC创作的（日本）社会背景](./hojocn/fc_background/readme.md)  
- [FC的绘画-修图](./hojocn/fc_bugfix/readme.md)  
- [如何化解FC里堂表兄妹恋在中国文化里的尴尬](./hojocn/fc_cousin_love_in_chinese_culture/readme.md)  
- [FC的一些细节](./hojocn/fc_details/readme.md)  
- [雅彦日记](./hojocn/fc_diary/fc_diary_masahiko.md)  
- [FC的绘画特点(汇总)](./hojocn/fc_drawing/readme.md)  
    - [FC的绘画-对话框](./hojocn/fc_dialogue/readme.md)  
    - [FC的画风-面部-年龄变化](./hojocn/fc_drawing_style__aging/readme.md)  
    - [FC的画风-人物身材比例](./hojocn/fc_drawing_style__body_proportion/readme.md)  
    - [FC的画风-衣服/布料/褶皱](./hojocn/fc_drawing_style__cloth/readme.md)  
    - [FC的绘画-面部](./hojocn/fc_drawing_style__face/readme.md)  
    - [FC的绘画-面部2](./hojocn/fc_drawing_style__face/readme2.md)  
    - [FC的画风-面部(侧面)](./hojocn/fc_drawing_style__face/fc_face_side.md)  
    - [FC的画风-面部-眉](./hojocn/fc_drawing_style__face_brow/readme.md)  
    - [FC的画风-面部-眉2](./hojocn/fc_drawing_style__face_brow2/readme.md)  
    - [FC的画风-面部-五官组合(正面)](./hojocn/fc_drawing_style__face_composition/readme.md)  
    - [FC的画风-面部-耳(侧面)](./hojocn/fc_drawing_style__face_ear_side/readme.md)  
    - [FC的画风-面部-眼部](./hojocn/fc_drawing_style__face_eye/readme.md)  
    - [FC的画风-面部-眼睛2](./hojocn/fc_drawing_style__face_eye2/readme.md)  
    - [FC的画风-面部-眼睛(侧面)](./hojocn/fc_drawing_style__face_eye_side/readme.md)  
    - [FC的画风-面部-嘴](./hojocn/fc_drawing_style__face_mouth/readme.md)  
    - [FC的画风-面部-嘴(正面)](./hojocn/fc_drawing_style__face_mouth2/readme.md)  
    - [FC的画风-面部-鼻](./hojocn/fc_drawing_style__face_nose/readme.md)  
    - [FC的画风-面部-鼻(正面)](./hojocn/fc_drawing_style__face_nose2/readme.md)  
    - [FC的画风-面部比例](./hojocn/fc_drawing_style__face_proportion/readme.md)  
    - [FC的画风-面部2(侧面)](./hojocn/fc_drawing_style__face_side2/readme.md)  
    - [FC的画风-特效](./hojocn/fc_drawing_style__fx/readme.md)  
    - [FC的画风-动作/姿态](./hojocn/fc_drawing_style__gesture/readme.md)  
    - [FC的画风-头发](./hojocn/fc_drawing_style__hair/readme.md)  
    - [FC的画风-面部-头发2(正面)](./hojocn/fc_drawing_style__hair2/readme.md)  
    - [FC的画风-手](./hojocn/fc_drawing_style__hand/readme.md)  
    - [FC的画风-腿](./hojocn/fc_drawing_style__leg/readme.md)  
    - [FC的画风-化妆](./hojocn/fc_drawing_style__make-up/readme.md)  
    - [FC的画风-颈部](./hojocn/fc_drawing_style__neck/readme.md)  
    - [FC的绘画-透视](./hojocn/fc_drawing_style__perspective/readme.md)  
    - [FC的绘画-写实风格](./hojocn/fc_drawing_style__realistic/readme.md)  
    - [FC的画风-场景](./hojocn/fc_drawing_style__scene/readme.md)  
    - [FC的画风-着色](./hojocn/fc_drawing_style__shading/readme.md)  
    - [FC的服饰](./hojocn/fc_dress/readme.md)  
- [FC表情包](./hojocn/fc_expression)  
    - [表情-雅彦](./hojocn/fc_expression/fc_exp_masahiko.md)  
    - [表情-紫苑](./hojocn/fc_expression/fc_exp_shion.md)  
    - [表情-其他人](./hojocn/fc_expression/fc_exp_others.md)  
- [FC里某些角色的颜值](./hojocn/fc_face_score/fc_face_score.md)  
- [FC的画风-食物](./hojocn/fc_food/readme.md)  
- [FC里的幽默](./hojocn/fc_humour/readme.md)  
- [FC同人文--我是一只流浪猫](./hojocn/fc_im_a_stray_cat/readme.md)  
- [整理FC的一些信息](./hojocn/fc_information/readme.md)， 
    - [非常家庭 - wikipedia](./hojocn/fc_information/fc_zh_wiki.md)  
    - [考究一下FC漫画名的意思](./hojocn/fc_information/fc_title.md)  
    - [FC时间线](./hojocn/fc_information/fc_timeline.md)  
- [FC玉皇朝版封底彩图收录](./hojocn/fc_jade_dynasty_back_color/readme.md)  
- [[Todo]FC的画风-风景](./hojocn/fc_landscapes/readme.md)  
- [FC的分镜](./hojocn/fc_sequence/readme.md)  
- [FC里的人物姓名与日本姓氏](./hojocn/fc_surname_vs_jap_surname/readme.md)  
- [[zz]关于"紫苑"这个名字](./hojocn/fc_surname_vs_jap_surname/zz_shion_jianshu/zz_shion_jianshu.md)  
- [FC实物集](./hojocn/fc_things_in_real_world/readme.md)  
- [把FC故事板化所遇到的问题](./hojocn/fc_to_storyboard/fc_to_storyboard.md)  
- [若苗家的户型和布局](./hojocn/fc_wakanae_house_layout/readme.md)  
- [FC的编剧](./hojocn/fc_writing/readme.md)， 
    - [FC的编剧 - FC每话情节概括](./hojocn/fc_writing/fc_chapters_summary.md)  
    - [FC每话剧情与"Pixar的故事讲述规则"的对照](./hojocn/fc_writing/fc_vs_pixar_storytelling_in_chapters.md)  
    - ["Pixar的故事讲述规则"对照FC的剧情](./hojocn/fc_writing/fc_vs_pixar_storytelling_overall.md)  
    - [FC后续剧情的推测](./hojocn/fc_writing/fc_future_stories.md)  
- [(AIGC)FC Training](./hojocn/aigc_fc_training/readme.md)  


## AH:  
- [AH的相关信息](./hojocn/ah_info/readme.md)  
- [AH里最受欢迎的10个故事(访谈)](./hojocn/ah2_vol10_best_EP_10/readme.md)  
- [AH的一些细节](./hojocn/ah_details/ah_details.md)  
- [AH的故事原型](./hojocn/ah_details/ah_prototype.md)  
- [AH的绘画 - 脚](./hojocn/ah_drawing__foot/readmd.md)  
- [AH的分镜](./hojocn/ah_sequence/readme.md)  

## AH (2nd Season):  
- [AH2的相关信息](./hojocn/ah2_info/readme.md)  


## Interviews:  
- [(1991)《北条司Illustrations》中的访谈（待搜集）](./hojocn/zz_illustrations1991/interview.md)  
- [(2000)北条司漫画家20周年纪念(访谈)](./hojocn/zz_anniversary_20th/anni_20th_interview.md)， 
    - [评论](./hojocn/zz_anniversary_20th/reviews.md)  
- [(2000)大美术 20周年纪念](./hojocn/zz_anniversary_20th/dameishu.md)  
- [(2002)Hunting Manga Dreams in the City](./zz/2002-08_hunting_manga_dreams_in_the_city.md)  
- [(2004年02月)"北条司 无尽的魅力"专访|CityHunter 完全版 Volume Y](./hojocn/zz_ch_vol-y/ch5_interview.md)  
- [(2004年08月)神谷明 专访|CityHunter 完全版 Volume X](./hojocn/zz_ch_vol-x/ch5_interview.md)  
- [(2008)Angel Heart Official Guide Book 中的专访](./hojocn/zz_ahg_interview/readme.md))  
- [北条司 拾遗集 画业35周年纪念](./hojocn/zz_anniversary_35th/readme.md)  
- [(2013)Message from Tsukasa Hojo](./hojocn/zz_www.manga-audition.com/zz_message/README.md)  
- [(2014～2015)AH里最受欢迎的10个故事(访谈)](./hojocn/ah2_vol10_best_EP_10/readme.md)  
- [(2015)北条司访谈: "Until the very last moment"](./hojocn/zz_www.manga-audition.com/zz_until-the-very-last-moment/README.md)  
- [(2015)北条司访谈: "Music to my Eyes"](./hojocn/zz_www.manga-audition.com/zz_music_to_my_eyes/README.md)  
- [(2017)井上武彦 & 北条司：漫画师徒 - 独家报道！](./hojocn/zz_www.manga-audition.com/zz_takehiko_hojo/readme.md)  
- [(2017)北条司和井上雄彦谈到了那部杰作的另一面!　一场极好的师徒对话已经实现了!](./hojocn/zz_ddnavi.com/2017-02-06.md)  
- [(2017)一个无与伦比的好色之徒! 传说中的清道夫冴羽獠的32年! 从《城市猎人》到《天使心》!](./hojocn/zz_ddnavi.com/2017-09-11.md)  
- [(2018)成为漫画家 之二 北条司](./hojocn/zz_www.manga-audition.com/zz_making_a_mangaka/readme.md)  
- [(2019)「剧场版CityHunter <新宿Private Eyes>」特集  北条司（原作者）x神谷明（冴羽獠的声优）Interview](./hojocn/zz_natalie.mu/2019-02-01_ch_movie01.md)  
- [(2019)从《城市猎人》到《天使心》的结束! 冴羽獠相伴的32年【采访】](./hojocn/zz_ddnavi.com/2019-03-01.md)  
- [(2019)北条司于60岁当导演！ 无对白的《天使印记》的吸引力是什么？](./hojocn/zz_moviewalker.jp/2019-11-16(2).md)  
- [(2022)鲁邦三世VSCat'sEye - 週刊文春（待搜集）](./hojocn/zz_weeklyBunShun/zz_interview_2022.md)  
- [(2023)Cat's Eye之北条司采访，与鲁邦三世对决「乐在其中」](./hojocn/zz_www.47news.jp/2023-01-25_lupin_vs_ce.md)  
- [(2023)北条司「Cat's Eye」超过原作40周年，揭示最終回里名台词秘话&不画续集的理由](./hojocn/zz_moviewalker.jp/2023-01-26_lupin_vs_ce.md)  
- (2024)铃木亮平、北条司「城市猎人」对话： 
  [@CinemaToday](./hojocn/zz_www.cinematoday.jp/2024-04-20.md)， 
  [@ComicZenon](./hojocn/zz_comic-zenon.com/2024-04-26_ch_talk.md) （不完整，待搜集）  
- [(2024)｢城市猎人｣人气再受关注。漫画家北条司的40年](./hojocn/zz_www.nikkei.com/2024-06-11_interview.md)  

## 其它:  
- [关于北条司最喜欢的5本书](./hojocn/hojo_best5books/readme.md)  
- [北条司作品中的重要日期](./hojocn/hojo_big_days_in_the_works/hojo_big_days_in_the_works.md)  
- [北条司作品研究纲领](./hojocn/hojo_program/hojo_program.md)  
- [北条司作品里某些人物的相貌原型(猜测)](./hojocn/hojo_prototype/hojo_prototype.md)  
- [连载漫画作品写实程度的一种衡量方法](./hojocn/hojo_realistic/readme.md)  
- [关于北条司的签名](./hojocn/hojo_signature/readme.md)  
- [“日本福冈县的北条氏“ 与 “日本历史上的北条氏“](./hojocn/fc_surname_vs_jap_surname/surname_hojo.md)  
- [与北条司相关的论文](./hojocn/zz_papers/zz_papers.md)  
- [北条司作品卷首语](./hojocn/zz_preambles/readme.md)  
- [聊天机器人回答北条司作品相关问题](./hojocn/aigc_qa/readme.md)  


## 转载、翻译:  
- [CH同人 - 飛ばない鳥は(by たくさんの大好きを。)](./hojocn/zz_9393279339.amebaownd.com/the_flightless_bird.md)  
- [[zz]animenewsnetwork.com上与北条司相关的新闻](./hojocn/zz_animenewsnetwork.com/readme.md)    
- [[zz]海坊主闭眼识世界的能力是真实存在的](./hojocn/zz_blind/readme.md)  
- [[zz]blogspot.com上与北条司及其作品相关的信息](./hojocn/zz_blogspot.com/readme.md)  
- [[zz]book.pia.co.jp上与北条司及其作品相关的信息](./hojocn/zz_book.pia.co.jp/readme.md)
- [[zz]comicarttracker.com上与北条司及其作品相关的信息](./hojocn/zz_comicarttracker.com/readme.md)  
- [[zz]geminight.com上和北条司及其作品相关的信息](./hojocn/zz_ce_geminight/img_geminight.md)  
(CE动画线稿)  
- [[zz]www.paffio.it上和北条司及其作品相关的信息](./hojocn/zz_ce_paffio/paffio.md)  
    - OCCHI DI GATTO Season1 DVD (Cat's Eye线稿、猫眼咖啡屋布局的示意图。)  
- [[zz]cityhunter-movie.com上和北条司及其作品相关的信息](./hojocn/zz_cityhunter-movie.com/readme.md)  
- [[zz]crank-in.net上和北条司及其作品相关的信息](./hojocn/zz_crank-in.net/readme.md)  
- [[zz]ddnavi.com上与北条司及其作品相关的信息](./hojocn/zz_ddnavi.com/readme.md)（2014年至今，文章数：25+）  
- [[zz]edition-88.com上和北条司及其作品相关的信息](./hojocn/zz_edition-88.com/readme.md)  
- [[zz]fanfiction上的同人作品](./hojocn/zz_fanfiction/):  
    - [[FC]"我也想道歉"](./hojocn/zz_fanfiction/fc_I_too_have_to_apologize.md)  
- [[zz]Family Compo 里的转义词语（tvtropes版）](./hojocn/zz_fc_tropes/tv_tropes.md)  
- [[zz]Family Compo 里的转义词语（allthetropes版）](./hojocn/zz_fc_tropes/all_the_tropes.md)  
- [[zz]《Forever Tsukasa Hojo(永远的北条司)》](./hojocn/zz_forever_tsukasa_hojo/readme.md)  
- [[zz]第23届Golden Romics之北条司](./hojocn/zz_golden_romics_xxiii/README.md)  
- [[zz]HFC上的同人作品](./hojocn/zz_hfc/readme.md)  
- [[zz]HotToys上与北条司及其作品相关的信息](./hojocn/zz_hottoys.jp/readme.md)  
- [[zz]japan-expo-paris.com上和北条司及其作品相关的信息](./hojocn/zz_japan_expo_paris/readme.md)  
- [[zz]日本Jump粉丝排名了那些想继续看到的80年代漫画](./hojocn/zz_jump_reboot/readme.md)  
- [[zz]store.line.me上的表情包](./hojocn/zz_store.line.me/ch_ah_faces.md)  
- [[zz]comicvine.gamespot.com上和北条司及其作品相关的信息](./hojocn/zz_comicvine.gamespot.com/readme.md)  
    - 少年Jump的一些封面  
    - 漫画杂志ALLMan的封面  
- [[zz]可以观看动画片「Cat's Eye」系列的视频发送服务(VOD)比较](./hojocn/zz_me-houdai.com/2019-01_ce_vod.md)  
- [[zz]moviewalker.jp上和北条司及其作品相关的信息](./hojocn/zz_moviewalker.jp/readme.md)  
- [[zz]natalie.mu上和北条司及其作品相关的信息](./hojocn/zz_natalie.mu/readme.md):(2008年至今，文章数：300+)  
    - [特集・Interview](./hojocn/zz_natalie.mu/readme.md#pp)  
    - [News](./hojocn/zz_natalie.mu/readme.md#news)  
- [[zz]note.com上的相关信息](./hojocn/zz_note.com/readme.md)
- [[zz]FC的两个法语版(Panini版、Tonkam版)的比较](./hojocn/zz_panini_vs_tonkam/README.md)  
- [[zz]ramenparados.com上和北条司及其作品相关的信息](./hojocn/zz_ramenparados):  
    - [Arechi漫画获得《城市猎人》漫画的授权](./hojocn/zz_ramenparados/2020-10-27.md)  
- [[zz]4月，猫眼与城市猎人之父 北条司将会出席Romics大会](./hojocn/zz_romics23/README.md)  
- [[zz]sina.com.cn上和北条司及其作品相关的信息](./hojocn/zz_sina.com.cn):  
    - [[zz]日本写实画风漫画家总结](./hojocn/zz_sina.com.cn/artists_with_realistic_drawing_style.md)  
- [[zz]weibo.cn上和北条司及其作品相关的信息](./hojocn/zz_weibo.cn/readme.md)  
- [times.abema.tv上和北条司及其作品相关的信息](./hojocn/zz_times.abema.tv):  
    - [佐藤蓝子20年来首次和北条司重逢，并为不可思议的联系而兴奋](./hojocn/zz_times.abema.tv/readme.md)  
- [Twitter上和北条司及其作品相关的信息](./hojocn/zz_twitter/readme.md)  
    - pound_poundy的同人作品《つなぐ》   
- [The History Of Japan](./hojocn/history_japan/readme.md)  
    - [[zz]Category 東京都港区の歴史 - Wikipedia](./hojocn/history_japan/Category%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%AF%E5%8C%BA%E3%81%AE%E6%AD%B4%E5%8F%B2%20-%20Wikipedia.html)  
- [[zz]Hipster Manga: Angel Heart](./hojocn/zz_womenwriteaboutcomics.com/hipster_manga_ah.md)  

---

- [[zz]F.COMPO応援隊(备份+中英文翻译)](./hojocn/zz_www.biglobe.ne.jp_julianus/readme.md)， 
    - [[zz]北条司応援隊(备份+中英文翻译)](./hojocn/zz_www.biglobe.ne.jp_julianus/support.md)  
- [[zz]北条司研究序说(HTKJ)(中文翻译)](./hojocn/zz_htkj_cn/readme.md)  
- [[zz]Sweeper Office(中文翻译)](./hojocn/zz_sweeper-office_cn/readme.md)  

---  

- [[zz]历代《少年Jump》周刊的信息，以及其中对北条司的介绍](./hojocn/zz_www.biwa.ne.jp/readme.md)  
- [[zz]www.cinematoday.jp上和北条司及其作品相关的信息](./hojocn/zz_www.cinematoday.jp/readme.md)  
- [[zz]www.coamix.co.jp上和北条司及其作品相关的信息](./hojocn/zz_www.coamix.co.jp/readme.md)  
- [[zz]comic-zenon.com上和北条司及其作品相关的信息](./hojocn/zz_comic-zenon.com/readme.md)  
- [[zz]www.manga-audition.com上与北条司及其作品相关的信息](./hojocn/zz_www.manga-audition.com/readme.md):    
    - (2019.11)2019熊本国际漫画营的报道  
    - (2018.05)成为漫画家 之二 北条司  
    - (2018.04)SMA’s Italian Job  
    - (2017.01)井上雄彦 & 北条司：漫画师徒 - 独家报道！  
    - (2016.03.16)SMA MASTERCLASS 2016  
    - (2015.07.25)北条司签售会  
    - (2015.05.05)THE SHOW AIN'T OVER YET!!  
    - (2015.04)北条司访谈: "Until the very last moment"  
    - (2015.02)北条司访谈: "Music to my Eyes"  
    - (2013.08)Message from Tsukasa Hojo  
    - Japanese Manga 101/  
        - “双页胜过单页” / JapaneseManga101 ＃002 双页展开(2014.12)  
        - ...  
    - Kakimoji S.O.S./  
    - 北条司简介  
- [[zz]MangaHot上和北条司及其作品相关的信息](./hojocn/zz_mangahot.jp/readme.md)  
- [[zz]www.douban.com上和北条司及其作品相关的信息](./hojocn/zz_www.douban.com/readme.md)  
- [[zz]官方网站的一些历史数据（含部分中文翻译）](./hojocn/zz_www.hojo-tsukasa.com/readme.md)  
- [[zz]www.ktqmm.jp上与北条司相关的信息](./hojocn/zz_www.ktqmm.jp/readme.md)  
- [[zz]forum.gamer.com.tw上与北条司相关的信息](./hojocn/zz_forum.gamer.com.tw/readme.md)  
    - 城市獵人－台灣歷史軌跡  
    - 城市獵人－台灣歷史軌跡-續   
- [[zz]CharacterDesignReferences.com上与北条司相关的信息](./hojocn/zz_characterdesignreferences.com/readme.md)  
    - [Art of Cat's Eye](./hojocn/zz_characterdesignreferences.com/art-of-catseye.md)  
    - [Art of City Hunter](./hojocn/zz_characterdesignreferences.com/art-of-cityhunter.md)  
- [[zz]转载未分类 - 目录](hojocn/zz/index.md):    
    - [(2011-03-29)A Koala's Guide to the Best Japanese Mangas](./hojocn/zz/2011-03-29_koala_guide_to_best_jap_mangas.md)  
    - [(2014-11-01)Anime’s 10 biggest male perverts, as chosen by Japanese fans](./hojocn/zz/2014-11-01_anime_top10_male_pervert.md)  
    - [(2020-11-03)The Heroines of Comics: Japan](./hojocn/zz/2020-11-03_heromines_of_comics_japan.md)  

---  

## Topics  

### 2024  
- [北条司展(2024)](./hojocn/ch_40th/readme.md)  
- [[zz]Netflix版City Hunter电影（2024）的相关信息](./hojocn/ch_movie_netflix/readme.md)  


### 2023  

- 「鲁邦三世 VS Cat's Eye」:  
    - [[zz]Cat's Eye之北条司采访，与鲁邦三世对决「乐在其中」](./hojocn/zz_www.47news.jp/2023-01-25_lupin_vs_ce.md)  
    - [[zz]北条司「Cat's Eye」超过原作40周年，揭示最終回里名台词秘话&不画续集的理由](./hojocn/zz_moviewalker.jp/2023-01-26_lupin_vs_ce.md)  

### 2015  
- [City Hunter 30周年](./hojocn/ch_30th/readme.md)  
    - 原画展  
    - 30问答  
